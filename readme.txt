Before replacing the 404 Redirect gadget with a newer version, keep in consideration the following changes.
1. 	Done changes in the 
	public CustomRedirect Find(string oldUrl) function 
	of CustomRedirectCollection class 
	in BVNetwork.FileNotFound.Redirects namespace
	present in location \EpiCode.404Handler\404Handler\Bvn\FileNotFound\CustomRedirects\CustomRedirectCollection.cs