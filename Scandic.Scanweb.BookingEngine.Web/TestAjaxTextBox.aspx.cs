using System;
using System.Collections.Generic;

namespace Scandic.Scanweb.BookingEngine.Web
{
    public partial class TestAjaxTextBox : EPiServer.TemplatePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected List<string> ExcludeStrings()
        {
            List<string> excludeStrs = new List<string>();
            // Read through the details
            for (int i = 0; i < 2; i++)
            {
                excludeStrs.Add("Scandic");
            }
            return excludeStrs;
        }
    }
}