﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Scandic.Scanweb.BookingEngine.Web.HotelTimezone
{
    public class HotelTimeZones
    {
        [XmlElement("Hotel")]
        public List<HotelTimeZone> Hotels { get; set; }
    }
}
