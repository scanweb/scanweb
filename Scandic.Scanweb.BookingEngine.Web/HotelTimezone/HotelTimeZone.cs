﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Scandic.Scanweb.BookingEngine.Web.HotelTimezone
{
    [XmlRoot("Hotel")]
    public class HotelTimeZone
    {
        [XmlAttribute("id")]
        public string HotelId { get; set; }
        [XmlAttribute("timezoneOffset")]
        public double TimeZoneOffeset { get; set; }
    }
}
