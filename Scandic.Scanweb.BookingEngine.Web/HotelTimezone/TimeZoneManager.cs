﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.XPath;
using System.Xml.Serialization;
using System.Xml;
using System.Text;

namespace Scandic.Scanweb.BookingEngine.Web.HotelTimezone
{
    /// <summary>
    /// This class expose method to get the current time of hotel with regard to CET timezone
    /// </summary>
    public class TimeZoneManager
    {
        #region Declaration
        private static TimeZoneManager instance;
        private static object syncLock = new object();
        private HotelTimeZones hotelTimeZones;
        #endregion

        #region Constructor
        public TimeZoneManager()
        {
            LoadHotelTimezones();
        }
        #endregion

        #region Property
        public static TimeZoneManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncLock)
                    {
                        if (instance == null)
                        {
                            instance = new TimeZoneManager();
                        }
                    }
                }
                return instance;
            }
        }
        #endregion

        #region Methods

        private void LoadHotelTimezones()
        {
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string xmlFileName = HttpContext.Current.Server.MapPath("~/HotelTimeZones.xml");
                xmlDoc.Load(xmlFileName);
                XmlNode section = xmlDoc.DocumentElement;

                hotelTimeZones = LoadXML(section);
            }
            finally
            {
                xmlDoc = null;
            }
        }

        private HotelTimeZones LoadXML(System.Xml.XmlNode section)
        {
            HotelTimeZones settings = null;
            if (section == null)
            {
                return settings;
            }
            XPathNavigator navigator = section.CreateNavigator();
            String typeName = (string)navigator.Evaluate("string(@type)");
            Type sectionType = Type.GetType(typeName);

            XmlSerializer xs = new XmlSerializer(sectionType);
            XmlNodeReader reader = new XmlNodeReader(section);

            try
            {
                settings = xs.Deserialize(reader) as HotelTimeZones;
            }
            catch (Exception ex)
            {
                StringBuilder strErrorMessage = new StringBuilder();
                strErrorMessage.Append("Unable to Load Hotel timezone xml: ");
                strErrorMessage.Append(ex.Message);
                throw new ApplicationException(strErrorMessage.ToString(), ex);
            }
            finally
            {
                xs = null;
            }
            return settings;
        }

        public DateTime GetHotelCurrentTime(string hotelId)
        {
            var cet = TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time");
            var currentTime = TimeZoneInfo.ConvertTime(DateTime.Now, cet);
            var hotelTime = currentTime;
            HotelTimeZone hotelTimeZone = null;
            if (hotelTimeZones != null && hotelTimeZones.Hotels != null && hotelTimeZones.Hotels.Count > 0)
            {
                hotelTimeZone = hotelTimeZones.Hotels.Where(timeZone => string.Equals(timeZone.HotelId, hotelId, StringComparison.InvariantCultureIgnoreCase)
                                            ).FirstOrDefault<HotelTimeZone>();
            }

            if (hotelTimeZone != null)
            {
                hotelTime = currentTime.AddHours(hotelTimeZone.TimeZoneOffeset);
            }
            return hotelTime;
        }

        #endregion
    }
}
