<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">

    protected void SendSMS_Click(object sender, EventArgs e)
    {
        Scandic.Scanweb.CMS.code.Util.SMSManager.SMSManager smsManager =
            new Scandic.Scanweb.CMS.code.Util.SMSManager.SMSManager(mobile.Value, message.Value);
        if (!smsManager.SendSMS())
        {
            error.Visible = true;
            error.InnerHtml = "Message: " + message.Value + " cannot be sent to " + mobile.Value +
                              " <br/> Due to error::::: " + smsManager.errMessage;
        }
        else
        {
            success.Visible = true;
            success.InnerHtml = "Message: " + message.Value + " sent successfully to " + mobile.Value;
        }
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>SMS Test</title>
</head>
<body>
	<form id="form1" runat="server">
	    <div id="error" runat=server style="color:Red" visible=false></div>
	    <div id="success" runat=server style="color:Green" visible=false></div>
	    Mobile Number: <input type="text" id="mobile" runat=server />
		<br/>
		<br/>
	    Message: <input type="text" id="message" runat=server />
		<br/>
		<br/>
	    <asp:Button ID="SendSMS" runat="server" OnClick="SendSMS_Click" Text="Send SMS" />
	</form>
</body>
</html>
