<%@ Page language="c#" Inherits="Scandic.Scanweb.BookingEngine.Web.Error" Codebehind="Error.aspx.cs" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Error</title>
   <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/common.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/booking.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/calendar.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/reservation.css") %>?v=<%=CmsUtil.GetCSSVersion()%>" />
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/loyalty.css") %>" />
   <style> 
    .scandicErrorMsg{
	width: 460px;
	margin: 100px auto;
	padding: 2px;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 0.75em;
	border: 1px solid #d4d4d4;
	color: #5e5e5e;	
}
.scandicErrorMsgContent{
	padding: 35px 23px 20px;
	background: #fafafa;
}
.scandicErrorMsg h3{
	margin: 0 0 19px;
	padding: 0 0 0 26px;
	font-size: 16px;
	font-weight: bold;
	line-height: 20px;
	background: url('/Templates/Booking/Images/red_alert_icon.gif') 0 2px no-repeat;
	color: #d81a20;
}
.scandicErrorMsg p{
	margin: 0 0 10px;
	line-height: 18px;
}
.scandicErrorMsg a{
	text-decoration: underline;
	color: #069;
}
.scandicErrorMsg a:hover, .scandicErrorMsg a:active,
.scandicErrorMsg a:focus{text-decoration: none;}
    </style>
</head>
<body>
    <form runat="server" id="form1"> 
    <div id="ErrorPage" class="BE">


    <div class="scandicErrorMsg">
		<div class="scandicErrorMsgContent">
			<h3><asp:Label runat="server" ID="lblHeader" /></h3>
			<p><asp:Label runat="server" ID="errMsgContent" /></p>
			
		</div>
	</div>
    
    
    
	<%--<div class="formGroupError">
		<div class="redAlertIcon"><%=WebUtil.GetTranslatedText("/bookingengine/booking/errorpage/apperrhead")%></div>
		<div class="errorMsg">
		    <div id = "errMsgContent" runat="server"></div>
			</div>
		<div class="columnOne">
			<p class="link">
				<a href="javascript:history.go(-1)"><%=WebUtil.GetTranslatedText("/bookingengine/booking/errorpage/previousscreen")%></a>
			</p>
			<div class="hr">&nbsp;</div>
			<p class="link">
				<a href="/."><%=WebUtil.GetTranslatedText("/bookingengine/booking/errorpage/redirecttohome")%></a>
			</p>
		</div>
	</div>--%>
</div>
    </form>
</body>
</html> 