using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;

#region Scandic Namespaces

#endregion Scandic Namespaces

namespace Scandic.Scanweb.BookingEngine.Web
{
    public partial class TestLogin : EPiServer.TemplatePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserLoggedInSessionWrapper.UserLoggedIn)
            {
                Response.Expires = -1;
                Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
                Response.AddHeader("pragma", "no-cache");
                Response.AddHeader("cache-control", "private");
                Response.CacheControl = "no-cache";
            }
        }
    }
}