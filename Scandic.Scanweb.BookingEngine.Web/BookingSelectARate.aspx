<%@ Page language="c#" Inherits="Scandic.Scanweb.BookingEngine.Web.BookingSelectARate" Codebehind="BookingSelectARate.aspx.cs" %>
<%@ Register Src="~/Templates/Booking/Units/SelectRate.ascx" TagName="SelectRate" TagPrefix="uc1" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style>
body {
	color:#666666;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 0.75em;
	font-weight:normal;
	line-height:1.7em;
	text-align:center;
}
</style>
<link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/reservation.css") %>?v=<%=CmsUtil.GetCSSVersion()%>" />
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/Javascript/dhtml_utils.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/Javascript/reservation.js") %>"></script>
<title>Reservation - Select Rate</title>
</head>
<body>
    <form id="a">
    <uc1:SelectRate ID="SelectRate1" runat="server">
    </uc1:SelectRate>&nbsp;</body>
    </form>
</html>