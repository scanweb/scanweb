<%@ Page language="c#" Inherits="Scandic.Scanweb.BookingEngine.Web.EnrollGuestProg" Codebehind="EnrollGuestProg.aspx.cs" %>

<%@ Register Src="Templates/Booking/Units/UpdateMessageHeader.ascx" TagName="UpdateMessageHeader"
    TagPrefix="uc2" %>
<%@ Register Src="Templates/Booking/Units/EnrollGuestProgram.ascx" TagName="EnrollGuestProgram"
    TagPrefix="uc1" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head1" runat="server">
    <title>EnrollGuestProg</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
body {
	color:#666666;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 0.75em;
	font-weight:normal;
	line-height:1.7em;
	text-align:center;
}
</style>
<link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/common.css") %>" />
<link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/booking.css") %>" />
<link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/calendar.css") %>" />
<link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/reservation.css") %>?v=<%=CmsUtil.GetCSSVersion()%>" />
<link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/loyalty.css") %>" />
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/prototype.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/scriptaculous.js?load=builder") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/init.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/dhtml_utils.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/autoSuggest.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/booking.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/initCalendar.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/dateManip.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/calendar.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/preCalendar.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/validation.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/AjaxFramework.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/Common.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/AjaxCall.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/reservation.js") %>"></script>
</head>
<body>
    <form runat="server" id="form1">
    <div>
        <uc2:UpdateMessageHeader id="UpdateMessageHeader1" runat="server">
        </uc2:UpdateMessageHeader><br />
        <uc1:EnrollGuestProgram id="EnrollGuestProgram1" runat="server">
        </uc1:EnrollGuestProgram>
        &nbsp;</div>
    </form>
</body>
</html>