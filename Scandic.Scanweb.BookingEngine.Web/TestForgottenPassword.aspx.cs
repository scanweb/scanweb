using System;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// Test Forgotten Password
    /// </summary>
    public partial class TestForgottenPassword : EPiServer.TemplatePage
    {
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}