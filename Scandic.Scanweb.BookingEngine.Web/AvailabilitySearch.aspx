<%@ Page language="c#" Inherits="Scandic.Scanweb.BookingEngine.Web.AvailabilitySearch" Codebehind="AvailabilitySearch.aspx.cs" %>
<%@ Register Src="~/Templates/Booking/Units/BookingModuleBig.ascx" TagName="Availability" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style>
body {
	color:#666666;
	font-size: 0.75em;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight:normal;
	line-height:1.7em;
	text-align:center;
}
</style>
<link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/booking.css") %>" />
<link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/calendar.css") %>" />
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/init.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/dhtml_utils.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/autoSuggest.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/booking.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/initCalendar.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/dateManip.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/calendar.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/preCalendar.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/Validation.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/AjaxFramework.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/Common.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/AjaxCall.js") %>"></script>

<title>Booking Module</title>
</head>
<body>
    <form id = "BookingForm" runat="server">
        <uc1:Availability ID="BE" runat="server">
        </uc1:Availability>&nbsp;            
    </form>        
</body>
</html>