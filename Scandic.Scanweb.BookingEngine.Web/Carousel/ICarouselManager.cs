﻿using EPiServer.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Carousel
{
    public interface ICarouselManager
    {
        CarouselEntity GetCarouselData(PageData pageData);
    }
}
