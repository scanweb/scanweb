﻿namespace Scandic.Scanweb.BookingEngine.Carousel
{
    public struct  CarouselConstants
    {
        public const string ActivateImageCarousel = "ActivateImageCarousel";
        public const string Alignment = "Width";
        public const string AutoSlideRequired = "AutoSlideRequired";
        public const string ImageUrl = "ImageUrl";
        public const string AltText = "AltText";
        public const string HotelPageTypeId = "33";
        public const string TimeOut = "CarouselTimeOut";
        public const string TransitionEffect = "TransitionEffect";
        public const string UseInImageCaraousel = "UseInImageCaraousel";
        public const int ImageCount = 10;
        public const string GeneralImage = "GeneralImage";
        public const string RestBarImage = "RestBarImage";
        public const string RoomImage = "RoomImage";
        public const string LeisureImage = "LeisureImage";
        public const string UseFor = "UseFor";
        public const string Title = "Title";

        //Hotel overview and FGP Overview 
        public const string OverViewLeftCenterBoxHeight = "492";
        public const string OverViewLeftCenterBoxWidth = "265";

        public const string OverViewLeftCenterNoBoxWidth = "718";
        public const string OverViewLeftCenterNoBoxHeight = "265";


        //Standard Pages
        public const string StandardCenterWidth = "472";
        public const string StandardCenterHeight = "265";

        public const string StandardCenterRightWidth = "718";
        public const string StandardCenterRightHeight = "265";

        public const string StandardFullPageWidth = "964";
        public const string StandardFullPageHeight = "265";


        //offer page
        public const string OfferWidth = "595";
        public const string OfferHeight = "265";

        //hotel room description
        public const string RoomDescriptionWidth = "472";
        public const string RoomDescriptionHeight = "265";

        public const string StandardPageId = "45";
        public const string HotelOverViewPageId = "33";
        public const string FGPOverViewPageId = "92";
        public const string OfferPageId = "30";
        public const string HotelRoomDescriptionId = "34";

        //Standard Page Alignment types
        public const string CenterColumn = "100";
        public const string CenterRight = "101";
        public const string FullPage = "102";


        //Overview page Alignment type
        public const string LeftCenterWithBox = "200";
        public const string LeftCenterNoBox = "201";


    }
}
