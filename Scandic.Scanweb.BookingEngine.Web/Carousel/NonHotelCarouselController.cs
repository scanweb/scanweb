﻿using System;
using System.Configuration;
using System.Globalization;
using System.Text;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Entity;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Web.Carousel;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;

namespace Scandic.Scanweb.BookingEngine.Carousel
{
    public class NonHotelCarouselController : ICarouselManager
    {
        //Dictionary<int,StringBuilder>[] alignmentDictionary;
        public CarouselEntity GetCarouselData(PageData pageData)
        {
            CarouselEntity carouselEntity = null;
           
            if (pageData != null)
            {
                carouselEntity = new CarouselEntity();
                carouselEntity.PageTypeId = pageData.PageTypeID.ToString();
                carouselEntity.ActivateImageCaraousel = pageData[CarouselConstants.ActivateImageCarousel] !=null ? bool.Parse(pageData[CarouselConstants.ActivateImageCarousel].ToString()) : false;
                carouselEntity.AutoSlideRequired = pageData[CarouselConstants.AutoSlideRequired] !=null ? bool.Parse(pageData[CarouselConstants.AutoSlideRequired].ToString()) : false;
                carouselEntity.Alignment = pageData[CarouselConstants.Alignment] !=null ? pageData[CarouselConstants.Alignment].ToString() : "0";
                carouselEntity.TimeOut = Convert.ToInt32(ConfigurationManager.AppSettings[CarouselConstants.TimeOut]);
                carouselEntity.TransitionEffect =
                    ConfigurationManager.AppSettings[CarouselConstants.TransitionEffect].ToString(
                        CultureInfo.InvariantCulture);
               
                //get Image Height and Width
                CreateAlignmentDictionary();
                var carouselUtilityObj = new CarouselUtility();
                var carouselDimensionsObj = new CarouselDimensions();
                carouselDimensionsObj = carouselUtilityObj.GetCarouselDimentions(pageData.PageTypeID.ToString(), carouselEntity.Alignment.ToString());
                carouselEntity.Width = carouselDimensionsObj.Width;
                carouselEntity.Height = carouselDimensionsObj.Height;
                carouselEntity.SlideCss = carouselDimensionsObj.CssStyle.Slide;
                carouselEntity.LeftCss = carouselDimensionsObj.CssStyle.Left;
                carouselEntity.RightCss = carouselDimensionsObj.CssStyle.Right;
                carouselEntity.TopCss = carouselDimensionsObj.CssStyle.Top;
                carouselEntity.PlaceHolder = carouselDimensionsObj.CssStyle.PlaceHolder;
                carouselEntity.ImageList = GetImageListFromPageData(pageData, carouselEntity.Width);
                
            }

            return carouselEntity;
        }

        private void CreateAlignmentDictionary()
        {   
            
          

        }

        private string[] GetHeightWidthOfImage(string p, PageData pageData)
        {
           string[] arrDimensions= new string[2];
          
           
           return arrDimensions;

        }
        private List<CarouselImageEntity> GetImageListFromPageData(PageData pageData, string width)
        {
            var imageList = new List<CarouselImageEntity>();
            int imageWidth = Convert.ToInt32(width);
            if (pageData != null)
            {

                for (var i = 1; i <= CarouselConstants.ImageCount; i++)
                {
                    if (pageData["Image" + i] !=null && !string.IsNullOrEmpty(pageData["Image" + i].ToString()))
                    {
                        var altText = new ImageAltText();
                        var image = new CarouselImageEntity()
                                        {
                                            ImageUrl = WebUtil.GetImageVaultImageUrl(pageData["Image" + i].ToString(),imageWidth),
                                            AltText = altText.GetAltText(pageData.LanguageID, pageData["Image" + i].ToString())
                                        };
                        imageList.Add(image);
                    }
                }
            }
            return imageList;
        }
    }
}
