﻿
namespace Scandic.Scanweb.BookingEngine.Carousel
{
    public static class CarouselFactory 
    {
        public static ICarouselManager CreateInstance(string pageTypeId)
        {
            switch (pageTypeId)
            {
                case CarouselConstants.HotelPageTypeId:
                                return new HotelCarouselController();
                                break;
                default:
                                return new NonHotelCarouselController();
                                break;
            }
        }
    }
}
