﻿using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using System.Xml;
using Scandic.Scanweb.BookingEngine.Carousel;
using EPiServer.Core;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Core.Core;


namespace Scandic.Scanweb.BookingEngine.Web.Carousel
{
    /// <summary>
    /// Make this class as singleton
    /// </summary>
    public class CarouselUtility
    {
        public CarouselDimensions GetCarouselDimentions(string pageTypeId, string templateId)
        {
            var xmlDocument = GetCarouselDimentionsXml();

            if (pageTypeId == CarouselConstants.OfferPageId || pageTypeId == CarouselConstants.HotelRoomDescriptionId)
                templateId = "-1";
            
            string xPathQuery =string.Format("//PageType [@Id='{0}']/Template [@Id = '{1}']",pageTypeId,templateId);
            if (pageTypeId == CarouselConstants.OfferPageId || pageTypeId == CarouselConstants.HotelRoomDescriptionId)
                templateId = "-1";

            var nodeDimentions = xmlDocument.SelectSingleNode(xPathQuery);

            //1. filter node with xpath query / xml 2 linq
            //2. map result node to CarouselDimentions object
            var carouselDimentions = new CarouselDimensions
                                         {
                                             Height = nodeDimentions.SelectSingleNode("Dimentions/Height").InnerText,
                                             Width = nodeDimentions.SelectSingleNode("Dimentions/Width").InnerText,
                                             CssStyle = GetCssStyles(nodeDimentions)
                                         };

            return carouselDimentions; //parse this as json object.
        }

        private CssStyles GetCssStyles(XmlNode node)
        {
            //1.Process xml node and return css styles
            //return new CssStyles { Left = node.SelectSingleNode("CssClass/Left").InnerText, Right = node.SelectSingleNode("CssClass/Right").InnerText, Top = node.SelectSingleNode("CssClass/Top").InnerText};
            return new CssStyles { Left = node.SelectSingleNode("CssClass/Left").InnerText, 
                                   Right = node.SelectSingleNode("CssClass/Right").InnerText, 
                                   Top = node.SelectSingleNode("CssClass/Top").InnerText, 
                                   Slide = node.SelectSingleNode("CssClass/Slides").InnerText,
                                   PlaceHolder = node.SelectSingleNode("CssClass/PlaceHolder") != null ? node.SelectSingleNode("CssClass/PlaceHolder").InnerText : string.Empty,
            };
        }
        private XmlDocument GetCarouselDimentionsXml()
        {
            if (ScanwebCacheManager.Instance.LookInCache<XmlDocument>("CarouselDimensionXML") == null)
            {
                //XmlTextReader xtxtReader = new XmlTextReader(HttpContext.Current.Server.MapPath("CarouselDimentions.xml"));
                string xmlFileName = HttpContext.Current.Server.MapPath("~/CarouselDimentions.xml");
                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(xmlFileName);
                // xdoc.Load(xtxtReader);
                ScanwebCacheManager.Instance.AddToCache("CarouselDimensionXML", xdoc, xmlFileName);
                return xdoc;
            }
            else
            {
                return ScanwebCacheManager.Instance.LookInCache<XmlDocument>("CarouselDimensionXML");
            }

        }
    

        public void GetNumberOfImageCountHotelOverviewPage(PageData pageData, ref int imageCount)
        {
            CheckForImagesCount(pageData, CarouselConstants.GeneralImage, ref imageCount);
            CheckForImagesCount(pageData, CarouselConstants.RoomImage, ref imageCount);
            CheckForImagesCount(pageData, CarouselConstants.RestBarImage, ref imageCount);
            CheckForImagesCount(pageData, CarouselConstants.LeisureImage, ref imageCount);
        }

        
        public bool CheckForImageCountOtherTemplates(PageData pageData)
        {
          
           for (var i = 1; i <= CarouselConstants.ImageCount; i++)
           {
               if (pageData["Image" + i] != null && !string.IsNullOrEmpty(pageData["Image" + i].ToString()))
               {
                   return true;
               }
           }

           return false;

        }

        
        private void CheckForImagesCount(PageData pageData, string imageType,ref int imageCount) //overloaded function
        {
            string imageField = string.Empty;
            
            for (var i = 1; i <= CarouselConstants.ImageCount; i++)
            {
                string useInImageCarousel = string.Format("{0}{1}{2}", CarouselConstants.UseFor, imageType, i);
                if (pageData[useInImageCarousel] != null && bool.Parse(pageData[useInImageCarousel].ToString()))
                {
                    imageField = pageData[imageType + i] != null ? pageData[imageType + i].ToString() : string.Empty;
                    if (!string.IsNullOrEmpty(imageField))
                    {
                        imageCount++;
                    }
                }
            }
           
        }

    }
}
