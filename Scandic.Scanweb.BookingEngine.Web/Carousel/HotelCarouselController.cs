﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Web.Carousel;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;

namespace Scandic.Scanweb.BookingEngine.Carousel
{
    public class HotelCarouselController : ICarouselManager
    {
        public CarouselEntity GetCarouselData(PageData pageData)
        {
            CarouselEntity carouselEntity = null;
            if (pageData != null)
            {
                carouselEntity = new CarouselEntity();
                carouselEntity.PageTypeId = pageData.PageTypeID.ToString();
                carouselEntity.ActivateImageCaraousel = pageData[CarouselConstants.ActivateImageCarousel]!=null 
                                                        ? bool.Parse(pageData[CarouselConstants.ActivateImageCarousel].ToString())
                                                        : false;
                carouselEntity.AutoSlideRequired = pageData[CarouselConstants.AutoSlideRequired] != null 
                                                        ? bool.Parse(pageData[CarouselConstants.AutoSlideRequired].ToString())
                                                        : false;
                carouselEntity.Alignment = pageData[CarouselConstants.Alignment] !=null 
                                            ? pageData[CarouselConstants.Alignment].ToString()
                                            :"0";
                carouselEntity.TimeOut = Convert.ToInt32(ConfigurationManager.AppSettings[CarouselConstants.TimeOut]);
                carouselEntity.TransitionEffect = ConfigurationManager.AppSettings[CarouselConstants.TransitionEffect].ToString(CultureInfo.InvariantCulture);
               
                var carouselUtilityObj = new CarouselUtility();
                var carouselDimensionsObj = new CarouselDimensions();
                carouselDimensionsObj = carouselUtilityObj.GetCarouselDimentions(pageData.PageTypeID.ToString(), carouselEntity.Alignment.ToString());
                carouselEntity.Width = carouselDimensionsObj.Width;
                carouselEntity.Height = carouselDimensionsObj.Height;
                carouselEntity.ImageList = GetImageListFromPageData(pageData, carouselEntity.Width);
            }

            return carouselEntity;
        }
        private List<CarouselImageEntity> GetImageListFromPageData(PageData pageData, string width )
        {
            List<CarouselImageEntity> imageList = null;
            int ImageWidth = Convert.ToInt32(width);

            if (pageData != null)
            {
                imageList = new List<CarouselImageEntity>();
                imageList.AddRange(GetImageListForACategory(pageData, CarouselConstants.GeneralImage,ImageWidth));
                imageList.AddRange(GetImageListForACategory(pageData, CarouselConstants.RoomImage, ImageWidth));
                imageList.AddRange(GetImageListForACategory(pageData, CarouselConstants.RestBarImage, ImageWidth));
                imageList.AddRange(GetImageListForACategory(pageData, CarouselConstants.LeisureImage, ImageWidth));

            }
            return imageList;
        }
        private IEnumerable<CarouselImageEntity> GetImageListForACategory(PageData pageData, string imageType, int imageWidth)
        {
            var imageList =  new List<CarouselImageEntity>();
            var useInImageCarousel = string.Empty;
            var imageField = string.Empty;            

            for (var i = 1; i <= CarouselConstants.ImageCount; i++)
            {
                useInImageCarousel = string.Format("{0}{1}{2}", CarouselConstants.UseFor, imageType, i);
                if (pageData[useInImageCarousel] != null && bool.Parse(pageData[useInImageCarousel].ToString()) && imageList.Count<=10) //check for 10 images
                {
                    imageField = pageData[imageType + i] != null ? pageData[imageType + i].ToString() : string.Empty;
                    if (!string.IsNullOrEmpty(imageField))
                    {
                        var altText = new ImageAltText();
                        var image = new CarouselImageEntity()
                                        {
                                            ImageUrl = WebUtil.GetImageVaultImageUrl(pageData[imageType + i].ToString(), imageWidth),
                                            AltText = altText.GetAltText(pageData.LanguageID, pageData[imageType + i].ToString())                                                
                                        };
                        imageList.Add(image);
                    }
                }

            }
            return imageList;
        }
    }
}
