﻿
namespace Scandic.Scanweb.BookingEngine.Web.Carousel
{
    public class CarouselDimensions
    {
        public string Width { get; set; }
        public string Height { get; set; }
        public CssStyles CssStyle { get; set; }
    }

    public class CssStyles
    {
        public string Left {get;set; }
        public string Top {get;set; }
        public string Right {get;set; }
        public string Slide {get;set; }
        public string PlaceHolder { get; set; }
    }
}
