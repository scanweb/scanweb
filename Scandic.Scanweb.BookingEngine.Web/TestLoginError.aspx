<%@ Page language="c#" Inherits="Scandic.Scanweb.BookingEngine.Web.TestLoginError" Codebehind="TestLoginError.aspx.cs" %>

<%@ Register Src="Templates/Booking/Units/LoginError.ascx" TagName="LoginError" TagPrefix="uc1" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<%-- To add markup for this WebForm you can do one of two things: 
        1. Use a Masterpage by adding the masterpage attribute in the @Page declaration above.
        2. Add markup directly in this file. You can start by uncommenting the section below.
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head1" runat="server">
    <title>TestLoginError</title>
     <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/common.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/booking.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/calendar.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/reservation.css") %>?v=<%=CmsUtil.GetCSSVersion()%>" />
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/loyalty.css") %>" />

    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/DestinationSearchEntity.js") %>"></script>
    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/init.js") %>"></script>
    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/dhtml_utils.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/autoSuggest.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/booking.js") %>"></script>
    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/initCalendar.js") %>"></script>
    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/dateManip.js") %>"></script>
    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/calendar.js") %>"></script>
    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/preCalendar.js") %>"></script>
    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/validation.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/AjaxFramework.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/Common.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/AjaxCall.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/reservation.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/MissingPointValidation.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/Loyalty.js") %>"></script>

</head>
<body>
    <form runat="server" id="form1">   TestLoginError
    </form>
</body>
</html>
 