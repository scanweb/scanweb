﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.code.Interface
{
    public interface ITripAdvisorRatingProcessor
    {
        TripAdvisorPlaceListEntity GetTripAdvisorRating();
    }
}
