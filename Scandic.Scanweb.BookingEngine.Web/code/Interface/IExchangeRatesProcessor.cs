﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.code.Interface
{
    public interface IExchangeRatesProcessor
    {
 
        //Dictionary<string, string> ExchangeRates { get; set; }
        //string ExchangeRatesProvider { get; set; }
        List<ExchangeRateEntity> GetConvertedExchangeRates(string fromCurrency, string toCurrency, string amount, Dictionary<string, string> allLocaleCurrencies);
    }  
    
}
