﻿
namespace Scandic.Scanweb.BookingEngine.Web.code.Interface
{
    interface ICategoryMaps
    {
         void GetRateTypeCategoryMap();
         void GetRoomTypeCategoryMap();
    }
}
