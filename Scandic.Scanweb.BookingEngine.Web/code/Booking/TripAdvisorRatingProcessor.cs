﻿using System;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Web.code.Interface;
using Scandic.Scanweb.Entity;
using System.Xml;
using System.Configuration;
using Scandic.Scanweb.Core;
using System.Globalization;

namespace Scandic.Scanweb.BookingEngine.Web.code.Booking
{
    public class TripAdvisorRatingProcessor : ITripAdvisorRatingProcessor
    {
        public TripAdvisorPlaceListEntity GetTripAdvisorRating()
        {
            TripAdvisorPlaceListEntity TAPlaceList = new TripAdvisorPlaceListEntity();
            TAPlaceList.TAPlaces = new List<TripAdvisorPlaceEntity>();
            XmlDocument xdom = new XmlDocument();
            try
            {
                string TAFeedProvider = Convert.ToString(ConfigurationManager.AppSettings[AppConstants.TripAdvisorRatingFeedsUrl]);
                if (System.IO.File.Exists(TAFeedProvider))
                {
                    xdom.Load(TAFeedProvider);
                    XmlElement root = xdom.DocumentElement;
                    TAPlaceList = GetALLTripAdvisorRatings(root.ChildNodes);
                }
                else
                    AppLogger.LogInfoMessage("Unable to read from Feed file " + TAFeedProvider + ". Either it does not exists or there is no permission to read it");
            }
            catch (Exception e)
            {
                AppLogger.LogFatalException(e, e.Message);
                return null;
            }

            return TAPlaceList;
        }

        private TripAdvisorPlaceEntity PrepareTARatingEntity(XmlNode place)
        {
            TripAdvisorPlaceEntity tripAdvisorPlaceEntity = new TripAdvisorPlaceEntity();
            tripAdvisorPlaceEntity.ImageURL = new Dictionary<string, string>();
            tripAdvisorPlaceEntity.Id = place.Attributes["id"] != null ? Convert.ToInt32(place.Attributes["id"].Value, CultureInfo.InvariantCulture) : 0;
            tripAdvisorPlaceEntity.LocationId = place.Attributes["geoid"] != null ? Convert.ToString(place.Attributes["geoid"].Value) : null;
            tripAdvisorPlaceEntity.ReviewCount = place.Attributes["reviewcnt"] != null ? Convert.ToInt32(place.Attributes["reviewcnt"].Value, CultureInfo.InvariantCulture) : 0;
            tripAdvisorPlaceEntity.AverageRating = place.Attributes["avgrating"] != null ? Convert.ToDouble(place.Attributes["avgrating"].Value, CultureInfo.InvariantCulture) : 0.0;
            tripAdvisorPlaceEntity.PartnerID = place.SelectSingleNode("PARTNERID") != null ? Convert.ToString(place.SelectSingleNode("PARTNERID").InnerText) : null;

            if (place.SelectNodes("Url") != null & place.SelectNodes("Url").Count > 0)
            {
                XmlNodeList urlList = place.SelectNodes("Url");
                foreach (XmlNode item in urlList)
                {
                    if (!tripAdvisorPlaceEntity.ImageURL.ContainsKey(item.Attributes["lang"] != null ? Convert.ToString(item.Attributes["lang"].Value.ToUpper()) : AppConstants.TRIP_ADVISOR_DEFAULT_URL_LANG))
                        tripAdvisorPlaceEntity.ImageURL.Add(item.Attributes["lang"] != null ? Convert.ToString(item.Attributes["lang"].Value.ToUpper()) : AppConstants.TRIP_ADVISOR_DEFAULT_URL_LANG,
                       Convert.ToString(item.InnerText));
                }
            }
            
            //tripAdvisorPlaceEntity.TripAdvisorImageURL = place.SelectSingleNode("Url") != null ? Convert.ToString(place.SelectSingleNode("Url").InnerText) : null;
            if (tripAdvisorPlaceEntity.ImageURL.Count > 0)
            {
                tripAdvisorPlaceEntity.TripAdvisorImageURL = WebUtil.GetTAImageURLForCurrentCulture(tripAdvisorPlaceEntity.ImageURL);
                tripAdvisorPlaceEntity.TripAdvisorImageDefaultURL = Convert.ToString(tripAdvisorPlaceEntity.ImageURL[AppConstants.TRIP_ADVISOR_DEFAULT_URL_LANG]);
            }
            return tripAdvisorPlaceEntity;
        }
        private TripAdvisorPlaceListEntity GetALLTripAdvisorRatings(XmlNodeList nodeList)
        {
            TripAdvisorPlaceListEntity taPlaceList = new TripAdvisorPlaceListEntity();
            taPlaceList.TAPlaces = new List<TripAdvisorPlaceEntity>();
            foreach (XmlNode xnod in nodeList)
            {
                taPlaceList.TAPlaces.Add(PrepareTARatingEntity(xnod));
            }
            return taPlaceList;
        }
    }
}
