//  Description					:   SelectHotelUtil                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Core.Core;
using Scandic.Scanweb.Entity;
using System.Linq;
using System.Web;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// Contains members of select hotel utility.
    /// </summary>
    public class SelectHotelUtil
    {
        private const string ALL_CITY_MAP = "ALL_CITY_MAP";

        private static AvailabilityController _availabilityController = null;

        /// <summary>
        /// Gets m_AvailabilityController
        /// </summary>
        public static AvailabilityController _AvailabilityController
        {
            get
            {
                if (_availabilityController == null)
                {
                    _availabilityController = new AvailabilityController();
                }
                return _availabilityController;
            }
        }

        /// <summary>
        /// For all the list of hotels saved to the session, if atleast one hotel
        /// has negotiated rate available then true is returned
        /// If none of the hotel has negotiated rates or the search being made is 
        /// not negotiated then false is returned to the user
        /// </summary>
        /// <returns></returns>
        public static bool HasHotelsWithNegotiatedRates()
        {
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
            if (SearchType.CORPORATE != searchCriteria.SearchingType)
            {
                return false;
            }
            else
            {
                return TotalNegotiatedRateAvailableHotels() > 0;
            }

        }

        /// <summary>
        /// Returnt the total number of hotels having negotiated rate available
        /// </summary>
        /// <returns></returns>
        public static int TotalNegotiatedRateAvailableHotels()
        {
            int totalHotels = 0;
            List<IHotelDetails> hotels = HotelResultsSessionWrapper.HotelResults;
            if (null != hotels && hotels.Count > 0)
            {
                foreach (IHotelDetails hotel in hotels)
                {
                    if (!Utility.IsBlockCodeBooking)
                    {
                        NegotiatedHotelDetails hotelDetails = hotel as NegotiatedHotelDetails;
                        if ((hotelDetails != null) && (hotelDetails.HasNegotiateRoomRates))
                        {
                            totalHotels++;
                        }
                    }
                    else
                    {
                        BlockCodeHotelDetails hotelDetails = hotel as BlockCodeHotelDetails;
                        if ((hotelDetails != null) && (hotelDetails.IsBlockRates))
                        {
                            totalHotels++;
                        }
                    }
                }
            }
            return totalHotels;
        }

        /// <summary>
        /// Return all hotels which has negotiated rates available.
        /// </summary>
        /// <returns></returns>
        public static List<IHotelDetails> NegotiatedRateAvailableHotels()
        {
            List<IHotelDetails> negHotels = new List<IHotelDetails>();

            List<IHotelDetails> hotels = HotelResultsSessionWrapper.HotelResults;
            if (null != hotels && hotels.Count > 0)
            {
                foreach (IHotelDetails hotel in hotels)
                {
                    NegotiatedHotelDetails hotelDetails = hotel as NegotiatedHotelDetails;
                    if ((hotelDetails != null) && (hotelDetails.HasNegotiateRoomRates))
                    {
                        negHotels.Add(hotelDetails);
                    }
                }
            }
            return negHotels;
        }

        /// <summary>
        /// For all the list of hotels saved to the session, if atleast one hotel
        /// has promotion rate available then true is returned
        /// If none of the hotel has promotion rates or the search being made is 
        /// not Promotion then false is returned to the user
        /// </summary>
        /// <returns></returns>
        public static bool HasHotelsWithPromotionRates()
        {
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
            if (searchCriteria != null)
            {
                if (SearchType.REGULAR != searchCriteria.SearchingType &&
                    searchCriteria.CampaignCode == null)
                {
                    return false;
                }
                else
                {
                    return TotalPromotionRateAvailableHotels() > 0;
                }
            }

            return false;
        }

        /// <summary>
        /// Return the total number hotels which has Promo rates available
        /// </summary>
        /// <returns></returns>
        public static List<IHotelDetails> PromotionRateAvailableHotels()
        {
            List<IHotelDetails> promoHotels = new List<IHotelDetails>();
            List<IHotelDetails> hotels = HotelResultsSessionWrapper.HotelResults;
            List<string> operaIds = new List<string>();
            if (null != hotels && hotels.Count > 0)
            {
                foreach (IHotelDetails hotel in hotels)
                {
                    PromotionHotelDetails hotelDetails = hotel as PromotionHotelDetails;

                    if ((hotelDetails != null) && (hotelDetails.HasPromotionRoomRates))
                    {
                        if (!operaIds.Contains(hotelDetails.HotelDestination.OperaDestinationId))
                        {
                            promoHotels.Add(hotelDetails);
                            operaIds.Add(hotelDetails.HotelDestination.OperaDestinationId);
                        }
                    }
                    //Merchandising:R3:Display ordinary rates for unavailable promo rates 
                    else if (HotelResultsSessionWrapper.HotelDetails != null && hotelDetails.HotelDestination.OperaDestinationId == HotelResultsSessionWrapper.HotelDetails.HotelDestination.OperaDestinationId)
                    {
                        if (!operaIds.Contains(hotelDetails.HotelDestination.OperaDestinationId))
                        {
                            promoHotels.Add(hotelDetails);
                            operaIds.Add(hotelDetails.HotelDestination.OperaDestinationId);
                        }
                    }
                }
            }
            return promoHotels;
        }

        /// <summary>
        /// Return the total number hotels which has Promo rates available
        /// </summary>
        /// <returns></returns>
        public static int TotalPromotionRateAvailableHotels()
        {
            int total = 0;
            List<IHotelDetails> hotels = HotelResultsSessionWrapper.HotelResults;
            if (null != hotels && hotels.Count > 0)
            {
                foreach (IHotelDetails hotel in hotels)
                {
                    PromotionHotelDetails hotelDetails = hotel as PromotionHotelDetails;
                    if ((hotelDetails != null) && (hotelDetails.HasPromotionRoomRates))
                    {
                        total++;
                    }
                }
            }
            return total;
        }

        public static bool SelectedHotelHasPromoRates(string hotelID)
        {
            bool retVal = false;
            List<IHotelDetails> hotels = HotelResultsSessionWrapper.HotelResults;
            if (hotels != null && hotels.Count > 0)
            {
                IHotelDetails hotel = hotels.Where(o => Convert.ToInt32(o.HotelDestination.OperaDestinationId) == Convert.ToInt32(hotelID)).FirstOrDefault();
                PromotionHotelDetails hotelDetails = hotel as PromotionHotelDetails;
                if ((hotelDetails != null) && (hotelDetails.HasPromotionRoomRates))
                    retVal = true;

            }
            return retVal;
        }

        /// <summary>
        /// Gets all hotels.
        /// </summary>
        /// <returns></returns>
        public static List<IHotelDetails> GetAllHotels()
        {
            List<IHotelDetails> hotels = HotelResultsSessionWrapper.HotelResults;
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;

            if (hotelSearch != null)
            {
                switch (hotelSearch.SearchingType)
                {
                    case (SearchType.CORPORATE):
                        break;
                    case SearchType.REGULAR:
                        //Merchandising:R3:Display ordinary rates for unavailable promo rates 
                        //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
                        if (hotelSearch != null && hotelSearch.CampaignCode != null)
                        {
                            if (hotelSearch.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                            {
                                if (null != hotelSearch.CampaignCode && SelectHotelUtil.HasHotelsWithPromotionRates())
                                    hotels = SelectHotelUtil.PromotionRateAvailableHotels();
                            }
                            else
                            {
                                if (AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                                    GetAlternateCityPromoHotels();
                            }
                        }
                        break;
                    case SearchType.VOUCHER:
                        if (HasHotelsWithPromotionRates())
                        {
                            hotels = PromotionRateAvailableHotels();
                        }
                        break;
                }
            }

            return hotels;
        }

        //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
        public static void GetAlternateCityPromoHotels()
        {
            List<IHotelDetails> hotels = HotelResultsSessionWrapper.HotelResults;
            List<HotelDestination> cityHotels = AlternateCityHotelsSearchSessionWrapper.CityAltHotelDetails;
            List<IHotelDetails> hotelsToRemove = new List<IHotelDetails>();
            if (cityHotels != null && cityHotels.Count > 0 && hotels != null && hotels.Count > 0)
            {
                var listToRemove = from item in hotels
                                   where cityHotels.Contains(item.HotelDestination)
                                   select item;
                foreach (var item in listToRemove)
                {
                    PromotionHotelDetails hotelDetails = item as PromotionHotelDetails;
                    if (hotelDetails != null && !hotelDetails.HasPromotionRoomRates)
                        hotelsToRemove.Add(item);
                }
                for (int i = 0; i < hotelsToRemove.Count; i++)
                {
                    hotels.Remove(hotelsToRemove[i]);
                }
            }
        }

        /// <summary>
        /// Checks the total Regional availability results returned and
        /// the number of the general availablity calls made to OWS.
        /// If the values are not equal true is returned else false is returned
        /// 
        /// Initially the totalRegionalAvailabityHotels and totalGeneralAvailable hotels
        /// will be set to int.MinValue in this scenario also true is returned.
        /// </summary>
        /// <returns></returns>
        public static bool IsSearchToBeDone()
        {
            int totalGeneralAvailHotels = TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels;
            int totalRegionalAvailHotels = TotalRegionalAvailableHotelsSessionWrapper.TotalRegionalAvailableHotels;

            if (totalRegionalAvailHotels == int.MinValue ||
                totalGeneralAvailHotels == int.MinValue)
            {
                return true;
            }
            else if (totalRegionalAvailHotels == totalGeneralAvailHotels)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchType"></param>
        /// <param name="hotel"></param>
        /// <param name="campaignCode"></param>
        /// <param name="displayRegularRates"></param>
        /// <param name="isPerStay"></param>
        /// <returns></returns>
        public static IHotelDisplayInformation GetHotelDisplayObject(SearchType searchType, IHotelDetails hotel,
                                                                     string campaignCode,
                                                                     bool displayRegularRates, bool isPerStay)
        {
            IHotelDisplayInformation hotelDisplay = null;
            switch (searchType)
            {
                case SearchType.REGULAR:
                    if (null == campaignCode)
                    {
                        RegularHotelDetails regularHotelDetails = hotel as RegularHotelDetails;
                        if (regularHotelDetails != null)
                            hotelDisplay = new RegularSearchHotelDisplay(regularHotelDetails, isPerStay);
                    }
                    else
                    {
                        PromotionHotelDetails promoHotelDetails = hotel as PromotionHotelDetails;
                        //Merchandising:R3:Display ordinary rates for unavailable promo rates
                        //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
                        if (promoHotelDetails != null)
                        {
                            if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                            {
                                if ((promoHotelDetails.HasPromotionRoomRates || displayRegularRates) || HotelResultsSessionWrapper.HotelDetails == promoHotelDetails)
                                    hotelDisplay = new PromotionHotelDisplay(promoHotelDetails, isPerStay);
                            }
                            else
                                hotelDisplay = new PromotionHotelDisplay(promoHotelDetails, isPerStay);
                        }
                    }
                    break;
                case SearchType.VOUCHER:
                    VoucherHotelDetails voucherHotelDetails = hotel as VoucherHotelDetails;
                    if ((voucherHotelDetails != null) && (voucherHotelDetails.HasPromotionRoomRates))
                        hotelDisplay = new VoucherHotelDisplay(voucherHotelDetails, true);

                    else if (displayRegularRates)
                    {
                        RegularHotelDetails regularHotelDetails = hotel as RegularHotelDetails;
                        if (regularHotelDetails != null)
                            hotelDisplay = new PromotionHotelDisplay(regularHotelDetails, isPerStay);
                    }
                    break;
                case SearchType.CORPORATE:
                    BlockCodeHotelDetails hotelDetails = hotel as BlockCodeHotelDetails;
                    if (hotelDetails != null)
                    {
                        hotelDisplay = new BlockCodeHotelDisplay(hotel as BlockCodeHotelDetails, isPerStay);
                    }
                    else
                    {
                        NegotiatedHotelDetails negotiatedHotelDetails = hotel as NegotiatedHotelDetails;
                        if (negotiatedHotelDetails != null)
                            hotelDisplay = new NegotiatedHotelDisplay(negotiatedHotelDetails, isPerStay);
                    }

                    break;
                case SearchType.BONUSCHEQUE:
                    BonusChequeHotelDetails bonusChequeHotelDetails = hotel as BonusChequeHotelDetails;
                    if (bonusChequeHotelDetails != null)
                        hotelDisplay = new BonusChequeHotelDisplay(bonusChequeHotelDetails, isPerStay);
                    break;
                case SearchType.REDEMPTION:
                    RedemptionHotelDetails redemptionHotelDetails = hotel as RedemptionHotelDetails;
                    if (redemptionHotelDetails != null)
                        hotelDisplay = new RedemptionHotelDisplay(redemptionHotelDetails);
                    break;
            }
            if (hotelDisplay != null)
                hotelDisplay.IsUnblockRequired = WebUtil.IsUblockRequired(hotelDisplay.ID);

            return hotelDisplay;
        }

        #region SortHotelsByName

        /// <summary>
        /// This will sort the hotels by name as per the CHR29 rules. If the search is done for city then
        /// the CityAndHotelComparer is used else normal HotelNameComparer is used to sort the hotels
        /// </summary>
        /// <param name="hotels">The List of hotels to be sorted</param>
        public static void SortHotelsByName(List<IHotelDetails> hotels)
        {
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
            if (AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
            {
                hotels.Sort(new HotelNameComparer());
            }
            else if (searchCriteria.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City &&
                     AlternateHotelsSessionWrapper.DisplayAlternateHotelsAvailable == false &&
                     !(Reservation2SessionWrapper.IsCitySortDoneInSelectHotel))
            {
                CityAndHotelComparer cityHotelComparer =
                    new CityAndHotelComparer(searchCriteria.SearchedFor.SearchString);
                hotels.Sort(cityHotelComparer);
                Reservation2SessionWrapper.IsCitySortDoneInSelectHotel = true;
            }
            else
            {
                hotels.Sort(new HotelNameComparer());
            }
        }

        #endregion SortHotelsByName

        #region SortByCityCenterDistance

        /// <summary>
        /// The hotels belonging to the city searched for comes first irrespective of the distance of hotels in other City.
        ///  1.Separate out the hotels in different city.
        ///  2.Put the hotels in same city in a collection 
        ///  3.Sort this collection.
        ///  4.Put different city's hotel in different collection.
        ///  5.Sort this collection.
        ///  6.Reassign both hotel collection with the sorted oreder.
        /// </summary>
        public static void SortByCityCenterDistance(List<IHotelDetails> hotels, List<string> map)
        {
            List<IHotelDetails> searchedCityHotels = new List<IHotelDetails>();

            List<IHotelDetails> otherCityHotels = new List<IHotelDetails>();

            foreach (IHotelDetails tempHotel in hotels)
            {
                if (map.Contains(tempHotel.HotelDestination.OperaDestinationId))
                {
                    searchedCityHotels.Add(tempHotel);
                }
                else
                {
                    otherCityHotels.Add(tempHotel);
                }
            }

            searchedCityHotels.Sort(new CityCentreDistanceComparer());

            otherCityHotels.Sort(new HotelNameComparer());

            hotels.Clear();

            hotels.AddRange(searchedCityHotels);

            hotels.AddRange(otherCityHotels);
        }

        /// <summary>
        /// SortByCityCenterDistance
        /// </summary>
        /// <param name="hotels"></param>
        public static void SortByCityCenterDistance(List<IHotelDetails> hotels)
        {
            hotels.Sort(new CityCentreDistanceComparer());
        }

        #endregion SortByCityCenterDistance

        #region SortByMaxRate

        /// <summary>
        /// This will sort the hotels in select hotel page using max rate if two hotels have same min rate.
        /// This fix is done for April Release/Release 1.2 (CR6 |Availability Regional |Rate Display order inaccurate).
        /// </summary>
        /// <param name="hotels">The List of hotels to be sorted using Max room rate per night.</param>
        public static void SortByMaxRate(List<IHotelDetails> hotels)
        {
            List<IHotelDetails> tempHotel = new List<IHotelDetails>();

            List<IHotelDetails> sortedHotels = new List<IHotelDetails>();

            for (int outerCounter = 0; outerCounter < hotels.Count; outerCounter++)
            {
                if (0 == tempHotel.Count)
                {
                    tempHotel.Add(hotels[outerCounter]);
                }

                for (int innerCounter = outerCounter + 1; innerCounter < hotels.Count; innerCounter++)
                {
                    if (hotels[outerCounter].MinRate.Rate != hotels[innerCounter].MinRate.Rate)
                    {
                        break;
                    }
                    else
                    {
                        tempHotel.Add(hotels[innerCounter]);
                        outerCounter++;
                    }
                }

                if (1 == tempHotel.Count)
                {
                    sortedHotels.Add(tempHotel[tempHotel.Count - 1]);
                }
                else
                {
                    tempHotel.Sort(new HotelMaxRateComparer());

                    for (int sortedHotelcounter = 0; sortedHotelcounter < tempHotel.Count; sortedHotelcounter++)
                    {
                        sortedHotels.Add(tempHotel[sortedHotelcounter]);
                    }
                }
                tempHotel.Clear();
            }
            HotelResultsSessionWrapper.HotelResults = sortedHotels;
        }


        public static void SortByDiscountTypeAndRate(List<IHotelDetails> hotels)
        {
            List<IHotelDetails> tempHotel = new List<IHotelDetails>();
            if (hotels != null && hotels.Count > 0 && hotels[0].GetType() == typeof(PromotionHotelDetails))
            {
                tempHotel = hotels.OrderByDescending(o => ((PromotionHotelDetails)o).HasPromotionRoomRates).ThenBy(p => p.MinRate != null ?
                    p.MinRate.Rate : p.MinPoints != null ? p.MinPoints.PointsRequired : double.MaxValue).ToList();
            }
            else
                tempHotel = hotels;
            HotelResultsSessionWrapper.HotelResults = tempHotel;
        }


        public static void SortByTARatingMaxRate(List<IHotelDetails> hotels)
        {
            List<IHotelDetails> tempHotel = new List<IHotelDetails>();
            tempHotel = hotels.OrderByDescending(o => o.TripAdvisorHotelRating).ThenBy(p => p.MinRate != null ? p.MinRate.Rate : p.MinPoints != null ?
                                                                                            p.MinPoints.PointsRequired : double.MaxValue).ToList();
            HotelResultsSessionWrapper.HotelResults = tempHotel;
        }

        public static List<HotelDestination> SortByTARatingMaxRateForCityLandingPages(List<HotelDestination> hotels)
        {
            List<HotelDestination> tempHotel = new List<HotelDestination>();
            tempHotel = hotels.OrderByDescending(o => o.TripAdvisorHotelRating).ToList();
            return tempHotel;
        }

        #endregion SortByMaxRate

        #region SortByDistanceToCityCenterOfSearchedHotel

        /// <summary>
        /// This will sort the hotels by the city center distance form the searched hotels.
        /// The non configured hotels are sorted first using their name,then hotels with distance configured.
        /// </summary>
        /// <param name="hotels"></param>
        public static void SortByDistanceToCityCenterOfSearchedHotel(List<IHotelDetails> hotels)
        {
            List<IHotelDetails> configuredHotels = new List<IHotelDetails>();
            List<IHotelDetails> nonConfiguredHotels = new List<IHotelDetails>();

            if ((hotels != null) && (SearchCriteriaSessionWrapper.SearchCriteria != null))
            {
                var searchedHotel = hotels.FirstOrDefault(hotel => (hotel.HotelDestination != null) &&
                                                                string.Equals(hotel.HotelDestination.OperaDestinationId,
                                                                              SearchCriteriaSessionWrapper.SearchCriteria
                                                                                  .SelectedHotelCode,
                                                                              StringComparison.InvariantCultureIgnoreCase));
                if ((searchedHotel != null) && (searchedHotel.HotelDestination != null))
                {
                    searchedHotel.DistanceToCityCentreOfSearchedHotel = Convert.ToString(searchedHotel.HotelDestination.CityCenterDistance);
                }
            }

            foreach (IHotelDetails currentHotel in hotels)
            {
                if (AppConstants.HYPHEN == currentHotel.DistanceToCityCentreOfSearchedHotel)
                {
                    nonConfiguredHotels.Add(currentHotel);
                }
                else
                {
                    configuredHotels.Add(currentHotel);
                }
            }

            for (int counter = 0; counter < configuredHotels.Count; counter++)
            {
                IHotelDetails temporaryHotel;
                for (int innerCounter = counter + 1; innerCounter < configuredHotels.Count; innerCounter++)
                {
                    if ((Convert.ToSingle(configuredHotels[counter].DistanceToCityCentreOfSearchedHotel)) >
                        (Convert.ToSingle(configuredHotels[innerCounter].DistanceToCityCentreOfSearchedHotel)))
                    {
                        temporaryHotel = configuredHotels[counter];
                        configuredHotels[counter] = configuredHotels[innerCounter];
                        configuredHotels[innerCounter] = temporaryHotel;
                    }
                }
            }
            SortHotelsByName(nonConfiguredHotels);
            hotels.Clear();
            hotels.AddRange(configuredHotels);
            hotels.AddRange(nonConfiguredHotels);
        }

        #endregion SortByDistanceToCityCenterOfSearchedHotel

        #region Initiate Search

        /// <summary>
        /// Initiates asynchronous search.
        /// </summary>
        /// <param name="hotelSearch"></param>
        public static void InitiateAsynchronousSearch(HotelSearchEntity hotelSearch)
        {
            AvailabilityController controller = new AvailabilityController();
            switch (hotelSearch.SearchingType)
            {
                case SearchType.REGULAR:
                case SearchType.VOUCHER:
                case SearchType.CORPORATE:
                case SearchType.BONUSCHEQUE:
                    controller.SearchHotels(hotelSearch);
                    break;
                case SearchType.REDEMPTION:
                    ArrayList membershipList =
                        controller.FetchGuestCardList(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID,
                                                      AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE);
                    if ((membershipList != null) && (membershipList.Count > 0))
                    {
                        MembershipEntity membershipEntity = (MembershipEntity)membershipList[0];
                        string membershipLevel = membershipEntity.MembershipLevel;
                        controller.RedemptionSearchHotels(hotelSearch, membershipLevel);
                    }
                    break;
            }
        }

        /// <summary>
        /// This method call appropriate search method for each search type
        /// </summary>
        /// <param name="hotelSearch"></param>
        public static void InitiateAsynchronousSearchForAltCityHotels(HotelSearchEntity hotelSearch)
        {
            AvailabilityController controller = new AvailabilityController();

            //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
            if (!AlternateCityHotelsSearchSessionWrapper.IsDestinationAlternateFlow)
                CleanSessionSearchResultsSessionWrapper.CleanSessionSearchResults();

            switch (hotelSearch.SearchingType)
            {
                case SearchType.REGULAR:
                case SearchType.VOUCHER:
                case SearchType.CORPORATE:
                case SearchType.BONUSCHEQUE:
                    controller.SearchAlternativeCityHotels(hotelSearch);
                    break;
                case SearchType.REDEMPTION:
                    ArrayList membershipList =
                        controller.FetchGuestCardList(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID,
                                                      AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE);
                    if ((membershipList != null) && (membershipList.Count > 0))
                    {
                        MembershipEntity membershipEntity = (MembershipEntity)membershipList[0];
                        string membershipLevel = membershipEntity.MembershipLevel;
                        controller.RedemptionSearchAlternativeCityHotels(hotelSearch, membershipLevel);
                    }
                    break;
            }
        }

        #endregion Initiate Search

        #region FindAHotel HotelListingControl

        private static string currentLanguageString
        {
            get { return EPiServer.Globalization.ContentLanguage.PreferredCulture.Name; }
        }

        private const string HotelDestinationsWithCityID = "HotelDestinationsWithCityID{0}{1}";

        /// <summary>
        /// Makes a Call to availabilityController when brings the data from CMS 
        /// </summary>
        /// <param name="searchCode">the unique code used for search OperaId/Country code</param>
        /// <param name="searchType">has the value of city,country or hotel depending upon search done</param>
        public static void InitiateSearchForFindAHotelDestination(string searchCode, bool hotelSearchType)
        {
            AvailabilityController availabilityController = new AvailabilityController();
            availabilityController.SearchHotelsForFindAHotel(searchCode, hotelSearchType);
        }

        /// <summary>
        /// Sorts the HotelDestination List
        /// </summary>
        /// <param name="hotelDestinationListResult"></param>
        public static void SortHotelsDestinationByName()
        {
            List<HotelDestination> hotelDestinationListResult = FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults();
            if (null != hotelDestinationListResult)
                hotelDestinationListResult.Sort(new HotelDestinationNameComparer());
        }

        /// <summary>
        /// Sorts the hotels from city centre distance
        /// </summary>
        /// <param name="hotels"></param>
        public static void SortHotelsDestinationByCityCenterDistance()
        {
            List<HotelDestination> hotelDestinationListResult = FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults();
            hotelDestinationListResult.Sort(new HotelDestinationCityCentreDistanceComparer());
        }


        /// <summary>
        /// Returns the hotelDestination Display object which has data specific to the HotelListingControl
        /// </summary>
        /// <param name="searchType"></param>
        /// <param name="hotelDestination"></param>
        /// <returns></returns>
        public static HotelListingDisplay GetHotelDestinationDisplayObject(
            SearchType searchType,
            HotelDestination hotelDestination, int counter)
        {
            if (hotelDestination.TALocationID > 0 && !hotelDestination.HideHotelTARating)
            {
                var tripAdviserHotelObj = WebUtil.GetTAReviewEntityForHotel(hotelDestination.TALocationID);

                hotelDestination.TripAdvisorHotelRating = tripAdviserHotelObj != null
                                                              ? tripAdviserHotelObj.AverageRating
                                                              : 0;
            }
            else
                hotelDestination.TripAdvisorHotelRating = 0;

            HotelListingDisplay hotelDisplay = null;
            hotelDisplay = new HotelListingDisplay(hotelDestination, counter);
            return hotelDisplay;
        }

        #endregion


        #region Map For Searched City
        /// <summary>
        /// This change is done due to CR 14 |Release 1.3(May release) |Distance by city center.
        /// This will create the map for searched cities.
        /// This Map will contain the collection of searched cities hotel opera ID.
        /// </summary>  
        /// <returns>List of City opera Id</returns>
        public static List<string> GetSearchedCityMap()
        {
            List<CityDestination> cityList;
            if (GetCacheValue(ALL_CITY_MAP) == null)
            {
                cityList = _AvailabilityController.FetchAllDestinations();
                if (null != cityList)
                {
                    InsertValueInCache(ALL_CITY_MAP, cityList);
                }
            }
            else
            {
                cityList = GetCacheValue(ALL_CITY_MAP) as List<CityDestination>;
            }
            List<HotelDestination> exactCities = new List<HotelDestination>();
            if (cityList != null)
            {
                for (int cityCounter = 0; cityCounter < cityList.Count; cityCounter++)
                {
                    if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor != null &&
                        cityList[cityCounter].OperaDestinationId == SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.SearchCode)
                    {
                        exactCities.AddRange(cityList[cityCounter].SearchedHotels);
                        break;
                    }
                }
            }
            List<string> map = new List<string>();
            for (int counter = 0; counter < exactCities.Count; counter++)
            {
                map.Add(exactCities[counter].OperaDestinationId);
            }
            return map;
        }

        /// <summary>
        /// Stores object in HTTP cache
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="cacheValue"></param>
        private static void InsertValueInCache(string cacheKey, object cacheValue)
        {
            ScanwebCacheManager.Instance.AddToCache(cacheKey, cacheValue);
        }

        /// <summary>
        /// Gets cache value for the given cachekey.
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        private static object GetCacheValue(string cacheKey)
        {
            return ScanwebCacheManager.Instance.LookInCache<object>(cacheKey);
        }

        public static void SortCityCenterDistance(List<IHotelDetails> hotels)
        {
            if (AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
            {
                SelectHotelUtil.SortByCityCenterDistance(hotels);
            }
            else
            {
                List<string> cityMap = SelectHotelUtil.GetSearchedCityMap();
                if (null != cityMap)
                {
                    SelectHotelUtil.SortByCityCenterDistance(hotels, cityMap);
                }
            }
        }

        #endregion Map For Searched City
    }
}