﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Scandic.Scanweb.BookingEngine.Web.code.Booking
{
    public class CustomJSSerializer : JavaScriptSerializer
    {
        public CustomJSSerializer()
            : base()
        {
            this.RegisterConverters(new JavaScriptConverter[] { new DateStringJSONConverter() });
        }
    }

    public class DateStringJSONConverter : JavaScriptConverter
    {
        private List<Type> supportedTypes;
        private string DateFormat = "dd MMM yyyy";
        public DateStringJSONConverter()
        {
            supportedTypes = new List<Type>(1);
            supportedTypes.Add(typeof(DateTime));
        }

        public object DeSerializeObject(IDictionary<String, Object> dictionary, Type type, JavaScriptSerializer serializer, string dateFormat)
        {
            DateFormat = dateFormat;
            return Deserialize(dictionary, type, serializer);
        }

        public override object Deserialize(IDictionary<String, Object> dictionary, Type type, JavaScriptSerializer serializer)
        {
            DateTime dt = DateTime.ParseExact(dictionary
            ["DateString"].ToString(), DateFormat, null);
            return dt;
        }

        public IDictionary<String, Object> SerializeObject(object obj, JavaScriptSerializer serializer, string dateFormat)
        {
            DateFormat = dateFormat;
            return Serialize(obj, serializer);
        }

        public override IDictionary<String, Object> Serialize(object obj, JavaScriptSerializer serializer)
        {
            DateTime dt = Convert.ToDateTime(obj);
            Dictionary<String, Object> dicDateTime = new Dictionary<String, Object>(1);
            dicDateTime.Add("DateString", dt.ToString(DateFormat));
            return dicDateTime;
        }

        public override IEnumerable<Type> SupportedTypes
        {
            get { return this.supportedTypes; }
        }
    }
}
