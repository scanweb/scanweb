#region Description

////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: This is the block code rooom rate dispaly room rate     //
//                                display created when user do a block code booking.      //
//	                              This is resulted in Accomodation rate block.            //
//                                CR2 | Release 1.4 |Accomodation rate block              //                        
//----------------------------------------------------------------------------------------//
//  Author						: Raj Kishore Marandi	                                  //
//  Author email id				:                           							  //
//  Creation Date				: 08th September 2008									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
// Revision History			: -NA-													      //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#endregion Description

#region System NameSpace

using System;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;

#endregion System NameSpace

    #region Scandic Namespace

#endregion Scandic Namespace

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// This class represents BlockCodeRoomRateDisplay
    /// </summary>
    public class BlockCodeRoomRateDisplay : BaseRateDisplay
    {
        /// <summary>
        /// Rate category Id.
        /// </summary>
        private string rateCategoryId;

        /// <summary>
        /// Rate category name
        /// </summary>
        private string rateCategoryName;

        /// <summary>
        /// Rate category header display list.
        /// </summary>
        private List<RateCategoryHeaderDisplay> rateCategoryDisplayList = new List<RateCategoryHeaderDisplay>();

        private string minRateForEachRoom;

        private string maxRateForEachRoom;

        private string minRatePerStayForEachRoom;

        private string maxRatePerStayForEachRoom;

        /// <summary>
        /// Gets/Sets IsRateCodeNotAssociated
        /// </summary>
        public bool IsRateCodeNotAssociated { get; set; }

        /// <summary>
        /// Gets/Sets RateDescription
        /// </summary>
        public string RateDescription { get; set; }

        /// <summary>
        /// Gets/Sets RateUrl
        /// </summary>
        public string RateUrl { get; set; }

        /// <summary>
        /// Gets RateCategoriesDisplay
        /// </summary>
        public override List<RateCategoryHeaderDisplay> RateCategoriesDisplay
        {
            get { return rateCategoryDisplayList; }
        }

        /// <summary>
        /// Constructor of BlockCodeRoomRateDisplay class
        /// </summary>
        /// <param name="roomRateDetails"></param>
        public BlockCodeRoomRateDisplay(BlockCodeRoomRateDetails roomRateDetails)
        {
            if (roomRateDetails != null)
            {
                this.roomRateDetails = roomRateDetails;
                bool status = roomRateDetails.IsRateCodeNotAssociated;
                if (status)
                {
                    IsRateCodeNotAssociated = status;
                    SetRateCategoryNoRateCodeAssociated();
                }
                else
                {
                    SetRateCategoriesDisplay();
                }
                this.minRateForEachRoom = (roomRateDetails.MinRateForEachRoom != null
                                           && roomRateDetails.MinRateForEachRoom.Rate != Double.MinValue)
                                              ? (roomRateDetails.MinRateForEachRoom.Rate.ToString() + " "
                                                 + roomRateDetails.MinRateForEachRoom.CurrencyCode)
                                              : string.Empty;
                this.maxRateForEachRoom = (roomRateDetails.MaxRateForEachRoom != null
                                           && roomRateDetails.MaxRateForEachRoom.Rate != Double.MinValue)
                                              ? (roomRateDetails.MaxRateForEachRoom.Rate.ToString() + " "
                                                 + roomRateDetails.MaxRateForEachRoom.CurrencyCode)
                                              : string.Empty;
                this.minRatePerStayForEachRoom = (roomRateDetails.MinRatePerStayForEachRoom != null
                                                  && roomRateDetails.MinRatePerStayForEachRoom.Rate != Double.MinValue)
                                                     ? (roomRateDetails.MinRatePerStayForEachRoom.Rate.ToString() + " "
                                                        + roomRateDetails.MinRatePerStayForEachRoom.CurrencyCode)
                                                     : string.Empty;
                this.maxRatePerStayForEachRoom = (roomRateDetails.MaxRatePerStayForEachRoom != null
                                                  && roomRateDetails.MaxRatePerStayForEachRoom.Rate != Double.MinValue)
                                                     ? (roomRateDetails.MaxRatePerStayForEachRoom.Rate.ToString() + " "
                                                        + roomRateDetails.MaxRatePerStayForEachRoom.CurrencyCode)
                                                     : string.Empty;
            }
        }

        /// <summary>
        /// Gets rate cell display
        /// </summary>
        /// <param name="roomCategory"></param>
        /// <param name="rateCategorySlNo"></param>
        /// <param name="rateCategoryID"></param>
        /// <param name="showPricePerStay"></param>
        /// <param name="roomNumber"></param>
        /// <returns></returns>
        public override RateCellDisplay GetRateCellDisplay(RoomCategoryEntity roomCategory, int rateCategorySlNo,
                                                           string rateCategoryID, bool showPricePerStay, int roomNumber)
        {
            RateCellDisplay rateCell = new RateCellDisplay();
            RateEntity rate = null;
            string stringFormat = string.Empty;
            if (AppConstants.BLOCK_CODE_QUALIFYING_TYPE == rateCategoryID)
            {
                Dictionary<string, List<RoomRateEntity>> roomRateEntity = roomCategory.RateCategories;
                if (showPricePerStay)
                {
                    foreach (string key in roomRateEntity.Keys)
                    {
                        List<RoomRateEntity> room = roomRateEntity[key];
                        rate = room[0].TotalRate;
                        stringFormat = GetPerStayStringFormat();
                        break;
                    }
                }
                else
                {
                    foreach (string key in roomRateEntity.Keys)
                    {
                        List<RoomRateEntity> room = roomRateEntity[key];
                        rate = room[0].BaseRate;
                        stringFormat = GetPerNightStringFormat();
                        break;
                    }
                }
            }
            else
            {
                if (showPricePerStay)
                {
                    rate = roomCategory.GetCommonTotalRate(rateCategoryID);
                    stringFormat = GetPerStayStringFormat();
                }
                else
                {
                    rate = roomCategory.GetCommonBaseRate(rateCategoryID);
                    stringFormat = GetPerNightStringFormat();
                }
            }

            rateCell.RateString = string.Empty;
            if (null != rate)
            {
                ExchangeRateManager exchangeRateManager = new ExchangeRateManager();
                string rateValue = rate.Rate.ToString();
                string currencyCode = rate.CurrencyCode;
                rateCell.RateString = string.Format(stringFormat, rateValue, currencyCode);
                rateCell.AlternateRateString = exchangeRateManager.GetExchangeRateString(stringFormat, currencyCode, rateValue);
                rateCell.BookUrl = "javascript:RedirectToBookDetail(\'" + rateCategorySlNo + "\', \'"
                                   + rateCategoryID + "\', \'" + roomNumber + "\')";
            }
            return rateCell;
        }

        /// <summary>
        /// Gets the rate cell display for shopping cart.
        /// </summary>
        /// <param name="roomCategory">The room category.</param>
        /// <param name="rateCategoryID">The rate category ID.</param>
        /// <param name="showPricePerStay">if set to <c>true</c> [show price per stay].</param>
        /// <param name="roomNumber">The room number.</param>
        /// <returns></returns>
        public override RateCellDisplay GetRateDisplayStringForShoppingCart(RoomCategoryEntity roomCategory,
                                                                            string rateCategoryID, bool showPricePerStay,
                                                                            int roomNumber)
        {
            RateCellDisplay rateCell = new RateCellDisplay();
            RateEntity rate = null;
            string stringFormat = string.Empty;
            if (AppConstants.BLOCK_CODE_QUALIFYING_TYPE == rateCategoryID)
            {
                Dictionary<string, List<RoomRateEntity>> roomRateEntity = roomCategory.RateCategories;
                if (roomRateEntity != null)
                {
                    if (showPricePerStay)
                    {
                        foreach (string key in roomRateEntity.Keys)
                        {
                            List<RoomRateEntity> room = roomRateEntity[key];
                            rate = room[0].TotalRate;
                            stringFormat = GetPerStayStringFormat();
                            break;
                        }
                    }
                    else
                    {
                        foreach (string key in roomRateEntity.Keys)
                        {
                            List<RoomRateEntity> room = roomRateEntity[key];
                            rate = room[0].BaseRate;
                            stringFormat = GetPerNightStringFormat();
                            break;
                        }
                    }
                }
            }
            else
            {
                if (showPricePerStay)
                {
                    rate = roomCategory.GetCommonTotalRate(rateCategoryID);
                    stringFormat = GetPerStayStringFormat();
                }
                else
                {
                    rate = roomCategory.GetCommonBaseRate(rateCategoryID);
                    stringFormat = GetPerNightStringFormat();
                }
            }

            rateCell.RateString = string.Empty;
            if (null != rate)
            {
                string rateValue = rate.Rate.ToString();
                string currencyCode = rate.CurrencyCode;
                rateCell.RateString = string.Format(stringFormat, rateValue, currencyCode);
            }
            return rateCell;
        }

        /// <summary>
        /// Gets the total rate display string for shopping cart.
        /// </summary>
        /// <param name="showPricePerStay">if set to <c>true</c> [show price per stay].</param>
        /// <param name="total">The total.</param>
        /// <param name="CurrencyCode">The currency code.</param>
        /// <returns></returns>
        public override string GetTotalRateDisplayStringForShoppingCart(bool showPricePerStay, double total,
                                                                        string currencyCode)
        {
            string stringFormat;
            if (showPricePerStay)
            {
                stringFormat = GetPerStayStringFormat();
            }
            else
            {
                stringFormat = GetPerNightStringFormat();
            }
            string totalRateString = string.Format(stringFormat, total.ToString(), currencyCode);
            return totalRateString;
        }


        /// <summary>
        /// Get per night string.
        /// </summary>
        /// <returns>Per night string</returns>
        private string GetPerNightStringFormat()
        {
            return WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/normalratepernight");
        }

        /// <summary>
        /// Get Price per stay string.
        /// </summary>
        /// <returns>Price per stay string.</returns>
        private string GetPerStayStringFormat()
        {
            return WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/normalrateperstay");
        }

        /// <summary>
        /// Gets the max rate for each room.
        /// </summary>
        /// <value>The max rate for each room.</value>
        public override string MaxRateForEachRoom
        {
            get { return maxRateForEachRoom; }
        }

        /// <summary>
        /// Gets MinRateForEachRoom
        /// </summary>
        public override string MinRateForEachRoom
        {
            get { return minRateForEachRoom; }
        }

        /// <summary>
        /// Gets MinRatePerStayForEachRoom
        /// </summary>
        public override string MinRatePerStayForEachRoom
        {
            get { return minRatePerStayForEachRoom; }
        }

        /// <summary>
        /// Gets MaxRatePerStayForEachRoom
        /// </summary>
        public override string MaxRatePerStayForEachRoom
        {
            get { return maxRatePerStayForEachRoom; }
        }

        /// <summary>
        /// Set Rate category dispaly when rate is configured.
        /// </summary>
        private void SetRateCategoriesDisplay()
        {
            BlockCodeRoomRateDetails roomRateDetailsCasted = roomRateDetails as BlockCodeRoomRateDetails;
            if (roomRateDetailsCasted != null && roomRateDetailsCasted.RateCategories != null
                && roomRateDetailsCasted.RateCategories.Count > 0)
            {
                foreach (RateCategory rateCategory in roomRateDetailsCasted.RateCategories)
                {
                    if (rateCategory != null && (!(rateCategory.RateCategoryId).Contains("BC")))
                    {
                        this.rateCategoryId = rateCategory.RateCategoryId;
                        this.rateCategoryName = rateCategory.RateCategoryName;
                        this.RateDescription = rateCategory.RateCategoryDescription;
                        this.RateUrl = rateCategory.RateCategoryURL;
                        rateCategoryDisplayList.Add(new RateCategoryHeaderDisplay(rateCategory.RateCategoryId,
                                                                                  rateCategory.RateCategoryName,
                                                                                  RateDescription, RateUrl,
                                                                                  rateCategory.CoulmnNumber,
                                                                                  rateCategory.DisplayMoreInfoLink,
                                                                                  rateCategory.RateCategoryColor,
                                                                                  rateCategory.RateHighlightTextWeb,
                                                                                  rateCategory.RateHighlightTextMobile,
                                                                                  rateCategory.RateCategoryLanguage,
                                                                                  rateCategory.HeaderList));
                    }
                }
            }
        }

        /// <summary>
        /// Set when the rate category is not configured.
        /// </summary>
        private void SetRateCategoryNoRateCodeAssociated()
        {
            Block block = null;
            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
            {
                block = ContentDataAccess.GetBlockCodePages(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
            }
            else
            {
                block = ContentDataAccess.GetBlockCodePages(BookingEngineSessionWrapper.BookingDetails.HotelSearch.CampaignCode);
            }

            if (null != block)
            {
                this.rateCategoryId = AppConstants.BLOCK_CODE_QUALIFYING_TYPE;

                this.rateCategoryName = block.BlockName;
                this.RateDescription = block.BlockDescription;
                this.RateUrl = string.Empty;
                rateCategoryDisplayList.Add(new RateCategoryHeaderDisplay(this.rateCategoryId, this.rateCategoryName,
                                                                          RateDescription, RateUrl, 1, false,
                                                                          block.BlockCategoryColor, string.Empty, string.Empty,string.Empty,block.Headers));
            }
            else
            {
                throw new CMSException(CMSExceptionConstants.BLOCK_NOT_CONFIGURED);
            }
        }
    }
}