using Scandic.Scanweb.BookingEngine.Controller;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// NegotiatedHotelDisplay
    /// </summary>
    public class NegotiatedHotelDisplay : RegularSearchHotelDisplay
    {
        /// <summary>
        /// NegotiatedHotelDisplay
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="isPerStay"></param>
        public NegotiatedHotelDisplay(NegotiatedHotelDetails hotelDetails, bool isPerStay)
        {
            SetHotelDestinationDetails(hotelDetails.HotelDestination);
            SetPriceString(hotelDetails, isPerStay);
            if (hotelDetails.HasNegotiateRoomRates)
                this.rateTitle = hotelDetails.CompanyName;
        }
    }
}