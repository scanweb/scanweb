using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using System;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// BlockCodeHotelDisplay
    /// </summary>
    public class BlockCodeHotelDisplay : BaseHotelDisplay
    {
        public BlockCodeHotelDisplay(BlockCodeHotelDetails hotelDetails, bool isPerStay)
        {
            bool hideARBPrice = false;
            if (hotelDetails.IsBlockRates)
            {
                Block block = ContentDataAccess.GetBlockCodePages(hotelDetails.BlockCode);
                if ((null != block) && (!string.IsNullOrEmpty(block.BlockName)))
                {
                    this.rateTitle = block.BlockName;
                    hideARBPrice = block.HideARBPrice;
                    BookingEngineSessionWrapper.HideARBPrice = block.HideARBPrice;
                }

            }
            SetHotelDestinationDetails(hotelDetails.HotelDestination);
            SetPriceString(hotelDetails, isPerStay, hideARBPrice);
        }

        /// <summary>
        /// SetPriceString
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="isPerStay"></param>
        private void SetPriceString(BlockCodeHotelDetails hotelDetails, bool isPerStay, bool hideARBPrice)
        {
            ExchangeRateManager exchangeRateManager = new ExchangeRateManager();
            string convertedRateString = string.Empty;
            string approxText = string.Empty;
            string disclaimerTextLine1 = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/disclaimertextline1");
            string disclaimerTextLine2 = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/disclaimertextline2");
            approxText = String.Format("{0}{1}", WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/about"), AppConstants.SPACE);
            if (hideARBPrice)
            {
                this.price = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/prepaid");
                RateString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/roomperstay");
                this.FromText = string.Empty;
            }
            else
            {
                if (isPerStay)
                {
                    string rateString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/ratestring");
                    string MinRatePerStay = string.Empty;
                    string currencyCode = string.Empty;
                    if (null != hotelDetails.MinRatePerStay)
                    {
                        MinRatePerStay = hotelDetails.MinRatePerStay.Rate.ToString();
                        ;
                        currencyCode = hotelDetails.MinRatePerStay.CurrencyCode;
                    }
                    rateString = string.Format(rateString, MinRatePerStay, currencyCode);
                    this.price = rateString;
                    this.altPriceTooltipLine1 = disclaimerTextLine1;
                    this.altPriceTooltipLine2 = disclaimerTextLine2;
                    convertedRateString = exchangeRateManager.GetExchangeRateString(rateString, rateString.Split(Convert.ToChar(AppConstants.SPACE))[1],
                                                                            rateString.Split(Convert.ToChar(AppConstants.SPACE))[0]);
                    if (convertedRateString != string.Empty)
                    {
                        this.alternatePrice = String.Format("{0}{1}", approxText, convertedRateString);
                    }
                    RateString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/roomperstay");
                }
                else
                {
                    string rateString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/ratestring");
                    string minRate = string.Empty;
                    string currencyCode = string.Empty;
                    if (null != hotelDetails.MinRate)
                    {
                        minRate = hotelDetails.MinRate.Rate.ToString();
                        ;
                        currencyCode = hotelDetails.MinRate.CurrencyCode;
                    }
                    rateString = string.Format(rateString, minRate, currencyCode);

                    this.price = rateString;
                    this.altPriceTooltipLine1 = disclaimerTextLine1;
                    this.altPriceTooltipLine2 = disclaimerTextLine2;
                    convertedRateString = exchangeRateManager.GetExchangeRateString(rateString, rateString.Split(Convert.ToChar(AppConstants.SPACE))[1],
                                                                            rateString.Split(Convert.ToChar(AppConstants.SPACE))[0]);
                    if (convertedRateString != string.Empty)
                    {
                        this.alternatePrice = String.Format("{0}{1}", approxText, convertedRateString);
                    }
                    this.perNight = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/perroompernight");

                    RateString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/perroompernight");
                }
            }
        }
    }
}