#region Revision history

///  Description					  : This class is used for HotelListing control. This extends IHotelListingDisplayInformation
///                                     and is used to display all the CMS related data into HotelListing control 
///----------------------------------------------------------------------------------------------------------
/// Author						      : Kshipra Thombre
/// Creation Date				      : 17-Sept-2009
///	Version	#					      : 1.0													  
///----------------------------------------------------------------------------------------------------------
/// Revision History				  : 
///	Last Modified Date			      :	
///	Modified By                       : 
///	Latest Version                    : 

#endregion

#region using

using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion using

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// HotelListingDisplay
    /// </summary>
    public class HotelListingDisplay : IHotelListingDisplayInformation
    {
        #region declerations

        protected string id;
        protected string imageSrc;
        protected string imageAltText;
        protected string hotelURL;
        protected string hotelName;
        protected string address;
        protected string description;
        private string distance;
        private int counter;
        private int taLocationID;
        private bool hideHotelTARatings;
        private bool hideCityTARatings;
        private double tripAdvisorHotelRating;
        #endregion declerations

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public HotelListingDisplay()
        {
        }

        /// <summary>
        /// Constructor used to set the values for the Display object
        /// </summary>
        /// <param name="hotel"></param>
        public HotelListingDisplay(HotelDestination hotel, int counter)
        {
            this.id = hotel.OperaDestinationId;
            this.imageSrc = hotel.ImageURL;
            this.imageAltText = hotel.Name;
            this.hotelURL = hotel.HotelPageURL;
            this.hotelName = hotel.Name;
            this.address = hotel.HotelAddress.ToString();
            this.description = GetThuncatedHotelDescription(hotel.HotelDescription);
            this.distance = hotel.CityCenterDistance.ToString();
            this.counter = counter + 1;
            this.TALocationID = hotel.TALocationID;
            this.HideHotelTARatings = hotel.HideHotelTARating;
            this.HideCityTARatings = hotel.HideCityTARating;
            this.TripAdvisorHotelRating = hotel.TripAdvisorHotelRating;
        }

        #endregion Constructor

        #region Properties

        public string ID
        {
            get { return id; }
        }

        private static string NO_IMAGE_PATH = "/Templates/Booking/Images/noimage.gif";

        public string ImageSrc
        {
            get
            {
                if (string.IsNullOrEmpty(imageSrc))
                    return NO_IMAGE_PATH;
                else
                    return imageSrc;
            }
        }

        public string ImageAltText
        {
            get { return imageAltText; }
        }

        public string HotelURL
        {
            get { return hotelURL; }
        }

        public string HotelName
        {
            get { return hotelName; }
        }

        public string Address
        {
            get { return address; }
        }


        public string Description
        {
            get { return description; }
        }


        public string Distance
        {
            get { return distance; }
        }


        public int Counter
        {
            get { return counter; }
        }

        public int TALocationID
        {
            get { return taLocationID; }
            set { taLocationID = value; }
        }

        public bool HideHotelTARatings
        {
            get { return hideHotelTARatings; }
            set { hideHotelTARatings = value; }
        }

        public bool HideCityTARatings
        {
            get { return hideCityTARatings; }
            set { hideCityTARatings = value; }
        }

        public double TripAdvisorHotelRating
        {
            get { return tripAdvisorHotelRating; }
            set { tripAdvisorHotelRating = value; }
        }

        #endregion Properties

        #region Private Methods

        /// <summary>
        /// Truncate Hotel Description to specific length and append with "..."
        /// </summary>
        /// <param name="hotelDescription"></param>
        /// <returns>Truncated Hotel Description</returns>
        private string GetThuncatedHotelDescription(string hotelDescription)
        {
            string truncatedHotelDesc = hotelDescription;
            if (truncatedHotelDesc.Length > AppConstants.MAX_LENGTH_SHORTDESCRIPTION_LISTING_CONTROL)
            {
                truncatedHotelDesc = truncatedHotelDesc.Substring(0,
                                                                    AppConstants.
                                                                      MAX_LENGTH_SHORTDESCRIPTION_LISTING_CONTROL);
                truncatedHotelDesc += AppConstants.TRIPPLE_DOTS;
            }
            return truncatedHotelDesc;
        }

        #endregion private methods
    }
}