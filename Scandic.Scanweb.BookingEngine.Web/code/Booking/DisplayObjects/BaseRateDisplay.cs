using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// Base Rate Display
    /// </summary>
    public abstract class BaseRateDisplay
    {
        protected BaseRoomRateDetails roomRateDetails;

        public List<RoomCategoryEntity> RateRoomCategories
        {
            get { return roomRateDetails.RateRoomCategories; }
        }

        public HotelDestination HotelDestination
        {
            get { return roomRateDetails.HotelDestination; }
        }

        public abstract List<RateCategoryHeaderDisplay> RateCategoriesDisplay { get; }

        public abstract string MinRateForEachRoom { get; }

        public abstract string MaxRateForEachRoom { get; }

        public abstract string MinRatePerStayForEachRoom { get; }
        public abstract string MaxRatePerStayForEachRoom { get; }

        public abstract RateCellDisplay GetRateCellDisplay(RoomCategoryEntity roomCategory, int rateCategorySlNo,
                                                           string rateCategoryID, bool showPricePerStay, int roomNumber);

        public abstract RateCellDisplay GetRateDisplayStringForShoppingCart(RoomCategoryEntity roomCategory,
                                                                            string rateCategoryID, bool showPricePerStay,
                                                                            int roomNumber);

        public abstract string GetTotalRateDisplayStringForShoppingCart(bool showPricePerStay, double total,
                                                                        string CurrencyCode);
    }
}