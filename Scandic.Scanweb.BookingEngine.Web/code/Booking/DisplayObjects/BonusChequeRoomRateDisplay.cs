//  Description					: BonusChequeRoomRateDisplay                              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// This class represents bonus cheque room rate display.
    /// </summary>
    public class BonusChequeRoomRateDisplay : BaseRateDisplay
    {
        private List<RateCategoryHeaderDisplay> rateCategoryDisplayList = new List<RateCategoryHeaderDisplay>();
        private string minRateForEachRoom;
        private string maxRateForEachRoom;
        private string minRatePerStayForEachRoom;
        private string maxRatePerStayForEachRoom;

        /// <summary>
        /// min rate display in rooms tab for bonus cheque
        /// </summary>
        private static bool _isRoomsTab;

        /// <summary>
        /// Gets/Sets IsRoomsTab
        /// </summary>
        public static bool IsRoomsTab
        {
            get { return _isRoomsTab; }
            set { _isRoomsTab = value; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="roomRateDetails"></param>
        public BonusChequeRoomRateDisplay(BonusChequeRoomRateDetails roomRateDetails)
        {
            if (roomRateDetails != null)
            {
                this.roomRateDetails = roomRateDetails;
                SetRateCategoriesDisplay();
                SetMinMaxDisplay(roomRateDetails);
            }
        }

        /// <summary>
        /// Sets rate categories to display
        /// </summary>
        public void SetRateCategoriesDisplay()
        {
            if (roomRateDetails != null && roomRateDetails.RateCategories != null &&
                roomRateDetails.RateCategories.Count > 0)
            {
                foreach (RateCategory rateCategory in roomRateDetails.RateCategories)
                {
                    if (rateCategory != null)
                    {
                        string rateCategoryId = rateCategory.RateCategoryId;
                        string rateCategoryName = rateCategory.RateCategoryName;
                        rateCategoryDisplayList.Add(new RateCategoryHeaderDisplay(rateCategoryId, rateCategoryName,
                                                                                  rateCategory.RateCategoryDescription,
                                                                                  rateCategory.RateCategoryURL,
                                                                                  rateCategory.CoulmnNumber,
                                                                                  rateCategory.DisplayMoreInfoLink,
                                                                                  rateCategory.RateCategoryColor,
                                                                                  rateCategory.RateHighlightTextWeb,
                                                                                  rateCategory.RateHighlightTextMobile,
                                                                                  rateCategory.RateCategoryLanguage,
                                                                                  rateCategory.HeaderList));
                    }
                }
            }
        }

        /// <summary>
        /// Gets RateCategoriesDisplay
        /// </summary>
        public override List<RateCategoryHeaderDisplay> RateCategoriesDisplay
        {
            get { return rateCategoryDisplayList; }
        }


        /// <summary>
        /// Gets the max rate for each room.
        /// </summary>
        /// <value>The max rate for each room.</value>
        public override string MaxRateForEachRoom
        {
            get { return maxRateForEachRoom; }
        }

        /// <summary>
        /// Gets MinRateForEachRoom
        /// </summary>
        public override string MinRateForEachRoom
        {
            get { return minRateForEachRoom; }
        }

        /// <summary>
        /// Gets MinRatePerStayForEachRoom
        /// </summary>
        public override string MinRatePerStayForEachRoom
        {
            get { return minRatePerStayForEachRoom; }
        }

        /// <summary>
        /// Gets MaxRatePerStayForEachRoom
        /// </summary>
        public override string MaxRatePerStayForEachRoom
        {
            get { return maxRatePerStayForEachRoom; }
        }

        /// <summary>
        /// Gets rate cell to display
        /// </summary>
        /// <param name="roomCategory"></param>
        /// <param name="rateCategorySlNo"></param>
        /// <param name="rateCategoryID"></param>
        /// <param name="showPricePerStay"></param>
        /// <param name="roomNumber"></param>
        /// <returns></returns>
        public override RateCellDisplay GetRateCellDisplay(RoomCategoryEntity roomCategory, int rateCategorySlNo,
                                                           string rateCategoryID, bool showPricePerStay, int roomNumber)
        {
            BonusChequeRoomRateDetails roomRateDetailsCasted = roomRateDetails as BonusChequeRoomRateDetails;
            RateCellDisplay rateCell = new RateCellDisplay();
            RateEntity rate;
            int noOfNights = 0;
            int noOfRooms = 0;
            string stringFormat = "{0} {1}";
            string convertedRateString = string.Empty;
            string parseRateValue = string.Empty;
            string bonusCheckString = string.Empty;
            if (showPricePerStay)
            {
                rate = roomCategory.GetCommonTotalRate(rateCategoryID);
                noOfNights = RoomRateDisplayUtil.GetNoOfNights();
                noOfRooms = AppConstants.PER_ROOM; 
            }
            else
            {
                rate = roomCategory.GetCommonBaseRate(rateCategoryID);
                noOfNights = AppConstants.PER_NIGHT;
                noOfRooms = AppConstants.PER_ROOM;
            }

            rateCell.RateString = string.Empty;
            string[] bonusChequeRates = RoomRateUtil.GetBonusChequeRateCodes();
            if (null != rate)
            {
                if (StringUtil.IsStringInArray(bonusChequeRates, rateCategoryID))
                {
                    ExchangeRateManager exchangeRateManager = new ExchangeRateManager();
                    rateCell.RateString =
                        WebUtil.GetBonusChequeRateString("/bookingengine/booking/selectrate/bonuschequerate",
                                                         roomRateDetailsCasted.ArrivalDate.Year,
                                                         roomRateDetailsCasted.CountryCode, rate.Rate, rate.CurrencyCode,
                                                         noOfNights, noOfRooms);
                    if (rateCell.RateString.Split(Convert.ToChar(AppConstants.PLUS)).Length > 1)
                    {
                        parseRateValue =
                            rateCell.RateString.Split(Convert.ToChar(AppConstants.PLUS))[1].Trim().Split(
                                Convert.ToChar(AppConstants.SPACE))[0];
                        convertedRateString = exchangeRateManager.GetExchangeRateString(stringFormat, rate.CurrencyCode,
                                                                                        parseRateValue);
                        if (convertedRateString != string.Empty)
                        {
                            bonusCheckString = rateCell.RateString.Split(Convert.ToChar(AppConstants.PLUS))[0];
                            rateCell.AlternateRateString = String.Format("{0}{1}{2}{3}", bonusCheckString,
                                                                         AppConstants.PLUS, AppConstants.SPACE,
                                                                         convertedRateString);
                        }
                    }
                    rateCell.BookUrl = "javascript:RedirectToBookDetail(\'" + rateCategorySlNo + "\', \'" +
                                       rateCategoryID + "\', \'" + roomNumber + "\')";
                }
            }

            return rateCell;
        }

        /// <summary>
        /// Gets the rate display string for shopping cart.
        /// </summary>
        /// <param name="roomCategory">The room category.</param>
        /// <param name="rateCategoryID">The rate category ID.</param>
        /// <param name="showPricePerStay">if set to <c>true</c> [show price per stay].</param>
        /// <param name="roomNumber">The room number.</param>
        /// <returns></returns>
        public override RateCellDisplay GetRateDisplayStringForShoppingCart(RoomCategoryEntity roomCategory,
                                                                            string rateCategoryID, bool showPricePerStay,
                                                                            int roomNumber)
        {
            BonusChequeRoomRateDetails roomRateDetailsCasted = roomRateDetails as BonusChequeRoomRateDetails;
            RateCellDisplay rateCell = new RateCellDisplay();
            RateEntity rate;
            int noOfNights = 0;
            int noOfRooms = 0;

            if (showPricePerStay)
            {
                rate = roomCategory.GetCommonTotalRate(rateCategoryID);

                noOfNights = RoomRateDisplayUtil.GetNoOfNights();
                noOfRooms = AppConstants.PER_ROOM; 
            }
            else
            {
                rate = roomCategory.GetCommonBaseRate(rateCategoryID);
                noOfNights = AppConstants.PER_NIGHT;
                noOfRooms = AppConstants.PER_ROOM;
            }

            rateCell.RateString = string.Empty;
            string[] bonusChequeRates = RoomRateUtil.GetBonusChequeRateCodes();
            if ((null != rate) && (roomRateDetailsCasted != null))
            {
                if (StringUtil.IsStringInArray(bonusChequeRates, rateCategoryID))
                {
                    rateCell.RateString =
                        WebUtil.GetBonusChequeRateString("/bookingengine/booking/selectrate/bonuschequerate",
                                                         roomRateDetailsCasted.ArrivalDate.Year,
                                                         roomRateDetailsCasted.CountryCode, rate.Rate, rate.CurrencyCode,
                                                         noOfNights, noOfRooms);
                }
            }
            return rateCell;
        }

        /// <summary>
        /// Gets the total rate display string for shopping cart.
        /// </summary>
        /// <param name="showPricePerStay">if set to <c>true</c> [show price per stay].</param>
        /// <param name="total">The total.</param>
        /// <param name="CurrencyCode">The currency code.</param>
        /// <returns></returns>
        public override string GetTotalRateDisplayStringForShoppingCart(bool showPricePerStay, double total,
                                                                        string CurrencyCode)
        {
            BonusChequeRoomRateDetails roomRateDetailsCasted = roomRateDetails as BonusChequeRoomRateDetails;
            int noOfNights = 0;
            int noOfRooms = 0;

            if (showPricePerStay)
            {
                noOfNights = RoomRateDisplayUtil.GetNoOfNights();
                if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null &&
                    HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count > 0)
                    noOfRooms = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count;
            }
            else
            {
                noOfNights = AppConstants.PER_NIGHT;
                noOfRooms = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count;
            }

            string countryCode = ((roomRateDetailsCasted != null) && (roomRateDetailsCasted.CountryCode != null))
                                     ? roomRateDetailsCasted.CountryCode
                                     : string.Empty;
            string totalRateString =
                WebUtil.GetBonusChequeRateString("/bookingengine/booking/selectrate/bonuschequerate",
                                                 roomRateDetailsCasted.ArrivalDate.Year,
                                                 countryCode, total, CurrencyCode, noOfNights, noOfRooms);

            return totalRateString;
        }


        private void SetMinMaxDisplay(BonusChequeRoomRateDetails roomRateDetails)
        {
            BonusChequeRoomRateDetails roomRateDetailsCasted = roomRateDetails as BonusChequeRoomRateDetails;
         
            if (roomRateDetailsCasted != null && roomRateDetails != null)
            {
                if (_isRoomsTab)
                {
                    this.minRateForEachRoom = ((roomRateDetails.MinRateForEachRoom != null) &&
                                               (roomRateDetails.MinRateForEachRoom.Rate != null))
                                                  ? (WebUtil.GetBonusChequeRateString(
                                                      "/bookingengine/booking/selectrate/BCrate",
                                                      roomRateDetailsCasted.ArrivalDate.Year,
                                                      roomRateDetailsCasted.CountryCode,
                                                      roomRateDetails.MinRateForEachRoom.Rate,
                                                      roomRateDetails.MinRateForEachRoom.CurrencyCode,
                                                      AppConstants.PER_NIGHT, AppConstants.PER_ROOM))
                                                  : string.Empty;
                }
                else
                {
                    this.minRateForEachRoom = (((roomRateDetails.MinRateForEachRoom != null) &&
                                                (roomRateDetails.MinRateForEachRoom.Rate != null))
                                                   ? (WebUtil.GetBonusChequeRateString(
                                                       "/bookingengine/booking/selectrate/bonuschequerate",
                                                       roomRateDetailsCasted.ArrivalDate.Year,
                                                       roomRateDetailsCasted.CountryCode,
                                                       roomRateDetails.MinRateForEachRoom.Rate,
                                                       roomRateDetails.MinRateForEachRoom.CurrencyCode,
                                                       AppConstants.PER_NIGHT, AppConstants.PER_ROOM))
                                                   : string.Empty);
                }

                this.maxRateForEachRoom = ((roomRateDetails.MaxRateForEachRoom != null) &&
                                           (roomRateDetails.MaxRateForEachRoom.Rate != null))
                                              ? (WebUtil.GetBonusChequeRateString(
                                                  "/bookingengine/booking/selectrate/bonuschequerate",
                                                  roomRateDetailsCasted.ArrivalDate.Year,
                                                  roomRateDetailsCasted.CountryCode,
                                                  roomRateDetails.MaxRateForEachRoom.Rate,
                                                  roomRateDetails.MaxRateForEachRoom.CurrencyCode,
                                                  AppConstants.PER_NIGHT, AppConstants.PER_ROOM))
                                              : string.Empty;

                if (_isRoomsTab)
                {
                    this.minRatePerStayForEachRoom = ((roomRateDetails.MinRatePerStayForEachRoom != null) &&
                                                      (roomRateDetails.MinRatePerStayForEachRoom.Rate != null))
                                                         ? (WebUtil.GetBonusChequeRateString(
                                                             "/bookingengine/booking/selectrate/BCrate",
                                                             roomRateDetailsCasted.ArrivalDate.Year,
                                                             roomRateDetailsCasted.CountryCode,
                                                             roomRateDetails.MinRatePerStayForEachRoom.Rate,
                                                             roomRateDetails.MinRatePerStayForEachRoom.CurrencyCode,
                                                             RoomRateDisplayUtil.GetNoOfNights(), AppConstants.PER_ROOM))
                                                         : string.Empty;
                }
                else
                {
                    this.minRatePerStayForEachRoom = ((roomRateDetails.MinRatePerStayForEachRoom != null) &&
                                                      (roomRateDetails.MinRatePerStayForEachRoom.Rate != null))
                                                         ? (WebUtil.GetBonusChequeRateString(
                                                             "/bookingengine/booking/selectrate/bonuschequerate",
                                                             roomRateDetailsCasted.ArrivalDate.Year,
                                                             roomRateDetailsCasted.CountryCode,
                                                             roomRateDetails.MinRatePerStayForEachRoom.Rate,
                                                             roomRateDetails.MinRatePerStayForEachRoom.CurrencyCode,
                                                             RoomRateDisplayUtil.GetNoOfNights(), AppConstants.PER_ROOM))
                                                         : string.Empty;
                }
                this.maxRatePerStayForEachRoom = ((roomRateDetails.MaxRatePerStayForEachRoom != null) &&
                                                  (roomRateDetails.MaxRatePerStayForEachRoom.Rate != null))
                                                     ? (WebUtil.GetBonusChequeRateString(
                                                         "/bookingengine/booking/selectrate/bonuschequerate",
                                                         roomRateDetailsCasted.ArrivalDate.Year,
                                                         roomRateDetailsCasted.CountryCode,
                                                         roomRateDetails.MaxRatePerStayForEachRoom.Rate,
                                                         roomRateDetails.MaxRatePerStayForEachRoom.CurrencyCode,
                                                         RoomRateDisplayUtil.GetNoOfNights(), AppConstants.PER_ROOM))
                                                     : string.Empty;
            }
        }
    }
}