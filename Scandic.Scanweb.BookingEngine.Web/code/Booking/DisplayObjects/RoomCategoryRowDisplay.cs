namespace Scandic.Scanweb.BookingEngine.Web.code.Booking.DisplayObjects
{
    /// <summary>
    /// RoomCategoryRowDisplay
    /// </summary>
    public class RoomCategoryRowDisplay
    {
        private string id;
        private string name;
        private string description;
        private string url;
    }
}