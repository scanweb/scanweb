using Scandic.Scanweb.BookingEngine.Controller;
using System;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// RegularSearchHotelDisplay
    /// </summary>
    public class RegularSearchHotelDisplay : BaseHotelDisplay
    {
        /// <summary>
        /// Default Consructor
        /// </summary>
        public RegularSearchHotelDisplay()
        {
        }

        /// <summary>
        /// Overloaded Constructor
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="isPerStay"></param>
        public RegularSearchHotelDisplay(RegularHotelDetails hotelDetails, bool isPerStay)
        {
            if (hotelDetails != null)
            {
                SetHotelDestinationDetails(hotelDetails.HotelDestination);
                SetPriceString(hotelDetails, isPerStay);
            }
        }

        /// <summary>
        /// SetPriceString
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="isPerStay"></param>
        public void SetPriceString(IHotelDetails hotelDetails, bool isPerStay)
        {
            string disclaimerTextLine1 = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/disclaimertextline1");
            string disclaimerTextLine2 = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/disclaimertextline2");
            string approxText = String.Format("{0}{1}", WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/about"), AppConstants.SPACE);
            string convertedRateString = string.Empty;
            ExchangeRateManager exchangeRateManager = new ExchangeRateManager();
            if (isPerStay)
            {
                string rateString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/ratestring");
                string strFormat = rateString;
                string MinRatePerStay = string.Empty;
                string currencyCode = string.Empty;
                if (null != hotelDetails.MinRatePerStay)
                {
                    MinRatePerStay = hotelDetails.MinRatePerStay.Rate.ToString();
                    currencyCode = hotelDetails.MinRatePerStay.CurrencyCode;
                }
                rateString = string.Format(rateString, MinRatePerStay, currencyCode);
                this.price = rateString;
                this.altPriceTooltipLine1 = disclaimerTextLine1;
                this.altPriceTooltipLine2 = disclaimerTextLine2;
                convertedRateString = exchangeRateManager.GetExchangeRateString(strFormat, rateString.Split(Convert.ToChar(AppConstants.SPACE))[1],
                                                                            rateString.Split(Convert.ToChar(AppConstants.SPACE))[0]);
                if (convertedRateString != string.Empty)
                {
                    this.alternatePrice = String.Format("{0}{1}", approxText, convertedRateString);
                }
                RateString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/roomperstay");
            }
            else
            {
                string rateString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/ratestring");
                string strFormat = rateString;
                string minRate = string.Empty;
                string currencyCode = string.Empty;
                if (null != hotelDetails.MinRate)
                {
                    minRate = hotelDetails.MinRate.Rate.ToString();
                    currencyCode = hotelDetails.MinRate.CurrencyCode;
                }
                rateString = string.Format(rateString, minRate, currencyCode);
                this.price = rateString;

                this.perNight = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/perroompernight");
                this.altPriceTooltipLine1 = disclaimerTextLine1;
                this.altPriceTooltipLine2 = disclaimerTextLine2;
                convertedRateString = exchangeRateManager.GetExchangeRateString(strFormat, rateString.Split(Convert.ToChar(AppConstants.SPACE))[1],
                                                                            rateString.Split(Convert.ToChar(AppConstants.SPACE))[0]);
                if (convertedRateString != string.Empty)
                {
                    this.alternatePrice = String.Format("{0}{1}", approxText, convertedRateString);
                }
                RateString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/perroompernight");
            }
        }
    }
}