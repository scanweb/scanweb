#region Revision history
///  Description					  : This Interface is used to display the AvailabilityCalender Items.
///----------------------------------------------------------------------------------------------------------
/// Author						      : Bhavya Shekar
/// Creation Date				      : 13-July-2010
///	Version	#					      : 1.0													  
///----------------------------------------------------------------------------------------------------------
/// Revision History				  : 
///	Last Modified Date			      :	
///	Modified By                       : 
///	Latest Version                    : 
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace Scandic.Scanweb.BookingEngine.Web
{
    public interface IAvailabilityCalendarItem
    {
        string ID
        {
            get;
        }
      
        string ArrivalDate
        {
            get;
        }

         string MinPerNightRateString
        {
            get;
        }


         string MaxPerNightRateString
        {
            get;
        }


        string MinPerStayRateString
        {
            get;
        }


         string MaxPerStayRateString
        {
            get;
        }
    }
}
