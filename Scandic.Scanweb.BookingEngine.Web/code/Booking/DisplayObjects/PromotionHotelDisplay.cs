//  Description					:   PromotionHotelDisplay                                 //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// This class represents promotion hotel display controller.
    /// </summary>
    public class PromotionHotelDisplay : RegularSearchHotelDisplay
    {
        /// <summary>
        /// Constructor of PromotionHotelDisplay class.
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="isPerStay"></param>
        public PromotionHotelDisplay(PromotionHotelDetails hotelDetails, bool isPerStay)
        {
            SetHotelDestinationDetails(hotelDetails.HotelDestination);
            SetPriceString(hotelDetails, isPerStay);
            if (hotelDetails.HasPromotionRoomRates)
            {
                this.rateTitle = WebUtil.GetTranslatedText(AppConstants.PROMO_RATE_MESSAGE)
                    + AppConstants.SPACE + AppConstants.HYPHEN + AppConstants.SPACE + hotelDetails.promotionCode;
            }
            else
            {
                this.noDiscount = WebUtil.GetTranslatedText(AppConstants.NO_DISCOUNT_MESSAGE);
                //this.FromText = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from");
            }
        }

        /// <summary>
        ///  Constructor of PromotionHotelDisplay class.
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="isPerStay"></param>
        public PromotionHotelDisplay(RegularHotelDetails hotelDetails, bool isPerStay)
        {
            SetHotelDestinationDetails(hotelDetails.HotelDestination);
            SetPriceString(hotelDetails, isPerStay);
        }
    }
}