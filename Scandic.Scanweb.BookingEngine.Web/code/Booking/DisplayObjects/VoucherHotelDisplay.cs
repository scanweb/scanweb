using Scandic.Scanweb.BookingEngine.Controller;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// VoucherHotelDisplay
    /// </summary>
    public class VoucherHotelDisplay : BaseHotelDisplay
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="isPerStay"></param>
        public VoucherHotelDisplay(RegularHotelDetails hotelDetails, bool isPerStay)
        {
            SetHotelDestinationDetails(hotelDetails.HotelDestination);
            SetPriceString(hotelDetails, isPerStay);
        }

        /// <summary>
        /// SetPriceString
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="isPerStay"></param>
        protected void SetPriceString(IHotelDetails hotelDetails, bool isPerStay)
        {

			//MERCHANDISING | CR:Have number of gift vouchers printed |Parvathi
            this.price = Utility.GetNumberofVouchers();

            //start - defect artf1155244 - mayur - set correct voucher string, rate value as per "per night" or "per stay"
            if (isPerStay)
            {
                this.perNight = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rdoperStay").ToLower();
                this.RateString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rdoperStay").ToLower();
            }
            else
            {
                this.perNight = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rdoPernight").ToLower();
                this.RateString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rdoPernight").ToLower();
            }
        }
    }
}