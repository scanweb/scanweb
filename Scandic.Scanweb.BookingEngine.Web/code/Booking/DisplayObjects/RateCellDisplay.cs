namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// Rate Cell Display
    /// </summary>
    public class RateCellDisplay
    {
        public string RateString { get; set; }

        public string AlternateRateString { get; set; }

        public string BookUrl { get; set; }

        public string BookString { get; set; }
    }
}