#region Revision history

///  Description					  : This Interface is used to display the hotel details.
///----------------------------------------------------------------------------------------------------------
/// Author						      : Kshipra Thombre
/// Creation Date				      : 17-Sept-2009
///	Version	#					      : 1.0													  
///----------------------------------------------------------------------------------------------------------
/// Revision History				  : 
///	Last Modified Date			      :	
///	Modified By                       : 
///	Latest Version                    : 

#endregion

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// HotelDisplayInformation interface
    /// </summary>
    public interface IHotelDisplayInformation
    {
        /// <summary>
        /// Gets ID
        /// </summary>
        string ID { get; }

        /// <summary>
        /// Gets ImageSrc
        /// </summary>
        string ImageSrc { get; }

        /// <summary>
        /// Gets ImageAltText
        /// </summary>
        string ImageAltText { get; }

        /// <summary>
        /// Gets HotelURL
        /// </summary>
        string HotelURL { get; }

        /// <summary>
        /// Gets HotelName
        /// </summary>
        string HotelName { get; }

        /// <summary>
        /// Gets RateTitle
        /// </summary>
        string RateTitle { get; }

        /// <summary>
        /// Gets RateTitle
        /// </summary>
        string NoDiscount { get; }

        /// <summary>
        /// Gets Price
        /// </summary>
        string Price { get; }

        /// <summary>
        /// Gets PerNight
        /// </summary>
        string PerNight { get; }

        /// <summary>
        /// Gets RateString
        /// </summary>
        string RateString { get; }

        /// <summary>
        /// Gets CityCenterDistance
        /// </summary>
        string CityCenterDistance { get; set; }

        /// <summary>
        /// Gets SelectHotel
        /// </summary>
        string SelectHotel { get; }

        /// <summary>
        /// Gets Address
        /// </summary>
        string Address { get; }

        /// <summary>
        /// Gets Teaser
        /// </summary>
        string Teaser { get; }

        /// <summary>
        /// Gets XML
        /// </summary>
        string XML { get; }

        /// <summary>
        /// Gets Distance
        /// </summary>
        string Distance { get; }

        /// <summary>
        /// Gets Direction
        /// </summary>
        string Direction { get; }

        /// <summary>
        /// Gets DrivingTime
        /// </summary>
        string DrivingTime { get; }

        /// <summary>
        /// Gets/Sets HotelSearch
        /// </summary>
        string HotelSearch { get; set; }

        /// <summary>
        /// Gets/Sets IsUnblockRequired
        /// </summary>
        bool IsUnblockRequired { get; set; }

        string FromText { get; }

        int TALocationID { get; set; }

        string TripAdvisorWidget { get; set; }

        bool HideHotelTARatings { get; set; }

        //HotelRateDiscount HotelPriceDiscount { get; set; }
    }
}