//  Description					: RedemptionRoomRateDisplay                               //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                                                         //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				:                                                         //
///	Last Modified Date			:                                                         //
////////////////////////////////////////////////////////////////////////////////////////////


using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// This class contains members to manipulate room rate display.
    /// </summary>
    public class RedemptionRoomRateDisplay : BaseRateDisplay
    {
        private List<RateCategoryHeaderDisplay> rateCategoryDisplayList = new List<RateCategoryHeaderDisplay>();
        private int noOfNights;

        private string minPointsForEachRoom;
        private string maxPointsForEachRoom;

        /// <summary>
        /// Constructor of RedemptionRoomRateDisplay
        /// </summary>
        /// <param name="roomRateDetails"></param>
        public RedemptionRoomRateDisplay(RedemptionRoomRateDetails roomRateDetails)
        {
            if (roomRateDetails != null)
            {
                this.roomRateDetails = roomRateDetails;
                this.noOfNights = roomRateDetails.NoOfNights;
                SetRateCategoriesDisplay();
                this.minPointsForEachRoom = ((roomRateDetails.MinPointsForEachRoom != null)
                                             && (roomRateDetails.MinPointsForEachRoom.PointsRequired != null))
                                                ? (roomRateDetails.MinPointsForEachRoom.PointsRequired.ToString())
                                                : string.Empty;
                this.maxPointsForEachRoom = ((roomRateDetails.MaxPointsForEachRoom != null)
                                             && (roomRateDetails.MaxPointsForEachRoom.PointsRequired != null))
                                                ? (roomRateDetails.MaxPointsForEachRoom.PointsRequired.ToString())
                                                : string.Empty;
            }
        }

        /// <summary>
        /// Sets rate categories that needs to be displayed
        /// </summary>
        public void SetRateCategoriesDisplay()
        {
            foreach (RateCategory rateCategory in roomRateDetails.RateCategories)
            {
                if (rateCategory != null)
                {
                    rateCategoryDisplayList.Add(new RateCategoryHeaderDisplay(rateCategory.RateCategoryId,
                                                                              rateCategory.RateCategoryName,
                                                                              rateCategory.RateCategoryDescription,
                                                                              rateCategory.RateCategoryURL,
                                                                              rateCategory.CoulmnNumber,
                                                                              rateCategory.DisplayMoreInfoLink,
                                                                              rateCategory.RateCategoryColor,
                                                                              rateCategory.RateHighlightTextWeb,
                                                                              rateCategory.RateHighlightTextMobile,
                                                                              rateCategory.RateCategoryLanguage,
                                                                              rateCategory.HeaderList));
                }
            }
        }

        /// <summary>
        /// Gets RateCategoriesDisplay
        /// </summary>
        public override List<RateCategoryHeaderDisplay> RateCategoriesDisplay
        {
            get { return rateCategoryDisplayList; }
        }

        /// <summary>
        /// Gets rate cell to display
        /// </summary>
        /// <param name="roomCategory"></param>
        /// <param name="rateCategorySlNo"></param>
        /// <param name="rateCategoryID"></param>
        /// <param name="showPricePerStay"></param>
        /// <param name="roomNumber"></param>
        /// <returns></returns>
        public override RateCellDisplay GetRateCellDisplay(RoomCategoryEntity roomCategory, int rateCategorySlNo,
                                                           string rateCategoryID, bool showPricePerStay, int roomNumber)
        {
            RateCellDisplay rateCell = new RateCellDisplay();
            double points;
            string stringFormat;
            if (showPricePerStay)
            {
                points = ((roomCategory.GetCommonPoints(rateCategoryID) != null)
                          && (roomCategory.GetCommonPoints(rateCategoryID).PointsRequired) != null)
                             ? (roomCategory.GetCommonPoints(rateCategoryID).PointsRequired*noOfNights)
                             : 0;
                stringFormat = GetPerStayStringFormat();
            }
            else
            {
                points = (roomCategory.GetCommonPoints(rateCategoryID) != null)
                             ? roomCategory.GetCommonPoints(rateCategoryID).PointsRequired
                             : 0;
                stringFormat = GetPerNightStringFormat();
            }

            rateCell.RateString = string.Empty;
            if (null != points)
            {
                rateCell.RateString = string.Format(stringFormat, points);
                rateCell.BookUrl = "javascript:RedirectToBookDetail(\'"
                                   + rateCategorySlNo + "\', \'" + rateCategoryID + "\', \'" + roomNumber + "\')";
            }

            return rateCell;
        }


        /// <summary>
        /// Gets the rate cell display for shopping cart.
        /// </summary>
        /// <param name="roomCategory">The room category.</param>
        /// <param name="rateCategoryID">The rate category ID.</param>
        /// <param name="showPricePerStay">if set to <c>true</c> [show price per stay].</param>
        /// <param name="roomNumber">The room number.</param>
        /// <returns></returns>
        public override RateCellDisplay GetRateDisplayStringForShoppingCart(RoomCategoryEntity roomCategory,
                                                                            string rateCategoryID,
                                                                            bool showPricePerStay, int roomNumber)
        {
            RateCellDisplay rateCell = new RateCellDisplay();
            double points;
            string stringFormat;
            if (showPricePerStay)
            {
                points = ((roomCategory.GetCommonPoints(rateCategoryID) != null)
                          && (roomCategory.GetCommonPoints(rateCategoryID).PointsRequired != null))
                             ? roomCategory.GetCommonPoints(rateCategoryID).PointsRequired*noOfNights
                             : 0;
                stringFormat = GetPerStayStringFormat();
            }
            else
            {
                points = (roomCategory.GetCommonPoints(rateCategoryID) != null)
                             ? (roomCategory.GetCommonPoints(rateCategoryID).PointsRequired)
                             : 0;
                stringFormat = GetPerNightStringFormat();
            }

            rateCell.RateString = string.Empty;
            if (null != points)
            {
                rateCell.RateString = string.Format(stringFormat, points);
            }

            return rateCell;
        }

        /// <summary>
        /// Gets the total rate display string for shopping cart.
        /// </summary>
        /// <param name="showPricePerStay">if set to <c>true</c> [show price per stay].</param>
        /// <param name="total">The total.</param>
        /// <param name="CurrencyCode">The currency code.</param>
        /// <returns></returns>
        public override string GetTotalRateDisplayStringForShoppingCart(bool showPricePerStay, double total,
                                                                        string currencyCode)
        {
            string stringFormat;
            double points;
            if (showPricePerStay)
            {
                points = total*noOfNights;
                stringFormat = GetPerStayStringFormat();
            }
            else
            {
                points = total;
                stringFormat = GetPerNightStringFormat();
            }
            string totalRateString = string.Format(stringFormat, points.ToString(), currencyCode);
            return totalRateString;
        }

        private string GetPerNightStringFormat()
        {
            return WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/redemptionratepernight");
        }

        private string GetPerStayStringFormat()
        {
            return WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/redemptionrateperstay");
        }


        /// <summary>
        /// Gets the max rate for each room.
        /// </summary>
        /// <value>The max rate for each room.</value>
        public override string MaxRateForEachRoom
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Gets MinRateForEachRoom
        /// </summary>
        public override string MinRateForEachRoom
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Gets MinRatePerStayForEachRoom
        /// </summary>
        public override string MinRatePerStayForEachRoom
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Gets MaxRatePerStayForEachRoom
        /// </summary>
        public override string MaxRatePerStayForEachRoom
        {
            get { return string.Empty; }
        }
    }
}