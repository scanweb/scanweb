#region Revision history

///  Description					  : This interface is used to display the hotel details in the hotel listing control
///----------------------------------------------------------------------------------------------------------
/// Author						      : Kshipra Thombre
/// Creation Date				      : 17-Sept-2009
///	Version	#					      : 1.0													  
///----------------------------------------------------------------------------------------------------------
/// Revision History				  : 
///	Last Modified Date			      :	
///	Modified By                       : 
///	Latest Version                    : 

#endregion

#region using



#endregion using

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// interface contains all the property used to display data on the HotelListing control
    /// </summary>
    public interface IHotelListingDisplayInformation
    {
        #region Properties

        string ID { get; }

        string ImageSrc { get; }

        string ImageAltText { get; }

        string HotelURL { get; }

        string HotelName { get; }

        string Description { get; }

        string Address { get; }
        
        string Distance { get; }

        int Counter { get; }

        int TALocationID { get; set; }

        bool HideHotelTARatings { get; set; }
        
        bool HideCityTARatings { get; set; }

        double TripAdvisorHotelRating { get; set; }

        #endregion Properties
    }
}