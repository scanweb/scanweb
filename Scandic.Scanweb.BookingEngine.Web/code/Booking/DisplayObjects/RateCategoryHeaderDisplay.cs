using System.Collections.Generic;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// RateCategoryHeaderDisplay
    /// </summary>
    public class RateCategoryHeaderDisplay
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string URL { get; set; }

        public int CoulmnNumber { get; set; }

        private bool showLinkURL;

        public bool ShowLinkURL
        {
            get { return showLinkURL; }
        }

        private string rateCategoryColor = string.Empty;
        public string RateCategoryColor
        {
            get { return rateCategoryColor; }
        }
        private string rateHighlightTextWeb = string.Empty;
        public string RateHighlightTextWeb
        {
            get { return rateHighlightTextWeb; }
        }
        private string rateHighlightTextMobile = string.Empty;
        public string RateHighlightTextMobile
        {
            get { return rateHighlightTextMobile; }
        }

        private string rateCategoryLanguage = string.Empty;
        public string RateCategoryLanguage
        {
            get { return rateCategoryLanguage; }
        }

        private List<string> headers;
        
        public List<string> Headers
        {
            get { return headers; }
        }

        /// <summary>
        /// Gets or sets the about our rate.
        /// </summary>
        /// <value>The about our rate.</value>
        public string AboutOurRate { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="url"></param>
        /// <param name="coulmnNumber"></param>
        /// <param name="showLinkURL"></param>
        /// <param name="rateCategoryColor"></param>
        /// <param name="headers"></param>
        /// <param name="blockRateTolTip"></param>
        /// <param name="aboutOurRate"></param>
        public RateCategoryHeaderDisplay(string id, string title, string description, string url, int coulmnNumber,
                                         bool showLinkURL, string rateCategoryColor, string rateHighlightTextWeb, string rateHighlightTextMobile,string rateCategoryLanguage, List<string> headers)
        {
            this.Id = id;
            this.Title = title;
            this.Description = description;
            this.URL = url;
            this.CoulmnNumber = coulmnNumber;
            this.showLinkURL = showLinkURL;
            this.rateCategoryColor = rateCategoryColor;
            this.rateHighlightTextWeb = rateHighlightTextWeb;
            this.rateHighlightTextMobile = rateHighlightTextMobile;
            this.rateCategoryLanguage = rateCategoryLanguage;
            this.headers = headers;
        }
    }

    /// <summary>
    /// SortHeaderDisplay
    /// </summary>
    public class SortHeaderDisplay : IComparer<RateCategoryHeaderDisplay>
    {
        public int Compare(RateCategoryHeaderDisplay first, RateCategoryHeaderDisplay second)
        {
            return (second.CoulmnNumber.CompareTo(first.CoulmnNumber));
        }
    }
}