using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Scandic.Scanweb.BookingEngine.Entity;
using Scandic.Scanweb.BookingEngine.Controller;

namespace Scandic.Scanweb.BookingEngine.Web

{
    public class AvailabilityCalendarItem:IAvailabilityCalendarItem
    {
        private string id;
        public string ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        private string arrivalDate;
        public string ArrivalDate
        {
            get
            {
                return arrivalDate;
            }
            set
            {
                arrivalDate = value;
            }
        }

        private string minPerNightRateString;
        /// <summary>
        /// Gets or sets the min rate text.
        /// </summary>
        /// <value>The min rate text.</value>
        public string MinPerNightRateString
        {
            get { return minPerNightRateString; }
            set { minPerNightRateString = value; }
        }

        private string maxPerNightRateString;
        /// <summary>
        /// Gets or sets the max per night rate string.
        /// </summary>
        /// <value>The max per night rate string.</value>
        public string MaxPerNightRateString
        {
            get { return maxPerNightRateString; }
            set { maxPerNightRateString = value; }
        }


        private string minPerStayRateString;
        /// <summary>
        /// Gets or sets the min per stay rate string.
        /// </summary>
        /// <value>The min per stay rate string.</value>
        public string MinPerStayRateString
        {
            get { return minPerStayRateString; }
            set { minPerStayRateString = value; }
        }


        private string maxPerStayRateString;
        /// <summary>
        /// Gets or sets the max per stay rate string.
        /// </summary>
        /// <value>The max per stay rate string.</value>
        public string MaxPerStayRateString
        {
            get { return maxPerStayRateString; }
            set { maxPerStayRateString = value; }
        }

        //public bool IsSearchDone
        //{
        //    get { return isSearchDone; }
        //    set { isSearchDone = value; }
        //}

        public string XML
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                //sb.Append("<AvailabilityCalenderItem>");
                sb.Append("<ID>").Append(MinPerNightRateString).Append("</ID>");
                sb.Append("<ArrivalDate>").Append(ArrivalDate).Append("</ArrivalDate>");
                if (SessionWrapper.IsPerStaySelectedInSelectRatePage )
                {
                    sb.Append("<MinRateString>").Append(MinPerStayRateString).Append("</MinRateString>");
                }
                else
                {
                    sb.Append("<MinRateString>").Append(MinPerNightRateString).Append("</MinRateString>");
                }
                //sb.Append("<MinPerNightRateString>").Append(MinPerNightRateString).Append("</MinPerNightRateString>");
                //sb.Append("<MinPerStayRateString>").Append(MinPerStayRateString).Append("</MinPerStayRateString>");
                //sb.Append("</AvailabilityCalenderItem>");
                return sb.ToString();
            }
        }


            public AvailabilityCalendarItem(AvailCalendarItemEntity availCalenderEntity)
            {
                ID = "";
                MinPerNightRateString = availCalenderEntity.MinPerNightRateString;
                MinPerStayRateString = availCalenderEntity.MinPerStayRateString;
                ArrivalDate = availCalenderEntity.ArrivalDate.ToShortDateString();
            }

    }
}
