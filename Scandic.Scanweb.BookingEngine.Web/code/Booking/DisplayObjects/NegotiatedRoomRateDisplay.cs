using System;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// NegotiatedRoomRateDisplay
    /// </summary>
    public class NegotiatedRoomRateDisplay : BaseRateDisplay
    {
        private List<RateCategoryHeaderDisplay> rateCategoryDisplayList = new List<RateCategoryHeaderDisplay>();

        private string minRateForEachRoom;
        private string maxRateForEachRoom;
        private string minRatePerStayForEachRoom;
        private string maxRatePerStayForEachRoom;

        /// <summary>
        /// NegotiatedRoomRateDisplay
        /// </summary>
        /// <param name="roomRateDetails"></param>
        public NegotiatedRoomRateDisplay(NegotiatedRoomRateDetails roomRateDetails)
        {
            if (roomRateDetails != null)
            {
                this.roomRateDetails = roomRateDetails;
                SetRateCategoriesDisplay();
                this.minRateForEachRoom =
                    (roomRateDetails.MinRateForEachRoom != null)
                        ? (roomRateDetails.MinRateForEachRoom.Rate.ToString() + " " +
                           roomRateDetails.MinRateForEachRoom.CurrencyCode)
                        : string.Empty;
                this.maxRateForEachRoom =
                    (roomRateDetails.MaxRateForEachRoom != null)
                        ? (roomRateDetails.MaxRateForEachRoom.Rate.ToString() + " " +
                           roomRateDetails.MaxRateForEachRoom.CurrencyCode)
                        : string.Empty;
                this.minRatePerStayForEachRoom =
                    (roomRateDetails.MinRatePerStayForEachRoom != null)
                        ? (roomRateDetails.MinRatePerStayForEachRoom.Rate.ToString() + " "
                           + roomRateDetails.MinRatePerStayForEachRoom.CurrencyCode)
                        : string.Empty;
                this.maxRatePerStayForEachRoom =
                    (roomRateDetails.MaxRatePerStayForEachRoom != null)
                        ? (roomRateDetails.MaxRatePerStayForEachRoom.Rate.ToString() + " " +
                           roomRateDetails.MaxRatePerStayForEachRoom.CurrencyCode)
                        : string.Empty;
            }
        }

        /// <summary>
        /// SetRateCategoriesDisplay
        /// </summary>
        public void SetRateCategoriesDisplay()
        {
            NegotiatedRoomRateDetails roomRateDetailsCasted = roomRateDetails as NegotiatedRoomRateDetails;
            if
                (roomRateDetails != null &&
                 roomRateDetails.RateCategories != null && roomRateDetails.RateCategories.Count > 0)
            {
                foreach (RateCategory rateCategory in roomRateDetails.RateCategories)
                {
                    string rateCategoryId = String.Empty;
                    string rateCategoryName = String.Empty;
                    if (rateCategory != null)
                    {
                        rateCategoryId = rateCategory.RateCategoryId;
                        rateCategoryName = rateCategory.RateCategoryName;
                    }
                    if
                        (roomRateDetailsCasted != null &&
                         roomRateDetailsCasted.RateCategoryIdString.Contains(rateCategoryId))
                    {
                        rateCategoryName = roomRateDetailsCasted.CompanyName;
                    }
                    if (!string.IsNullOrEmpty(rateCategoryId))
                    {
                        rateCategoryDisplayList.Add
                            (new RateCategoryHeaderDisplay
                                 (rateCategoryId, rateCategoryName, rateCategory.RateCategoryDescription,
                                  rateCategory.RateCategoryURL, rateCategory.CoulmnNumber,
                                  rateCategory.DisplayMoreInfoLink,
                              rateCategory.RateCategoryColor, rateCategory.RateHighlightTextWeb, rateCategory.RateHighlightTextMobile, rateCategory.RateCategoryLanguage, rateCategory.HeaderList));
                    }
                }
            }
        }

        /// <summary>
        /// RateCategoriesDisplay
        /// </summary>
        public override List<RateCategoryHeaderDisplay> RateCategoriesDisplay
        {
            get { return rateCategoryDisplayList; }
        }

        /// <summary>
        /// GetRateCellDisplay
        /// </summary>
        /// <param name="roomCategory"></param>
        /// <param name="rateCategorySlNo"></param>
        /// <param name="rateCategoryID"></param>
        /// <param name="showPricePerStay"></param>
        /// <param name="roomNumber"></param>
        /// <returns>RateCellDisplay</returns>
        public override RateCellDisplay GetRateCellDisplay
            (RoomCategoryEntity roomCategory, int rateCategorySlNo, string rateCategoryID,
             bool showPricePerStay, int roomNumber)
        {
            RateCellDisplay rateCell = new RateCellDisplay();
            RateEntity rate;
            string stringFormat;
            if (showPricePerStay)
            {
                rate = roomCategory.GetCommonTotalRate(rateCategoryID);
                stringFormat = GetPerStayStringFormat();
            }
            else
            {
                rate = roomCategory.GetCommonBaseRate(rateCategoryID);
                stringFormat = GetPerNightStringFormat();
            }

            rateCell.RateString = string.Empty;
            if (null != rate && rate.Rate != null)
            {
                ExchangeRateManager exchangeRateManager = new ExchangeRateManager();
                string rateValue = rate.Rate.ToString();
                string currencyCode = rate.CurrencyCode;
                rateCell.RateString = string.Format(stringFormat, rateValue, currencyCode);
                rateCell.AlternateRateString = exchangeRateManager.GetExchangeRateString(stringFormat, currencyCode, rateValue);
                rateCell.BookUrl =
                    "javascript:RedirectToBookDetail(\'" + rateCategorySlNo + "\', \'" + rateCategoryID +
                    "\', \'" + roomNumber + "\')";
            }

            return rateCell;
        }

        /// <summary>
        /// Gets the rate display string for shopping cart.
        /// </summary>
        /// <param name="roomCategory">The room category.</param>
        /// <param name="rateCategoryID">The rate category ID.</param>
        /// <param name="showPricePerStay">if set to <c>true</c> [show price per stay].</param>
        /// <param name="roomNumber">The room number.</param>
        /// <returns></returns>
        public override RateCellDisplay GetRateDisplayStringForShoppingCart
            (RoomCategoryEntity roomCategory, string rateCategoryID, bool showPricePerStay, int roomNumber)
        {
            RateCellDisplay rateCell = new RateCellDisplay();
            RateEntity rate;
            string stringFormat;
            if (showPricePerStay)
            {
                rate = roomCategory.GetCommonTotalRate(rateCategoryID);
                stringFormat = GetPerStayStringFormat();
            }
            else
            {
                rate = roomCategory.GetCommonBaseRate(rateCategoryID);
                stringFormat = GetPerNightStringFormat();
            }

            rateCell.RateString = string.Empty;
            if (null != rate && rate.Rate != null)
            {
                string rateValue = rate.Rate.ToString();
                string currencyCode = rate.CurrencyCode;
                rateCell.RateString = string.Format(stringFormat, rateValue, currencyCode);
            }

            return rateCell;
        }

        /// <summary>
        /// Gets the total rate display string for shopping cart.
        /// </summary>
        /// <param name="showPricePerStay">if set to <c>true</c> [show price per stay].</param>
        /// <param name="total">The total.</param>
        /// <param name="currencyCode">The currency code.</param>
        /// <returns></returns>
        public override string GetTotalRateDisplayStringForShoppingCart
            (bool showPricePerStay, double total, string currencyCode)
        {
            string stringFormat;
            if (showPricePerStay)
            {
                stringFormat = GetPerStayStringFormat();
            }
            else
            {
                stringFormat = GetPerNightStringFormat();
            }

            string rateString = string.Format(stringFormat, total.ToString(), currencyCode);
            return rateString;
        }

        /// <summary>
        /// GetPerNightStringFormat
        /// </summary>
        /// <returns></returns>
        private string GetPerNightStringFormat()
        {
            return WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/normalratepernight");
        }

        /// <summary>
        /// GetPerStayStringFormat
        /// </summary>
        /// <returns></returns>
        private string GetPerStayStringFormat()
        {
            return WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/normalrateperstay");
        }

        /// <summary>
        /// Gets the max rate for each room.
        /// </summary>
        /// <value>The max rate for each room.</value>
        public override string MaxRateForEachRoom
        {
            get { return maxRateForEachRoom; }
        }

        public override string MinRateForEachRoom
        {
            get { return minRateForEachRoom; }
        }

        public override string MinRatePerStayForEachRoom
        {
            get { return minRatePerStayForEachRoom; }
        }

        public override string MaxRatePerStayForEachRoom
        {
            get { return maxRatePerStayForEachRoom; }
        }
    }
}