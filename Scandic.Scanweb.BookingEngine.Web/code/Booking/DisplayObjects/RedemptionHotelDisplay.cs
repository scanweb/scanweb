using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// RedemptionHotelDisplay
    /// </summary>
    public class RedemptionHotelDisplay : BaseHotelDisplay
    {
        public RedemptionHotelDisplay(RedemptionHotelDetails hotelDetails)
        {
            SetHotelDestinationDetails(hotelDetails.HotelDestination);
            SetPriceString(hotelDetails);
        }

        /// <summary>
        /// SetPriceString
        /// </summary>
        /// <param name="hotelDetails">RedemptionHotelDetails</param>
        private void SetPriceString(RedemptionHotelDetails hotelDetails)
        {
            string currentLanguage = Utility.GetCurrentLanguage();

            string redeemString =
                WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/redeempoints", currentLanguage);

            int noOfNights = 1;
            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
            {
                noOfNights = SearchCriteriaSessionWrapper.SearchCriteria.NoOfNights;
            }
            this.price =
                (hotelDetails != null &&
                 hotelDetails.MinPoints != null)
                    ? (string.Format(redeemString, hotelDetails.MinPoints.PointsRequired*noOfNights))
                    : string.Empty;

            this.perNight = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/perstay", currentLanguage);
        }
    }
}