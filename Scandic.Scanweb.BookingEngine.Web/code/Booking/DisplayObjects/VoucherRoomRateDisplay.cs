//  Description					: VoucherRoomRateDisplay                                  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// This class represents VoucherRoomRateDisplay
    /// </summary>
    public class VoucherRoomRateDisplay : BaseRateDisplay
    {
        private List<RateCategoryHeaderDisplay> rateCategoryDisplayList = new List<RateCategoryHeaderDisplay>();
        private string minRateForEachRoom;
        private string maxRateForEachRoom;
        private string minRatePerStayForEachRoom;
        private string maxRatePerStayForEachRoom;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="roomRateDetails"></param>
        public VoucherRoomRateDisplay(VoucherRoomRateDetails roomRateDetails)
        {
            if (roomRateDetails != null)
            {
                this.roomRateDetails = roomRateDetails;
                SetRateCategoriesDisplay();
                this.minRateForEachRoom = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrate");
                this.maxRateForEachRoom = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrate");
                this.minRatePerStayForEachRoom =
                    WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrate");
                this.maxRatePerStayForEachRoom =
                    WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrate");
            }
        }

        /// <summary>
        /// Sets rate categories to display
        /// </summary>
        public void SetRateCategoriesDisplay()
        {
            if (roomRateDetails != null && roomRateDetails.RateCategories != null &&
                roomRateDetails.RateCategories.Count > 0)
            {
                foreach (RateCategory rateCategory in roomRateDetails.RateCategories)
                {
                    if (rateCategory != null)
                        rateCategoryDisplayList.Add(new RateCategoryHeaderDisplay(rateCategory.RateCategoryId,
                                                                                  rateCategory.RateCategoryName,
                                                                                  rateCategory.RateCategoryDescription,
                                                                                  rateCategory.RateCategoryURL,
                                                                                  rateCategory.CoulmnNumber,
                                                                                  rateCategory.DisplayMoreInfoLink,
                                                                                  rateCategory.RateCategoryColor,
                                                                                  string.Empty,
                                                                                  string.Empty,
                                                                                  string.Empty,
                                                                                  rateCategory.HeaderList));
                }
            }
        }

        /// <summary>
        /// Gets RateCategoriesDisplay
        /// </summary>
        public override List<RateCategoryHeaderDisplay> RateCategoriesDisplay
        {
            get { return rateCategoryDisplayList; }
        }

        /// <summary>
        /// Gets the max rate for each room.
        /// </summary>
        /// <value>The max rate for each room.</value>
        public override string MaxRateForEachRoom
        {
            get { return maxRateForEachRoom; }
        }

        /// <summary>
        /// Gets MinRateForEachRoom
        /// </summary>
        public override string MinRateForEachRoom
        {
            get { return minRateForEachRoom; }
        }

        /// <summary>
        /// Gets MinRatePerStayForEachRoom
        /// </summary>
        public override string MinRatePerStayForEachRoom
        {
            get { return minRatePerStayForEachRoom; }
        }

        /// <summary>
        /// Gets 
        /// </summary>
        public override string MaxRatePerStayForEachRoom
        {
            get { return maxRatePerStayForEachRoom; }
        }
              
        /// <summary>
        /// GetRateCellDisplay
        /// </summary>
        /// <param name="roomCategory"></param>
        /// <param name="rateCategorySlNo"></param>
        /// <param name="rateCategoryID"></param>
        /// <param name="showPricePerStay"></param>
        /// <param name="roomNumber"></param>
        /// <returns></returns>
        public override RateCellDisplay GetRateCellDisplay(RoomCategoryEntity roomCategory, int rateCategorySlNo,
                                                           string rateCategoryID, bool showPricePerStay, int roomNumber)
        {
            RateCellDisplay rateCell = new RateCellDisplay();
            RateEntity rate;
            if (showPricePerStay)
            {
                rate = roomCategory.GetCommonTotalRate(rateCategoryID);
            }
            else
            {
                rate = roomCategory.GetCommonBaseRate(rateCategoryID);
            }

            rateCell.RateString = string.Empty;
            if (null != rate)
            {
                rateCell.RateString = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrate");
                rateCell.BookUrl = "javascript:RedirectToBookDetail(\'" + rateCategorySlNo + "\', \'" + rateCategoryID +
                                   "\', \'" + roomNumber + "\')";
            }

            return rateCell;
        }


        /// <summary>
        /// Gets the rate cell display.
        /// </summary>
        /// <param name="roomCategory">The room category.</param>
        /// <param name="rateCategorySlNo">The rate category sl no.</param>
        /// <param name="rateCategoryID">The rate category ID.</param>
        /// <param name="showPricePerStay">if set to <c>true</c> [show price per stay].</param>
        /// <param name="roomNumber">The room number.</param>
        /// <returns></returns>
        public override RateCellDisplay GetRateDisplayStringForShoppingCart(RoomCategoryEntity roomCategory,
                                                                            string rateCategoryID, bool showPricePerStay,
                                                                            int roomNumber)
        {
            string voucher = string.Empty;
            RateCellDisplay rateCell = new RateCellDisplay();
            RateEntity rate;
            if (showPricePerStay)
            {
                rate = roomCategory.GetCommonTotalRate(rateCategoryID);
            }
            else
            {
                rate = roomCategory.GetCommonBaseRate(rateCategoryID);
            }

            rateCell.RateString = string.Empty;
            if (null != rate)
            {
                int noOfNights = default(int);
                if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                {
                    noOfNights = SearchCriteriaSessionWrapper.SearchCriteria.NoOfNights;
                }
                else if (BookingEngineSessionWrapper.BookingDetails != null && BookingEngineSessionWrapper.BookingDetails.HotelSearch != null)
                {
                    noOfNights = BookingEngineSessionWrapper.BookingDetails.HotelSearch.NoOfNights;
                }

                if(noOfNights > 1)
                    voucher = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrates");
                else
                    voucher = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrate");

                rateCell.RateString = string.Format("{0} {1}", noOfNights, voucher);
            }

            return rateCell;
        }

        /// <summary>
        /// Gets the total rate display string for shopping cart.
        /// </summary>
        /// <param name="showPricePerStay">if set to <c>true</c> [show price per stay].</param>
        /// <param name="total">The total.</param>
        /// <param name="CurrencyCode">The currency code.</param>
        /// <returns></returns>
        public override string GetTotalRateDisplayStringForShoppingCart(bool showPricePerStay, double total,
                                                                        string CurrencyCode)
        {
            return Utility.GetNumberofVouchers();
        }
    }
}