using System.Text;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.CMS.DataAccessLayer;
using System;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// BaseHotelDisplay
    /// </summary>
    public class BaseHotelDisplay : IHotelDisplayInformation
    {
        protected string id;
        protected string imageSrc;
        protected string imageAltText;
        protected string hotelURL;
        protected string hotelName;
        protected string rateTitle;
        protected string price;
        protected string alternatePrice;
        protected string altPriceTooltipLine1;
        protected string altPriceTooltipLine2;
        protected string perNight;
        protected string perStay;
        protected string cityCenterDistance;
        private string selectHotel;
        protected string address;
        protected string teaser;
        private string distance;
        private string direction;
        private string drivingTime;
        private string fullDescriptionText;
        private string imageGalleryText;
        private string fromText;
        private string specialAlert;
        private int taLocationID;
        private string tripAdvisorWidget;
        private bool hideHotelTARatings;
        private const string DISPLAY_STYLE_BLOCK = "block";
        private const string DISPLAY_STYLE_NONE = "none";
        protected string noDiscount;
        private string hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria.SearchingType.ToString();
        //private HotelRateDiscount hotelPriceDiscount;
        #region Properties

        public string FromText
        {
            get
            {
                return fromText;
            }
            set
            {
                fromText = value;
            }
        }
        public string RateString { get; set; }

        public string ID
        {
            get { return id; }
        }

        private static string NO_IMAGE_PATH = "/Templates/Booking/Images/noimage.gif";

        public string ImageSrc
        {
            get
            {
                if (string.IsNullOrEmpty(imageSrc))
                    return NO_IMAGE_PATH;
                else
                    return imageSrc;
            }
        }

        public string ImageAltText
        {
            get { return imageAltText; }
        }

        public string HotelURL
        {
            get { return hotelURL; }
        }

        public string HotelName
        {
            get { return hotelName; }
        }

        public string RateTitle
        {
            get { return rateTitle; }
        }

        public string NoDiscount
        {
            get { return noDiscount; }
        }

        public string Price
        {
            get { return price; }
        }

        public string AlternatePrice
        {
            get { return alternatePrice; }
        }
        public string AltPriceTooltipLine1
        {
            get { return altPriceTooltipLine1; }
        }
        public string AltPriceTooltipLine2
        {
            get { return altPriceTooltipLine2; }
        }

        public string PerNight
        {
            get { return perNight; }
        }

        public string PerStay
        {
            get { return perStay; }
        }

        public string CityCenterDistance
        {
            get { return cityCenterDistance; }
            set { cityCenterDistance = value; }
        }

        public string SelectHotel
        {
            get { return selectHotel; }
        }

        public string Address
        {
            get { return address; }
        }

        public string Teaser
        {
            get { return teaser; }
        }

        public string Distance
        {
            get { return distance; }
        }

        public string Direction
        {
            get { return direction; }
        }

        public string DrivingTime
        {
            get { return drivingTime; }
        }

        public int TALocationID
        {
            get { return taLocationID; }
            set { taLocationID = value; }
        }

        public string TripAdvisorWidget
        {
            get { return tripAdvisorWidget; }
            set { tripAdvisorWidget = value; }
        }

        public bool HideHotelTARatings
        {
            get { return hideHotelTARatings; }
            set { hideHotelTARatings = value; }
        }

        //public HotelRateDiscount HotelPriceDiscount
        //{
        //    get { return hotelPriceDiscount; }
        //    set { hotelPriceDiscount = value; }
        //}

        //Merchanidising - Special Alert in booking flow
        public string SpecialAlert { get { return specialAlert; } }
        public bool HideARBPrice { get; set; }

        #region TransformDestinationToXML

        public string XML
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<hotel>");
                if (null != imageSrc)
                    sb.Append("<image><![CDATA[").Append(imageSrc).Append("]]></image>");
                sb.Append("<code>").Append(id).Append("</code>");
                sb.Append("<name><![CDATA[").Append(hotelName).Append("]]></name>");
                if (null != hotelURL)
                    sb.Append("<url><![CDATA[").Append(hotelURL).Append("]]></url>");
                if (null != teaser)
                    sb.Append("<description><![CDATA[").Append(teaser).Append("]]></description>");
                sb.Append("<address><![CDATA[").Append(address).Append("]]></address>");

                if (null != rateTitle)
                    sb.Append("<rateTitle><![CDATA[").Append(rateTitle).Append("]]></rateTitle>");
                if (null != noDiscount)
                    sb.Append("<noDiscount><![CDATA[").Append(noDiscount).Append("]]></noDiscount>");
                if (null != price)
                    sb.Append("<rate>").Append(price).Append("</rate>");
                if (null != alternatePrice)
                    sb.Append("<alternatePrice>").Append(alternatePrice).Append("</alternatePrice>");
                if (null != altPriceTooltipLine1)
                    sb.Append("<altPriceTooltipLine1><![CDATA[").Append(altPriceTooltipLine1).Append("]]></altPriceTooltipLine1>");
                if (null != altPriceTooltipLine2)
                    sb.Append("<altPriceTooltipLine2><![CDATA[").Append(altPriceTooltipLine2).Append("]]></altPriceTooltipLine2>");
                if (perNight != null)
                {
                    sb.Append("<prpn>").Append(perNight).Append("</prpn>");
                }
                sb.Append("<displayed><![CDATA[").Append("0").Append("]]></displayed>");
                sb.Append("<sH><![CDATA[").Append(selectHotel).Append("]]></sH>");
                if (null != distance)
                    sb.Append("<distance>").Append(distance).Append("</distance>");
                if (null != direction)
                    sb.Append("<direction>").Append(direction).Append("</direction>");
                if (null != drivingTime)
                    sb.Append("<drivingTime>").Append(drivingTime).Append("</drivingTime>");
                sb.Append("<distanceText><![CDATA[").Append(
                    EPiServer.Core.LanguageManager.Instance.Translate("/bookingengine/booking/selecthotel/distance")).
                    Append("]]></distanceText>");
                sb.Append("<directionText><![CDATA[").Append(
                    EPiServer.Core.LanguageManager.Instance.Translate("/bookingengine/booking/selecthotel/direction")).
                    Append("]]></directionText>");
                sb.Append("<drivingTimeText><![CDATA[").Append(
                    EPiServer.Core.LanguageManager.Instance.Translate("/bookingengine/booking/selecthotel/drivingtime"))
                    .Append("]]></drivingTimeText>");
                sb.Append("<fulldescription><![CDATA[").Append(fullDescriptionText).Append("]]></fulldescription>");
                sb.Append("<imagegallery>").Append(imageGalleryText).Append("</imagegallery>");
                sb.Append("<fromText><![CDATA[").Append(fromText).Append("]]></fromText>");
                sb.Append("<HotelSearch><![CDATA[").Append(hotelSearch).Append("]]></HotelSearch>");
                sb.Append("<IsUnblockRequired>").Append(IsUnblockRequired).Append("</IsUnblockRequired>");
                sb.Append("<specialalert><![CDATA[").Append(specialAlert).Append("]]></specialalert>");
                sb.Append("<hidearbprice>").Append(HideARBPrice).Append("</hidearbprice>");
                sb.Append("<citycenterdistance>").Append(CityCenterDistance).Append("</citycenterdistance>");
                if (!string.IsNullOrEmpty(TripAdvisorWidget))
                    sb.Append("<tripAdvisorWidget><![CDATA[").Append(TripAdvisorWidget).Append("]]></tripAdvisorWidget>");
                sb.Append("</hotel>");

                return sb.ToString();
            }
        }

        #endregion TransformDestinationToXML

        public string HotelSearch
        {
            get { return hotelSearch; }
            set { hotelSearch = value; }
        }

        #endregion

        /// <summary>
        /// SetHotelDestinationDetails
        /// </summary>
        /// <param name="hotel">HotelDestination</param>
        protected void SetHotelDestinationDetails(HotelDestination hotel)
        {
            if (hotel != null)
            {
                this.id = hotel.OperaDestinationId;
                this.imageSrc = hotel.ImageURL;
                this.imageAltText = hotel.Name;
                this.hotelURL = hotel.HotelPageURL;
                this.hotelName = hotel.Name;
                this.address = hotel.HotelAddress.ToString();
                this.teaser = hotel.HotelDescription;
                this.direction = hotel.AltCityDirection;
                this.distance = hotel.AltCityDistance;
                this.drivingTime = hotel.AltCityDrivingTime;
                this.selectHotel = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/roomsandrates");
                this.fullDescriptionText = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/FullDescription");
                this.imageGalleryText = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/imagegallery");
                this.HideARBPrice = hotel.HideARBPrice;
                if (!hotel.HideARBPrice)
                {
                    this.FromText = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from");
                }
                this.specialAlert = hotel.SpecialAlert;
                this.TALocationID = hotel.TALocationID;
                this.HideHotelTARatings = hotel.HideHotelTARating == true ? hotel.HideHotelTARating : false;
                if (!WebUtil.HideTARating(false, this.HideHotelTARatings))
                    this.TripAdvisorWidget = WebUtil.CreateTripAdvisorRatingWidget(Convert.ToInt32(hotel.TALocationID));
                else
                    this.TripAdvisorWidget = "";
            }
        }

        public bool IsUnblockRequired { get; set; }

        #region IHotelDisplayInformation Members

        #endregion
    }
}