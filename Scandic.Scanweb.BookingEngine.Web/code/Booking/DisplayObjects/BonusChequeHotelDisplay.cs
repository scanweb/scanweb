using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.Core;
using System;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;
using System.Text.RegularExpressions;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// BonusChequeHotelDisplay
    /// </summary>
    public class BonusChequeHotelDisplay : BaseHotelDisplay
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="isPerStay"></param>
        public BonusChequeHotelDisplay(BonusChequeHotelDetails hotelDetails, bool isPerStay)
        {
            SetHotelDestinationDetails(hotelDetails.HotelDestination);
            SetPriceString(hotelDetails, isPerStay);
        }

        /// <summary>
        /// SetPriceString
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="isPerStay"></param>
        private void SetPriceString(BonusChequeHotelDetails hotelDetails, bool isPerStay)
        {
            string parseRateValue = string.Empty;
            string parseRateCurrency = string.Empty;
            string convertedRateString = string.Empty;
            string bonusCheckString = string.Empty;
            string stringFormat = "{0} {1}";
            string disclaimerTextLine1 = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/disclaimertextline1");
            string disclaimerTextLine2 = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/disclaimertextline2");
            string approxText = String.Format("{0}{1}", WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/about"), AppConstants.SPACE);
            ExchangeRateManager exchangeRateManager = new ExchangeRateManager();
            if (isPerStay)
            {
                if ((hotelDetails != null) && (!string.IsNullOrEmpty(hotelDetails.CountryCode)))
                {
                    if (hotelDetails.MinRatePerStay != null && hotelDetails.MinRatePerStay.CurrencyCode != null)
                    {
                        this.price =
                            WebUtil.GetBonusChequeRateString("/bookingengine/booking/selecthotel/BCrate",
                                                             hotelDetails.ArrivalDate.Year, hotelDetails.CountryCode,
                                                             hotelDetails.MinRatePerStay.Rate,
                                                             hotelDetails.MinRatePerStay.CurrencyCode,
                                                             RoomRateDisplayUtil.GetNoOfNights(), AppConstants.PER_ROOM);
                        string[] splitedPrice = Price.Split(Convert.ToChar(AppConstants.PLUS));
                        if (splitedPrice.Length > 1)
                        {
                            if(splitedPrice[1].Trim().Contains(AppConstants.SPACE))
                            {
                                parseRateValue = splitedPrice[1].Trim().Split(Convert.ToChar(AppConstants.SPACE))[0];
                                parseRateCurrency = splitedPrice[1].Trim().Split(Convert.ToChar(AppConstants.SPACE))[1];
                            }
                            else
                            {
                                parseRateValue = Regex.Replace(splitedPrice[1].Trim(), @"[^\d]", "");
                                parseRateCurrency = Regex.Replace(splitedPrice[1].Trim(), @"[^a-zA-Z]", "");
                            }
                            
                            convertedRateString = exchangeRateManager.GetExchangeRateString(stringFormat,
                                                                                            parseRateCurrency,
                                                                                            parseRateValue);
                            if (convertedRateString != string.Empty)
                            {
                                this.altPriceTooltipLine1 = disclaimerTextLine1;
                                this.altPriceTooltipLine2 = disclaimerTextLine2;
                                bonusCheckString = splitedPrice[0];
                                this.alternatePrice = String.Format("{0}{1}{2}{3}{4}", approxText, bonusCheckString,
                                                                    AppConstants.PLUS, AppConstants.SPACE,
                                                                    convertedRateString);
                            }
                        }
                    }
                }
                RateString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/roomperstay");
            }
            else
            {
                if ((hotelDetails != null) && (!string.IsNullOrEmpty(hotelDetails.CountryCode)))
                {
                    if (hotelDetails.MinRate != null)
                    {
                        this.price = WebUtil.GetBonusChequeRateString("/bookingengine/booking/selecthotel/BCrate",
                                                         hotelDetails.ArrivalDate.Year, hotelDetails.CountryCode,
                                                         hotelDetails.MinRate.Rate, hotelDetails.MinRate.CurrencyCode,
                                                         AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                        string[] splitedPrice = Price.Split(Convert.ToChar(AppConstants.PLUS));
                        if (splitedPrice.Length > 1)
                        {
                            if (splitedPrice[1].Trim().Contains(AppConstants.SPACE))
                            {
                                parseRateValue = splitedPrice[1].Trim().Split(Convert.ToChar(AppConstants.SPACE))[0];
                                parseRateCurrency = splitedPrice[1].Trim().Split(Convert.ToChar(AppConstants.SPACE))[1];
                            }
                            else
                            {
                                parseRateValue = Regex.Replace(splitedPrice[1].Trim(), @"[^\d]", "");
                                parseRateCurrency = Regex.Replace(splitedPrice[1].Trim(), @"[^a-zA-Z]", "");
                            }
                            
                            convertedRateString = exchangeRateManager.GetExchangeRateString(stringFormat,
                                                                                            parseRateCurrency,
                                                                                            parseRateValue);
                            if (convertedRateString != string.Empty)
                            {
                                this.altPriceTooltipLine1 = disclaimerTextLine1;
                                this.altPriceTooltipLine2 = disclaimerTextLine2;
                                bonusCheckString = splitedPrice[0];
                                this.alternatePrice = String.Format("{0}{1}{2}{3}{4}", approxText, bonusCheckString,
                                                                    AppConstants.PLUS, AppConstants.SPACE,
                                                                    convertedRateString);
                            }
                        }
                    }
                }

                this.perNight = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/perroompernight");

                RateString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/perroompernight");
            }
        }
    }
}