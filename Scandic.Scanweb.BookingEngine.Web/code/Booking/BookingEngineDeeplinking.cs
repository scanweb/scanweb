//  Description					: BookingEngineDeeplinking                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System.Web;
using EPiServer.Globalization;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// Contains members to handle booking engine deeplink.
    /// </summary>
    public class BookingEngineDeeplinking
    {
        /// <summary>
        /// Returns the URL where the user need to be redirected when called using
        /// Deeplinking method.
        /// 
        /// Will read the parameters passed as part of the <code>deepLinkingEntity</code>
        /// and based on the rules defined for Booking Deeplinking, the details will be saved to session
        /// the appropriate relative URL to be redirected will be returned
        /// </summary>
        /// <param name="deepLinkingEntity"></param>
        /// <returns>The Relative URL the user need to be redirected</returns>
        public static string GetBookingPageUrl(DeepLinkingEntity deepLinkingEntity, bool requestFromMobile, out bool isBookingPage, out bool isHomePageRedirection)
        {
            HotelSearchEntity hotelSearch = new HotelSearchEntity();
            hotelSearch.ArrivalDate = deepLinkingEntity.ArrivalDate;
            hotelSearch.DepartureDate = deepLinkingEntity.DepartureDate;
            hotelSearch.NoOfNights = Core.DateUtil.DateDifference(hotelSearch.DepartureDate, hotelSearch.ArrivalDate);
            hotelSearch.RoomsPerNight = deepLinkingEntity.RN;

            hotelSearch.ListRooms = new List<HotelSearchRoomEntity>();
            for (int i = 0; i < deepLinkingEntity.ListDeepLinkingRooms.Count; i++)
            {
                HotelSearchRoomEntity roomSearch = new HotelSearchRoomEntity();
                roomSearch.AdultsPerRoom = deepLinkingEntity.ListDeepLinkingRooms[i].AN;
                roomSearch.ChildrenPerRoom = deepLinkingEntity.ListDeepLinkingRooms[i].CN;
                roomSearch.SelectedRateCategory = deepLinkingEntity.ListDeepLinkingRooms[i].SelectedRateCategory;
                roomSearch.SelectedRoomCategory = deepLinkingEntity.ListDeepLinkingRooms[i].SelectedRoomCategory;
                roomSearch.SelectedRate = deepLinkingEntity.ListDeepLinkingRooms[i].SelectedRate;
                if (roomSearch.ChildrenPerRoom > 0)
                {
                    roomSearch.ChildrenDetails = new ChildrensDetailsEntity();
                    roomSearch.ChildrenDetails.AdultsPerRoom = (uint)roomSearch.AdultsPerRoom;

                    roomSearch.ChildrenDetails.ListChildren = new List<ChildEntity>();
                    for (int j = 0; j < deepLinkingEntity.ListDeepLinkingRooms[i].ListDeepLinkingChildren.Count; j++)
                    {
                        ChildEntity child = new ChildEntity();
                        child.Age = deepLinkingEntity.ListDeepLinkingRooms[i].ListDeepLinkingChildren[j].Age;
                        child.ChildAccommodationType =
                            deepLinkingEntity.ListDeepLinkingRooms[i].ListDeepLinkingChildren[j].ChildAccommodationType;

                        roomSearch.ChildrenDetails.ListChildren.Add(child);
                    }

                    string cot = string.Empty, extraBed = string.Empty, sharingBed = string.Empty;
                    Utility.GetLocaleSpecificChildrenAccomodationTypes(ref cot, ref extraBed, ref sharingBed);
                    roomSearch.ChildrenDetails.SetAccommodationString(cot, extraBed, sharingBed);

                    roomSearch.ChildrenOccupancyPerRoom =
                        Utility.GetNoOfChildrenToBeAccommodated(roomSearch.ChildrenDetails);
                }
                hotelSearch.ListRooms.Add(roomSearch);
            }

            hotelSearch.SearchingType = deepLinkingEntity.TypeOfSearch;

            string campaignCode = deepLinkingEntity.CampaignCode;
            if (null != campaignCode)
            {
                hotelSearch.CampaignCode = Utility.FormatCode(campaignCode);

                hotelSearch.QualifyingType = Utility.GetQualifyingType(hotelSearch.CampaignCode);
            }
            SearchedForEntity searchedFor = GetSearchedFor(deepLinkingEntity.HO, deepLinkingEntity.CO);
            if (null != searchedFor)
            {
                hotelSearch.SearchedFor = searchedFor;

                if (searchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                    hotelSearch.SelectedHotelCode = searchedFor.SearchCode;
            }
            else
            {
                HttpContext.Current.Session["deepLinkDates"] = "true";
            }

            SearchCriteriaSessionWrapper.SearchCriteria = hotelSearch;
            if (Utility.IsBlockCodeBooking)
                hotelSearch.IsBlockCodeRate = true;
            Node node = GetSearchedNode(deepLinkingEntity.HO, deepLinkingEntity.CO, deepLinkingEntity.CUO);
            FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView = node;
            FindAHotelSessionVariablesSessionWrapper.IsDeepLinking = true;
            FindAHotelSessionVariablesSessionWrapper.EventSource = SiteCatalystLocation.DEEPLINKING;
            return GetPageToRedirect(deepLinkingEntity, hotelSearch, requestFromMobile, out isBookingPage, out isHomePageRedirection);
        }

        /// <summary>
        /// This Function searches the Node structure and returns the node which matches to HotelOperaID or cityOperaID or countryCode
        /// </summary>
        /// <param name="hotelOperaID">hotelOperaID</param>
        /// <param name="cityOperaID">cityOperaID</param>
        /// <param name="countryCode">countryCode</param>
        /// <returns></returns>
        private static Node GetSearchedNode(string hotelOperaID, string cityOperaID, string countryCode)
        {
            Node resultant = null;
            AvailabilityController availabilityControllerObj = new AvailabilityController();
            if ((!string.IsNullOrEmpty(hotelOperaID)) || (!string.IsNullOrEmpty(cityOperaID)) ||
                (!string.IsNullOrEmpty(countryCode)))
            {
                if (!string.IsNullOrEmpty(hotelOperaID))
                {
                    HotelDestination hotel = availabilityControllerObj.GetHotelDestination(hotelOperaID);
                    if (null != hotel)
                    {
                        resultant = GetNodefromTreeView(hotel.Name);
                    }
                }
                else if (!string.IsNullOrEmpty(cityOperaID))
                {
                    CityDestination city = availabilityControllerObj.GetCityDestination(cityOperaID);
                    if (city != null)
                    {
                        resultant = GetNodefromTreeView(city.Name);
                    }
                }
                else if (!string.IsNullOrEmpty(countryCode))
                {
                    string countryName = availabilityControllerObj.GetcountryNameByCountryCode(countryCode);
                    if (!string.IsNullOrEmpty(countryName))
                    {
                        resultant = GetNodefromTreeView(countryName);
                    }
                }
            }


            return resultant;
        }

        /// <summary>
        /// Get the Page to Redirect
        /// </summary>
        /// <param name="deepLinkingEntity"></param>
        /// <returns></returns>
        private static string GetPageToRedirect(DeepLinkingEntity deepLinkingEntity, HotelSearchEntity hotelSearch, bool requestFromMobile, out bool isBookingPage, out bool isHomePageRedirection)
        {
            isHomePageRedirection = false;
            isBookingPage = false;
            string pageToRedirect = GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE);
            bool isMobileRedirectionPossible = false;

            if (requestFromMobile && deepLinkingEntity.RN == 1 && string.IsNullOrEmpty(deepLinkingEntity.RB) && string.IsNullOrEmpty(deepLinkingEntity.VC))
            {
                isMobileRedirectionPossible = true;
            }
            if (deepLinkingEntity.DeepLinkingPageId != 0)
            {
                PageData pageData = null;
                try
                {
                    if (deepLinkingEntity.DeepLinkingPageId > 0)
                    {
                        pageData = DataFactory.Instance.GetPage(new PageReference(deepLinkingEntity.DeepLinkingPageId));
                    }
                }
                catch (PageNotFoundException pne)
                {
                }
                if (pageData != null)
                {
                    PageReference pageReference = pageData["PageToBeRedirected"] as PageReference;
                    if (pageReference != null)
                    {
                        PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                         EPiServer.Security.AccessLevel.NoAccess);

                        int selectHotelpageLink = GlobalUtil.GetPageID(EpiServerPageConstants.SELECT_HOTEL_PAGE);
                        int selectRatepageLink = GlobalUtil.GetPageID(EpiServerPageConstants.SELECT_RATE_PAGE);
                        int childrenDetailsPageID = GlobalUtil.GetPageID(EpiServerPageConstants.CHILDRENS_DETAILS_PAGE);

                        if (pageReference.ID.Equals(selectHotelpageLink) || pageReference.ID.Equals(selectRatepageLink) ||
                            pageReference.ID.Equals(childrenDetailsPageID))
                        {
                            if (null != hotelSearch.SearchedFor)
                            {
                                if (hotelSearch.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City)
                                {
                                    //initaite search only if deeplink Query String will contain FS=1
                                    if (!isMobileRedirectionPossible && !string.Equals(deepLinkingEntity.FS, "1"))
                                        SelectHotelUtil.InitiateAsynchronousSearch(hotelSearch);
                                    pageToRedirect = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_HOTEL_PAGE);
                                }
                                else if (hotelSearch.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                                {
                                    pageToRedirect = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE);
                                }
                            }
                        }
                        else
                        {
                            string currentLanguage = string.Equals(LanguageSelection.GetLanguageFromHost(), LanguageConstant.LANGUAGE_RUSSIA, StringComparison.InvariantCultureIgnoreCase) ?
                                                     LanguageConstant.LANGUAGE_RUSSIA_RU : LanguageSelection.GetLanguageFromHost();

                            PageData hotelOverviewPageData = ContentDataAccess.GetPageData(AppConstants.HotelOverviewPage, currentLanguage);
                            pageToRedirect = GlobalUtil.GetUrlToPage(pageReference);
                            if (!string.IsNullOrEmpty(pageToRedirect) && hotelOverviewPageData != null && pageToRedirect.Contains(hotelOverviewPageData.URLSegment) &&
                                !string.IsNullOrEmpty(deepLinkingEntity.CMPID) && !string.IsNullOrEmpty(deepLinkingEntity.HO))
                            {
                                if (hotelSearch != null && hotelSearch.SearchedFor != null &&
                                    !string.IsNullOrEmpty(hotelSearch.SearchedFor.SearchCode))
                                {
                                    HotelDestination hoteldestination =
                                        ContentDataAccess.GetHotelByOperaID(hotelSearch.SearchedFor.SearchCode.ToString());
                                    pageToRedirect = hoteldestination.HotelPageURL;
                                }
                            }
                        }

                    }
                }
            }
            else if (null != hotelSearch.SearchedFor)
            {
                {
                    bool selectHomePage = false;
                    foreach (DeepLinkingRoomEntity deepLinkingRoomEntity in deepLinkingEntity.ListDeepLinkingRooms)
                    {
                        if (deepLinkingRoomEntity != null && deepLinkingRoomEntity.ListDeepLinkingChildren != null)
                        {
                            foreach (
                                DeepLinkingChildEntity deepLinkingChildEntity in
                                    deepLinkingRoomEntity.ListDeepLinkingChildren)
                            {
                                if (deepLinkingChildEntity != null && deepLinkingChildEntity.Age == 0 &&
                                    deepLinkingChildEntity.ChildAccommodationType ==
                                    ChildAccommodationType.SelectBedType)
                                {
                                    selectHomePage = true;
                                }
                            }
                        }
                    }
                    if (selectHomePage == false)
                    {
                        if (hotelSearch.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City)
                        {
                            if (!isMobileRedirectionPossible)
                                SelectHotelUtil.InitiateAsynchronousSearch(hotelSearch);
                            pageToRedirect = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_HOTEL_PAGE);
                        }
                        else if (hotelSearch.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                        {
                            pageToRedirect = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE);
                        }
                    }
                }
            }
            if (string.Equals(pageToRedirect, GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_HOTEL_PAGE), StringComparison.InvariantCultureIgnoreCase) ||
                string.Equals(pageToRedirect, GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE), StringComparison.InvariantCultureIgnoreCase) ||
                string.Equals(pageToRedirect, GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE), StringComparison.InvariantCultureIgnoreCase))
            {
                if (isMobileRedirectionPossible)
                    isBookingPage = true;
            }

            if (string.Equals(pageToRedirect, GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE), StringComparison.InvariantCultureIgnoreCase))
                isHomePageRedirection = true;

            return pageToRedirect;
        }

        /// <summary>
        /// Get the SearchedFor Entity
        /// </summary>
        /// <param name="hotelOperaID"></param>
        /// <param name="cityOperaID"></param>
        /// <returns></returns>
        private static SearchedForEntity GetSearchedFor(string hotelOperaID, string cityOperaID)
        {
            SearchedForEntity searchedFor = null;
            if (null == hotelOperaID && null == cityOperaID)
            {
                return null;
            }

            if (null != hotelOperaID)
            {
                AvailabilityController availabilityControllerObj = new AvailabilityController();
                HotelDestination hotel = availabilityControllerObj.GetHotelDestination(hotelOperaID);
                if (null != hotel)
                {
                    searchedFor = new SearchedForEntity(hotel.Name);
                    searchedFor.SearchCode = hotel.OperaDestinationId;
                    searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.Hotel;
                }
            }

            if ((null != hotelOperaID && null == searchedFor && null != cityOperaID) ||
                (null == hotelOperaID && null != cityOperaID))
            {
                AvailabilityController availabilityControllerObj = new AvailabilityController();
                CityDestination city = availabilityControllerObj.GetCityDestination(cityOperaID);
                if (null != city)
                {
                    searchedFor = new SearchedForEntity(city.Name);
                    searchedFor.SearchCode = city.OperaDestinationId;
                    searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.City;
                }
            }

            return searchedFor;
        }

        /// <summary>
        /// This function gets the mainNode list from CDAL and searches the Node structure,
        /// matches the nodes with searchName and returns that node back
        /// </summary>
        /// <param name="searchName">searchName</param>
        /// <returns></returns>
        private static Node GetNodefromTreeView(string searchName)
        {
            Node resultNode = null;
            if (!string.IsNullOrEmpty(searchName))
            {
                Node rootPage = ContentDataAccess.GetPageDataNodeTree(false);
                if (rootPage != null)
                {
                    List<Node> mainNodeList = rootPage.GetChildren();
                    if (mainNodeList != null && mainNodeList.Count > 0)
                    {
                        FindPageNode(mainNodeList, ref resultNode, searchName);
                    }
                }
            }

            return resultNode;
        }

        /// <summary>
        /// Find the Hotel/city/country node whose name matches 
        /// with the search string specified
        /// </summary>
        /// <param name="mainNodeList">List of CMS Node structure returned from CDAL</param>
        /// <param name="resultNode">node to store the selected node</param>
        /// <param name="pageLinkId">searchName of the selected node.</param>
        /// <remarks>Find a hotel release</remarks>
        private static void FindPageNode(List<Node> mainNodeList, ref Node resultNode, string searchName)
        {
            if (mainNodeList != null && mainNodeList.Count > 0)
            {
                foreach (Node node in mainNodeList)
                {
                    if (null == resultNode)
                    {
                        int hotelPageTypeID =
                            PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                        PageData pageData = node.GetPageData();
                        if (pageData.PageTypeID.Equals(hotelPageTypeID))
                        {
                            if (searchName.Equals(pageData["Heading"].ToString()))
                            {
                                resultNode = node;
                                break;
                            }
                        }
                        else if (node.GetChildren().Count > 0)
                        {
                            if (searchName.Equals(pageData.PageName.ToString()))
                            {
                                resultNode = node;
                                break;
                            }
                            else
                            {
                                List<Node> subNodeList = node.GetChildren();
                                FindPageNode(subNodeList, ref resultNode, searchName);
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }
}