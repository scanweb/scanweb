//  Description					: Utility class for BookingEngine.Web          			  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Shankar Dasgupta                                   	  //
/// Author email id				:                           							  //
/// Creation Date				: 16th November  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			: 29th May 2008											  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.Globalization;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Web;

using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System.Text.RegularExpressions;

#endregion

#region Scandic Namespaces

#endregion

#region Episerver Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// Utility
    /// </summary>
    public class Utility
    {
        /// <summary>
        /// This method fetches the IP address of the client's browser from X-Forwarded-For header 
        /// in the Request object.
        /// </summary>
        /// <returns></returns>
        public static string GetUserIPAddress()
        {
            string ipAddress = string.Empty;
            if (HttpContext.Current.Request.Headers["X-Forwarded-For"] != null)
            {
                var headers = HttpContext.Current.Request.Headers["X-Forwarded-For"].Split(new char[] { ',' });
                if (headers.Length > 0)
                {
                    string strIPTemp = headers[0].Trim();

                    if (!string.IsNullOrEmpty(strIPTemp))
                    {
                        ipAddress = strIPTemp;
                    }
                }
            }
            return ipAddress;
        }

        #region Room Rates

        /// <summary>
        ///  Get the RoomRate in String 
        /// </summary>
        /// <param name="rateCategoryName">
        /// Rate Category name
        /// </param>
        /// <param name="rate">
        /// RateEntity
        /// </param>
        /// <param name="points">
        /// Points
        /// </param>
        /// <param name="searchType">
        /// SearchType
        /// </param>
        /// <param name="year">
        /// Year
        /// </param>
        /// <param name="country">
        /// Country
        /// </param>
        /// <param name="noOfNights">No of Nights</param>
        /// <param name="noOfRooms">No Of Rooms</param>
        /// <returns>RoomRate String
        /// </returns>
        public static string GetRoomRateString
            (string rateCategoryName, RateEntity rate, double points, SearchType searchType, int year,
             string country, int noOfNights, int noOfRooms)
        {
            string toReturn = string.Empty;
            switch (searchType)
            {
                case SearchType.BONUSCHEQUE:
                    {
                        if (null == country)
                        {
                            throw new ArgumentException("Bonus cheque requires 'year' and 'country' parameter");
                        }
                        toReturn =
                            WebUtil.GetBonusChequeRateString
                                ("/bookingengine/booking/selectrate/bonuschequerate", year, country, rate.Rate,
                                 rate.CurrencyCode, noOfNights, noOfRooms);
                        break;
                    }
                case SearchType.REDEMPTION:
                    {
                        toReturn = WebUtil.GetRedeemString(points);
                        break;
                    }
                case SearchType.VOUCHER:
                    {
                        toReturn = Utility.GetNumberofVouchers();
                        break;
                    }
                default:
                    {
                        if (null == rate)
                            throw new ArgumentException("Noram rate requires the 'rate' parameter");
                        toReturn = rate.Rate + AppConstants.SPACE + rate.CurrencyCode;
                        break;
                    }
            }
            if (null != rateCategoryName)
            {
                string normalRateString = string.Empty;
                if (searchType == SearchType.REDEMPTION)
                {
                    if (rateCategoryName == String.Empty)
                    {
                        normalRateString =
                            WebUtil.GetTranslatedText
                                ("/bookingengine/booking/bookingdetail/normalratestringRedemptionwithnocategory");
                    }
                    else
                    {
                        normalRateString =
                            WebUtil.GetTranslatedText
                                ("/bookingengine/booking/bookingdetail/normalratestringRedemption");
                    }
                }
                else
                {
                    if (rateCategoryName == String.Empty)
                    {
                        normalRateString =
                            WebUtil.GetTranslatedText
                                ("/bookingengine/booking/bookingdetail/normalratestringwithnocategory");
                    }
                    else
                    {
                        normalRateString =
                            WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/normalratestring");
                    }
                }


                toReturn = string.Format(normalRateString, rateCategoryName, toReturn);
            }
            return toReturn;
        }

        /// <summary>
        /// This is the overloaded method get called when Grid booking is done using ARB.
        /// </summary>
        /// <param name="rate">Rate from where rate string is created.</param>
        /// <returns>String to be displayed in Reservation information container.</returns>
        public static string GetRoomRateString(RateEntity rate)
        {
            string toReturn = string.Empty;
            if (null != rate)
            {
                string normalRateString = rate.Rate + AppConstants.SPACE + rate.CurrencyCode;
                toReturn = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/blockCodenormalratestring");
                toReturn = string.Format(toReturn, normalRateString);
            }
            return toReturn;
        }

        /// <summary>
        /// Get culture specific room rate string.
        /// </summary>
        /// <param name="rateCategoryName">Rate Category name.</param>
        /// <param name="rate">RateEntity</param>
        /// <param name="points">Points</param>
        /// <param name="searchType">The type of hotel search.</param>
        /// <param name="year">Year of booking.</param>
        /// <param name="country">Country</param>
        /// <param name="noOfNights">No Of Nights</param>
        /// <param name="noOfRooms">No Of Rooms</param>
        /// <param name="language">Culture specific language string.</param>
        /// <returns>Room rate String</returns>
        public static string GetRoomRateString
            (string rateCategoryName, RateEntity rate, double points, SearchType searchType,
             int year, string country, int noOfNights, int noOfRooms, string language)
        {
            string toReturn = string.Empty;
            switch (searchType)
            {
                case SearchType.BONUSCHEQUE:
                    {
                        if (null == country)
                        {
                            throw new ArgumentException("Bonus cheque requires 'year' and 'country' parameter");
                        }
                        toReturn = WebUtil.GetBonusChequeRateString("/bookingengine/booking/selectrate/bonuschequerate",
                                                                    year, country, rate.Rate, rate.CurrencyCode,
                                                                    noOfNights, noOfRooms, language);
                        if
                            (toReturn.ToLower().Contains
                                (WebUtil.GetTranslatedText
                                     (TranslatedTextConstansts.SELECT_RATE_PER_NIGHT, language).ToLower()))
                        {
                            toReturn = toReturn.Substring(0,
                                                          toReturn.Length -
                                                          WebUtil.GetTranslatedText
                                                              (TranslatedTextConstansts.SELECT_RATE_PER_NIGHT, language)
                                                              .Length);
                        }
                        break;
                    }
                case SearchType.REDEMPTION:
                    {
                        toReturn =
                            WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/redeempoints", language);
                        toReturn = string.Format(toReturn, points);
                        break;
                    }
                case SearchType.VOUCHER:
                    {
                        toReturn =
                            WebUtil.GetTranslatedText
                                ("/bookingengine/booking/selecthotel/voucherratedescription", language);
                        toReturn = toReturn.Substring(0,
                                                      toReturn.Length -
                                                      WebUtil.GetTranslatedText(
                                                          TranslatedTextConstansts.SELECT_RATE_PER_NIGHT, language).
                                                          Length);
                        break;
                    }
                default:
                    {
                        if (null == rate)
                            throw new ArgumentException("Normal rate requires the 'rate' parameter");
                        toReturn = rate.Rate + AppConstants.SPACE + rate.CurrencyCode;
                        break;
                    }
            }
            if (null != rateCategoryName)
            {
                string normalRateString = string.Empty;
                if (searchType == SearchType.REDEMPTION)
                {
                    normalRateString =
                        WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/normalratestringRedemption",
                                                  language);
                }
                else
                {
                    normalRateString =
                        WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/normalratestring", language);
                }
                toReturn = string.Format(normalRateString, rateCategoryName, toReturn);
            }
            return toReturn;
        }

        /// <summary>
        /// Returns the room and rate category string to be displayed on the enter details 
        /// and booking confirmation page
        /// </summary>
        /// <param name="roomRateDetails">
        /// RoomRate Details
        /// </param>
        /// <param name="searchType">
        /// SearchType
        /// </param>
        /// <param name="selectedRateCategoryId">
        /// Selected RateCategory Id
        /// </param>
        /// <returns>
        /// RateCategory Name
        /// </returns>
        public static string GetRateCategoryName
            (BaseRoomRateDetails roomRateDetails, SearchType searchType, string selectedRateCategoryId)
        {
            string rateCategoryName = string.Empty;
            if (null != roomRateDetails.RateCategories)
            {
                foreach (RateCategory rateCategory in roomRateDetails.RateCategories)
                {
                    if (rateCategory != null)
                    {
                        string rateCategoryId = rateCategory.RateCategoryId;
                        if (selectedRateCategoryId == rateCategoryId)
                        {
                            rateCategoryName = rateCategory.RateCategoryName;
                            if (searchType == SearchType.CORPORATE)
                            {
                                NegotiatedRoomRateDetails roomRateDetailsCasted =
                                    roomRateDetails as NegotiatedRoomRateDetails;
                                if
                                    ((null != roomRateDetailsCasted) &&
                                     (roomRateDetailsCasted.HasNegotiatedRates &&
                                      rateCategoryId != AppConstants.RATE_CATEGORY_EARLY &&
                                      rateCategoryId != AppConstants.RATE_CATEGORY_FLEX))
                                {
                                    rateCategoryName = roomRateDetailsCasted.CompanyName;
                                }
                            }
                        }
                    }
                }
            }
            return rateCategoryName;
        }


        /// <summary>
        /// Change sort order of bed type preferences.        
        /// Room type Priority sort order is assigned according to the sequence returned by 
        /// General Availability response. This will overrite the sort order of room types 
        /// fetched from CMS with the sort order from Opera.
        /// </summary>
        /// <param name="roomTypes">List Of Room Types</param>        
        public static void SetRoomTypeSortOrderAsInOpera(List<RoomType> roomTypes)
        {
            List<RoomTypeEntity> allRoomTypes = new List<RoomTypeEntity>();
            Dictionary<string, int> priorityMap = new Dictionary<string, int>();
            if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null)
            {
                IList<BaseRoomRateDetails> listHotelRoomRate = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                BaseRoomRateDetails roomRateDetails =
                    (listHotelRoomRate != null && listHotelRoomRate.Count > 0) ? listHotelRoomRate[0] : null;

                if (roomRateDetails != null)
                {
                    allRoomTypes = roomRateDetails.RoomTypes;
                    for (int ctrRoomType = 0; ctrRoomType < allRoomTypes.Count; ctrRoomType++)
                    {
                        priorityMap[allRoomTypes[ctrRoomType].Code] = allRoomTypes[ctrRoomType].SortOrder;
                    }

                    if (priorityMap != null)
                    {
                        foreach (RoomType tempRoomType in roomTypes)
                        {
                            if (tempRoomType != null)
                            {
                                if (priorityMap.ContainsKey(tempRoomType.OperaRoomTypeId))
                                {
                                    tempRoomType.Priority = priorityMap[tempRoomType.OperaRoomTypeId];
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Business Entities Conversion

        /// <summary>
        /// Convert UserProfile Entity to GuestInformation Entity
        /// </summary>
        /// <param name="userProfileEntity">
        /// UserProfile Entity
        /// </param>
        /// <returns>
        /// GuestInformation Entity
        /// </returns>
        public static GuestInformationEntity ConvertToGuestInformation(UserProfileEntity userProfileEntity)
        {
            GuestInformationEntity guestInfoEntity = null;
            if (userProfileEntity != null)
            {
                guestInfoEntity = new GuestInformationEntity();
                guestInfoEntity.Title = userProfileEntity.NameTitle;
                guestInfoEntity.FirstName = userProfileEntity.FirstName;
                guestInfoEntity.LastName = userProfileEntity.LastName;
                guestInfoEntity.Gender = GetStringFromUserGender(userProfileEntity.Gender);

                guestInfoEntity.AddressLine1 = userProfileEntity.AddressLine1;
                guestInfoEntity.AddressLine2 = userProfileEntity.AddressLine2;
                guestInfoEntity.AddressID = userProfileEntity.AddressOperaID.ToString();
                guestInfoEntity.City = userProfileEntity.City;
                guestInfoEntity.Country = userProfileEntity.Country;
                guestInfoEntity.PostCode = userProfileEntity.PostCode;
                guestInfoEntity.AddressType = userProfileEntity.AddressType;

                guestInfoEntity.Landline = new PhoneDetailsEntity();
                guestInfoEntity.Landline.Number = userProfileEntity.HomePhone;
                guestInfoEntity.Landline.OperaId = userProfileEntity.HomePhoneOperaID;
                guestInfoEntity.Landline.PhoneType = PhoneContants.PHONETYPE_HOME;

                if ((userProfileEntity.MobilePhone != string.Empty) && (userProfileEntity.MobilePhone != null))
                {
                    guestInfoEntity.Mobile = new PhoneDetailsEntity();
                    guestInfoEntity.Mobile.Number = userProfileEntity.MobilePhone;
                    guestInfoEntity.Mobile.OperaId = userProfileEntity.MobilePhoneOperaID;
                    guestInfoEntity.Mobile.PhoneType = PhoneContants.PHONETYPE_MOBILE;
                }
            }
            if (!string.IsNullOrEmpty(userProfileEntity.EmailID))
            {
                guestInfoEntity.EmailDetails = new EmailDetailsEntity();
                guestInfoEntity.EmailDetails.EmailID = userProfileEntity.EmailID;
                guestInfoEntity.EmailDetails.EmailOperaID = userProfileEntity.EmailOperaId;
            }
            if (!string.IsNullOrEmpty(userProfileEntity.NonBusinessEmailID))
            {
                guestInfoEntity.NonBusinessEmailDetails = new EmailDetailsEntity();
                guestInfoEntity.NonBusinessEmailDetails.EmailID = userProfileEntity.NonBusinessEmailID;
                guestInfoEntity.NonBusinessEmailDetails.EmailOperaID = userProfileEntity.NonBusinessEmailOperaId;
            }
            if (userProfileEntity.CreditCard != null)
            {
                guestInfoEntity.GuranteeInformation = new GuranteeInformationEntity();
                guestInfoEntity.GuranteeInformation.GuranteeType = GuranteeType.CREDITCARD;
                guestInfoEntity.GuranteeInformation.CreditCard = userProfileEntity.CreditCard;
                guestInfoEntity.GuranteeInformation.CreditCardOperaId = userProfileEntity.CreditCardOperaId;
            }
            UserPreferenceEntity[] userPreferenceList = userProfileEntity.UserPreference;
            if (userPreferenceList != null)
            {
                int totalUserPreference = userPreferenceList.Length;
                SpecialRequestEntity[] specialRequestList = new SpecialRequestEntity[totalUserPreference];
                for (int preferenceCount = 0; preferenceCount < totalUserPreference; preferenceCount++)
                {
                    specialRequestList[preferenceCount] = new SpecialRequestEntity();
                    specialRequestList[preferenceCount].RequestType =
                        userPreferenceList[preferenceCount].RequestType;
                    specialRequestList[preferenceCount].RequestValue =
                        userPreferenceList[preferenceCount].RequestValue;
                }
                guestInfoEntity.SpecialRequests = specialRequestList;
            }
            guestInfoEntity.PreferredLanguage = userProfileEntity.PreferredLanguage;

            return guestInfoEntity;
        }

        /// <summary>
        /// Get the UserProfile Entity from GuestInformation Entity
        /// </summary>
        /// <param name="guestInfoEntity">
        /// GuestInformation Entity
        /// </param>
        /// <returns>
        /// UserProfile Entity
        /// </returns>
        public static UserProfileEntity ConvertToUserProfileEntity(GuestInformationEntity guestInfoEntity)
        {
            UserProfileEntity userProfileEntity = null;
            if (guestInfoEntity != null)
            {
                userProfileEntity = new UserProfileEntity();
                userProfileEntity.NameTitle = guestInfoEntity.Title;
                userProfileEntity.FirstName = guestInfoEntity.FirstName;
                userProfileEntity.LastName = guestInfoEntity.LastName;
                userProfileEntity.Gender = GetUserGenderFromString(guestInfoEntity.Gender);

                userProfileEntity.AddressLine1 = guestInfoEntity.AddressLine1;
                userProfileEntity.AddressLine2 = guestInfoEntity.AddressLine2;
                if ((guestInfoEntity.AddressID != string.Empty) && (guestInfoEntity.AddressID != null))
                {
                    userProfileEntity.AddressOperaID = long.Parse(guestInfoEntity.AddressID);
                }
                userProfileEntity.City = guestInfoEntity.City;
                userProfileEntity.Country = guestInfoEntity.Country;
                userProfileEntity.PostCode = guestInfoEntity.PostCode;
                userProfileEntity.AddressType = guestInfoEntity.AddressType;

                if (guestInfoEntity.Landline != null)
                {
                    userProfileEntity.HomePhone = guestInfoEntity.Landline.Number;
                    userProfileEntity.HomePhoneOperaID = guestInfoEntity.Landline.OperaId;
                }

                if (guestInfoEntity.Mobile != null)
                {
                    userProfileEntity.MobilePhone = guestInfoEntity.Mobile.Number;
                    userProfileEntity.MobilePhoneOperaID = guestInfoEntity.Mobile.OperaId;
                }

                if (guestInfoEntity.EmailDetails != null)
                {
                    userProfileEntity.EmailID = guestInfoEntity.EmailDetails.EmailID;
                    userProfileEntity.EmailOperaId = guestInfoEntity.EmailDetails.EmailOperaID;
                }
                if (guestInfoEntity.NonBusinessEmailDetails != null)
                {
                    userProfileEntity.NonBusinessEmailID = guestInfoEntity.NonBusinessEmailDetails.EmailID;
                    userProfileEntity.NonBusinessEmailOperaId = guestInfoEntity.NonBusinessEmailDetails.EmailOperaID;
                }

                if (guestInfoEntity.GuranteeInformation != null)
                {
                    userProfileEntity.CreditCard = guestInfoEntity.GuranteeInformation.CreditCard;
                    userProfileEntity.CreditCardOperaId = guestInfoEntity.GuranteeInformation.CreditCardOperaId;
                }

                userProfileEntity.ProfileType = UserProfileType.BOOKING_DETAILS;

                NameController nameController = new NameController();
                DateOfBirthEntity DOB = nameController.GetMemberDOB(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID);

                if (DOB != null)
                {
                    userProfileEntity.DateOfBirth = DOB;
                }
                userProfileEntity.UserPreference =
                    ConvertSpecialRequestsToUserPreferences(guestInfoEntity.SpecialRequests);
                userProfileEntity.PreferredLanguage = guestInfoEntity.PreferredLanguage;
            }
            return userProfileEntity;
        }

        /// <summary>
        /// Converting Special Request Of the User Logged IN
        /// </summary>
        /// <param name="userPreferenceEntity">List Of Special Request Entity</param>
        /// <returns>List of User Preference Entity</returns>
        private static UserPreferenceEntity[] ConvertSpecialRequestsToUserPreferences
            (SpecialRequestEntity[] specialReqEntity)
        {
            List<UserPreferenceEntity> userPrefEntity = new List<UserPreferenceEntity>();
            if (specialReqEntity != null)
            {
                int totalRequest = specialReqEntity.Length;
                for (int requestCount = 0; requestCount < totalRequest; requestCount++)
                {
                    UserPreferenceEntity userPreferenceEntity = new UserPreferenceEntity();
                    userPreferenceEntity.RequestType = specialReqEntity[requestCount].RequestType;
                    userPreferenceEntity.RequestValue = specialReqEntity[requestCount].RequestValue;
                    userPrefEntity.Add(userPreferenceEntity);
                }
            }
            return userPrefEntity.ToArray();
        }

        /// <summary>
        /// Convert UserPreferenceEntity List to Special Request List
        /// </summary>
        /// <param name="userProfileEntityList">
        /// Array of UserPreferenceEntity
        /// </param>
        /// <returns>
        /// Array of SpecialRequestEntity
        /// </returns>
        public static SpecialRequestEntity[] ConvertToSpecialRequestEntityList(UserPreferenceEntity[] userPreferenceList)
        {
            SpecialRequestEntity[] specialRequestList = null;
            if (userPreferenceList != null)
            {
                int totalUserPreference = userPreferenceList.Length;
                specialRequestList = new SpecialRequestEntity[totalUserPreference];
                for (int preferenceCount = 0; preferenceCount < totalUserPreference; preferenceCount++)
                {
                    specialRequestList[preferenceCount] = new SpecialRequestEntity();
                    specialRequestList[preferenceCount].RequestType =
                        userPreferenceList[preferenceCount].RequestType;
                    specialRequestList[preferenceCount].RequestValue =
                        userPreferenceList[preferenceCount].RequestValue;
                }
            }
            return specialRequestList;
        }

        /// <summary>
        /// Convert to UserPreferenceEntity List
        /// </summary>
        /// <param name="specialRequestList">
        /// SpecialRequest List
        /// </param>
        /// <returns>
        /// UserPreference List
        /// </returns>
        public static UserPreferenceEntity[] ConvertToUserPreferenceEntityList(SpecialRequestEntity[] specialRequestList)
        {
            UserPreferenceEntity[] userPreferenceList = null;
            if (specialRequestList != null)
            {
                int totalRequeset = specialRequestList.Length;
                userPreferenceList = new UserPreferenceEntity[totalRequeset];
                for (int requestCount = 0; requestCount < totalRequeset; requestCount++)
                {
                    userPreferenceList[requestCount] = new UserPreferenceEntity();
                    userPreferenceList[requestCount].RequestType =
                        specialRequestList[requestCount].RequestType;
                    userPreferenceList[requestCount].RequestValue =
                        specialRequestList[requestCount].RequestValue;
                }
            }
            return userPreferenceList;
        }

        /// <summary>
        /// Compare the New Guest Information with the previously Fetched Guest Information
        /// </summary>
        /// <param name="guestInfoEntity">
        /// GuestInformation
        /// </param>
        public static void CompareGuestInformation(GuestInformationEntity guestInfoEntity)
        {
            GuestInformationEntity previousGuestInfoEntity = GuestBookingInformationSessionWrapper.FetchedGuestInformation;
            if ((guestInfoEntity != null) && (previousGuestInfoEntity != null))
            {
                if (guestInfoEntity.City != previousGuestInfoEntity.City ||
                    guestInfoEntity.Country != previousGuestInfoEntity.Country)
                {
                    guestInfoEntity.AddressID = previousGuestInfoEntity.AddressID;
                    guestInfoEntity.AddressLine1 = previousGuestInfoEntity.AddressLine1;
                    guestInfoEntity.AddressLine2 = previousGuestInfoEntity.AddressLine2;
                    guestInfoEntity.PostCode = previousGuestInfoEntity.PostCode;
                }

                if ((guestInfoEntity.Mobile != null) && (previousGuestInfoEntity.Mobile != null))
                {
                    if (guestInfoEntity.Mobile.Number != previousGuestInfoEntity.Mobile.Number)
                    {
                        guestInfoEntity.Mobile.OperaId = previousGuestInfoEntity.Mobile.OperaId;
                    }
                    else
                    {
                        guestInfoEntity.Mobile = null;
                    }
                }

                if ((guestInfoEntity.EmailDetails != null) && (previousGuestInfoEntity.EmailDetails != null))
                {
                    if (guestInfoEntity.EmailDetails.EmailID != previousGuestInfoEntity.EmailDetails.EmailID)
                    {
                        guestInfoEntity.EmailDetails.EmailOperaID = previousGuestInfoEntity.EmailDetails.EmailOperaID;
                        guestInfoEntity.NonBusinessEmailDetails.EmailOperaID =
                            previousGuestInfoEntity.NonBusinessEmailDetails.EmailOperaID;
                    }
                    else
                    {
                        guestInfoEntity.EmailDetails = null;
                        guestInfoEntity.NonBusinessEmailDetails = null;
                    }
                }

                if ((guestInfoEntity.GuranteeInformation != null) &&
                    (previousGuestInfoEntity.GuranteeInformation != null))
                {
                    CreditCardEntity newCreditCard = guestInfoEntity.GuranteeInformation.CreditCard;
                    CreditCardEntity previousCreditCard = previousGuestInfoEntity.GuranteeInformation.CreditCard;
                    if ((newCreditCard != null) && (previousCreditCard != null))
                    {
                        if ((newCreditCard.CardNumber != previousCreditCard.CardNumber) ||
                            (newCreditCard.CardType != previousCreditCard.CardType) ||
                            (newCreditCard.NameOnCard != previousCreditCard.NameOnCard) ||
                            (newCreditCard.ExpiryDate.Month != previousCreditCard.ExpiryDate.Month) ||
                            (newCreditCard.ExpiryDate.Year != previousCreditCard.ExpiryDate.Year))
                        {
                            guestInfoEntity.GuranteeInformation.CreditCardOperaId =
                                previousGuestInfoEntity.GuranteeInformation.CreditCardOperaId;
                        }
                        else
                        {
                            guestInfoEntity.GuranteeInformation = null;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get the Gender Name from UserGender
        /// </summary>
        /// <param name="userGender">
        /// Enum UserGender
        /// </param>
        /// <returns>
        /// Gender String
        /// </returns>
        private static string GetStringFromUserGender(UserGender userGender)
        {
            string gender = AppConstants.MALE;
            switch (userGender)
            {
                case UserGender.MALE:
                    {
                        gender = AppConstants.MALE;
                        break;
                    }
                case UserGender.FEMALE:
                    {
                        gender = AppConstants.FEMALE;
                        break;
                    }
            }
            return gender;
        }

        /// <summary>
        /// Get UserGender from String
        /// </summary>
        /// <param name="gender">
        /// String representing Gender
        /// </param>
        /// <returns>
        /// UserGender
        /// </returns>
        private static UserGender GetUserGenderFromString(string gender)
        {
            UserGender userGender = UserGender.MALE;
            switch (gender)
            {
                case AppConstants.MALE:
                    {
                        userGender = UserGender.MALE;
                        break;
                    }
                case AppConstants.FEMALE:
                    {
                        userGender = UserGender.FEMALE;
                        break;
                    }
            }
            return userGender;
        }

        /// <summary>
        /// This method will return the full country name, for a country code and language 
        /// </summary>
        /// <param name="countryCode">The code used to identify the country.</param>
        /// <returns>The full name of the country.</returns>
        public static string GetCountryName(string countryCode, string language)
        {
            OrderedDictionary countryCodeMap = DropDownService.GetCountryCodes(language);
            if (countryCodeMap != null && !string.IsNullOrEmpty(countryCode) && countryCodeMap[countryCode] != null)
            {
                return countryCodeMap[countryCode].ToString().Trim();
            }
            return countryCode;
        }

        /// <summary>
        /// This method will return the full country name, for a country code.
        /// </summary>
        /// <param name="countryCode">The code used to identify the country.</param>
        /// <returns>The full name of the country.</returns>
        public static string GetCountryName(string countryCode)
        {
            return GetCountryName(countryCode, string.Empty);
        }


        /// <summary>
        /// Get the corresponding Booking Details against the specified leg number
        /// </summary>
        /// <param name="legNumber">Leg-Number of the booking</param>
        /// <returns>Booking Details Entity</returns>
        public static BookingDetailsEntity GetBookingDetail(string legNumber)
        {
            List<BookingDetailsEntity> bookingList = BookingEngineSessionWrapper.AllBookingDetails;
            BookingDetailsEntity bookingEntity = null;
            if (bookingList != null && bookingList.Count > 0)
            {
                int totalBookings = bookingList.Count;
                for (int count = 0; count < totalBookings; count++)
                {
                    if (bookingList[count].LegNumber == legNumber)
                    {
                        bookingEntity = bookingList[count];
                        break;
                    }
                }
            }
            return bookingEntity;
        }

        /// <summary>
        /// GetLegBookingDetail
        /// </summary>
        /// <returns>BookingDetailsEntity</returns>
        public static BookingDetailsEntity GetLegBookingDetail()
        {
            List<BookingDetailsEntity> bookingList = BookingEngineSessionWrapper.AllBookingDetails;
            BookingDetailsEntity bookingEntity = null;
            if (bookingList != null && bookingList.Count > 0 && bookingList[0] != null)
            {
                bookingEntity = bookingList[0];
            }
            return bookingEntity;
        }

        /// <summary>
        /// Get the Lowest Leg Number in the Booking Details List
        /// </summary>
        /// <param name="guestsList">
        /// Guest List
        /// </param>
        /// <param name="legNumber">
        /// out parameter - Lowest Leg Number
        /// </param>
        /// <returns>
        /// BookingDetails Entity with the lowest leg number
        /// </returns>
        public static GuestInformationEntity GetLowestLegNumber
            (SortedList<string, GuestInformationEntity> guestsList, out int legNumber)
        {
            GuestInformationEntity legGuestInformation = null;
            legNumber = 1;
            if (guestsList != null && guestsList.Count > 0)
            {
                List<BookingDetailsEntity> listBookingDetails = new List<BookingDetailsEntity>();
                int totalGuest = guestsList.Count;
                foreach (string key in guestsList.Keys)
                {
                    BookingDetailsEntity bookingDetails = new BookingDetailsEntity(null, null, guestsList[key], null);
                    bookingDetails.LegNumber = guestsList[key].LegNumber;
                    listBookingDetails.Add(bookingDetails);
                }
                BookingDetailsEntity legBookingDetails = GetLowestLegNumber(listBookingDetails, out legNumber);
                if (legBookingDetails != null)
                {
                    legGuestInformation = legBookingDetails.GuestInformation;
                }
            }
            return legGuestInformation;
        }

        /// <summary>
        /// Get the Lowest Leg Number in the Booking Details List
        /// </summary>
        /// <param name="bookingList">
        /// List of Booking Details
        /// </param>
        /// <param name="legNumber">
        /// out parameter - Lowest Leg Number
        /// </param>
        /// <returns>
        /// BookingDetails Entity with the lowest leg number
        /// </returns>
        public static BookingDetailsEntity GetLowestLegNumber(List<BookingDetailsEntity> bookingList, out int legNumber)
        {
            BookingDetailsEntity legBookingDetails = null;
            legNumber = 1;
            if (bookingList != null && bookingList.Count > 0)
            {
                Int32.TryParse(bookingList[0].LegNumber, out legNumber);
                legBookingDetails = bookingList[0];
                int totalBookings = bookingList.Count;
                for (int count = 0; count < totalBookings; count++)
                {
                    int leg = 1;
                    Int32.TryParse(bookingList[count].LegNumber, out leg);
                    if (leg < legNumber)
                    {
                        legNumber = leg;
                        legBookingDetails = bookingList[count];
                    }
                }
            }
            return legBookingDetails;
        }

        #endregion

        #region SetLoginDemographics

        /// <summary>
        /// Stores the login control names into the session variable "SessionWrapper.LoginDemographics".
        /// It is a dictionary of entities of type "LoginDemographicEntity". The key is of type 
        /// "LoginSourceModule" as this remains unique. At a point of time when any page loads, 
        /// it have unique sub modules loded internally. Hence to identify each modules loaded, 
        /// we store these info in Dictionary.
        /// </summary>
        /// <param name="userNameControl"></param>
        /// <param name="passwordControl"></param>
        /// <param name="rememberMeChkBox"></param>
        /// <param name="loginSource"></param>
        /// <param name="pageIdentifier"></param>
        public static void SetLoginDemographics(string userNameControlPrefix, string userNameControl,
                                                string passwordControl, string rememberMeChkBox,
                                                LoginSourceModule loginSource, string pageIdentifier)
        {
            LoginDemographicEntity loginDemographicEntity =
                new LoginDemographicEntity(userNameControlPrefix, userNameControl, passwordControl, rememberMeChkBox, pageIdentifier);
            Dictionary<LoginSourceModule, LoginDemographicEntity> loginDemographics =
                new Dictionary<LoginSourceModule, LoginDemographicEntity>();
            loginDemographics = LoginDemographicsSessionWrapper.LoginDemographics;

            try
            {
                loginDemographics[loginSource] = loginDemographicEntity;
            }
            catch (KeyNotFoundException knfe)
            {
                loginDemographics.Add(loginSource, loginDemographicEntity);
            }
            LoginDemographicsSessionWrapper.LoginDemographics = loginDemographics;
        }

        #endregion SetLoginDemographics

        #region StoreLastVisitedPage

        /// <summary>
        /// Store the last visited page details in the session "SessionWrapper.LastVisitedPage".
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="pageUrl"></param>
        public static void StoreLastVisitedPage(int pageId, string pageUrl)
        {
            PageDetails pageDetails = new PageDetails(pageId, pageUrl);
            LastVisitedPageSessionWrapper.LastVisitedPage = pageDetails;
        }

        #endregion StoreLastVisitedPage

        #region Loyalty

        /// <summary>
        /// This method formate the phone number from loyalty details, for eg: "+ 91 999882333"
        /// </summary>
        /// <param name="telephone">Phone number from loyalty details</param>
        /// <returns>Formated phone number</returns>
        public static string FormatePhoneToDisplay(string telephone)
        {
            char[] zeroChars = new char[1];
            zeroChars[0] = '0';
            string formatedPhone = string.Empty;
            if (!string.IsNullOrEmpty(telephone))
            {
                string telPhoneNumber = telephone;
                string phoneCode = string.Empty;

                if (telPhoneNumber.Length > AppConstants.MAX_LENGTH_PHONECODE)
                {
                    if (!telPhoneNumber.StartsWith(AppConstants.PLUS))
                    {
                        phoneCode = telPhoneNumber.Substring(0, AppConstants.MAX_LENGTH_PHONECODE);
                        telPhoneNumber = telPhoneNumber.Substring(AppConstants.MAX_LENGTH_PHONECODE).TrimStart(zeroChars);
                        phoneCode = (phoneCode != string.Empty)
                                        ? phoneCode.TrimStart(zeroChars)
                                        : string.Empty;
                    }
                    else
                    {
                        phoneCode = telPhoneNumber.Substring(AppConstants.ONE_INT, AppConstants.TWO_INT);
                        telPhoneNumber = telPhoneNumber.Substring(AppConstants.THREE_INT).TrimStart('0');
                        phoneCode = (phoneCode != string.Empty) ? phoneCode.TrimStart(zeroChars) : string.Empty;
                    }
                }
                formatedPhone = string.Format("{0}{1}{2}{3}", AppConstants.PLUS, phoneCode, AppConstants.SPACE, telPhoneNumber);
            }
            return formatedPhone;
        }

        /// <summary>
        ///Append extra "0" to the mobile phone code if the length of the
        ///code is less than AppConstants.MAX_LENGTH_PHONECODE
        /// </summary>
        /// <param name="phoneCode">Phone code to be formatted</param>
        /// <returns>Formatted Phone Code</returns>
        public static string FormatePhoneCode(string phoneCode)
        {
            StringBuilder formatedPhoneCode = new StringBuilder(phoneCode);
            int maxCount = (AppConstants.MAX_LENGTH_PHONECODE - formatedPhoneCode.Length);
            if (formatedPhoneCode.Length == 2)
            {
                return string.Format("{0}{1}", AppConstants.PLUS, formatedPhoneCode);
            }
            for (int count = 0; count < maxCount; count++)
            {
                formatedPhoneCode.Insert(0, "0");
            }
            return formatedPhoneCode.ToString();
        }

        #endregion

        #region NewsLetter

        /// <summary>
        /// Send News Letter
        /// </summary>
        /// <param name="guestInfoEntity">
        /// GuestInformation Entity 
        /// </param>
        /// <param name="currentPage">
        /// Current Episerver Page
        /// </param>
        /// <param name="country">
        /// Selected Country
        /// </param>        
        //public static void SendNewsLetter
        //    (GuestInformationEntity guestInfoEntity, EPiServer.Core.PageData currentPage, string country)
        //{
        //    KOMPOSTRequestEntity kOMPOSTRequestEntity = new KOMPOSTRequestEntity();
        //    kOMPOSTRequestEntity.Form = GetAction(currentPage);
        //    kOMPOSTRequestEntity.Country = country;
        //    if (guestInfoEntity.EmailDetails != null)
        //    {
        //        kOMPOSTRequestEntity.Email = guestInfoEntity.EmailDetails.EmailID;
        //    }
        //    string postCode = guestInfoEntity.PostCode;
        //    kOMPOSTRequestEntity.Zipcode = string.IsNullOrEmpty(postCode)
        //                                       ? AppConstants.DUMMYZIPCODE
        //                                       : postCode;
        //    kOMPOSTRequestEntity.Sitelanguage = GetLanguage(currentPage);
        //    kOMPOSTRequestEntity.Firstname = guestInfoEntity.FirstName;
        //    kOMPOSTRequestEntity.Lastname = guestInfoEntity.LastName;
        //    if (guestInfoEntity.Mobile != null)
        //    {
        //        kOMPOSTRequestEntity.Mobilenumber = guestInfoEntity.Mobile.Number;
        //    }
        //    kOMPOSTRequestEntity.City = guestInfoEntity.City;
        //    kOMPOSTRequestEntity.Companyaddress = guestInfoEntity.CompanyName;
        //    kOMPOSTRequestEntity.Companyorganization = guestInfoEntity.CompanyName;
        //    kOMPOSTRequestEntity.Street = guestInfoEntity.AddressLine1;

        //    KOMPostSubmitter kOMPostSubmitter = new KOMPostSubmitter();
        //    kOMPostSubmitter.Post(kOMPOSTRequestEntity);
        //}

        /// <summary>
        /// Returns a string representing the correct action for the form
        /// </summary>
        /// <param name="currentPage">
        /// Current Episerver page
        /// </param>
        /// <returns>Action as string</returns>
        protected static string GetAction(EPiServer.Core.PageData currentPage)
        {
            if (currentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_SWEDISH))
                return ActionConstant.ACTION_SWEDISH;
            else if (currentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_ENGLISH))
                return ActionConstant.ACTION_ENGLISH;
            else if (currentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_FINNISH))
                return ActionConstant.ACTION_FINNISH;
            else if (currentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_DANISH))
                return ActionConstant.ACTION_DANISH;
            else if (currentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_NORWEGIAN))
                return ActionConstant.ACTION_NORWEGIAN;
            else
                return ActionConstant.ACTION_ENGLISH;
        }

        /// <summary>
        /// Return the correct language for the form
        /// </summary>
        /// <param name="currentPage">
        /// Current Episerver page
        /// </param>
        /// <returns>Language as form</returns>
        public static string GetLanguage(PageData currentPage)
        {
            if (currentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_SWEDISH))
                return LanguageNameConstant.LANGUAGE_SWEDISH;
            else if (currentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_ENGLISH))
                return LanguageNameConstant.LANGUAGE_ENGLISH;
            else if (currentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_FINNISH))
                return LanguageNameConstant.LANGUAGE_FINNISH;
            else if (currentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_DANISH))
                return LanguageNameConstant.LANGUAGE_DANISH;
            else if (currentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_NORWEGIAN))
                return LanguageNameConstant.LANGUAGE_NORWEGIAN;
            else if (currentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_GERMANY))
                return LanguageNameConstant.LANGUAGE_GERMANY;
            else if (currentPage.LanguageID.Equals(CurrentPageLanguageConstant.LANGAUGE_RUSSIAN))
                return LanguageNameConstant.LANGUAGE_RUSSIA;
            else
                return LanguageNameConstant.LANGUAGE_ENGLISH;
        }

        #endregion

        #region Phone Number

        /// <summary>
        /// Select Selected Phone Codes
        /// </summary>
        /// <param name="totalphoneNumber"></param>
        /// <param name="phoneNumber"></param>
        /// <returns>Phone Code Numebers</returns>
        public static string GetPhoneCodesNumber(string totalphoneNumber, out string phoneNumber)
        {
            char[] zeroChars = new char[1];
            zeroChars[0] = '0';
            phoneNumber = string.Empty;

            string phoneCode = string.Empty;
            if (!string.IsNullOrEmpty(totalphoneNumber))
            {
                if (totalphoneNumber.Length > AppConstants.MAX_LENGTH_PHONECODE)
                {
                    if (!totalphoneNumber.StartsWith(AppConstants.PLUS))
                    {
                        phoneCode = totalphoneNumber.Substring(0, AppConstants.MAX_LENGTH_PHONECODE);
                        phoneNumber = totalphoneNumber.Substring(AppConstants.MAX_LENGTH_PHONECODE).TrimStart(zeroChars);
                        phoneCode = (phoneCode != string.Empty)
                                        ? phoneCode.TrimStart(zeroChars)
                                        : string.Empty;
                    }
                    else
                    {
                        phoneCode = totalphoneNumber.Substring(AppConstants.ONE_INT, AppConstants.TWO_INT);
                        phoneNumber = totalphoneNumber.Substring(AppConstants.THREE_INT).TrimStart('0');
                        phoneCode = (phoneCode != string.Empty) ? phoneCode.TrimStart(zeroChars) : string.Empty;
                    }
                }
            }
            return phoneCode;
        }

        #endregion

        #region Booking Details

        /// <summary>
        /// This method will format the First Name, Last name & The city name i.e. 
        /// Add Line break after every 22 characters
        /// So that the details will not overlap the boundaries, it will go to next line, 
        /// only while displaying on PDF & Printer friendly
        /// </summary>
        /// <param name="details"></param>
        /// <param name="length"></param>
        /// <returns>Formatted String</returns>
        public static string FormatFieldsToDisplay(string details, Int32 length)
        {
            StringBuilder formatter = new System.Text.StringBuilder();
            string finalvalue = details;

            int len = (finalvalue.Length / length);

            if (len > 0)
            {
                int index;
                for (int i = 0; i < len; i++)
                {
                    index = i * length;
                    if ((index + length) <= finalvalue.Length)
                    {
                        formatter.Append(finalvalue.Substring(index, length));
                        formatter.Append("<br/>");
                    }
                    else
                    {
                        formatter.Append(finalvalue.Substring(index));
                    }
                }
                if ((finalvalue.Length) > (len * length))
                {
                    formatter.Append(finalvalue.Substring(len * length));
                }
                finalvalue = formatter.ToString();
            }
            return finalvalue;
        }

        /// <summary>
        /// E-mail address on confirmation page 
        /// </summary>
        /// <param name="details"></param>
        /// <returns>Formatted String</returns>
        public static string FormateFieldsToDisplayEmail(string details)
        {
            StringBuilder formatter = new System.Text.StringBuilder();

            if (details.Length > 29)
            {
                formatter.Append("<br/>");
                formatter.Append(details);
                details = formatter.ToString();
            }
            return details;
        }

        /// <summary>
        /// Collect Hotel Specific Email Details
        /// </summary>
        /// <param name="bookingMap">
        /// Dictionary of Email template tokens with its corresponding values
        /// </param>
        /// <param name="guestIterator"></param>
        public static void CollectHotelRoomEmailDetails(ref Dictionary<string, string> bookingMap, int guestIterator)
        {
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
            string campaignCode = SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode;
            if ((SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.BONUSCHEQUE)
                && (campaignCode != null) && (campaignCode.Trim() != string.Empty))
            {
                bookingMap[CommunicationTemplateConstants.DNUMBER] = SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode;
            }
            else
            {
                bookingMap[CommunicationTemplateConstants.NO_DNUMBER] = CommunicationTemplateConstants.BLANK_SPACES;
            }
            IList<BaseRoomRateDetails> listHotelRoomRate = HotelRoomRateSessionWrapper.ListHotelRoomRate;
            BaseRoomRateDetails roomRateDetails =
                (listHotelRoomRate != null && listHotelRoomRate.Count > 0) ? listHotelRoomRate[0] : null;
            TimeSpan timeSpan = hotelSearch.DepartureDate.Subtract(hotelSearch.ArrivalDate);

            string selectedHotelRoomRate = string.Empty;
            string selectedHotelTotalRate = string.Empty;
            string hotelRoomRateForSms = string.Empty;

            Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
            if ((selectedRoomAndRatesHashTable != null) &&
                 (selectedRoomAndRatesHashTable.ContainsKey(guestIterator)) &&
                 (roomRateDetails != null) && (!BookingEngineSessionWrapper.DirectlyModifyContactDetails))
            {
                SelectedRoomAndRateEntity selectedRoomRates =
                    selectedRoomAndRatesHashTable[guestIterator] as SelectedRoomAndRateEntity;

                string hotelCountryCode = roomRateDetails.CountryCode;
                double basePoints = 0;
                double totalPoints = 0;
                if (null != selectedRoomRates.GetCommonBasePoints())
                {
                    basePoints = selectedRoomRates.GetCommonBasePoints().PointsRequired;
                    totalPoints = selectedRoomRates.GetCommonBasePoints().PointsRequired * timeSpan.Days;
                }
                string selectedRateCategoryName = string.Empty;
                selectedRateCategoryName =
                    Utility.GetRateCategoryName
                        (roomRateDetails, hotelSearch.SearchingType, selectedRoomRates.RateCategoryID);

                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                {
                    selectedHotelRoomRate =
                        Utility.GetRoomRateString
                            (selectedRateCategoryName, selectedRoomRates.GetCommonRate(), totalPoints,
                             hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year, hotelCountryCode,
                             hotelSearch.NoOfNights, hotelSearch.RoomsPerNight);
                }
                else
                {
                    selectedHotelRoomRate =
                        Utility.GetRoomRateString(selectedRateCategoryName, selectedRoomRates.GetCommonRate(),
                                                  basePoints, hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year,
                                                  hotelCountryCode,
                                                  AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                }

                selectedHotelTotalRate =
                    Utility.GetRoomRateString(null, selectedRoomRates.GetCommonTotalRate(),
                                              totalPoints, hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year,
                                              hotelCountryCode,
                                              hotelSearch.NoOfNights, hotelSearch.RoomsPerNight);

                bookingMap[CommunicationTemplateConstants.ROOM_RATE] = selectedHotelRoomRate;
                bookingMap[CommunicationTemplateConstants.TOTAL_PRICE] = selectedHotelTotalRate;
                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                {
                    hotelRoomRateForSms =
                        Utility.GetRoomRateString(selectedRateCategoryName, selectedRoomRates.GetCommonRate(),
                                                  totalPoints, hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year,
                                                  hotelCountryCode,
                                                  hotelSearch.NoOfNights, hotelSearch.RoomsPerNight,
                                                  LanguageConstant.LANGUAGE_ENGLISH);
                }
                else
                {
                    hotelRoomRateForSms =
                        Utility.GetRoomRateString(selectedRateCategoryName, selectedRoomRates.GetCommonRate(),
                                                  basePoints, hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year,
                                                  hotelCountryCode,
                                                  AppConstants.PER_NIGHT, AppConstants.PER_ROOM,
                                                  LanguageConstant.LANGUAGE_ENGLISH);
                }
                bookingMap[CommunicationTemplateConstants.ROOM_TYPE] = selectedRoomRates.SelectedRoomCategoryName;
            }
            else
            {
                List<BookingDetailsEntity> activeBookingDetails = BookingEngineSessionWrapper.ActiveBookingDetails;
                if ((activeBookingDetails != null) && (activeBookingDetails.Count > 0))
                {
                    for (int iterator = 0; iterator < activeBookingDetails.Count; iterator++)
                    {
                        if (activeBookingDetails[guestIterator] != null)
                        {
                            BookingDetailsEntity activeBooking = activeBookingDetails[guestIterator];
                            string roomTypeCode = activeBooking.HotelRoomRate.RoomtypeCode;
                            RoomCategory roomCategory = RoomRateUtil.GetRoomCategory(roomTypeCode);
                            if (null != roomCategory)
                            {
                                bookingMap[CommunicationTemplateConstants.ROOM_TYPE] = roomCategory.RoomCategoryName;
                            }
                            RetrieveRateDetails
                                (out selectedHotelRoomRate, out selectedHotelTotalRate, out hotelRoomRateForSms,
                                 activeBookingDetails[guestIterator].HotelRoomRate);
                        }
                        bookingMap[CommunicationTemplateConstants.ROOM_RATE] = selectedHotelRoomRate;
                        ;
                        bookingMap[CommunicationTemplateConstants.TOTAL_PRICE] = selectedHotelTotalRate;
                    }
                }
            }
            bookingMap[CommunicationTemplateConstants.ROOM_RATE_SMS] = hotelRoomRateForSms;
        }

        /// <summary>
        /// Collect Generic Hotel Email Details
        /// </summary>
        /// <returns>Email Details</returns>
        public static Dictionary<string, string> CollectGenericHotelEmailDetails()
        {
            Dictionary<string, string> bookingMap = new Dictionary<string, string>();
            int totalAdults = GetTotalAdults();
            int totalChildren = GetTotalChildren();
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
            if (hotelSearch != null)
            {
                HotelDestination hotelDestination = ContentDataAccess.GetHotelByOperaID(hotelSearch.SelectedHotelCode);
                bookingMap[CommunicationTemplateConstants.CITY_HOTEL] = (hotelDestination != null) ? (hotelDestination.Name) : string.Empty;
                if (hotelDestination != null && hotelDestination.HotelAddress != null)
                {
                    bookingMap[CommunicationTemplateConstants.HOTELADDRESS] = hotelDestination.HotelAddress.StreetAddress.ToString();
                    bookingMap[CommunicationTemplateConstants.HOTELCITY] = hotelDestination.HotelAddress.City.ToString();
                    if (!string.IsNullOrEmpty(hotelDestination.HotelAddress.Country))
                    {
                        bookingMap[CommunicationTemplateConstants.HOTELCOUNTRY] = hotelDestination.HotelAddress.Country.ToString();
                    }
                    else
                    {
                        bookingMap[CommunicationTemplateConstants.HOTELCOUNTRY] = CommunicationTemplateConstants.BLANK_SPACES;
                    }
                    bookingMap[CommunicationTemplateConstants.HOTELPOSTCODE] = hotelDestination.HotelAddress.PostCode.ToString();
                }
                else
                {
                    bookingMap[CommunicationTemplateConstants.HOTELADDRESS] = CommunicationTemplateConstants.BLANK_SPACES;
                    bookingMap[CommunicationTemplateConstants.HOTELCITY] = CommunicationTemplateConstants.BLANK_SPACES;
                    bookingMap[CommunicationTemplateConstants.HOTELCOUNTRY] = CommunicationTemplateConstants.BLANK_SPACES;
                    bookingMap[CommunicationTemplateConstants.HOTELPOSTCODE] = CommunicationTemplateConstants.BLANK_SPACES;
                }
                if ((hotelDestination != null) && (!string.IsNullOrEmpty(hotelDestination.Telephone)))
                {
                    bookingMap[CommunicationTemplateConstants.HOTELTELEPHONE] = hotelDestination.Telephone.ToString();
                }
                else
                {
                    bookingMap[CommunicationTemplateConstants.HOTELTELEPHONE] = CommunicationTemplateConstants.BLANK_SPACES;
                }
                if ((hotelDestination != null) && (!string.IsNullOrEmpty(hotelDestination.Email)))
                {
                    bookingMap[CommunicationTemplateConstants.HOTELEMAIL] = hotelDestination.Email.ToString();
                }
                else
                {
                    bookingMap[CommunicationTemplateConstants.HOTELEMAIL] = CommunicationTemplateConstants.BLANK_SPACES;
                }
                bookingMap[CommunicationTemplateConstants.NUMBER_ROOM] = AppConstants.ONE;
                IFormatProvider cultureInfo = new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
                bookingMap[CommunicationTemplateConstants.ARRIVAL_DATE] = hotelSearch.ArrivalDate.ToString("dd MMMM yyyy", cultureInfo);
                bookingMap[CommunicationTemplateConstants.DEPARTURE_DATE] = hotelSearch.DepartureDate.ToString("dd MMMM yyyy", cultureInfo);
                bookingMap[CommunicationTemplateConstants.BOOKING_DATE] = DateTime.Now.ToString("dd MMMM yyyy", cultureInfo);
                bookingMap[CommunicationTemplateConstants.NUMBER_OF_DAYS] = hotelSearch.NoOfNights.ToString();
                bookingMap[CommunicationTemplateConstants.NUMBER_OF_ADULTS] = totalAdults.ToString();
                bookingMap[CommunicationTemplateConstants.NUMBER_OF_CHILDREN] = totalChildren.ToString();
            }
            return bookingMap;
        }

        private static string GetViewModifyDLRedirectLink(string reservationNo, string lastName)
        {
            var encodedLastName = lastName;
            if (!string.IsNullOrEmpty(lastName))
            {
                encodedLastName = lastName.Replace(' ', '+');
            }
            return HttpContext.Current.Request.Url.Host + "/" + AppConstants.DLREDIRECT + "?" + AppConstants.DEEPLINK_VIEW_MODIFY_RESERVATION_ID + "=" + reservationNo + "&"
                + AppConstants.DEEPLINK_VIEW_MODIFY_LAST_NAME + "=" + encodedLastName;
        }

        /// <summary>
        /// Retrieve the Room Rate Details
        /// </summary>
        /// <param name="selectedHotelRoomRate">
        /// out BaseRoomRate
        /// </param>
        /// <param name="selectedHotelTotalRate">
        /// out TotalRoomRate
        /// </param>
        /// <param name="hotelRoomRateForSms">
        /// Get the room rate string in english, for SMS.
        /// </param>
        /// <param name="hotelRoomRateEntity"></param>
        private static void RetrieveRateDetails
            (out string selectedHotelRoomRate, out string selectedHotelTotalRate, out string hotelRoomRateForSms,
             HotelRoomRateEntity hotelRoomRateEntity)
        {
            string selectedRateCategoryName = string.Empty;
            selectedHotelRoomRate = string.Empty;
            selectedHotelTotalRate = string.Empty;
            hotelRoomRateForSms = string.Empty;
            HotelSearchEntity hotelSearch = BookingEngineSessionWrapper.BookingDetails.HotelSearch;
            if (hotelRoomRateEntity != null && (hotelSearch != null))
            {
                string hotelCountryCode = hotelSearch.HotelCountryCode;
                TimeSpan timeSpan = hotelSearch.DepartureDate.Subtract(hotelSearch.ArrivalDate);
                double basePoints = 0;
                double totalPoints = 0;
                basePoints = hotelRoomRateEntity.Points;
                if (hotelRoomRateEntity.TotalRate != null)
                {
                    totalPoints = hotelRoomRateEntity.TotalRate.Rate;
                }
                string ratePlanCode = hotelRoomRateEntity.RatePlanCode;
                if ((ratePlanCode != null) && (ratePlanCode != string.Empty))
                {
                    Rate rate = RoomRateUtil.GetRate(ratePlanCode);
                    if (rate != null)
                    {
                        selectedRateCategoryName = rate.RateCategoryName;
                    }
                }
                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                {
                    selectedHotelRoomRate =
                        Utility.GetRoomRateString(selectedRateCategoryName, hotelRoomRateEntity.Rate,
                                                  totalPoints, hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year,
                                                  hotelCountryCode, hotelSearch.NoOfNights, hotelSearch.RoomsPerNight);
                }
                else
                {
                    selectedHotelRoomRate =
                        Utility.GetRoomRateString(selectedRateCategoryName, hotelRoomRateEntity.Rate, basePoints,
                                                  hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year,
                                                  hotelCountryCode,
                                                  AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                }
                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                {
                    hotelRoomRateForSms =
                        Utility.GetRoomRateString(selectedRateCategoryName, hotelRoomRateEntity.Rate, totalPoints,
                                                  hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year,
                                                  hotelCountryCode,
                                                  hotelSearch.NoOfNights, hotelSearch.RoomsPerNight,
                                                  LanguageConstant.LANGUAGE_ENGLISH);
                }
                else
                {
                    hotelRoomRateForSms =
                        Utility.GetRoomRateString(selectedRateCategoryName, hotelRoomRateEntity.Rate, basePoints,
                                                  hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year,
                                                  hotelCountryCode,
                                                  AppConstants.PER_NIGHT, AppConstants.PER_ROOM,
                                                  LanguageConstant.LANGUAGE_ENGLISH);
                }

                switch (hotelSearch.SearchingType)
                {
                    case SearchType.REDEMPTION:
                        {
                            selectedHotelTotalRate =
                                Utility.GetRoomRateString(null, null, hotelRoomRateEntity.Points,
                                                          hotelSearch.SearchingType,
                                                          hotelSearch.ArrivalDate.Year, hotelCountryCode,
                                                          hotelSearch.NoOfNights,
                                                          hotelSearch.RoomsPerNight);
                            break;
                        }
                    case SearchType.VOUCHER:
                        {
                            selectedHotelTotalRate =
                                Utility.GetRoomRateString(null, null, totalPoints, hotelSearch.SearchingType,
                                                          hotelSearch.ArrivalDate.Year, hotelCountryCode,
                                                          hotelSearch.NoOfNights,
                                                          hotelSearch.RoomsPerNight);
                            break;
                        }
                    default:
                        {
                            selectedHotelTotalRate =
                                Utility.GetRoomRateString(null, hotelRoomRateEntity.TotalRate, totalPoints,
                                                          hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year,
                                                          hotelCountryCode,
                                                          hotelSearch.NoOfNights, hotelSearch.RoomsPerNight);
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Send Booking SMS Confirmation to Guest
        /// </summary>
        /// <param name="emailBookingMap"></param>
        /// <param name="confirmationNumber"></param>
        public static void SendSMSConfirmationToGuest
            (ref Dictionary<string, string> emailBookingMap, string legNo, string confirmationNo)
        {
            try
            {
                BookingEngineSessionWrapper.IsSmsRequired = true;
                Dictionary<string, string> smsBookingMap = new Dictionary<string, string>();
                smsBookingMap[CommunicationTemplateConstants.FIRST_NAME] = emailBookingMap[CommunicationTemplateConstants.FIRST_NAME];
                smsBookingMap[CommunicationTemplateConstants.LASTNAME] = emailBookingMap[CommunicationTemplateConstants.LASTNAME];
                smsBookingMap[CommunicationTemplateConstants.CITY_HOTEL] = emailBookingMap[CommunicationTemplateConstants.CITY_HOTEL];
                smsBookingMap[CommunicationTemplateConstants.HOTELADDRESS] = emailBookingMap[CommunicationTemplateConstants.HOTELADDRESS];
                smsBookingMap[CommunicationTemplateConstants.HOTELTELEPHONE] = emailBookingMap[CommunicationTemplateConstants.HOTELTELEPHONE];
                smsBookingMap[CommunicationTemplateConstants.CONFIRMATION_NUMBER] = emailBookingMap[CommunicationTemplateConstants.CONFIRMATION_NUMBER];
                smsBookingMap[CommunicationTemplateConstants.ROOM_RATE] = emailBookingMap[CommunicationTemplateConstants.ROOM_RATE_SMS];

                string last_Name = string.Empty;
                if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REDEMPTION)
                    last_Name = GuestBookingInformationSessionWrapper.FetchedGuestInformation.LastName;
                else
                    last_Name = emailBookingMap[CommunicationTemplateConstants.LASTNAME];

                //ConfirmationNumber is coming as Leg number here.
                //for First room, SMS should be generated without Leg number in deeplink and for other rooms with Leg Number
                if (legNo == "1")
                {
                    smsBookingMap[CommunicationTemplateConstants.SMS_DEEPLINK] = GetViewModifyDLRedirectLink(confirmationNo, last_Name);
                }
                else
                {
                    smsBookingMap[CommunicationTemplateConstants.SMS_DEEPLINK] =
                        GetViewModifyDLRedirectLink(emailBookingMap[CommunicationTemplateConstants.CONFIRMATION_NUMBER], last_Name);
                }
                smsBookingMap[CommunicationTemplateConstants.NUMBER_OF_DAYS] = emailBookingMap[CommunicationTemplateConstants.NUMBER_OF_DAYS];

                IFormatProvider cultureInfo = new CultureInfo("en-US");
                DateTime arrivalDate, departDate;
                if (DateTime.TryParse(emailBookingMap[CommunicationTemplateConstants.ARRIVAL_DATE], out arrivalDate))
                {
                    smsBookingMap[CommunicationTemplateConstants.ARRIVAL_DATE] = arrivalDate.ToString("dd MMM yy", cultureInfo);
                }
                if (DateTime.TryParse(emailBookingMap[CommunicationTemplateConstants.DEPARTURE_DATE], out departDate))
                {
                    smsBookingMap[CommunicationTemplateConstants.DEPARTURE_DATE] = departDate.ToString("dd MMM yy", cultureInfo);
                }

                SMSEntity smsEntity = CommunicationUtility.GetBookingConfirmationSMS(smsBookingMap);
                if ((smsEntity != null))
                {
                    smsEntity.PhoneNumber = emailBookingMap[CommunicationTemplateConstants.TELEPHONE2].StartsWith(AppConstants.PLUS) ? emailBookingMap[CommunicationTemplateConstants.TELEPHONE2].TrimStart('+') : emailBookingMap[CommunicationTemplateConstants.TELEPHONE2].TrimStart(new char[] { '0' });
                    if (!CommunicationService.SendSMS(smsEntity))
                    {
                        ErrorsSessionWrapper.AddFailedSMSRecepient(smsEntity.PhoneNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Failed to send booking confirmation SMS to the Guest for confirmation number: " +
                         confirmationNo);
            }
        }


        /// <summary>
        /// Send Booking SMS Confirmation to Guest
        /// </summary>
        /// <param name="currentGuestInformationEntity">The current guest information entity.</param>
        /// <param name="hotelSearch">The hotel search.</param>
        /// <param name="guestIterator">The guest iterator.</param>
        public static void SendSMSConfirmationToGuest
            (GuestInformationEntity currentGuestInformationEntity, HotelSearchEntity hotelSearch, int guestIterator)
        {
            try
            {
                BookingEngineSessionWrapper.IsSmsRequired = true;
                Dictionary<string, string> smsBookingMap = new Dictionary<string, string>();
                smsBookingMap[CommunicationTemplateConstants.FIRST_NAME] = currentGuestInformationEntity.FirstName;
                smsBookingMap[CommunicationTemplateConstants.LASTNAME] = currentGuestInformationEntity.LastName;
                smsBookingMap[CommunicationTemplateConstants.CITY_HOTEL] = hotelSearch.SearchedFor.SearchString;

                HotelDestination hotelDestination = ContentDataAccess.GetHotelByOperaID(hotelSearch.SelectedHotelCode);
                if (hotelDestination != null)
                {
                    smsBookingMap[CommunicationTemplateConstants.HOTELADDRESS] = hotelDestination.HotelAddress.StreetAddress;
                    smsBookingMap[CommunicationTemplateConstants.HOTELTELEPHONE] = hotelDestination.Telephone;
                }

                smsBookingMap[CommunicationTemplateConstants.CONFIRMATION_NUMBER] = currentGuestInformationEntity.ReservationNumber;
                smsBookingMap[CommunicationTemplateConstants.ROOM_RATE] = GethotelRoomRateStringForSms(hotelSearch, guestIterator);
                IFormatProvider cultureInfo = new CultureInfo("en-US");
                DateTime arrivalDate, departDate;
                if (DateTime.TryParse(hotelSearch.ArrivalDate.ToString(), out arrivalDate))
                {
                    smsBookingMap[CommunicationTemplateConstants.ARRIVAL_DATE] = arrivalDate.ToString("dd MMM yy", cultureInfo);
                }
                if (DateTime.TryParse(hotelSearch.DepartureDate.ToString(), out departDate))
                {
                    smsBookingMap[CommunicationTemplateConstants.DEPARTURE_DATE] = departDate.ToString("dd MMM yy", cultureInfo);
                }

                SMSEntity smsEntity = CommunicationUtility.GetBookingConfirmationSMS(smsBookingMap);
                if ((smsEntity != null))
                {
                    smsEntity.PhoneNumber = GuestBookingInformationSessionWrapper.GuestBookingInformation.Mobile.Number;
                    if (!CommunicationService.SendSMS(smsEntity))
                    {
                        ErrorsSessionWrapper.AddFailedSMSRecepient(smsEntity.PhoneNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException
                    (ex, "Failed to send booking confirmation SMS to the Guest for confirmation number: " +
                         GuestBookingInformationSessionWrapper.GuestBookingInformation.ReservationNumber);
            }
        }


        /// <summary>
        /// get SMS room rate string 
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="guestIterator"></param>
        /// <returns>SMS room rate string </returns>
        public static string GethotelRoomRateStringForSms(HotelSearchEntity hotelSearch, int guestIterator)
        {
            IList<BaseRoomRateDetails> listHotelRoomRate = HotelRoomRateSessionWrapper.ListHotelRoomRate;
            BaseRoomRateDetails roomRateDetails =
                (listHotelRoomRate != null && listHotelRoomRate.Count > 0) ? listHotelRoomRate[guestIterator] : null;
            TimeSpan timeSpan = hotelSearch.DepartureDate.Subtract(hotelSearch.ArrivalDate);

            string selectedHotelRoomRate = string.Empty;
            string selectedHotelTotalRate = string.Empty;
            string hotelRoomRateForSms = string.Empty;

            Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;

            if
                ((selectedRoomAndRatesHashTable != null) &&
                 (selectedRoomAndRatesHashTable.ContainsKey(guestIterator)) &&
                 (roomRateDetails != null) && (!BookingEngineSessionWrapper.DirectlyModifyContactDetails))
            {
                SelectedRoomAndRateEntity selectedRoomRates =
                    selectedRoomAndRatesHashTable[guestIterator] as SelectedRoomAndRateEntity;
                string hotelCountryCode = roomRateDetails.CountryCode;
                double basePoints = 0;
                double totalPoints = 0;
                if (null != selectedRoomRates.GetCommonBasePoints())
                {
                    basePoints = selectedRoomRates.GetCommonBasePoints().PointsRequired;
                    totalPoints = selectedRoomRates.GetCommonBasePoints().PointsRequired * timeSpan.Days;
                }
                string selectedRateCategoryName = string.Empty;
                selectedRateCategoryName =
                    Utility.GetRateCategoryName
                        (roomRateDetails, hotelSearch.SearchingType, selectedRoomRates.RateCategoryID);

                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                {
                    selectedHotelRoomRate =
                        Utility.GetRoomRateString
                            (selectedRateCategoryName, selectedRoomRates.GetCommonRate(), totalPoints,
                             hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year, hotelCountryCode,
                             hotelSearch.NoOfNights, hotelSearch.RoomsPerNight);
                }
                else
                {
                    selectedHotelRoomRate =
                        Utility.GetRoomRateString
                            (selectedRateCategoryName, selectedRoomRates.GetCommonRate(), basePoints,
                             hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year, hotelCountryCode,
                             AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                }

                selectedHotelTotalRate =
                    Utility.GetRoomRateString
                        (null, selectedRoomRates.GetCommonTotalRate(), totalPoints, hotelSearch.SearchingType,
                         hotelSearch.ArrivalDate.Year, hotelCountryCode, hotelSearch.NoOfNights,
                         hotelSearch.RoomsPerNight);
                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                {
                    hotelRoomRateForSms =
                        Utility.GetRoomRateString(selectedRateCategoryName, selectedRoomRates.GetCommonRate(),
                                                  totalPoints, hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year,
                                                  hotelCountryCode,
                                                  hotelSearch.NoOfNights, hotelSearch.RoomsPerNight,
                                                  LanguageConstant.LANGUAGE_ENGLISH);
                }
                else
                {
                    hotelRoomRateForSms =
                        Utility.GetRoomRateString(selectedRateCategoryName, selectedRoomRates.GetCommonRate(),
                                                  basePoints, hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year,
                                                  hotelCountryCode,
                                                  AppConstants.PER_NIGHT, AppConstants.PER_ROOM,
                                                  LanguageConstant.LANGUAGE_ENGLISH);
                }
            }
            return hotelRoomRateForSms;
        }


        /// <summary>
        /// Send Booking Email Confirmation to Guest
        /// </summary>
        /// <param name="emailBookingMap">Booking map</param>
        /// <param name="guestInfoEntity">Guest Information Entity</param>
        /// <param name="confirmationNumber">Confirmation number for this booking.</param>
        public static void SendEmailConfirmationToGuest
            (ref Dictionary<string, string> emailBookingMap, GuestInformationEntity guestInfoEntity,
             string confirmationNumber)
        {
            try
            {
                bool ismodifyable = BookingEngineSessionWrapper.IsModifyBooking;
                EmailEntity emailEntity =
                    CommunicationUtility.GetBookingConfirmationEmail(emailBookingMap, ismodifyable);
                if (emailBookingMap != null)
                {
                    string[] emailList = null;
                    if (guestInfoEntity.IsRewardNightGift && LoyaltyDetailsSessionWrapper.LoyaltyDetails != null)
                    {
                        emailList =
                            new string[]
                                {
                                    emailBookingMap[CommunicationTemplateConstants.EMAIL],
                                    emailBookingMap[CommunicationTemplateConstants.EMAIL2]
                                };
                    }
                    else
                    {
                        emailList = new string[] { emailBookingMap[CommunicationTemplateConstants.EMAIL] };
                    }
                    emailEntity.Recipient = emailList;
                    bool sendStatus = CommunicationService.SendMail(emailEntity, confirmationNumber);
                    if (!sendStatus)
                    {
                        if (emailEntity.Recipient != null && emailEntity.Recipient.Length > 0)
                        {
                            for (int i = 0; i < emailEntity.Recipient.Length; i++)
                            {
                                ErrorsSessionWrapper.AddFailedEmailRecipient(emailEntity.Recipient[i].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException
                    (ex, "Failed to send booking confirmation email to the Guest for confirmation number: " +
                         confirmationNumber);
            }
        }


        /// <summary>
        /// SendEmailConfirmationToGuest
        /// </summary>
        /// <param name="emailEntity"></param>
        /// <param name="confirmationNumber"></param>
        public static void SendEmailConfirmationToGuest(EmailEntity emailEntity, string confirmationNumber)
        {
            try
            {
                bool sendStatus = CommunicationService.SendMail(emailEntity, confirmationNumber);
                if (!sendStatus)
                {
                    if (emailEntity.Recipient != null && emailEntity.Recipient.Length > 0)
                    {
                        for (int i = 0; i < emailEntity.Recipient.Length; i++)
                        {
                            ErrorsSessionWrapper.AddFailedEmailRecipient(emailEntity.Recipient[i].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException
                    (ex, "Failed to send booking confirmation email to the Guest for confirmation number: " +
                         confirmationNumber);
            }
        }

        /// <summary>
        /// Clean the Session from previous search
        /// </summary>
        public static void CleanSession()
        {
            SearchCriteriaSessionWrapper.SearchCriteria = null;
            HotelRoomRateSessionWrapper.ListHotelRoomRate = null;
            HotelRoomRateSessionWrapper.TotalRateCalculated = false;
            HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable = null;
            HotelRoomRateSessionWrapper.ListRoomStay = null;
            Reservation2SessionWrapper.AvailabilityCalendar = null;
            Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage = false;
            BookingEngineSessionWrapper.DirectlyModifyContactDetails = false;
            BookingEngineSessionWrapper.IsModifyBooking = false;
            HygieneSessionWrapper.IsModifySelectRateFromBreadCrumb = false;
            BookingEngineSessionWrapper.IsModifyComboBooking = false;
            Reservation2SessionWrapper.PaymentGuestInformation = null;
            Reservation2SessionWrapper.PaymentHotelRoomRateInformation = null;
            Reservation2SessionWrapper.PaymentIsSessionBooking = false;
            Reservation2SessionWrapper.PaymentOrderAmount = null;
            HygieneSessionWrapper.IsEarlyBookingUsingStoredPanHashCC = false;
            Reservation2SessionWrapper.PaymentTransactionId = null;
            GenericSessionVariableSessionWrapper.IsPrepaidBooking = false;
            SearchCriteriaSessionWrapper.IsPromocodeInvalidForHotel = false;
        }

        #endregion

        #region Booking Search

        /// <summary>
        /// Remove the following character from the passed string
        /// '-' [Hyphen]
        /// ' ' [Space]
        /// </summary>
        /// <param name="code">
        /// string to be formatted
        /// </param>
        /// <returns>
        /// Formatted string
        /// </returns>
        public static string FormatCode(string code)
        {
            string negotiatedCode = string.Empty;
            if (!string.IsNullOrEmpty(code))
            {
                negotiatedCode = code.ToUpper().Trim();
                char[] charsToRemove = new char[] { '\n', '\r', '\t', ' ', '-' };
                foreach (char c in charsToRemove)
                {
                    negotiatedCode = negotiatedCode.Replace(c.ToString(), "");
                }
            }
            return negotiatedCode;
        }

        #endregion

        #region GetMembershipNameId

        /// <summary>
        /// Get Membership NameId
        /// </summary>
        /// <param name="membershipNo"></param>
        /// <param name="password"></param>
        /// <returns>Membership Name Id</returns>
        public static string GetMembershipNameId(string membershipNo, string password)
        {
            string nameID = string.Empty;

            try
            {
                AvailabilityController availController = new AvailabilityController();

                nameID = availController.AuthenticateUser(membershipNo, password);

                return nameID;
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
                return nameID;
            }
        }

        #endregion GetMembershipNameId

        #region GetMembershipOperaId

        /// <summary>
        /// Fetching the Membership Opera Id.
        /// </summary>
        /// <param name="membershipNameID"></param>
        /// <returns>MembershipOperaId</returns>
        public static string GetMembershipOperaId(string membershipNameID)
        {
            string membershipOperaID = string.Empty;

            try
            {
                AvailabilityController availabilityController = new AvailabilityController();

                ArrayList membershipList =
                    availabilityController.FetchGuestCardList(membershipNameID,
                                                              AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE);
                if ((membershipList != null) && (membershipList.Count > 0))
                {
                    MembershipEntity membershipEntity = (MembershipEntity)membershipList[0];
                    membershipOperaID = membershipEntity.OperaId;
                }

                return membershipOperaID;
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
                return membershipOperaID;
            }
        }

        #endregion GetMembershipOperaId

        #region GetQualifyingType

        /// <summary>
        /// Method to get the Qualifying type.
        /// </summary>
        /// <param name="campaignCode">Campaign code, which user inserted</param>
        /// <returns>If the Campaign code starts with "L" then the Qualifying type returned will be TRAVEL_AGENT
        /// or else it will return CORPORATE.</returns>
        public static string GetQualifyingType(string campaignCode)
        {
            string returnStr = string.Empty;
            if (campaignCode.Substring(0, 1) == "L")
            {
                returnStr = AppConstants.NEG_BOOKING_QUALIFYING_TYPE_TRAVEL_AGENT;
            }
            else if (campaignCode.Substring(0, 1) == "B")
            {
                returnStr = AppConstants.BLOCK_CODE_QUALIFYING_TYPE;
            }
            else
            {
                returnStr = AppConstants.NEGOTITATED_BOOKING_QUALIFYING_TYPE;
            }
            return returnStr;
        }

        #endregion GetQualifyingType

        #region GetForgottenMembershipNoPageReference

        /// <summary>
        /// This will return the URL of the help popup configured for Forgotten Membership Number.
        /// </summary>
        /// <returns>url</returns>
        public static PageReference GetForgottenMembershipNoPageReference()
        {
            PageData rootPage =
                DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess);
            PageReference pageLink = rootPage[EpiServerPageConstants.HOME_PAGE] as PageReference;
            PageData referencedPage = DataFactory.Instance.GetPage(pageLink, EPiServer.Security.AccessLevel.NoAccess);
            return referencedPage["ForgottenMembershipNo"] as PageReference;
        }


        /// <summary>
        /// Get the Page Reference for Remember Functionality
        /// </summary>
        /// <returns>PageReference</returns>
        public static PageReference GetRememberFunctionalityPageReference()
        {
            PageReference pageReference = null;
            PageData rootPage =
                DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess);
            PageReference pageLink = null;
            if (rootPage != null)
            {
                pageLink = rootPage[EpiServerPageConstants.HOME_PAGE] as PageReference;
                if (pageLink != null)
                {
                    PageData referencedPage =
                        DataFactory.Instance.GetPage(pageLink, EPiServer.Security.AccessLevel.NoAccess);
                    if (referencedPage != null)
                    {
                        pageReference = referencedPage["RememberFunctionalityHelp"] as PageReference;
                    }
                }
            }
            return pageReference;
        }

        /// <summary>
        /// Get the Page Reference for Booking Module Help Text
        /// </summary>
        /// <returns>PageReference</returns>
        public static PageReference GetBookingHelpPageReference()
        {
            PageReference pageReference = null;
            PageData rootPage =
                DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess);
            PageReference pageLink = null;
            if (rootPage != null)
            {
                pageLink = rootPage[EpiServerPageConstants.HOME_PAGE] as PageReference;
                if (pageLink != null)
                {
                    PageData referencedPage =
                        DataFactory.Instance.GetPage(pageLink, EPiServer.Security.AccessLevel.NoAccess);
                    if (referencedPage != null)
                    {
                        pageReference = referencedPage["Help Text"] as PageReference;
                    }
                }
            }
            return pageReference;
        }

        #endregion GetForgottenMembershipNoPageReference

        #region IsPrePaidBlock

        /// <summary>
        /// Checks is the block booking is prepaid block.
        /// Returns True if the block booking and CaptureGuarantee is not checked in CMS.
        /// </summary>
        /// <remarks>True/False</remarks>
        public static bool IsPrePaidBlock
        {
            get
            {
                bool returnValue = true;
                Block block = ContentDataAccess.GetBlockCodePages(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                if ((null != block) && (!block.CaptureGuarantee))
                {
                    returnValue = block.CaptureGuarantee;
                }
                return returnValue;
            }
        }

        #endregion IsPrePaidBlock

        #region IsBlockCodeBooking

        /// <summary>
        /// True - if it is block code booking
        /// False - if it is not block code booking.
        /// </summary>
        /// <remarks>True/False</remarks>
        public static bool IsBlockCodeBooking
        {
            get
            {
                bool returnStatue = false;
                if (BookingEngineSessionWrapper.IsModifyBooking)
                {
                    if
                        (BookingEngineSessionWrapper.BookingDetails != null &&
                         BookingEngineSessionWrapper.BookingDetails.HotelSearch.QualifyingType ==
                         AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
                    {
                        returnStatue = true;
                    }
                }
                else
                {
                    if
                        (SearchCriteriaSessionWrapper.SearchCriteria != null &&
                         SearchCriteriaSessionWrapper.SearchCriteria.QualifyingType == AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
                    {
                        returnStatue = true;
                    }
                }
                return returnStatue;
            }
        }

        #endregion IsBlockCodeBooking

        /// <summary>
        /// Block Code booking  not cancellabel
        /// </summary>
        /// <param name="isModifyCancel"></param>
        /// <param name="booking"></param>
        /// <returns></returns>
        public static bool BlockCodeBooking(bool isModifyCancel, BookingDetailsEntity booking, out string bookingStatus)
        {
            Block block = null;
            bookingStatus = string.Empty;
            block = Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(booking.HotelSearch.CampaignCode);
            if ((null != block))
            {
                DateTime cancellableDate = block.ModifyAndCancellableDate;
                int i = DateTime.Compare(cancellableDate, DateTime.Today.Date);
                if (i == -1)
                    bookingStatus = WebUtil.GetTranslatedText(TranslatedTextConstansts.BLOCK_NOT_CANCELABLE) + AppConstants.SPACE;
                else
                    isModifyCancel = true;

                if (block.GuranteeType == Convert.ToString(ConfigurationManager.AppSettings["PrePaid.Block"]))
                {
                    BookingEngineSessionWrapper.IsPrepaidBlockBooking = true;
                    isModifyCancel = false;
                }
            }
            return isModifyCancel;
        }

        #region GetModifyCancelableDate

        /// <summary>
        /// Get the modify and cancelable date.
        /// </summary>
        /// <returns>Cancelable and modify Date string</returns>
        public static string GetModifyCancelableDate()
        {
            string date = string.Empty;
            if (BookingEngineSessionWrapper.IsModifyBooking)
            {
                BookingDetailsEntity bookingDetailsEntity = BookingEngineSessionWrapper.AllBookingDetails[0];
                if (null != bookingDetailsEntity)
                {
                    date = DateUtil.DateToDDMMYYYYWords(bookingDetailsEntity.HotelSearch.CancelByDate);
                }
            }
            else
            {
                Block block = ContentDataAccess.GetBlockCodePages(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                if ((null != block) && (null != block.ModifyAndCancellableDate))
                {
                    DateTime cancellableDate = block.ModifyAndCancellableDate;

                    TimeSpan diffTime = cancellableDate.Subtract(SearchCriteriaSessionWrapper.SearchCriteria.CancelByDate);
                    if (diffTime.TotalDays < 0)
                        SearchCriteriaSessionWrapper.SearchCriteria.CancelByDate = cancellableDate;
                    date = DateUtil.DateToDDMMYYYYWords(SearchCriteriaSessionWrapper.SearchCriteria.CancelByDate);
                }
            }
            return date;
        }

        #endregion GetModifyCancelableDate

        #region OverrideCancelableDateIfBlockCode

        /// <summary>
        /// Reassign the modify and cancellable date from CMS,If this is block code booking.
        /// </summary>
        /// <param name="entities">Booking Details Entity</param>
        /// <returns>If block code is configured in CMS then return false.
        /// So that it will redirect to MODIFY_CANCEL_CHANGE_DATES
        /// </returns>
        public static void OverrideCancelableDateIfBlockCode(List<BookingDetailsEntity> entities)
        {
            string code = string.Empty;
            if (null != entities[0].HotelSearch)
                code = entities[0].HotelSearch.CampaignCode;

            Block block = ContentDataAccess.GetBlockCodePages(code);
            if ((null != block) && (null != block.ModifyAndCancellableDate))
            {
                DateTime cancellableDate = block.ModifyAndCancellableDate;
                int entityCount = entities.Count;
                for (int counter = 0; counter < entityCount; counter++)
                {
                    TimeSpan diffTime = cancellableDate.Subtract(entities[counter].HotelSearch.CancelByDate);
                    if (diffTime.TotalDays < 0)
                        entities[counter].HotelSearch.CancelByDate = cancellableDate;
                }
            }
        }

        #endregion OverrideCancelableDateIfBlockCode

        #region GetHideARBPrice
        public static bool GetHideARBPrice(string blockCode)
        {
            bool hideARBPrice = false;
            Block block = ContentDataAccess.GetBlockCodePages(blockCode);
            if (block != null)
            {
                hideARBPrice = block.HideARBPrice;
            }
            return hideARBPrice;
        }
        #endregion

        #region Kids Concept Code

        #region RedirectBookingFlow

        /// <summary>
        /// This is a generic method which is used to control
        /// the booking flow based on conditional branching 
        /// of the flow.
        /// Different flows possible are 
        /// HomePage->Childrens Page->Select Hotel->Select Rate OR
        /// HomePage->Childrens Page->Select Rate OR
        /// HomePage->Select Hotel->Select Rate OR
        /// HomePage->Select Rate
        /// </summary>
        /// <param name="bookingEnginePosition">The present position of the booking flow engine</param>
        /// <returns>true if method is success, false otherwise</returns>
        public static string RedirectBookingFlow(BookingEnginePosition bookingEnginePosition)
        {
            string urlToRedirect = string.Empty;

            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
            {
                if
                    (SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor != null &&
                     SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.SearchCode != null)
                {
                    switch (SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.UserSearchType)
                    {
                        case SearchedForEntity.LocationSearchType.City:
                            SelectHotelUtil.InitiateAsynchronousSearch(SearchCriteriaSessionWrapper.SearchCriteria);
                            urlToRedirect = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_HOTEL_PAGE);
                            break;
                        case SearchedForEntity.LocationSearchType.Hotel:
                            urlToRedirect = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE);
                            break;
                    }
                }
            }
            return urlToRedirect;
        }

        #endregion RedirectBookingFlow

        #region RedirectModifyKidsFlow

        /// <summary>
        /// This method is used to redirect to the correct page in the modify flow
        /// </summary>
        /// <param name="urlToRedirect"></param>
        /// <returns>True/False</returns>
        public static bool RedirectModifyKidsFlow(ref string urlToRedirect)
        {
            bool success = true;

            if
                (SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor != null &&
                 SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.SearchCode != null)
            {
                if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.ChildrenPerRoom > 0)
                {
                    urlToRedirect = GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CHILDREN_DETAILS_PAGE);
                }
                else
                {
                    urlToRedirect = GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE);
                }
            }
            else
            {
                success = false;
            }

            return success;
        }

        #endregion RedirectModifyKidsFlow

        #region ShowErrorMessage

        /// <summary>
        /// Generic method to show the error message on a div container
        /// </summary>
        /// <param name="clientErrorDiv">id os the div</param>
        /// <param name="errorMessage">error message string</param>
        public static void ShowErrorMessage(HtmlGenericControl clientErrorDiv, string errorMessage)
        {
            clientErrorDiv.Attributes.Add("class", "errorText");

            string start =
                "<div id='errorReport' class='formGroupError'><div class='redAlertIcon'>" +
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") +
                "</div><ul class='circleList'>";
            string end = "</ul></div>";
            clientErrorDiv.InnerHtml = start + errorMessage + end;
        }

        /// <summary>
        /// ShowErrorMessageForAccordian
        /// </summary>
        /// <param name="clientErrorDiv"></param>
        /// <param name="errorMessage"></param>
        public static void ShowErrorMessageForAccordian(HtmlGenericControl clientErrorDiv, string errorMessage)
        {
            clientErrorDiv.Attributes.Add("class", "errorText");
            string start =
                "<div id='errorReport' class='formGroupError'><div class='redAlertIcon'>" + errorMessage +
                "</div><ul class='circleList'></ul></div>";

            clientErrorDiv.InnerHtml = start;
        }

        #endregion ShowErrorMessage

        #region IsFamilyBooking

        /// <summary>
        /// It will returns true/false.
        /// If No Of Children > 0 then it return TRUE or else FALSE.
        /// </summary>
        /// <returns>True/False</returns>
        public static bool IsFamilyBooking()
        {
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            if (hotelSearch == null)
            {
                BookingDetailsEntity bookingEntity = BookingEngineSessionWrapper.BookingDetails;
                if (bookingEntity != null)
                {
                    if (bookingEntity.HotelSearch.ChildrenPerRoom == 0)
                        return false;
                    else
                        return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (hotelSearch.ChildrenPerRoom == 0)
                    return false;
                else
                    return true;
            }
        }

        #endregion IsFamilyBooking

        #region PopulateChildrenAccomodationInnerHtml

        /// <summary>
        /// Gets the each children row.
        /// </summary>
        /// <param name="childCount">No of dropdown to be created</param>
        /// <param name="maxAge">Maximum age that will be displayed in the dropdown</param>
        /// <param name="childrenDetailsEntity">Children entity</param>
        /// <returns>Html table row</returns>
        public static HtmlTableRow PopulateChildrenAccomodationInnerHtml(int childCount, int maxAge,
                                                                         ChildrensDetailsEntity childrenDetailsEntity)
        {
            int minAge = Convert.ToInt32(ConfigurationManager.AppSettings["Booking.Children.MinAge"]);
            IList<ChildEntity> childEntity = null;
            if (null != childrenDetailsEntity)
            {
                childEntity = childrenDetailsEntity.ListChildren;
            }
            HtmlTableRow row = new HtmlTableRow();
            HtmlTableCell cell1 = new HtmlTableCell();
            HtmlTableCell cell2 = new HtmlTableCell();
            HtmlTableCell cell3 = new HtmlTableCell();

            StringBuilder rowLabel = new StringBuilder();
            rowLabel.Append("<span id =\"SpanKids" + childCount + "\"><strong>");
            rowLabel.Append(WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child"));
            rowLabel.Append(AppConstants.SPACE);
            rowLabel.Append(childCount + 1);
            rowLabel.Append("</strong></span>");
            cell1.InnerHtml = rowLabel.ToString();
            StringBuilder dropdownString = new StringBuilder();
            dropdownString.Append
                ("<select name=\"DropDown" + childCount + "\" id=\"DropDown" + childCount +
                 "\" onclick=\"GenerateRadioStr('RadioDiv" + childCount + "','DropDown" + childCount + "' );\">");
            dropdownString.Append
                ("<option value=\"Default\">" +
                 WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/select") + "</option>");
            for (int age = minAge; age <= maxAge; age++)
            {
                if ((childEntity != null) && (childCount < childEntity.Count) &&
                    (childEntity[childCount].Age == age))
                {
                    dropdownString.Append
                        ("<option value=\"" + age.ToString() + "\" selected=\"selected\">" +
                         age.ToString() + "</option>");
                }
                else
                {
                    dropdownString.Append("<option value=\"" + age.ToString() + "\" >" + age.ToString() + "</option>");
                }
            }
            dropdownString.Append("</select>");
            cell2.InnerHtml = dropdownString.ToString();
            if ((childEntity == null))
            {
                cell3.InnerHtml = "<div id='RadioDiv" + childCount + "'/>";
            }
            else
            {
                if ((childEntity != null) && (childCount < childEntity.Count))
                {
                    cell3.InnerHtml = GetRadioString(childEntity[childCount].Age, childCount,
                                                     childEntity[childCount].AccommodationString);
                }
            }
            row.Cells.Add(cell1);
            row.Cells.Add(cell2);
            row.Cells.Add(cell3);

            return row;
        }

        #endregion PopulateChildrenAccomodationInnerHtml

        #region GetRadioString

        /// <summary>
        /// Generates the Html string which will render the radio buttons.
        /// </summary>
        /// <param name="childAge">Age of the current child</param>
        /// <param name="childCount">Count of the child in current entity</param>
        /// <param name="accString">Bed type accomodation string of current children</param>
        /// <returns>HTML string</returns>
        public static string GetRadioString(int childAge, int childCount, string accString)
        {
            string criteria = WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/childcriteria");
            string[] eachCriteria = criteria.Split(new Char[] { ',' });
            int eachCriteriaCount = eachCriteria.Length;
            StringBuilder radioString = new StringBuilder();
            for (int count = 0; count < eachCriteriaCount; count++)
            {
                string[] ageAndAcc = eachCriteria[count].Split(new Char[] { '|' });
                string[] ages = ageAndAcc[0].Split(new Char[] { '-' });
                if ((childAge >= Convert.ToInt32(ages[0])) && (childAge <= Convert.ToInt32(ages[1])))
                {
                    radioString.Append("<div id=\"RadioDiv" + childCount + "\">");
                    string[] beds = ageAndAcc[1].Split(new Char[] { '_' });
                    int noOfBeds = beds.Length;
                    for (int bedCount = 0; bedCount < noOfBeds; bedCount++)
                    {
                        radioString.Append
                            ("<span><input id=\"RadioButtonRadioDiv" + childCount +
                             "\" type=\"radio\" onclick=\"CheckforDuplicates();\"");
                        radioString.Append
                            (" value=\"" + beds[bedCount] + "\" name=\"RadioButtonRadioDiv" + childCount + "\"");
                        if (accString == beds[bedCount])
                        {
                            radioString.Append(" checked=\"true\"");
                        }
                        radioString.Append(">");
                        radioString.Append(beds[bedCount]);
                        radioString.Append("</input></span>");
                    }
                    radioString.Append("</div>");
                    break;
                }
            }

            return radioString.ToString();
        }

        #endregion GetRadioString

        #region GetNoOfChildrenToBeAccommodated

        /// <summary>
        /// This method calculats no of children to be considered as occupancy under the given 
        /// search criteria since CIPB is not considered as an occupancy
        /// </summary>
        /// <param name="childrensDetailsEntity">ChildrensDetailsEntity</param>
        /// <returns>No Of Children To Be Accommodated</returns>
        public static int GetNoOfChildrenToBeAccommodated(ChildrensDetailsEntity childrensDetailsEntity)
        {
            int noOfChildrenToBeAccommodated = 0;
            try
            {
                if (childrensDetailsEntity != null)
                {
                    if (childrensDetailsEntity.ListChildren.Count > 0)
                    {
                        foreach (ChildEntity child in childrensDetailsEntity.ListChildren)
                        {
                            if (child != null)
                            {
                                if
                                    ((child.ChildAccommodationType == ChildAccommodationType.CRIB) ||
                                     (child.ChildAccommodationType == ChildAccommodationType.XBED))
                                {
                                    noOfChildrenToBeAccommodated++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Could not get the actual number of children to be accommodated.");
                throw;
            }

            return noOfChildrenToBeAccommodated;
        }

        /// <summary>
        /// Get the locate specific string for cot, extra bed and Child in adult's bed
        /// </summary>
        /// <param name="cot">locale for cot</param>
        /// <param name="extraBed">locale for extra bed</param>
        /// <param name="sharingBed">locale for Child in adult's bed</param>
        public static void GetLocaleSpecificChildrenAccomodationTypes
            (ref string cot, ref string extraBed, ref string sharingBed)
        {
            cot = WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/crib");
            extraBed = WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/extrabed");
            sharingBed =
                WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/sharingbed");
        }

        #endregion

        #endregion Kids Concept Code

        /// <summary>
        /// Computes the total of adults based on the active booking
        /// </summary>
        /// <returns>total adults</returns>
        public static int GetTotalAdults()
        {
            int noOfAdults = 0;
            foreach (BookingDetailsEntity booking in BookingEngineSessionWrapper.ActiveBookingDetails)
            {
                noOfAdults += booking.HotelSearch.AdultsPerRoom;
            }
            return noOfAdults;
        }

        /// <summary>
        /// Computes the total of adults based on the cancelled booking
        /// </summary>
        /// <returns>Total Adults Cancellation</returns>
        public static int GetTotalAdultsCancellation()
        {
            int noOfAdults = 0;
            if (BookingEngineSessionWrapper.AllBookingDetails != null)
            {
                if (BookingEngineSessionWrapper.AllCancelledDetails != null)
                {
                    foreach (CancelDetailsEntity cnclEntity in BookingEngineSessionWrapper.AllCancelledDetails)
                    {
                        foreach (BookingDetailsEntity bkngEntity in BookingEngineSessionWrapper.AllBookingDetails)
                        {
                            if
                                (cnclEntity.ReservationNumber == bkngEntity.ReservationNumber &&
                                 cnclEntity.LegNumber == bkngEntity.LegNumber)
                            {
                                noOfAdults += bkngEntity.HotelSearch.AdultsPerRoom;
                                break;
                            }
                        }
                    }
                }
            }
            return noOfAdults;
        }

        /// <summary>
        /// Computes the total of adults based on the active booking
        /// </summary>
        /// <returns>Total Children</returns>
        public static int GetTotalChildren()
        {
            int noOfChildren = 0;
            foreach (BookingDetailsEntity booking in BookingEngineSessionWrapper.ActiveBookingDetails)
            {
                noOfChildren += booking.HotelSearch.ChildrenPerRoom;
            }
            return noOfChildren;
        }

        /// <summary>
        /// Computes the total of adults based on the cancelled booking
        /// </summary>
        /// <returns>Total Children Cancellation</returns>
        public static int GetTotalChildrenCancellation()
        {
            int noOfChildren = 0;
            if (BookingEngineSessionWrapper.AllBookingDetails != null)
            {
                if (BookingEngineSessionWrapper.AllCancelledDetails != null)
                {
                    foreach (CancelDetailsEntity cnclEntity in BookingEngineSessionWrapper.AllCancelledDetails)
                    {
                        foreach (BookingDetailsEntity bkngEntity in BookingEngineSessionWrapper.AllBookingDetails)
                        {
                            if
                                (cnclEntity.ReservationNumber == bkngEntity.ReservationNumber &&
                                 cnclEntity.LegNumber == bkngEntity.LegNumber)
                            {
                                noOfChildren += bkngEntity.HotelSearch.ChildrenPerRoom;
                                break;
                            }
                        }
                    }
                }
            }
            return noOfChildren;
        }

        /// <summary>
        /// Sets values in the passed CountryDropDown
        /// </summary>
        /// <param name="ddlCountry"></param>
        /// <param name="CurrentPage"></param>
        public static void SetCountryDropDown
            (System.Web.UI.WebControls.DropDownList ddlCountry, EPiServer.Core.PageData CurrentPage)
        {
            OrderedDictionary countryCodeMap = DropDownService.GetCountryCodes();
            if (countryCodeMap != null)
            {
                ddlCountry.Items.Clear();
                foreach (string key in countryCodeMap.Keys)
                {
                    ddlCountry.Items.Add(new System.Web.UI.WebControls.ListItem(countryCodeMap[key].ToString(), key));
                }
            }
            System.Web.UI.WebControls.ListItem selectedDefaultItem =
                ddlCountry.Items.FindByValue(AppConstants.DEFAULT_VALUE_CONST);
            if (selectedDefaultItem != null)
            {
                ddlCountry.ClearSelection();
                selectedDefaultItem.Selected = true;
            }
            PrePopulateCountry(ddlCountry, CurrentPage);
        }

        /// <summary>
        /// For Dannish and Swedish site the country should be selected bydefault to Dannish and Swedish respectivelly.
        /// </summary>
        private static void PrePopulateCountry
            (System.Web.UI.WebControls.DropDownList ddlCountry, EPiServer.Core.PageData CurrentPage)
        {
            string languageCode = CurrentPage.LanguageID.ToUpper();
            if (languageCode == LanguageConstant.LANGUAGE_SWEDISH)
            {
                languageCode = LanguageConstant.LANGUAGE_SV_MAP_COUNTRY_CODE;
            }
            else if (languageCode == LanguageConstant.LANGUAGE_DANISH)
            {
                languageCode = LanguageConstant.LANGUAGE_DA_MAP_COUNTRY_CODE;
            }
            else if (languageCode == LanguageConstant.LANGUAGE_RUSSIA_RU)
            {
                languageCode = LanguageConstant.LANGUAGE_RUSSIA;
            }
            System.Web.UI.WebControls.ListItem selectedDefaultItem = ddlCountry.Items.FindByValue(languageCode);
            if (selectedDefaultItem != null)
            {
                ddlCountry.ClearSelection();
                ddlCountry.SelectedItem.Selected = false;
                selectedDefaultItem.Selected = true;
            }
        }

        /// <summary>
        /// This will populate the telephone code and do a default select.
        /// </summary>
        /// <param name="ddlMobileNumber">
        /// </param>
        /// <param name="selectedCountryCode"></param>
        public static void SetPhoneCodesDropDown
            (System.Web.UI.WebControls.DropDownList ddlMobileNumber, string selectedCountryCode)
        {
            OrderedDictionary phoneCodesMap = DropDownService.GetTelephoneCodes();
            if (phoneCodesMap != null)
            {
                IDictionaryEnumerator phoneCodeEnumerator = phoneCodesMap.GetEnumerator();
                while (phoneCodeEnumerator.MoveNext())
                {
                    if (phoneCodeEnumerator.Key.ToString() == AppConstants.DEFAULT_VALUE_CONST ||
                        phoneCodeEnumerator.Key.ToString() == AppConstants.DEFAULT_SEPARATOR_CONST)
                    {
                        ddlMobileNumber.Items.Add
                            (new System.Web.UI.WebControls.ListItem(phoneCodesMap[phoneCodeEnumerator.Key.ToString()].ToString(), phoneCodeEnumerator.Key.ToString()));
                    }
                    else
                    {
                        ddlMobileNumber.Items.Add(new System.Web.UI.WebControls.ListItem(phoneCodeEnumerator.Key.ToString() + "  (" + phoneCodeEnumerator.Value.ToString() + ")", phoneCodeEnumerator.Key.ToString()));
                    }
                }
            }
            System.Web.UI.WebControls.ListItem selectedDefaultItem =
                ddlMobileNumber.Items.FindByValue(AppConstants.DEFAULT_VALUE_CONST);
            if (selectedDefaultItem != null)
            {
                selectedDefaultItem.Selected = true;
            }
            SetPhoneCodeAgainstCountry(ddlMobileNumber, selectedCountryCode);
        }

        /// <summary>
        /// Sets the phone code according to the country selected in country dropdown.
        /// </summary>
        /// <param name="ddlMobileNumber">
        /// </param>
        /// <param name="selectedCountryCode"></param>
        /// <returns>PhoneCode</returns>
        public static string SetPhoneCodeAgainstCountry
            (System.Web.UI.WebControls.DropDownList ddlMobileNumber, string selectedCountryCode)
        {
            string phoneCode = "";
            OrderedDictionary countryPhoneCodesMap = DropDownService.GetCountryPhoneCodeMap();
            if (countryPhoneCodesMap != null)
            {
                phoneCode = countryPhoneCodesMap[selectedCountryCode] as string;
                int selectedPhoneCode = -1;
                int.TryParse(phoneCode, out selectedPhoneCode);
                if (selectedPhoneCode > 0)
                {
                    if (!string.IsNullOrEmpty(phoneCode))
                    {
                        if (ddlMobileNumber != null)
                        {
                            System.Web.UI.WebControls.ListItem selectedItem =
                                ddlMobileNumber.Items.FindByValue(phoneCode);
                            if (selectedItem != null)
                            {
                                ddlMobileNumber.ClearSelection();
                                selectedItem.Selected = true;
                            }
                        }
                    }
                }
                else
                    phoneCode = string.Empty;
            }
            return phoneCode;
        }

        /// <summary>
        /// This is a generic method added to split the phone code & phone number,
        /// And then populate the phone code dropdown.
        /// </summary>
        /// <param name="ddlMobileNumber"></param>
        /// <param name="phoneNumber"></param>
        /// <returns>Returns the phone number</returns>
        public static string SetPhoneCountryCode
         (System.Web.UI.WebControls.DropDownList ddlMobileNumber, string phoneNumber)
        {
            char[] zeroChars = new char[1];
            zeroChars[0] = '0';
            string mobilePhoneCode = string.Empty;
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                if (phoneNumber.Length > AppConstants.MAX_LENGTH_PHONECODE || phoneNumber.StartsWith(AppConstants.PLUS))
                {
                    if (!phoneNumber.StartsWith(AppConstants.PLUS))
                    {
                        mobilePhoneCode = phoneNumber.Substring(0, AppConstants.MAX_LENGTH_PHONECODE);
                        phoneNumber = phoneNumber.Substring(AppConstants.MAX_LENGTH_PHONECODE).TrimStart(zeroChars);
                        mobilePhoneCode = (mobilePhoneCode != string.Empty)
                                              ? mobilePhoneCode.TrimStart(zeroChars)
                                              : string.Empty;
                    }
                    else
                    {
                        mobilePhoneCode = phoneNumber.Substring(AppConstants.ONE_INT, AppConstants.TWO_INT);
                        phoneNumber = phoneNumber.Substring(AppConstants.THREE_INT).TrimStart('0');
                        mobilePhoneCode = (mobilePhoneCode != string.Empty) ? mobilePhoneCode.TrimStart(zeroChars) : string.Empty;
                    }
                }
            }
            ListItem selectedMobileCode = ddlMobileNumber.Items.FindByValue(mobilePhoneCode);
            if (selectedMobileCode != null)
            {
                ddlMobileNumber.SelectedItem.Selected = false;
                selectedMobileCode.Selected = true;
            }

            return phoneNumber;
        }


        /// <summary>
        /// Returns the guarantee text to be shown on the page based on the Guarantee Information type
        /// </summary>
        /// <param name="guaranteeInfoEntity"></param>
        /// <param name="selectedroomrateentity"></param>
        /// <param name="roomNumber"></param>
        /// <returns>Guarantee Text</returns>
        public static string GetGuaranteeText
            (GuranteeInformationEntity guaranteeInfoEntity, SelectedRoomAndRateEntity selectedroomrateentity,
             int roomNumber)
        {
            string policyText = string.Empty;
            if
                (selectedroomrateentity != null &&
                 HygieneSessionWrapper.PolicyTextConfirmation != null &&
                 selectedroomrateentity.RateCategoryID != null)
            {
                if (roomNumber == 0) roomNumber = 1;
                if
                    (HygieneSessionWrapper.PolicyTextConfirmation.ContainsKey
                        (selectedroomrateentity.RateCategoryID + AppConstants.Room + roomNumber))
                {
                    policyText =
                        HygieneSessionWrapper.PolicyTextConfirmation[selectedroomrateentity.RateCategoryID +
                                                              AppConstants.Room + roomNumber] as string;
                }
            }
            string guaranteeText = string.Empty;
            switch (guaranteeInfoEntity.GuranteeType)
            {
                case GuranteeType.CREDITCARD:
                    if (guaranteeInfoEntity.CreditCard != null)
                    {
                        guaranteeText = policyText;
                    }
                    break;
                case GuranteeType.HOLD:
                    guaranteeText = policyText;
                    break;
                case GuranteeType.PREPAID:
                    guaranteeText =
                        policyText.Replace(CommunicationTemplateConstants.CANCELBYDATE,
                                           Utility.GetModifyCancelableDate());
                    break;
                case GuranteeType.REDEMPTION:
                    guaranteeText =
                        WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_RESERVATION_GUARANTEED);
                    break;
                case GuranteeType.PREPAIDINPROGRESS:
                case GuranteeType.PREPAIDSUCCESS:
                    guaranteeText = policyText;
                    break;
                default:
                    break;
            }
            return guaranteeText;
        }

        /// <summary>
        /// Gets the rate cell display object.
        /// </summary>
        /// <param name="roomNumber">The room number.</param>
        /// <param name="selectedRoomAndRateEntity">The selected room and rate entity.</param>
        /// <returns>Base Rate Display</returns>
        public static BaseRateDisplay GetRateCellDisplayObject
            (int roomNumber, SelectedRoomAndRateEntity selectedRoomAndRateEntity)
        {
            BaseRateDisplay rateDisplay = null;
            IList<BaseRoomRateDetails> listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
            if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
            {
                if (roomNumber < listRoomRateDetails.Count && listRoomRateDetails[roomNumber] != null)
                {
                    rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(listRoomRateDetails[roomNumber]);
                }
            }
            return rateDisplay;
        }

        /// <summary>
        /// Calculates the total rate in case of modify flow.
        /// </summary>
        /// <returns>total rate in case of modify flow</returns>
        public static double CalculateTotalRateInCaseOfModifyFlow()
        {
            int totalBooking = 0;
            List<BookingDetailsEntity> listBookingDetails = BookingEngineSessionWrapper.ActiveBookingDetails;
            double totalRate = 0;
            if (listBookingDetails != null && listBookingDetails.Count > 0)
            {
                totalBooking = listBookingDetails.Count;
                for (int count = 0; count < totalBooking; count++)
                {
                    BookingDetailsEntity bookingDetails = listBookingDetails[count];
                    if ((bookingDetails.HotelRoomRate != null) && (bookingDetails.HotelRoomRate.TotalRate != null)
                        && (!string.IsNullOrEmpty(bookingDetails.HotelRoomRate.TotalRate.Rate.ToString())))
                    {
                        totalRate += bookingDetails.HotelRoomRate.TotalRate.Rate;
                    }
                }
            }
            return totalRate;
        }

        /// <summary>
        /// Calculates the total rate in case of modify cancel.
        /// </summary>
        /// <returns>total rate in case of modify cancel</returns>
        public static double CalculateTotalRateInCaseOfModifyCancel()
        {
            int totalBooking = 0;
            List<BookingDetailsEntity> listBookingDetails = BookingEngineSessionWrapper.AllBookingDetails;
            double totalRate = 0;
            if (listBookingDetails != null && listBookingDetails.Count > 0)
            {
                totalBooking = listBookingDetails.Count;
                for (int count = 0; count < totalBooking; count++)
                {
                    BookingDetailsEntity bookingDetails = listBookingDetails[count];

                    if (bookingDetails.HotelSearch.SearchingType == SearchType.REDEMPTION)
                    {
                        if (bookingDetails.HotelRoomRate != null && bookingDetails.HotelRoomRate.Points != null)
                        {
                            totalRate += bookingDetails.HotelRoomRate.Points;
                            break;
                        }
                    }
                    if ((bookingDetails.HotelRoomRate != null) && (bookingDetails.HotelRoomRate.TotalRate != null)
                        && (!string.IsNullOrEmpty(bookingDetails.HotelRoomRate.TotalRate.Rate.ToString())))
                    {
                        totalRate += bookingDetails.HotelRoomRate.TotalRate.Rate;
                    }
                }
            }
            return totalRate;
        }

        /// <summary>
        /// Calculates the total rate in case of cancel flow.
        /// </summary>
        /// <returns>total rate in case of cancel flow.</returns>
        public static double CalculateTotalRateInCaseOfCancelFlow()
        {
            List<CancelDetailsEntity> listBookingDetails = BookingEngineSessionWrapper.AllCancelledDetails;
            double totalRate = 0;
            if (listBookingDetails != null && listBookingDetails.Count > 0)
            {
                foreach (CancelDetailsEntity cnclEntity in listBookingDetails)
                {
                    foreach (BookingDetailsEntity bkngEntity in BookingEngineSessionWrapper.AllBookingDetails)
                    {
                        if
                            (bkngEntity.ReservationNumber == cnclEntity.ReservationNumber &&
                             bkngEntity.LegNumber == cnclEntity.LegNumber)
                        {
                            if (bkngEntity.HotelSearch.SearchingType == SearchType.REDEMPTION)
                            {
                                if (bkngEntity.HotelRoomRate != null && bkngEntity.HotelRoomRate.Points != null)
                                {
                                    totalRate += bkngEntity.HotelRoomRate.Points;
                                    break;
                                }
                            }

                            if ((bkngEntity.HotelRoomRate != null) && (bkngEntity.HotelRoomRate.TotalRate != null)
                                && (!string.IsNullOrEmpty(bkngEntity.HotelRoomRate.TotalRate.Rate.ToString())))
                            {
                                totalRate += bkngEntity.HotelRoomRate.TotalRate.Rate;
                                break;
                            }
                        }
                    }
                }
            }
            return totalRate;
        }

        /// <summary>
        /// Get Current Language
        /// </summary>
        /// <returns>Current Language </returns>
        public static string GetCurrentLanguage()
        {
            string currentLanguage = string.Empty;
            if (Reservation2SessionWrapper.CurrentLanguage != null)
            {
                currentLanguage = Reservation2SessionWrapper.CurrentLanguage;
            }
            else
            {
                currentLanguage = EPiServer.Globalization.ContentLanguage.PreferredCulture.Name;
            }
            return currentLanguage;
        }

        /// <summary>
        /// Get Current Language
        /// </summary>
        /// <returns>Current Language </returns>
        public static string CurrentLanguage
        {
            get
            {
                string[] language = EPiServer.Globalization.ContentLanguage.PreferredCulture.Name.Split(AppConstants.HYPHEN.ToCharArray());
                if (language != null && language.Length > 0)
                {
                    return language[0];
                }

                return string.Empty;
            }
        }

        public static string CurrentLanguageBranch
        {
            get
            {
                return EPiServer.Globalization.ContentLanguage.PreferredCulture.Name;
            }
        }

        #region Methods used in Modify flow to check if its a combo reservation

        /// <summary>
        /// Determines whether this instance [can modify booking].
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if this instance [can modify booking]; otherwise, <c>false</c>.
        /// </returns>
        public static void SetBookingEntityIsModifiableAndIsComboReservation()
        {
            bool canModify = true;
            List<BookingDetailsEntity> activeBookings = BookingEngineSessionWrapper.ActiveBookingDetails;
            bool hasNonCancellableReservation = false;
            System.Collections.Hashtable htblModifyRate = new System.Collections.Hashtable();
            System.Collections.Hashtable htblCancelPolicy = new System.Collections.Hashtable();
            int counter = 0;
            int cntrCombo = 0;
            foreach (BookingDetailsEntity booking in activeBookings)
            {
                if (booking.HotelSearch.SearchingType == SearchType.REDEMPTION)
                {
                    canModify = false;
                    booking.IsCurrentBookingModifiable = false;
                    break;
                }
                if (booking.HotelSearch.CancelPolicy != null)
                {
                    if
                        (booking.HotelSearch != null &&
                         booking.HotelSearch.CancelPolicy == AppConstants.NON_CANCELLABLE_RESERVATION)
                    {
                        htblCancelPolicy.Add(counter, true);
                        booking.IsCurrentBookingModifiable = false;
                    }
                    else
                    {
                        htblCancelPolicy.Add(counter, false);
                        booking.IsCurrentBookingModifiable = true;
                    }
                }
                if (booking.HotelRoomRate != null && booking.HotelSearch != null)
                {
                    canModify = CheckBookingStatus(booking.HotelRoomRate.RatePlanCode, booking.HotelSearch);
                    booking.IsCurrentBookingModifiable = canModify;
                    htblModifyRate.Add(counter, canModify);

                    if (!canModify)
                        cntrCombo++;
                }
                counter++;
            }

            if
                (htblModifyRate != null &&
                 htblModifyRate.Count > 0 &&
                 htblModifyRate.ContainsValue(true))
                canModify = true;
            if
                (htblCancelPolicy != null &&
                 htblCancelPolicy.Count > 0 && htblCancelPolicy.ContainsValue(false))
                canModify = true;

            if ((activeBookings.Count < 1) ||
                (activeBookings.FindAll
                     (delegate(BookingDetailsEntity bd) { return bd.IsCancelledBySytem; }).Count == activeBookings.Count)
                )
            {
                canModify = false;
            }

            if (!(cntrCombo == 0) && (cntrCombo < activeBookings.Count))
                HygieneSessionWrapper.IsComboReservation = true;
            else
                HygieneSessionWrapper.IsComboReservation = false;
        }


        /// <summary>
        /// Checks the booking status.
        /// </summary>
        /// <param name="ratePlanCode">The rate plan code.</param>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>
        /// <returns>True/False</returns>
        public static bool CheckBookingStatus(string ratePlanCode, HotelSearchEntity hotelSearchEntity)
        {
            bool canModify = true;
            if (!string.IsNullOrEmpty(ratePlanCode))
            {
                Rate rate = RoomRateUtil.GetRate(ratePlanCode);
                if (rate != null)
                {
                    if (!rate.IsModifiable)
                    {
                        canModify = false;
                    }
                }
            }
            return canModify;
        }

        /// <summary>
        /// Prepopulates the non modifiable rooms in selected room rates hash table.
        /// </summary>
        public static void PrepopulateNonModifiableRoomsInSelectedRoomRatesHashTable()
        {
            List<BookingDetailsEntity> bookingList = BookingEngineSessionWrapper.ActiveBookingDetails;
            if (bookingList != null && bookingList.Count > 0)
            {
                Hashtable selectedRoomAndRates = new Hashtable();
                int roomIndex = 0;
                foreach (BookingDetailsEntity bookingEntity in bookingList)
                {
                    bool isModifiable = true;
                    if
                        ((bookingEntity.HotelRoomRate != null) &&
                         (bookingEntity.HotelRoomRate.RatePlanCode != null) && (bookingEntity.HotelSearch != null))
                    {
                        isModifiable =
                            Utility.CheckBookingStatus
                                (bookingEntity.HotelRoomRate.RatePlanCode, bookingEntity.HotelSearch);
                    }
                    if
                        ((!isModifiable) &&
                         (bookingEntity.HotelSearch.ListRooms != null) &&
                         (bookingEntity.HotelSearch.ListRooms.Count > 0))
                    {
                        SelectedRoomAndRateEntity selectedRoomRate = new SelectedRoomAndRateEntity();
                        RoomRateEntity roomRate =
                            new RoomRateEntity
                                (bookingEntity.HotelRoomRate.RoomtypeCode, bookingEntity.HotelRoomRate.RatePlanCode);
                        roomRate.BaseRate = bookingEntity.HotelRoomRate.Rate;
                        roomRate.IsSpecialRate = bookingEntity.HotelRoomRate.IsSpecialRate;
                        roomRate.TotalRate = bookingEntity.HotelRoomRate.TotalRate;
                        selectedRoomRate.RoomRates = new List<RoomRateEntity>();
                        selectedRoomRate.RoomRates.Add(roomRate);

                        RateCategory rateCategory =
                            RoomRateUtil.GetRateCategoryByRatePlanCode(bookingEntity.HotelRoomRate.RatePlanCode);
                        RoomCategory roomCategory =
                            RoomRateUtil.GetRoomCategory(bookingEntity.HotelRoomRate.RoomtypeCode);
                        if (Utility.IsBlockCodeBooking)
                        {
                            selectedRoomRate.RateCategoryID = AppConstants.BLOCK_CODE_QUALIFYING_TYPE;
                        }
                        else
                        {
                            selectedRoomRate.RateCategoryID = rateCategory != null ? rateCategory.RateCategoryId : null;
                        }

                        selectedRoomRate.RoomCategoryID = roomCategory != null ? roomCategory.RoomCategoryId : null;
                        selectedRoomRate.SelectedRoomCategoryName =
                            roomCategory != null ? roomCategory.RoomCategoryName : null;

                        selectedRoomAndRates.Add(roomIndex, selectedRoomRate);
                    }
                    roomIndex++;
                }
                if
                    ((HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable == null) ||
                     (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count == 0))
                {
                    HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable = selectedRoomAndRates;
                }
            }
        }

        #endregion

        /// <summary>
        /// Truncates the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="length">The length.</param>
        /// <returns>source</returns>
        public static string TruncateAndAppendDots(string source, int length)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length);
                source = source + "...";
            }
            return source;
        }

        /// <summary>
        /// This is an extension method which converts strings containing security elements like ' [LeftAngularBracket] [RightAngularBracket] & \ to equivalent ascii codes
        /// This function can be used in case any string which is content is used to render on client side as JSON string or script 
        /// parameter. 
        /// Ex: If any string is of format :  what's new this will be converted to what&apos;s new
        /// </summary>
        /// <param name="inputString">input string</param>
        /// <returns>output string with escape characters</returns>
        public static string Escape(string inputString)
        {
            if (string.IsNullOrEmpty(inputString))
            {
                return inputString;
            }
            if (inputString.Contains("'"))
            {
                inputString = inputString.Replace("'", "%27");
            }
            if (inputString.Contains("\""))
            {
                inputString = inputString.Replace("\"", "%22");
            }
            if (inputString.Contains("\r\n"))
            {
                inputString = inputString.Replace("\r\n", "<br/>");
            }
            return inputString;
        }
        #region Setting Tab Order to the controls

        /// <summary>
        ///This functions sets the tab order for the control passed in.
        /// other controls
        /// </summary>
        /// <param name="ctrParent">the parent control</param>
        /// <param name="indexUnit">integer value for the tab indexing</param>
        public static void FetchInnerControlSetTabIndex(System.Web.UI.Control ctrParent, ref Int16 indexUnit)
        {
            for (int ctrlCount = 0; ctrlCount < ctrParent.Controls.Count; ctrlCount++)
            {
                if (ctrParent.Controls[ctrlCount].Controls.Count > 0)
                {
                    FetchInnerControlSetTabIndex(ctrParent.Controls[ctrlCount], ref indexUnit);
                }
                else
                {
                    SetTabIndex(ctrParent.Controls[ctrlCount], ref indexUnit);
                }
            }
        }

        /// <summary>
        /// Set the tabIndex to the controls
        /// </summary>
        /// <param name="ctrToSetTabIndex"></param>
        /// <param name="indexUnit"></param>
        public static void SetTabIndex(System.Web.UI.Control ctrToSetTabIndex, ref Int16 indexUnit)
        {
            switch (ctrToSetTabIndex.GetType().Name.ToUpper())
            {
                case "HTMLINPUTTEXT":
                    {
                        HtmlInputText htmlTxt = (HtmlInputText)ctrToSetTabIndex;
                        htmlTxt.Attributes.Add("tabindex", Convert.ToString(indexUnit));
                        indexUnit++;
                        break;
                    }
                case "HTMLINPUTPASSWORD":
                    {
                        HtmlInputPassword htmlTxtPw = (HtmlInputPassword)ctrToSetTabIndex;
                        htmlTxtPw.Attributes.Add("tabindex", Convert.ToString(indexUnit));
                        indexUnit++;
                        break;
                    }
                case "RADIO":
                    {
                        RadioButton rdoButton = (RadioButton)ctrToSetTabIndex;
                        rdoButton.TabIndex = indexUnit;
                        indexUnit++;
                        break;
                    }
                case "HTMLTEXTAREA":
                    {
                        HtmlTextArea htmlTxtAr = (HtmlTextArea)ctrToSetTabIndex;
                        htmlTxtAr.Attributes.Add("tabindex", Convert.ToString(indexUnit));
                        indexUnit++;
                        break;
                    }
                case "CHECKBOX":
                    {
                        CheckBox chkBox = (CheckBox)ctrToSetTabIndex;
                        chkBox.TabIndex = indexUnit;
                        indexUnit++;
                        break;
                    }
                case "DROPDOWNLIST":
                    {
                        DropDownList ddlList = (DropDownList)ctrToSetTabIndex;
                        ddlList.TabIndex = indexUnit;
                        indexUnit++;
                        break;
                    }
                case "HTMLINPUTRADIOBUTTON":
                    {
                        HtmlInputRadioButton htmlRdo = (HtmlInputRadioButton)ctrToSetTabIndex;
                        htmlRdo.Attributes.Add("tabindex", Convert.ToString(indexUnit));
                        indexUnit++;
                        break;
                    }
                case "HTMLINPUTCHECKBOX":
                    {
                        HtmlInputCheckBox htmlChkBox = (HtmlInputCheckBox)ctrToSetTabIndex;
                        htmlChkBox.Attributes.Add("tabindex", Convert.ToString(indexUnit));
                        indexUnit++;
                        break;
                    }
                case "TEXTBOX":
                    {
                        TextBox txt = (TextBox)ctrToSetTabIndex;
                        txt.TabIndex = indexUnit;
                        indexUnit++;
                        break;
                    }
                case "RADIOBUTTONLIST":
                    {
                        RadioButtonList rdoBtnList = (RadioButtonList)ctrToSetTabIndex;
                        rdoBtnList.TabIndex = indexUnit;
                        indexUnit++;
                        break;
                    }
                case "LINKBUTTON":
                    {
                        LinkButton lnkBtn = (LinkButton)ctrToSetTabIndex;
                        if
                            ((lnkBtn.Text != null) &&
                             (lnkBtn.Text.Trim().ToUpper() ==
                              WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/Search").Trim().ToUpper()
                              || lnkBtn.Text.Trim().ToUpper() ==
                                 WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Continue").Trim().ToUpper
                                     ()
                              || (!string.IsNullOrEmpty(lnkBtn.ID) && lnkBtn.ID.Trim().ToUpper() == "BTNROOMSEARCH")
                              || (!string.IsNullOrEmpty(lnkBtn.ID) && lnkBtn.ID.Trim().ToUpper() == "BTNSEARCH")
                              || lnkBtn.Text.Trim().ToUpper() ==
                              WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/login").Trim().ToUpper()))
                        {
                            lnkBtn.TabIndex = indexUnit;
                            indexUnit++;
                        }
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        #endregion Setting Tab Order to the controls

        /// <summary>
        /// GetFormattedDate
        /// </summary>
        /// <param name="date">DateTime</param>
        /// <returns>FormattedDate</returns>
        public static string GetFormattedDate(DateTime date)
        {
            string formattedDate = DateUtil.DateToDDMMYYYYWords(date);
            if (Utility.GetCurrentLanguage().ToUpper() == LanguageConstant.LANGUAGE_GERMAN)
            {
                string[] words = formattedDate.Split(' ');
                formattedDate = String.Concat(words[0], '.', AppConstants.SPACE, words[1], AppConstants.SPACE, words[2]);
            }
            return formattedDate;
        }

        /// <summary>
        /// GetNameWithTitle
        /// </summary>
        /// <param name="selTitle"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns>Name With Title</returns>
        public static string GetNameWithTitle(string selTitle, string firstName, string lastName)
        {
            System.Globalization.TextInfo tInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
            string nameTitle =
                (selTitle == null || selTitle == AppConstants.TITLE_DEFAULT_VALUE) ? string.Empty : selTitle;
            string fullName = string.Empty;
            if (Utility.GetCurrentLanguage().ToUpper() == LanguageConstant.LANGUAGE_GERMAN)
            {
                fullName =
                    String.Concat(tInfo.ToTitleCase(nameTitle), AppConstants.SPACE,
                                  tInfo.ToTitleCase(firstName), AppConstants.SPACE, tInfo.ToTitleCase(lastName),
                                  AppConstants.SPACE);
            }
            else
            {
                fullName =
                    String.Concat(tInfo.ToTitleCase(firstName), AppConstants.SPACE, tInfo.ToTitleCase(lastName),
                                  AppConstants.SPACE);
            }
            return fullName;
        }

        /// <summary>
        /// GetNameTitle
        /// </summary>
        /// <param name="selTitle"></param>
        /// <returns>Name Title</returns>
        public static string GetNameTitle(string selTitle)
        {
            string nameTitle = string.Empty;
            if (Utility.GetCurrentLanguage().ToUpper() == LanguageConstant.LANGUAGE_GERMAN || EPiServer.Globalization.ContentLanguage.PreferredCulture.Name.ToUpper() == LanguageConstant.LANGUAGE_GERMAN)
            {
                System.Globalization.TextInfo tInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
                nameTitle = (selTitle == null || selTitle ==
                             AppConstants.TITLE_DEFAULT_VALUE)
                                ? string.Empty
                                : selTitle;
                nameTitle = tInfo.ToTitleCase(nameTitle);
            }
            return nameTitle;
        }

        /// <summary>
        /// Added to retrieve if the Page is offered in a fall back lang.
        /// </summary>
        /// <returns>bool</returns>
        public static bool CheckifFallback()
        {
            var page = HttpContext.Current.Handler as PageBase;
            string currentLanguage = EPiServer.Globalization.ContentLanguage.PreferredCulture.Name;
            string correctLanguageStringFromHost = LanguageSelection.GetLanguageFromHost();
            AppLogger.LogInfoMessage(page.CurrentPage.LanguageID);
            AppLogger.LogInfoMessage("The currentLanguage Is: " + currentLanguage);
            AppLogger.LogInfoMessage("The getLanguageFromHost Is: " + correctLanguageStringFromHost);
            if
                (currentLanguage != page.CurrentPage.LanguageID &&
                 currentLanguage == CurrentPageLanguageConstant.LANGAUGE_RUSSIAN)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Sets the total rate and currency code.
        /// </summary>
        public static string GetTotalRateAndCurrencyCode()
        {
            double totalRate = 0;
            string currencyCode = string.Empty;
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
            Hashtable selectedRoomAndRatesHashTable = BookingEngineSessionWrapper.SelectedRoomAndRatesHashTable;
            SelectedRoomAndRateEntity firstSelectedRoomAndRateEntity = null;
            string totalRateValue = string.Empty;
            if (selectedRoomAndRatesHashTable != null)
            {
                foreach (int key in selectedRoomAndRatesHashTable.Keys)
                {
                    SelectedRoomAndRateEntity selectedRoomAndRateEntity = selectedRoomAndRatesHashTable[key] as SelectedRoomAndRateEntity;
                    if (selectedRoomAndRateEntity != null)
                    {
                        if (firstSelectedRoomAndRateEntity == null)
                        {
                            firstSelectedRoomAndRateEntity = selectedRoomAndRatesHashTable[key] as SelectedRoomAndRateEntity;
                        }

                        List<RoomRateEntity> selectedRoomRates = selectedRoomAndRateEntity.RoomRates;
                        if (selectedRoomAndRateEntity.RoomRates != null && selectedRoomAndRateEntity.RoomRates.Count > 0)
                        {
                            RoomRateEntity roomRate = selectedRoomAndRateEntity.RoomRates[0];

                            if (searchCriteria.SearchingType == SearchType.REDEMPTION)
                            {
                                totalRate += roomRate.PointsDetails.PointsRequired;// *search.NoOfNights;
                                currencyCode = string.Empty;
                            }
                            else
                            {
                                totalRate += roomRate.TotalRate.Rate;
                                currencyCode = roomRate.TotalRate.CurrencyCode;
                            }
                            if (searchCriteria.SearchingType == SearchType.VOUCHER)
                            {
                                currencyCode = string.Empty;
                            }
                        }
                    }
                }
                // A dummy object created to invoke Total rate method
                BaseRateDisplay firstRateDisplayObject = Utility.GetRateCellDisplayObject(0, firstSelectedRoomAndRateEntity);
                if (firstRateDisplayObject != null)
                    totalRateValue = firstRateDisplayObject.GetTotalRateDisplayStringForShoppingCart(
                               true, totalRate, currencyCode);
            }
            return totalRateValue;
        }

        public static string SetTotalRateAndCurrencyCodeForModify()
        {
            BookingDetailsEntity bookingDetails = BookingEngineSessionWrapper.BookingDetails;
            string totalRateString = string.Empty;
            double totalRateToAdd = 0;
            RateEntity rateEntity = null;
            string CurrencyCodeValue = string.Empty;

            if (bookingDetails != null)
            {
                HotelSearchEntity hotelSearchEntity = bookingDetails.HotelSearch;
                HotelRoomRateEntity hotelRoomRateEntity = bookingDetails.HotelRoomRate;
                GuestInformationEntity guestInfoEntity = bookingDetails.GuestInformation;
                InformationController informationController = default(InformationController);

                HotelDestination hotelDestination = null;
                if ((hotelSearchEntity != null) && (hotelRoomRateEntity != null) && (guestInfoEntity != null))
                {
                    AvailabilityController availabilityController = new AvailabilityController();
                    hotelDestination = availabilityController.GetHotelDestinationEntity(hotelSearchEntity.SelectedHotelCode);

                    RoomCategory roomCategory = RoomRateUtil.GetRoomCategory(hotelRoomRateEntity.RoomtypeCode);

                    double basePoints = hotelRoomRateEntity.Points;
                    double totalPoints = 0;
                    if (hotelRoomRateEntity.TotalRate != null)
                        totalPoints = hotelRoomRateEntity.TotalRate.Rate;

                    string ratePlanCode = hotelRoomRateEntity.RatePlanCode;
                    string selectedRateCategoryName = string.Empty;

                    if (!string.IsNullOrEmpty(ratePlanCode))
                    {
                        Rate rate = RoomRateUtil.GetRate(ratePlanCode);
                        if (rate != null)
                            selectedRateCategoryName = rate.RateCategoryName;
                    }
                    string hotelCountryCode = hotelSearchEntity.HotelCountryCode;
                    if (string.IsNullOrEmpty(hotelCountryCode))
                    {
                        if (hotelSearchEntity.SearchingType == SearchType.BONUSCHEQUE)
                        {
                            informationController = new InformationController();
                            hotelSearchEntity.HotelCountryCode = informationController.GetHotelCountryCode(hotelSearchEntity.SelectedHotelCode);
                        }
                    }

                    string baseRateString = string.Empty;
                    if (hotelSearchEntity.SearchingType == SearchType.REDEMPTION)
                        baseRateString = GetRoomRateString(selectedRateCategoryName, hotelRoomRateEntity.Rate,
                                                                   hotelRoomRateEntity.Points,
                                                                   hotelSearchEntity.SearchingType,
                                                                   hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                                                   hotelSearchEntity.NoOfNights,
                                                                   hotelSearchEntity.RoomsPerNight);

                    else if ((Utility.IsBlockCodeBooking) && (string.IsNullOrEmpty(hotelRoomRateEntity.RatePlanCode))
                             && (!string.IsNullOrEmpty(hotelRoomRateEntity.RoomtypeCode)))
                        baseRateString = GetRoomRateString(hotelRoomRateEntity.Rate);
                    else
                        baseRateString = GetRoomRateString(selectedRateCategoryName, hotelRoomRateEntity.Rate,
                                                                   basePoints, hotelSearchEntity.SearchingType,
                                                                   hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                                                   AppConstants.PER_NIGHT, AppConstants.PER_ROOM);

                    HotelRoomRateEntity hotelRoomRate = null;
                    if (BookingEngineSessionWrapper.BookingDetails != null)
                    {
                        BookingDetailsEntity bookDetEntity = BookingEngineSessionWrapper.GetLegBookingDetails(BookingEngineSessionWrapper.BookingDetails.LegNumber,
                                                                             BookingEngineSessionWrapper.BookingDetails.ReservationNumber);
                        hotelRoomRate = bookDetEntity.HotelRoomRate;
                        totalRateToAdd += hotelRoomRate.TotalRate.Rate;
                        CurrencyCodeValue = hotelRoomRate.TotalRate.CurrencyCode;
                    }

                    totalRateToAdd = CalculateTotalRateInCaseOfModifyFlow();

                    rateEntity = new RateEntity(totalRateToAdd, CurrencyCodeValue);
                    switch (hotelSearchEntity.SearchingType)
                    {
                        case SearchType.REDEMPTION:
                            totalRateString = GetRoomRateString(null, null, hotelRoomRateEntity.Points,
                                                                        hotelSearchEntity.SearchingType,
                                                                        hotelSearchEntity.ArrivalDate.Year,
                                                                        hotelCountryCode,
                                                                        hotelSearchEntity.NoOfNights,
                                                                        hotelSearchEntity.RoomsPerNight);
                            break;
                        case SearchType.VOUCHER:
                            totalRateString = GetRoomRateString(null, null, totalPoints,
                                                                        hotelSearchEntity.SearchingType,
                                                                        hotelSearchEntity.ArrivalDate.Year,
                                                                        hotelCountryCode,
                                                                        hotelSearchEntity.NoOfNights,
                                                                        hotelSearchEntity.RoomsPerNight);
                            break;
                        default:
                            totalRateString = GetRoomRateString(null, rateEntity, totalPoints,
                                                                        hotelSearchEntity.SearchingType,
                                                                        hotelSearchEntity.ArrivalDate.Year,
                                                                        hotelCountryCode,
                                                                        hotelSearchEntity.NoOfNights,
                                                                        hotelSearchEntity.RoomsPerNight);
                            break;
                    }
                }
            }
            return totalRateString;
        }


        //MERCHANDISING | CR:Have number of gift vouchers printed |Parvathi
        public static string GetNumberofVouchers()
        {
            string currentLanguage = Utility.GetCurrentLanguage();
            int noOfNights = default(int);
            int noOfSelectedRooms = 1;
            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
            {
                noOfNights = SearchCriteriaSessionWrapper.SearchCriteria.NoOfNights;
                noOfSelectedRooms = SearchCriteriaSessionWrapper.SearchCriteria.RoomsPerNight;
            }
            else if (BookingEngineSessionWrapper.BookingDetails != null && BookingEngineSessionWrapper.BookingDetails.HotelSearch != null)
            {
                noOfNights = BookingEngineSessionWrapper.BookingDetails.HotelSearch.NoOfNights;
                noOfSelectedRooms = BookingEngineSessionWrapper.BookingDetails.HotelSearch.RoomsPerNight;
            }

            if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null)
                noOfSelectedRooms = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count;

            string voucher = string.Empty;
            int totalVouchers = noOfSelectedRooms * noOfNights;
            if (totalVouchers > 1)
            {
                voucher = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrates", currentLanguage);
            }
            else
            {
                voucher = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrate", currentLanguage);
            }
            if (BookingEngineSessionWrapper.AllCancelledDetails != null && BookingEngineSessionWrapper.AllCancelledDetails.Count > 0)
                totalVouchers = BookingEngineSessionWrapper.AllCancelledDetails.Count * noOfNights;

            return string.Format("{0} {1}", totalVouchers, voucher);
        }

        public static string GetPrepaidString()
        {
            return WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/prepaid");
        }
        /// <summary>
        /// Checks the configuration in CMS to display or hide tripadvisor for a hotel
        /// </summary>
        /// <returns>True - Display ratings and reviews on hotellanding page/false hide </returns>
        public static bool DisplayTripAdvisor(PageData hotelpage)
        {
            if (hotelpage != null)
            {
                if (hotelpage["TALocationID"] != null && !string.IsNullOrEmpty(hotelpage["TALocationID"].ToString()) &&
                    hotelpage["TripAdvisor"] != null && Convert.ToBoolean(hotelpage["TripAdvisor"]))
                {
                    return true;
                }
            }
            return false;
        }

        public static string GetAddthisScript(string currentPageLangID)
        {
            string vbSharelink = string.Empty;
            if (!string.IsNullOrEmpty(currentPageLangID) && currentPageLangID.ToLower().Equals(CurrentPageLanguageConstant.LANGAUGE_RUSSIAN.ToLower()))
            {
                vbSharelink = "<a class='addthis_button_vk'></a>";
            }

            return string.Format("{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}", vbSharelink,
                                     "<a class='addthis_button_facebook'></a>",
                                     "<a class='addthis_button_twitter'></a>",
                                     "<a class='addthis_button_pinterest_share'></a>",
                                     "<a class='addthis_button_email'></a>",
                                     "<a class='addthis_button_print'></a>",
                                     "<a class='addthis_button_compact'></a>",
                                     "<a class='addthis_counter addthis_bubble_style'></a>");

        }

        public static int GetLoginForDeepLinkPageId()
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings["LoginForDeepLink.PageId"]);
        }


        /// <summary>
        /// Function to implement Zanox Tracking in confirmation page. It embeds a URL in the confirmation page.
        /// </summary>
        public static string ZanoxTracking(HttpCookie SCampaigns, string ReservationNumber, string voucherCode, string price, string ReviewNote)
        {
            string script = String.Empty;
            if (SCampaigns != null)
            {
                string CurrencySymbol = string.Empty;
                string TotalPrice = string.Empty;
                string OrderId = ReservationNumber;
                if (!string.IsNullOrEmpty(price))
                {
                    if (price.Contains(AppConstants.SPACE))
                    {
                        string[] priceStrings = price.Trim().Split(Convert.ToChar(AppConstants.SPACE));
                        CurrencySymbol = priceStrings[1];
                        TotalPrice = priceStrings[0];
                    }
                    else
                    {
                        TotalPrice = Regex.Replace(price, @"[^\d]", "");
                        CurrencySymbol = Regex.Replace(price, @"[^a-zA-Z]", "");
                    }
                }
                HttpCookie ZanoxSCampaigns = SCampaigns;
                voucherCode = !string.IsNullOrEmpty(voucherCode) ? voucherCode : AppConstants.ONE;
                if (ZanoxSCampaigns != null && string.Equals(ZanoxSCampaigns.Value, ConfigurationManager.AppSettings["AffiliatedToZanox"], StringComparison.InvariantCultureIgnoreCase))
                {
                    string zanoxURL = string.Format("{0}&mode=[[1]]&CustomerID=[[{1}]]&OrderID=[[{2}]]&CurrencySymbol=[[{3}]]&TotalPrice=[[{4}]]&PartnerID=[[]]&ReviewNote=[[{5}]]",
                        Convert.ToString(ConfigurationManager.AppSettings["ZanoxUrl"]), voucherCode, OrderId, CurrencySymbol, TotalPrice, ReviewNote);
                    script = "<script type='text/javascript' src='" + HttpUtility.HtmlDecode(zanoxURL) + "'></script>";
                }
            }

            return script;
        }

        /// <summary>
        /// Function to implement Trivago Tracking in confirmation page. It embeds a URL in the confirmation page.
        /// </summary>
        public static string TrivagoTracking(string partnerId, string hotelId, DateTime arrivalDate, DateTime departureDate,
            string price, string reservationNumber, string domain, string bucketId)
        {
            string script = string.Empty;
            string CurrencySymbol = string.Empty;
            string TotalPrice = string.Empty;

            if (!string.IsNullOrEmpty(price))
            {
                if (price.Contains(AppConstants.SPACE))
                {
                    string[] priceStrings = price.Trim().Split(Convert.ToChar(AppConstants.SPACE));
                    CurrencySymbol = priceStrings[1];
                    TotalPrice = priceStrings[0];
                }
                else
                {
                    TotalPrice = Regex.Replace(price, @"[^\d]", "");
                    CurrencySymbol = Regex.Replace(price, @"[^a-zA-Z]", "");
                }
            }
            long unixArrivalTimestamp = GetUnixTimeStamp(arrivalDate);
            long unixDepartureTimestamp = GetUnixTimeStamp(departureDate);

            StringBuilder sb = new StringBuilder();
            sb.Append("{0}&ref={1}");
            if (!string.IsNullOrEmpty(hotelId))
                sb.Append("&hotel={2}");

            sb.Append("&arrival={3}&departure={4}&volume={5}&curency={6}");

            if (!string.IsNullOrEmpty(domain))
                sb.Append("&domain={7}");
            if (!string.IsNullOrEmpty(reservationNumber))
                sb.Append("&booking_id={8}");
            if (!string.IsNullOrEmpty(bucketId))
                sb.Append("&bucket_id={9}");

            //"{0}&ref={1}&hotel={2}&arrival={3}&departure={4}&volume={5}&curency={6}&domain={7}&booking_id={8}&bucket_id={9}&margin={10}",
            string trivagoURL = string.Format(sb.ToString(),
                Convert.ToString(ConfigurationManager.AppSettings["TrivagoTrackingUrl"]), partnerId, hotelId, unixArrivalTimestamp, unixDepartureTimestamp, TotalPrice, CurrencySymbol, domain, reservationNumber, bucketId);
          
            //script = string.Format(HttpUtility.HtmlDecode(Convert.ToString(ConfigurationManager.AppSettings["TrivagoTrackingImgTag"])), trivagoURL);

            return trivagoURL;// script.ToString();
        }
        public static long GetUnixTimeStamp(DateTime dateTime)
        {
            long unixTimestamp = (Int32)(dateTime.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            return unixTimestamp;
        }

        public static void AddMetaInfoForSchemaOrg(PlaceHolder metaContentPlaceHolder, string attributeName, string attributeValue, string metaContent)
        {
            HtmlMeta meta = new HtmlMeta();
            meta.Content = metaContent;
            meta.Attributes.Add(attributeName, attributeValue);
            /*if (metaContentPlaceHolder.Controls.Count > 0)
                metaContentPlaceHolder.Controls.Clear();*/
            metaContentPlaceHolder.Controls.Add(meta);
        }
    }
}