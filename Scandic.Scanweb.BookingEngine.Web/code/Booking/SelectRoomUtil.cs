#region System Namespaces

using System;
using System.Collections;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using System.Linq;

#endregion

    #region Scandic Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// SelectRoomUtil
    /// </summary>
    public class SelectRoomUtil
    { 
        public const string notEnoughPointsMsg =
            "THIS CERTIFICATE OR AWARDS DONOT HAVE ENOUGH POINTS TO BOOK RESERVATION.";
        public const string roomUnavailableForRedeemption = "RESORT IS RESTRICTED OR ROOM IS NOT AVAILABLE.";
        public const string roomUnavailableForRegular = "ROOM_UNAVAILABLE";

        /// <summary>
        /// SetRoomAndRateSelection
        /// </summary>
        /// <param name="roomCategoryID"></param>
        /// <param name="rateCategory"></param>
        /// <param name="roomNumber"></param>
        /// <returns>SelectedRoomAndRateEntity</returns>
        public static SelectedRoomAndRateEntity SetRoomAndRateSelection(string roomCategoryID, string rateCategory,
                                                                        string roomNumber)
        {
            SelectedRoomAndRateEntity selectedRoomRate = new SelectedRoomAndRateEntity();
            if (!string.IsNullOrEmpty(roomCategoryID) && !string.IsNullOrEmpty(rateCategory) &&
                !string.IsNullOrEmpty(roomNumber))
            {
                bool IssortRequired = false;
                int selectedRoomIndex = -1;
                int.TryParse(roomNumber, out selectedRoomIndex);
                RoomCategoryEntity selectedRoomCategory = null;
                IList<BaseRoomRateDetails> listHotelRoomRate = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                if (HotelRoomRateSessionWrapper.ListHotelRoomRate == null && SearchCriteriaSessionWrapper.SearchCriteria != null)
                {
                    AppLogger.LogInfoMessage("Loading SessionWrapper.ListHotelRoomRate");
                    AvailabilityController availController = new AvailabilityController();
                    listHotelRoomRate = availController.GetSelectRateDetails(SearchCriteriaSessionWrapper.SearchCriteria);
                    IssortRequired = true;
                }

                BaseRoomRateDetails roomRateDetails = null;
                if (listHotelRoomRate != null && listHotelRoomRate.Count > 0 && selectedRoomIndex >= 0 &&
                    selectedRoomIndex < listHotelRoomRate.Count && listHotelRoomRate[selectedRoomIndex] != null)
                {
                    roomRateDetails = listHotelRoomRate[selectedRoomIndex];
                }
                BaseRateDisplay rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                if (rateDisplay != null)
                {
                    if (IssortRequired)
                        rateDisplay.RateRoomCategories.Sort(new RoomCategoryEntityComparer());
                    selectedRoomCategory = rateDisplay.RateRoomCategories[int.Parse(roomCategoryID)];
                }
                if (selectedRoomCategory != null)
                {
                    selectedRoomRate.RateCategoryID = rateCategory;
                    selectedRoomRate.RoomCategoryID = selectedRoomCategory.Id;
                    selectedRoomRate.SelectedRoomCategoryEntity = selectedRoomCategory;
                    selectedRoomRate.SelectedRoomCategoryName = selectedRoomCategory.Name;
                }
                BlockCodeRoomRateDisplay blockCodeRateDisplay = rateDisplay as BlockCodeRoomRateDisplay;
                if ((Utility.IsBlockCodeBooking) && (null != blockCodeRateDisplay) &&
                    (blockCodeRateDisplay.IsRateCodeNotAssociated))
                {
                    if (selectedRoomCategory != null && selectedRoomCategory.RateCategories != null
                        && selectedRoomCategory.RateCategories.Count > 0 &&
                        selectedRoomCategory.RateCategories.Keys != null
                        && selectedRoomCategory.RateCategories.Keys.Count > 0)
                    {
                        foreach (string key in selectedRoomCategory.RateCategories.Keys)
                        {
                            selectedRoomRate.RoomRates = selectedRoomCategory.RateCategories[key];
                            break;
                        }
                    }
                }
                else
                {
                    if (selectedRoomCategory != null && selectedRoomCategory.RateCategories != null &&
                        selectedRoomCategory.RateCategories.Count > 0 &&
                        selectedRoomCategory.RateCategories.ContainsKey(rateCategory))
                    {
                        selectedRoomRate.RoomRates = selectedRoomCategory.RateCategories[rateCategory];
                    }
                }

                Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                if (selectedRoomAndRatesHashTable == null)
                {
                    selectedRoomAndRatesHashTable = new Hashtable();
                }

                if (selectedRoomAndRatesHashTable.ContainsKey(selectedRoomIndex))
                {
                    selectedRoomAndRatesHashTable.Remove(selectedRoomIndex);
                    HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                    if (search != null && search.ListRooms != null && search.ListRooms.Count > 0 &&
                        selectedRoomIndex >= 0 && selectedRoomIndex < search.ListRooms.Count &&
                        search.ListRooms[selectedRoomIndex] != null)
                    {
                        if (search.ListRooms[selectedRoomIndex].IsBlocked &&
                            !string.IsNullOrEmpty(search.ListRooms[selectedRoomIndex].RoomBlockReservationNumber))
                        {                      
                            try
                            {
                                WebUtil.UnblockRoom(search.ListRooms[selectedRoomIndex].RoomBlockReservationNumber);
                                search.ListRooms[selectedRoomIndex].RoomBlockReservationNumber = string.Empty;
                                search.ListRooms[selectedRoomIndex].IsBlocked = false;
                            }
                            catch (OWSException  owsException)
                            {
                                if (owsException.ErrCode == "15")
                                {
                                    AppLogger.LogFatalException(owsException, owsException.ErrMessage);
                                }
                            }
                        }
                    }
                }
                selectedRoomAndRatesHashTable.Add(selectedRoomIndex, selectedRoomRate);
                HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable = selectedRoomAndRatesHashTable;
            }
            return selectedRoomRate;
        }

        /// <summary>
        /// BlockRoom
        /// </summary>
        /// <param name="selectedRoomRate"></param>
        /// <param name="roomNumber"></param>
        /// <returns>Blocked room</returns>
        public static string BlockRoom(SelectedRoomAndRateEntity selectedRoomRate, string roomNumber)
        {
            return BlockRoom(selectedRoomRate, roomNumber, false, string.Empty);
        }

        /// <summary>
        /// BlockRoom
        /// </summary>
        /// <param name="selectedRoomRate"></param>
        /// <param name="roomNumber"></param>
        /// <param name="isMobileRequest"></param>
        /// <param name="mobileSourceCode"></param>
        /// <returns>Blocked Room</returns>
        public static string BlockRoom(SelectedRoomAndRateEntity selectedRoomRate, string roomNumber,
                                       bool isMobileRequest, string mobileSourceCode)
        {
            string errorMessage = string.Empty;
            try
            {
                int selectedRoomIndex = -1;
                int.TryParse(roomNumber, out selectedRoomIndex);

                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if (search != null)
                {
                    List<GuestInformationEntity> guestList = new List<GuestInformationEntity>();
                    guestList.Add(CreateTempGuestInfo());

                    string rateCategoryID = string.Empty;
                    if (selectedRoomRate != null)
                    {
                        rateCategoryID = selectedRoomRate.RateCategoryID;

                        if (rateCategoryID != string.Empty)
                        {
                            guestList[0].GuranteeInformation =
                                GetConvenientGuaranteeInfoOnRateOrBlockCategoryOrTime(rateCategoryID, search,
                                                                                      search.CampaignCode);
                        }
                    }

                    if (search.ListRooms != null && search.ListRooms.Count > 0 &&
                        selectedRoomIndex < search.ListRooms.Count)
                    {
                        guestList[0].ChildrensDetails = search.ListRooms[selectedRoomIndex].ChildrenDetails;
                    }

                    if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null)
                    {
                        string membershipID = LoyaltyDetailsSessionWrapper.LoyaltyDetails.MembershipID;
                        if (null != membershipID && string.Empty != membershipID)
                        {
                            guestList[0].MembershipID = long.Parse(membershipID);
                        }
                    }
                    HotelRoomRateEntity hotelRoomRate = GetHotelRoomRate(selectedRoomIndex);

                    if (!isMobileRequest)
                    {
                        CreateReservation(guestList, hotelRoomRate, selectedRoomIndex, true);
                    }
                    else
                    {
                        CreateReservation(guestList, hotelRoomRate, selectedRoomIndex, true, mobileSourceCode);
                    }
                    if (!isMobileRequest)
                    {
                        string relativeUrl = string.Empty;
                        relativeUrl = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE);
                        string urlToRedirect = WebUtil.GetSecureUrl(relativeUrl);
                        AppLogger.LogInfoMessage(urlToRedirect);
                    }
                }
            }
            catch (OWSException owsException)
            {

                if (owsException.Message.Equals(notEnoughPointsMsg, StringComparison.CurrentCultureIgnoreCase))
                {
                    errorMessage =
                        WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/owserror/not_enough_point_msg");
                }
                else if (
                    owsException.Message.Equals(roomUnavailableForRedeemption,
                                                StringComparison.CurrentCultureIgnoreCase) ||
                    owsException.Message.Equals(roomUnavailableForRegular, StringComparison.CurrentCultureIgnoreCase))
                {
                    errorMessage =
                        WebUtil.GetTranslatedText(TranslatedTextConstansts.ROOM_UNAVAILABLE);
                }
                else
                {
                    errorMessage = owsException.Message;
                }

            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }
            return errorMessage;
        }

        /// <summary>
        /// CreateReservation
        /// </summary>
        /// <param name="guestList"></param>
        /// <param name="hotelRoomRate"></param>
        /// <param name="selectedRoomIndex"></param>
        /// <param name="isSessionBooking"></param>
        private static void CreateReservation(List<GuestInformationEntity> guestList, HotelRoomRateEntity hotelRoomRate,
                                              int selectedRoomIndex, bool isSessionBooking)
        {
            CreateReservation(guestList, hotelRoomRate, selectedRoomIndex, isSessionBooking, string.Empty);
        }

        /// <summary>
        /// CreateReservation
        /// </summary>
        /// <param name="guestList"></param>
        /// <param name="hotelRoomRate"></param>
        /// <param name="selectedRoomIndex"></param>
        /// <param name="isSessionBooking"></param>
        /// <param name="mobileSourceCode"></param>
        private static void CreateReservation(List<GuestInformationEntity> guestList, HotelRoomRateEntity hotelRoomRate,
                                              int selectedRoomIndex, bool isSessionBooking, string mobileSourceCode)
        {
            string createdNameID = string.Empty;
            string sourceCode = string.IsNullOrEmpty(mobileSourceCode)
                                    ? WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SourceCode")
                                    : mobileSourceCode;
            ReservationController reservationController = new ReservationController();
            SortedList<string, GuestInformationEntity> reservationMap = new SortedList<string, GuestInformationEntity>();

            int totalGuests = guestList.Count;
            SortedList<string, string> legReservationMap = null;
            for (int guestCount = 0; guestCount < totalGuests; guestCount++)
            {
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if (search != null && search.ListRooms != null && search.ListRooms.Count > 0 &&
                    guestCount < search.ListRooms.Count)
                {
                    HotelSearchRoomEntity roomSearch = search.ListRooms[selectedRoomIndex];
                    legReservationMap = reservationController.CreateBooking(search, roomSearch, hotelRoomRate,
                                                                            guestList[guestCount], string.Empty,
                                                                            sourceCode, out createdNameID,
                                                                            isSessionBooking, HygieneSessionWrapper.PartnerID);
                }

                if (legReservationMap != null)
                {
                    foreach (string key in legReservationMap.Keys)
                    {
                        SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[selectedRoomIndex].RoomBlockReservationNumber =
                            legReservationMap[key];
                        SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[selectedRoomIndex].IsBlocked = true;
                        break;
                    }
                }
                if (createdNameID != string.Empty)
                {
                    SearchCriteriaSessionWrapper.SearchCriteria.NameID = createdNameID;
                }

                if ((SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.BONUSCHEQUE) &&
                    (!guestList[guestCount].IsValidDNumber))
                {
                    SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = null;
                }
            }
        }

        /// <summary>
        /// CreateTempGuestInfo
        /// </summary>
        /// <returns>GuestInformationEntity</returns>
        private static GuestInformationEntity CreateTempGuestInfo()
        {
            GuestInformationEntity guestInfoEntity = new GuestInformationEntity();
            guestInfoEntity.Title = SessionBookingConstants.SESSION_BOOKING_NAME_TITLE;
            guestInfoEntity.FirstName = SessionBookingConstants.SESSION_BOOKING_FIRST_NAME;
            guestInfoEntity.LastName = SessionBookingConstants.SESSION_BOOKING_LAST_NAME;
            guestInfoEntity.AddressType = AppConstants.ADDRESS_TYPE_HOME;
            guestInfoEntity.City = SessionBookingConstants.SESSION_BOOKING_CITY;
            guestInfoEntity.Country = SessionBookingConstants.SESSION_BOOKING_COUNTRY;
            return guestInfoEntity;
        }

        /// <summary>
        /// GetConvenientGuaranteeInfoOnRateOrBlockCategoryOrTime
        /// </summary>
        /// <param name="rateCategoryID"></param>
        /// <param name="search"></param>
        /// <param name="campaignCode"></param>
        /// <returns>GuranteeInformationEntity</returns>
        private static GuranteeInformationEntity GetConvenientGuaranteeInfoOnRateOrBlockCategoryOrTime(
            string rateCategoryID, HotelSearchEntity search, string campaignCode)
        {
            RateCategory rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(rateCategoryID);
            GuranteeInformationEntity guranteeInfoEntity = new GuranteeInformationEntity();

            bool noRateCodeButDisplayGuarantee;
            string creditCardGuranteeType;
            if (IsTruelyPrepaid(rateCategoryID, campaignCode, out noRateCodeButDisplayGuarantee,
                                out creditCardGuranteeType))
            {
                Block block = ContentDataAccess.GetBlockCodePages(campaignCode);
                if ((null != block) && (!string.IsNullOrEmpty(block.GuranteeType)))
                {
                    guranteeInfoEntity.GuranteeType = GuranteeType.PREPAID;
                    guranteeInfoEntity.ChannelGuranteeCode = block.GuranteeType;
                }
            }
            else if (rateCategory != null && rateCategory.HoldGuranteeAvailable)
            {
                guranteeInfoEntity.GuranteeType = GuranteeType.HOLD;
                guranteeInfoEntity.ChannelGuranteeCode = AppConstants.GURANTEE_TYPE_HOLD;
            }
            else
            {
                guranteeInfoEntity.GuranteeType = GuranteeType.CREDITCARD;

                if (noRateCodeButDisplayGuarantee)
                {
                    guranteeInfoEntity.ChannelGuranteeCode = creditCardGuranteeType;
                }
                else
                {
                    guranteeInfoEntity.ChannelGuranteeCode = rateCategory.CreditCardGuranteeType;
                }

                guranteeInfoEntity.CreditCard =
                    new CreditCardEntity(SessionBookingConstants.SESSION_BOOKING_LAST_NAME,
                                         SessionBookingConstants.SESSION_CREDITCARD_TYPE,
                                         SessionBookingConstants.SESSION_CREDITCARD_NUM,
                                         new ExpiryDateEntity(SessionBookingConstants.SESSION_CREDITCARD_EXP_MONTH,
                                                              SessionBookingConstants.SESSION_CREDITCARD_EXP_YEAR));
            }
  
            if (search.SearchingType == SearchType.REDEMPTION &&
                rateCategory.RateCategoryId.ToUpper().Equals(SearchType.REDEMPTION.ToString()))
            {
                guranteeInfoEntity.GuranteeType = GuranteeType.REDEMPTION;
                guranteeInfoEntity.ChannelGuranteeCode = rateCategory.CreditCardGuranteeType;
            }

            return guranteeInfoEntity;
        }

        /// <summary>
        /// GetHotelRoomRate
        /// </summary>
        /// <param name="selectedRoomIndex"></param>
        /// <returns>HotelRoomRateEntity</returns>
        private static HotelRoomRateEntity GetHotelRoomRate(int selectedRoomIndex)
        {
            HotelRoomRateEntity hotelRoomRate = null;

            if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null)
            {
                Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                SelectedRoomAndRateEntity selectedRoomAndRateEntity = null;

                if (selectedRoomAndRatesHashTable.ContainsKey(selectedRoomIndex))
                {
                    selectedRoomAndRateEntity =
                        selectedRoomAndRatesHashTable[selectedRoomIndex] as SelectedRoomAndRateEntity;
                }

                if (selectedRoomAndRateEntity != null)
                {
                    List<RoomRateEntity> selectedRoomRates = selectedRoomAndRateEntity.RoomRates;

                    hotelRoomRate = new HotelRoomRateEntity();
                    foreach (RoomRateEntity roomRate in selectedRoomRates)
                    {
                        if (roomRate != null)
                        {
                            hotelRoomRate.RatePlanCode = roomRate.RatePlanCode;
                            hotelRoomRate.IsSpecialRate = roomRate.IsSpecialRate;
                            hotelRoomRate.RoomtypeCode = roomRate.RoomTypeCode;
                            hotelRoomRate.Rate = roomRate.BaseRate;
                            hotelRoomRate.TotalRate = roomRate.TotalRate;
                            if (null != roomRate.PointsDetails)
                            {
                                hotelRoomRate.AwardType = roomRate.PointsDetails.AwardType;
                                hotelRoomRate.Points = roomRate.PointsDetails.PointsRequired;
                            }
                            break;
                        }
                    }
                }
            }
            return hotelRoomRate;
        }

        /// <summary>
        /// IsTruelyPrepaid
        /// </summary>
        /// <param name="rateCategoryId"></param>
        /// <param name="blockCode"></param>
        /// <param name="noRateCodeButDisplayGuarantee"></param>
        /// <param name="creditCardGuranteeType"></param>
        /// <returns>True/False</returns>
        private static bool IsTruelyPrepaid(string rateCategoryId, string blockCode,
                                            out bool noRateCodeButDisplayGuarantee, out string creditCardGuranteeType)
        {
            bool returnVaue = false;
            noRateCodeButDisplayGuarantee = false;
            creditCardGuranteeType = string.Empty;
            if (Utility.IsBlockCodeBooking)
            {
                if (rateCategoryId == AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
                {
                    Block blockPage = ContentDataAccess.GetBlockCodePages(blockCode);
                    if (null != blockPage)
                    {
                        if (blockPage.CaptureGuarantee)
                        {
                            noRateCodeButDisplayGuarantee = true;
                            if (!string.IsNullOrEmpty(blockPage.GuranteeType))
                            {
                                creditCardGuranteeType = blockPage.GuranteeType;
                            }
                        }
                        else
                        {
                            returnVaue = true;
                        }
                    }
                }
            }
            return returnVaue;
        }

        public static IList<BaseRoomRateDetails> MultiRoomAvailSearchOrPrefetch(HotelSearchEntity hotelSearch)
        {
            IList<BaseRoomRateDetails> listRoomRateDetails = null;
            AvailabilityController availController = new AvailabilityController();

            if (hotelSearch != null)
            {
                listRoomRateDetails = availController.GetSelectRateDetails(hotelSearch);
            }
            return listRoomRateDetails;
        }

        public static IList<BaseRoomRateDetails> MultiRoomUpdateRoomModifiableFlags(HotelSearchEntity hotelSearch)
        {
            IList<BaseRoomRateDetails> listRoomRateDetails = null;
            if (hotelSearch != null)
            {
                listRoomRateDetails = MultiRoomAvailSearchOrPrefetch(hotelSearch);
            }
            if (listRoomRateDetails != null)
            {
                for (int i = 0; i < hotelSearch.ListRooms.Count; i++)
                {
                    listRoomRateDetails[i].IsRoomModifiable = hotelSearch.ListRooms[i].IsRoomModifiable;
                }
            }

            return listRoomRateDetails;
        }

        /// <summary>
        /// method to merge the Promo and Public responses
        /// </summary>
        /// <param name="publicRateDetails">object containing public rates</param>
        /// <param name="promoRateDetails">object containing promo rates</param>
        /// <returns>merge response of promo and public rates</returns>
        public static IList<BaseRoomRateDetails> MergePublicRatesWithPromotionalRates(IList<BaseRoomRateDetails> publicRateDetails, IList<BaseRoomRateDetails> promoRateDetails)
        {
            IList<BaseRoomRateDetails> publicPromoRateDetails = publicRateDetails;
            if (publicPromoRateDetails != null && publicPromoRateDetails.Count > 0)
            {
                for (int i = 0; i < publicPromoRateDetails.Count; i++)
                {
                    if (publicPromoRateDetails[i].RateCategories != null && publicPromoRateDetails[i].RateCategories.Count > 0 &&
                        publicPromoRateDetails[i].RateRoomCategories != null && publicPromoRateDetails[i].RateRoomCategories.Count > 0 &&
                        publicPromoRateDetails[i].RoomTypes != null && publicPromoRateDetails[i].RoomTypes.Count > 0)
                    {
                        List<RateCategory> lstRCat = null;
                        List<RoomCategoryEntity> lstRCatEnt = null;
                        List<RoomTypeEntity> lstRTypeEnt = null;

                        if (promoRateDetails != null && promoRateDetails.Count > 0 &&
                       promoRateDetails[i].RateCategories != null && promoRateDetails[i].RateCategories.Count > 0)
                        {
                            //Assigning Column number 1 explicitly to promo rate column to make it appear on 1st position
                            //promoRateDetails[i].RateCategories.ForEach(o => o.CoulmnNumber = 1);
                            lstRCat = promoRateDetails[i].RateCategories;
    }
                        else
                            lstRCat = new List<RateCategory>();

                        if (publicPromoRateDetails[i].RateCategories != null && publicPromoRateDetails[i].RateCategories.Count > 0)
                        {
                            foreach (var item in publicPromoRateDetails[i].RateCategories)
                            {
                                if (!IsRateCategoryExists(lstRCat, item))
                                {
                                    lstRCat.Add(item);
                                }
                            }
                            publicPromoRateDetails[i].RateCategories = lstRCat;
                        }

                        if (promoRateDetails != null && promoRateDetails.Count > 0 &&
                       promoRateDetails[i].RateRoomCategories != null && promoRateDetails[i].RateRoomCategories.Count > 0)
                            lstRCatEnt = promoRateDetails[i].RateRoomCategories;
                        else
                            lstRCatEnt = new List<RoomCategoryEntity>();
                        if (publicPromoRateDetails[i].RateRoomCategories != null && publicPromoRateDetails[i].RateRoomCategories.Count > 0)
                        {
                            foreach (var item in publicRateDetails[i].RateRoomCategories)
                            {
                                if (!IsRateRoomCategoryExists(lstRCatEnt, item))
                                {
                                    lstRCatEnt.Add(item);
                                }
                                else
                                {
                                    var existingRateRoomsList = lstRCatEnt.Where(lr => lr.Id == item.Id).ToList() as List<RoomCategoryEntity> ;
                                    if (existingRateRoomsList != null && existingRateRoomsList.Count > 0)
                                    {
                                        foreach (var rc in item.RateCategories)
                                        {
                                            foreach (var existingRateRoom in existingRateRoomsList)
                                            {
                                                if (!existingRateRoom.RateCategories.ContainsKey(rc.Key))
                                                    existingRateRoom.RateCategories.Add(rc.Key, rc.Value);
                                            }
                                        }
                                    }
                                }
                            }
                            publicPromoRateDetails[i].RateRoomCategories = lstRCatEnt;
                        }

                        if (promoRateDetails != null && promoRateDetails.Count > 0 &&
                       promoRateDetails[i].RoomTypes != null && promoRateDetails[i].RoomTypes.Count > 0)
                            lstRTypeEnt = promoRateDetails[i].RoomTypes;
                        else
                            lstRTypeEnt = new List<RoomTypeEntity>();

                        if (publicPromoRateDetails[i].RoomTypes != null && publicPromoRateDetails[i].RoomTypes.Count > 0)
                        {
                            foreach (var item in publicPromoRateDetails[i].RoomTypes)
                            {
                                if (!IsRoomTypeExists(lstRTypeEnt, item))
                                {
                                    lstRTypeEnt.Add(item);
                                }
                            }
                            publicPromoRateDetails[i].RoomTypes = lstRTypeEnt;
                        }
                    }
                }
            }
            return publicPromoRateDetails;
        }

        private static bool IsRateCategoryExists(List<RateCategory> lstRTypeEnt, RateCategory item)
        {
            return lstRTypeEnt.Where(rt => rt.RateCategoryId == item.RateCategoryId).Count() > 0 ? true : false;
        }

        private static bool IsRoomTypeExists(List<RoomTypeEntity> lstRTypeEnt, RoomTypeEntity item)
        {
            return lstRTypeEnt.Where(rt => rt.Code == item.Code).Count() > 0 ? true : false;
        }

        private static bool IsRateRoomCategoryExists(List<RoomCategoryEntity> roomCategoryList, RoomCategoryEntity item)
        {
            return roomCategoryList.Where(rt => rt.Id == item.Id).Count() > 0 ? true : false;
        }

    }
}