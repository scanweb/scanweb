﻿using System.Collections.Generic;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Core.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Web.code.Interface;
using System.Configuration;
using System;

namespace Scandic.Scanweb.BookingEngine.Web.code.Booking
{
    public class TripAdvisorRatingFeedManager
    {

        public static TripAdvisorPlaceListEntity LoadTripAdvisorRatings()
        {
            TripAdvisorPlaceListEntity tripAdvisorListEntity = new TripAdvisorPlaceListEntity();
            List<TripAdvisorPlaceEntity> tripAdvisorEntity = null;

            tripAdvisorEntity = ScanwebCacheManager.Instance.LookInCache<List<TripAdvisorPlaceEntity>>
                               (Convert.ToString(ConfigurationManager.AppSettings[AppConstants.TripAdvisorRatingCacheKey]));

            if (tripAdvisorEntity != null && tripAdvisorEntity.Count > 0)
            {
                tripAdvisorListEntity.TAPlaces = tripAdvisorEntity;
            }
            else
            {
                ITripAdvisorRatingProcessor TAProcessor = new TripAdvisorRatingProcessor();
                tripAdvisorListEntity = TAProcessor.GetTripAdvisorRating();
            }
            return tripAdvisorListEntity;
        }
    }
}
