﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ImageStoreNET.Developer;
using ImageStoreNET.Developer.Core;

namespace Scandic.Scanweb.BookingEngine.Web.code.Booking
{
    public class ImageAltText
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public ImageAltText()
        {
        }

        /// <summary>
        /// Gets alttext in the requested language
        /// </summary>
        /// <param name="language">language prefix</param>
        /// <param name="image">ImageVault image property as string</param>
        /// <returns>Alternate Text</returns>
        public string GetAltText(string language, string image)
        {
            string rightAltText = "Alttext_" + language.ToUpper();
            IVUrlBuilder ub = IVUrlBuilder.ParseUrl(image);

            try
            {
                var altText = WebUtil.GetImageAltText(image);
                if (!string.IsNullOrEmpty(altText))
                {
                    return altText;
                } 
                IVFileData ivFile =
                    IVDataFactory.GetFile(ub.Id, ImageStoreNET.Developer.Security.IVAccessLevel.IgnoreAccess);
                foreach (IVMetaData meta in ivFile.MetaData)
                {
                    if (meta.Name == rightAltText)
                    {
                        if (meta.Value != "")
                            return HttpUtility.HtmlEncode(meta.Value);
                    }
                }
                return HttpUtility.HtmlEncode(ivFile.Title);
            }
            catch
            {
                return String.Empty;
            }
        }
    }
}
