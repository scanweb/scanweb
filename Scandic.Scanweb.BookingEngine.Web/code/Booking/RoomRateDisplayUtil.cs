using System;
using System.Collections.Generic;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// The utility class containing the various methods required for
    /// processing the room categories, room types, rate categories and rate plans
    /// </summary>
    public class RoomRateDisplayUtil
    {
        /// <summary>
        /// Get the Rate Display 
        /// </summary>
        /// <param name="hotelDetails">
        /// IHotelDetails
        /// </param>
        /// <returns>
        /// BaseRateDisplay
        /// </returns>
        public static BaseRateDisplay GetRateDisplayObject(BaseRoomRateDetails roomRateDetails)
        {
            BaseRateDisplay rateDisplay = null;
            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
            {
               SearchType searchType = SearchCriteriaSessionWrapper.SearchCriteria.SearchingType;
               switch (searchType)
                {
                    case SearchType.REGULAR:
                        {
                            if (null != SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode)
                            {
                                rateDisplay = new PromotionRoomRateDisplay(roomRateDetails as PromotionRoomRateDetails);
                            }
                            else
                            {
                                rateDisplay = new RegularRoomRateDisplay(roomRateDetails as RegularRoomRateDetails);
                            }
                            break;
                        }
                    case SearchType.VOUCHER:
                        {
                            VoucherRoomRateDetails voucherRoomRateDetails =
                                roomRateDetails as VoucherRoomRateDetails;
                            if (voucherRoomRateDetails.HasPromtionRates)
                                rateDisplay = new VoucherRoomRateDisplay(roomRateDetails as VoucherRoomRateDetails);
                            else
                                rateDisplay = new RegularRoomRateDisplay(roomRateDetails as RegularRoomRateDetails);
                            break;
                        }
                    case SearchType.CORPORATE:
                        {
                            if (Utility.IsBlockCodeBooking)
                            {
                                rateDisplay = new BlockCodeRoomRateDisplay(roomRateDetails as BlockCodeRoomRateDetails);
                            }
                            else
                            {
                                rateDisplay = new NegotiatedRoomRateDisplay(roomRateDetails as NegotiatedRoomRateDetails);
                            }
                            break;
                        }
                    case SearchType.BONUSCHEQUE:
                        {
                            rateDisplay = new BonusChequeRoomRateDisplay(roomRateDetails as BonusChequeRoomRateDetails);
                            break;
                        }
                    case SearchType.REDEMPTION:
                        {
                            rateDisplay = new RedemptionRoomRateDisplay(roomRateDetails as RedemptionRoomRateDetails);
                            break;
                        }
                    default:
                        break;
                }
            }
            return rateDisplay;
        }

        /// <summary>
        /// Check Room Type Availability
        /// </summary>
        /// <param name="roomCategoryList"></param>
        /// <returns>Availablility</returns>
        public static bool RoomTypeAvailable(List<RoomCategoryEntity> roomCategoryList, string hotelCode)
        {
            bool result = false;
            string[] defaultRoomTypeIds = GetDefaultRoomTypeIds(hotelCode);

            Dictionary<string, bool> dictDefaultRoomTypes = new Dictionary<string, bool>();

            if (defaultRoomTypeIds != null)
            {
                foreach (string eachRoomType in defaultRoomTypeIds)
                {
                    if (!dictDefaultRoomTypes.ContainsKey(eachRoomType))
                        dictDefaultRoomTypes.Add(eachRoomType, false);
                }
            }
            int totalCategoryCount = roomCategoryList.Count;
            for (int categoryCount = 0; categoryCount < totalCategoryCount; categoryCount++)
            {
                if (dictDefaultRoomTypes.ContainsKey(roomCategoryList[categoryCount].Id))
                {
                    dictDefaultRoomTypes[roomCategoryList[categoryCount].Id] = true;
                }
            }

            foreach (string key in dictDefaultRoomTypes.Keys)
            {
                if (dictDefaultRoomTypes[key] == true)
                {
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// Method to get the list of default room categories configured in CMS.
        /// </summary>
        /// <param name="hotelCode">Hotel Code to be passed to get the default room categories against it.</param>
        /// <returns>Returns the list of default room categories</returns>
        private static List<string> GetDefaultRoomCategoriesFromCMS(string hotelCode)
        {
            List<string> defaultRoomTypes = new List<string>();

            AvailabilityController availController = new AvailabilityController();
            HotelDestination hotelDestination = availController.GetHotelDestinationEntity(hotelCode);

            if (hotelDestination != null)
            {
                foreach (HotelRoomDescription hotelRoomDesc in hotelDestination.HotelRoomDescriptions)
                {
                    if (hotelRoomDesc.IsDefault)
                    {
                        defaultRoomTypes.Add(hotelRoomDesc.HotelRoomCategory.RoomCategoryId);
                    }
                }
            }

            return defaultRoomTypes;
        }

        /// <summary>
        /// Fetches and returns default room categories for the hotel.
        /// </summary>
        /// <param name="hotelCode"></param>
        /// <returns>Default room types</returns>
        public static string[] GetDefaultRoomTypeIds(string hotelCode)
        {
            List<string> defaultRoomTypesInCMS;
            string[] defaultRoomTypes = null;

            defaultRoomTypesInCMS = GetDefaultRoomCategoriesFromCMS(hotelCode);

            if (defaultRoomTypesInCMS.Count > 0)
            {
                defaultRoomTypes = new string[defaultRoomTypesInCMS.Count];

                for (int ctr = 0; ctr < defaultRoomTypesInCMS.Count; ctr++)
                {
                    defaultRoomTypes[ctr] = defaultRoomTypesInCMS[ctr];
                }
            }

            return defaultRoomTypes;
        }

        /// <summary>
        /// Has Non Default Room Available
        /// </summary>
        /// <param name="roomCategoryList"></param>
        /// <returns>Non default room availability</returns>
        public static bool HasNonDefaultRoomAvailable(List<RoomCategoryEntity> roomCategoryList, string hotelCode)
        {
            string[] defaultRoomTypeIds = GetDefaultRoomTypeIds(hotelCode);
            if (defaultRoomTypeIds != null)
            {
                for (int ctr = 0; ctr < defaultRoomTypeIds.Length; ctr++)
                {
                    if (HasMulitpleRoomCategories(defaultRoomTypeIds[ctr], roomCategoryList))
                    {
                        return true;
                    }
                }
            }

            foreach (RoomCategoryEntity roomCateogry in roomCategoryList)
            {
                if (false == Core.StringUtil.IsStringInArray(defaultRoomTypeIds, roomCateogry.Id))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Check if multiple room categories of <code>roomCategory</code> are existing in the 
        /// <code>roomCategoryList</code> by checking the <code>RoomCategoryEntity.Id</code> value
        /// with the values in the list.
        /// If more than one category with id that of roomCategory is existing true will be returned else false is returned
        /// </summary>
        /// <param name="roomCategoryId">Id of the RoomCategory which need to be checked against with</param>
        /// <param name="roomCategoryList">The list of RoomCategoryEntity in which needs to checked against</param>
        /// <returns>whether it has multiple room categories or not</returns>
        public static bool HasMulitpleRoomCategories(string roomCategoryId, List<RoomCategoryEntity> roomCategoryList)
        {
            int noOfRoomCategories = 0;

            foreach (RoomCategoryEntity roomCategoryEntity in roomCategoryList)
            {
                if (roomCategoryId == roomCategoryEntity.Id)
                {
                    noOfRoomCategories++;
                }
            }

            return (noOfRoomCategories > 1);
        }

        /// <summary>
        /// It returns NoOfNights of the hotel search criteria.
        /// </summary>
        /// <returns> No of Nights</returns>
        public static int GetNoOfNights()
        {
            int noOfNights = 0;

            try
            {
                if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                {
                    noOfNights = SearchCriteriaSessionWrapper.SearchCriteria.NoOfNights;
                }
                else
                {                    
                    throw new Exception("Unable to fetch NoOfNigths from SessionWrapper.SearchCriteria.");
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Unable to fetch NoOfNigths from SessionWrapper.SearchCriteria.");
                throw new Exception("Unable to fetch NoOfNigths from SessionWrapper.SearchCriteria.");
            }

            return noOfNights;
        }

        /// <summary>
        /// It returns Number of Room of the hotel search criteria.
        /// </summary>
        /// <returns>No of Rooms</returns>
        public static int GetNoOfRooms()
        {
            int noOfRooms = 0;

            try
            {
                if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                {
                    noOfRooms = SearchCriteriaSessionWrapper.SearchCriteria.RoomsPerNight;
                }
                else
                {
                    throw new Exception("Unable to fetch Number of Rooms from SessionWrapper.SearchCriteria.");
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Unable to fetch Number of Rooms from SessionWrapper.SearchCriteria.");
                throw new Exception("Unable to fetch Number of Rooms from SessionWrapper.SearchCriteria.");
            }
            return noOfRooms;
        }

        #region Alternate Hotels

        /// <summary>
        /// Display Alternate Hotels
        /// </summary>
        /// <param name="hotelDestination">
        /// HotelDestination
        /// </param>
        /// <returns>Alternate hotel url</returns>
        public static string GetAlternateHotels(HotelDestination hotelDestination)
        {
            string redirectUrl = string.Empty;
            List<AlternativeHotelReference> alternateHotelsList = GetAlternateHotelsList(hotelDestination);
            if ((alternateHotelsList != null))
            {
                redirectUrl = SearchAlternateHotels(alternateHotelsList);
            }
            else
            {
                AlternateHotelsSessionWrapper.DisplayNoHotelsAvailable = true;
                TotalRegionalAvailableHotelsSessionWrapper.TotalRegionalAvailableHotels = 0;
                TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels = 0;
                redirectUrl = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_HOTEL_PAGE);
            }
            return redirectUrl;
        }

        /// <summary>
        /// Get the List of alternate hotels 
        /// </summary>
        /// <param name="hotelDestination">Hotel Destination</param>
        /// <returns>
        /// List of AlternativeHotelReference
        /// </returns>
        private static List<AlternativeHotelReference> GetAlternateHotelsList(HotelDestination hotelDestination)
        {
            List<AlternativeHotelReference> alternateHotelsList =
                hotelDestination.AlternativeHotels;
            return alternateHotelsList;
        }

        /// <summary>
        /// Search for Alternate Hotels
        /// </summary>
        /// <param name="alternateHotelsList">
        /// List of AlternateHotel References
        /// </param>
        /// <returns>Alternate Hotels</returns>
        private static string SearchAlternateHotels(List<AlternativeHotelReference> alternateHotelsList)
        {
            string redirectUrl = string.Empty;
            bool isPromoFlow = false;
            if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode != null
                && SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REGULAR)
                isPromoFlow = true;
            
            if(!isPromoFlow)
                CleanSessionSearchResultsSessionWrapper.CleanSessionSearchResults();

            AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone = false;
            AvailabilityController availabilityController = new AvailabilityController();
            availabilityController.AlternateSearchHotels(alternateHotelsList);

            List<IHotelDetails> hotelDetails =
                HotelResultsSessionWrapper.GetHotelResults(HttpContext.Current.Session) as List<IHotelDetails>;

            //Merchandising:R3:Display ordinary rates for unavailable promo rates 
            if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode != null
                && SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REGULAR && HotelResultsSessionWrapper.HotelDetails != null)
            {
                hotelDetails.Add(HotelResultsSessionWrapper.HotelDetails);
            }

            if ((hotelDetails != null) && (hotelDetails.Count > 0))
            {
                TotalRegionalAvailableHotelsSessionWrapper.TotalRegionalAvailableHotels = hotelDetails.Count;
                TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels = hotelDetails.Count;
                AlternateHotelsSessionWrapper.DisplayAlternateHotelsAvailable = true;
                hotelDetails.Sort(new HotelNameComparer());
            }
            else
            {
                TotalRegionalAvailableHotelsSessionWrapper.TotalRegionalAvailableHotels = 0;
                TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels = 0;
            }
            redirectUrl = GlobalUtil.GetUrlToPage("ReservationSelectHotelPage");

            return redirectUrl;
        }

        #endregion Alternate Hotels
    }
}