﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Globalization;
using Scandic.Scanweb.Core;
using System.Configuration;
using System.Net;
using Scandic.Scanweb.Core.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Web.code.Interface;
using EPiServer.Globalization;

namespace Scandic.Scanweb.BookingEngine.Web.code.Booking
{
    public class ExchangeRateManager
    {
        /// <summary>
        /// Get all culture specific room rate currency codes.
        /// </summary>
        public string GetExchangeRateString(string stringFormat, string fromCurrency, string rateValue)
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings[AppConstants.EnableAlternateCurrencyDisplay]) == true)
            {
                string toCurrency = string.Empty;
                string localCurrencies = Convert.ToString(ConfigurationManager.AppSettings[AppConstants.LocaleCurrencyMapping]);
                Dictionary<string, string> localeCurrencyList = new Dictionary<string, string>();

                string currentLanguage = EPiServer.Globalization.ContentLanguage.PreferredCulture.Name;
                string correctLanguageStringFromHost = LanguageSelection.GetLanguageFromHost();

                string[] mappings = { string.Empty };
                //IFormatProvider cultureInfo = new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
                string currentLocale = Utility.CurrentLanguage;
                currentLocale = currentLocale == null ? "en" : currentLocale;
                string[] localeCurrencyMapping = localCurrencies.Split(Convert.ToChar(AppConstants.COMMA));
                foreach (string mapping in localeCurrencyMapping)
                {
                    mappings = mapping.Split(Convert.ToChar(AppConstants.HYPHEN));
                    localeCurrencyList.Add(mappings[0], mappings[1]);
                }
                localeCurrencyList.TryGetValue(currentLocale, out toCurrency);
                return GetCalculatedExchangeRate(stringFormat, fromCurrency, toCurrency, rateValue, localeCurrencyList);
            }
            else
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// Get alternate currency if Hotel rate currency rate is not equal to locale specific currency.
        /// </summary>
        /// <param name="stringFormat">Formater for diplay of alternate currency</param>
        /// <param name="fromCurrency">Currency to be converted to target currency</param>
        /// <param name="rateValue">Currency amount to be converted to target currency amount</param>
        public string GetCalculatedExchangeRate(string stringFormat, string fromCurrency, string toCurrency, string rateValue, Dictionary<string, string> allLocaleCurrencyList)
        {
            string exchangeRateProvider = Convert.ToString(ConfigurationManager.AppSettings[AppConstants.ExchangeRatesProvider]);
            string convertedRateString = string.Empty;
            ExchangeRateEntity exchangeRateEntity = null;
            if (toCurrency != fromCurrency)
            {
                //**exchangeRateEntity = CacheManager.GetExchangeRate(fromCurrency);
                List<ExchangeRateEntity> listexchangeRateEntity = new List<ExchangeRateEntity>();
                listexchangeRateEntity = ScanwebCacheManager.Instance.LookInCache<List<ExchangeRateEntity>>(AppConstants.ExchageRateCacheKey);

                if (listexchangeRateEntity != null && listexchangeRateEntity.Count > 0)
                    exchangeRateEntity = listexchangeRateEntity.Where(exRate => exRate.FromCurrency == fromCurrency).SingleOrDefault();

                if (exchangeRateEntity != null)
                {
                    convertedRateString = GetTotalConvertedAmountString(exchangeRateEntity, rateValue, toCurrency, stringFormat);
                }
                else
                {
                    if (exchangeRateProvider == AppConstants.ECB)
                    {
                        List<ExchangeRateEntity> exchangeRateEntityList = new List<ExchangeRateEntity>();
                        IExchangeRatesProcessor exchangeRatesProcessor = new ECBExchangeRatesProcessor();
                        exchangeRateEntityList = exchangeRatesProcessor.GetConvertedExchangeRates(fromCurrency, toCurrency, rateValue, allLocaleCurrencyList);
                        if (exchangeRateEntityList != null && exchangeRateEntityList.Count > 0)
                        {
                            ScanwebCacheManager.Instance.AddToCache(AppConstants.ExchageRateCacheKey, exchangeRateEntityList, 
                                Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.CacheExpiryTimeoutForCurrency]));

                            exchangeRateEntity = exchangeRateEntityList.Where(exRate => exRate.FromCurrency == fromCurrency).SingleOrDefault();
                            if (exchangeRateEntity != null)
                            {
                                convertedRateString = GetTotalConvertedAmountString(exchangeRateEntity, rateValue, toCurrency, stringFormat);
                                AppLogger.LogInfoMessage(String.Format("{0}{1} {2} = {3}", 1, "Exchange rate for ", fromCurrency, convertedRateString));
                            }
                            else
                            {
                                AppLogger.LogInfoMessage(String.Format("{0}{1}{2}", "Exchange rate for ", fromCurrency, " not available in service provider"));
                            }
                        }
                    }
                }

            }
            return convertedRateString;
        }
        /// <summary>
        /// Get total exchange rate by multiplying with Unit exchange rate
        /// </summary>
        /// <param name="unitExchangeRate">Unit exchangerate</param>
        /// <param name="rateValue">Amount to be converted</param>
        /// <param name="toCurrency">Target currency </param>
        /// <param name="stringFormat">Format to diplay alternate rate </param>
        private string GetTotalConvertedAmountString(ExchangeRateEntity exchangeRateEntity, string rateValue, string toCurrency, string stringFormat)
        {
            ConvertedCurrencyEntity convertedCurrencyEntity = new ConvertedCurrencyEntity();
            convertedCurrencyEntity = exchangeRateEntity.ConvertedCurrencies.Where(convCurrency => convCurrency.ToCurrency == toCurrency).SingleOrDefault();
            Double approxConvertedAmount = Convert.ToDouble(rateValue) * convertedCurrencyEntity.UnitAmount;
            return string.Format(stringFormat, Math.Ceiling(approxConvertedAmount), toCurrency);

        }
    }
}
