//  Description					: Utility class for Communication.          			  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Shankar Dasgupta                                   	  //
//  Author email id				:                           							  //
//  Creation Date				: 14th November  2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces

using System;
using System.Collections.Generic;
using System.Text;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion

    #region Scandic Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// This class helps to create various communication Entities for User Communication
    /// </summary>
    public class CommunicationUtility
    {
        #region Public Methods

        /// <summary>
        /// Get the Email Entity after parsing the template with the actual values.
        /// </summary>
        /// <param name="lostCardMap"></param>
        /// <returns></returns>
        public static EmailEntity GetForgottenPasswordConfirmationEmail(Dictionary<string, string> forgottenPasswordMap)
        {
            EmailEntity forgottenPasswordEmailEntity = TemplateManager.GetForgottenPasswordEmailTemplate();
            forgottenPasswordEmailEntity.Body = ParseTemplate(forgottenPasswordEmailEntity.Body, forgottenPasswordMap);
            return forgottenPasswordEmailEntity;
        }

        /// <summary>
        /// Get the Email Entity after parsing the template with the actual values.
        /// </summary>
        /// <param name="lostCardMap"></param>
        /// <returns></returns>
        public static EmailEntity GetLostCardConfirmationEmail(Dictionary<string, string> lostCardMap)
        {
            EmailEntity lostCardEmailEntity = TemplateManager.GetLostCardEmailTemplate();
            lostCardEmailEntity.Body = ParseTemplate(lostCardEmailEntity.Body, lostCardMap);
            return lostCardEmailEntity;
        }

        /// <summary>
        /// Get the Email Entity after parsing the template with the actual values.
        /// </summary>
        /// <param name="lostCardMap"></param>
        /// <param name="fromAddress"></param>
        /// <returns></returns>
        public static EmailEntity GetLostCardConfirmationEmailToUser(Dictionary<string, string> lostCardMap,
                                                                     string fromAddress)
        {
            EmailEntity lostCardEmailEntity = TemplateManager.GetLostCardEmailTemplateToUser(fromAddress);
            lostCardEmailEntity.Body = ParseTemplate(lostCardEmailEntity.Body, lostCardMap);
            return lostCardEmailEntity;
        }

        /// <summary>
        /// Get the Email Entity after parsing the template with the actual values.
        /// </summary>
        /// <param name="lostCardNoLogIn"></param>
        /// <returns></returns>
        public static EmailEntity GetLostCardNoLogInSCConfirmationEmail(Dictionary<string, string> lostCardNoLogInSCMap)
        {
            EmailEntity lostCardNoLogInSCEmailEntity = TemplateManager.GetLostCardNoLogInSCEmailTemplate();
            string originalTemplate = lostCardNoLogInSCEmailEntity.Body;
            if (lostCardNoLogInSCMap.ContainsKey(CommunicationTemplateConstants.NO_GUSTPRG_NUMBER))
            {
                originalTemplate = HideTemplateSections(originalTemplate,
                                                        CommunicationTemplateConstants.START_GUSTPRG_NUMBER,
                                                        CommunicationTemplateConstants.END_GUSTPRG_NUMBER);
            }
            else
            {
                originalTemplate =
                    originalTemplate.Replace("{" + CommunicationTemplateConstants.START_GUSTPRG_NUMBER + "}", "");
                originalTemplate =
                    originalTemplate.Replace("{" + CommunicationTemplateConstants.END_GUSTPRG_NUMBER + "}", "");
            }

            lostCardNoLogInSCEmailEntity.Body = ParseTemplate(originalTemplate, lostCardNoLogInSCMap);
            return lostCardNoLogInSCEmailEntity;
        }

        public static EmailEntity GetLostCardNoLogInConfirmationEmail(Dictionary<string, string> lostCardNoLogInMap,
                                                                      string fromAddress)
        {
            EmailEntity lostCardNoLogInEmailEntity = TemplateManager.GetLostCardNoLogInEmailTemplate(fromAddress);
            lostCardNoLogInEmailEntity.Body = ParseTemplate(lostCardNoLogInEmailEntity.Body, lostCardNoLogInMap);
            return lostCardNoLogInEmailEntity;
        }

        /// <summary>
        /// This Method email Entity for Contact Us
        /// </summary>
        /// <param name="contactUsMap"></param>
        /// <returns>EmailEntity</returns>
        public static EmailEntity GetContactUsConfirmationEmail(Dictionary<string, string> contactUsMap,
                                                                string toAddress)
        {
            EmailEntity contactUsEmailEntity =
                TemplateManager.GetContactUsEmailTemplate(toAddress);
            contactUsEmailEntity.Body = ParseTemplate(contactUsEmailEntity.Body, contactUsMap);
            return contactUsEmailEntity;
        }

        /// <summary>
        /// This Method email Entity for Contact Us
        /// </summary>
        /// <param name="contactUsMap"></param>
        /// <returns>EmailEntity</returns>
        public static EmailEntity GetContactUsSCConfirmationEmail(Dictionary<string, string> contactUsMap,
                                                                  string fromAddress,
                                                                  bool isChecked)
        {
            EmailEntity contactUsEmailEntity =
                TemplateManager.GetContactUsSCEmailTemplate(fromAddress, isChecked);
            string originalTemplate = contactUsEmailEntity.Body;
            if (contactUsMap.ContainsKey(CommunicationTemplateConstants.NO_PHONE))
            {
                originalTemplate = HideTemplateSections(originalTemplate, CommunicationTemplateConstants.START_PHONE,
                                                        CommunicationTemplateConstants.END_PHONE);
            }
            else
            {
                originalTemplate = originalTemplate.Replace("{" + CommunicationTemplateConstants.START_PHONE + "}", "");
                originalTemplate = originalTemplate.Replace("{" + CommunicationTemplateConstants.END_PHONE + "}", "");
            }

            if (contactUsMap.ContainsKey(CommunicationTemplateConstants.NO_FAX))
            {
                originalTemplate = HideTemplateSections(originalTemplate, CommunicationTemplateConstants.START_FAX,
                                                        CommunicationTemplateConstants.END_FAX);
            }
            else
            {
                originalTemplate = originalTemplate.Replace("{" + CommunicationTemplateConstants.START_FAX + "}", "");
                originalTemplate = originalTemplate.Replace("{" + CommunicationTemplateConstants.END_FAX + "}", "");
            }
            contactUsEmailEntity.Body = ParseTemplate(originalTemplate, contactUsMap);
            return contactUsEmailEntity;
        }

        /// <summary>
        /// Get the Email Entity after parsing the template with the actual values.
        /// </summary>
        /// <param name="lostCardMap"></param>
        /// <returns></returns>
        public static EmailEntity GetInviteAFriendToUserConfirmationEmail(
            Dictionary<string, string> inviteAFriendToUserMap,
            string toAddress)
        {
            EmailEntity inviteAFriendToUser = TemplateManager.GetInviteAFriendToUserConfirmationEmailTemplate(toAddress);
            inviteAFriendToUser.Body = ParseTemplate(inviteAFriendToUser.Body, inviteAFriendToUserMap);
            return inviteAFriendToUser;
        }

        /// <summary>
        /// This Method email Entity for Invite A Friend Email confirmation.
        /// </summary>
        /// <param name="InviteAFriendMap">Dictionary of Invite A Friend Map</param>
        /// <param name="masterList">Friends name and email id's</param>
        /// <returns>EmailEntity</returns>
        public static EmailEntity GetInviteAFriendConfirmationEmail(Dictionary<string, string> InviteAFriendMap,
                                                                    Dictionary<int, Dictionary<string, string>>
                                                                        masterList)
        {
            EmailEntity inviteAFriendEmailEntity =
                TemplateManager.GetInviteAFriendConfirmationEmailTemplate();
            inviteAFriendEmailEntity.Body = ParseTemplate(inviteAFriendEmailEntity.Body, InviteAFriendMap);

            string repeatingTemplate = inviteAFriendEmailEntity.PlaceHolder;
            StringBuilder nameEmailString = new StringBuilder();

            Dictionary<string, string> placeHolderMap = new Dictionary<string, string>();
            for (int counter = 0; counter < masterList.Count; counter++)
            {
                Dictionary<string, string> temp;
                temp = masterList[counter];
                foreach (string key in temp.Keys)
                {
                    placeHolderMap[CommunicationTemplateConstants.FRIENDSNAME + counter] = key;
                    placeHolderMap[CommunicationTemplateConstants.FRIENDSEMAILID + counter] = temp[key];

                    string te = repeatingTemplate.Replace("{" + CommunicationTemplateConstants.FRIENDSNAME + "}"
                                                          ,
                                                          "{" + CommunicationTemplateConstants.FRIENDSNAME + counter +
                                                          "}");
                    nameEmailString.Append(te.Replace("{" + CommunicationTemplateConstants.FRIENDSEMAILID + "}",
                                                      "{" + CommunicationTemplateConstants.FRIENDSEMAILID + counter +
                                                      "}"));
                }
            }
            inviteAFriendEmailEntity.PlaceHolder = ParseTemplate(nameEmailString.ToString(), placeHolderMap);
            inviteAFriendEmailEntity.Body =
                inviteAFriendEmailEntity.Body.Replace(CommunicationTemplateConstants.PLACEHOLDER_POINT,
                                                      inviteAFriendEmailEntity.PlaceHolder);

            return inviteAFriendEmailEntity;
        }

        /// <summary>
        /// This Method email Entity for Missing Point Email confirmation.
        /// </summary>
        /// <param name="missingPointMap">
        /// Dictionary of Missing Point Map</param>
        /// <returns>
        /// EmailEntity
        /// </returns>
        public static EmailEntity GetMissingPointConfirmationEmail(Dictionary<string, string> missingPointMap,
                                                                   string toAddress)
        {
            EmailEntity missingPointEmailEntity =
                TemplateManager.GetMissingPointConfirmationEmailTemplate(toAddress);
            missingPointEmailEntity.Body = ParseTemplate(missingPointEmailEntity.Body, missingPointMap);
            return missingPointEmailEntity;
        }

        /// <summary>
        /// This Method email Entity for Missing Point Email confirmation.
        /// </summary>
        /// <param name="missingPointMap">Dictionary of Missing Point Map</param>
        /// <param name="noOfMissingPoints">Missing pioint Entity Count</param>
        /// <returns>EmailEntity</returns>
        /// <remarks>Recently modified for artf809435</remarks>
        public static EmailEntity GetMissingPointSCConfirmationEmail(Dictionary<string, string> missingPointMap
                                                                     , int noOfMissingPoints)
        {
            EmailEntity missingPointEmailEntity =
                TemplateManager.GetMissingPointSCConfirmationEmailTemplate();

            string[] seperator = new string[1];
            seperator[0] = "{REPETATORTEMPLATE}";
            string[] seperatedCopy = missingPointEmailEntity.Body.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
            String templateString = seperatedCopy[1];
            string newString = string.Empty;
            for (int count = 0; count < noOfMissingPoints; count++)
            {
                StringBuilder sb = new StringBuilder(templateString);
                sb.Replace(CommunicationTemplateConstants.CITY, CommunicationTemplateConstants.CITY + count);
                sb.Replace(CommunicationTemplateConstants.ARRIVAL_DATE,
                           CommunicationTemplateConstants.ARRIVAL_DATE + count);
                sb.Replace(CommunicationTemplateConstants.DEPARTURE_DATE,
                           CommunicationTemplateConstants.DEPARTURE_DATE + count);
                sb.Replace(CommunicationTemplateConstants.HOTEL, CommunicationTemplateConstants.HOTEL + count);
                sb.Replace(CommunicationTemplateConstants.CONFIRMATION_NUMBER,
                           CommunicationTemplateConstants.CONFIRMATION_NUMBER + count);
                newString += sb.ToString();
            }

            string originalTemplate = seperatedCopy[0] + newString + seperatedCopy[2];

            if (missingPointMap.ContainsKey(CommunicationTemplateConstants.NO_RESERVATION_NUMBER))
            {
                originalTemplate = HideTemplateSections(originalTemplate,
                                                        CommunicationTemplateConstants.START_RESERVATION_NUMBER,
                                                        CommunicationTemplateConstants.END_RESERVATION_NUMBER);
            }
            else
            {
                originalTemplate =
                    originalTemplate.Replace("{" + CommunicationTemplateConstants.START_RESERVATION_NUMBER + "}", "");
                originalTemplate =
                    originalTemplate.Replace("{" + CommunicationTemplateConstants.END_RESERVATION_NUMBER + "}", "");
            }

            missingPointEmailEntity.Body = ParseTemplate(originalTemplate, missingPointMap);
            return missingPointEmailEntity;
        }

        /// <summary>
        /// Method to get the Email Entity for Enroll confirmation
        /// </summary>
        /// <param name="enrollMap">Dictionary containg the Identifier mapped to actual values </param>
        /// <returns>EmailEntity</returns>
        public static EmailEntity GetEnrollConfirmationEmail(Dictionary<string, string> enrollMap)
        {
            EmailEntity enrollEmailEntity =
                TemplateManager.GetEnrollProfileConfirmationEmailTemplate();
            enrollEmailEntity.Body = ParseTemplate(enrollEmailEntity.Body, enrollMap);
            return enrollEmailEntity;
        }

        /// <summary>
        /// Get the Email Entity after parsing the template with the actual values 
        /// </summary>
        /// <param name="bookingMap">
        /// Dictionary of booking Map</param>
        /// <returns>
        /// Email Entity
        /// </returns>
        public static EmailEntity GetBookingConfirmationEmail(Dictionary<string, string> bookingMap, bool isModifiable)
        {
            EmailEntity bookingEmailEntity =
                TemplateManager.GetBookingConfirmationEmailTemplate(isModifiable);
            bookingEmailEntity.Body = RemoveLabelFromEmailTemplateIfNotNeeded(bookingMap, bookingEmailEntity.Body);
            bookingEmailEntity.Body = ParseTemplate(bookingEmailEntity.Body, bookingMap);

            bookingEmailEntity.TextEmailBody = RemoveLabelFromEmailTemplateIfNotNeeded(bookingMap,
                                                                                       bookingEmailEntity.TextEmailBody);
            bookingEmailEntity.TextEmailBody = ParseTemplate(bookingEmailEntity.TextEmailBody, bookingMap);
            return bookingEmailEntity;
        }

        /// <summary>
        /// Get the Email Entity after parsing the template with the actual values 
        /// </summary>
        /// <param name="bookingMap">Dictionary of booking Map</param>
        /// <returns>Email Entity</returns>
        public static EmailEntity GetCancelBookingConfirmationEmail(Dictionary<string, string> bookingMap)
        {
            EmailEntity CancelbookingEmailEntity =
                TemplateManager.GetCancelBookingConfirmationEmailTemplate();
            string originalTemplate = CancelbookingEmailEntity.Body;
            string textTemplate = CancelbookingEmailEntity.TextEmailBody;

            if (bookingMap.ContainsKey(CommunicationTemplateConstants.NO_MEMBERID))
            {
                originalTemplate = HideTemplateSections(originalTemplate, CommunicationTemplateConstants.START_MEMBERID,
                                                        CommunicationTemplateConstants.END_MEMBERID);
                textTemplate = HideTemplateSections(textTemplate, CommunicationTemplateConstants.START_MEMBERID,
                                                    CommunicationTemplateConstants.END_MEMBERID);
            }
            else
            {
                originalTemplate = originalTemplate.Replace("{" + CommunicationTemplateConstants.START_MEMBERID + "}",
                                                            "");
                originalTemplate = originalTemplate.Replace("{" + CommunicationTemplateConstants.END_MEMBERID + "}", "");

                textTemplate = textTemplate.Replace("{" + CommunicationTemplateConstants.START_MEMBERID + "}", "");
                textTemplate = textTemplate.Replace("{" + CommunicationTemplateConstants.END_MEMBERID + "}", "");
            }

            CancelbookingEmailEntity.Body = ParseTemplate(originalTemplate, bookingMap);

            CancelbookingEmailEntity.TextEmailBody = ParseTemplate(textTemplate, bookingMap);

            return CancelbookingEmailEntity;
        }

        /// <summary>
        ///  Get the SMS Entity for Booking Confirmation after parsing the template with the actual values
        /// </summary>
        /// <param name="bookingMap">
        /// Dictionary of booking map
        /// </param>
        /// <returns>
        /// SMSEntity
        /// </returns>
        public static SMSEntity GetBookingConfirmationSMS(Dictionary<string, string> bookingMap)
        {
            SMSEntity bookingSmsEntity = TemplateManager.GetBookingConfirmationSMSTemplate();
            string currentTemplate = bookingSmsEntity.Message;
            int maxlength = AppConstants.SMS_MAX_LENGTH;

            string parsedMessage = ParseTemplate(bookingSmsEntity.Message, bookingMap);

            if (!string.IsNullOrEmpty(parsedMessage) && parsedMessage.Length > maxlength)
            {
                parsedMessage = GetTruncatedSMSMessage(ref currentTemplate, ref bookingMap);
                if (parsedMessage.Length > maxlength)
                {
                    bookingMap.Remove(CommunicationTemplateConstants.ROOM_RATE);
                    currentTemplate = currentTemplate.Replace(" {" + CommunicationTemplateConstants.ROOM_RATE + "}.",
                                                              string.Empty);
                    parsedMessage = ParseTemplate(currentTemplate, bookingMap);
                }
            }
            bookingSmsEntity.Message = parsedMessage;
            return bookingSmsEntity;
        }

        /// <summary>
        /// Get the SMS Entity for Booking Cancellation after parsing the template with the actual values
        /// </summary>
        /// <param name="bookingMap"></param>
        /// <returns></returns>
        public static SMSEntity GetBookingCancellationSMS(Dictionary<string, string> bookingCancelMap)
        {
            SMSEntity bookingSmsEntity = TemplateManager.GetBookingCancellationSMSTemplate();
            string currentTemplate = bookingSmsEntity.Message;
            int maxlength = AppConstants.SMS_MAX_LENGTH;

            string parsedMessage = ParseTemplate(bookingSmsEntity.Message, bookingCancelMap);

            if (!string.IsNullOrEmpty(parsedMessage) && parsedMessage.Length > maxlength)
            {
                string currentMessage = parsedMessage;
                char[] msgDelimiter = {'.'};
                string[] messageParts = currentMessage.Split(msgDelimiter, StringSplitOptions.RemoveEmptyEntries);
                StringBuilder newMessage = new StringBuilder();
                for (int i = 0; i < messageParts.Length - 1; i++)
                {
                    newMessage.Append(messageParts[i]);
                    newMessage.Append(".");
                }
                parsedMessage = newMessage.ToString();

                if (parsedMessage.Length > maxlength)
                {
                    string messageTemplate = currentTemplate.Trim();
                    messageParts = messageTemplate.Split(msgDelimiter, StringSplitOptions.RemoveEmptyEntries);
                    newMessage = new StringBuilder();
                    for (int i = 0; i < messageParts.Length - 1; i++)
                    {
                        newMessage.Append(messageParts[i]);
                        newMessage.Append(".");
                    }
                    currentTemplate = newMessage.ToString();

                    parsedMessage = GetTruncatedSMSMessage(ref currentTemplate, ref bookingCancelMap);
                }
            }
            bookingSmsEntity.Message = parsedMessage;
            return bookingSmsEntity;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// This method will remove the placeholder from email if there is no infromation 
        /// against this placeholder.
        /// </summary>
        /// <param name="bookingMap">Dymnamic content for this booking</param>
        /// <param name="bodyString">Body string read from email XML.</param>
        /// <returns>Body string</returns>
        /// <remarks>CR 4 | Text based email confirmation | Release 1.4</remarks>
        private static string RemoveLabelFromEmailTemplateIfNotNeeded(Dictionary<string, string> bookingMap,
                                                                      string bodyString)
        {
            if (bookingMap.ContainsKey(CommunicationTemplateConstants.NO_PHONE))
            {
                bodyString = HideTemplateSections(bodyString, CommunicationTemplateConstants.START_PHONE,
                                                  CommunicationTemplateConstants.END_PHONE);
            }
            else
            {
                bodyString = bodyString.Replace("{" + CommunicationTemplateConstants.START_PHONE + "}", "");
                bodyString = bodyString.Replace("{" + CommunicationTemplateConstants.END_PHONE + "}", "");
            }

            if (bookingMap.ContainsKey(CommunicationTemplateConstants.NO_MEMBERID))
            {
                bodyString = HideTemplateSections(bodyString, CommunicationTemplateConstants.START_MEMBERID,
                                                  CommunicationTemplateConstants.END_MEMBERID);
            }
            else
            {
                bodyString = bodyString.Replace("{" + CommunicationTemplateConstants.START_MEMBERID + "}", "");
                bodyString = bodyString.Replace("{" + CommunicationTemplateConstants.END_MEMBERID + "}", "");
            }

            if (bookingMap.ContainsKey(CommunicationTemplateConstants.NO_DNUMBER))
            {
                bodyString = HideTemplateSections(bodyString, CommunicationTemplateConstants.START_DNUMBER,
                                                  CommunicationTemplateConstants.END_DNUMBER);
            }
            else
            {
                bodyString = bodyString.Replace("{" + CommunicationTemplateConstants.START_DNUMBER + "}", "");
                bodyString = bodyString.Replace("{" + CommunicationTemplateConstants.END_DNUMBER + "}", "");
            }

            if (bookingMap.ContainsKey(CommunicationTemplateConstants.NO_CHILDREN_AGES))
            {
                bodyString = HideTemplateSections(bodyString, CommunicationTemplateConstants.START_CHILDRENAGES,
                                                  CommunicationTemplateConstants.END_CHILDRENAGES);
            }
            else
            {
                bodyString = bodyString.Replace("{" + CommunicationTemplateConstants.START_CHILDRENAGES + "}", "");
                bodyString = bodyString.Replace("{" + CommunicationTemplateConstants.END_CHILDRENAGES + "}", "");
            }

            if (bookingMap.ContainsKey(CommunicationTemplateConstants.NO_CHILDREN_ACCOMMODATION))
            {
                bodyString = HideTemplateSections(bodyString,
                                                  CommunicationTemplateConstants.START_CHILDREN_ACCOMMODATION,
                                                  CommunicationTemplateConstants.END_CHILDREN_ACCOMMODATION);
            }
            else
            {
                bodyString = bodyString.Replace(
                    "{" + CommunicationTemplateConstants.START_CHILDREN_ACCOMMODATION + "}", "");
                bodyString = bodyString.Replace("{" + CommunicationTemplateConstants.END_CHILDREN_ACCOMMODATION + "}",
                                                "");
            }

            return bodyString;
        }

        /// <summary>
        /// This method is called to truncate the SMS template string, so as to limit it to maxLength characters.
        /// The hotel's street address is removed first and then if required, the hotel telephone is also removed.
        /// </summary>
        /// <param name="currentTemplate">The template string that is used to create the SMS message.</param>
        /// <param name="bookingMap">The dictionary containing the values for the placeholders in the template string.</param>
        /// <returns></returns>
        private static string GetTruncatedSMSMessage(ref string currentTemplate,
                                                     ref Dictionary<string, string> bookingMap)
        {
            string truncatedMessage = string.Empty;
            bookingMap.Remove(CommunicationTemplateConstants.HOTELADDRESS);
            currentTemplate = currentTemplate.Replace("{" + CommunicationTemplateConstants.HOTELADDRESS + "}, ",
                                                      string.Empty);
            truncatedMessage = ParseTemplate(currentTemplate, bookingMap);

            int maxLength = AppConstants.SMS_MAX_LENGTH;
            if (truncatedMessage.Length > maxLength)
            {
                bookingMap.Remove(CommunicationTemplateConstants.HOTELTELEPHONE);
                currentTemplate = currentTemplate.Replace(", {" + CommunicationTemplateConstants.HOTELTELEPHONE + "}",
                                                          string.Empty);
                truncatedMessage = ParseTemplate(currentTemplate, bookingMap);
            }
            return truncatedMessage;
        }

        /// <summary>
        /// Method to replace the Section from the template
        /// </summary>
        /// <param name="Template">Original template</param>
        /// <param name="Start_Token">Section Start with</param>
        /// <param name="End_Token">Section Ends with</param>
        /// <returns></returns>
        private static string HideTemplateSections(string Template, string Start_Token, string End_Token)
        {
            string[] delimeter = new string[2];
            delimeter[0] = "{" + Start_Token + "}";
            delimeter[1] = "{" + End_Token + "}";

            string[] splitTemplateTextList = Template.Split(delimeter, StringSplitOptions.RemoveEmptyEntries);
            StringBuilder changedTemplate = new StringBuilder();
            if (splitTemplateTextList.Length > 0)
            {
                changedTemplate.Append(splitTemplateTextList[0]);
            }
            if (splitTemplateTextList.Length >= 2)
            {
                changedTemplate.Append(splitTemplateTextList[2]);
            }
            return changedTemplate.ToString();
        }

        /// <summary>
        /// Parse the Template with the actual values
        /// </summary>
        /// <param name="originalTemplate">
        /// Original Template
        /// </param>
        /// <param name="orderedDictionary">
        /// Dictionary containing the map of identifier and values
        /// </param>
        private static string ParseTemplate(string templateText, Dictionary<string, string> bookingMap)
        {
            string translatedValue = string.Empty;
            foreach (string key in bookingMap.Keys)
            {
                if (bookingMap[key] != null)
                {
                    translatedValue = bookingMap[key].ToString();
                    if (translatedValue == string.Empty)
                    {
                        translatedValue = CommunicationTemplateConstants.BLANK_SPACES;
                    }
                    string identifier = "{" + key + "}";
                    templateText = templateText.Replace(identifier, translatedValue);
                }
            }
            return templateText.Trim();
        }

        #endregion
    }
}