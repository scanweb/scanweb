#region System Namespaces

using System;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Caching;
using System.Xml;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Core.Core;
using System.Collections.Generic;
using System.Collections;

#endregion

#region Scandic Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// This is the service class which will have various methods to return 
    /// the drop down values for the booking engine pages.
    /// This class will cache the results read from the XML files and return the values
    /// </summary>
    public class DropDownService
    {
        private const string COUNTRIES_CACHE_KEY = "BECOUNTRIES{0}";
        private const string CREDITCARDS_CACHE_KEY = "BECREDITCARDS{0}";
        private const string TELEPHONECODES_CACHE_KEY = "BETELEPHONECODES{0}";
        private const string QUESTION_CACHE_KEY = "QUESTION{0}";
        private const string PARTNERPROGRAM_CACHE_KEY = "PARTNERPROGRAM{0}";
        private const string PREFERREDLANGUAGE_FILENAME_CACHE_KEY = "PREFERREDLANGUAGE{0}";
        private const string CONTACTSUBJECT_CACHE_KEY = "SUBJECTCODES{0}";
        private const string CURRENCY_CODES_CACHE_KEY = "BECURRENCYCODES{0}";
        private const string COUNTRY_PHONE_CACHE_KEY = "BECOUNTRYPHONECODEMAP";


        /// ////////////////////////////////////////////////////////////////////////////////////////
        //  Purpose 					: Fetch the Language preference codes from the Cache if available//
        //                                else loads the cache.                     		      //
        //																						  //
        //----------------------------------------------------------------------------------------//
        /// Input Parameters    		:                                                     	  //
        /// O/P Parameters				: Ordered Dictionary object             									  //
        ///---------------------------------------------------------------------------------------//
        /// Additional Information		:                                                         //
        ///	Last Modified By			:														  //
        ///	Last Modified Date          :                                                         //     
        ////////////////////////////////////////////////////////////////////////////////////////////        
        public static OrderedDictionary GetPreferredLanguageCodes()
        {
            string codeFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            codeFileName = codeFileName.Substring(0, codeFileName.LastIndexOf("\\"));

            string cachekey = string.Format(PREFERREDLANGUAGE_FILENAME_CACHE_KEY, currentLanguageString);
            codeFileName += "\\lang\\dropdowns\\" + AppConstants.PREFERREDLANGUAGE_FILENAME +
                            currentLanguageString + AppConstants.CODE_FILE_EXTN;

            return GetCodes(cachekey, codeFileName);
        }

        /// <summary>
        /// Get the Country and Phone Codes Mapping
        /// </summary>
        /// <returns>
        /// Ordered Dictionary of Country-Phone Codes
        /// </returns>
        public static OrderedDictionary GetCountryPhoneCodeMap()
        {
            string codeFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            codeFileName = codeFileName.Substring(0, codeFileName.LastIndexOf("\\"));

            string cachekey = COUNTRY_PHONE_CACHE_KEY;
            codeFileName += "\\lang\\dropdowns\\" + AppConstants.COUNTRYPHONECODESMAP_FILENAME +
                            AppConstants.CODE_FILE_EXTN;

            return GetCodes(cachekey, codeFileName);
        }

        /// ////////////////////////////////////////////////////////////////////////////////////////
        //  Purpose 					: Fetch the partner program codes from the Cache if available//
        //                                else loads the cache.                     		      //
        //																						  //
        //----------------------------------------------------------------------------------------//
        /// Input Parameters    		:                                                     	  //
        /// O/P Parameters				: Ordered Dictionary object             									  //
        ///---------------------------------------------------------------------------------------//
        /// Additional Information		:                                                         //
        ///	Last Modified By			:														  //
        ///	Last Modified Date          :                                                         //     
        ////////////////////////////////////////////////////////////////////////////////////////////        
        public static OrderedDictionary GetPartnerProgramCodes()
        {
            string codeFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            codeFileName = codeFileName.Substring(0, codeFileName.LastIndexOf("\\"));

            string cachekey = string.Format(PARTNERPROGRAM_CACHE_KEY, currentLanguageString);
            codeFileName += "\\lang\\dropdowns\\" + AppConstants.PARTNERPROGRAM_FILENAME +
                            currentLanguageString + AppConstants.CODE_FILE_EXTN;

            return GetCodes(cachekey, codeFileName);
        }

        /// ////////////////////////////////////////////////////////////////////////////////////////
        //  Purpose 					: Fetch the Question codes from the Cache if available   //
        //                                else loads the cache.                     		      //
        //																						  //
        //----------------------------------------------------------------------------------------//
        /// Input Parameters    		:                                                     	  //
        /// O/P Parameters				: Ordered Dictionary object             									  //
        ///---------------------------------------------------------------------------------------//
        /// Additional Information		:                                                         //
        ///	Last Modified By			:														  //
        ///	Last Modified Date          :                                                         //     
        ////////////////////////////////////////////////////////////////////////////////////////////        
        public static OrderedDictionary GetQuestionCodes()
        {
            string codeFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            codeFileName = codeFileName.Substring(0, codeFileName.LastIndexOf("\\"));

            string cachekey = string.Format(QUESTION_CACHE_KEY, currentLanguageString);
            codeFileName += "\\lang\\dropdowns\\" + AppConstants.QUESTION_FILENAME +
                            currentLanguageString + AppConstants.CODE_FILE_EXTN;

            return GetCodes(cachekey, codeFileName);
        }

        /// ////////////////////////////////////////////////////////////////////////////////////////
        //  Purpose 					: Fetch the Telephone codes from the Cache if available   //
        //                                else loads the cache.                     		      //
        //																						  //
        //----------------------------------------------------------------------------------------//
        /// Input Parameters    		:                                                     	  //
        /// O/P Parameters				: Ordered Dictionary object             									  //
        ///---------------------------------------------------------------------------------------//
        /// Additional Information		:                                                         //
        ///	Last Modified By			:														  //
        ///	Last Modified Date          :                                                         //     
        ////////////////////////////////////////////////////////////////////////////////////////////        
        public static OrderedDictionary GetTelephoneCodes()
        {
            string codeFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            codeFileName = codeFileName.Substring(0, codeFileName.LastIndexOf("\\"));

            string cachekey = string.Format(TELEPHONECODES_CACHE_KEY, currentLanguageString);
            codeFileName += "\\lang\\dropdowns\\" + AppConstants.PHONECODE_FILENAME +
                            currentLanguageString + AppConstants.CODE_FILE_EXTN;

            return GetCodes(cachekey, codeFileName);
        }

        /// <summary>
        /// Fetch the Country codes from the Cache if available else loads the cache by the language
        /// </summary>
        /// <returns>
        /// Ordered Dictionary object
        /// </returns>
        public static OrderedDictionary GetCountryCodes(string language)
        {
            string codeFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            codeFileName = codeFileName.Substring(0, codeFileName.LastIndexOf("\\"));

            string cachekey = null;

            if (!string.IsNullOrEmpty(language))
                cachekey = string.Format(COUNTRIES_CACHE_KEY, language);
            else
                cachekey = string.Format(COUNTRIES_CACHE_KEY, currentLanguageString);

            codeFileName += "\\lang\\dropdowns\\" + AppConstants.COUNTRYCODE_FILENAME +
                (!string.IsNullOrEmpty(language) ? language : currentLanguageString) + AppConstants.CODE_FILE_EXTN;

            return GetCodes(cachekey, codeFileName);
        }

        /// <summary>
        /// Fetch the Country codes from the Cache if available else loads the cache.
        /// </summary>
        /// <returns>
        /// Ordered Dictionary object
        /// </returns>
        public static OrderedDictionary GetCountryCodes()
        {
            return GetCountryCodes(string.Empty);
        }

        /// <summary>
        /// Fetch the values from xml for Subject combobox
        /// </summary>
        /// <returns>
        /// Ordered Dictionary object
        /// </returns>
        public static OrderedDictionary GetSubject()
        {
            string codeFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            codeFileName = codeFileName.Substring(0, codeFileName.LastIndexOf("\\"));

            string cachekey = string.Format(CONTACTSUBJECT_CACHE_KEY, currentLanguageString);
            codeFileName += "\\lang\\dropdowns\\" + AppConstants.EMAILSUBJECT +
                            currentLanguageString + AppConstants.CODE_FILE_EXTN;


            return GetCodes(cachekey, codeFileName);
        }

        /// <summary>
        /// Fetch the CreditCards codes from the Cache if available else loads the cache.
        /// </summary>
        /// <returns>
        /// Ordered Dictionary object
        /// </returns>
        public static OrderedDictionary GetCreditCardsCodes()
        {
            string codeFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            codeFileName = codeFileName.Substring(0, codeFileName.LastIndexOf("\\"));

            string cachekey = string.Format(CREDITCARDS_CACHE_KEY, currentLanguageString);
            codeFileName += "\\lang\\dropdowns\\" + AppConstants.CREDITCARDSTYPES_FILENAME +
                            currentLanguageString + AppConstants.CODE_FILE_EXTN;


            return GetCodes(cachekey, codeFileName);
        }

        /// <summary>
        /// Get the Currency Codes to be supported for the Currency Converter
        /// </summary>
        /// <returns>
        /// Ordered Dictionary object
        /// </returns>
        /// <remarks>
        /// This method has been added for CR-4(Currency Converter) - Release 1.3
        /// </remarks>
        public static OrderedDictionary GetCurrencyCodes()
        {
            string codeFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            codeFileName = codeFileName.Substring(0, codeFileName.LastIndexOf("\\"));

            string cachekey = string.Format(CURRENCY_CODES_CACHE_KEY, currentLanguageString);
            codeFileName += "\\lang\\dropdowns\\" + AppConstants.CURRENCYCODES_FILENAME +
                            currentLanguageString + AppConstants.CODE_FILE_EXTN;

            return GetCodes(cachekey, codeFileName);
        }

        /// <summary>
        /// Get the Codes from the cache if available else read from
        /// the xml file
        /// </summary>
        /// <param name="cacheKey">
        /// Cache Key
        /// </param>
        /// <param name="codeFileName">
        /// Name of the Xml file containing the codes
        /// </param>
        /// <returns></returns>
        private static OrderedDictionary GetCodes(string cacheKey, string codeFileName)
        {
            Scanweb.Core.AppLogger.LogInfoMessage("In DropDownService\\GetCodes()");
            OrderedDictionary codesMap = ScanwebCacheManager.Instance.LookInCache<OrderedDictionary>(cacheKey);
            if (codesMap == null)
            {
                Scanweb.Core.AppLogger.LogInfoMessage(string.Format("Codes Map {0} is Empty: Creating Codes Map from XML", cacheKey));
                XmlDocument codesXmlDoc = new XmlDocument();
                codesXmlDoc.Load(codeFileName);
                XmlNodeList codesXMLList = codesXmlDoc.SelectNodes("CodeList")[0].ChildNodes;
                codesMap = new OrderedDictionary();
                for (int codesCount = 0; codesCount < codesXMLList.Count; codesCount++)
                {
                    string code = codesXMLList[codesCount].Attributes["Code"].Value;
                    try
                    {
                        if (!codesMap.Contains(code))
                            codesMap.Add(code, codesXMLList[codesCount].InnerText);
                    }
                    catch (ArgumentException ex)
                    {
                        Scandic.Scanweb.Core.AppLogger.LogFatalException(ex, "Duplicate entries available in the xml file.");
                        throw ex;
                    }
                }
                StoreObjectInCache(cacheKey, codesMap, codeFileName);
            }
            else
            {
                Scanweb.Core.AppLogger.LogInfoMessage(string.Format("Codes Map {0} is retrieved from the cache", cacheKey));
            }
            return codesMap;
        }

        /// <summary>
        /// Store the Specified object to the Cache
        /// </summary>
        /// <param name="cacheKey">
        /// Key for the Cache Entity
        /// </param>
        /// <param name="objectToStore">
        /// Object to be stored into the cache
        /// </param>
        /// <param name="dependencyFile">
        /// The file on which the Cache object depends
        /// </param>
        private static void StoreObjectInCache(string cacheKey, object objectToStore, string dependencyFile)
        {
            if (dependencyFile.StartsWith("file:\\"))
            {
                dependencyFile = dependencyFile.Remove(0, 6);
            }
            ScanwebCacheManager.Instance.AddToCache(cacheKey, objectToStore, dependencyFile);
        }

        /// <summary>
        /// Get the Preferred language Name
        /// </summary>
        private static string currentLanguageString
        {
            get { return EPiServer.Globalization.ContentLanguage.PreferredCulture.Name; }
        }
    }
}