//  Description					:   WebUtil                                               //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Filters;
using EPiServer.Security;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;
using Scandic.Scanweb.BookingEngine.Web.HotelTimezone;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Core.Core;
using Scandic.Scanweb.Core.Encryption;
using Scandic.Scanweb.Core.Encryption.Interface;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using BVNetwork.FileNotFound;
namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// Contains all web util members.
    /// </summary>
    public class WebUtil
    {
        private AvailabilityController availabilityController = null;
        private const string BASE_ENCRYPTION_KEY = "ScanWebEncryptionKey";
        private static TripAdvisorPlaceListEntity m_tripAdvisorPlaceList;
        private static object offerSyncLock = new object();

        private static string booking_Promo_Code;
        public static string Booking_Promo_Code
        {
            get { return booking_Promo_Code; }
            set { booking_Promo_Code = value; }
        }

        private static string currentLanguageString
        {
            get { return EPiServer.Globalization.ContentLanguage.PreferredCulture.Name; }
        }

        public static bool IsNonSupportedMobileLanguage(HttpRequest request)
        {
            var nonSupportedPattren = ConfigurationManager.AppSettings["MobileNonSupportedLanguages"];
            var regx = new Regex(nonSupportedPattren);
            var pathToCheck = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, request.RawUrl);
            return regx.IsMatch(pathToCheck);
        }

        private static PageDataCollection GetOfferPages(bool isCacheFillerRequest, string fillerLanguage)
        {
            var language = currentLanguageString;
            if (isCacheFillerRequest)
            {
                language = fillerLanguage;
            }
            var cacheKey = string.Format("Scandic_Offers_{0}", language);
            PageDataCollection offerPages = new PageDataCollection();
            string message = string.Empty;
            if ((ScanwebCacheManager.Instance.LookInCache<PageDataCollection>(cacheKey) == null) || isCacheFillerRequest)
            {
                message = "CDAL - Scandic offers - Repopulating Cache - Key=" + cacheKey;

                //PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage, languageSelector, EPiServer.Security.AccessLevel.NoAccess);
                PageData rootPage = ContentDataAccess.GetPageData(PageReference.RootPage, false);
                PageReference offerContainer = rootPage["OfferContainer"] as PageReference;
                offerPages = GetAllOffers(offerContainer, language);
                if (offerPages != null)
                    ScanwebCacheManager.Instance.AddToCache(cacheKey, offerPages, new Collection<object> { true, language },
                        (Func<bool, string, PageDataCollection>)GetOfferPages);
            }
            else
            {
                message = "CDAL - Scandic offers  - Return value from cache - Key=" + cacheKey;
                offerPages = ScanwebCacheManager.Instance.LookInCache<PageDataCollection>(cacheKey);
            }
            ContentDataAccess.LogMessage(message);
            return offerPages;
        }

        public static PageDataCollection GetHotelOfferPages(PageData hotelData)
        {
            PageDataCollection offerPages = null;
            if (hotelData != null)
            {
                offerPages = new PageDataCollection();
                var allOffers = GetOfferPages(false, "");
                PageDataCollection offersFromOfferPages = GetOffersFromOfferPages(allOffers, hotelData);
                PageDataCollection offersFromHotelPages = GetOffersFromHotelPages(allOffers, hotelData);
                offerPages = GetConsolidatesOffersListForHotel(offersFromOfferPages, offersFromHotelPages);
                offerPages.Sort(new VersionDateComparer());
                offersFromHotelPages = null;
                offersFromOfferPages = null;
            }
            //offerPages = null;
            return offerPages;
        }

        private static PageDataCollection GetOffersFromHotelPages(PageDataCollection allOffers, PageData hotelData)
        {
            PageDataCollection offerPages = new PageDataCollection();
            if (hotelData != null)
            {
                if (hotelData["PromotionBoxOffers"] != null)
                {
                    string selectedOffers = hotelData["PromotionBoxOffers"] as string;
                    string[] selectedOfferList = selectedOffers.Split(',');
                    string selectedOffer = string.Empty;

                    for (int ctr = 0; ctr < selectedOfferList.Length; ctr++)
                    {
                        selectedOffer = selectedOfferList[ctr].Replace("{", "").Replace("}", "");

                        foreach (PageData eachOffer in allOffers)
                        {
                            if (eachOffer.PageLink.ID.ToString() == selectedOffer.Trim())
                            {
                                offerPages.Add(eachOffer);
                            }
                        }
                    }
                }
            }
            return offerPages;
        }

        private static PageDataCollection GetOffersFromOfferPages(PageDataCollection allOffers, PageData hotelData)
        {
            PageDataCollection offerPages = new PageDataCollection();
            if (hotelData != null)
            {
                foreach (PageData offerPage in allOffers)
                {
                    offerPages.Add(offerPage);
                }
                // Keep only offers that should be visible on this hotel
                FilterCompareTo offerTypeFilter = new FilterCompareTo("Hotels", "{" + hotelData["OperaID"] as string + "}");
                offerTypeFilter.Condition = CompareCondition.Contained;
                offerTypeFilter.StringComparison = StringComparison.CurrentCultureIgnoreCase;
                offerTypeFilter.Filter(offerPages);
            }
            return offerPages;
        }

        private static PageDataCollection GetConsolidatesOffersListForHotel(PageDataCollection offersFromOfferPages,
                                                                     PageDataCollection offersFromHotelPages)
        {
            PageDataCollection offerPages = new PageDataCollection();
            foreach (PageData offerPage in offersFromOfferPages)
            {
                if (!offerPages.Contains(offerPage))
                {
                    offerPages.Add(offerPage);
                }
            }
            foreach (PageData offerPage in offersFromHotelPages)
            {
                if (!offerPages.Contains(offerPage))
                {
                    offerPages.Add(offerPage);
                }
            }
            return offerPages;
        }

        private static PageDataCollection GetAllOffers(PageReference offerContainer, string language)
        {
            int offerPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["OfferPageTypeGUID"])).ID;
            PageDataCollection offerPages = new PageDataCollection();
            ILanguageSelector languageSelector = new LanguageSelector(language);
            PageDataCollection offerCategories = DataFactory.Instance.GetChildren(offerContainer, languageSelector);
            if (offerCategories != null && offerCategories.Count > 0)
            {
                foreach (PageData offerCategoryPage in offerCategories)
                {
                    PageDataCollection offersInCategory = DataFactory.Instance.GetChildren(offerCategoryPage.PageLink, languageSelector);
                    if (offersInCategory != null && offersInCategory.Count > 0)
                    {
                        foreach (PageData offer in offersInCategory)
                        {
                            if (offer.StopPublish > DateTime.Today)
                            {
                                if (offer.PageTypeID == offerPageTypeID)
                                {
                                    offerPages.Add(offer);
                                }
                            }
                        }
                    }
                }
            }
            if (offerPages.Count > 0)
            {
                for (int i = offerPages.Count - 1; i >= 0; i--)
                {
                    PageData p = offerPages[i];
                    if (!p.CheckPublishedStatus(PagePublishedStatus.Published) || !p.QueryDistinctAccess(AccessLevel.Read))
                        offerPages.Remove(offerPages[i]);
                }
            }
            return offerPages;
        }

        /// <summary>
        /// Gets m_AvailabilityController
        /// </summary>
        private AvailabilityController m_AvailabilityController
        {
            get
            {
                if (availabilityController == null)
                {
                    availabilityController = new AvailabilityController();
                }
                return availabilityController;
            }
        }

        /// <summary>
        /// Returns the PageData object of the root page in EPiServer
        /// </summary>
        public static PageData RootPage
        {
            get { return DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess); }
        }

        public static TripAdvisorPlaceListEntity m_TripAdvisorPlaceList
        {
            get { return m_tripAdvisorPlaceList; }
            set { m_tripAdvisorPlaceList = value; }
        }

        public static string ShowSearchHotelWidgetOnPageLoad()
        {
            StringBuilder setShowSearchHotel = new StringBuilder();
            setShowSearchHotel.Append("<script type=\"text/javascript\">");
            setShowSearchHotel.Append("var chDet = $('#yourStayMod05 div.regular div.changedDetail');");
            setShowSearchHotel.Append("var moDet = $('#yourStayMod05 div.regular div.modifyDetail');");
            setShowSearchHotel.Append("var moBrd = $('#yourStayMod05 div.regular div.broadBrd');");
            setShowSearchHotel.Append("chDet.hide();");
            setShowSearchHotel.Append("moDet.show();");
            setShowSearchHotel.Append("moBrd.hide();");
            setShowSearchHotel.Append("var errorMsgBox = $(\"div[id$='errorAlertDiv']\");");
            setShowSearchHotel.Append("if (errorMsgBox.length) {");
            setShowSearchHotel.Append(" for (i = 0; i < 3; i++) {");
            setShowSearchHotel.Append(" $(errorMsgBox).fadeTo('slow', 0.5).fadeTo('slow', 1.0);");
            setShowSearchHotel.Append("}");
            setShowSearchHotel.Append("}");
            setShowSearchHotel.Append("</script>");
            return setShowSearchHotel.ToString();
        }

        /// <summary>
        /// Sets the Charged at label text for multi room booking
        /// </summary>
        /// <param name="roomIterator"></param>
        /// <param name="lblChargedAtHotel"></param>
        /// <param name="isPint">for print/pdf font color red is not set</param>
        public static void SetChargedAtHotelLabel(int roomIterator, HtmlGenericControl lblChargeInfo, bool isPrint, bool isReceipt)
        {
            if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable == null)
                return;

            //if not combo flow then don't show the label
            if (!(WebUtil.IsAnyRoomHavingSaveCategory(HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable) && WebUtil.IsAnyRoomNotHavingSaveCategory(HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable)))
                return;

            SelectedRoomAndRateEntity selectedRoomRate = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[roomIterator] as SelectedRoomAndRateEntity;

            if ((selectedRoomRate == null) || string.IsNullOrEmpty(Reservation2SessionWrapper.PaymentTransactionId)) return;

            if (!RoomRateUtil.IsSaveRateCategory(selectedRoomRate.RateCategoryID))
            {
                if (isPrint)
                    if (isReceipt)
                        lblChargeInfo.InnerHtml = string.Concat(" - ", "<strong class='chargedAtLabelPrint'>", WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/selecthotel/chargedathotel"), "</strong>");
                    else
                        lblChargeInfo.InnerHtml = string.Concat(" - ", "<strong class='chargedAtLabelPrint'>", WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/chargedathotel"), "</strong>");
                else
                    if (isReceipt)
                        lblChargeInfo.InnerHtml = string.Concat(" - ", "<strong class='chargedAtLabelPrint'>", WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/selecthotel/chargedathotel"), "</strong>");
                    else
                        lblChargeInfo.InnerHtml = string.Concat(" - ", "<strong class='chargedAtLabel'>", WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/chargedathotel"), "</strong>");
            }
            else
            {
                if (isPrint)
                    if (isReceipt)
                        lblChargeInfo.InnerHtml = string.Concat(" - ", "<strong class='paidPrint'>", WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/bookingconfirmation/paid"), "</strong>");
                    else
                        lblChargeInfo.InnerHtml = string.Concat(" - ", "<strong class='paidPrint'>", WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/paid"), "</strong>");
                else
                    if (isReceipt)
                        lblChargeInfo.InnerHtml = string.Concat(" - ", "<strong class='paidLabel'>", WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/bookingconfirmation/paid"), "</strong>");
                    else
                        lblChargeInfo.InnerHtml = string.Concat(" - ", "<strong class='paidLabel'>", WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/paid"), "</strong>");
            }
        }

        /// <summary>
        /// Checks whether any one of the room is save category or not
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <returns></returns>
        public static bool IsAnyRoomHavingSaveCategory(Hashtable hashSelectedRoomRateEntity)
        {
            foreach (DictionaryEntry hashEntity in hashSelectedRoomRateEntity)
            {
                var selectedRoomRate = hashEntity.Value as SelectedRoomAndRateEntity;

                if (selectedRoomRate != null)
                {
                    if (RoomRateUtil.IsSaveRateCategory(selectedRoomRate.RateCategoryID))
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Checks whether any one of the room is flex category or not
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="hashSelectedRoomRateEntity"></param>
        /// <returns></returns>
        public static bool IsAnyRoomNotHavingSaveCategory(Hashtable hashSelectedRoomRateEntity)
        {
            foreach (DictionaryEntry hashEntity in hashSelectedRoomRateEntity)
            {
                var selectedRoomRate = hashEntity.Value as SelectedRoomAndRateEntity;

                if (selectedRoomRate != null)
                {
                    if (!RoomRateUtil.IsSaveRateCategory(selectedRoomRate.RateCategoryID))
                        return true;
                }
            }
            return false;

        }

        /// <summary>
        /// Checks whether any one of the room is save category or not
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="hashSelectedRoomRateEntity"></param>
        /// <returns></returns>
        public static bool IsAllRoomHavingSaveCategory(Hashtable hashSelectedRoomRateEntity)
        {
            for (int roomIterator = 0; roomIterator < hashSelectedRoomRateEntity.Count; roomIterator++)
            {
                SelectedRoomAndRateEntity selectedRoomRate = hashSelectedRoomRateEntity[roomIterator] as SelectedRoomAndRateEntity;
                if (RoomRateUtil.IsSaveRateCategory(selectedRoomRate.RateCategoryID))
                    continue;
                else
                    return false;
            }

            return true;
        }

        /// <summary>
        /// checks whether any rooms are having save category or not
        /// </summary>
        /// <returns></returns>
        public static bool IsAnyRoomHavingSaveCategory(List<BookingDetailsEntity> bookingDetailsList)
        {
            foreach (BookingDetailsEntity booking in bookingDetailsList)
            {
                if ((booking.GuestInformation.GuranteeInformation != null) && (booking.GuestInformation.GuranteeInformation.CreditCard != null) &&
                     (!string.IsNullOrEmpty(booking.GuestInformation.GuranteeInformation.CreditCard.CardType)) &&
                    (booking.GuestInformation.GuranteeInformation.CreditCard.CardType.StartsWith
                    (AppConstants.PREPAID_CCTYPE_PREFIX, StringComparison.InvariantCultureIgnoreCase)) &&
                    (booking.GuestInformation.GuranteeInformation.GuranteeType == GuranteeType.PREPAID))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Checks whether any one of the room is flex category or not from booking details list
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="hashSelectedRoomRateEntity"></param>
        /// <returns></returns>
        public static bool IsAnyRoomNotHavingSaveCategory(List<BookingDetailsEntity> bookingDetailsList)
        {
            foreach (BookingDetailsEntity booking in bookingDetailsList)
            {
                RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(booking.HotelRoomRate.RatePlanCode);
                if (!RoomRateUtil.IsSaveRateCategory(rateCategory.RateCategoryId))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// constructs the HTML list from credit card list dictionary
        /// </summary>
        /// <param name="creditCards"></param>
        /// <returns></returns>
        public static string ConstructCreditCardListHTML(Dictionary<string, CreditCardEntity> creditCards)
        {
            StringBuilder creditCardsHTML = new StringBuilder();

            if (creditCards != null && creditCards.Count > 0)
            {
                int i = 1;
                creditCardsHTML.Append("<ul class='creditCardsList'>");

                //cardkey is the credit card OperaID
                foreach (string cardKey in creditCards.Keys)
                {
                    string creditCardNo = GetScanwebCCNoForDisplay(creditCards[cardKey].CardType, creditCards[cardKey].CardNumber);
                    string expiredText = string.Empty;
                    string creditCardItemId = "creditCardItem" + i;

                    //if credit card expiry date less than todays date then display as expired
                    if ((creditCards[cardKey].ExpiryDate.ToDateTime() - DateTime.Now).Days < 0)
                        expiredText = string.Concat("&nbsp;(", WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/Expired"), ")");

                    creditCardsHTML.Append("<li id='");
                    creditCardsHTML.Append(creditCardItemId);
                    creditCardsHTML.Append("'><span class='cardIssuer'>");
                    creditCardsHTML.Append(creditCardNo);
                    creditCardsHTML.Append(expiredText);
                    creditCardsHTML.Append("</span><div class='loginActBtn fltRt cancelBtn'><a TabIndex='322' class='buttonInner fltLft closeroom'");
                    creditCardsHTML.Append(" href=\"javascript:DeleteCreditCard('#");
                    creditCardsHTML.Append(creditCardItemId);
                    creditCardsHTML.Append("',");
                    creditCardsHTML.Append(cardKey);
                    creditCardsHTML.Append(")\">");
                    creditCardsHTML.Append(WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/Remove"));
                    creditCardsHTML.Append("</a></div></li>");
                    i++;
                }
                creditCardsHTML.Append("</ul>");
            }
            else
            {
                creditCardsHTML.Append(WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/NoCreditCards"));
            }
            return creditCardsHTML.ToString();
        }

        /// <summary>
        /// Gives the credit card no for the display purpose
        /// </summary>
        public static string GetScanwebCCNoForDisplay(string operaCreditCardType, string crediCardNumber)
        {
            if (AppConstants.OPERA_SCANWEB_CCTYPE_MAPPING != null)
                return string.Concat(AppConstants.OPERA_SCANWEB_CCTYPE_MAPPING[operaCreditCardType], AppConstants.SPACE,
                                            GetMaskedCreditCardNo(crediCardNumber));
            else
                return GetMaskedCreditCardNo(crediCardNumber);
        }

        /// <summary>
        /// returns masked credit card number
        /// </summary>
        /// <param name="creditCardNumber"></param>
        /// <returns></returns>
        public static string GetMaskedCreditCardNo(string creditCardNumber)
        {
            //return string.Concat(AppConstants.CARD_MASK,
            //    creditCardNumber.Substring((creditCardNumber.Length - AppConstants.CREDIT_CARD_DISPLAY_CHARS)));

            return creditCardNumber;
        }

        /// <summary>
        /// Gets the translated text by reading the value from
        /// the xml files stored in the lang folder.
        /// 
        /// key is the XPATH string starting from the root element
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetTranslatedText(string key)
        {
            var sessionLang = MiscellaneousSessionWrapper.SessionLanguage;
            if (string.IsNullOrEmpty(sessionLang))
            {
                return EPiServer.Core.LanguageManager.Instance.Translate(key);
            }
            else
            {
                return GetTranslatedText(key, sessionLang);
            }
        }

        /// <summary>
        /// Gets the translated text by reading the value from
        /// the xml files stored in the lang folder.
        /// Overloaded to accept the language string to fetch culture specific translations.
        /// key is the XPATH string starting from the root element
        /// </summary>
        /// <param name="key">The key to get the translation string.</param>
        /// <param name="language">The language identifier string.</param>
        /// <returns></returns>
        public static string GetTranslatedText(string key, string language)
        {
            return EPiServer.Core.LanguageManager.Instance.Translate(key, language);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetTranslatedTextForReceipt(string key)
        {
            if (AppConstants.RECEIPT_IN_ENGLISH_ONLY)
                return GetTranslatedText(key, LanguageConstant.LANGUAGE_ENGLISH.ToLower());
            else
                return GetTranslatedText(key);
        }

        /// <summary>
        /// Gets redeem string.
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static string GetRedeemString(double points)
        {
            string redeemString = GetTranslatedText("/bookingengine/booking/selecthotel/redeempoints");
            redeemString = string.Format(redeemString, points);
            return redeemString;
        }

        public static string GetPricePerNight(double rate, int noOfNights)
        {
            string pricePerNight = string.Empty;
            double price = 0.0d;
            if (noOfNights > 0) //This will also prevent the division by zero exception 
                price = rate / noOfNights;

            if (Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings[AppConstants.ENABLE_DECIMAL_RATES]))
                pricePerNight = Convert.ToString(System.Math.Ceiling(price));
            else
                pricePerNight = Convert.ToString(price);
            return pricePerNight;
        }

        /// <summary>
        /// Gets bonus cheque rate string.
        /// </summary>
        /// <param name="langKey"></param>
        /// <param name="year"></param>
        /// <param name="countryCode"></param>
        /// <param name="rate"></param>
        /// <param name="currencyCode"></param>
        /// <param name="noOfNights"></param>
        /// <param name="noOfRooms"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public static string GetBonusChequeRateString(string langKey, int year, string countryCode, double rate,
                                                      string currencyCode, int noOfNights, int noOfRooms, params string[] language)
        {
            string bonusChequeString = string.Empty;
            try
            {
                AvailabilityController availabilityControllerObj = new AvailabilityController();
                double bonusChequeValue = availabilityControllerObj.GetBonusChequeRate(year, countryCode, currencyCode);
                int totalBonusCheques = 0;
                double additionalValue = 0;
                if (language != null && language.Length > 0)
                    bonusChequeString = GetTranslatedText(langKey, language[0]);
                else
                    bonusChequeString = GetTranslatedText(langKey);
                if (rate > 0)
                {
                    totalBonusCheques = noOfNights * noOfRooms;
                    additionalValue = rate - (bonusChequeValue * totalBonusCheques);
                    additionalValue = Math.Round(additionalValue, 2);
                    if (additionalValue < 0) additionalValue = 0;
                }
                bonusChequeString = string.Format(bonusChequeString, totalBonusCheques, additionalValue, currencyCode);
            }
            catch (ContentDataAccessException ex)
            {
                if (string.Equals(ex.ErrorCode, AppConstants.BONUS_CHEQUE_NOT_FOUND))
                {
                    throw ex;
                }
            }
            return bonusChequeString;
        }

        /// <summary>
        /// Gets the bonus cheque rate string.
        /// </summary>
        /// <param name="langKey">The lang key.</param>
        /// <param name="year">The year.</param>
        /// <param name="countryCode">The country code.</param>
        /// <param name="rate">The rate.</param>
        /// <param name="currencyCode">The currency code.</param>
        /// <param name="noOfNights">The no of nights.</param>
        /// <param name="noOfRooms">The no of rooms.</param>
        /// <param name="totalRateNonBC">The total rate non BC.</param>
        /// <param name="totalNonBCRooms">The total non BC rooms.</param>
        /// <param name="language">The language.</param>
        /// <returns>Bonus cheque string</returns>
        /// <remarks>Release 2.0 | 48 Hours need to be available during Bonus cheque booking.</remarks>
        public static string GetBonusChequeRateString(string langKey, int year, string countryCode, double rate,
                                                      string currencyCode, int noOfNights, int noOfRooms,
                                                      double totalRateNonBC, int totalNonBCRooms,
                                                      params string[] language)
        {
            string bonusChequeString = string.Empty;
            try
            {
                AvailabilityController availabilityControllerObj = new AvailabilityController();
                double bonusChequeValue = availabilityControllerObj.GetBonusChequeRate(year, countryCode, currencyCode);

                int totalBonusCheques = noOfNights * (noOfRooms - totalNonBCRooms);
                double additionalValue = rate - (bonusChequeValue * totalBonusCheques);
                additionalValue = Math.Round(additionalValue, 2);
                if (additionalValue < 0) additionalValue = 0;

                additionalValue += totalRateNonBC;

                if (language != null && language.Length > 0)
                    bonusChequeString = GetTranslatedText(langKey, language[0]);
                else
                    bonusChequeString = GetTranslatedText(langKey);
                bonusChequeString = string.Format(bonusChequeString, totalBonusCheques, additionalValue, currencyCode);
            }
            catch (ContentDataAccessException ex)
            {
            }
            return bonusChequeString;
        }

        #region ExcludeStringFromDestination

        /// <summary>
        /// It excludes all pre configured words from the input string 
        /// typed by the user in destination field.
        /// </summary>
        /// <param name="inputDestination"></param>
        /// <returns></returns>
        public static string ExcludeStringFromDestination(string inputDestination)
        {
            inputDestination = inputDestination.Trim().ToLower();
            Scanweb.Core.AppLogger.LogInfoMessage("In ExcludeStringFromDestination : Got the String : " + inputDestination);
            string destinationExclusionString = GetTranslatedText("/bookingengine/booking/searchhotel/destinationexclusion");
            string[] inputExclusions = destinationExclusionString.Split(',');
            for (int i = 0; i < inputExclusions.Length; i++)
            {
                string inputExclusion = inputExclusions[i].Trim().ToLower();
                Scanweb.Core.AppLogger.LogInfoMessage("For Exclusion  : " + inputExclusion);
                if (inputDestination.IndexOf(inputExclusion) != -1)
                {
                    Scanweb.Core.AppLogger.LogInfoMessage("Found the Exclusion  : " + inputExclusion);
                    inputDestination = inputDestination.Replace(inputExclusion, string.Empty).Trim();
                    Scanweb.Core.AppLogger.LogInfoMessage("String after Exclusion  : " + inputDestination);
                }
            }
            return inputDestination;
        }

        #endregion ExcludeStringFromDestination

        #region GetSecureUrl

        /// <summary>
        /// This takes a relative Url and returns the secure Url
        /// by changing the protocol from http to https.  
        /// </summary>
        /// <param name="relativePageUrl"></param>
        /// <returns></returns>
        public static string GetSecureUrl(string relativePageUrl)
        {
            string protocol = string.Empty;
            string hostName = string.Empty;
            StringBuilder secureUrl = new StringBuilder();
            try
            {
                protocol = AppConstants.SECURE_PROTOCOL;
                hostName = HttpContext.Current.Request.Url.Host;
                secureUrl.Append(protocol);
                secureUrl.Append(hostName);
                secureUrl.Append(relativePageUrl);
                return secureUrl.ToString();
            }
            catch (Exception Ex)
            {
                AppLogger.LogFatalException(Ex, "Exception in WebUtils\\GetSecureUrl");
                return string.Empty;
            }
        }

        /// <summary>
        /// returns web site url
        /// </summary>
        /// <returns></returns>
        public static string GetSiteUrl()
        {
            return string.Concat(AppConstants.HTTP_PROTOCOL, HttpContext.Current.Request.Url.Host);
        }

        /// <summary>
        /// This takes a relative Url and returns the secure Url
        /// by changing the protocol from http to https.  
        /// </summary>
        /// <param name="relativePageUrl"></param>
        /// <returns></returns>
        public static string GetCompletePageUrl(string relativePageUrl, bool IspageSecured)
        {
            string protocol = string.Empty;
            string hostName = string.Empty;
            StringBuilder secureUrl = new StringBuilder();
            try
            {
                protocol = (IspageSecured) ? AppConstants.SECURE_PROTOCOL : AppConstants.HTTP_PROTOCOL;
                hostName = HttpContext.Current.Request.Url.Host;
                secureUrl.Append(protocol);
                secureUrl.Append(hostName);
                secureUrl.Append(relativePageUrl);
                return secureUrl.ToString();
            }
            catch (Exception Ex)
            {
                AppLogger.LogFatalException(Ex, "Exception in WebUtils\\GetSecureUrl");
                return string.Empty;
            }
        }

        #endregion GetSecureUrl

        #region LoginValidatorUrl

        /// <summary>
        /// Returns the secure URL which validates the login credentials.
        /// </summary>
        /// <returns>The secure url string.</returns>
        public static string LoginValidatorUrl()
        {
            string protocol = string.Empty;
            string hostName = string.Empty;
            StringBuilder loginValidatorUrl = new StringBuilder();
            try
            {
                protocol = AppConstants.SECURE_PROTOCOL;
                hostName = HttpContext.Current.Request.Url.Host;
                loginValidatorUrl.Append(protocol);
                loginValidatorUrl.Append(hostName);
                loginValidatorUrl.Append(AppConstants.LOGIN_VALIDATOR_PATH);
            }
            catch (Exception Ex)
            {
                AppLogger.LogFatalException(Ex, "Exception in WebUtils\\LoginValidatorUrl");
            }
            return loginValidatorUrl.ToString();
        }

        #endregion LoginValidatorUrl

        #region ShowApplicationError

        /// <summary>
        /// When any unhandled error occurs, then user will be 
        /// redirected to a generic error page.
        /// </summary>
        /// <param name="errHeader"></param>
        /// <param name="errMessage"></param>
        public static string GetErrorPageUrl(string errHeader, string errMessage)
        {
            string url = string.Empty;
            try
            {
                GenericErrorPageSessionWrapper.GenericErrorHeader = errHeader;
                GenericErrorPageSessionWrapper.GenericErrorMessage = errMessage;
                url = GlobalUtil.GetUrlToPage(EpiServerPageConstants.GENERIC_ERROR_PAGE);
                return url;
            }
            catch (Exception Ex)
            {
                AppLogger.LogFatalException(Ex, "Exception in WebUtils\\GetErrorPageUrl");
                return string.Empty;
            }
        }

        /// <summary>
        /// ShowApplicationError
        /// </summary>
        /// <param name="errHeader"></param>
        /// <param name="errMessage"></param>
        public static void ShowApplicationError(string errHeader, string errMessage)
        {
            string url = string.Empty;
            try
            {
                url = GetErrorPageUrl(errHeader, errMessage);
                HttpContext.Current.Response.Redirect(url, false);
            }
            catch (Exception Ex)
            {
                AppLogger.LogFatalException(Ex, "Exception in WebUtils\\ShowApplicationError");
            }
        }

        /// <summary>
        /// Gets application error log.
        /// </summary>
        /// <param name="ex"></param>
        public static void ApplicationErrorLog(Exception ex)
        {
            AppLogger.LogCustomException(ex, AppConstants.APPLICATION_EXCEPTION);
            ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER), WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
        }

        #endregion ShowApplicationError

        public static bool IsSessionValid()
        {
            return IsSessionValid(null, true);
        }

        /// <summary>
        /// This method is used to check whether the session has expired or not.
        /// </summary>
        /// <returns>True, implies that the session is alive.</returns>
        public static bool IsSessionValid(PageData currentPage, bool performRedirect)
        {
            bool performSessionValidation = true;

            if (currentPage != null && string.Equals(currentPage.PageTypeName, PageIdentifier.ScanwebStandardpage, StringComparison.InvariantCultureIgnoreCase) &&
                !IsFGPPageWithValidSession(currentPage))
            {
                performSessionValidation = false;
            }

            if (performSessionValidation && HttpContext.Current.Session != null && HttpContext.Current.Session.IsNewSession)
            {
                string cookieHeader = HttpContext.Current.Request.Headers["Cookie"];
                if (!string.IsNullOrEmpty(cookieHeader) && cookieHeader.IndexOf("ASP.NET_SessionId") >= 0)
                {
                    AppLogger.LogFatalException(new ApplicationException("Session has Expired. Redirecting to error page."));
                    try
                    {
                        if (performRedirect)
                        {
                            if (HttpContext.Current.Response.Cookies["ActiveTab"] != null)
                            {
                                HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab1;
                            }
                            //redirect to current url again with SessionExpired=true querystring parameter so that 
                            //it can be trapped by same ScandicTemplatePage (base class of all web pages), where
                            //it will be redirected to session expiry page.
                            var urlFormat = "{0}?SessionExpired=true";
                            if (HttpContext.Current.Request.RawUrl.IndexOf("?") > 0)
                            {
                                urlFormat = "{0}&SessionExpired=true";
                            }
                            string redirectURL = string.Format(urlFormat, HttpContext.Current.Request.RawUrl);
                            HttpContext.Current.Response.Redirect(redirectURL, false);
                        }
                    }
                    catch
                    {
                    }
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// This method is used to check whether the session has expired or not.
        /// </summary>
        /// <returns>True, implies that the session is alive.</returns>
        public static bool ValidateSession()
        {
            if (HttpContext.Current.Session != null && HttpContext.Current.Session.IsNewSession)
            {
                string cookieHeader = HttpContext.Current.Request.Headers["Cookie"];
                if (!string.IsNullOrEmpty(cookieHeader) && cookieHeader.IndexOf("ASP.NET_SessionId") >= 0)
                {
                    AppLogger.LogFatalException(new ApplicationException("Session has Expired. Redirecting to error page."));
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// This method will store the user name and passwod. 
        /// </summary>
        /// <param name="userName">Membership number</param>
        /// <param name="password">Password</param>
        /// <param name="doStore">store if true else remove it from the cookie</param>
        /// <remarks>artf863183 | Release 1.6.1 | Remember me</remarks>
        public static void RememberUserIdPassword(string userName, string password, bool doStore)
        {
            if (doStore)
            {
                StoreCookie(AppConstants.USERID_COOKIE, userName);
                RemoveCookie(AppConstants.DONTREMEMBERME_COOKIE);
            }
            else
            {
                StoreCookie(AppConstants.DONTREMEMBERME_COOKIE, "false");
                RemoveCookie(AppConstants.USERID_COOKIE);
            }
            RememberMePassword(userName, password, doStore);
            //Remove password Cookie
            RemoveCookie(AppConstants.PASSWORD_COOKIE);
        }

        public static void StoreCookie(string cookieName, string cookieValue)
        {
            HttpCookie cookie = new HttpCookie(cookieName);
            cookie.Value = cookieValue;
            cookie.Expires = DateTime.Now.AddYears(AppConstants.SCANDIC_BOOKING_CODE_EXPIRYYEAR);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void StoreCookie(string cookieName, string cookieValue, string cookieDuration)
        {
            HttpCookie cookie = new HttpCookie(cookieName);
            cookie.Value = cookieValue;
            cookie.Expires = DateTime.Now.AddDays(Convert.ToInt64(cookieDuration));
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void RemoveCookie(string cookieName)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[cookieName];
            if (null != cookie)
            {
                HttpContext.Current.Response.Cookies[cookieName].Expires = DateTime.Now.AddYears(-30);
            }
        }

        public static void RememberMePassword(string userName, string password, bool doStore)
        {
            HttpCookie newPasswordCookie = null;
            string newPasswordCookieId = string.Empty;
            var encryptionKey = GetEncryptionKey(userName);
            var cryptoData = new CryptoData(AppConstants.NEW_PASSWORD_COOKIE);
            var encryptedData = ScanwebCryptography.Instance.Encrypt(cryptoData, encryptionKey);
            if (encryptedData != null && !string.IsNullOrEmpty(encryptedData.Base64))
            {
                newPasswordCookieId = encryptedData.Base64.Replace("=", "");
            }

            if (doStore)
            {
                if (!string.IsNullOrEmpty(newPasswordCookieId))
                {
                    var passwordCyptoData = new CryptoData(password);
                    var encryptedPassword = ScanwebCryptography.Instance.Encrypt(passwordCyptoData, encryptionKey);
                    newPasswordCookie = new HttpCookie(newPasswordCookieId);
                    if (encryptedPassword != null)
                    {
                        newPasswordCookie.Value = encryptedPassword.Base64;
                    }
                    newPasswordCookie.Expires = DateTime.Now.AddYears(AppConstants.SCANDIC_BOOKING_CODE_EXPIRYYEAR);
                    HttpContext.Current.Response.Cookies.Add(newPasswordCookie);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(newPasswordCookieId))
                {
                    newPasswordCookie = HttpContext.Current.Request.Cookies[newPasswordCookieId];
                }
                if (newPasswordCookie != null)
                {
                    HttpContext.Current.Response.Cookies[newPasswordCookieId].Expires = DateTime.Now.AddYears(-30);
                }
            }
        }

        public static void RemovePasswordCookie(string membershipId)
        {
            var encryptionKey = GetEncryptionKey(membershipId);
            var cryptoData = new CryptoData(AppConstants.NEW_PASSWORD_COOKIE);
            var encryptedData = ScanwebCryptography.Instance.Encrypt(cryptoData, encryptionKey);
            if (encryptedData != null && !string.IsNullOrEmpty(encryptedData.Base64))
            {
                var newPasswordCookieId = encryptedData.Base64.Replace("=", "");
                var newPasswordCookie = HttpContext.Current.Request.Cookies[newPasswordCookieId];
                if (newPasswordCookie != null)
                {
                    HttpContext.Current.Response.Cookies[newPasswordCookieId].Expires = DateTime.Now.AddYears(-30);
                }
            }
        }

        public static bool IsCookieExists(string cookieName)
        {
            bool status = false;
            HttpCookie cookie = HttpContext.Current.Request.Cookies[cookieName];
            if (cookie != null)
                status = true;
            return status;
        }

        /// <summary>
        /// Fetches the userID form the cookie.
        /// </summary>
        /// <returns>User id</returns>
        /// <remarks>artf863183 | Release 1.6.1 | Remember me</remarks>
        private static string GetUserId()
        {
            string userId = string.Empty;
            HttpCookie userIdCookie = HttpContext.Current.Request.Cookies[AppConstants.USERID_COOKIE];
            if (null != userIdCookie)
                userId = userIdCookie.Value;
            return userId;
        }

        /// <summary>
        /// Fetches the password form the cookie.
        /// </summary>
        /// <returns>Password</returns>
        /// <remarks>artf863183 | Release 1.6.1 | Remember me</remarks>
        public static string GetPassword(string membershipId)
        {
            string password = string.Empty;
            HttpCookie userIdCookie = HttpContext.Current.Request.Cookies[AppConstants.PASSWORD_COOKIE];
            if (null != userIdCookie)
            {
                password = userIdCookie.Value;
            }
            else
            {
                password = GetEncyptedPasswordFromCookie(membershipId);
            }
            return password;
        }

        public static string GetActualPassword(string enteredPassword, string membershipId)
        {
            string actualPassword = enteredPassword;
            try
            {
                if (!string.IsNullOrEmpty(enteredPassword) && !string.IsNullOrEmpty(membershipId))
                {
                    var passwordFromCookie = GetEncyptedPasswordFromCookie(membershipId);
                    if (string.Equals(passwordFromCookie, enteredPassword))
                    {
                        var encryptionKey = GetEncryptionKey(membershipId);
                        var cryptoData = new CryptoData();
                        ((ICryptoData)cryptoData).Base64 = passwordFromCookie;
                        var decryptedData = ScanwebCryptography.Instance.Decrypt(cryptoData, encryptionKey);
                        if (decryptedData != null)
                        {
                            actualPassword = decryptedData.Text;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Error decrypting the password.");
            }

            return actualPassword;
        }

        public static string GetEncyptedPasswordFromCookie(string membershipId)
        {
            var password = string.Empty;
            var encryptionKey = GetEncryptionKey(membershipId);
            var cryptoData = new CryptoData(AppConstants.NEW_PASSWORD_COOKIE);
            var encryptedData = ScanwebCryptography.Instance.Encrypt(cryptoData, encryptionKey);
            if (encryptedData != null && !string.IsNullOrEmpty(encryptedData.Base64))
            {
                var encryptedPasswordCookieId = encryptedData.Base64.Replace("=", "");
                var encryptedPasswordCookie = HttpContext.Current.Request.Cookies[encryptedPasswordCookieId];
                if (encryptedPasswordCookie != null)
                {
                    password = encryptedPasswordCookie.Value;
                }
            }
            return password;
        }

        public static string GetEncryptionKey(string membershipId)
        {
            string userId = string.Empty;
            if (!string.IsNullOrEmpty(membershipId) && membershipId.Length > 5)
            {
                userId = membershipId.Substring(membershipId.Length - 6);
            }
            else
            {
                userId = membershipId;
            }

            return string.Format("{0}{1}", BASE_ENCRYPTION_KEY, userId);
        }

        /// <summary>
        /// This will assign the userId text box and password text box with the cookie values.
        /// </summary>
        /// <param name="userIdTextBox">User Id textbox</param>
        /// <param name="passwordTextBox">Password textbox</param>
        /// <param name="checkBox">Remember me check box</param>
        /// <remarks>artf863183 | Release 1.6.1 | Remember me</remarks>
        public static bool SetCookieToTextBox(HtmlInputText userIDPrefixTextBox, HtmlInputText userIdTextBox, TextBox passwordTextBox,
                                              HtmlInputCheckBox checkBox)
        {
            bool isCookieValueSet = false;
            string userid = GetUserId();
            string password = GetPassword(userid);
            if (!string.IsNullOrEmpty(userid) && !string.IsNullOrEmpty(password))
            {
                userIDPrefixTextBox.Value = AppConstants.ScanWebMembershipNoPrefix;
                userid = userid.Substring(6, userid.Length - 6);
                userIdTextBox.Value = userid;
                passwordTextBox.Attributes["value"] = password;
                checkBox.Checked = true;
                isCookieValueSet = true;
            }

            return isCookieValueSet;
        }

        /// <summary>
        /// Gets the current time for hotel.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <returns></returns>
        public static DateTime GetCurrentTimeforHotel(double latitude, double longitude)
        {
            DateTime currentDateTime = new DateTime();
            try
            {
                string param1 = "value1";
                string param2 = "value2";
                ASCIIEncoding encoding = new ASCIIEncoding();
                string postData = string.Format("param1={0}&param2={1}", param1, param2);
                byte[] buffer = encoding.GetBytes(postData);
                HttpWebRequest myRequest =
                    (HttpWebRequest)WebRequest.Create("http://ws.geonames.org/timezone?" + "lat="
                                                       + latitude.ToString().Replace(',', '.') + "&lng=" +
                                                       longitude.ToString().Replace(',', '.'));
                myRequest.Method = "POST";

                myRequest.ContentType = "application/x-www-form-urlencoded";
                myRequest.ContentLength = buffer.Length;
                Stream newStream = myRequest.GetRequestStream();
                newStream.Write(buffer, 0, buffer.Length);
                newStream.Close();
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myRequest.GetResponse();
                Stream streamResponse = myHttpWebResponse.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                Char[] readBuffer = new Char[1000];
                int count = streamRead.Read(readBuffer, 0, 1000);
                while (count > 0)
                {
                    String resultData = new String(readBuffer, 0, count);
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(resultData);
                    XmlNodeList list = doc.GetElementsByTagName("time");
                    for (int _i = 0; _i < list.Count; ++_i)
                    {
                        String str = list[_i].InnerText;
                        currentDateTime = DateTime.ParseExact(str, "yyyy-MM-dd HH:mm",
                                                              null);
                    }
                    count = streamRead.Read(readBuffer, 0, 256);
                }
                streamRead.Close();
                streamResponse.Close();
                myHttpWebResponse.Close();
            }
            catch (Exception exp)
            {
                AppLogger.LogFatalException(exp, "Failed to return current time for Hotel in WebUtil.GetCurrentTimeforHotel()");
            }
            return currentDateTime;
        }

        /// <summary>
        /// Converts the URL to locale.
        /// </summary>
        /// <param name="linkURL">The link URL.</param>
        /// <param name="language">The language.</param>
        /// <param name="pageLink">The page link.</param>
        /// <returns>converted url</returns>
        public static string ConvertUrlToLocale(string linkURL, string language, PageReference pageLink)
        {
            string finalUrl = string.Empty;
            string languageSelectionURL = EPiServer.UriSupport.AddLanguageSelection(linkURL, language);
            string fullLanguageSelectionURL = EPiServer.UriSupport.AbsoluteUrlBySettings(languageSelectionURL);

            UrlBuilder finalUrlBuilder = new UrlBuilder(fullLanguageSelectionURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(finalUrlBuilder, pageLink, System.Text.UTF8Encoding.UTF8);
            finalUrl = finalUrlBuilder.ToString();

            string appSettingsKey = string.Format("ScandicSiteHostName{0}", language.ToUpper());
            string hostURL = ConfigurationManager.AppSettings[appSettingsKey] as string;
            Url url = new Url(fullLanguageSelectionURL);

            if (hostURL != null)
            {
                finalUrl = finalUrl.Replace(url.Host, hostURL);
            }
            return finalUrl;
        }

        /// <summary>
        /// Gets the localized version of the day.
        /// </summary>
        /// <param name="date">Date</param>
        /// <returns>Day name in localized version</returns>
        public static string GetDayFromDate(DateTime date)
        {
            string dayOfWeek = GetTranslatedText("/Templates/Scanweb/Units/Static/GuestProgramRedemptionList/Sun");
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    {
                        dayOfWeek = GetTranslatedText("/Templates/Scanweb/Units/Static/GuestProgramRedemptionList/Sun");
                    }
                    break;
                case DayOfWeek.Monday:
                    {
                        dayOfWeek = GetTranslatedText("/Templates/Scanweb/Units/Static/GuestProgramRedemptionList/Mon");
                    }
                    break;
                case DayOfWeek.Tuesday:
                    {
                        dayOfWeek = GetTranslatedText("/Templates/Scanweb/Units/Static/GuestProgramRedemptionList/Tue");
                    }
                    break;
                case DayOfWeek.Wednesday:
                    {
                        dayOfWeek = GetTranslatedText("/Templates/Scanweb/Units/Static/GuestProgramRedemptionList/Wed");
                    }
                    break;
                case DayOfWeek.Thursday:
                    {
                        dayOfWeek = GetTranslatedText("/Templates/Scanweb/Units/Static/GuestProgramRedemptionList/Thu");
                    }
                    break;
                case DayOfWeek.Friday:
                    {
                        dayOfWeek = GetTranslatedText("/Templates/Scanweb/Units/Static/GuestProgramRedemptionList/Fri");
                    }
                    break;
                case DayOfWeek.Saturday:
                    {
                        dayOfWeek = GetTranslatedText("/Templates/Scanweb/Units/Static/GuestProgramRedemptionList/Sat");
                    }
                    break;
            }
            return dayOfWeek;
        }

        /// <summary>
        /// Unblocking Process to Unblock the Room Ingaged
        /// </summary>
        /// <param name="roomBlockReservationNumber">The Reservation NumberRoom Which Need to unblock</param>
        public static void UnblockRoom(string roomBlockReservationNumber)
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if (search != null && !string.IsNullOrEmpty(roomBlockReservationNumber))
            {
                IgnoreBookingEntity ignoreBookingEntity = new IgnoreBookingEntity();
                ignoreBookingEntity.ReservationNumber = roomBlockReservationNumber;
                ignoreBookingEntity.HotelCode = search.SelectedHotelCode;
                ignoreBookingEntity.ChainCode = AppConstants.CHAIN_CODE;
                ReservationController reservationCtrl = new ReservationController();
                reservationCtrl.IgnoreBooking(ignoreBookingEntity);
            }
        }

        /// <summary>
        /// Confirm the passed booking reservation for particular legNumber
        /// Author: RK
        /// </summary>
        /// <param name="roomBlockReservationNumber">The Reservation NumberRoom Which Need to unblock</param>
        public static string ConfirmBooking(string reservationNumber, string legNumber)
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            ConfirmBookingEntity confirmBookingEntity = new ConfirmBookingEntity();
            confirmBookingEntity.ReservationNumber = reservationNumber;
            confirmBookingEntity.LegNumber = legNumber;
            confirmBookingEntity.HotelCode = search.SelectedHotelCode;
            confirmBookingEntity.ChainCode = AppConstants.CHAIN_CODE;
            ReservationController reservationCtrl = new ReservationController();
            return reservationCtrl.ConfirmBooking(confirmBookingEntity);
        }

        /// <summary>
        /// This method clears the selected rate information saved in session and 
        /// also unblocks the rooms if any blocked, on rate selection in selectrate page.
        /// </summary>
        public static void ClearSelectedRateSessionData()
        {
            try
            {
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if (search != null && search.ListRooms != null && search.ListRooms.Count > 0)
                {
                    foreach (HotelSearchRoomEntity room in search.ListRooms)
                    {
                        room.IsSearchDone = false;
                        if (room.IsBlocked)
                        {
                            WebUtil.UnblockRoom(room.RoomBlockReservationNumber);
                            room.RoomBlockReservationNumber = string.Empty;
                            room.IsBlocked = false;
                        }
                    }
                    if (HotelRoomRateSessionWrapper.ListRoomStay != null)
                        HotelRoomRateSessionWrapper.ListRoomStay = null;
                    if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null)
                    {
                        HotelRoomRateSessionWrapper.ListHotelRoomRate = null;
                    }
                    Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                    if (selectedRoomAndRatesHashTable != null)
                    {
                        selectedRoomAndRatesHashTable.Clear();
                        selectedRoomAndRatesHashTable = null;
                    }
                }
            }
            catch (OWSException owsExcpn)
            {
                if (string.Equals(owsExcpn.ErrCode, OWSExceptionConstants.BOOKING_NOT_FOUND_IGNORE, StringComparison.InvariantCultureIgnoreCase))
                {
                    string custMsg = string.Format("OWS Exception | Stack Trace: {0} \n Error Message:{1}", owsExcpn.StackTrace, owsExcpn.Message);
                    AppLogger.LogInfoMessage(custMsg);
                }
                else
                {
                    throw owsExcpn;
                }
            }
        }

        /// <summary>
        /// Clears blocked rooms.
        /// </summary>
        /// <returns></returns>
        public static bool ClearBlockedRooms()
        {
            bool clearStatus = true;
            try
            {
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if (search != null && search.ListRooms != null && search.ListRooms.Count > 0)
                {
                    for (int iListRoom = 0; iListRoom < search.ListRooms.Count; iListRoom++)
                    {
                        if (search.ListRooms[iListRoom].IsBlocked &&
                            (!string.IsNullOrEmpty(search.ListRooms[iListRoom].RoomBlockReservationNumber)) &&
                            (search.ListRooms[iListRoom].IsRoomModifiable))
                        {
                            WebUtil.UnblockRoom(search.ListRooms[iListRoom].RoomBlockReservationNumber);
                            search.ListRooms[iListRoom].RoomBlockReservationNumber = string.Empty;
                            search.ListRooms[iListRoom].IsBlocked = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = String.Format("ClearBlockedRooms Exception: ");
                AppLogger.LogFatalException(ex, errorMessage);
                clearStatus = false;
            }
            return clearStatus;
        }

        /// <summary>
        /// Checks unblock required or not.
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public static bool IsUblockRequired(string hotelID)
        {
            bool unblockRequired = false;
            NameValueCollection nvCollection = new NameValueCollection();
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if (search != null && search.SelectedHotelCode == hotelID)
            {
                if (search.ListRooms != null && search.ListRooms.Count > 0)
                {
                    foreach (HotelSearchRoomEntity room in search.ListRooms)
                    {
                        room.IsSearchDone = false;
                        if (room.IsBlocked)
                        {
                            nvCollection.Add(hotelID, room.RoomBlockReservationNumber);
                        }
                    }
                }
            }
            if (nvCollection.Count > 0)
            {
                SearchCriteriaSessionWrapper.UnblockRoomList = nvCollection;
                unblockRequired = true;
            }
            return unblockRequired;
        }

        /// <summary>
        /// Gets logo path.
        /// </summary>
        /// <returns></returns>
        public static string GetLogoPath()
        {
            if (String.Equals(Reservation2SessionWrapper.CurrentLanguage, CurrentPageLanguageConstant.LANGAUGE_RUSSIAN,
                StringComparison.InvariantCultureIgnoreCase) ||
                String.Equals(Reservation2SessionWrapper.CurrentLanguage, CurrentPageLanguageConstant.LANGUAGE_GERMANY,
                StringComparison.InvariantCultureIgnoreCase))
            {
                return "/Templates/Booking/Styles/Default/Images/Comodo.gif";
            }
            else
            {
                return "/Templates/Booking/Styles/Default/Images/verisign.gif";
            }
        }

        public static string GetExternalUrl(string linkURL)
        {
            UrlBuilder url = new UrlBuilder(linkURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, linkURL, System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }
        public static void GetBenefitsImageUrl(PageData currentPage, string benefitImageName, System.Web.UI.WebControls.Image benefitImage, int imageWidth)
        {
            if (currentPage[benefitImageName] != null)
            {
                string imageString = currentPage[benefitImageName] as string;
                benefitImage.ImageUrl = GetImageVaultImageUrl(imageString, imageWidth);
            }
        }


        public static string GetImageVaultImageUrl(string imageName, int imageWidth)
        {
            var imageUrl = string.Empty;
            var ub = !string.IsNullOrEmpty(imageName) ? ImageStoreNET.Classes.Util.UrlBuilder.ParseUrl(imageName) : null;
            if (ub != null)
            {
                ub.Width = imageWidth;
                ub.PreserveAspectRatio = true;
                var lowercaseImageString = imageName.ToLower();
                if (lowercaseImageString.Contains(".tif") || lowercaseImageString.Contains(".tiff"))
                {
                    ub.Filename = null;
                    ub.ConversionFormatType = ImageStoreNET.Classes.Data.ConversionFormatTypes.Jpeg;
                }
                else
                {
                    ub.ConversionFormatType = ImageStoreNET.Classes.Data.ConversionFormatTypes.WebSafe;
                }
                imageUrl = ub.ToString();
            }
            return imageUrl;
        }

        public static string GetImageAltText(string imagePath)
        {
            var imageAltText = string.Empty;

            if (!string.IsNullOrEmpty(imagePath))
            {
                var imageInfo = imagePath.Split('~');
                if (imageInfo.Length > 2)
                {
                    imageAltText = imageInfo[imageInfo.Length - 1];
                }

            }

            return imageAltText;
        }
        public static bool IsFGPPageWithValidSession(PageData currentPage)
        {
            bool bReturn = false;
            var pageData = currentPage;
            while (!bReturn)
            {
                if (string.Equals(pageData.PageTypeName, PageIdentifier.FGPOverviewPage, StringComparison.InvariantCultureIgnoreCase))
                    bReturn = true;
                else if (string.Equals(pageData.PageTypeName, PageIdentifier.ScanwebStartpage, StringComparison.InvariantCultureIgnoreCase))
                    break;
                else
                    pageData = DataFactory.Instance.GetPage(pageData.ParentLink);

            }
            return bReturn;
        }

        public static string CreateTripAdvisorRatingWidget(int PlaceID)
        {
            if (m_tripAdvisorPlaceList == null)
                m_tripAdvisorPlaceList = TripAdvisorRatingFeedManager.LoadTripAdvisorRatings();

            if (m_tripAdvisorPlaceList != null && m_tripAdvisorPlaceList.TAPlaces != null && m_tripAdvisorPlaceList.TAPlaces.Count > 0)
            {
                TripAdvisorPlaceEntity TAplace = m_tripAdvisorPlaceList.TAPlaces.Where(o => Convert.ToInt32(o.Id) == PlaceID).SingleOrDefault();
                if (TAplace != null && TAplace.ReviewCount > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<p id='lblTATravellerRating'>");
                    sb.Append(WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/TripAdvisorRating"));
                    sb.Append("</p>");
                    sb.Append("<img src='" + TAplace.TripAdvisorImageURL + "'/>");
                    sb.Append("<p id='lblTAReviewsCount'>");
                    sb.Append(WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/TripAdvisorRatingBasedOn"));
                    sb.Append("<a id='lnkReviewCount' class='tripReviewcount' href='" + CreateRatingPopupURL(PlaceID, Utility.CurrentLanguage) + "'>");
                    ///sb.Append(AppConstants.SPACE);
                    sb.Append(WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/TripAdvisorRatingCountLinkText").Replace("{0}", Convert.ToString(TAplace.ReviewCount)) + "</a>");
                    sb.Append("</p>");
                    return sb.ToString();
                }
                else
                    return "";
            }
            else
                return "";
        }

        /// <summary>
        /// Returns the Trip Advisor place Entity object for a Hotel
        /// </summary>
        /// <param name="hotelPlaceID">Hotel Place ID</param>
        /// <returns>TripAdvisorPlaceEntity</returns>
        public static TripAdvisorPlaceEntity GetTAReviewEntityForHotel(int hotelPlaceID)
        {
            TripAdvisorPlaceEntity TAplace = null;

            if (m_tripAdvisorPlaceList == null)
                m_tripAdvisorPlaceList = TripAdvisorRatingFeedManager.LoadTripAdvisorRatings();

            if (m_tripAdvisorPlaceList != null && m_tripAdvisorPlaceList.TAPlaces != null && m_tripAdvisorPlaceList.TAPlaces.Count > 0)
            {
                TAplace = m_tripAdvisorPlaceList.TAPlaces.Where(o => Convert.ToInt32(o.Id) == hotelPlaceID).SingleOrDefault();
            }
            return TAplace;
        }
        /// <summary>
        /// Creates the URL for Trip Advisor Review image
        /// </summary>
        /// <param name="locationID">Location ID of review hotel </param>
        /// <param name="type">defines type if URL is for Web or Mobile</param>
        /// <returns></returns>
        public static string CreateRatingPopupURL(int locationID, string lang)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(ConfigurationManager.AppSettings[AppConstants.TripAdvisorRatingPopupBaseUrl]);
            sb.Append("locationId=" + Convert.ToString(locationID));
            sb.Append("&partnerId=" + ConfigurationManager.AppSettings[AppConstants.TripAdvisorRatingPartnerID]);
            sb.Append("&display=true");
            sb.Append("&lang=" + lang);
            return sb.ToString();
        }


        public static string GetTAImageURLForCurrentCulture(Dictionary<string, string> TAUrl)
        {
            string currentLanguage;
            string url = string.Empty;
            if (!string.IsNullOrEmpty(CMSSessionWrapper.CurrentLanguage))
            {
                currentLanguage = CMSSessionWrapper.CurrentLanguage;
            }
            else
            {
                currentLanguage = AppConstants.TRIP_ADVISOR_DEFAULT_URL_LANG;
            }
            switch (currentLanguage.ToUpper())
            {
                case LanguageConstant.LANGUAGE_SWEDISH:  //sv
                    url = SetTAImageURL(TAUrl, LanguageConstant.LANGUAGE_SWEDISH);
                    break;
                case LanguageConstant.LANGUAGE_DANISH:  //da
                    url = SetTAImageURL(TAUrl, LanguageConstant.LANGUAGE_DANISH);
                    break;
                case LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION:  //no
                    url = SetTAImageURL(TAUrl, LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION);
                    break;
                case LanguageConstant.LANGUAGE_FINNISH: //fi
                    url = SetTAImageURL(TAUrl, LanguageConstant.LANGUAGE_FINNISH);
                    break;
                case LanguageConstant.LANGUAGE_ENGLISH:  //en
                    url = SetTAImageURL(TAUrl, AppConstants.TRIP_ADVISOR_DEFAULT_URL_LANG);
                    break;
                case LanguageConstant.LANGUAGE_RUSSIA:  //ru
                    url = SetTAImageURL(TAUrl, LanguageConstant.LANGUAGE_RUSSIA);
                    break;
                case LanguageConstant.LANGUAGE_GERMAN:  //de
                    url = SetTAImageURL(TAUrl, LanguageConstant.LANGUAGE_GERMAN);
                    break;
                default:
                    url = Convert.ToString(TAUrl[AppConstants.TRIP_ADVISOR_DEFAULT_URL_LANG]);
                    break;
            }
            return url;
        }

        private static string SetTAImageURL(Dictionary<string, string> TAUrl, string key)
        {
            string url = string.Empty;
            if (TAUrl.ContainsKey(key))
                url = Convert.ToString(TAUrl[key]);
            else
                url = Convert.ToString(TAUrl[AppConstants.TRIP_ADVISOR_DEFAULT_URL_LANG]);
            return url;
        }
        /// <summary>
        /// If Trip Advisor review widget needs to be shown 
        /// </summary>
        /// <param name="isCityRatingON">if City Display is ON in CMS</param>
        /// <param name="isHotelRatingON">if Hotel Display is ON in CMS</param>
        /// <returns>bool balue</returns>
        public static bool HideTARating(bool isCityRatingOff, bool isHotelRatingOff)
        {
            bool isRatingOff = true;
            if (!isCityRatingOff)
                if (!isHotelRatingOff)
                    isRatingOff = false;
            return isRatingOff;
        }

        public static double CalculateCurrentLocationDistance(double sLatitude, double sLongitude, double eLatitude, double eLongitude)
        {
            var sLatitudeRadians = sLatitude * (Math.PI / 180.0);
            var sLongitudeRadians = sLongitude * (Math.PI / 180.0);
            var eLatitudeRadians = eLatitude * (Math.PI / 180.0);
            var eLongitudeRadians = eLongitude * (Math.PI / 180.0);

            var dLongitude = eLongitudeRadians - sLongitudeRadians;
            var dLatitude = eLatitudeRadians - sLatitudeRadians;

            var result1 = Math.Pow(Math.Sin(dLatitude / 2.0), 2.0) +
                          Math.Cos(sLatitudeRadians) * Math.Cos(eLatitudeRadians) *
                          Math.Pow(Math.Sin(dLongitude / 2.0), 2.0);

            // Using 3956 as the number of miles around the earth
            // Using 6371 as the number of Kms around the earth
            var result2 = 6371.0 * 2.0 *
                          Math.Atan2(Math.Sqrt(result1), Math.Sqrt(1.0 - result1));

            return result2;
        }

        public static string GetViewModifyDLRedirectLink(string host, string dlredirectText, string viewModifyLinkText,
            string reservationNo, string lastNameText, string last_Name)
        {
            return string.Format("{0}/{1}?{2}={3}&{4}={5}", host, dlredirectText, viewModifyLinkText, reservationNo, lastNameText, last_Name);
        }

        public static bool IsBookingAfterSixPM(HotelSearchEntity hotelSearch)
        {
            const int SIXPMTIME = 18;
            TimeSpan timeSpan = new TimeSpan();
            bool isSIXPMBooking = false;
            if (hotelSearch != null && hotelSearch.ArrivalDate != null &&
                SearchCriteriaSessionWrapper.SearchCriteria != null && !string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode))
            {
                DateTime currentTimeforHotel = new DateTime();
                currentTimeforHotel = TimeZoneManager.Instance.GetHotelCurrentTime(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode);
                if (currentTimeforHotel != DateTime.MinValue)
                {
                    int result = DateTime.Compare(hotelSearch.ArrivalDate.Date, currentTimeforHotel.Date);
                    if (result == 0)
                    {
                        timeSpan = currentTimeforHotel.TimeOfDay;
                        if (timeSpan.Hours > SIXPMTIME || (timeSpan.Hours == SIXPMTIME && timeSpan.Minutes > 0))
                        {
                            isSIXPMBooking = true;
                        }
                    }
                }
            }
            return isSIXPMBooking;
        }

        public static bool CheckUnPublishedPage(PageData currentPage, System.Web.UI.Page currentWebPage)
        {
            if ((currentPage != null) && (currentPage.PendingPublish || currentPage.StopPublish <= DateTime.Now))
            {
                if (!IsEditOrPreviewMode())
                {
                    //Checking if unpublished pages are having custom redirects defined
                    NotFoundPageUtil.HandleOnLoad(currentWebPage, currentWebPage.Request.RawUrl,
                                                  currentWebPage.Request.UrlReferrer != null
                                                      ? currentWebPage.Request.UrlReferrer.AbsoluteUri
                                                    : "");

                    //Status code 404 will be set inside HandleOnLoad function if there no mapping for unpublished page
                    if (currentWebPage.Response.StatusCode == 404)
                    {
                        currentWebPage.Server.Transfer("/404/404notfound.aspx", false);
                    }
                    return true;
                }
            }
            return false;
        }

        public static bool IsEditOrPreviewMode()
        {
            if (HttpContext.Current.Request.QueryString["id"] != null)
            {
                PageReference pageVersionReference = PageReference.Parse(HttpContext.Current.Request.QueryString["id"]);

                if ((pageVersionReference != null && pageVersionReference.WorkID > 0) || (HttpContext.Current.Request.UrlReferrer != null &&
                    Regex.IsMatch(HttpContext.Current.Request.UrlReferrer.LocalPath, "/admin/|/edit/", RegexOptions.IgnoreCase)))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Verifies whether Arrival date and Departure date are valid or not.
        /// </summary>
        public static bool IsValidDates(string arrDate, string depDate)
        {
            bool result = false;
            var endDateOfArrival = DateTime.Today.AddDays(Convert.ToInt32(AppConstants.ALLOWED_DAYS_FOR_DATE_OF_ARRIVAL));
            var allowedDepartureDate = Core.DateUtil.StringToDDMMYYYDate(arrDate).AddDays(99);

            if (Core.DateUtil.StringToDDMMYYYDate(arrDate).Subtract(DateTime.Today).Days >= 0 &&
                Core.DateUtil.StringToDDMMYYYDate(arrDate) <= endDateOfArrival &&
                Core.DateUtil.StringToDDMMYYYDate(depDate) <= endDateOfArrival &&
                Core.DateUtil.StringToDDMMYYYDate(depDate) <= allowedDepartureDate)
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        ///Validates whether the given campaign code is a FamilyAndFriends Dnumber or not
        /// </summary>
        /// <param name="dNumber"></param>
        /// <returns>true, if the dNumber is a valid FamilyAndFriendsDNumber</returns>
        public static bool IsFamilyandFriendsDnumber(string dNumber)
        {
            bool result = false;

            if (!string.IsNullOrEmpty(AppConstants.FAMILTY_AND_FRIENDS_DNUMBER) && AppConstants.FAMILTY_AND_FRIENDS_DNUMBER.Contains(";" + dNumber + ";"))
                result = true;

            return result;
        }

        /// <summary>
        /// Validates whether the incoming request is from Scandic's Intranet or not
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns>true, if the request is from Scandic Intranet</returns>
        public static bool IsRequestedSourceValidForFamilyAndFriendsBooking(List<string> lstIpAddresses)
        {
            bool retVal = false;

            if (lstIpAddresses != null && lstIpAddresses.Count > 0 && !string.IsNullOrEmpty(AppConstants.VALID_INTRANET_IP_ADDRESSES_FOR_FAMILY_AND_FRIENDS_BOOKING))
            {
                foreach (string ipAddress in lstIpAddresses)
                {
                    if (AppConstants.VALID_INTRANET_IP_ADDRESSES_FOR_FAMILY_AND_FRIENDS_BOOKING.Contains(";" + ipAddress + ";"))
                    {
                        retVal = true;
                        break;
                    }
                }
                
            }
                

            return retVal;
        }

        /// <summary>
        /// Redirects to Homepage if the given campaignCode is FamilyAndFriends Dnumber, but the requested source is not valid.
        /// </summary>
        /// <param name="campaignCode"></param>
        public static void ValidateCampaignCodeForFamilyAndFriendsDNumber(string campaignCode)
        {
            List<string> strIpAddresses = new List<string>();

            if (HttpContext.Current.Request.Headers["X-Forwarded-For"] != null)
                strIpAddresses = HttpContext.Current.Request.Headers["X-Forwarded-For"].Split(',').ToList();

            if (!string.IsNullOrEmpty(campaignCode) && IsFamilyandFriendsDnumber(campaignCode) && !IsRequestedSourceValidForFamilyAndFriendsBooking(strIpAddresses))
                HttpContext.Current.Response.Redirect(string.Format("{0}?ErrCode=D1", GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE)));
        }
    }

    /// <summary>
    /// Comparer class to compare the last published version of the pages and oder it in descending.
    /// </summary>
    public class VersionDateComparer : IComparer<PageData>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(PageData first, PageData second)
        {
            PageVersion pv1 = PageVersion.LoadPublishedVersion(first.PageLink, EPiServer.Globalization.ContentLanguage.PreferredCulture.Name);
            PageVersion pv2 = PageVersion.LoadPublishedVersion(second.PageLink, EPiServer.Globalization.ContentLanguage.PreferredCulture.Name);
            return pv2.Saved.CompareTo(pv1.Saved);
        }
    }
}