//  Description					: Template Class for maintaing the templates 
//                                for various communications
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Shankar Dasgupta                                  	  //
//  Author email id				:                           							  //
//  Creation Date				: 14th November  2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces
using System.IO;
using System.Reflection;
using System.Xml;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// This class helps to create different types of templates used for commmunication
    /// </summary>
    public class TemplateManager
    {
        #region Public Methods
        /// <summary>
        ///Get the lostCard Email Template 
        /// </summary>
        /// <returns></returns>
        public static EmailEntity GetForgottenPasswordEmailTemplate()
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["ForgottenPasswordEmailTemplate"];
            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.FORGOTTEN_PASSWORD_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailForgottenPasswordTemplateXML();
                        break;
                    }
                case TemplateType.FORGOTTEN_PASSWORD_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailForgottenPasswordTemplateXML();
                        break;
                    }
            }
            return emailEntity;
        }

        private static EmailEntity ReadEmailForgottenPasswordTemplateXML()
        {
            string templateFileName =
                 Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.FORGOTTEN_PASSWORD_CONFIRMATION_EMAIL_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();
            XmlNode bodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.FORGOTTEN_PASSWORD_CONFIRMATION_EMAIL_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;
            XmlNode subjectNode =
                templateXmlDocument.SelectSingleNode(TemplateType.FORGOTTEN_PASSWORD_CONFIRMATION_EMAIL_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;
            XmlNode senderNode =
                templateXmlDocument.SelectSingleNode(TemplateType.FORGOTTEN_PASSWORD_CONFIRMATION_EMAIL_SENDER);
            emailEntity.Sender = senderNode.InnerText;

            return emailEntity;
        }

        /// <summary>
        ///Get the lostCard Email Template 
        /// </summary>
        /// <returns></returns>
        public static EmailEntity GetLostCardEmailTemplate()
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["LostCardConfrimationEmailTemplate"];
            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.LOSTCARD_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailLostCardTemplateXML();
                        break;
                    }
                case TemplateType.LOSTCARD_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailLostCardTemplateXML();
                        break;
                    }
            }
            return emailEntity;
        }

        /// <summary>
        ///Get the lostCard Email Template 
        /// </summary>
        /// <param name="fromAddress"></param>
        /// <returns></returns>
        public static EmailEntity GetLostCardEmailTemplateToUser(string fromAddress)
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["LostCardConfrimationEmailTemplate"];
            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.LOSTCARD_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailLostCardToUserTemplateXML(fromAddress);
                        break;
                    }
                case TemplateType.LOSTCARD_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailLostCardToUserTemplateXML(fromAddress);
                        break;
                    }
            }
            return emailEntity;
        }

        private static EmailEntity ReadEmailLostCardToUserTemplateXML(string toAddress)
        {
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.LOSTCARD_CONFIRMATION_EMAIL_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();
            XmlNode bodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARD_CONFIRMATION_EMAIL_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;
            XmlNode subjectNode = templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARD_CONFIRMATION_EMAIL_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;

            XmlNode sender = templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARD_CONFIRMATION_EMAIL_SENDER);
            emailEntity.Sender = sender.InnerText;

            string[] recipientList = new string[1];
            recipientList[0] = toAddress;
            emailEntity.Recipient = recipientList;

            return emailEntity;
        }

        private static EmailEntity ReadEmailLostCardTemplateXML()
        {
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.LOSTCARD_CONFIRMATION_SC_EMAIL_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();
            XmlNode bodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARD_CONFIRMATION_SC_EMAIL_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;
            XmlNode subjectNode =
                templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARD_CONFIRMATION_SC_EMAIL_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;

            XmlNode recipient =
                templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARD_CONFIRMATION_SC_EMAIL_RECIEVER);

            string[] emailListRecipient = recipient.InnerText.Split(new char[] { ';' });
            emailEntity.Recipient = emailListRecipient;

            XmlNode sender = templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARD_CONFIRMATION_SC_EMAIL_SENDER);
            emailEntity.Sender = sender.InnerText;

            return emailEntity;
        }

        /// <summary>
        /// Gets invite a friend confirmation email template
        /// </summary>
        /// <returns></returns>
        public static EmailEntity GetInviteAFriendConfirmationEmailTemplate()
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["InviteAFriendConfrimationEmailTemplate"];
            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.INVITEAFRIEND_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailInviteAFriendTemplateXML();
                        break;
                    }
                case TemplateType.INVITEAFRIEND_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailInviteAFriendTemplateXML();
                        break;
                    }
            }
            return emailEntity;
        }

        /// <summary>
        /// Gets invite a friend to user confirmation.
        /// </summary>
        /// <param name="toAddress"></param>
        /// <returns></returns>
        public static EmailEntity GetInviteAFriendToUserConfirmationEmailTemplate(string toAddress)
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["InviteAFriendConfrimationEmailTemplate"];
            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.INVITEAFRIEND_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailInviteAFriendToUserTemplateXML(toAddress);
                        break;
                    }
                case TemplateType.INVITEAFRIEND_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailInviteAFriendToUserTemplateXML(toAddress);
                        break;
                    }
            }
            return emailEntity;
        }

        private static EmailEntity ReadEmailInviteAFriendToUserTemplateXML(string toAddress)
        {
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.INVITEAFRIEND_CONFIRMATION_EMAIL_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();
            XmlNode bodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.INVITEAFRIEND_CONFIRMATION_EMAIL_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;
            XmlNode subjectNode =
                templateXmlDocument.SelectSingleNode(TemplateType.INVITEAFRIEND_CONFIRMATION_EMAIL_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;
            XmlNode sender =
                templateXmlDocument.SelectSingleNode(TemplateType.INVITEAFRIEND_CONFIRMATION_TEMPLATE_TYPE_SENDER);
            emailEntity.Sender = sender.InnerText;

            string[] recipientList = new string[1];
            recipientList[0] = toAddress;
            emailEntity.Recipient = recipientList;

            return emailEntity;
        }

        private static EmailEntity ReadEmailInviteAFriendTemplateXML()
        {
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.INVITEAFRIEND_CONFIRMATION_EMAIL_SC_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();
            XmlNode bodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.INVITEAFRIEND_CONFIRMATION_EMAIL_SC_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;
            XmlNode subjectNode =
                templateXmlDocument.SelectSingleNode(TemplateType.INVITEAFRIEND_CONFIRMATION_EMAIL_SC_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;
            XmlNode mulitpleFriendNode =
                templateXmlDocument.SelectSingleNode(
                    TemplateType.INVITEAFRIEND_CONFIRMATION_EMAIL_SC_PLACEHOLDERTEMPLATE);
            emailEntity.PlaceHolder = mulitpleFriendNode.InnerText;

            XmlNode recipient =
                templateXmlDocument.SelectSingleNode(TemplateType.INVITEAFRIEND_CONFIRMATION_SC_TEMPLATE_TYPE_RECIEVER);
            string[] recipientList = recipient.InnerText.Split(new char[] { ';' });
            emailEntity.Recipient = recipientList;

            XmlNode sender =
                templateXmlDocument.SelectSingleNode(TemplateType.INVITEAFRIEND_CONFIRMATION_SC_TEMPLATE_TYPE_SENDER);
            emailEntity.Sender = sender.InnerText;

            return emailEntity;
        }

        /// <summary>
        /// Get the lostCardWithoutLogin Email Template
        /// </summary>
        /// <param name="fromAddress"></param>
        /// <returns></returns>
        public static EmailEntity GetLostCardNoLogInEmailTemplate(string fromAddress)
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["LostCardNoLoginConfrimationEmailTemplate"];
            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.LOSTCARDNOLOGIN_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailLostCardNoLogInTemplateXML(fromAddress);
                        break;
                    }
                case TemplateType.LOSTCARDNOLOGIN_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailLostCardNoLogInTemplateXML(fromAddress);
                        break;
                    }
            }
            return emailEntity;
        }

        private static EmailEntity ReadEmailLostCardNoLogInTemplateXML(string toAddress)
        {
            string templateFileName =
                  Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.LOSTCARDNOLOGIN_CONFIRMATION_EMAIL_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();
            XmlNode bodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARDNOLOGIN_CONFIRMATION_EMAIL_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;

            XmlNode subjectNode =
                templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARDNOLOGIN_CONFIRMATION_EMAIL_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;
            XmlNode sender = templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARDNOLOGIN_CONFIRMATION_EMAIL_SENDER);
            emailEntity.Sender = sender.InnerText;
            if (toAddress != string.Empty)
            {
                string[] emailListRecipient = new string[1];
                emailListRecipient[0] = toAddress;
                emailEntity.Recipient = emailListRecipient;
            }
            return emailEntity;
        }

        /// <summary>
        ///Get the lostCardWithoutLogin Email Template 
        /// </summary>
        /// <returns></returns>
        public static EmailEntity GetLostCardNoLogInSCEmailTemplate()
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["LostCardNoLoginConfrimationEmailTemplate"];
            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.LOSTCARDNOLOGIN_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailLostCardNoLogInSCTemplateXML();
                        break;
                    }
                case TemplateType.LOSTCARDNOLOGIN_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailLostCardNoLogInSCTemplateXML();
                        break;
                    }
            }
            return emailEntity;
        }

        /// <summary>
        /// Get the ContactUs Email Template 
        /// </summary>
        /// <returns>EmaiEntity</returns>
        public static EmailEntity GetContactUsSCEmailTemplate(string fromAddress, bool isChecked)
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["ContactUSConfrimationEmailTemplate"];
            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.CONTACTUS_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailContactUsSCTemplateXML(fromAddress, isChecked);
                        break;
                    }
                case TemplateType.CONTACTUS_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailContactUsSCTemplateXML(fromAddress, isChecked);
                        break;
                    }
            }
            return emailEntity;
        }

        /// <summary>
        /// Get the ContactUs Email Template 
        /// </summary>
        /// <returns>EmaiEntity</returns>
        public static EmailEntity GetContactUsEmailTemplate(string toAddress)
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["ContactUSConfrimationEmailTemplate"];
            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.CONTACTUS_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailContactUsTemplateXML(toAddress);
                        break;
                    }
                case TemplateType.CONTACTUS_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailContactUsTemplateXML(toAddress);
                        break;
                    }
            }
            return emailEntity;
        }

        /// <summary>
        /// This Parse the Contact Us Template XML  
        /// </summary>
        /// <returns>EmailEntity</returns>
        private static EmailEntity ReadEmailContactUsTemplateXML(string toAddress)
        {
            string templateFileName =
                  Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.CONTACTUS_CONFIRMATION_EMAIL_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();
            XmlNode bodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.CONTACTUS_CONFIRMATION_EMAIL_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;
            XmlNode subjectNode = templateXmlDocument.SelectSingleNode(TemplateType.CONTACTUS_CONFIRMATION_EMAIL_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;

            XmlNode sender = templateXmlDocument.SelectSingleNode(TemplateType.CONTACTUS_CONFIRMATION_EMAIL_SENDER);
            emailEntity.Sender = sender.InnerText;
            string[] emailListRecipient = new string[1];
            emailListRecipient[0] = toAddress;
            emailEntity.Recipient = emailListRecipient;


            return emailEntity;
        }

        /// <summary>
        /// GetMissingPointConfirmationEmailTemplate
        /// </summary>
        /// <param name="fromAddress"></param>
        /// <returns></returns>
        public static EmailEntity GetMissingPointConfirmationEmailTemplate(string fromAddress)
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["MissinPointConfrimationEmailTemplate"];
            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.MISSING_POINT_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailMissingPointTemplateXML(fromAddress);
                        break;
                    }
                case TemplateType.MISSING_POINT_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailMissingPointTemplateXML(fromAddress);
                        break;
                    }
            }
            return emailEntity;
        }

        private static EmailEntity ReadEmailMissingPointTemplateXML(string toAddress)
        {
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.MISSING_POINT_CONFIRMATION_EMAIL_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();
            XmlNode bodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.MISSING_POINT_CONFIRMATION_EMAIL_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;
            XmlNode subjectNode =
                templateXmlDocument.SelectSingleNode(TemplateType.MISSING_POINT_CONFIRMATION_EMAIL_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;
            XmlNode sender = templateXmlDocument.SelectSingleNode(TemplateType.MISSING_POINT_CONFIRMATION_EMAIL_SENDER);
            emailEntity.Sender = sender.InnerText;

            string[] emailListRecipient = new string[1];
            emailListRecipient[0] = toAddress;

            emailEntity.Recipient = emailListRecipient;

            return emailEntity;
        }

        /// <summary>
        /// Gets missing points sc confirmation email template
        /// </summary>
        /// <returns></returns>
        public static EmailEntity GetMissingPointSCConfirmationEmailTemplate()
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["MissinPointConfrimationEmailTemplate"];
            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.MISSING_POINT_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailMissingPointSCTemplateXML();
                        break;
                    }
                case TemplateType.MISSING_POINT_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailMissingPointSCTemplateXML();
                        break;
                    }
            }
            return emailEntity;
        }

        private static EmailEntity ReadEmailLostCardNoLogInSCTemplateXML()
        {
            string collatedScandicAddressTo = string.Empty;
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.LOSTCARDNOLOGIN_CONFIRMATION_SC_EMAIL_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();
            XmlNode bodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARDNOLOGIN_CONFIRMATION_SC_EMAIL_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;
            XmlNode subjectNode =
                templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARDNOLOGIN_CONFIRMATION_SC_EMAIL_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;

            XmlNode reciever =
                templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARDNOLOGIN_CONFIRMATION_SC_EMAIL_RECIEVER);
            string[] emailListRecipient = reciever.InnerText.Split(new char[] { ';' });
            emailEntity.Recipient = emailListRecipient;

            XmlNode sender =
                templateXmlDocument.SelectSingleNode(TemplateType.LOSTCARDNOLOGIN_CONFIRMATION_SC_EMAIL_SENDER);
            emailEntity.Sender = sender.InnerText;

            return emailEntity;
        }

        /// <summary>
        /// This Parse the Contact Us Template XML  
        /// </summary>
        /// <returns>EmailEntity</returns>
        private static EmailEntity ReadEmailContactUsSCTemplateXML(string fromAddress, bool isChecked)
        {
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.CONTACTUS_CONFIRMATION_SC_EMAIL_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();
            XmlNode bodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.CONTACTUS_CONFIRMATION_SC_EMAIL_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;
            XmlNode subjectNode =
                templateXmlDocument.SelectSingleNode(TemplateType.CONTACTUS_CONFIRMATION_SC_EMAIL_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;

            XmlNode recipient =
                templateXmlDocument.SelectSingleNode(TemplateType.CONTACTUS_CONFIRMATION_SC_EMAIL_RECIEVER);
            string[] emailListRecipient = recipient.InnerText.Split(new char[] { ';' });
            emailEntity.Recipient = emailListRecipient;

            if (isChecked)
            {
                string[] carbonCopy = new string[1];
                carbonCopy[0] = fromAddress;
                emailEntity.CarbonCopy = carbonCopy;
            }

            XmlNode sender = templateXmlDocument.SelectSingleNode(TemplateType.CONTACTUS_CONFIRMATION_SC_EMAIL_SENDER);
            emailEntity.Sender = sender.InnerText;

            return emailEntity;
        }

        private static EmailEntity ReadEmailMissingPointSCTemplateXML()
        {
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.MISSING_POINT_CONFIRMATION_SC_EMAIL_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();
            XmlNode bodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.MISSING_POINT_CONFIRMATION_SC_EMAIL_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;
            XmlNode subjectNode =
                templateXmlDocument.SelectSingleNode(TemplateType.MISSING_POINT_CONFIRMATION_SC_EMAIL_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;
            XmlNode recipient =
                templateXmlDocument.SelectSingleNode(TemplateType.MISSING_POINT_CONFIRMATION_SC_EMAIL_RECIEVER);
            string[] recipientList = recipient.InnerText.Split(new char[] { ';' });
            emailEntity.Recipient = recipientList;

            XmlNode sender =
                templateXmlDocument.SelectSingleNode(TemplateType.MISSING_POINT_CONFIRMATION_SC_EMAIL_SENDER);
            emailEntity.Sender = sender.InnerText;

            return emailEntity;
        }


        /// <summary>
        /// Get the BookingConfirmation Email Template 
        /// </summary>
        /// <param name="isModifyable">If IsModifyable is true get Modify Booking confirmation email Template</param>
        /// <returns></returns>
        public static EmailEntity GetBookingConfirmationEmailTemplate(bool isModifyable)
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["BookingConfirmationEmailTemplate"];

            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.BOOKING_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailBookingTemplateXML(isModifyable);
                        break;
                    }
                case TemplateType.BOOKING_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailBookingTemplateXML(isModifyable);
                        break;
                    }
            }
            return emailEntity;
        }

        /// <summary>
        /// Get the CancelBookingConfirmation Email Template
        /// </summary>
        /// <returns></returns>
        public static EmailEntity GetCancelBookingConfirmationEmailTemplate()
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["BookingConfirmationEmailTemplate"];

            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.BOOKING_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailCancelBookingTemplateXML();
                        break;
                    }
                case TemplateType.BOOKING_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailCancelBookingTemplateXML();
                        break;
                    }
            }
            return emailEntity;
        }

        /// <summary>
        /// Gets enroll profile confirmation email template
        /// </summary>
        /// <returns></returns>
        public static EmailEntity GetEnrollProfileConfirmationEmailTemplate()
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["EnrollProfileConfirmationEmailTemplate"];

            EmailEntity emailEntity = null;
            switch (templateformat)
            {
                case TemplateType.ENROLL_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        emailEntity = ReadEmailBookingTemplateXML();
                        break;
                    }
                case TemplateType.ENROLL_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        emailEntity = ReadEmailEnrollTemplateXML();
                        break;
                    }
            }
            return emailEntity;
        }

        /// <summary>
        /// Get the BookingConfirmation SMS Template
        /// </summary>
        /// <returns>
        /// SMSEntity
        /// </returns>
        public static SMSEntity GetBookingConfirmationSMSTemplate()
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["BookingConfirmationSMSTemplate"];

            SMSEntity smsEntity = null;
            switch (templateformat)
            {
                case TemplateType.BOOKING_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        smsEntity = ReadSMSBookingTemplateXML();
                        break;
                    }
                case TemplateType.BOOKING_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        smsEntity = ReadSMSBookingTemplateXML();
                        break;
                    }
            }
            return smsEntity;
        }

        /// <summary>
        /// Get the Booking Cancellation SMS Template
        /// </summary>
        /// <returns></returns>
        public static SMSEntity GetBookingCancellationSMSTemplate()
        {
            string templateformat =
                System.Configuration.ConfigurationManager.AppSettings["BookingConfirmationSMSTemplate"];

            SMSEntity smsEntity = null;
            switch (templateformat)
            {
                case TemplateType.BOOKING_CONFIRMATION_TEMPLATE_TYPE_XML:
                    {
                        smsEntity = ReadSMSBookingTemplateXML();
                        break;
                    }
                case TemplateType.BOOKING_CONFIRMATION_TEMPLATE_TYPE_CMS:
                    {
                        break;
                    }
                default:
                    {
                        smsEntity = ReadSMSBookingCancellationTemplateXML();
                        break;
                    }
            }
            return smsEntity;
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Read the Booking Confirmation SMS Template from XML
        /// </summary>
        /// <returns></returns>
        private static SMSEntity ReadSMSBookingTemplateXML()
        {
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.BOOKING_CONFIRMATION_SMS_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            SMSEntity smsEntity = new SMSEntity();
            XmlNode bodyNode = templateXmlDocument.SelectSingleNode(TemplateType.BOOKING_CONFIRMATION_SMS_BODY_TEMPLATE);
            smsEntity.Message = bodyNode.InnerText;

            return smsEntity;
        }

        /// <summary>
        /// Read the Booking Cancellation SMS Template from XML
        /// </summary>
        /// <returns></returns>
        private static SMSEntity ReadSMSBookingCancellationTemplateXML()
        {
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.BOOKING_CONFIRMATION_SMS_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            SMSEntity smsEntity = new SMSEntity();
            XmlNode bodyNode = templateXmlDocument.SelectSingleNode(TemplateType.BOOKING_CANCELLATION_SMS_BODY_TEMPLATE);
            smsEntity.Message = bodyNode.InnerText;

            return smsEntity;
        }

        /// <summary>
        /// Read the Booking Confirmation Email Template from XML
        /// </summary>
        /// <param name="xpath">
        /// XPath
        /// </param>
        /// <returns>
        /// Template
        /// </returns>
        private static EmailEntity ReadEmailBookingTemplateXML(bool isModifyable)
        {
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.BOOKING_CONFIRMATION_EMAIL_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();
            if (!isModifyable)
            {
                XmlNode bodyNode =
                    templateXmlDocument.SelectSingleNode(TemplateType.BOOKING_CONFIRMATION_EMAIL_BODY_TEMPLATE);
                emailEntity.Body = bodyNode.InnerText;

                XmlNode textBodyNode =
                    templateXmlDocument.SelectSingleNode(TemplateType.BOOKING_CONFIRMATION_TEXT_EMAIL_BODY_TEMPLATE);
                emailEntity.TextEmailBody = textBodyNode.InnerText;

                XmlNode subjectNode =
                    templateXmlDocument.SelectSingleNode(TemplateType.BOOKING_CONFIRMATION_EMAIL_SUBJECT);
                emailEntity.Subject = subjectNode.InnerText;
                XmlNode senderNode = templateXmlDocument.SelectSingleNode(TemplateType.BOOKING_CONFIRMATION_EMAIL_SENDER);
                emailEntity.Sender = senderNode.InnerText;
            }
            else
            {
                XmlNode bodyNode =
                    templateXmlDocument.SelectSingleNode(TemplateType.MODIFY_BOOKING_CONFIRMATION_EMAIL_BODY_TEMPLATE);
                emailEntity.Body = bodyNode.InnerText;

                XmlNode textBodyNode =
                    templateXmlDocument.SelectSingleNode(
                        TemplateType.MODIFY_BOOKING_CONFIRMATION_TEXT_EMAIL_BODY_TEMPLATE);
                emailEntity.TextEmailBody = textBodyNode.InnerText;

                XmlNode subjectNode =
                    templateXmlDocument.SelectSingleNode(TemplateType.MODIFY_BOOKING_CONFIRMATION_EMAIL_SUBJECT);
                emailEntity.Subject = subjectNode.InnerText;
                XmlNode senderNode =
                    templateXmlDocument.SelectSingleNode(TemplateType.MODIFY_BOOKING_CONFIRMATION_EMAIL_SENDER);
                emailEntity.Sender = senderNode.InnerText;
            }

            return emailEntity;
        }


        private static EmailEntity ReadEmailCancelBookingTemplateXML()
        {
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.BOOKING_CONFIRMATION_EMAIL_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();

            XmlNode bodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.CANCEL_BOOKING_CONFIRMATION_EMAIL_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;

            XmlNode textBodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.CANCEL_BOOKING_CONFIRMATION_TEXT_EMAIL_BODY_TEMPLATE);
            emailEntity.TextEmailBody = textBodyNode.InnerText;

            XmlNode subjectNode =
                templateXmlDocument.SelectSingleNode(TemplateType.CANCEL_BOOKING_CONFIRMATION_EMAIL_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;

            XmlNode senderNode =
                templateXmlDocument.SelectSingleNode(TemplateType.CANCEL_BOOKING_CONFIRMATION_EMAIL_SENDER);
            emailEntity.Sender = senderNode.InnerText;

            return emailEntity;
        }

        /// <summary>
        /// Read the Booking Confirmation Email Template from XML
        /// </summary>
        /// <param name="xpath">
        /// XPath
        /// </param>
        /// <returns>
        /// Template
        /// </returns>
        private static EmailEntity ReadEmailBookingTemplateXML()
        {
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\lang\\" + TemplateType.BOOKING_CONFIRMATION_EMAIL_TEMPLATE +
                                currentLanguageString + AppConstants.CODE_FILE_EXTN;

            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();

            XmlNode bodyNode =
                templateXmlDocument.SelectSingleNode(TemplateType.BOOKING_CONFIRMATION_EMAIL_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;
            XmlNode subjectNode = templateXmlDocument.SelectSingleNode(TemplateType.BOOKING_CONFIRMATION_EMAIL_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;
            XmlNode senderNode = templateXmlDocument.SelectSingleNode(TemplateType.BOOKING_CONFIRMATION_EMAIL_SENDER);
            emailEntity.Sender = senderNode.InnerText;


            return emailEntity;
        }


        private static EmailEntity ReadEmailEnrollTemplateXML()
        {
            string templateFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));

            string currentLanguage = string.Empty;
            if (PreferredLanguageForEmailSessionWrapper.UserPreferredLanguage != null)
            {
                currentLanguage = PreferredLanguageForEmailSessionWrapper.UserPreferredLanguage;
                if (currentLanguage == LanguageConstant.LANGUAGE_ENGLISH_CODE)
                {
                    currentLanguage = LanguageConstant.LANGUAGE_ENGLISH;
                }
                else if (currentLanguage == LanguageConstant.LANGUAGE_DANISH_CODE)
                {
                    currentLanguage = LanguageConstant.LANGUAGE_DANISH;
                }
                else if (currentLanguage == LanguageConstant.LANGUAGE_NORWEGIAN_CODE)
                {
                    currentLanguage = LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION;
                }
                else if (currentLanguage == LanguageConstant.LANGUAGE_SWEDISH_CODE)
                {
                    currentLanguage = LanguageConstant.LANGUAGE_SWEDISH;
                }
                else if (currentLanguage == LanguageConstant.LANGUAGE_FINNISH_CODE)
                {
                    currentLanguage = LanguageConstant.LANGUAGE_FINNISH;
                }
                else if (currentLanguage == LanguageConstant.LANGUAGE_GERMAN_CODE)
                {
                    currentLanguage = LanguageConstant.LANGUAGE_GERMAN;
                }
                else if (currentLanguage == LanguageConstant.LANGUAGE_RUSSIA_CODE)
                {
                    currentLanguage = LanguageConstant.LANGUAGE_RUSSIA_RU;
                }

                templateFileName += "\\lang\\" + TemplateType.ENROLL_CONFIRMATION_EMAIL_TEMPLATE +
                                    currentLanguage + AppConstants.CODE_FILE_EXTN;
            }
            else
            {
                templateFileName += "\\lang\\" + TemplateType.ENROLL_CONFIRMATION_EMAIL_TEMPLATE +
                                    currentLanguageString + AppConstants.CODE_FILE_EXTN;
            }
            XmlDocument templateXmlDocument = new XmlDocument();
            templateXmlDocument.Load(templateFileName);

            EmailEntity emailEntity = new EmailEntity();
            XmlNode bodyNode = templateXmlDocument.SelectSingleNode(TemplateType.ENROLL_CONFIRMATION_EMAIL_BODY_TEMPLATE);
            emailEntity.Body = bodyNode.InnerText;
            XmlNode subjectNode = templateXmlDocument.SelectSingleNode(TemplateType.ENROLL_CONFIRMATION_EMAIL_SUBJECT);
            emailEntity.Subject = subjectNode.InnerText;
            XmlNode senderNode = templateXmlDocument.SelectSingleNode(TemplateType.ENROLL_CONFIRMATION_EMAIL_SENDER);
            emailEntity.Sender = senderNode.InnerText;
            return emailEntity;
        }

        /// <summary>
        /// Get the Preferred language Name
        /// </summary>
        private static string currentLanguageString
        {
            get { return EPiServer.Globalization.ContentLanguage.PreferredCulture.Name; }
        }

        #endregion
    }
}