﻿using System;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.Core;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

public class OperaBookingCallsAfterNetsPayment
{
    private delegate void CreatePDF(List<EmailEntity> email, List<string> reservations);
    private PaymentController paymentController = null;
    private PaymentController m_PaymentController
    {
        get
        {
            if (paymentController == null)
            {
                IContentDataAccessManager contentDataAccessManager = new ContentDataAccessManager();
                IAdvancedReservationDomain advancedReservationDomain = new AdvancedReservationDomain();
                ILoyaltyDomain loyaltyDomain = new LoyaltyDomain();
                IReservationDomain reservationDomain = new ReservationDomain();
                paymentController = new PaymentController(contentDataAccessManager, advancedReservationDomain, loyaltyDomain, reservationDomain);
            }
            return paymentController;
        }
    }

    public string ProcessOperaBookingCallsAfterNetsPayment(string transactionID, string responseCode)
    {
        try
        {
            string redirectionAbsoluteURL = string.Empty;
            string sessionTransactionId = Reservation2SessionWrapper.PaymentTransactionId;

            if (!string.IsNullOrEmpty(sessionTransactionId) && sessionTransactionId.Equals(transactionID, StringComparison.InvariantCultureIgnoreCase)
                && !string.IsNullOrEmpty(responseCode) && (responseCode.Equals(SessionBookingConstants.NETS_PAYMENT_RESPONSECODE, StringComparison.InvariantCultureIgnoreCase)))
            {
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                List<GuestInformationEntity> guestInformationEntities = Reservation2SessionWrapper.PaymentGuestInformation;
                List<HotelRoomRateEntity> hotelInformationEntities = Reservation2SessionWrapper.PaymentHotelRoomRateInformation;
                bool isSessionBooking = Reservation2SessionWrapper.PaymentIsSessionBooking;
                ReservationNumberSessionWrapper.IsReservationDone = false;
                ReservationNumberSessionWrapper.IsAuthorizationDone = false;
                string authStatus = m_PaymentController.NetsAuthProcess(sessionTransactionId, search.SelectedHotelCode);
                if (authStatus.Equals(SessionBookingConstants.NETS_PAYMENT_RESPONSECODE, StringComparison.InvariantCultureIgnoreCase))
                {
                    try
                    {
                        ReservationNumberSessionWrapper.IsAuthorizationDone = true;

                        if (BookingEngineSessionWrapper.IsModifyBooking)
                        {
                            ReservationNumberSessionWrapper.IsModifyOnlinePaymentMode = true;
                            CreateReservation(guestInformationEntities, hotelInformationEntities, isSessionBooking, false, null);
                        }

                        PaymentInfo paymentInfo = m_PaymentController.GetPaymentInfo(sessionTransactionId, search.SelectedHotelCode);
                        ConfirmBooking(guestInformationEntities);
                        ReservationNumberSessionWrapper.IsReservationDone = true;

                        paymentInfo.CaptureSuccess =
                        m_PaymentController.CaptureCall(sessionTransactionId, Reservation2SessionWrapper.PaymentOrderAmount, search.SelectedHotelCode);
                        bool makePaymentStatus = false;
                        if (paymentInfo.CaptureSuccess)
                            makePaymentStatus = paymentController.MakePayment(paymentInfo);
                        if (paymentInfo.CaptureSuccess && !makePaymentStatus)
                        {
                            paymentInfo.MakePaymentFailure = true;
                            EmailEntity emailEntity = new EmailEntity();
                            emailEntity.Recipient = AppConstants.EmailRecipientsForOnlinePaymentFailures;
                            emailEntity.Sender = AppConstants.EmailSenderForOnlinePaymentFailures;
                            string body =
                            string.Format("Online payment(Desktop) | Make payment call failed for the transaction with the reservation number {0} and the transaction Id {1} with hotel operaId {2}.",
                                                               ReservationNumberSessionWrapper.ReservationNumber, Reservation2SessionWrapper.PaymentTransactionId, SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode);
                            string subject = string.Format("Make payment call failed in the process of online payment process.");
                            SendEmailOnOWSFailures(subject, body);
                        }
                        try
                        {
                            CreateReservation(guestInformationEntities, hotelInformationEntities, isSessionBooking, true, paymentInfo);
                        }
                        catch (Exception ex)
                        {
                            string body =
                             string.Format("Online payment(Desktop) | Modify booking failed for the transaction with the reservation number {0} and the transaction Id {1} with hotel operaId {2}.",
                                                                ReservationNumberSessionWrapper.ReservationNumber, Reservation2SessionWrapper.PaymentTransactionId, SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode);
                            string subject = string.Format("Modify booking call failed.");
                            SendEmailOnOWSFailures(subject, body);
                            AppLogger.LogOnlinePaymentFatalException(ex, body);
                        }
                        SortedList<string, GuestInformationEntity> sortdListGuestInfoEntity = new SortedList<string, GuestInformationEntity>();
                        foreach (GuestInformationEntity guest in guestInformationEntities)
                        {
                            sortdListGuestInfoEntity.Add(guest.LegNumber, guest);
                        }
                        GuestBookingInformationSessionWrapper.AllGuestsBookingInformations = sortdListGuestInfoEntity;
                        SendConfirmationToGuest();
                        SendMailConfirmationToGuestsUsingHTMLString();
                        string confirmationRelativeUrl = GlobalUtil.GetUrlToPage(EpiServerPageConstants.BOOKING_CONFIRMATION_PAGE);
                        redirectionAbsoluteURL = WebUtil.GetSecureUrl(string.Format("{0}?BNo={1}&LNm={2}", confirmationRelativeUrl, guestInformationEntities[0].ReservationNumber, guestInformationEntities[0].LastName));
                    }
                    catch (KeyNotFoundException keyNotFoundException)
                    {
                        AppLogger.LogOnlinePaymentFatalException(keyNotFoundException, keyNotFoundException.Message);
                        redirectionAbsoluteURL = WebUtil.GetSecureUrl(GlobalUtil.GetUrlToPage(EpiServerPageConstants.BOOKING_DETAILS_PAGE));
                        m_PaymentController.NetsAnnulProcess(sessionTransactionId);
                        Reservation2SessionWrapper.NetsPaymentErrorMessage = WebUtil.GetTranslatedText("/PSPErrors/InvalidCard");
                    }
                    catch (Exception ex)
                    {
                        AppLogger.LogOnlinePaymentFatalException(ex, ex.Message);
                        redirectionAbsoluteURL = WebUtil.GetSecureUrl(GlobalUtil.GetUrlToPage(EpiServerPageConstants.BOOKING_DETAILS_PAGE));
                        m_PaymentController.NetsAnnulProcess(sessionTransactionId);
                        Reservation2SessionWrapper.NetsPaymentErrorMessage = WebUtil.GetTranslatedText("/PSPErrors/UnhandledError");
                    }
                }
                else
                {
                    UnBlockOnRdBtnChange();
                    redirectionAbsoluteURL = WebUtil.GetSecureUrl(GlobalUtil.GetUrlToPage(EpiServerPageConstants.BOOKING_DETAILS_PAGE));
                }
            }
            return redirectionAbsoluteURL;
        }
        catch (Exception ex)
        {
            AppLogger.LogOnlinePaymentFatalException(ex, ex.Message);
            throw ex;
        }
    }

    private void UnBlockOnRdBtnChange()
    {
        try
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if (search != null && search.ListRooms != null && search.ListRooms.Count > 0)
            {
                WebUtil.UnblockRoom(search.ListRooms[0].RoomBlockReservationNumber);
                for (int iListRoom = 0; iListRoom < search.ListRooms.Count; iListRoom++)
                {
                    if (search.ListRooms[iListRoom].IsBlocked &&
                        (!string.IsNullOrEmpty(search.ListRooms[iListRoom].RoomBlockReservationNumber)) &&
                        (search.ListRooms[iListRoom].IsRoomModifiable))
                    {
                        search.ListRooms[iListRoom].RoomBlockReservationNumber = string.Empty;
                        search.ListRooms[iListRoom].IsBlocked = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void CreateReservation(List<GuestInformationEntity> guestList, List<HotelRoomRateEntity> hotelRoomRate,
                                   bool isSessionBooking, bool isModifyBookingAfterNetsPayment, PaymentInfo paymentInfo)
    {
        string createdNameID = string.Empty;
        string sourceCode = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SourceCode");

        ReservationController reservationController = new ReservationController();
        SortedList<string, GuestInformationEntity> reservationMap = new SortedList<string, GuestInformationEntity>();

        string reservationNumber = string.Empty;
        int totalGuests = guestList.Count;
        SortedList<string, string> legReservationMap = null;
        for (int guestCount = 0; guestCount < totalGuests; guestCount++)
        {
            legReservationMap = null;
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if (search != null && search.ListRooms != null && search.ListRooms.Count > 0 &&
                guestCount < search.ListRooms.Count && search.ListRooms[guestCount].IsRoomModifiable)
            {
                HotelSearchRoomEntity roomSearch = search.ListRooms[guestCount];
                if ((paymentInfo != null) && (guestList[guestCount].GuranteeInformation.GuranteeType == GuranteeType.PREPAIDINPROGRESS))
                {
                    guestList[guestCount].GuranteeInformation.CreditCard =
                         new CreditCardEntity(paymentInfo.PanHash, paymentInfo.CardType, paymentInfo.CreditCardNumberMasked,
                                              new ExpiryDateEntity(paymentInfo.ExpiryDate.Month, paymentInfo.ExpiryDate.Year));
                }
                if ((isModifyBookingAfterNetsPayment || BookingEngineSessionWrapper.IsModifyBooking) && (!string.IsNullOrEmpty(guestList[guestCount].LegNumber))
                    && ((roomSearch.IsRoomConfirmed == true) || isModifyBookingAfterNetsPayment))
                {
                    if (guestList[guestCount].GuranteeInformation.GuranteeType == GuranteeType.PREPAIDINPROGRESS)
                    {
                        guestList[guestCount].UserPreferences =
                            Utility.ConvertToUserPreferenceEntityList(guestList[guestCount].SpecialRequests);
                        if (isModifyBookingAfterNetsPayment && (guestList[guestCount].GuranteeInformation != null))
                        {
                            if (paymentInfo.MakePaymentFailure)
                            {
                                guestList[guestCount].GuranteeInformation.GuranteeType = GuranteeType.MAKEPAYMENTFAILURE;
                                guestList[guestCount].GuranteeInformation.ChannelGuranteeCode =
                                    AppConstants.GUARANTEETYPE_MAKEPAYMENTFAILURE;
                            }
                            else if (paymentInfo.CaptureSuccess)
                            {
                                guestList[guestCount].GuranteeInformation.GuranteeType = GuranteeType.PREPAIDSUCCESS;
                                guestList[guestCount].GuranteeInformation.ChannelGuranteeCode =
                                    AppConstants.GUARANTEETYPE_PREAPIDSUCCESS;
                            }
                            else
                            {
                                guestList[guestCount].GuranteeInformation.GuranteeType = GuranteeType.PREPAIDFAILURE;
                                guestList[guestCount].GuranteeInformation.ChannelGuranteeCode =
                                    AppConstants.GUARANTEETYPE_PREAPIDFAILURE;
                            }

                        }
                        if (ReservationNumberSessionWrapper.IsModifyOnlinePaymentMode)
                        {
                            legReservationMap = reservationController.ModifyBooking(search, roomSearch,
                                                                                hotelRoomRate[guestCount],
                                                                                guestList[guestCount],
                                                                                guestList[guestCount].ReservationNumber,
                                                                                sourceCode, out createdNameID,
                                                                                isSessionBooking);
                            ReservationNumberSessionWrapper.IsModifyOnlinePaymentMode = false;
                        }
                        else
                        {
                            legReservationMap = reservationController.ModifyGuaranteeInformation(search, roomSearch,
                                                                                                 hotelRoomRate[guestCount],
                                                                                                 guestList[guestCount],
                                                                                                 guestList[guestCount].
                                                                                                     ReservationNumber,
                                                                                                 sourceCode,
                                                                                                 out createdNameID,
                                                                                                 isSessionBooking);
                        }
                    }
                }
                else
                {
                    legReservationMap = reservationController.CreateBooking(search, roomSearch,
                                                                            hotelRoomRate[guestCount],
                                                                            guestList[guestCount], reservationNumber,
                                                                            sourceCode, out createdNameID,
                                                                            isSessionBooking,
                                                                            HygieneSessionWrapper.PartnerID);
                }
            }

            if (legReservationMap != null)
            {
                foreach (string key in legReservationMap.Keys)
                {
                    reservationNumber = legReservationMap[key];
                    guestList[guestCount].LegNumber = key;
                    guestList[guestCount].ReservationNumber = reservationNumber;
                    reservationMap[reservationNumber + AppConstants.HYPHEN + key] = guestList[guestCount];
                    break;
                }
            }

            if (createdNameID != string.Empty)
            {
                SearchCriteriaSessionWrapper.SearchCriteria.NameID = createdNameID;
            }

            if ((SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.BONUSCHEQUE) &&
                (!guestList[guestCount].IsValidDNumber))
            {
                SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = null;
            }
            if ((HygieneSessionWrapper.IsComboReservation) && (Reservation2SessionWrapper.IsModifyFlow) &&
                (!search.ListRooms[guestCount].IsRoomModifiable))
            {
                if ((GuestBookingInformationSessionWrapper.AllGuestsBookingInformations != null) && (guestList[guestCount] != null))
                {
                    string legNumber = guestList[guestCount].LegNumber.ToString();
                    string resNumber = guestList[guestCount].ReservationNumber.ToString();
                    reservationMap[resNumber + AppConstants.HYPHEN + legNumber] =
                        GuestBookingInformationSessionWrapper.AllGuestsBookingInformations[resNumber + AppConstants.HYPHEN + legNumber];
                }
            }
        }

        ReservationNumberSessionWrapper.ReservationNumber = reservationNumber;
        GuestBookingInformationSessionWrapper.AllGuestsBookingInformations = reservationMap;
    }

    private void ConfirmBooking(List<GuestInformationEntity> guestList)
    {
        try
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if ((search != null) && (guestList.Count == search.ListRooms.Count))
            {
                for (int i = 0; i < guestList.Count; i++)
                {
                    if (search.ListRooms[i].IsRoomModifiable)
                    {
                        GuestInformationEntity guest = guestList[i];
                        WebUtil.ConfirmBooking(guest.ReservationNumber, guest.LegNumber);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void SendConfirmationToGuest()
    {
        try
        {
            Dictionary<string, string> emailBookingMap = new Dictionary<string, string>();
            emailBookingMap = Utility.CollectGenericHotelEmailDetails();
            SortedList<string, GuestInformationEntity> guestsList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;

            Hashtable htbl = new Hashtable();

            if (guestsList != null)
            {
                emailBookingMap[CommunicationTemplateConstants.LOGOPATH] = AppConstants.LOGOPATH;
                Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                int totalGuests = guestsList.Count;
                int guestIterator = 0;
                bool isSMSOptionSelected = false;
                foreach (string key in guestsList.Keys)
                {
                    int roomNumber = Convert.ToInt32(key);
                    if (selectedRoomAndRatesHashTable != null && selectedRoomAndRatesHashTable.Count > 0)
                    {
                        SelectedRoomAndRateEntity selectedRoomAndRates = selectedRoomAndRatesHashTable[guestIterator] as SelectedRoomAndRateEntity;
                        CollectBookingConfirmationEmailDetails(ref emailBookingMap, selectedRoomAndRates, guestsList[key], guestIterator);
                        if (!htbl.ContainsKey(selectedRoomAndRates.RateCategoryID + AppConstants.Room + roomNumber))
                        {
                            htbl.Add(selectedRoomAndRates.RateCategoryID + AppConstants.Room + roomNumber,
                                     emailBookingMap[CommunicationTemplateConstants.POLICY_TEXT]);
                        }
                        else
                        {
                            if (!htbl.ContainsValue(emailBookingMap[CommunicationTemplateConstants.POLICY_TEXT]))
                            {
                                htbl.Add(selectedRoomAndRates.RateCategoryID + AppConstants.Room + roomNumber,
                                         emailBookingMap[CommunicationTemplateConstants.POLICY_TEXT]);
                            }
                        }
                    }
                    string resNumber = string.Empty;
                    if (Convert.ToInt32(key) > 1)
                        resNumber = guestsList[key].ReservationNumber + "-" + key;
                    else
                        resNumber = guestsList[key].ReservationNumber;

                    CollectGuestsSpecificEmailDetails(ref emailBookingMap, guestsList[key], resNumber);
                    ConfirmationHeaderSessionWrapper.ConfirmationHeader = emailBookingMap[CommunicationTemplateConstants.HTMLPOLICYHEADER];
                    BookingEngineSessionWrapper.IsSmsRequired = false;
                    ErrorsSessionWrapper.FailedSMSRecepient = null;
                    if ((guestsList[key].IsSMSChecked) &&
                        (SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[guestIterator].IsRoomModifiable))
                    {
                        isSMSOptionSelected = true;
                        Utility.SendSMSConfirmationToGuest(ref emailBookingMap, key, resNumber);
                    }
                    guestIterator++;
                }
                BookingEngineSessionWrapper.IsSmsRequired = isSMSOptionSelected;
            }
            HygieneSessionWrapper.PolicyTextConfirmation = htbl;
        }
        catch (Exception ex)
        {
            AppLogger.LogFatalException(ex, "Failed to send booking confirmation in SendConfirmationToGuest() to the Guest");
        }
    }

    public void SendMailConfirmationToGuestsUsingHTMLString()
    {
        Scandic.Scanweb.Core.AppLogger.LogInfoMessage("Start callBack method");

        try
        {
            SortedList<int, string> emailBookingMap = new SortedList<int, string>();
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
            ErrorsSessionWrapper.ClearFailedEmailRecipient();
            string templateFileName = string.Empty;
            string bookingType = PageIdentifier.BookingPageIdentifier;

            if (BookingEngineSessionWrapper.IsModifyBooking)
                bookingType = PageIdentifier.ModifyBookingPageIdentifier;

            if (hotelSearch != null && hotelSearch.ListRooms.Count > 0)
            {
                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:GenerateHTMLString for Email and PDF:Start time");
                for (int roomIterator = 0; roomIterator < hotelSearch.ListRooms.Count; roomIterator++)
                {
                    string htmlStringForEmail = string.Empty;
                    string htmlStringForPDF = string.Empty;
                    string hostName = string.Empty;
                    string alterhtmlStringForPDF = string.Empty;
                    string creditCardReceiptHtmlString = string.Empty;

                    if (roomIterator == 0)
                    {
                        GenerateConfirmationPageHtmlString(
                            @"/templates/Scanweb/Pages/ConfirmationEmailPage.aspx?AllROOMSTOBESHOWN=true&BOOKINGTYPE=" +
                            bookingType, ref htmlStringForEmail);

                        GenerateConfirmationPageHtmlString(
                            @"/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx?command=print&isPDF=true",
                            ref htmlStringForPDF);

                        if (string.IsNullOrEmpty(htmlStringForPDF))
                            GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx?command=print&isPDF=true",
                                ref htmlStringForPDF);

                        //send receipt email only for the first person
                        if (WebUtil.IsAnyRoomHavingSaveCategory(HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable) && roomIterator == 0)
                            GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/Booking/Receipt.aspx?command=print&isPDF=true",
                                ref creditCardReceiptHtmlString);

                        templateFileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();

                        templateFileName = templateFileName.Remove(templateFileName.IndexOf("\\bin"));

                        alterhtmlStringForPDF = htmlStringForPDF.Replace("/Templates", templateFileName + "/Templates");

                        creditCardReceiptHtmlString = creditCardReceiptHtmlString.Replace("/Templates", templateFileName + "/Templates");

                        emailBookingMap.Add(roomIterator, htmlStringForEmail + "^" + alterhtmlStringForPDF + "^" + creditCardReceiptHtmlString);
                    }
                    else
                    {
                        if (
                            (!(Reservation2SessionWrapper.IsModifyFlow)) ||
                            ((Reservation2SessionWrapper.IsModifyFlow) && !(HygieneSessionWrapper.IsComboReservation)) ||
                            ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation) &&
                             (hotelSearch.ListRooms[roomIterator].IsRoomModifiable))
                            )
                        {
                            GenerateConfirmationPageHtmlString(
                                @"/templates/Scanweb/Pages/ConfirmationEmailPage.aspx?AllROOMSTOBESHOWN=false&ROOMNUMBERTOBEDISPLAYED=" +
                                roomIterator + "&BOOKINGTYPE=" + bookingType, ref htmlStringForEmail);

                            GenerateConfirmationPageHtmlString(
                                @"/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx?AllROOMSTOBESHOWN=false&isPDF=true&command=print&roomNumber=" +
                                roomIterator, ref htmlStringForPDF);

                            if (string.IsNullOrEmpty(htmlStringForPDF))
                                GenerateConfirmationPageHtmlString(
                                    @"/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx?isPDF=true&command=print&roomNumber=" +
                                    roomIterator, ref htmlStringForPDF);

                            //send receipt email only for the first person
                            if (WebUtil.IsAnyRoomHavingSaveCategory(HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable) && roomIterator == 0)
                                GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/Booking/Receipt.aspx?command=print&isPDF=true",
                                    ref creditCardReceiptHtmlString);

                            templateFileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();

                            templateFileName = templateFileName.Remove(templateFileName.IndexOf("\\bin"));
                            alterhtmlStringForPDF = htmlStringForPDF.Replace("/Templates", templateFileName + "/Templates");
                            creditCardReceiptHtmlString = creditCardReceiptHtmlString.Replace("/Templates", templateFileName + "/Templates");
                            emailBookingMap.Add(roomIterator, htmlStringForEmail + "^" + alterhtmlStringForPDF + "^" + creditCardReceiptHtmlString);
                        }
                    }
                }
                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:GenerateHTMLString for Email and PDF:End time");
            }

            SortedList<string, GuestInformationEntity> guestsList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;

            if ((guestsList != null) &&
                (guestsList.Count > 0) &&
                (hotelSearch != null) &&
                (hotelSearch.ListRooms != null) &&
                (hotelSearch.ListRooms.Count > 0) &&
                (guestsList.Count == hotelSearch.ListRooms.Count))
            {
                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:Send EMail and create PDF:Start time");
                int counter = 0;
                List<EmailEntity> emails = new List<EmailEntity>();
                List<string> reservations = new List<string>();
                foreach (string key in guestsList.Keys)
                {

                    if ((counter == 0) ||
                        !(Reservation2SessionWrapper.IsModifyFlow) ||
                        ((Reservation2SessionWrapper.IsModifyFlow) && !(HygieneSessionWrapper.IsComboReservation)) ||
                        ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation) &&
                         (hotelSearch.ListRooms[counter].IsRoomModifiable)))
                    {
                        string emailText = string.Empty;
                        string[] emailTextAndPDF = null;
                        GuestInformationEntity currentGuestInformationEntity = guestsList[key] as GuestInformationEntity;
                        if (currentGuestInformationEntity != null)
                        {
                            emailText = ((emailBookingMap[counter] != null) ? emailBookingMap[counter] : string.Empty);
                            if (!string.IsNullOrEmpty(emailText))
                                emailTextAndPDF = emailText.Split('^');
                            EmailEntity emailEntity = new EmailEntity();
                            if (emailTextAndPDF != null)
                            {
                                if (emailTextAndPDF[0] != null)
                                    emailEntity.Body = emailTextAndPDF[0];

                                if (emailTextAndPDF[1] != null)
                                    emailEntity.TextEmailBody = emailTextAndPDF[1];

                                if (emailTextAndPDF.Length > 2)
                                    if (!string.IsNullOrEmpty(emailTextAndPDF[2]))
                                        emailEntity.CreditCardReceiptHtmlText = emailTextAndPDF[2];
                            }
                            if (currentGuestInformationEntity.IsRewardNightGift &&
                                LoyaltyDetailsSessionWrapper.LoyaltyDetails != null &&
                                !string.IsNullOrEmpty(LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email))
                            {
                                emailEntity.Recipient = new string[]
                                {
                                    currentGuestInformationEntity.EmailDetails.EmailID,
                                    LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email
                                };
                            }
                            else
                            {
                                emailEntity.Recipient = new string[] { currentGuestInformationEntity.EmailDetails.EmailID };
                            }

                            emailEntity.Subject = WebUtil.GetTranslatedText("/bookingengine/booking/confirmation/subject");
                            emailEntity.Sender = WebUtil.GetTranslatedText("/bookingengine/booking/confirmation/fromEmail");

                            string keyValue = key;
                            keyValue = keyValue.Replace(currentGuestInformationEntity.ReservationNumber + "-", string.Empty);

                            string reservationNumber = currentGuestInformationEntity.ReservationNumber + "-" + keyValue;

                            emailEntity.ReceiptReservationNumber = string.Concat(currentGuestInformationEntity.ReservationNumber, "-",
                                            WebUtil.GetTranslatedText("/bookingengine/booking/BookingReceipt/BookingTypeSave"));
                            emails.Add(emailEntity);
                            reservations.Add(reservationNumber);

                        }
                    }
                    counter++;
                }
                SendMail(emails, reservations);
                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:Send EMail and create PDF:End time");
            }
        }
        catch (Exception ex)
        {
            AppLogger.LogFatalException(ex, " Exception in send email");
        }
    }

    private void GenerateConfirmationPageHtmlString(string url, ref string htmlString)
    {
        #region Very IMP code To render the preview page

        Scandic.Scanweb.Core.AppLogger.LogInfoMessage(
            "UserContactBookingDetails:GenerateConfirmationPageHtmlString() Begin");
        StringWriter sw = new StringWriter();
        try
        {
            HttpContext.Current.Server.Execute(url, sw);
            htmlString = (sw.ToString()).Trim().Replace("\r\n", "").Replace("\"", "'");
        }
        catch (Exception ex)
        {
        }
        Scandic.Scanweb.Core.AppLogger.LogInfoMessage(
            "UserContactBookingDetails:GenerateConfirmationPageHtmlString() End");

        #endregion
    }

    public void SendMail(List<EmailEntity> emails, List<string> reservations)
    {
        try
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendMail() Start Time");
            CreatePDF createPDFDeligate = new CreatePDF(SendEmailAsync);
            IAsyncResult result = createPDFDeligate.BeginInvoke(emails, reservations, null, null);
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendMail() End Time");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void SendEmailAsync(List<EmailEntity> emails, List<string> reservations)
    {
        Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendEmailAsync(): Excuted sucessfully:Start Time");
        int counter = 0;
        foreach (EmailEntity email in emails)
        {
            Utility.SendEmailConfirmationToGuest(email, reservations[counter]);
            counter++;
        }
        Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendEmailAsync(): Excuted sucessfully: End Time");
    }
    private Dictionary<string, string> CollectBookingConfirmationEmailDetails(
        ref Dictionary<string, string> bookingMap, SelectedRoomAndRateEntity selectedRoomAndRateEntity,
        GuestInformationEntity guestInfoEntity, int guestIterator)
    {
        GuranteeType guaranteePolicyType = guestInfoEntity.GuranteeInformation != null ? guestInfoEntity.GuranteeInformation.GuranteeType : GuranteeType.HOLD;
        Utility.CollectHotelRoomEmailDetails(ref bookingMap, guestIterator);
        CollectGenericGuestEmailDetails(ref bookingMap, guestInfoEntity);
        CollectDynamicTextFromCMS(ref bookingMap, selectedRoomAndRateEntity, guestIterator, guaranteePolicyType);
        return bookingMap;
    }

    private void CollectGuestsSpecificEmailDetails(ref Dictionary<string, string> bookingMap, GuestInformationEntity guestInfoEntity, string reservationNumber)
    {
        bookingMap[CommunicationTemplateConstants.CONFIRMATION_NUMBER] = reservationNumber;
        if (guestInfoEntity.GuestAccountNumber != null && guestInfoEntity.GuestAccountNumber != string.Empty)
        {
            bookingMap[CommunicationTemplateConstants.MEMBERSHIPNUMBER] = guestInfoEntity.GuestAccountNumber;
        }
        else
        {
            bookingMap[CommunicationTemplateConstants.NO_MEMBERID] = AppConstants.SPACE;
        }
        bookingMap[CommunicationTemplateConstants.FIRST_NAME] = guestInfoEntity.FirstName;
        bookingMap[CommunicationTemplateConstants.LASTNAME] = guestInfoEntity.LastName;

        if (guestInfoEntity.Mobile != null && guestInfoEntity.Mobile.Number != null)
        {
            bookingMap[CommunicationTemplateConstants.TELEPHONE2] = guestInfoEntity.Mobile.Number;
        }

        if (guestInfoEntity.EmailDetails != null)
        {
            bookingMap[CommunicationTemplateConstants.EMAIL] = guestInfoEntity.EmailDetails.EmailID;
            if (guestInfoEntity.IsRewardNightGift)
            {
                if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null &&
                    !string.IsNullOrEmpty(LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email))
                {
                    bookingMap[CommunicationTemplateConstants.EMAIL2] = LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email;
                }
            }
        }
    }

    private void CollectGenericGuestEmailDetails(ref Dictionary<string, string> bookingMap, GuestInformationEntity guestInfoEntity)
    {
        if (guestInfoEntity != null)
        {
            bookingMap[CommunicationTemplateConstants.CITY] = guestInfoEntity.City;

            OrderedDictionary countryCodes = DropDownService.GetCountryCodes();
            if (countryCodes != null)
            {
                string country = countryCodes[guestInfoEntity.Country] as string;
                country = country.Replace("\r\n ", "");
                bookingMap[CommunicationTemplateConstants.COUNTRY] = country;
            }

            if (BedTypePreferenceSessionWrapper.UserBedTypePreference != null)
            {
                bookingMap[CommunicationTemplateConstants.BED_TYPE_PREFERENCE] = BedTypePreferenceSessionWrapper.UserBedTypePreference;
            }
            else
            {
                bookingMap[CommunicationTemplateConstants.BED_TYPE_PREFERENCE] = CommunicationTemplateConstants.BLANK_SPACES;
            }

            string selectedRateCategoryID = HotelRoomRateSessionWrapper.SelectedRateCategoryID;
            RateCategory rateCategory = null;
            if ((selectedRateCategoryID != null) && (selectedRateCategoryID != string.Empty))
            {
                rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(selectedRateCategoryID);
            }
            RetrieveSpecialRequestDetails(bookingMap);
            RetrieveChildrenDetails(bookingMap);
        }
    }

    private void CollectDynamicTextFromCMS(ref Dictionary<string, string> bookingMap,
                                           SelectedRoomAndRateEntity selectedRoomAndRateEntity, int guestIterator, GuranteeType guaranteePolicyType)
    {
        string selectedRateCategoryID = selectedRoomAndRateEntity.RateCategoryID;

        string guranteeType = string.Empty;
        RateCategory rateCategory = null;
        if (!string.IsNullOrEmpty(selectedRateCategoryID))
        {
            rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(selectedRateCategoryID);
        }
        bool noRateCodeButDisplayGuarantee;
        string creditCardGuranteeType;
        if (IsTruelyPrepaid(selectedRateCategoryID, SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode,
                            out noRateCodeButDisplayGuarantee, out creditCardGuranteeType))
        {
            string[] prepaidGuranteeTypes = AppConstants.GUARANTEE_TYPE_PRE_PAID;
            if ((prepaidGuranteeTypes != null) && (prepaidGuranteeTypes.Length > 0))
            {
                guranteeType = prepaidGuranteeTypes[0];
            }
        }
        else if (noRateCodeButDisplayGuarantee)
        {
            guranteeType = creditCardGuranteeType;
        }
        else if (null != rateCategory)
        {
            guranteeType = rateCategory.CreditCardGuranteeType;
        }

        string pageIdentifier = string.Empty;
        if (BookingEngineSessionWrapper.IsModifyBooking)
        {
            pageIdentifier = PageIdentifier.ModifyBookingPageIdentifier;
        }
        else
        {
            pageIdentifier = PageIdentifier.BookingPageIdentifier;
        }

        CollectCmsText(bookingMap, guranteeType, pageIdentifier, guestIterator, guaranteePolicyType);

        if ((null != rateCategory) && (rateCategory.RateCategoryId.ToUpper().Equals(SearchType.REDEMPTION.ToString())))
        {
            bookingMap["POLICYTEXT"] = (null != ContentDataAccess.GetTextForEmailTemplate(pageIdentifier)["POLICYTEXT"])
                                           ? ContentDataAccess.GetTextForEmailTemplate(pageIdentifier)["POLICYTEXT"].ToString()
                                           : AppConstants.SPACE;

        }
    }

    private static void RetrieveSpecialRequestDetails(Dictionary<string, string> bookingMap)
    {
        GuestInformationEntity guestInfoEntity = GuestBookingInformationSessionWrapper.GuestBookingInformation as GuestInformationEntity;

        if (guestInfoEntity != null)
        {
            if (guestInfoEntity.SpecialRequests != null)
            {
                string otherPeferencesList = string.Empty;
                string specialRequestList = string.Empty;
                int totalSpecialRequest = guestInfoEntity.SpecialRequests.Length;
                for (int spRequestCount = 0; spRequestCount < totalSpecialRequest; spRequestCount++)
                {
                    switch (guestInfoEntity.SpecialRequests[spRequestCount].RequestValue)
                    {
                        case UserPreferenceConstants.LOWERFLOOR:
                            {
                                otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.LOWERFLOOR);
                                otherPeferencesList += " <br />";
                                break;
                            }
                        case UserPreferenceConstants.HIGHFLOOR:
                            {
                                otherPeferencesList +=
                                    WebUtil.GetTranslatedText(TranslatedTextConstansts.HIGHERFLOOR);
                                otherPeferencesList += " <br />";
                                break;
                            }
                        case UserPreferenceConstants.AWAYFROMELEVATOR:
                            {
                                otherPeferencesList +=
                                    WebUtil.GetTranslatedText(TranslatedTextConstansts.AWAYFROMELEVATOR);
                                otherPeferencesList += " <br />";
                                break;
                            }
                        case UserPreferenceConstants.NEARELEVATOR:
                            {
                                otherPeferencesList +=
                                    WebUtil.GetTranslatedText(TranslatedTextConstansts.NEARELEVATOR);
                                otherPeferencesList += " <br />";
                                break;
                            }
                        case UserPreferenceConstants.NOSMOKING:
                            {
                                otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.NOSMOKING);
                                otherPeferencesList += " <br />";
                                break;
                            }
                        case UserPreferenceConstants.SMOKING:
                            {
                                otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.SMOKING);
                                otherPeferencesList += " <br />";
                                break;
                            }
                        case UserPreferenceConstants.ACCESSIBLEROOM:
                            {
                                otherPeferencesList +=
                                    WebUtil.GetTranslatedText(TranslatedTextConstansts.ACCESSABLEROOM);
                                otherPeferencesList += " <br />";
                                break;
                            }
                        case UserPreferenceConstants.ALLERGYROOM:
                            {
                                otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.ALLERYROOM);
                                otherPeferencesList += " <br />";
                                break;
                            }

                        case UserPreferenceConstants.EARLYCHECKIN:
                            {
                                specialRequestList +=
                                    WebUtil.GetTranslatedText(TranslatedTextConstansts.EARLYCHECKIN);
                                specialRequestList += " <br />";
                                break;
                            }
                        case UserPreferenceConstants.LATECHECKOUT:
                            {
                                specialRequestList +=
                                    WebUtil.GetTranslatedText(TranslatedTextConstansts.LATECHECKOUT);
                                specialRequestList += " <br />";
                                break;
                            }
                        default:
                            break;
                    }
                }
                if (bookingMap == null)
                {
                    bookingMap = new Dictionary<string, string>();
                }
                if (!string.IsNullOrEmpty(otherPeferencesList))
                {
                    bookingMap[CommunicationTemplateConstants.OTHER_PREFERENCE] = otherPeferencesList;
                    string otherPreference = otherPeferencesList.Replace("<br />", "\r\n\t");
                    bookingMap[CommunicationTemplateConstants.TEXTFORMAT_OTHER_PREFERENCE] = otherPreference;
                }
                else
                {
                    bookingMap[CommunicationTemplateConstants.OTHER_PREFERENCE] = CommunicationTemplateConstants.BLANK_SPACES;
                    bookingMap[CommunicationTemplateConstants.TEXTFORMAT_OTHER_PREFERENCE] = CommunicationTemplateConstants.BLANK_SPACES;
                }

                if (string.IsNullOrEmpty(specialRequestList))
                {
                    bookingMap[CommunicationTemplateConstants.SPECIAL_SERVICE_REQUESTS] = specialRequestList;
                    string specilalRequest = specialRequestList.Replace("<br />", "\r\n\t");
                    bookingMap[CommunicationTemplateConstants.TEXTFORMAT_SPECIAL_SERVICE_REQUESTS] = specilalRequest;
                }
                else
                {
                    bookingMap[CommunicationTemplateConstants.SPECIAL_SERVICE_REQUESTS] = CommunicationTemplateConstants.BLANK_SPACES;
                    bookingMap[CommunicationTemplateConstants.TEXTFORMAT_SPECIAL_SERVICE_REQUESTS] = CommunicationTemplateConstants.BLANK_SPACES;
                }
            }
        }
    }

    private static void RetrieveChildrenDetails(Dictionary<string, string> bookingMap)
    {
        GuestInformationEntity guestInfoEntity = GuestBookingInformationSessionWrapper.GuestBookingInformation as GuestInformationEntity;
        if ((guestInfoEntity != null) && (guestInfoEntity.ChildrensDetails != null))
        {
            ChildrensDetailsEntity childrensDetailsEntity = guestInfoEntity.ChildrensDetails;
            bookingMap[CommunicationTemplateConstants.CHILDREN_AGES] = childrensDetailsEntity.GetChildrensAgesInString();

            string cot = string.Empty, extraBed = string.Empty, sharingBed = string.Empty;
            Utility.GetLocaleSpecificChildrenAccomodationTypes(ref cot, ref extraBed, ref sharingBed);
            bookingMap[CommunicationTemplateConstants.CHILDREN_ACCOMMODATION] =
                childrensDetailsEntity.GetChildrensAccommodationInString(sharingBed, cot, extraBed);
        }
        else
        {
            bookingMap[CommunicationTemplateConstants.NO_CHILDREN_AGES] = CommunicationTemplateConstants.BLANK_SPACES;
            bookingMap[CommunicationTemplateConstants.NO_CHILDREN_ACCOMMODATION] = CommunicationTemplateConstants.BLANK_SPACES;
        }
    }

    private bool IsTruelyPrepaid(string rateCategoryId, string blockCode, out bool noRateCodeButDisplayGuarantee, out string creditCardGuranteeType)
    {
        bool returnVaue = false;
        noRateCodeButDisplayGuarantee = false;
        creditCardGuranteeType = string.Empty;
        if (Utility.IsBlockCodeBooking)
        {
            if (rateCategoryId == AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
            {
                Block blockPage = ContentDataAccess.GetBlockCodePages(blockCode);
                if (null != blockPage)
                {
                    if (blockPage.CaptureGuarantee)
                    {
                        noRateCodeButDisplayGuarantee = true;
                        if (!string.IsNullOrEmpty(blockPage.GuranteeType))
                        {
                            creditCardGuranteeType = blockPage.GuranteeType;
                        }
                    }
                    else
                    {
                        returnVaue = true;
                    }
                }
            }
        }
        return returnVaue;
    }

    private void CollectCmsText(Dictionary<string, string> bookingMap, string guranteeType, string pageIdentifier,
                                int guestIterator, GuranteeType guaranteePolicyType)
    {
        Dictionary<string, string> cmsText = null;
        string[] prepaidGuranteeTypes = AppConstants.GUARANTEE_TYPE_PRE_PAID;
        HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
        if ((prepaidGuranteeTypes != null) && (prepaidGuranteeTypes.Contains(guranteeType)) && !ContentDataAccess.GetPaymentFallback(search.SelectedHotelCode))
        {
            cmsText = ContentDataAccess.GetTextForEmailTemplate(guranteeType, pageIdentifier, string.Empty, GenericSessionVariableSessionWrapper.IsPrepaidBooking);

            if ((null != cmsText) && (null != cmsText["POLICYTEXT"]))
            {
                cmsText["POLICYTEXT"] = cmsText["POLICYTEXT"].Replace(CommunicationTemplateConstants.CANCELBYDATE, Utility.GetModifyCancelableDate());

            }
        }
        else if (WebUtil.IsBookingAfterSixPM(search) || Enum.Equals(guaranteePolicyType, GuranteeType.CREDITCARD))
        {
            cmsText = ContentDataAccess.GetTextForEmailTemplate(guranteeType, pageIdentifier,
                      CommunicationTemplateConstants.WITHCREDITCARD, GenericSessionVariableSessionWrapper.IsPrepaidBooking);
        }
        else
        {
            cmsText = ContentDataAccess.GetTextForEmailTemplate(guranteeType, pageIdentifier,
                      CommunicationTemplateConstants.WITHOUTCREDITCARD, GenericSessionVariableSessionWrapper.IsPrepaidBooking);
        }

        if (null != cmsText)
        {
            bookingMap[CommunicationTemplateConstants.CONTACTUS_TEXT] = cmsText.ContainsKey(CommunicationTemplateConstants.CONTACTUS_TEXT)
                    ? cmsText[CommunicationTemplateConstants.CONTACTUS_TEXT] : AppConstants.SPACE;

            bookingMap[CommunicationTemplateConstants.CONFIRMATION_TEXT] = cmsText.ContainsKey(CommunicationTemplateConstants.CONFIRMATION_TEXT)
                    ? cmsText[CommunicationTemplateConstants.CONFIRMATION_TEXT] : AppConstants.SPACE;

            bookingMap[CommunicationTemplateConstants.STORYBOX_TEXT] = cmsText.ContainsKey(CommunicationTemplateConstants.STORYBOX_TEXT)
                    ? cmsText[CommunicationTemplateConstants.STORYBOX_TEXT] : AppConstants.SPACE;

            bookingMap[CommunicationTemplateConstants.HTMLPOLICYHEADER] = cmsText.ContainsKey(CommunicationTemplateConstants.HTMLPOLICYHEADER)
                    ? cmsText[CommunicationTemplateConstants.HTMLPOLICYHEADER] : AppConstants.SPACE;

            SortedList<string, GuestInformationEntity> guestInformationList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
            if (guestInformationList != null)
            {
                bookingMap[CommunicationTemplateConstants.POLICY_TEXT] = cmsText.ContainsKey("POLICYTEXT") ? cmsText["POLICYTEXT"] : AppConstants.SPACE;
            }
        }
        else
        {
            SetupCmsMapToEmptyString(bookingMap);
        }
    }

    private static void SetupCmsMapToEmptyString(Dictionary<string, string> bookingMap)
    {
        if (bookingMap != null)
        {
            string emptyString = AppConstants.SPACE;

            bookingMap[CommunicationTemplateConstants.CONTACTUS_TEXT] = emptyString;
            bookingMap[CommunicationTemplateConstants.CONFIRMATION_TEXT] = emptyString;
            bookingMap[CommunicationTemplateConstants.STORYBOX_TEXT] = emptyString;
            bookingMap[CommunicationTemplateConstants.POLICY_TEXT] = emptyString;
        }
    }

    private void SendEmailOnOWSFailures(string subject, string body)
    {
        EmailEntity emailEntity = new EmailEntity();
        emailEntity.Recipient = AppConstants.EmailRecipientsForOnlinePaymentFailures;
        emailEntity.Sender = AppConstants.EmailSenderForOnlinePaymentFailures;
        emailEntity.Subject = subject;
        emailEntity.Body = body;
        CommunicationService.SendMail(emailEntity, null);
    }
}