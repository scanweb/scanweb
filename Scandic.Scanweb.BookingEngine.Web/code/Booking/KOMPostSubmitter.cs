//  Description					: Class encapsulate specific KOMPOST HTTP POST               //
//																						     //
//-------------------------------------------------------------------------------------------//
/// Author						: Girish Krishnan                                   	     //
/// Author email id				:                           							     //
/// Creation Date				: 26th December  2007									     //
///	Version	#					: 1.0													     //
///------------------------------------------------------------------------------------------//
/// Revison History				: -NA-													     //
///	Last Modified Date			:														     //
///////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Specialized;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// Class encapsulate specific KOMPOST HTTP POST   
    /// </summary>
    public class KOMPostSubmitter : BasePostSubmitter
    {
        #region Public Methods

        /// <summary>
        /// Convert passed entity in to PostItems and do a HTTP POST
        /// with the specified URL
        /// </summary>
        /// <param name="kOMPOSTRequestEntity"></param>
        /// <returns>response for the HTTP POST</returns>
        public string Post(KOMPOSTRequestEntity kOMPOSTRequestEntity)
        {
            this.PostItems = GetNameValueCollection(kOMPOSTRequestEntity);
            this.Type = PostTypeEnum.Post;
            this.Url = HTTPRequestConstants.URL + kOMPOSTRequestEntity.Form;
            return Post();
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        ///  Convert the entity in to collection
        /// </summary>
        /// <param name="kOMPOSTRequestEntity"></param>
        /// <returns>NameValueCollection which contains all the POST Items</returns>
        private NameValueCollection GetNameValueCollection(KOMPOSTRequestEntity kOMPOSTRequestEntity)
        {
            NameValueCollection nameValueCollection = new NameValueCollection();
            nameValueCollection.Add(KOMPOSTItemConstants.EMAIL, kOMPOSTRequestEntity.Email);
            nameValueCollection.Add(KOMPOSTItemConstants.ZIPCODE, kOMPOSTRequestEntity.Zipcode);
            nameValueCollection.Add(KOMPOSTItemConstants.COUNTRY, kOMPOSTRequestEntity.Country);

            if (kOMPOSTRequestEntity.Corporateinformation != null)
            {
                nameValueCollection.Add
                    (KOMPOSTItemConstants.CORPORATE_INFORMATION, kOMPOSTRequestEntity.Corporateinformation);
            }
            if (kOMPOSTRequestEntity.Pressreleases != null)
            {
                nameValueCollection.Add(KOMPOSTItemConstants.PRESS_RELEASES, kOMPOSTRequestEntity.Pressreleases);
            }
            if (kOMPOSTRequestEntity.Meetingandconference != null)
            {
                nameValueCollection.Add
                    (KOMPOSTItemConstants.MEETING_AND_CONFERENCE, kOMPOSTRequestEntity.Meetingandconference);
            }
            if (kOMPOSTRequestEntity.Leisure != null)
            {
                nameValueCollection.Add(KOMPOSTItemConstants.LEISURE, kOMPOSTRequestEntity.Leisure);
            }
            if (kOMPOSTRequestEntity.Companyaddress != null)
            {
                nameValueCollection.Add(KOMPOSTItemConstants.COMPANY_ADDRESS, kOMPOSTRequestEntity.Companyaddress);
            }
            if (kOMPOSTRequestEntity.Companyorganization != null)
            {
                nameValueCollection.Add
                    (KOMPOSTItemConstants.COMPANY_ORGANIZATION, kOMPOSTRequestEntity.Companyorganization);
            }
            if (kOMPOSTRequestEntity.Mobilenumber != null)
            {
                nameValueCollection.Add(KOMPOSTItemConstants.MOBILE_NUMBER, kOMPOSTRequestEntity.Mobilenumber);
            }
            if (kOMPOSTRequestEntity.Gender != null)
            {
                nameValueCollection.Add(KOMPOSTItemConstants.GENDER, kOMPOSTRequestEntity.Gender);
            }
            if (kOMPOSTRequestEntity.Birthyear != null)
            {
                nameValueCollection.Add(KOMPOSTItemConstants.BIRTH_YEAR, kOMPOSTRequestEntity.Birthyear);
            }
            if (kOMPOSTRequestEntity.City != null)
            {
                nameValueCollection.Add(KOMPOSTItemConstants.CITY, kOMPOSTRequestEntity.City);
            }
            if (kOMPOSTRequestEntity.Street != null)
            {
                nameValueCollection.Add(KOMPOSTItemConstants.STREET, kOMPOSTRequestEntity.Street);
            }
            if (kOMPOSTRequestEntity.Title != null)
            {
                nameValueCollection.Add(KOMPOSTItemConstants.TITLE, kOMPOSTRequestEntity.Title);
            }
            if (kOMPOSTRequestEntity.Lastname != null)
            {
                nameValueCollection.Add(KOMPOSTItemConstants.LAST_NAME, kOMPOSTRequestEntity.Lastname);
            }
            if (kOMPOSTRequestEntity.Firstname != null)
            {
                nameValueCollection.Add(KOMPOSTItemConstants.FIRST_NAME, kOMPOSTRequestEntity.Firstname);
            }
            if (kOMPOSTRequestEntity.Sitelanguage != null)
            {
                nameValueCollection.Add(KOMPOSTItemConstants.SITE_LANGUAGE, kOMPOSTRequestEntity.Sitelanguage);
            }

            return nameValueCollection;
        }

        #endregion Private Methods
    }
}