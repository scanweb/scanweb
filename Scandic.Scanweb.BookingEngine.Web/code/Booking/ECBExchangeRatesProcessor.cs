﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.BookingEngine.Web.code.Interface;
using System.Net;
using System.Configuration;
using Scandic.Scanweb.Core;
using System.Globalization;
using System.Text.RegularExpressions;
using Scandic.Scanweb.Entity;
using System.Xml;

namespace Scandic.Scanweb.BookingEngine.Web.code.Booking
{
    public class ECBExchangeRatesProcessor : IExchangeRatesProcessor
    {
        public List<ExchangeRateEntity> GetConvertedExchangeRates(string fromCurrency, string toCurrency, string amount, Dictionary<string, string> allLocaleCurrencies)
        {
            ExchangeRateEntity convertedRatesEntity = new ExchangeRateEntity();
            ConvertedCurrencyEntity convertedCurrencyEntity = new ConvertedCurrencyEntity();
            List<ConvertedCurrencyEntity> convertedCurrencyEntityList = new List<ConvertedCurrencyEntity>();
            List<ExchangeRateEntity> convertedRatesList = new List<ExchangeRateEntity>();
            XmlDocument xdom = new XmlDocument();
            try
            {
                xdom.Load(Convert.ToString(ConfigurationManager.AppSettings[AppConstants.ECBCurrencyConverterFeedUrl]));
                XmlElement root = xdom.DocumentElement;
                Dictionary<string, string> unitExchangerates = new Dictionary<string, string>();
                unitExchangerates.Add(AppConstants.ECBExchageRateBaseCurrency, AppConstants.ONE);
                GetUnitExchangerateList(root.ChildNodes, ref unitExchangerates, allLocaleCurrencies);
                GetALLExchangeRates(unitExchangerates, ref convertedRatesList);
                return convertedRatesList;
            }
            catch(Exception e)
            {
                AppLogger.LogFatalException(e, e.Message);
                return null;
            }
        }
        private void GetALLExchangeRates(Dictionary<string, string> unitExchangerates, ref List<ExchangeRateEntity> convertedRatesList)
        {
            double additionalAmount = Convert.ToDouble(ConfigurationManager.AppSettings[AppConstants.AdditinalExchangeAmount],CultureInfo.InvariantCulture)/100;
            foreach (string fromCurrency in unitExchangerates.Keys)
            {
                double baseCurrency = 1 / Convert.ToDouble(unitExchangerates[fromCurrency], CultureInfo.InvariantCulture);
                Double unitAmount = Double.MinValue;
                List<ConvertedCurrencyEntity> convertedCurrencyEntityList = new List<ConvertedCurrencyEntity>();
                ExchangeRateEntity convertedRatesEntity = new ExchangeRateEntity();
                convertedRatesEntity.FromCurrency = fromCurrency;
                foreach (string toCurrency in unitExchangerates.Keys)
                {                    
                    ConvertedCurrencyEntity convertedCurrencyEntity = new ConvertedCurrencyEntity();
                    if (toCurrency != fromCurrency)
                    {
                        convertedCurrencyEntity.ToCurrency = toCurrency;
                        unitAmount = (Convert.ToDouble(unitExchangerates[toCurrency], CultureInfo.InvariantCulture) * baseCurrency);
                        convertedCurrencyEntity.UnitAmount = unitAmount + (unitAmount*additionalAmount);
                        convertedCurrencyEntityList.Add(convertedCurrencyEntity);
                    }                    
                }
                convertedRatesEntity.ConvertedCurrencies = convertedCurrencyEntityList;
                convertedRatesList.Add(convertedRatesEntity);
            }
        }

        private void GetUnitExchangerateList(XmlNodeList nodeList, ref Dictionary<string, string> unitExchangerates, Dictionary<string, string> allLocaleCurrencies)
        {

            foreach (XmlNode xnod in nodeList)
            {
                if (xnod.NodeType == XmlNodeType.Element)
                {
                    if (xnod.Attributes["currency"] != null && xnod.Attributes["rate"] != null)
                    {
                        if(allLocaleCurrencies.ContainsValue(xnod.Attributes["currency"].Value))
                            unitExchangerates.Add(xnod.Attributes["currency"].Value, xnod.Attributes["rate"].Value);
                    }
                    if (xnod.HasChildNodes)
                    {
                        GetUnitExchangerateList(xnod.ChildNodes, ref unitExchangerates, allLocaleCurrencies);
                    }
                    else if (xnod.NextSibling != null)
                    {
                        GetUnitExchangerateList(xnod.NextSibling.ChildNodes, ref unitExchangerates, allLocaleCurrencies);
                    }
                }
                    
            }
        }        
    }
}

