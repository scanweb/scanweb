﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scandic.Scanweb.BookingEngine.Web.code.Attributes
{
    /// <summary>
    /// This attribute will be used to check the access to the webpage,
    /// when session data is valid or not. In case accessible property of
    /// this attribute retuns false and session is invalid then, webpage 
    /// should be redirected to session error page.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class AccessibleWhenSessionInValid : Attribute
    {
        #region Declaration

        private bool isAccessible = false;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="accessible"></param>
        public AccessibleWhenSessionInValid(bool accessible)
        {
            isAccessible = accessible;
        }

        #endregion

        #region Properties

        public bool Accessible
        {
            get { return isAccessible; }
        }

        #endregion
    }
}
