﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scandic.Scanweb.BookingEngine.Web.code.Attributes
{
    /// <summary>
    /// This attribute will be used to check the access to the webpage when user's session
    /// has expired. In most of the cases all page on session expiry will be redirected 
    /// to the session expiry page, but there will be some pages link start page, error pages
    /// that need to be provided access even in case of session expiry.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class AccessibleWhenSessionExpired : Attribute
    {
        #region Declaration
            private bool isAccessible = false;
            private string expiredPageKeyData = string.Empty;
        #endregion

        #region Constructor

            public AccessibleWhenSessionExpired(bool accessible, string expiredPageKey)
            {
                isAccessible = accessible;
                expiredPageKeyData = expiredPageKey;
            }

        #endregion

        #region Properties
            public bool Accessible { get { return isAccessible; } }
            public string ExpiredPageKey { get { return expiredPageKeyData; } }
        #endregion
    }
}
