﻿
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.BookingEngine.Web.code.Interface;
using Scandic.Scanweb.BookingEngine.Controller;


namespace Scandic.Scanweb.BookingEngine.Web.code.Repository
{

    public class CategoryMapRepository : ICategoryMaps
    {
        public void GetRateTypeCategoryMap()
        {
            RoomRateUtil.RatePlanMap = ContentDataAccess.GetRateTypeCategoryMap(false);
        }

        public void GetRoomTypeCategoryMap()
        {
            RoomRateUtil.RoomTypeMap = ContentDataAccess.GetRoomTypeCategoryMap();
        }
    }
}
