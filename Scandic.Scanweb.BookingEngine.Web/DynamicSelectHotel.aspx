<%@ Page language="c#" Inherits="Scandic.Scanweb.BookingEngine.Web.DynamicSelectHotel" Codebehind="DynamicSelectHotel.aspx.cs" %>

<%@ Register Src="Templates/Booking/Units/BookingModuleMedium.ascx" TagName="BookingModuleMedium"
    TagPrefix="uc1" %>
<%@ Register Src="~/Templates/Booking/Units/SelectHotel.ascx" TagName="SelectHotel" TagPrefix="Booking" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Dynamic Hotel Display - Select Hotel</title>
        <style>
        body
        {
            margin: 0;
            padding-bottom: 0;
	        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	        font-size: 0.75em;   
            text-align: center;    
            background-color: #FFFFFF;
            color: #666666;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/reservation.css") %>?v=<%=CmsUtil.GetCSSVersion()%>" />
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/Javascript/dhtml_utils.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/Javascript/reservation.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/Javascript/AjaxFramework.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/Javascript/Common.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/Javascript/AjaxCall.js") %>"></script>
    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/init.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/autoSuggest.js") %>"></script>
    <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/booking.js") %>"></script>
    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/initCalendar.js") %>"></script>
    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/dateManip.js") %>"></script>
    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/calendar.js") %>"></script>
    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/preCalendar.js") %>"></script>
    <script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/Validation.js") %>"></script>

</head>
<body>
    <form id="Form1" name="sortHotelForm" action="#" method=post runat=server>
    <Booking:SelectHotel id="BE" runat="server">
    </Booking:SelectHotel>
        <br />
        <uc1:BookingModuleMedium ID="BookingModuleMedium1" runat="server" />
    </form>
</body>
</html>