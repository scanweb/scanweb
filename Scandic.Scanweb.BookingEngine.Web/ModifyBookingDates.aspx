<%@ Page language="c#"  AutoEventWireup="true" Inherits="Scandic.Scanweb.BookingEngine.Web.ModifyBookingDates" Codebehind="ModifyBookingDates.aspx.cs" %>
<%@ Register Src ="~/Templates/Booking/Units/ModifyBookingDates.ascx" TagName="ModifyBookingDates" TagPrefix="ucMBD" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title>Modify Booking Dates</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
body {
	color:#666666;
	font-family:Verdana;
	font-size:0.7em;
	font-weight:normal;
	line-height:1.7em;
	text-align:center;
}
</style>
<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/common.css")%>" />
<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/booking.css")%>" />
<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/calendar.css")%>" />
<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/reservation.css")%>" />
<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/loyalty.css")%>" />

 <script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/init.js")%>"></script>
    <script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/prototype.js")%>"></script>
    <script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/dhtml_utils.js")%>"></script>
    <script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/autoSuggest_sitedev.js")%>"></script>
    <script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/dateManip.js")%>"></script>
    <script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/initCalendar.js")%>"></script>
    <script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/booking.js")%>"></script>
    <script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/preCalendar.js")%>"></script>
    <script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/calendar.js")%>"></script>
    <script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/reservation.js")%>"></script>
    <script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/loyalty.js")%>"></script>
    <script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/Validation.js")%>"></script>
    <script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/AjaxFramework.js")%>"></script>
    <script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/Common.js")%>"></script>
    <script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/AjaxCall.js")%>"></script>
    </head>
<body>
    <form id="ModifyBookingDates" runat="server">
    <table width="100%">
    <tr><td>                             
    <ucMBD:ModifyBookingDates ID="MBDates" runat="server" />
    </td></tr> 
    </table>    
    </form>
</body>
</html>