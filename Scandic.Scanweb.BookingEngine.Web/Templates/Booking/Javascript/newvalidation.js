var txtFtName = $fn(_endsWith(("RoomInformation0_txtFNameRoom")));
      var txtFtName1 = $fn(_endsWith(("RoomInformation1_txtFNameRoom")));
      var txtFtName2 = $fn(_endsWith(("RoomInformation2_txtFNameRoom")));
      var txtFtName3 = $fn(_endsWith(("RoomInformation3_txtFNameRoom")));


      var txtLtName = $fn(_endsWith(("RoomInformation0_txtLNameRoom")));
      var txtLtName1 = $fn(_endsWith(("RoomInformation1_txtLNameRoom")));
      var txtLtName2 = $fn(_endsWith(("RoomInformation2_txtLNameRoom")));
      var txtLtName3 = $fn(_endsWith(("RoomInformation3_txtLNameRoom")));

      var countrymd = $fn(_endsWith("RoomInformation0_ddlCountry"));
      var countrymd1 = $fn(_endsWith("RoomInformation1_ddlCountry"));
      var countrymd2 = $fn(_endsWith("RoomInformation2_ddlCountry"));
      var countrymd3 = $fn(_endsWith("RoomInformation3_ddlCountry"));
      
      $(countrymd).change(function() {
          $(mobCodemd).valid();
      });
      $(countrymd1).change(function() {
          $(mobCodemd1).valid();
      });
      $(countrymd2).change(function() {
          $(mobCodemd2).valid();
      });
      $(countrymd3).change(function() {
          $(mobCodemd3).valid();
      });


      var mobCodemd = $fn(_endsWith("RoomInformation0_ddlMobileNumber"));
      var mobCodemd1 = $fn(_endsWith("RoomInformation1_ddlMobileNumber"));
      var mobCodemd2 = $fn(_endsWith("RoomInformation2_ddlMobileNumber"));
      var mobCodemd3 = $fn(_endsWith("RoomInformation3_ddlMobileNumber"));


      var mobilemd = $fn(_endsWith("RoomInformation0_txtTelephone"));
      var mobilemd1 = $fn(_endsWith("RoomInformation1_txtTelephone"));
      var mobilemd2 = $fn(_endsWith("RoomInformation2_txtTelephone"));
      var mobilemd3 = $fn(_endsWith("RoomInformation3_txtTelephone")); 



      var emailmd = $fn(_endsWith("RoomInformation0_txtEmail"));
      var emailmd1 = $fn(_endsWith("RoomInformation1_txtEmail"));
      var emailmd2 = $fn(_endsWith("RoomInformation2_txtEmail"));
      var emailmd3 = $fn(_endsWith("RoomInformation3_txtEmail"));


      var bedtypemd = $fn(_endsWith("RoomInformation0_ddlBedTypePreference"));
      var bedtypemd1 = $fn(_endsWith("RoomInformation1_ddlBedTypePreference"));
      var bedtypemd2 = $fn(_endsWith("RoomInformation2_ddlBedTypePreference"));
      var bedtypemd3 = $fn(_endsWith("RoomInformation3_ddlBedTypePreference"));


	  var fgpnumbermd = $fn(_endsWith("RoomInformation0_txtMembershipNumberRoom"));
      var fgpnumbermd1 = $fn(_endsWith("RoomInformation1_txtMembershipNumberRoom"));
      var fgpnumbermd2 = $fn(_endsWith("RoomInformation2_txtMembershipNumberRoom"));
      var fgpnumbermd3 = $fn(_endsWith("RoomInformation3_txtMembershipNumberRoom"));

      // credit card information
      var cardnamemd = $fn(_endsWith("RoomInformation0_txtCardHolder"));
      var cardnamemd1 = $fn(_endsWith("RoomInformation1_txtCardHolder"));
      var cardnamemd2 = $fn(_endsWith("RoomInformation2_txtCardHolder"));
      var cardnamemd3 = $fn(_endsWith("RoomInformation3_txtCardHolder"));


      var cardnumbermd = $fn(_endsWith("RoomInformation0_txtCardNumber"));
      var cardnumbermd1 = $fn(_endsWith("RoomInformation1_txtCardNumber"));
      var cardnumbermd2 = $fn(_endsWith("RoomInformation2_txtCardNumber"));
      var cardnumbermd3 = $fn(_endsWith("RoomInformation3_txtCardNumber"));
	  
	  var expmonthmd = $fn(_endsWith("RoomInformation0_ddlExpiryMonth"));
	  var expmonthmd1 = $fn(_endsWith("RoomInformation1_ddlExpiryMonth"));
	  var expmonthmd2 = $fn(_endsWith("RoomInformation2_ddlExpiryMonth"));
	  var expmonthmd3 = $fn(_endsWith("RoomInformation3_ddlExpiryMonth"));
	  
	  var expyearmd = $fn(_endsWith("RoomInformation0_ddlExpiryYear"));
	  var expyearmd1 = $fn(_endsWith("RoomInformation1_ddlExpiryYear"));
	  var expyearmd2 = $fn(_endsWith("RoomInformation2_ddlExpiryYear"));
	  var expyearmd3 = $fn(_endsWith("RoomInformation3_ddlExpiryYear"));

	  var cccommonname = $fn(_endsWith("txtCardHolderCommon"));
	  var cccommonnumber = $fn(_endsWith("txtCardNumberCommon"));
	  var cccommontype = $fn(_endsWith("ddlCardTypeCommon"));
	  var ccexpmonth = $fn(_endsWith("ddlExpiryMonthCommon"));
	  var ccexpyear = $fn(_endsWith("ddlExpiryYearCommon"));

	  var panhashcccommon = $fn(_endsWith("ddlPanHashCreditCardsCommon"));
	  var panhashcc = $fn(_endsWith("RoomInformation0_ddlPanHashCreditCards"));
	  var panhashcc1 = $fn(_endsWith("RoomInformation1_ddlPanHashCreditCards"));
	  var panhashcc2 = $fn(_endsWith("RoomInformation2_ddlPanHashCreditCards"));
	  var panhashcc3 = $fn(_endsWith("RoomInformation3_ddlPanHashCreditCards"));
	  
      var cd1 = $(cardnumbermd).attr('id');
      var cd2 = $(cardnumbermd1).attr('id');
      var cd3 = $(cardnumbermd2).attr('id');
      var cd4 = $(cardnumbermd3).attr('id');
      var cdCommon = $(cccommonnumber).attr('id');
      
      var cd0Value = $(cardnumbermd).val();
      var cd1Value = $(cardnumbermd1).val();
      var cd2Value = $(cardnumbermd2).val();
      var cd3Value = $(cardnumbermd3).val();
      var cdCommonValue = $(cccommonnumber).val();

      
	      
      var cc_number_saved = "";
      $(cardnumbermd).focus(function() {
          if ($(this).val() != cc_number_saved) {
              $(this).val(cc_number_saved);
		  }
      });
      $(cardnumbermd).blur(function() {
          cc_number_saved = $(this).val();
          $(this).val($(this).val().replace(/[^\d]/g, ''));
      });


      var cc_number_saved1 = "";
      $(cardnumbermd1).focus(function() {
          if ($(this).val() != cc_number_saved1) {
              $(this).val(cc_number_saved1);
          }
      });
      $(cardnumbermd1).blur(function() {
          cc_number_saved1 = $(this).val();
          $(this).val($(this).val().replace(/[^\d]/g, ''));
      });


      var cc_number_saved2 = "";
      $(cardnumbermd2).focus(function() {
          if ($(this).val() != cc_number_saved2) {
              $(this).val(cc_number_saved2);
          }
      });
      $(cardnumbermd2).blur(function() {
          cc_number_saved2 = $(this).val();
          $(this).val($(this).val().replace(/[^\d]/g, ''));
      });


      var cc_number_saved3 = "";
      $(cardnumbermd3).focus(function() {
          if ($(this).val() != cc_number_saved3) {
              $(this).val(cc_number_saved3);
          }
      });
      $(cardnumbermd3).blur(function() {
          cc_number_saved3 = $(this).val();
          $(this).val($(this).val().replace(/[^\d]/g, ''));
      });

      var cc_number_saved4 = "";
      $(cccommonnumber).focus(function() {
          if ($(this).val() != cc_number_saved4) {
              $(this).val(cc_number_saved4);
          }
      });
      $(cccommonnumber).blur(function() {
          cc_number_saved4 = $(this).val();
          $(this).val($(this).val().replace(/[^\d]/g, ''));
      });


      var cardtypemd = $fn(_endsWith("RoomInformation0_ddlCardType"));
      var cardtypemd1 = $fn(_endsWith("RoomInformation1_ddlCardType"));
      var cardtypemd2 = $fn(_endsWith("RoomInformation2_ddlCardType"));
      var cardtypemd3 = $fn(_endsWith("RoomInformation3_ddlCardType"));
      var cardtypemdCommon = $fn(_endsWith("BookingContactDetails_ddlCardTypeCommon"));

      // enroll field

      var txtEnFNamemd = $fn(_endsWith(("BookingContactDetails_txtEnFName")));
      var txtEnLNamemd = $fn(_endsWith(("BookingContactDetails_txtEnLName")));
      var txtEnAddressLine1md = $fn(_endsWith(("BookingContactDetails_txtEnAddressLine1")));
      var txtEnAddressLine2md = $fn(_endsWith(("BookingContactDetails_txtEnAddressLine2")));
      var txtEnPostCodemd = $fn(_endsWith(("BookingContactDetails_txtEnPostCode")));
      var txtEnCityOrTownmd = $fn(_endsWith(("BookingContactDetails_txtEnCityOrTown")));
      var txtEnEmailmd = $fn(_endsWith(("BookingContactDetails_txtEnEmail")));
      var ddlEnCountrymd = $fn(_endsWith(("BookingContactDetails_ddlEnCountry")));
      var ddlEnTelephoneCodemd = $fn(_endsWith(("BookingContactDetails_ddlEnTelephoneCode")));
      var txtEnTelephonemd = $fn(_endsWith(("BookingContactDetails_txtEnTelephone")));
      var ddlEnDOBDaymd = $fn(_endsWith(("BookingContactDetails_ddlEnDOBDay")));
      var ddlEnDOBMonthmd = $fn(_endsWith(("BookingContactDetails_ddlEnDOBMonth")));
      var ddlEnDOBYearmd = $fn(_endsWith(("BookingContactDetails_ddlEnDOBYear")));
      var gendermd = $fn(_endsWith(("BookingContactDetails_gender")));
      var rdoEnMalemd = $fn(_endsWith(("BookingContactDetails_rdoEnMale")));
      var rdoEnFemalemd = $fn(_endsWith(("BookingContactDetails_rdoEnFemale")));
      var ddlEnPreferredLangmd = $fn(_endsWith(("BookingContactDetails_ddlEnPreferredLang")));
      var txtEnrollPasswordmd = $fn(_endsWith(("BookingContactDetails_txtEnrollPassword")));
      var txtEnrollReTypePasswordmd = $fn(_endsWith(("BookingContactDetails_txtEnrollReTypePassword")));
      var ddlEnPasswordQuestionmd = $fn(_endsWith(("BookingContactDetails_ddlEnPasswordQuestion")));
      var txtEnAnswermd = $fn(_endsWith(("BookingContactDetails_txtEnAnswer")));
      var lblAgeconfirmationmd = $fn(_endsWith(("BookingContactDetails_lblAgeconfirmation")));
      var chkReceiveScandicInfomd = $fn(_endsWith(("BookingContactDetails_chkReceiveScandicInfo")));
      var chkTermsConditionmd = $fn(_endsWith(("BookingContactDetails_chkTermsCondition")));
      var chkMembershipTerms = $fn(_endsWith(("BookingContactDetails_chkMembershipTerms")));


      var rulesObject = {}; // creating the rules object
      var messageObject = {}; // creating the message object



      // Room 0

      $(cardtypemd).change(function() {
         if (cardnumbermd != null && cardnumbermd.value != null && cardnumbermd.value.length != 0) {
              if ($(cardnumbermd).val() != cc_number_saved) {
                 $(cardnumbermd).val(cc_number_saved); 
              }
          $(cardnumbermd).valid();       
         }
       });

      rulesObject[$(txtFtName).attr('name')] = {
          required: true,
          No_Spl_Char: true,
          //notEqualToGroup: ['.distinctusername']
          userNameNotEqualToGroup: ['.distinctusername']
      };
      rulesObject[$(txtLtName).attr('name')] = {
          required: true,
          No_Spl_Char: true,
          userNameNotEqualToGroup: ['.distinctusername']
      };
      rulesObject[$(emailmd).attr('name')] = {
          required: true,
          email: true
      };
      rulesObject[$(countrymd).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(mobCodemd).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(mobilemd).attr('name')] = {
		  required: true,
		  customRegExp: true
      };
      rulesObject[$(bedtypemd).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(cardnamemd).attr('name')] = {
          required: true,
          No_Spl_Char: true
      };
      rulesObject[$(cardnumbermd).attr('name')] = {
          required: true,
          AllowStar: true,
          maxlength: 20,
          checkcredit: true
      };
      rulesObject[$(cardtypemd).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"

      };	  
	  rulesObject[$(expmonthmd).attr('name')] = {
          valueNotEquals: "DFT"
      };
	  rulesObject[$(expyearmd).attr('name')] = {
          valueNotEquals: "DFT"
      };
	  rulesObject[$(fgpnumbermd).attr('name')] = {
          digits: true,
		  notEqualToGroup: ['.distinctfgpnum']
      };



      // Room 1

$(cardtypemd1).change(function() {
    if (cardnumbermd1 != null && cardnumbermd1.value != null && cardnumbermd1.value.length != 0) {
         if ($(cardnumbermd1).val() != cc_number_saved1) {
                $(cardnumbermd1).val(cc_number_saved1); 
            }
        $(cardnumbermd1).valid();
    }
});
      rulesObject[$(txtFtName1).attr('name')] = {
          required: true,
          No_Spl_Char: true,
          //notEqualToGroup: ['.distinctusername']
          userNameNotEqualToGroup: ['.distinctusername']
      };
      rulesObject[$(txtLtName1).attr('name')] = {
          required: true,
          No_Spl_Char: true,
          userNameNotEqualToGroup: ['.distinctusername']
      };
      rulesObject[$(emailmd1).attr('name')] = {
          required: true,
          email: true
      };
      rulesObject[$(countrymd1).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(mobCodemd1).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(mobilemd1).attr('name')] = {
		  required: true,
		  customRegExp: true
      };
      rulesObject[$(bedtypemd1).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(cardnamemd1).attr('name')] = {
          required: true,
          No_Spl_Char: true
      };
      rulesObject[$(cardnumbermd1).attr('name')] = {
          required: true,
          AllowStar: true,
          maxlength: 20,
          checkcredit1: true
      };
      rulesObject[$(cardtypemd1).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
	  rulesObject[$(expmonthmd1).attr('name')] = {
          valueNotEquals: "DFT"
      };
	  rulesObject[$(expyearmd1).attr('name')] = {
          valueNotEquals: "DFT"
      };
	  rulesObject[$(fgpnumbermd1).attr('name')] = {
          digits: true,
		  notEqualToGroup: ['.distinctfgpnum']
      };




      // Room 2

$(cardtypemd2).change(function() {
    if (cardnumbermd2 != null && cardnumbermd2.value != null && cardnumbermd2.value.length != 0) {
     if ($(cardnumbermd2).val() != cc_number_saved2) {
                $(cardnumbermd2).val(cc_number_saved2); 
            }
        $(cardnumbermd2).valid();
    }
});
      rulesObject[$(txtFtName2).attr('name')] = {
          required: true,
          No_Spl_Char: true,
          //notEqualToGroup: ['.distinctusername']
          userNameNotEqualToGroup: ['.distinctusername']
      };
      rulesObject[$(txtLtName2).attr('name')] = {
          required: true,
          No_Spl_Char: true,
          userNameNotEqualToGroup: ['.distinctusername']
      };
      rulesObject[$(emailmd2).attr('name')] = {
          required: true,
          email: true
      };
      rulesObject[$(countrymd2).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(mobCodemd2).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(mobilemd2).attr('name')] = {
		  required: true,
		  customRegExp: true
      };
      rulesObject[$(bedtypemd2).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(cardnamemd2).attr('name')] = {
          required: true,
           No_Spl_Char: true
      };
      rulesObject[$(cardnumbermd2).attr('name')] = {
          required: true,
          AllowStar: true,
          maxlength: 20,
          checkcredit2: true
      };
      rulesObject[$(cardtypemd2).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
	  rulesObject[$(expmonthmd2).attr('name')] = {
          valueNotEquals: "DFT"
      };
	  rulesObject[$(expyearmd2).attr('name')] = {
          valueNotEquals: "DFT"
      };
	  rulesObject[$(fgpnumbermd2).attr('name')] = {
          digits: true,
		  notEqualToGroup: ['.distinctfgpnum']
      };



      // Room 3
$(cardtypemd3).change(function() {
    if (cardnumbermd3 != null && cardnumbermd3.value != null && cardnumbermd3.value.length != 0) {
     if ($(cardnumbermd3).val() != cc_number_saved3) {
                $(cardnumbermd3).val(cc_number_saved3); 
            }
        $(cardnumbermd3).valid();
    }
});


      rulesObject[$(txtFtName3).attr('name')] = {
          required: true,
          No_Spl_Char: true,
          //notEqualToGroup: ['.distinctusername']
          userNameNotEqualToGroup: ['.distinctusername']
      };
      rulesObject[$(txtLtName3).attr('name')] = {
          required: true,
          No_Spl_Char: true,
          userNameNotEqualToGroup: ['.distinctusername']
      };
      rulesObject[$(emailmd3).attr('name')] = {
          required: true,
          email: true
      };
      rulesObject[$(countrymd3).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(mobCodemd3).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(mobilemd3).attr('name')] = {
		  required: true,
		  customRegExp: true
      };
      rulesObject[$(bedtypemd3).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(cardnamemd3).attr('name')] = {
          required: true,
	  No_Spl_Char: true
      };
      rulesObject[$(cardnumbermd3).attr('name')] = {
          required: true,
          AllowStar: true,
          maxlength: 20,
          checkcredit3: true
      };
      rulesObject[$(cardtypemd3).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
	  rulesObject[$(expmonthmd3).attr('name')] = {
          valueNotEquals: "DFT"
      };
	  rulesObject[$(expyearmd3).attr('name')] = {
          valueNotEquals: "DFT"
      };
      rulesObject[$(panhashcccommon).attr('name')] = {
            required: true,
            valueNotEquals: "DFT"
      };
      rulesObject[$(panhashcc).attr('name')] = {
            required: true,
            valueNotEquals: "DFT"
      };
      rulesObject[$(panhashcc1).attr('name')] = {
           required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(panhashcc2).attr('name')] = {
            required: true,
            valueNotEquals: "DFT"
      };
      rulesObject[$(panhashcc3).attr('name')] = {
            required: true,
            valueNotEquals: "DFT"
      };
	  rulesObject[$(fgpnumbermd3).attr('name')] = {
          digits: true,
		  notEqualToGroup: ['.distinctfgpnum']
      };


	  
	  // Common Credit card validation
	  
$(cccommontype).change(function() {
    if (cccommonnumber != null && cccommonnumber.value != null && cccommonnumber.value.length != 0) {
     if ($(cccommonnumber).val() != cc_number_saved4) {
                $(cccommonnumber).val(cc_number_saved4); 
            }
        $(cccommonnumber).valid();
    }
});
	  rulesObject[$(cccommonname).attr('name')] = {
          required: true,
          No_Spl_Char: true
      };
      rulesObject[$(cccommonnumber).attr('name')] = {
          required: true,
          AllowStar: true,
          maxlength: 20,
          checkcreditcommon: true
      };
      rulesObject[$(cccommontype).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
	  rulesObject[$(ccexpmonth).attr('name')] = {
          valueNotEquals: "DFT"
      };
	  rulesObject[$(ccexpyear).attr('name')] = {
          valueNotEquals: "DFT"
      };

      // enroll validation


      rulesObject[$(txtEnFNamemd).attr('name')] = {
          required: true,
          No_Spl_Char: true
      };
      rulesObject[$(txtEnLNamemd).attr('name')] = {
          required: true,
          No_Spl_Char: true
      };
      rulesObject[$(txtEnAddressLine1md).attr('name')] = {
          required: true,
          No_Spl_Char: true
      };
      rulesObject[$(txtEnAddressLine2md).attr('name')] = {
          No_Spl_Char: true
      };
      rulesObject[$(txtEnPostCodemd).attr('name')] = {
          required: true
      };
      rulesObject[$(txtEnCityOrTownmd).attr('name')] = {
          required: true,
          No_Spl_Char: true
      };
      rulesObject[$(txtEnEmailmd).attr('name')] = {
          required: true,
          email: true
      };
      rulesObject[$(ddlEnCountrymd).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(ddlEnTelephoneCodemd).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(txtEnTelephonemd).attr('name')] = {
		  required: true,
		  customRegExp: true

      };

      rulesObject[$(ddlEnDOBDaymd).attr('name')] = {
          required: true,
          groupValidation: "DFT"
      };
      rulesObject[$(ddlEnDOBMonthmd).attr('name')] = {
          required: true,
          groupValidation: "DFT"
      };
      rulesObject[$(ddlEnDOBYearmd).attr('name')] = {
          required: true,
          groupValidation: "DFT"
      };
      //rulesObject[$(ddlEnPreferredLangmd).attr('name')] = {
      //   required: true
      //};
      rulesObject[$(txtEnrollPasswordmd).attr('name')] = {
          required: true
      };
      rulesObject[$(txtEnrollReTypePasswordmd).attr('name')] = {
          required: true,
          equalTo: "#" + [$(txtEnrollPasswordmd).attr('id')]
      };
      rulesObject[$(ddlEnPasswordQuestionmd).attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$(txtEnAnswermd).attr('name')] = {
          required: true
      };
      rulesObject[$(lblAgeconfirmationmd).attr('name')] = {
          required: true
      };
      rulesObject[$(chkReceiveScandicInfomd).attr('name')] = {
          required: true
      };
      rulesObject[$(chkTermsConditionmd).attr('name')] = {
          required: true
      };
      rulesObject[$(chkMembershipTerms).attr('name')] = {
          required: true
      };



      // messages begins room0


      messageObject[$(txtFtName).attr('name')] = {
          required: $('#requiredFldfirstname').val(),
          No_Spl_Char: $('#restrictSplChars').val(),
          //notEqualToGroup: $('#sameGuestNames').val()
          userNameNotEqualToGroup: $('#sameGuestNames').val()
      };
      messageObject[$(txtLtName).attr('name')] = {
          required: $('#requiredFldlastname').val(),
          No_Spl_Char: $('#restrictSplChars').val(),
          userNameNotEqualToGroup: $('#sameGuestNames').val()
      };
      messageObject[$(emailmd).attr('name')] = {
          required: $('#emailmessage').val(),
          email: $('#invalidemailmessage').val()
      };
      messageObject[$(countrymd).attr('name')] = {
          required: $('#cntrymessage').val(),
          valueNotEquals: $('#cntrymessage').val()
      };
      messageObject[$(mobCodemd).attr('name')] = {
          required: $('#cntrycodemessage').val(),
          valueNotEquals: $('#cntrycodemessage').val()
      };
      messageObject[$(mobilemd).attr('name')] = {
          required: $('#phonemessage').val(),
		  customRegExp: $('#telephone').val()
      };
      messageObject[$(bedtypemd).attr('name')] = {
          required: $('#bedprefmessage').val(),
          valueNotEquals: $('#bedprefmessage').val()
      };
      messageObject[$(cardnamemd).attr('name')] = {
          required: $('#creditname').val(),
	  No_Spl_Char: $('#restrictSplChars').val()
      };
      messageObject[$(cardnumbermd).attr('name')] = {
          required: $('#creditnumber').val(),
          AllowStar: $('#validcreditcard').val(),
    checkcredit: $('#validcreditcard').val()
      };
      messageObject[$(cardtypemd).attr('name')] = {
          required: $('#cccardtype').val(),
          valueNotEquals: $('#cccardtype').val()
      };
	            
	  messageObject[$(expmonthmd).attr('name')] = {
		valueNotEquals: $('#ccmonthmessage').val()
      };
	  messageObject[$(expyearmd).attr('name')] = {
		valueNotEquals: $('#ccyearmessage').val()
      };	
				
	  messageObject[$(fgpnumbermd).attr('name')] = {
		  digits: $('#invalidfgp').val(),
		  notEqualToGroup: $('#dontMatch').val()
      };
	  


      // messages begins room1


      messageObject[$(txtFtName1).attr('name')] = {
          required: $('#requiredFldfirstname').val(),
          No_Spl_Char: $('#restrictSplChars').val(),
          //notEqualToGroup: $('#sameGuestNames').val()
          userNameNotEqualToGroup: $('#sameGuestNames').val()
      };
      messageObject[$(txtLtName1).attr('name')] = {
          required: $('#requiredFldlastname').val(),
          No_Spl_Char: $('#restrictSplChars').val(),
          userNameNotEqualToGroup: $('#sameGuestNames').val()
      };
      messageObject[$(emailmd1).attr('name')] = {
          required: $('#emailmessage').val(),
          email: $('#invalidemailmessage').val()
      };
      messageObject[$(countrymd1).attr('name')] = {
          required: $('#cntrymessage').val(),
          valueNotEquals: $('#cntrymessage').val()
      };
      messageObject[$(mobCodemd1).attr('name')] = {
          required: $('#cntrycodemessage').val(),
          valueNotEquals: $('#cntrycodemessage').val()
      };
      messageObject[$(mobilemd1).attr('name')] = {
          required: $('#phonemessage').val(),
		  customRegExp: $('#telephone').val()
      };
      messageObject[$(bedtypemd1).attr('name')] = {
          required: $('#bedprefmessage').val(),
          valueNotEquals: $('#bedprefmessage').val()
      };
      messageObject[$(cardnamemd1).attr('name')] = {
          required: $('#creditname').val(),
	  No_Spl_Char: $('#restrictSplChars').val()
      };
      messageObject[$(cardnumbermd1).attr('name')] = {
          required: $('#creditnumber').val(),
          AllowStar: $('#validcreditcard').val(),
          checkcredit: $('#validcreditcard').val()
      };
      messageObject[$(cardtypemd1).attr('name')] = {
          required: $('#cccardtype').val(),
          valueNotEquals: $('#cccardtype').val()
      };
	  messageObject[$(expmonthmd1).attr('name')] = {
		valueNotEquals: $('#ccmonthmessage').val()
      };
	  messageObject[$(expyearmd1).attr('name')] = {
		valueNotEquals: $('#ccyearmessage').val()
      };	
	  messageObject[$(fgpnumbermd1).attr('name')] = {
		  digits: $('#invalidfgp').val(),
		  notEqualToGroup: $('#dontMatch').val()
      };


      // messages begins room2


      messageObject[$(txtFtName2).attr('name')] = {
          required: $('#requiredFldfirstname').val(),
          No_Spl_Char: $('#restrictSplChars').val(),
          //notEqualToGroup: $('#sameGuestNames').val()
          userNameNotEqualToGroup: $('#sameGuestNames').val()
      };
      messageObject[$(txtLtName2).attr('name')] = {
          required: $('#requiredFldlastname').val(),
          No_Spl_Char: $('#restrictSplChars').val(),
          userNameNotEqualToGroup: $('#sameGuestNames').val()
      };
      messageObject[$(emailmd2).attr('name')] = {
          required: $('#emailmessage').val(),
          email: $('#invalidemailmessage').val()
      };
      messageObject[$(countrymd2).attr('name')] = {
          required: $('#cntrymessage').val(),
          valueNotEquals: $('#cntrymessage').val()
      };
      messageObject[$(mobCodemd2).attr('name')] = {
          required: $('#cntrycodemessage').val(),
          valueNotEquals: $('#cntrycodemessage').val()
      };
      messageObject[$(mobilemd2).attr('name')] = {
          required: $('#phonemessage').val(),
		  customRegExp: $('#telephone').val()
      };
      messageObject[$(bedtypemd2).attr('name')] = {
          required: $('#bedprefmessage').val(),
          valueNotEquals: $('#bedprefmessage').val()
      };
      messageObject[$(cardnamemd2).attr('name')] = {
          required: $('#creditname').val(),
 No_Spl_Char: $('#restrictSplChars').val()
      };
      messageObject[$(cardnumbermd2).attr('name')] = {
          required: $('#creditnumber').val(),
          AllowStar: $('#validcreditcard').val(),
  checkcredit: $('#validcreditcard').val()
      };
      messageObject[$(cardtypemd2).attr('name')] = {
          required: $('#cccardtype').val(),
          valueNotEquals: $('#cccardtype').val()
      };
	  
	  messageObject[$(expmonthmd2).attr('name')] = {
		valueNotEquals: $('#ccmonthmessage').val()
      };
	  messageObject[$(expyearmd2).attr('name')] = {
		valueNotEquals: $('#ccyearmessage').val()
      };
	  messageObject[$(fgpnumbermd2).attr('name')] = {
		  digits: $('#invalidfgp').val(),
		  notEqualToGroup: $('#dontMatch').val()
      };

      // messages begins room3


      messageObject[$(txtFtName3).attr('name')] = {
          required: $('#requiredFldfirstname').val(),
          No_Spl_Char: $('#restrictSplChars').val(),
          //notEqualToGroup: $('#sameGuestNames').val()
          userNameNotEqualToGroup: $('#sameGuestNames').val()
      };
      messageObject[$(txtLtName3).attr('name')] = {
          required: $('#requiredFldlastname').val(),
          No_Spl_Char: $('#restrictSplChars').val(),
          userNameNotEqualToGroup: $('#sameGuestNames').val()
      };
      messageObject[$(emailmd3).attr('name')] = {
          required: $('#emailmessage').val(),
          email: $('#invalidemailmessage').val()
      };
      messageObject[$(countrymd3).attr('name')] = {
          required: $('#cntrymessage').val(),
          valueNotEquals: $('#cntrymessage').val()
      };
      messageObject[$(mobCodemd3).attr('name')] = {
          required: $('#cntrycodemessage').val(),
          valueNotEquals: $('#cntrycodemessage').val()
      };
      messageObject[$(mobilemd3).attr('name')] = {
          required: $('#phonemessage').val(),
		  customRegExp: $('#telephone').val()
      };
      messageObject[$(bedtypemd3).attr('name')] = {
          required: $('#bedprefmessage').val(),
          valueNotEquals: $('#bedprefmessage').val()
      };
      messageObject[$(cardnamemd3).attr('name')] = {
          required: $('#creditname').val(),
  No_Spl_Char: $('#restrictSplChars').val()
      };
      messageObject[$(cardnumbermd3).attr('name')] = {
          required: $('#creditnumber').val(),
          AllowStar: $('#validcreditcard').val(),
 checkcredit: $('#validcreditcard').val()
      };
      messageObject[$(cardtypemd3).attr('name')] = {
          required: $('#cccardtype').val(),
          valueNotEquals: $('#cccardtype').val()
      };
	  messageObject[$(expmonthmd3).attr('name')] = {
		valueNotEquals: $('#ccmonthmessage').val()
      };
	  messageObject[$(expyearmd3).attr('name')] = {
		valueNotEquals: $('#ccyearmessage').val()
      };		  
	  messageObject[$(fgpnumbermd3).attr('name')] = {
		  digits: $('#invalidfgp').val(),
		  notEqualToGroup: $('#dontMatch').val()
       };
       messageObject[$(cardtypemd3).attr('name')] = {
           required: $('#cccardtype').val(),
           valueNotEquals: $('#cccardtype').val()
       };

       messageObject[$(panhashcccommon).attr('name')] = {
            required: $('#panhashccvalidatationmsg').val(),
            valueNotEquals: $('#panhashccvalidatationmsg').val()
       };

       messageObject[$(panhashcc).attr('name')] = {
           required: $('#panhashccvalidatationmsg').val(),
           valueNotEquals: $('#panhashccvalidatationmsg').val()
       };

       messageObject[$(panhashcc1).attr('name')] = {
           required: $('#panhashccvalidatationmsg').val(),
           valueNotEquals: $('#panhashccvalidatationmsg').val()
       };

       messageObject[$(panhashcc2).attr('name')] = {
           required: $('#panhashccvalidatationmsg').val(),
           valueNotEquals: $('#panhashccvalidatationmsg').val()
       };

       messageObject[$(panhashcc3).attr('name')] = {
           required: $('#panhashccvalidatationmsg').val(),
           valueNotEquals: $('#panhashccvalidatationmsg').val()
       };

	  
	  
	  // Common Credit card message
	  
	     messageObject[$(cccommonname).attr('name')] = {
          required: $('#creditname').val(),
No_Spl_Char: $('#restrictSplChars').val()
      };
      messageObject[$(cccommonnumber).attr('name')] = {
          required: $('#creditnumber').val(),
          AllowStar: $('#validcreditcard').val()
      };
      messageObject[$(cccommontype).attr('name')] = {
          required: $('#cccardtype').val(),
          valueNotEquals: $('#cccardtype').val()
      };
	            
	  messageObject[$(ccexpmonth).attr('name')] = {
		valueNotEquals: $('#ccmonthmessage').val()
      };
	  messageObject[$(ccexpyear).attr('name')] = {
		valueNotEquals: $('#ccyearmessage').val()
      };
	  
	  
	  
      // enroll guest messages

      messageObject[$(txtEnFNamemd).attr('name')] = {
          required: $('#requiredFldfirstname').val(),
          No_Spl_Char: $('#restrictSplChars').val()
      };
      messageObject[$(txtEnLNamemd).attr('name')] = {
          required: $('#requiredFldlastname').val(),
          No_Spl_Char: $('#restrictSplChars').val()
      };
      messageObject[$(txtEnAddressLine1md).attr('name')] = {
          required: $('#valaddress').val(),
          No_Spl_Char: $('#restrictSplChars').val()
      };
      messageObject[$(txtEnAddressLine2md).attr('name')] = {
          No_Spl_Char: $('#restrictSplChars').val()
      };
      messageObject[$(txtEnPostCodemd).attr('name')] = {
          required: $('#valpostal').val(),
          digits: $('#valpostal').val()
      };
      messageObject[$(txtEnCityOrTownmd).attr('name')] = {
          required: $('#encity').val(),
          No_Spl_Char: $('#restrictSplChars').val()
      };
      messageObject[$(txtEnEmailmd).attr('name')] = {
          required: $('#emailmessage').val(),
          email: $('#invalidemailmessage').val()
      };
      messageObject[$(ddlEnCountrymd).attr('name')] = {
          required: $('#cntrymessage').val(),
          valueNotEquals: $('#cntrymessage').val()
      };
      messageObject[$(ddlEnTelephoneCodemd).attr('name')] = {
          required: $('#cntrycodemessage').val(),
          valueNotEquals: $('#cntrycodemessage').val()
      };
      messageObject[$(txtEnTelephonemd).attr('name')] = {
          required: $('#phonemessage').val(),
		  customRegExp: $('#telephone').val()
      };



      messageObject[$(ddlEnDOBDaymd).attr('name')] = {
          required: $('#CheckboxMultiple').val(),
          groupValidation: $('#CheckboxMultiple').val()
      };
      messageObject[$(ddlEnDOBMonthmd).attr('name')] = {
          required: $('#CheckboxMultiple').val(),
          groupValidation: $('#CheckboxMultiple').val()
      };
      messageObject[$(ddlEnDOBYearmd).attr('name')] = {
          required: $('#CheckboxMultiple').val(),
          groupValidation: $('#CheckboxMultiple').val()
      };
      messageObject[$(ddlEnPreferredLangmd).attr('name')] = {
          required: $('#prefdlang').val()
      };
      messageObject[$(txtEnrollPasswordmd).attr('name')] = {
          required: $('#passwordmessage').val()
      };
      messageObject[$(txtEnrollReTypePasswordmd).attr('name')] = {
          required: $('#passwordmessage').val(),
          equalTo: $('#samepassword').val()
      };
      messageObject[$(ddlEnPasswordQuestionmd).attr('name')] = {
          required: $('#sercurityquestion').val(),
          valueNotEquals: $('#sercurityquestion').val()
      };
      messageObject[$(txtEnAnswermd).attr('name')] = {
          required: $('#secanswer').val()
      };
      messageObject[$(lblAgeconfirmationmd).attr('name')] = {
          required: $('#plconfirm').val()
      };
      messageObject[$(chkReceiveScandicInfomd).attr('name')] = {
          required: $('#plconfirm').val()
      };
      messageObject[$(chkTermsConditionmd).attr('name')] = {
      required: $('#plagree').val()
      };
      messageObject[$(chkMembershipTerms).attr('name')] = {
          required: $('#plagree').val()
      };

      $(document).ready(function() {
          // validate the comment form when it is submitted
          $("#aspnetForm").validate({
              onfocusout: function(element) { $(element).valid(); },
              onkeyup: false,
              success: function(label) {
                  label.addClass("validfld").text(" ");
				  var name = label.attr('htmlfor');
				  var emailsenttext = $('#emailwillbesent').val();
				  if (name == "ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_RoomInformation0_txtEmail") {
				      var emailvalue = $(emailmd).val();
				      // label.addClass("validfld").text("Email will be sent to: " + emailvalue);
				      label.addClass("validfld emailwidth").text(emailsenttext + " " + emailvalue);
				  }
				  else if (name == "ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_RoomInformation1_txtEmail") {
				      var emailvalue1 = $(emailmd1).val();
				      //label.addClass("validfld").text("Email will be sent to: " + emailvalue1);
					   label.addClass("validfld emailwidth").text(emailsenttext + " " + emailvalue1);
				  }
				  else if (name == "ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_RoomInformation2_txtEmail") {
				      var emailvalue2 = $(emailmd2).val();
				      // label.addClass("validfld").text("Email will be sent to: " + emailvalue2);
					   label.addClass("validfld emailwidth").text(emailsenttext + " " + emailvalue2);
				  }
				  else if (name == "ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_RoomInformation3_txtEmail") {
				      var emailvalue3 = $(emailmd3).val();
				     // label.addClass("validfld").text("Email will be sent to: " + emailvalue3);
					   label.addClass("validfld emailwidth").text(emailsenttext + " " + emailvalue3);
				  }
              },
			  groups: {
			  dobGroup: "ctl00$FullBodyRegion$MainBodyRegion$ctl00$BookingContactDetails$ddlEnDOBDay ctl00$FullBodyRegion$MainBodyRegion$ctl00$BookingContactDetails$ddlEnDOBMonth ctl00$FullBodyRegion$MainBodyRegion$ctl00$BookingContactDetails$ddlEnDOBYear"},
			 // telephoneGroup: "ctl00$MainBodyRegion$MainBodyLeftRegion$ctl04$ddlDOBDay ctl00$MainBodyRegion$MainBodyLeftRegion$ctl04$ddlDOBMonth ctl00$MainBodyRegion$MainBodyLeftRegion$ctl04$ddlDOBYear"			  
              validClass: "validfld",
              rules: rulesObject,
              messages: messageObject,
              errorElement: "div",
			  ignore: ".ignore, :hidden",
              errorPlacement: function(error, element) {
				var next = element.next();
				if (next.hasClass('toolTipMe')){
					$(error).insertAfter(next);
				}else if (element.attr("name") == $("select[id$='ddlEnDOBDay']").attr('name') || element.attr("name") == $("select[id$='ddlEnDOBMonth']").attr('name') || element.attr("name") == $("select[id$='ddlEnDOBYear']").attr('name')) {
					$(error).insertAfter($("select[id$='ddlEnDOBYear']"));}
				else if(next.hasClass('signupTandC')){					
					$(error).prependTo(next);					
				}else{
					$(error).insertAfter(element);
				}
              },
              submitHandler: function(form) {
                  $fn(_endsWith("SubmitButtonDiv")).style.display = "none";
                  $fn(_endsWith("BookingProgressDiv")).style.display = "block";
                  isClicked = true;
                  form.submit();
              }
          });
		  			// this function requires month day and year selections
		$.validator.addMethod("groupValidation", function(value, element, arg) {
			if($("#ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_ddlEnDOBDay option:selected").val() != arg && 
			$("#ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_ddlEnDOBMonth option:selected").val() != arg && 
			$("#ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_ddlEnDOBYear option:selected").val() != arg) {
				$("#ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_ddlEnDOBDay, #ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_ddlEnDOBMonth, #ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_ddlEnDOBYear").removeClass("error");
				  return true;
				} else {
				  return false;
				}
		}, "Please select valid date of birth");
      });
	  	$.validator.addMethod("customRegExp", function(value){
			
				var regEx = new RegExp("^[\\s\\d()\\/\\-]*$");
				return regEx.test(value);
				
			}, "Select proper phone");

/*================================================================================================*/

/*

This routine checks the credit card number. The following checks are made:

1. A number has been provided
2. The number is a right length for the card
3. The number has an appropriate prefix for the card
4. The number has a valid modulus 10 number check digit if required

If the validation fails an error is reported.

The structure of credit card formats was gleaned from a variety of sources on the web, although the 
best is probably on Wikepedia ("Credit card number"):

http://en.wikipedia.org/wiki/Credit_card_number

Parameters:
cardnumber           number on the card
cardname             name of card as defined in the card list below

Author:     John Gardner
Date:       1st November 2003
Updated:    26th Feb. 2005      Additional cards added by request
Updated:    27th Nov. 2006      Additional cards added from Wikipedia
Updated:    18th Jan. 2008      Additional cards added from Wikipedia
Updated:    26th Nov. 2008      Maestro cards extended
Updated:    19th Jun. 2009      Laser cards extended from Wikipedia
Updated:    11th Sep. 2010      Typos removed from Diners and Solo definitions (thanks to Noe Leon)
Updated:    10th April 2012     New matches for Maestro, Diners Enroute and Switch
Updated:    17th October 2012   Diners Club prefix 38 not encoded

*/

/*
If a credit card number is invalid, an error reason is loaded into the global ccErrorNo variable. 
This can be be used to index into the global error  string array to report the reason to the user 
if required:
   
e.g. if (!checkCreditCard (number, name) alert (ccErrors(ccErrorNo);
*/

var ccErrorNo = 0;
var ccErrors = new Array();

ccErrors[0] = "Unknown card type";
ccErrors[1] = "No card number provided";
ccErrors[2] = "Credit card number is in invalid format";
ccErrors[3] = "Credit card number is invalid";
ccErrors[4] = "Credit card number has an inappropriate number of digits";
ccErrors[5] = "Warning! This credit card number is associated with a scam attempt";

function checkCreditCard(cardnumber, cardname,cardcontrol) {

    // Array to hold the permitted card characteristics
    var cards = new Array();

    // Define the cards we support. You may add addtional card types as follows.
    //  Name:         As in the selection box of the form - must be same as user's
    //  Length:       List of possible valid lengths of the card number for the card
    //  prefixes:     List of possible prefixes for the card
    //  checkdigit:   Boolean to say whether there is a check digit

    cards[0] = { name: "VA",
        length: "13,16",
        prefixes: "4",
        checkdigit: true
    };
    cards[1] = { name: "MC",
        length: "16",
        prefixes: "51,52,53,54,55",
        checkdigit: true
    };
    cards[2] = { name: "DC",
        length: "14,16",
        prefixes: "36,38,54,55",
        checkdigit: true
    };
    cards[3] = { name: "AX",
        length: "15",
        prefixes: "34,37",
        checkdigit: true
    };
   
    // Establish card type
    var cardType = -1;
    for (var i = 0; i < cards.length; i++) {

        // See if it is this card (ignoring the case of the string)
        if (cardname.toLowerCase() == cards[i].name.toLowerCase()) {
            cardType = i;
            break;
        }
    }

    // If card type is "Visa Dankort" no need of validation
    if(cardname == "DK")
    {
        return true;
    }
    
    // If card type not found, report an error
    if (cardType == -1) {
        ccErrorNo = 0;
        return false;
    }

    // Ensure that the user has provided a credit card number
    if (cardnumber.length == 0) {
        ccErrorNo = 1;
        return false;
    }

    // Now remove any spaces from the credit card number
    cardnumber = cardnumber.replace(/\s/g, "");

    // Check that the number is numeric
	var cardNo = cardnumber;
    var cardexp = /^[0-9]{13,19}$/;


	var ctrlValue = null;
		    if (cardcontrol.search("_") >= 1){
			    var ctrlIndex = cardcontrol.substring(cardcontrol.indexOf("_") - 1,cardcontrol.indexOf("_"));
			    switch (ctrlIndex) {
			    case '0':
				    ctrlValue = cd0Value;
				    break;
			    case '1':
				    ctrlValue = cd1Value;
				    break;
				case '2':
				    ctrlValue = cd2Value;
					break;
			    case '3':
				    ctrlValue = cd3Value;
				    break;
			    }
		       } else {
			    ctrlValue = cdCommonValue;
		    }
	if(cardnumber == ctrlValue) {
		return true;

	} else {
		
		if (!cardexp.exec(cardNo)) {
            ccErrorNo = 2;
            return false;
        }
	}
	

    // Now check the modulus 10 check digit - if required
    if (cards[cardType].checkdigit) {
        var checksum = 0;                                  // running checksum total
        var mychar = "";                                   // next char to process
        var j = 1;                                         // takes value of 1 or 2

        // Process each digit one by one starting at the right
        var calc;
        for (i = cardNo.length - 1; i >= 0; i--) {

            // Extract the next digit and multiply by 1 or 2 on alternative digits.
            calc = Number(cardNo.charAt(i)) * j;

            // If the result is in two digits add 1 to the checksum total
            if (calc > 9) {
                checksum = checksum + 1;
                calc = calc - 10;
            }

            // Add the units element to the checksum total
            checksum = checksum + calc;

            // Switch the value of j
            if (j == 1) { j = 2 } else { j = 1 };
        }

        // All done - if checksum is divisible by 10, it is a valid modulus 10.
        // If not, report an error.
        if (checksum % 10 != 0) {
            ccErrorNo = 3;
            return false;
        }
    }

    // Check it's not a spam number
    if (cardNo == '5490997771092064') {
        ccErrorNo = 5;
        return false;
    }

    // The following are the card-specific checks we undertake.
    var LengthValid = false;
    var PrefixValid = false;
    var undefined;

    // We use these for holding the valid lengths and prefixes of a card type
    var prefix = new Array();
    var lengths = new Array();

    // Load an array with the valid prefixes for this card
    prefix = cards[cardType].prefixes.split(",");

    // Now see if any of them match what we have in the card number
    for (i = 0; i < prefix.length; i++) {
        var exp = new RegExp("^" + prefix[i]);
        if (exp.test(cardNo)) PrefixValid = true;
    }

    // If it isn't a valid prefix there's no point at looking at the length
    if (!PrefixValid) {
        ccErrorNo = 3;
        return false;
    }

    // See if the length is valid for this card
    lengths = cards[cardType].length.split(",");
    for (j = 0; j < lengths.length; j++) {
        if (cardNo.length == lengths[j]) LengthValid = true;
    }

    // See if all is OK by seeing if the length was valid. We only check the length if all else was 
    // hunky dory.
    if (!LengthValid) {
        ccErrorNo = 4;
        return false;
    };

    // The credit card is in the required format.
    return true;
}

/*================================================================================================*/
	  