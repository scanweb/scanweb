//jQuery.noConflict();    

jQuery(function($) {
    var messageElement = $('#CrisisContainer')[0];
    var CrisisFlagValue = $('input#CrisisFlag').val();

    if (CrisisFlagValue == 'true') {
        $.blockUI({
            message: messageElement,
            css: { width: '450px', height: 'auto', left: '50%', marginLeft: '-225px', textAlign: 'left', border: 'none', cursor: 'normal', backgroundColor: 'transparent' }
        });

        //setCookie('CrisisShown', CrisisFlagValue, cookieExpDate, '', '', '');
    } else {
        document.onclick = closePopupDivs;
    }
    $('.skip_btn').click($.unblockUI);
});

//method to close the loginpoup when clicked on the document
jQuery(document).click(function(e) {
    if ($(e.target).is('#Login, #Login *')) {
        return;
    }
    else {
        $('#Login_Box').hide();
        $('#LogoutControl').hide();
        $('#LoginControl').css('visibility', 'visible');
    }
});


$(document).ready(function() {
    var pwdPwd = $('.password-password');
    if (pwdPwd.length > 0 && $(pwdPwd)[0] != undefined && $(pwdPwd)[0].value != "") {
        $('.password-clear').hide();
        $('.password-password').show();
    }
    else {
        $('.password-clear').show();
        $('.password-password').hide();
    }
    $('input.input').focus(function() {

        var default_value = $(this).attr('rel');
        if (this.value == default_value) {
            this.value = '';
            $(this).removeClass('defaultColor');
        }
    });

    $('input.input').blur(function() {
        $(this).removeClass('defaultColor');
        var default_value = $(this).attr('rel');
        if (this.value == '') {
            this.value = default_value;
            $(this).addClass('defaultColor');
            ////RK: Release 2.0 | Enroll User | Show the Enroll div if the FGP number is not entered
            var txtMembership = $fn(_endsWith("_txtMembershipNumberRoom"));
            if (txtMembership != null && txtMembership.id == this.id
	        && txtMembership.value == default_value) {
                $("div.bookingDetailWrapper div.FGP").show();
            }
        }
    });

    //suresh:booking details FGP
    $('.extrWidthBxMember input.input').keyup(function(event) {
        if (event.keyCode == '13') {
            event.preventDefault();
        }
        ////RK: Release 2.0 | Enroll User | Hiding the Enroll div if the FGP number is entered
        var default_value = $(this).attr('rel');
        var txtMembership = $fn(_endsWith("_txtMembershipNumberRoom"));
        //Check if the the textbox being focussed is the membership box and its value is not same as rel text
        if (txtMembership != null && txtMembership.id == this.id) {
            if ($("div.bookingDetailWrapper div.FGP:visible")) {
                $("div.bookingDetailWrapper div.FGP").hide();
            }
            if (txtMembership.value == "") {
                $("div.bookingDetailWrapper div.FGP").show();
            }

        }

    });

    $('input.password-clear').focus(function() {
        var txtHTMLTxtObj = $(this);
        var txtASPTxtBoxObj = $(this).prev();
        $(txtHTMLTxtObj).hide();
        $(txtASPTxtBoxObj).show().focus();
    });

    $('input.password-password').blur(function() {
        var txtHTMLTxtObj = $(this).next();
        var txtASPTxtBoxObj = $(this);

        if ($(txtASPTxtBoxObj).val() == '') {
            $(txtHTMLTxtObj).show();
            $(txtASPTxtBoxObj).hide();
        }
    });



    $('.pwdTxt').focus(function() {
        $(this).hide();
        $(this).prev().show();
        $(this).prev().focus();
        if ($(this).hasClass('mandatory')) {
            $(this).prev().addClass('mandatory');
        }

    })


});

$(function() {
    $('.password-password').blur(function() {
        $(this).removeClass('defaultColor');
        var default_value = $(this).attr('rel');
        if (this.value == '') {
            //3423842:Password field still shows yellow| Log in module
            if ($(this).hasClass('ShowColor')) {
                $(this).addClass('mandatory');
                $(this).next().addClass('mandatory');
            }
            $(this).next().val(default_value);
            $(this).next().show();
            $(this).hide();

            var txtMembership = $fn(_endsWith("_txtMembershipNumberRoom"));
            if (txtMembership != null && txtMembership.id == this.id
	        && txtMembership.value == default_value) {
                $("div.bookingDetailWrapper div.FGP").show();
            }
        }

    });
});

$(function() {

    var divObj = $('.cookieLaw');
    var cookieLawCookie = getCookie('cookieLaw');

    if (cookieLawCookie != 'true') {
        $(divObj).show();
    }

    $('.cookielink').click(function() {

        $(divObj).animate({ top: "+=15px", opacity: 0 }, "slow");
        $(divObj).hide();
        var date = new Date();
        date.setMonth(date.getMonth() + 1);
        var expires = "; expires=" + date.toGMTString();
        document.cookie = "cookieLaw=true" + expires + "; path=/";

    });

});

$(document).ready(function() {
    $("input[id*='btnSearch']").click(function() {
        var valid = true;
        var ParticipantVal = $("input[id*='txtParticipants']").val();
        if (isNaN(ParticipantVal)) {
            validateParticipant();
            valid = false;
        }
        else if (ParticipantVal.indexOf(".") != -1 || ParticipantVal.indexOf("-") != -1) {
            validateParticipant();
            valid = false;
        }
        var HotelVal = $("input[id*='selectedDestIdFGP']").val();
        if (HotelVal == null || HotelVal.length == 0) {
            $("input[id$='txtdestName']").addClass('error');
            var errorTxtVal = $(".error-txt").val();
            var errorTxt = '<div class="error">' + errorTxtVal + '</div>';
            $(".errorP").html(errorTxt);
            $(".errorP").show();
            valid = false;
        }
        return valid;
    });
    $("input[id*='txtParticipants']").blur(function() {
        var ParticipantVal = $(this).val();
        if (isNaN(ParticipantVal)) {
            validateParticipant();
            valid = false;
        }
        else if (ParticipantVal.indexOf(".") != -1 || ParticipantVal.indexOf("-") != -1) {
            validateParticipant();
            valid = false;
        }
        else {
            $("input[id*='txtParticipants']").removeClass('error');
            $(".errorP1").hide();
        }
    });
    if (($(".MeetingSearchDesc").height()) < 158) {
        $(".MeetingImageHolder").css('height', '140px');
    }
    $("div[class*='meetingHotelWrapper']").find(".green_tick").addClass('green_tick-white');

});

function validateParticipant() {
    $("input[id*='txtParticipants']").addClass('error');
    var errorTxtVal = $(".error-txtp").val();
    var errorTxt = '<div class="error">' + errorTxtVal + '</div>';
    $(".errorP1").html(errorTxt);
    $(".errorP1").show();

}

// Meeting Search Result - Paragraph Text
$(function() {
    var showChar = 180;
    if ($.browser.msie) {
        showChar = 150;
    }
    $('.meetingHotelDesc').each(function() {
        var content = $(this).text();
        if (content.length > showChar) {
            var con = content.substr(0, showChar);
            var hcon = content.substr(showChar, content.length - 3 - showChar);
            var txt = con + '<span class="morecontent"><span>' + hcon + '</span><a href="" title="Read More" class="moretxt">...</a></span>';
            $(this).html(txt);
        }
    });
    $(".moretxt").click(function() {
        if ($(this).hasClass("exp")) {
            $(this).removeClass("exp");
        } else {
            $(this).addClass("exp");
        }
        $(this).prev().show();
        $(this).hide();
        return false;
    });
    var LoginBtn = $('.bookingContent').find('a[id$="btnLogin"]');
    $(LoginBtn).click(function() {
        $(LoginBtn).parents(".whitBoxRt").siblings(".colmLft").find('input[id$="txtUserName"]').addClass("w140");
        $(LoginBtn).parents(".whitBoxRt").siblings(".colmLft").find('.userPrefixMed').siblings('input[id$="txtUserName"]').addClass("w75");
        $(LoginBtn).parents(".whitBoxRtUsr").siblings(".colmLft").find('input[id$="txtUserName"]').addClass("w110");
    });

    $('.MeetingRoomDetails').each(function() {
        $('span[id$="largestMeetingRoom"]').parent('div').css('margin-top', '-2px');
        if ($.browser.webkit) {
            $('span[id$="largestMeetingRoom"]').parent('div').css('margin-top', '-4px');
        }
        else if ($.browser.msie && parseInt($.browser.version, 10) === 8) {
            $('span[id$="largestMeetingRoom"]').parent('div').css('margin-top', '-4px');
        }
    });
    var homeOffersMaxH = 0;
    $('#offersArea .smallPromoBoxLight').each(function() {
        homeOffersMaxH = Math.max(homeOffersMaxH, $(this).height());
        if (homeOffersMaxH == 0) {
            $('#offersArea .smallPromoBoxLight').height();
        }
        else {
            $('#offersArea .smallPromoBoxLight').height(homeOffersMaxH + 5);
        }
    });

    //For My profile
    var $myProfilePreferences = $(".RoomPref input[type='checkbox']");
    if ($myProfilePreferences.length > 0) {
        setPreferencePopupOnLoad($myProfilePreferences);
    }
    $(".RoomPref input[type='checkbox']").click(function() {
        var $this = $(this);
        $($this).siblings(".additional-info").slideToggle();
    });

    //For Booking Details
    var $bookingDetPreferences = $(".otherRequestList input[type='checkbox']");
    if ($bookingDetPreferences.length > 0) {
        setPreferencePopupOnLoad($bookingDetPreferences);
    }
    $(".otherRequestList input[type='checkbox']").click(function() {
        var $this = $(this);
        $($this).siblings(".additional-info").slideToggle();
    });

    function setPreferencePopupOnLoad(preferences) {
        var $checked = $(preferences).filter(':checked');
        $(".additional-info").hide();
        $($checked).each(function(index) {
            $(this).siblings(".additional-info").show()
        });
    }
    //For Campaign promo popup    
    $(".popuptopCntOverLay .popupOverLay .cnt a.blkClose").click(function() {
        $(this).parents(".popuptopCntOverLay").hide();
        return false;
    });
}); 
