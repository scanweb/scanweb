/* ------------------------------------------------------------------------------
DHTML X-Browser Utilities.
------------------------------------------------------------------------------ */
// -------- START: browser check 
var ns4	= (document.layers) ? 1 : 0; // Netscape 4
var op 	= (window.opera) ? 1 : 0; // Opera
var op5 = (navigator.userAgent.indexOf("Opera 5")!=-1) ||(navigator.userAgent.indexOf("Opera/5")!=-1);
var op6 = (navigator.userAgent.indexOf("Opera 6")!=-1) ||(navigator.userAgent.indexOf("Opera/6")!=-1);
var op7 = (navigator.userAgent.match(new RegExp("[\w\s ]*Opera.7[\w\s ]*"))) ? 1 : 0; // Opera 7
var op8 = (navigator.userAgent.match(new RegExp("[\w\s ]*Opera.8[\w\s ]*"))) ? 1 : 0; // Opera 8
var ie 	= (document.all && !op) ? 1 : 0; // MS Internet Explorer
var ie50 = (ie && !document.createEventObject) ? 1 : 0; // MS Internet Explorer 5.0
var moz = (document.getElementById && !ie) ? 1 : 0; // Mozilla/Gecko Browser
var saf = (navigator.userAgent.lastIndexOf("Safari") > 0);
var saf30 = (navigator.userAgent.lastIndexOf("Safari/523.15") > 0);
var osx = (navigator.userAgent.lastIndexOf("OS X") > 0);
var agt	=navigator.userAgent.toLowerCase();
var mac = (agt.indexOf("mac")!=-1);
var mac_ie = mac && ie;	
// Detect IE5.5+
var version=0;
if (navigator.appVersion.indexOf("MSIE")!=-1){
	temp=navigator.appVersion.split("MSIE");
	version=parseFloat(temp[1]);
}
var ie7 = (version == 7)? 1 : 0;
var ie6 = (version == 6)? 1 : 0;
var ie55 = (version == 5.5)? 1 : 0;
var isSafari3 = false;
if(window.devicePixelRatio) isSafari3 = true;
// -------- END: browser check

// Below code will fix the IE6 Background Iamge Flicker
// Refer: http://ajaxian.com/archives/no-more-ie6-background-flicker

if (ie6){
	try {
  		document.execCommand("BackgroundImageCache", false, true);
	} catch(err) {}
}


//  	Function which returns an object when passed the name (for IE) or ID of the object, as a string.
//  	Also can accept the specific window object to  use.  Thus we can find objects in frames or  spawned windows.
function getObject() {
	
	var obj;	//  Object to return
	var objRef;	//  String name of the object to find
	var winRef;	//  Window object which the object should be found
	var args;	//  Array of arguments passed to this function
	var doc;	//  Final document object

	args    = getObject.arguments;
	objRef  = args[0];
	winRef  = args[1];

	//  If an actual object was already passed here, just pass it back
	//  otherwise, find the real object.    
	if (typeof objRef == "object") {
		obj = objRef;
	} else {
		//  Assume that unless we get an actual window object
		//  passed, we'll use the current window.
		if (typeof winRef == "object") {
			doc = winRef.document;
		} else {
			doc = document;
		}

		if (document.all) {
			//  Internet Explorer 5.5 and below
			obj = eval("doc.all." + objRef);
		} else if (document.layers) {
			//  Netscape Navigator 4.7x and below
			obj = eval("doc.layers[\"" + objRef + "\"]");
		} else if (document.getElementById) {
			// Internet Explorer and Netscape Navigator 6
			obj = eval("doc.getElementById('" + objRef + "')");
		} else {
			//  Who knows?
			obj = null;
		}
	}
	return obj;
}
// 	 Function to set the size of any object (height and width)
function setObjectSize() {	
	var objRef;
	var winRef;
	var args;
	var w;
	var h;

	args    = setObjectSize.arguments;
	objRef  = args[0];
	w       = args[1];
	h       = args[2];
	winRef  = args[3];

	var theObj  = getObject(objRef, winRef);

	if (document.all) {
		with (theObj) {
			style.width     = w + "px";
			style.height    = h + "px";
		}
	} else if (document.layers) {
		with (theObj) {
			clip.width      = w;
			clip.height     = h;
		}
	} else if (document.getElementById) {
		with (theObj) {
			style.width     = w + "px";
			style.height    = h + "px";
		}
	} else {
	}
}

//  Function to locate an object on the page, from the top-left corner
//  of the browser.  Returns a two-element array containing x and y (in pixels)
//  Can find objects either by reference (a passed string) or as a direct object.
function getObjectCoords() {
	
	var objRef;
	var winRef;
	var args;
	var theObj;
	var x;
	var y;
	var arrayCoords = new Array(2);
	
	args    = getObjectCoords.arguments;
	objRef  = args[0];
	winRef  = args[1];

	var theObj  = getObject(objRef, winRef);

	var x = y = 0;
	if (theObj.offsetParent) {
		x = theObj.offsetLeft
		y = theObj.offsetTop
		while (theObj = theObj.offsetParent) {
			x += theObj.offsetLeft
			y += theObj.offsetTop
		}
	}
	arrayCoords[0]  = x;
	arrayCoords[1]  = y;

	return arrayCoords;
}
//  Function to locate an object on the 'Booking Engine' DIV, from the top-left corner
//  of the DIV class 'RightColumnShiftUp'.  Returns a two-element array containing x and y (in pixels)
//  Can find objects either by reference (a passed string) or as a direct object.
function getCalFieldCoords() {
	
	var objRef;
	var winRef;
	var args;
	var theObj;
	var x;
	var y;
	var arrayCoords = new Array(2);
	var objPos;
	
	args    = getCalFieldCoords.arguments;
	objRef  = args[0];
	winRef  = args[1];

	var theObj  = getObject(objRef, winRef);
	// GB: Fix for artf621297
	//Calendar popup needs to be right aligned to departure date field 
	if (theObj != 'null')
	{
		if (theObj.id == ArrivalDate || theObj.id == ArrivalDate1)
		{ 
			tmpLeft = 0;
		}
		else if (theObj.id == DepartureDate || theObj.id == DepartureDate1)
		{                       
			tmpLeft = -112;       
		}
	}
	var x = y = 0;
	if (theObj.offsetParent) {
		x = theObj.offsetLeft
		y = theObj.offsetTop
		while (theObj = theObj.offsetParent) {			
			objPos = getElementStyle(theObj,'position','position');			
			if (objPos == 'relative')
			{	//tmpLeft	= 0;
				tmpTop	= 19;
				break;				
			}
			// Artifact artf631902 : Booking page
			else if ((theObj.className == 'RightColumnShiftUp') || (theObj.id == 'BookingPageLeftAreaShiftUp'))
			{
				//tmpLeft	= 0;
				tmpTop	= 19;
				break;
			}
			else
			{
				//tmpLeft	= 0;
				tmpTop	= 20;
				x += theObj.offsetLeft;
				y += theObj.offsetTop;
			}
		}
	}
	arrayCoords[0]  = x;
	arrayCoords[1]  = y;

	return arrayCoords;
}
//  Function to locate an login control Div on the page, from the top-left corner
//  of the 'MainMenuArea' Div.  Returns a two-element array containing x and y (in pixels)
//  Can find objects either by reference (a passed string) or as a direct object.
function getLoginCoords() {
	
	var objRef;
	var winRef;
	var args;
	var theObj;
	var x;
	var y;
	var arrayCoords = new Array(2);
	
	args    = getLoginCoords.arguments;
	objRef  = args[0];
	winRef  = args[1];

	var theObj  = getObject(objRef, winRef);

	var x = y = 0;
	if (theObj.offsetParent) {
		x = theObj.offsetLeft
		y = theObj.offsetTop
		while (theObj = theObj.offsetParent) {			
			if (theObj.id == 'MainMenuArea')
			{
				loginLeft	= -201;
				loginTop	= -10;
				break;
			}
			else
			{
				loginLeft	= -201;
				loginTop	= -10;
				x += theObj.offsetLeft;
				y += theObj.offsetTop;
			}
		}
	}
	arrayCoords[0]  = x;
	arrayCoords[1]  = y;

	return arrayCoords;
}
// 	Function to set the (x,y) coordinates for the upper left-hand corner of a DIV
function setObjectCoords() {
	
	var objRef;
	var winRef;
	var args;
	var oX;
	var oY;

	args    = setObjectCoords.arguments;
	objRef  = args[0];
	oX      = args[1];
	oY      = args[2];
	winRef  = args[3];

	var theObj  = getObject(objRef, winRef);

	if (document.all && !document.getElementById) {
		with (theObj) {
			style.pixelLeft     = oX;
			style.pixelTop      = oY;
		}
	} else if (document.layers) {
		with (theObj) {
			left                = oX;
			top                 = oY;
		}
	} else if (document.getElementById) {
		with (theObj) {
			style.left          = oX + "px";
			style.top           = oY + "px";
		}
	} else {
	}
}
//	Reading Effective Style Sheet Property Values 
function getElementStyle(elemRef, IEStyleProp, CSSStyleProp) {
    var elem = getObject(elemRef);
    if (elem.currentStyle) {
        return elem.currentStyle[IEStyleProp];
    } else if (window.getComputedStyle) {
        var compStyle = window.getComputedStyle(elem, "");
        return compStyle.getPropertyValue(CSSStyleProp);
    }
    return "";
}

// 	generic search function for nodeTypes (eg. DIV) with specific CSS classes
//	 cName=className, nType=nodeType(e.g. nType="DIV"), mNode=mothernode to start the search
function getElementsByClassName(cName, nType, mNode) { 
	var nType = (nType==null) ? "DIV" : nType;
	var mNode = (mNode==null) ? window.document : mNode;
	var nodepts = mNode.getElementsByTagName(nType);
	var hits = new Array();
	for(var count=0; count<nodepts.length; count++) {
		if(nodepts[count].className.indexOf(cName,0)!= -1) {
			hits.push(nodepts[count]);
		}
	}
	return hits;
}

// Function to chnages the Class Names
function cssClassChanger(strAct,obj,strClass1,cstrClass2){
	if(strAct=="flip"){
		obj.className=!cssClassChanger('verify',obj,strClass1)?obj.className.replace(strClass2,strClass1):
		obj.className.replace(strClass1,strClass2);
	}
	else if(strAct=="add"){
		if(!cssClassChanger('verify',obj,strClass1)){obj.className+=obj.className?' '+strClass1:strClass1;}
	}
	else if(strAct=="remove"){
		var rep=obj.className.match(' '+strClass1)?' '+strClass1:strClass1;
		obj.className=obj.className.replace(rep,'');
	}
	else if(strAct=="verify"){
		return new RegExp('\\b'+strClass1+'\\b').test(obj.className);
	}
}

//The functions return the  height of an object.
//NOTE: This code will not work for images in Netscape 4.x. Use getImageWidth and getImageHeight instead.
function getElementHeight(Elem) {
	if (ns4) {
		var elem = getObjNN4(document, Elem);
		return elem.clip.height;
	} else {
		if(document.getElementById) {
			var elem = document.getElementById(Elem);
		} else if (document.all){
			var elem = document.all[Elem];
		}
		if (op5) { 
			xPos = elem.style.pixelHeight;
		} else {
			xPos = elem.offsetHeight;
		}
		return xPos;
	} 
}

//The functions return the  width  of an object.
function getElementWidth(Elem) {
	if (ns4) {
		var elem = getObjNN4(document, Elem);
		return elem.clip.width;
	} else {
		if(document.getElementById) {
			var elem = document.getElementById(Elem);
		} else if (document.all){
			var elem = document.all[Elem];
		}
		if (op5) {
			xPos = elem.style.pixelWidth;
		} else {
			xPos = elem.offsetWidth;
		}
		return xPos;
	}
}

function getObjNN4(obj,name)
{
	var x = obj.layers;
	var foundLayer;
	for (var i=0;i<x.length;i++)
	{
		if (x[i].id == name)
		 	foundLayer = x[i];
		else if (x[i].layers.length)
			var tmp = getObjNN4(x[i],name);
		if (tmp) foundLayer = tmp;
	}
	return foundLayer;
}


//The functions return the width  of an image.
//NOTE: All the browsers except Netscape 4.x just call getElementWidth(myImage) or getElementHeight(myImage) to get the width or height.
function getImageWidth(myImage) {
	var x, obj;
	if (document.layers) {
		var img = getImage(myImage);
		return img.width;
	} else {
		return getElementWidth(myImage);
	}
	return -1;
}

//The functions return the height of an image.
function getImageHeight(myImage) {
	var y, obj;
	if (document.layers) {
		var img = getImage(myImage);
		return img.height;
	} else {
		return getElementHeight(myImage);
	}
	return -1;
}

// add events to objects
function addEventToObject(obj, evt, func) {
                try{
                	var oldhandler = obj[evt];
                	obj[evt] = (typeof obj[evt] != 'function') ? func : function(){oldhandler();func();};
                 }catch(e){}
}

// This function is used to open a new window for glossary definitions. 
// USAGE: 
//    <a onclick="openPopupWin('popup.html','width=412,height=544,scrollbars'); return false" href="#">text here</a>
//
function openPopupWin(URL, windowFeatures) {
	newPopupWin = window.open(URL,'popupWin',windowFeatures);
}