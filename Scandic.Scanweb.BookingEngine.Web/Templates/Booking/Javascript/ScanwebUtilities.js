﻿// ***** PopUpPage ******

function OpenPopUpwindow(url,h,w) 
{ 
    window.open(url,'name','height='+h+',width='+w+',resizable=yes');
}

function OpenPopUpFloorPlan(url,text,h,w) 
{ 
    window.open(url,'name','height='+h+',width='+w+',resizable=yes');
}

 //******PopUpPage End ********
 
// ******** Plugins ********
  
  //This script detects the following:
//Flash
//Windows Media Player
//Java
//Shockwave
//RealPlayer
//QuickTime
//Acrobat Reader
//SVG Viewer

function GetString()
{
    var agt=navigator.userAgent.toLowerCase();
    var ie  = (agt.indexOf("msie") != -1);
    var ns  = (navigator.appName.indexOf("Netscape") != -1);
    var win = ((agt.indexOf("win")!=-1) || (agt.indexOf("32bit")!=-1));
    var mac = (agt.indexOf("mac")!=-1);

    if (ie && win) {	pluginlist = detectIE("Adobe.SVGCtl","SVG Viewer") + detectIE("SWCtl.SWCtl.1","Shockwave Director") + detectIE("ShockwaveFlash.ShockwaveFlash.1","Shockwave Flash") + detectIE("rmocx.RealPlayer G2 Control.1","RealPlayer") + detectIE("QuickTimeCheckObject.QuickTimeCheck.1","QuickTime") + detectIE("MediaPlayer.MediaPlayer.1","Windows Media Player") + detectIE("PDF.PdfCtrl.5","Acrobat Reader"); }
    if (ns || !win) {
		    nse = ""; for (var i=0;i<navigator.mimeTypes.length;i++) nse += navigator.mimeTypes[i].type.toLowerCase();
		    pluginlist = detectNS("image/svg-xml","SVG Viewer") + detectNS("application/x-director","Shockwave Director") + detectNS("application/x-shockwave-flash","Shockwave Flash") + detectNS("audio/x-pn-realaudio-plugin","RealPlayer") + detectNS("video/quicktime","QuickTime") + detectNS("application/x-mplayer2","Windows Media Player") + detectNS("application/pdf","Acrobat Reader");
    }

    function detectIE(ClassID,name) { result = false; document.write('<SCRIPT LANGUAGE=VBScript>\n on error resume next \n result = IsObject(CreateObject("' + ClassID + '"))</SCRIPT>\n'); if (result) return name+','; else return ''; }
    function detectNS(ClassID,name) { n = ""; if (nse.indexOf(ClassID) != -1) if (navigator.mimeTypes[ClassID].enabledPlugin != null) n = name+","; return n; }

    pluginlist += navigator.javaEnabled() ? "Java," : "";
    if (pluginlist.length > 0) pluginlist = pluginlist.substring(0,pluginlist.length-1);
    
    return pluginlist;
}

function BrowserHasFlash()
{
    var agt=navigator.userAgent.toLowerCase();
    var ie  = (agt.indexOf("msie") != -1);
    var ns  = (navigator.appName.indexOf("Netscape") != -1);
    var win = ((agt.indexOf("win")!=-1) || (agt.indexOf("32bit")!=-1));
    var mac = (agt.indexOf("mac")!=-1);

    if (ie && win) {	pluginlist = detectIE("ShockwaveFlash.ShockwaveFlash.1","Shockwave Flash"); }
    if (ns || !win) {
		    nse = ""; for (var i=0;i<navigator.mimeTypes.length;i++) nse += navigator.mimeTypes[i].type.toLowerCase();
		    pluginlist = detectNS("application/x-shockwave-flash","Shockwave Flash");
    }

    function detectIE(ClassID,name) { result = false; document.write('<SCRIPT LANGUAGE=VBScript>\n on error resume next \n result = IsObject(CreateObject("' + ClassID + '"))</SCRIPT>\n'); if (result) return name+','; else return ''; }
    function detectNS(ClassID,name) { n = ""; if (nse.indexOf(ClassID) != -1) if (navigator.mimeTypes[ClassID].enabledPlugin != null) n = name+","; return n; }
    
    if (pluginlist.length > 0) pluginlist = pluginlist.substring(0,pluginlist.length-1);
    
    if (pluginlist.indexOf("Flash")!=-1)
        return true;
    else
        return false;
}

// ******** Plugins End********

// ****** ExpandCollapse *******

/* 
  ------------------------------------
  Expand and Collapse functions 
  for content areas
  ------------------------------------
*/
var hide;
var hideMeeting;

function populate(id, flag, maxlang, minlang)		
    { 			
        var element = document.getElementById(id);
        var elementimg = document.getElementById(id + 'img');
        
        if (element)
        {
            if (flag && flag != null)
            {
                element.style.display = 'block';
                element.style.visibility = 'visible';
                elementimg.src = '/Templates/Scanweb/Styles/Default/Images/Icons/box_minus.gif';
                elementimg.alt = minlang;
                hide = false;
            }
            else
            {
                element.style.display = 'none';
                element.style.visibility = 'hidden';
                elementimg.src = '/Templates/Scanweb/Styles/Default/Images/Icons/box_plus.gif';
                elementimg.alt = maxlang;
                hide = true;
            }
         }
     }   
     
     
function ExpandCollapse(id, flag, maxlang, minlang)		
    { 			
        var element = document.getElementById(id);
        var elementimg = document.getElementById(id + 'img');
        
        if (element)
        {
            if (flag)
            {
                element.style.display = 'block';
                element.style.visibility = 'visible';
                elementimg.src = '/Templates/Scanweb/Styles/Default/Images/Icons/box_minus.gif';
                elementimg.alt = minlang;
                hide = false;
            }
            else
            {
                element.style.display = 'none';
                element.style.visibility = 'hidden';
                elementimg.src = '/Templates/Scanweb/Styles/Default/Images/Icons/box_plus.gif';
                elementimg.alt = maxlang;
                hide = true;
            }
         }
     }
     

function SwitchClassName(elementID, firstClassName, alternativeClassName)		
    { 		

        var element = document.getElementById(elementID);
   
        if (element)
        {
            if (element.className == firstClassName){
                element.className = alternativeClassName;
            }
            else{
                element.className = firstClassName;
            }
         }
     }

     
function populateMeeting(id, flag, maxlang, minlang)		
    { 			
        var elementimages = document.getElementById('HotelMeetingImagesToBeHidden' + id);
        var elementcontent = document.getElementById('HotelMeetingContentToBeHidden' + id);
        var elementimg = document.getElementById('HotelMeeting' + id + 'img');
        
        if (elementimages && elementcontent)
        {
            if (flag)
            {
                elementimages.style.display = 'block';
                elementimages.style.visibility = 'visible';
                elementcontent.style.display = 'block';
                elementcontent.style.visibility = 'visible';
                elementimg.src = '/Templates/Scanweb/Styles/Default/Images/Icons/box_minus.gif';
                elementimg.alt = minlang;
                hideMeeting = false;
            }
            else
            {
                elementimages.style.display = 'none';
                elementimages.style.visibility = 'hidden';
                elementcontent.style.display = 'none';
                elementcontent.style.visibility = 'hidden';
                elementimg.src = '/Templates/Scanweb/Styles/Default/Images/Icons/box_plus.gif';
                elementimg.alt = maxlang;
                hideMeeting = true;
            }
         }
     }

// ****** ExpandCollapse End*******

// ****** Newsletter ********
function validateForm()
{
    var emailRegEx = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;
    if (document.getElementById("email").value == "") { 
           document.getElementById("errormessage").value = "Email address is mandatory";           
           return false;
    }
    else if (!(emailRegEx.test(document.getElementById("email").value)))
    {
        document.getElementById("errormessage").value = "Email address is invalid";
        return false;
    }
    else if (document.getElementById("country").value == "none") { 
        document.getElementById("errormessage").value = "Country is mandatory";        
        return false;
    }
    else if (document.getElementById("zipcode").value == "") { 
        document.getElementById("errormessage").value = "Zip code is mandatory";
        return false;
    } 
    return true;
}

// ****** Newsletter End********

// ****** Hotel Overview Image Switch *******

function SetImageSwitchTrigger(linkElementID, imageElementID, onMouseOverImageSrc, onMouseOutImageSrc){
    var linkElement = document.getElementById(linkElementID);
    var imageElement = document.getElementById(imageElementID);

    if (linkElement != null && imageElement != null){
        linkElement.onmouseover = function(){ SetImageSrc(imageElementID, onMouseOverImageSrc); }
        linkElement.onmouseout = function(){ SetImageSrc(imageElementID, onMouseOutImageSrc); }
    }
}

function SetImageSrc(ímageID, src){
    document.getElementById(imageID).src = src;
}

// ***** Hotel Overview Image Switch End *****


// **** Alternating Rows Table ****
function RenderAllAlteratingRowsTable(){
    var tables = document.getElementsByTagName('table'); 
    for (var i = 0; i < tables.length; i++){
        if (tables[i].className == 'ScandicTable'){
            var rows = tables[i].getElementsByTagName("tr");
            for (var x = 0; x < rows.length; x++){
                if (x%2==1){
                    rows[x].className = 'AlternateTableRow';
                }
            }
        }
    }
}
// **** Alternating Rows Table End ***