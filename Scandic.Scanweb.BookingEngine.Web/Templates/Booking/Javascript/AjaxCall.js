﻿var moreResults = true;
var totalHotels;
var waitDuration = 62000;
var repeatCallDuration = 0;
var isSearchCriteriaValid = true;
var bookingIndex = 0;
var StandardPageTypeId = 45;
var HotelPageTypeId = 33;
var OverviewPageTypeId = 80;
var SearchCountryCityHotel = null;
var paginationEnabled = false;
var showSearchHotelWidget = "";
var removeDiscountTypeSort = "";
//var isSelectHotelTabGoogleMapReloaded = false;


function fetchHotels(firstTimeCall) {
    var currentTime = new Date().getTime();
    // destSearchStartTime variable is set as global variable in SelectHotes.ascx control
    var diff = (currentTime - destSearchStartTime) / 1000;
    if (diff >= destSearchTimeOut)
        return;
    var paramList = "MethodToCall=GetAvailableHotels";
    paramList += "&FirstTimeCall=" + encodeURI(firstTimeCall);
    var async = true;
    var method = "POST";
    getDataFromServer(requestUrl, callBackFetchHotels, async, method, paramList);
}
function updateIE6MessageStatus(firstTimeCall) {
    var paramList = "MethodToCall=updateIE6MessageStatus";
    paramList += "&FirstTimeCall=" + encodeURI(firstTimeCall);
    var async = true;
    var method = "POST";
    getDataFromServer(requestedUrl, callBackFetchHotels, async, method, paramList);
}

function CarouselProcessing(CurrentPageTypeID, CurrentPageLinkID) {


    var paramList = "methodToCall=CreateCarousel";
    paramList += "&CurrentPageTypeID=" + encodeURI(CurrentPageTypeID);
    paramList += "&CurrentPageLinkID= " + encodeURI(CurrentPageLinkID);
    var async = true;
    var method = "POST";
    getDataFromServer(requestedUrl, callBackParseJSON, async, method, paramList);

}


function callBackParseJSON() {
    var data = this.req.responseText;
    var jsonObj = $.parseJSON(data);
    parseJsonCollection(jsonObj);

}

function parseJsonCollection(jsonObj) {
    var slideDiv;
    if (jsonObj.ActivateImageCaraousel == true) {

        if (jsonObj.PageTypeId == StandardPageTypeId) {
            slideDiv = "#ctl00_" + jsonObj.PlaceHolder + "_slides";
            $(slideDiv).addClass(jsonObj.TopCss);
        }
        else if (jsonObj.PageTypeId == HotelPageTypeId) {

            if (parseInt(jsonObj.Alignment)) {
                $('<div class="carousalWithBox"><div id="slides" class="topMainImg"></div></div>').insertBefore('.topStoolImg');
                $('.topStoolImg').addClass('mrgLeft12');
                $('.topImgText').addClass('mrgLeft12');
                $('.topImgLink').addClass('mrgLeft12');
            }

            else {
                $('#hotelOverviewImageCarousel').addClass('carousalWithOutBox').html('<div id="slides"></div>');

            }
            slideDiv = "#slides";
        }
        else if (jsonObj.PageTypeId == OverviewPageTypeId) {
            if (jsonObj.PlaceHolder == 'TopAreaFullWidthRegion') {
                slideDiv = "#ctl00_" + jsonObj.PlaceHolder + "_slides";
                $(slideDiv).addClass(jsonObj.TopCss);
            }
            else {
                $("#OverviewPageTopAreaContent").html('<div id="slides"></div>');
                slideDiv = "#slides";
            }
        } else
            slideDiv = "#slides";
        $('#ImageDiv').css('display', 'none');
        $(slideDiv).append('<div class="slider"></div>');
        $('.slider').append('<div class="flexslider"></div>');
        $('.flexslider').append('<ul class="slides"></ul>');

        for (var i = 0; i < jsonObj.ImageList.length; i++) {
            $('.slides').append('<li id=aSlide' + i + '></li>');
            $('#aSlide' + i).append('<img id=imgSlide' + i + '></img>');
            $('#imgSlide' + i).attr({
                'src': jsonObj.ImageList[i].ImageUrl,
                'alt': jsonObj.ImageList[i].AltText,
                'title': jsonObj.ImageList[i].AltText,
                'width': jsonObj.Width,
                'height': jsonObj.Height

            });

        }
        GenerateCarousel(jsonObj);
    }
}


function GenerateCarousel(data) {
    $('.flexslider').flexslider({
        animation: data.TransitionEffect,
        // itemWidth:data.Width,
        slideshowSpeed: data.TimeOut,
        slideshow: data.AutoSlideRequired,
        pauseOnAction: false,
        pauseOnHover: true
    });
}



function ValueToTab() {
    $fn(_endsWith("sT")).value = 'Tab3';
}

function GetCurrentBookings(firstTimeCall, bookingIndex) {
    var paramList = "MethodToCall=GetCurrentBookings";
    paramList += "&FirstTimeCall=" + encodeURI(firstTimeCall) + "&BookingIndex=" + encodeURI(bookingIndex);
    var async = true;
    var method = "POST";
    getDataFromServer(requestedUrl, GetCurrentBookingsCallBack, async, method, paramList);
}
function GetCurrentBookingsCallBack() {
    var value = this.req.responseText;
    var currentBookings = null;
    var noOfBookings = getCookie('NoOfBookings');
    if (value.search("nomoreresults") == 1) {
        var finishSearchDiv = $fn(_endsWith("DivBooking"));
        finishSearchDiv.innerHTML = value;
        $('#viewMoreResults').hide();
    }
    else if (value.search("session") == 1) {
        value = value.replace("<session>", "");
        value = value.replace("<session>", "");
        window.location.href = value;
    }
    else if (value.search("error") == -1) {

        currentBookings = $.parseJSON(value);
    }
    else {
        var errorBookingDiv = $fn(_endsWith("DivBooking"));
        errorBookingDiv.innerHTML = value;
    }
    if (currentBookings != null && currentBookings.length > 0) {
        for (var i = 0; i < currentBookings.length; i++) {

            var cityName = currentBookings[i].HotelName;
            var hotelName = currentBookings[i].City;
            var cell1 = null;
            var enableViewLink = true;
            if (cityName != null && cityName != '' && hotelName != null && hotelName != '')
                cell1 = hotelName + ", " + cityName;
            else if (hotelName != null && hotelName != '')
                cell1 = hotelName;
            else if (cityName != null && cityName != '') {
                enableViewLink = false;
                cell1 = cityName;
            }
            else {
                enableViewLink = false;
                cell1.InnerText = ' ';
            }
            var fromDateObj = currentBookings[i].FromDate.DateString;
            var toDateObj = currentBookings[i].ToDate.DateString;

            if ($fn(siteLang) != null) {
                gSiteLanguage = $fn(siteLang).value;
            }
            var liElement = null;

            if (enableViewLink) {
                liElement = '<li class="reservationLists"><span class="hotelLink" onclick="GetCurrentBookingsUrl(' + currentBookings[i].ConfirmationId + ');ShowprogressDiv();">' + cell1 + '</span><span class="resDates">' + fromDateObj + " - " + toDateObj + '</span></li>';
            }
            else {
                liElement = '<li class="reservationLists"><span class="hotelLink">' + cell1 + '</span><span class="resDates">' + fromDateObj + " - " + toDateObj + '</span></li>';
            }

            $('#currentBkngsUl').append(liElement);
        }
        if ((noOfBookings - bookingIndex) > currentBookings.length) {
            $fn(_endsWith("viewMoreResults")).style.display = 'block';
        }
        else {
            $fn(_endsWith("viewMoreResults")).style.display = 'none';
        }
        bookingIndex = bookingIndex + currentBookings.length;
    }
    $('#BookingListProgressDiv').hide();
    //$('#currentBkngsUl li:odd').addClass('odd');
}
function ShowprogressDiv() {
    $('#BookingListProgressDiv').show();
}
function GetCurrentBookingsUrl(confirmationId) {
    var paramList = "MethodToCall=GetCurrentBookingsRedirectUrl";
    paramList += "&ConfirmationId=" + confirmationId;
    var async = true;
    var method = "POST";
    getDataFromServer(requestedUrl, GetCurrentBookingsUrlCallBack, async, method, paramList);
}
function GetCurrentBookingsUrlCallBack() {
    var value = this.req.responseText;
    var errorBookingDiv = $fn(_endsWith("DivBooking"));
    if (value.search("session") == 1) {
        value = value.replace("<session>", "");
        value = value.replace("<session>", "");
        window.location.href = value;
    }
    else if (value.search("error") == -1) {
        window.location.href = value;
    } else {
        errorBookingDiv.innerHTML = value;
    }
}

/*
function SetTab(divObj,tab)
//function SetTab(firstTimeCall)
{
var paramList = "MethodToCall=TabInSelectHotel";
paramList += "&SelectedDiv=" + divObj;
paramList += "&SelectedTab=" + tab;
var async = true;
var method = "POST";
//    getDataFromServer(requestedUrl, DummyMethod, async, method, paramList);
getDataFromServer(requestedUrl, DummyMethod, async, method, paramList);
}
*/
function stillHotelToFetch() {
    var paramList = "MethodToCall=TabInSelectHotel";
    paramList += "&stillHotelToFetch=" + "true";
    var async = true;
    var method = "POST";
    getDataFromServer(requestedUrl, DummyMethod, async, method, paramList);
    return fetchStatus;
}

//This is just to handle the response.
function DummyMethod() {
}

function stillHotelToFetchResult() {

    var value = this.req.responseText;
    var xDoc = loadXMLDoc(value);
    var xmlNodes = xDoc.getElementsByTagName("value");
    if (null != fetchStatus) {
        fetchStatus = xmlNodes;
    }
}

function callBackFetchHotels() {
    var value = this.req.responseText;
    var data;
    var xDoc = loadXMLDoc(value);

    var xmlNodes = xDoc.getElementsByTagName("hotels");
    var errorMessage;
    var operaElement = xDoc.getElementsByTagName("isOperaconnected");
    //Sateesh Chandolu artf1193886 : Scanweb - Hotel listing renders without rates
    //var operaConnected = xDoc.getElementsByTagName("isOperaconnected")[0].firstChild.nodeValue;
    if (xmlNodes.length == 0) {
        xmlNodes = xDoc.getElementsByTagName("error");

        //RK: Hide the tab if there are no hotels available and also, edit stay module shld be visible
        if ($fn(_endsWith("TabStructure")) != null)
            $fn(_endsWith("TabStructure")).style.display = "none";
        var totalHotel = 0;
        UpdateTotalHotelResultsInSearch(totalHotel);
        if ($('#yourStayMod05 div.regular div.modifyDetail') != null) {
            var chDet = $("#yourStayMod05 div.regular div.changedDetail");
            var moDet = $("#yourStayMod05 div.regular div.modifyDetail");
            var moBrd = $("#yourStayMod05 div.regular div.broadBrd");
            chDet.hide();
            moDet.show();
            moBrd.hide();
        }
        if (xmlNodes.length > 0) {
            if (xmlNodes[0].getAttribute("redirect") == "true") {
                pageurl = xmlNodes[0].getAttribute("pageurl");
                window.location.href = pageurl;
            }
            else {
                $("label[id$='errorLabel']").text(xmlNodes[0].firstChild.nodeValue);
                $fn(_endsWith("errorAlertDiv")).style.display = 'block';
                //                $fn(_endsWith("divNoAvailabilityMessage")).innerHTML = xmlNodes[0].firstChild.nodeValue;
                //                $fn(_endsWith("alertDiv")).style.display = 'block';


                if (null != $fn(_endsWith("subHeader"))) {
                    $fn(_endsWith("subHeader")).innerHTML = "";
                }
                if (null != $fn(_endsWith("allHotels"))) {
                    $fn(_endsWith("allHotels")).innerHTML = "";
                }
                if (null != $fn(_endsWith("footerContainer"))) {
                    $fn(_endsWith("footerContainer")).innerHTML = "";
                }
                hidePaginationSearch();
            }
        }
    }
    else {
        var operaConnected = false;
        operaConnected = operaElement[0].firstChild.nodeValue;
        //Sateesh Chandolu artf1193886 : Scanweb - Hotel listing renders without rates
        if (operaConnected == "True") {
            xmlNodes = xDoc.getElementsByTagName("hotel");

            // totalHotels = xDoc.getElementsByTagName("hotels")[0].getElementsByTagName('totalhotels')[0].firstChild.nodeValue;
            $(xDoc).find("hotels").each(function() {
                totalHotels = $(this).find("totalhotels").text();
                paginationEnabled = $(this).find("paginationEnabled").text();
                showSearchHotelWidget = $(this).find("showSearchHotelWidget").text();
                if (showSearchHotelWidget == "true") {
                    var chDet = $('#yourStayMod05 div.regular div.changedDetail');
                    var moDet = $('#yourStayMod05 div.regular div.modifyDetail');
                    var moBrd = $('#yourStayMod05 div.regular div.broadBrd');
                    chDet.hide();
                    moDet.show();
                    moBrd.hide();

                    var alertBox = $("div[id$='errorAlertDiv']");
                    if (alertBox.length) {
                        for (i = 0; i < 3; i++) {
                            $(alertBox).fadeTo('slow', 0.5).fadeTo('slow', 1.0);
                        }
                    }
                }
            });

            // Display the error message if returned in the hotel response in the top block of the page
            var errorTag = xDoc.getElementsByTagName("hotels")[0].getElementsByTagName('error')[0];
            if (errorTag) {
                var errorMessage;
                if ($.browser.msie) {
                    errorMessage = errorTag.xml;
                }
                else {
                    errorMessage = $(errorTag).html();
                }
                $("label[id$='errorLabel']").html(errorMessage);
                $fn(_endsWith("errorAlertDiv")).style.display = 'block';

                var isBonusChequeNotFound = xDoc.getElementsByTagName("hotels")[0].getElementsByTagName('isbonuschequenotfounderror')[0];
                if (isBonusChequeNotFound && isBonusChequeNotFound.firstChild.nodeValue == "TRUE") {
                    if (null != $fn(_endsWith("subHeader"))) {
                        $fn(_endsWith("subHeader")).style.display = 'none';
                    }
                    if (null != $fn(_endsWith("footerContainer"))) {
                        $fn(_endsWith("footerContainer")).style.display = 'none';
                    }
                    if (null != $fn(_endsWith("TabStructure"))) {
                        $fn(_endsWith("TabStructure")).style.display = 'none';
                    }
                }
                if (xDoc.getElementsByTagName("hotels")[0].getElementsByTagName('removeDiscountTypeSort').length > 0)
                    removeDiscountTypeSort = xDoc.getElementsByTagName("hotels")[0].getElementsByTagName('removeDiscountTypeSort')[0].firstChild.nodeValue;
                if (removeDiscountTypeSort == "true") {
                    $("#" + _endsWith('ddlSortHotel') + " option[value='DISCOUNTTYPE']").remove();
                }
            }

            //Commented due to (359402 | Map | Search List and Google map are not displayed in the search result page.)
            //This is no more required
            /*
            // R1.4 Marketting City
            // if alCityHotelTag is true then hide the divAvailability of ReservationInformationContainer  
            var altCityHotelTag = xDoc.getElementsByTagName("hotels")[0].getElementsByTagName('altCityHotelsfound')[0]
            if (altCityHotelTag)
            {
            var errorMessage = errorTag.firstChild.nodeValue;
            $fn(_endsWith("divAvailability")).innerHTML = "";
            }
            */

            var hotelCount = xmlNodes.length;
            if (hotelCount > 0) {
                hotelParent = $fn(_endsWith('allHotels'));
                if (hotelParent != null) {
                    hotelParent.innerHTML = '';
                }
                if (paginationEnabled == "true") {
                    for (ctr = 0; ctr < hotelCount && ctr < pageSize; ctr++) {
                        if (xmlNodes[ctr].childNodes.length > 0) {
                            if (xmlNodes[ctr].getElementsByTagName("displayed")[0].firstChild.nodeValue == "0") {
                                hotel = GetHotel(xmlNodes[ctr]);
                                /* Uncomment Raj*/
                                AddHotel(hotel, hotelParent);
                            }
                        }
                    }
                }
                else {
                    for (ctr = 0; ctr < hotelCount; ctr++) {
                        if (xmlNodes[ctr].childNodes.length > 0) {
                            if (xmlNodes[ctr].getElementsByTagName("displayed")[0].firstChild.nodeValue == "0") {
                                hotel = GetHotel(xmlNodes[ctr]);
                                /* Uncomment Raj*/
                                AddHotel(hotel, hotelParent);
                            }
                        }
                    }
                }

                if (ctr == pageSize) {
                    showPaginationSearch();
                }
            }

            if (xDoc.getElementsByTagName("hotels")[0].getElementsByTagName('morehotelstofetch')[0].firstChild.nodeValue == "true") {
                setTimeout('fetchHotels(false)', 500);
            }
            else {
                data = "<div class=\"clear\">&nbsp;</div>";
                addData(data);
                SetPageStrs();
                SetPagination();
                hidePaginationSearch();
                //RefreshGoogleMapIframe(); // Using Iframe
                UpdateTotalHotelResultsInSearch(totalHotels);
                //	        RefreshGoogleMapTimer(); // Using Updatepanel
            }
        }
        //Sateesh Chandolu artf1193886 : Scanweb - Hotel listing renders without rates
        else {
            var tst = $fn(_endsWith("TabStructure"));
            tst.style.display = "none";

            //$fn(_endsWith("errorLabel")).innerHTML = $fn(_endsWith("operaErr")).value;

            //            $fn(_endsWith("divNoAvailabilityMessage")).innerHTML = $fn(_endsWith("operaErr")).value;
            //            $fn(_endsWith("alertDiv")).style.display = 'block';
            $("label[id$='errorLabel']").text($fn(_endsWith("operaErr")).value);
            $fn(_endsWith("errorAlertDiv")).style.display = 'block';
        }
    }
}

function GetHotel(hotelXML) {

    hotel = new HotelDetails();
    hotel.ID = getStringFromNode(hotelXML.getElementsByTagName("code")[0]);
    hotel.imageSrc = getStringFromNode(hotelXML.getElementsByTagName("image")[0]);
    if (!hotel.imageSrc)
        hotel.imageSrc = "/Templates/Booking/Images/noimage.gif";
    hotel.imageAlt = getStringFromNode(hotelXML.getElementsByTagName("name")[0]);
    hotel.hotelUrl = getStringFromNode(hotelXML.getElementsByTagName("url")[0]);
    hotel.hotelName = getStringFromNode(hotelXML.getElementsByTagName("name")[0]);
    hotel.rateTitle = getStringFromNode(hotelXML.getElementsByTagName("rateTitle")[0]);
    hotel.noDiscount = getStringFromNode(hotelXML.getElementsByTagName("noDiscount")[0]);
    hotel.price = getStringFromNode(hotelXML.getElementsByTagName("rate")[0]);
    hotel.alternatePrice = getStringFromNode(hotelXML.getElementsByTagName("alternatePrice")[0]);
    hotel.altPriceTooltipLine1 = getStringFromNode(hotelXML.getElementsByTagName("altPriceTooltipLine1")[0]);
    hotel.altPriceTooltipLine2 = getStringFromNode(hotelXML.getElementsByTagName("altPriceTooltipLine2")[0]);
    hotel.perNight = getStringFromNode(hotelXML.getElementsByTagName("prpn")[0]);
    //Merchandising - Special Alerts in Booking Flow
    hotel.splAlert = getStringFromNode(hotelXML.getElementsByTagName("specialalert")[0]);
    hotel.hideARBPrice = getStringFromNode(hotelXML.getElementsByTagName("hidearbprice")[0]);
    hotel.selectHotel = getStringFromNode(hotelXML.getElementsByTagName("sH")[0]);
    hotel.address = getStringFromNode(hotelXML.getElementsByTagName("address")[0]);
    hotel.teaser = getStringFromNode(hotelXML.getElementsByTagName("description")[0]);
    hotel.distance = getStringFromNode(hotelXML.getElementsByTagName("distance")[0]);
    hotel.direction = getStringFromNode(hotelXML.getElementsByTagName("direction")[0]);
    hotel.drivingTime = getStringFromNode(hotelXML.getElementsByTagName("drivingTime")[0]);
    // R1.4 Marketting city
    hotel.distanceText = getStringFromNode(hotelXML.getElementsByTagName("distanceText")[0]);
    hotel.directionText = getStringFromNode(hotelXML.getElementsByTagName("directionText")[0]);
    //R2.0 | Iteration 2
    hotel.drivingTimeText = getStringFromNode(hotelXML.getElementsByTagName("drivingTimeText")[0]);
    hotel.fullDescriptionText = getStringFromNode(hotelXML.getElementsByTagName("fulldescription")[0]);
    hotel.imageGalleryText = getStringFromNode(hotelXML.getElementsByTagName("imagegallery")[0]);
    hotel.fromText = getStringFromNode(hotelXML.getElementsByTagName("fromText")[0]);
    //Defect Id-430452 (Change the font of Bonus Cheque rate displayed in Select Hotel page.) -parvathi 
    hotel.HotelSearch = getStringFromNode(hotelXML.getElementsByTagName("HotelSearch")[0]);
    //RK: R2.0 - CR - Unblock Rooms - this property says whether rooms have to be unblocked.
    hotel.IsUnblockRequired = getStringFromNode(hotelXML.getElementsByTagName("IsUnblockRequired")[0]);
    if (hotel.IsUnblockRequired == "False") {
        hotel.IsUnblockRequired = "false";
    }
    else {
        hotel.IsUnblockRequired = "true";
    }
    hotel.CityCenterDistance = getStringFromNode(hotelXML.getElementsByTagName("citycenterdistance")[0]);
    //RK: EndOfUnblock Rooms
    hotel.TripAdvisorWidget = getStringFromNode(hotelXML.getElementsByTagName("tripAdvisorWidget")[0]);

    return hotel;
}

function HotelDetails() {
    this.ID = null;
    this.imageSrc = null;
    this.imageAlt = null;
    this.hotelUrl = null;
    this.hotelName = null;
    this.rateTitle = null;
    this.noDiscount = null;
    this.price = null;
    this.alternatePrice = null;
    this.altPriceTooltipLine1 = null;
    this.altPriceTooltipLine2 = null;
    this.perNight = null;
    this.splAlert = null;
    this.hideARBPrice = false;
    this.selectHotel = null;
    this.address = null;
    this.teaser = null;
    this.distance = null;
    this.direction = null;
    this.drivingTime = null;
    // R1.4 Marketting City
    this.distanceText = null;
    this.directionText = null;
    this.drivingTimeText = null;
    this.fullDescriptionText = null;
    this.imageGalleryText = null;
    this.fromText = null;
    //Defect Id-430452 (Change the font of Bonus Cheque rate displayed in Select Hotel page.) -parvathi 
    this.HotelSearch = null;
    this.IsUnblockRequired = false;
    this.CityCenterDistance = null;
    this.TripAdvisorWidget = null;
}

function AddHotel(hotel, parent) {
    hotelDetailBody = $.DIV({ className: 'hotelInfoCnt' });
    imgHolder = $.DIV({ className: 'imageHolder' });
    sprite = $.DIV({ className: 'hd sprite' });
    $(imgHolder).append(sprite);
    cnt = $.DIV({ className: 'cnt' });
    image = $.IMG({ 'src': hotel.imageSrc, 'alt': hotel.imageAlt, 'width': '226', 'height': '148' });
    $(cnt).append(image);
    $(imgHolder).append(cnt);
    ftsprite = $.DIV({ className: 'ft sprite' });
    $(imgHolder).append(ftsprite);

    $(hotelDetailBody).append(imgHolder);
    //------
    desc = $.DIV({ className: 'desc' });
    hotelName = $.H2({}, hotel.hotelName);
    $(desc).append(hotelName);
    address = $.P({}, $.SPAN({}, $.A({ href: hotel.hotelUrl }, hotel.address)));
    $(desc).append(address);
    teaser = $.P({}, hotel.teaser);
    $(desc).append(teaser);
    ULelement = $.UL({ className: 'links' });
    //Release R2.0 - Bhavya to Fix navigation to hotel landing page not happening error.
    Li1 = $.LI({}, $.A({ className: 'goto scansprite', href: hotel.hotelUrl }, hotel.fullDescriptionText));
    $(ULelement).append(Li1);
    //    scandic.windowpopup();
    //Image Gallery Implementation
    //Li2			= $.LI({},$.A({title:'Image gallery',className:'overlay jqModal spriteIcon',href:requestUrl+"?methodToCall=GetImageGallery&HotelID="+hotel.ID},hotel.imageGalleryText));
    Li2 = $.LI({}, $.A({ href: '#', className: 'overlay jqModal scansprite', rel: requestUrl + "?methodToCall=GetImageGallery&HotelID=" + hotel.ID + "&HotelName=" + hotel.hotelName }, hotel.imageGalleryText));
    // Li2			= $.LI({},$.A({title:'Image gallery',className:'overlay jqModal spriteIcon',href:'javascript:GetImageData('+ x + ');'},hotel.imageGalleryText));
    $(ULelement).append(Li2);
    $(desc).append(ULelement);
    $(hotelDetailBody).append(desc);
    ratesholder = $.DIV({ className: 'ratesHolder' });
    rates = $.DIV({ className: 'rates' });

    //artf1167579 : Corporate Identifier not present in Select Hotel listing :Rajneesh
    if (hotel.rateTitle) {
        ratesHolderContainer = $.DIV({ className: 'ratesHolderContainer ratesDNumber' });
    }
    else {
        ratesHolderContainer = $.DIV({ className: 'ratesHolderContainer' });
    }
    ratesHolderTop = $.DIV({ className: 'ratesHolderTop' });
    ratesHolderBottom = $.DIV({ className: 'ratesHolderBottom' });
    para4 = $.P({ className: 'rateText cityCenterDist' }, hotel.CityCenterDistance);
    $(ratesHolderContainer).append(para4);
    $(ratesHolderContainer).append(ratesHolderTop);


    //artf1167579 : Corporate Identifier not present in Select Hotel listing-Rajneesh
    if (hotel.rateTitle) {
        var str = hotel.rateTitle;
        //Merchandising:R3:Display ordinary rates for unavailable promo rates
        if (hotel.HotelSearch == 'REGULAR') {
            str = hotel.rateTitle;
        }
        else if (str.length >= 14) {
            str = str;  //(str.substring(0, 14)) + "...";
        }
        parrateTitle = $.DIV({}, $.DIV({ className: 'ratetitleclass' }, str));
        $(rates).append(parrateTitle);
    }
    else {
        var str = hotel.noDiscount;
        nodiscountMsg = $.DIV({}, $.DIV({ className: 'ratetitleclass' }, str));
        $(rates).append(nodiscountMsg);
    }

    if (hotel.fromText) {
        para1 = $.P({}, $.SPAN({}, hotel.fromText));
        $(rates).append(para1);
    }

    //Defect Id-430452 (Change the font of Bonus Cheque rate displayed in Select Hotel page.) -parvathi 
    if (hotel.HotelSearch == 'BONUSCHEQUE' || hotel.HotelSearch == 'REDEMPTION')
        para2 = $.P({ className: 'ratesFont' }, $.STRONG({}, hotel.price));
    else {
        para2 = $.P({ className: 'rates' }, $.STRONG({}, hotel.price));
    }
    if (hotel.alternatePrice) {
        var altTooltipTitle = '<b>' + hotel.altPriceTooltipLine1 + '</b><br/>' + hotel.altPriceTooltipLine2;
        altrate = $.SPAN({ className: 'alternateCurrencySelectHotel', title: altTooltipTitle }, hotel.alternatePrice);
    }

    $(rates).append(para2);
    if (typeof (altrate) != 'undefined') {
        $(rates).append(altrate);
    }
    hr1 = $.HR({ className: 'HR' });
    $(rates).append(hr1);
    para3 = $.P({ className: 'rateText' }, hotel.perNight);

    $(rates).append(para3);
    //$(rates).append(para4);
    $(ratesholder).append(rates);

    //Rajneesh
    $(ratesHolderContainer).append(ratesholder);
    $(ratesHolderContainer).append(ratesHolderBottom);
    $(hotelDetailBody).append(ratesHolderContainer);

    //Trip Advisor
    tripadvisorWrapper = $.DIV({ className: 'tripadvisorWrapper' });
    tripAdvisor = $.DIV({ id: 'divTripAdvisorRating', className: 'divTripAdvisorRating' });
    $(tripAdvisor).append(hotel.TripAdvisorWidget);
    $(tripadvisorWrapper).append(tripAdvisor);

    roomsAndrates = $.DIV({ className: 'roomsAndrates' });
    wrapper = $.DIV({ className: 'actionBtnContainer' });
    acnBtn = $.DIV({ className: 'actionBtn' });
    a1 = $.A({ className: 'buttonInner', href: 'javascript:UpdateSHListSelection();RedirectToSelectRate(' + hotel.ID + ',' + '' + hotel.IsUnblockRequired + '' + ');' }, hotel.selectHotel);
    $(acnBtn).append(a1);
    a2 = $.A({ className: 'buttonRt scansprite noText', href: 'javascript:UpdateSHListSelection();RedirectToSelectRate(' + hotel.ID + ',' + '' + hotel.IsUnblockRequired + '' + ');' }, hotel.selectHotel);
    $(acnBtn).append(a2);
    $(wrapper).append(acnBtn);
    $(roomsAndrates).append(wrapper);
    $(tripadvisorWrapper).append(roomsAndrates);

    $(hotelDetailBody).append(tripadvisorWrapper);

    if (hotel.splAlert) {
        splalertwrap = $.DIV({ className: 'selecthotelwrpalert' });

        splalertcontent = $.DIV({ className: 'splalertcontentratepage' });
        //splAlert = $.P({}, hotel.splAlert);
        splalertcontent.innerHTML = hotel.splAlert;
        // $(splalertcontent).append(splAlert);
        $(splalertwrap).append(splalertcontent);
        $(hotelDetailBody).append(splalertwrap);
    }

    //$(hotelDetailBody).append(ratesholder);
    $(parent).append(hotelDetailBody);

}

function addData(data) {
    $fn("hotelDetailContainer").innerHTML = $fn("hotelDetailContainer").innerHTML + data;
}

var totalPages;
var pageDetails = new Array();
var headerDtMsg1;
var val1;
var availabity1;
var valHdnAlert;
function SetPageStrs() {


    //parvathi- defect id 430682 Appropriate search result text is not displayed in select hotel page.
    headerDtMsg1 = $fn(_endsWith("headerDtMsg"));
    val1 = $fn(_endsWith("txtAvailabilityMsg1"));
    headerDtMsg1.innerHTML = "<strong>" + totalHotels + " " + val1.value + "</>";
    availabity1 = $fn(_endsWith("txtAvailabilityMsg2"));
    availabity1.value = headerDtMsg1.innerHTML
    if (totalHotels > pageSize)
        totalPages = Math.ceil(totalHotels / pageSize)
    else
        totalPages = 1;

    var startVal = 1;
    for (i = 0; i < totalPages; i++) {
        var endVal = startVal + pageSize - 1;
        if (endVal > totalHotels)
            endVal = totalHotels;
        if (startVal == endVal)
            pageDetails[i] = startVal;
        else
            pageDetails[i] = startVal + "-" + endVal;
        startVal = endVal + 1;
    }
    //artf1153012 : Bonus Cheque booking is not possible for 99 nights 
    valHdnAlert = $fn(_endsWith("infoAlert"));
    valHdnAlert.style.display = "block";

    return pageDetails;
}

var currentPage;
var paginationDiv;
var headerDtMsg2;
var val2;
var availabity2;
var val1HdnAlert;
function SetPagination() {
    //artf1153012 : Bonus Cheque booking is not possible for 99 nights 
    val1HdnAlert = $fn(_endsWith("infoAlert"));
    val1HdnAlert.style.display = "block";

    //parvathi- defect id 430682 Appropriate search result text is not displayed in select hotel page.
    headerDtMsg2 = $fn(_endsWith("headerDtMsg"));
    val2 = $fn(_endsWith("txtAvailabilityMsg1"));
    headerDtMsg2.innerHTML = "<strong>" + totalHotels + " " + val2.value + "</>";
    availabity2 = $fn(_endsWith("txtAvailabilityMsg2"));
    availabity2.value = headerDtMsg2.innerHTML

    if (totalPages == 1) {
        var hotelFooter = $fn("hotelFooterContainer");
        if (hotelFooter) {
            hotelFooter.innerHTML = "";
        }
    }
    else {
        currentPage = $fn(_endsWith("txtPageNo")).value;
        paginationDiv = $fn("pageListing");
        if (paginationDiv != null) {
            var pD = "";

            pD = pD + "<span class=\"prevLink\">";
            if (currentPage != 1) {
                pD = pD + "<a href=\"javascript:SelectPagination(1);\">" + prv + "</a>";
            }
            else {
                pD = pD + prv;
            }
            pD = pD + "</span>";

            for (i = 0; i < pageDetails.length; i++) {
                pD = pD + " | ";
                pD = pD + "<span class=\"pageItem\">";
                if (currentPage != i + 1) {
                    pD = pD + "<a href=\"javascript:SelectPagination(" + (i + 1) + ");\">" + pageDetails[i] + "</a>";
                }
                else {
                    pD = pD + "<span class=\"pageItemSelected\">" + pageDetails[i] + "</span>"
                }
                pD = pD + "</span>"
            }

            pD = pD + " | ";
            pD = pD + "<span class=\"nextLink\">";
            if (currentPage != totalPages) {
                pD = pD + "<a href=\"javascript:SelectPagination(" + ((currentPage * 1) + 1) + ");\">" + nxt + "</a>";
            }
            else {
                pD = pD + nxt;
            }
            pD = pD + "</span>";

            paginationDiv.innerHTML = pD;
        }
    }
}

function SelectPagination(pn) {
    $fn(_endsWith("txtPageNo")).value = pn;
    var SelectPagination = $fn(_endsWith('SelectPagination'));
    eval(SelectPagination.href);

}

function RefreshGoogleMapIframe() {
    //Remove the load indicator in the select a hotel page.			
    RemoveMyOverlay();


    var googleMapIframe = document.getElementById('SelectHotelGoogleMapIframe');
    // As the reloading works only if the loading succeeded !!!!! Assign newly.
    //googleMapIframe.contentWindow.location.reload(true);
    googleMapIframe.src = "/Templates/Scanweb/Pages/Booking/BookingGoogleMapPage.aspx";

    //isSelectHotelTabGoogleMapReloaded = true;

    var rdoPerNight = $fn(_endsWith('rdoPerNightMap'));
    rdoPerNight.disabled = false;
    var rdoPerStay = $fn(_endsWith('rdoPerStayMap'));
    rdoPerStay.disabled = false;

    // document.getElementById('SelectHotelGoogleMapIframe').contentDocument.location.reload(true);
    // var f = document.getElementById('iframe1');
    // f.src = f.src;
}

function RefreshGoogleMapTimer() {
    if (typeof RefreshGoogleMap == 'function') {
        RefreshGoogleMap();
    }
    else {
        setTimeout('RefreshGoogleMapTimer();', 100);
    }
}

function RedirectToSelectRate(hotelCode, isUnblockRequired) {

    var requestUrl = parent.location.href;
    if (requestUrl.indexOf("?") == -1) {
        requestUrl = requestUrl + "?SelectedHotelId=" + hotelCode;
    }
    else {
        requestUrl = requestUrl + "&SelectedHotelId=" + hotelCode;
    }
    requestUrl = requestUrl.replace("#", "");
    parent.location.href = requestUrl;
    /*var TxtHotelId = $fn(_endsWith('txtHotelId'));    
    TxtHotelId.value = hotelCode;
    var SelectOneHotel = $fn(_endsWith('SelectOneHotel'));
    eval($(SelectOneHotel).attr("href"));    
    }*/

    //RK: R2.0 - CR - Unblock Rooms - unblocks the rooms if blocked rooms are no longer required.
    if (!isUnblockRequired) {
        ClearBlockedRooms();
    }
    //RK: End Of Unblock Rooms 

}
//RK: R2.0 - CR - Unblock Rooms - to release the blocked rooms.
function ClearBlockedRooms() {
    var paramList = "MethodToCall=ClearBlockedRooms";
    var async = true;
    var method = "POST";
    getDataFromServer(requestUrl, DummyMethod, async, method, paramList);
}
//RK: End Of Unblock Rooms 

function RedirectToBookDetail(roomCategoryID, rateCategory, roomNumber) {
    //bug id: 462312 : called appropriate method to display overlay while transitionaing to the next room tab
    //defect id: artf1150702 added delay switch
    selectRatemoveRoom(roomNumber, false, false);
    $fn(_endsWith("txtRoomCategoryID")).value = roomCategoryID;
    $fn(_endsWith("txtRateCategory")).value = rateCategory;
    $fn(_endsWith("txtRoomNumber")).value = roomNumber;
    $fn(_endsWith("txtPerStay")).value = "";
    bordfy(roomNumber, 1);
    //R2.0 Bug id:441792-Ashish Following Commented
    // theForm.submit();
    //End R2.0 Bug id:441792-Ashish

}
//Bhavya - this is not required as the value selection of perstay/pernight should not impact the rate displayed for individual rooms in confirmation page.
//R2.0 -Parvathi:artf1149148: Price per night/stay
function setPerNightSelectedOnSelectRatePage(value) {
    var val = $fn(_endsWith("txtPerStay"));
    if (val != null) {
        $fn(_endsWith("txtPerStay")).value = value;
    }
    // alert($fn(_endsWith("txtPerStay")).value);
}
function FindDestinationByName(InputDestination, flag) {
    var destArray;
    var matchingCityDestinations;
    var totalCities;
    //If non bookable hotels need to be included in type ahead dropDown then following will execute.
    if (flag) {
        matchingCityDestinations = getMatchingDestinations(nonBookableHotelList, InputDestination, true);
    }
    else {
        //This will execute in normal flow.
        matchingCityDestinations = getMatchingDestinations(masterCityList, InputDestination, true);
    }
    if (matchingCityDestinations != null) {
        totalCities = matchingCityDestinations.length;
        destArray = new Array(totalCities);

        for (ctr = 0; ctr < totalCities; ctr++) {
            destArray[ctr] = new Array(2);

            destArray[ctr][0] = matchingCityDestinations[ctr].ID + "#$:~" + matchingCityDestinations[ctr].name;
            if (matchingCityDestinations[ctr].hotels != null) {
                destArray[ctr][1] = new Array(matchingCityDestinations[ctr].hotels.length);
                for (ctrHotel = 0; ctrHotel < matchingCityDestinations[ctr].hotels.length; ctrHotel++) {
                    destArray[ctr][1][ctrHotel] = matchingCityDestinations[ctr].hotels[ctrHotel].ID + "#$:~" + matchingCityDestinations[ctr].hotels[ctrHotel].name;
                }
            }
        }
    }
    return destArray;
}
function FindDestinationByCountryName(InputDestination, flag) {
    var destArray;
    var matchingCityDestinations;
    var totalCountries;
    //If non bookable hotels need to be included in type ahead dropDown then following will execute.
    if (flag) {
        matchingCityDestinations = getMatchingDestinations(nonBookableHotelList, InputDestination, true);
    }
    else {
        //This will execute in normal flow.
        matchingCityDestinations = getMatchingDestinationsforCountries(masterCountryList, InputDestination, true);
    }
    if (matchingCityDestinations != null) {
        totalCountries = matchingCityDestinations.length;
        destArray = new Array(totalCountries);

        for (ctr = 0; ctr < totalCountries; ctr++) {
            destArray[ctr] = new Array(2);

            destArray[ctr][0] = matchingCityDestinations[ctr].countryCode + "#$:~" + matchingCityDestinations[ctr].name;

            if (matchingCityDestinations[ctr].cities != null) {

                destArray[ctr][1] = new Array(matchingCityDestinations[ctr].cities.length);

                for (ctrCity = 0; ctrCity < matchingCityDestinations[ctr].cities.length; ctrCity++) {
                    destArray[ctr][1][ctrCity] = new Array(2);

                    destArray[ctr][1][ctrCity][0] = matchingCityDestinations[ctr].cities[ctrCity].ID + "#$:~" + matchingCityDestinations[ctr].cities[ctrCity].name;

                    if (matchingCityDestinations[ctr].cities[ctrCity].hotels != null) {
                        destArray[ctr][1][ctrCity][1] = new Array(matchingCityDestinations[ctr].cities[ctrCity].hotels.length);
                        for (ctrHotel = 0; ctrHotel < matchingCityDestinations[ctr].cities[ctrCity].hotels.length; ctrHotel++) {
                            destArray[ctr][1][ctrCity][1][ctrHotel] = matchingCityDestinations[ctr].cities[ctrCity].hotels[ctrHotel].ID + "#$:~" + matchingCityDestinations[ctr].cities[ctrCity].hotels[ctrHotel].name;
                        }
                    }
                }
            }
        }
    }
    return destArray;
}

// Please do not delete this block for now. -Himansu
//***********************************************************************************************************
//var destArray;
//function FindDestinationByName(InputDestination)
//{
//    //destArray = new Array();
//    var paramList = "MethodToCall=FindDestinationByName";
//    paramList +=  "&InputDestination=" + encodeURI(InputDestination);
//    var async = false;
//    var method = "POST";

//    getDataFromServer(requestUrl, callBackFindDestinationByName, async, method, paramList);
//    return destArray;
//}

//function callBackFindDestinationByName()
//{
//    var value = this.req.responseText;    

//    var xDoc = loadXMLDoc(value);
//    var xmlNodes = xDoc.getElementsByTagName("error");
//    var errorMessage;

//    if(xmlNodes.length > 0)
//    {
//        /*if (xmlNodes[0].getAttribute("redirect") == "true")
//            window.location.href = "Error.aspx?ErrMsg=" + xmlNodes[0].firstChild.nodeValue;
//        else        */
//            $fn(_endsWith(errorDivID)).innerHTML = xmlNodes[0].text;
//    }
//    else
//    {
//        xmlNodes = xDoc.getElementsByTagName("destination");
//        destArray = new Array(xmlNodes.length);
//        
//        for(ctr=0; ctr < xmlNodes.length; ctr++)
//	    {	        
//	        destArray[ctr] = new Array(2);
//	        var xmlCityNode = xmlNodes[ctr].getElementsByTagName("city");
//	        destArray[ctr][0] = xmlCityNode[0].getAttribute("code") + ":" + getStringFromNode(xmlCityNode[0]);
//	        
//	        var xmlHotelNode = xmlNodes[ctr].getElementsByTagName("hotel");
//	        destArray[ctr][1] = new Array(xmlHotelNode.length);
//	        for(ctrHotel=0; ctrHotel < xmlHotelNode.length; ctrHotel++)
//	        {	            
//	            destArray[ctr][1][ctrHotel] = xmlHotelNode[ctrHotel].getAttribute("code") + ":" + getStringFromNode(xmlHotelNode[ctrHotel]);
//	        }
//	    }
//	}
//	return destArray;
//}
//***********************************************************************************************************


function SetHideUnHide() {
    var sA = $fn(_endsWith("txtShowAll"));
    if (sA.value == "TRUE") sA.value = "FALSE";
    else sA.value = "TRUE";
    //artf1045294 -Start
    if (null != $fn(_endsWith("txtRoomCategoryID"))) {
        $fn(_endsWith("txtRoomCategoryID")).value = "";
    }
    if (null != $fn(_endsWith("txtRateCategory"))) {
        $fn(_endsWith("txtRateCategory")).value = "";
    }
    //artf1045294 -End
}

function SetHideUnHideListingControl() {
    var sA = $fn(_endsWith("txtShowAll"));
    if (sA.value == "TRUE") sA.value = "FALSE";
    else sA.value = "TRUE";
}

function SetViewPricePerStay() {
    var perStay = $fn(_endsWith("txtPerStay"));
    if (perStay.value == "TRUE") perStay.value = "FALSE";
    else perStay.value = "TRUE";

    $fn(_endsWith("txtRoomCategoryID")).value = "";
    $fn(_endsWith("txtRateCategory")).value = "";
}

//Method for full description 
function fullDescriptionPopup(url) {
    window.open(url, 'full description', 'height=600px,width=600px,resizable=yes');

}
//Release R2.0 - Bhavya - Availability Calendar - method to fetch the availability Calender on Select Rate page load
function FetchAvailabilityCalenders(firstTimeCall) {

    var paramList = "MethodToCall=GetAvailabilityCalendars";
    paramList += "&FirstTimeCall=" + encodeURI(firstTimeCall);
    var async = true;
    var method = "POST";
    getDataFromServer(requestUrl, callBackFetchAvailabilityCalenders, async, method, paramList);
}
//Release R2.0 - Bhavya - - Availability Calendar - method to fetch the availability Calender on Select Rate page load
function callBackFetchAvailabilityCalenders() {
    var value = this.req.responseText;
    var xDoc = loadXMLDoc(value);
    if ($fn(_endsWith("hdnNotAvailableString")) != null)
        var notAvailable = $fn(_endsWith("hdnNotAvailableString")).value;
    if (xDoc != null) {
        if (xDoc.documentElement != null) {
            if (xDoc.documentElement.hasChildNodes) {
                var availabilityCalenderXmlNodes = xDoc.documentElement.getElementsByTagName("AvailabilityCalenderItem");
                var availabilityCalenderItemsCount = availabilityCalenderXmlNodes.length;

                for (var iterator = 0; iterator < availabilityCalenderItemsCount; iterator++) {
                    var minRate = "";
                    var arrivalDate = "";
                    var searchState = "";
                    if ($.browser.msie) {
                        minRate = availabilityCalenderXmlNodes[iterator].getElementsByTagName("MinRateString")[0].text;
                        arrivalDate = availabilityCalenderXmlNodes[iterator].getElementsByTagName("ArrivalDate")[0].text;
                        searchState = xDoc.documentElement.getElementsByTagName("SearchState")[0].text;
                    }
                    else {
                        minRate = availabilityCalenderXmlNodes[iterator].getElementsByTagName("MinRateString")[0].textContent;
                        arrivalDate = availabilityCalenderXmlNodes[iterator].getElementsByTagName("ArrivalDate")[0].textContent;
                        searchState = xDoc.documentElement.getElementsByTagName("SearchState")[0].textContent;
                    }
                    if (arrivalDate != null && minRate != null) {
                        var spnMinimumRate = $("#my_carousel .carouselData li .cnt p .arrivalDate:contains('" + arrivalDate + "')").parent().parent().find('.minimumRate');
                        if (spnMinimumRate.text() == "" || spnMinimumRate.text() == null) {
                            spnMinimumRate.append(minRate);
                        }
                    }
                }
                // Stop ajax call when search status is complete or the execution duration exceeds the specified time.
                if (searchState != "COMPLETED" && repeatCallDuration < waitDuration && isSearchCriteriaValid) {
                    repeatCallDuration = repeatCallDuration + 500;
                    setTimeout('FetchAvailabilityCalenders(false);', 500);
                }
                else {
                    // After a set wait time populate rate as NotAvailable for all those arrival dates where rates are not fetched from opera.             
                    for (var liIterator = 0; liIterator < availabilityCalenderItemsCount; liIterator++) {
                        var arrivalDateNORate = "";
                        if ($.browser.msie) {
                            arrivalDateNORate = availabilityCalenderXmlNodes[liIterator].getElementsByTagName("ArrivalDate")[0].text;
                        }
                        else {
                            arrivalDateNORate = availabilityCalenderXmlNodes[liIterator].getElementsByTagName("ArrivalDate")[0].textContent;
                        }
                        if (arrivalDateNORate != null) {
                            var spnUnPopulatedMinimumRate = $("#my_carousel .carouselData li .cnt p .arrivalDate:contains('" + arrivalDateNORate + "')").parent().parent().find('.minimumRate');
                            if (spnUnPopulatedMinimumRate.text() == "" || spnUnPopulatedMinimumRate.text() == null) {
                                spnUnPopulatedMinimumRate.text(notAvailable);

                            }
                        }
                    }
                }
                //Disable click event on the div if minimum rate is not available.
                var parentDivObj = $("#my_carousel .carouselData .minimumRate:contains('" + notAvailable + "')").parent().parent();
                parentDivObj.attr('onclick', "");
            }
        }
    }
}
var fgpCountryDisplayFirstTime;
function GetRedemptionPoints(firstTimeCall) {
    var fgpSearchField = $fn(_endsWith("txtdestName"));
    var paramList = "MethodToCall=GetRedemptionPoints";
    var sorting = "", sortingBasedOn = "";
    var headersup = $(".headerSortUp");
    var headersdown = $(".headerSortDown");
    var searchAll = $fn(_endsWith("hiddenAll")).value;
    if (firstTimeCall) { fgpCountryDisplayFirstTime = true; }
    else { fgpCountryDisplayFirstTime = false; }
    if (headersup.hasClass("fgpHotel")) {
        sortingBasedOn = "HOTEL";
        sorting = "Descending";
    }
    else if (headersup.hasClass("fgpDestination")) {
        sortingBasedOn = "DESTINATION";
        sorting = "Descending";
    }
    else if (headersup.hasClass("fgpRedemPoints")) {
        sortingBasedOn = "POINTS";
        sorting = "Descending";
    }
    else if (headersdown.hasClass("fgpHotel")) {
        sortingBasedOn = "HOTEL";
        sorting = "Ascending";
    }
    else if (headersdown.hasClass("fgpDestination")) {
        sortingBasedOn = "DESTINATION";
        sorting = "Ascending";
    }
    else if (headersdown.hasClass("fgpRedemPoints")) {
        sortingBasedOn = "POINTS";
        sorting = "Ascending";
    }
    var SearchCountryCityHotel = $('#selectedDestIdFGP').val().split(":")[1];
    var searchType = $('#selectedDestIdFGP').val().split(":")[0].toUpperCase(); //Vipul to dynamically set the search type
    if (!firstTimeCall && ($(fgpSearchField).val() == null || $(fgpSearchField).val() == "" || $(fgpSearchField).val() == $(fgpSearchField).attr('rel'))) {
        $("span[id$='searchedFor']").text(searchAll);
        SearchCountryCityHotel = "";
        searchType = "";
    }
    if ($fn(_endsWith("minFGPPoints")) != null || $fn(_endsWith("maxFGPPoints")) != null) {
        var minPoints = $fn(_endsWith("minFGPPoints")).value;
        var maxPoints = $fn(_endsWith("maxFGPPoints")).value;
        paramList += "&FirstTimeCall=" + encodeURI(firstTimeCall) + "&SearchType=" + encodeURI(searchType) + "&SearchName=" + encodeURI(SearchCountryCityHotel) + "&MinPoints=" + encodeURI(minPoints) + "&MaxPoints=" + encodeURI(maxPoints) + "&SortingBasedOn=" + encodeURI(sortingBasedOn) + "&Sorting=" + encodeURI(sorting);
        var async = true;
        var method = "POST";
        getDataFromServer(requestedUrl, GetRedemptionPointsCallBack, async, method, paramList);
    }
}
function GetRedemptionPointsCallBack() {
    var value = this.req.responseText;
    var redemptionData = null;
    var fgpPointsTableRow = "", campStartDate = "", campEndDate = "", campPoints = "";
    $('#ErrorFGP').hide();
    var redemptionPointsCampaignPeriodtext = $('#redemptionPointsCampaignPeriod').val();
    //var noOfBookings =  getCookie('NoOfBookings');

    if (value.search("empty") == 1) {
        $('.searchContainerFGP').hide();
        $('table.fgpPointsRegion > tbody').empty();
        $('#ErrorFGP').show().html(value);
        $('#fgpPointsProgressDiv').hide();
    }
    else if (value.search("error") == -1) {

        redemptionData = $.parseJSON(value);

    }
    else {
        //  var errorBookingDiv = $fn(_endsWith("DivBooking"));
        //  errorBookingDiv.innerHTML = value;
        $('#ErrorFGP').show().html(value);
        $('#fgpPointsProgressDiv').hide();
    }


    if (redemptionData != null && redemptionData.length > 0) {
        $('.searchContainerFGP').show();

        $('table.fgpPointsRegion > tbody').empty();

        if (fgpCountryDisplayFirstTime) {
            var searchedFor = redemptionData[0].Country;
            $("span[id$='searchedFor']").text(searchedFor);
        }
        for (var i = 0; i < redemptionData.length; i++) {
            // Country City and Hotel Names
            var country = redemptionData[i].Country;
            var city = redemptionData[i].City;
            //var hotel = redemptionData[i].Hotel;

            var destination = null;
            //Combine the city and country
            if (country != null && country != '' && city != null && city != '')
                destination = city + ", " + country;
            if (redemptionData[i].CampaignPeriod != null) {
                var campPeriod = "", campPeriodPoints = "";
                for (var j = 0; j < redemptionData[i].CampaignPeriod.length; j++) {

                    var startDateObj = eval("new " + redemptionData[i].CampaignPeriod[j].StartDate.replace(/\//g, ""));
                    var endDateObj = eval("new " + redemptionData[i].CampaignPeriod[j].EndDate.replace(/\//g, ""));

                    var d1 = startDateObj.getDate();
                    var d2 = endDateObj.getDate();


                    var day1 = (d1 < 10) ? '0' + d1 : d1;
                    var day2 = (d2 < 10) ? '0' + d2 : d2;

                    var month1 = getShortMonthString(startDateObj); //(m1 < 10) ? '0' + m1 : m1;
                    var month2 = getShortMonthString(endDateObj); //(m2 < 10) ? '0' + m2 : m2;

                    // var yy1 = startDateObj.getYear();
                    var yy2 = endDateObj.getYear();

                    //var year1 = (yy1 < 1000) ? yy1 + 1900 : yy1;
                    var year2 = (yy2 < 1000) ? yy2 + 1900 : yy2;

                    campStartDate = day1 + " " + month1;
                    campEndDate = day2 + " " + month2 + " " + year2;
                    campPoints = redemptionData[i].CampaignPeriod[j].Points;


                    campPeriod += "<br/><i class='campdate'> " + campStartDate + " - " + campEndDate + " </i>";
                    campPeriodPoints += "<br/><i class='camptext'> " + campPoints + " </i>";


                }
            }
            if (campPeriod != "" && campPeriodPoints != "") {

                fgpPointsTableRow += "<tr><td><a href='" + redemptionData[i].HotelLnk + "'>" + redemptionData[i].Hotel + "</a><br/> <i class='camptext'>" + redemptionPointsCampaignPeriodtext + "</i></td><td>" + destination + " " + campPeriod + "</td><td class='pntFGP'>" + redemptionData[i].Points + "" + campPeriodPoints + "</td></tr>";
                // var campperiodText = "<table><tr><td> </td></tr></table>";


            }
            else {
                // fgpPointsTableRow += "<tr><td>" + redemptionData[i].Hotel + "</td><td>" + destination + "</td><td class='pntFGP'>" + redemptionData[i].Points + "</td></tr>";
                fgpPointsTableRow += "<tr><td><a href='" + redemptionData[i].HotelLnk + "'>" + redemptionData[i].Hotel + "</a></td><td>" + destination + "</td><td class='pntFGP'>" + redemptionData[i].Points + "</td></tr>";
            }
        }
    }
    var $html_data = $(fgpPointsTableRow);
    $('table.fgpPointsRegion > tbody').append($html_data).trigger('update').trigger('applyWidgets');
    $('table.fgpPointsRegion').show();
    $('#fgpPointsProgressDiv').hide();
}
function validateCountryDestinationMethod(controlName, destId) {
    if ($fn(_endsWith(destId)).value == "") {
        var matchingCountryDestinations;
        var totalCountries;
        var totalCities;
        var totalHotels;
        var searchStringAfterExclusion = getDestinationAfterExclusion($fn(_endsWith(controlName)).value);
        searchStringAfterExclusion = trim(searchStringAfterExclusion, "");
        var noOfMatchesFound = 0;
        var countryNameAfterExclusion;
        var cityNameAfterExclusion;
        var hotelNameAfterExclusion;
        var IsSearchStringToCompare = false;

        // Get the search string replaced with regex mapping attributes to nordi characters.
        // AMS BUG FIX: artf720557
        searchString = getStringAfterReplace(searchStringAfterExclusion);

        // Fetch all CityDestinations which are matching with the string 
        // entered by the user. 3rd parameter made as "false" as we are 
        // going to compare the whole string.
        matchingCountryDestinations = getMatchingDestinationsforCountries(masterCountryList, searchString, IsSearchStringToCompare);

        // When we get matching CityDestinations then we will further 
        // identiy whether it is matching with CITY or HOTEL
        if (matchingCountryDestinations != null) {
            totalCountries = matchingCountryDestinations.length;

            // Looping through all the cities
            for (var countryCtr = 0; countryCtr < totalCountries; countryCtr++) {
                countryNameAfterExclusion = getDestinationAfterExclusion(matchingCountryDestinations[countryCtr].name);

                // Verifying if the search string matches with the city name
                // if(cityName.toLowerCase() == searchString.toLowerCase())

                // BUG FIX: artf711351 | Partial search should not be supported on Search and Refine search.
                // We are also comparing the length of string, as .matches() returns TRUE 
                // when city name startswith the searchString. But we need to see the 
                // exact match between the cityName and searchString.
                if (matchingCountryDestinations[countryCtr].matches(searchString, IsSearchStringToCompare) &&
                countryNameAfterExclusion.length == searchStringAfterExclusion.length) {
                    $fn(_endsWith(destId)).value = "COUNTRY:" + matchingCountryDestinations[countryCtr].countryCode;

                    //Res2.2.8 | Artifact artf1233484 : Scanweb - Search in Find a Hotel page is not working properly 
                    $fn(_endsWith(controlName)).value = matchingCountryDestinations[countryCtr].name;

                    // We are using the counter "noOfMatchesFound", to see if there 
                    // are multiple cities with same name
                    noOfMatchesFound = noOfMatchesFound + 1;
                }
                else {
                    // Verifying if the hotels in the cities are matching
                    if (matchingCountryDestinations[countryCtr].cities != null) {
                        // Fetching all hotels available in the city
                        totalCities = matchingCountryDestinations[countryCtr].cities.length;

                        for (var cityCtr = 0; cityCtr < totalCities; cityCtr++) {
                            cityNameAfterExclusion = getDestinationAfterExclusion(matchingCountryDestinations[countryCtr].cities[cityCtr].name);

                            //if(hotelName.toLowerCase() == searchString.toLowerCase())

                            // BUG FIX: artf711351 | Partial search should not be supported on Search and Refine search.
                            // We are also comparing the length of string, as .matches() returns TRUE 
                            // when hotel name startswith the searchString. But we need to see the 
                            // exact match between the hotelName and searchString.
                            if (matchingCountryDestinations[countryCtr].cities[cityCtr].matches(searchString, IsSearchStringToCompare) &&
                            cityNameAfterExclusion.length == searchStringAfterExclusion.length) {
                                $fn(_endsWith(destId)).value = "CITY:" + matchingCountryDestinations[countryCtr].cities[cityCtr].ID;

                                //Res2.2.8 | Artifact artf1233484 : Scanweb - Search in Find a Hotel page is not working properly 
                                $fn(_endsWith(controlName)).value = matchingCountryDestinations[countryCtr].cities[cityCtr].name;

                                // We are using the counter "noOfMatchesFound", to see if there 
                                // are multiple hotels with same name
                                noOfMatchesFound = noOfMatchesFound + 1;
                            }
                            else {

                                if (matchingCountryDestinations[countryCtr].cities[cityCtr].hotels != null) {
                                    // Fetching all hotels available in the city
                                    totalHotels = matchingCountryDestinations[countryCtr].cities[cityCtr].hotels.length;

                                    for (var hotelCtr = 0; hotelCtr < totalHotels; hotelCtr++) {
                                        hotelNameAfterExclusion = getDestinationAfterExclusion(matchingCountryDestinations[countryCtr].cities[cityCtr].hotels[hotelCtr].name);

                                        //if(hotelName.toLowerCase() == searchString.toLowerCase())

                                        // BUG FIX: artf711351 | Partial search should not be supported on Search and Refine search.
                                        // We are also comparing the length of string, as .matches() returns TRUE 
                                        // when hotel name startswith the searchString. But we need to see the 
                                        // exact match between the hotelName and searchString.
                                        if (matchingCountryDestinations[countryCtr].cities[cityCtr].hotels[hotelCtr].matches(searchString, IsSearchStringToCompare) &&
													hotelNameAfterExclusion.length == searchStringAfterExclusion.length) {
                                            $fn(_endsWith(destId)).value = "HOTEL:" + matchingCountryDestinations[countryCtr].cities[cityCtr].hotels[hotelCtr].ID;

                                            //Res2.2.8 | Artifact artf1233484 : Scanweb - Search in Find a Hotel page is not working properly 
                                            $fn(_endsWith(controlName)).value = matchingCountryDestinations[countryCtr].cities[cityCtr].hotels[hotelCtr].name;

                                            // We are using the counter "noOfMatchesFound", to see if there 
                                            // are multiple hotels with same name
                                            noOfMatchesFound = noOfMatchesFound + 1;
                                        }
                                    }
                                }

                            }

                        }
                    }
                }
            }
        }
    }
    else {
        noOfMatchesFound = 1;
    }

    return noOfMatchesFound;
}
function validateCountryDestination() {
    $('#fgpPointsProgressDiv').show();
    $('table.fgpPointsRegion').hide();
    var searchFieldVal = $('#txtdestName').val();
    if (searchFieldVal != null && searchFieldVal != "" && searchFieldVal != $('#txtdestName').attr('rel')) {
        var noOfMatchesFound;
        noOfMatchesFound = validateCountryDestinationMethod('txtdestName', 'selectedDestIdFGP');
        if ((noOfMatchesFound == 0) || (noOfMatchesFound > 1)) {
            var errorMessage = $('#notmatchingDest').val();
            $('#ErrorFGP').show().html(errorMessage);
            $('.searchContainerFGP').hide();
            $('#fgpPointsProgressDiv').hide();
            $('table.fgpPointsRegion > tbody').empty();
            return false;
        }
        else {
            $("span[id$='searchedFor']").text(searchFieldVal);
            GetRedemptionPoints(false);
        }
    } else {
        $("span[id$='searchedFor']").text(searchFieldVal);
        GetRedemptionPoints(false);
    }
}
// Find a hotel validation Vipul Patel MR6
function validateFindYourDestination() {
    var searchFieldVal = $('#txtFindaHotel').val();
    if (searchFieldVal == null || searchFieldVal == "" || searchFieldVal == $('#txtFindaHotel').attr('rel')) {
        var errorMessage = $('#notmatchingDestination').val();
        $('#ErrorFGP').show().html(errorMessage);
        return false;
    }
    if (searchFieldVal != null && searchFieldVal != "" && searchFieldVal != $('#txtFindaHotel').attr('rel')) {
        var noOfMatchesFound;
        noOfMatchesFound = validateDestination('txtFindaHotel', 'selectedDestinationFYD');
        if ((noOfMatchesFound == 0) || (noOfMatchesFound > 1)) {
            var errorMessage = $('#notmatchingDestination').val();
            $('#ErrorFGP').show().html(errorMessage);
            return false;
        }
        else if ($('.gmapclickevent').is(':visible')) {
            SetMaps();
        }
        else {
            $('#ErrorFGP').hide();
            CityHotelLandingRedirect();
        }
    }
}

function CityHotelLandingRedirect() {
    var searchValue = $fn(_endsWith("selectedDestinationFYD")).value;
    var inputData = "{'currentlyRenderedRoomTypes':'" + searchValue + "'}";
    $.ajax({
        type: "POST",
        url: Url + "/CityHotelLandingRedirect",
        // Pass parameter, via JSON object.
        data: inputData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            window.location.href = data.d;
        },
        error: function(data) {
            return;
        }
    });
}

function GetCountryCityDestination() {
    //var inputData = "{pageName:HotelOverviewPage}";
    $.ajax({
        type: "POST",
        url: Url + "/GetCountryCityDestinations",
        // Pass parameter, via JSON object.
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            ProcessAvailableHotels(data.d);
        },
        error: function(data) {
            return;
        }
    });
}
function ProcessAvailableHotels(data) {

    if (data != null && data.length == 1 && data[0].ClientMsg.search("empty") == 1) {
        $('#ErrorFGP').show().html(data[0].ClientMsg);
        return;
    }
    if (data != null && data.length == 1 && data[0].ClientMsg.search("error") == 1) {
        $('#ErrorFGP').show().html(data[0].ClientMsg);
        return;
    }
    if (data != null && data.length > 0 && data[0].Errors == undefined) {
        var hotelsData = data;
        var CountryHeading = "";
        var cities = "";
        var city = "";
        var country = "";

        if (hotelsData != null && hotelsData.length > 0) {

            for (var i = 0; i < hotelsData.length; i++) {

                country = hotelsData[i].DisplayName;
                cities = hotelsData[i].SearchedCities;
                var cityHeading = "";
                if (country != null && country != '' && cities != null && cities != '') {
                    var loop = 0;
                    cityHeading = "";
                    var master = $("<div class='citywrapper'>");
                    var noofcityinonecolumn = Math.round(cities.length / 3);
                    if (noofcityinonecolumn <= 4) {
                        noofcityinonecolumn = 4;
                    }
                    for (var j = 0; j < cities.length; j++) {

                        city = cities[j].Name;

                        if (cities[j].TopDestination) {

                            cityHeading += "<span class='cityColumn'><a class='cityLink cityLinkbold' href= '" + cities[j].CityLinkUrl + "'>" + city + "</a></span>";
                        }
                        else {
                            cityHeading += "<span class='cityColumn'><a class='cityLink' href= '" + cities[j].CityLinkUrl + "' >" + city + "</a></span>";
                        }
                        loop++;
                        if (loop >= noofcityinonecolumn) {
                            master.append($('<div/>', { "class": 'eachCityContainer', html: cityHeading }));
                            cityHeading = "";
                            loop = 0;
                        }

                    }
                    master.append($('<div/>', { "class": 'eachCityContainer', html: cityHeading }));
                    CountryHeading = "<div class='countrywrapper'><span class='countryHeading'><a class='countryLink' href= " + hotelsData[i].CountryLinkUrl + " >" + country + "</a></span></div>";
                    $('.countryCityContainer').append(CountryHeading);
                    $('.countryCityContainer').append(master);
                }
            }
        }
    }
}
function ProcessCountryCityDestinationMap() {
    var searchValue = $fn(_endsWith("selectedDestinationFYD")).value;
    var googleMapIframe = document.getElementById('DestinationLandingPageMapIframe');
    googleMapIframe.src = "/Templates/Scanweb/Pages/DestinationLandingPageMap.aspx?MapSearch=" + searchValue;
}

function GetCountryLandingJsonObject() {
    $.ajax({
        type: "POST",
        url: Url + "/GetCountryLandingJsonObject",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            ProcessCountryLandingJsonObject(data.d);
        },
        error: function(data) {
            return;
        }
    });
}
function ProcessCountryLandingJsonObject(data) {

    if (data != null && data.length == 1 && data[0].ClientMsg.search("empty") == 1) {
        $('#ErrorFGP').show().html(data[0].ClientMsg);
        return;
    }
    if (data != null && data.length == 1 && data[0].ClientMsg.search("error") == 1) {
        $('#ErrorFGP').show().html(data[0].ClientMsg);
        return;
    }
    if (data != null && data.length > 0 && data[0].Errors == undefined) {
        var hotelsData = data;

        if (hotelsData != null && hotelsData.length > 0) {
            ProcessCountryList(hotelsData)
        }
    }
}

function ProcessCountryList(hotelsData) {

    var hotelsData = hotelsData;
    var CountryHeading = "";
    var countryCode = $fn(_endsWith("countryid")).value;
    var country = "";
    var cities = "";
    for (var i = 0; i < hotelsData.length; i++) {

        country = hotelsData[i].Name;


        if (countryCode == hotelsData[i].CountryCode) {

            cities = hotelsData[i].SearchedCities;
            var cityHeading = "";
            if (cities != null && cities != '') {
                var loop = 0;
                cityHeading = "";
                var master = $("<div class='citywrapper'>");
                var noofcityinonecolumn = Math.round(cities.length / 3);

                if (noofcityinonecolumn <= 4) {
                    noofcityinonecolumn = 4;
                }
                for (var j = 0; j < cities.length; j++) {

                    city = cities[j].Name;

                    if (cities[j].TopDestination) {

                        cityHeading += "<span class='cityColumn'><a class='cityLink cityLinkbold' href= '" + cities[j].CityLinkUrl + "'>" + city + "</a></span>";
                    }
                    else {
                        cityHeading += "<span class='cityColumn'><a class='cityLink' href= '" + cities[j].CityLinkUrl + "' >" + city + "</a></span>";
                    }
                    loop++;
                    if (loop >= noofcityinonecolumn) {
                        master.append($('<div/>', { "class": 'eachCityContainer', html: cityHeading }));
                        cityHeading = "";
                        loop = 0;
                    }
                }
                master.append($('<div/>', { "class": 'eachCityContainer', html: cityHeading }));
                // $('.countryCityContainer').append($(cityHeading).wrapAll('<div class="citywrapper"></div>').parent());
                CountryHeading = "<div class='LinkListItem'><div class='NotLastLink'><a class='IconLink NoLink'>" + country + "</a></div></div>";
                $('#CountryLinkContainer').append(CountryHeading);
                $('.countryCityContainer').append(master);
            }
        }
        else {
            CountryHeading = "<div class='LinkListItem'><div class='NotLastLink'><a class='IconLink' href= " + hotelsData[i].CountryLinkUrl + " >" + country + "</a></div></div>";
            $('#CountryLinkContainer').append(CountryHeading);
        }

    }
}

function GetCityLandingJsonObject() {
    $.ajax({
        type: "POST",
        url: Url + "/GetCityLandingJsonObject",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            ProcessCityLandingJsonObject(data.d);
        },
        error: function(data) {
            return;
        }
    });
}
function ProcessCityLandingJsonObject(data) {
    if (data != null && data.length == 1 && data[0].ClientMsg.search("empty") == 1) {
        $('#ErrorFGP').show().html(data[0].ClientMsg);
        return;
    }
    if (data != null && data.length == 1 && data[0].ClientMsg.search("error") == 1) {
        $('#ErrorFGP').show().html(data[0].ClientMsg);
        return;
    }
    if (data != null && data.length > 0 && data[0].Errors == undefined) {
        var hotelsData = data;
        if (hotelsData != null && hotelsData.length > 0) {
            ProcessCityList(hotelsData)
        }
    }
}

function ProcessCityList(data) {
    var cities = "";
    var CityHeading = "";
    var cityId = $fn(_endsWith("cityid")).value;
    for (var i = 0; i < data.length; i++) {
        cities = data[i].SearchedCities;
        for (var j = 0; j < cities.length; j++) {
            if (cityId == cities[j].OperaDestinationId) {
                CityHeading = "<div class='LinkListItem'><div class='NotLastLink'><a class='IconLink NoLink'>" + cities[j].Name + "</a></div></div>";
                $('#CityLinkContainer').append(CityHeading);
            }
            else {
                CityHeading = "<div class='LinkListItem'><div class='NotLastLink'><a class='IconLink' href= " + cities[j].CityLinkUrl + " >" + cities[j].Name + "</a></div></div>";
                $('#CityLinkContainer').append(CityHeading);
            }
        }
    }
}