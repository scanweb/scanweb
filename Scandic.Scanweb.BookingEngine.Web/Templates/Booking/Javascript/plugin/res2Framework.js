﻿/*created by suresh for reservation 2.0*/
var scandic = {
    init: function() {
        scandic.tabify();
        /*scandic.floatingShoppingCart();*/
        scandic.flyout();
        scandic.toolTip();
        scandic.roomSelect();
        var leftVisibleCarouselIndex = $fn(_endsWith("leftVisibleCarouselIndex"));
        var current = 0;
        if (leftVisibleCarouselIndex != null && leftVisibleCarouselIndex.value != "") {
            current = parseInt(leftVisibleCarouselIndex.value);
        }
        //scandic.calendarCarousel('.carouselData', '#my_carousel', '.btnnext', '.btnprev', 5, 5, 0, 143, 130);
        scandic.calendarCarousel('.carouselData', '#my_carousel', '.btnnext', '.btnprev', 5, 5, current, 143);
        scandic.carsouel('#bedTypeList', '.LightBoxCntnt', '.m_NextBtn', '.m_PrevBtn', 1, 1, 474);
        scandic.showColumn();
        scandic.modifyRoom();
        scandic.addRoom();
        scandic.removeRoom();
        scandic.columnColoring();
        scandic.toggleMenu();
        scandic.enrollDetails();
        scandic.enableFields();
        scandic.cancelCheck();
        scandic.chkLogin();
        scandic.stayRoomsel();
        scandic.chSelect();
        scandic.textBoxcol();
        scandic.togglePref();
        scandic.toggleFutbooking();
        scandic.displayRoomType();
        scandic.bedPref();
        scandic.replaceHeader();
        scandic.imgGallery();
        scandic.imgGalleryHide();
        scandic.flyoutSort();
        scandic.tooltipSelectRate();
        scandic.showHideFacilities();
        scandic.addFltlefttooltip();
		scandic.ie7fixSelectrateTip();
    },

    /* Method to replace stool header with border in case heading is missing */
    replaceHeader: function() {
        $('div.stoolHeader').each(function() {
            if ($(this).find('h2').length > 0 || $(this).find('h3').length > 0) {
            } else {
                $(this).parent().css('border-top', '1px solid #ccc');
                $(this).remove();
            }
        });
    },

    /*
    Function to Handle the Facilities Hide and Show Functionality
    Author: Sheril Jebasingh
    Release: Site Expansion
    */
    showHideFacilities: function() {
        $('.hotelFacilitiesBtn').click(function(event) {
            $('.hotelFacilites').toggle();
            if (!$('.hotelFacilites').is(':visible'))
                $('.hotelFacilitiesBtn').text($('#hdnViewFullDescription').val());
            else
                $('.hotelFacilitiesBtn').text($('#hdnHideFullDescription').val());
            event.preventDefault();
        });
    },
    addFltlefttooltip: function() {
        $.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());
        if ($.browser.chrome) {
            $('.toolTipMe').addClass("fltLft");
        }
    },

    flyoutSort: function() {

        var cols = 4; //defines no of columns
        var container = $('.BFlyOut .linkList ul');
        var items = container.find('li');
        var itemsPerCol = Math.ceil(items.length / cols); //get Items per column
        var stack = []; //array to hold items
        for (var i = 0; i < itemsPerCol; i++) {
            for (var k = 0; k < cols; k++) {
                //Priya Singh: Code implemented to check if the list item is undefined, push empty list item
                //Res 2.2.7 - AMS Patch4 - Artifact artf1219210 : Scanweb - All destinations not showing in "All Destinations"
                if (items[i + (itemsPerCol * k)] != undefined) {
                    stack.push(items[i + (itemsPerCol * k)]); //push items into respective columns
                }
                else {
                    var new_element = document.createElement('li');
                    new_element.innerHTML = "&#160;";
                    new_element.setAttribute("class", "emptyCountryList");
                    stack.push(new_element);
                }
            }
        }

        items = container.find('li');
        items.css({
            'float': 'left',
            'width': '135px'
        });

        container.html(stack).append($('<br>').css({ clear: 'both' })); //paint the columns in sorted order
        scandic.flyout();
    },

    /* Scadic Tabs */
    tabify: function() {
        $('#tabWrapper a.tabs').click(function(event) {
            event.preventDefault();
            $('#tabWrapper a').removeClass('active');
            $('.tabCntnt').hide();
            $("div#TabStructure .tabCntnt .tableData .topCnt").hide();

            /* Get Tab reference to be shown*/
            var divObj = $(this).attr('rel');
            if (!divObj) {
                return;
            }
            if (divObj) {
                $(divObj).show();
                $(this).addClass('active');
                /*$(".aboutOurRate div").hide();
                $(".aboutOurRate "+divObj+"").show();*/
            }
            scandic.columnColoring();
        });
    },

    /* Image Gallery Tab */
    imgGallery: function() {
        $('#imgGallery a').eq(0).addClass('active');
        $('#imgGallery a').click(function(event) {
            event.preventDefault();
            $('#imgGallery a').removeClass('active');
            $('.tabCntntt').hide();
            /*$('.tabCntnt').show();*/
            /* Get Tab Reference to be shown */
            var divObj = $(this).attr('rel');
            if (!divObj) {
                return;
            }
            if (divObj) {
                $(divObj).show();
                $(this).addClass('active');
            }
        });
    },
    imgGalleryHide: function() {
        $('.tabCntntt .jqmClose').click(function(event) {
            event.preventDefault();
            $('.dialog').jqmHide();
            $('#imgGallery').hide();
            $('.bedTypeListclass').empty();
        });
    },

    bedPref: function() {
        $('.bedTypeList td input').click(function() {
            var getParent = $(this).parents('ul');
            var getRoomNo = getParent.attr('rel');
            var getText = $(this).next('label').text();
            $(getRoomNo).text(getText);
        });
    },

    displayRoomType: function() {
        $('#TabStructure .RRWrapper .overlay,.newroomoverlay').click(function() {
            /*get the class name from rel attribute*/
            var getClass = $(this).attr('rel');
            if (!getClass) {
                return;
            }
            else {
                $('.LightBoxCntnt .hideRoomType').hide();
                $(".LightBoxCntnt " + getClass).show();
            }
        });
    },

    togglePref: function() {
        $("h2.trigger").click(function() {
            $(this).next().slideToggle('slow');
            $(this).toggleClass("active").next().next().slideToggle('slow');


        });
    },
    toggleFutbooking: function() {
        $("span.viewfReservation").click(function() {
            $('.futureReservationData').slideToggle('slow');
            $("span.hidefReservation").show();
            $(this).hide();
        });
        $("span.hidefReservation").click(function() {
            $('.futureReservationData').slideToggle('slow');
            $("span.viewfReservation").show();
            $(this).hide();
        });
    },
    textBoxcol: function() {
        $('input.text-input').keyup(function() {
            if ($(this).attr('rel') == $(this).val()) {
                $(this).css('color', '#999');
            } else {
                $(this).css('color', '#000');
            }
        });
    },
    /*check login for box */
    chkLogin: function() {
        $('a[rel="loginOn"]').click(function() {
            $("div.LoginHeader .loginCnt").show();
        });
        $('a[rel="loginClose"]').click(function() {
            $("div.LoginHeader .loginCnt").hide();
        });
    },
    /* room cancel check all boxes*/
    cancelCheck: function() {
        $('input.chkBxAll').click(function() {
            var isChkall = $(this).is(':checked');
            if (isChkall) {
                $("input.cancChkbx").attr('checked', 'checked');
            } else {
                $("input.cancChkbx").removeAttr('checked');
            }
        });
    },
    /* enable the input fields */
    enableFields: function() {
        var j = 0;
        $('.chkEnable').each(function() {
            $(this).click(function() {
                var isChk = $(this).is(':checked');
                if (isChk) {
                    $(this).parent().next().find('input[type="text"], select').removeAttr('disabled');
                } else {
                    $(this).parent().next().find('input[type="text"], select').attr('disabled', 'disabled').removeClass('mandatory').removeClass('error');
                    $('div.formError').hide();
                }
            });
            j++;
        });
    },
    /* Enroll For validation */
    enrollDetails: function() {
        var i = 0;
        $('.chkEnroll').each(function() {
            $(this).click(function() {
                var chkVal = $(this).val();
                var divObj = $(this).attr('rel');
                if (chkVal == 'holdOn') {

                    $("div.bookingDetailWrapper div.depositInfo " + "." + divObj).hide();
                    var strnew1 = divObj.substring(4, divObj.length);
                    var controlr = $fn(_endsWith('RoomInformation' + strnew1 + '_txtCardHolder')); //$fn(_endsWith('_txtCardHolder'));
                    $(controlr).removeClass('validate[required], mandatory'); //$(controlr).removeClass('validate[required]');mandatory
                    //$(controlr).removeClass('mandatory');
                    var controlr1 = $fn(_endsWith('RoomInformation' + strnew1 + '_txtCardNumber')); //$fn(_endsWith('_txtCardNumber'));
                    $(controlr1).removeClass('validate[required], mandatory');
                    //$(controlr1).removeClass('mandatory');
                    var controlr2 = $fn(_endsWith('RoomInformation' + strnew1 + '_ddlCardType')); //$fn(_endsWith('_ddlCardType'));
                    $(controlr2).removeClass('validate[required], mandatory');
                    //$(controlr2).removeClass('mandatory');

                    if ($fn(_endsWith("panHashCreditCardDivCommon")) != null) {
                        $fn(_endsWith("panHashCreditCardDivCommon")).style.display = "none";
                        $fn(_endsWith("panHashCreditCardDivCommon")).removeClass('validate[required], mandatory');
                    }

                    if ($fn(_endsWith("RoomInformation" + strnew1 + "_panHashCreditCardDiv")) != null) {
                        $fn(_endsWith("RoomInformation" + strnew1 + "_panHashCreditCardDiv")).style.display = "none";
                        $fn(_endsWith("RoomInformation" + strnew1 + "_panHashCreditCardDiv")).removeClass('validate[required], mandatory');
                    }

                    var controlr3 = $fn(_endsWith('_txtCardHolderCommon'));
                    $(controlr3).removeClass('validate[required], mandatory');
                    //$(controlr3).removeClass('mandatory');
                    var controlr4 = $fn(_endsWith('_txtCardNumberCommon'));
                    $(controlr4).removeClass('validate[required], mandatory');
                    //$(controlr4).removeClass('mandatory');
                    var controlr5 = $fn(_endsWith('_ddlCardTypeCommon'));
                    $(controlr5).removeClass('validate[required], mandatory');
                    //$(controlr5).removeClass('mandatory');
                }
                else if (chkVal == 'enroll') {
                    //RK: Release 2.0 | Enroll User | populate the enrollmentvalues if the the enroll checkbox is checked
                    if (this.checked) {
                        // var pos=$('#lblTermAndCondition').position();
                        //pos.top=pos.top+520;
                        $("div.formError:contains(checkbox)").css("top", "1758.73px");
                        $("div.bookingDetailWrapper div.FGP div.essenInfo").show();
                        // added 25/10
                        // var showtoolTip = $fn(_endsWith('_chkTermsConditionformError'));
                        // alert($("div _chkTermsConditionformError ").length);

                        var firstName = $fn(_endsWith('_txtFNameRoom'));
                        var lastName = $fn(_endsWith('_txtLNameRoom'));
                        var country = $fn(_endsWith('_ddlCountry'));
                        var countryCode = $fn(_endsWith('_ddlMobileNumber'));
                        var phoneNumber = $fn(_endsWith('_txtTelephone'));
                        //var city = $fn(_endsWith('_txtCityOrTown'));
                        var emailAddress = $fn(_endsWith('_txtEmail'));
                        //var confirmEmailAddress = $fn(_endsWith('_txtConfirmEmailRoom'));

                        var enFirstName = $fn(_endsWith('_txtEnFName'));
                        var enLastName = $fn(_endsWith('_txtEnLName'));
                        var enCountry = $fn(_endsWith('_ddlEnCountry'));
                        var enCountryCode = $fn(_endsWith('_ddlEnTelephoneCode'));
                        var enTelePhone = $fn(_endsWith('_txtEnTelephone'));
                        //var enCity = $fn(_endsWith('_txtEnCityOrTown'));
                        var enEmailAddress = $fn(_endsWith('_txtEnEmail'));
                        //var enConfirmEmailAddress = $fn(_endsWith('_txtConfirmEmailRoom'));

                        SetValue(firstName, enFirstName);
                        SetValue(lastName, enLastName);
                        SetValue(country, enCountry);
                        SetValue(countryCode, enCountryCode);
                        //SetValue(city, enCity);
                        SetValue(emailAddress, enEmailAddress);
                        SetValue(phoneNumber, enTelePhone);
                        if ($(enFirstName).val()) { $(enFirstName).valid(); }
                        if ($(enLastName).val()) { $(enLastName).valid(); }
                        if ($(enCountry).val()) {
                            if ($(enCountry).val() != "DFT") { $(enCountry).valid(); }
                        }
                        if ($(enCountryCode).val()) {
                            if ($(enCountryCode).val() != "DFT") { $(enCountryCode).valid(); }
                        }
                        if ($(enTelePhone).val()) { $(enTelePhone).valid(); }
                        if ($(enEmailAddress).val()) { $(enEmailAddress).valid(); }
                    }
                    else //If not checked, hide the div
                    {
                        $("div.bookingDetailWrapper div.FGP div.essenInfo").hide();
                    }
                }
                else {
//                    $("div.bookingDetailWrapper div.depositInfo " + "." + divObj).show();
                    var strnew2 = divObj.substring(4, divObj.length);
                    var control = $fn(_endsWith('RoomInformation' + strnew2 + '_txtCardHolder')); //$fn(_endsWith('_txtCardHolder'));
                    $(control).addClass('validate[required]');
                    var control1 = $fn(_endsWith('RoomInformation' + strnew2 + '_txtCardNumber')); //$fn(_endsWith('_txtCardNumber'));
                    $(control1).addClass('validate[required]');
                    var control2 = $fn(_endsWith('RoomInformation' + strnew2 + '_ddlCardType')); //$fn(_endsWith('_ddlCardType'));
                    $(control2).addClass('validate[required]');

                    if ($fn(_endsWith("panHashCreditCardDivCommon")) != null) {
                        $fn(_endsWith("panHashCreditCardDivCommon")).style.display = "block";
                        //$fn(_endsWith("panHashCreditCardDivCommon")).addClass('validate[required]');
                       jQuery($fn(_endsWith("panHashCreditCardDivCommon"))).attr("class","validate[required]");
                    }

                    if ($fn(_endsWith("RoomInformation" + strnew2 + "_panHashCreditCardDiv")) != null) {
                        $fn(_endsWith("RoomInformation" + strnew2 + "_panHashCreditCardDiv")).style.display = "block";
                        //$fn(_endsWith("RoomInformation" + strnew2 + "_panHashCreditCardDiv")).addClass('validate[required]');
                       jQuery($fn(_endsWith("RoomInformation" + strnew2 + "_panHashCreditCardDiv"))).attr("class","validate[required]");
                    }

                    var control3 = $fn(_endsWith('_txtCardHolderCommon'));
                    $(control3).addClass('validate[required]');
                    var control4 = $fn(_endsWith('_txtCardNumberCommon'));
                    $(control4).addClass('validate[required]');
                    var control5 = $fn(_endsWith('_ddlCardTypeCommon'));
                    $(control5).addClass('validate[required]');
                }
            });
            i++;
        });
    },
    toggleMenu: function() {
        $('.roomDetails .hdLft').click(function() {

            /*set variables for header, panel and footer*/
            var menuItem = $(this);
            var menuItemParent = menuItem.parent();
            var panel = menuItem.next();
            var panelFooter = panel.next();

            //Res 2.1 -Parvathi :artf1157048 : Help icon/text by Reservation number is	incorrect
            var divPrevious = $fn(_endsWith('divPrevBookingHeader'));
            if (menuItem = divPrevious) {
                var helpIcon = $fn(_endsWith("lnkReservationToolTip"));
                if (menuItemParent.hasClass("expand")) {
                    helpIcon.setAttribute("style", "display:none");
                }
                else if (menuItemParent.hasClass("collapse")) {
                    helpIcon.removeAttribute("style");
                }
            }

            /*search for the class expand and on click make it Collapse */
            if (menuItemParent.hasClass("expand")) {
                menuItemParent.removeClass('expand').addClass('collapse');
                panel.slideUp();
                panelFooter.hide();
            }
            /*search for the class collapse and on click make it expand */
            else if (menuItemParent.hasClass("collapse")) {
                menuItemParent.removeClass('collapse').addClass('expand');
                panel.slideDown();
                panelFooter.show();

            }
        });
    },


    floatingShoppingCart: function() {
        /*fetch the class  of the layer to be scrolled*/
        var divObj = $('div.floatingCart');
        var addMgn = $(divObj).height();
        /*set margin-top for the next module as shooping cart is absolutely positioned it will be super imposed so need a margin here*/
        $(divObj).next().css('margin-top', addMgn + 20 + 'px');
        /*Trigger a scroll method here*/
        $(window).scroll(function() {
            if (!divObj) { return; }
            if (divObj) {
                $(divObj).animate({ top: $(window).scrollTop() + "px" }, { queue: false, duration: 350 });
            }
        });
    },

    flyout: function() {

        $('div[id$="BookingArea"] span.flyOutRt').click(function() {
            $(".activeHeader .mainContent .errorText").hide();
            var getFlyout = $('#BFlyOut');
            $(getFlyout).show();
            var bonus = $(this).attr('bookingType');
            if (bonus == "bonusCheque") {
                $(getFlyout).css('top', '248px');
            }
            else if (bonus == "redeem") {
                $(getFlyout).css('top', '288px');
            }
            else {
                $(getFlyout).css('top', '208px');
            }
        });

        $('#blkClose').click(function() {
            $('#BFlyOut').hide();
        });
        $('#BFlyOut ul li a').click(function() {
            var inputText = $(this).text(); //RK:Reservation 2.0 | Getting the text value of link clicked rather than title
            $('#BFlyOut').hide();
            $('#BookingArea .stayInputLft').val(inputText);
            if (null != $fn(_endsWith("selectedDestId")))//) && (null != $fn(_endsWith("selectedDestId1")))))
            {
                var val = $(this).attr('rel');
                $fn(_endsWith("selectedDestId")).value = val;
                //	    $fn(_endsWith("selectedDestId1")).value = val;
                //	    DestinationRN
            }
        });
        $('#autosuggestFGP ul li a').click(function() {
            var inputText = $(this).text();
            if (null != $fn(_endsWith("selectedDestIdFGP")))//) && (null != $fn(_endsWith("selectedDestId1")))))
            {
                var val = $(this).attr('id');
                $fn(_endsWith("selectedDestId")).value = val;
            }
        });
    },

    accordian: function(id, index) {
        $('' + id + ' .expand .mainContent').hide();
        $('' + id + ' .expand .mainContent').eq(index).show();
        $('' + id + ' .expand ').eq(index).addClass('activeHeader');
        /*Method on clicking header*/
        $('' + id + ' div.expand .hdDeflt,' + id + ' .hdFrst').click(function() {
            $('' + id + ' div.expand').removeClass('activeHeader');
            $(this).parent().addClass('activeHeader');
            var checkElement = $(this).next();
            var checkTab = $(this);
            if (null != $fn(_endsWith("sT"))) {
                $fn(_endsWith("sT")).value = $(this).attr('id');
                associateControlBasedOnAccordianSelected();
                //Release R2.0 - Bhavya - Defect Fix - 466702 - this method binds the tab out method to the text box when the current visible accordian is changed. 
                bindTabOutToNoOfNights();

                //Res 2.0 -Parvathi:artf1153056: Not possible to search on start page(Issue2 is fixed)
                if ($fn(_endsWith("sT")).value == 'Tab1') {
                    var errorDivBC = $fn(_endsWith('BCClientErrorDiv'));
                    errorDivBC.style.display = "none";

                    var errorDivBC = $fn(_endsWith('RNClientErrorDiv'));
                    errorDivBC.style.display = "none";

                    var errorDivBC = $fn(_endsWith('clientErrorDivBD'));
                    errorDivBC.style.display = "none";

                    var txtUsr = $fn(_endsWith('txtUserName'));
                    $(txtUsr).removeClass('mandatory');
                    $(txtUsr).addClass('input input defaultColor input defaultColor');

                    var txtBkNo = $fn(_endsWith('txtBookingNumber'));
                    $(txtBkNo).removeClass('mandatory');
                    $(txtBkNo).addClass('input input defaultColor input defaultColor');

                    var txtSrNm = $fn(_endsWith('txtSurname'));
                    $(txtSrNm).removeClass('mandatory');
                    $(txtSrNm).addClass('input input defaultColor input defaultColor');

                    var txtIpPsw = $fn(_endsWith('inputtxtPassword'));
                    $(txtIpPsw).removeClass('mandatory');
                    $(txtIpPsw).addClass('defaultColor password-clear fltLft');

                    // R2.0 - Bhavya - artf1154229 : Children age field color (yellow) is not cleared. Remove the Mandatory Class 
                    $('#tabContainer2 .mandatory').removeClass('mandatory');
                    $('#tabContainer3 .mandatory').removeClass('mandatory');


                }
                else if ($fn(_endsWith("sT")).value == 'Tab2') {
                    var errorDivReg = $fn(_endsWith('RegClientErrorDiv'));
                    errorDivReg.style.display = "none";

                    var errorDivBC = $fn(_endsWith('RNClientErrorDiv'));
                    errorDivBC.style.display = "none";

                    var errorDivBC = $fn(_endsWith('clientErrorDivBD'));
                    errorDivBC.style.display = "none";

                    var txtUsr = $fn(_endsWith('txtUserName'));
                    $(txtUsr).removeClass('mandatory');
                    $(txtUsr).addClass('input input defaultColor input defaultColor');

                    var txtBkNo = $fn(_endsWith('txtBookingNumber'));
                    $(txtBkNo).removeClass('mandatory');
                    $(txtBkNo).addClass('input input defaultColor input defaultColor');

                    var txtSrNm = $fn(_endsWith('txtSurname'));
                    $(txtSrNm).removeClass('mandatory');
                    $(txtSrNm).addClass('input input defaultColor input defaultColor');

                    var txtIpPsw = $fn(_endsWith('inputtxtPassword'));
                    $(txtIpPsw).removeClass('mandatory');
                    $(txtIpPsw).addClass('defaultColor password-clear fltLft');

                    // R2.0 - Bhavya - artf1154229 : Children age field color (yellow) is not cleared. Remove the Mandatory Class 
                    $('#tabContainer1 .mandatory').removeClass('mandatory');
                    $('#tabContainer3 .mandatory').removeClass('mandatory');

                }
                else if ($fn(_endsWith("sT")).value == 'Tab3') {
                    var errorDivReg = $fn(_endsWith('RegClientErrorDiv'));
                    errorDivReg.style.display = "none";

                    var errorDivBC = $fn(_endsWith('BCClientErrorDiv'));
                    errorDivBC.style.display = "none";

                    var errorDivBC = $fn(_endsWith('clientErrorDivBD'));
                    errorDivBC.style.display = "none";

                    var txtBkNo = $fn(_endsWith('txtBookingNumber'));
                    $(txtBkNo).removeClass('mandatory');
                    $(txtBkNo).addClass('input input defaultColor input defaultColor');

                    var txtSrNm = $fn(_endsWith('txtSurname'));
                    $(txtSrNm).removeClass('mandatory');
                    $(txtSrNm).addClass('input input defaultColor input defaultColor');

                    // R2.0 - Bhavya - artf1154229 : Children age field color (yellow) is not cleared. Remove the Mandatory Class 
                    $('#tabContainer1 .mandatory').removeClass('mandatory');
                    $('#tabContainer2 .mandatory').removeClass('mandatory');

                }
                else if ($fn(_endsWith("sT")).value == 'Tab4') {
                    var errorDivReg = $fn(_endsWith('RegClientErrorDiv'));
                    errorDivReg.style.display = "none";

                    var errorDivBC = $fn(_endsWith('RNClientErrorDiv'));
                    errorDivBC.style.display = "none";

                    var errorDivBC = $fn(_endsWith('BCClientErrorDiv'));
                    errorDivBC.style.display = "none";

                    var txtUsr = $fn(_endsWith('txtUserName'));
                    $(txtUsr).removeClass('mandatory defaultColor');
                    $(txtUsr).addClass('input input defaultColor input defaultColor');

                    var txtIpPsw = $fn(_endsWith('inputtxtPassword'));
                    $(txtIpPsw).removeClass('mandatory');
                    $(txtIpPsw).addClass('defaultColor password-clear fltLft');
                    // R2.0 - Bhavya - artf1154229 : Children age field color (yellow) is not cleared. Remove the Mandatory Class 
                    $('#tabContainer1 .mandatory').removeClass('mandatory');
                    $('#tabContainer2 .mandatory').removeClass('mandatory');
                    $('#tabContainer3 .mandatory').removeClass('mandatory');

                }
            }
            if ((checkElement.is('div')) && (checkElement.is(':visible'))) {
                return false;
            }
            if ((checkElement.is('div')) && (!checkElement.is(':visible'))) {
                $('' + id + ' div.expand .mainContent:visible').slideUp('slow');
                checkElement.slideDown('slow', function() {
                    checkTab.children('.hdRtDeflt').find("a.tabFocus").focus();
                });
                return false;
            }
        });
    },

    toolTip: function() {
    var tooltipimages = new Array()
    function preload() {
        for (i = 0; i < preload.arguments.length; i++) {
            tooltipimages[i] = new Image()
            tooltipimages[i].src = preload.arguments[i]
        }
    }
    preload(
				"/Templates/Booking/Styles/Default/Images/reservation2.0/toolTipSprite.png",
				"/Templates/Booking/Styles/Default/Images/reservation2.0/toolTipCotent.png",
				"/Templates/Booking/Styles/Default/Images/reservation2.0/pointerRight.png",
				"/Templates/Booking/Styles/Default/Images/reservation2.0/tooltipBullet.gif",
				"/Templates/Booking/Styles/Default/Images/reservation2.0/ratesSprite.png",
				"/Templates/Booking/Styles/Default/Images/reservation2.0/M15_selectRoomToolTipCnt.png",
				"/Templates/Booking/Styles/Default/Images/reservation2.0/pointerLeft.png"
			)
        $('.toolTipMe').hover(function() {

            //artf1150961 : Missing space in FGP log in button |Rajneesh

            //artf1150961 : Missing space in FGP log in button |Rajneesh
            //Fixed For:Tooltip is hiding from the booking modulesmall in Logout Confirmation and Login Status page because the z-index for booking module small was higher then that of tooltip span

            //Release 2.2.0|artf1167498 : Your stay covers FGP log in box on mouse over|Rajneesh
            //Release 2.2.0|artf1169126 : Tooltip box for FGP dispays behind your stay |Rajneesh
            //Release 2.2.0|artf1165007 : Tooltip on map view displayes behind your stay |Rajneesh
            if (($(this).attr("id") == "remembrMeLoginError")) {
                $('#remembrMeLoginError').css('z-index', '998');
                $('.regular').css('z-index', '997');
            }
            else if ($(this).attr("id") == "remembrMeTryAgain") {
                $('#remembrMeTryAgain').css('z-index', '998');
                $('.regular').css('z-index', '997');
            }
            else if ($(this).attr("id") == "tipPerStayMap") {
                $('#tipPerStayMap').css('z-index', '1');
                $('#yourStayMod05').css('position', 'relative');
            }
            else if ($(this).attr("id") == "rememberMeBookingDetailLogin") {
                $('#yourStayMod05').css('z-index', '-1');
                $('#yourStayMod05').css('position', 'relative');
            }
            else if ($(this).attr("id") == "corporate") {
                $('.RRWrapper').css('z-index', '1');
            }
            else if ($(this).attr("id") == "plsSelect") {
                $('.regular').css('z-index', '5');
            }
            else if ($(this).attr("id") == "shopcartedtbkcodehelp") {
                $('.regular').css('z-index', '5');
            }
			else if ($(this).hasClass("alternateCurrency")) {
                $(this).css('text-decoration', 'none');
            }
			
            else {
                $('#remembrMeLoginError').css('z-index', '');
                $('#remembrMeTryAgain').css('z-index', '');
            }


            // $('.regular').css('position', 'relative'); commented by Vipul in Branding project
            this.tip = this.title;
            var toolTipLength = this.tip.length;
            if (toolTipLength > 75 && toolTipLength < 500) {
                setToolTipWidth = "350px";
            }
            else if (toolTipLength > 500) {
                setToolTipWidth = "450px";
            } else {
                setToolTipWidth = "200px";
            }
            
            var popUpAddAnotherHotelId = $fn(_endsWith('idAddAnotherHotel'));
			if(popUpAddAnotherHotelId)
				popUpAddAnotherHotelId=popUpAddAnotherHotelId.id;
				
			// added for tooltip in popup booking content Azhar
			if ($(this).parents('#dialog').length > 0 || $(this).parents('#'+popUpAddAnotherHotelId).length > 0 )
			{
				if (toolTipLength > 500) {
					setToolTipWidth = "650px";
				}
				
				$("#aspnetForm").append(
					'<span id="dialogTtContainer"><div class="ttContainer" style="width:' + setToolTipWidth + '">'
					+ '<div class="ttTop"><span>&#160;</span></div>'
					+ '<div class="ttMainContentWrapper">'
					+ '<div class="ttContentWrapper">'
					+ '<div class="ttContent">'
						+ this.tip
					+ '</div>'
					+ '</div>'
					+ '</div>'
					+ '<div class="ttBottom"><span>&#160;</span></div>'
					+ '<span class="ttpointer">&#160;</span>'
					+ '</div></span>'
				);
				
				
				this.title = "";
				
				var position =$(this).offset();
				
				$('#dialogTtContainer').show();
				$('#dialogTtContainer').children('.ttContainer').show();
				$('#dialogTtContainer').css({'position':'absolute','top':position.top+11,'left':position.left,'z-index':'99999998'});
				$('.ttpointer').css({'height':'11px'});
			}
			else
			{
				$(this).append(
					'<div class="ttContainer" style="width:' + setToolTipWidth + '">'
						+ '<div class="ttTop"><span>&#160;</span></div>'
						+ '<div class="ttMainContentWrapper">'
						+ '<div class="ttContentWrapper">'
						+ '<div class="ttContent">'
							+ this.tip
						+ '</div>'
						+ '</div>'
						+ '</div>'
						+ '<div class="ttBottom"><span>&#160;</span></div>'
						+ '<span class="ttpointer">&#160;</span>'
					+ '</div>'
				);
				this.title = "";
				$(this).children('.ttContainer').show();
			}
        },
	function() {
	    $('.regular').css('z-index', '0');
		

		if($(this).hasClass("alternateCurrency")) {
                $(this).css('text-decoration', 'underline');
            }
	    // added for tooltip in popup booking content Azhar
	    var popUpAddAnotherHotelId = $fn(_endsWith('idAddAnotherHotel'));
	    if (popUpAddAnotherHotelId)
	        popUpAddAnotherHotelId = popUpAddAnotherHotelId.id;

	    // added for tooltip in popup booking content Azhar
	    if ($(this).parents('#dialog').length > 0 || $(this).parents('#' + popUpAddAnotherHotelId).length > 0) {
	        $('#dialogTtContainer').fadeOut(100);
	        $('#dialogTtContainer').remove();
	    }
	    else {
	        $('.ttContainer').fadeOut(100);
	        $(this).children().remove();
	    }
	    if (this.tip == undefined) {
	        this.tip = this.title;
	    } else {
	        this.title = this.tip;
	    }
	}
	);
        $('.toolTipMe').click(function(e) {
            e.preventDefault();
        });

    },
	ie7fixSelectrateTip: function(){
	
	$('.tableData table th a.help').hover(function(){
	if ($.browser.msie  && parseInt($.browser.version, 10) === 7) {
							 $('.RRWrapper').css('z-index', '10');
			}
	},	
	function(){
	if ($.browser.msie  && parseInt($.browser.version, 10) === 7) {
							 $('.RRWrapper').css('z-index', '0');
			}
	});	
	
	},
    tooltipSelectRate: function() {
        $('.roomTypeToolTip a.overlay').hover(function() {
            $(this).next(".ttContainer").show();
        }, function() {
            $(this).next(".ttContainer").hide();
        });
    },

    //calendarCarousel('.carouselData','#my_carousel','.btnnext','.btnprev',5,5,0, 143,130);
    calendarCarousel: function(ulID, containerID, nextBtn, prevBtn, step, visible, cal_current, liSize) {
        //set Index for active tab
    var activeIndex = $('.selectedCalendarItemIndex').val();
    if (activeIndex >= 0)
            $(ulID + ' li').eq(activeIndex%5).addClass("active");

        var step = step;
        //var current = 0; 
        var maximum = $('' + ulID + ' li').size();
        var visible = visible;
        var speed = 200;
        var liSize = liSize;
        var carouselId = $(ulID);
        var ulSize = (liSize * maximum) + 1;
        //var divSize = liSize * visible; 
        /*calculate ul size and based on li*/
        //$(carouselId).css("width", ulSize+"px").css("left", -(cal_current * liSize)).css("position", "absolute");
        $(carouselId).css("width", ulSize + "px").css("left", -(cal_current * liSize));
        //$(containerID).css("width", divSize+"px");

        /*Setting server next and previous buttons*/
        var prevServerBtn = $('.btnprev1');
        var nextServerBtn = $('.btnnext1');
        prevServerBtn.show();
        nextServerBtn.show();

        var prevButton = $('.btnprev');
        var nextButton = $('.btnnext');
        prevButton.hide();
        nextButton.hide();

        var firstBatch = $('.firstBatch').val();
        var lastBatch = $('.lastBatch').val();

        if (prevServerBtn != null) {
            prevServerBtn.show();
        }

        if (nextServerBtn != null) {
            nextServerBtn.show();
        }

        if (cal_current == 0) {
            if (prevServerBtn != null) {
                if (firstBatch != "true") {
                    prevServerBtn.show();
                }
                /*else {
                    prevServerBtn.hide();
                }*/
            }
        }
        else if (cal_current + step >= maximum) {
            if (nextServerBtn != null) {
                if (lastBatch != "true") {
                    nextServerBtn.show();
                }
               /* else {
                    nextServerBtn.hide();
                }*/
            }
        }
        /*else {
            if (prevServerBtn != null) {
                prevServerBtn.hide();
            }
            if (nextServerBtn != null) {
                nextServerBtn.hide();
            }
        }*/

        if (prevServerBtn != null) {
            if (firstBatch != "true") {
                prevServerBtn.show();
            }
        }
        
        /*Method to click li function*/
        $('' + ulID + ' li .cnt').click(function() {
            $('' + ulID + ' li').removeClass("active");
            //Defect fix - artf1148171 - Bhavya - No Availability calendar block should not be selectable.
            var notAvailableText = $fn(_endsWith("hdnNotAvailableString")).value;
            var calendarDiv = $(this).attr("id");
            if ($('#' + calendarDiv + ' #spnMinimumBaseRate').text() != notAvailableText) {
                $(this).parent().addClass("active");
            }
        });
    },

    carsouel: function(ulID, containerID, nextBtn, prevBtn, step, visible, liSize) {

        var step = step;
        var current = 0;
        var maximum = $('' + ulID + ' li').size();
        var visible = visible;
        var speed = 200;
        var liSize = liSize;
        var carousel_height = carousel_height;
        var carouselId = $(ulID);
        var ulSize = (liSize * maximum) + 1;
        var divSize = liSize * visible;
        /*calculate ul size and based on li*/
        $(carouselId).css("width", ulSize + "px").css("left", -(current * liSize)).css("position", "absolute");
        $(containerID).css("width", divSize + "px").css("visibility", "visible").css("overflow", "hidden").css("position", "relative");

        //if has some class
		if(maximum == 1)
			$(nextBtn).hide();
        $(prevBtn).hide();

        /*Method to trigger next button*/
        $(nextBtn).click(function() {

            if (current + step < 0 || current + step > maximum - visible) {
                //$(nextBtn).hide();
                return;
            }
            else {
                if (current + step == maximum - 1) {
                    $(nextBtn).hide();
                }
                current = current + step;
                $(carouselId).animate({ left: -(liSize * current) }, speed, null);
            }
            $(prevBtn).show();
            return false;

        });
        /*Method to Trigger prev button*/
        $(prevBtn).click(function() {
            if (current - step < 0 || current - step > maximum - visible) {
                //$(prevBtn).hide();
                return;
            }
            else {
                if (current - step == 0) {
                    $(prevBtn).hide();
                }
                current = current - step;
                $(carouselId).animate({ left: -(liSize * current) }, speed, null);

            }
            $(nextBtn).show();
            return false;
        });
        /*Method to click li function*/
        $('' + ulID + ' li .cnt').click(function() {
            $('' + ulID + ' li').removeClass("active");
            $(this).parent().addClass("active");
            var dateText = $(this).html().toLowerCase();
            var datearr = dateText.split('</p>');
            var curDate = datearr[0].split('<p>');
            $("#yourStayMod05 .regular .cnt p.stockText").replaceWith('<p class="stockText"><strong>Stockholm</strong><br />' + curDate[1].trim() + '</p>');
        });
    },

    showColumn: function(rm) {

        /*$(".HiddenRows").hide();*/
        //$(".usrBtn a[rel='hideSome']").hide();
        $("#TabStructure .usrBtn a[rel='showAll']").click(function() {
            var TableObj = $(this).parent().attr('rel');
            $(TableObj + " .HiddenRows").show();
            $(TableObj + ".usrBtn a[rel='showAll']").hide();
            //$(TableObj + ".usrBtn a[rel='hideSome']").show();
        	
        	var txtShowAllRoomTypes = $fn(_endsWith("txtShowAllRoomTypes")).value;
        	txtShowAllRoomTypes = txtShowAllRoomTypes + TableObj + ",";
        	$fn(_endsWith("txtShowAllRoomTypes")).value = txtShowAllRoomTypes;
        	
        });

//        $("#TabStructure .usrBtn a[rel='hideSome']").click(function() {
//            var TableObj = $(this).parent().attr('rel');
//            $(TableObj + " .HiddenRows").hide();
//            //$(TableObj + ".usrBtn a[rel='hideSome']").hide();
//            $(TableObj + ".usrBtn a[rel='showAll']").show();
//        	
//        	
//        	var txtShowAllRoomTypes="";
//			var roomShowTypeArray = $fn(_endsWith("txtShowAllRoomTypes")).value.toString().split(',');
//			
//			for( var roomCount=0; roomCount < roomShowTypeArray.length - 1; roomCount++) {
//				if(roomShowTypeArray[roomCount] != TableObj)
//					txtShowAllRoomTypes = txtShowAllRoomTypes + roomShowTypeArray[roomCount] + ",";
//			}
//			
//        	$fn(_endsWith("txtShowAllRoomTypes")).value = txtShowAllRoomTypes;
//                
//        });



    },
    columnColoring: function() {

        $("#TabStructure .m15TabularData tr td").hover(function() {

            var className = $(this).attr('rel');

            var Index = $(this).index();
            if (Index !== 0) {

                $(this).parents('table').find('td:nth-child(' + ($(this).index() + 1) + ')').
                     add($(this).parent()).addClass('grayMe');

                $(this).parent().addClass('grayMe');
                //$(this).addClass(className).removeClass('grayMe');
                if (className == "Pink" || className == "PINK" || className == "pink") {
                    // R2.2 | artf1156100
                    $(this).css("cssText", "background: #c36 !important").removeClass('grayMe');
                }
                else if (className == "Grey" || className == "GREY" || className == "grey")//Res 2.0 -Parvathi : Grey is not working in IE 7.0
                {
                    // R2.2 | artf1156100
                    $(this).css("cssText", "background: #828282 !important").removeClass('grayMe');
                }
                else if (className == "Blue" || className == "BLUE" || className == "blue")//Vipul : for Branding
                {
                    $(this).css("cssText", "background: #218da3 !important").removeClass('grayMe');
                }
                else if (className == "Orange" || className == "ORANGE" || className == "orange")//Vipul : for Branding
                {
                    $(this).css("cssText", "background: #d17a33 !important").removeClass('grayMe');
                }
                else if (className == "Brown" || className == "BROWN" || className == "brown")//Vipul : for Branding
                {
                    $(this).css("cssText", "background: #614132 !important").removeClass('grayMe');
                }
                else {
                    // R2.2 | artf1156100
                    newClassName = "background: " + className + " !important";
                    $(this).css("cssText", newClassName);
                    $(this).removeClass('grayMe');
                }
                $(this).css('color', '#fff');
                $(this).find('a,input,label').addClass('whiteColor');


            }
            else { return; }
            if ($(this).hasClass('selected')) { $(this).removeClass('grayMe'); }
        },
         function() {
             var className = $(this).attr('rel');
             var Index = $(this).index();
             if (Index !== 0) {
                 $(this).parents('table').find('td:nth-child(' + ($(this).index() + 1) + ')').
                      add($(this).parent()).removeClass('grayMe');
                 $(this).parent().removeClass('grayMe');
                 // R2.2 | artf1156100
                 $(this).css("cssText", "background: none");
                 $(this).find('a,input,label').removeClass('whiteColor');
             }
             else { return; }
             if ($(this).hasClass('selected')) { $(this).removeClass('grayMe').addClass('selected') }
         });

        //trigger click event on cells expect first column cells
        $("#TabStructure .m15TabularData tr td.not(':eq(0)')").click(function() {

            var className = $(this).attr('rel');
            /*Method to flush previous selected column */
            scandic.flush();
            $(this).parents('table').find('td:nth-child(' + ($(this).index() + 1) + ')').
                add($(this).parent()).addClass('grayMe');
            $(this).parent().addClass('grayMe');

            if (className == "Pink" || className == "PINK" || className == "pink") {
                $(this).css("background", "#c36");
                $(this).removeClass('grayMe');
            }
            else if (className == "Grey" || className == "GREY" || className == "grey")//Res 2.0 -Parvathi : Grey is not working in IE 7.0
            {
                $(this).css("background", "#828282").removeClass('grayMe');
            }
            else {
                // R2.2 | artf1156100
                newClassName = "background: " + className + " !important";
                $(this).css("cssText", newClassName);
                $(this).removeClass('grayMe');
            }

        });

    },

    flush: function() {
        $('#TabStructure .m15TabularData tr').removeClass('grayMe');
        $('#TabStructure .m15TabularData tr td').removeClass('grayMe');
    },

    modifyRoom: function() {
        $("#yourStayMod05 div.regular div.cnt a").click(function() {
            var aVal = $(this).attr('rel');
            var chDet = $("#yourStayMod05 div.regular div.changedDetail");
            var moDet = $("#yourStayMod05 div.regular div.modifyDetail");
            var moBrd = $("#yourStayMod05 div.regular div.broadBrd");
	    var moEdityorStay = $('p[id*="editYourStayLink"]');
            if (aVal == 'changeQury') {
                chDet.hide();
                moDet.show();
                moBrd.hide();
            } if (aVal == 'gobackDet') {
                moDet.hide();
                chDet.show();
                moBrd.show();
                moEdityorStay.show();
            }

            if (aVal == 'gobackAdd') {
                $("div.broadBrd .cnt .addRoomLink").show();
                $('div.broadBrd .cnt .thinBrdContainer.grayBox').hide();

                //Parvathi:486102:Book Now button is getting displayed on clicking the add another room link
                //Parvathi:Add another room functionality is not working in modify booking flow
                if ($fn(_endsWith("bookNowBtnBlock")) != null) {
                    $fn(_endsWith("bookNowBtnBlock")).style.display = "block";
                }

                //Parvathi:562522:Shopping cart | Error message is remaining even the function is cancelled.
                var errorDiv = $fn(_endsWith('RegShoppingCartClientErrorDiv'));
                errorDiv.style.display = "none";
            }
        });
        //RK: Added new method to handle the add room click on edit stay shopping cart
        $(".addRoomBlock .addRoomLink a").click(function() {
            var noOfVisibleRooms = $('#regularBking .howManyRoom:visible').length;
            if (noOfVisibleRooms == 4) {
                return;
            }
            var nextRoom = noOfVisibleRooms + 1;
            //RK: Reservation 2.0 | artf1156036 | Room boxes were getting hidden when add new room 
            //$('#regularBking .howManyRoom').hide();
            $('#regularBking .howManyRoom:lt(' + nextRoom + ')').show();
            if (nextRoom > 1)
                $fn(_endsWith("noOfRoomsSelected")).value = nextRoom;
            //RK: If the 4th room is added, hide the add another hotel link
            if (nextRoom == 4) {
                $(".addRoomBlock .addRoomLink").hide();
            }
            //Parvathi:486102:Book Now button is getting displayed on clicking the add another room link
            //Parvathi:Add another room functionality is not working in modify booking flow
            if ($fn(_endsWith("bookNowBtnBlock")) != null) {
                $fn(_endsWith("bookNowBtnBlock")).style.display = "none";
            }
        });
    },
    removeRoom: function() {

        $(".thinBrdContainer .cnt .close").click(function() {

            //set hidden variable
            var removeRoomIndex = $(this).attr("rel");
            $fn(_endsWith("removeRoomIndexHiddenField")).value = removeRoomIndex;
            $('div.singleCol').css('position', 'relative');
            $('.topCntRemoveRoom').show();
            var getStayModuleHeight = $('#yourStayMod05 .regular').height() - 18;
            $('.topCntRemoveRoom .ratesOverLayCnt').css('height', getStayModuleHeight);
            scandic.removeBack();
        });
    },
    removeBack: function() {
        $('div.backgrd .removAlertBox .usrBtn .goBack').click(function() {
            $('div.singleCol #yourStayMod05 .topCntRemoveRoom').hide();
            $('div.singleCol').css('position', 'static');
        });
    },

    addRoom: function() {

        $("a[rel='addRoom']").click(function() {
            //RK: Commented below line as the BookNow button should be hidden only when the search button is clicked
            var lin = $('div.broadBrd .cnt .thinBrdContainer').length;
            var adLnk = $("div.broadBrd .cnt .addRoomLink");
            var bkno = lin + 1;
            $('.addRoomBlock .thinBrdContainer').show();
            adLnk.hide();
            var attr1 = 'room' + (lin + 1);
            var attr2 = 'roomcnt' + (lin + 1);
            var attr3 = (lin + 1);
            scandic.removeRoom();



        });
    },

    roomSelect: function() {
        $("select.roomSelect").change(function() {
            var selected = $(this).find(':selected');
            var divObj = $(this).attr('rel');
            var objValue = selected.val();
            $(divObj + ' .howManyRoom').hide();
            $(divObj + ' .howManyRoom:lt(' + objValue + ')').show();
            //making suer the dropdown selected value is in sync with hidden value
            var hdnVariable = $fn(_endsWith("noOfRoomsSelected"))
            if (hdnVariable != null) {
                hdnVariable.value = objValue;
            }
        });
    },

    chSelect: function() {
        $("select.child").change(function() {
            //scandic.genericChildBedTypeSelect();
            $(this).find(":selected").attr('selected', 'selected');
            var chid = $(this).find(":selected").val();
            var name = $(this).attr('rel');
            $("#chdBedWrapper").show();
            //hide colm5 of specific child module
            $('.' + name + " .colm5").hide();


            //var newStr='.'+name;
            if (chid > 0) {
                $('.' + name).show();
                $('.' + name + " .colm5:lt(" + chid + ")").show();

                //for bookingmodulebig
                $('.' + name + " .colm5:lt(" + chid + ") .widthMr").hide();

                //for bookingmodulesmall
                $('.' + name + " .colm5:(" + chid + ") [class*=bedoutput]").hide();

                //to start with, hide select bed type header
                if ($(this).parent().parent().siblings('.childSelct').children().children('.selHdr1').length > 0) {
                    $(this).parent().parent().siblings('.childSelct').children().children('.selHdr1').hide();
                }
                //R2.0 Bug:artf1153005 : Select bed type is displayed by default in the add another hotel flow. -Ashish
                else if ($(this).parent().parent().siblings('.childSelct').children().children().children('.selHdr1').length > 0) {
                    //$(this).parent().parent().siblings('.childSelct').children().children('.selHdr1').hide();
                    $(this).parent().parent().siblings('.childSelct').children().children().children('.selHdr1').hide();
                    //$(this).parent().parent().siblings('.childSelct').children().children().children('.selHdr1')
                }
                else if ($(this).parent().next().children().children('.selHdr1').length > 0) {
                    $(this).parent().next().children().children('.selHdr1').hide();
                }

                if ($(this).parent().parent().next().children().children('.colm5:visible').length > 0) {
                    $(this).parent().parent().next().children().children('.colm5:visible').each(
		               function() {
		                   var ageId = $(this).children('.subchild').find(":selected").val();
		                   if ((ageId != null) && !isNaN(ageId)) {
		                       $(this).children(".widthMr").show();
		                       $(this).siblings('.selHdr1').show();
		                   } //end of if
		                   else {
		                       $(this).children(".widthMr").hide();
		                   } //end of else
		               } //end of function
		           ); //end of each
                } //end of if // Vrushali | this change is for small booking module.
                else if ($(this).parent().next().children().children('.colm5:visible').length > 0) {
                    $(this).parent().next().children().children('.colm5:visible').each(
		               function() {
		                   var ageId = $(this).children('.subchild').find(":selected").val();
		                   if ((ageId != null) && !isNaN(ageId)) {
		                       $(this).children("[class*=bedoutput]").show();
		                       $(this).siblings('.selHdr1').show();
		                   } //end of if
		                   else {
		                       $(this).children("[class*=bedoutput]").hide();
		                   } //end of else
		               } //end of function
		           ); //end of each
                } //end of else if
                //R2.0 Bug:artf1153005 : Select bed type is displayed by default in the add another hotel flow. -Ashish
                else if ($(this).parent().parent().siblings('.childWrapper').children().children('.colm5:visible').length > 0) {
                    $(this).parent().parent().siblings('.childWrapper').children().children('.colm5:visible').each(
		                   function() {
		                       var ageId = $(this).children('.subchild').find(":selected").val();
		                       if ((ageId != null) && !isNaN(ageId)) {
		                           $(this).children("[class*=bedoutput]").show();
		                           $(this).siblings().children('.selHdr1').show();
		                       } //end of if
		                       else {
		                           $(this).children("[class*=bedoutput]").hide();
		                       } //end of else
		                   } //end of function
		               );
                }
            }
            else {
                $('.' + name).hide();
            }
            scandic.agSelct();
        });
    },
    agSelct: function() {
        $("select.subchild").change(function() {
            $(this).find(":selected").attr('selected', 'selected')
            var ageid = $(this).find(":selected").val();
            var name = $(this).attr('rel');
            var hdrToShow = false;
            if (isNaN(ageid)) {
                $(this).next().hide();

                $(this).parent().siblings().children('.subchild:visible').each(
	            function() {
	                if (!isNaN($(this).find(":selected").val())) {
	                    hdrToShow = true;
	                }
	            }
	        )//each ends here

                if (hdrToShow) {
                    $(this).parent().siblings('.selHdr1').show();
                }
                else {
                    $(this).parent().siblings('.selHdr1').hide();
                }
            }
            else {
                if (isNaN($('.' + name).find(":selected").val())) {
                    //populate only if no value selected previously
                    $('.' + name).html(populateDropDown(ageid));
                }
                $(this).next().show();
                $(this).parent().siblings('.selHdr1').show();
                //R2.0 Bug:artf1153005 : Select bed type is displayed by default in the add another hotel flow. -Ashish
                $(this).parent().parent().children().children().siblings('.selHdr1').show();
            }
        });
    },
    stayRoomsel: function() {
        $("select.stayRoomsel").change(function() {
            var selected = $(this).find(':selected');
            var output = "";

            for (var i = 1; i <= selected.val(); i++) {
                output += '<div class="roundMe grayBox M05B"><div class="colmMerg fltLft roomcnt' + i + '"><div class="colm formColmChn fltLft"><label for="rooms">Rooms ' + i + '</label></div><div class="colm1"><label for="adult">Adults</label><select id="adult' + i + '" name="child"><option value="1">1</option><option value="2">2</option></select></div><div class="colm2"><label for="child">Children</label><select  name="child' + i + '" class="child' + i + '" onchange="childSect(' + i + ')"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select></div><div  class="childSelct choutput' + i + '"></div><div class="selHdr' + i + '"  style="display:none;">Select bedtype</div></div></div>';
            }

            $("div.stayRoomput").html(output);
            scandic.toolTip();
        });
    }

};

$(function(){
//RK: Reservation 2.0 | Adding click event on the bedtype radio button 
//so that prompt is closed on click of any related radio buttons
    $('.bedTypeList input').click(function(){
       var mandatoryObjs = $('.bedTypeList input.mandatory');
       var clickedObjName = this.name;
           mandatoryObjs.each(function(index){
               if(clickedObjName == this.name)
               {
                   $.validationEngine.isError = false;
                   $(this).removeClass("mandatory");
                   $.validationEngine.closePrompt(this);  
               }
           });     
    });

//RK: Reservation 2.0 | Adding click event on the radio button 
//so that prompt is closed on click of any related radio buttons
    $('.selectGender').click(function(){
       var mandatoryObjs = $('.selectGender.mandatory');
       mandatoryObjs.each(function(index){
           $.validationEngine.isError = false;
           $(this).removeClass("mandatory");
           $.validationEngine.closePrompt(this);  
        });
    });
});

//RK: Release 2.0 | Enroll User | Method to take care of setting value from one object to the other
function SetValue(fromObject, toObject)
{
    toObject.value = (fromObject != null && $(fromObject).attr('rel') !=  fromObject.value) ? fromObject.value : toObject.value;
}

function notify(message, timeOut) {
	$('.tableData .topCnt').show();
//	setTimeout(function(){
//		$('.topCnt').fadeOut();
//	}, timeOut*3000);
}
function bordfy(m, timeOut){

$('#roomWrapper .thinBrdContainer').eq(m).addClass('blueBorder')
	//$("div.broadBrd .roomcnt"+m+"").addClass('blueBorder');
	setTimeout(function(){
	$('#roomWrapper .thinBrdContainer').eq(m).removeClass('blueBorder')
		//$("div.broadBrd .roomcnt"+m+"").removeClass('blueBorder');
	}, timeOut*3000);
}

function jm_phonemask(t)
{
    var patt1 = /(\d{3}).*(\d{3}).*(\d{4})/;
    var patt2 = /^\((\d{3})\).(\d{3})-(\d{4})$/;
   
    var str = t.value;   
    var result;
   
    if (!str.match(patt2))
    {
        result = str.match(patt1);
       
        if (result!= null)
        {
               }
        else
        {
            if (t.value.match(/[^\d]/gi))
            t.value = t.value.replace(/[^\d]/gi,'');
        }
    }
}


$(document).ready(function () {


//$('.hotelInfoCnt .jqModal').live("click",function(e){
$('.hotelInfoCnt .jqModal').live("click",function(e){
var relTagValue=$(this).attr('rel'); //RK: Modified the method to pass the rel value directly instead of passing the object
e.preventDefault();
createImageGallery(relTagValue);
});

	$('.tripReviewcount').live("click", function(e) {
        e.preventDefault();	
		var that = $(this);
		$('#tripAdvisorIframe').empty(); 	
		var hrefVal = $(this).attr('href');	
		TrackTripAdvisor(hrefVal);
		$("#tripAdvisorIframe").append($("<iframe />").attr({src : hrefVal, width : '666px', height : '700px', frameborder : '0'}));				
		$('#tripAdvisoModal').jqmShow();	
    });
	$('.tripReviewcount').click(function(e) {
        e.preventDefault();	
		var that = $(this);
		$('#tripAdvisorIframe').empty(); 	
		var hrefVal = $(this).attr('href');	
		TrackTripAdvisor(hrefVal);
		$("#tripAdvisorIframe").append($("<iframe />").attr({src : hrefVal, width : '666px', height : '700px', frameborder : '0'}));				
		$('#tripAdvisoModal').jqmShow();	
    });

    $('.hotelInfoCnt .jqModal,.topImgLink .jqModal, .LinkListItem .jqModal, .LinkContent .jqModal').click(function(e) {
        var relTagValue = $(this).attr('rel'); //RK: Modified the method to pass the rel value directly instead of passing the object
        e.preventDefault();
        createImageGallery(relTagValue);
    });

scandic.init();	
    $('.dialog').jqm();
    $('.aboutOurRate .room1').show();
    //Cufon.replace("#footerWrapper h3.title,#yourStayMod05 h3.title,h2.stoolHeading,h3.stoolHeading,#hotels-header-title h1,#hotels-header-title h3");
    // Commented the cufon code for Branding Project - Vipul Patel  */ 
});
 
//RK: Modified the method to accept the rel value directly instead of getting the object and then using the value
function createImageGallery(imageUrl){
$('.dialog').jqm(); //show overlay dilog
$('.dialog').jqmShow(); //show overlay during ajax call

//reset window
$('#hotelDetailContainer .bedTypeListclass').empty();
$('#imgGallery ul.tabsCnt li a').removeClass('active');
//Ajax call for fetching Image Gallery data
$.ajax({
            type:"GET",
            url:imageUrl,
            dataType:"text",
            data:"",
            success: function(msg){  
              
          //            $('#hotelDetailContainer .bedTypeListclass').empty();
//            $('#imgGallery ul.tabsCnt li a').removeClass('active');
            //$('#imgGallery ul.tabsCnt li a').eq(0).addClass('active');
            $('#imgGallery li a').removeClass('active');
            $('#imgGallery li a').eq(0).addClass('active');
            $('.bedTypeListclass').empty();
            $('.bedTypeListclass').append("<div id='galleryLoadingContent'>Loading Image Gallery ...</div>");           
            $('.bedTypeListclass').append(msg);//Binding the gallery response
            $('#galleryLoadingContent').remove();  
             //R2.2 - Bhavya - Artf1157465 - Image gallery |360 degree tab displayed even if image not available.    
             $('#imgGallery').show();
             var image360Count = $('#image360Count').val();
             var imageCount = $('#imageCount').val();          
           
              if (imageCount == 0 || imageCount == '')
              {
                 $('#imgGallery li').eq(0).hide();
              }
              else
              {
                 $('#imgGallery li').eq(0).show();
                 $('#imgGallery li a').eq(0).addClass('active');
              }
              if (image360Count == 0 || image360Count == '')
              {
                 $('#imgGallery li').eq(1).hide();
              }
              else
              {
                 $('#imgGallery li').eq(1).show();
                 if(!$('#imgGallery li a').eq(0).hasClass('active'))
                 {
                    $('#imgGallery li a').eq(1).addClass('active');
                 }
              }
            scandic.carsouel('#bedTypeList','.LightBoxCntnt','.m_NextBtn','.m_PrevBtn',1,1,474,469); 
            scandic.carsouel('#bedTypeList1','.LightBoxCntnt','.m_NextBtnGallery','.m_PrevBtnGallery',1,1,474,469);
            scandic.imgGalleryHide(); //hide overlay on close icon click
            }
        });
        
}


function CaptureBedTypes()
{
    var BTS = "";
    var RC = "";
    var CD = "";
    var currentaccordian = $fn(_endsWith("sT")).value;
    //Find which Tab is selected.
    if(currentaccordian == 'Tab1')
    {
        RC = $fn(_endsWith("ddlNoOfRoomsReg")).value;
        CD = "ddlChildPerRoom";
        BT = "bedTypeforRoom";
    }
    else if(currentaccordian == 'Tab2')
    {
        RC = $fn(_endsWith("ddlNoOfRoomsBC")).value;
        CD = "ddlBCChildPerRoom";
        BT = "ddlBCBedTypeforRoom";
    }
    else if(currentaccordian == 'Tab3')
    {
        if($fn(_endsWith("ddlNoOfRoomsRed"))!=null)
        {
            RC = $fn(_endsWith("ddlNoOfRoomsRed")).value;
            CD = "ddlRNChildPerRoom";
            BT = "ddlRNBedTypeforRoom";
        }
    }
    for(var count=1;count<=RC;count++)
    {
        var childCount = $fn(_endsWith(CD+ count)).value;
        for(var CC=1;CC<=childCount;CC++)
        {
            var BTC = BT+count+"Child"+CC;
            BTS += BTC+","+$fn(_endsWith(BTC)).value+"|";
        }
    }
    $fn(_endsWith('bedTypeCollection')).value = "";
    $fn(_endsWith('bedTypeCollection')).value = BTS;    
}


//artifact-1179008 -Rajneesh-Scanweb - Problem with reservation regarding free nights-Rajneesh
function CaptureBedTypesForShopingcartEdit()
{
    var BTS = "";
    var RC = "";
    var CD = "";
    
    var currentaccordian = $fn(_endsWith("sT")).value;
    //Find which Tab is selected.
    
    if(currentaccordian == 'Tab1')
    {
        RC = $fn(_endsWith("ddlNoOfRoomsReg")).value;
        CD = "ddlChildPerRoom";
        BT = "bedTypeforRoom";
    }
    else if(currentaccordian == 'Tab2')
    {
        RC = $fn(_endsWith("ddlNoOfRoomsBC")).value;
        CD = "ddlBCChildPerRoom";
        BT = "ddlBCBedTypeforRoom";
    }
    else if(currentaccordian == 'Tab3')
    {                     
        
       if($fn(_endsWith("ddlNoOfRoomsReg"))!=null)
        {
            RC = $fn(_endsWith("ddlNoOfRoomsReg")).value;
            CD = "ddlChildPerRoom";
            BT = "bedTypeforRoom";
        }
    }
    for(var count=1;count<=RC;count++)
    {
        var childCount = $fn(_endsWith(CD+ count)).value;
        for(var CC=1;CC<=childCount;CC++)
        {
            var BTC = BT+count+"Child"+CC;
            BTS += BTC+","+$fn(_endsWith(BTC)).value+"|";
        }
    }   
    $fn(_endsWith('bedTypeCollection')).value = "";
    $fn(_endsWith('bedTypeCollection')).value = BTS;    
    }
//R2.0 Bug Id: 499942-Ashish Need to change because in this case we do not have ddl for no of rooms
function CaptureBedTypesShoppingCart()
{
    var BTS = "";
    var RC = "";
    var CD = "";
    var currentaccordian = $fn(_endsWith("sT")).value;
    var countOfRoom=0;
    for(var roomCount=1;roomCount<5;roomCount++)
    {
    //var VarString='room'+roomCount
     if($fn(_endsWith('room'+roomCount)).style.display == 'block')
      {
        countOfRoom=roomCount;
      }
    }
    
    //Find which Tab is selected.
    if(currentaccordian == 'Tab1')
    {
        //RC = $fn(_endsWith("ddlNoOfRoomsReg")).value;
        CD = "ddlChildPerRoom";
        BT = "bedTypeforRoom";
    }
    else if(currentaccordian == 'Tab2')
    {
        //RC = $fn(_endsWith("ddlNoOfRoomsBC")).value;
        CD = "ddlBCChildPerRoom";
        BT = "ddlBCBedTypeforRoom";
    }
    else if(currentaccordian == 'Tab3')
    {
        //RC = $fn(_endsWith("ddlNoOfRoomsRed")).value;
        CD = "ddlRNChildPerRoom";
        BT = "ddlRNBedTypeforRoom";
    }
    
    for(var count=1;count<=countOfRoom;count++)
    {
        var childCount = $fn(_endsWith(CD+ count)).value;
        for(var CC=1;CC<=childCount;CC++)
        {
            var BTC = BT+count+"Child"+CC;
            BTS += BTC+","+$fn(_endsWith(BTC)).value+"|";
        }
    }
    $fn(_endsWith('bedTypeCollection')).value = "";
    $fn(_endsWith('bedTypeCollection')).value = BTS;
}
 
//RK: Reservation 2.0 | artf1150346 |Added new function to get the Next Room which can be focussed on the select rate
function getNextRoomToFocus(roomNo)
{
  //Picking only the count of rooms which are modifiable
  var totalRooms = $("#TabStructure ul.tabsCnt li a.tabs").not('.notModifiable').length;
  var nextRoom = -1 ;//= parseInt(roomNo)+1;
  var selectedRooms = $('.RRWrapper input[@type=radio]:checked="checked"').length;
  if (selectedRooms == totalRooms)
  {
       //RK: Add this condition as in case user removed a room, it should return the last room selected
       if( isNaN(roomNo) || roomNo == null || roomNo == '' )
       {
            var arrayOfTabs = $("#TabStructure ul.tabsCnt li a.tabs");
            var arrayOfmodifiableElements = $("#TabStructure ul.tabsCnt li a.tabs").not('.notModifiable').eq(0);
            var indexOfModifiable = $(arrayOfTabs).index(arrayOfmodifiableElements);
            if(indexOfModifiable != null || indexOfModifiable == "")
            {
                nextRoom = indexOfModifiable+1; //index starts with 0 and room starts with 1 so incremented to 1
            }
            else
            {
            nextRoom = totalRooms;
       }
       }
       else
       {
            nextRoom = 0;
       }     
  }
  else
  {
        var nextRoom = findUnSelectedRoom(roomNo)
        if (nextRoom == -1) //RK: If the room not found, start from begining
        {
            nextRoom = findUnSelectedRoom(0)
        }
        
  }
  return nextRoom;
}

//RK: Reservation 2.0 | Below method will search for room which is greater than the currentRoomNo passed
// and for which rate has not been selected
function findUnSelectedRoom(currentRoomNo)
{
    var roomNumber = 0;
    var unSelectedRoom = -1;    
    //Get the room for which no rate has been selected by looping through all the room tabs.
   $('.RRWrapper .tabCntnt').each(function () {
        var divId = this.id;
        var chkLen = $('#'+divId+' input[@type=radio]:checked="checked"').length;
        roomNumber = roomNumber+1;
        var isNonModifiable = $('.RRWrapper .room'+roomNumber).find('label').hasClass('nonEditable');
        //Added condition to check if the room is nonmodifiable.
        if ( (chkLen == null || chkLen < 1 ) && roomNumber >= currentRoomNo && isNonModifiable == false )
        {
            unSelectedRoom = roomNumber;
            return false; //RK: This will break the loop
        }
    });
    return unSelectedRoom;
}


//RK: Reservation 2.0| Modified the below method for change in functionality of overlay-artf1150346
function selectRatemoveRoom(roomNo, delayNeeded, isFromPageLoad){

 //RK: Reservation 2.0 | artf1162308 & artf1162300|Making sure that when room 
 //is unavailable, the user is not redirected to other tab and the rate is not shown as selected 
 //Check if the room is unavailable, if yes, then dont proceed forward
 var errorDiv = $fn(_endsWith('errorDiv'));
 
 //Res2.2.8 | Artifact artf1231383 : Scanweb - Grey box is displayed twice on Select Room and rate 
 //Added this condtion to block the display of the overlay twice.
 if(isFromPageLoad)
 {
     if(errorDiv != null && $(errorDiv).is(":visible"))
     {
        //RK: The below code should be executed only if it is from page load as errordiv is shown/hidden from server
        //Res2.2.8 | Artifact artf1231383 : Scanweb - Grey box is displayed twice on Select Room and rate 
        //Below if Condition Commented.
        //if(isFromPageLoad)
        //{
            //$('#TabStructure ul.tabsCnt li a').removeClass('active');
            $('#TabStructure ul.tabsCnt li a.tabs').eq(roomNo).addClass('active');
            $('#TabStructure .room1').hide();
            var visibleRoom = '.room'+(parseInt(roomNo)+1)
            $(visibleRoom).show();
            //Get the user selected radio button
            var checkedOption = $('#TabStructure '+visibleRoom+' .tableData input[type=radio]:checked');
            checkedOption.attr('checked',false);//RK: Reset the radio button
            return;
         //}   
 }
 
 var tlen = $("#TabStructure ul.tabsCnt li a.tabs").length;
 //RK: Adding this condition for when the edit stay, the roomNo is set on radiobutton select event and
 // while edit stay, it doesnt get set fired. Due to this, the roomNo remains what was set before
 var editFlow = false;
 //RK: Passing roomNo +1 here as the roomNumber starts with zero but the div id starts with 1
 var nextRoom = getNextRoomToFocus(parseInt(roomNo)+1);
 //RK: If the nextRoom is 0 and there are more than 1 room, it indicates rate for all the rooms has been selected
 if(nextRoom == 0 && tlen >1)
 {
    $('#TabStructure .RRWrapper .topCntOverLay').show();
    delay(true, delayNeeded);
    $('#TabStructure ul.tabsCnt li a').removeClass('active');
    $('#TabStructure ul.tabsCnt li a.tabs').eq(roomNo).addClass('active');
    $('#TabStructure .room1').hide();
    $('.room'+(parseInt(roomNo)+1)).show();
    return;
 }
 //RK: Doing -1 below as the nextroom will indicate the room to be which is again not a zero indexed as the tab
 var nextTab = nextRoom-1;
// if((parseInt(roomNo)+1) > tlen)
// {
//    roomNo = "";
//    editFlow = true;
// }
 $('#TabStructure .room1').hide();
 
 if(tlen >1)
 {
    //
    if(roomNo == null || roomNo=="")
    {
//       $('#TabStructure .room1').show();
//       $('#TabStructure ul.tabsCnt li a').eq(0).addClass('active');

        //RK: In case the roomNo is Not a Number, it indicates its a fresh search(Add a room). In such a case,
        // the user should be sent to the tab where the rate is not selected.Hence, passing always a higher number
        // to get the next selectable room
         if( isNaN(nextRoom) || nextRoom == '' || nextRoom == 0 )
         {
            nextRoom = getNextRoomToFocus(7);
         }   
         nextTab = nextRoom-1;
         $('#TabStructure .room'+nextRoom).show();
         $('#TabStructure ul.tabsCnt li a').eq(nextTab).addClass('active');
        //$('.RRWrapper .topCntOverLay').css('display','none');
    }
    else
    {
        // Logic: Check if the roomNo lies between the max and min tab, only then show overlay
        //if( ( parseInt(roomNo)+1 ) < tlen && roomNo >= 0){
        if( nextRoom > 0 ){
            $('#TabStructure .RRWrapper .topCntOverLay').show();
            //defect id: artf1150702 added delay switch
            delay(false, delayNeeded);
        }
        
//        if(roomNo< tlen-1)
//        {
//            roomNo=parseInt(roomNo)+1;
//        }    
        
        $('#TabStructure ul.tabsCnt li a').removeClass('active');
       
        var nextRoomObj;
        var prevRoomObj;
        if (editFlow == true)
        {
            nextRoomObj = parseInt(roomNo);
            prevRoomObj = parseInt(roomNo-1);
        }   
        else
        {
            nextRoomObj =nextRoom;
            prevRoomObj =parseInt(nextRoom)-1;
        }
        if(nextRoom > 0)
        {
            $('#TabStructure ul.tabsCnt li a.tabs').eq(nextTab).addClass('active');
        }
         
       $('.room'+nextRoomObj).show();
       $('.room'+prevRoomObj).hide();
    }
 }
 else
 {
    if ( isNaN(roomNo) || roomNo == '' )
    {
        $('.room1').show();
    }   
    else
    {
        var divId  = $('.RRWrapper .tabCntnt').attr("id");
        var chkLen = $('#'+divId+' input[@type=radio]:checked="checked"').length;
        if ( chkLen != null && chkLen >0 )
        {
            $('#TabStructure .RRWrapper .topCntOverLay').show();
            delay(true, delayNeeded);
        }    
        $('.room1').show();    
    } 
    return;
 }
   }
}

//RK: Reservation 2.0 | Changed the signature of below method as now we need to show message based on
// if its a last room or not. We dont need to show room number in the message
//function delay(roomIndex, delayNeeded)
function delay(isLastRoom, delayNeeded)
{
    //$('#selectedRoom').text("Room"+(parseInt(roomIndex)+1)+" selected.");
    //$('#nextRoom').text("Room"+(parseInt(roomIndex)+2)+": Please select rate and type.");
//    var totalRooms = $("#TabStructure ul.tabsCnt li a.tabs").length;
//    var selectedRooms = $('.RRWrapper input[@type=radio]:checked="checked"').length;
    var msgHeader;
    var selectionMessage;
    if(isLastRoom)
    {
        msgHeader = $('#lastRateHeaderMsg').val();
        selectionMessage = $('#lastRoomMessage').val();
    }
    else
    {
        msgHeader = $('#selectedID').val();
        selectionMessage = $('#nextRoomMessage').val();
    }
    
    $('#selectedRoom').text(msgHeader);
    $('#nextRoom').text(selectionMessage);
    
//    $('#selectedRoom').text($('#roomID').val()+(parseInt(roomIndex)+1)+" "+ $('#selectedID').val());

//    //$('#nextRoom').text($('#roomID').val()+(parseInt(roomIndex)+2)+": "+ $('#nextRoomMessage').val());
//    $('#nextRoom').text($('#nextRoomMessage').val() +" "+(parseInt(roomIndex)+2));

    if (delayNeeded == true)
    {
        setTimeout(function(){$('#TabStructure .RRWrapper .topCntOverLay').hide()}, 5000);
    }
}

function ShowAppropriateAccordian(bookingModule,someval)
{   
    if(someval == 'Tab1')
    {
        scandic.accordian(bookingModule,0);
       
    }
    else if(someval == 'Tab2')
    {
        scandic.accordian(bookingModule,1);
     
    }
    else if(someval == 'Tab3')
    {
        scandic.accordian(bookingModule,2);
    }
    else if(someval == 'Tab4')
    {
        scandic.accordian(bookingModule,3);
    }
}
function getPrinter(val)
{
	window.open(val,null,"height=600,width=770,status=yes,scrollbars=1,toolbar=no,menubar=no,location=no");
}

////supressRightMouseClick
function handleRightMouseClick(event) {

if (event.button == 2)
    {
        return false;
    }
    else
    {
        return true;
    }

}


function OpenPringDialog()
{
    window.print();
}
//RK: Reservation 2.0 | ID#465382 | Added new method for Updating Credit Card details for other rooms
function updateCreditCardDetails(){

    //Parvathi | Res2.1 | CR - Modify combo reservation. 
    var updateCreditCard= $fn(_endsWith('updateCreditCard')); 
    $(updateCreditCard).click(function(){
        var checkedVal = this.checked;
        var roomCount  = $('.bookingDetailWrapper .depositInfo').length;
            
        if(checkedVal)
        {
            var txtRoom0CardHolder = $fn(_endsWith('RoomInformation0_txtCardHolder'));
            var txtRoom0CardNumber = $fn(_endsWith('RoomInformation0_txtCardNumber'));
            var ddlRoom0CardType = $fn(_endsWith('RoomInformation0_ddlCardType'));
            var ddlRoom0ExpiryMonth = $fn(_endsWith('RoomInformation0_ddlExpiryMonth'));
            var ddlRoom0ExpiryYear = $fn(_endsWith('RoomInformation0_ddlExpiryYear'));   
            //RK: Starting the loop from 1 as the checkbox
            for(var i = 1;i<=roomCount-1; i++)
            {
                updateCreditCardForRoom(i,txtRoom0CardHolder.value,txtRoom0CardNumber.value,ddlRoom0CardType.value,ddlRoom0ExpiryMonth.value,ddlRoom0ExpiryYear.value,true)
            }
        }
        else
        {
            for(var i = 1;i<=roomCount-1; i++)
            {
                updateCreditCardForRoom(i,null,null,null,null,null,false)
            }
        }
    });
}
//RK: Reservation 2.0 | ID#465382 | Added new method for Updating Credit Card details for other rooms
// Will update the details based on parameters passed
function updateCreditCardForRoom(roomNumber,cardHolder,cardNumber,cardType,expiryMonth,expiryYear,copyGuaranteeDetails)
{
    showGuaranteeInfo(roomNumber,copyGuaranteeDetails);
    var txtCardHolder = $fn(_endsWith('RoomInformation'+roomNumber+'_txtCardHolder'));
    var txtCardNumber = $fn(_endsWith('RoomInformation'+roomNumber+'_txtCardNumber'));
    var ddlCardType = $fn(_endsWith('RoomInformation'+roomNumber+'_ddlCardType'));
    var ddlExpiryMonth = $fn(_endsWith('RoomInformation'+roomNumber+'_ddlExpiryMonth'));
    var ddlExpiryYear = $fn(_endsWith('RoomInformation'+roomNumber+'_ddlExpiryYear'));
    if (txtCardHolder == null) return;
    
     if(!$(txtCardHolder).attr("disabled"))
     {
    //RK: If the guarantee details are to be copied, set the credit card related fields
    if(copyGuaranteeDetails)
    {
        txtCardHolder.value = cardHolder;
        txtCardNumber.value = cardNumber;
        ddlCardType.value = cardType;
        ddlExpiryMonth.value = expiryMonth;
        ddlExpiryYear.value = expiryYear;
    }
    else //RK: Else, set back the default fields
    {
        txtCardHolder.value = $(txtCardHolder).attr('rel');
        txtCardNumber.value = $(txtCardNumber).attr('rel');
        ddlCardType.value = $(ddlCardType).val(0);
        ddlExpiryMonth.value = $(ddlExpiryMonth).val(0);
        ddlExpiryYear.value = $(ddlExpiryYear).val(0);
    }    
}
}
//RK: Reservation 2.0 | ID#465382 | Added new method for Updating Credit Card details for other rooms
//Shows or hides the guarantee info radio button
function showGuaranteeInfo(roomNumber,copyGuaranteeDetails)
{
    var guaranteeRadio = $fn(_endsWith('RoomInformation'+roomNumber+'_rdoLateArrivalGurantee'));
    //RK: If the guarantee info radio button is available, show it, else , hide it
    if(guaranteeRadio != null && copyGuaranteeDetails)
    {
        guaranteeRadio.checked = true;
        
        if ($fn(_endsWith("RoomInformation" + roomNumber + "_panHashCreditCardDiv")) != null)
            $fn(_endsWith("RoomInformation" + roomNumber + "_panHashCreditCardDiv")).style.display = "block";
            
        $('div.bookingDetailWrapper div.depositInfo .Room'+roomNumber).show();
    }
    else if(guaranteeRadio != null && !copyGuaranteeDetails)
    {
        guaranteeRadio.checked = false;

        if ($fn(_endsWith("RoomInformation" + roomNumber + "_panHashCreditCardDiv")) != null)
            $fn(_endsWith("RoomInformation" + roomNumber + "_panHashCreditCardDiv")).style.display = "none";
        
        $('div.bookingDetailWrapper div.depositInfo .Room'+roomNumber).hide();
        $fn(_endsWith('RoomInformation'+roomNumber+'_rdoHoldRoom')).checked = true;
    }
    
}

function updateCreditCardPanHashDetails() {

    var updateCreditCardPanHash = $fn(_endsWith('updateCreditCardPanHash'));

    $(updateCreditCardPanHash).click(function() {
        var checkedVal = this.checked;
        var roomCount = $('.bookingDetailWrapper .depositInfo').length;

        if (checkedVal) {
            var ddlRoom0PanHashCreditCards = $fn(_endsWith('RoomInformation0_ddlPanHashCreditCards'));

            for (var i = 1; i <= roomCount - 1; i++) {
                var ddlPanHashCreditCardsOtherRooms = $fn(_endsWith('RoomInformation' + i + '_ddlPanHashCreditCards'));
                
                if(ddlPanHashCreditCardsOtherRooms != null)
                    ddlPanHashCreditCardsOtherRooms.value = ddlRoom0PanHashCreditCards.value;

                showGuaranteeInfo(i, true);
            }
        }
        else {
            for (var i = 1; i <= roomCount - 1; i++) {
                var ddlPanHashCreditCardsOtherRooms = $fn(_endsWith('RoomInformation' + i + '_ddlPanHashCreditCards'));
                
                if(ddlPanHashCreditCardsOtherRooms != null)
                    ddlPanHashCreditCardsOtherRooms.value = $(ddlPanHashCreditCardsOtherRooms).val(0);
                    
                showGuaranteeInfo(i, false);
            }
        }
    });
}

//Release R2.0 - Bhavya - This method binds teh tab out event to the active accordians NO of nights.
function bindTabOutToNoOfNights(){

	Nights_Id = (Nights_Id == "") ? null : Nights_Id;
	if(Nights_Id != null){
	    $('#'+Nights_Id).bind("blur", function() {	
		    var NightsVal = $.trim($('#'+Nights_Id).val());
		    setDepDate(NightsVal);	
		    return false;
	    });		
	}
	}
	var maxNights = 99;
	var localmaxDates;
	if (typeof (maxDates) == 'undefined') //maxDates is defined in Header.ascx. maxDates will not be available in iFrame.
	    localmaxDates = parent.maxDates;
	else
	    localmaxDates = maxDates;
		//'serverDate' is already defined in Header.ascx which gives the date object based on server macine.
	var serDate = new Date(serverDate) || new Date();
		//Set the Departure date value
	//Release R2.0 - Bhavya - This method sets the departure date based the no of nights enterd
	function setDepDate(NightsVal){
	    setMinMax();
		//var NoOfNightsVal = $('#'+Nights_Id).val();
		var arrDateVal = $('#'+ArrDate_Id).val();
		var depDateVal = $('#'+DepDate_Id).val();		
		var arrDateArray = arrDateVal.split("/");
		var aDateLen = arrDateArray[0].length;
		arrDateArray[0] = arrDateArray[0].substr(aDateLen-2,aDateLen);
		var tDate = new Date(arrDateArray[2],arrDateArray[1]-1,arrDateArray[0]);	
		tDate.setDate(tDate.getDate()+parseInt(NightsVal));		
		
		if (isPositiveInt(NightsVal) && arrDateVal!="" ) {
			if (NightsVal != '0' && NightsVal != '') {		
				if (isValidDepDate(tDate)){
					$('#'+DepDate_Id).val(formatDateAsString(tDate));
				}	else {
					var validNights = diffDays(formatDateAsString(maxDepDate), $('#'+ArrDate_Id).val());	
					//alert(validNights);
					$('#'+DepDate_Id).val(addDays(arrDateVal, validNights));
					$('#'+Nights_Id).val(validNights);	
				}				
			} else 	if(NightsVal > maxNights) {							
				$('#'+DepDate_Id).val(addDays(arrDateVal, maxNights));
				$('#'+Nights_Id).val(maxNights);
			} else {
				$('#'+DepDate_Id).val(addDays(arrDateVal, minNights));
				$('#'+Nights_Id).val(minNights);									
			}
		} else if (arrDateVal!="") {
			$('#'+DepDate_Id).val(addDays(arrDateVal, minNights));
			$('#'+Nights_Id).val(minNights);									
		}		
	}
	
	
	//set the Min/Max value for Departure date.
	function setMinMax(){
		if($('#'+ArrDate_Id).val() != ""){
				var arrDateVal = $('#'+ArrDate_Id).val();
				var arrDateArray = arrDateVal.split("/");
				var dateLen = arrDateArray[0].length;
				var noOfNightsValue = 1;
				arrDateArray[0] = arrDateArray[0].substr(dateLen-2,dateLen);
				var actualArrDate = new Date(arrDateArray[2],arrDateArray[1]-1,arrDateArray[0]);
				minDepDate = new Date(arrDateArray[2],arrDateArray[1]-1,arrDateArray[0]);
				//Vrushali | Res2.0 | Defect Fix : 409172 - Room selection is not working in Bonus cheques section
				var noOfNights_Id = $fn(_endsWith("txtnoOfNights"));
				if(noOfNights_Id != null)
				 noOfNightsValue = actualArrDate.getDate() + parseInt(noOfNights_Id.value);
				//Vrushali | Res2.0 | Defect Fix : 409172 - Room selection is not working in Bonus cheques section
				//minDepDate.setDate(actualArrDate.getDate()+1);
				minDepDate.setDate(noOfNightsValue);
				
				var tMaxDepDate = new Date(arrDateArray[2],arrDateArray[1]-1,arrDateArray[0]);
				tMaxDepDate.setDate(actualArrDate.getDate()+parseInt(maxNights)); //Actual Arrival Date + 99						
				
				maxDepDate= new Date(serDate);
				maxDepDate.setDate(maxDepDate.getDate() + parseInt(localmaxDates)); 
				
				if(Date.parse(tMaxDepDate) < Date.parse(maxDepDate)){								 				
						maxDepDate = tMaxDepDate;				
				}	
		}
	}
//Returns true if the given date object is less than or equal to Max.deparurure date
	function isValidDepDate(date){		
		if (Date.parse(date) <= Date.parse(maxDepDate))
		{
			return true;
		}	else {
			return false;
		}
}

/*
Function to Handle the Bed Type Error
Author: Sheril Jebasingh
Version: R2.3.1
*/
function selectBedTypeError(noOfRoom, noOfChild1, noOfChild2, noOfChild3, noOfChild4, outputContainer, selectFieldText){
  // Hide Select BedType label                 
  $('.selHdr1').hide();
  // Setting Child's default age to "Select Age"
  $('.subchild').attr("value", selectFieldText);


    for (roomNo=1; roomNo<=noOfRoom; roomNo++){
        var noOfChild = eval("noOfChild"+roomNo);
        
        // Container Name for Children Detail
        var containerName = outputContainer+roomNo;
        
        // Initially hide all the Select Age Details
        $('.'+containerName+" .colm5").hide();
        
       // Showing the required Child Info
       if(noOfChild >0){
           $('.'+containerName).show();
           $('.'+containerName+' .colm5:lt('+noOfChild+')').show();
           
           // Only for Small Booking Module
           $('.'+containerName+' .colm5 [class*=bedoutput]').hide();
       }
       $('.'+containerName+ ' .widthMr').hide();
    }
}

function setChildrenDetails(noOfRoom, noOfChild1, noOfChild2, noOfChild3, noOfChild4, outputContainer, selectFieldText){

    // Hide Select BedType label    
	var	outputContainerArray =  new Array("choutput","choutputBC","choutputRN");
	
	// Clearing all the values for other tabs
	for(var containerCount=0;containerCount<outputContainerArray.length;containerCount++)
	{
		if(outputContainerArray[containerCount]==outputContainer)
			continue;
		var containerChildAge = "";
		var containerAdultChildCount = "";
		var containerRoomCount = "";
		switch(outputContainerArray[containerCount]) {
		    
		    case "choutput": 
		        containerChildAge = "c";
			    containerAdultChildCount="";
			    containerRoomCount="Reg";
		    	break;
		    
		    case "choutputBC":
		    	containerChildAge = "ddlBCC";
			    containerAdultChildCount="BC";
			    containerRoomCount="BC";
		    	break;
		    	
		    case "choutputRN":
		    	containerChildAge = "ddlRNC";
			    containerAdultChildCount="RN";
			    containerRoomCount="RN";
		    	break;
		}
			
		$($fn(_endsWith('ddlNoOfRooms'+containerRoomCount))).val("1");
		
		for(var roomNo=1;roomNo<=4;roomNo++)
		{
			var containerName = outputContainerArray[containerCount]+roomNo;
			$('.'+containerName+ ' .widthMr').hide();
			$($fn(_endsWith('ddl'+containerAdultChildCount+'ChildPerRoom'+roomNo))).val("0");
			$($fn(_endsWith('ddl'+containerAdultChildCount+'AdultsPerRoom'+roomNo))).val("1");
			
			for(var childNo=1;childNo<=5;childNo++)
			{
				$($fn(_endsWith(containerChildAge+'hildAgeforRoom'+roomNo+'Child'+childNo))).attr("value", selectFieldText);
			}
		}
		
	}
	
	var containerChildAge = "";
	var containerBedType = "";
	var containerRoom = "";
	switch(outputContainer) {
	
	    case "choutput":
	    	containerChildAge="c";
		    containerBedType="b";
		    containerRoom="";
		    $fn(_endsWith("sT")).value = 'Tab1';
	    	break;
	    	
	    case "choutputBC":
	    	containerChildAge = "ddlBCC";
		    containerBedType="ddlBCB";
		    containerRoom="BC";
		    $fn(_endsWith("sT")).value = 'Tab2';
	    	break;
	    	
	    case "choutputRN":
	    	containerChildAge = "ddlRNC";
		    containerBedType="ddlRNB";
		    containerRoom="RN";
		    $fn(_endsWith("sT")).value = 'Tab3';
	    	break;
	    	
	}

	$('.selHdr1').hide();
	
	var bedTypeCollection = $fn(_endsWith('bedTypeCollection')).value;
	
	var bedTypeCollectionArray = bedTypeCollection.split("|");
	
	var bedTypeCollectionDictionary = {};
	for( var count=0; count<bedTypeCollectionArray.length; count++)
	{
	   var eachBedTypeArray = bedTypeCollectionArray[count].split(",");
	   bedTypeCollectionDictionary[eachBedTypeArray[0]]=eachBedTypeArray[1];
	}	
	
	for (roomNo=1; roomNo<=noOfRoom; roomNo++){
	
		// Container Name for Children Detail
		$($fn(_endsWith('id'+containerRoom+'Room'+roomNo))).show();
		var containerName = outputContainer+roomNo;
		
		// Initially hide all the Select Age Details
		$('.'+containerName+" .colm5").hide();
		
		var noOfChild = eval("noOfChild"+roomNo);
		
	   // Showing the required Child Info
	   if(noOfChild >0){
		   $('.'+containerName).show();
		   $('.'+containerName+' .colm5:lt('+noOfChild+')').show();           
	   }
	
		
		for(var childNo=1; childNo<=noOfChild; childNo++)
		{		
			var childAgeId = containerChildAge+'hildAgeforRoom'+roomNo+'Child'+childNo;
			var bedType = containerBedType+'edTypeforRoom'+roomNo+'Child'+childNo;
			var childAgeControl = $fn(_endsWith(childAgeId));
			$(childAgeControl).find(":selected").attr('selected', 'selected');
			var ageid = $(childAgeControl).find(":selected").val();
			var name = $(childAgeControl).attr('rel');
			var hdrToShow = false;
			if (isNaN(ageid)) {
				$(childAgeControl).next().hide();

				$(childAgeControl).parent().siblings().children('.subchild:visible').each(
					function() {
						if (!isNaN($(childAgeControl).find(":selected").val())) {
							hdrToShow = true;
						}
					}
				)//each ends here

				if (hdrToShow) {
					$(childAgeControl).parent().siblings('.selHdr1').show();
				}
				else {
					$(childAgeControl).parent().siblings('.selHdr1').hide();
				}
			}
			else {
				if (isNaN($('.' + name).find(":selected").val())) {
					//populate only if no value selected previously
					$('.' + name).html(populateDropDown(ageid));
					$('.' + name).val(bedTypeCollectionDictionary[bedType]);
				}
				$(childAgeControl).next().show();
				$(childAgeControl).parent().siblings('.selHdr1').show();
				//R2.0 Bug:artf1153005 : Select bed type is displayed by default in the add another hotel flow. -Ashish
				$(childAgeControl).parent().parent().children().children().siblings('.selHdr1').show();
			}
		}
	}
}

//R2.2 Tool Tip - Make the Tooltip visible in a Modal Window
//$(function(){
//    $(".help").hover(function(){
//        $(".jqmID1").css("overflow-x", "visible");
//        $(".jqmID1").css("overflow-y", "visible");
//        $(".jqmID1").css("overflow", "visible");
//        $(".jqmID1 #LeftContentArea").addClass("modalWindowHelpTT");
//    },
//    function(){
//        $(".jqmID1").css("overflow-x", "hidden");
//        $(".jqmID1").css("overflow-y", "scroll");
//        $(".jqmID1 #LeftContentArea").removeClass("modalWindowHelpTT");        
//    });
//});
