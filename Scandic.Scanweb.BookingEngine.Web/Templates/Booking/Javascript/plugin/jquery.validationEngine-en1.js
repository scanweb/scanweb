
(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Add your regex rules here, you can take telephone as an example
						"regex":$('#lengthNone').val(),
						"alertText":$('#requiredFld').val(),
						"alertTextCheckboxMultiple":$('#CheckboxMultiple').val(),
						"alertTextCheckboxe":$('#CheckboxRd').val()},
					"requireds":{    			// Add your regex rules here, you can take telephone as an example
						"regex":$('#lengthNone').val(),
						"alertText":"",						
						"alertTextCheckboxMultiple":"",
						"alertTextCheckboxe":""},	
					"length":{
						"regex":$('#lengthNone').val(),
						"alertText":$('#lengthBetween').val(),
						"alertText2":$('#lengthAnd').val(),
						"alertText3":$('#lengthChar').val()},
					"maxCheckbox":{
						"regex":$('#lengthNone').val(),
						"alertText":$('#maxCheckbox').val()},	
					"minCheckbox":{
						"regex":$('#lengthNone').val(),
						"alertText":" Please select ",
						"alertText2":" options"},	
					"confirm":{
						"regex":$('#lengthNone').val(),
						"alertText":$('#confirm').val(),	
						"alertText3":$('#dontMatch').val()},				    
					"telephone":{
						"regex":"/^[0-9\-\(\)\ ]+$/",
						"alertText":" Invalid phone number"},	
					"email":{
						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
						"alertText":$('#email').val()},	
					"date":{
                         "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                         "alertText":" Invalid date, must be in YYYY-MM-DD format"},
					"onlyNumber":{
						"regex":"/^[0-9\ ]+$/",
						"alertText":$('#onlyNumber').val()},	
					"noSpecialCaracters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":" No special caracters allowed"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"name=eric",
						"alertTextOk":" This user is available",	
						"alertTextLoad":" Loading, please wait",
						"alertText":" This user is already taken"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":" This name is already taken",
						"alertTextOk":" This name is available",	
						"alertTextLoad":" Loading, please wait"},		
					"onlyLetter":{
						"regex":"/^[a-zA-Z\ \']+$/",
						"alertText":" Letters only"}
					}	
		}
	}
})(jQuery);

$(document).ready(function() {	
	$.validationEngineLanguage.newLang()
});