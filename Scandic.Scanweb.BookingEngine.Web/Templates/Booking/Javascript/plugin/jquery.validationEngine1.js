/*
 * Inline Form Validation Engine 1.7, jQuery plugin
 * 
 * Copyright(c) 2010, Cedric Dugas
 * http://www.position-relative.net
 *	
 * Form validation engine allowing custom regex rules to be added.
 * Thanks to Francois Duquette and Teddy Limousin 
 * and everyone helping me find bugs on the forum
 * Licenced under the MIT Licence
 */
 
(function($) {

    $.fn.validationEngine = function(settings) {

        if ($.validationEngineLanguage) {				// IS THERE A LANGUAGE LOCALISATION ?
            allRules = $.validationEngineLanguage.allRules;
        } else {
            $.validationEngine.debug("Validation engine rules are not loaded check your external file");
        }
        settings = jQuery.extend({
            allrules: allRules,
            validationEventTriggers: "focusout",
            inlineValidation: true,
            returnIsValid: false,
            liveEvent: true,
            unbindEngine: true,
            containerOverflow: false,
            containerOverflowDOM: "",
            ajaxSubmit: false,
            scroll: true,
            promptPosition: "topRight", // OPENNING BOX POSITION, IMPLEMENTED: topLeft, topRight, bottomLeft, centerRight, bottomRight
            success: false,
            beforeSuccess: function() { },
            failure: function() { }
        }, settings);
        $.validationEngine.settings = settings;
        $.validationEngine.ajaxValidArray = new Array(); // ARRAY FOR AJAX: VALIDATION MEMORY 

        if (settings.inlineValidation == true) { 		// Validating Inline ?
            if (!settings.returnIsValid) {					// NEEDED FOR THE SETTING returnIsValid
                allowReturnIsvalid = false;
                var $this = $(this);
                if (settings.liveEvent) {
                    // LIVE event, vast performance improvement over BIND
                    $this.find("[class*=validate][type!=checkbox]").live(settings.validationEventTriggers, function(caller) { _inlinEvent(this); })
                    $this.find("[class*=validate][type=checkbox]").live("click", function(caller) { _inlinEvent(this); })
                } else {
                    $this.find("[class*=validate]").not("[type=checkbox]").bind(settings.validationEventTriggers, function(caller) { _inlinEvent(this); })
                    $this.find("[class*=validate][type=checkbox]").bind("click", function(caller) { _inlinEvent(this); })
                }
                firstvalid = false;
            }
            function _inlinEvent(caller) {
                $.validationEngine.settings = settings;
                if ($.validationEngine.intercept == false || !$.validationEngine.intercept) {		// STOP INLINE VALIDATION THIS TIME ONLY
                    //$.validationEngine.onSubmitValid=false;
                    $.validationEngine.loadValidation(caller);
                } else {
                    $.validationEngine.intercept = false;
                }
            }
        }
        if (settings.returnIsValid) {		// Do validation and return true or false, it bypass everything;
            if ($.validationEngine.submitValidation(this, settings)) {
                return false;
            } else {
                return true;
            }
        }
        $(this).bind("submit", function(caller) {   // ON FORM SUBMIT, CONTROL AJAX FUNCTION IF SPECIFIED ON DOCUMENT READY
            $.validationEngine.onSubmitValid = true;
            $.validationEngine.settings = settings;
            if ($.validationEngine.submitValidation(this, settings) == false) {

                //RK: Reservation 2.0 | Commented below call as the validation is being done from framework
                //		        if(ValidateGuestNames())
                //		        {
                if ($fn(_endsWith("SubmitButtonDiv")) != null && $fn(_endsWith("BookingProgressDiv")) != null) {
                    $fn(_endsWith("SubmitButtonDiv")).style.display = "none";
                    $fn(_endsWith("BookingProgressDiv")).style.display = "block";
                }
                //                }
                else {
                    return false;
                }
                if ($.validationEngine.submitForm(this, settings) == true) {
                    //$(caller).removeClass("mandatory");

                    return false;
                }
            } else {
                settings.failure && settings.failure();
                //$(caller).addClass("mandatory");
                //$(".formError").hide();

                return false;
            }
        })
        $(".formError").live("click", function() {	 // REMOVE BOX ON CLICK
            $(this).fadeOut(150, function() { $(this).remove() })
        })
    };
    $.validationEngine = {
        defaultSetting: function(caller) {		// NOT GENERALLY USED, NEEDED FOR THE API, DO NOT TOUCH
            if ($.validationEngineLanguage) {
                allRules = $.validationEngineLanguage.allRules;
            } else {
                $.validationEngine.debug("Validation engine rules are not loaded check your external file");
            }
            settings = {
                allrules: allRules,
                validationEventTriggers: "blur",
                inlineValidation: true,
                containerOverflow: false,
                containerOverflowDOM: "",
                returnIsValid: false,
                scroll: true,
                unbindEngine: true,
                ajaxSubmit: false,
                promptPosition: "topRight", // OPENNING BOX POSITION, IMPLEMENTED: topLeft, topRight, bottomLeft, centerRight, bottomRight
                success: false,
                failure: function() { }
            }
            $.validationEngine.settings = settings;
        },
        loadValidation: function(caller) {		// GET VALIDATIONS TO BE EXECUTED
            var frmFld = $(caller);

            if (!$.validationEngine.settings) $.validationEngine.defaultSetting()
            rulesParsing = frmFld.attr('class');
            rulesRegExp = /\[(.*)\]/;
            getRules = rulesRegExp.exec(rulesParsing);
            if (getRules == null) return false;
            str = getRules[1];
            pattern = /\[|,|\]/;
            result = str.split(pattern);
            var validateCalll = $.validationEngine.validateCall(caller, result)
            return validateCalll;
        },
        validateCall: function(caller, rules) {	// EXECUTE VALIDATION REQUIRED BY THE USER FOR THIS FIELD
            var promptText = "";

            //Commented the below line as it was causing blank tooltip to display	
            //$('#fillThis').val('');
            var frmFld = $(caller);
            if (!frmFld.attr("id")) $.validationEngine.debug("This field have no ID attribut( name & class displayed): " + frmFld.attr("name") + " " + frmFld.attr("class"))

            caller = caller;
            ajaxValidate = false;
            var callerName = frmFld.attr("name");
            $.validationEngine.isError = false;
            $.validationEngine.showTriangle = true;
            callerType = frmFld.attr("type");

            for (i = 0; i < rules.length; i++) {
                switch (rules[i]) {
                    case "optional":
                        if (!frmFld.val()) {
                            $.validationEngine.closePrompt(caller);
                            return $.validationEngine.isError;
                        }
                        break;
                    case "required":
                        _required(caller, rules);
                        break;
                    case "custom":
                        _customRegex(caller, rules, i);
                        break;
                    case "exemptString":
                        _exemptString(caller, rules, i);
                        break;
                    //case "ajax":   
                    //	if(!$.validationEngine.onSubmitValid) _ajax(caller,rules,i);	  
                    //break;  
                    case "length":
                        _length(caller, rules, i);
                        break;
                    case "maxCheckbox":
                        _maxCheckbox(caller, rules, i);
                        groupname = frmFld.attr("name");
                        caller = $("input[name='" + groupname + "']");
                        break;
                    case "minCheckbox":
                        _minCheckbox(caller, rules, i);
                        groupname = frmFld.attr("name");
                        caller = $("input[name='" + groupname + "']");
                        break;
                    case "confirm":
                        _confirm(caller, rules, i);
                        // $(caller).blur(function(){
                        //	 $.validationEngine.closePrompt(caller);
                        //	  });
                        break;
                    case "sameFGPNumber":
                        _validateSameFGPNumber(caller, rules, i);
                        break;
                    //Site Expansion - Added by Anil
                    case "restrictSplChrForNames":
                        _validateRestrictSplChrForNames(caller, rules, i);
                        break;
                    case "restrictSplChrForAddress":
                        _validateRestrictSplChrForAddress(caller, rules, i);
                        break;
                    case "sameNames":

                        //artf1152962 | Unique name error message display is not proper |Rajeesh
                        //Don't validate the same room name for room1
                        if (frmFld.attr('fieldName') != 'name0') {
                            //RK: Added new rule for validating same names
                            _validateSameNames(caller, rules, i);
                        }
                    default: ;
                };
            };
            radioHack();
            /*if ($.validationEngine.isError == true){
            var linkTofieldText = "." +$.validationEngine.linkTofield(caller);
            if(linkTofieldText != "."){
            if(!$(linkTofieldText)[0]){
            $.validationEngine.buildPrompt(caller,promptText,"error")
            }else{	
            $.validationEngine.updatePromptText(caller,promptText);
            }	
            }else{
            $.validationEngine.updatePromptText(caller,promptText);
            }
            }else{
            $.validationEngine.closePrompt(caller);
            }	*/
            if ($.validationEngine.isError == true) {

                linkTofield = $.validationEngine.linkTofield(caller);
                ($("div." + linkTofield).size() == 0) ? $.validationEngine.buildPrompt(caller, promptText, "error") : $.validationEngine.updatePromptText(caller, promptText);

            } else { $.validationEngine.closePrompt(caller); }
            /* UNFORTUNATE RADIO AND CHECKBOX GROUP HACKS */
            /* As my validation is looping input with id's we need a hack for my validation to understand to group these inputs */
            function radioHack() {
                if ($("input[name='" + callerName + "']").size() > 1 && (callerType == "radio:visible" || callerType == "checkbox:visible")) {        // Hack for radio/checkbox group button, the validation go the first radio/checkbox of the group
                    caller = $("input[name='" + callerName + "'][type!=hidden]:first");
                    $.validationEngine.showTriangle = false;
                }
            }
            /* VALIDATION FUNCTIONS */
            function _required(caller, rules) {  // VALIDATE BLANK FIELD
                var formFld = $(caller);
                if (formFld.is(':visible')) {
                    callerType = formFld.attr("type");

                    if (callerType == "text" || callerType == "password" || callerType == "textarea") {
                        var callRel = formFld.attr('rel');
                        if ((!formFld.val()) || (formFld.val() == callRel)) {
                            if (!formFld.hasClass('mandatory')) {
                                formFld.focus(function() {
                                    if (formFld.hasClass('mandatory')) {
                                        $.validationEngine.isError = true;
                                        promptText = $('#fillThis').val() + "<br />";
                                        linkTofield = $.validationEngine.linkTofield(caller);
                                        ($("div." + linkTofield).size() == 0) ? $.validationEngine.buildPrompt(caller, promptText, "error") : $.validationEngine.updatePromptText(caller, promptText);
                                        formFld.css('filter', 'none');
                                    }
                                });
                            }
                            formFld.addClass("mandatory");
                            $.validationEngine.isError = true;
                        } else {
                            formFld.removeClass("mandatory");
                            $.validationEngine.isError = false;

                        }
                    }
                    if (callerType == "radio" || callerType == "checkbox") {
                        callerName = formFld.attr("name");

                        if ($("input[name='" + callerName + "']:checked").size() == 0) {
                            $.validationEngine.isError = true;
                            if ($("input[name='" + callerName + "']").size() == 1) {
                                if (callerType == "checkbox") {
                                    //promptText = $(caller).parent) //$.validationEngine.settings.allrules[rules[i]].alertTextCheckboxe+"<br />";
                                    promptText = formFld.attr("tip") + "<br />";
                                }
                                else {
                                    promptText = formFld.parent().attr("tip") + "<br />";
                                    //promptText = $.validationEngine.settings.allrules[rules[i]].alertTextCheckboxMultiple+"<br />";
                                }
                            }
                            else {
                                if (callerType == "checkbox") {
                                    promptText = formFld.attr("tip") + "<br />";
                                    //promptText = $.validationEngine.settings.allrules[rules[i]].alertTextCheckboxe+"<br />"; 
                                }
                                else {
                                    promptText = formFld.parent().attr("tip") + "<br />";
                                    //promptText = $.validationEngine.settings.allrules[rules[i]].alertTextCheckboxMultiple+"<br />";
                                }
                                //promptText = $.validationEngine.settings.allrules[rules[i]].alertTextCheckboxMultiple+"<br />";
                                //promptText = $.validationEngine.settings.allrules[rules[i]].alertTextCheckboxe+"<br />"; 
                            }
                        }
                        if ($.validationEngine.isError == true) {
                            formFld.addClass("mandatory");
                            $.validationEngine.buildPrompt(caller, promptText, "error");
                        }
                        else {
                            formFld.removeClass("mandatory");
                            $.validationEngine.isError = false;
                        }
                    }
                    if ((callerType == "select-one") || (callerType == "select-multiple")) { // added by paul@kinetek.net for select boxes, Thank you		
                        if (formFld.val() == "DFT") {//DFT: default drop down value for index -1 don't remove it

                            $(caller).focus(function() {
                                if (formFld.hasClass('mandatory')) {
                                    $.validationEngine.isError = true;
                                    //promptText += $.validationEngine.settings.allrules[rules[i]].alertText+"<br />";
                                    //promptText += "You need to filled this in <br />";
                                    //promptText += $.validationEngine.settings.allrules[rules[i]].alertText+"<br />";
                                    promptText += $('#fillThis').val() + "<br />";
                                }
                            });

                            formFld.addClass("mandatory");
                            $.validationEngine.isError = true;
                        }
                        else {
                            formFld.removeClass("mandatory");
                            $.validationEngine.isError = false;
                        }
                    }

                }
            }
            function _customRegex(caller, rules, position) {		 // VALIDATE REGEX RULES
                //RK: If the user had not entered any text, do not validate the control
                var relAttribute = $(caller).attr('rel');
                var textValue = $(caller).val();
                var frmFld = $(caller);
                if (textValue != null && (textValue != relAttribute) && (textValue != "")) {
                    customRule = rules[position + 1];
                    pattern = eval($.validationEngine.settings.allrules[customRule].regex);

                    if (!pattern.test(frmFld.attr('value'))) {
                        frmFld.focus(function() {
                            if (frmFld.hasClass('error')) {

                                $.validationEngine.isError = true;
                                promptText += $.validationEngine.settings.allrules[customRule].alertText + "<br />";
                                linkTofield = $.validationEngine.linkTofield(caller);
                                ($("div." + linkTofield).size() == 0) ? $.validationEngine.buildPrompt(caller, promptText, "error") : $.validationEngine.updatePromptText(caller, promptText);
                            }
                        });
                        $.validationEngine.isError = true;
                        frmFld.addClass("error");
                    } else {
                        //$.validationEngine.isError = false;
                        frmFld.removeClass("error");
                    }

                }
                else {
                    //$.validationEngine.isError = false;
                    frmFld.removeClass("error");
                }
            }

            function _exemptString(caller, rules, position) {		 // VALIDATE REGEX RULES
                customString = rules[position + 1];
                var frmFld = $(caller);

                if (customString == frmFld.attr('value')) {
                    $.validationEngine.isError = true;
                    promptText += $.validationEngine.settings.allrules['required'].alertText + "<br />";

                }
            }

            function _funcCall(caller, rules, position) {  		// VALIDATE CUSTOM FUNCTIONS OUTSIDE OF THE ENGINE SCOPE
                var frmFld = $(caller);
                customRule = rules[position + 1];

                funce = $.validationEngine.settings.allrules[customRule].nname;

                var fn = window[funce];
                if (typeof (fn) === 'function') {
                    var fn_result = fn();
                    if (!fn_result) {
                        $.validationEngine.isError = true;
                    }

                    promptText += $.validationEngine.settings.allrules[customRule].alertText + "<br />";
                }
            }
            /*function _ajax(caller,rules,position){				 // VALIDATE AJAX RULES
			
			customAjaxRule = rules[position+1];
            postfile = $.validationEngine.settings.allrules[customAjaxRule].file;
            fieldValue = $(caller).val();
            ajaxCaller = caller;
            fieldId = $(caller).attr("id");
            ajaxValidate = true;
            ajaxisError = $.validationEngine.isError;
			
			if($.validationEngine.settings.allrules[customAjaxRule].extraData){
            extraData = $.validationEngine.settings.allrules[customAjaxRule].extraData;
            }else{
            extraData = "";
            }
            /* AJAX VALIDATION HAS ITS OWN UPDATE AND BUILD UNLIKE OTHER RULES */
            i/*f(!ajaxisError){
				$.ajax({
				   	type: "POST",
				   	url: postfile,
				   	async: true,
				   	data: "validateValue="+fieldValue+"&validateId="+fieldId+"&validateError="+customAjaxRule+"&extraData="+extraData,
				   	beforeSend: function(){		// BUILD A LOADING PROMPT IF LOAD TEXT EXIST		   			
				   		if($.validationEngine.settings.allrules[customAjaxRule].alertTextLoad){
				   		
				   			if(!$("div."+fieldId+"formError")[0]){				   				
	 			 				return $.validationEngine.buildPrompt(ajaxCaller,$.validationEngine.settings.allrules[customAjaxRule].alertTextLoad,"load");
	 			 			}else{
	 			 				$.validationEngine.updatePromptText(ajaxCaller,$.validationEngine.settings.allrules[customAjaxRule].alertTextLoad,"load");
	 			 			}
			   			}
			  	 	},
			  	 	error: function(data,transport){ $.validationEngine.debug("error in the ajax: "+data.status+" "+transport) },
					success: function(data){					// GET SUCCESS DATA RETURN JSON
						data = eval( "("+data+")");				// GET JSON DATA FROM PHP AND PARSE IT
						ajaxisError = data.jsonValidateReturn[2];
						customAjaxRule = data.jsonValidateReturn[1];
						ajaxCaller = $("#"+data.jsonValidateReturn[0])[0];
						fieldId = ajaxCaller;
						ajaxErrorLength = $.validationEngine.ajaxValidArray.length;
						existInarray = false;
						
			 			 if(ajaxisError == "false"){			// DATA FALSE UPDATE PROMPT WITH ERROR;
			 			 	
			 			 	_checkInArray(false)				// Check if ajax validation alreay used on this field
			 			 	
			 			 	if(!existInarray){		 			// Add ajax error to stop submit		 		
				 			 	$.validationEngine.ajaxValidArray[ajaxErrorLength] =  new Array(2);
				 			 	$.validationEngine.ajaxValidArray[ajaxErrorLength][0] = fieldId;
				 			 	$.validationEngine.ajaxValidArray[ajaxErrorLength][1] = false;
				 			 	existInarray = false;
			 			 	}
				
			 			 	$.validationEngine.ajaxValid = false;
							promptText += $.validationEngine.settings.allrules[customAjaxRule].alertText+"<br />";
							$.validationEngine.updatePromptText(ajaxCaller,promptText,"",true);				
						 }else{	 
						 	_checkInArray(true);
						 	$.validationEngine.ajaxValid = true; 			
						 	if(!customAjaxRule)	{$.validationEngine.debug("wrong ajax response, are you on a server or in xampp? if not delete de ajax[ajaxUser] validating rule from your form ")}		   
						 	if($.validationEngine.settings.allrules[customAjaxRule].alertTextOk){	// NO OK TEXT MEAN CLOSE PROMPT	 			
	 			 				 				$.validationEngine.updatePromptText(ajaxCaller,$.validationEngine.settings.allrules[customAjaxRule].alertTextOk,"pass",true);
 			 				}else{
				 			 	ajaxValidate = false;		 	
				 			 	$.validationEngine.closePrompt(ajaxCaller);
 			 				}		
			 			 }
			 			function  _checkInArray(validate){
			 				for(x=0;x<ajaxErrorLength;x++){
			 			 		if($.validationEngine.ajaxValidArray[x][0] == fieldId){
			 			 			$.validationEngine.ajaxValidArray[x][1] = validate;
			 			 			existInarray = true;
			 			 		
			 			 		}
			 			 	}
			 			}
			 		}				
				});
			}
		}*/
            function _confirm(caller, rules, position) {
                var frmFld = $(caller);
                //Added below code to check whether control has mandatory class.if it is there,no further validation will be done for confirmation|Rajneesh
                if ($(caller).hasClass('mandatory')) {
                    return;
                }

                // VALIDATE FIELD MATCH
                confirmField = rules[position + 1];
                if (frmFld.is(':visible')) {
                    /*custom method added for confirm*/
                    var confField = frmFld.attr('fieldName');

                    //RK: Reservation 2.0 | artf1153614 | Commented the check for validating the email against rel text
                    if ( //$("."+confField).attr('value') != $("."+confField).attr('rel') && 
			        frmFld.attr('value') != $("." + confField).attr('value')) {
                        frmFld.focus(function() {

                            if (frmFld.hasClass('error')) {

                                var callRel = frmFld.attr('rel');
                                $.validationEngine.isError = true;
                                promptText += $.validationEngine.settings.allrules["confirm"].alertText + "<br />";
                                //promptText += "Invalid "+ callRel +"<br />";
                                linkTofield = $.validationEngine.linkTofield(caller);
                                ($("div." + linkTofield).size() == 0) ? $.validationEngine.buildPrompt(caller, promptText, "error") : $.validationEngine.updatePromptText(caller, promptText);
                            }
                        });
                        frmFld.addClass("error");
                        $.validationEngine.isError = true;
                    } else {
                        frmFld.removeClass("error");
                        $("." + confField).removeClass("error");
                        $.validationEngine.isError = false;
                    }
                }
            }
            function _validateSameFGPNumber(caller, rules, position)// Bhavya - Validates whether same fgp is entered multiple rooms
            {

                var frmFld = $(caller);
                fgpNumField = rules[position + 1];
                if ((frmFld.attr('disabled') != true) && frmFld.is(':visible') && (frmFld.attr('value') != $('#membershipNumberText').val()) && Trim(frmFld.attr('value')) != "") {
                    var isError = false;
                    var fgpNumberField = frmFld.attr('fieldName');
                    switch (fgpNumberField) {
                        case 'fgpNumber0':
                            var fgpFieldValue1 = $("." + 'fgpNumber1').attr('value');
                            var fgpFieldValue2 = $("." + 'fgpNumber2').attr('value');
                            var fgpFieldValue3 = $("." + 'fgpNumber3').attr('value');
                            if (frmFld.attr('value') == fgpFieldValue1 || (frmFld.attr('value') == fgpFieldValue2) || (frmFld.attr('value') == fgpFieldValue3)) {
                                isError = true;
                            }
                            break;
                        case 'fgpNumber1':
                            var fgpFieldValue = $("." + 'fgpNumber0').attr('value');
                            var fgpFieldValue2 = $("." + 'fgpNumber2').attr('value');
                            var fgpFieldValue3 = $("." + 'fgpNumber3').attr('value');
                            if (frmFld.attr('value') == fgpFieldValue || (frmFld.attr('value') == fgpFieldValue2) || (frmFld.attr('value') == fgpFieldValue3)) {
                                isError = true;
                            }
                            break;
                        case 'fgpNumber2':
                            var fgpFieldValue0 = $("." + 'fgpNumber0').attr('value');
                            var fgpFieldValue1 = $("." + 'fgpNumber1').attr('value');
                            var fgpFieldValue3 = $("." + 'fgpNumber3').attr('value');

                            if ((frmFld.attr('value') == fgpFieldValue0) || (frmFld.attr('value') == fgpFieldValue1) || (frmFld.attr('value') == fgpFieldValue3)) {
                                isError = true;
                            }
                            break;
                        case 'fgpNumber3':
                            var fgpFieldValue0 = $("." + 'fgpNumber0').attr('value');
                            var fgpFieldValue1 = $("." + 'fgpNumber1').attr('value');
                            var fgpFieldValue2 = $("." + 'fgpNumber2').attr('value');
                            if ((frmFld.attr('value') == fgpFieldValue0) || (frmFld.attr('value') == fgpFieldValue1) || (frmFld.attr('value') == fgpFieldValue2)) {
                                isError = true;
                            }
                            break;
                        default:
                            break;
                    }
                }
                if (isError) {
                    frmFld.focus(function() {
                        if (frmFld.hasClass('errorFGP')) {
                            var callRel = frmFld.attr('rel');
                            $.validationEngine.isError = true;
                            promptText += $.validationEngine.settings.allrules["confirm"].alertText3 + "<br />";
                            //promptText += "Invalid "+ callRel +"<br />";
                            linkTofield = $.validationEngine.linkTofield(caller);
                            ($("div." + linkTofield).size() == 0) ? $.validationEngine.buildPrompt(caller, promptText, "errorFGP") : $.validationEngine.updatePromptText(caller, promptText);
                        }
                    });
                    frmFld.addClass("errorFGP");
                    $.validationEngine.isError = true;
                }
                else {
                    frmFld.removeClass("errorFGP");
                    $.validationEngine.isError = false;
                }
            }

            // Site Expansion - Added by Anil - To allow valid 41 characters
            function _validateRestrictSplChrForNames(caller, rules, i) {
                var frmFld = $(caller);
                var frmFldValue = frmFld.val();
                //var validCharacters = '�����������������������������������������';
                //var replacedString = "";
                var invalidCharFound = false;

                frmFldValue = frmFldValue.replace(/^\s+|\s+$/g, ""); //Ltrim and Rtrim - replace initial and terminating spaces
                frmFldValue = frmFldValue.replace(/[a-zA-Z0-9]/g, ""); //Replace alphabets
                frmFldValue = frmFldValue.replace(/[\/\-]/g, ""); //replace allowed characters
                frmFldValue = frmFldValue.replace(/[\s]/g, ""); //replace allowed spaces between words
                
                if (frmFldValue.length > 0) {
                    for (var k = 0; k < frmFldValue.length; k++) {
                        var code = frmFldValue.charCodeAt(k);
                        if (code < 191 || code > 255) {
                            invalidCharFound = true;
                            break;
                        }
                        else {
                            invalidCharFound = testIfAllowedSpecialCharsFound(code);
                            if (invalidCharFound == true)
                                break;
                        }
                    }
                }

                if (invalidCharFound) {
                    $.validationEngine.isError = true;
                    promptText += $('#restrictSplChars').val() + "<br />";
                    linkTofield = $.validationEngine.linkTofield(caller);
                    ($("div." + linkTofield).size() == 0) ? $.validationEngine.buildPrompt(caller, promptText, "error") : $.validationEngine.updatePromptText(caller, promptText);
                    frmFld.addClass("error");
                }
                else {
                    $(frmFld).removeClass("error");
                    //$.validationEngine.isError = false;
                }
            }

            // Site Expansion - Added by Anil - To allow validate Address
            function _validateRestrictSplChrForAddress(caller, rules, i) {
                var frmFld = $(caller);
                var frmFldValue = frmFld.val();
                var invalidCharFound = false;

                frmFldValue = frmFldValue.replace(/^\s+|\s+$/g, ""); //Ltrim and Rtrim - replace initial and terminating spaces
                frmFldValue = frmFldValue.replace(/[a-zA-Z0-9]/g, ""); //Replace alphabets
                frmFldValue = frmFldValue.replace(/[\#\,]/g, ""); //replace allowed characters in Address
                frmFldValue = frmFldValue.replace(/[\s]/g, ""); //replace allowed spaces between words
                
                if (frmFldValue.length > 0) {
                    for (var k = 0; k < frmFldValue.length; k++) {
                        var code = frmFldValue.charCodeAt(k);
                        if (code < 191 || code > 255) {
                            invalidCharFound = true;
                            break;
                        }
                        else {
                            invalidCharFound = testIfAllowedSpecialCharsFound(code);
                            if (invalidCharFound == true)
                                break;
                        }
                    }
                }

                if (invalidCharFound) {
                    $.validationEngine.isError = true;
                    promptText += $('#restrictSplChars').val() + "<br />";
                    linkTofield = $.validationEngine.linkTofield(caller);
                    ($("div." + linkTofield).size() == 0) ? $.validationEngine.buildPrompt(caller, promptText, "error") : $.validationEngine.updatePromptText(caller, promptText);
                    frmFld.addClass("error");
                }
                else {
                    $(frmFld).removeClass("error");
                    //$.validationEngine.isError = false;
                }
            }

            // Site Expansion - Anil : Check if input characters are valid or not
            function testIfAllowedSpecialCharsFound(num) {
                var charFound = false;
                //Boxi: removed check for extra Cyrilic charecters
                if (num == 215 || num == 247) {
                    charFound = true;
                }             
                return charFound;
            }

            //RK: Reservation 2.0 | Added rule for checking same names
            function _validateSameNames(caller, rules, position) {
                var frmFld = $(caller);
                nameField = rules[position + 1];
                if (frmFld.is(':visible')) {
                    //var isError = false;
                    var nameFieldToValidate = frmFld.attr('fieldName');

                    $.validationEngine.isError = (CheckForSameNames(nameFieldToValidate) || $.validationEngine.isError) ? true : false;

                }
            }
            //RK: Reservation 2.0 | Added bind for binding the error to text box.
            function bindOrUnbindError(fieldToBind, isError) {
                if (isError) {
                    $(fieldToBind).focus(function() {
                        if ($(fieldToBind).hasClass('error')) {
                            $.validationEngine.isError = true;
                            promptText += $("#sameGuestNames").val() + "<br />";
                            linkTofield = $.validationEngine.linkTofield(fieldToBind);
                            ($("div." + linkTofield).size() == 0) ? $.validationEngine.buildPrompt(fieldToBind, promptText, "error") : $.validationEngine.updatePromptText(fieldToBind, promptText);
                        }
                    });
                    $(fieldToBind).addClass("error");
                    $.validationEngine.isError = true;
                }
                else {
                    $(fieldToBind).removeClass("error");
                    //				 $.validationEngine.isError = false;
                }
            }
            //RK: Reservation 2.0 | Added method to check if there are same first and last names in booking details
            function CheckForSameNames(fieldBeingValidated) {
                var isError = false;
                var R0FirstName = $fn(_endsWith(("RoomInformation0_txtFNameRoom")));
                var R0LastName = $fn(_endsWith(("RoomInformation0_txtLNameRoom")));
                var R1FirstName = $fn(_endsWith(("RoomInformation1_txtFNameRoom")));
                var R1LastName = $fn(_endsWith(("RoomInformation1_txtLNameRoom")));
                var R2FirstName = $fn(_endsWith(("RoomInformation2_txtFNameRoom")));
                var R2LastName = $fn(_endsWith(("RoomInformation2_txtLNameRoom")));
                var R3FirstName = $fn(_endsWith(("RoomInformation3_txtFNameRoom")));
                var R3LastName = $fn(_endsWith(("RoomInformation3_txtLNameRoom")));
                var R0NameVal = null, R1NameVal = null, R2NameVal = null, R3NameVal = null;
                if (R0FirstName != null && Trim($(R0FirstName).val()) != "" && $(R0FirstName).val() != $(R0FirstName).attr("rel")
                && R0LastName != null && Trim($(R0LastName).val()) != "" && $(R0LastName).val() != $(R0LastName).attr("rel")) {
                    R0NameVal = Trim($(R0FirstName).val()) + ";" + Trim($(R0LastName).val());
                    R0NameVal = R0NameVal.toLowerCase();
                }
                if (R1FirstName != null && Trim($(R1FirstName).val()) != "" && $(R1FirstName).val() != $(R1FirstName).attr("rel")
                && R1LastName != null && Trim($(R1LastName).val()) != "" && $(R1LastName).val() != $(R1LastName).attr("rel")) {
                    R1NameVal = Trim($(R1FirstName).val()) + ";" + Trim($(R1LastName).val());
                    R1NameVal = R1NameVal.toLowerCase();
                }
                if (R2FirstName != null && Trim($(R2FirstName).val()) != "" && $(R2FirstName).val() != $(R2FirstName).attr("rel")
                && R2LastName != null && Trim($(R2LastName).val()) != "" && $(R2LastName).val() != $(R2LastName).attr("rel")) {
                    R2NameVal = Trim($(R2FirstName).val()) + ";" + Trim($(R2LastName).val());
                    R2NameVal = R2NameVal.toLowerCase();
                }
                if (R3FirstName != null && Trim($(R3FirstName).val()) != "" && $(R3FirstName).val() != $(R3FirstName).attr("rel")
                && R3LastName != null && Trim($(R3LastName).val()) != "" && $(R3LastName).val() != $(R3LastName).attr("rel")) {
                    R3NameVal = Trim($(R3FirstName).val()) + ";" + Trim($(R3LastName).val());
                    R3NameVal = R3NameVal.toLowerCase();
                }

                switch (fieldBeingValidated) {
                    case 'name0':
                        if (R1NameVal != null && R0NameVal == R1NameVal
                        || R2NameVal != null && R0NameVal == R2NameVal
                        || R3NameVal != null && R0NameVal == R3NameVal
                        ) {
                            isError = true;
                        }
                        bindOrUnbindError(R0FirstName, isError)
                        bindOrUnbindError(R0LastName, isError)
                        break;
                    case 'name1':
                        if (R0NameVal != null && R1NameVal == R0NameVal
                        || R2NameVal != null && R1NameVal == R2NameVal
                        || R3NameVal != null && R1NameVal == R3NameVal
                        ) {
                            isError = true;
                        }
                        bindOrUnbindError(R1FirstName, isError)
                        bindOrUnbindError(R1LastName, isError)
                        break;
                    case 'name2':
                        if (R0NameVal != null && R2NameVal == R0NameVal
                        || R1NameVal != null && R2NameVal == R1NameVal
                        || R3NameVal != null && R2NameVal == R3NameVal
                        ) {
                            isError = true;
                        }
                        bindOrUnbindError(R2FirstName, isError)
                        bindOrUnbindError(R2LastName, isError)
                        break;
                    case 'name3':
                        if (R0NameVal != null && R3NameVal == R0NameVal
                        || R1NameVal != null && R3NameVal == R1NameVal
                        || R2NameVal != null && R3NameVal == R2NameVal
                        ) {
                            isError = true;
                        }
                        bindOrUnbindError(R3FirstName, isError)
                        bindOrUnbindError(R3LastName, isError)
                        break;
                    default:
                        break;
                }
                return isError;
            }

            function _length(caller, rules, position) {    	  // VALIDATE LENGTH

                startLength = eval(rules[position + 1]);
                endLength = eval(rules[position + 2]);
                var frmFld = $(caller);

                feildLength = $(caller).attr('value').length;
                if (feildLength < startLength || feildLength > endLength) {
                    $.validationEngine.isError = true;
                    promptText += $.validationEngine.settings.allrules["length"].alertText + startLength + $.validationEngine.settings.allrules["length"].alertText2 + endLength + $.validationEngine.settings.allrules["length"].alertText3 + "<br />"
                }
            }
            function _maxCheckbox(caller, rules, position) {  	  // VALIDATE CHECKBOX NUMBER

                nbCheck = eval(rules[position + 1]);
                groupname = $(caller).attr("name");

                groupSize = $("input[name='" + groupname + "']:checked").size();
                if (groupSize > nbCheck) {
                    $.validationEngine.showTriangle = false;
                    $.validationEngine.isError = true;
                    promptText += $.validationEngine.settings.allrules["maxCheckbox"].alertText + "<br />";
                }
            }
            function _minCheckbox(caller, rules, position) {  	  // VALIDATE CHECKBOX NUMBER

                nbCheck = eval(rules[position + 1]);
                groupname = $(caller).attr("name");
                var frmFld = $(caller);
                groupSize = $("input[name='" + groupname + "']:checked").size();
                if (groupSize < nbCheck) {

                    $.validationEngine.isError = true;
                    $.validationEngine.showTriangle = false;
                    promptText += $.validationEngine.settings.allrules["minCheckbox"].alertText + " " + nbCheck + " " + $.validationEngine.settings.allrules["minCheckbox"].alertText2 + "<br />";
                }
            }
            return ($.validationEngine.isError) ? $.validationEngine.isError : false;
        },
        submitForm: function(caller) {
            if ($.validationEngine.settings.ajaxSubmit) {
                if ($.validationEngine.settings.ajaxSubmitExtraData) {
                    extraData = $.validationEngine.settings.ajaxSubmitExtraData;
                } else {
                    extraData = "";
                }
                $.ajax({
                    type: "POST",
                    url: $.validationEngine.settings.ajaxSubmitFile,
                    async: true,
                    data: $(caller).serialize() + "&" + extraData,
                    error: function(data, transport) { $.validationEngine.debug("error in the ajax: " + data.status + " " + transport) },
                    success: function(data) {
                        if (data == "true") {			// EVERYTING IS FINE, SHOW SUCCESS MESSAGE
                            $(caller).css("opacity", 1)
                            $(caller).animate({ opacity: 0, height: 0 }, function() {
                                $(caller).css("display", "none");
                                $(caller).before("<div class='ajaxSubmit'>" + $.validationEngine.settings.ajaxSubmitMessage + "</div>");
                                $.validationEngine.closePrompt(".formError", true);
                                $(".ajaxSubmit").show("slow");
                                if ($.validationEngine.settings.success) {	// AJAX SUCCESS, STOP THE LOCATION UPDATE
                                    $.validationEngine.settings.success && $.validationEngine.settings.success();
                                    return false;
                                }
                            })
                        } else {						// HOUSTON WE GOT A PROBLEM (SOMETING IS NOT VALIDATING)
                            data = eval("(" + data + ")");
                            if (!data.jsonValidateReturn) {
                                $.validationEngine.debug("you are not going into the success fonction and jsonValidateReturn return nothing");
                            }
                            errorNumber = data.jsonValidateReturn.length
                            for (index = 0; index < errorNumber; index++) {
                                fieldId = data.jsonValidateReturn[index][0];
                                promptError = data.jsonValidateReturn[index][1];
                                type = data.jsonValidateReturn[index][2];
                                $.validationEngine.buildPrompt(fieldId, promptError, type);
                            }
                        }
                    }
                })
                return true;
            }
            // LOOK FOR BEFORE SUCCESS METHOD		
            if (!$.validationEngine.settings.beforeSuccess()) {
                if ($.validationEngine.settings.success) {	// AJAX SUCCESS, STOP THE LOCATION UPDATE
                    if ($.validationEngine.settings.unbindEngine) { $(caller).unbind("submit") }
                    $.validationEngine.settings.success && $.validationEngine.settings.success();
                    return true;
                }
            } else {
                return true;
            }
            return false;
        },
        buildPrompt: function(caller, promptText, type, ajaxed) {			// ERROR PROMPT CREATION AND DISPLAY WHEN AN ERROR OCCUR
            if (!$.validationEngine.settings) {
                $.validationEngine.defaultSetting()
            }
            deleteItself = "." + $(caller).attr("id") + "formError"

            if ($(deleteItself)[0]) {
                $(deleteItself).stop();
                $(deleteItself).remove();
            }
            /*var divFormError = document.createElement('div');
            var formErrorContent = document.createElement('div');*/

            var divFormWrapper = document.createElement('div');
            var divFormError = document.createElement('div');
            var formErrorTop = document.createElement('div');
            var formErrorContent = document.createElement('div');
            linkTofield = $.validationEngine.linkTofield(caller)
            $(divFormError).addClass("formError")

            //if(type == "pass") $(divFormError).addClass("greenPopup")
            //if(type == "load") $(divFormError).addClass("blackPopup")
            //if(ajaxed) $(divFormError).addClass("ajaxed")

            //if(ajaxed){ $(divFormError).addClass("ajaxed") }

            $(divFormError).addClass(linkTofield);

            $(formErrorTop).addClass("formErrorTop");
            $(formErrorContent).addClass("formErrorContent");

            if ($.validationEngine.settings.containerOverflow) {		// Is the form contained in an overflown container?
                $(caller).before(divFormError);
            } else {
                $("body").append(divFormError);
            }
            $(divFormError).append(formErrorTop);
            $(divFormError).append(formErrorContent);

            if ($.validationEngine.showTriangle != false) {		// NO TRIANGLE ON MAX CHECKBOX AND RADIO
                var arrow = document.createElement('div');
                $(arrow).addClass("formErrorArrow");
                $(divFormError).append(arrow);
                if ($.validationEngine.settings.promptPosition == "bottomLeft" || $.validationEngine.settings.promptPosition == "bottomRight") {
                    $(arrow).addClass("formErrorArrowBottom")
                    $(arrow).html('<div class="line1"><!-- --></div><div class="line2"><!-- --></div><div class="line3"><!-- --></div><div class="line4"><!-- --></div><div class="line5"><!-- --></div><div class="line6"><!-- --></div><div class="line7"><!-- --></div><div class="line8"><!-- --></div><div class="line9"><!-- --></div><div class="line10"><!-- --></div>');
                }
                if ($.validationEngine.settings.promptPosition == "topLeft" || $.validationEngine.settings.promptPosition == "topRight") {
                    $(divFormError).append(arrow);
                    $(arrow).html('<div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div>');
                }
            }
            $(formErrorContent).html(promptText)

            var calculatedPosition = $.validationEngine.calculatePosition(caller, promptText, type, ajaxed, divFormError)

            calculatedPosition.callerTopPosition += "px";
            calculatedPosition.callerleftPosition += "px";
            calculatedPosition.marginTopSize += "px"
            $(divFormError).css({
                "top": calculatedPosition.callerTopPosition,
                "left": calculatedPosition.callerleftPosition,
                "marginTop": calculatedPosition.marginTopSize,
                "opacity": 0
            })
            return $(divFormError).animate({ "opacity": 1, "filter": "none" }, function() { return true; });
        },
        updatePromptText: function(caller, promptText, type, ajaxed) {	// UPDATE TEXT ERROR IF AN ERROR IS ALREADY DISPLAYED

            linkTofield = $.validationEngine.linkTofield(caller);
            var updateThisPrompt = "." + linkTofield;

            /*if(type == "pass") { $(updateThisPrompt).addClass("greenPopup") }else{ $(updateThisPrompt).removeClass("greenPopup")};
            if(type == "load") { $(updateThisPrompt).addClass("blackPopup") }else{ $(updateThisPrompt).removeClass("blackPopup")};
            if(ajaxed) { $(updateThisPrompt).addClass("ajaxed") }else{ $(updateThisPrompt).removeClass("ajaxed")};*/

            $(updateThisPrompt).find(".formErrorContent").html(promptText);

            //var calculatedPosition = $.validationEngine.calculatePosition(caller,promptText,type,ajaxed,updateThisPrompt)

            //calculatedPosition.callerTopPosition +="px";
            //calculatedPosition.callerleftPosition +="px";
            //calculatedPosition.marginTopSize +="px"
            //$(updateThisPrompt).animate({ "top":calculatedPosition.callerTopPosition,"marginTop":calculatedPosition.marginTopSize });
            $(updateThisPrompt).css({ 'visibility': 'visible' });
            if ($(updateThisPrompt).find(".formErrorContent").is(':empty')) {
                $(updateThisPrompt).css({ 'visibility': 'hidden' })
            }
        },
        calculatePosition: function(caller, promptText, type, ajaxed, divFormError) {

            var frmFld = $(caller);
            if ($.validationEngine.settings.containerOverflow) {		// Is the form contained in an overflown container?
                callerTopPosition = 0;
                callerleftPosition = 0;
                callerWidth = frmFld.width();
                inputHeight = $(divFormError).height(); 				// compasation for the triangle
                var marginTopSize = "-" + inputHeight;
            } else {
                callerTopPosition = frmFld.offset().top;
                callerleftPosition = frmFld.offset().left;
                callerWidth = frmFld.width();
                inputHeight = $(divFormError).height();
                var marginTopSize = 0;
            }

            /* POSITIONNING */
            if ($.validationEngine.settings.promptPosition == "topRight") {

                if ($.validationEngine.settings.containerOverflow) {		// Is the form contained in an overflown container?
                    callerleftPosition += callerWidth - 30;
                } else {
                    callerleftPosition += callerWidth - 30;
                    callerTopPosition += -inputHeight - 18;
                }

                if (callerType == "radio" || callerType == "checkbox") {
                    callerTopPosition += -inputHeight + 50;
                }
            }
            if ($.validationEngine.settings.promptPosition == "topLeft") {
                callerTopPosition += -inputHeight - 10;
            }

            if ($.validationEngine.settings.promptPosition == "centerRight") {
                callerleftPosition += callerWidth + 13;
            }

            if ($.validationEngine.settings.promptPosition == "bottomLeft") {
                callerHeight = frmFld.height();
                callerTopPosition = callerTopPosition + callerHeight + 15;
            }
            if ($.validationEngine.settings.promptPosition == "bottomRight") {
                callerHeight = frmFld.height();
                callerleftPosition += callerWidth - 30;
                callerTopPosition += callerHeight + 5;
            }
            return {
                "callerTopPosition": callerTopPosition,
                "callerleftPosition": callerleftPosition,
                "marginTopSize": marginTopSize
            }
        },
        linkTofield: function(caller) {
            var linkTofield = $(caller).attr("id") + "formError";
            linkTofield = linkTofield.replace(/\[/g, "");
            linkTofield = linkTofield.replace(/\]/g, "");
            return linkTofield;
        },
        closePrompt: function(caller, outside) {						// CLOSE PROMPT WHEN ERROR CORRECTED
            if (!$.validationEngine.settings) {
                $.validationEngine.defaultSetting()
            }
            if (outside) {
                $(caller).fadeTo("fast", 0, function() {
                    $(caller).remove();
                });
                return false;
            }
            if (typeof (ajaxValidate) == 'undefined') { ajaxValidate = false }
            if (!ajaxValidate) {
                linkTofield = $.validationEngine.linkTofield(caller);
                closingPrompt = "." + linkTofield;
                $(closingPrompt).fadeTo("fast", 0, function() {
                    $(closingPrompt).remove();
                });
            }
        },
        debug: function(error) {
            if (!$("#debugMode")[0]) {
                $("body").append("<div id='debugMode'><div class='debugError'><strong>This is a debug mode, you got a problem with your form, it will try to help you, refresh when you think you nailed down the problem</strong></div></div>");
            }
            $(".debugError").append("<div class='debugerror'>" + error + "</div>");
        },
        submitValidation: function(caller) {					// FORM SUBMIT VALIDATION LOOPING INLINE VALIDATION
            var stopForm = false;
            $.validationEngine.ajaxValid = true;
            var toValidateSize = $(caller).find("[class*=validate]").size();
            var frmFld = $(caller);
            frmFld.find("[class*=validate]").not(':hidden').each(function() {
                linkTofield = $.validationEngine.linkTofield(this);

                if (!$("." + linkTofield).hasClass("ajaxed")) {	// DO NOT UPDATE ALREADY AJAXED FIELDS (only happen if no normal errors, don't worry)
                    var validationPass = $.validationEngine.loadValidation(this);
                    return (validationPass) ? stopForm = true : "";
                };
            });
            ajaxErrorLength = $.validationEngine.ajaxValidArray.length; 	// LOOK IF SOME AJAX IS NOT VALIDATE
            for (x = 0; x < ajaxErrorLength; x++) {
                if ($.validationEngine.ajaxValidArray[x][1] == false) $.validationEngine.ajaxValid = false;
            }
            if (stopForm || !$.validationEngine.ajaxValid) {		// GET IF THERE IS AN ERROR OR NOT FROM THIS VALIDATION FUNCTIONS
                if ($.validationEngine.settings.scroll) {
                    if (!$.validationEngine.settings.containerOverflow) {
                        var destination = $(".formError:not('.greenPopup'):first").offset().top;
                        $(".formError:not('.greenPopup')").each(function() {
                            testDestination = $(this).offset().top;
                            if (destination > testDestination) destination = $(this).offset().top;
                        })
                        $("html:not(:animated),body:not(:animated)").animate({ scrollTop: destination }, 1100);
                    } else {
                        var destination = $(".formError:not('.greenPopup'):first").offset().top;
                        var scrollContainerScroll = $($.validationEngine.settings.containerOverflowDOM).scrollTop();
                        var scrollContainerPos = -parseInt($($.validationEngine.settings.containerOverflowDOM).offset().top);
                        var destination = scrollContainerScroll + destination + scrollContainerPos - 5
                        var scrollContainer = $.validationEngine.settings.containerOverflowDOM + ":not(:animated)"

                        $(scrollContainer).animate({ scrollTop: destination }, 1100);
                    }
                }
                return true;
            } else {
                return false;
            }
        }
    }
})(jQuery);