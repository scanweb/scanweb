﻿/* Danish initialisation for the jQuery UI date picker plugin. */
/* Written by Jan Christensen ( deletestuff@gmail.com). */
/*Sanjetha CR changes */
jQuery(function($) {
    $.datepicker.regional['de'] = { clearText: 'Nulstil', clearStatus: 'Nulstil den aktuelle dato',
        closeText: 'Luk', closeStatus: 'Luk uden ændringer',
        prevText: 'Zurück', prevStatus: 'Vis forrige måned',
        nextText: 'Weiter', nextStatus: 'Vis næste måned',
        currentText: 'Idag', currentStatus: 'Vis aktuel måned',
        monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
        monthNamesShort: ['Jan', 'Feb', 'Maz', 'Apr', 'Mai', 'Jun',
        'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
        monthStatus: 'Vis en anden måned', yearStatus: 'Vis et andet år',
        weekHeader: 'KW', weekStatus: 'Årets uge',
        dayNames: ['Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag'],
        dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
        dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
        dayStatus: 'Sæt DD som første ugedag', dateStatus: 'Vælg D, M d',
        dateFormat: 'D dd/mm/yy', firstDay: 1,
        initStatus: 'Vælg en dato', isRTL: false
    };
    $.datepicker.setDefaults($.datepicker.regional['de']);
    $.datepicker.setDefaults({ showWeeks: true });
});







