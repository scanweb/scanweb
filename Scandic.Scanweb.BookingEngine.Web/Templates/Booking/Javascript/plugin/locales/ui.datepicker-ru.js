﻿/* Russian initialisation for the jQuery UI date picker plugin. */
/* Written by Santanu Boxi ( sboxi@sapient.com). */
/* Sanjetha 06/02/12 defect artf1262743 fixed.*/
jQuery(function($) {
    $.datepicker.regional['ru'] = { clearText: 'сброс', clearStatus: 'Сброс текущей даты',
        closeText: 'близко', closeStatus: 'Закрыть без изменений',
        prevText: 'назад', prevStatus: 'Посмотреть Предыдущий месяц',
        nextText: 'вперед', nextStatus: 'Просмотр Следующий месяц',
        currentText: 'сегодня', currentStatus: 'зрения сегодня дату',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'июля', 'Август', 'сентября', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['ianv', 'fevr', 'mart', 'apr', 'mai', 'iiun', 'iiul', 'avg', 'sent', 'okt', 'noiabr', 'dek'],
        monthStatus: 'Просмотр различный месяц', yearStatus: 'Просмотр другой год',
        weekHeader: 'Нед', weekStatus: 'В этом году неделя',
        dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dayStatus: 'Sæt DD som første ugedag', dateStatus: 'Vælg D, M d',
        dateFormat: 'D dd/mm/yy', firstDay: 1,
        initStatus: 'Vælg en dato', isRTL: false
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);
    $.datepicker.setDefaults({ showWeeks: true });
});
