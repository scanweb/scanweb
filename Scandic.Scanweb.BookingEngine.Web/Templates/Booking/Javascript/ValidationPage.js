/////////////////////////////////////////////////////////////////////////////////////////////////////////
// This file contains all the Page level validation methods
// Created Date : 9/1/2008
/////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// This function will be called from each ascx codebehinds by passing the corresponding pagename,
// Based on the page name it will call the appropriate validation methods.If all validations passed 
// this will return true or else it will return false.
/////////////////////////////////////////////////////////////////////////////////////////////////////////

//Vrushali | fix 3372992 | Not able to modify a combo reservation with children bed type 'Extra bed'. | P2
var isModifyFlowComboReservation = "False";

function performValidation(pageName) {
    // bool variable (true /false)
    var validationResult = false;

    // AMS Fix: artf833540
    // This will disable multiple bookings happening on clicking book button multiple times in IE.
    if (pageName == PAGE_BOOKING_DETAIL) {
        if (isClicked == true) {
            return false;
        }
        else {
            //$fn(_endsWith("SubmitButtonDiv")).style.display = "none";
            //$fn(_endsWith("BookingProgressDiv")).style.display = "block";            
        }
    }  /*R1.7.3 [Opera V5 patch upgrade] | artf1056125: the div is added to hide the send button so that the user can not make multiple registrations. */
    else if (pageName == PAGE_ENROLL_GUEST) {
        if (isSendClicked == true) {
            return false;
        }
        else {
            $fn(_endsWith("SendButtonDiv")).style.display = "none";
            $fn(_endsWith("SendingProgressDiv")).style.display = "block";
        }
    }


    switch (pageName) {
        case PAGE_SHOPPING_CART:
            validationResult = validation_ShoppingCart();
            break;
        case PAGE_MODIFY_SHOPPING_CART:
            validationResult = validation_ModifyShoppingCart();
            break;
        case PAGE_BOOKING_MODULE_BIG:
            validationResult = validation_BookingBig();
            break;
        case PAGE_ACCOUNT_INFO:
            validationResult = validation_AccountInfo();
            break;
        case PAGE_CONTACT_US:
            validationResult = validation_ContactUs();
            break;
        case PAGE_FORGOTTEN_PASSWORD:
            validationResult = validation_ForgottenPassword();
            break;
        case PAGE_LOGIN_ERROR:
            validationResult = validation_LoginError();
            break;
        case PAGE_LOGIN_BOOKING_DETAIL:
            // R1.3 | CR5 | StoryId: | Login on Booking Detail Screen.
            validationResult = validation_BookingDetailLogin();
            break;
        case PAGE_LOGIN_STATUS:
            validationResult = validation_LoyalityLogin();
            break;
        case PAGE_MISSING_POINTS:
            validationResult = validation_MissingPoints();
            break;
        case PAGE_LOSTCARD_NOLOGIN:
            validationResult = validation_Lostcardwithoutlogin();
            break;
        case PAGE_ENROLL_GUEST:
            validationResult = validation_EnrollLoyalty();
            break;
        case PAGE_INVITE_A_FRIEND:
            validationResult = validation_InviteFriend();
            break;
        case PAGE_UPDATE_PROFILE:
            validationResult = validation_UpdateProfile();
            break;
        case PAGE_BOOKING_DETAIL:
            validationResult = validation_BookingDetail();
            break;
        case PAGE_BOOKING_SEARCH:
            validationResult = Validation_BookingSearch();
            break;
        case PAGE_CANCEL_BOOKING_CONFIRMATION:
            validationResult = Validation_CancelConfirmation();
            break;
        case PAGE_CUSTOMER_SERVICE_CONTACT_US:
            // R1.4 | CR# 7 | Contact Us
            validationResult = Validation_CustomerServiceContactUs();
            break;
        case PAGE_CAMPAIGN_LANDING:
            //Release 1.5 | artf809466 | FGP � Fast Track Enrolment
            validationResult = Validation_CampaignLanding();
            break;
        // 1.6 | Kids Concept | START    
        case PAGE_CHILDREN_DETAIL:
            validationResult = Validation_ChildrensDetails();
            break;
        case PAGE_MODIFY_CHILDREN_DETAIL:
            validationResult = Validation_ChildrensDetails();
            break;
        // 1.6 Kids Concept | END    
        //Find a hotel release    
        case PAGE_FIND_A_HOTEL:
            validationResult = validation_FindAHotel();
            break;

    }

    // AMS Fix: artf833540
    // This will disable multiple bookings happening on clicking book button multiple times in IE.
    if (pageName == PAGE_BOOKING_DETAIL) {
        if (validationResult == true) {
            $fn(_endsWith("SubmitButtonDiv")).style.display = "none";
            $fn(_endsWith("BookingProgressDiv")).style.display = "block";
            isClicked = true;
        }
        else {
            $fn(_endsWith("SubmitButtonDiv")).style.display = "block";
            $fn(_endsWith("BookingProgressDiv")).style.display = "none";
            isClicked = false;
        }
    }   //R1.7.3 [Opera V5 patch upgrade] | artf1056125: the div is added to hide the send button so that the user can not make multiple registrations. 
    else if (pageName == PAGE_ENROLL_GUEST) {
        if (validationResult == true) {

            $fn(_endsWith("SendButtonDiv")).style.display = "none";
            $fn(_endsWith("SendingProgressDiv")).style.display = "block";
            isSendClicked = true;
        }
        else {
            $fn(_endsWith("SendButtonDiv")).style.display = "block";
            $fn(_endsWith("SendingProgressDiv")).style.display = "none";
            isSendClicked = false;
        }
    }
    else if (pageName == PAGE_LOGIN_STATUS) {
        if (validationResult == true) {
            $fn(_endsWith("LoginProgressDiv")).style.display = "block";
            isSendClicked = true;
        }
        else {
            $fn(_endsWith("LoginProgressDiv")).style.display = "none";
            isSendClicked = false;
        }
    }
    else if (pageName == PAGE_LOGIN_BOOKING_DETAIL) {
        if (validationResult == true) {
            $fn(_endsWith("BookingDetailsLoginProgressDiv")).style.display = "block";
            isSendClicked = true;
        }
        else {
            $fn(_endsWith("BookingDetailsLoginProgressDiv")).style.display = "none";
            isSendClicked = false;
        }
    }
    else if (pageName == PAGE_LOGIN_ERROR) {
        if (validationResult == true) {
            if ($fn(_endsWith("LogoutConfirmProgressDiv")) != null && $fn(_endsWith("LogoutConfirmProgressDiv")) != 'undefined')
                $fn(_endsWith("LogoutConfirmProgressDiv")).style.display = "block";
            if ($fn(_endsWith("LoginErrorProgressDiv")) != null && $fn(_endsWith("LoginErrorProgressDiv")) != 'undefined')
                $fn(_endsWith("LoginErrorProgressDiv")).style.display = "block";
            isSendClicked = true;
        }
        else {
            if ($fn(_endsWith("LogoutConfirmProgressDiv")) != null && $fn(_endsWith("LogoutConfirmProgressDiv")) != 'undefined')
                $fn(_endsWith("LogoutConfirmProgressDiv")).style.display = "none";
            if ($fn(_endsWith("LoginErrorProgressDiv")) != null && $fn(_endsWith("LoginErrorProgressDiv")) != 'undefined')
                $fn(_endsWith("LoginErrorProgressDiv")).style.display = "none";
            isSendClicked = false;
        }
    }
    else if (pageName == PAGE_CAMPAIGN_LANDING) {
        if (validationResult == true) {
            $fn(_endsWith("CampLandLoginProgressDiv")).style.display = "block";
            isSendClicked = true;
        }
        else {
            $fn(_endsWith("CampLandLoginProgressDiv")).style.display = "none";
            isSendClicked = false;
        }
    }
    else if (pageName == PAGE_BOOKING_MODULE_BIG) {
        if (validationResult == true) {
            if ($fn(_endsWith("RwrdNgtBigLoginProgressDiv")) != null && $fn(_endsWith("RwrdNgtBigLoginProgressDiv")) != 'undefined')
                $fn(_endsWith("RwrdNgtBigLoginProgressDiv")).style.display = "block";
            if ($fn(_endsWith("RwrdNgtSmallLoginProgressDiv")) != null && $fn(_endsWith("RwrdNgtSmallLoginProgressDiv")) != 'undefined')
                $fn(_endsWith("RwrdNgtSmallLoginProgressDiv")).style.display = "block";
            if ($fn(_endsWith("RwrdNgtMedLoginProgressDiv")) != null && $fn(_endsWith("RwrdNgtMedLoginProgressDiv")) != 'undefined')
                $fn(_endsWith("RwrdNgtMedLoginProgressDiv")).style.display = "block";
            isSendClicked = true;
        }
        else {
            if ($fn(_endsWith("RwrdNgtBigLoginProgressDiv")) != null && $fn(_endsWith("RwrdNgtBigLoginProgressDiv")) != 'undefined')
                $fn(_endsWith("RwrdNgtBigLoginProgressDiv")).style.display = "none";
            if ($fn(_endsWith("RwrdNgtSmallLoginProgressDiv")) != null && $fn(_endsWith("RwrdNgtSmallLoginProgressDiv")) != 'undefined')
                $fn(_endsWith("RwrdNgtSmallLoginProgressDiv")).style.display = "none";
            if ($fn(_endsWith("RwrdNgtMedLoginProgressDiv")) != null && $fn(_endsWith("RwrdNgtMedLoginProgressDiv")) != 'undefined')
                $fn(_endsWith("RwrdNgtMedLoginProgressDiv")).style.display = "none";
            isSendClicked = false;
        }
    }
    // if  the validation passed this returns true else returns false
    return validationResult;
}

//Find a hotel release.
function validation_FindAHotel() {
    var validator = null;
    var validation = null;
    validator = new Validator(PAGE_FIND_A_HOTEL, "destinationErrorDiv");

    // Destination name
    validation = new Validation("txtDestinationName", CONTROL_TYPE_TEXTFIELD, "destinationDiv", "errorText", RULE_NONEMPTY, $fn(_endsWith("destinationError")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtDestinationName", CONTROL_TYPE_TEXTFIELD, "destinationDiv", "errorText", RULE_NOTANUMBER, $fn(_endsWith("destinationError")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtDestinationName", CONTROL_TYPE_TEXTFIELD, "destinationDiv", "errorText", RULE_DESTINATION, $fn(_endsWith("destinationError")).value, CONST_CUSTOM_TRUE, "validateDestinationCustom('txtDestinationName','selectedFAHDestId')");
    validator.addValidation(validation);
    validation = new Validation("txtDestinationName", CONTROL_TYPE_TEXTFIELD, "destinationDiv", "errorText", RULE_DUPLICATE_DESTINATION, $fn(_endsWith("duplicateDestinationError")).value, CONST_CUSTOM_TRUE, "validateDestinationDuplicate('txtDestinationName','selectedFAHDestId')");
    validator.addValidation(validation);
    validator.validateAll();

    if (validator.errorMessages.length > 0)
        return false;
    else
        return true;
}


//RK: Reservation 2.0 | Adding new method for validation on Shopping cart
function validation_ShoppingCart() {
    validator = new Validator(PAGE_SHOPPING_CART, "RegClientErrorDiv");
    AttachValidation("txtHotelName", "txtArrivalDate", "txtDepartureDate", "txtnoOfNights", "txtRegularCode", validator);
    validator.validateAll();

    if (validator.errorMessages.length > 0) {
        return false;
    }
    else if ((validator.errorMessages.length <= 0))//Child Validation For Regular Booking
    {
        validator = new Validator(PAGE_SHOPPING_CART, "RegClientErrorDiv");
        if (!ValidatingAgeAndChildDDl("ddlNoOfRoomsReg", "ddlChildPerRoom", "ddlAdultsPerRoom", "childAgeforRoom", "bedTypeforRoom", "", validator, "RegClientErrorDiv"))
            return false;
        else {
            $fn(_endsWith("loginSrchPageID")).value = $fn(_endsWith("loginSrchID")).value;

            //artifact-1179008 -Rajneesh-Scanweb - Problem with reservation regarding free nights-Rajneesh
            CaptureBedTypesForShopingcartEdit();
            return true;
        }
    }
}

//RK: Reservation 2.0 | Adding new method for validation on Modify Shopping cart
function validation_ModifyShoppingCart() {
    validator = new Validator(PAGE_MODIFY_SHOPPING_CART, "RegClientErrorDiv");
    AttachValidation(null, "txtArrivalDate", "txtDepartureDate", null, "txtRegularCode", validator);
    validator.validateAll();

    if (validator.errorMessages.length > 0) {
        return false;
    }
    else if ((validator.errorMessages.length <= 0)) {
        validator = new Validator(PAGE_MODIFY_SHOPPING_CART, "RegClientErrorDiv");
        if (!ValidatingAgeAndChildDDl("noOfRoomsSelected", "ddlChildPerRoom", "ddlAdultsPerRoom", "childAgeforRoom", "bedTypeforRoom", "", validator, "RegClientErrorDiv"))
            return false;
        else {
            $fn(_endsWith("loginSrchPageID")).value = $fn(_endsWith("loginSrchID")).value;
            //R2.0 Bug Id: 499942-Ashish Need to change because in this case we do not have ddl for no of rooms
            CaptureBedTypesShoppingCart();
            return true;
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Booking Module Big : START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function validation_BookingBig() {
    $fn(_endsWith("loginSrchPageID")).value = $fn(_endsWith("loginSrchID")).value;
    var validator = null;
    var validation = null;
    var tabValue = $fn(_endsWith("sT")).value;
    var redemtionLogin = $fn(_endsWith("guestLogin")).value;

    if ((tabValue == "Tab3") && (redemtionLogin == 'false')) {
        validator = new Validator(PAGE_BOOKING_MODULE_BIG, "RNClientErrorDiv");
        // User name for redemption
        validation = new Validation("txtUserName", CONTROL_TYPE_TEXTFIELD, "txtUserName", "error", RULE_NONEMPTY, $fn(_endsWith("invalidGuestNumberError")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation("txtUserName", CONTROL_TYPE_TEXTFIELD, "txtUserName", "mandatory", RULE_MEMBERSHIP_NUM, $fn(_endsWith("invalidGuestNumberError")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Password for redemption
        validation = new Validation("txtPassword", CONTROL_TYPE_TEXTFIELD, "inputtxtPassword", "mandatory", RULE_NONEMPTY, $fn(_endsWith("invalidGuestPINError")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
    }
    else if (tabValue == "Tab1") {
        validator = new Validator(PAGE_BOOKING_MODULE_BIG, "RegClientErrorDiv");
        //Release R 2.0| Change txtnoOfNights to ddlnoOfNights (Shameem)
        AttachValidation("txtHotelName", "txtArrivalDate", "txtDepartureDate", "txtnoOfNights", "txtRegularCode", validator);

    }
    else if (tabValue == "Tab2") {
        validator = new Validator(PAGE_BOOKING_MODULE_BIG, "BCClientErrorDiv");
        //Release R 2.0| Change txtBonousChequeNoofNights to ddlBonousChequeNoofNights (Shameem)
        AttachValidation("txtHotelNameBNC", "txtBCArrivalDate", "txtBCDepartureDate", "txtBonousChequeNoofNights", null, validator);

    }
    else if (tabValue == "Tab3") {
        validator = new Validator(PAGE_BOOKING_MODULE_BIG, "RNClientErrorDiv");
        //Release R 2.0| Change txtRewardNightNoOfNights to ddlRewardNightNoOfNights(Shameem)
        AttachValidation("txtHotelNameRewardNights", "txtRNArrivalDate", "txtRNDepartureDate", "txtRewardNightNoOfNights", null, validator);

    }
    else if (tabValue == "Tab4") {
        validator = new Validator(PAGE_BOOKING_MODULE_BIG, "clientErrorDivBD");
        // Validate Booking Number
        validation = new Validation("txtBookingNumber", CONTROL_TYPE_TEXTFIELD, "txtBookingNumber", "error", RULE_NONEMPTY, $fn(_endsWith("invalidBookingNumber")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation("txtBookingNumber", CONTROL_TYPE_TEXTFIELD, "txtBookingNumber", "mandatory", RULE_NUMERIC, $fn(_endsWith("invalidBookingNumber")).value, CONST_CUSTOM_FALSE);

        validation = new Validation("txtBookingNumber", CONTROL_TYPE_TEXTFIELD, "txtBookingNumber", "mandatory", RULE_BOOKING_CONFIRMATION_NUMBER, $fn(_endsWith("invalidBookingNumber")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Validate Surname
        validation = new Validation("txtSurname", CONTROL_TYPE_TEXTFIELD, "txtSurname", "error", RULE_NONEMPTY, $fn(_endsWith("invalidSurname")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation("txtSurname", CONTROL_TYPE_TEXTFIELD, "txtSurname", "mandatory", RULE_NAME, $fn(_endsWith("invalidSurname")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
    }
    validator.validateAll();

    if (validator.errorMessages.length > 0) {
        return false;
    }
    //Res2.2.7 - artf1216848 : Scanweb - Incorrect error when searching a destination that is not choosen from list 
    /*else if(!IsDestinationInBookableHotelList(tabValue) && (tabValue == "Tab1"))
    { 
    var errorDiv = $fn(_endsWith('RegClientErrorDiv'));
    var errormsg = $fn(_endsWith('customerMessage')).value;            
    errorDiv.innerHTML = "<div id='errorReport' class='formGroupError'><div class='redAlertIcon'>"+ errormsg +"</div></div>";
    errorDiv.style.visibility     = isVisible;
    errorDiv.style.display        = "block";
    return false;
    }
    else if(!IsDestinationInBookableHotelList(tabValue) && (tabValue == "Tab2"))
    { 
    var errorDiv = $fn(_endsWith('BCClientErrorDiv'));
    var errormsg = $fn(_endsWith('customerMessage')).value;            
    errorDiv.innerHTML = "<div id='errorReport' class='formGroupError'><div class='redAlertIcon'>"+ errormsg +"</div></div>";
    errorDiv.style.visibility     = isVisible;
    errorDiv.style.display        = "block";
    return false;
    }*/
    else if ((tabValue == "Tab1") && (validator.errorMessages.length <= 0))//Child Validation For Regular Booking
    {
        validator = new Validator(PAGE_BOOKING_MODULE_BIG, "RegClientErrorDiv");
        if (!ValidatingAgeAndChildDDl("ddlNoOfRoomsReg", "ddlChildPerRoom", "ddlAdultsPerRoom", "childAgeforRoom", "bedTypeforRoom", "", validator, "RegClientErrorDiv"))
            return false;
        else {
            $fn(_endsWith("loginSrchPageID")).value = $fn(_endsWith("loginSrchID")).value;
            CaptureBedTypes();
            return true;
        }
    }
    else if ((tabValue == "Tab2") && (validator.errorMessages.length <= 0))//Child Validation For Bonus Booking
    {
        validator = new Validator(PAGE_BOOKING_MODULE_BIG, "BCClientErrorDiv");
        if (!ValidatingAgeAndChildDDl("ddlNoOfRoomsBC", "ddlBCChildPerRoom", "ddlBCAdultsPerRoom", "ddlBCChildAgeforRoom", "ddlBCBedTypeforRoom", "BC", validator, "BCClientErrorDiv"))
            return false;
        else {
            $fn(_endsWith("loginSrchPageID")).value = $fn(_endsWith("loginSrchID")).value;
            CaptureBedTypes();
            return true;
        }
    }
    else if ((tabValue == "Tab3") && (validator.errorMessages.length <= 0))//Child Validation For Reward Night Booking
    {
        validator = new Validator(PAGE_BOOKING_MODULE_BIG, "RNClientErrorDiv");
        if (!ValidatingAgeAndChildDDl("ddlNoOfRoomsRed", "ddlRNChildPerRoom", "ddlRNAdultsPerRoom", "ddlRNChildAgeforRoom", "ddlRNBedTypeforRoom", "RN", validator, "RNClientErrorDiv"))
            return false;
        else {
            $fn(_endsWith("loginSrchPageID")).value = $fn(_endsWith("loginSrchID")).value;
            CaptureBedTypes();
            return true;
        }
    }
    else {
        $fn(_endsWith("loginSrchPageID")).value = $fn(_endsWith("loginSrchID")).value;
        CaptureBedTypes();
        return true;
    }
}
//Validation for the Child Details in home Page Accordians
function ValidatingAgeAndChildDDl(ddlNoOfRoom, ddlChildPerRoom, ddlAdultsPerRoom, ddlChildAge, ddlBedTypeForChild, tabPrefix, validator, errorDiv) {
    var validation = null;
    var RoomCount = 0;
    if ($fn(_endsWith(ddlNoOfRoom)) != null) {
        RoomCount = $fn(_endsWith(ddlNoOfRoom)).value;
    }
    for (var rCount = 1; rCount <= RoomCount; rCount++) {
        var CIPBCount = 0;
        var ChildCount = $fn(_endsWith(ddlChildPerRoom + rCount)).value;
        var AdultsCount = $fn(_endsWith(ddlAdultsPerRoom + rCount)).value;
        for (var cCount = 1; cCount <= ChildCount; cCount++) {
            setLabelCss(ddlChildAge + rCount + "Child" + cCount, "fltLft subchild");
            setLabelCss(ddlBedTypeForChild + rCount + "Child" + cCount, "selBedTyp subchild" + tabPrefix + rCount + "" + cCount + "4");
            if (isNaN($fn(_endsWith(ddlChildAge + rCount + "Child" + cCount)).value))//Always a number only the first select age item will be string 
            {
                validation = new Validation(ddlChildAge + rCount + "Child" + cCount, CONTROL_TYPE_DROPDOWN, ddlChildAge + rCount + "Child" + cCount, "fltLft subchild mandatory", RULE_DROPDOWN_SELECTED, $fn(_endsWith("destinationError")).value, CONST_CUSTOM_FALSE);
                validator.addValidation(validation);
            }
            if ($fn(_endsWith(ddlBedTypeForChild + rCount + "Child" + cCount)).value == "")//Either string or empty string
            {
                validation = new Validation(ddlBedTypeForChild + rCount + "Child" + cCount, CONTROL_TYPE_DROPDOWN, ddlBedTypeForChild + rCount + "Child" + cCount, "selBedTyp subchild" + tabPrefix + rCount + "" + cCount + "4 mandatory", RULE_NONEMPTY, $fn(_endsWith("destinationError")).value, CONST_CUSTOM_FALSE);
                validator.addValidation(validation);
            }
        }
    }
    validator.validateAll(); // First validating all Above bed And Age funda                

    if (validator.errorMessages.length > 0) {
        // Shows the error message.
        var errorDiv = $fn(_endsWith(errorDiv));
        var errorMsz = $fn(_endsWith('childAgeNotSelectedError')).value + " " + $fn(_endsWith('ChildBedTypeNotSelectedError')).value;
        errorDiv.innerHTML = "<div id='errorReport' class='formGroupError'><div class='redAlertIcon'>" + errorMsz + "</div></div>";
        errorDiv.style.visibility = isVisible;
        errorDiv.style.display = "block";
        return false;
    }
    //Vrushali | fix 3372992 | Not able to modify a combo reservation with children bed type 'Extra bed'. | P2
    if (isModifyFlowComboReservation == "False" || isModifyFlowComboReservation == "false") {
        if (ChildinAdultBednAdultsCount(RoomCount, ddlChildPerRoom, ddlAdultsPerRoom, ddlChildAge, ddlBedTypeForChild, tabPrefix, validator, errorDiv))
            return true;
        else
            return false;
    }
    else if (isModifyFlowComboReservation == "true" || isModifyFlowComboReservation == "True") {
        if (ChildinAdultBednAdultsCountComboModifyFlow(RoomCount, ddlChildPerRoom, ddlAdultsPerRoom, ddlChildAge, ddlBedTypeForChild, tabPrefix, validator, errorDiv))
            return true;
        else
            return false;

    }


}
//Vrushali | fix 3372992 | Not able to modify a combo reservation with children bed type 'Extra bed'. | P2
function ChildinAdultBednAdultsCountComboModifyFlow(RoomCount, ddlChildPerRoom, ddlAdultsPerRoom, ddlChildAge, ddlBedTypeForChild, tabPrefix, validator, errorDiv) {

    //Child In Adults bed Validation Will Send true if the number of adults in a rooom >=No . selction of bed were we had selected Child in adults bed
    //        var RoomCount=$fn(_endsWith(ddlNoOfRoom)).value;
    for (var rCount = 1; rCount <= RoomCount; rCount++) {
        var CIPBCount = 0;
        var ChildCount = $fn(_endsWith(ddlChildPerRoom + rCount)).value;
        var AdultsCount = $fn(_endsWith(ddlAdultsPerRoom + rCount)).value;
        var isRoomModifiableVal = null;
        var modifiableRoom = $fn(_endsWith("IsRoomModifiable" + rCount));
        if (modifiableRoom != null)
            isRoomModifiableVal = modifiableRoom.value;

        for (var cCount = 1; cCount <= ChildCount; cCount++) {
            if ((modifiableRoom != null) && ((isRoomModifiableVal == "True") || ((isRoomModifiableVal == "")))) {
                if ($fn(_endsWith(ddlBedTypeForChild + rCount + "Child" + cCount)).value == $fn(_endsWith('childInAdultsBedType')).value) {
                    CIPBCount++;
                    if (CIPBCount > AdultsCount) {
                        // Shows the error message.
                        var errorDiv = $fn(_endsWith(errorDiv));
                        var errorMsz = $fn(_endsWith('childInAdultBedandAdultCountError')).value;
                        errorDiv.innerHTML = "<div id='errorReport' class='formGroupError'><div class='redAlertIcon'>" + errorMsz + "</div></div>";
                        errorDiv.style.visibility = isVisible;
                        errorDiv.style.display = "block";
                        return false;
                    }
                }
            }
        }
    }
    return true;
}




function ChildinAdultBednAdultsCount(RoomCount, ddlChildPerRoom, ddlAdultsPerRoom, ddlChildAge, ddlBedTypeForChild, tabPrefix, validator, errorDiv) {

    //Child In Adults bed Validation Will Send true if the number of adults in a rooom >=No . selction of bed were we had selected Child in adults bed
    //        var RoomCount=$fn(_endsWith(ddlNoOfRoom)).value;
    for (var rCount = 1; rCount <= RoomCount; rCount++) {
        var CIPBCount = 0;
        var ChildCount = $fn(_endsWith(ddlChildPerRoom + rCount)).value;
        var AdultsCount = $fn(_endsWith(ddlAdultsPerRoom + rCount)).value;
        for (var cCount = 1; cCount <= ChildCount; cCount++) {
            if ($fn(_endsWith(ddlBedTypeForChild + rCount + "Child" + cCount)).value == $fn(_endsWith('childInAdultsBedType')).value) {
                CIPBCount++;
                if (CIPBCount > AdultsCount) {
                    // Shows the error message.
                    var errorDiv = $fn(_endsWith(errorDiv));
                    var errorMsz = $fn(_endsWith('childInAdultBedandAdultCountError')).value;
                    errorDiv.innerHTML = "<div id='errorReport' class='formGroupError'><div class='redAlertIcon'>" + errorMsz + "</div></div>";
                    errorDiv.style.visibility = isVisible;
                    errorDiv.style.display = "block";
                    return false;
                }
            }
        }
    }
    return true;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Booking Module Big : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function AttachValidation(hotelNameField, arrivalDateField, departureDateField, noOfNightsField, bookingCodeField, validator) {
    // Destination name
    //RK: Reservation 2.0| Adding null check as same method is used in getting validation for modifyshoppingcart
    if (hotelNameField != null) {
        validation = new Validation(hotelNameField, CONTROL_TYPE_TEXTFIELD, hotelNameField, "stayInputLft widthChng input mandatory", RULE_NONEMPTY, $fn(_endsWith("destinationError")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        //validation              = new Validation(hotelNameField, CONTROL_TYPE_TEXTFIELD, hotelNameField, "stayInputLft widthChng input error", RULE_NOTANUMBER, $fn(_endsWith("destinationError")).value, CONST_CUSTOM_FALSE);
        //validator.addValidation(validation);
        validation = new Validation(hotelNameField, CONTROL_TYPE_TEXTFIELD, hotelNameField, "input", RULE_DESTINATION, $fn(_endsWith("destinationError")).value, CONST_CUSTOM_TRUE, "validateDestinationCustom('" + hotelNameField + "','selectedDestId')");
        validator.addValidation(validation);
        validation = new Validation(hotelNameField, CONTROL_TYPE_TEXTFIELD, hotelNameField, "stayInputLft widthChng input error", RULE_DUPLICATE_DESTINATION, $fn(_endsWith("duplicateDestinationError")).value, CONST_CUSTOM_TRUE, "validateDestinationDuplicate('" + hotelNameField + "','selectedDestId')");
        validator.addValidation(validation);

        validation = new Validation(hotelNameField, CONTROL_TYPE_TEXTFIELD, hotelNameField, "stayInputLft widthChng input mandatory", RULE_NONEMPTY, $fn(_endsWith("destinationError")).value, CONST_CUSTOM_TRUE, "validateDefaultHotelText('" + hotelNameField + "')");
        validator.addValidation(validation);

    }
    // Arrival Date
    validation = new Validation(arrivalDateField, CONTROL_TYPE_TEXTFIELD, arrivalDateField, "dateRange txtArrivalDate mandatory", RULE_NONEMPTY, $fn(_endsWith("arrivaldateError")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation(arrivalDateField, CONTROL_TYPE_TEXTFIELD, arrivalDateField, "dateRange txtArrivalDate error", RULE_DATE, $fn(_endsWith("arrivaldateError")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Departure Date
    validation = new Validation(departureDateField, CONTROL_TYPE_TEXTFIELD, departureDateField, "dateRange txtDepartureDate mandatory", RULE_NONEMPTY, $fn(_endsWith("departuredateError")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation(departureDateField, CONTROL_TYPE_TEXTFIELD, departureDateField, "dateRange txtDepartureDate error", RULE_DATE, $fn(_endsWith("departuredateError")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Validate Departure date greater than Arrival date
    //            RemoveClassName(arrivalDateField, "errorText");
    //            RemoveClassName(departureDateField, "errorText");
    var arrDate = $fn(_endsWith(arrivalDateField)).value;
    var depDate = $fn(_endsWith(departureDateField)).value;
    validation = new Validation(departureDateField, CONTROL_TYPE_TEXTFIELD, departureDateField, "dateRange txtDepartureDate error", null, $fn(_endsWith("departuredateError")).value, CONST_CUSTOM_TRUE, "ValidateDepartureDateGreaterThanArrivalDate('" + arrDate + "', '" + depDate + "')");
    validator.addValidation(validation);

    //RK: Reservation 2.0| Adding null check as same method is used in getting validation for modifyshoppingcart    
    if (noOfNightsField != null) {   // No of Nights

        validation = new Validation(noOfNightsField, CONTROL_TYPE_TEXTFIELD, noOfNightsField, "mandatory", RULE_NONEMPTY, $fn(_endsWith("NoOfNightError")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation(noOfNightsField, CONTROL_TYPE_TEXTFIELD, noOfNightsField, "error", RULE_NUMERIC, $fn(_endsWith("NoOfNightError")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation(noOfNightsField, CONTROL_TYPE_TEXTFIELD, noOfNightsField, "error", RULE_POSITIVE, $fn(_endsWith("NoOfNightError")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation(noOfNightsField, CONTROL_TYPE_TEXTFIELD, noOfNightsField, "error", RULE_NON_ZERO, $fn(_endsWith("NoOfNightError")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        var noOfNights = $fn(_endsWith(noOfNightsField)).value;
        validation = new Validation(noOfNightsField, CONTROL_TYPE_TEXTFIELD, noOfNightsField, "error", RULE_NO_OF_NIGHTS_1_TO_99, $fn(_endsWith("NoOfNightError")).value, CONST_CUSTOM_TRUE, "validateNoOfNights1To99('" + noOfNights + "')");
        validator.addValidation(validation);
    }

    if (bookingCodeField != null && $fn(_endsWith(bookingCodeField)) != null) {
        var bookingCode = $fn(_endsWith(bookingCodeField)).value;
        var exists = bookingCode.search('/');
        if (bookingCode != null && bookingCode.length > 0 && exists >= 0) {
            var splitBookingCode = bookingCode.split('/');
            $fn(_endsWith("IATANumber")).value = splitBookingCode[1];
            $fn(_endsWith("BookingCodeIata")).value = splitBookingCode[0];
            bookingCodeField = $fn(_endsWith("BookingCodeIata")).id
        }

        var element = bookingCode.substring(1, 0);
        //Boxi: Defect fix for validating booking code if user has entered any value
        if (bookingCode != $fn(_endsWith("defaultBookingCode")).value) {

            if (element == "B" || element == "b") {
                //Boxi : Added special charecter validation in Booking Code field
                validation = new Validation(bookingCodeField, CONTROL_TYPE_TEXTFIELD, bookingCodeField, "normalTextCont input defaultColor", RULE_ALPHANUMERIC_NORDIC, $fn(_endsWith("invalidBlockCodeChars")).value, CONST_CUSTOM_FALSE);
                validator.addValidation(validation);

                validation = new Validation(bookingCodeField, CONTROL_TYPE_TEXTFIELD, bookingCodeField, "normalTextCont input defaultColor", RULE_BLOCK_CODE, $fn(_endsWith("invalidBlockCode")).value, CONST_CUSTOM_FALSE);
                validator.addValidation(validation);


            }
            else {
                //Boxi : Added special charecter validation in Booking Code field
                validation = new Validation(bookingCodeField, CONTROL_TYPE_TEXTFIELD, bookingCodeField, "normalTextCont input defaultColor", RULE_ALPHANUMERIC_NORDIC, $fn(_endsWith("invalidBlockCodeChars")).value, CONST_CUSTOM_FALSE);
                validator.addValidation(validation);


            }
            if (exists >= 0) {
                validation = new Validation($fn(_endsWith("IATANumber")).id, CONTROL_TYPE_TEXTFIELD, $fn(_endsWith("IATANumber")).id, "normalTextCont input defaultColor", RULE_NUMERIC_IATA, $fn(_endsWith("InvalidIATAnumber")).value, CONST_CUSTOM_FALSE);
                validator.addValidation(validation);
            }
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Contact Us : START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function validation_ContactUs() {
    var validator = null;
    var validation = null;

    validator = new Validator(PAGE_CONTACT_US, "ContactusErrorDiv");

    validation = new Validation("txtComposedMessage", CONTROL_TYPE_TEXTFIELD, null, "errorText", RULE_NONEMPTY, $fn(_endsWith("contactMessageError")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validator.validateAll();

    if (validator.errorMessages.length > 0)
        return false;
    else
        return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Contact Us : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Forgotten Password : START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function validation_ForgottenPassword() {
    var validator = null;
    var validation = null;

    validator = new Validator(PAGE_FORGOTTEN_PASSWORD, "ForgotPasswordErrorDiv");

    validation = new Validation("txtMembershipNumber", CONTROL_TYPE_TEXTFIELD, "spanFPFrequentGuestProgramme", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidprogNo")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtMembershipNumber", CONTROL_TYPE_TEXTFIELD, "spanFPFrequentGuestProgramme", "errorText", RULE_MEMBERSHIP_NUM, $fn(_endsWith("invalidprogNo")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    //CR 9 | Forgotten PIN | Release 1.4
    //Set the following span back to normal color.
    setLabelCss("SpanFPQuestion", CONST_EMPTY_STRING);
    setLabelCss("spanFPAnswer", CONST_EMPTY_STRING);
    validator.validateAll();

    if (validator.errorMessages.length > 0)
        return false;
    else
        return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Forgotten Password : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Login Error : START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function validation_LoginError() {
    var validator = null;
    var validation = null;

    validator = new Validator(PAGE_LOGIN_ERROR, "LoginErrorDiv");

    validation = new Validation("txtLoginErrorUserName", CONTROL_TYPE_TEXTFIELD, "spanLEUserName", "errorText", RULE_NONEMPTY, $fn(_endsWith("LoginErrorUserNameInvalid")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtLoginErrorUserName", CONTROL_TYPE_TEXTFIELD, "spanLEUserName", "errorText", RULE_MEMBERSHIP_NUM, $fn(_endsWith("LoginErrorUserNameInvalid")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtLoginErrorPassword", CONTROL_TYPE_TEXTFIELD, "spanLEPassword", "errorText", RULE_NONEMPTY, $fn(_endsWith("LoginErrorPasswordInvalid")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validator.validateAll();

    if (validator.errorMessages.length > 0) {
        return false;
    }
    else {
        $fn(_endsWith("loginErrPageID")).value = $fn(_endsWith("loginErrID")).value;
        return true;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Login Error : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Login control in Booking detail page: START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// R1.3 | CR5 | StoryId: | Login on Booking Detail Screen.
function validation_BookingDetailLogin() {
    var validator = null;
    var validation = null;

    validator = new Validator(PAGE_LOGIN_BOOKING_DETAIL, "LoginErrorDiv");

    validation = new Validation("txtBDLoginUserName", CONTROL_TYPE_TEXTFIELD, "spanBDLoginUserName", "errorText", RULE_NONEMPTY, $fn(_endsWith("BDLoginUserNameInvalid")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtBDLoginUserName", CONTROL_TYPE_TEXTFIELD, "spanBDLoginUserName", "errorText", RULE_MEMBERSHIP_NUM, $fn(_endsWith("BDLoginUserNameInvalid")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtBDLoginPassword", CONTROL_TYPE_TEXTFIELD, "spanBDLoginPassword", "errorText", RULE_NONEMPTY, $fn(_endsWith("BDLoginPasswordInvalid")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validator.validateAll();

    if (validator.errorMessages.length > 0) {
        //        var errDiv = $fn(_endsWith("BDLoginErrorDiv"));
        //        if (errDiv != null)
        //            errDiv.style.visibility = 'hidden';        
        return false;
    }
    else {
        $fn(_endsWith("loginBookingDetailPage")).value = $fn(_endsWith("loginBookingDetail")).value;
        return true;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Login control in Booking detail page: END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Login Status : START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function validation_LoyalityLogin() {
    var validator = null;
    var validation = null;

    validator = new Validator(PAGE_LOGIN_STATUS, "loyaltyLoginErrDiv");

    validation = new Validation("txtLoyaltyUsername", CONTROL_TYPE_TEXTFIELD, "spanUserName", "errorText", RULE_NONEMPTY, $fn(_endsWith("LoyaltyUserNameInvalid")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtLoyaltyUsername", CONTROL_TYPE_TEXTFIELD, "spanUserName", "errorText", RULE_MEMBERSHIP_NUM, $fn(_endsWith("LoyaltyUserNameInvalid")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtLoyaltyPassword", CONTROL_TYPE_TEXTFIELD, "spanPassword", "errorText", RULE_NONEMPTY, $fn(_endsWith("LoyaltyUserPasswordInvalid")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validator.validateAll();

    if (validator.errorMessages.length > 0) {
        return false;
    }
    else {
        $fn(_endsWith("loginPopUpPageID")).value = $fn(_endsWith("loginPopUpID")).value;
        return true;
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Login Status : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Lost Card-NoLogin : START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function validation_Lostcardwithoutlogin() {
    var validator = null;
    var validation = null;

    validator = new Validator(PAGE_LOSTCARD_NOLOGIN, "errMessageDivLostcardWithoutlogin");

    validation = new Validation("txtFirstName", CONTROL_TYPE_TEXTFIELD, "spanLCFirstName", "errorText", RULE_NONEMPTY, $fn(_endsWith("lcwolFirstname")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtFirstName", CONTROL_TYPE_TEXTFIELD, "spanLCFirstName", "errorText", RULE_NAME, $fn(_endsWith("lcwolFirstname")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtLastName", CONTROL_TYPE_TEXTFIELD, "spanLCLastName", "errorText", RULE_NONEMPTY, $fn(_endsWith("lcwolLastname")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtLastName", CONTROL_TYPE_TEXTFIELD, "spanLCLastName", "errorText", RULE_NAME, $fn(_endsWith("lcwolLastname")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Email
    validation = new Validation("txtEMail", CONTROL_TYPE_TEXTFIELD, "spanLCEMail", "errorText", RULE_EMAIL, $fn(_endsWith("lcwolEmail")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    //R1.3 | CR16 | Loyalty Enhancement
    //Validation added for phone number mandatory
    // Telephone
    validation = new Validation("ddlTelephone", CONTROL_TYPE_TEXTFIELD, "spanLCPhone", "errorText", RULE_DROPDOWN_WITH_SEPARATOR, $fn(_endsWith("lcwolPhoneCode")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtHomePhone", CONTROL_TYPE_TEXTFIELD, "spanLCPhone", "errorText", RULE_NONEMPTY, $fn(_endsWith("lcwolPhone")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtHomePhone", CONTROL_TYPE_TEXTFIELD, "spanLCPhone", "errorText", RULE_NUMERIC_SPACE, $fn(_endsWith("lcwolPhone")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);


    validation = new Validation("txtAddressLine1", CONTROL_TYPE_TEXTFIELD, "spanLCAddressline1", "errorText", RULE_NONEMPTY, $fn(_endsWith("lcwolAddressline1")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtAddressLine1", CONTROL_TYPE_TEXTFIELD, "spanLCAddressline1", "errorText", RULE_ALPHANUMERIC_NORDIC_WITH_SPECIAL_CHARACTER, $fn(_endsWith("lcwolAddressline1")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtAddressLine2", CONTROL_TYPE_TEXTFIELD, "spanLCAddressline2", "errorText", RULE_ALPHANUMERIC_NORDIC_WITH_SPECIAL_CHARACTER, $fn(_endsWith("lcwolAddressline2")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtPostCode", CONTROL_TYPE_TEXTFIELD, "spanLCPostcode", "errorText", RULE_NONEMPTY, $fn(_endsWith("lcwolPostcode")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    /*validation              = new Validation("txtPostCode", CONTROL_TYPE_TEXTFIELD, "spanLCPostcode", "errorText", RULE_ALPHANUMERIC, $fn(_endsWith("lcwolPostcode")).value, CONST_CUSTOM_FALSE);    
    validator.addValidation(validation);*/

    validation = new Validation("txtCity", CONTROL_TYPE_TEXTFIELD, "spanLCCity", "errorText", RULE_NONEMPTY, $fn(_endsWith("lcwolCitytown")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    //artf1262238 : Error message is not displayed in City/Town field 
    validation = new Validation("txtCity", CONTROL_TYPE_TEXTFIELD, "spanLCCity", "errorText", RULE_ALPHANUMERIC_NORDIC_WITH_SPECIAL_CHARACTER, $fn(_endsWith("lcwolCitytown")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtGuestNumber", CONTROL_TYPE_TEXTFIELD, "spanLCGuestNo", "errorText", RULE_MEMBERSHIP_NUM, $fn(_endsWith("lcwolMembershipNumber")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("ddlCountry", CONTROL_TYPE_TEXTFIELD, "spanLCCountry", "errorText", RULE_DROPDOWN_WITH_SEPARATOR, $fn(_endsWith("lcwolCountry")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validator.validateAll();

    if (validator.errorMessages.length > 0)
        return false;
    else
        return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Lost Card-NoLogin : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Lost Missing Point : START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function validation_MissingPoints() {

    var validator = null;
    var validation = null;

    validator = new Validator(PAGE_MISSING_POINTS, "clientErrorDiv1");

    // Delete Departure date error class if exists
    var spnDepDate = $fn(_endsWith("spanDepartureDate1"));
    if (spnDepDate == null)
        spnDepDate = $fn("spanDepartureDate1");
    RemoveClassName(spnDepDate, "errorText");

    // Arrival Date
    validation = new Validation("txtArrivalDate1", CONTROL_TYPE_TEXTFIELD, "spanArrivalDate1", "errorText", RULE_NONEMPTY, $fn(_endsWith("arrivaldateError1")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Departure Date
    validation = new Validation("txtDepartureDate1", CONTROL_TYPE_TEXTFIELD, "spanDepartureDate1", "errorText", RULE_NONEMPTY, $fn(_endsWith("departuredateError1")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Validate Departure date greater than Arrival date

    var arrDate = $fn(_endsWith("txtArrivalDate1")).value;
    var depDate = $fn(_endsWith("txtDepartureDate1")).value;
    validation = new Validation("txtArrivalDate1", CONTROL_TYPE_TEXTFIELD, "spanArrivalDate1", "errorText", null, $fn(_endsWith("arrivaldateError1")).value, CONST_CUSTOM_TRUE, "ValidateDepartureDateGreaterThanArrivalDate('" + arrDate + "', '" + depDate + "')");
    validator.addValidation(validation);

    // Validate reservation number
    validation = new Validation("txtReservation", CONTROL_TYPE_TEXTFIELD, "spanReservation", "errorText", RULE_NUMERIC, $fn(_endsWith("MissingpointsReservationError")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Validate city
    validation = new Validation("ddlCity", CONTROL_TYPE_DROPDOWN, "spanCity", "errorText", RULE_DROPDOWN_SELECTED, $fn(_endsWith("MissingpointsCityError")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);


    // Validate Hotel selected
    validation = new Validation("ddlHotel", CONTROL_TYPE_DROPDOWN, "spanHotel", "errorText", RULE_DROPDOWN_SELECTED, $fn(_endsWith("MissingpointsHotelError")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Validate for duplicate stays
    var bookingNum = $fn(_endsWith("txtReservation")).value;
    var arrivalDt = $fn(_endsWith("txtArrivalDate1")).value;
    var departureDt = $fn(_endsWith("txtDepartureDate1")).value;
    var city = "";
    var hotel = "";

    if ($fn(_endsWith("ddlCity")).length > 0) {
        city = $fn(_endsWith("ddlCity")).options[$fn(_endsWith("ddlCity")).selectedIndex].text;
    }

    if ($fn(_endsWith("ddlHotel")).length > 0) {
        hotel = $fn(_endsWith("ddlHotel")).options[$fn(_endsWith("ddlHotel")).selectedIndex].text;
    }

    validation = new Validation("txtArrivalDate1", CONTROL_TYPE_TEXTFIELD, "spanArrivalDate1;spanDepartureDate1", "errorText", null, $fn(_endsWith("DuplicateStay")).value, CONST_CUSTOM_TRUE, "ValidateForDuplicateStays('" + bookingNum + "', '" + arrivalDt + "', '" + departureDt + "', '" + city + "', '" + hotel + "')");
    validator.addValidation(validation);

    validator.validateAll();

    if (validator.errorMessages.length > 0)
        return false;
    else
        return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Lost Missing Point : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Enroll Loyalty : START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function requiredToValidateCreditCard() {
    if ($fn(_endsWith("txtCardHolder")) != null && $fn(_endsWith("txtCardNumber")) != null
              && $fn(_endsWith("ddlCardType")) != null) {
        if (Trim($fn(_endsWith("txtCardHolder")).value) != "" ||
            Trim($fn(_endsWith("txtCardNumber")).value) != "" ||
            $fn(_endsWith("ddlCardType")).selectedIndex > 0) {
            return true;
        }
        else {
            return false;
        }
    }
}

function checkIfPrefPartnerProgNotSelected(selectCtrl) {
    if ('DFT' == selectCtrl[0].options[selectCtrl[0].selectedIndex].value) {
        for (var count = 1; count < selectCtrl.length; count++) {
            if ('DFT' != selectCtrl[count].options[selectCtrl[count].selectedIndex].value) {
                return false;
            }
        }
    }
    return true;
}

function pushEntityToHiddenField(selectCtrl) {
    var textCtrl = document.getElementsByName("memberNo");
    var val = "";
    for (var count = 0; count < selectCtrl.length; count++) {
        val += selectCtrl[count].options[selectCtrl[count].selectedIndex].value + ',' + textCtrl[count].value + '%';
    }
    $fn(_endsWith("selectedPartnerProgramValues")).value = val;
}

function validation_EnrollLoyalty() {
    var validator = null;
    var validation = null;

    validator = new Validator(PAGE_ENROLL_GUEST, "clientErrorDivEL");

    //First track enrollment code
    //Release 1.5 | artf809466 | FGP � Fast Track Enrolment
    if (requiredToValidateCampaignCode()) {
        validation = new Validation("txtCampaignCode", CONTROL_TYPE_TEXTFIELD, "spanCampaignCode", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidCampaignCode")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation("txtCampaignCode", CONTROL_TYPE_TEXTFIELD, "spanCampaignCode", "errorText", null, $fn(_endsWith("invalidCampaignCode")).value, CONST_CUSTOM_TRUE, "ValidateCampaignCode('" + $fn(_endsWith("txtCampaignCode")).value + "', '" + $fn(_endsWith("hdnCampaignCode")).value + "')");
        validator.addValidation(validation);
    }
    // First Name
    validation = new Validation("txtFName", CONTROL_TYPE_TEXTFIELD, "lblFname", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidFirstName")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtFName", CONTROL_TYPE_TEXTFIELD, "lblFname", "errorText", RULE_NAME, $fn(_endsWith("invalidFirstName")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    //validation              = new Validation("txtFName", CONTROL_TYPE_TEXTFIELD, "lblFname", "errorText", RULE_NAME_RESTRICT_SPL_CHR_FOR_NAMES, $fn(_endsWith("invalidFirstName")).value, CONST_CUSTOM_FALSE);
    //validator.addValidation(validation);

    // Last Name
    validation = new Validation("txtLName", CONTROL_TYPE_TEXTFIELD, "lblLname", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidLastName")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtLName", CONTROL_TYPE_TEXTFIELD, "lblLname", "errorText", RULE_NAME, $fn(_endsWith("invalidLastName")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Date of Birth
    var indexPosDD = $fn(_endsWith("ddlDOBDay")).selectedIndex;
    var indexPosMM = $fn(_endsWith("ddlDOBMonth")).selectedIndex;
    var indexPosYY = $fn(_endsWith("ddlDOBYear")).selectedIndex;
    validation = new Validation("ddlDOBDay", CONTROL_TYPE_DROPDOWN, "lblDateOfBirth", "errorText", RULE_DATE_OF_BIRTH, $fn(_endsWith("invalidDateofBirth")).value, CONST_CUSTOM_TRUE, "validateDateOfBirth(" + indexPosDD + ", " + indexPosMM + ", " + indexPosYY + ")");
    validator.addValidation(validation);

    // Email
    validation = new Validation("txtEmail1", CONTROL_TYPE_TEXTFIELD, "lblEmail", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidEmail")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtEmail1", CONTROL_TYPE_TEXTFIELD, "lblEmail", "errorText", RULE_EMAIL, $fn(_endsWith("invalidEmail")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Mobile
    validation = new Validation("ddlTel2", CONTROL_TYPE_TEXTFIELD, "lblMobilePhone", "errorText", RULE_DROPDOWN_WITH_SEPARATOR, $fn(_endsWith("invalidMobilePhoneCode")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtTelephone2", CONTROL_TYPE_TEXTFIELD, "lblMobilePhone", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidTelephone2")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtTelephone2", CONTROL_TYPE_TEXTFIELD, "lblMobilePhone", "errorText", RULE_NUMERIC_SPACE, $fn(_endsWith("invalidMobile")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Landline
    areaCode = $fn(_endsWith("ddlTel1")).value;
    telNo = $fn(_endsWith("txtTelephone1")).value;

    //if area code is default value (select) and telephone number is empty,
    //no validation should happens for landline code and number since it is not a mandatory
    //else 
    //validate them accordingly

    if (!validateDropdownDefault(areaCode) || validateNonEmpty(telNo)) {
        validation = new Validation("ddlTel1", CONTROL_TYPE_TEXTFIELD, "lblEnrollLandline", "errorText", RULE_DROPDOWN_WITH_SEPARATOR, $fn(_endsWith("invalidHomePhoneCode")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        validation = new Validation("txtTelephone1", CONTROL_TYPE_TEXTFIELD, "lblEnrollLandline", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidTelephone1")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        validation = new Validation("txtTelephone1", CONTROL_TYPE_TEXTFIELD, "lblEnrollLandline", "errorText", RULE_NUMERIC_SPACE, $fn(_endsWith("invalidTelephone1")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
    }
    else {
        setLabelCss("lblEnrollLandline", CONST_EMPTY_STRING);
    }


    // AddressLine1
    validation = new Validation("txtAddressLine1", CONTROL_TYPE_TEXTFIELD, "lblAddressLine1", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidaddressLine1")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtAddressLine1", CONTROL_TYPE_TEXTFIELD, "lblAddressLine1", "errorText", RULE_ALPHANUMERIC_NORDIC_WITH_SPECIAL_CHARACTER, $fn(_endsWith("invalidaddressLine1")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // AddressLine2
    validation = new Validation("txtAddressLine2", CONTROL_TYPE_TEXTFIELD, "lblAddressLine2", "errorText", RULE_ALPHANUMERIC_NORDIC_WITH_SPECIAL_CHARACTER, $fn(_endsWith("invalidaddressLine2")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // City Or Town
    validation = new Validation("txtCityOrTown", CONTROL_TYPE_TEXTFIELD, "lblCityTown", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidCityTown")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtCityOrTown", CONTROL_TYPE_TEXTFIELD, "lblCityTown", "errorText", RULE_ALPHANUMERIC_NORDIC, $fn(_endsWith("invalidCityTown")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Post Code
    validation = new Validation("txtPostCode", CONTROL_TYPE_TEXTFIELD, "lblPostcode", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidPostcode")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    //validation              = new Validation("txtPostCode", CONTROL_TYPE_TEXTFIELD, "lblPostcode", "errorText", RULE_ALPHANUMERIC, $fn(_endsWith("invalidPostcode")).value, CONST_CUSTOM_FALSE);    
    //validator.addValidation(validation);

    // Country
    validation = new Validation("ddlCountry", CONTROL_TYPE_DROPDOWN, "lblCountry", "errorText", RULE_DROPDOWN_WITH_SEPARATOR, $fn(_endsWith("invalidCountryCode")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Creditcard
    //R1.5 | Artifact artf809436 : FGP - Update profile CC field 
    if (requiredToValidateCreditCard()) {
        // Card holder name
        validation = new Validation("txtCardHolder", CONTROL_TYPE_TEXTFIELD, "lblCardHolder", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidCardHolder")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation("txtCardHolder", CONTROL_TYPE_TEXTFIELD, "lblCardHolder", "errorText", RULE_NAME, $fn(_endsWith("invalidCardHolder")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Card type
        validation = new Validation("ddlCardType", CONTROL_TYPE_DROPDOWN, "lblCardType", "errorText", RULE_DROPDOWN_SELECTED, $fn(_endsWith("invalidCardType")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Card number
        validation = new Validation("txtCardNumber", CONTROL_TYPE_TEXTFIELD, "lblCardNumber", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidCardNumber")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation("txtCardNumber", CONTROL_TYPE_TEXTFIELD, "lblCardNumber", "errorText", RULE_NUMERIC, $fn(_endsWith("invalidCardNumber")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Card expiry date
        var expiryMonth = $fn(_endsWith("ddlExpiryMonth")).value;
        var expiryYear = $fn(_endsWith("ddlExpiryYear")).value;
        validation = new Validation("ddlExpiryMonth", CONTROL_TYPE_DROPDOWN, "lblExpiryDate", "errorText", RULE_CREDITCARD_EXPIRY, $fn(_endsWith("invalidExpiryDate")).value, CONST_CUSTOM_TRUE, "validateCreditCardExpiry('" + expiryMonth + "', '" + expiryYear + "')");
        validator.addValidation(validation);
    }
    else {
        setLabelCss("lblCardHolder", CONST_EMPTY_STRING);
        setLabelCss("lblCardType", CONST_EMPTY_STRING);
        setLabelCss("lblCardNumber", CONST_EMPTY_STRING);
        setLabelCss("lblExpiryDate", CONST_EMPTY_STRING);
    }
    //START:Release 1.5 | artf809605 | FGP - add more than one partner preference
    var ctrl = document.getElementsByName("selectPgm");
    var textCtrl = document.getElementsByName("memberNo");
    for (var controlCount = 0; controlCount < ctrl.length; controlCount++) {
        // Partner program
        var partnerProgramIndex = ctrl[controlCount].selectedIndex;
        var partnerProgramAcctNo = textCtrl[controlCount].value.replace(/'/g, "\\\'");

        validation = new Validation(ctrl[controlCount], CONTROL_TYPE_DROPDOWN, "lblPartnerProgram" + controlCount, "errorText", RULE_PARTNER_PROGRAM, $fn(_endsWith("invalidPartnerProgram")).value, CONST_CUSTOM_TRUE, "validatePartnerProgram(" + partnerProgramIndex + ", '" + partnerProgramAcctNo + "')");
        validator.addValidation(validation);

        // Partner program account no
        validation = new Validation(textCtrl[controlCount], CONTROL_TYPE_TEXTFIELD, "lblAccountNo" + controlCount, "errorText", RULE_PARTNER_PROGRAM_ACCT_NO, $fn(_endsWith("invalidAccountNo")).value, CONST_CUSTOM_TRUE, "validatePartnerProgramAcctNo(" + partnerProgramIndex + ", '" + partnerProgramAcctNo + "')");
        validator.addValidation(validation);
    }

    var selectCtrl = document.getElementsByName("selectPgm");
    var validaterCounter = 0;
    if (checkIfPrefPartnerProgNotSelected(selectCtrl)) {
        for (var selectCtrlCount = 0; selectCtrlCount < selectCtrl.length - 1; selectCtrlCount++) {
            for (var innerCounter = selectCtrlCount + 1; innerCounter < selectCtrl.length; innerCounter++) {
                //R1.5 | artf837495: Add Partner Preference||Error message is displayed if use leave partner preference blank
                if ((selectCtrl[selectCtrlCount].options[selectCtrl[selectCtrlCount].selectedIndex].value == selectCtrl[innerCounter].options[selectCtrl[innerCounter].selectedIndex].value)
                        && ('DFT' != selectCtrl[selectCtrlCount].options[selectCtrl[selectCtrlCount].selectedIndex].value)
                        && ('DFT' != selectCtrl[innerCounter].options[selectCtrl[innerCounter].selectedIndex].value)) {
                    // If it is the first validator then attach the error message               
                    if (validaterCounter == 0) {
                        validation = new Validation(selectCtrl[selectCtrlCount], CONTROL_TYPE_DROPDOWN, "lblPartnerProgram" + selectCtrlCount, "errorText", RULE_FAILED_VALIDATION, $fn(_endsWith("uniquePartnerProg")).value, CONST_CUSTOM_FALSE);
                        validator.addValidation(validation);
                    }
                    // for the other validators we are not attaching the error message because same error message will go for all the validators here
                    // there for avoiding duplicates error messages we are attaching error message only for first validator
                    else {
                        validation = new Validation(selectCtrl[selectCtrlCount], CONTROL_TYPE_DROPDOWN, "lblPartnerProgram" + selectCtrlCount, "errorText", RULE_FAILED_VALIDATION, null, CONST_CUSTOM_FALSE);
                        validator.addValidation(validation);
                    }
                    validation = new Validation(selectCtrl[innerCounter], CONTROL_TYPE_DROPDOWN, "lblPartnerProgram" + innerCounter, "errorText", RULE_FAILED_VALIDATION, null, CONST_CUSTOM_FALSE);
                    validator.addValidation(validation);
                    validaterCounter++;
                }
            }
        }
    }
    else {
        var controlCount = 0;
        validation = new Validation(selectCtrl[controlCount], CONTROL_TYPE_DROPDOWN, "lblPartnerProgram" + controlCount, "errorText", RULE_FAILED_VALIDATION, $fn(_endsWith("choosePreferred")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        validation = new Validation(textCtrl[controlCount], CONTROL_TYPE_TEXTFIELD, "lblAccountNo" + controlCount, "errorText", RULE_FAILED_VALIDATION, null, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
    }

    //END:Release 1.5 | artf809605 | FGP - add more than one partner preference

    // Password
    validation = new Validation("txtEnrollPassword", CONTROL_TYPE_TEXTFIELD, "lblPassword", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidPassword")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Retype Password
    validation = new Validation("txtEnrollReTypePassword", CONTROL_TYPE_TEXTFIELD, "lblRetypePassword", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidRetypePassword")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    var password = $fn(_endsWith("txtEnrollPassword")).value.replace(/'/g, "\\\'");
    var retypePassword = $fn(_endsWith("txtEnrollReTypePassword")).value.replace(/'/g, "\\\'");
    // Validate for dollar sign    
    validation = new Validation("txtEnrollPassword", CONTROL_TYPE_TEXTFIELD, "lblPassword", "errorText", RULE_PASSWORDNODOLLARSIGN, $fn(_endsWith("invalidPasswordDollarSign")).value, CONST_CUSTOM_TRUE, "validatePasswordForDollarSign('" + password + "')");
    validator.addValidation(validation);
    validation = new Validation("txtEnrollReTypePassword", CONTROL_TYPE_TEXTFIELD, "lblRetypePassword", "errorText", RULE_PASSWORDNODOLLARSIGN, $fn(_endsWith("invalidPasswordDollarSign")).value, CONST_CUSTOM_TRUE, "validatePasswordForDollarSign('" + retypePassword + "')");
    validator.addValidation(validation);

    //Validate for nordic characters Artifact artf1072335 : Scanweb - workaround for double byte chars in password and answer fields 
    validation = new Validation("txtEnrollPassword", CONTROL_TYPE_TEXTFIELD, "lblPassword", "errorText", RULE_PASSWORDNONORDICCHAR, $fn(_endsWith("invalidPasswordNordicChar")).value, CONST_CUSTOM_TRUE, "validateNordicCharacters('" + password + "')");
    validator.addValidation(validation);
    validation = new Validation("txtEnrollReTypePassword", CONTROL_TYPE_TEXTFIELD, "lblRetypePassword", "errorText", RULE_PASSWORDNONORDICCHAR, $fn(_endsWith("invalidPasswordNordicChar")).value, CONST_CUSTOM_TRUE, "validateNordicCharacters('" + retypePassword + "')");
    validator.addValidation(validation);

    // Validate for password match
    validation = new Validation("txtEnrollReTypePassword", CONTROL_TYPE_TEXTFIELD, "lblRetypePassword", "errorText", RULE_PASSWORDMATCH, $fn(_endsWith("invalidPasswordMatch")).value, CONST_CUSTOM_TRUE, "validatePasswordMatch('" + password + "', '" + retypePassword + "')");
    validator.addValidation(validation);

    // SecurityAnswer
    var securityQuestion = $fn(_endsWith("ddlPasswordQuestion")).selectedIndex;
    var securityAnswer = $fn(_endsWith("txtAnswer")).value.replace(/'/g, "\\\'");
    validation = new Validation("txtAnswer", CONTROL_TYPE_TEXTFIELD, "lblAnswer", "errorText", RULE_SECURITY_ANSWER, $fn(_endsWith("invalidSecurityAnswer")).value, CONST_CUSTOM_TRUE, "validateSecurityAnswer(" + securityQuestion + ", '" + securityAnswer + "')");
    validator.addValidation(validation);

    //Artifact artf1072335 : Scanweb - workaround for double byte chars in password and answer fields 
    validation = new Validation("txtAnswer", CONTROL_TYPE_TEXTFIELD, "lblAnswer", "errorText", RULE_PASSWORDNONORDICCHAR, $fn(_endsWith("invalidSecretAnswerNordicChar")).value, CONST_CUSTOM_TRUE, "validateNordicCharacters('" + securityAnswer + "')");
    validator.addValidation(validation);

    //Defect Fix - Artifact artf652899(R1.3)
    validation = new Validation("txtAnswer", CONTROL_TYPE_TEXTFIELD, "lblQuestion", "errorText", RULE_SECURITY_ANSWER, $fn(_endsWith("invalidSecurityQuestion")).value, CONST_CUSTOM_TRUE, "validateSecurityQuestion(" + securityQuestion + ", '" + securityAnswer + "')");
    validator.addValidation(validation);
    //Defect Fix - Artifact artf652899(R1.3) ends here

    // Check age
    validation = new Validation("chkReceiveScandicInfo", CONTROL_TYPE_CHECKBOX, "lblAgeconfirmation", "errorText", RULE_CHECKBOX_SELECTED, $fn(_endsWith("invalidAgeConfirmation")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validator.validateAll();

    if (validator.errorMessages.length > 0) {
        window.scrollTo(1, 1);
        return false;
    }
    else {
        // Dynamic control values will be added to the hidden field
        // so that this can be accessed from ascx.cs (code behind)    
        pushEntityToHiddenField(selectCtrl);
        return true;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Enroll Loyalty : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Invite a Friend: START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
function validation_InviteFriend() {
    var controls = new Array();
    var controlCounter = 0;
    var InviteAFriendErrMsgTitle = "InviteAFriendErrMsgTitle";
    var thisElement = $fn("InviteFriend");
    var ErrorInvAFrnd = "ErrorInvAFrnd";
    var inviteAFriendErrorDiv = $fn(_endsWith(ErrorInvAFrnd));
    var inviteFriendErrors = new Array();
    var isInvalidFriendMessagePushed = false;
    var isInvalidEmailMessagePushed = false;

    controls = thisElement.getElementsByTagName("*");


    var validator = null;
    var validation = null;
    var validationEmpty = null;
    var validationAlpha = null;
    var validationEmail = null;
    validator = new Validator(PAGE_INVITE_A_FRIEND, "ErrorInvAFrnd");

    for (controlCounter = 0; controlCounter < controls.length; controlCounter++) {
        if (controls[controlCounter].getAttribute('type') == 'text') {
            var str = controls[controlCounter].id.toString();
            var spanId = "span" + str.substr(3);

            if (str.substr(3, 4) == "Col1") {
                // Validate Friend name                                       
                validation = new Validation(str, CONTROL_TYPE_TEXTFIELD, spanId, "errorText", RULE_NONEMPTY, $fn(_endsWith("InvalidFriendName")).value, CONST_CUSTOM_FALSE);
                validator.addValidation(validation);
                validation = new Validation(str, CONTROL_TYPE_TEXTFIELD, spanId, "errorText", RULE_ALPHA, $fn(_endsWith("InvalidFriendName")).value, CONST_CUSTOM_FALSE);
                validator.addValidation(validation);

                if (isInvalidFriendMessagePushed == false) {
                    FriendNamevalidator = new Validator(PAGE_INVITE_A_FRIEND, "ErrorInvAFrnd");

                    validationEmpty = new Validation(str, CONTROL_TYPE_TEXTFIELD, null, "errorText", RULE_NONEMPTY, $fn(_endsWith("InvalidFriendName")).value, CONST_CUSTOM_FALSE);
                    FriendNamevalidator.addValidation(validationEmpty);
                    validationAlpha = new Validation(str, CONTROL_TYPE_TEXTFIELD, null, "errorText", RULE_ALPHA, $fn(_endsWith("InvalidFriendName")).value, CONST_CUSTOM_FALSE);
                    FriendNamevalidator.addValidation(validationAlpha);

                    FriendNamevalidator.validateAll();

                    if (FriendNamevalidator.errorMessages.length > 0) {
                        //inviteFriendErrors.push(validationAlpha);
                        isInvalidFriendMessagePushed = true;
                    }
                }
            }

            if (str.substr(3, 4) == "Col2") {
                // Validate Email                  
                validation = new Validation(str, CONTROL_TYPE_TEXTFIELD, spanId, "errorText", RULE_NONEMPTY, $fn(_endsWith("InvalidEmail")).value, CONST_CUSTOM_FALSE);
                validator.addValidation(validation);
                validation = new Validation(str, CONTROL_TYPE_TEXTFIELD, spanId, "errorText", RULE_EMAIL, $fn(_endsWith("InvalidEmail")).value, CONST_CUSTOM_FALSE);
                validator.addValidation(validation);

                if (isInvalidEmailMessagePushed == false) {
                    Emailvalidator = new Validator(PAGE_INVITE_A_FRIEND, "ErrorInvAFrnd");

                    validationEmpty = new Validation(str, CONTROL_TYPE_TEXTFIELD, null, "errorText", RULE_NONEMPTY, $fn(_endsWith("InvalidEmail")).value, CONST_CUSTOM_FALSE);
                    Emailvalidator.addValidation(validationEmpty);
                    validationEmail = new Validation(str, CONTROL_TYPE_TEXTFIELD, null, "errorText", RULE_EMAIL, $fn(_endsWith("InvalidEmail")).value, CONST_CUSTOM_FALSE);
                    Emailvalidator.addValidation(validationEmail);

                    Emailvalidator.validateAll();

                    if (Emailvalidator.errorMessages.length > 0) {
                        //inviteFriendErrors.push(validationEmail);
                        isInvalidEmailMessagePushed = true;
                    }
                }
            }
        }
    }

    validator.validateAll();
    inviteAFriendErrorDiv.innerHTML = "";

    if (isInvalidFriendMessagePushed) {
        inviteFriendErrors.push(validationAlpha);
    }

    if (isInvalidEmailMessagePushed) {
        inviteFriendErrors.push(validationEmail);
    }

    if (inviteFriendErrors.length > 0) {
        validator.errorMessages = inviteFriendErrors;
        validator.displayValidationErrors();

        return false;
    }
    else {
        var store = "storage";
        var hiddenStore = $fn(_endsWith(store));
        hiddenStore.value = CollectInformation(controls);
        return true;
    }

}

function CollectInformation(control) {
    var controls = control;
    var geneatedString = "";
    for (controlCounter = 0; controlCounter < controls.length; controlCounter++) {
        if (controls[controlCounter].getAttribute('type') == 'text') {
            var str = controls[controlCounter].id.toString();
            if (str.substr(3, 4) == "Col1") {
                var col2Control = "txt" + "Col2" + str.substr(7);
                col2Control = $fn(_endsWith(col2Control));
                geneatedString = geneatedString + controls[controlCounter].value + "," + col2Control.value + ";";
            }
        }
    }
    geneatedString = geneatedString.substr(0, geneatedString.lastIndexOf(";", geneatedString.length));
    return geneatedString;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Invite a Friend : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Update Profile : START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function validation_UpdateProfile() {
    var validator = null;
    var validation = null;

    var ErrorHeaderMessage = $fn(_endsWith("SuccessResponse"));
    if (ErrorHeaderMessage != null) {
        ErrorHeaderMessage.innerHTML = "";
    }

    validator = new Validator(PAGE_UPDATE_PROFILE, "clientErrorDivUP");


    // Email
    validation = new Validation("txtEmail1", CONTROL_TYPE_TEXTFIELD, "lblEmail", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidEmail")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtEmail1", CONTROL_TYPE_TEXTFIELD, "lblEmail", "errorText", RULE_EMAIL, $fn(_endsWith("invalidEmail")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Mobile
    validation = new Validation("ddlTel2", CONTROL_TYPE_TEXTFIELD, "lblUpdateMobilePhone", "errorText", RULE_DROPDOWN_WITH_SEPARATOR, $fn(_endsWith("invalidMobilePhoneCode")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtTelephone2", CONTROL_TYPE_TEXTFIELD, "lblUpdateMobilePhone", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidTelephone2")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("txtTelephone2", CONTROL_TYPE_TEXTFIELD, "lblUpdateMobilePhone", "errorText", RULE_NUMERIC_SPACE, $fn(_endsWith("onlyNumber")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Landline
    //**REM
    /*areaCode                = $fn(_endsWith("ddlTel1")).value;
    telNo                   = $fn(_endsWith("txtTelephone1")).value;*/

    //if area code is default value (select) and telephone number is empty,
    //no validation should happens for landline code and number since it is not a mandatory
    //else 
    //validate them accordingly
    //**REM
    /*
    if(!validateDropdownDefault(areaCode) || validateNonEmpty(telNo))
    {
    validation              = new Validation("ddlTel1", CONTROL_TYPE_TEXTFIELD, "lblUpdateLandline", "errorText", RULE_DROPDOWN_WITH_SEPARATOR, $fn(_endsWith("invalidHomePhoneCode")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
            
    validation              = new Validation("txtTelephone1", CONTROL_TYPE_TEXTFIELD, "lblUpdateLandline", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidTelephone1")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
                
    validation              = new Validation("txtTelephone1", CONTROL_TYPE_TEXTFIELD, "lblUpdateLandline", "errorText", RULE_NUMERIC_SPACE, $fn(_endsWith("onlyNumber")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);   
    }
    else
    {
    setLabelCss("lblUpdateLandline", CONST_EMPTY_STRING);
    }
    */


    // AddressLine1
    validation = new Validation("txtAddressLine1", CONTROL_TYPE_TEXTFIELD, "lblAddressLine1", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidaddressLine1")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtAddressLine1", CONTROL_TYPE_TEXTFIELD, "lblAddressLine1", "errorText", RULE_ALPHANUMERIC_NORDIC_WITH_SPECIAL_CHARACTER, $fn(_endsWith("invalidaddressLine1")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // AddressLine2
    validation = new Validation("txtAddressLine2", CONTROL_TYPE_TEXTFIELD, "lblAddressLine2", "errorText", RULE_ALPHANUMERIC_NORDIC_WITH_SPECIAL_CHARACTER, $fn(_endsWith("invalidaddressLine2")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // City Or Town
    validation = new Validation("txtCityOrTown", CONTROL_TYPE_TEXTFIELD, "lblCityTown", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidCityTown")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtCityOrTown", CONTROL_TYPE_TEXTFIELD, "lblCityTown", "errorText", RULE_ALPHANUMERIC_NORDIC, $fn(_endsWith("invalidCityTown")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Post Code
    validation = new Validation("txtPostCode", CONTROL_TYPE_TEXTFIELD, "lblPostcode", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidPostcode")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    //validation              = new Validation("txtPostCode", CONTROL_TYPE_TEXTFIELD, "lblPostcode", "errorText", RULE_ALPHANUMERIC, $fn(_endsWith("invalidPostcode")).value, CONST_CUSTOM_FALSE);    
    //validator.addValidation(validation);

    // Country
    validation = new Validation("ddlCountry", CONTROL_TYPE_DROPDOWN, "lblCountry", "errorText", RULE_DROPDOWN_WITH_SEPARATOR, $fn(_endsWith("invalidCountryCode")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Creditcard
    //R1.5 | Artifact artf809436 : FGP - Update profile CC field 
    if (requiredToValidateCreditCard()) {
        // Card holder name
        validation = new Validation("txtCardHolder", CONTROL_TYPE_TEXTFIELD, "lblCardHolder", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidCardHolder")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation("txtCardHolder", CONTROL_TYPE_TEXTFIELD, "lblCardHolder", "errorText", RULE_NAME, $fn(_endsWith("invalidCardHolder")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Card type
        validation = new Validation("ddlCardType", CONTROL_TYPE_DROPDOWN, "lblCardType", "errorText", RULE_DROPDOWN_SELECTED, $fn(_endsWith("invalidCardType")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Card number
        validation = new Validation("txtCardNumber", CONTROL_TYPE_TEXTFIELD, "lblCardNumber", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidCardNumber")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation("txtCardNumber", CONTROL_TYPE_TEXTFIELD, "lblCardNumber", "errorText", RULE_NUMERIC_STAR, $fn(_endsWith("invalidCardNumber")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Card expiry date
        var expiryMonth = $fn(_endsWith("ddlExpiryMonth")).value;
        var expiryYear = $fn(_endsWith("ddlExpiryYear")).value;
        validation = new Validation("ddlExpiryMonth", CONTROL_TYPE_DROPDOWN, "lblExpiryDate", "errorText", RULE_CREDITCARD_EXPIRY, $fn(_endsWith("invalidExpiryDate")).value, CONST_CUSTOM_TRUE, "validateCreditCardExpiry('" + expiryMonth + "', '" + expiryYear + "')");
        validator.addValidation(validation);
    }
    else {
        setLabelCss("lblCardHolder", CONST_EMPTY_STRING);
        setLabelCss("lblCardType", CONST_EMPTY_STRING);
        setLabelCss("lblCardNumber", CONST_EMPTY_STRING);
        setLabelCss("lblExpiryDate", CONST_EMPTY_STRING);
    }
    //START:Release 1.5 | artf809605 | FGP - add more than one partner preference
    var ctrl = document.getElementsByName("selectPgm");
    var textCtrl = document.getElementsByName("memberNo");
    for (var controlCount = 0; controlCount < ctrl.length; controlCount++) {
        // Partner program
        var partnerProgramIndex = ctrl[controlCount].selectedIndex;
        var partnerProgramAcctNo = textCtrl[controlCount].value.replace(/'/g, "\\\'");

        validation = new Validation(ctrl[controlCount], CONTROL_TYPE_DROPDOWN, "lblPartnerProgram" + controlCount, "errorText", RULE_PARTNER_PROGRAM, $fn(_endsWith("invalidPartnerProgram")).value, CONST_CUSTOM_TRUE, "validatePartnerProgram(" + partnerProgramIndex + ", '" + partnerProgramAcctNo + "')");
        validator.addValidation(validation);

        // Partner program account no
        validation = new Validation(textCtrl[controlCount], CONTROL_TYPE_TEXTFIELD, "lblAccountNo" + controlCount, "errorText", RULE_PARTNER_PROGRAM_ACCT_NO, $fn(_endsWith("invalidAccountNo")).value, CONST_CUSTOM_TRUE, "validatePartnerProgramAcctNo(" + partnerProgramIndex + ", '" + partnerProgramAcctNo + "')");
        validator.addValidation(validation);
    }

    var selectCtrl = document.getElementsByName("selectPgm");
    var validaterCounter = 0;
    if (checkIfPrefPartnerProgNotSelected(selectCtrl)) {
        for (var selectCtrlCount = 0; selectCtrlCount < selectCtrl.length - 1; selectCtrlCount++) {
            for (var innerCounter = selectCtrlCount + 1; innerCounter < selectCtrl.length; innerCounter++) {
                if (selectCtrl[selectCtrlCount].options[selectCtrl[selectCtrlCount].selectedIndex].value != 'DFT') {
                    //R1.5 | artf837495: Add Partner Preference||Error message is displayed if use leave partner preference blank
                    if ((selectCtrl[selectCtrlCount].options[selectCtrl[selectCtrlCount].selectedIndex].value == selectCtrl[innerCounter].options[selectCtrl[innerCounter].selectedIndex].value)
	                        && ('DFT' != selectCtrl[selectCtrlCount].options[selectCtrl[selectCtrlCount].selectedIndex].value)
	                        && ('DFT' != selectCtrl[innerCounter].options[selectCtrl[innerCounter].selectedIndex].value)) {
                        // If it is the first validator then attach the error message               
                        if (validaterCounter == 0) {
                            validation = new Validation(selectCtrl[selectCtrlCount], CONTROL_TYPE_DROPDOWN, "lblPartnerProgram" + selectCtrlCount, "errorText", RULE_FAILED_VALIDATION, $fn(_endsWith("uniquePartnerProg")).value, CONST_CUSTOM_FALSE);
                            validator.addValidation(validation);
                        }
                        // for the other validators we are not attaching the error message because same error message will go for all the validators here
                        // there for avoiding duplicates error messages we are attaching error message only for first validator
                        else {
                            validation = new Validation(selectCtrl[selectCtrlCount], CONTROL_TYPE_DROPDOWN, "lblPartnerProgram" + selectCtrlCount, "errorText", RULE_FAILED_VALIDATION, null, CONST_CUSTOM_FALSE);
                            validator.addValidation(validation);
                        }
                        validation = new Validation(selectCtrl[innerCounter], CONTROL_TYPE_DROPDOWN, "lblPartnerProgram" + innerCounter, "errorText", RULE_FAILED_VALIDATION, null, CONST_CUSTOM_FALSE);
                        validator.addValidation(validation);
                        validaterCounter++;
                    }
                }
            }
        }

    }
    else {
        var controlCount = 0;
        validation = new Validation(selectCtrl[controlCount], CONTROL_TYPE_DROPDOWN, "lblPartnerProgram" + controlCount, "errorText", RULE_FAILED_VALIDATION, $fn(_endsWith("choosePreferred")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        validation = new Validation(textCtrl[controlCount], CONTROL_TYPE_TEXTFIELD, "lblAccountNo" + controlCount, "errorText", RULE_FAILED_VALIDATION, null, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
    }
    //END:Release 1.5 | artf809605 | FGP - add more than one partner preference

    if (requiredToValidatePassword()) {
        var oldPassword = $fn(_endsWith("txtUpdatePassword")).value.replace(/'/g, "\\\'");
        var password = $fn(_endsWith("txtUpdateReTypePassword")).value.replace(/'/g, "\\\'");
        var retypePassword = $fn(_endsWith("txtUpdateReTypeNewPassword")).value.replace(/'/g, "\\\'");
        // Password
        validation = new Validation("txtUpdatePassword", CONTROL_TYPE_TEXTFIELD, "lblPassword", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidPassword")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // New Password
        validation = new Validation("txtUpdateReTypePassword", CONTROL_TYPE_TEXTFIELD, "lblRetypePassword", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidNewPassword")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Retype New Password
        validation = new Validation("txtUpdateReTypeNewPassword", CONTROL_TYPE_TEXTFIELD, "lblRetypeNewPassword", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidRetypePassword")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        //Validate for nordic characters Artifact artf1072335 : Scanweb - workaround for double byte chars in password and answer fields 
        validation = new Validation("txtUpdatePassword", CONTROL_TYPE_TEXTFIELD, "lblPassword", "errorText", RULE_PASSWORDNONORDICCHAR, $fn(_endsWith("invalidPasswordNordicChar")).value, CONST_CUSTOM_TRUE, "validateNordicCharacters('" + oldPassword + "')");
        validator.addValidation(validation);
        validation = new Validation("txtUpdateReTypePassword", CONTROL_TYPE_TEXTFIELD, "lblRetypePassword", "errorText", RULE_PASSWORDNONORDICCHAR, $fn(_endsWith("invalidPasswordNordicChar")).value, CONST_CUSTOM_TRUE, "validateNordicCharacters('" + password + "')");
        validator.addValidation(validation);
        validation = new Validation("txtUpdateReTypeNewPassword", CONTROL_TYPE_TEXTFIELD, "lblRetypeNewPassword", "errorText", RULE_PASSWORDNONORDICCHAR, $fn(_endsWith("invalidPasswordNordicChar")).value, CONST_CUSTOM_TRUE, "validateNordicCharacters('" + retypePassword + "')");
        validator.addValidation(validation);

        validation = new Validation("txtUpdateReTypeNewPassword", CONTROL_TYPE_TEXTFIELD, "lblRetypePassword", "errorText", RULE_PASSWORDMATCH, $fn(_endsWith("invalidRetypePassword")).value, CONST_CUSTOM_TRUE, "validatePasswordMatch('" + password + "', '" + retypePassword + "')");
        validator.addValidation(validation);
    }
    else {
        setLabelCss("lblPassword", CONST_EMPTY_STRING);
        setLabelCss("lblRetypePassword", CONST_EMPTY_STRING);
        setLabelCss("lblRetypeNewPassword", CONST_EMPTY_STRING);
    }

    // SecurityAnswer
    var securityQuestion = $fn(_endsWith("ddlPasswordQuestion")).selectedIndex;
    var securityAnswer = $fn(_endsWith("txtAnswer")).value.replace(/'/g, "\\\'");
    validation = new Validation("txtAnswer", CONTROL_TYPE_TEXTFIELD, "lblAnswer", "errorText", RULE_SECURITY_ANSWER, $fn(_endsWith("invalidSecurityAnswer")).value, CONST_CUSTOM_TRUE, "validateSecurityAnswer(" + securityQuestion + ", '" + securityAnswer + "')");
    validator.addValidation(validation);

    //Artifact artf1072335 : Scanweb - workaround for double byte chars in password and answer fields 
    validation = new Validation("txtAnswer", CONTROL_TYPE_TEXTFIELD, "lblAnswer", "errorText", RULE_PASSWORDNONORDICCHAR, $fn(_endsWith("invalidSecretAnswerNordicChar")).value, CONST_CUSTOM_TRUE, "validateNordicCharacters('" + securityAnswer + "')");
    validator.addValidation(validation);

    //Defect Fix - Artifact artf652899(R1.3)
    validation = new Validation("txtAnswer", CONTROL_TYPE_TEXTFIELD, "lblQuestion", "errorText", RULE_SECURITY_ANSWER, $fn(_endsWith("invalidSecurityQuestion")).value, CONST_CUSTOM_TRUE, "validateSecurityQuestion(" + securityQuestion + ", '" + securityAnswer + "')");
    validator.addValidation(validation);
    //Defect Fix - Artifact artf652899(R1.3) ends here

    // Check age
    validation = new Validation("chkReceiveScandicInfo", CONTROL_TYPE_CHECKBOX, "lblAgeconfirmation", "errorText", RULE_CHECKBOX_SELECTED, $fn(_endsWith("invalidAgeConfirmation")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);


    var password = $fn(_endsWith("txtUpdateReTypePassword")).value.replace(/'/g, "\\\'");
    var retypePassword = $fn(_endsWith("txtUpdateReTypeNewPassword")).value.replace(/'/g, "\\\'");
    // Validate for dollar sign
    validation = new Validation("txtUpdateReTypePassword", CONTROL_TYPE_TEXTFIELD, "lblRetypePassword", "errorText", RULE_PASSWORDNODOLLARSIGN, $fn(_endsWith("invalidNewPasswordDollarSign")).value, CONST_CUSTOM_TRUE, "validatePasswordForDollarSign('" + password + "')");
    validator.addValidation(validation);
    validation = new Validation("txtUpdateReTypeNewPassword", CONTROL_TYPE_TEXTFIELD, "lblRetypeNewPassword", "errorText", RULE_PASSWORDNODOLLARSIGN, $fn(_endsWith("invalidNewPasswordDollarSign")).value, CONST_CUSTOM_TRUE, "validatePasswordForDollarSign('" + retypePassword + "')");
    validator.addValidation(validation);
    // Validate for password match
    validation = new Validation("txtUpdateReTypeNewPassword", CONTROL_TYPE_TEXTFIELD, "lblRetypePassword", "errorText", RULE_PASSWORDMATCH, $fn(_endsWith("invalidRetypePassword")).value, CONST_CUSTOM_TRUE, "validatePasswordMatch('" + password + "', '" + retypePassword + "')");
    validator.addValidation(validation);

    validator.validateAll();

    if (validator.errorMessages.length > 0) {
        window.scrollTo(1, 1);
        return false;
    }
    else {
        // Dynamic control values will be added to the hidden field
        // so that this can be accessed from ascx.cs (code behind) 
        pushEntityToHiddenField(document.getElementsByName("selectPgm"));
        return true;
    }
}

//Release 1.5 | artf809466 | FGP � Fast Track Enrolment
function requiredToValidateCampaignCode() {
    var status = false;

    if ($fn(_endsWith("txtCampaignCode")) != null) {
        status = true;
    }

    return status;
}

function requiredToValidatePassword() {
    if (Trim($fn(_endsWith("txtUpdatePassword")).value) != "" ||
            Trim($fn(_endsWith("txtUpdateReTypePassword")).value) != "" ||
            Trim($fn(_endsWith("txtUpdateReTypeNewPassword")).value) != "") {
        return true;
    }
    else {
        return false;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Update Profile : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Booking Detail : START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function requiredToValidateCreditCardForBooking() {
    if ($fn(_endsWith("GauranteeInfo"))) {
        if (!$fn(_endsWith("rdoHoldRoom"))) {
            return true;
        }
        else if ($fn(_endsWith("rdoLateArrivalGurantee"))) {
            return $fn(_endsWith("rdoLateArrivalGurantee")).checked;
        }
    }
    else {
        return false;
    }
}

function validation_BookingDetail() {
    var validator = null;
    var validation = null;

    validator = new Validator(PAGE_BOOKING_DETAIL, "clientErrorDivBD");

    // City Or Town
    validation = new Validation("txtCityOrTown", CONTROL_TYPE_TEXTFIELD, "lblCityTown", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidCityTown")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtCityOrTown", CONTROL_TYPE_TEXTFIELD, "lblCityTown", "errorText", RULE_ALPHANUMERIC_NORDIC, $fn(_endsWith("invalidCityTown")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Country
    validation = new Validation("ddlCountry", CONTROL_TYPE_DROPDOWN, "lblCountry", "errorText", RULE_DROPDOWN_WITH_SEPARATOR, $fn(_endsWith("invalidCountryCode")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // DNumber validation to happen only for the Bonus Cheque booking only. 
    // Hence we are checking if the DNumber field is visible on the screen.
    // As only for BONUS CHEQUE booking, we make this field visible.
    var DNumberControl = $fn(_endsWith("txtD_Number"));
    if (DNumberControl != null) {
        // BUG FIX: artf712912
        validation = new Validation("txtD_Number", CONTROL_TYPE_TEXTFIELD, "lblD_Number", "errorText", RULE_DNUMBER, $fn(_endsWith("invalidDNumber")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
    }

    /***************************Start Rooms Validation *****************************/
    for (var ctr = 1; ctr <= 4; ctr++) {
        var fName = "txtFNameRoom" + ctr;
        var lName = "txtLNameRoom" + ctr;
        var mobileCode = "ddlMobileNumberRoom" + ctr;
        var mobileNumber = "txtTelephoneRoom" + ctr;
        var email = "txtEmailRoom" + ctr;
        var emailConf = "txtConfirmEmailRoom" + ctr;
        var lblFName = "lblRoom" + ctr + "FName";
        var lblLName = "lblRoom" + ctr + "LName";
        var lblPhoneNumber = "lblRoom" + ctr + "PhoneNumber";
        var lblEmail = "lblRoom" + ctr + "Email";
        var lblConfirmEmail = "lblRoom" + ctr + "ConfirmEmail";
        validator = ValidateRoomsDetails(validator, fName, lName, mobileCode, mobileNumber, email, emailConf, lblFName, lblLName, lblPhoneNumber, lblEmail, lblConfirmEmail);
    }
    /***************************End Rooms Validation *****************************/
    /*************************Name Compare Validation****************************/
    var RmCount = $fn(_endsWith("noOfRoomReserVationContainer")).value;
    for (var rCount = 1; rCount <= RmCount; rCount++) {
        //var currentCountryCodeObject = eval($fn(_endsWith(("RoomInformation" + roomNulmber + "_txtFNameRoom"))));
        var R1FirstName = eval($fn(_endsWith(("RoomInformation" + rCount + "_txtFNameRoom"))));
        var R1LastName = eval($fn(_endsWith(("RoomInformation" + rCount + "_txtLNameRoom"))));
    }
    var R1FirstName = eval($fn(_endsWith(("RoomInformation1_txtFNameRoom"))));
    var R1LastName = eval($fn(_endsWith(("RoomInformation1_txtLNameRoom"))));
    var R2FirstName = eval($fn(_endsWith(("RoomInformation2_txtFNameRoom"))));
    var R2LastName = eval($fn(_endsWith(("RoomInformation2_txtLNameRoom"))));
    var R3FirstName = eval($fn(_endsWith(("RoomInformation3_txtFNameRoom"))));
    var R3LastName = eval($fn(_endsWith(("RoomInformation3_txtLNameRoom"))));
    var R4FirstName = eval($fn(_endsWith(("RoomInformation4_txtFNameRoom"))));
    var R4LastName = eval($fn(_endsWith(("RoomInformation4_txtLNameRoom"))));
    var R1NamePrePopulated = false;
    var R2NamePrePopulated = false;
    var R3NamePrePopulated = false;
    var R4NamePrePopulated = false;

    var R1NameVal = null, R2NameVal = null, R3NameVal = null, R4NameVal = null;
    // Initiate the combination of First name and last name to the varaibles for all 4 rooms only if it is not empty
    if (R1FirstName != null && Trim(R1FirstName.value) != "" && R1LastName != null && Trim(R1LastName.value) != "" && (R1FirstName.style.display != "none") && (R1LastName.style.display != "none")) {
        R1NameVal = Trim(R1FirstName.value) + ";" + Trim(R1LastName.value);
    }
    else if ($fn(_endsWith("lblFirstName")) != null && $fn(_endsWith("lblLastName")) != null && ($fn(_endsWith("lblFirstName")).style.display != "none") && ($fn(_endsWith("lblLastName")).style.display != "none")) {
        R1NameVal = Trim($fn(_endsWith("lblFirstName")).innerHTML) + ";" + Trim($fn(_endsWith("lblLastName")).innerHTML);
        R1NamePrePopulated = true;
    }

    if (R2FirstName != null && Trim(R2FirstName.value) != "" && R2LastName != null && Trim(R2LastName.value) != "") {
        R2NameVal = Trim(R2FirstName.value) + ";" + Trim(R2LastName.value);
    }
    else if ($fn(_endsWith("lblFNameRoom2")) != null && $fn(_endsWith("lblLNameRoom2")) != null) {
        R2NameVal = Trim($fn(_endsWith("lblFNameRoom2")).innerHTML) + ";" + Trim($fn(_endsWith("lblLNameRoom2")).innerHTML);
        R2NamePrePopulated = true;
    }

    if (R3FirstName != null && Trim(R3FirstName.value) != "" && R3LastName != null && Trim(R3LastName.value) != "") {
        R3NameVal = Trim(R3FirstName.value) + ";" + Trim(R3LastName.value);
    }
    else if ($fn(_endsWith("lblFNameRoom3")) != null && $fn(_endsWith("lblLNameRoom3")) != null) {
        R3NameVal = Trim($fn(_endsWith("lblFNameRoom3")).innerHTML) + ";" + Trim($fn(_endsWith("lblLNameRoom3")).innerHTML);
        R3NamePrePopulated = true;
    }

    if (R4FirstName != null && Trim(R4FirstName.value) != "" && R4LastName != null && Trim(R4LastName.value) != "") {
        R4NameVal = Trim(R4FirstName.value) + ";" + Trim(R4LastName.value);
    }
    else if ($fn(_endsWith("lblFNameRoom4")) != null && $fn(_endsWith("lblLNameRoom4")) != null) {
        R4NameVal = Trim($fn(_endsWith("lblFNameRoom4")).innerHTML) + ";" + Trim($fn(_endsWith("lblLNameRoom4")).innerHTML);
        R4NamePrePopulated = true;
    }

    if (R1NameVal != null) {
        R1NameVal = R1NameVal.toLowerCase();
    }
    if (R2NameVal != null) {
        R2NameVal = R2NameVal.toLowerCase();
    }
    if (R3NameVal != null) {
        R3NameVal = R3NameVal.toLowerCase();
    }
    if (R4NameVal != null) {
        R4NameVal = R4NameVal.toLowerCase();
    }

    // check Room 1 name is matching against some/all of the other three room names.  
    if (!CompareNames(R1NameVal, R2NameVal, R3NameVal, R4NameVal) && !R1NamePrePopulated) {
        // Room 1 name not matching against some/all of the other room names , so adding validations 
        // for all the first name text boxes for all rooms.
        // Last name text boxes also need to be highlighted if the validation fails , for that we added
        // lblLname in to the label parameter.
        // Error message need to be dispalyed only ONE time , for that we are passing error string only to 
        // txtFNameRoom1 validation.For other validations we are passing null instead of error string.

        // Custom validator which checks the Room 1 name is matching against some/all of the other three room names.
        // If matching then validation fails and throws the error message  and highlight lblFname & lblLname
        validation = new Validation("txtFNameRoom1", CONTROL_TYPE_TEXTFIELD, "txtFNameRoom1", "errorText", null, $fn(_endsWith("sameUsernames")).value, CONST_CUSTOM_TRUE, "CompareNames('" + R1NameVal + "', '" + R2NameVal + "', '" + R3NameVal + "', '" + R4NameVal + "')");
        validator.addValidation(validation);

        // Custom validator which checks the Room 2 name is matching against some/all of the other three room names.
        // If matching then validation fails and highlight lblRoom2FName & lblRoom2LName
        validation = new Validation("txtFNameRoom2", CONTROL_TYPE_TEXTFIELD, "txtFNameRoom2", "errorText", null, null, CONST_CUSTOM_TRUE, "CompareNames('" + R2NameVal + "', '" + R1NameVal + "', '" + R3NameVal + "', '" + R4NameVal + "')");
        validator.addValidation(validation);

        // Custom validator which checks the Room 3 name is matching against some/all of the other three room names.
        // If matching then validation fails and highlight lblRoom3FName & lblRoom3LName
        validation = new Validation("txtFNameRoom3", CONTROL_TYPE_TEXTFIELD, "txtFNameRoom3", "errorText", null, null, CONST_CUSTOM_TRUE, "CompareNames('" + R3NameVal + "', '" + R1NameVal + "', '" + R2NameVal + "', '" + R4NameVal + "')");
        validator.addValidation(validation);

        // Custom validator which checks the Room 4 name is matching against some/all of the other three room names.
        // If matching then validation fails and highlight lblRoom4FName & lblRoom4LName
        validation = new Validation("txtFNameRoom4", CONTROL_TYPE_TEXTFIELD, "txtFNameRoom4", "errorText", null, null, CONST_CUSTOM_TRUE, "CompareNames('" + R4NameVal + "', '" + R1NameVal + "', '" + R2NameVal + "', '" + R3NameVal + "')");
        validator.addValidation(validation);
    }   // Same logic applies for the following two cases also, Hence not putting same comments for the following
    else if (!CompareNames(R2NameVal, R3NameVal, R4NameVal, R1NameVal) && !R2NamePrePopulated) {

        // Confirmed from the previous IF statement that validation for room1 name is passed so NOT putting the      
        // validation for room1 name inside this block        
        validation = new Validation("txtFNameRoom2", CONTROL_TYPE_TEXTFIELD, "txtFNameRoom2", "errorText", null, $fn(_endsWith("sameUsernames")).value, CONST_CUSTOM_TRUE, "CompareNames('" + R2NameVal + "', '" + R3NameVal + "', '" + R4NameVal + "', '" + R1NameVal + "')");
        validator.addValidation(validation);

        validation = new Validation("txtFNameRoom3", CONTROL_TYPE_TEXTFIELD, "txtFNameRoom3", "errorText", null, null, CONST_CUSTOM_TRUE, "CompareNames('" + R3NameVal + "', '" + R1NameVal + "', '" + R2NameVal + "', '" + R4NameVal + "')");
        validator.addValidation(validation);

        validation = new Validation("txtFNameRoom4", CONTROL_TYPE_TEXTFIELD, "txtFNameRoom4", "errorText", null, null, CONST_CUSTOM_TRUE, "CompareNames('" + R4NameVal + "', '" + R1NameVal + "', '" + R2NameVal + "', '" + R3NameVal + "')");
        validator.addValidation(validation);

    }
    else if (!CompareNames(R3NameVal, R4NameVal, R2NameVal, R1NameVal) && !R3NamePrePopulated) {
        // Confirmed from the previous IF and ELSE IF statement that validation for room1 name and 
        // room2 name are passed so NOT putting the      
        // validation for room1 name and room2 name inside this block   
        validation = new Validation("txtFNameRoom3", CONTROL_TYPE_TEXTFIELD, "txtFNameRoom3", "errorText", null, $fn(_endsWith("sameUsernames")).value, CONST_CUSTOM_TRUE, "CompareNames('" + R3NameVal + "', '" + R1NameVal + "', '" + R2NameVal + "', '" + R4NameVal + "')");
        validator.addValidation(validation);

        validation = new Validation("txtFNameRoom4", CONTROL_TYPE_TEXTFIELD, "txtFNameRoom4", "errorText", null, null, CONST_CUSTOM_TRUE, "CompareNames('" + R4NameVal + "', '" + R1NameVal + "', '" + R2NameVal + "', '" + R3NameVal + "')");
        validator.addValidation(validation);
    }
    else if (!CompareNames(R4NameVal, R3NameVal, R2NameVal, R1NameVal) && !R4NamePrePopulated) {
        // Confirmed from the previous IF and ELSE IF statement that validation for room1 name and 
        // room2 name are passed so NOT putting the      
        // validation for room1 name and room2 name inside this block   

        validation = new Validation("txtFNameRoom4", CONTROL_TYPE_TEXTFIELD, "txtFNameRoom4", "errorText", null, $fn(_endsWith("sameUsernames")).value, CONST_CUSTOM_TRUE, "CompareNames('" + R4NameVal + "', '" + R1NameVal + "', '" + R2NameVal + "', '" + R3NameVal + "')");
        validator.addValidation(validation);
    }


    /********************************************************************************************/
    // Creditcard
    //1.8.2:Check if Credit card section is visible for the booking then validate all credit card fields.
    if ($fn(_endsWith("HideUnhideCreditCardDiv")) && requiredToValidateCreditCardForBooking()) {
        // Card holder name
        validation = new Validation("txtCardHolder", CONTROL_TYPE_TEXTFIELD, "lblCardHolder", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidCardHolder")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation("txtCardHolder", CONTROL_TYPE_TEXTFIELD, "lblCardHolder", "errorText", RULE_NAME, $fn(_endsWith("invalidCardHolder")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Card number
        validation = new Validation("txtCardNumber", CONTROL_TYPE_TEXTFIELD, "lblCardNumber", "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidCardNumber")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Card number
        validation = new Validation("txtCardNumber", CONTROL_TYPE_TEXTFIELD, "lblCardNumber", "errorText", RULE_NUMERIC_STAR, $fn(_endsWith("invalidCardNumber")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        var newCardNumber = $fn(_endsWith("txtCardNumber")).value;
        if (validateNumericStar(newCardNumber)) {
            validation = new Validation("txtCardNumber", CONTROL_TYPE_TEXTFIELD, "lblCardNumber", "errorText", null, $fn(_endsWith("invalidCardNumber")).value, CONST_CUSTOM_TRUE, "ValidateCreditCardNumberForChanges('" + orginalCardNumber + "', '" + newCardNumber + "')");
            validator.addValidation(validation);
        }
        // Card type
        validation = new Validation("ddlCardType", CONTROL_TYPE_DROPDOWN, "lblCardType", "errorText", RULE_DROPDOWN_SELECTED, $fn(_endsWith("invalidCardType")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Card expiry date
        var expiryMonth = $fn(_endsWith("ddlExpiryMonth")).value;
        var expiryYear = $fn(_endsWith("ddlExpiryYear")).value;
        validation = new Validation("ddlExpiryMonth", CONTROL_TYPE_DROPDOWN, "lblExpiryDate", "errorText", RULE_CREDITCARD_EXPIRY, $fn(_endsWith("invalidExpiryDate")).value, CONST_CUSTOM_TRUE, "validateCreditCardExpiry('" + expiryMonth + "', '" + expiryYear + "')");
        validator.addValidation(validation);
    }
    else {
        setLabelCss("lblCardHolder", CONST_EMPTY_STRING);
        setLabelCss("lblCardNumber", CONST_EMPTY_STRING);
        setLabelCss("lblCardType", CONST_EMPTY_STRING);
        setLabelCss("lblExpiryDate", CONST_EMPTY_STRING);
    }

    // Terms and Conditions
    validation = new Validation("chkTermsCondition", CONTROL_TYPE_CHECKBOX, "lblTermAndCondition", "errorText", RULE_CHECKBOX_SELECTED, $fn(_endsWith("invalidTermsConfirmation")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("chkMembershipTerms", CONTROL_TYPE_CHECKBOX, "lblMembershipTerms", "errorText", RULE_CHECKBOX_SELECTED, $fn(_endsWith("invalidTermsConfirmation")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validation = new Validation("chkReceivePartnersInfo", CONTROL_TYPE_CHECKBOX, "lblReceivePartnerInfo", "errorText", RULE_CHECKBOX_SELECTED, $fn(_endsWith("invalidTermsConfirmation")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validator.validateAll();

    if (validator.errorMessages.length > 0) {
        window.scrollTo(1, 1);
        return false;
    }
    else {
        return true;
    }
}

function ValidateRoomsDetails(validator, fName, lName, mobileCode, mobileNumber, email, emailConf, lblFName, lblLName, lblPhoneNumber, lblEmail, lblConfirmEmail) {
    var validation = null;
    var firstName = $fn(_endsWith(fName));
    var lastName = $fn(_endsWith(lName));
    if (firstName != null && lastName != null) {
        if (firstName.style.display != "none" && lastName.style.display != "none") {
            //Validate First Name        
            validation = new Validation(fName, CONTROL_TYPE_TEXTFIELD, lblFName, "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidFirstName")).value, CONST_CUSTOM_FALSE);
            validator.addValidation(validation);
            validation = new Validation(fName, CONTROL_TYPE_TEXTFIELD, lblFName, "errorText", RULE_NAME, $fn(_endsWith("invalidFirstName")).value, CONST_CUSTOM_FALSE);
            validator.addValidation(validation);

            //Validate Last Name
            validation = new Validation(lName, CONTROL_TYPE_TEXTFIELD, lblLName, "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidLastName")).value, CONST_CUSTOM_FALSE);
            validator.addValidation(validation);
            validation = new Validation(lName, CONTROL_TYPE_TEXTFIELD, lblLName, "errorText", RULE_NAME, $fn(_endsWith("invalidLastName")).value, CONST_CUSTOM_FALSE);
            validator.addValidation(validation);
        }
        else {
            setLabelCss(lblFName, CONST_EMPTY_STRING);
            setLabelCss(lblLName, CONST_EMPTY_STRING);
        }
    }
    if ($fn(_endsWith(mobileCode)) != null && $fn(_endsWith(mobileNumber)) != null && $fn(_endsWith(email)) != null && $fn(_endsWith(emailConf)) != null) {
        //Validate Phone Number
        validation = new Validation(mobileCode, CONTROL_TYPE_TEXTFIELD, lblPhoneNumber, "errorText", RULE_DROPDOWN_WITH_SEPARATOR, $fn(_endsWith("invalidMobilePhoneCode")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        validation = new Validation(mobileNumber, CONTROL_TYPE_TEXTFIELD, lblPhoneNumber, "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidTelephone2")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        validation = new Validation(mobileNumber, CONTROL_TYPE_TEXTFIELD, lblPhoneNumber, "errorText", RULE_NUMERIC_SPACE, $fn(_endsWith("invalidTelephone2")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Validate Email
        validation = new Validation(email, CONTROL_TYPE_TEXTFIELD, lblEmail, "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidEmail")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        validation = new Validation(email, CONTROL_TYPE_TEXTFIELD, lblEmail, "errorText", RULE_EMAIL, $fn(_endsWith("invalidEmail")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        // Validate Confirm Email
        validation = new Validation(emailConf, CONTROL_TYPE_TEXTFIELD, lblConfirmEmail, "errorText", RULE_NONEMPTY, $fn(_endsWith("invalidEmail")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        validation = new Validation(emailConf, CONTROL_TYPE_TEXTFIELD, lblConfirmEmail, "errorText", RULE_EMAIL, $fn(_endsWith("invalidEmail")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        var email1R2 = $fn(_endsWith(email)).value;
        var email2R2 = $fn(_endsWith(emailConf)).value;

        if (validateEmail(email1R2) && validateEmail(email2R2)) {
            validation = new Validation(emailConf, CONTROL_TYPE_TEXTFIELD, lblConfirmEmail, "errorText", RULE_PASSWORDMATCH, $fn(_endsWith("notsameEmail")).value, CONST_CUSTOM_TRUE, "validatePasswordMatch('" + email1R2 + "', '" + email2R2 + "')");
            validator.addValidation(validation);
        }
    }
    return validator;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Booking Detail : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for BookingSearch : START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function Validation_BookingSearch() {

    var validator = null;
    var validation = null;

    validator = new Validator(PAGE_BOOKING_SEARCH, "clientErrorDivBD");

    // Validate Booking Number
    validation = new Validation("txtBookingNumber", CONTROL_TYPE_TEXTFIELD, "txtBookingNumber", "error", RULE_NONEMPTY, $fn(_endsWith("invalidBookingNumber")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtBookingNumber", CONTROL_TYPE_TEXTFIELD, "txtBookingNumber", "error", RULE_NUMERIC, $fn(_endsWith("invalidBookingNumber")).value, CONST_CUSTOM_FALSE);

    validation = new Validation("txtBookingNumber", CONTROL_TYPE_TEXTFIELD, "txtBookingNumber", "mandatory", RULE_BOOKING_CONFIRMATION_NUMBER, $fn(_endsWith("invalidBookingNumber")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Validate Surname
    validation = new Validation("txtSurname", CONTROL_TYPE_TEXTFIELD, "txtSurname", "mandatory", RULE_NONEMPTY, $fn(_endsWith("invalidSurname")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    // Release R2.0 - Bhavya - Defect fix - 466742 - To check whether user has entered last name,even if the field has default text it indicates that user has not entered last name.            
    validation = new Validation("txtSurname", CONTROL_TYPE_TEXTFIELD, "txtSurname", "mandatory", DEFAULT_TEXT, $fn(_endsWith("invalidSurname")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtSurname", CONTROL_TYPE_TEXTFIELD, "txtSurname", "error", RULE_NAME, $fn(_endsWith("invalidSurname")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validator.validateAll();

    if (validator.errorMessages.length > 0) {
        return false;
    }
    else {
        return true;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for BookingSearch : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Cancel Confirmation : START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function Validation_CancelConfirmation() {
    var validator = null;
    var validation = null;
    var roomCnt = 0;
    //validator               = new Validator(PAGE_CANCEL_BOOKING_CONFIRMATION, "clientErrorDivCB");
    //artf1150983: Error message on cancel a reservation
    validator = new Validator(PAGE_CANCEL_BOOKING_CONFIRMATION, "divSelectRatePageErrorMessage");

    // Validate if any room selected for cancellation.
    validation = new Validation("Room1chkCancel", null, null, "errorText", null, $fn(_endsWith("invalidCancellation")).value, CONST_CUSTOM_TRUE, "validateRoomSelectedToCancel()");
    validator.addValidation(validation);
    for (roomCnt = 1; roomCnt <= 4; roomCnt++) {
        if ($fn(_endsWith("chkSMSRoom" + roomCnt)) != null) {
            //validate the SMS confirmation check box checked
            validation = new Validation("chkSMSRoom" + roomCnt, CONTROL_TYPE_CHECKBOX, "lblSMSRoom" + roomCnt, "errorText", null, $fn(_endsWith("invalidMobileCodeOrNumber")).value, CONST_CUSTOM_TRUE, "validateForRoom(" + roomCnt + ")");
            validator.addValidation(validation);
            //Boxi: Added Missing Numeric and Numeric with space validation
            // START:Sateesh Chandolu artf1282222 : Scanweb - SMS number validation is broken
            //argument changed to RULE_NUMERIC_SPACE from RULE_NUMERIC
            validation = new Validation("txtMobileNumberRoom" + roomCnt, CONTROL_TYPE_TEXTFIELD, "lblSMSRoom" + roomCnt, "errorText", RULE_NUMERIC_SPACE, $fn(_endsWith("invalidNonNumericNumber")).value, CONST_CUSTOM_FALSE);
            validator.addValidation(validation);

        }
    }
    validator.validateAll();
    if (validator.errorMessages.length > 0) {
        return false;
    }
    else {
        return true;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Cancel Confirmation : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Customer Service Contact Us : START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function Validation_CustomerServiceContactUs() {
    var validator = null;
    var validation = null;

    validator = new Validator(PAGE_CUSTOMER_SERVICE_CONTACT_US, "clientErrorDivCSCU");

    // Validate Nature Of Query
    validation = new Validation("ddlNatureOfQuery", CONTROL_TYPE_DROPDOWN, "lblNatureOfQuery", "errorText", RULE_DROPDOWN_SELECTED, $fn(_endsWith("CSCU_InvalidNatureofQuery")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    // Validate Email
    validation = new Validation("txtEmailId", CONTROL_TYPE_TEXTFIELD, "lblEmailId", "errorText", RULE_NONEMPTY, $fn(_endsWith("CSCU_InvalidEmail")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);
    validation = new Validation("txtEmailId", CONTROL_TYPE_TEXTFIELD, "lblEmailId", "errorText", RULE_EMAIL, $fn(_endsWith("CSCU_InvalidEmail")).value, CONST_CUSTOM_FALSE);
    validator.addValidation(validation);

    validator.validateAll();

    if (validator.errorMessages.length > 0) {
        return false;
    }
    else {
        return true;
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for Customer Service Contact Us : END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for FGP Campaign Landing Page: START   Release 1.5 | artf809466 | FGP � Fast Track Enrolment
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
function Validation_CampaignLanding() {
    var validator = null;
    var validation = null;

    validator = new Validator(PAGE_CAMPAIGN_LANDING, "clientErrorDivCCLP");
    //If the already member is selected then do the validation.
    if (requiredToValidateCampaignLandingPage()) {
        validation = new Validation("txtmembershipno", CONTROL_TYPE_TEXTFIELD, "spanMemberNumber", "errorText", RULE_NONEMPTY, $fn(_endsWith("CampaignCodeLandingPageUserNameInvalid")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation("txtmembershipno", CONTROL_TYPE_TEXTFIELD, "spanMemberNumber", "errorText", RULE_MEMBERSHIP_NUM, $fn(_endsWith("CampaignCodeLandingPageUserNameInvalid")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        validation = new Validation("txtPin", CONTROL_TYPE_TEXTFIELD, "spanPin", "errorText", RULE_NONEMPTY, $fn(_endsWith("CampaignCodeLandingPagePasswordInvalid")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        validation = new Validation("txtCampaignCode", CONTROL_TYPE_TEXTFIELD, "spanCampaignCode", "errorText", RULE_NONEMPTY, $fn(_endsWith("CampaignCodeLandingPageCampaignCodeInvalid")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
        validation = new Validation("txtCampaignCode", CONTROL_TYPE_TEXTFIELD, "spanCampaignCode", "errorText", null, $fn(_endsWith("CampaignCodeLandingPageCampaignCodeInvalid")).value, CONST_CUSTOM_TRUE, "ValidateCampaignCode('" + $fn(_endsWith("txtCampaignCode")).value + "', '" + $fn(_endsWith("hdnCampaignCode")).value + "')");
        validator.addValidation(validation);
    }

    validator.validateAll();

    if (validator.errorMessages.length > 0)
        return false;
    else
        return true;
}

function requiredToValidateCampaignLandingPage() {
    var status = false;
    if ($fn(_endsWith("rdoAlreadyMember")).checked == true) {
        status = true;
    }
    return status;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creating Validator for FGP Campaign Landing Page: END   Release 1.5 | artf809466 | FGP � Fast Track Enrolment
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
function Validation_ChildrensDetails() {
    var validator = null;
    var validation = null;
    validator = new Validator(PAGE_CHILDREN_DETAIL, "kidsErrorDiv");

    var maxChild = parseInt($fn(_endsWith("noOfChildrenHidden")).value);
    for (var childCount = 0; childCount < maxChild; childCount++) {
        validation = new Validation("DropDown" + childCount, CONTROL_TYPE_DROPDOWN, "SpanKids" + childCount, "errorText", RULE_DROPDOWN_SELECTED, $fn(_endsWith("InvalidKidsAge")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);

        validation = new Validation("RadioButtonRadioDiv" + childCount, CONTROL_TYPE_RADIOOPTION, "SpanKids" + childCount, "errorText", RULE_RADIO_CHECKED, $fn(_endsWith("InvalidBedType")).value, CONST_CUSTOM_FALSE);
        validator.addValidation(validation);
    }
    validator.validateAll();
    if (validator.errorMessages.length < 1) {
        if (CheckforDuplicates() == 1) {
            ShowCIPBError();
            return false;
        }
        else {
            /**********************************************************************
            AMS Artifact :artf964810 , Prod Patch R 1.6.2 - an extra level of validation is applied which would
            check whether the Radio Buttons are visible or not when 
            the Drop Down have selected value other than default i.e Select Age.
            ***********************************************************************/
            if (CheckforRadioButtonVisiblity() == 1) {
                ShowBedtypeError();
                return false;
            }

            else {
                collectSelectedAccomodationFromUI();
                return true;
            }
        }

    }
    else {
        return false;
    }
}

function CheckforAdultsnChldInAdultBed(maxChild, adults) {
    //This needs to be refactored
    var CIPBCount = 0;
    var isCIPBErrorFlag = 0;
    //var adults              = parseInt($fn(_endsWith("noOfAdults")).value);        
    // var maxChild            = parseInt($fn(_endsWith("noOfChildrenHidden")).value);
    var CIPBstr = $fn(_endsWith("CIPBString")).value;

    $fn(_endsWith("kidsErrorDiv")).innerHTML = "";
    $fn(_endsWith("kidsErrorDiv")).style.display = "none";

    for (var childCount = 0; childCount < maxChild; childCount++) {
        var allRadio = document.getElementsByName("RadioButtonRadioDiv" + childCount);

        for (var radioCount = 0; radioCount < allRadio.length; radioCount++) {
            if ((allRadio[radioCount].checked) && (allRadio[radioCount].value == CIPBstr)) {
                CIPBCount++;

                if (CIPBCount > adults) {
                    // Shows the error message.
                    ShowCIPBError();

                    isCIPBErrorFlag = 1;
                    break;
                }
            }
        }
    }

    return isCIPBErrorFlag;
}
//Sateesh artf1283065 : Scanweb - locale selection not saved in cookie
function setLangCookie(lang) {
    //var expireDate=new Date("dec 31,2012 00:00:00");
    var expireDate = new Date();
    //expireDate.setTime(expireDate.getTime()+(30*24*60*60*1000));
    expireDate.setDate(expireDate.getDate() + 365);
    if (document.cookie.indexOf("langCookie=") != -1) {
        updateLangCookie(lang, expireDate);
    }
    else {

        document.cookie = "langCookie=" + escape(lang) + ";path=/;expires=" + expireDate
    }

}
function updateLangCookie(langvalue, expirydate) {
    document.cookie = "langCookie=" + escape(langvalue) + ";path=/;expires=" + expirydate
}
