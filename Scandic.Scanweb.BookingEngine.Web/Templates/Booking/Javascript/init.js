/* ------------------------------------Variable Declarations--------------------------------------- */

// get platform and browser versions
var platform = navigator.platform.substr(0,3);
var browserName = navigator.appName;
var browserVer;
		
//for IE 5, the first number in "navigator.appVersion" is 4.0, so need to parse the string further 
if (navigator.appVersion.indexOf('MSIE 5') != -1) {
	browserVer=5;
}
else {
	browserVer = parseInt(navigator.appVersion);
}

var imgPath = "/Templates/Booking/Styles/Default/Images/";
var errorImgPath= "/Templates/Booking/Styles/Default/Images/";
var showNextMonthText 	= "Next Month";
var showLastMonthText	= "Last Month";	

//////////////////////////////////////////////////////////////////////////////////////////
// Global variable to hold all phone codes associated with country for the phone drop down
var phoneCodesWithCountry;

function EachCountryPhone(countryCode, phoneCode)
{
    this.countryCode = countryCode;
	this.phoneCode = phoneCode;	
}

function CountryPhoneCodesMap()
{
	this.phoneCodes = new Array();
}

CountryPhoneCodesMap.prototype.addCountryCode = function(eachCountryPhone)
{    
	this.phoneCodes.push(eachCountryPhone);	
}
//////////////////////////////////////////////////////////////////////////////////////////

//Location of the calendar layer based on the Input Field
var tmpLeft	= 0;
var tmpTop	= 20;
				
// Declaring valid cookie expriry date
var cookieExpDate = new Date();

// Declaring valid date character, minimum year and maximum year
var dtCh	= "/";   
//var today 	= new Date();
var today 	= serverDate;
var ad 	= today.getDate();
var am 	= today.getMonth()+1;
var ay 	= today.getFullYear();

var sysDate		= ad+"/"+am+"/"+ay ;
//var todayDate 	= ad+"/"+am+"/"+ay ;
var endMonth 	= am-1; /*CURRENT MONTH -1 */
var endYear		= ay+2; /*CURRENT YEAR -1 */
var startDay 	= ad;
var startMonth 	= am - 1;
var startYear 	= ay;

var minYear		= ay;  
var maxYear		= minYear+2;

var isVisible	= "visible";
var isHidden 	= "hidden";

var allowPastDate = 'n';
var allowFutureDate = 'y';
//error messages
var errMsgTitle		    = "errMsgTitle"; 
var invalidDestination	= "destinationError";
var duplicateDestination= "duplicateDestinationError";
var invalidArrivalDate	= "arrivaldateError"; 
var invalidDepartureDate= "departuredateError";
var invalidArrivalDate1	= "arrivaldateError1"; 
var invalidDepartureDate1= "departuredateError1";
var invalidNoOfNight	= "NoOfNightError";
var invalidGuestNumber	= "invalidGuestNumberError";
var invalidGuestPIN 	= "invalidGuestPINError";
var invalidGuestLogin 	= "invalidGuestLoginError";
var invalidSpecialCodeError    = "invalidSpecialCodeError";
var invalidNegotiatedCode ="invalidNegotiatedCode"; 
var invalidVoucherCode    ="invalidVoucherCode";
 // R1.3 | CR7 | Automatic switch to corporate tab                                
// Below variable is used to store the message to be displayed
// when the user enters a Special Code starting with the character "d" or "D".
var specialCodeToDNumber    ="specialCodeToDNumber";	
// R1.3 | CR7 | Automatic switch to corporate tab Ends here
	
// variables used for Error Msg Display			
var errorColor 			= "#FF0000";
var errorTextClass 		= "errorText";
var fieldLabelColor 	= "#666";
var errorDivID;

			
var gSiteLanguage;
var siteLang;	
var gCalendarFlag;
var aDiv, dDiv, nDiv;
var dt, at, nt, spcode, vouCode, negCode, promoCode; 
var bRedemtionLogin;
			
var errorMessagesArray = new Array();
var invisibleFields = new Array();	

//error Booking Search
var errorSpan = "errorSpan";
var invalidBookingNumber = "invalidBookingNumber";
var divServerErrorSearchBooking = "divServerErrorSearchBooking";
var invalidSurname = "invalidSurname";
var cannotUpdateNameAndAccountNumberSimultaneosly = "cannotUpdateNameAndAccountNumberSimultaneosly";

// R1.3 | CR1 | Connect Bonuscheque Bookings with DNumber.
// It holds the control which has the error message for not filling up DNumber.
var invalidDNumber = "invalidDNumber";

//Below varibales values are id of the HTML module, can be changed according to .NET code
// START: FORM CONTROLS
var sT;
var Destination;
var guestLogin;
var ArrivalDate;
var DepartureDate;
var ArrivalDate1;
var DepartureDate1;
var splBookingCode;
var DNumber;
var VoucherNumber;
var NoOfNight;
var NoOfRooms;
var AdultsPerRoom;
var ChildPerRoom;
var UserName;
var Password;
var formName;
var selBookingType;
var ddlHotel;

// END: FORM CONTROLS

var destCont		= "destCont";
var atCont 			= "atCont";
var dtCont 			= "dtCont";

var atCont1 		= "spanArrivalDate1";
var dtCont1 		= "spanDepartureDate1";

var nnCont 			= "nnCont";
var UsernameCont	=  "UsernameCont";
var pwdCont			= "pwdCont";
var autosuggest		= "autosuggest";
var commonControls	= "commonControls";
var singleLeg = "SingleLeg";
var multiLeg = "MultiLeg";
var clientErrorDivBD 	= "clientErrorDivBD";
var clientErrorDivEL 	= "clientErrorDivEL";
var clientErrorDivUP 	= "clientErrorDivUP";
var commonFooter	= "commonFooter";
var RedemptionFooter= "RedemptionFooter";
var activeTab		= "activeTab";
var clientErrorDiv		="clientErrorDiv";
var clientErrorDiv1		= "clientErrorDiv1";

/* Start: jQuery calendar variable declarations */
var ArrDate_Id = "";
var DepDate_Id = "";
var Nights_Id = "";
var DateRange_Class = "";
var mp_ArrDate_Id = "";
var mp_DepDate_Id = "";
var mp_DateRange_Class = "";
var Locale_Id = "";
/* End initialisation */
	    

//Below varibales values are name of the form module, can be changed according to .NET code
var date1;
var date2;
var date3;
var date4;
//Constructor to create an object for Booking detail values.
function BookingDetailValues(fName, lName, mobile, email, confirmEmail) 
{
    this.fName =fName;
    this.lName =lName;
    this.mobile =mobile;
    this.email =email;
    this.confirmEmail =confirmEmail;
}
var bookingRoomDetailsListOld;
/* --------------------------Booking Details variables ------------------------------------ */

/*-------------------------- Modify /Cancel Booking variables------------------------------ */
var bookingSearchNumber;
var bookingSearchSurname;
var oldfirstname;
var oldlastname;
var oldaccountnumber;
/*-------------------------- End of Modify /Cancel Booking variables------------------------ */

/* ------------------------------------Functions--------------------------------------- */
	//function to return the object of the passed id.
	function $fn(element) {
		element = document.getElementById(element);
		return element;
	}
    // Function to initialize variables for 'enter booking deatils' page
	function  initBD()
    {
        //Following If statement is added in CR 1 | Accomodation of Rate block | Release 1.5.3
        //Javascript error was displaying up. 
        if(null != $fn(_endsWith("txtCardNumber")))
        {
            //Orginal Card Number  
            orginalCardNumber   = $fn(_endsWith("txtCardNumber")).value;   
        }
    }
    
	function initBB() {
        //Below varibales values are id of the HTML module, can be changed according to .NET code
        // START: FORM CONTROLS
        sT 				= _endsWith("sT");
        Destination 	= _endsWith("txtHotelName");
        DestinationBNC  =_endsWith("txtHotelNameBNC");
         DestinationRN   = _endsWith("txtHotelNameRewardNights");
        guestLogin 	  	= _endsWith("guestLogin");
        ArrivalDate 	= _endsWith("txtArrivalDate");
        DepartureDate 	= _endsWith("txtDepartureDate");
        splBookingCode 	= _endsWith("txtRegularCode");
        DNumber 		= _endsWith("txtNegotiatedCode");
        VoucherNumber 	= _endsWith("txtVocherCode");
        NoOfNight 		= _endsWith("txtNoOfNights");
        AdultsPerRoom 	= _endsWith("ddlAdultsPerRoom");
        ChildPerRoom 	= _endsWith("ddlChildPerRoom");
        UserName 		= _endsWith("txtUserName");
        Password 		= _endsWith("txtPassword");
        formName 		= _endsWith("BookingForm");
		siteLang        = _endsWith("siteLang");
        // END: FORM CONTROLS
        
        date1 			= ArrivalDate;
        date2 			= DepartureDate;
        
		errorDivID 	= clientErrorDiv;
		destDiv	= $fn(destCont);
		aDiv 	= $fn(atCont);
		dDiv 	= $fn(dtCont);
		nDiv 	= $fn(nnCont);
		
		// BUG FIX: artf695582 | Free Night is not coming as selected in refine search module.
		var actvTab = getCookie("ActiveTab")
		
		if (!actvTab)
		{
		    actvTab = 'Tab1';
		}
		
		$fn(sT).value = actvTab;

//		changeActiveTab('a' + actvTab);
		//showTabContent(actvTab);
				
		new AutoSuggest($fn(Destination),'autosuggest','selectedDestId',false);	
		new AutoSuggest($fn(DestinationBNC),'autosuggest1','selectedDestId',false);
		//when loads first time 'DestinationRN' does not exist
		if(null != $fn(DestinationRN))
		{
		    new AutoSuggest($fn(DestinationRN),'autosuggest2','selectedDestId',false);
		}
		gCalendarFlag = 0;
		document.onclick = closePopupDivs;
		
		/* Start: jQuery calendar initialisation */
//		ArrDate_Id = _endsWith("txtArrivalDate");
//	    DepDate_Id = _endsWith("txtDepartureDate");
//        Nights_Id = _endsWith("ddlnoOfNights");
	    DateRange_Class = "dateRange";
	    Locale_Id = _endsWith("siteLang");
	    /* End initialisation */
	}

	// 	Function to initialize variables for 'Booking Module - Medium & Small'
	function initBMS() {
        // START: FORM CONTROLS
        sT 				= _endsWith("sT");
        Destination 	= _endsWith("txtHotelName");
        DestinationBNC  =_endsWith("txtHotelNameBNC");
        //Defect Fix - 439122 - Bhavya - autosuggest for reward nights
        DestinationRN   = _endsWith("txtHotelNameRewardNights");
        guestLogin 	  	= _endsWith("guestLogin");
        ArrivalDate 	= _endsWith("txtArrivalDate");
        DepartureDate 	= _endsWith("txtDepartureDate");
        splBookingCode 	= _endsWith("txtRegularCode");
        DNumber 		= _endsWith("txtNegotiatedCode");
        VoucherNumber 	= _endsWith("txtVocherCode");
        NoOfNight 		= _endsWith("txtNoOfNights");
        AdultsPerRoom 	= _endsWith("ddlAdultsPerRoom");
        ChildPerRoom 	= _endsWith("ddlChildPerRoom");
        UserName 		= _endsWith("txtUserName");
        Password 		= _endsWith("txtPassword");
        formName 		= _endsWith("BookingForm");
        selBookingType = _endsWith("ddlBookingType");
		siteLang        = _endsWith("siteLang");
        // END: FORM CONTROLS

        //Below varibales values are name of the form module, can be changed according to .NET code
        date1 			= ArrivalDate;
        date2 			= DepartureDate;

		errorDivID 	= clientErrorDiv;		
		destDiv	= $fn(destCont);
		aDiv 	= $fn(atCont);
		dDiv 	= $fn(dtCont);
		nDiv 	= $fn(nnCont);
		
		// BUG FIX: artf695582 | Free Night is not coming as selected in refine search module.
		var actvTab = getCookie("ActiveTab")
		
		if (!actvTab)
		{
		    actvTab = 'Tab1';
		}
		//SCANR-187 : Modify booking with chidren throws page could not be loaded in case of BC
		if ($fn(_endsWith("hdnModifyOrCancelPageName")) != null && $fn(_endsWith("hdnModifyOrCancelPageName")).value == "ModifyOrCancelPage")
		    actvTab = 'Tab1';
		
		$fn(sT).value = actvTab;
		
		//This is not required Raj.
		//$fn(selBookingType).value = (actvTab).charAt(3);
//		showTabContent(actvTab);
		
		if(Destination != null)		
		{
		    new AutoSuggest($fn(Destination),'autosuggest','selectedDestId',false);
		}
		if(DestinationBNC != null)    
		{
		    new AutoSuggest($fn(DestinationBNC),'autosuggest1','selectedDestId',false);
		}   
		//when loads first time 'DestinationRN' does not exist
		if(DestinationRN != null)
		{
		    new AutoSuggest($fn(DestinationRN),'autosuggest2','selectedDestId',false);
		} 		
		gCalendarFlag = 0;
		document.onclick = closePopupDivs;
		
		/* Start: jQuery calendar initialisation */
//		ArrDate_Id = _endsWith("txtArrivalDate");
//	    DepDate_Id = _endsWith("txtDepartureDate");
//	    Nights_Id = _endsWith("txtNoOfNights");
	    DateRange_Class = "dateRange";
	    Locale_Id = _endsWith("siteLang");
	    /* End initialisation */
	}

	// 	Function to initialize variables for Refine Search
	function initRS() {	
		errorDivID 	= clientErrorDiv;
		aDiv 		= $fn(atCont);
		dDiv 		= $fn(dtCont);
		nDiv 		= $fn(nnCont); 
		
		dt 	= $fn(DepartureDate);
		at 	= $fn(ArrivalDate);
		nt 	= $fn(NoOfNight);
		
		gCalendarFlag = 0;
		document.onclick = closePopupDivs;
		/* Start: jQuery calendar initialisation */
		ArrDate_Id = _endsWith("txtArrivalDate");
	    DepDate_Id = _endsWith("txtDepartureDate");
	    Nights_Id = _endsWith("txtNoOfNights");
	    DateRange_Class = "dateRange";
	    Locale_Id = _endsWith("siteLang");
	    /* End initialisation */
	}

	// 	Function to initialise variable for calendar in the 'main body' of the 'Missing Points Page'
	function initCal() {
	    ArrivalDate1 	= _endsWith("txtArrivalDate1");
        DepartureDate1 	= _endsWith("txtDepartureDate1");
        ddlHotel           = _endsWith("ddlHotel");
		date3 			= ArrivalDate1;
        date4 			= DepartureDate1;
		errorDivID 	= clientErrorDiv1;
		aDiv1 		= $fn(atCont1);
		dDiv1 		= $fn(dtCont1);
	
	
		dt1 = $fn(DepartureDate1);
		at1 = $fn(ArrivalDate1);	
		
		/* Start: jQuery calendar initialisation */
		mp_ArrDate_Id = _endsWith("txtArrivalDate1");
	    mp_DepDate_Id = _endsWith("txtDepartureDate1");
		mp_DateRange_Class = "mp_dateRange";
		Locale_Id = _endsWith("siteLang");
	    /* End initialisation */	
	}
	
	//	Function to initialize variables for Modify Booking Calendar
	function initBookingCal() {	
		errorDivID 	= clientErrorDiv;
		
		aDiv 		= $fn(atCont);
		dDiv 		= $fn(dtCont);
		nDiv 		= $fn(nnCont);	
		
		ArrivalDate 	= _endsWith("txtmodifyArrivalDate");
		DepartureDate 	= _endsWith("txtmodifyDepartureDate");
		NoOfNight 		= _endsWith("modifyNoOfNight");
		siteLang        = _endsWith("siteLang");
		
		dt 	= $fn(DepartureDate);
		at 	= $fn(ArrivalDate);
		nt  = $fn(NoOfNight);
		
		date1 = ArrivalDate;
        date2 = DepartureDate;
        		
		gCalendarFlag = 0;
		document.onclick = closePopupDivs;		
		
		/* Start: jQuery calendar initialisation */
		ArrDate_Id = _endsWith("txtmodifyArrivalDate");
	    DepDate_Id = _endsWith("txtmodifyDepartureDate");
	    Nights_Id = _endsWith("modifyNoOfNight");
	    DateRange_Class = "modifyDateRange";
	    Locale_Id = _endsWith("siteLang");
	    /* End initialisation */	
	}
 
	function displayClientError(msg, errorclass) {
		var obj = document.getElementById(_endsWith(errorDivID));

		if (msg == "noError" || msg == "") {
		obj.style.visibility = isHidden;
		obj.style.display = "none";
		} else {
			obj.style.visibility = isVisible;
			obj.style.display = "block";
        
			var errorStrings = "";
			for(var i=0; i<errorMessagesArray.length; i++) {
				var errorMsgDiv = document.getElementById(_endsWith(errorMessagesArray[i])); 
				if(errorMsgDiv != null) {
						errorStrings = errorStrings + "<li>" + errorMsgDiv.value + "</li>\n";				
				}
			}
    			
			if (errorStrings != "") {      			
				var start = "<div id='errorReport' class='formGroupError'><!-- Error Message --><div class='redAlertIcon'>"+$fn(errMsgTitle).value+"</div><ul class='circleList'>"
				var end = "</ul><!-- /Error Message --></div>";          		       		
				obj.innerHTML = start + errorStrings + end;
       		}
       		if(gCalendarFlag != null)
       		{
		        if (gCalendarFlag == 1 || gCalendarFlag == 2) {
				    closeCal();
		        }
        	}
    	}
    } 
		
function initRefineBooking()
{
	errorDivID 	= clientErrorDiv;
	
	aDiv 	= $fn(atCont);
	dDiv 	= $fn(dtCont);
	nDiv 	= $fn(nnCont);
	
	ArrivalDate 	= _endsWith("txtArrivalDate");
	DepartureDate 	= _endsWith("txtDepartureDate");
	NoOfNight 		= _endsWith("txtNoOfNights");
	AdultsPerRoom 	= _endsWith("ddlAdultsPerRoom");
	ChildPerRoom 	= _endsWith("ddlChildPerRoom");
	siteLang        = _endsWith("siteLang");
	
	dt 	= $fn(DepartureDate);
	at 	= $fn(ArrivalDate);
	nt  = $fn(NoOfNight);
	
	date1 			= ArrivalDate;
	date2 			= DepartureDate;
   
	gCalendarFlag = 0;
	document.onclick = closePopupDivs;
	
	/* Start: jQuery calendar initialisation */
	ArrDate_Id = _endsWith("txtArrivalDate");
    DepDate_Id = _endsWith("txtDepartureDate");
    Nights_Id = _endsWith("txtNoOfNights");
    DateRange_Class = "dateRange";
    Locale_Id = _endsWith("siteLang");
    /* End initialisation */
	    
}

function initCancelBookingConfirmation()
{
	chkcancelBookingSMS = $fn(_endsWith("chkcancelBookingSMS"));
	ddlTel2 = $fn(_endsWith("ddlTel2"));
	txtTelephone2 = $fn(_endsWith("txtTelephone2"));
}

//Function to load Javscript links dynamically. @param : URL of the .js file to be loaded
function dhtmlLoadScript(url)
{
   var e = document.createElement("script");
   e.src = url;
   e.type="text/javascript";
   document.getElementsByTagName("head")[0].appendChild(e);
}

//Close the popup div's on document click.
function closePopupDivs(event)
{
	if(typeof (autosuggest) != "undefined")
	{
	    if ($fn(autosuggest) != null) {		
		    closeAutoSuggest();
	        }
	}/*
	if(typeof (LoginControl) != "undefined")
	{
		if ($fn(LoginControl) != null) 
		{	
			showLoginPopup(event);		
		}
	}*/
}
//This will associate controls based on the accordian selected so that the calendar will work properly.
function associateControlBasedOnAccordianSelected()
{
    var currentaccordian = $fn(_endsWith("sT")).value;
    if(currentaccordian == 'Tab1')
    {
        //If Tab1 selected
        ArrDate_Id = _endsWith("txtArrivalDate");
        DepDate_Id = _endsWith("txtDepartureDate");
        Nights_Id = _endsWith("txtnoOfNights");
    }
    else if(currentaccordian == 'Tab2')
    {
        //If Tab2 selected
        ArrDate_Id = _endsWith("txtBCArrivalDate");
        DepDate_Id = _endsWith("txtBCDepartureDate");
        //Release R 2.0| Change txtBonousChequeNoofNights to ddlBonousChequeNoofNights(Shameem)
        Nights_Id = _endsWith("txtBonousChequeNoofNights");
    }
    else if(currentaccordian == 'Tab3')
    {
        //If Tab3 selected
        if((_endsWith("txtArrivalDate") != null) && (_endsWith("txtDepartureDate")!= null) && (_endsWith("txtRewardNightNoOfNights")!= null))
        {
            //If Tab3 selected
            //Release R 2.0| Change ddlRewardNightNoOfNights to txtRewardNightNoOfNights(Shameem)
            ArrDate_Id = _endsWith("txtRNArrivalDate");
            DepDate_Id = _endsWith("txtRNDepartureDate");
            Nights_Id = _endsWith("txtRewardNightNoOfNights");
        }
    }
}
