	
	// keep global vars in collection object
	var hotels_globals = {
		selectedCountry: ''
	}; 
	
	// reference to function later defined in jquery context
	var assembleHotels, initScroll;

	// calculate scroll content width
	function calculateContentWidth() {
		contentW = 0;
		var $content = $("#content-content");
		$(".content-item:visible", $content).each(function() {
			contentW += $(this).outerWidth(true); 
		});
		$content.css("width", contentW + "px");
		return(contentW);
	}
	
	// ---------------------------------------------------
	
	function filterHotelsIE6(country) {
		var $hotels = $("#content-content"); // cache context
		
		// prepare content
		$hotels.css("width", "200000px");
		$hotels[0].innerHTML = "...";
		
		// label each hotel as visible/invisible
		var i; n=aHotels.length;
		for (i=0; i<n; i++) {
			aHotels[i].visible = (country) ? aHotels[i].country === country : true;	
		}
		
		// recreate hotels
		assembleHotels();
		
		// disable/enable scrollbar based on width of scrollable content
		var isContentScollable = isScrollable();
		var scrollOpacity = (isContentScollable) ? 1 : 0;
		$("#scroll-btn-left, #scrollbar-btn-center, #scrollbar-btn, #scroll-btn-right").css("opacity", scrollOpacity);	
		
		// scroll to beginning (to the left)
		gotoBeginning();
		
	}
	
	
	function filterHotels(country) {  // filter function: show only a specific country's hotels
	
		// hotfix for ie6, 
		if (hotels_globals.ie6) {filterHotelsIE6(country);return;}
        
		var $hotels = $("#content-content"); // cache context
		var country = (country.length) ? country : '';
		
		// first, show all hotels..
		var $theHotels = $(".content-item[country]", $hotels);
		if (hotels_globals.ie67){ 
			$theHotels.show(); // quirk fix (opacity does not work well in ie6/ie7)
		} else {
			$theHotels.css("opacity",0).show();
			$theHotels.animate({opacity:1},'fast');
		}
		
		// ..then filter on selected country
		if (country.length) {
			var selector = ".content-item[country!=" + country + "]";
			var otherHotels = $(selector, $hotels);

			if (hotels_globals.ie67) { 
				otherHotels.hide(); // ie6 quirk fix (opacity does not work well in ie6)
			} else {
				otherHotels.animate({opacity:0},'fast','',otherHotels.hide());
			}
		}
		
		// new
		calculateContentWidth();
		
		// update global vars object
		hotels_globals.selectedCountry = country;
		
		// disable/enable scrollbar based on width of scrollable content
		var isContentScollable = isScrollable();
		var scrollOpacity = (isContentScollable) ? 1 : 0;
		$("#scroll-wrap").animate({opacity:scrollOpacity}, 'fast');
		
		// ie fix (ie cannot set opacity on parent object, must set on each child)
		if ($.browser.msie) {
			$("#scroll-btn-left, #scrollbar-btn-center, #scrollbar-btn, #scroll-btn-right").css("opacity", scrollOpacity);	
		}
		
		// scroll to beginning (to the left)
		gotoBeginning();
	}
	
	// ---------------------------------------------------	
	
	function gotoBeginning() {
		// move scrollbar button
		$("#scrollbar-btn").animate({left:"0px"}, 'fast');
		// move hotels
		$("#content-content").animate({marginLeft: "0px"}, 'fast');
	}
	
	// ---------------------------------------------------	
	
	function isScrollable() {
		var $visibleChildren = $("#content-content .content-item").filter(":visible");
		var w = 0;
		$visibleChildren.each(function() {
			w += $(this).outerWidth(true);						   
		});
		return(w > $("#content-wrap").width())
	}
	
	// ---------------------------------------------------	
	
	function initHotelCalendar() {
		//Flashvars passed into the swf
		var flashvars = {
			lang: $fn(_endsWith('SelectedLanguage')).value,
			xmlPath: $fn(_endsWith('XMLFilePath')).value
		};			
		// override with custom language
		var params = location.search.substring(1).split("&");
		for (var i=0; i<params.length; i++) {
			var keyValue = params[i].split("=");
			if (keyValue[0] == 'lang' && keyValue[1]) {flashvars.lang = keyValue[1];break;}
		}
		//Params for the swf object
		var params = {
			menu: "false",
			scale: "noScale",
			allowScriptAccess: "always",
			bgcolor: "#000000",	
			wmode: "transparent"
		};

		//Attributes for the swf object
		var attributes = {
			id: 'calendarSwf',
			name: 'calendarSwf'
		};
		
		//Object embedding
		swfobject.embedSWF(
			$fn(_endsWith('FlashFilePath')).value,
			'flashCalendar',
			'1004',
			'74',
			'9',
			null,
			flashvars,
			params,
			attributes);
	}

	// ---------------------------------------------------

	function getSelectedCountry() {
		return(hotels_globals.selectedCountry);
	}
	
	function CenterGoogleMapOverlay()
    {
        var viewportwidth;
        var viewportheight;

        // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
        if (typeof window.innerWidth != 'undefined')
        {
          viewportwidth = window.innerWidth,
          viewportheight = window.innerHeight
        }

        // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
        else if (typeof document.documentElement != 'undefined'
         && typeof document.documentElement.clientWidth !=
         'undefined' && document.documentElement.clientWidth != 0)
        {
           viewportwidth = document.documentElement.clientWidth,
           viewportheight = document.documentElement.clientHeight
        }

        // older versions of IE
        else
        {
           viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
           viewportheight = document.getElementsByTagName('body')[0].clientHeight
        }
        
        var mapOverlayWidth = $('#googleMapContainer').width();
        var mapOverlayHeight = $('#googleMapContainer').height();
        
        var mapOverlayLeft = (viewportwidth / 2) - (mapOverlayWidth / 2);
        var mapOverlayTop = (viewportheight / 2) - (mapOverlayHeight / 2);
        
        if(mapOverlayLeft < 0) mapOverlayLeft = 0;
        if(mapOverlayTop < 30) mapOverlayTop = 30;
        
        $('#googleMapContainer').css('left', mapOverlayLeft+'px');
        $('#googleMapContainer').css('top', mapOverlayTop+'px');
        $('#hotels-wrap').css('position','static');
   
    }
	
	// ---------------------------------------------------	
	
	$(document).ready(function(){
		
		// show scrollbar if more than 4 hotels are available
		if (aHotels.length > 4) {$("#scroll-wrap").show();}

		
		// cufon fonts
		
		$("#hotels-header #hotels-header-title h1, #hotels-header #hotels-header-title h3").show();
							   
		// browser detection shortcuts
		hotels_globals.ie6 = ($.browser.msie && $.browser.version == 6);
		hotels_globals.ie7 = ($.browser.msie && $.browser.version == 7);
		hotels_globals.ie67 = (hotels_globals.ie6 || hotels_globals.ie7);
		hotels_globals.ff2 = ($.browser.mozilla && parseInt($.browser.version) < 3);
						    
		// assemble hotels html output
		assembleHotels = function() {
			
			var sHotels = "";
			for (var i=0; i<aHotels.length; i++) {
				
				// IE6 bug fix for scrolling after filter
				if (hotels_globals.ie6 && typeof(aHotels[i].visible) === "boolean" && aHotels[i].visible === false) {continue;}
				
//				var s = '<div class="info">';
//					s += 	'<img class="thumb" src="' + aHotels[i].thumb + '" />';
//					s += 	'<h2>' + aHotels[i].name + '<span class="a-moreinfo">' + aHotels[i].urltitle + '</span></h2>';
//					s += '</div>';
//					s += '<div class="moreinfo-bg"></div>';
//					s += '<div class="moreinfo">';
//					s += 	'<p>' + ReplaceAll(aHotels[i].text,'#',"'") + '</p>';
//					s += 	'<cite>' + aHotels[i].info + '</cite>';
//					s += '</div>';
//					s += '<div class="clear"/></div>';
//				sHotels += "<div class='content-item' country='" + aHotels[i].country + "' url='" + aHotels[i].url + "'>" + s + "</div>";
//				
				var s = '<div class="info">';
					s += 	'<img class="thumb" src="' + aHotels[i].thumb + '" />';
					s += 	'<table width="100%"><tr>';
					s +=		'<td class="hotel-name" valign="top">' + aHotels[i].name + '</td>';
					s +=		'<td valign="top" align="right"><div class="btn-left"><div class="btn-right">' + aHotels[i].urltitle.replace(/ /gi, "&nbsp;")  + '</div></div></td>';
					s +=	'</tr></table>';
					s += '</div>';
					s += '<div class="moreinfo-bg"></div>';
					s += '<div class="moreinfo">';
					s += 	'<p>' + ReplaceAll(aHotels[i].text,'#',"'") + '</p>';
					s += 	'<cite>' + aHotels[i].info + '</cite>';
					s += '</div>';
					s += '<div class="clear"/></div>';
				sHotels += "<div class='content-item' country='" + aHotels[i].country + "' url='" + aHotels[i].url + "'>" + s + "</div>";
			}
			//Replace # with single quotes in Hotel description.
			function ReplaceAll(Source,stringToFind,stringToReplace){
                var temp = Source;
                var index = temp.indexOf(stringToFind);
                while(index != -1){
                    temp = temp.replace(stringToFind,stringToReplace);
                    index = temp.indexOf(stringToFind);
                }
                return temp;
            }
			// insert hotels into DOM
			$("#content-content")[0].innerHTML = sHotels;
		
			// hotel mouseover : show more info			
			$("#content-content .content-item").hover(
				function(event) { // mouseover
					$(".moreinfo", this).css({opacity: 1});
					$(".moreinfo-bg", this).css({opacity: .75});
					if($.browser.msie) {$("cite", this).show().css({opacity:1});}
				},
				function(event) { // mouseout
					$(".moreinfo", this).animate({opacity: 0}, 250);
					$(".moreinfo-bg", this).animate({opacity: 0}, 250);
					if($.browser.msie) {$("cite", this).show().animate({opacity:0}, 250);}
				}
			);
				
			// hide more info layers
			$("#content-content .moreinfo, #content-content .moreinfo-bg").css("opacity", 0);
			// ie css bug fix
			if($.browser.msie) {$("#content-content .moreinfo cite").hide();}
		
			// click handler for each hotel
			$("#content-content .content-item").click(function(event) {
				//window.open($(this).attr("url"));
				document.location.href  =$(this).attr("url");
			});
		}
		assembleHotels();
		
		// init country dropdown
		$("#country-dropdown div.selected").click(function() {
			$("#country-dropdown-options ul").slideToggle();
		});
		
		// ie6-specific: add hover effect dropdown objects 
		if (hotels_globals.ie6) {
			$("#hotels-wrap div.selected").hover(
				function() {$(this).css("background-position", "0 -90px");},
				function() {$(this).css("background-position", "0 -70px");}
			);
			$("#hotels-wrap div.dropdown ul li").hover(
				function() {$(this).css("background","black");},
				function() {$(this).css("background","");}
			);

		}
					   
		// select country
		$("div.dropdown ul li").click(function() {
			$this = $(this); // cache context
			
			// confirm selection
			$("#country-dropdown div.selected").text($this.text());
			
			// close dropdown list
			$("div.dropdown ul").slideToggle();
		});
		
		// remove border from last item in country dropdown
		$("#country-dropdown ul li:last").css("border-bottom", "0px solid #ccc");
		
		// close dropdown when click elsewhere
		$(document).click(function(e) {
			if ($(e.target).parents("#country-dropdown").length == 0) {
				$("div.dropdown ul").slideUp();	
			}
		});
		
		 // google map btn hover
		$("#btnMap").hover(function() {$(this).addClass("hover");},function() {$(this).removeClass("hover");});
		
		// google map overlay
		$("#btnMap").click(function() {
			$("#googleMapBackground").show();
			$("#googleMapContainer").css({marginLeft:0});
			
			// fix background for ie6
			if (hotels_globals.ie6) {
				var $googleBackground = $("#googleMapBackground");
				var docWidth = $(document).width();
				var docHeight = $(document).height();
				var winWidth = $(window).width();
				var winHeight = $(window).height();
				var bgWidth = (docWidth > winWidth) ? docWidth : winWidth;
				var bgHeight = (docHeight > winHeight) ? docHeight : winHeight;
				bgHeight=bgHeight+250;
				var bgLeft = -$googleBackground.offset().left + "px";
				var bgTop = -$googleBackground.offset().top + "px";
				$googleBackground.css({
					position: "absolute"
					,marginLeft: -$("#hotels-header").offset().left + "px"
					,marginTop: -$("#hotels-header").offset().top + "px"
					,height: bgHeight + "px"
					,width: bgWidth + "px"
				});
				$('#MainMenuArea').css('z-index','-1');
			}
			
			CenterGoogleMapOverlay();
		});
		
		// click handler for google close button
		$("#btnCloseGoogleMap").click(function() {
		    if (typeof ResetMap == 'function'){ ResetMap(); }
			$("#googleMapBackground").hide();
			$("#googleMapContainer").css({marginLeft:'-9999px'});
			$('#MainMenuArea').css('z-index','10');
			$('#hotels-wrap').css('position','relative');
          
		});
		
		// turn on shadowblur edges for png enabled browsers
		$("#content-wrap").append('<div id="blur-left"></div><div id="blur-right"></div>');
		if (hotels_globals.ie6) {
			// fix google map background for ie6 (belated_png cannot handle fixed bg)
			DD_belatedPNG.fix('#blur-left, #blur-right, #btnCloseGoogleMap, #HotelFilter p img, .hotelFilter p img');
		};
		
		function initScroll() {
			// cache jquery objects
			var scrollBtn 		= document.getElementById("scrollbar-btn");
			var $scrollBtn 		= $(scrollBtn);
			var $scrollWrap		= $("#scrollbar-btn-center");
			var $content		= $("#content-content");
			var $contentWrap 	= $("#content-wrap");
			
			// vars used for drag events
			var contentW, contentWrapWidth, scrollW, scrollRatio, visibleChildren;
			var scrollBtnWidth = $scrollBtn.outerWidth(true); // width of scrollbutton (including margins & padding)
			scrollW	= $scrollWrap.width() - scrollBtnWidth; // width of scrollable estate in scrollbar
			
			calculateContentWidth(); // recalculate total width of the scrollable content
			
			// enable/disable scrollbar based on scrollable content width
			$("#scrollbar-btn").mousedown(function() {
				var draggable = (isScrollable()) ? "enable" : "disable";
				$(this).draggable(draggable);
			});
			
			function scrollIt() {
				var scrollBtnLeft = $scrollBtn.css("left").split("px")[0];
				var contentLeft = parseInt(scrollBtnLeft * scrollRatio);
				$content.css("margin-left", -contentLeft + "px"); 
			}
			
			// init scrollbar
			$("#scrollbar-btn").draggable({
				containment: 'parent',
				axis: 'x',
				start: function() {
					calculateContentWidth();
					contentW 			= $content.outerWidth(); // width of scrollable content
					contentWrapWidth 	= $contentWrap.width();
					scrollRatio			= (contentW-contentWrapWidth) / scrollW;
					
					// ie cannot use the drag event (too laggy)
					if ($.browser.msie) {setInterval(scrollIt, 50);}
					
				},
				drag: function(e) {
					if (!$.browser.msie) {scrollIt();}
				}
			});
			
			// set up left and right scroll buttons
			$("#scroll-btn-left").mousedown(function() {
						
				// disable button if there is no scrollable content (all content is showing)
				if (!isScrollable()) {return;}
				
				// make sure content is not already at farmost left
				var $content 	= $("#content-content");
				var contentLeft = parseInt($content.css("margin-left"))
				if (contentLeft == 0) {return};
				
				// cache jquery objects
				var $contentWrap 	= $("#content-wrap");
				var $scrollBar 		= $("#scrollbar-btn-center");
				var $scrollBtn 		= $("#scrollbar-btn");
				
				// calculate distances to move for both content and scrollbar
				var contentWidth = calculateContentWidth();
				var scrollableWidth = contentWidth - $contentWrap.width();
				var scrollUnit = $(".content-item:first", $content).outerWidth(true); // = width of one content-item (hotel)
				var moveDistance = (Math.abs(contentLeft) > scrollUnit) ? scrollUnit : Math.abs(contentLeft);
				var scrollRatio = moveDistance / scrollableWidth;
				var scrollWidth = $scrollBar.width() - $scrollBtn.outerWidth(true);
				var scrollDistance = parseInt(scrollWidth * scrollRatio);
				
				// css values for animation
				var cssMarginLeft = (moveDistance < scrollUnit) ? "0px" : "+=" + moveDistance + "px";
				var cssLeft    = (moveDistance < scrollUnit) ? "0px" : "-=" + ((scrollDistance > scrollWidth) ? scrollWidth : scrollDistance) + "px";
				
				// animate content and scrollbar button
				$content.animate({marginLeft: cssMarginLeft}, 'fast');
				$scrollBtn.animate({left: cssLeft}, 'fast');
			});
			
			// set up left and right scroll buttons
			$("#scroll-btn-right").mousedown(function() {
												
				// disable button if there is no scrollable content (all content is showing)
				if (!isScrollable()) {return;}
				
				// cache jquery objects
				var $contentWrap 	= $("#content-wrap");
				var $scrollBar 		= $("#scrollbar-btn-center");
				var $scrollBtn 		= $("#scrollbar-btn");
				var $content 		= $("#content-content");
				
				// calculate distances to move for both content and scrollbar
				var contentWidth = calculateContentWidth();
				var contentPadding = parseInt($content.css("padding-right")) + parseInt($content.css("padding-left"));
				
				var contentLeft = parseInt($content.css("margin-left"));
				contentLeft = (isNaN(contentLeft)) ? 0 : contentLeft;
				var contentRight = contentWidth + contentLeft + contentPadding;
				var contentWrapWidth = $contentWrap.width();
				var scrollableWidth = contentWidth - $contentWrap.width();
				var scrollUnit = $(".content-item:first", $content).outerWidth(true); // = width of one content-item (hotel)
				var moveDistance = (contentRight - scrollUnit > contentWrapWidth) ? scrollUnit : contentRight - contentWrapWidth; //Math.abs(contentLeft);
				
				var scrollRatio = moveDistance / scrollableWidth;
				var scrollWidth = $scrollBar.width() - $scrollBtn.outerWidth(true);
				var scrollDistance = parseInt(scrollWidth * scrollRatio);
				var scrollBtnLeft = parseInt($scrollBtn.css("left"));
				
				if (scrollBtnLeft + scrollDistance >= scrollWidth) {
					scrollDistance = scrollWidth - scrollBtnLeft;
				}
				
				// css values for animation
				var cssMarginLeft = (moveDistance < scrollUnit) ? -(contentWidth - contentWrapWidth + contentPadding) + "px" : "-=" + moveDistance + "px";
				var cssLeft    = (moveDistance < scrollUnit) ? scrollWidth + "px" : "+=" + ((scrollDistance > scrollWidth) ? scrollWidth : scrollDistance) + "px";
				
				// animate content and scrollbar button
				$content.animate({marginLeft: cssMarginLeft}, 'fast');
				$scrollBtn.animate({left: cssLeft},'fast');
				
			});
			
		}
		
		// init hotel scrollable area
		initScroll();
		
		// load hotel calendar
		initHotelCalendar();
		
		// load google map
		//loadGoogleMap();
	 });