      var rulesObject = {}; // creating the rules object
      var messageObject = {}; // creating the message object
	  // Enroll Validation
	  
	  rulesObject[$("input[id$='txtFName']").attr('name')] = {
          required: true,
          No_Spl_Char: true
      };
      rulesObject[$("input[id$='txtLName']").attr('name')] = {
          required: true,
          No_Spl_Char: true
      };
      rulesObject[$("input[id$='txtAddressLine1']").attr('name')] = {
          required: true,
          No_Spl_Char_Except_Dot: true
      };
      rulesObject[$("input[id$='txtAddressLine2']").attr('name')] = {
          No_Spl_Char_Except_Dot: true
      };
	  rulesObject[$("input[id$='txtCityOrTown']").attr('name')] = {
          required: true,
          No_Spl_Char: true
      };
      rulesObject[$("input[id$='txtPostCode']").attr('name')] = {
          required: true
      };
	  rulesObject[$("select[id$='ddlCountry']").attr('name')] = {
          required: true,
          valueNotEquals: "DFT"
      };
      rulesObject[$("input[id$='txtEmail1']").attr('name')] = {
          required: true,
          email: true
      };

      rulesObject[$("select[id$='ddlTel2']").attr('name')] = {
          required: true,
          groupValidationPhone: "DFT"
      };
      rulesObject[$("input[id$='txtTelephone2']").attr('name')] = {
          required: true,
		  groupValidationPhone: "DFT",
		  customRegExp: true
      };

      rulesObject[$("select[id$='ddlDOBDay']").attr('name')] = {
          required: true,
          groupValidation: "DFT"
      };
      rulesObject[$("select[id$='ddlDOBMonth']").attr('name')] = {
          required: true,
          groupValidation: "DFT"
      };
      rulesObject[$("select[id$='ddlDOBYear']").attr('name')] = {
          required: true,
          groupValidation: "DFT"
      };
      rulesObject[$("input[id$='txtEnrollPassword']").attr('name')] = {
          required: true
      };
      rulesObject[$("input[id$='txtEnrollReTypePassword']").attr('name')] = {
          required: true,
          equalTo: "#" + [$("input[id$='txtEnrollPassword']").attr('id')]
      };
      rulesObject[$("input[id$='chkReceiveScandicInfo']").attr('name')] = {
          required: true
      };
	  
	  
	  
		  // enroll guest messages

      messageObject[$("input[id$='txtFName']").attr('name')] = {
          required: $('#invalidFirstName').val(),
          No_Spl_Char: $('#restrictSplChars').val()
      };
      messageObject[$("input[id$='txtLName']").attr('name')] = {
          required: $('#invalidLastName').val(),
          No_Spl_Char: $('#restrictSplChars').val()
      };
      messageObject[$("input[id$='txtAddressLine1']").attr('name')] = {
          required: $('#invalidaddressLine1').val(),
          No_Spl_Char_Except_Dot: $('#restrictSplChars').val()
      };
      messageObject[$("input[id$='txtAddressLine2']").attr('name')] = {
          No_Spl_Char_Except_Dot: $('#restrictSplChars').val()
      };
      messageObject[$("input[id$='txtPostCode']").attr('name')] = {
          required: $('#invalidPostcode').val(),
          digits: $('#invalidPostcode').val()
      };
      messageObject[$("input[id$='txtCityOrTown']").attr('name')] = {
          required: $('#invalidCityTown').val(),
          No_Spl_Char: $('#restrictSplChars').val()
      };
      messageObject[$("input[id$='txtEmail1']").attr('name')] = {
          required: $('#invalidEmail').val(),
          email: $('#invalidEmail').val()
      };
      messageObject[$("select[id$='ddlCountry']").attr('name')] = {
          required: $('#invalidCountryCode').val(),
          valueNotEquals: $('#invalidCountryCode').val()
      };
      messageObject[$("select[id$='ddlTel2']").attr('name')] = {
          required: $('#invalidMobile').val(),
          groupValidationPhone: $('#invalidMobile').val()
      };
      messageObject[$("input[id$='txtTelephone2']").attr('name')] = {
          required: $('#invalidMobile').val(),
		  groupValidationPhone: $('#invalidMobile').val(),
		  customRegExp: $('#invalidMobile').val()
      };
      messageObject[$("select[id$='ddlDOBDay']").attr('name')] = {
          required: $('#invalidDateofBirth').val(),
          groupValidation: $('#invalidDateofBirth').val()
      };
      messageObject[$("select[id$='ddlDOBMonth']").attr('name')] = {
          required: $('#invalidDateofBirth').val(),
          groupValidation: $('#invalidDateofBirth').val()
      };
      messageObject[$("select[id$='ddlDOBYear']").attr('name')] = {
          required: $('#invalidDateofBirth').val(),
          groupValidation: $('#invalidDateofBirth').val()
      };

      messageObject[$("input[id$='txtEnrollPassword']").attr('name')] = {
          required: $('#invalidPassword').val()
      };
      messageObject[$("input[id$='txtEnrollReTypePassword']").attr('name')] = {
          required: $('#invalidPassword').val(),
          equalTo: $('#invalidPasswordMatch').val()
      };
      messageObject[$("input[id$='chkReceiveScandicInfo']").attr('name')] = {
      required: $('#invalidTermsConfirmation').val()
      };
	  $(document).ready(function() {
          // validate the comment form when it is submitted
          $("#aspnetForm").validate({
              onfocusout: function(element) { $(element).valid(); },
              onkeyup: false,
              success: function(label) {
                  label.addClass("validfld").text(" ");				 
              },
              validClass: "validfld",
              rules: rulesObject,
              messages: messageObject,
              errorElement: "div",
			  ignore: ".ignore, :hidden",
			  groups: {
			 // dobGroup: $("select[id$='ddlDOBDay']").attr('name') + "" + $("select[id$='ddlDOBMonth']").attr('name') + "" +  $("select[id$='ddlDOBYear']").attr('name'),
			 // telephoneGroup: $("select[id$='ddlTel2']").attr('name') + "" + $("input[id$='txtTelephone2']").attr('name')
			 dobGroup: "ctl00$MainBodyRegion$MainBodyLeftRegion$ctl04$ddlTel2 ctl00$MainBodyRegion$MainBodyLeftRegion$ctl04$txtTelephone2",
			 telephoneGroup: "ctl00$MainBodyRegion$MainBodyLeftRegion$ctl04$ddlDOBDay ctl00$MainBodyRegion$MainBodyLeftRegion$ctl04$ddlDOBMonth ctl00$MainBodyRegion$MainBodyLeftRegion$ctl04$ddlDOBYear"
			  },
              errorPlacement: function(error, element) {
			   var next = element.next();
					if (element.attr("name") == $("select[id$='ddlDOBDay']").attr('name') || element.attr("name") == $("select[id$='ddlDOBMonth']").attr('name') || element.attr("name") == $("select[id$='ddlDOBYear']").attr('name')) {
					  $(error).insertAfter($("select[id$='ddlDOBYear']"));}
					else if (element.attr("name") == $("select[id$='ddlTel2']").attr('name') || element.attr("name") == $("input[id$='txtTelephone2']").attr('name')) {
					  $(error).insertAfter($("input[id$='txtTelephone2']"));
					}else if(next.hasClass('signupTandC')){					
					$(error).prependTo(next);					
					}else{
                     $(error).insertAfter(element);
					}
              },
              submitHandler: function(form) {
                  $("div[id$='SendButtonDiv']").hide();
                  $("div[id$='SendingProgressDiv']").show();
                  isSendClicked = true;
                  form.submit();
              }
          });
      });
	  
 $(document).ready(function() {
            $.validator.addMethod("No_Spl_Char", function(value, element) {
                var regExp = /[^A-Za-z¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿ/0-9-\s]/;
                var charpos = value.search(regExp);
                var returnVal = true;
                if (value.length > 0 && charpos >= 0) { returnVal = false; }
                return this.optional(element) || returnVal;
            },
	"Special Characters are not allowed");	 
	 $.validator.addMethod("No_Spl_Char_Except_Dot", function(value, element) {
                var regExp = /[^A-Za-z¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿ/0-9-\.\s]/;
                var charpos = value.search(regExp);
                var returnVal = true;
                if (value.length > 0 && charpos >= 0) { returnVal = false; }
                return this.optional(element) || returnVal;
            },
	"Special Characters are not allowed Except Dot");	 
$.validator.addMethod("valueNotEquals", function(value, element, arg) {
                return this.optional(element) || arg != value;
            }, "Value must not equal arg.");
            jQuery.validator.addMethod("notEqualToGroup", function(value, element, options) {
                // get all the elements passed here with the same class
                var elems = $(element).parents('form').find(options[0]);
                // the value of the current element
                var valueToCompare = value;
                // count
                var matchesFound = 0;
                // loop each element and compare its value with the current value
                // and increase the count every time we find one
                jQuery.each(elems, function() {
                    thisVal = $(this).val();
                    if (thisVal == valueToCompare) {
                        matchesFound++;
                    }
                });
                //Added by Chandra--Start
                // count should be either 0 or 1 max
                if (this.optional(element) || matchesFound <= 1) {
                    elems.removeClass('error');
                    return this.optional(element) || true;
                } else {
                    elems.addClass('error');
                }
            }, "Guest names should be unique for each room.");	
			
			// this function requires month day and year selections
		$.validator.addMethod("groupValidation", function(value, element, arg) {
			if($("#ctl00_MainBodyRegion_MainBodyLeftRegion_ctl04_ddlDOBYear option:selected").val() != arg && 
			$("#ctl00_MainBodyRegion_MainBodyLeftRegion_ctl04_ddlDOBDay option:selected").val() != arg && 
			$("#ctl00_MainBodyRegion_MainBodyLeftRegion_ctl04_ddlDOBMonth option:selected").val() != arg) {
				$("#ctl00_MainBodyRegion_MainBodyLeftRegion_ctl04_ddlDOBYear, #ctl00_MainBodyRegion_MainBodyLeftRegion_ctl04_ddlDOBDay, #ctl00_MainBodyRegion_MainBodyLeftRegion_ctl04_ddlDOBMonth").removeClass("error");
				  return true;
				} else {
				  return false;
				}
		}, "Please select valid date of birth");
  
		$.validator.addMethod("groupValidationPhone", function(value, element, arg) {	
			var str = $("#ctl00_MainBodyRegion_MainBodyLeftRegion_ctl04_txtTelephone2").val();
			str = $.trim(str);
			
			if($("#ctl00_MainBodyRegion_MainBodyLeftRegion_ctl04_ddlTel2 option:selected").val() != arg && str.length != 0 ) {
				$("#ctl00_MainBodyRegion_MainBodyLeftRegion_ctl04_ddlTel2, ctl00_MainBodyRegion_MainBodyLeftRegion_ctl04_txtTelephone2").removeClass("error");
				  return true;
				} else {
				  return false;
			}
		}, "Select your country code");
  
  

  
			});
			$.validator.addMethod("customRegExp", function(value){
			
				var regEx = new RegExp("^[\\s\\d()\\/\\-]*$");
				return regEx.test(value);
				
			}, "Select proper phone");
