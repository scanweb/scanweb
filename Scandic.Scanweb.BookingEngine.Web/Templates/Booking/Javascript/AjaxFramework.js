﻿//[MS] Ajax call framework

/* namespacing object */
var net = new Object();

net.READY_STATE_UNINITIALIZED=0;
net.READY_STATE_LOADING=1;
net.READY_STATE_LOADED=2;
net.READY_STATE_INTERACTIVE=3;
net.READY_STATE_COMPLETE=4;

/*— content loader object for cross-browser requests —*/
net.ContentLoader=function(url,onload,async,method,params,onerror,contentType){
  this.req=null;
  this.onload=onload;
  this.async=async;
  this.onerror=(onerror) ? onerror : this.defaultError;
  this.loadXMLDoc(url,method,params,contentType);
  this.parameter = null;
}

net.ContentLoader.prototype.loadXMLDoc=function(url,method,params,contentType){
  if (!method){
    method="GET";
  }
  
  if (!contentType && method=="POST"){
    contentType='application/x-www-form-urlencoded';
  }
  if (window.XMLHttpRequest){
    this.req=new XMLHttpRequest();
  } 
  else if (window.ActiveXObject){
    this.req=new ActiveXObject("Microsoft.XMLHTTP");
  }
  if (this.req){
    try{
      var loader=this;
      this.req.onreadystatechange=function(){
        net.ContentLoader.onReadyState.call(loader);
      }
      var async;
      if (this.async==false) async=false; else async=true;
      this.req.open(method,url,async);
      if (contentType){
        this.req.setRequestHeader('Content-Type', contentType);
      }
      
      //[MS]
      //showWait();
      
      this.req.send(params);
    }
    catch (err){
      this.onerror.call(this);
    }
  }
}

net.ContentLoader.onReadyState=function(){
  var req=this.req;
  var ready=req.readyState;
  if (ready==net.READY_STATE_COMPLETE){
    var httpStatus=req.status;
      if (httpStatus==200 || httpStatus==0){
        //[MS]
        //hideWait();
        this.onload.call(this);
      }
      else {
        //[MS]
        //hideWait();
        this.onerror.call(this);
      }
  }
}

net.ContentLoader.prototype.defaultError=function(){
/*
  alert("error fetching data!"
    +"\n\nreadyState:"+this.req.readyState
    +"\nstatus: "+this.req.status
    +"\nheaders: "+this.req.getAllResponseHeaders());
  */  
    //document.body.innerHTML=this.req.responseText;
}

function getDataFromServer(url, callback, async, method, params) {
  loader = new net.ContentLoader(url, callback, async, method, params);
}

