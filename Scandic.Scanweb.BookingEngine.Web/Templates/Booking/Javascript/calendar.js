var tmpDay	= "";
var gThisMonth	= "";
var gThisYear	= "";
var gLastMonth	= "";
var gLastYear	= "";
var gNextMonth	= "";
var gNextYear	= "";
var closeText = ""

var calHTML	= "";

var lastFocus	= "";

var activeCal	= 0;
var hiliteStartNum = 0;
var hiliteEndNum = 0;
var hiliteColor	= "#FFFFFF";


// KW: Fixed The Calender Close Issue
//var gImgID			= "emptyDiv";
//var gcurrentImgID 	= "0";
// End of Changes

var posEventObj = -1;

var todayDay	= startDay;
var todayYear	= startYear;
var todayMonth	= startMonth;

//To be used for CR439
var curMonth1 = "";
var curYear1 = "";
var curMonth2 = "";
var curYear2 = "";
var curMonth3 = "";
var curYear3 = "";
var curMonth4 = "";
var curYear4 = "";
var i = 0;
// end of declaration

var cal_on	= new Image();
var cal_left	= new Image();
var cal_left_off	= new Image();
var cal_right	= new Image();
var cal_right_off	= new Image();
var cal_exit	= new Image();

cal_on.src	= imgPath + "icn_calender.gif";
cal_left.src	= imgPath + "btn_page_prev_active.gif";
cal_right.src	= imgPath + "btn_page_next_active.gif";
cal_left_off.src	= imgPath + "btn_page_prev_inactive.gif";
cal_right_off.src	= imgPath + "btn_page_next_inactive.gif";
cal_exit.src	= imgPath + "btn_cal_close_window.gif";

//This is called from Arrival date field of the booking module.
function setCalFieldNOpenCal1(arrivalDate, departureDate, calState) {
	errorDivID 	= clientErrorDiv;
	if ($fn(siteLang)!=null){
		gSiteLanguage = $fn(siteLang).value;
	}
	allowPastDate = 'n';
	allowFutureDate = 'y';
	preOpenCal(arrivalDate, departureDate, calState);
}

//This is called from departure date field of the booking module.
function setCalFieldNOpenCal2(arrivalDate, departureDate, calState) {	
	errorDivID 	= clientErrorDiv;	
	if ($fn(siteLang)!=null){
		gSiteLanguage = $fn(siteLang).value;
	}
	allowPastDate = 'n';
	allowFutureDate = 'y';
	preOpenCal(arrivalDate, departureDate, calState);
}

function openCalendarPopup(arrivalDate1, departureDate1, calState) {
	errorDivID 	= clientErrorDiv1;
	if ($fn(siteLang)!=null){
		gSiteLanguage = $fn(siteLang).value;
	}
	allowPastDate = 'y';
	allowFutureDate = 'n';
	openCalendar(arrivalDate1, departureDate1, calState);
}

function doCalendar(mo, yr)
{
	var newHTML	= "";
	newHTML	= genCalendar(mo, yr);
	showLayer("calendarLayer", newHTML);
}

function showLayer(layername, layerContent)
{
	$fn(layername).innerHTML	= layerContent;
}

function genCalendar(mo, yr)
{
	args	= new Array();
	calHTML	= "";

	setGlobals(mo, yr);
	begintable();
	gentableHeader(mo, yr);
	makeMonth(mo, yr);
	gentableFooter()
	endtable();
	return calHTML;
}

function setGlobals(mo, yr)
{
	args	= getLinkArgs(mo, yr);
	gThisMonth	= mo;
	gThisYear	= yr;
	gLastMonth	= args[0];
	gLastYear	= args[1];
	gNextMonth	= args[2];
	gNextYear	= args[3];
}

function begintable()
{
	calHTML = calHTML + "<table border=0 id='cTbl' width=\"186\"cellpadding=\"0\"cellspacing=\"0\">\n";
}

function endtable()
{
	calHTML = calHTML + "</table>\n";
}

function beginRow()
{
	calHTML = calHTML + "<tr>\n";
}

function endRow()
{
	calHTML = calHTML + "</tr>\n";
	calHTML = calHTML + "<tr>\n";
	calHTML = calHTML + "<td class='dclose' height=\"4\"colspan=\"7\"><img src='"+ imgPath + "spacer.gif' alt=\"\"height=\"4\"border=\"0\"/></td>";
	calHTML = calHTML + "</tr>\n";
}

function makeMonth(mo, yr)
{
	var monthDateObj	= new Date(yr, mo, 01);
	var lastDay	= daysInMonth(monthDateObj);
	var firstDay	= monthDateObj.getDay();
	var calPos	= 0;
	var calDate	= 0;

	while (calDate < lastDay)
	{
		beginRow();

		for (var i = 0;i < 7;i++)
		{
			if ((calPos < firstDay)  || (calDate > lastDay - 1) )
			{
				gentableCell();
			}
			else
			{
				calDate++;

				if (isToday(yr, mo, calDate) )
				{
					gentableCell(calDate, false);
				}
				else
				{
					gentableCell(calDate, false);
				}
			}

			calPos++;
		}

		endRow();
	}
}

function getLinkArgs(mo, yr)
{
	argsArray	= new Array();
	lastArray	= new Array();
	nextArray	= new Array();

	var monthDateObj	= new Date(yr, mo, 01);
	lastArray	= monthAddSimple(monthDateObj, -1);
	nextArray	= monthAddSimple(monthDateObj, 1);

	argsArray[0]  = lastArray[0];
	argsArray[1]  = lastArray[1];
	argsArray[2]  = nextArray[0];
	argsArray[3]  = nextArray[1];

	return argsArray;
}


function gentableHeader(mo, yr)
{
	calHTML = calHTML + "<tr>\n";
	calHTML = calHTML + "<td colspan=2 class='dclose' ><img src='"+ imgPath + "spacer.gif' width=\"186\"alt=\"\"height=\"1\"border=\"0\"/></td>";
	calHTML = calHTML + "</tr>\n";
	calHTML = calHTML + "<tr>\n";
	calHTML = calHTML + "<td colspan=2 class='dclose' align=\"left\" valign=\"middle\">";


	calHTML = calHTML + "<table border=\"0\"cellpadding=\"0\"cellspacing=\"0\" width=\"160\"align=\"center\">\n"; //  Bug Fixed : 18894
	calHTML = calHTML + "<tr>\n";
	calHTML = calHTML + "<td class='dclose' height=\"10\"colspan=\"3\"><img src='"+ imgPath + "spacer.gif' alt=\"\"height=\"10\"border=\"0\"/></td>";
	calHTML = calHTML + "</tr>\n";
	calHTML = calHTML + "<tr>\n";

	tempmonthyear = new Array();
	tempmonthyear = monthAddSimple(today, 23);
	var tempmonth = tempmonthyear[0];
	var tempyear = tempmonthyear[1];

	//For Disabling Year prior to current year.
	if (allowPastDate == 'y') {
		endMonth	=	am-1;
		endYear	= ay+2;		
	} else {
		endMonth 	= todayMonth; /*CURRENT MONTH -1 */
		endYear		= todayYear+2; /*CURRENT YEAR -1 */
	}
	//For Disabling Future year.
	if (allowFutureDate == 'n') {
		endMonth 	= todayMonth; /*CURRENT MONTH  */
		endYear		= todayYear; /*CURRENT YEAR  */
	}
	
	if ((gThisYear == todayYear)  && (gThisMonth == todayMonth) && (allowPastDate == 'n'))
	{
		calHTML = calHTML + "<td width=\"20\"align=\"left\" class=\"calHeader dclose\"><img hspace=\"3\" class=\"dclose\"src='"+ imgPath + "btn_page_prev_inactive.gif' border=\"0\"alt=\"Inactive Previous\"></td>\n";
	} 
	else
	{
		calHTML = calHTML + "<td width=\"20\"align=\"left\" class=\"calHeader dclose\"><a href=\"javascript:lastMonth();\"title=\""+ showLastMonthText + "\"><img hspace=\"3\" id='imgPrevious' src='"+ imgPath + "btn_page_prev_active.gif' alt=\"Active Previous\" border=\"0\"></a></td>\n"
	}

	    calHTML = calHTML + "<td align=\"center\" class=\"calHeader txtModuleTitlecal dclose\">"+ monthAsString(mo)  + " "+ yr + "</td>\n";

	if ((gThisYear == endYear)  && (gThisMonth == endMonth) && (allowFutureDate == 'n'))
	{
		calHTML = calHTML + "<td width=\"20\"align=\"right\" class=\"calHeader dclose\"><img hspace=\"3\" class=\"dclose\"src='"+ imgPath + "btn_page_next_inactive.gif' alt=\"Inactive Next\" border=\"0\"></td>\n";
	}
	else if ((gThisYear >= endYear)  && (gThisMonth >= endMonth) && (allowFutureDate == 'y'))
	{
		calHTML = calHTML + "<td width=\"20\"align=\"right\" class=\"calHeader dclose\"><img hspace=\"3\" class=\"dclose\"src='"+ imgPath + "btn_page_next_inactive.gif' alt=\"Inactive Next\" border=\"0\"></td>\n";
	}
	else
	{
		calHTML = calHTML + "<td width=\"20\"align=\"right\" class=\"calHeader dclose\"><a href=\"javascript:nextMonth();\"title=\""+ showNextMonthText + "\"><img hspace=\"3\" id='imgNext' src='"+ imgPath + "btn_page_next_active.gif' alt=\"Active Next\" border=\"0\"></a></td>\n";
	}

	calHTML = calHTML + "</tr>\n";
	calHTML = calHTML + "</table>\n";
	calHTML = calHTML + "<table border=\"0\"cellpadding=\"0\"cellspacing=\"0\" width=\"160\"height=\"2\">\n";
	calHTML = calHTML + "<tr>\n";
	calHTML = calHTML + "<td class='dclose'><img src='"+ imgPath + "spacer.gif' alt=\"\"height=\"2\"border=\"0\"/></td>";
	calHTML = calHTML + "</tr>\n";
	calHTML = calHTML + "</table>\n";
	calHTML = calHTML + "</td>\n";
	calHTML = calHTML + "</tr>\n";
	calHTML = calHTML + "<tr>\n";
	calHTML = calHTML + "<td width=\"15\"><img src='"+ imgPath + "spacer.gif' alt=\"\"height=\"3\" width=\"10\"border=\"0\"/></td>\n";
	calHTML = calHTML + "<td class='dclose' align=\"left\" valign=\"middle\">";
	calHTML = calHTML + "<table border=\"0\"cellpadding=\"0\"cellspacing=\"0\" width=\"156\"><tr>\n";
	calHTML = calHTML + "<td class='dclose' height=\"4\"colspan=\"7\"><img src='"+ imgPath + "spacer.gif' alt=\"\"height=\"4\"border=\"0\"/></td>";
	calHTML = calHTML + "</tr>\n";
	calHTML = calHTML + "<tr>\n";

	for (var i = 0;i < 7;i++)
	{
		calHTML = calHTML + "<td class='dclose' id='W"+ i + "'"+ "align=\"center\" valign=\"middle\"><b class='dclose'>"+ daysArray[i]  + "</b></td>";
	}

	calHTML = calHTML + "\n</tr>\n";
	calHTML = calHTML + "<tr>\n";
	calHTML = calHTML + "<td class='dclose' height=\"4\"colspan=\"7\"><img src='"+ imgPath + "spacer.gif' alt=\"\"height=\"4\"border=\"0\"/></td>";
	calHTML = calHTML + "</tr>\n";

}

function gentableFooter()
{   
    switch(gSiteLanguage)
	{
		case "EN":
					closeText="Close";
					break;
		case "DA":
					closeText="Luk";
					break;
		case "FI":
					closeText="Sulje";
					break;
		case "NO":
					closeText="Lukk";
					break;
		case "SV":
					closeText="St&auml;ng";
					break;
		default:
					closeText="Close";
					break;
	}
	calHTML = calHTML + "<tr>\n";
	calHTML = calHTML + "<td class='dclose' align=\"right\" valign=\"middle\"colspan=\"7\">";
	calHTML = calHTML + "<a href=\"javascript:closeCal('y');\"><img class='closeCalendar' hspace='2' src='"+ imgPath + "btn_cal_close_window.gif' border=\"0\"alt=\"Close popup calendar\">  </a>";

	calHTML = calHTML + "<a href=\"javascript:closeCal('y');\">"+closeText+"</a>";
	calHTML = calHTML + "</td>\n";
	calHTML = calHTML + "</tr>\n";
	calHTML = calHTML + "<tr>\n";
	calHTML = calHTML + "<td class='dclose' height=\"3\"colspan=\"7\"><img src='"+ imgPath + "spacer.gif' alt=\"\"height=\"3\"border=\"0\"/></td>";
	calHTML = calHTML + "</tr>\n";
	calHTML = calHTML + "</table></td>\n";
	calHTML = calHTML + "</tr>\n";
}

function gentableCell(num, todayFlag)
{
	
	preA	= "";
	postA	= "";

	if ((num == todayDay)  && (gThisMonth == todayMonth)  && (gThisYear == todayYear))
	{
		var aclass = " classToday";
		tdclass	= "txtBody tdToday ";
	}
	else
	{
		aclass = "";
		tdclass	= "txtBody ";
	}

	var newDate = new Date();
	var expectedDate = new Date();

	newDate.setYear(gThisYear);
	newDate.setMonth(gThisMonth);
	newDate.setDate(num);

	expectedDate.setYear(endYear);
	expectedDate.setMonth(endMonth);
	if (allowFutureDate == 'n') {
		expectedDate.setDate(todayDay+1);
	} else {
		expectedDate.setDate(todayDay);
	}

	var newdate = newDate.getTime();
	var expecteddate = expectedDate.getTime();

	if (!num)
	{
		num	= " ";
		beginA	= "";
		endA	= "";
	}
	else
	{
		beginA	= "<a id=\"aday"+ num + "\" href=\"javascript:void setSelectedDate("+ num + "); closeCal('y');\" class=\""+ aclass + "\">";
		endA	= "</a>";
	}

	if ((num < todayDay)  && (gThisMonth == todayMonth)  && (gThisYear == todayYear) && (allowPastDate == 'n'))
	{
		calHTML = calHTML + "<td align=\"center\" valign=\"middle\" width=\"20\" class=\""+ tdclass + "dclose\" id=\"day_"+ num + "\">"+ num + "</td>";
	}
	else if (newdate >= expecteddate)
	{
		if ((num >= hiliteStartNum)  && (num <= hiliteEndNum) )
		{
			calHTML = calHTML + "<td  bgcolor=\""+ hiliteColor + "\" align=\"center\" valign=\"middle\" width=\"20\" class=\""+ tdclass + "dclose \" id=\"day_"+ num + "\">"+ num + "</td>";
		}
		else
		{
			calHTML = calHTML + "<td align=\"center\" valign=\"middle\" width=\"20\" class=\""+ tdclass + "dclose \" id=\"day_"+ num + "\">"+ num + "</td>";
		}
	}
	else if ((num >= hiliteStartNum)  && (num <= hiliteEndNum)  && (num != " ") )
	{
		calHTML = calHTML + "<td  bgcolor=\""+ hiliteColor + "\" align=\"center\" valign=\" middle\" width=\"20\" class=\""+ tdclass + "dclose \" id=\"day_"+ num + "\">"+ beginA + preA + num + postA + endA + "</td>";
	}
	else if (num == " ")
	{
		calHTML = calHTML + "<td align=\"center\" valign=\"middle\" width=\"20\" class=\""+ tdclass + "dclose \">"+ beginA + preA + ""+ postA + endA + "</td>";
	}
	else
	{
		calHTML = calHTML + "<td align=\"center\" valign=\"middle\" width=\"20\" class=\""+ tdclass + "dclose \" id=\"day_"+ num + "\">"+ beginA + preA + num + postA + endA + "</td>";
	}
}

function lastMonth()
{
	decrementMonthYear(activeCal);
	getHighlightMonthYear();
	doCalendar(gLastMonth, gLastYear);
}

function nextMonth()
{
	incrementMonthYear(activeCal);
	getHighlightMonthYear();
	doCalendar(gNextMonth, gNextYear);
}

function setSelectedDate(theDay)
{
	var stheDay = theDay;

	if (stheDay < 10)
	{
		stheDay = "0"+ theDay;
	}
	var sThisMonth = parseInt(gThisMonth + 1);
	if (sThisMonth < 10)
	{
		sThisMonth = "0"+ sThisMonth;
	}

	dateString	= sThisMonth  + "/"+ stheDay + "/"+ gThisYear;
	dateStringField	= stheDay + "/"+ sThisMonth  + "/"+ gThisYear;

	// Set the color of the selected date to some contrasting color.
	strEval	= "$fn(\"day_"+ theDay + "\").style.fontWeight ='bold'";
	eval(strEval);

	//	Change the color if the other selected square back to white.

	if (tmpDay != "")
	{
		strEval	= "$fn(\"day_"+ tmpDay + "\").style.fontWeight ='normal'";
	}

	tmpDay	= theDay;
	setHighlightDates(theDay, sThisMonth, gThisYear, activeCal);
	setFields(dateString, dateStringField);
}

function setFields(dateString, dateStringField)
{
	var fieldname = eval("date"+ activeCal);
	$fn(fieldname).value = dateStringField;

	if (activeCal == 1)
	{
		validateArrivalDate();
	}
	else if (activeCal == 2)
	{
		validateDepartureDate();
	}
	else if (activeCal == 3)
	{
		validateArrivalDate1();
	}
	else if (activeCal == 4)
	{
		validateDepartureDate1();
	}
}

function DaysArray(n) 
{   
    var d = 0;
    for (var i = 1; i <= n; i++) 
    {
        d = 31;
        if (i==4 || i==6 || i==9 || i==11) {
            d = 30;
        }
        if (i==2) {
            d = 29;
        }
    } 
    return d;
}

// 	Function set the  values for ArrivalDate and DepartureDate variables(day, month and year)
// 	IF ArrivalDate or DepartureDate  values are empty set the variables(day, month and year)  based on system date 
function preOpenCal(arrivalDate, departureDate, calState) {	

    gCalendarFlag = calState;
    if (arrivalDate ==""|| departureDate =="") {
        var todayDateArray = sysDate.split(dtCh);
        var today = new Date(todayDateArray[2],todayDateArray[1],todayDateArray[0]);

        ad = today.getDay();
        am = today.getMonth();
        ay = today.getYear();
        var todayDateString = ad+"/"+am+"/"+ay ;
		
        var tomorrow = addDays(todayDateString,1);
        var tomorrowDateArray = tomorrow.split("/");
        dd = parseInt(tomorrowDateArray[0],10);
        dm = parseInt(tomorrowDateArray[1],10);
        dy = parseInt(tomorrowDateArray[2],10);             
    } else {
        var arrivalDateArray = arrivalDate.split("/");
        ad = parseInt(arrivalDateArray[0],10);
        am = parseInt(arrivalDateArray[1],10);
        ay = parseInt(arrivalDateArray[2],10);

        var departureDateArray = departureDate.split("/");
        dd = parseInt(departureDateArray[0],10);
        dm = parseInt(departureDateArray[1],10);
        dy = parseInt(departureDateArray[2],10);            
    }	
    openCal(calState);      
} 

function openCal(fieldNum)
{
	setCurDate(fieldNum);
	getHighlightDates(fieldNum);
	var locArray	= new Array();
	var currentImg	= "calendar"+ fieldNum;
	var lastImg	= "calendar"+ activeCal;

	if (fieldNum == '1')
	{
		calendar1 = at;
	}
	else if (fieldNum == '2')
	{
		calendar2 = dt;
	}

	if (activeCal != fieldNum)
	{
		if (activeCal != 0)
		{
			closeCal();
		}

		if (fieldNum == '1')
		{
			locArray = getCalFieldCoords(at);
		}
		else if (fieldNum == '2')
		{
			locArray = getCalFieldCoords(dt);
		}

		setObjectCoords("hiddenCalendar", locArray[0]  + tmpLeft, locArray[1]  + tmpTop);

		i = 0;
		var m1 = 0;
		var y1 = 0;

		if (fieldNum == '1')
		{
			m1 = curMonth1;
			y1 = curYear1;
		}
		else if (fieldNum == '2')
		{
			m1 = curMonth2;
			y1 = curYear2;
		}
		else if (fieldNum == '3')
		{
			m1 = curMonth3;
			y1 = curYear3;
		}
		else if (fieldNum == '4')
		{
			m1 = curMonth4;
			y1 = curYear4;
		}

		if (i == 0)
		{
			m1 = parseInt(m1 - 1);

			if (m1 == -1)
			{
				m1 = 12;
				y1 = parseInt(y1 - 1);
			}

			i = 1;
		}

		doCalendar(m1, y1);
		if (ie6)
		{
			if ((fieldNum == '1') || (fieldNum == '2'))
			{
				if($fn(NoOfRooms) != null){
				hideDropDowns(NoOfRooms);
				}			
				hideDropDowns();			
			}
			if ((fieldNum == '3')||(fieldNum == '4'))
			{
				hideDropDowns(ddlHotel);
			}		
		}
		$fn("hiddenCalendar").style.visibility	= "visible";
		$fn("hiddenCalendar").style.display	= "block";

		

		activeCal	= fieldNum;

	}
	else
	{
		activeCal	= 0;
		closeCal();
	}
}

function closeCal(obj)
{
	if (obj == 'y' || posEventObj == -1)
	{
		if($fn("hiddenCalendar") != null) {
			$fn("hiddenCalendar").style.visibility	= "hidden";
			$fn("hiddenCalendar").style.display	= "none";
			
			if( (activeCal == 1) || (activeCal == 2) ) {	
				activeCal = 0
				showDropDowns();
			} else if ((activeCal == 3) || (activeCal == 4)) {	
				activeCal = 0
				showDropDowns(ddlHotel);
			}
		}
	}
}

function closeCalOnClick(event)
{
	if(typeof (autosuggest) != "undefined")
	{
	    if ($fn(autosuggest) != null) {		
		closeAutoSuggest();			
	    }
	}
	if(typeof (LoginControl) != "undefined")
	{
		if ($fn(LoginControl) != null) 
		{	
			showLoginPopup(event);		
		}
	}

	var eveSource	= "noDiv";
	var eveType	= 0;
	var eveName	= "noName";
	var eveObjClass = "";
	var flag	= 0;

	if (browserName == "Microsoft Internet Explorer")
	{
		eveSource = window.event.srcElement.id;
		eveObjClass = window.event.srcElement.className;
	}
	else
	{
		eveSource	= event.target.id;
		eveName = event.target.nodeName;
		eveType = event.target.nodeType;
		eveObjClass = event.target.className;
	}

	var posDay = -1;
	var posNext = -1;
	var posPrevious = -1;
	var posDayEmpty = -1;

	if (eveSource != "undefined")
	{
		posDay = eveSource.indexOf("aday");
		posNext = eveSource.indexOf("Next");
		posPrevious = eveSource.indexOf("Previous");
		posDayEmpty = eveSource.indexOf("day_");
		posEventObj = eveObjClass.indexOf("dclose");
	}

	if (posDay != -1 || posNext != -1 || posPrevious != -1 || posDayEmpty != -1)
	{
		return true;
	}
	else if (eveName == "#text"&& eveType == 3)
	{
		return true;
	}
	else
	{
		if (gCalendarFlag == 0)
		{
			closeCal();
		}

		if (eveObjClass != "inputTextSmall"&& flag == 0)
		{
			gCalendarFlag = 0;
		}

		if (posEventObj != -1)
		{
			closeCal();
		}

		gCalendarFlag = 0;
	}

	if (ie6)
	{
		if ((eveSource == ArrivalDate) || (eveSource == DepartureDate))
		{
			if($fn(NoOfRooms) != null){
			hideDropDowns(NoOfRooms);
			}			
			hideDropDowns();			
		}
		if ((eveSource == ArrivalDate1)||(eveSource == DepartureDate1))
		{
			hideDropDowns(ddlHotel);
		}		
	}
}

function showLoginPopup(event)
{
	if($fn(LoginControl).style.visibility == "hidden" ) 
	{         
		var eveSrc, parentDiv;                 
		if (event)
		{
			eveSrc  = event.target.id;                        
			parentDiv = event.target.offsetParent.id;   
			
			if( eveSrc != _endsWith('LoginLink') ) 
			{
				if(parentDiv != 'Login_Box') 
				{                    
					showLoginControl();
				}
			}           
		}
		//For IE
		if (window.event)
		{
			eveSrc   = window.event.srcElement.id;               
			parentDiv = window.event.srcElement.offsetParent.id;
			
			if ( eveSrc == _endsWith('txtLoyaltyUsername') || eveSrc == _endsWith('txtLoyaltyPassword') || eveSrc == 'spanLogIn') 
			{                                                       
				return true;
			} 
			else if ( eveSrc != _endsWith('LoginLink') ) 
			{                                            
				if(parentDiv != 'Login_Box') 
				{                    
					showLoginControl();
				}
			}                                   
		}                       
	}
}

function openCalendar(arrivalDate1, departureDate1, calState)
{

	gCalendarFlag = calState;
	
	if (arrivalDate1 == ""|| departureDate1 == "")
	{
		var todayDateArray = sysDate.split(dtCh);
		var today = new Date(todayDateArray[2], todayDateArray[1], todayDateArray[0] );

		ad = today.getDay();
		am = today.getMonth();
		ay = today.getYear();

		var todayDateString = ad + "/"+ am + "/"+ ay ;
		var tomorrow = addDays(todayDateString, 1);

		var tomorrowDateArray = tomorrow.split("/");
		dd = parseInt(tomorrowDateArray[0], 10);
		dm = parseInt(tomorrowDateArray[1], 10);
		dy = parseInt(tomorrowDateArray[2], 10);
	}
	else
	{
		var arrivalDateArray = arrivalDate1.split("/");
		ad = parseInt(arrivalDateArray[0], 10);
		am = parseInt(arrivalDateArray[1], 10);
		ay = parseInt(arrivalDateArray[2], 10);

		var departureDateArray = departureDate1.split("/");
		dd = parseInt(departureDateArray[0], 10);
		dm = parseInt(departureDateArray[1], 10);
		dy = parseInt(departureDateArray[2], 10);
	}

	fieldNum = calState;
	setCurDate(fieldNum);
	getHighlightDates(fieldNum);
	var locArray	= new Array();
	var currentImg	= "calendar"+ fieldNum;
	var lastImg	= "calendar"+ activeCal;

	if (fieldNum == '3')
	{
		calendar3 = at1;
	}
	else if (fieldNum == '4')
	{
		calendar4 = dt1;
	}

	if (activeCal != fieldNum)
	{
		if (activeCal != 0)
		{
			closeCal();
		}

		if (fieldNum == '3')
		{
			locArray = getCalFieldCoords(at1);
		}
		else if (fieldNum == '4')
		{
			locArray = getCalFieldCoords(dt1);
		}

		setObjectCoords("hiddenCalendar", locArray[0]  + tmpLeft, locArray[1]  + tmpTop);

		i = 0;
		var m1 = 0;
		var y1 = 0;

		if (fieldNum == '3')
		{
			m1 = curMonth3;
			y1 = curYear3;
		}
		else if (fieldNum == '4')
		{
			m1 = curMonth4;
			y1 = curYear4;
		}

		if (i == 0)
		{
			m1 = parseInt(m1 - 1);

			if (m1 == -1)
			{
				m1 = 12;
				y1 = parseInt(y1 - 1);
			}

			i = 1;
		}

		doCalendar(m1, y1);
		if (ie6) hideDropDowns(ddlHotel);

		$fn("hiddenCalendar").style.visibility	= "visible";
		$fn("hiddenCalendar").style.display	= "block";
		activeCal	= fieldNum;
	}
	else
	{
		activeCal	= 0;
		closeCal();
	}
}


function closeCalendar(obj)
{
	if (obj == 'y' || posEventObj == -1)
	{
		activeCal = 0;
		$fn("hiddenCalendar").style.visibility	= "hidden";
		$fn("hiddenCalendar").style.display	= "none";
		showDropDowns(ddlHotel);
	}	
}


function closeCalendarOnClick(event)
{
	var eveSource	= "noDiv";
	var eveType	= 0;
	var eveName	= "noName";
	var eveObjClass = "";
	var flag	= 0;

	if (browserName == "Microsoft Internet Explorer")
	{
		eveSource = window.event.srcElement.id;
		eveObjClass = window.event.srcElement.className;
	}
	else
	{
		eveSource	= event.target.id;
		eveName = event.target.nodeName;
		eveType = event.target.nodeType;
		eveObjClass = event.target.className;
	}

	var posDay = -1;
	var posNext = -1;
	var posPrevious = -1;
	var posDayEmpty = -1;

	if (eveSource != "undefined")
	{
		posDay = eveSource.indexOf("aday");
		posNext = eveSource.indexOf("Next");
		posPrevious = eveSource.indexOf("Previous");
		posDayEmpty = eveSource.indexOf("day_");
		posEventObj = eveObjClass.indexOf("dclose");
	}

	if (posDay != -1 || posNext != -1 || posPrevious != -1 || posDayEmpty != -1)
	{
		return true;
	}
	else if (eveName == "#text"&& eveType == 3)
	{
		return true;
	}
	else
	{
		if (gCalendarFlag == 0)
		{
			closeCal();
		}

		if (eveObjClass != "inputTextSmall"&& flag == 0)
		{
			gCalendarFlag = 0;
		}

		if (posEventObj != -1)
		{
			closeCal();
		}

		gCalendarFlag = 0;
	}
	if (ie6)
	{
		if ((eveSource == ArrivalDate1)  || (eveSource == DepartureDate1))
		{
			$fn(ddlHotel).style.visibility = "visible";
		}
	}
}

function getHighlightDates(calState)
{
	if (dy == ay)
	{
		if (dm == am)
		{
			if (dd >= ad)
			{
				hiliteStartNum = ad;
				hiliteEndNum = dd;
			}
			else
			{
				hiliteStartNum = 0;
				hiliteEndNum = 0;
			}
		}
		else if (dm > am)
		{
			if (calState == 2)
			{
				hiliteStartNum = 1;
				hiliteEndNum = dd;
			}
			else
			{
				hiliteStartNum = ad;
				hiliteEndNum = 31;
			}
		}
		else if (dm < am)
		{
			hiliteStartNum = 0;
			hiliteEndNum = 0;
		}
	}
	else if (dy > ay)
	{
		if (calState == 2)
		{
			hiliteStartNum = 1;
			hiliteEndNum = dd;
		}
		else
		{
			hiliteStartNum = ad;
			hiliteEndNum = 31;
		}
	}
	else
	{
		hiliteStartNum = 0;
		hiliteEndNum = 0;
	}
}

function setHighlightDates(day, month, year, calState)
{
	if (calState == '1')
	{
		hiliteStartNum = day;
		ad = day;
		am = month;
		ay = year;
		curMonth1 = month;
		curYear1 = year;
	}
	else if (calState == '2')
	{
		hiliteEndNum = day;
		dd = day;
		dm = month;
		dy = year;
		curMonth2 = month;
		curYear2 = year;
	}
	else if (calState == '3')
	{
		hiliteStartNum = day;
		ad = day;
		am = month;
		ay = year;
		curMonth3 = month;
		curYear3 = year;
	}
	else if (calState == '4')
	{
		hiliteEndNum = day;
		dd = day;
		dm = month;
		dy = year;
		curMonth4 = month;
		curYear4 = year;
	}
}

function getHighlightMonthYear()
{
	var cm = "";
	var cy = "";

	if (activeCal != 0)
	{
		cm = "curMonth"+ activeCal;
		cy = "curYear"+ activeCal;
	}

	if (dy == ay && ay == eval(cy) )
	{
		if (am > eval(cm)  && eval(cm)  < dm)
		{
			hiliteStartNum = 0;
			hiliteEndNum = 0;
		}
		else if (am == eval(cm)  && eval(cm)  < dm)
		{
			hiliteStartNum = ad;
			hiliteEndNum = 31;
		}
		else if (am == eval(cm)  && eval(cm)  == dm)
		{
			hiliteStartNum = ad;
			hiliteEndNum = dd;
		}
		else if (am < eval(cm)  && eval(cm)  > dm)
		{
			hiliteStartNum = 0;
			hiliteEndNum = 0;
		}
		else if (am > eval(cm)  && eval(cm)  < dm)
		{
			hiliteStartNum = 1;
			hiliteEndNum = 31;
		}
		else if (am < eval(cm)  && eval(cm)  == dm)
		{
			hiliteStartNum = 1;
			hiliteEndNum = dd;
		}
		else if (am < eval(cm)  && eval(cm)  < dm)
		{
			hiliteStartNum = 1;
			hiliteEndNum = 31;
		}
		else
		{}

	}
	else if (dy > ay && eval(cy)  == dy)
	{
		if (eval(cm)  < dm)
		{
			hiliteStartNum = 1;
			hiliteEndNum = 31;
		}
		else if (eval(cm)  == dm)
		{
			hiliteStartNum = 1;
			hiliteEndNum = dd;
		}
		else
		{
			hiliteStartNum = 0;
			hiliteEndNum = 0;
		}
	}
	else if (dy > ay && ay == eval(cy) )
	{
		if (am > eval(cm) )
		{
			hiliteStartNum = 0;
			hiliteEndNum = 0;
		}
		else if (am < eval(cm) )
		{
			hiliteStartNum = 1;
			hiliteEndNum = 31;
		}
		else if (am == eval(cm) )
		{
			hiliteStartNum = ad;
			hiliteEndNum = 31;
		}
	}
	else
	{
		hiliteStartNum = 0;
		hiliteEndNum = 0;
	}
}

function setCurDate(calState)
{
	if (calState == 1)
	{
		curMonth1 = am;
		curYear1 = ay;
	}
	else if (calState == 2)
	{
		curMonth2 = dm;
		curYear2 = dy;
	}

	else if (calState == 3)
	{
		curMonth3 = am;
		curYear3 = ay;
	}
	else if (calState == 4)
	{
		curMonth4 = dm;
		curYear4 = dy;
	}
}

function incrementMonthYear(calState)
{
	if (calState == 1)
	{
		if (curMonth1 == 12)
		{
			curMonth1 = 0;
			curYear1 = parseInt(curYear1 + 1);
		}

		curMonth1 = parseInt(curMonth1 + 1);
	}
	else if (calState == 2)
	{
		if (curMonth2 == 12)
		{
			curMonth2 = 0;
			curYear2 = parseInt(curYear2 + 1);
		}

		curMonth2 = parseInt(curMonth2 + 1);
	}
	else if (calState == 3)
	{
		if (curMonth3 == 12)
		{
			curMonth3 = 0;
			curYear3 = parseInt(curYear3 + 1);
		}

		curMonth1 = parseInt(curMonth3 + 1);
	}
	else if (calState == 4)
	{
		if (curMonth4 == 12)
		{
			curMonth4 = 0;
			curYear4 = parseInt(curYear4 + 1);
		}

		curMonth4 = parseInt(curMonth4 + 1);
	}
}

function decrementMonthYear(calState)
{
	if (calState == 1)
	{
		curMonth1 = curMonth1 - 1;

		if (curMonth1 == 0)
		{
			curMonth1 = 12;
			curYear1 = parseInt(curYear1 - 1);
		}
	}
	else if (calState == 2)
	{
		curMonth2 = curMonth2 - 1;

		if (curMonth2 == 0)
		{
			curMonth2 = 12;
			curYear2 = parseInt(curYear2 - 1);
		}
	}
	else if (calState == 3)
	{
		curMonth1 = curMonth1 - 1;

		if (curMonth3 == 0)
		{
			curMonth3 = 12;
			curYear3 = parseInt(curYear3 - 1);
		}
	}
	else if (calState == 4)
	{
		curMonth4 = curMonth4 - 1;

		if (curMonth4 == 0)
		{
			curMonth4 = 12;
			curYear4 = parseInt(curYear4 - 1);
		}
	}
}

function addDays(myDate,days) 
{
    var dateArray = myDate.split("/");
    var newDate = new Date(dateArray[2],dateArray[1]-1,dateArray[0]);
    var changedDate = new Date(dateArray[2],dateArray[1]-1,dateArray[0]);
    changedDate.setDate(newDate.getDate() + days);
    var month=parseInt(changedDate.getMonth(),10)+1;
    var year = parseInt(changedDate.getYear(),10);
    if(navigator.appName == "Netscape")
    {
        year = year+1900;       
    }

    var sday = changedDate.getDate();
    if (sday < 10 ){
        sday = "0" + sday;
    }
	if (month < 10 ){
        month = "0" + month;
    }
    var addedDate = sday+"/"+month+"/"+year;  
    return addedDate;  
}

function subtractDays(myDate,days) 
{ 
    var dateArray = myDate.split("/");
    var newDate = new Date(dateArray[2],dateArray[1]-1,dateArray[0]);
    var changedDate = new Date(dateArray[2],dateArray[1]-1,dateArray[0]);
	changedDate.setDate(newDate.getDate() - days);
    var month=parseInt(changedDate.getMonth(),10)+1;
    var year = parseInt(changedDate.getYear(),10);
    if(navigator.appName == "Netscape")
    {
        year = year+1900;
    }
    var sday = changedDate.getDate();
    if (sday < 10 ){
        sday = "0" + sday;
    }
	if (month < 10 ){
        month = "0" + month;
    }
    var subtractedDate = sday+"/"+month+"/"+year;  
    return subtractedDate;
}

/*** START OF PAGE SPECIFIC CALENDAR  ****/
/* CONSTANTS */
//  relative path to the directory containing calendar-related images. leave the trailing slash off.
var jsPathToUtils= "";
var jsPathToCal= "";
var today = new Date();
var ad = today.getDate();
var am = today.getMonth()+1;
var ay = today.getYear();
if(navigator.appName == "Netscape"){ ay = ay + 1900;}
var todayDateString = ad+"/"+am+"/"+ay ;
var tomorrow = addDays(todayDateString,1);
var tomorrowDateArray = tomorrow.split("/");
var dd = parseInt(tomorrowDateArray[0]);
var dm = parseInt(tomorrowDateArray[1]);
var dy = parseInt(tomorrowDateArray[2]);
var endMonth = am-1; /*CURRENT MONTH -1 */
var endYear = ay+2; /*CURRENT YEAR -1 */
var startDay = ad;
var startMonth = am - 1;
var startYear = ay;

/*** END OF PAGE SPECIFIC CALENDAR  ****/

/* Stores the complete list of start day of the weeks between 
the date range with respective week numbers. */
var masterCalendarWeekList;

// Object to store each start day of week and associated week no.
function CalendarWeek(startDateOfWeek, weekNo) 
{
    var dt = new Date(startDateOfWeek);
    
	this.startDateOfWeek = dt;
	this.weekNo = weekNo;	
}

/* This updated the array masterCalendarWeekList with 
each start day of the week with week no. */
function populateCalendarWeeks(startDateOfWeek, weekNo)
{
    if (!masterCalendarWeekList)
        masterCalendarWeekList = new Array();

    // Creates an instance of the CalendarWeek object and add to the master list array.
    var calendarWeek = new CalendarWeek(startDateOfWeek, weekNo);
    masterCalendarWeekList.push(calendarWeek);
}

/* Returns the week number of the date being passed as parameter. */
function getWeekNumber(date, dRow, drawMonth, drawYear)
{
    var weekObj         = null;
    var weekNo          = 0;
    var dateToCompare   = null;
    var january         = 0;
    var firstWeek       = 0;

    // If the Month is January and week number of the first 
    // week to be fetched, then reinstantiate the date with 1st of january.
    if(drawMonth == january && dRow == firstWeek)
    {
        date = new Date(drawYear, january, 1);
    }
    
    // If "masterCalendarWeekList" is already populated with the week numbers for the dates.
    if(masterCalendarWeekList != null)
    {
        // For each week in the entire master list "masterCalendarWeekList".
        for(var weekCtr = 0; weekCtr < masterCalendarWeekList.length; weekCtr++)
        {
            weekObj = masterCalendarWeekList[weekCtr];
            
            if(weekObj != null)
            {
                dateToCompare = weekObj.startDateOfWeek;
                
                // When the date matches, return the associated week number.
                if(date.getFullYear() == dateToCompare.getFullYear() &&
                date.getMonth() == dateToCompare.getMonth() && 
                date.getDate() == dateToCompare.getDate())
                {                    
                    weekNo = weekObj.weekNo;
                    break;
                }
            }
        }
    }
    
    // Return the week no.
    return weekNo;
}