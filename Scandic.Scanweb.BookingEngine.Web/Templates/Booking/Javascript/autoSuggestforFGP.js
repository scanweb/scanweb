// This javascript helps you to give suggested values below to the text box
var idCounterFGP = 0;
var ajax_list_activeItemFGP;
var ajax_list_FirstItemFGP = false;
var ulListFGP = false;
var iframeTagFGP = '';

function AutoSuggestforFGP(elem, autoSuggestDiv, destId, includeBookableHotels) {
    var me = this;
    this.elem = elem;
    this.eligible = new Array();
    this.inputText = null;
    this.highlighted = -1;
    this.div = $fn(autoSuggestDiv);
    var MAXHEIGHT = 160;
    var ENTER = 13;
    var ESC = 27;
    var UP_ARROW = 38;
    var DOWN_ARROW = 40;
    var TAB = 9;
    var incl = includeBookableHotels;

    if (!elem.id) {
        var id = "autosuggest" + idCounterFGP;
        idCounterFGP++;
        elem.id = id;
    }

    elem.onkeydown = function(ev) {
        var key = me.getKeyCode(ev);
        switch (key) {
            case ENTER:
                me.useSuggestion();
                ajax_options_rollOverActiveItem(ajax_list_FirstItemFGP, true);
                me.cancelEvent(ev);
				me.initiateMeetingSearch();
                break;
            case ESC:
                me.hideDiv();
                showDropDowns();
                break;
            case TAB:
                me.hideDiv();
                break;
            case UP_ARROW:
                if (me.highlighted > 0) {
                    me.highlighted--;
                }
                if (!ajax_list_activeItemFGP) return;
                if (ajax_list_activeItemFGP && !ajax_list_activeItemFGP.previousSibling) return;
                ajax_options_rollOverActiveItem(ajax_list_activeItemFGP.previousSibling, true);
                me.changeHighlight();
                me.cancelEvent(ev);
                break;
            case DOWN_ARROW:
                var lis = me.div.getElementsByTagName('A');
                if (me.highlighted < (lis.length - 1)) {
                    me.highlighted++;
                }
                if (!ajax_list_activeItemFGP) {
                    ajax_options_rollOverActiveItem(ajax_list_FirstItemFGP, true);
                } else {
                    if (!ajax_list_activeItemFGP.nextSibling) {
                        me.changeHighlight();
                        return;
                    }
                    ajax_options_rollOverActiveItem(ajax_list_activeItemFGP.nextSibling, true);
                }
                me.changeHighlight();
                me.cancelEvent(ev);
                break;
        }
    };

    elem.onkeyup = function(ev) {
        var key = me.getKeyCode(ev);
        switch (key) {
            case ENTER:			
			validateCountryDestination();
			me.hideDiv();
            case ESC:
            case UP_ARROW:
            case DOWN_ARROW:
                return;
            default:
                if (this.value.length != 0) {
                    $('.BFlyOut').hide();
                    me.inputText = this.value;
                    me.getEligible();
                    me.createDiv();
                    me.positionDiv();
                    me.showDiv();
                    ulListFGP = $fn(autoSuggestDiv).firstChild;
                    /*fix for artf880761*/
                    if (ulListFGP.innerHTML == "") {
                        document.getElementById('autosuggestFGP').style.display = "none";
                        ulListFGP.style.display = "none";
                    }
                    /*if(iframeTagFGP==''){
                    iframeTagFGP =  $fn(autoSuggestDiv).lastChild;
                    }*/
                    //set the autosuggest box height to enable scrolling
                    if (ulListFGP.offsetHeight <= MAXHEIGHT && ulListFGP.offsetHeight >= 15) {
                        ulListFGP.style.height = ulListFGP.offsetHeight + "px";
                        //iframeTagFGP.style.height  = ulListFGP.offsetHeight+"px";
                    }
                    else if (ulListFGP.offsetHeight > MAXHEIGHT) {
                        ulListFGP.style.height = MAXHEIGHT + 15 + "px";
                        //iframeTagFGP.style.height  = MAXHEIGHT+15+"px";
                    }
                    //Cover the select boxes using ifrmae for less than IE 7 browser
                    if (ie6) {
                        iframeTagFGP.style.height = ulListFGP.offsetHeight + "px";
                        iframeTagFGP.style.display = "block";
                    }
                }
                else {
                    //artf1152998 | Fly out functionality is not working properly in the home page |Rajneesh
                    //Commented the below line as it is displaying the fly out when the last character entered is removed
                    //$('.BFlyOut').show();
                    me.hideDiv();
                    showDropDowns();
                }
        }
    };
    function removeError() {		
		var HotelVal = $("input[id$='selectedDestIdFGP']").val();
		if (HotelVal.length != 0) { 
			$("input[id$='txtdestName']").removeClass('error');			
			$(".errorP").hide();			
		}
	}
    this.useSuggestion = function() {
        if (this.highlighted > -1) {
            var lis = me.div.getElementsByTagName('A');
            for (i = 0; i < lis.length; i++) {
                if (me.highlighted == i) {
                    this.elem.value = lis[i].innerHTML;
                    $fn(_endsWith(destId)).value = lis[i].id;
                    removeError();
                    break;
                }
            }
            this.hideDiv();
            setTimeout("$fn('" + this.elem.id + "').focus()", 0);
            showDropDowns();
        }
    };
	
	//Only For MeetingSearch button
    this.initiateMeetingSearch = function() {
            $("#FindHotelVenue").find("[id$='btnSearch']").trigger("click");
    };

    this.showDiv = function() {
        this.div.style.display = 'block';
        //IF not windows safari 3.0.4 version, change the marginTop
        if (!saf30) {
            //this.div.style.marginTop = '37px';
        }
        ajax_options_rollOverActiveItem(ajax_list_FirstItemFGP, true);
        ajax_list_FirstItemFGP.className = ''; //remove highlight on autosuggest load;
    };

    this.hideDiv = function() {
        this.div.style.display = 'none';
        this.highlighted = -1;
    };

    this.changeHighlight = function() {
        var lis = this.div.getElementsByTagName('LI');
        for (i = 0; i < lis.length; i++) {
            var li = lis[i];
            if (this.highlighted == i) {
                li.className = "selected";
            }
            else {
                li.className = "";
            }
        }
    };

    this.positionDiv = function() {
        var el = this.elem;
        var x = 0;
        var y = el.offsetHeight;
        while (el.offsetParent && el.tagName.toUpperCase() != 'BODY') {
            x += el.offsetLeft;
            y += el.offsetTop;
            el = el.offsetParent;
        }
        x += el.offsetLeft;
        y += el.offsetTop;
    };

    this.createDiv = function() {
        var countryCode;
        var destinationID;
        var destinationName;
        var arrayDestinations;
        var ul = document.createElement('ul');
        ajax_list_FirstItemFGP = false;

        //loop thro destinations
        if (this.eligible) {
            for (i = 0; i < this.eligible.length; i++) {
                if (i != "swap") {
                    //countryCode = this.eligible[i][0].split("#$:~")[0]
                    destinationID = this.eligible[i][0].split("#$:~")[0];
                    destinationName = this.eligible[i][0].split("#$:~")[1];
                    //add city name


                    var li = document.createElement('li');

                    //Release2.0|artf1148157|Shameem: make the font bold for city.
                    li.innerHTML = '<strong><a id="Country:' + destinationID + '" href="#" onclick="javascript: return false;" style="margin-left:5px;">' + destinationName + '</a><strong>';
                    //Release2.0|BugID:484851|Rajneesh
                    //li.style.borderBottom = "3px solid #F2F2F2";
                    li.style.borderBottom = "1px solid #cccccc"; //Release2.0|artf1148157|Shameem:make the line thin.
                    li.style.backgroundColor = "#F1EDE9"; //Release2.0|artf1148157|Shameem:make the line thin.

                    if (!ajax_list_FirstItemFGP) ajax_list_FirstItemFGP = li;
                    ul.appendChild(li);
                    //add hotel names
                    if (this.eligible[i][1]) {
                        for (j = 0; j < this.eligible[i][1].length; j++) {
                            destinationID = this.eligible[i][1][j][0].split("#$:~")[0];
                            destinationName = this.eligible[i][1][j][0].split("#$:~")[1];
                            var citylist = document.createElement('li');
                            citylist.innerHTML = '<strong><a id="City:' + destinationID + '" href="#" style="margin-left:15px;">' + destinationName + '</a></strong>';
                            //citylist.style.borderBottom = "1px solid #cccccc";
                            ul.appendChild(citylist);
                            if (this.eligible[i][1][j][1]) {
                                for (k = 0; k < this.eligible[i][1][j][1].length; k++) {
                                    destinationID = this.eligible[i][1][j][1][k].split("#$:~")[0];
                                    destinationName = this.eligible[i][1][j][1][k].split("#$:~")[1];
                                    var hotelList = document.createElement('li');
                                    hotelList.innerHTML = '<a id="HOTEL:' + destinationID + '" href="#" style="margin-left:15px;">' + destinationName + '</a>';
                                    ul.appendChild(hotelList);
                                }
                            }
                        }
                    }

                }
            }
        }
        this.div.replaceChild(ul, this.div.childNodes[0]);
        ul.onmouseover = function(ev) {
            var target = me.getEventSource(ev);
            while (target.parentNode && target.tagName.toUpperCase() != 'LI') {
                target = target.parentNode;
            }
            var lis = me.div.getElementsByTagName('LI');
            for (i = 0; i < lis.length; i++) {
                var li = lis[i];
                if (li == target) {
                    me.highlighted = i;
                    break;
                }
            }
            me.changeHighlight();
        };

        ul.onclick = function(ev) {
            me.useSuggestion();
            me.hideDiv();
            showDropDowns();
            me.cancelEvent(ev);
            return false;
        };
        this.div.className = "suggestion_list";
    };

    this.getEligible = function() {
        //Below is used for HTML purpose
        //this.eligible = findOption(this.elem.value);
        //use below for .NET code
        $fn(_endsWith(destId)).value = "";
        this.eligible = FindDestinationByCountryName(this.elem.value, incl);
    };

    this.getKeyCode = function(ev) {
        if (ev) {
            return ev.keyCode;
        }
        if (window.event) {
            return window.event.keyCode;
        }
    };

    this.getEventSource = function(ev) {
        if (ev) {
            return ev.target;
        }
        if (window.event) {
            return window.event.srcElement;
        }
    };

    this.cancelEvent = function(ev) {
        if (!ev) var ev = window.event;
        if (ev.preventDefault) ev.preventDefault();
        if (ev.stopPropagation) ev.stopPropagation();
        ev.returnValue = false;
        ev.cancelBubble = true;
    };
}

function ajax_options_rollOverActiveItem(item, fromKeyBoard) {
    if (ajax_list_activeItemFGP) ajax_list_activeItemFGP.className = '';
    item.className = 'selected';
    ajax_list_activeItemFGP = item;
    if (fromKeyBoard) {
        if (ajax_list_activeItemFGP.offsetTop > ulListFGP.offsetHeight) {
            ulListFGP.scrollTop = eval(ajax_list_activeItemFGP.offsetTop - ulListFGP.offsetHeight + ajax_list_activeItemFGP.offsetHeight + 5);
            // for safari version less than 3 -> Add Extra 10 pixel for Horizantal scrollbar height
            if (saf && !isSafari3) {
                ulListFGP.scrollTop = ulListFGP.scrollTop + 10;
            }
        }
        if (ajax_list_activeItemFGP.offsetTop < ulListFGP.scrollTop) {
            ulListFGP.scrollTop = 0;
        }
    }
}
// Below code is used for HTML purpose


$(function() {
    var m226 = $("div[id$='MeetingSearch226']");
	    if (m226.length){ 	        
		    $(m226).append("<div class='ft sprite'></div>");	
		}	
});
