﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Validator Entity
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function Validator(pageName, errDivId)
{
    this.pageName           = pageName;
    this.errDivId           = errDivId;
    this.validations        = new Array();
    this.errorMessages      = new Array();
}

Validator.prototype.addValidation = function(validation)
{
    this.validations.push(validation);
}

Validator.prototype.addFailedMsg = function(eachValidation)
{
    var failedMessage   = null;
    
    this.errorMessages.push(eachValidation);
}

Validator.prototype.isErrorAlreadyDisplayed = function(controlId, endIndex)
{
    var ctr = 0;
    var found = false;
    
    for (ctr = 0; ctr <= endIndex; ctr++)
    {
        if (this.errorMessages[ctr].controlId == controlId)
        {
            found = true;
            break;
        }
    }
    
    return found;
}

Validator.prototype.validateAll = function()
{    
    var eachValidation  = null;
    var ctr             = 0;
    
    for (ctr = 0; ctr < this.validations.length; ctr++)
    {
        eachValidation = this.validations[ctr];
	    this.validateEach(eachValidation);
	}
	
	this.displayValidationErrors();
}

Validator.prototype.displayValidationErrors = function()
{
    var errDiv              = $fn(_endsWith(this.errDivId));
    var errDisplayStart     = "";
    var errDisplayEnd       = "";
    var errorStrings        = "";
    var ctr                 = 0;
    var errorMsgCncl = "";
    
	if (this.errorMessages.length == 0) 
	{
	    errDiv.style.visibility     = isHidden;
	    errDiv.style.display        = "none";
	} 
	else 
	{
		errDiv.style.visibility     = isVisible;
		errDiv.style.display        = "block";
		
		for (ctr = 0; ctr < this.errorMessages.length; ctr++) 
		{
		    if (!this.isErrorAlreadyDisplayed(this.errorMessages[ctr].controlId, ctr - 1))
		    {
		        if (this.errorMessages[ctr].labelId != null)
		        {
		            var labels = this.errorMessages[ctr].labelId.split(";");
    		        
		            for (var labelCtr = 0; labelCtr < labels.length; labelCtr++)
		            {
	                    setLabelCss(labels[labelCtr], this.errorMessages[ctr].cssErrClass);
	                }
	            }
	            var newErrorMessage = this.errorMessages[ctr].validationMsg;
	            //Removes the duplicate error message if any.Release 1.5 | artf809605 | FGP - add more than one partner preference 
	            if ((newErrorMessage != null) && (errorStrings.indexOf(newErrorMessage)<0))
	            {
	                //Res 2.0-Parvathi:artf1150983: Error message on cancel a reservation
	                errorMsgCncl = this.errorMessages[ctr].validationMsg;
	                errorStrings = errorStrings + "<li>" + this.errorMessages[ctr].validationMsg + "</li> \n";			
	            }
		    }
		}
		//artf1150983: Error message on cancel a reservation	
		if (errorStrings != "" && this.errDivId != "divSelectRatePageErrorMessage") 
		{      			
			errDisplayStart     = errDisplayStart + "<!-- BEGIN: Conditional Error Div with Horizontal-line -->";
			errDisplayStart     = errDisplayStart + "<div id='errorReport' class='formGroupError'>";
			errDisplayStart     = errDisplayStart + "<!-- Error Message -->";
			errDisplayStart     = errDisplayStart + "<div class='redAlertIcon'>";
			errDisplayStart     = errDisplayStart + $fn(errMsgTitle).value;
			errDisplayStart     = errDisplayStart + "</div>";
			errDisplayStart     = errDisplayStart + "<ul class='circleList'>";
			
			errDisplayEnd       = "</ul><!-- Error Message --></div>";
			
			errDiv.innerHTML    = errDisplayStart + errorStrings + errDisplayEnd;
   		}
   		//artf1150983: Error message on cancel a reservation	
   		if (errorStrings != "" && this.errDivId == "divSelectRatePageErrorMessage")
   		{
   		    var cnclAlert = $fn(_endsWith("cnclAltert"));
   		    
   		     if ($.browser.msie)
             {
                cnclAlert.innerText = errorMsgCncl;//Res 2.0-Parvathi:artf1150983: Error message on cancel a reservation
   		     }
   		     else
   		     {
   		        cnclAlert.innerHTML = errorStrings;//Res 2.0-Parvathi:artf1150983: Error message on cancel a reservation
   		     }
   		    
   		    var divSltRatePg = $fn(_endsWith("divSelectRatePageErrorMessage"));
   		    divSltRatePg.style.display = "block";
   		    
            var cnclDiv =  $fn(_endsWith("cancelError"));
            //Res 2.0 Parvathi : artf1163929: Cancellation of booking
            if(cnclDiv != null)
            {
                cnclDiv.style.display = "none";
            }
   		}
	}
}

Validator.prototype.validateEach = function(eachValidation)
{   
    var controlToValidate = $fn(_endsWith(eachValidation.controlId))

	if (eachValidation.isCustomValidation)
	{	
	    if (!eval(eachValidation.validationFunction))
        {
            this.addFailedMsg(eachValidation);
        }
        else
        {
            setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
        }
    }
    else
    {
        switch(eachValidation.rule)
        {
            case RULE_ALPHA:
                if (!validateAlpha($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
                case DEFAULT_TEXT:
                // Release R2.0 - Bhavya - Defect fix - 466742 - if the field has default text[last name], indicates user has not entered last name.
               var getsurNameText=$fn(_endsWith(eachValidation.controlId)).value;
               var lastNameTransText=$fn(_endsWith("hdnlastNameTranslated")).value;               
               if(getsurNameText==lastNameTransText)
                {
                    this.addFailedMsg(eachValidation);
                } 
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }            
                break;
            case RULE_ALPHANUMERIC:
                if (!validateAlphanumeric($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_ALPHANUMERIC_NORDIC:
                if (!validateAlphanumericNordic($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_NAME:
                if (!validateName($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_ALPHANUMERIC_NORDIC_WITH_SPECIAL_CHARACTER:
                if (!validateAlphanumericNordicWithSpecialCharacters($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_NUMERIC:
                if (!validateNumeric($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_NUMERIC_IATA:
                var iataValue  = formateBookingCode($fn(_endsWith(eachValidation.controlId)).value);
                if (!validateNumericIata(iataValue))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_NUMERIC_STAR:
                if (!validateNumericStar($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_NUMERIC_SPACE:
                if (!validateNumericWithSpace($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_EMAIL:
                if (!validateEmail($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_NONEMPTY:
                if (!validateNonEmpty($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_NOTANUMBER:
                if (!validateNotANumber($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_DATE:
                if (!validateDate($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
                
            case RULE_STARTS_WITH_VO:
                if (!validateStartsWithVO($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_NOT_STARTS_WITH_VO:
                if (!validateNotStartsWithVO($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_DROPDOWN_SELECTED:
                if (!validateDropdownSelected($fn(_endsWith(eachValidation.controlId)).selectedIndex))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_DROPDOWN_WITH_SEPARATOR:
                if (!validateDropdownWithSeparator($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_CHECKBOX_SELECTED:
                if (!$fn(_endsWith(eachValidation.controlId)).checked)
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_RADIO_CHECKED:
                if (!validateRadioChecked(eachValidation.controlId))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_MEMBERSHIP_NUM:
                // Defect Fix - Artifact artf632664 : UAT | Frequent guest booking 
                if (!validateMembershipNo($fn(_endsWith(eachValidation.controlId)).value))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_DNUMBER: 
                //R1.4.1 | artf799566 | D-Number Enhancement
                var dNumberValue  = formateBookingCode($fn(_endsWith(eachValidation.controlId)).value);
                if (!validateDNumber(dNumberValue))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
                
            case RULE_BLOCK_CODE: 
                //Priya Singh: Implemented to validate block code;
                //Format is always B + 3 alphabetical characters + 6 numerical numbers i.e:(BXXXDDMMYY)
                //Res 2.2.7 - AMS Patch4 - Artifact artf1217694 : Scanweb - Input control missing for incorrect block code 
                var bookingCodeValue  = formateBookingCode($fn(_endsWith(eachValidation.controlId)).value);
                if (!validateBlockcode(bookingCodeValue))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
                
            case RULE_BOOKING_CONFIRMATION_NUMBER:
                if (!validateBookingConfirmationNumber($fn(_endsWith(eachValidation.controlId))))
                {
                    this.addFailedMsg(eachValidation);
                }
                else
                {
                    setLabelCss(eachValidation.labelId, CONST_EMPTY_STRING);
                }
                break;
            case RULE_FAILED_VALIDATION:
                this.addFailedMsg(eachValidation);
                break;
        }
    }
}

function setLabelCss(labelId, cssClass)
{
    if(labelId != null)
    {
        var labelControl = $fn(_endsWith(labelId));
    		    
        if (labelControl == null)		        
            labelControl = $fn(labelId);
        
        if (labelControl != null)
        labelControl["className"] = cssClass;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BasicValidation Entity
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function Validation(controlId, controlType, labelId, cssErrClass, rule, validationMsg, isCustomValidation, validationFunction)
{
	this.controlId	        = controlId;
	this.controlType        = controlType;
	this.labelId            = labelId;
	this.cssErrClass        = cssErrClass;
	this.rule               = rule;
	this.validationMsg      = validationMsg;
	this.isCustomValidation = isCustomValidation;
	this.validationFunction = validationFunction;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants to be used across the Application
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var CONST_DATE_SEP                      = "/";
var CONST_EMPTY_STRING                  = "";
var CONST_CUSTOM_TRUE                   = true;
var CONST_CUSTOM_FALSE                  = false;
var CONST_VO                            = "VO";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants to be used for the Control Types
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var CONTROL_TYPE_TEXTAREA               = "TextArea";
var CONTROL_TYPE_TEXTFIELD              = "TextField";
var CONTROL_TYPE_PASSWORD               = "Password";
var CONTROL_TYPE_CHECKBOX               = "CheckBox";
var CONTROL_TYPE_DROPDOWN               = "Dropdown";
var CONTROL_TYPE_RADIOOPTION            = "RadioOption";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants to be used for the Rules
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Basic Validation constants
var RULE_ALPHA                      = "validateAlpha";
var DEFAULT_TEXT                    ="validateDefaultText";
var RULE_ALPHANUMERIC               = "validateAlphanumeric";
var RULE_ALPHANUMERIC_NORDIC        = "validateAlphanumericNordic";
var RULE_NAME                       = "validateName";
var RULE_NUMERIC                    = "validateNumeric";
var RULE_NUMERIC_IATA               = "validateNumericIata";
var RULE_NUMERIC_SPACE              = "validateNumericWithSpace";        
var RULE_NUMERIC_STAR               = "validateNumericStar";
var RULE_POSITIVE                   = "validatePositive";
var RULE_NON_ZERO                   = "validateNonZero";
var RULE_EMAIL                      = "validateEmail";
var RULE_NONEMPTY                   = "validateNonEmpty";
var RULE_NOTANUMBER                 = "validateNotANumber";
var RULE_DATE                       = "validateDate";
var RULE_PAST_DATE                  = "validatePastDate";
var RULE_STARTS_WITH_VO             = "validateStartsWithVO";
var RULE_NOT_STARTS_WITH_VO         = "validateNotStartsWithVO";
var RULE_DROPDOWN_SELECTED          = "validateDropdownSelected";
var RULE_DROPDOWN_WITH_SEPARATOR    = "validateDropdownWithSeparator";
var RULE_CHECKBOX_SELECTED          = "validateCheckBoxSelected";
var RULE_RADIO_CHECKED              = "validateRadioChecked";
var RULE_ALPHANUMERIC_NORDIC_WITH_SPECIAL_CHARACTER  = "validateAlphanumericNordicWithSpecialCharacter";
// Defect Fix - Artifact artf632664 : UAT | Frequent guest booking
var RULE_MEMBERSHIP_NUM             = "validateMembershipNo";
var RULE_BOOKING_CONFIRMATION_NUMBER = "validateBookingConfirmationNumber";
var RULE_FAILED_VALIDATION          = "failedValidation";

// Custom validation constants
var RULE_DESTINATION                = "validateDestinationCustom";
var RULE_DUPLICATE_DESTINATION      = "validateDestinationDuplicate";
var RULE_CREDITCARD_EXPIRY          = "validateCreditCardExpiry";
var RULE_TELEPHONE                  = "validateTelephone";
var RULE_PARTNER_PROGRAM            = "validatePartnerProgram";
var RULE_PARTNER_PROGRAM_ACCT_NO    = "validatePartnerProgramAcctNo";
var RULE_PASSWORDMATCH              = "validatePasswordMatch";
var RULE_PASSWORDNODOLLARSIGN       = "validatePasswordForDollarSign";
var RULE_SECURITY_ANSWER            = "validateSecurityAnswer";
var RULE_DATE_OF_BIRTH              = "validateDateOfBirth";
var RULE_NO_OF_NIGHTS_1_TO_99       = "validateNoOfNights1To99";
var RULE_DNUMBER                    = "validateDNumber";
var RULE_PASSWORDNONORDICCHAR       = "validateNordicCharacters";
var RULE_BLOCK_CODE                 = "validateBlockcode";


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constants to be used for the Pages
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var PAGE_ACCOUNT_INFO                   = "AccountInformation";
var PAGE_BOOKING_CONFIRMATION           = "BookingConfirmation";
var PAGE_BOOKING_DETAIL                 = "BookingDetail";
var PAGE_BOOKING_MODULE_BIG             = "BookingModuleBig";
var PAGE_BOOKING_MODULE_MEDIUM          = "BookingModuleMedium";
var PAGE_BOOKING_MODULE_SMALL           = "BookingModuleSmall";
var PAGE_BOOKING_SEARCH                 = "BookingSearch";
var PAGE_CANCEL_BOOKING                 = "CancelBooking";
var PAGE_CANCEL_BOOKING_CONFIRMATION    = "CancelBookingConfirmation";
var PAGE_CONTACT_US                     = "ContactUs";
var PAGE_CURRENT_BOOKINGS               = "CurrentBookings";
var PAGE_EMAIL_CONFIRMATION             = "EmailConfirmation";
var PAGE_ENROLL_GUEST                   = "EnrollGuest";
var PAGE_FORGOTTEN_PASSWORD             = "ForgottenPassword";
var PAGE_INVITE_A_FRIEND                = "InviteAFriend";
var PAGE_LOGIN_ERROR                    = "LoginError";
var LOGIN_CONFLOGOUT_MODULE             = "LoginConfLogOut";
var PAGE_LOGIN_STATUS                   = "LoginStatus";
var PAGE_LOGIN_BOOKING_DETAIL           = "BookingDetailLogin";
var PAGE_LOGOUT_CONFIRMATION            = "LogoutConfirmation";
var PAGE_LOSTCARD_LOGIN                 = "LostcardLogin";
var PAGE_LOSTCARD_NOLOGIN               = "LostcardNoLogin";
var PAGE_MISSING_POINTS                 = "MissingPoints";
var PAGE_UPDATE_PROFILE                 = "UpdateProfile";
var PAGE_CUSTOMER_SERVICE_CONTACT_US    = "CustomerServiceContactUs";
var PAGE_CAMPAIGN_LANDING               = "CampaignLanding";
var PAGE_CHILDREN_DETAIL                = "ChildrensDetailsPage"; // For Kids Concept(1.6)
var PAGE_MODIFY_CHILDREN_DETAIL         = "ModifyChildrensDetailsPage";
var PAGE_FIND_A_HOTEL                   = "FindAHotelPage";
var PAGE_SHOPPING_CART                  = "ShoppingCart";
var PAGE_MODIFY_SHOPPING_CART           = "ModifyShoppingCart";


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Basic validations 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function validateAlpha(value)
{
    var regExp      = /[^A-Za-z\s]/;    
    var charpos     = value.search(regExp); 
    var returnVal   = true; 
    
    if(value.length > 0 &&  charpos >= 0) 
    {        
        returnVal = false; 
    }
                    
    return returnVal;
}


function validateAlphanumeric(value)
{
    var regExp      = /[^A-Za-z0-9\s]/;    
    var charpos     = value.search(regExp); 
    var returnVal   = true; 
    
    if(value.length > 0 &&  charpos >= 0) 
    {        
        returnVal = false; 
    }
                    
    return returnVal;
}

function validateAlphanumericNordic(value)
{
    var regExp      = /[^A-Za-z¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿ0-9\s]/;
    var charpos     = value.search(regExp); 
    var returnVal   = true; 
    
    if(value.length > 0 &&  charpos >= 0) 
    {        
        returnVal = false; 
    }
                    
    return returnVal;
}

function validateName(value)
{
    var regExp      = /[^A-Za-z¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿ0-9-\s]/;
    var charpos     = value.search(regExp); 
    var returnVal   = true; 
    
    if(value.length > 0 &&  charpos >= 0) 
    {        
        returnVal = false; 
    }
                    
    return returnVal;
}

function validateAlphanumericNordicWithSpecialCharacters(value)
{
    var regExp      = /[^A-Za-z¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿ0-9#,:./\-\s]/;
    var charpos     = value.search(regExp); 
    var returnVal   = true; 
    
    if(value.length > 0 &&  charpos >= 0) 
    {        
        returnVal = false; 
    }
                    
    return returnVal;
}
//Validate for nordic characters Artifact artf1072335 : Scanweb - workaround for double byte chars in password and answer fields 
function validateNordicCharacters(value)
{  
    var regExp      = /[^A-Za-z¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿ0-9#*%@\s]/;
    var charpos     = value.search(regExp); 
    var returnVal   = true; 
    
    if(value.length > 0 &&  charpos >= 0) 
    {        
        returnVal = false; 
    }
                    
    return returnVal;
}

function validateNumericWithSpace(value)
{
    var regExp      = /[^0-9\s]/;
    var charpos     = value.search(regExp);
    var returnVal   = true; 
    
    if(value.length > 0 &&  charpos >= 0) 
    {        
        returnVal = false; 
    }
                    
    return returnVal;
}

function validateNumeric(value)
{
    var regExp      = /[^0-9]/;
    var charpos     = value.search(regExp);
    var returnVal   = true; 
    
    if(value.length > 0 &&  charpos >= 0) 
    {        
        returnVal = false; 
    }
                    
    return returnVal;
}

function validateNumericIata(value)
{
   // var regExp      = /[^0-9]\d{6}$/;
   //var regExp = /^(?= *-?[0-9]*$).{5}/;
   var regExp = /^\d{8}$/;
    var charpos     = value.match(regExp);
    var returnVal   = true; 
    
    if(charpos == null) 
    {        
        returnVal = false; 
    }
                    
    return returnVal;
}

//This function is used for credit card validations
function validateNumericStar(value)
{
    var regExp      = /[^0-9*]/;
    var charpos     = value.search(regExp);
    var returnVal   = true; 
    
    if(value.length > 0 &&  charpos >= 0) 
    {        
        returnVal = false; 
    }
                    
    return returnVal;
}

function validatePositive(value)
{
    if (validateNumeric(value))
    {
        if (value > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }   
}

function validateNonZero(value)
{
    if (value == "0")
    {
        return false;
    }
    
    return true;
}

function validateEmail(value)
{
    //var regExp = /^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$/;   
    var regExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
    
    value = Trim(value);
    
    if (value != "")
    {
        returnval = regExp.test(value);
            
        return returnval;
    }
    else
    {
        return true;
    }
}

function validateNonEmpty(value)
{
    var regExp = /^(\s*)$/;
    
    return !regExp.test(value);
}

function validateNotANumber(value)
{
    return isNaN(value);
}

function validateDate(value)
{
    var pos1        = value.indexOf(CONST_DATE_SEP);
    var pos2        = value.indexOf(CONST_DATE_SEP, pos1+1);
    var strDay      = value.substring(0, pos1);
    strDay          = strDay.substring(strDay.length-2,strDay.length);
    var strMonth    = value.substring(pos1+1, pos2);
    var strYear     = value.substring(pos2+1);
    
    value=strMonth + strDay + strYear;
    strYr=strYear;
    
    if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1);
    if (strMonth.charAt(0) == "0" && strMonth.length > 1) strMonth = strMonth.substring(1);
    
    for (var i = 1; i <= 3; i++) 
    {
        if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1);
    }
    
    month   = parseInt(strMonth, 10);
    day     = parseInt(strDay, 10);
    year    = parseInt(strYr, 10);
    
    if (pos1 == -1 || pos2 == -1)
    {
        return false;
    }
    if (strMonth.length < 1 || month < 1 || month > 12)
    {   
        return false;
    }
    if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > DaysArray(month))
    {   
        return false;
    }
    if (strYear.length != 4 || year == 0 )
    {   
        return false;
    }
    if (value.indexOf(dtCh, pos2+1) != -1 || validateNumeric(stripCharsInBag(value, CONST_DATE_SEP)) == false)
    {   
        return false;
    } 
    else
    {
        return true;
    }
}

function validatePastDate(value, isArrival)
{
    var dateArray           = value.split(CONST_DATE_SEP);
    var todayDateArray      = sysDate.split(CONST_DATE_SEP);
    var enteredDt           = new Date(dateArray[2], dateArray[1] - 1, dateArray[0]);
    var todayDt             = new Date(todayDateArray[2], todayDateArray[1] - 1, todayDateArray[0]);
    var twoYearLaterDt      = DateAdd("y", 2, todayDt);

    if (isArrival == true)
    {
        twoYearLaterDt      = DateDiff("d", 1, twoYearLaterDt);
    }
 
    var divider             = 3600*24*1000;
    
    var enteredDate         = enteredDt.getTime() / divider;
    var todayDate           = todayDt.getTime() / divider;
    var twoYearLaterDate    = twoYearLaterDt.getTime() / divider;

    
    var futureDate          = (enteredDate <= twoYearLaterDate);
    var pastDate            = (enteredDate >= todayDate);
    
    if ((futureDate) && (pastDate)) 
    {
        return false;
    }
    else 
    {
        return true;
    }
}

function validateStartsWithVO(value)
{
    return startsWith(value, CONST_VO);
}

function validateNotStartsWithVO(value)
{
    return !startsWith(value, CONST_VO);
}

function validateDropdownSelected(indexPos)
{
    if(indexPos <= 0)
        return false;
    else
        return true;
}

function validateDropdownWithSeparator(value)
{
    if(value == "DFT" || value == "SEPR")
    {
        return false;
    }
    else
    {
        return true;
    }
}
function validateRadioChecked(value)
{
    var allRadio = document.getElementsByName(value);
    var checkedFlag = 0;
    for(var cnt=0;cnt<allRadio.length;cnt++)
    {
        if(allRadio[cnt].checked)
        {
            checkedFlag = 1;
            break;
        }
    }
    if((checkedFlag == 0) && (allRadio.length != 0))
    {
        return false;
    }
    else
    {
        return true;
    }
}
function validateDropdownDefault(value)
{
    if(value == "DFT")
    {
        return true;
    }
    else
    {
        return false;
    }
}

// Defect Fix - Artifact artf632664 : UAT | Frequent guest booking 
function validateMembershipNo(value)
{
    var newValue = Trim(value);
    return validateNumeric(newValue);
}

function validateDNumber(value)
{ 
    // This regular expression checks for the value whose 
    // 1st letter is "D" or "d" and can end with any numbers.
    //
    // Following are the proper valid DNumbers
    // "D12345", "d6474", "D1111111", "d909099090990" etc.
    
    //R1.4 | Fix for artf767278: WSP||User is not taken to corporate tab when 
    //searching with L number in general tab. 
    // This regular expression will also check for the values whose 
    // 1st letter is "L" or "l" and can end with any numbers, for Travel Agent Booking.
    var regExp      = /^[Dd][0-9]+/;
    var resultValue = value.match(regExp);
    var returnVal   = false; 
    
    // If the value is Empty, we should not proceed to check
    if (value == null || Trim(value).length <= 0)
    {
        returnVal = true;
    }
    else
    {    
        if (resultValue != null)
        { 
            if (resultValue[0].length == value.length)
            {
                returnVal = true;
            }
        }
    }
    
    return returnVal;
}

//Priya Singh: Method added to validate block code;
//Format is always B + 3 alphabetical characters + 6 numerical numbers i.e:(BXXXDDMMYY)
//Res 2.2.7 - AMS Patch4 - Artifact artf1217694 : Scanweb - Input control missing for incorrect block code  
function validateBlockcode(value)
{
    // This regular expression checks for the value whose 
    // 1st letter is "B" or "b", followed by 3 alphabets and ends with 6 digit number.
    // Following could be the valid BlockCode
    // "BABC260711", "bAAA260711", "bABC260711", "bvvv260711" etc.
    
    var regExp      = /^[Bb][A-z]{3}\d{6}$/; 
    var resultValue = value.match(regExp);
    var returnVal   = false; 
    
    if (resultValue != null)
    { 
        if (resultValue[0].length == value.length)
        {
            returnVal = true;
        }
    }
    
    return returnVal;
}

function validateLNumber(value)
{ 
    // This regular expression checks for the value whose 
    // 1st letter is "L" or "l" and can end with any numbers.
    //
    // Following are the proper valid Travel Agent Code
    // "L12345", "l6474", "L1111111", "l909099090990" etc.
    //Replaced /^[Dd][0-9]+/ with /^[Ll][0-9]+/
    var regExp      = /^[Ll][0-9]+/;       
    var resultValue = value.match(regExp);
    var returnVal   = false; 
    
    // If the value is Empty, we should not proceed to check
    if (value == null || Trim(value).length <= 0)
    {
        returnVal = true;
    }
    else
    {    
        if (resultValue != null)
        { 
            if (resultValue[0].length == value.length)
            {
                returnVal = true;
            }
        }
    }
    
    return returnVal;
}

function validateBookingConfirmationNumber(ctrl)
{
    value = ctrl.value;
    var regExpFirst = /^\d{8,}$/
    var regExpSecond = /^\d{8,}-\d{1}$/
    value = Trim(value);
    if (value != "")
    {
       if(value.indexOf('-') == -1)
       {
         retValue = regExpFirst.test(value);
         return retValue;
       }   
       else 
       {
         secondValue= regExpSecond.test(value);
         if(secondValue==false)
           {
             ctrl.value=value.split('-', 1)[0];
           }
         return true;
       }
    }
}

