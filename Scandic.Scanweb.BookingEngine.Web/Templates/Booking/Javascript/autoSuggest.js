// This javascript helps you to give suggested values below to the text box
var idCounter = 0;
var ajax_list_activeItem;
var ajax_list_FirstItem = false;
var ulList = false;
var iframeTag = '';

function AutoSuggest(elem, autoSuggestDiv, destId, includeBookableHotels) {
    var me = this;
    this.elem = elem;
    this.eligible = new Array();
    this.inputText = null;
    this.highlighted = -1;
    this.div = $fn(autoSuggestDiv);
    var MAXHEIGHT = 160;
    var ENTER = 13;
    var ESC = 27;
    var UP_ARROW = 38;
    var DOWN_ARROW = 40;
    var TAB = 9;
    var incl = includeBookableHotels;

    if (!elem.id) {
        var id = "autosuggest" + idCounter;
        idCounter++;
        elem.id = id;
    }

    elem.onkeydown = function(ev) {
        var key = me.getKeyCode(ev);
        switch (key) {
            case ENTER:
                me.useSuggestion();
                ajax_options_rollOverActiveItem(ajax_list_FirstItem, true);
                me.cancelEvent(ev);
                break;
            case ESC:
                me.hideDiv();
                showDropDowns();
                break;
            case TAB:
                me.hideDiv();
                break;
            case UP_ARROW:
                if (me.highlighted > 0) {
                    me.highlighted--;
                }
                if (!ajax_list_activeItem) return;
                if (ajax_list_activeItem && !ajax_list_activeItem.previousSibling) return;
                ajax_options_rollOverActiveItem(ajax_list_activeItem.previousSibling, true);
                me.changeHighlight();
                me.cancelEvent(ev);
                break;
            case DOWN_ARROW:
                var lis = me.div.getElementsByTagName('A');
                if (me.highlighted < (lis.length - 1)) {
                    me.highlighted++;
                }
                if (!ajax_list_activeItem) {
                    ajax_options_rollOverActiveItem(ajax_list_FirstItem, true);
                } else {
                    if (!ajax_list_activeItem.nextSibling) {
                        me.changeHighlight();
                        return;
                    }
                    ajax_options_rollOverActiveItem(ajax_list_activeItem.nextSibling, true);
                }
                me.changeHighlight();
                me.cancelEvent(ev);
                break;
        }
    };

    elem.onkeyup = function(ev) {
        var key = me.getKeyCode(ev);
        switch (key) {
            case ENTER:
			validateFindYourDestination();
			me.hideDiv();		
            case ESC:
            case UP_ARROW:
            case DOWN_ARROW:
                return;
            default:
                if (this.value.length != 0) {
                    $('.BFlyOut').hide();
                    me.inputText = this.value;
                    me.getEligible();
                    me.createDiv();
                    me.positionDiv();
                    me.showDiv();
                    ulList = $fn(autoSuggestDiv).firstChild;
                    /*fix for artf880761*/
                    if (ulList.innerHTML == "") {
                        document.getElementById('autosuggest').style.display = "none";
                        ulList.style.display = "none";
                    }
                    /*if(iframeTag==''){
                    iframeTag =  $fn(autoSuggestDiv).lastChild;
                    }*/
                    //set the autosuggest box height to enable scrolling
                    if (ulList.offsetHeight <= MAXHEIGHT && ulList.offsetHeight >= 15) {
                        ulList.style.height = ulList.offsetHeight + "px";
                        //iframeTag.style.height  = ulList.offsetHeight+"px";
                    }
                    else if (ulList.offsetHeight > MAXHEIGHT) {
                        ulList.style.height = MAXHEIGHT + 15 + "px";
                        //iframeTag.style.height  = MAXHEIGHT+15+"px";
                    }
                    //Cover the select boxes using ifrmae for less than IE 7 browser
                    if (ie6) {
                        iframeTag.style.height = ulList.offsetHeight + "px";
                        iframeTag.style.display = "block";
                    }
                }
                else {
                    //artf1152998 | Fly out functionality is not working properly in the home page |Rajneesh
                    //Commented the below line as it is displaying the fly out when the last character entered is removed
                    //$('.BFlyOut').show();
                    me.hideDiv();
                    showDropDowns();
                }
        }
    };

    this.useSuggestion = function() {
        if (this.highlighted > -1) {
            var lis = me.div.getElementsByTagName('A');
            for (i = 0; i < lis.length; i++) {
                if (me.highlighted == i) {
                    this.elem.value = htmlDecode(lis[i].innerHTML);
                    $fn(_endsWith(destId)).value = lis[i].id;
					this.attachGmapEvent();
                    break;
                }
            }
            this.hideDiv();
            setTimeout("$fn('" + this.elem.id + "').focus()", 0);
            showDropDowns();
        }
    };
	this.attachGmapEvent = function(){
	if($('.gmapclickevent').is(':visible')){
	SetMaps();
	}
	};
    this.showDiv = function() {
        this.div.style.display = 'block';
        //IF not windows safari 3.0.4 version, change the marginTop
        if (!saf30) {
            //this.div.style.marginTop = '37px';
        }
        ajax_options_rollOverActiveItem(ajax_list_FirstItem, true);
        ajax_list_FirstItem.className = ''; //remove highlight on autosuggest load;
    };

    this.hideDiv = function() {
        this.div.style.display = 'none';
        this.highlighted = -1;
    };

    this.changeHighlight = function() {
        var lis = this.div.getElementsByTagName('LI');
        for (i = 0; i < lis.length; i++) {
            var li = lis[i];
            if (this.highlighted == i) {
                li.className = "selected";
            }
            else {
                li.className = "";
            }
        }
    };

    this.positionDiv = function() {
        var el = this.elem;
        var x = 0;
        var y = el.offsetHeight;
        while (el.offsetParent && el.tagName.toUpperCase() != 'BODY') {
            x += el.offsetLeft;
            y += el.offsetTop;
            el = el.offsetParent;
        }
        x += el.offsetLeft;
        y += el.offsetTop;
    };

    this.createDiv = function() {
        var destinationID;
        var destinationName;
        var arrayDestinations;
        var ul = document.createElement('ul');
        ajax_list_FirstItem = false;

        //loop thro destinations
        if (this.eligible) {
            for (i = 0; i < this.eligible.length; i++) {
                if (i != "swap") {
                    destinationID = this.eligible[i][0].split("#$:~")[0];
                    destinationName = this.eligible[i][0].split("#$:~")[1];
                    //add city name
                    var li = document.createElement('li');

                    //Release2.0|artf1148157|Shameem: make the font bold for city.
                    li.innerHTML = '<strong><a id="CITY:' + destinationID + '" href="#" onclick="javascript: return false;" >' + destinationName + '</a><strong>';
                    //Release2.0|BugID:484851|Rajneesh
                    //li.style.borderBottom = "3px solid #F2F2F2";
                    li.style.borderBottom = "1px solid #cccccc"; //Release2.0|artf1148157|Shameem:make the line thin.

                    if (!ajax_list_FirstItem) ajax_list_FirstItem = li;
                    ul.appendChild(li);
                    //add hotel names
                    if (this.eligible[i][1]) {
                        for (j = 0; j < this.eligible[i][1].length; j++) {
                            destinationID = this.eligible[i][1][j].split("#$:~")[0];
                            destinationName = this.eligible[i][1][j].split("#$:~")[1];
                            var li = document.createElement('li');
                            li.innerHTML = '<a id="HOTEL:' + destinationID + '" href="#">' + destinationName + '</a>';
                            ul.appendChild(li);
                        }
                    }
                }
            }
        }
        this.div.replaceChild(ul, this.div.childNodes[0]);
        ul.onmouseover = function(ev) {
            var target = me.getEventSource(ev);
            while (target.parentNode && target.tagName.toUpperCase() != 'LI') {
                target = target.parentNode;
            }
            var lis = me.div.getElementsByTagName('LI');
            for (i = 0; i < lis.length; i++) {
                var li = lis[i];
                if (li == target) {
                    me.highlighted = i;
                    break;
                }
            }
            me.changeHighlight();
        };

        ul.onclick = function(ev) {
            me.useSuggestion();
            me.hideDiv();
            showDropDowns();
            me.cancelEvent(ev);
            return false;
        };
        this.div.className = "suggestion_list";
    };

    this.getEligible = function() {
        //Below is used for HTML purpose
        //this.eligible = findOption(this.elem.value);
        //use below for .NET code
        $fn(_endsWith(destId)).value = "";
        this.eligible = FindDestinationByName(this.elem.value, incl);
    };

    this.getKeyCode = function(ev) {
        if (ev) {
            return ev.keyCode;
        }
        if (window.event) {
            return window.event.keyCode;
        }
    };

    this.getEventSource = function(ev) {
        if (ev) {
            return ev.target;
        }
        if (window.event) {
            return window.event.srcElement;
        }
    };

    this.cancelEvent = function(ev) {
        if (!ev) var ev = window.event;
        if (ev.preventDefault) ev.preventDefault();
        if (ev.stopPropagation) ev.stopPropagation();
        ev.returnValue = false;
        ev.cancelBubble = true;
    };
}

function htmlDecode(value) {
    return $('<div/>').html(value).text();
}

function ajax_options_rollOverActiveItem(item, fromKeyBoard) {
    if (ajax_list_activeItem) ajax_list_activeItem.className = '';
    item.className = 'selected';
    ajax_list_activeItem = item;
    if (fromKeyBoard) {
        if (ajax_list_activeItem.offsetTop > ulList.offsetHeight) {
            ulList.scrollTop = eval(ajax_list_activeItem.offsetTop - ulList.offsetHeight + ajax_list_activeItem.offsetHeight + 5);
            // for safari version less than 3 -> Add Extra 10 pixel for Horizantal scrollbar height
            if (saf && !isSafari3) {
                ulList.scrollTop = ulList.scrollTop + 10;
            }
        }
        if (ajax_list_activeItem.offsetTop < ulList.scrollTop) {
            ulList.scrollTop = 0;
        }
    }
}
// Below code is used for HTML purpose
function findOption(inputStr) {
    var destArray = [["STOCKHOLM:Stockholm", ["830:Scandic Alvik", "810:Scandic Anglais", "805:Scandic Ariadne", "815:Scandic Bromma", "811:Scandic Continental", "804:Scandic Hasselbacken", "803:Scandic Infra City", "865:Scandic J�rva Krog", "857:Scandic Kungens Kurva", "812:Scandic Malmen", "833:Scandic Norra Bantorget", "813:Scandic Park", "807:Scandic Sergel Plaza", "814:Scandic Sj�fartshotellet", "886:Scandic Star Sollentuna", "838:Scandic T�by", "875:Scandic Upplands V�sby"]]];
    return destArray;
}
//	Function to close AutoSuggest DIV when user click on the page.
function closeAutoSuggest(event) {
    var eveSource;
    if (event) {
        eveSource = event.target.id;
    }
    if (window.event) {
        eveSource = window.event.srcElement.id;
    }
    if ((eveSource != autosuggest)) {
        //This is a hack.Finds out all autosuggest divs.
        var allD = document.getElementsByTagName('div');
        if (allD && allD.length > 0) {
            for (i = 0; i < allD.length; i++) {
                var pos = allD[i].id.indexOf("autosuggest");
                if (pos == 0) {
                    allD[i].style.display = "none";
                }
            }
        }
    }
}