//ID of the selct hotel page
var hotelDetailSearch = "hotelDetailSearch";
var paginationSearch = "paginationSearch";
var pageListingContainer = "pageListingContainer";
var sortBy = "sortBy";

//ID of the select rate page


//Declaration for select rate page
var locArray = new Array();
var tempLeft = -5;
var tempTop = -6;
var loginLeft = 0;
var loginTop = 0;
var LoginControl = 'LoginControl';
var descBoxes = new Array('Desc_Column1', 'Desc_Column2', 'Desc_Column3', 'Desc_Col1', 'Desc_Col2', 'Desc_Col3', 'blkHotelDetails', 'Login_Box', 'Desc_Roomtype0',
'Desc_Roomtype1', 'Desc_Roomtype2', 'Desc_Roomtype3', 'Desc_Roomtype4', 'Desc_Roomtype5',
'Desc_Roomtype6', 'Desc_Roomtype7', 'Desc_Roomtype8', 'Desc_Roomtype9', 'Desc_Roomtype10',
'Desc_Roomtype11', 'Desc_Roomtype12', 'Desc_Roomtype13', 'Desc_Roomtype14', 'Desc_Roomtype15',
'Desc_Roomtype16', 'Desc_Roomtype17', 'Desc_Roomtype18', 'Desc_Roomtype19', 'Desc_Roomtype20', 'LogoutControl');


function hideLoginControl() {
    $fn(LoginControl).style.visibility = "hidden";
    var args = hideLoginControl.arguments;
    var aPos = $fn(args[0].id);
    var divDesc = $fn(args[1]);
    locArray = getLoginCoords(aPos);
    //setObjectCoords(divDesc, locArray[0] + loginLeft, locArray[1] + loginTop);
    setObjectCoords(divDesc, 750, -6);
    hideAllDesc();
    divDesc.style.display = "block";
}
function showLoginControl() {
    if ($fn('Login_Box') != null) {
        $fn('Login_Box').style.display = "none";
    }
    $fn(LoginControl).style.visibility = "visible";

}

function showLogoutBox() {
  /*  if ($fn('LogoutControl') != null) {
        if ($fn('LogoutControl').style.display == "none") {
            $fn('LogoutControl').style.display = "block";
        }
        else
            $fn('LogoutControl').style.display = "none";
    } */
      
   $("#LogoutControl").toggle(); 
   }

// Function to show the contents of hidden div
function expandDiv() {
    var args = expandDiv.arguments;
    var objRef = args[0];
    var divRef = args[1];
    if (objRef.parentNode.className == 'collapseLink') {
        objRef.parentNode.className = 'expandLink'
        hideAllDesc(); // hide all open description boxes
        $fn(divRef).style.display = "none";
    } else if (objRef.parentNode.className == 'expandLink') {
        objRef.parentNode.className = 'collapseLink'
        $fn(divRef).style.display = "block";
    }
}

function hideDesc() {
    var args = hideDesc.arguments;
    if (args.length != 0) {
        for (i = 0; i < args.length; i++) {
            if ($fn(args[i]) != null) {
                $fn(args[i]).style.display = "none";
            }
        }
    }
    else {
        hideAllDesc();
    }
}

function hideAllDesc() {
    for (i = 0; i < descBoxes.length; i++) {
        if ($fn(_endsWith(descBoxes[i])) != null) {
            $fn(_endsWith(descBoxes[i])).style.display = "none";
        }
    }
}

function showDesc() {
    var args = showDesc.arguments;
    var aPos = $fn(args[0].id);
    var divDesc = $fn(_endsWith(args[1]));
    locArray = getObjectCoords(aPos);
    setObjectCoords(divDesc, locArray[0] + tempLeft, locArray[1] + tempTop);
    hideAllDesc();
    divDesc.style.display = "block";
}

function showHotelDetailSearch() {
    if ($fn(_endsWith(hotelDetailSearch)))
        $fn(_endsWith(hotelDetailSearch)).style.display = "block";
    if ($fn(_endsWith(paginationSearch)))
        $fn(_endsWith(paginationSearch)).style.display = "none";
    if ($fn(pageListingContainer))
        $fn(pageListingContainer).style.visibility = "hidden";
    if ($fn(sortBy))
        $fn(sortBy).style.visibility = "hidden";
}

function showPaginationSearch() {
    if ($fn(_endsWith(paginationSearch)))
        $fn(_endsWith(paginationSearch)).style.display = "block";
    if ($fn(_endsWith(hotelDetailSearch)))
        $fn(_endsWith(hotelDetailSearch)).style.display = "none";
    if ($fn(pageListingContainer))
        $fn(pageListingContainer).style.visibility = "hidden";
    if ($fn(sortBy))
        $fn(sortBy).style.visibility = "hidden";
}

function hidePaginationSearch() {
    if (_endsWith(paginationSearch))
        $fn(_endsWith(paginationSearch)).style.display = "none";
    if (_endsWith(hotelDetailSearch))
        $fn(_endsWith(hotelDetailSearch)).style.display = "none";
    if ($fn(pageListingContainer))
        $fn(pageListingContainer).style.visibility = "visible";
    if ($fn(sortBy))
        $fn(sortBy).style.visibility = "visible";
}

function setRatesColumnHeight() {
    var columns = new Array('detColumn1', 'detColumn2', 'detColumn3', 'detColumn4');
    var detRow = getElementsByClassName('detRow', 'DIV', $fn('roomDetailContainer'));

    var elementsArray = new Array();
    //var	tempHeight          = 0;	
    var finalHeightForRow = 0;

    for (i = 0; i < detRow.length; i++) {
        for (x = 0; x < columns.length; x++) {
            var elem = getElementsByClassName(columns[x], 'DIV', detRow[i]);

            for (j = 0; j < elem.length; j++) {
                /*if (tempHeight < elem[j].offsetHeight)
                {
                tempHeight = elem[j].offsetHeight;
                }
                elem[j].style.height = tempHeight + "px";				
                */

                // Bug Fix: artf913665 : Select Rate page | Non-English locales | UE alignment problem 				
                // Looping through all columns in a row and find out the maximum height. 
                // Store the maximum height in the variable "finalHeight". Each element in each 
                // column stored in the array "elementsArray". Later we will loop through all the 
                // elements in this array and apply the style "height" with the value of "finalHeight".
                elementsArray.push(elem[j]);

                if (finalHeightForRow < elem[j].offsetHeight) {
                    finalHeightForRow = elem[j].offsetHeight;
                }
            }
        }
        //tempHeight = 0;

        // Bug Fix: artf913665 : Select Rate page | Non-English locales | UE alignment problem	    
        // Looping through all elements stored in the array "elementsArray", 
        // and apply the style "Height" for each element with the value of "finalHeight".
        for (ctr = 0; ctr < elementsArray.length; ctr++) {
            elementsArray[ctr].style.height = finalHeightForRow + "px";
        }

        // Reinitialising the finalHeight and elements in the array 
        // before the loop starts for the next row.
        finalHeightForRow = 0;
        elementsArray = new Array();
    }
}

// Function to toggle the div
function toggleDiv() {
    var args = toggleDiv.arguments;
    var objRef = args[0];
    var divRef = args[1];
    if (objRef.parentNode.className == 'collapseDiv') {
        objRef.parentNode.className = 'expandDiv'
        $fn(divRef).style.display = "none";
    } else if (objRef.parentNode.className == 'expandDiv') {
        objRef.parentNode.className = 'collapseDiv'
        $fn(divRef).style.display = "block";
    }
}
// Function to toggle the loginDiv 
//artf1081836: Scanweb - My Login section at booking details should be expanded if login fails
function toggleLogin() {
    var args = toggleLogin.arguments;
    var objRef = args[0];
    var divRef = args[1];
    if (objRef.parentNode.className == 'collapseDiv') {
        objRef.parentNode.className = 'expandDiv'
        $fn(_endsWith(divRef)).className = "loginInfo loginInfoCollapsed";
    } else if (objRef.parentNode.className == 'expandDiv') {
        objRef.parentNode.className = 'collapseDiv'
        $fn(_endsWith(divRef)).className = "loginInfo";
    }
}

//Function for Enabling creditcard section.
function EnableCreditcardinfo(roomNo) {
    for (var count = 0; count < roomNo; count++) {
        doHideShow(count);
    }
}

function EnableCreditcardinfoForCommonRoom() {

    if ($fn(_endsWith("rdoLateArrivalGuranteeCommon")) != null) {
        if (!$fn(_endsWith("rdoLateArrivalGuranteeCommon")).checked) {
            if ($fn(_endsWith("HideUnhideCreditCardDivCommon")) != null)
                $fn(_endsWith("HideUnhideCreditCardDivCommon")).style.display = "none";

            if ($fn(_endsWith("panHashCreditCardDivCommon")) != null)
                $fn(_endsWith("panHashCreditCardDivCommon")).style.display = "none";
        }
        else {
            $fn(_endsWith("HideUnhideCreditCardDivCommon")).style.display = "block";

            if ($fn(_endsWith("panHashCreditCardDivCommon")) != null)
                $fn(_endsWith("panHashCreditCardDivCommon")).style.display = "block";
        }
    }
    
    if ($("input[id$='hid6PMBooking']").val() == "1") {
        $("div[id$='HideUnhideCreditCardDivCommon']").show();
        $("input[id$='rdoLateArrivalGuranteeCommon']").hide();        
    }

}

function doHideShow(count) {

    if ($fn(_endsWith("RoomInformation" + count + "_rdoLateArrivalGurantee")) != null) {
        if (!$fn(_endsWith("RoomInformation" + count + "_rdoLateArrivalGurantee")).checked) {

            if ($fn(_endsWith("RoomInformation" + count + "_panHashCreditCardDiv")) != null)
                $fn(_endsWith("RoomInformation" + count + "_panHashCreditCardDiv")).style.display = "none";

            if ($fn(_endsWith("RoomInformation" + count + "_HideUnhideCreditCardDiv")) != null)
                $fn(_endsWith("RoomInformation" + count + "_HideUnhideCreditCardDiv")).style.display = "none";

        }
        else {

            if ($fn(_endsWith("RoomInformation" + count + "_panHashCreditCardDiv")) != null)
                $fn(_endsWith("RoomInformation" + count + "_panHashCreditCardDiv")).style.display = "block";

            if ($fn(_endsWith("RoomInformation" + count + "_HideUnhideCreditCardDiv")) != null)
                $fn(_endsWith("RoomInformation" + count + "_HideUnhideCreditCardDiv")).style.display = "block";
        }
    }
}

function DisplayCreditcardInfo(value) {
    if ($fn(_endsWith("rdoLateArrivalGurantee" + value)).checked) {
        $fn(_endsWith("HideUnhideCreditCardDiv" + value)).style.display = "block";
        $fn(_endsWith("rdoLateArrivalGurantee" + value)).checked = true;
    }
}
function DisableCreditcardInfo(value) {
    $fn(_endsWith("HideUnhideCreditCardDiv" + value)).style.display = "none";
    $fn(_endsWith("rdoHoldRoom" + value)).checked = true;
}

function UpdateCheckoutSelection() {
    s = s_gi(s_account);
    s.linkTrackVars = 'events,eVar62';
    s.linkTrackEvents = 'event31';
    s.events = 'event31';
    s.eVar62 = 'CC Checkout';
    s.tl(this, 'o', 'Checkout');
}

//Added a Site catalyst method for Add anathor Hotel
function AddPropertiesforAddAnathorHotel() {
    s_gi(s_account);
    s.linkTrackVars = 'eVar35';
    s.eVar35 = 'Add Another hotel';
    s.tl(this, 'o', 'Add Another Hotel');
}
//Added a Site catalyst method for Map tab clicked in Select hotel page
function UpdateSHMapSelection() {
    try {
        s_gi(s_account);
        s.linkTrackVars = 'eVar36';
        s.eVar36 = 'Map View';
        s.tl(this, 'o', 'Select Hotel Button');
    }
    catch (err) {
    }
}
//This method is used for List tab data when user clicks on list tab in select hotel page.
function UpdateSHListSelection() {

    try {
        s_gi(s_account);
        s.linkTrackVars = 'eVar36';
        s.eVar36 = 'List View';
        s.tl(this, 'o', 'Select Hotel Button');
    }
    catch (err) {
    }

}

function UpdateTotalHotelResultsInSearch(totalHotels) {
    try {
        s_gi(s_account);
        s.linkTrackVars = 'eVar18,prop18';
        s.prop18 = totalHotels;
        s.eVar18 = totalHotels;
        s.tl(this, 'o', 'Select Hotel Results');
    }
    catch (err) {
    }
}

function trackPromobox(prop54Value, prop44Value) {
    var sfunc = s_gi(s_account);
    sfunc.linkTrackVars = 'prop44,prop54';
    sfunc.prop44 = prop44Value;
    sfunc.prop54 = prop54Value;
    sfunc.tl(this, 'o', 'promo box');
    return true;

}

function AddSiteCatalystForLogin(val) {
    s_gi(s_account);
    s.linkTrackVars = 'eVar38,prop21,prop22';
    s.eVar38 = 'Top Right Login Button';
    s.prop21 = 'Top Right Login Button';
    //s.prop22='homepage';
    s.prop22 = val;
    s.tl(this, 'o', 'Login Button');
}
//Added this method for log in in Booking details page.
function GetSiteCatalystForBookingLogin(val) {
    s_gi(s_account);
    s.linkTrackVars = 'eVar38,prop21,prop22';
    s.eVar38 = 'Booking Details Login Button';
    s.prop21 = 'Booking Details Login Button';
    s.prop22 = val;
    s.tl(this, 'o', 'Login Button');
}
//This method will be called when user clicks on join button in any page
function AddSiteCatalystForJoin(val) {
    s_gi(s_account);
    s.linkTrackVars = 'eVar38,prop21,prop22';
    s.eVar38 = 'Top Right Sign Up Button';
    s.prop21 = 'Top Right Sign Up Button';
    s.prop22 = val;
    s.tl(this, 'o', 'Sign Up Button');
}
//This method is used for adding site catalyst when user clicks on sign up in footer
function SiteCatalystforFooterSignUP(val) {
    s_gi(s_account);
    s.linkTrackVars = 'eVar38,prop21,prop22';
    s.eVar38 = 'Footer Sign Up Button';
    s.prop21 = 'Footer Sign Up Button';
    s.prop22 = val;
    s.tl(this, 'o', 'Sign Up Button');
}

//Added a Site catalyst method for Trip Advisor Review clicked in Select Hotel page
function TrackTripAdvisor(linkurl) {
    try {
        s_gi(s_account);
        s.prop25 = 'Tripadvisor function';
        s.prop26 = linkurl;
        s.tl(this, 'o', 'Trip Advisor Reviews Link');
    }
    catch (err) {
    }
}

function ShowOrHideTitle(dropdown) {
    var selIndex = dropdown.selectedIndex;
    var selValue = dropdown.options[selIndex].value;
    if (selValue == 'GER' || selValue == "GER")
	{
        $("[id$=lblNameTitle]").show();
		$("[id$=dvNameTitle]").show();
	}
    else
	{
        $("[id$=dvNameTitle]").hide();
		$("[id$=lblNameTitle]").hide();
	}
}