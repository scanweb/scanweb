jQuery(function($) {
    var minNights = 1; 	//Minmum No.of Nights as per business
    var maxNights = 99; 	//Maximum No.of Nights as per business
    var localcalmaxDates;
    if (typeof (maxDates) == 'undefined') //maxDates is defined in Header.ascx. maxDates will not be available in iFrame.
        localcalmaxDates = parent.maxDates;
    else
        localcalmaxDates = maxDates;
    var pkgURL = 'i18n/ui.datepicker' //relative path to localised js package
    var langCode = '';

    //'serverDate' is already defined in Header.ascx which gives the date object based on server macine.
    var serDate = new Date(serverDate) || new Date();

    //Declare variable for min/max dates 
    var minArrDate = new Date(serDate);
    var maxArrDate = new Date(serDate);
    var minDepDate = new Date(serDate);
    var maxDepDate = new Date(serDate);
    maxArrDate.setDate(maxArrDate.getDate() + parseInt(localcalmaxDates - 1)); 
    //setMinMax(); //Set the min/max date values for deparuture date

    //Declare variable for min/max dates for missing points page
    //var mp_minArrDate = null;
    // Missing poins cannot be recalculated prior to 01st jan 2008. 
    // This is according to Elin, Please refer the artifact "https://resultspace2.sapient.com/sf/sfmain/do/go/artf785148"
    var mp_minArrDate = new Date(2008, 00, 01);

    // Artifact artf959555 : R1.6.1 | Missing points - calendar dates 
    // NEW REQUEST 2: Disable all dates older than 6 months back in time. -  START
    var mp_minArrDateTemp = new Date(serDate.getFullYear(), serDate.getMonth() - 6, serDate.getDate());
    if (mp_minArrDateTemp > mp_minArrDate) {
        mp_minArrDate = mp_minArrDateTemp;
    }
    // NEW REQUEST 2: Disable all dates older than 6 months back in time. -  END

    var mp_maxArrDate = new Date(serDate);
    var mp_minDepDate = new Date(serDate);
    var mp_maxDepDate = new Date(serDate);
    mp_maxArrDate.setDate(serDate.getDate() - parseInt(7)); // 1.6.1 Missing Points disabled todays date and 5 days back is disabled for arrival
    mp_maxDepDate.setDate(serDate.getDate() - parseInt(6)); // 1.6.1 Missing Points disabled todays date and 6 days back is disabled for departure
    // 1.6.1 Missing Points
    //mp_setMinMax();	//Set the min/max date values for deparuture date in missing points page


    if (Locale_Id != '' && Locale_Id != null) {
        langCode = $('#' + Locale_Id).val().toLowerCase();
        langCode = (langCode == 'en') ? '' : langCode;
    }

    $.datepicker.setDefaults({
        //beforeShowDay: nationalDays,
        //showAnim: "slideDown",		
        //showWeeks: true, 
        closeAtTop: false,
        mandatory: true,
        changeFirstDay: false,
        changeMonth: false,
        changeYear: false
        //prevText: '', 
        //nextText: '',
        //dayNamesMin: ['S','M','T','W','T','F','S'],
    });

    //Attach the datepicker control to the input element of a booking module
    DateRange_Class = (DateRange_Class == "") ? null : DateRange_Class;
    $('.' + DateRange_Class).datepicker($.extend(
		{
		    beforeShow: customRange,
		    onClose: setNights
		}, $.datepicker.regional[langCode]));

    //Method to prevent user input in date range field
    $('.' + DateRange_Class).bind("keydown", function(e) {
        //Handle Tab keys
        if (!e) var e = window.event;
        if (e.keyCode == 9) {
            return true;
        } else if (e.keyCode == 9 && e.shiftKey) {
            return true;
        } else {
            return false;
        }
    });


    // Customize two date pickers to work as a date range for booking module
    function customRange(input) {
        if (this.id == ArrDate_Id) {
            setMinMax();
            return { minDate: minArrDate, maxDate: maxArrDate };
        } else if (this.id == DepDate_Id) {
            setMinMax();
            return { minDate: (input.id == DepDate_Id ? minDepDate : null), maxDate: maxDepDate };
        }
    }

    //set the Min/Max value for Departure date.
    function setMinMax() {
        if ($('#' + ArrDate_Id).val() != "") {
            var arrDateVal = $('#' + ArrDate_Id).val();
            var arrDateArray = arrDateVal.split("/");
            var dateLen = arrDateArray[0].length;
            var noOfNightsValue = 1;
            arrDateArray[0] = arrDateArray[0].substr(dateLen - 2, dateLen);
            var actualArrDate = new Date(arrDateArray[2], arrDateArray[1] - 1, arrDateArray[0]);
            minDepDate = new Date(arrDateArray[2], arrDateArray[1] - 1, arrDateArray[0]);
            //Vrushali | Res2.0 | Defect Fix : 409172 - Room selection is not working in Bonus cheques section
            var noOfNights_Id = $fn(_endsWith("txtnoOfNights"));
            if (noOfNights_Id != null)
                noOfNightsValue = actualArrDate.getDate() + parseInt(noOfNights_Id.value);
            //Vrushali | Res2.0 | Defect Fix : 409172 - Room selection is not working in Bonus cheques section
            //RK: | Res2.0 | Defect Fix:441602 - Earlier dates for the departure date were getting disabled.
            minDepDate.setDate(actualArrDate.getDate() + 1);
            //minDepDate.setDate(noOfNightsValue);

            var tMaxDepDate = new Date(arrDateArray[2], arrDateArray[1] - 1, arrDateArray[0]);
            tMaxDepDate.setDate(actualArrDate.getDate() + parseInt(maxNights)); //Actual Arrival Date + 99						

            maxDepDate = new Date(serDate);
            maxDepDate.setDate(maxDepDate.getDate() + parseInt(localcalmaxDates)); //Maximum Arrival Date 396 days

            if (Date.parse(tMaxDepDate) < Date.parse(maxDepDate)) {
                maxDepDate = tMaxDepDate;
            }
        }
    }

    //set number of nights field
    function setNights(input) {
        setMinMax();
        var NightsVal = diffDays($('#' + DepDate_Id).val(), $('#' + ArrDate_Id).val());
        if (isPositiveInt(NightsVal)) {
            if (this.id == DepDate_Id) {
                setDepDate(NightsVal);
                $('#' + Nights_Id).val(NightsVal);
            } else if (this.id == ArrDate_Id) {
                NightsVal = $('#' + Nights_Id).val();
                setDepDate(NightsVal);
            }
        } else if (this.id == ArrDate_Id) {
            NightsVal = $('#' + Nights_Id).val();
            setDepDate(NightsVal);
        }

        return {};
    }
    //Set the Departure date value
    function setDepDate(NightsVal) {
        setMinMax();
        //var NoOfNightsVal = $('#'+Nights_Id).val();
        var arrDateVal = $('#' + ArrDate_Id).val();
        var depDateVal = $('#' + DepDate_Id).val();
        var arrDateArray = arrDateVal.split("/");
        var aDateLen = arrDateArray[0].length;
        arrDateArray[0] = arrDateArray[0].substr(aDateLen - 2, aDateLen);
        var tDate = new Date(arrDateArray[2], arrDateArray[1] - 1, arrDateArray[0]);
        tDate.setDate(tDate.getDate() + parseInt(NightsVal));

        if (isPositiveInt(NightsVal) && arrDateVal != "") {
            if (NightsVal != '0' && NightsVal != '') {
                if (isValidDepDate(tDate)) {
                    $('#' + DepDate_Id).val(formatDateAsString(tDate));


                }
                else {
                    var validNights = diffDays(formatDateAsString(maxDepDate), $('#' + ArrDate_Id).val());
                    //alert(validNights);
                    $('#' + DepDate_Id).val(addDays(arrDateVal, validNights));
                    $('#' + Nights_Id).val(validNights);
                }
            } else if (NightsVal > maxNights) {
                $('#' + DepDate_Id).val(addDays(arrDateVal, maxNights));
                $('#' + Nights_Id).val(maxNights);
            } else {
                $('#' + DepDate_Id).val(addDays(arrDateVal, minNights));
                $('#' + Nights_Id).val(minNights);
            }
        } else if (arrDateVal != "") {
            $('#' + DepDate_Id).val(addDays(arrDateVal, minNights));
            $('#' + Nights_Id).val(minNights);
        }
    }
    //Returns true if the given date object is less than or equal to Max.deparurure date
    function isValidDepDate(date) {
        if (Date.parse(date) <= Date.parse(maxDepDate)) {
            return true;
        } else {
            return false;
        }
    }

    Nights_Id = (Nights_Id == "") ? null : Nights_Id;
    if (Nights_Id != null) {
        $('#' + Nights_Id).bind("blur", function() {
            var NightsVal = $.trim($('#' + Nights_Id).val());
            setDepDate(NightsVal);
            return false;
        });
    }


    ///////////////////////////////////////////////
    ////			Missing Points Calendar Setup ///////
    ///////////////////////////////////////////////

    //Attach the datepicker control to the input element of missing points page


    mp_DateRange_Class = (mp_DateRange_Class == "") ? null : mp_DateRange_Class;
    $('.' + mp_DateRange_Class).datepicker($.extend(
		{
		    dateFormat: 'dd/mm/yy',
		    beforeShow: mp_customRange,
		    onClose: set_mpDepDate
		}, $.datepicker.regional[langCode]));

    //Method to prevent user input in date range field
    $('.' + mp_DateRange_Class).bind("keydown", function(e) {
        //Handle Tab keys
        if (!e) var e = window.event;
        if (e.keyCode == 9) {
            return true;
        } else if (e.keyCode == 9 && e.shiftKey) {
            return true;
        } else {
            return false;
        }
    });

    // Customize two date pickers to work as a date range for booking module
    function mp_customRange(input) {

        if (this.id == mp_ArrDate_Id) {
            mp_setMinMax();
            return { minDate: mp_minArrDate, maxDate: mp_maxArrDate };
        } else if (this.id == mp_DepDate_Id) {
            mp_setMinMax(); //set the Min/Max value for Departure date in Missing points page.								
            return { minDate: (input.id == mp_DepDate_Id ? mp_minDepDate : null), maxDate: mp_maxDepDate };
        }
    }

    //set the Min/Max value for Departure date in Missing points page.
    function mp_setMinMax() {
        mp_minDepDate = new Date(serDate);
        mp_maxDepDate = new Date(serDate);
        if ($('#' + mp_ArrDate_Id).val() != "") {
            var mp_arrDateVal = $('#' + mp_ArrDate_Id).val();
            var mp_arrDateArray = mp_arrDateVal.split("/");

            //R:2.0|BugId:532602||Rajneesh
            //Summary:Departure date calendar is not working properly in the missing points page
            //String Manipulation is done for "mp_arrDateArray[0]" array to convert the days value like "Tues 12" to "12".

            var mDateLen = mp_arrDateArray[0].length;
            mp_arrDateArray[0] = mp_arrDateArray[0].substr(mDateLen - 2, mDateLen);

            var mp_actualArrDate = new Date(mp_arrDateArray[2], mp_arrDateArray[1] - 1, mp_arrDateArray[0]);
            mp_minDepDate = new Date(mp_arrDateArray[2], mp_arrDateArray[1] - 1, mp_arrDateArray[0]);
            mp_minDepDate.setDate(mp_actualArrDate.getDate() + 1);

            var mp_tempMaxDepDate = new Date(mp_arrDateArray[2], mp_arrDateArray[1] - 1, mp_arrDateArray[0]);
            mp_tempMaxDepDate.setDate(mp_actualArrDate.getDate() + parseInt(maxNights)); //Actual Arrival Date + 99				

            mp_maxDepDate = new Date(serDate);

            if (Date.parse(mp_tempMaxDepDate) < Date.parse(mp_maxDepDate)) {
                mp_maxDepDate = mp_tempMaxDepDate;
            }
            else {
                mp_maxDepDate.setDate(serDate.getDate() - parseInt(6)); // 1.6.1 Missing Points
            }
        }
        else {
            mp_minDepDate.setDate(serDate.getDate() - parseInt(6)); // 1.6.1 Missing Points
            mp_maxDepDate.setDate(serDate.getDate() - parseInt(6)); // 1.6.1 Missing Points
        }
    }

    //set departure night field for missing points
    function set_mpDepDate(input) {
        mp_setMinMax();
        var mp_arrDateVal = $('#' + mp_ArrDate_Id).val();
        var mp_depDateVal = $('#' + mp_DepDate_Id).val();
        var mp_NightsVal = diffDays(mp_depDateVal, mp_arrDateVal);

        if (isPositiveInt(mp_NightsVal)) {
            if (this.id == mp_DepDate_Id) {
                $('#' + mp_DepDate_Id).val(addDays(mp_arrDateVal, mp_NightsVal));
            } else if (this.id == mp_ArrDate_Id && mp_NightsVal < maxNights) {
                $('#' + mp_DepDate_Id).val(addDays(mp_arrDateVal, mp_NightsVal));
            } else if (this.id == mp_ArrDate_Id && mp_NightsVal > maxNights) {
                $('#' + mp_DepDate_Id).val(addDays(mp_arrDateVal, maxNights));
            }
        } else if (this.id == mp_ArrDate_Id) {
            $('#' + mp_DepDate_Id).val(addDays(mp_arrDateVal, minNights));
        }

        return {};
    }
    /////////// END: Missing points page clanedar setup ////////////////////////


    /*
    //NOTE: This feature is not implemented for Release 1.4	
	
	// Highlight certain national days on the calendar
    //(Sweden: 6june | Norway: 17may | Denmark: 5june | Finland: )
    var natDays = [[1, 26, 'au'], [2, 6, 'nz'], [3, 17, 'ie'], [4, 27, 'za'], [5, 17, 'no'], [6, 6, 'se'],
    [7, 4, 'us'], [8, 17, 'id'], [9, 7, 'br'], [10, 1, 'cn'], [11, 22, 'lb'], [12, 12, 'ke']];


	//Highlight national holidays
    function nationalDays(date) {
    for (i = 0; i < natDays.length; i++) {
    if (date.getMonth() == natDays[i][0] - 1 && date.getDate() == natDays[i][1]) {
    return [false, natDays[i][2] + '_day'];
    }
    }
    return [true, ''];
    }
    */


});

///////////////////////////////////////////////////////
//// GENERAL FUNCTIONS related 	to calendar popup /////
//////////////////////////////////////////////////////


// ------------------------------------------------------------------
// Function to compute the difference between ArrivalDate & DepartureDate filed values
// values are in dd/mm/yyyy format
// Return the day diffenece/No.ofNights
// ------------------------------------------------------------------
function diffDays(depDate,arrDate) 
{
	if(depDate != "" || arrDate != ""){
		var dateArray1 = depDate.split("/");
		var dateArray2 = arrDate.split("/");
		var aDate = dateArray1[0].length;
		dateArray1[0] = dateArray1[0].substr(aDate-2,aDate);
		var newDate1 = new Date(dateArray1[2],dateArray1[1]-1,dateArray1[0]);
		var dDate = dateArray2[0].length;
		dateArray2[0] = dateArray2[0].substr(dDate-2,dDate);
		var newDate2 = new Date(dateArray2[2],dateArray2[1]-1,dateArray2[0]);           
		var dayDiff ;
		var diffInMilliSec = newDate1.getTime() - newDate2.getTime();
		var milliSecInDay = 24*60*60*1000;	
	
		if ((diffInMilliSec % milliSecInDay == 0)  || (((diffInMilliSec % milliSecInDay)/milliSecInDay) < 0.499)) {
			dayDiff = (diffInMilliSec / milliSecInDay);
		} else {
			dayDiff = (diffInMilliSec / milliSecInDay) + 1;
		}	
		
		return Math.floor(dayDiff);  
	}	else {
		return false;
	}
}

// ------------------------------------------------------------------
// addDays (date_string in mm/dd/yyyy format, days in integer)
// Returns a date in in mm/dd/yyyy format
// ------------------------------------------------------------------
function addDays(myDate,days) 
{
		if(myDate != "" && myDate != null){
			if( typeof(myDate) == 'string' ){
				var dateArray = myDate.split("/");
				var dateLen = dateArray[0].length;
                dateArray[0] = dateArray[0].substr(dateLen-2,dateLen);
                var changedDate = new Date(dateArray[2],dateArray[1]-1,dateArray[0]);
				changedDate.setDate(changedDate.getDate()+parseInt(days));
			}	else if( typeof(myDate) == 'object' ) {
				var changedDate = new Date(myDate.getFullYear(),myDate.getMonth(),myDate.getDate());
				changedDate.setDate(changedDate.getDate()+parseInt(days));
			}
		} else
		{
			// return false; // 1.6.1 Missing Points - DELETED
			//R2.2 Artifact artf1195317 : Departure date is setting as null on tab out or on click -Ashish
			//pass the empty string instead of Null.
			return ""; // 1.6.1 Missing Points - ADDED
		}
		var month=parseInt(changedDate.getMonth(),10)+1;
		var year = parseInt(changedDate.getFullYear(),10);
	
		var sday = changedDate.getDate();
		if (sday < 10 ){
				sday = "0" + sday;
		}
		if (month < 10 ){
				month = "0" + month;
		}
		var addedDate = sday+"/"+month+"/"+year;  
		return addedDate;  
}	

// ------------------------------------------------------------------
// formatDateAsString (date_string in mm/dd/yyyy format, days in integer)
// Returns a date in in mm/dd/yyyy format
// ------------------------------------------------------------------
function formatDateAsString(objDate) 
{          
        //Release2.0|BugID:496172|Rajneesh
        //modified the code to format the date to display the day's name with date
        var day = parseInt(objDate.getDate(),10);
		var month=parseInt(objDate.getMonth(),10);
		var year = parseInt(objDate.getFullYear(),10);	

		//if (day < 10 ) {	day = "0" + day;	}
		//if (month < 10 ){	month = "0" + month;	}
		//var result = weekday[dayofWeek]+" "+day+"/"+month+"/"+year;  
		//return result; 
		var date =  new Date(year, month, day);
		return $.datepicker.formatDate('D dd/mm/yy', date); 
}	

// ------------------------------------------------------------------
// Function to check the string - 's' 
// Return true if it is a Integer value
// ------------------------------------------------------------------
function isInteger(s) 
{
		for (var i = 0; i < s.length; i++) {  			 
				// Check that current character is number.
				var c = s.charAt(i);
		if ((c < "0") || (c > "9")) return false;
		}    
		return true;
}

// ------------------------------------------------------------------
// Function to ensure an argument is a positive integer
// Return true if it is a positive integer
// ------------------------------------------------------------------
function isPositiveInt(arg) {
	if (isNaN(arg)) {
		return false;
	}
	if (Math.ceil(arg) != Math.floor(arg)) {
		return false;
	}
	if (arg < 0) {
		return false;
	}	
	if (arg == '') {
		return false;
	}	
	return true; //	Passed all tests, must be OK
}
