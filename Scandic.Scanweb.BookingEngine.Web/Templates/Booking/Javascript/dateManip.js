//	Declare Arrays														*//
var daysArray = new Array("S","M","T","W","T","F","S");
var monthsArray	= new Array("January","February","March","April","May","June","July", "August", "September", "October","November","December");
var daysInMonthArray	= new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

//var today			= serverDate;

//	Add Prototypes to the Date Object for holidays
Date.prototype.isCalendarHoliday	= "false";
Date.prototype.isBusinessHoliday	= "false";
Date.prototype.holidayName			= "N/A";

//	Function to verify and adjust all years to four-digit format
function adjustYear(myYear) {
	if ((myYear + "").length == 4) {
		return myYear;
	} else if (myYear <= 30) {
		return (myYear + 2000);
	} else if (myYear > 30) {
		return (myYear + 1900);
	} else {
		return -1;
	}
}

//	Function to Check Leap Year
function isLeapYear(myYear) {

	vYear	= adjustYear(myYear);
	
	if ((vYear % 4) == 0) {
		return true;
	} else {
		return false;
	}
}

//	Function to determine if a set of arguments will create a real date.
//	Check to ensure each argument is numeric, whole number, positive, 
//	and not out of range.
function isDate(yr, mo, dy) {
	var yearLow		= 1900;
	var yearHigh	= 2500;
	var maxDays		= 0;
	
//	Check to ensure we have positive integers.
	if (!isPositiveInt(yr) || !isPositiveInt(mo) || !isPositiveInt(dy)) {
		return false;
	}
	
//	Now check out-of-range numbers.
	if ((yr < yearLow) || (yr > yearHigh)) {
		return false;
	}
	if ((mo < 0) || (mo > 11)) {
		return false;
	}
//	Leap year adjustment for February
	if ((isLeapYear(yr)) && (mo == 1)) {
		maxDays = daysInMonthArray[mo] + 1;
	} else {
		maxDays	= daysInMonthArray[mo];
	}
	if ((dy < 1) || (dy > maxDays)) {
		return false;
	}
	
//	If we made it this far, we've got the potential for creating a real date.
	return true;
}

//	Function to return the day of the week as a string, given a Date object
function getDayString(dateObj) {
	return dayAsString(dateObj.getDay());
}

//	Function to return the month of the year as a string, given a Date object
function getMonthString(dateObj) {
	return monthAsString(dateObj.getMonth());
}

//	Function to return the abbreviated day of the week as a string, given a Date object
function getShortDayString(dateObj) {
	return dayAsString(dateObj.getDay()).substring(0,3);
}

//	Function to return the abbreviated day (single char) of the week as a string, given a day string like "s" for sunday
function getVeryShortDayString(day) {
	return day.substring(0,1);
}

//	Function to return the abbreviated month of the year as a string, given a Date object
function getShortMonthString(dateObj) {
	return monthAsString(dateObj.getMonth()).substring(0,3);
}

//	Given an integer between 0 and 6, return the associated day as a string
function dayAsString(iDay) {	
	if ((iDay > -1) && (iDay < 7)) {
		return daysArray[iDay];
	} else {
		return "ERROR : Day out of range";
	}
}

//	Given the day as a string, return an integer value
function dayAsInteger(sDay)	{

	var	baseDay	= sDay.toLowerCase().substring(0,3);
	var	j		= -1
	
	for (var i=0;i<7;i++) {
		if (daysArray[i].toLowerCase().substring(0,3) == baseDay) {
			j	= i;
			break;
		}
	}
	
	return j;
}

//	Function to calculate the number of days in a given month, which is 
//	passed as part of a date object.  Returns integer number of days.
function daysInMonth(dateObj) {

	var mo	= dateObj.getMonth();					// Integer month
	var	yr	= dateObj.getFullYear();				// Want 4-digit year
	
	var numDays	= daysInMonthArray[mo];

	if ((mo == 1) && (isLeapYear(yr))) {			// February Leap Year Adjustment
		numDays++;
	}
	
	return numDays;
}

//	Function which returns the date of the first day of the month, as a Date object.
function firstDateOfMonth(dateObj) {

	if (typeof dateObj == "object") {

		var tmpYr	= dateObj.getFullYear();
		var tmpMon	= dateObj.getMonth();
		var	tmpDay	= dateObj.getDate();
		var tmpDate	= new Date(tmpYr, tmpMon, 1);
				
		return tmpDate;
	} else {
		return -1;
	}
}

//	Function which returns the first day of the month, as an integer.
//	This can be converted to a string using the dayAsString function.
function firstDayOfMonth(dateObj) {

	if (typeof dateObj == "object") {

		
		var firstDay	= firstDateOfMonth(dateObj).getDay();
		
		return firstDay;
	} else {
		return -1;
	}
}

//	Function which returns the last day of the month, as an integer.
//	This can be converted to a string using the dayAsString function.
function lastDayOfMonth(dateObj) {
	if (typeof dateObj == "object") {

		var lastDay	= lastDateOfMonth(dateObj).getDay();
		
		return lastDay;
	} else {
		return -1;
	}
	
}

//	Function which returns the date last day of the month, as a Date object.
function lastDateOfMonth(dateObj) {
	if (typeof dateObj == "object") {

		var tmpYr	= dateObj.getFullYear();
		var tmpMon	= dateObj.getMonth();
		var	tmpDay	= daysInMonth(dateObj);
		var tmpDate	= new Date(tmpYr, tmpMon, tmpDay);
		
		return tmpDate;
	} else {
		return -1;
	}
	
}

//	Function which returns the first day of the year, as an integer.
//	This can be converted to a string using the dayAsString function.
function firstDayOfYear(dateObj) {

	if (typeof dateObj == "object") {

		var tmpYr	= dateObj.getFullYear();
		var tmpDate	= new Date(tmpYr, 0, 1);
		
		var firstDay	= tmpDate.getDay();
		
		return firstDay;
	} else {
		return -1;
	}
}

//	Function which returns the last day of the year, as an integer.
//	This can be converted to a string using the dayAsString function.
function lastDayOfYear(dateObj) {

	if (typeof dateObj == "object") {

		var tmpYr	= dateObj.getFullYear();
		var tmpDate	= new Date(tmpYr, 11, 31);
				
		var lastDay	= tmpDate.getDay();
		
		return lastDay;
	} else {
		return -1;
	}
}

//	Given the month as a string, return an integer value
function monthAsInteger(sMonth) {
	var baseMonth	= sMonth.toLowerCase().substring(0,3);
	var j			= -1;
	
	for (var i=0;i<12;i++) {
		if (monthsArray[i].toLowerCase().substring(0,3) == baseMonth) {
			j	= i;
			break;
		}
	}
	
	return j;
}

//	Given an integer between 0 and 11, return the associated month as a string
function monthAsString(iMonth) {

//	Change the month string based on the sitelanguage
	switch(gSiteLanguage)
	{
		case "EN":
					monthsArray = new Array("January","February","March","April","May","June","July","August", "September","October","November","December");
					// R1.2 | artf685027 | Change characters for week days
					daysArray = new Array("S","M","T","W","T","F","S");
					break;
		case "DA":
					monthsArray = new Array("Januar","Februar","Marts","April","Maj","Juni","Juli","August", "September","Oktober","November","December");
					// R1.2 | artf685027 | Change characters for week days
					daysArray = new Array("S","M","T","O","T","F","L");
					break;
		case "FI":
					monthsArray = new Array("Tammikuu","Helmikuu","Maaliskuu","Huhtikuu","Toukokuu","Kes&auml;kuu","Hein&auml;kuu","Elokuu","Syyskuu","Lokakuu","Marraskuu","Joulukuu");
					// R1.2 | artf685027 | Change characters for week days
					daysArray = new Array("S","M","T","K","T","P","L");
					break;
		case "NO":
					monthsArray = new Array("Januar","Februar","Mars","April","Mai","Juni","Juli", "August", "September", "Oktober","November","Desember");
					// R1.2 | artf685027 | Change characters for week days
					daysArray = new Array("S","M","T","O","T","F","L");
					break;
		case "SV":
					monthsArray = new Array("Januari","Februari","Mars","April","Maj","Juni","Juli", "Augusti", "September","Oktober","November","December");
					// R1.2 | artf685027 | Change characters for week days
					daysArray = new Array("S","M","T","O","T","F","L");
					break;
		case "DE":
					monthsArray = new Array("Jan","Feb","Maz","Apr","Mai","Jun","Jul", "Aug", "Sep","Okt","Nov","Dez");
					// R1.2 | artf685027 | Change characters for week days
					daysArray = new Array("S","M","T","O","T","F","L");
					break;
		default:
					monthsArray = new Array("January","February","March","April","May","June","July", "August", "September", "October","November","December");
					// R1.2 | artf685027 | Change characters for week days
					daysArray = new Array("S","M","T","W","T","F","S");
					break;
	}
	if ((iMonth > -1) && (iMonth < 12)) {
		return monthsArray[iMonth];
	} else {
		return "ERROR : Month out of range";
	}
}

//	Function to add/subtract months from a given date object
function monthAdd(dateObj, numMonths) {
	var adjYrs		= dateObj.getYear();
	var	adjMos		= dateObj.getMonth();
	var tempDate	= dateObj;
	
	var mos, yrs;
	
	mos			= (adjMos + numMonths) % 12;
	if (numMonths < 0) {										// Need to round the proper direction
		yrs			= Math.ceil((adjMos + numMonths) / 12);
	} else {
		yrs			= Math.floor((adjMos + numMonths) / 12);
	}
		
	date1		= new Date(tempDate.setMonth(mos));
	date2		= yearAdd(date1, yrs);
	date3 = date1;
	date4 = date2;
	return date2;
	
}

//	Function to add/subtract months from a given date object
function monthAddSimple(dateObj, numMonths) {
	var origYrs		= dateObj.getFullYear();
	var	origMos		= dateObj.getMonth();
		
	var addMos, addYrs;
	
	rValue			= new Array();
	
	addMos			= numMonths % 12;
	if (numMonths < 0) {										// Need to round the proper direction
		addYrs		= Math.ceil(numMonths / 12);
	} else {
		addYrs		= Math.floor(numMonths / 12);
	}

	var mos			= origMos + addMos;
	var yrs			= origYrs + addYrs;
	
	if (mos < 0) {
		rValue[0]		= mos + 12;
		rValue[1]		= yrs - 1;
	} else if (mos > 11) {
		rValue[0]		= mos - 12;
		rValue[1]		= yrs + 1;		
	} else {
		rValue[0]		= mos;
		rValue[1]		= yrs;
	}

	return rValue;
}

//	Function to determine if a given year, month, day is "today".  Returns
//	true or false.  Requires integer year, month, and day arguments.
function isToday(yr, mo, dy) {
	var todayYear	= today.getFullYear();
	var todayMonth	= today.getMonth();
	var	todayDay	= today.getDate();
	
	if ((yr == todayYear) && (mo == todayMonth) && (dy == todayDay)) {
		return true;
	} else {
		return false;
	}

}

//	Function used to return "now" as a date object.
//	This is for simplicity and allowing for ColdFusion-like syntax.
function now() {
	return new Date();
}

//	Function to find the last specific day in a month.  For example, to
//	find the last Thursday in November (Thanksgiving).  Returns a Date 
//	object set to the desired day of the month.
function findLastDayInMonth(findDay, dateObj) {
	var intDay		= dayAsInteger(findDay);
	var lastDay		= lastDayOfMonth(dateObj);
	var lastDate	= lastDateOfMonth(dateObj)
	var diffDays;
		
	if ((intDay != -1) || (lastDay != -1)) {
		if (intDay > lastDay) {
			diffDays	= (lastDay + 7) - intDay;
		} else {
			diffDays	= lastDay - intDay;
		}

		retDate		= dateAdd(lastDate, -diffDays, 'd');
		return retDate;
	} else {
		return "ERROR.";
	}
}

//	Function to "clone" a date object for manipulation.  This will allow
//	the original date to be preserved
function cloneDate(dateObj) {
	var mDate	= dateObj.getTime();
	var cDate	= new Date(mDate);
	
	return cDate;

}

//	Function to ensure an argument is a positive integer
function isPositiveInt(arg) {

	if (isNaN(arg)) {
		return false;
	}
	if (Math.ceil(arg) != Math.floor(arg)) {
		return false;
	}
	if (arg < 0) {
		return false;
	}
	
//	Passed all tests, must be OK
	return true;
}

//	Function to add leading zero to any one-digit date component.
function makeTwoDigit(numCheck) {
	if ((numCheck + "").length < 2) {
		return "0" + numCheck;
	} else {
		return numCheck;
	}
}