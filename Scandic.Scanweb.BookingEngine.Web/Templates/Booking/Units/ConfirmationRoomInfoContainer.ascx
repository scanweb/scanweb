<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ConfirmationRoomInfoContainer.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ConfirmationRoomInfoContainer"  %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%
    if ((Request.QueryString["command"] == null) ||
        ((Request.QueryString["command"] != null) && (Request.QueryString["command"].ToString() == "isPDF")))
    {%>
<div id="divSingleRoom1" runat="server"  class="roomDetails expand">
    <!-- Header -->
    <div class="hdLft scansprite" id="Header" runat="server">
        <h2 class="hdRt">
            <span id="lblMainGuestReservationNumber" runat="server" style="float:left;"></span>
            <span class="help scansprite toolTipMe ie9tip ie7mgtop5" id="lblAnchMainGuestRNo" runat="server"></span>
        </h2>
    </div>

    <!-- BEGIN Print Header -->
    <div class="hdPrnt" id="printHeader" runat="server">
     <img src="<%= ResolveUrl("/templates/Scanweb/Images/Images_Email/singleBrdrBox_top.gif") %>" alt="" style="width: 716px; height: 5px; border-width: 0px; display: block;"  />
    </div>
    <!-- Top Border -->
    <div class="headCancelFlow" id="divCancelFlowRoomHeaderBorder" visible="false" runat="server">&#160;</div>
    <div class="cnt" style="width: 694px;">
        <div id="divCancelFlowRoomHeader" visible="false" runat="server">
            <!-- Header -->
            <div class="cancelFlowRoomHd" >
                <h2 id="reservationNumberText">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingReservationNumber") %>: <span id="reservationNumber" runat="server" ></span><br /><br />
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/cancellationNumber") %>: <span id="cancellationNumber" runat="server" ></span>
                </h2>
            </div>
        </div>
        
        <!-- END Print Header -->        
        <!-- BEGIN User Stay Information -->
        <div class="userStayInfoContainer" id ="divUserStayInfoContainer" runat ="server" visible ="false">
            <div class="row">
                <p class="userStayTitle"><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/namehotelheader") %></strong></p>
                <p class="userStayDetail"><span id ="spnHotelName" runat="server"></span></p>
                <div class="clearAll"><!-- Clearing Float --></div>
            </div> 
            <div class="row">
                <p class="userStayTitle"><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin") %></strong></p>
                <p class="userStayDetail"><span id ="spnArrivalDate" runat="server"></span></p>
                <div class="clearAll"><!-- Clearing Float --></div>
            </div>
            <div class="row">
                <p class="userStayTitle"><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut") %></strong></p>
                <p class="userStayDetail"><span id ="spnDepartureDate" runat="server"></span></p>
                <div class="clearAll"><!-- Clearing Float --></div>
            </div> 
            <hr />
        </div>
        <!-- END User Stay Information -->
        
        <div class="roomInfo">
            <h2 class="prntHeading" id="prntHeading" runat="server">
                <span id="spanMainGuestReservationNumber" runat="server"></span>
            </h2>
            <div runat="server" id="DNumberContainer" class="wrapper">
                <p>
                    <strong class="dNumber">
                        <label for="Adultschildren" id="DnumberLabel" runat="server">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/DNumber") %>                            
                        </label>
                        <%= ShowDnumber %>
                    </strong>
                </p>
            </div>
            <div class="col1">
                <div class="wrapper" id="pdfRoomTypeInfo" runat="server">
                    <p>
                        <label for="Adultschildren">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/RoomType") %>
                        </label>
                        <span id="lblRoomType" runat="server"></span>
                    </p>
                </div>
            </div>
            <div class="col2 pdfRoomRateInfo">
                <div class="wrapper fltLft">
                    <p class="fltLft">
                        <label for="Other preferences">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/sortRate") %>
                        </label>
						
                        <span class="fltLft" style="width:180px;"><span id="lblRoomRate" runat="server"></span><a class="help spriteIcon toolTipMe" style="float:none;"
                            id="lblAnchRoomRate" runat="server"></a></span>
                    </p>
                    <p class="fltLft">
                        <label for="Special service requests" id="lblPernightstay" runat="server">&#160;</label>
                        <span id="lblGuestRatePerNight" runat="server" class="fltLft"></span>
                    </p>
                </div>
            </div>
            <br class="clear" />
           
            <!--<h3>Contact person</h3>-->
            <!--start - divRoomPreferences - not required in cancel flow-->
             <div id ="divRoomPreferences" runat="server" visible="true">
             <hr class="clear" style="border-top: 1px solid #ccc; width: 652px; margin: 22px 0 0 10px;" />
            <div class="col1 mrgTop22">
                <div class="wrapper">
                    <p>
                        <label for="Adultschildren">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AdultsChilds") %>
                        </label>
                        <span id="lblAdults" runat="server" style="display: none"></span><span id="lblChild"
                            runat="server" style="display: none"></span>
                        <%
                            if (!string.IsNullOrEmpty(lblAdults.InnerText) && Convert.ToInt32(lblAdults.InnerText) > 1 &&
                                !string.IsNullOrEmpty(lblChild.InnerText) && Convert.ToInt32(lblChild.InnerText) > 0)
                            {
                                %>
                        <%= lblAdults.InnerText %>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                        ,
                        <% } %>
                        <%
                            else if (!string.IsNullOrEmpty(lblAdults.InnerText) &&
                                     Convert.ToInt32(lblAdults.InnerText) == 1 &&
                                     !string.IsNullOrEmpty(lblChild.InnerText) && Convert.ToInt32(lblChild.InnerText) > 0)
                            {%>
                        <%= lblAdults.InnerText %>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Adult") %>
                        ,
                        <% } %>
                        <%
                            else if (!string.IsNullOrEmpty(lblAdults.InnerText) &&
                                     Convert.ToInt32(lblAdults.InnerText) > 1 &&
                                     !string.IsNullOrEmpty(lblChild.InnerText) &&
                                     Convert.ToInt32(lblChild.InnerText) == 0)
                            {%>
                        <%= lblAdults.InnerText %>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                        <% } %>
                        <%
                            else
                            {%>
                        <%= lblAdults.InnerText %>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Adult") %>
                        <% } %>
                        <% if (!string.IsNullOrEmpty(lblChild.InnerText) && Convert.ToInt32(lblChild.InnerText) > 1)
                           { %>
                        <%= lblChild.InnerText %>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/children") %>
                        <% } %>
                        <%
                           else if (!string.IsNullOrEmpty(lblChild.InnerText) && Convert.ToInt32(lblChild.InnerText) == 1)
                           {%>
                        <%= lblChild.InnerText %>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child") %>
                        <% } %>
                    </p>
                    <p>
                        <label for="Adult'sbedtype">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AdultBedType") %>
                        </label>
                        <span id="lblBedType" class="fltLft lblBedType" runat="server"></span>
                    </p>
                    <p id="lblChildsbedtype" runat="server" class="bookingConfirmationBT">
                        <label for="Child'sbedtype">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildBedType") %>
                        </label>
                    </p>
                    <ul id="lblDynChilds" runat="server" style="margin: 0pt; padding: 0pt 0pt 10px; float: left;
                        list-style-type: none; list-style-image: none; list-style-position: outside;width:150px;"
                        class="conf_List">
                    </ul>
                </div>
            </div>
            <div class="col2 other-req mrgTop22">
                <div class="wrapper">
                    <p class="fltLft" id="OthersRequestCtrl" runat="server">
                        <label for="Other preferences" class="fltLft">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/OtherPreferences") %>
                        </label>
                        <div id="lblGuestOtherPref" runat="server" class="fltLft lblBedType"></div>
                        <div class="clearAll"> &#160;</div>
                    </p>
                    <p class="fltLft" id="SpecialRequestCtrl" runat="server">
                        <label for="Special service requests" class="fltLft">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/specialservicerequests") %>
                        </label>
                        <div id="lblGuestSpecialReq" runat="server" class="fltLft lblBedType">                        </div>
                        <div class="clearAll">&#160;</div>
                    </p>
                </div>
            </div>
        
        <br class="clear" />
        <!--START:Addtional request -->
        <div class="roomInfo addRequest">
            <strong style="float: left; width: 145px; display: block;" id="addRequestHdr" runat="server">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/AdditionalReq") %>
            </strong>
            <div id="addRequestText" runat="server" style="width: 660px; clear: both;">            </div>
            <div class="clearAll">                &#160;</div>
        </div>
       </div>
        <!--END:Addtional request -->
        <hr class="clear" style="border-top: 1px solid #ccc; width: 652px; margin: 22px 0 0 10px;" />
        </div>
        <!-- End - divRoomPreferences - not required in cancel flow--> 
       
        <div class="roomInfo">
            <div class="col1 col1Ext">
                <h3>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/Contactperson") %>
                </h3>
                <div class="wrapper">
                    <p style="width: 650px;">
                        <label for="Adultschildren">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/Name") %>
                        </label>
                        <!-- Ramana: Added as part of title for name -->
                        <% if (Utility.GetCurrentLanguage().ToUpper() == LanguageConstant.LANGUAGE_GERMAN)
                           {%>
                            <span id="lblTitle"  runat="server" />&nbsp;
                            <% } %>
                        <span id="lblFName" runat="server"></span>&nbsp;<span id="lblLName" runat="server"></span>
                    </p>
                     <%--<p style="width: 650px;">
                        <label for="Adult'sbedtype">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/townorcity") %>
                        </label>
                        <span id="lblCityOrTown" runat="server"></span>
                    </p>--%>
                    <p>
                        <label for="Child'sbedtype">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/countryname") %>
                        </label>
                        <span id="lblCountry" runat="server"></span>
                    </p>
                    <p style="width: 650px;">
                        <label for="Child'sbedtype">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/addressemail") %>
                        </label>
                        <span id="spanEmailPrnt" runat="server"></span><span id="spanEmail" runat="server"><a
                            id="lblEmail" tabindex="25" runat="server"></a></span><span id="spanEmailAnch" runat="server"><a
                                id="lblAnchEmail" runat="server"></a></span>
                    </p>
                    <p>
                        <label for="Child'sbedtype">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/PhoneNumber") %>
                        </label>
                        <span id="lblMobileValue" runat="Server"></span>
                    </p>
                    <%--<br class="clear" />--%>                    <%--<fix artif: 1148182 in RS2 ADD A <P> IN ALL THE div tags. >--%>
                    <p>
                        <div id="divFGPNumber" runat="server" style="display: none; padding-bottom: 10px;" class="mgnBtm10">
                            <label for="Child'sbedtype">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/accountnumber") %>
                            </label>
                            <span id="lblFGPNumber" runat="Server"></span>
                        </div>
                    </p>
                    <p runat="server" id="creditCardSectionForConfPage" visible="false">
                        <p id="ccNameSection" runat="server" >
                            <div id="divCreditCardName" runat="server" visible="false" class="mgnBtm10">
                                <label>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/cardholder") %>
                                </label>
                                <span id="lblCreditCardName" runat="Server"></span>
                            </div>
                        </p>
                        <p>
                            <div runat="server" visible="false" id="divCreditCardNum" class="mgnBtm10">
                                <label for="Child'sbedtype">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/cardnumber") %>
                                </label>
                                <span id="lblCreditCardNumber" runat="Server"></span>
                            </div>
                        </p>
                        <p>
                            <div runat="server" visible="false" id="divCreditCardType" class="mgnBtm10">
                                <label>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/cardtype") %>
                                </label>
                                <span id="lblCreditCardType" runat="Server"></span>
                            </div>
                        </p>
                        <p>
                            <div id="divExpiryDate" runat="server" visible="false">
                                <label for="Child'sbedtype">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/expirydate") %>
                                </label>
                                <span id="lblExpiryDate" runat="Server"></span>
                            </div>
                        </p>
                    </p>
                </div>
            </div>
        </div>
        <br class="clear" />
        <div class="roomInfo" id="guaranteeInformation" runat="server">
            <hr class="clear" style="border-top: 1px solid #ccc; width: 652px; margin: 22px 0 0;" />
            <div class="guaranteTypeInfo">
                <h3>
                    <asp:Label ID="lblGuaranteeOptionHeader" runat="server"></asp:Label>
                    <%--  <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/GuaranteeInformation")%>--%>
                </h3>
                <label id="lblGuaranteeOption" runat="server" class="lblGuarantee">
                </label>
                <%--<ul>
                    <li>
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/GaurenteeText1")%>
                    </li>
                    <li>
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/GaurenteeText2")%>
                    </li>
                    <li>
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/GaurenteeText3")%>
                        <a title="www.scandichotels.com" href="#">www.scandichotels.com</a>
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/GaurenteeText4")%>
                    </li>
                </ul>--%>
            </div>
            <br class="clear" />
        </div>
    </div>
    <!-- R2.2 | Sheril | Added Style for PDF -->
    <div class="ft sprite" id="pdfFooter" runat="server">&nbsp;</div>
</div>
 <% } %>
 
 
 <!-- BEGIN Print Room Information -->
   <% if (Request.QueryString["command"] != null && Request.QueryString["command"].ToString() == "print")
      {%>
<div id="printRoomInfo" class="section">
	<h3><span id="pfLblMainGuestReservationNumber" runat="server"></span></h3>
	<div id="pfDivCancelFlowRoomHeader" visible="false" runat="server">
        <h3>
            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingReservationNumber") %>: <span id="pfReservationNumber" runat="server" ></span><br />
            <%= WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/cancellationNumber") %>: <span id="pfCancellationNumber" runat="server" ></span>
        </h3>
    </div>
	<!-- User Stay Information -->
	<div id ="pfDivUserStayInfoContainer" runat ="server" visible ="false">
            <div>
                <p><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/namehotelheader") %></strong></p>
                <p><span id ="pfSpnHotelName" runat="server"></span></p>
                <div class="clearAll"></div>
            </div> 
            <div>
                <p><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin") %></strong></p>
                <p ><span id ="pfSpnArrivalDate" runat="server"></span></p>
                <div class="clearAll"></div>
            </div>
            <div class="row">
                <p><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut") %></strong></p>
                <p><span id ="pfSpnDepartureDate" runat="server"></span></p>
                <div class="clearAll"></div>
            </div> 
            <hr />
        </div>
	
	    <div id="viewModifyDLRedirectPDFLinkForOtherRooms" visible="false" runat="server">
            <div class="row clearFix">
                <table cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/ViewModifyReservationMessage")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a id="ancViewModifyDLRedirectLinkOtherRoom" runat="server"></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
         </div>
	<!-- Hotel Information -->
	<div class="row clearFix">
		<div class="col1">
			<table>
				<tbody>
				<!-- AMS Patch7| artf1263355 : Multileg confirmation pdf does not contain dates-->
				    <div id ="pfDivCheckInandCheckOutDateContainer" runat ="server" visible ="false">
				        <tr>
						    <td class="dataCol1"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin") %></td>
						    <td class="dataCol2"><span id="pfSpnCheckInDate" runat="server"></span></td>
					    </tr>
					    <tr>
						    <td class="dataCol1"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut") %></td>
						    <td class="dataCol2"><span id="pfSpnCheckOutDate" runat="server"></span></td>
					    </tr>
					</div>				
					<tr>
						<td class="dataCol1"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/RoomType") %></td>
						<td class="dataCol2"><span id="pfLblRoomType" runat="server"></span></td>
					</tr>
					<tr>
						<td class="dataCol1"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/sortRate") %></td>
						<td class="dataCol2"><span id="pfLblRoomRate" runat="server"></span></td>
					</tr>
					<tr>
						<td class="dataCol1"><span id="pfLblPernightstay" runat="server"></span></td>
						<td class="dataCol2"><span id="pfLblGuestRatePerNight" runat="server"></span></td>
					</tr>
					<tr>
						<td class="dataCol1"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AdultsChilds") %></td>
						<td class="dataCol2">
						    <span id="pfLblAdults" runat="server" style="display: none"></span>
                            <span id="pfLblChild" runat="server" style="display: none"></span>
                        <%
                            if (!string.IsNullOrEmpty(pfLblAdults.InnerText) &&
                                Convert.ToInt32(pfLblAdults.InnerText) > 1 &&
                                !string.IsNullOrEmpty(pfLblChild.InnerText) && Convert.ToInt32(pfLblChild.InnerText) > 0)
                            {
                                %>
                        <%= pfLblAdults.InnerText %>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                        ,
                        <% } %>
                        <%
                            else if (!string.IsNullOrEmpty(pfLblAdults.InnerText) &&
                                     Convert.ToInt32(pfLblAdults.InnerText) == 1 &&
                                     !string.IsNullOrEmpty(pfLblChild.InnerText) &&
                                     Convert.ToInt32(pfLblChild.InnerText) > 0)
                            {%>
                        <%= pfLblAdults.InnerText %>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Adult") %>
                        ,
                        <% } %>
                        <%
                            else if (!string.IsNullOrEmpty(pfLblAdults.InnerText) &&
                                     Convert.ToInt32(pfLblAdults.InnerText) > 1 &&
                                     !string.IsNullOrEmpty(pfLblChild.InnerText) &&
                                     Convert.ToInt32(pfLblChild.InnerText) == 0)
                            {%>
                        <%= pfLblAdults.InnerText %>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                        <% } %>
                        <%
                            else
                            {%>
                        <%= pfLblAdults.InnerText %>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Adult") %>
                        <% } %>
                        <% if (!string.IsNullOrEmpty(pfLblChild.InnerText) && Convert.ToInt32(pfLblChild.InnerText) > 1)
                           { %>
                        <%= pfLblChild.InnerText %>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/children") %>
                        <% } %>
                        <%
                           else if (!string.IsNullOrEmpty(pfLblChild.InnerText) &&
                                    Convert.ToInt32(pfLblChild.InnerText) == 1)
                           {%>
                        <%= pfLblChild.InnerText %>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child") %>
                        <% } %>
						</td>
					</tr>
					<tr>
						<td class="dataCol1"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AdultBedType") %></td>
						<td class="dataCol2"><span id="pfLblBedType" class="fltLft lblBedType" runat="server"></span></td>
					</tr>
					<tr>
					<div id="pfpLblChildsbedtype" runat="server">
						<td class="dataCol1"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildBedType") %></td>
						<td class="dataCol2"><span id="pfLblDynChilds" runat="server" ></span></td>
					</div>
					</tr>
					<tr>
					    <div id="pfDNumberContainer" runat="server" visible="false">
					        <td class="dataCol1"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/DNumber") %></td>
					        <td class="dataCol2"><%= ShowDnumber %></td>                     
	                    </div>
					</tr>
					<tr>
					    <div id="pfDivFGPNumber" runat="server" visible="false">
					        <td class="dataCol1"><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/accountnumber") %></td>
					        <td class="dataCol2"><span id="pfLblFGPNumber" runat="Server"></span></td>
					    </div>
					</tr>					
				</tbody>
			</table>
		</div>
		<div class="col2 contact">
			<h4><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/Contactperson") %></h4>
			<table>
				<tr>
					<td class="dataCol3"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/Name") %></td>
					<td class="dataCol4"><span id="pfLblFName" runat="server"></span>&nbsp;<span id="pfLblLName" runat="server"></span></td>
				</tr>
				<%--<tr>
					<td class="dataCol3"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/townorcity") %></td>
					<td class="dataCol4"><span id="pfLblCityOrTown" runat="server"></span></td>
				</tr>--%>
				<tr>
					<td class="dataCol3"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/countryname") %></td>
					<td class="dataCol4"><span id="pfLblCountry" runat="server"></span></td>
				</tr>
				<tr>
					<td class="dataCol3"><%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/addressemail") %></td>
					<td class="dataCol4"><span id="pfLblEmail" runat="server"></span></td>
				</tr>
				<tr>
					<td class="dataCol3"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/PhoneNumber") %></td>
					<td class="dataCol4"><span id="pfLblMobileValue" runat="Server"></span></td>
				</tr>
			</table>
			
			<!--Start: AMS Patch7 | artf1263082: CC Information for PDF and Printer Friendly -->	
		   <div id="pfdivCreditCardInfo" runat="server" visible="false">
		    <table>
                    <tr  id="pfCCNameSection" runat="server">
	                <td class="dataCol3"><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/cardholder") %></td>		        
	                <td class="dataCol4"><span id="pflblCreditCardName" runat="server"></span></td>		        
                </tr>                 
                    
                <tr>
	                <td class="dataCol3"><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/cardnumber") %></td>		        
	                <td class="dataCol4"><span id="pflblCreditCardNumber" runat="server"></span></td>		        
                </tr>     
                    
                <tr>
	                <td class="dataCol3"><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/cardtype") %></td>		        
	                <td class="dataCol4"><span id="pflblCreditCardType" runat="server"></span></td>		        
                </tr>     
                    
                <tr>
	                <td class="dataCol3"><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/expirydate") %></td>		        
	                <td class="dataCol4"><span id="pflblExpiryDate" runat="server"></span></td>		        
                </tr>     
            </table>
            </div>
        <!-- End: AMS Patch7 | artf1263082: CC Information for PDF and Printer Friendly-->	
			
			
		</div>
	</div>
	<div class="row requests clearFix" id="specialRequestSection" runat="server" visible="false">
		<div class="col1"  id="pfOthersRequestCtrl" runat="server">
			<table>
				<tr>
					<td class="dataCol1"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/OtherPreferences") %></td>
					<td class="dataCol2">
						<ul>
							<span id="pfLblGuestOtherPref" runat="server" ></span>
						</ul>
					</td>
				</tr>
			</table>
		</div>

		<div class="col2" id="pfSpecialRequestCtrl" runat="server">
			<table>
				<tr>
				
					<td class="dataCol3"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/specialservicerequests") %></td>
					<td class="dataCol4">
						<ul>
							<span id="pfLblGuestSpecialReq" runat="server"></span>
						</ul>
					</td>
				</tr>
			</table>
		</div>
	</div>	
	<div class="additionalReq clearFix" id="pfAddRequestHdr" runat="server">
		<h5><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/AdditionalReq") %></h5>
		<p><span id="pfAddRequestText" runat="server"></span></p>
	</div>
	<div class="row roomGaurenteeInfo clearFix" id="pfguaranteeInformation" runat="server">
         <h5><asp:Label ID="pflblGuaranteeOptionHeader" runat="server" /></h5>
         <label id="pflblGuaranteeOption" runat="server" class="lblGuarantee" />
    </div>
	<div class="clearFix">&#160;</div>
 </div>	
<% } %>	
