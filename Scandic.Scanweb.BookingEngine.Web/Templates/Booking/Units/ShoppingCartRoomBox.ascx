<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartRoomBox.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ShoppingCartRoomBox" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<div class="thinBrdContainer" id="shoppingCartRoomBox" runat="server">
	<div class="hd sprite"></div>
	<div class="cnt">
		<% if (IsCloseButtonEnabled)
           { %>
		    <a class="close spriteIcon" rel="<%= RoomNumber %>" href="#"></a>
		    <%--<asp:LinkButton rel="<%=RoomNumber%>" CssClass="close spriteIcon" OnClick="OnRemoveRoom_Click" runat="server"></asp:LinkButton>--%>
		<% } %>
		<%--<p class="roomHdr"><strong><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Room")%> <%=RoomNumber%><span>-<%=roomCategory%></span></strong></p>--%>
		<p class="roomHdr">
    <!-- Merchandising - Link to select rate -->
		    <strong>   <% if (!IsRateDisplayed)
                          { %>
		        <span style="display:inline;" ><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room") %> <%= RoomNumber %></span>
		        <a style="display:none;"></a>
		     <% } %>			    
		        <%
                          else
		                  {
		                     
		                      
		                      
                          %>
		        <span style="display:none;" ></span>
		        <a class="change jqModal newroomoverlay" id="poproomInfo" tabindex="650" rel=".<%= RoomCategoryID %>" href="#"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room") %> <%= RoomNumber %></a>
		        <span style="display:inline;">- <a class="change jqModal newroomoverlay" tabindex="655" id="poproomInfo2" rel=".<%= RoomCategoryID %>" href="#"><%= RoomCategory %></a></span>
		        <span style="display:none;">- <%= RoomCategoryID %></span>
		        <% } %>
		    </strong> 
		</p>
		<hr />
		<div class="colm1">
	    <!--R2.0 /BugID:496402/Rajneesh
	    Description:Client random feedback| Shopping Cart
	    Modification:Added one else-If statement to handle the condition:Multiple Adults,No Child selection
	    -->	
	    <% if (Convert.ToInt32(NoOfAdults) > 1 && Convert.ToInt32(NoOfChildren) > 0)
           { %>
		   <p><%= NoOfAdults %> <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>,</p>
		    <% } %>
		    <%
           else if (Convert.ToInt32(NoOfAdults) == 1 && Convert.ToInt32(NoOfChildren) > 0)
           {%>
		   <p><%= NoOfAdults %> <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Adult") %>,</p>
		    <% } %>
		    
		    <%
           else if (Convert.ToInt32(NoOfAdults) > 1 && Convert.ToInt32(NoOfChildren) == 0)
           {%>
		   <p><%= NoOfAdults %> <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %></p>
		    <% } %>
		    
		     <%
           else
           {%>
		     <p><%= NoOfAdults %> <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Adult") %></p>
		    <% } %>
		</div>
		<div class="colm2">
		   <% if (Convert.ToInt32(NoOfChildren) > 1)
              { %>
		   <p><%= NoOfChildren %> <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/children") %></p>
		    <% } %>
		    
		  <%
              else if (Convert.ToInt32(NoOfChildren) == 1)
              {%>
		   <p><%= NoOfChildren %> <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child") %></p>
		    <% } %>
		    
		</div>
		<div class="bedVal">
		    <% if (IsChildrenAvailable)
               { %>
		    <p class="chbedType"><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %></strong></p>
		    <% } %>
		    <div class="colCib"><%= ChildrenAccomodationText %></div>
		    <br class="clear" />
		    <% if (IsRateAvailable)
               { %>
                <% if (!IsRateDisplayed)
                   { %>
		        <div class="cost" style="display:block;">
		            <p class="plsSelct fltLft" style="display:block;"><span class="fltLft"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectRoomRate") %></span>
		             <span class="help spriteIcon toolTipMe fltLft" id="plsSelect" title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectRoomRateToolTip") %>"></span>
		            <%--<a class="help spriteIcon" title="#corpRate" href="#"></a>--%>
		            
		            </p>
		        </div>
		        <% }
                   else
                   { %>
		        <div class="costSelct" style="display:block;">
		        
		            <!-- R2.3 | Sheril | Cart Info -->
		            <div class="cartRoomInfo">
		                <div class="cartRoomType"><%= RateType %></div>
		                <div id="cartCost" runat="server" class="cartRoomCost"><%= Rate %>&#160;<%= BaseOrTotalRateText %></div>
		                <div class="clearAll"><!-- Clearing Float --></div>
		            </div> 

		        
		            <%--<p>
		                <span class="fltLft"><%=RateType%></span>
		                <span class="fltRt mrgRt8">
		                    <strong class="roomRate"><%=Rate %></strong> 
		                    <%=BaseOrTotalRateText %>
		                </span>
		            </p>--%>
		            <p class="roomCnt<%= RoomNumber %>"><asp:Label id="bedPref" runat="server" ></asp:Label></p>
		        </div>
		        <% } %>
		    <% } %>
	    </div>
    </div>
	<div class="ft sprite"></div>
</div>