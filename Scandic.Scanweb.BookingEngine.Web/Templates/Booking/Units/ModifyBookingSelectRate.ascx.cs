//  Description					: Code Behind class for ModifyBookingDates Control		  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Shankar Dasgupta                                   	  //
/// Author email id				:                           							  //
/// Creation Date				: 10th December  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: Bhavya Shekar - Changes due to cancel story
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces

using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;

#endregion

#region Scandic Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for ModifyBookingSelectRate
    /// </summary>
    public partial class ModifyBookingSelectRate : EPiServer.UserControlBase
    {
        #region Public Variables
        public string SELECT_RATE_ALL_ROOM_TYPES =
            WebUtil.GetTranslatedText(TranslatedTextConstansts.SELECT_RATE_ALL_ROOM_TYPES);

        public string SELECT_RATE_SPECIFIC_ROOM_TYPES =
            WebUtil.GetTranslatedText(TranslatedTextConstansts.SELECT_RATE_SPECIFIC_ROOM_TYPES);

        private bool isSessionValid = true;
        public const int carouselCount = 25;
        public const int visibleCarouselCount = 5;
        public const double calendarItemGapDays = 1.0;
        public const int noOfYearsInAvailCalendar = 2;
        public string roomNumber = null;
        public bool isComboReservation = false;

        private IContentDataAccessManager contentDataAccessManager = null;
        /// <summary>
        /// Gets m_ContentDataAccessManager
        /// </summary>
        private IContentDataAccessManager m_ContentDataAccessManager
        {
            get
            {
                if (contentDataAccessManager == null)
                {
                    return new ContentDataAccessManager();
                }

                return contentDataAccessManager;
            }
        }

        #endregion

        public string RoomUnavailableErrorMessage
        {
            get
            {
                return operaErrorMsg.InnerHtml;
            }
            set
            {
                operaErrorMsg.InnerHtml = value;
            }
        }
        #region Page_Load

        /// <summary>
        /// Page Load Methods for ModifySelectRate Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ReservationInfo.SetPageType(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE);

            blkRoomDetailContainer.Controls.Clear();
            ProgressBar.SetProgressBarSelection(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE);
            HotelName.SetPageType(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE);
            RoomTab.SetPageType(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE);
            isSessionValid = WebUtil.IsSessionValid();
            ReservationInfo.PopulateReservationInfoContainer(true);

            perNight.Text = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rdoPernight");
            perStay.Text = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rdoperStay");

            if (this.Visible && isSessionValid)
            {
                try
                {
                    isComboReservation = HygieneSessionWrapper.IsComboReservation;
                    if ((txtRoomCategoryID.Value.Length > 0) && (txtRateCategory.Value.Length > 0) &&
                        (txtRoomNumber.Value.Length) > 0)
                    {
                        roomNumber = txtRoomNumber.Value;

                        OnSelectRoomRate(null, null);
                    }
                    if (!string.IsNullOrEmpty(txtPerStay.Value))
                    {
                        Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage = Convert.ToBoolean(txtPerStay.Value);
                    }
                    perNight.Checked = !Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage;
                    perStay.Checked = Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage;

                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                    if (hotelSearch != null)
                    {
                        if (!IsPostBack)
                        {
                            if ((Reservation2SessionWrapper.IsModifyFlow) && !(HygieneSessionWrapper.IsComboReservation) && !Reservation2SessionWrapper.AvailabilityCalendarAccessed)
                            {
                                CreateAvailabilityCalender(hotelSearch);
                            }

                            // Merchandising - Special Alert - Display special alert in booking flow
                            string spAlert = ContentDataAccess.GetSpecialAlert(hotelSearch.SelectedHotelCode, false);
                            if (!string.IsNullOrEmpty(spAlert))
                            {
                                divSpAlertWrap.Visible = true;
                                divSpAlert.InnerHtml = spAlert;
                            }
                            else
                            {
                                divSpAlertWrap.Visible = false;
                            }
                        }
                        if (hotelSearch.SearchingType == SearchType.VOUCHER ||
                            hotelSearch.SearchingType == SearchType.REDEMPTION)
                        {
                            perStay.Visible = false;
                        }
                        else
                        {
                            perStay.Visible = true;
                        }


                        DisplayRates(Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage, true);
                    }

                    DisplayRatesFooterGrid();
                }
                catch (OWSException owsException)
                {
                    AppLogger.LogCustomException(owsException, " OWS Exception occured in SelectRate: Page_Load Method");

                    switch (owsException.ErrCode)
                    {
                        case OWSExceptionConstants.PROMOTION_CODE_INVALID:
                            {
                                HideMainBody();

                                errorLabel.InnerText = WebUtil.GetTranslatedText(owsException.TranslatePath);
                                errorDiv.Visible = true;

                                break;
                            }
                        case OWSExceptionConstants.PROPERTY_NOT_AVAILABLE:
                        case OWSExceptionConstants.INVALID_PROPERTY:
                        case OWSExceptionConstants.PROPERTY_RESTRICTED:
                        case OWSExceptionConstants.PRIOR_STAY:
                            {
                                txtNoroomsAvailable.Value = "TRUE";
                                errorLabel.InnerHtml =
                                    WebUtil.GetTranslatedText(TranslatedTextConstansts.NO_ROOMS_AVAILABLE_MESSAGE);
                                errorDiv.Visible = true;
                                this.Parent.Parent.FindControl("SecondaryBodyRegion").FindControl("OurRates").Visible =
                                    false;
                            }
                            break;
                    }
                }
                catch (CMSException bex)
                {
                    switch (bex.ErrCode)
                    {
                        case CMSExceptionConstants.BLOCK_NOT_CONFIGURED:
                            {
                                HideMainBody();
                                errorLabel.InnerText = WebUtil.GetTranslatedText(bex.TranslatePath);
                                break;
                            }
                    }
                }
                catch (ContentDataAccessException cdaException)
                {
                    if (string.Equals(cdaException.ErrorCode, AppConstants.BONUS_CHEQUE_NOT_FOUND,
                                      StringComparison.InvariantCultureIgnoreCase))
                    {
                        HideMainBody();
                        DisplayErrorMessage(
                            WebUtil.GetTranslatedText(
                                "/scanweb/bookingengine/errormessages/businesserror/bonuschequenotfound"));

                    }
                    else
                        WebUtil.ApplicationErrorLog(cdaException);
                }
                catch (Exception busEx)
                {
                    WebUtil.ApplicationErrorLog(busEx);
                }
            }
        }

        #endregion Page_Load

        #region ShowRoomTypesPerCondition

        /// <summary>
        /// Event Handler for Show Room Condition
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowRoomTypesPerCondition(object sender, EventArgs e)
        {
            bool showPricePerStay = true;
            RadioButton selectedRadio = (sender) as RadioButton;
            if (selectedRadio.ClientID.ToLower().EndsWith("pernight"))
                showPricePerStay = false;
            Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage = showPricePerStay;
            ClearSelectedRoomAndRates();
            if (HygieneSessionWrapper.IsComboReservation)
                Utility.PrepopulateNonModifiableRoomsInSelectedRoomRatesHashTable();

            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE), false);
        }

        #endregion ShowRoomTypesPerCondition

        /// <summary>
        /// Clears the selected room and rates.
        /// </summary>
        private void ClearSelectedRoomAndRates()
        {
            Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
            if (selectedRoomAndRatesHashTable != null)
            {
                selectedRoomAndRatesHashTable.Clear();
                selectedRoomAndRatesHashTable = null;
            }

            UnBlockOnRdBtnChange();
        }

        /// <summary>
        /// Display the Rates Footer
        /// </summary>
        private void DisplayRatesFooterGrid()
        {
            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
            {
                if (SearchCriteriaSessionWrapper.SearchCriteria.ChildrenPerRoom > 0)
                {
                }
            }
        }


        /// <summary>
        /// Function Thatb calls webutil function to unblock the rooms
        /// </summary>
        private void UnBlockOnRdBtnChange()
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if (search != null && search.ListRooms != null && search.ListRooms.Count > 0)
            {
                for (int iListRoom = 0; iListRoom < search.ListRooms.Count; iListRoom++)
                {
                    if (search.ListRooms[iListRoom].IsBlocked &&
                        !string.IsNullOrEmpty(search.ListRooms[iListRoom].RoomBlockReservationNumber))
                    {
                        WebUtil.UnblockRoom(search.ListRooms[iListRoom].RoomBlockReservationNumber);
                        search.ListRooms[iListRoom].RoomBlockReservationNumber = string.Empty;
                        search.ListRooms[iListRoom].IsBlocked = false;
                    }
                }
            }
        }

        private IList<BaseRoomRateDetails> GetPublicRatesForPromotionalSearch(HotelSearchEntity hotelSearch)
        {
            IList<BaseRoomRateDetails> listpublicRateDetails = SelectRoomUtil.MultiRoomAvailSearchOrPrefetch(hotelSearch);
            return listpublicRateDetails;
        }

        /// <summary>
        /// Display the Rates
        /// </summary>
        /// <param name="showPricePerStay">
        /// Boolean indicates as to display the price per Stay
        /// </param>
        /// <param name="showAllRoomTypes">Boolean indicates to show all Room Types
        /// </param>
        private void DisplayRates(bool showPricePerStay, bool showAllRoomTypes)
        {
            string strCampaignCode = null;
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            bool paymentFallback = m_ContentDataAccessManager.GetPaymentFallback(hotelSearch.SelectedHotelCode);
            IList<BaseRoomRateDetails> listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;

            if (!IsPostBack || listRoomRateDetails == null)
            {
                if ((!HygieneSessionWrapper.IsModifySelectRateFromBreadCrumb && !IsPostBack) || listRoomRateDetails == null)
                {
                    listRoomRateDetails = SelectRoomUtil.MultiRoomUpdateRoomModifiableFlags(hotelSearch);
                    if ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation))
                    {
                        PopulateRoomRateDetailsForNonModifiableRooms(listRoomRateDetails);
                        if (!paymentFallback)
                        {
                            RemoveSaveRateFromComboBooking(listRoomRateDetails);
                        }
                    }
                    HotelRoomRateSessionWrapper.ListHotelRoomRate = listRoomRateDetails;
                }
            }

            if (Convert.ToBoolean(AppConstants.ENABLE_PUBLIC_RATE_SEARCH_FOR_PROMOTIONS))
            {
                if (hotelSearch.SearchingType == SearchType.REGULAR && !string.IsNullOrEmpty(hotelSearch.CampaignCode))
                {
                    strCampaignCode = hotelSearch.CampaignCode;
                    hotelSearch.CampaignCode = null;
                    AppLogger.LogInfoMessage("Promo Search: An extra call to fetch the Public Rates for Promotional Search");
                    IList<BaseRoomRateDetails> listHotelPublicRoomRateDetails = GetPublicRatesForPromotionalSearch(hotelSearch);
                    hotelSearch.CampaignCode = strCampaignCode;
                    listRoomRateDetails = SelectRoomUtil.MergePublicRatesWithPromotionalRates(listRoomRateDetails, listHotelPublicRoomRateDetails);
                    HotelRoomRateSessionWrapper.ListHotelRoomRate = listRoomRateDetails;
                }
            }

            List<RoomCategoryEntity> roomCategoryListforPopup = new List<RoomCategoryEntity>();
            if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
            {
                int count = 0;
                for (int roomNumber = 0; roomNumber < listRoomRateDetails.Count; roomNumber++)
                {
                    BaseRoomRateDetails roomRateDetails = listRoomRateDetails[roomNumber];
                    if ((Utility.IsBlockCodeBooking) && (roomRateDetails.RateCategories == null)
                        && (roomRateDetails.RateRoomCategories == null) ||
                        (roomRateDetails.RateRoomCategories.Count == 0))
                    {
                        HotelDestination hotelDestination = roomRateDetails.HotelDestination;
                        DisplayAlternateHotels(hotelDestination);
                        return;
                    }
                    else if ((!Utility.IsBlockCodeBooking) &&
                             ((roomRateDetails.RateCategories == null || (roomRateDetails.RateCategories.Count == 0)
                               || (roomRateDetails.RateRoomCategories == null))))
                    {
                        HotelDestination hotelDestination = roomRateDetails.HotelDestination;
                        DisplayAlternateHotels(hotelDestination);
                        return;
                    }
                    else
                    {
                        if (roomRateDetails.IsRoomModifiable)
                        {
                            BaseRateDisplay rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                            bool defaultRoomTypesAvailable =
                                RoomRateDisplayUtil.RoomTypeAvailable(rateDisplay.RateRoomCategories,
                                                                      SearchCriteriaSessionWrapper.SearchCriteria.
                                                                          SelectedHotelCode);
                            List<RoomCategoryEntity> roomCategoryList = rateDisplay.RateRoomCategories;

                            RoomCategoryEntity configuredRoomCategory = null;
                            if (roomCategoryList != null)
                            {
                                for (int roomCategoryCount = 0;
                                     roomCategoryCount < roomCategoryList.Count;
                                     roomCategoryCount++)
                                {
                                    RoomCategoryEntity roomCategory = roomCategoryList[roomCategoryCount];
                                    if (rateDisplay.HotelDestination != null &&
                                        rateDisplay.HotelDestination.HotelRoomDescriptions != null
                                        && rateDisplay.HotelDestination.HotelRoomDescriptions.Count > 0)
                                    {
                                        bool roomCategoryFound = false;
                                        foreach (
                                            HotelRoomDescription hotelRoomDescription in
                                                rateDisplay.HotelDestination.HotelRoomDescriptions)
                                        {
                                            if (
                                                roomCategoryList[roomCategoryCount].Id.Equals(
                                                    hotelRoomDescription.HotelRoomCategory.RoomCategoryId))
                                            {
                                                configuredRoomCategory = new RoomCategoryEntity();
                                                configuredRoomCategory.Id = roomCategoryList[roomCategoryCount].Id;
                                                if (
                                                    !string.IsNullOrEmpty(
                                                        hotelRoomDescription.HotelRoomCategory.
                                                            RoomCategoryDescription))
                                                    configuredRoomCategory.Description =
                                                        hotelRoomDescription.HotelRoomCategory.
                                                            RoomCategoryDescription;
                                                else
                                                    configuredRoomCategory.Description =
                                                        roomCategoryList[roomCategoryCount].Description;
                                                configuredRoomCategory.Url =
                                                    hotelRoomDescription.HotelRoomCategory.ImageURL;
                                                roomCategoryFound = true;
                                                break;
                                            }
                                        }
                                        if (!roomCategoryFound)
                                        {
                                            configuredRoomCategory = new RoomCategoryEntity();
                                            configuredRoomCategory.Id = roomCategoryList[roomCategoryCount].Id;
                                            configuredRoomCategory.Description =
                                                roomCategoryList[roomCategoryCount].Description;
                                        }
                                    }
                                    if (configuredRoomCategory != null &&
                                        !roomCategoryListforPopup.Contains(configuredRoomCategory))
                                        roomCategoryListforPopup.Add(configuredRoomCategory);
                                }
                            }

                            RoomTypeRateContainer roomTypeRateControl =
                                LoadControl("RoomTypeRateContainer.ascx") as RoomTypeRateContainer;

                            if (roomTypeRateControl != null)
                            {
                                roomTypeRateControl.SetRateDisplay(rateDisplay, defaultRoomTypesAvailable,
                                                                   showPricePerStay, showAllRoomTypes, roomNumber);
                                HtmlGenericControl dynDiv = new HtmlGenericControl("DIV");
                                dynDiv.ID = "blkRoomTableContainer" + roomNumber.ToString();
                                dynDiv.Controls.Add(roomTypeRateControl);
                                dynDiv.Attributes.Add("runat", "server");
                                if (!HygieneSessionWrapper.IsComboReservation)
                                {
                                    if (roomNumber == 0)
                                    {
                                        dynDiv.Attributes.Add("class", "tabCntnt default room" + (roomNumber + 1));
                                    }
                                    else
                                    {
                                        dynDiv.Attributes.Add("class", "tabCntnt room" + (roomNumber + 1));
                                    }
                                }
                                else
                                {
                                    dynDiv.Attributes.Add("class", "tabCntnt room" + (roomNumber + 1));
                                }


                                blkRoomDetailContainer.Controls.Add(dynDiv);
                            }
                        }
                        else
                        {
                            HtmlGenericControl dynDiv = new HtmlGenericControl("DIV");
                            dynDiv.ID = "blkRoomTableContainer" + roomNumber.ToString();
                            HtmlGenericControl dynLabel = new HtmlGenericControl("label");
                            dynLabel.Attributes.Add("class", "nonEditable");
                            dynLabel.InnerHtml =
                                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/NewNotModifiable");
                            dynDiv.Controls.Add(dynLabel);

                            dynDiv.Attributes.Add("class", "tabCntnt room" + (roomNumber + 1));

                            blkRoomDetailContainer.Controls.Add(dynDiv);
                        }
                    }
                    switch (hotelSearch.SearchingType)
                    {
                        case SearchType.CORPORATE:
                            {
                                if (!Utility.IsBlockCodeBooking)
                                {
                                    NegotiatedRoomRateDetails negotiatedRoomRate =
                                        roomRateDetails as NegotiatedRoomRateDetails;
                                    if ((null != negotiatedRoomRate)
                                        && !(negotiatedRoomRate.HasNegotiatedRates))
                                    {
                                        string errMsg = string.Empty;
                                        if (!string.IsNullOrEmpty(hotelSearch.CampaignCode))
                                            errMsg = string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/noroomratewithdnumber"), hotelSearch.CampaignCode);
                                        else
                                            errMsg = string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/noroomratewithdnumber"), "");

                                        DisplayErrorMessage(errMsg);
                                        break;
                                    }
                                }
                                else
                                {
                                    BlockCodeRoomRateDetails blockCodeRoomRate =
                                        roomRateDetails as BlockCodeRoomRateDetails;
                                    if ((blockCodeRoomRate != null) && (!blockCodeRoomRate.IsBlockRates))
                                    {
                                        if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                                        {
                                            SearchCriteriaSessionWrapper.SearchCriteria.IsBlockCodeRate = false;
                                        }
                                        string errMsg = string.Empty;
                                        if (!string.IsNullOrEmpty(hotelSearch.CampaignCode))
                                            errMsg = string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/noroomratewithdnumber"), hotelSearch.CampaignCode);
                                        else
                                            errMsg = string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/noroomratewithdnumber"), "");

                                        DisplayErrorMessage(errMsg);
                                    }
                                    else
                                    {
                                        if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                                        {
                                            SearchCriteriaSessionWrapper.SearchCriteria.IsBlockCodeRate = true;
                                        }
                                    }
                                }
                                break;
                            }
                        case SearchType.REGULAR:
                            {
                                if (null != hotelSearch.CampaignCode)
                                {
                                    PromotionRoomRateDetails promoRoomRate = roomRateDetails as PromotionRoomRateDetails;
                                    if (false == promoRoomRate.HasPromtionRates)
                                    {
                                    }
                                }
                                break;
                            }
                        case SearchType.VOUCHER:
                            {
                                VoucherRoomRateDetails voucherRoomRate = roomRateDetails as VoucherRoomRateDetails;
                                if (false == voucherRoomRate.HasPromtionRates)
                                {
                                }
                                break;
                            }
                    }
                }


                foreach (RoomCategoryEntity configuredRoomCategory in roomCategoryListforPopup)
                {
                    RoomDescriptionPopup roomDescriptionPopup =
                        LoadControl("RoomDescriptionPopup.ascx") as RoomDescriptionPopup;
                    roomDescriptionPopup.SetRoomCategoryDetails(configuredRoomCategory);
                    RoomCategoryContainer.Controls.Add(roomDescriptionPopup);
                }
            }
        }

        /// <summary>
        /// Remove SAVE type room rate details for Combo Reservation(SAVE + FLEX).
        /// </summary>
        /// <param name="listRoomRateDetails">The list room rate details.</param>
        private void RemoveSaveRateFromComboBooking(IList<BaseRoomRateDetails> listRoomRateDetails)
        {
            if (listRoomRateDetails.Count > 1)
            {
                for (int i = 0; i < listRoomRateDetails.Count; i++)
                {
                    for (int j = 0; j < listRoomRateDetails[i].RateCategories.Count; j++)
                    {
                        if (RoomRateUtil.IsSaveRateCategory(listRoomRateDetails[i].rateCategories[j].RateCategoryId))
                            listRoomRateDetails[i].rateCategories[j] = null;
                    }
                }
                
                DisplayErrorMessage(WebUtil.GetTranslatedText(
                                                "/bookingengine/booking/ModifySelectRate/NoSaveRateInComboRes"));
            }
        }

        /// <summary>
        /// Populates the room rate details for non modifiable rooms.
        /// </summary>
        /// <param name="listRoomRateDetails">The list room rate details.</param>
        private void PopulateRoomRateDetailsForNonModifiableRooms(IList<BaseRoomRateDetails> listRoomRateDetails)
        {
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            IList<HotelSearchRoomEntity> searchedRoomsList = hotelSearch.ListRooms;
            if ((searchedRoomsList != null) && (searchedRoomsList.Count > 0))
            {
                for (int i = 0; i < searchedRoomsList.Count; i++)
                {
                    if ((searchedRoomsList[i] != null) && (!searchedRoomsList[i].IsRoomModifiable) &&
                        (listRoomRateDetails.Count == searchedRoomsList.Count))
                    {
                        List<BookingDetailsEntity> currentActiveBookingDetailsList = BookingEngineSessionWrapper.ActiveBookingDetails;
                        if ((currentActiveBookingDetailsList != null) && (currentActiveBookingDetailsList.Count > 0))
                        {
                            if (currentActiveBookingDetailsList[i] != null && listRoomRateDetails[i] != null)
                            {
                                BookingDetailsEntity currentBookingDetailsEntity = currentActiveBookingDetailsList[i];
                                BaseRoomRateDetails currentBaseRoomRateDetails = listRoomRateDetails[i];
                                RateCategory rateCategory =
                                    RoomRateUtil.GetRateCategoryByRatePlanCode(
                                        currentBookingDetailsEntity.HotelRoomRate.RatePlanCode);
                                if (rateCategory != null)
                                    currentBaseRoomRateDetails.RateCategories.Add(rateCategory);

                                RoomRateEntity roomRateEntity =
                                    new RoomRateEntity(currentBookingDetailsEntity.HotelRoomRate.RoomtypeCode,
                                                       currentBookingDetailsEntity.HotelRoomRate.RatePlanCode);
                                RoomCategoryEntity roomCategoryEntity = new RoomCategoryEntity();
                                List<RoomRateEntity> roomRateEntityList = new List<RoomRateEntity>();
                                roomRateEntityList.Add(roomRateEntity);
                                roomCategoryEntity.RateCategories.Add(
                                    currentBookingDetailsEntity.HotelRoomRate.RatePlanCode, roomRateEntityList);
                                roomCategoryEntity.RoomTypes.Add(currentBookingDetailsEntity.HotelRoomRate.RoomtypeCode);
                                currentBaseRoomRateDetails.RateRoomCategories.Add(roomCategoryEntity);
                            }
                        }
                    }
                }
            }
        }

        #region Alternate Hotels

        #region Alternate Hotels

        /// <summary>
        /// Display Alternate Hotels
        /// </summary>
        /// <param name="hotelDestination">
        /// HotelDestination
        /// </param>
        private void DisplayAlternateHotels(HotelDestination hotelDestination)
        {
            SearchedHotelCityNameSessionWrapper.SearchedHotelCityName = hotelDestination.HotelAddress.City;

            string redirectURL = RoomRateDisplayUtil.GetAlternateHotels(hotelDestination);
            if ((redirectURL != null) && (redirectURL != string.Empty))
            {
                Response.Redirect(redirectURL, false);
            }
        }

        #endregion Alternate Hotels

        /// <summary>
        /// OnSelectRoomRate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnSelectRoomRate(object sender, EventArgs e)
        {
            string roomCategoryID = txtRoomCategoryID.Value;
            string rateCategory = txtRateCategory.Value;
            string roomNumber = txtRoomNumber.Value;
            operaErrorMsg.InnerText = string.Empty;
            SelectedRoomAndRateEntity selectedRoomRate = SelectRoomUtil.SetRoomAndRateSelection(
                txtRoomCategoryID.Value, txtRateCategory.Value, txtRoomNumber.Value);
            string errorMsg = SelectRoomUtil.BlockRoom(selectedRoomRate, roomNumber);
            int selectedRoomIndex = -1;
            int.TryParse(roomNumber, out selectedRoomIndex);
            if (!string.IsNullOrEmpty(errorMsg) || !string.IsNullOrEmpty(operaErrorMsg.InnerText))
            {
                errorDiv.Visible = true;
                if (operaErrorMsg.InnerText.Contains(errorMsg))
                {
                    operaErrorMsg.InnerHtml = errorMsg;
                }
                else
                {
                    operaErrorMsg.InnerHtml += errorMsg;
                }
                Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                if (selectedRoomAndRatesHashTable != null &&
                    selectedRoomAndRatesHashTable.ContainsKey(selectedRoomIndex))
                {
                    selectedRoomAndRatesHashTable.Remove(selectedRoomIndex);
                }
            }
            else
            {
                errorDiv.Visible = false;
            }
            txtRoomCategoryID.Value = string.Empty;
            txtRateCategory.Value = string.Empty;
            txtRoomNumber.Value = string.Empty;
            string relativeUrl = string.Empty;
            relativeUrl = GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE);
            string urlToRedirect = WebUtil.GetSecureUrl(relativeUrl);
            AppLogger.LogInfoMessage(urlToRedirect);
        }


        /// <summary>
        /// Hide the Main Body
        /// </summary>
        private void HideMainBody()
        {
            divSelectRateMainBody.Visible = false;
        }


        /// <summary>
        /// Displays the error message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void DisplayErrorMessage(string message)
        {
            infoErrorDiv.Visible = true;
            errorLabel.InnerText = message;
        }

        /// <summary>
        /// Creates the availability calender.
        /// </summary>
        /// <param name="search">The search.</param>
        public void CreateAvailabilityCalender(HotelSearchEntity search)
        {
            int selectedItemIndex = 0;

            AvailabilityCalendarEntity availabilityCalendar = Reservation2SessionWrapper.AvailabilityCalendar;
            if (availabilityCalendar == null)
            {
                availabilityCalendar = SetAvailCalendarEntities(search);
            }

            AvailabilityController availController = new AvailabilityController();
            availController.MultiroomMultidateCalendarAvailSearch(search, availabilityCalendar);
        }

        /// <summary>
        /// Sets the avail calendar entities.
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns>AvailabilityCalendarEntity</returns>
        private AvailabilityCalendarEntity SetAvailCalendarEntities(HotelSearchEntity search)
        {
            AvailabilityCalendarEntity availabilityCalendar = null;

            if (search != null)
            {
                availabilityCalendar = Reservation2SessionWrapper.AvailabilityCalendar;
                if (availabilityCalendar == null)
                {
                    availabilityCalendar = new AvailabilityCalendarEntity();
                    availabilityCalendar.LeftCarouselIndex = 0;
                    availabilityCalendar.RightCarouselIndex = 0;
                    Reservation2SessionWrapper.AvailabilityCalendar = availabilityCalendar;
                    leftVisibleCarouselIndex.Value = "0";
                    Reservation2SessionWrapper.LeftVisibleCarouselIndex = 0;
                }

                availabilityCalendar.SearchState = SearchState.NONE;
                availabilityCalendar.ListAvailCalendarItems = new List<AvailCalendarItemEntity>();
                DateTime arrivalDateLimit = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                DateTime departureDateLimit = DateTime.Today.AddYears(AppConstants.NO_OF_YEARS_IN_AVAILCALENDAR);

                DateTime iteratorArrivalDate = search.ArrivalDate.AddDays(-(double)(AppConstants.CAROUSEL_COUNT / 2));
                DateTime iteratorDepartureDate = search.DepartureDate.AddDays(-(double)(AppConstants.CAROUSEL_COUNT / 2));
                leftVisibleCarouselIndex.Value = AppConstants.DEFAULT_LEFT_VISIBLECAROUSAL_INDEX.ToString();
                Reservation2SessionWrapper.LeftVisibleCarouselIndex = AppConstants.DEFAULT_LEFT_VISIBLECAROUSAL_INDEX;

                if (iteratorArrivalDate < arrivalDateLimit)
                {
                    iteratorArrivalDate = arrivalDateLimit;
                    iteratorDepartureDate = iteratorArrivalDate.AddDays(search.NoOfNights);
                    leftVisibleCarouselIndex.Value = "0";
                    Reservation2SessionWrapper.LeftVisibleCarouselIndex = 0;
                    HtmlInputHidden firstBatch = this.Parent.FindControl("firstBatch") as HtmlInputHidden;
                    if (firstBatch != null)
                    {
                        firstBatch.Value = "true";
                    }
                    Reservation2SessionWrapper.IsCalendarFirstBatch = true;
                }

                for (int iterator = 0;
                     iterator < AppConstants.CAROUSEL_COUNT;
                     iterator++, availabilityCalendar.RightCarouselIndex++)
                {
                    if (search.ArrivalDate.Equals(iteratorArrivalDate))
                    {
                        selectedCalendarItemIndex.Value = Convert.ToString(iterator);
                        Reservation2SessionWrapper.SelectedCalendarItemIndex = iterator;
                        int step = iterator / AppConstants.VISIBLE_CAROUSEL_COUNT;
                        leftVisibleCarouselIndex.Value = Convert.ToString(step * AppConstants.VISIBLE_CAROUSEL_COUNT);
                        Reservation2SessionWrapper.LeftVisibleCarouselIndex =
                            Convert.ToInt32(step * AppConstants.VISIBLE_CAROUSEL_COUNT);
                    }
                    if (iteratorArrivalDate >= departureDateLimit)
                        break;
                    AvailCalendarItemEntity calendarItem = new AvailCalendarItemEntity();
                    calendarItem.ArrivalDate = iteratorArrivalDate;
                    calendarItem.DepartureDate = iteratorDepartureDate;
                    availabilityCalendar.ListAvailCalendarItems.Add(calendarItem);

                    iteratorArrivalDate = iteratorArrivalDate.AddDays(AppConstants.CALENDARITEM_GAP_DAYS);
                    iteratorDepartureDate = iteratorDepartureDate.AddDays(AppConstants.CALENDARITEM_GAP_DAYS);
                }
                availabilityCalendar.RightCarouselIndex--;
            }
            return availabilityCalendar;
        }

        #endregion Private Methods
    }
}