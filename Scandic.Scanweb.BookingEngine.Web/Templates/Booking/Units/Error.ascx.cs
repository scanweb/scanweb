using System;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Error
    /// </summary>
    public partial class Error : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                errMsgContent.InnerHtml = WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR);
            }
        }
    }
}