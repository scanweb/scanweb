#region System Namespaces

using System;
using System.Collections;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Entity;

#endregion

    #region Scandic Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class of Booking Confirmation. It displays
    /// the Confirmation details
    /// </summary>
    public partial class BookingConfirmation : EPiServer.UserControlBase
    {
        private bool isSessionValid = true;

        #region Protected Methods

        /// <summary>
        /// Page Load Method of Booking Confirmation Control
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="e">
        /// Arguments for the Event
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("BookingConfirmationASCX:Page_Load() StartTime:::" +
                                                          DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
            ProgressBar.SetProgressBarSelection(EpiServerPageConstants.BOOKING_CONFIRMATION_PAGE);

            if (!IsPostBack)
            {
                if (this.Visible)
                {
                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
                    Hashtable SelectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;

                    if (hotelSearch != null)
                    {
                        SortedList<string, GuestInformationEntity> guestsList =
                            GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
                        if (guestsList != null)
                        {
                            GuestInformation.PopulateUI(guestsList, SelectedCardTypeSessionWrapper.SelectedCardType);
                        }
                        DisplayErrorMessages();
                    }
                    else
                    {
                        if (BookingEngineSessionWrapper.IsModifyBooking)
                        {
                            Response.Redirect(
                                GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH));
                        }
                        else
                        {
                            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE));
                        }
                    }
                }
            }
            if (Request.QueryString["command"] != null)
            {
                if (Request.QueryString["command"].ToString() == "print")
                {
                    progressBarPlaceHolder.Visible = false;
                    clientErrorDivBC.Visible = false;
                    invalidFGPBox.Visible = false;
                    prntWrapper.Attributes.Add("class", "mgnLft20 prntWrapper");
                }
                if (Request.QueryString["isPDF"] != null && Request.QueryString["isPDF"].ToString() == "true")
                {
                    scandicLogo.Visible = true;
                    progressBarPlaceHolder.Visible = false;
                    clientErrorDivBC.Visible = false;
                    invalidFGPBox.Visible = false;
                    prntWrapper.Attributes.Add("class", "mgnLft20 prntWrapper");
                }
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("BookingConfirmationASCX:Page_Load() EndTime:::" +
                                                          DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Display Error message if any.
        /// </summary>
        private void DisplayErrorMessages()
        {
            List<string> listofFailedEmailRecepient = ErrorsSessionWrapper.FailedEmailRecipient;
            if (listofFailedEmailRecepient != null && listofFailedEmailRecepient.Count > 0)
            {
                int totalCounts = listofFailedEmailRecepient.Count;

                clientErrorDivBC.InnerHtml = string.Empty;
                operaErrorMsg.InnerHtml = string.Empty;

                for (int count = 0; count < totalCounts; count++)
                {
                    string failedEmailRecipient = listofFailedEmailRecepient[count];
                    if (!string.IsNullOrEmpty(failedEmailRecipient))
                    {
                        if (string.IsNullOrEmpty(operaErrorMsg.InnerHtml))
                        {
                            operaErrorMsg.InnerHtml +=
                                WebUtil.GetTranslatedText(
                                    TranslatedTextConstansts.BOOKING_CONFIRMATION_FAILED_EMAIL_RECIPIENT) +
                                " <br />" + failedEmailRecipient + " <br />";
                        }
                        else
                        {
                            operaErrorMsg.InnerHtml += failedEmailRecipient + " <br />";
                        }
                    }
                }
            }

            List<string> listOffailedSMSRecepient = ErrorsSessionWrapper.FailedSMSRecepient;
            if (listOffailedSMSRecepient != null && listOffailedSMSRecepient.Count > 0)
            {
                int totalFails = listOffailedSMSRecepient.Count;
                for (int count = 0; count < totalFails; count++)
                {
                    string failedSMSRecepient = listOffailedSMSRecepient[count];
                    if (!string.IsNullOrEmpty(failedSMSRecepient))
                    {
                        if (string.IsNullOrEmpty(operaErrorMsg.InnerHtml))
                        {
                            operaErrorMsg.InnerHtml += " <br />";
                            operaErrorMsg.InnerHtml +=
                                WebUtil.GetTranslatedText(
                                    TranslatedTextConstansts.BOOKING_CONFIRMATION_FAILED_SMS_RECIPIENT) +
                                " <br />" + failedSMSRecepient + " <br />";
                        }
                        else
                        {
                            operaErrorMsg.InnerHtml += failedSMSRecepient + " <br />";
                            ;
                        }
                    }
                }
            }

            SortedList<string, GuestInformationEntity> slGuestList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
            if (slGuestList != null)
            {
                string invalidFGPNumbers = string.Empty;
                string relMembershipText =
                    WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/accountnumber");
                foreach (GuestInformationEntity guest in slGuestList.Values)
                {
                    if (!guest.IsRewardNightGift)
                    {
                        if (!guest.IsValidFGPNumber && guest.GuestAccountNumber != relMembershipText)
                        {
                            invalidFGPNumbers += invalidFGPNumbers != string.Empty
                                                     ? ", " + (guest.GuestAccountNumber + "/" + guest.LastName)
                                                     : (guest.GuestAccountNumber + "/" + guest.LastName);
                        }
                    }
                }
                if (invalidFGPNumbers != string.Empty)
                {
                    lblInvalidFGPNum.InnerText =
                        WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/InvalidFGPLastName") + " " +
                        invalidFGPNumbers;
                    invalidFGPBox.Visible = true;
                }
            }
            if (string.IsNullOrEmpty(operaErrorMsg.InnerHtml))
            {
                clientErrorDivBC.Visible = false;
            }
            else
            {
                clientErrorDivBC.Visible = true;
            }
        }
        #endregion

    }
}