<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserBookingContactDetails.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.UserBookingContactDetails" %>
<%@ Register Src="~/Templates/Booking/Units/BookingDetailLogin.ascx" TagName="LoginControl"
    TagPrefix="Booking" %>
<%--<%@ Register TagPrefix="Scanweb" TagName="ProgressBar" Src="~/Templates/Booking/Units/ProgressBar.ascx" %>--%>
<%@ Register TagPrefix="Scanweb" TagName="RoomInformation" Src="~/Templates/Booking/Units/RoomInformationContainer.ascx" %>
<%@ Register Src="~/Templates/Booking/Units/RoomDescriptionPopup.ascx" TagName="PopupRoomDescription"
    TagPrefix="Booking" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%@ Import Namespace="Scandic.Scanweb.Entity" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<%--RK: Reservation 2.0 | Moved the progress bar to modify boking & booking details containers--%>
<%--<Scanweb:ProgressBar id="ProgressBar" runat="server" />--%>
<input type="hidden" id="noOfRoomReserVationContainer" runat="server" value="" />
<div class="BE">
    <div id="Header">
        <h2 class="borderH2">
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SecureCheckOut")%>
        </h2>
        <br class="clear" />
    </div>
    <!-- Progress Tab -->
    <div id="BookingDetailsLogin" runat="server">
        <!-- R1.3 | CR5 | StoryId: | Login on Booking Detail Screen. -->
        <Booking:LoginControl ID="BookingDetailsLoginControl" runat="server">
        </Booking:LoginControl>
    </div>
    <div id="divBookForSomeOneElse" runat="server" class="rewardTitle">
        <label>
            <input type="checkbox" id="chkBookForSomeoneElse" tabindex="11" style="border: none;"
                runat="server" onclick="setBookForSomeOneElseStatus();" />
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/giftrewardnight")%>
        </label>
    </div>
    <!-- Step3: Enter Booking Details -->
    <!-- Details -->
    <div id="GuestInfo" runat="server" onkeypress="return WebForm_FireDefaultButton(event, '<%= lnkBtnBook.ClientID %>')">
        <div class="infoAlertBox" id="divInfoMessage" runat="server" visible="False">
            <div class="threeCol alertContainer">
                <div class="hd sprite">
                    &nbsp;</div>
                <div class="cnt sprite">
                    <p id="alert2" class="scansprite">
                        <label id="lblInfoMessage" runat="server">
                        </label>
                    </p>
                </div>
                <div class="ft sprite">
                </div>
            </div>
        </div>
        <div id="UpdateGuestInfo" runat="server">
            <!-- R1.3 | CR5 | StoryId: | Login on Booking Detail Screen.            
      	       -The below checkbox will be visile if user is logged in.
               -Enabling the checkbox allows the user to modify his/her profile.
            -->
            <p class="formRow modifyProfileChk">
                <label>
                    <input type="checkBox" class="ignore" tabindex="12" name="chkModifyProfile" id="chkModifyProfile"
                        runat="server" maxlength="30" onclick="UpdateGuestInfo();" />
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/ModifyProfileInfo")%>
                </label>
            </p>
        </div>
        <!-- BEGIN New Opera Message -->
        <div class="clearAll">
            <!-- Clearing Float -->
        </div>
        <div id="clientErrorDivBD" class="infoAlertBox infoAlertBoxRed" runat="server" visible="false">
            <div class="threeCol alertContainer">
                <div class="hd sprite">
                    &#160;</div>
                <div class="cnt sprite">
                    <span class="alertIconRed">&#160;</span>
                    <div id="Div1" class="descContainer" runat="server">
                        <label class="titleRed">
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/Rate48HRS/PleaseNote")%></label>
                        <label id="operaErrorMsg" runat="server" class="infoAlertBoxDesc">
                        </label>
                    </div>
                    <div class="clearAll">
                        &#160;</div>
                </div>
                <div class="ft sprite">
                    &#160;</div>
            </div>
            <!-- END New Opera Message -->
        </div>
        <!-- Error -->
        <div id="clientErrorDivBookingDetail" runat="server">
        </div>
        <div class="errorDivClient">
            <input type="hidden" id="invalidDNumber" name="invalidDNumber" value="/scanweb/bookingengine/errormessages/businesserror/invalidDNumber" />
            <input type="hidden" id="addressID" name="addressID" runat="server" />
            <input type="hidden" id="transactionCount" name="transactionCount" runat="server" />
            <input type="hidden" id="previousCardNumber" name="orginalCardNumber" runat="server" />
            <input type="hidden" id="isModifyBooking" name="isModifyBooking" runat="server" />
            <input type="hidden" id="UserLoggedIn" name="UserLoggedIn" runat="server" />
            <input type="hidden" id="nameID" name="nameID" runat="server" />
            <input type="hidden" id="homePhoneOperaID" name="homePhoneOperaID" runat="server" />
            <input type="hidden" id="div1LegNumber" name="div1LegNumber" runat="server" />
            <input type="hidden" id="div2LegNumber" name="div2LegNumber" runat="server" />
            <input type="hidden" id="div3LegNumber" name="div3LegNumber" runat="server" />
            <input type="hidden" id="div4LegNumber" name="div4LegNumber" runat="server" />
            <input type="hidden" id="divMainLegNumber" name="divMainLegNumber" runat="server" />
            <input type="hidden" id="partnerProgramValues" name="partnerProgramValues" runat="server" />
            <input type="hidden" id="selectedPartnerProgramValues" name="selectedPartnerProgramValues"
                runat="server" />
            <input type="hidden" id="uniquePartnerProg" name="uniquePartnerProg" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/uniquePartnerProg")%>" />
            <input type="hidden" id="choosePreferred" name="choosePreferred" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/choosePreferred")%>" />
            <input type="hidden" id="prefPartnerText" name="prefPartnerText" value="<%=WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/preferredPartnerProgramme")%>" />
            <input type="hidden" id="optPartnerText" name="optPartnerText" value="<%=WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/optionalPartnerProgram")%>" />
            <input type="hidden" id="prefPartnerCount" name="prefPartnerCount" runat="server" />
            <!--START: TRANSLATION TEXT FOR VALIDATION -->
            <input type="hidden" id="requiredFld" name="requiredFld" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/requiredFld")%>" />
            <input type="hidden" id="requiredFldfirstname" name="requiredFldfirstname" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/requiredFldfirstname")%>" />
            <input type="hidden" id="requiredFldlastname" name="requiredFldlastname" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/requiredFldlastname")%>" />
            <input type="hidden" id="emailmessage" name="emailmessage" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/emailmessage")%>" />
            <input type="hidden" id="invalidemailmessage" name="invalidemailmessage" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/invalidemailmessage")%>" />
            <input type="hidden" id="cntrymessage" name="cntrymessage" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/cntrymessage")%>" />
            <input type="hidden" id="cntrycodemessage" name="cntrycodemessage" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/cntrycodemessage")%>" />
            <input type="hidden" id="phonemessage" name="phonemessage" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/phonemessage")%>" />
            <input type="hidden" id="bedprefmessage" name="bedprefmessage" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/bedprefmessage")%>" />
            <input type="hidden" id="creditname" name="creditname" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/creditname")%>" />
            <input type="hidden" id="creditnumber" name="creditnumber" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/creditnumber")%>" />
            <input type="hidden" id="cccardtype" name="cccardtype" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/cccardtype")%>" />
            <input type="hidden" id="validcreditcard" name="validcreditcard" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/validcreditcard")%>" />
            <input type="hidden" id="prefdlang" name="prefdlang" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/prefdlang")%>" />
            <input type="hidden" id="passwordmessage" name="passwordmessage" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/passwordmessage")%>" />
            <input type="hidden" id="samepassword" name="samepassword" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/samepassword")%>" />
            <input type="hidden" id="sercurityquestion" name="sercurityquestion" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/sercurityquestion")%>" />
            <input type="hidden" id="secanswer" name="secanswer" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/secanswer")%>" />
            <input type="hidden" id="plconfirm" name="plconfirm" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/plconfirm")%>" />
            <input type="hidden" id="plagree" name="plagree" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/plagree")%>" />
            <input type="hidden" id="ccmonthmessage" name="ccmonthmessage" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/ccmonthmessage")%>" />
            <input type="hidden" id="ccyearmessage" name="ccyearmessage" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/ccyearmessage")%>" />
            <input type="hidden" id="encity" name="encity" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/encity")%>" />
            <input type="hidden" id="valpostal" name="valpostal" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/valpostal")%>" />
            <input type="hidden" id="valaddress" name="valaddress" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/valaddress")%>" />
            <input type="hidden" id="invalidfgp" name="invalidfgp" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/invalidfgp")%>" />
            <input type="hidden" id="emailvarification" name="emailvarification" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/emailvarification")%>" />
            <input type="hidden" id="CheckboxMultiple" name="CheckboxMultiple" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/CheckboxMultiple")%>" />
            <input type="hidden" id="CheckboxRd" name="CheckboxRd" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/CheckboxRd")%>" />
            <input type="hidden" id="maxCheckbox" name="maxCheckbox" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/maxCheckbox")%>" />
            <input type="hidden" id="minCheckbox" name="minCheckbox" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/minCheckbox")%>" />
            <input type="hidden" id="confirm" name="confirm" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/confirm")%>" />
            <input type="hidden" id="telephone" name="telephone" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/telephone")%>" />
            <input type="hidden" id="email" name="email" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/email")%>" />
            <input type="hidden" id="date" name="date" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/date")%>" />
            <input type="hidden" id="onlyNumber" name="onlyNumber" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/onlyNumber")%>" />
            <input type="hidden" id="noSpecialCaracters" name="noSpecialCaracters" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/noSpecialCaracters")%>" />
            <input type="hidden" id="onlyLetter" name="onlyLetter" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/onlyLetter")%>" />
            <input type="hidden" id="lengthNone" name="lengthNone" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/lengthNone")%>" />
            <input type="hidden" id="lengthBetween" name="lengthBetween" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/lengthBetween")%>" />
            <input type="hidden" id="lengthAnd" name="lengthAnd" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/lengthAnd")%>" />
            <input type="hidden" id="lengthChar" name="lengthChar" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/lengthChar")%>" />
            <input type="hidden" id="fillThis" name="fillThis" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/fillThis")%>" />
            <input type="hidden" id="dontMatch" name="dontMatch" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/dontMatch")%>" />
            <input type="hidden" id="membershipNumberText" name="membershipNumberText" value="<%=WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/accountnumber")%>" />
            <input type="hidden" id="sameGuestNames" name="sameGuestNames" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/sameguestname")%>" />
            <input type="hidden" id="restrictSplChars" name="restrictSplChars" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/noSpecialCaracters")%>" />
            <input type="hidden" id="emailwillbesent" name="emailwillbesent" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/emailwillbesent")%>" />
            <input type="hidden" id="panhashccvalidatationmsg" name="panhashccvalidatationmsg"
                value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/panhashcreditcard")%>" />
            <!--END: TRANSLATION TEXT FOR VALIDATION -->
            <!--Hidden Field For filling Booking For Some One else Old Values Ashish-->
            <input type="hidden" id="hdnFName" runat="server" />
            <input type="hidden" id="hdnLName" runat="server" />
            <input type="hidden" id="hdnEmail" runat="server" />
            <input type="hidden" id="hdnConfEmail" runat="server" />
            <input type="hidden" id="hdnPhCode" runat="server" />
            <input type="hidden" id="hdnPhoneNumber" runat="server" />
            <input type="hidden" id="hdnCity" runat="server" />
            <input type="hidden" id="hdnCountry" runat="server" />
            <!--End Hidden Field For filling Booking For Some One else Old Values Ashish-->
        </div>
        <!-- Error -->
        <!-- Comment for Room One -->
        <div class="bookingDetailWrapper fltLft">
            <!-- Start of room1 -->
            <div id="divRoom0" runat="server">
                <Scanweb:RoomInformation ID="RoomInformation0" visible="false" runat="server">
                </Scanweb:RoomInformation>
            </div>
            <!-- End of room1 -->
            <!-- Start of room2 -->
            <div id="divRoom1" runat="server" class="mrgTop19">
                <Scanweb:RoomInformation ID="RoomInformation1" visible="false" runat="server">
                </Scanweb:RoomInformation>
                <!-- End of room2 -->
                <!-- Start of room3 -->
                <div id="divRoom2" runat="server" visible="false" class="mrgTop19">
                    <Scanweb:RoomInformation ID="RoomInformation2" visible="false" runat="server">
                    </Scanweb:RoomInformation>
                </div>
                <!-- End of room3 -->
                <!-- Start of room4 -->
                <div id="divRoom3" runat="server" visible="false" class="mrgTop19">
                    <Scanweb:RoomInformation ID="RoomInformation3" visible="false" runat="server">
                    </Scanweb:RoomInformation>
                </div>
                <!-- Start of room4 -->
                <div class="depositInfo reterm fltLft mrgTop19 depositInfoMultiRoom" id="HideUnhideGuranteeDivCommon"
                    runat="server" visible="false">
                    <h3>
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteedeposite")%></h3>
                    <asp:Label ID="guaranteeDepositAppendForComboReservation" runat="server"></asp:Label>
                    <p class="gType fieldOption">
                        <label id="blkHoldRoomCommon" runat="server" visible="false">
                           <%-- <input type="radio" id="rdoHoldRoomCommon" runat="server" checked="true" name="depinfo4"
                                value="holdOn" tabindex="300" class="radio chkEnroll" />--%>
                            <span id="spanHoldRoomCommon" runat="server"></span>
                            <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/gaurenteediv")%>--%>
                            <%--<a href="#" title="Hold my booking until 6PM on the day of check in" class="help spriteIcon toolTipMe"></a>--%>
                        </label>
                    </p>
                    <p class="gType fieldOption">
                        <label id="lblGuranteewithRadioCommon" runat="server">
                            <input type="checkbox" name="depinfo4" value="creditInfo" tabindex="301" class="radio chkEnroll"
                                id="rdoLateArrivalGuranteeCommon" runat="server" />
                            <span id="txtGuranteewithRadioCommon" class="lblGuarantee" runat="server"></span>
                        </label>
                        <label id="lblGuranteewithoutRadioCommon" visible="false" runat="server">
                            <span id="txtGuranteeWithoutRadioCommon" runat="server"></span>
                        </label>
                        <%-- <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/gaurenteeCreditCard")%>--%>
                        <%--<a href="#" title="Guarantee my booking" class="help spriteIcon toolTipMe"></a>--%>
                        <asp:HiddenField runat="server" id="hid6PMBooking" ></asp:HiddenField>
                    </p>
                    <div class="ccInfoDetails fltLft" id="HideUnhideCreditCardDivCommon" runat="server">
                        <label id="lblGuranteewithRadioCCDivCommonInfo" visible = "true" runat="server">
                            <span><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/cancellationexpand") %></span> <br/><br/>
                        </label>
                        <div class="ccInfo">
                            <div class="formFieldCol">
                                <label class="formLabel" for="<%=txtCardHolderCommon.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/NameOnCard")%>
                                    <span class="importantMark">&#42;</span></label>
                                <input type="text" class="defaultColor " name="cc_name" tabindex="302" id="txtCardHolderCommon"
                                    runat="server" />
                            </div>
                            <div class="formFieldCol">
                                <label class="formLabel" for="<%=ddlCardTypeCommon.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/cardtype")%>
                                    <span class="importantMark">&#42;</span></label>
                                <asp:DropDownList ID="ddlCardTypeCommon" runat="server" TabIndex="303">
                                </asp:DropDownList>
                            </div>
                            <div class="formFieldCol">
                                <label class="formLabel" for="<%=txtCardNumberCommon.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/NumberOnCard") %>
                                    <span class="importantMark">&#42;</span></label>
                                <input type="text" class="defaultColor" tabindex="304" name="cc_num" id="txtCardNumberCommon"
                                    maxlength="20" runat="server" />
                            </div>
                        </div>
                        <div class="ccDate fltLft">
                            <div class="formFld fltLft">
                                <label class="fltLft formLabel" for="<%=ddlExpiryMonthCommon.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/CardExpDate")%>
                                    <span class="importantMark">&#42;</span>
                                </label>
                                <span class="formFieldCol">
                                    <asp:DropDownList ID="ddlExpiryMonthCommon" runat="server" TabIndex="305">
                                    </asp:DropDownList>
                                </span><span class="formFieldCol">
                                    <asp:DropDownList ID="ddlExpiryYearCommon" runat="server" TabIndex="306">
                                    </asp:DropDownList>
                                </span>
                            </div>
                            <img height="57" width="114" alt="" class="fltRt ccimgclas" src="<%=WebUtil.GetLogoPath()%>" />
                        </div>
                        <div class="clearAll">
                            <!-- Clearing Float -->
                        </div>
                        <!-- R2.2.5 Credit Card Info -->
                        <div class="ccExtraInfoWrapper">
                            <div class="ccExtraInfo">
                                <div class="content">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Dankort")%></div>
                            </div>
                        </div>
                    </div>
                    <div class="ccInfoDetails fltLft" id="panHashCreditCardDivCommon" runat="server">
                        <div class="ccInfo">
                            <div class="formFld fltLft">
                                <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/PanhashCCLabel") %>
                                <span class="formFieldCol">
                                    <asp:DropDownList ID="ddlPanHashCreditCardsCommon" TabIndex="307" runat="server"
                                        CssClass="panHashDropDown">
                                    </asp:DropDownList>
                                </span>
                            </div>
                            <img height="57" width="114" alt="" class="fltRt ccimgclas" src="<%=WebUtil.GetLogoPath()%>" />
                        </div>
                        <div class="clearAll">
                        </div>
                        <div class="ccExtraInfoWrapper">
                            <div class="ccExtraInfo">
                                <div class="content">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Dankort")%></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bknewcoment ie7bknewcoment ie8width reterm paymentMethod" id="divSaveRateCodePayment"
                    runat="server" visible="false">
                    <div class="reterm fltLft">
                        <h3>
                            <strong>
                                <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/paymentmethod")%></strong>
                        </h3>
                    </div>
                    <asp:RadioButtonList TabIndex="308" runat="server" ID="rdoListFGPSavedCreditCards" />
                </div>
                <!--Scandic�s Frequent Guest Programme-->
                <div class="FGP reterm fltLft mrgTop19 ie8width" id="lblFGPDiv" runat="server" style="display: block;">
                    <h3>
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/ScandicGstPrg")%>
                    </h3>
                    <hr />
                    <div class="dyn">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/DidYouKnow")%>
                    </div>
                    <p>
                        <input type="checkbox" name="chkBox" id="lblChkEnroll" runat="server" class="chkEnroll chkBx"
                            value="enroll" tabindex="400" />
                        <strong>
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPEnroll")%>
                        </strong><span style="color: #CCCCCC;"><strong>
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPInfoNeed")%>
                        </strong></span>
                    </p>
                    <div class="essenInfo" style="display: none">
                        <div class="fltLft" style="width:100%;">
                            <p>
                                <label class="formLabel" for="<%=txtEnFName.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPFName")%>
                                    <span class="importantMark">&#42;</span></label>
                                <input type="text" name="street" id="txtEnFName" maxlength="30" runat="server" tabindex="403"
                                    class="defaultColor" />
                            </p>
                            <p>
                                <label class="formLabel" for="<%=txtEnLName.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPLName")%>
                                    <span class="importantMark">&#42;</span></label>
                                <input type="text" name="street2" id="txtEnLName" maxlength="30" runat="server" tabindex="405"
                                    class="defaultColor" />
                            </p>
                            <p>
                                <label class="formLabel" for="<%=txtEnAddressLine1.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPAddress1")%>
                                    <span class="importantMark">&#42;</span></label>
                                <input type="text" name="adress" id="txtEnAddressLine1" class="defaultColor" runat="server"
                                    tabindex="409" /></p>
                                    <p>
                                <label class="formLabel" for="<%=txtEnAddressLine2.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPAddress2")%></label>
                                <input type="text" name="adress2" id="txtEnAddressLine2" runat="server" tabindex="411"
                                    class="defaultColor" /></p>
                           <p>
                                <label class="formLabel" for="<%=txtEnCityOrTown.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/townorcity")%>
                                    <span class="importantMark">&#42;</span></label>
                                <input type="text" name="city2" id="txtEnCityOrTown" runat="server" tabindex="414"
                                    class="defaultColor" /></p>
                            <p>
                                <label class="formLabel" for="<%=txtEnPostCode.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/postcode")%>
                                    <span class="importantMark">&#42;</span></label>
                                <input type="text" name="postcode" id="txtEnPostCode" class="defaultColor" runat="server"
                                    tabindex="417" /></p>
                            <p>
                                <label class="formLabel" for="<%=ddlEnCountry.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/countryname")%>
                                    <span class="importantMark">&#42;</span></label>
                                <asp:DropDownList ID="ddlEnCountry" runat="server" TabIndex="421" CssClass="countryDropdown">
                                </asp:DropDownList>
                            </p>
                            <p>
                                <label class="formLabel" for="<%=txtEnEmail.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPEmail")%>
                                    <span class="importantMark">&#42;</span></label>
                                <input type="text" name="email" id="txtEnEmail" runat="server" tabindex="423" class="defaultColor" />
                            </p>
                        </div>

                        <div class="clearAll">
                            &#160;</div>
                        <div class="row1 fltLft" style="width: 100%; margin-bottom:15px">
                            <p>
                               
                                    <label class="formLabel" for="<%=ddlEnTelephoneCode.ClientID %>">
                                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/CountryCode")%>
                                        <span class="importantMark">&#42;</span></label>
                                    <asp:DropDownList ID="ddlEnTelephoneCode" runat="server" TabIndex="425" CssClass="contryselect">
                                    </asp:DropDownList>
                              </p>
                              <p>
                                    <label class="formLabel" for="<%=txtEnTelephone.ClientID %>">
                                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/PrimaryPhNo")%>
                                    </label>
                                    <input type="text" tabindex="428" name="phno" class="defaultColor telNumber" id="txtEnTelephone"
                                        maxlength="20" runat="server" />
                                
                            </p>
                            <p tip="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/CheckboxMultiple") %>"
                                class="clear">
                                <label class="formLabel" for="">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/GenderOption")%></label>
                                <input type="radio" class="radio selectGender" id="rdoEnMale" runat="server" tabindex="439"
                                    name="gender" />&nbsp;<%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/male")%>&nbsp;&nbsp;
                                <input type="radio" name="gender" id="rdoEnFemale" runat="server" tabindex="440"
                                    class="radio selectGender" />&nbsp;<%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/female")%>
                            </p>
                            <p class="clear bday">
                                <label class="formLabel" for="<%=ddlEnDOBDay.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/updateprofile/dateofbirth")%>
                                    <span class="importantMark">&#42;</span></label>
                               <div class="clear">
                                <div class="dobselect">
                                    <asp:DropDownList ID="ddlEnDOBDay" runat="server" TabIndex="441" CssClass="">
                                    </asp:DropDownList>
                                </div>
                                &nbsp;&nbsp;
                                <div class="dobselect1">
                                    <asp:DropDownList ID="ddlEnDOBMonth" TabIndex="442" runat="server" CssClass="">
                                    </asp:DropDownList>
                                </div>
                                &nbsp;&nbsp;
                                <div class="dobselect1">
                                    <asp:DropDownList ID="ddlEnDOBYear" TabIndex="443" runat="server" CssClass="">
                                    </asp:DropDownList>
                                </div>
                                </div>
                            </p>

                        </div>
                        <div class="clear fltLft">
                            <p>
                                <label class="formLabel" for="<%=ddlEnPreferredLang.ClientID %>">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/prefferedlanguage")%></label>
                                <asp:DropDownList ID="ddlEnPreferredLang" onchange="ShowOrHideTitle(this);" TabIndex="444" runat="server" CssClass="frmSelectBig prefLang countryDropdown">
                                </asp:DropDownList>
                              
                            </p>
                        </div>
                        <!-- title field add only for German Language !-->
                        <div id="dvNameTitle" runat="server">
                            <p class="formRow">
                                <span id="lblNameTitle"  class="spnNameTitle">
                                    <label>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nametitle") %></label>
                                </span>
                                <br />
                                <asp:DropDownList CssClass="ddlNameTitle" ID="ddlTitle" runat="server" />
                            </p>
                        </div>
                        <!--artf1148103 | Missing tool tip |Added a div tag and remove the 'clear' class from class="borderBtm clear" |Rajneesh-->
                        <div class="clearAll">
                        </div>
                        <div class="borderBtm">
                            <div>
                                <p>
                                    <label class="formLabel" for="<%=txtEnrollPassword.ClientID %>">
                                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SeltPwd")%>
                                        <span class="importantMark">&#42;</span></label>
                                    <asp:TextBox CssClass="text-input defaultColor ShowColor EnrollPassword" ID="txtEnrollPassword"
                                        TabIndex="445" TextMode="Password" MaxLength="20" runat="server" />
                                    <%--<input type="text" name="password" maxlength="20" id="txtEnrollPasswordtextbox" runat="server" tabindex="327" class="validate[required] text-input defaultColor pwdTxt" />--%>
                                </p>
                                <p>
                                    <label class="formLabel" for="<%=txtEnrollReTypePassword.ClientID %>">
                                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/retypepassword")%>
                                        <span class="importantMark">&#42;</span></label>
                                    <asp:TextBox CssClass="ShowColor EnrollReTypePassword" ID="txtEnrollReTypePassword"
                                        TabIndex="447" TextMode="Password" MaxLength="20" runat="server" />
                                    <%--<input type="text" tabindex="329" name="cnfrmPassowrd" id="EnrollReTypePassword" maxlength="20" runat="server" class="validate[required] validate[confirm[password]] text-input defaultColor pwdTxt"/>--%>
                                </p>
                            </div>
                            
                                
                            
                        </div>
                        <div class="clear">
                        </div>
                        <hr />
                        <div class="ulterms">
                            
                            <div style="width: 100%" id="tandc">
                      
                        <p class="formRow">
                            <input id="chkMembershipTerms" name="chkMembershipTerms" type="checkbox" class="checkbox frmInputChkBox"
                                value="" runat="server" tabindex="455" />
                            <label class="frmCheckBox signupTandC">
                                <span>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/ireadterms") %>
                                    <a href="#" tabindex="470" onclick="openPopupWin('<%= GlobalUtil.GetUrlToPage(EpiServerPageConstants.ENROLL_TERMS_AND_CONDITION) %>','width=800,height=600,scrollbars=yes,resizable=yes'); return false;">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/receivescandicinfolatest") %>
                                    </a> <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/receivesscandicinfolatestmore") %>
                                    
                                    </span>
                               
                            </label>
                            <br class="clear" />
                        </p>
                       
                       
                        <p class="formRow">
                            <input id="chkReceivePartnersInfo" name="chkReceivePartnersInfo" type="checkbox"
                                class="checkbox frmInputChkBox" value="" runat="server" tabindex="490" />
                            
                                <label class="frmCheckBox signupTandC">
                                <span>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/receivepartnersinfo") %></label>
                            </span>
                            <br class="clear" />
                        </p>
                       
                    </div>
                           
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <!--End of Scandic�s Frequent Guest Programme-->
            </div>
            <!-- End of Gauranteen here -->
            <!--Start Reservation terms and conditions -->
            <div id="Comments" class="bknewcoment ie7bknewcoment ie8width">
                <!-- header -->
                <div class="reterm fltLft">
                    <h2>
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/reservationtermsandconditions")%>
                    </h2>
                </div>
                <!-- /header -->
                <!-- details onkeyup="return ValidatePasteLength(this)"-->
                <!-- Defect Fix - Artifact artf623478 : epitest|Details|Entering max values in the comments, the data entered will not be shown.-->
                <!-- oninput event is required for the paste event in Non-IE browsers. 
			     onkeypress will be fired for all browsers and onpaste will take care of the paste event in IE.-->
                <p class="formRow">
                    <textarea  name="txtComments" cols="10" rows="3" class="frmInputTextAreaBig"
                        id="txtComments" runat="server" onkeypress="return FilterMaxLength(event, this)"
                        onpaste="ValidateIEPasteLength(this)" oninput="return ValidatePasteLength(this)"></textarea>
                </p>
                <p class="formRow">
                    <span id="lblTermAndCondition">
                        <label>
                            <input tabindex="505" name="chkTermsCondition" type="checkbox" class="checkbox frmInputChkBox"
                                value="" runat="server" id="chkTermsCondition" />
                            <%--<span style="color: #B82025;">--%>
                            <span>
                                <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/readandaccept")%>                                     
                                  <a href="#" tabindex="509" onclick="openPopupWin('<%=GlobalUtil.GetUrlToPage(EpiServerPageConstants.RESERVATION_TERMS_AND_CONDITIONS_PAGE)%>','width=800,height=600,scrollbars=yes,resizable=yes'); return false;">
                                   <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/termsconditions")%></a>                                 
                                    </span><span>
                                <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/aswellas")%>
                            </span><a href='#' tabindex="511" onclick="openPopupWin('/Hotels/Rates/','width=800,height=600,scrollbars=yes,resizable=yes');return false;">
                                <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/rateinformation")%>
                            </a>
                        </label>
                    </span>
                </p>               
                <!-- /details -->
            </div>
            <!--End Reservation terms and conditions -->
        </div>
        <div class="clear">
            &nbsp;</div>
        <!-- /details -->
    </div>
    <div class="bkingDetBtmLinks bknewcoment cartTotal1 ie8width">
        <div class="goBackLink fltLft">
            <div class="cartAmount">
  		
			                <span class="rate"><h5><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Total")%></h5>:</span>
			                <span class="rate" id="spnTotalRate" runat="server"></span>
			                <span class="currency" id="spnCurrencyCode" runat="server"></span>		                
                            <div class="cartTerms"> <span id="spnCartTerms" runat="server"></span></div>
             </div>
            <a id="btnBack" runat="server" tabindex="515">
                <%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/goback")%>
            </a>
        </div>
        <div class="bkingLink fltRt">
            <div class="actionBtn fltRt submitbuttonnew" id="SubmitButtonDiv" runat="server">
                <asp:Button ID="lnkBtnBook" runat="server" tabindex="517" OnClick="lnkBtnBook_Click" class="submit buttonInner scansprite"/>
            </div>
        </div>
    </div>
    <div class="booking-progress" id="BookingProgressDiv" runat="server" style="display: none">
        <img src=<%=ResolveUrl("~/Templates/Booking/Styles/Default/Images/rotatingclockani.gif")%>
            alt="image" align="middle" width="25px" height="25px" />
        <span>
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/bookinginprogress")%>
        </span>
    </div>
</div>

<script type="text/javascript" language="javascript">       

function bindFocusMethod(fieldToBind)
{

$(fieldToBind).focus(function()
{                    				
 if($(this).hasClass('error'))
 {                    					
	 var callRel = $(this).attr('rel');
	 $.validationEngine.isError = true;
//						 promptText += $.validationEngine.settings.allrules["confirm"].alertText3+"<br />";
        
	 var promptText = $("#sameGuestNames").val() +"<br />";
       var linkTofield = $.validationEngine.linkTofield(this);						                    
	 ($("div."+linkTofield).size() ==0) ? $.validationEngine.buildPrompt(this,promptText,"error")	: $.validationEngine.updatePromptText(this,promptText);
  }
});
$.validationEngine.isError = true;
var promptText = $("#sameGuestNames").val() +"<br />";
    linkTofield = $.validationEngine.linkTofield(fieldToBind);
    ($("div."+linkTofield).size() ==0) ? $.validationEngine.buildPrompt(fieldToBind,promptText,"error")	: $.validationEngine.updatePromptText(fieldToBind,promptText);
}

function CamparesArrayNames(arrayNames)
{
    
   var strIndexes=""; 
   var strMainIndex="";
   for(var iArray=0;iArray<arrayNames.length;iArray++)
      {
         for(var iArrayInner=iArray+1;iArrayInner<arrayNames.length;iArrayInner++)
           {
             if(arrayNames[iArray]==arrayNames[iArrayInner])
              {
                 if(strIndexes=="")
                   strIndexes=iArray+":"+iArrayInner;
                 else
                   strIndexes+=":"+iArrayInner
              }         
           } 
            
             if(strIndexes!="")
              {
                 strMainIndex+=strIndexes+"#";
                 strIndexes="";   
              }       
      }
      return strMainIndex;
}

function setLabelCssForBookingControls(labelId, cssClass,errorClass)
{
    if(labelId != null)
    {
        var labelControl = $fn(_endsWith(labelId));
    		    
        if (labelControl == null)	
        {	        
            labelControl = $fn(labelId);
        }
        if (labelControl != null)
        {
            labelControl["className"] = cssClass+""+errorClass;
        }
    }
}

// This method will check whether nametobecomparedwith is equal to nameone or nametwo or namethree
function CompareNames(nametobecomparedwith, nameone, nametwo, namethree)
{
    
    var result = true;
    if(nametobecomparedwith != null)
    {
        if (    (nameone != null && nametobecomparedwith == nameone ) 
             || (nametwo != null && nametobecomparedwith == nametwo )
             || (namethree != null && nametobecomparedwith == namethree ))
        {
            result = false;            
        } 
    }
    return result;
}
    // AMS Fix: artf833540
    // This will disable multiple bookings happening on clicking book button multiple times in IE.
   var isClicked = false;
   // $fn(_endsWith("SubmitButtonDiv")).style.display = "block";
    //$fn(_endsWith("BookingProgressDiv")).style.display = "none";    
    
    // storing the loaded values to variables,these loaded values would be 
    // assign back while disabling these controls
  //  var oldAddress1 = $fn(_endsWith("txtAddressLine1")).value; 
   // var oldAddress2 = $fn(_endsWith("txtAddressLine2")).value;
   // var oldCity = $fn(_endsWith("txtCityOrTown")).value;
   // var oldPostcode = $fn(_endsWith("txtPostCode")).value;
    //var oldCountry = $fn(_endsWith("ddlCountry")).value;
    //var oldMobCode = $fn(_endsWith("ddlMobileNumberRoom1")).value;
   // var oldMobile = $fn(_endsWith("txtTelephoneRoom1")).value;
   // var oldLandCode = $fn(_endsWith("ddlTel1")).value;
   // var oldLandPhone = $fn(_endsWith("txtTelephone1")).value;    
   // var oldEmail = $fn(_endsWith("txtEmailRoom1")).value;
   // var oldConfirmEmail = $fn(_endsWith("txtConfirmEmailRoom1")).value;
   // var oldRadioMale = $fn(_endsWith("rdoMale")).checked;
   // var oldRadioFemale = $fn(_endsWith("rdoFemale")).checked;
   
    initBD();
    UpdateGuestInfo();
    
    // Add Country Codes
    if(!phoneCodesWithCountry)
    {
        phoneCodesWithCountry = new CountryPhoneCodesMap();
        <%
            OrderedDictionary countryPhoneCodesMap = GetCountryPhoneCodeMap();
            if (countryPhoneCodesMap != null)
            {
                foreach (string key in countryPhoneCodesMap.Keys)
                {
        %>                        
                        var eachCountryPhone = new EachCountryPhone('<%=key%>', '<%=countryPhoneCodesMap[key] as string%>');
                        phoneCodesWithCountry.addCountryCode(eachCountryPhone);                        
        <%
                }
            }
        %>
    }
    
//Defect Fix - Artifact artf623478 : epitest|Details|Entering max values in the comments, the data entered will not be shown.
function ValidatePasteLength(text) 
{
    var maxlength = new Number(500);
    if (text.value.length > maxlength){
        text.value = text.value.substring(0,maxlength);
    }
}
function ValidateIEPasteLength(text)
{
    event.returnValue = false;
    var pasteString = window.clipboardData.getData("Text");
    if (pasteString.length > 500){
        text.value = pasteString.substring(0,500);
    }
}
//Defect Fix - Artifact artf623478 : epitest|Details|Entering max values in the comments, the data entered will not be shown.
function FilterMaxLength(e,o){

    // we must allow spaces , backspace etc to go through
    var iKeyCode, strKey;
    if (window.event) {
        iKeyCode = e.keyCode;
    } else {
        iKeyCode = e.which;
    }
    //strKey = String.fromCharCode(iKeyCode);
    // Make sure we let special keys go through.
    if (iKeyCode==null || iKeyCode==0 || iKeyCode==8)
        return true;
    var maxLength = 500;
    if(o.value.length > maxLength-1){
        return false;
    }
    return true
}

// R1.3 | CR5 | StoryId: | Login on Booking Detail Screen.
// This function will enable/disable the contact info fields, based on the login status.
function UpdateGuestInfo()
{
    
    var chkUpdateProfile = $fn(_endsWith("chkModifyProfile"));
    if (chkUpdateProfile != null)
    {
        var chkModifyInfo = $fn(_endsWith("chkModifyProfile"));
        
        //var city = $fn(_endsWith("txtCityOrTown"));
        var country = $fn(_endsWith("ddlCountry"));
        var mobCode = $fn(_endsWith("ddlMobileNumber"));
        var mobile = $fn(_endsWith("txtTelephone"));
        var email = $fn(_endsWith("txtEmail"));
        //var confirmEmail = $fn(_endsWith("txtConfirmEmailRoom"));
        //Release R2.0 - due to multiroom implementation enable/disable the membershipnumber based on login status.
        
        //START PRIYA SINGH | 17 April 2012 |AMS patch7|Artifact artf1259094 : Scanweb - Changed profile name online
		var txtFName = eval($fn(_endsWith(("RoomInformation0_txtFNameRoom"))));
        var txtLName = eval($fn(_endsWith(("RoomInformation0_txtLNameRoom")))); 
        //END PRIYA SINGH | 17 April 2012 |AMS patch7|Artifact artf1259094 : Scanweb - Changed profile name online
        
        if (chkModifyInfo != null)
        {        
            var canModify = !chkModifyInfo.checked;
           // city.disabled = canModify;
            country.disabled = canModify;
            mobCode.disabled = canModify;
            mobile.disabled = canModify;
            email.disabled = canModify;
            //confirmEmail.disabled = canModify;
            if(canModify)
             {
               //$($fn(_endsWith("ddlMobileNumber"))).removeClass("validate[required]");//validate[required] mandatory
               $($fn(_endsWith("ddlMobileNumber"))).removeClass("mandatory");
             }
             else
             {
                //START PRIYA SINGH | 17 April 2012 |AMS patch7|Artifact artf1259094 : Scanweb - Changed profile name online
                var chkBookForSomeoneElse = $fn(_endsWith("chkBookForSomeoneElse"));
                if (chkBookForSomeoneElse != null)
                {
                    var isBookForSomeoneElse = chkBookForSomeoneElse.checked;
                     if(isBookForSomeoneElse == true)
                     {
                        $fn(_endsWith("chkBookForSomeoneElse")).checked = false;
                        txtFName.value = fName;  
                        txtLName.value = lName;
                        //city.value = oldCity;
                        country.value = oldCountry;
                        mobCode.value = oldMobCode;
                        mobile.value = oldMobile;
                        email.value = oldEmail;
                      //  confirmEmail.value = oldConfirmEmail;
                     }
                }
                //END PRIYA SINGH | 17 April 2012 |AMS patch7|Artifact artf1259094 : Scanweb - Changed profile name online
               //$($fn(_endsWith("ddlMobileNumber"))).addClass("validate[required]");                
             }             
        }
//        else
//        {
//            // Assing back the loaded values while disabling         
//            // address1.value = oldAddress1;
//            // address2.value = oldAddress2;
//            city.value = oldCity;
//           // postcode.value = oldPostcode;
//            country.value = oldCountry;
//            mobCode.value = oldMobCode;
//            mobile.value = oldMobile;
//           // landCode.value = oldLandCode;
//           // landPhone.value = oldLandPhone;
//            email.value = oldEmail;
//            confirmEmail.value = oldConfirmEmail;
//           // radioMale.checked = oldRadioMale;
//           //  radioFemale.checked = oldRadioFemale;
//    
//           // address1.disabled = 1;
//           // address2.disabled = 1;
//            city.disabled = 1;
//           // postcode.disabled = 1;
//            country.disabled = 1;
//            mobCode.disabled = 1;
//            mobile.disabled = 1;
//           // landCode.disabled = 1;
//           // landPhone.disabled = 1;
//            email.disabled = 1;
//            confirmEmail.disabled = 1;    
//            membershipNumber1.disabled = 1;      
//           // radioMale.disabled = 1;
//           // radioFemale.disabled = 1;
//        }
    }
    $('input[disabled]').addClass("disableColor");
    $('select[disabled]').addClass("disableColor");
    $('input:enabled').removeClass("disableColor");
    $('select:enabled').removeClass("disableColor");

}   
   var R1FirstName = eval($fn(_endsWith(("RoomInformation0_txtFNameRoom"))));
        var R1LastName  = eval($fn(_endsWith(("RoomInformation0_txtLNameRoom")))); 
        
 //  assign back while disabling these controls
  var fName=$fn(_endsWith("hdnFName")).value;
   var lName=$fn(_endsWith("hdnLName")).value;
   // var oldCity = $fn(_endsWith("hdnCity")).value;
    //var oldPostcode = $fn(_endsWith("txtPostCode")).value;
    var oldCountry = $fn(_endsWith("hdnCountry")).value;
    var oldMobCode = $fn(_endsWith("hdnPhCode")).value;
    var oldMobile = $fn(_endsWith("hdnPhoneNumber")).value;
    //var oldLandCode = $fn(_endsWith("ddlTel1")).value;
    //var oldLandPhone = $fn(_endsWith("txtTelephone1")).value;    
    var oldEmail = $fn(_endsWith("hdnEmail")).value;
   // var oldConfirmEmail = $fn(_endsWith("hdnConfEmail")).value;       
        
//var isSelectHotelTabGoogleMapReloaded = false;
function setBookForSomeOneElseStatus()
{
    
    var chkBookForSomeoneElse = $fn(_endsWith("chkBookForSomeoneElse"));
    if (chkBookForSomeoneElse != null)
    {
        var isBookForSomeoneElse = chkBookForSomeoneElse.checked;
        
        var lblFName = $fn(_endsWith("RoomInformation0_lblFirstName"));
        var lblLName = $fn(_endsWith("RoomInformation0_lblLastName"));
        var txtFName = eval($fn(_endsWith(("RoomInformation0_txtFNameRoom"))));
        var txtLName = eval($fn(_endsWith(("RoomInformation0_txtLNameRoom")))); 
        //var city = $fn(_endsWith("RoomInformation0_txtCityOrTown"));
        var country = $fn(_endsWith("RoomInformation0_ddlCountry"));
        var mobCode = $fn(_endsWith("RoomInformation0_ddlMobileNumber"));
        var mobile = $fn(_endsWith("RoomInformation0_txtTelephone"));
        var email = $fn(_endsWith("RoomInformation0_txtEmail"));
       // var confirmEmail = $fn(_endsWith("RoomInformation0_txtConfirmEmailRoom"));
        var updateGuestInfo = $fn(_endsWith("UpdateGuestInfo"));
        var chkModifyProfile = $fn(_endsWith("chkModifyProfile"));
        chkModifyProfile.checked = false;
        
        if(isBookForSomeoneElse == true)
        {
            updateGuestInfo.style.display = "none";
            //lblFName.visible = false;
            //lblFName.style.display = "none";
            //lblLName.style.display = "none";
            //txtFName.style.display = "block";
            //txtLName.style.display = "block";
            
              //R 2.0 Bug Id:511122-Rajneesh
            //Labels are not populated in "gueste information" section in booking details page | Give away redemption
            //Modified the code to assign the label values to the text box fields.
            
            txtFName.value ="";// '<%=firstNameText%>';       
            txtLName.value ="";// '<%=lastNameText%>';
           // city.value ="";// '<%=townOrCity%>';           
            mobile.value ="";// '<%=phone%>';
            email.value = "";//'<%=email%>';
           // confirmEmail.value = "";//'<%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/confirmemailadd")%>';
             country.selectedIndex = 0;
            mobCode.selectedIndex = 0;
            //city.disabled = 0;
            country.disabled = 0;
            mobCode.disabled = 0;
            mobile.disabled = 0;
            email.disabled = 0;
            //confirmEmail.disabled = 0;
             txtFName.disabled=0;
             txtLName.disabled=0;
        }
        else
        {
            updateGuestInfo.style.display = "block";
            //txtFName.style.display = "block";
            //txtLName.style.display = "block";
            //txtFName.style.display = "none";
            //txtLName.style.display = "none";
            txtFName.value=fName;  
            txtLName.value=  lName;
            //city.value = oldCity;
            country.value = oldCountry;
            mobCode.value = oldMobCode;
            mobile.value = oldMobile;
            email.value = oldEmail;
            //confirmEmail.value = oldConfirmEmail;
           // city.disabled = 1;
            country.disabled = 1;
            mobCode.disabled = 1;
            mobile.disabled = 1;
            email.disabled = 1;
            //confirmEmail.disabled = 1;
            txtFName.disabled=1;
            txtLName.disabled=1;
            selectPhoneCode('RoomInformation0_ddlCountry','RoomInformation0_ddlMobileNumber');
        }
        $('input[disabled]').addClass("disableColor");
        $('select[disabled]').addClass("disableColor");
        $('input:enabled').removeClass("disableColor");
        $('select:enabled').removeClass("disableColor");
    }
}
$(document).ready(function(){
    //START PRIYA SINGH | 14 June 2012 |AMS patch7|Artifact artf1259094 : Scanweb - Changed profile name online
    var isCheckedBookForSomeoneElse;
    var txtFName = eval($fn(_endsWith(("RoomInformation0_txtFNameRoom"))));
    var txtLName = eval($fn(_endsWith(("RoomInformation0_txtLNameRoom")))); 
    var chkModifyInfo = $fn(_endsWith("chkModifyProfile"));
    //var city = $fn(_endsWith("txtCityOrTown"));
    var country = $fn(_endsWith("ddlCountry"));
    var mobCode = $fn(_endsWith("ddlMobileNumber"));
    var mobile = $fn(_endsWith("txtTelephone"));
    var email = $fn(_endsWith("txtEmail"));
    //var confirmEmail = $fn(_endsWith("txtConfirmEmailRoom"));
    var chk1 = $fn(_endsWith("chkBookForSomeoneElse")); 
    if(chk1 != null)
        isCheckedBookForSomeoneElse = chk1.checked;
    if(isCheckedBookForSomeoneElse == true) 
    { 
        var chk2 = $fn(_endsWith("UpdateGuestInfo"));
        if(chk2 != null) 
        chk2.style.display = "none"; 
        //city.disabled = 0;
        country.disabled = 0;
        mobCode.disabled = 0;
        mobile.disabled = 0;
        email.disabled = 0;
        //confirmEmail.disabled = 0;
        txtFName.disabled=0;
        txtLName.disabled=0;
    }
    else if(isCheckedBookForSomeoneElse == false)
    {
        txtFName.value=fName;  
        txtLName.value=lName;
        //city.value = oldCity;
        country.value = oldCountry;
        mobCode.value = oldMobCode;
        mobile.value = oldMobile;
        email.value = oldEmail;
        //confirmEmail.value = oldConfirmEmail;
        //city.disabled = 1;
        country.disabled = 1;
        mobCode.disabled = 1;
        mobile.disabled = 1;
        email.disabled = 1;
        //confirmEmail.disabled = 1;
        txtFName.disabled=1;
        txtLName.disabled=1;
        selectPhoneCode('RoomInformation0_ddlCountry','RoomInformation0_ddlMobileNumber');
    }
 });
</script>

       <!-- To Display Lightbox for Moadal Pop up window -->
    <div class="jqmWindow dialog imgGalPopUp">
        <div class="hd sprite">
        </div>
        <div class="cnt roomDescription">
            <a href="#" class="jqmClose scansprite blkClose"></a>
            <div class="LightBoxCntnt" id="RoomCategoryContainer" runat= "server">
            </div>
        </div>
        <div class="ft sprite">
        </div>
    </div>
    


  <script type="text/javascript" src="/Templates/Booking/Javascript/newvalidation.js?v=<%=CmsUtil.GetJSVersion()%>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.validator.addMethod("No_Spl_Char", function(value, element) {
                var regExp = /[^A-Za-z���������������������������������������������������������������/0-9-\s]/;
                var charpos = value.search(regExp);
                var returnVal = true;
                if (value.length > 0 && charpos >= 0) { returnVal = false; }
                return this.optional(element) || returnVal;
            },
	"Special Characters are not allowed");
            $.validator.addMethod("AllowStar", function(value, element) {
                var regExp = /[^0-9-*\s]/;
                var charpos = value.search(regExp);
                var returnVal = true;
                if (value.length > 0 && charpos >= 0) { returnVal = false; }
                return this.optional(element) || returnVal;
            },
        "Enter a valid payment card number");
            $.validator.addMethod("valueNotEquals", function(value, element, arg) {
                return this.optional(element) || arg != value;
            }, "Value must not equal arg.");
            jQuery.validator.addMethod("notEqualToGroup", function(value, element, options) {
                // get all the elements passed here with the same class
                var elems = $(element).parents('form').find(options[0]);
                // the value of the current element
                var valueToCompare = value;
                // count
                var matchesFound = 0;
                // loop each element and compare its value with the current value
                // and increase the count every time we find one
                jQuery.each(elems, function() {
                    thisVal = $(this).val();
                    if (thisVal == valueToCompare) {
                        matchesFound++;
                    }
                });
                //Added by Chandra--Start
                // count should be either 0 or 1 max
                if (this.optional(element) || matchesFound <= 1) {
                    elems.removeClass('error');
                    return this.optional(element) || true;
                } else {
                    elems.addClass('error');
                }
            }, "Guest names should be unique for each room.");

            jQuery.validator.addMethod("userNameNotEqualToGroup", function(value, element, options) {
                // get all the elements passed here with the same class
                var elems = $(element).parents('form').find(options[0]);

                var RoomNames = new Array();
                var count = elems.length / 2;
                switch (count) {
                    case 1:
                        for (var i = 0; i < count; i++)
                            RoomNames[i] = (trim(elems[i].value) + trim(elems[i + 1].value)).toLowerCase();
                        break;
                    case 2:
                        for (var i = 0; i < count; i++)
                            RoomNames[i] = (trim(elems[2 * i].value) + trim(elems[2 * i + 1].value)).toLowerCase();
                        break;
                    case 3:
                        for (var i = 0; i < count; i++)
                            RoomNames[i] = (trim(elems[2 * i].value) + trim(elems[2 * i + 1].value)).toLowerCase();
                        break;
                    case 4:
                        for (var i = 0; i < count; i++)
                            RoomNames[i] = (trim(elems[2 * i].value) + trim(elems[2 * i + 1].value)).toLowerCase();
                        break;
                }
                if (element.id.indexOf('_txtFNameRoom') > 0) {
                    var lastName = $(element).parent().next().find('input[id$="txtLNameRoom"]').val()
                    value = trim(value) + trim(lastName);
                }
                else if (element.id.indexOf('_txtLNameRoom') > 0) {
                    var firstName = $(element).parent().prev().find('input[id$="txtFNameRoom"]').val();
                    value = trim(firstName) + trim(value);

                }
                var valueToCompare = value.toLowerCase();
                // count
                var matchesFound = 0;
                // loop each element and compare its value with the current value
                // and increase the count every time we find one
                for (var i = 0; i < RoomNames.length; i++) {
                    if (RoomNames[i] == valueToCompare) {
                        matchesFound++;
                    }
                }
                //Added by Chandra--End
                // count should be either 0 or 1 max
                if (this.optional(element) || matchesFound <= 1) {
                    $(element).removeClass('error'); //elems.removeClass('error');
                    return this.optional(element) || true;
                } else {
                    $(element).addClass('error'); //elems.addClass('error');
                }
            }, "Guest names should be unique for each room.");
			
			
			 $.validator.addMethod("checkcredit", function(value, element) {
			cardname = $(cardtypemd).val();		
			cardnumber = value;
			if(cardnumber.length != 0){			
			if(checkCreditCard(cardnumber, cardname,'RoomInformation0_txtCardNumber')){
				return checkCreditCard(cardnumber,cardname,'RoomInformation0_txtCardNumber');
				}
			}			    		
		
		}, "Creditcard Number is not matched with Creditcard Type");
		
		$.validator.addMethod("checkcredit1", function(value, element) {
			cardname1 = $(cardtypemd1).val();		
			cardnumber1 = value;
			if(cardnumber1.length != 0){			
			if(checkCreditCard(cardnumber1, cardname1,'RoomInformation1_txtCardNumber')){
				return checkCreditCard(cardnumber1,cardname1,'RoomInformation1_txtCardNumber');
				}
			}			    		
		
		}, "Creditcard Number is not matched with Creditcard Type");
		
		$.validator.addMethod("checkcredit2", function(value, element) {
			cardname2 = $(cardtypemd2).val();		
			cardnumber2 = value;
			if(cardnumber2.length != 0){			
			if(checkCreditCard(cardnumber2, cardname2,'RoomInformation2_txtCardNumber')){
				return checkCreditCard(cardnumber2,cardname2,'RoomInformation2_txtCardNumber');
				}
			}			    		
		
		}, "Creditcard Number is not matched with Creditcard Type");
		
		$.validator.addMethod("checkcredit3", function(value, element) {
			cardname3 = $(cardtypemd3).val();		
			cardnumber3 = value;
			if(cardnumber3.length != 0){			
			if(checkCreditCard(cardnumber3, cardname3,'RoomInformation3_txtCardNumber')){
				return checkCreditCard(cardnumber3,cardname3,'RoomInformation3_txtCardNumber');
				}
			}			    		
		
		}, "Creditcard Number is not matched with Creditcard Type");
		
		$.validator.addMethod("checkcreditcommon", function(value, element) {
			cardnameCommon = $(cccommontype).val();		
			cardnumberCommon = value;
			if(cardnumberCommon.length != 0){			
			if(checkCreditCard(cardnumberCommon, cardnameCommon,'txtCardNumberCommon')){
				return checkCreditCard(cardnumberCommon,cardnameCommon,'txtCardNumberCommon');
				}
			}			    		
		
		}, "Creditcard Number is not matched with Creditcard Type");
       
		$("a[id$='spnAddSpecialrequests']").attr('href','javascript:void(0)');
        
        });

    function TrimWhiteSpace() {
        trimWhiteSpaces("txtMembershipNumberRoom");
    }


    $("input[id$='rdoLateArrivalGurantee']").live('click',function () {
       if (this.checked && $(this).attr('rel') == "Room0") {
            $("div[id$='RoomInformation0_HideUnhideCreditCardDiv']").show();
            if($("div[id$='RoomInformation0_panHashCreditCardDiv']") !=null)
                $("div[id$='RoomInformation0_panHashCreditCardDiv']").show();
        } else if (!this.checked && $(this).attr('rel')== "Room0") {
            $("div[id$='RoomInformation0_HideUnhideCreditCardDiv']").hide();
            if($("div[id$='RoomInformation0_panHashCreditCardDiv']") !=null)
                $("div[id$='RoomInformation0_panHashCreditCardDiv']").hide();
        }

        if (this.checked && $(this).attr('rel') == "Room1") {
            $("div[id$='RoomInformation1_HideUnhideCreditCardDiv']").show();
            if($("div[id$='RoomInformation1_panHashCreditCardDiv']") !=null)
                $("div[id$='RoomInformation1_panHashCreditCardDiv']").show();
        } else if (!this.checked && $(this).attr('rel') == "Room1") {
            $("div[id$='RoomInformation1_HideUnhideCreditCardDiv']").hide();
            if($("div[id$='RoomInformation1_panHashCreditCardDiv']") !=null)
                $("div[id$='RoomInformation1_panHashCreditCardDiv']").hide();
        }

        if (this.checked && $(this).attr('rel') == "Room2") {
            $("div[id$='RoomInformation2_HideUnhideCreditCardDiv']").show();
            if($("div[id$='RoomInformation2_panHashCreditCardDiv']") !=null)
                $("div[id$='RoomInformation2_panHashCreditCardDiv']").show();
        } else if (!this.checked && $(this).attr('rel')== "Room2") {
            $("div[id$='RoomInformation2_HideUnhideCreditCardDiv']").hide();
            if($("div[id$='RoomInformation2_panHashCreditCardDiv']") !=null)
                $("div[id$='RoomInformation2_panHashCreditCardDiv']").hide();
        }

        if (this.checked && $(this).attr('rel')== "Room3") {
            $("div[id$='RoomInformation3_HideUnhideCreditCardDiv']").show();
            if($("div[id$='RoomInformation3_panHashCreditCardDiv']") !=null)
                $("div[id$='RoomInformation3_panHashCreditCardDiv']").show();
        } else if (!this.checked && $(this).attr('rel') == "Room3") {
            $("div[id$='RoomInformation3_HideUnhideCreditCardDiv']").hide();
            if($("div[id$='RoomInformation3_panHashCreditCardDiv']") !=null)
                $("div[id$='RoomInformation3_panHashCreditCardDiv']").hide();
        }

    });

        $("input[id$='rdoLateArrivalGuranteeCommon']").live("click", function() {
		        if (this.checked) {
            $("div[id$='panHashCreditCardDivCommon']").show();
            $("div[id$='HideUnhideCreditCardDivCommon']").show();
        } else {
            $("div[id$='panHashCreditCardDivCommon']").hide();
            $("div[id$='HideUnhideCreditCardDivCommon']").hide();
        }
    });
   
  </script>
