<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AvailabilityCalendar.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.AvailabilityCalendar" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register Src="AvalibilityCalendarItem.ascx" TagName="AvailabilityCalenderItem" TagPrefix="SelectRate" %>
<div id="modifyFlowTab" runat="server">
<div id="my_carousel">

<div class="scansprite btnprev"><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/Previous5days") %></strong></div>
<div class="scansprite btnprev1"><asp:LinkButton ID="btnPrevBatch" runat="server" onclick="btnOnPrevCalendarRange_Click"><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/Previous5days") %></strong></asp:LinkButton></div>

<div class="scansprite btnnext"><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/Next5days") %></strong></div>
<div class="scansprite btnnext1"><asp:LinkButton ID="btnNextBatch" runat="server" onclick="btnOnNextCalendarRange_Click"><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/Next5days") %></strong></asp:LinkButton></div>
<!--START: carsouel dates -->

	<ul id="carouselData" runat="server" class="carouselData">
		<li>
            <%--AvailabilityCalenderItem will be loaded here--%>
		</li>	
	</ul>
<!--END: carsouel dates -->

</div>
</div>
 <%-- holds the translated version for "Not Available"--%>
<input id="hdnNotAvailableString" type = "hidden" name = "hdnNotAvailableString" value = '<%= WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/NotAvailable") %>' />

