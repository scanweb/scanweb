using System;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// RoomSummaryInfoForEmail
    /// </summary>
    public partial class RoomSummaryInfoForEmail : System.Web.UI.UserControl
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            setImageSource();
            setFieldsForRoomSummaryDetails();
        }

        #region Private

        /// <summary>
        /// Sets the image source.
        /// </summary>
        private void setImageSource()
        {
            string authority = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);

            greyBoxTop.ImageUrl = authority + ResolveUrl("/templates/Scanweb/Images/Images_Email/grayBox_top.gif");
            grayBoxBottom.ImageUrl = authority + ResolveUrl("/templates/Scanweb/Images/Images_Email/grayBox_btm.gif");
        }

        /// <summary>
        /// setFieldsForRoomSummaryDetails
        /// </summary>
        private void setFieldsForRoomSummaryDetails()
        {
            roomLabel.InnerHtml = propRoomLabel;
            roomRateCategory.InnerHtml = propRoomRateCategory;
            roomTypeID.InnerHtml = propRoomTypeID;

            priceInfo.InnerHtml = propPriceInfo;

            priceString.InnerHtml = propPriceString;
            if (!string.IsNullOrEmpty(reservationLeg))
            {
                reservationLeglabel.Visible = true;
                reservationLeglabel.InnerHtml = reservationLeg;

                WebUtil.SetChargedAtHotelLabel(roomNumber-1, lblChargedAtHotel, false,false);
            }

        }

        #endregion

        #region Properties

        private string roomIdentifier = string.Empty;

        /// <summary>
        /// Gets or sets the room identifier.
        /// </summary>
        /// <value>The room identifier.</value>
        public string RoomIdentifier
        {
            get { return roomIdentifier; }
            set { roomIdentifier = value; }
        }

        private string propRoomLabel = string.Empty;

        /// <summary>
        /// Gets or sets the room label.
        /// </summary>
        /// <value>The room label.</value>
        public string RoomLabel
        {
            get { return propRoomLabel; }
            set { propRoomLabel = value; }
        }

        
        private string propRoomRateCategory = string.Empty;

        /// <summary>
        /// Gets or sets the room rate category.
        /// </summary>
        /// <value>The room rate category.</value>
        public string RoomRateCategory
        {
            get { return propRoomRateCategory; }
            set { propRoomRateCategory = value; }
        }

        private string propRoomTypeID = string.Empty;

        /// <summary>
        /// Gets or sets the room type ID.
        /// </summary>
        /// <value>The room type ID.</value>
        public string RoomTypeID
        {
            get { return propRoomTypeID; }
            set { propRoomTypeID = value; }
        }

        private string propPriceInfo = string.Empty;

        /// <summary>
        /// Gets or sets the price info.
        /// </summary>
        /// <value>The price info.</value>
        public string PriceInfo
        {
            get { return propPriceInfo; }
            set { propPriceInfo = value; }
        }

        private string propPriceString = string.Empty;

        /// <summary>
        /// Gets or sets the price string.
        /// </summary>
        /// <value>The price string.</value>
        public string PriceString
        {
            get { return propPriceString; }
            set { propPriceString = value; }
        }

        private string reservationLeg = string.Empty;

        
        public string ReservationLeg
        {
            get { return reservationLeg; }
            set { reservationLeg = value; }
        }

        private int roomNumber;

        public int RoomNumber
        {
            get { return roomNumber; }
            set { roomNumber = value; }
        }

        #endregion
    }
}