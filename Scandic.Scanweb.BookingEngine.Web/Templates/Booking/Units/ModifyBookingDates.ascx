<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ModifyBookingDates.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ModifyBookingDates" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx"
    TagName="ReservationInformation" TagPrefix="BookingModify" %>
<%@ Register Src="Calendar.ascx" TagName="Calendar" TagPrefix="uc2" %>

<div id="Header">
                <h2 class="borderH2 clsMargin">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ModifyOrCancel") %>
                </h2>
            </div>
            
<div class="">
    <!-- Step2: Select Rate -->
    <!--START: helptext  -->
    <div class="infoAlertBox" id ="divHelpDescriptionText" runat ="server" visible="false">
        <div class="threeCol alertContainer">
         
            <div class="hd sprite">
                &nbsp;</div>
            <div class="cnt sprite">
                <p class="scansprite">                                       
                    <label id="lblHelpDescriptionText" runat="server"/>
                </p>
            </div>
            <div class="ft sprite">
            </div>
        </div>
    </div>
    <div class="infoAlertBox infoAlertBoxRed" id="errorAlertDiv" runat="server" Visible="false">
	<div class="threeCol alertContainer">
		<div class="hd sprite">&nbsp;</div>
		<div class="cnt sprite">
		   <span class="alertIconRed">&nbsp;</span>
		    <div id="errorDiv" class="descContainer">
				<label class="titleRed"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/PleaseNote") %></label>
				<label id="errorLabel" runat="server" class="infoAlertBoxDesc"></label>
			</div>       
		    <div class="clearAll">&nbsp;</div>
		</div>
		<div class="ft sprite">&nbsp;</div>
	</div>               
	</div>
    <!--END: helptext  -->
    <div id="InformativeTextModify">
     <label id="lblInformativeTextModify" runat="server" visible ="false"></label>
    </div>
    <div id="InformativeText" class="txtContnr fltLft">
        <label id="lblInformativeText" runat="server">
        </label>
        <div class="clearAll">&#160;</div>
    </div>
    <div class="clearAll">&#160;</div>
    <div class="topCanclBtn fltRt">
        <div class="cancelBtn">
            <asp:LinkButton TabIndex="3" OnClick="btnCancelBooking_Click" ID="btnCancelBooking"
                runat="server" class="buttonInner"><%= WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/CancelButtonText") %></asp:LinkButton>
            <span id="spanbtnCancelBooking" runat="server" class="buttonRt"></span>
        </div>
    </div>
    <div class="clearAll">&#160;</div>
    <div id="divBookingStatus" runat="server" class="errorText error errorRate mgnBtm20" visible="false">
    </div>
    <div id="masterReserMod21" class="masterReserMod21">
        <div class="greyRd">
            <%--<div class="hd sprite">
            </div>
            <p class="fltLft lblReservationHeader">
                <strong><span id="lblReservationHeader" runat="server"></span></strong><a class="help spriteIcon toolTipMe"
                    id="reservationNumberHelpIcon" href="#" runat="server"></a>
                <span id="lblReservationNumber" runat="server"></span>
            </p>--%>
            <%--<p class="fltRt" sizcache="0" sizset="56">
                <asp:LinkButton class="modLink" title="Modify Contact Details" id="lnkModifyContactDetails" OnClick="lnkModifyContactDetails_Click" runat="server"><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/ModifyContactDetails")%></asp:LinkButton>
            </p>--%>
            <BookingModify:ReservationInformation ID="ReservationInfo" runat="server">
            </BookingModify:ReservationInformation>
            <div class="ft">
                &nbsp;</div>
        </div>
    </div>
    <div class="guaranteTypeInfo">
        <label id="lblGuaranteeInfo" runat="server"></label>
    </div>
    <!-- LinksCont -->
    <%--		<div id="LinksCont">
				<div class="alignLeft">					
					<span class="link">
				    	 <a href='<%=SessionWrapper.PreviousPageURL%>' class="" ><%=WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/ReturnToPrevious")%></a> 
					</span>
					
				</div>
	<%--			<div class="alignRight">
					<span class="btnSubmit">
						<asp:LinkButton ID="btnCancelBooking" runat="server" OnClick="btnCancelBooking_Click"><span><%=WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/CancelBooking")%></span></asp:LinkButton>	
					</span>
				</div>--%>
    <%--	<div class="clear">&nbsp;</div>
			</div>--%>
    <!-- LinksCont -->
    <!--Modify this booking-->
    <%--<asp:PlaceHolder ID="divmodifyInfo" runat="server">
				<!-- header -->
				<asp:PlaceHolder ID="divModifyHeaderInfo" runat="server" >
				<div class="darkHeading">
				<h2><span><%=WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/ModifyThisBooking")%></span></h2>
				</div>
				<div class="bookingDates">
					<p><%=WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/loremipsum")%></p>
				</div>
				</asp:PlaceHolder>
				<!-- /header -->
				<div id="BookingInfo">
					<div id="clientErrorDiv" ></div>
					<div class="errorDivClient">
					    <input type="hidden" id="siteLang" name="siteLang" value="<%=EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper()%>" />
						<input type="hidden" id="errMsgTitle" name="errMsgTitle" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading")%>" />
						<input type="hidden" id="arrivaldateError" name="arrivaldateError" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/arrival_date")%>" />
						<input type="hidden" id="departuredateError" name="departuredateError" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/departure_date")%>" />
						<input type="hidden" id="NoOfNightError" name="NoOfNightError" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/no_of_nights")%>" />
					</div>
					<!-- Common Controls -->
					<div id="commonControls" runat="server">
						<div class="colOne">
							<p>
								<span id="atCont">
									<label><%=WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/ArrivalDate")%></label>
									<br />
									<input class="inputTextSmall modifyDateRange" type="text" id="txtmodifyArrivalDate" name="ArrivalDate" runat="server"/>
								</span>
							</p>
							
						</div>
						<div class="colTwo">
							<p>
								<span id="dtCont">
									<label><%=WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/DepartureDate")%></label>
									<br />
									<input class="inputTextSmall modifyDateRange" type="text" id="txtmodifyDepartureDate" name="DepartureDate" runat="server" />
								</span>
							</p>
							
						</div>
						<div class="colThree">
							<p>
								<span id="nnCont">
									<label><%=WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/NumberOfNights")%></label>
									<br />
									<input class="inputTextSmall" type="text" id="modifyNoOfNight" runat="server" name="NoOfNight" size="3" maxlength="2" />
								</span></p>
						</div>
						<div class="clear">&nbsp;</div>
						<div class="colOne">
							<p>
								<label><%=WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/AdultsPerRoom")%></label>
								<br />
								<asp:DropDownList CssClass="frmSelect"  ID="ddlAdultsPerRoom" runat="server"></asp:DropDownList>
							</p>
						</div>
						<div class="colTwo" style="width: 210px; margin-right: 0;">
							<p>
								<label><%=WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/ChildrenPerRoom")%></label>
								<br />
								<asp:DropDownList CssClass="frmSelect"  ID="ddlChildPerRoom" runat="server"></asp:DropDownList>								
							</p>
						</div>
						<div class="colThree" style="width: 210px; margin-right: 0;">
							<p>
								<label><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/noofrooms")%></label>
								<br />
								<asp:DropDownList CssClass="frmSelect" ID="ddlNumberOfRooms" runat="server"></asp:DropDownList>
							</p>
						</div>
						<div class="clear">&nbsp;</div>
					</div>
					<!-- /Common Controls -->
					<!-- Buttons -->
					<div id="FooterContainer">
						<div class="alignLeft">
							<span class="link">				
							    <asp:LinkButton ID="lnkDirectlyModifyContact" runat="server" OnClick="lnkModifyContact_Click"><%=WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/ModifyYourContactDetails")%></asp:LinkButton>			
							</span>
						</div>
						<div class="alignRight">
							<span class="btnSubmit">
							     <asp:LinkButton ID="btnModifyBooking" runat="server" OnClick="btnModifyBooking_Click"><span><%=WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/ModifyThisBooking")%></span></asp:LinkButton>	
							</span>
						</div>
						<div class="clear">&nbsp;</div>
					</div>
					<!-- Buttons -->
					<div class="clear">&nbsp;</div>
					
				    </div>
				    <!-- Calendar popup -->
					<div id="emptyDiv"></div>
				    <div id="hiddenCalendar" class="calendarContainer">
					    <table width="186" cellpadding="0" cellspacing="0" border="0">
						    <tr>
							    <td align="left" valign="top"><div id="calendarLayer">&nbsp;</div>
									    <div id="calendarBase"></div></td>
						    </tr>
					    </table>
				    </div>
    				
				    <script language="javascript" type="text/javascript">
				    initBookingCal();
				    </script>	  
			</asp:PlaceHolder>--%>
    <!-- /Reservation information -->
</div>
<div id="calendar">
    <uc2:Calendar id="Calendar1" runat="server" />
</div>
