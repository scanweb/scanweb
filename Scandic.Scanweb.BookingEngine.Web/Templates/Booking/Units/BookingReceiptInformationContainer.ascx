<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingReceiptInformationContainer.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.BookingReceiptInformationContainer" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<!-- Reservation information -->
<%
     if (Request.QueryString["command"] != null &&
         ((Request.QueryString["command"] == "print") ||
          (Request.QueryString["command"] == "CancelPageprint")))
     {%>
<!-- Page Title -->
<div id="divPrinterFriendly" visible="false" runat="server">
    <div class="section">
        <div>
            <table cellspacing="0" width="300px">
                <tbody>
                    <tr>
                        <td class="dataCol1 receiptResvNo">
                            <h3>
                                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/searchhotel/BookingReservationNumber") %>
                            </h3>
                        </td>
                        <td class="dataCol2">
                            <h3>
                            <%= ReservationNumberSessionWrapper.ReservationNumber %>
                            -
                            <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/BookingTypeSave") %>
                            </h3>
                        </td>
                        <td class="dataCol3">
                        </td>
                        <td class="dataCol4">
                    </tr>
                    <tr>
                        <td class="dataCol1">
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/Created") %>
                        </td>
                        <td class="dataCol2">
                        <span id ="CreateDate" runat="server"></span>
                         
                        </td>
                        <td class="dataCol3">
                        </td>
                        <td class="dataCol4">
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="row clearFix">
            <h4>
                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/ContactPerson") %></h4>
            <table cellspacing="0">
                <tbody>
                    <tr>
                        <td class="dataCol1">
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/Name") %>
                        </td>
                        <td class="dataCol2">
                            <span id="pfName" runat="server"></span>
                        </td>
                        <td class="dataCol3">
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/CardNumber") %>
                        </td>
                        <td class="dataCol4">
                            <span id="pfCardNumber" runat="server"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="dataCol1">
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/Country") %>
                        </td>
                        <td class="dataCol2">
                            <span id="pfCountry" runat="server"></span>
                        </td>
                        <td class="dataCol3">
                        </td>
                        <td class="dataCol4">
                        </td>
                    </tr>
                    <tr>
                        <td class="dataCol1">
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/Email") %>
                        </td>
                        <td class="dataCol2">
                            <span id="pfEmail" runat="server"></span>
                        </td>
                        <td class="dataCol3">
                        </td>
                        <td class="dataCol4">
                        </td>
                    </tr>
                    <tr>
                        <td class="dataCol1">
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/MobilePhone") %>
                        </td>
                        <td class="dataCol2">
                            <span id="pfMobilePhone" runat="server"></span>
                        </td>
                        <td class="dataCol3">
                        </td>
                        <td class="dataCol4">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="row clearFix">
            <h4>
                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/AboutTheBooking") %></h4>
            <table cellspacing="0">
                <tbody>
                    <tr>
                        <td class="dataCol1">
                            <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/Hotel") %>
                        </td>
                        <td class="dataCol2">
                            <span id="pfLblHotelName" runat="server"></span>
                        </td>
                        <td class="dataCol3">
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/NoOfNights")%>
                        </td>
                        <td class="dataCol4">
                            <span id="pfLblNoOfNights" runat="server"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="dataCol1">
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/ArrivalDate") %>
                        </td>
                        <td class="dataCol2">
                            <span id="pfLblCheckIn" runat="server"></span>
                        </td>
                        <td class="dataCol3">
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/NoOfRooms") %>
                        </td>
                        <td class="dataCol4">
                            <span id="pfLblNoOfRooms" runat="server"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="dataCol1">
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/DepartureDate") %>
                        </td>
                        <td class="dataCol2">
                            <span id="pfLblCheckOut" runat="server"></span>
                        </td>
                        <td class="dataCol3">
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/searchhotel/Occupants") %>
                        </td>
                        <td class="dataCol4">
                            <span id="pfLblNoOfAdultsValue" runat="server" style="display: none"></span><span
                                id="pfLblNoOfChildrenValue" runat="server" style="display: none"></span>
                            <% if (Convert.ToInt32(NoOfAdults) > 1 && Convert.ToInt32(NoOfChildren) > 0)
                               { %>
                            <%= NoOfAdults %>
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/searchhotel/adults") %>,
                            <% } %>
                            <%
                               else if (Convert.ToInt32(NoOfAdults) == 1 && Convert.ToInt32(NoOfChildren) > 0)
                               {%>
                            <%= NoOfAdults %>
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/searchhotel/Adult") %>,
                            <% } %>
                            <%
                               else if (Convert.ToInt32(NoOfAdults) > 1 && Convert.ToInt32(NoOfChildren) == 0)
                               {%>
                            <%= NoOfAdults %>
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/searchhotel/adults") %>
                            <% } %>
                            <%
                               else
                               {%>
                            <%= NoOfAdults %>
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/searchhotel/Adult") %>
                            <% } %>
                            <% if (Convert.ToInt32(NoOfChildren) > 1)
                                   { %>
                            <%= NoOfChildren %>
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/childrensdetails/children") %>
                            <% } %>
                            <%
                                   else if (Convert.ToInt32(NoOfChildren) == 1)
                                   {%>
                            <%= NoOfChildren %>
                            <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/childrensdetails/child") %>
                            <% } %>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- Room1 Information -->
        <div id="pfDivSummRoom1" runat="server" visible="false">
            <div class="row clearFix">
                <table cellspacing="0">
                    <tbody>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/bookingdetail/room1") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfRoom1No" runat="server"></span>
                            </td>
                            <td class="dataCol3">
                                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/selecthotel/sortRate") %>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblRatePlan1" runat="server"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/bookingdetail/RoomType") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfLblRoomCatName1" runat="server"></span>
                            </td>
                            <%--<td class="dataCol3">
                                <span id="pfRateRoom1" runat="server"></span>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblPricePerNight1" runat="server"><span id="pfPerRoom1" runat="server"></span>
                                </span><span id="pflblChargedAtHotelRoom1" runat="server"></span>
                            </td>--%>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Room2 Information -->
        <div id="pfDivSummRoom2" runat="server" visible="false">
            <div class="row clearFix">
                <table cellspacing="0">
                    <tbody>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/bookingdetail/room2") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfRoom2No" runat="server"></span>
                            </td>
                            <td class="dataCol3">
                                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/selecthotel/sortRate") %>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblRatePlan2" runat="server"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/bookingdetail/RoomType") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfLblRoomCatName2" runat="server"></span>
                            </td>
<%--                            <td class="dataCol3">
                                <span id="pfRateRoom2" runat="server"></span>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblPricePerNight2" runat="server"><span id="pfPerRoom2" runat="server"></span>
                                </span><strong><span id="pflblChargedAtHotelRoom2" runat="server"></span></strong>
                            </td>--%>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Room3 Information -->
        <div id="pfDivSummRoom3" runat="server" visible="false">
            <div class="row clearFix">
                <table cellspacing="0">
                    <tbody>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/bookingdetail/room3") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfRoom3No" runat="server"></span>
                            </td>
                            <td class="dataCol3">
                                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/selecthotel/sortRate") %>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblRatePlan3" runat="server"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/bookingdetail/RoomType") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfLblRoomCatName3" runat="server"></span>
                            </td>
<%--                            <td class="dataCol3">
                                <span id="pfRateRoom3" runat="server"></span>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblPricePerNight3" runat="server"><span id="pfPerRoom3" runat="server"></span>
                                </span><strong><span id="pflblChargedAtHotelRoom3" runat="server"></span></strong>
                            </td>--%>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Room4 Information -->
        <div id="pfDivSummRoom4" runat="server" visible="false">
            <div class="row clearFix">
                <table cellspacing="0">
                    <tbody>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/bookingdetail/room4") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfRoom4No" runat="server"></span>
                            </td>
                            <td class="dataCol3">
                                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/selecthotel/sortRate") %>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblRatePlan4" runat="server"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/bookingdetail/RoomType") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfLblRoomCatName4" runat="server"></span>
                            </td>
<%--                            <td class="dataCol3">
                                <span id="pfRateRoom4" runat="server"></span>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblPricePerNight4" runat="server"><span id="pfPerRoom4" runat="server"></span>
                                </span><strong><span id="pflblChargedAtHotelRoom4" runat="server"></span></strong>
                            </td>--%>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Total -->
        <div class="row totalWrap clearFix">
            <div>
                <table>
                    <tr>
                        <td class='dataCol1'>
                        </td>
                        <td class='dataCol2 receiptCol3'>
                        </td>
                        <td class='dataCol3'>
                            <asp:Table ID="totalValuesTable" runat="server" Width="177px">
                            </asp:Table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <!--Guarantee and deposit information -->
        <div class="row totalWrap clearFix">
            <div class="dataCol1">
                <strong>
                    <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/GuaranteeDepositInfoHeader")%></strong></div>
            <div class="dataCol2" id="pfDivGuaranteeInformation" runat="server">
            </div>
        </div>
        <div class="row totalWrap clearFix">
            <div class="dataCol1">
                <strong>
                    <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/preliminaryReceiptInfo")%></strong>
            </div>
        </div>
    </div>
</div>
<% } %>
