//  Description					: Code Behind class for Rate description and Rate Heading Control      //
//	This will hold controls for displaying Rate Heading on About our rate control.  			       //
//---------------------------------------------------------------------------------------------------- //
/// Author						: Ranajit Nayak                                   	                   //
/// Author email id				:                           							               //
/// Creation Date				: 24rd June  2010									                   //
///	Version	#					: 2.0													               //
///----------------------------------------------------------------------------------------------------//
/// Revison History				: -NA-													               //
///	Last Modified Date			:														               //
/////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Configuration;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// This class <see cref="RateDescription"/> contains the members to manipulate the rates. 
    /// </summary>
    public partial class RateDescription : System.Web.UI.UserControl
    {
        /// <summary>
        /// Sets rate description and Rate heading in Ratedescription control
        /// </summary>
        /// <param name="rateCategory"></param>
        /// <param name="categoryCount"></param>
        public void RateCaegoryDetails(RateCategory rateCategory, int categoryCount)
        {
            if (rateCategory != null)
            {
                rateInfo.Attributes.Add("class", "room" + (categoryCount + 1));
                string rateCategoryColor = rateCategory.RateCategoryColor;
                string defaultColorString = ConfigurationManager.AppSettings.Get("SelectRate.RateCategoryDefaultColor");

                if (!string.IsNullOrEmpty(rateCategoryColor))
                    h4Heading.Attributes.Add("class", rateCategoryColor);
                else if ((defaultColorString != null))
                    h4Heading.Attributes.Add("class", defaultColorString);
                else
                    h4Heading.Attributes.Add("class", string.Empty);

                rateHeading.InnerHtml = rateCategory.RateCategoryName;
                aboutRateDesc.Text = rateCategory.RateCategoryDescription;
            }
        }

        /// <summary>
        /// Rates the caegory details. It's a overloaded method of <see cref="RateCaegoryDetails"/>
        /// </summary>
        /// <param name="rateCategory">The rate category.</param>
        /// <param name="categoryCount">The category count.</param>
        public void RateCaegoryDetails(RateCategoryHeaderDisplay rateCategory, int categoryCount)
        {
            if (rateCategory != null)
            {
                rateInfo.Attributes.Add("class", "room" + (categoryCount + 1));
                string rateCategoryColor = rateCategory.RateCategoryColor;
                string defaultColorString = ConfigurationManager.AppSettings.Get("SelectRate.RateCategoryDefaultColor");

                if (!string.IsNullOrEmpty(rateCategoryColor))
                    h4Heading.Attributes.Add("class", rateCategoryColor);
                else if ((defaultColorString != null))
                    h4Heading.Attributes.Add("class", defaultColorString);
                else
                    h4Heading.Attributes.Add("class", string.Empty);

                SearchType searchType = SearchCriteriaSessionWrapper.SearchCriteria.SearchingType;
                rateHeading.InnerHtml = rateCategory.Title;
                if (!string.IsNullOrEmpty(rateHeading.InnerHtml))
                {
                    if (rateHeading.InnerHtml.Length > 30 && rateCategoryColor == "Blue")
                        h4Heading.Attributes.Add("style", "background-position: 0 -17px");
                    else if (rateHeading.InnerHtml.Length > 30 && rateCategoryColor == "Orange")
                        h4Heading.Attributes.Add("style", "background-position: 0 -110px");
                    else if (rateHeading.InnerHtml.Length > 30 && rateCategoryColor == "Green")
                        h4Heading.Attributes.Add("style", "background-position: 0 -41px");
                    else if (rateHeading.InnerHtml.Length > 30 && rateCategoryColor == "Brown")
                        h4Heading.Attributes.Add("style", "background-position: 0 -62px");
                }
                h4Heading.Attributes.Add("style", "margin-bottom: -18px");
                aboutRateDesc.Text = rateCategory.Description;
            }
        }

        /// <summary>
        /// Method to populate Rate Category Details
        /// </summary>
        /// <param name="block"></param>
        /// <param name="categoryCount"></param>
        public void RateCategoryDetails(Block block, int categoryCount)
        {
            if (block != null)
            {
                rateInfo.Attributes.Add("class", "room" + (categoryCount + 1));
                string categoryColor = block.BlockCategoryColor;
                string defaultColorString = ConfigurationManager.AppSettings.Get("SelectRate.RateCategoryDefaultColor");

                if (!string.IsNullOrEmpty(categoryColor))
                    h4Heading.Attributes.Add("class", categoryColor);
                else if ((defaultColorString != null))
                    h4Heading.Attributes.Add("class", defaultColorString);
                else
                    h4Heading.Attributes.Add("class", string.Empty);

                rateHeading.InnerHtml = block.BlockCategoryName;
                aboutRateDesc.Text = block.BlockDescription;
            }
        }
    }
}