using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System.Web.UI;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for RoomRateRowContainer
    /// </summary>
    public partial class RoomRateRowContainer : EPiServer.UserControlBase
    {
        /// <summary>
        /// This method will add a 
        /// new property named roomTeaserText and  roomCategoryTooltip according to new requirment.
        /// This will show teaser text and tooltip below the room Icon on select Rate description page
        /// </summary>
        /// <param name="rateDisplay"></param>
        /// <param name="rateCategorySlNo"></param>
        /// <param name="roomCategory"></param>
        /// <param name="rateCategories"></param>
        /// <param name="showPricePerStay"></param>
        /// <param name="roomRatesRow"></param>
        /// <param name="roomNumber"></param>
        /// <param name="roomTeaserText"></param>
        /// <param name="roomCategoryTooltip"> </param>
        /// <param name="radioButtonCount"> </param>
        public void SetRoomRateDisplay(BaseRateDisplay rateDisplay, int rateCategorySlNo,
                                       RoomCategoryEntity roomCategory, List<RateCategoryHeaderDisplay> rateCategories,
                                       bool showPricePerStay, ref TableRow roomRatesRow, int roomNumber,
                                       string roomTeaserText, string roomCategoryTooltip, ref int radioButtonCount)
        {
            TableCell newCell = new TableCell();
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(roomCategory.Name))
            {
                sb.Append("<h3>" + roomCategory.Name + "</h3>");
            }
            else
            {
                sb.Append("<h3>&#160;</h3>");
            }
            sb.Append("<div class=\"roomTypeToolTip\">");
            sb.Append("<a id=\"toolTipRoomType\" class=\"overlay jqModal scansprite\" href=\"#\"" + "rel=\"" + "." +
                      roomCategory.Id + "\"</a>");

            sb.Append(
                "<div class=\"ttContainer\"><div class=\"ttTop\"><span>&nbsp;</span></div><div class=\"ttMainContentWrapper\"><div class=\"ttContentWrapper\"><div class=\"ttContent\">" +
                roomCategoryTooltip +
                "</div></div></div><div class=\"ttBottom\"><span>&nbsp;</span></div><span class=\"ttpointer\">&nbsp;</span></div>");
            sb.Append("</div>");
            sb.Append("<div class=\"teaserDesc\">" + roomTeaserText + "</div>");

            newCell.Attributes.Add("class", "first");
            newCell.Text = sb.ToString();
            roomRatesRow.Cells.Add(newCell);
            int rateCategoriesCount = rateCategories.Count;
            for (int count = 0; count < rateCategoriesCount; count++)
            {
                RateCellDisplay newRateCellDisplay = rateDisplay.GetRateCellDisplay(roomCategory, rateCategorySlNo,
                                                                                    rateCategories[count].Id,
                                                                                    showPricePerStay, roomNumber);
                if (Utility.IsBlockCodeBooking)
                {
                    Block block = Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
					
                    if(block !=null)
                        DisplayColumn(rateDisplay, rateCategories[count].CoulmnNumber, newRateCellDisplay, showPricePerStay, ref roomRatesRow, roomNumber, ref radioButtonCount, roomCategory, rateCategories[count].Id, block.BlockCategoryColor,null,null, rateCategorySlNo, rateCategories[count].Title,block.HideARBPrice);
                    else
                        DisplayColumn(rateDisplay, rateCategories[count].CoulmnNumber, newRateCellDisplay, showPricePerStay, ref roomRatesRow, roomNumber, ref radioButtonCount, roomCategory, rateCategories[count].Id,null,null,null, rateCategorySlNo, rateCategories[count].Title,false);
                }
                else
                {
                    DisplayColumn(rateDisplay, rateCategories[count].CoulmnNumber, newRateCellDisplay, showPricePerStay, ref roomRatesRow, roomNumber, ref radioButtonCount, roomCategory, rateCategories[count].Id, rateCategories[count].RateCategoryColor, rateDisplay.RateCategoriesDisplay[count].RateHighlightTextWeb,rateDisplay.RateCategoriesDisplay[count].RateCategoryLanguage, rateCategorySlNo, rateCategories[count].Title, false);
                }

            }

        }

        /// <summary>
        /// Displays the list of bed type preferences associated to the RoomCategoryEntity
        /// 
        /// It will iterate through the list of all RoomTypes and gets the corresponding BedType description
        /// and displays it to the screen.
        /// </summary>
        /// <param name="roomCategory"></param>
        public void DisplayBedTypes(RoomCategoryEntity roomCategory)
        {
            List<string> roomTypes = roomCategory.RoomTypes;

            StringBuilder additionalHtml = new StringBuilder();
            for (int i = 0; i < roomTypes.Count; i++)
            {
                string roomTypeCode = roomTypes[i];
                if (0 != i)
                    additionalHtml.Append("<br/>");
                additionalHtml.Append(AppConstants.HYPHEN);
                additionalHtml.Append(AppConstants.SPACE);
                additionalHtml.Append(RoomRateUtil.GetRoomType(roomTypeCode).BedTypeDescription);
            }
        }

        /// <summary>
        /// This generic method is used for displaying rate coulmns.
        /// It is intelligent enough to put the rate at correct place.
        /// </summary>
        /// <param name="coulmnNumber">Coulmn number where this rate to be placed.</param>
        /// <param name="rateCell">Rate cell object</param>
        /// <param name="IsPerstay">false if pernight(default),true if perstay rate</param>
        /// <remarks>Created in Release 1.8.2 | hygiene Release | Rate structure</remarks>
        private void DisplayColumn(BaseRateDisplay rateDisplay, int coulmnNumber, RateCellDisplay rateCell, bool IsPerstay, ref TableRow roomRatesRow, int roomNumber, ref int intCount, RoomCategoryEntity roomCategory, string rateCategoryID, string rateCategoryColor,string rateHighlightText,string rateCategoryLanguage, int roomCategoryCount, string rateCategoryTitle, bool hideARBPrice)
        {
            BlockCodeRoomRateDisplay newBlockCodeRoomRateDisplay = null;
            try
            {
                newBlockCodeRoomRateDisplay = rateDisplay as BlockCodeRoomRateDisplay;
            }
            catch
            {
            }

            if (string.Empty != rateCell.RateString)
            {
                string disclaimerText = string.Empty;
                string approxText = string.Empty;
                //disclaimerText = String.Format("{0}{1}", "<b>Currency exchanges are provided for information only.</b>", "Reservation rates are only guranteed in local country's currency at the time of booking.");
                disclaimerText = String.Format("{0}{1}{2}{3}{4}",AppConstants.BOLDSTART_TAG,
                                                WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/disclaimertextline1"),
                                                AppConstants.BOLDEND_TAG,AppConstants.BREAK_TAG,
                                                WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/disclaimertextline2"));
                approxText = String.Format("{0}{1}", WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/about"), AppConstants.SPACE);
                TableCell newTableCell = new TableCell();
                newTableCell.Controls.Clear();
                Label lblRatehighlight = new Label();
                Label lblAlternateCurrnecy = new Label();
                if (!String.IsNullOrEmpty(rateCell.AlternateRateString))
                {
                    lblAlternateCurrnecy.Text = String.Format("{0}{1}", approxText, rateCell.AlternateRateString);
                    lblAlternateCurrnecy.CssClass = "alternateCurrency toolTipMe";

                    lblAlternateCurrnecy.Attributes.Add("title", disclaimerText);
                }
                
                lblRatehighlight.CssClass = "rateHighlightText";
                //if (!Utility.IsPageFallback())
                //{
                //    lblRatehighlight.Text = rateHighlightText;
                //}
                lblRatehighlight.Text = Utility.GetCurrentLanguage() == rateCategoryLanguage ? rateHighlightText : string.Empty;
                PlaceHolder phTableCell = new PlaceHolder();
                string defaultColorString = ConfigurationManager.AppSettings.Get("SelectRate.RateCategoryDefaultColor");

                if (!string.IsNullOrEmpty(rateCategoryColor))
                    newTableCell.Attributes.Add("rel", rateCategoryColor);
                else if ((defaultColorString != null))
                    newTableCell.Attributes.Add("rel", defaultColorString);
                else
                    newTableCell.Attributes.Add("rel", string.Empty);


                RadioButton newInputRadioButton = new RadioButton();

                newInputRadioButton.GroupName = "rateCellRadioGroup";
                newInputRadioButton.TabIndex = 40;
                newInputRadioButton.ID = "rateCellRadio" + roomNumber.ToString() + "-" + intCount.ToString();
                newInputRadioButton.Attributes.Add("OnClick", rateCell.BookUrl);
                newInputRadioButton.CssClass = rateCategoryColor;
                newInputRadioButton.AutoPostBack = true;
                if (hideARBPrice)
                {
                    newInputRadioButton.Text = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/prepaid");
                    phTableCell.Controls.Add(newInputRadioButton);
                }
                else
                {
                    newInputRadioButton.Text = rateCell.RateString;
                    phTableCell.Controls.Add(newInputRadioButton);
                    phTableCell.Controls.Add(lblAlternateCurrnecy);
                    phTableCell.Controls.Add(lblRatehighlight);
                }
                newTableCell.Controls.Add(phTableCell);
                roomRatesRow.Cells.Add(newTableCell);
                intCount++;

                string currentRoomCategoryId = roomCategory.Id;
                string currentRoomcategoryName = roomCategory.Name;
                SelectedRoomAndRateEntity selectedRoomRateEntity = null;
                
                string errorMsg = string.Empty;
                if (currentRoomCategoryId.Trim().ToLower().Equals(SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[roomNumber].SelectedRoomCategory.Trim().ToLower())
                    && (rateCategoryID.Trim().ToLower().Equals(SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[roomNumber].SelectedRateCategory.Trim().ToLower()))
                    || (currentRoomcategoryName.Trim().ToLower().Equals(SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[roomNumber].SelectedRoomCategory.Trim().ToLower()) &&
                    rateCategoryTitle.Trim().ToLower().Equals(SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[roomNumber].SelectedRateCategory.Trim().ToLower()))
                    )
                {
                    try
                    {
                        SelectedRoomAndRateEntity selectedRoomRate = SelectRoomUtil.SetRoomAndRateSelection(roomCategoryCount.ToString(), rateCategoryID, roomNumber.ToString());
                        errorMsg = SelectRoomUtil.BlockRoom(selectedRoomRate, roomNumber.ToString());

                        SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[roomNumber].SelectedRoomCategory = string.Empty;
                        SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[roomNumber].SelectedRateCategory = string.Empty;

                        if (!string.IsNullOrEmpty(errorMsg))
                        {
                        }
                    }
                    catch (Exception ex)
                    {
                        Scandic.Scanweb.Core.AppLogger.LogFatalException(ex, ex.Message);
                    }

                }
                if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null && HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.ContainsKey(roomNumber))
                {
                    selectedRoomRateEntity = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[roomNumber] as SelectedRoomAndRateEntity;

                }

                if (selectedRoomRateEntity != null)
                {
                    if (
                        (currentRoomCategoryId.Equals(selectedRoomRateEntity.RoomCategoryID))
                        && (rateCategoryID.Equals(selectedRoomRateEntity.RateCategoryID)
                        ))
                    {
                        if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REDEMPTION)
                        {
                            RedeemptionPointsEntity currentRedemptEntity = roomCategory.GetCommonPoints(rateCategoryID);
                            if (
                                (currentRedemptEntity != null)
                                && (selectedRoomRateEntity.RoomRates[0] != null)
                                && (selectedRoomRateEntity.RoomRates[0].PointsDetails != null)
                                && (currentRedemptEntity.Equals(selectedRoomRateEntity.RoomRates[0].PointsDetails))
                                )
                            {
                                newInputRadioButton.Checked = true;

                            }
                        }
                        else if (SearchCriteriaSessionWrapper.SearchCriteria != null && newBlockCodeRoomRateDisplay != null && newBlockCodeRoomRateDisplay.IsRateCodeNotAssociated)
                        {
                            if (IsPerstay)
                            {
                                RateEntity currentRateEntity = roomCategory.GetCommonTotalRateForPrepaidBlock(roomCategory.Name);
                                if (
                                (currentRateEntity != null)
                                && (selectedRoomRateEntity.RoomRates[0] != null)
                                && (selectedRoomRateEntity.RoomRates[0].TotalRate != null)
                                && ((currentRateEntity).Equals(selectedRoomRateEntity.RoomRates[0].TotalRate))
                                )
                                {

                                    newInputRadioButton.Checked = true;

                                }
                            }

                            else
                            {
                                RateEntity currentRateEntity = roomCategory.GetCommonBaseRateForPrepaidBlock(roomCategory.Name);
                                if (
                                (currentRateEntity != null)
                                && (selectedRoomRateEntity.RoomRates[0] != null)
                                && (selectedRoomRateEntity.RoomRates[0].BaseRate != null)
                                && ((currentRateEntity.Equals(selectedRoomRateEntity.RoomRates[0].BaseRate)))
                                )
                                {
                                    newInputRadioButton.Checked = true;

                                }

                            }
                        }
                        else
                        {
                            if (IsPerstay)
                            {
                                RateEntity currentRateEntity = roomCategory.GetCommonTotalRate(rateCategoryID);
                                if (
                                    (currentRateEntity != null)
                                    && (selectedRoomRateEntity.RoomRates[0] != null)
                                    && (selectedRoomRateEntity.RoomRates[0].TotalRate != null)
                                    && (currentRateEntity.Equals(selectedRoomRateEntity.RoomRates[0].TotalRate))
                                    )
                                {

                                    newInputRadioButton.Checked = true;

                                }
                            }
                            else
                            {
                                RateEntity currentRateEntity = roomCategory.GetCommonBaseRate(rateCategoryID);
                                if (
                                (currentRateEntity != null)
                                && (selectedRoomRateEntity.RoomRates[0] != null)
                                && (selectedRoomRateEntity.RoomRates[0].BaseRate != null)
                                && (currentRateEntity.Equals(selectedRoomRateEntity.RoomRates[0].BaseRate))
                                )
                                {
                                    newInputRadioButton.Checked = true;

                                }
                            }


                        }
                    }

                }
                else
                    newInputRadioButton.Checked = false;

            }
            else
            {
                TableCell newTableCell = new TableCell();
                HtmlGenericControl newSpan = new HtmlGenericControl("Span");
                newSpan.InnerHtml = "&nbsp;";
                newTableCell.Controls.Add(newSpan);
                roomRatesRow.Cells.Add(newTableCell);

            }

        }

        #region Page_Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible)
            {
            }
        }

        #endregion Page_Load
    }
}