////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: Code Behind class for Campaign Landing page Control	  //
//	                              This was added as part of CR implementation			  //
//                                Release 1.5 | artf809466 | FGP � Fast Track Enrolment   //  
//----------------------------------------------------------------------------------------//
//  Author						: Raj Kishore Marandi                                     //
//  Author email id				:                           							  //
//  Creation Date				: 15th Dec  2008     									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code behind of CampaignLanding control.
    /// </summary>
    public partial class CampaignLanding : EPiServer.UserControlBase
    {
        protected string defaultOption = string.Empty;
        #region Page_Load

        /// <summary>
        /// Page load of this user control.
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event object</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (!Page.IsPostBack)
                {
                    try
                    {
                        RememberUserIdPwd.Checked = true;
                        if (!UserLoggedInSessionWrapper.UserLoggedIn)
                        {
                            if (WebUtil.IsCookieExists(AppConstants.DONTREMEMBERME_COOKIE))
                                RememberUserIdPwd.Checked = false;
                            else
                            {
                                WebUtil.SetCookieToTextBox(txtmembershipnoprefix, txtmembershipno, txtPin, RememberUserIdPwd);
                            }
                        }
                        PopulateCampaignCode();
                        SetRadioButtonsAndTextBoxes();
                        BtnEnroll.Attributes.Add("onclick", "javascript:return performValidation(PAGE_CAMPAIGN_LANDING);");
                        spnEnroll.Attributes.Add("onclick", "javascript:return performValidation(PAGE_CAMPAIGN_LANDING);");
                    }
                    catch (Exception ex)
                    {
                        WebUtil.ApplicationErrorLog(ex);
                    }
                }
            }
        }

        #endregion Page_Load

        #region BtnEnroll_Click

        /// <summary>
        /// This will redirect the user the Frequent guest program landing page or Enroll page according to
        /// the radio button choosed
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event object</param>
        protected void BtnEnroll_Click(object sender, EventArgs e)
        {
            if (rdoNewMember.Checked)
            {
                FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.EnrolmentThroughCampaignLandingPage = true;
                Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.ENROLL_LOYALTY), false);
            }
            else
            {
                try
                {
                    string campaignCode = string.Empty;
                    string membershipNumber = txtmembershipnoprefix.Value.Trim() + txtmembershipno.Value.Trim();
                    string password = WebUtil.GetActualPassword(txtPin.Text.Trim(), membershipNumber);
                    bool isUpdateMembershipNeeded = false;
                    string membershipOperaId = string.Empty;

                    if (FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.PrePopulatePromotionCode)
                    {
                        campaignCode = FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.PromotionCode.Trim().ToUpper();
                    }
                    else
                    {
                        campaignCode = txtCampaignCode.Value.Trim().ToUpper();
                    }

                    NameController nameController = new NameController();

                    try
                    {
                        nameController.Login(membershipNumber, password);
                        isUpdateMembershipNeeded = true;
                    }
                    catch (Exception ex)
                    {
                        AppLogger.LogInfoMessage("Invalid user trying to login in Campaign landing page. " + ex.Message);
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.LOGIN_ERROR), false);
                    }

                    if (isUpdateMembershipNeeded)
                    {
                        try
                        {
                            membershipOperaId = LoyaltyDetailsSessionWrapper.LoyaltyDetails.MembershipID;
                            nameController.UpdateMembershipWithPromoCode(membershipOperaId, campaignCode);
                            FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage =
                                WebUtil.GetTranslatedText(FastTrackEnrolmentMessageConstants.SuccessfulSubscription);
                        }
                        catch (OWSException owsException)
                        {
                            AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);

                            if (owsException.TranslatePath == AppConstants.SITE_WIDE_ERROR)
                            {
                                FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage =
                                    WebUtil.GetTranslatedText(FastTrackEnrolmentMessageConstants.PromoCodeGenericMessageForExistingUser);
                            }
                            else
                            {
                                if (owsException.TranslatePath == FastTrackEnrolmentMessageConstants.InvalidPromoCodeNewMember)
                                {
                                    FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage =
                                        WebUtil.GetTranslatedText(FastTrackEnrolmentMessageConstants.InvalidPromoCodeExistingMember);
                                }
                                else
                                {
                                    FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage =
                                        WebUtil.GetTranslatedText(owsException.TranslatePath);
                                }
                            }
                        }
                        catch (BusinessException busEx)
                        {
                            AppLogger.LogCustomException(busEx, AppConstants.BUSINESS_EXCEPTION);
                            FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage = busEx.Message;
                        }
                        catch (Exception ex)
                        {
                            AppLogger.LogFatalException(ex);
                            FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage = ex.Message;
                        }
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MYACCOUNT_BOOKING_PAGE), false);
                    }
                }
                catch (Exception ex)
                {
                    WebUtil.ApplicationErrorLog(ex);
                }
            }
        }

        #endregion BtnEnroll_Click

        #region Private Methods

        #region GetPromoCodeAndVisibilityStatus

        /// <summary>
        /// Gets the campaign code and visibility status of this campaign code.
        /// </summary>
        /// <param name="campaignCode">ref parameter which will hold the campaign code</param>
        /// <returns>Wheather to display this code in site or not</returns>
        private bool GetPromoCodeAndVisibilityStatus(ref string promotionCode)
        {
            bool status = false;

            if (null != CurrentPage["campaigncode"])
            {
                promotionCode = CurrentPage["campaigncode"].ToString();
            }

            if (null != CurrentPage["prepopulate"])
            {
                status = true;
            }
            return status;
        }

        #endregion GetPromoCodeAndVisibilityStatus

        #region PopulateCampaignCode

        /// <summary>
        /// This will populate the camaign code.
        /// </summary>
        private void PopulateCampaignCode()
        {
            string promotionCode = string.Empty;
            bool prePopulateCampaignCode = true;
            FastTrackEnrolmentEntity fastTrackEnrolmentEntity = new FastTrackEnrolmentEntity();
            prePopulateCampaignCode = GetPromoCodeAndVisibilityStatus(ref promotionCode);
            hdnCampaignCode.Value = promotionCode;
            fastTrackEnrolmentEntity.PromotionCode = promotionCode;
            fastTrackEnrolmentEntity.PrePopulatePromotionCode = prePopulateCampaignCode;
            FastTrackEnrolmentSessionWrapper.FastTrackEnrolment = fastTrackEnrolmentEntity;

            if (prePopulateCampaignCode)
            {
                txtCampaignCode.Value = promotionCode;
                hdnPrepopulateCampaignCode.Value = "1";
                txtCampaignCode.Disabled = true;
            }
            else
            {
                txtCampaignCode.Value = string.Empty;
                hdnPrepopulateCampaignCode.Value = "0";
                txtCampaignCode.Disabled = false;
            }
        }

        #endregion PopulateCampaignCode

        #region SetRadioButtonsAndTextBoxes

        /// <summary>
        /// Sets all the radio button and text boxes to its default state and default value.
        /// </summary>
        private void SetRadioButtonsAndTextBoxes()
        {
            //bool loginControlVisible = true;
            rdoNewMember.Checked = true;
            if (Request.QueryString["Default"] != null)
            {
                if (Convert.ToString(Request.QueryString["Default"]).ToUpper() == "EM")
                {
                    rdoAlreadyMember.Checked = true;
                    rdoNewMember.Checked = false;
                    defaultOption = "existingmember";
                }
            }
        }

        #endregion SetRadioButtonsAndTextBoxes

        #endregion Private Methods
    }
}