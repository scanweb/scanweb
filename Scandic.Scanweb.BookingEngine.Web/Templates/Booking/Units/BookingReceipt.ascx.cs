#region System Namespaces

using System;
using System.Collections.Generic;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Core;

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class of Booking receipt. It displays
    /// the booking receipt details 
    /// </summary>
    public partial class BookingReceipt : EPiServer.UserControlBase
    {
        #region Protected Methods

        /// <summary>
        /// Page Load Method of Booking receipt Control
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="e">
        /// Arguments for the Event
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Core.AppLogger.LogInfoMessage("BookingReceiptASCX:Page_Load() StartTime:::" +
                                                DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
              if ((HttpContext.Current.Session != null && HttpContext.Current.Session.IsNewSession) ||
                  (string.IsNullOrEmpty(ReservationNumberSessionWrapper.ReservationNumber)))
              {
                  string cookieHeader = HttpContext.Current.Request.Headers["Cookie"];
                  if (!string.IsNullOrEmpty(cookieHeader) && cookieHeader.IndexOf("ASP.NET_SessionId") >= 0)
                  {
                      Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE), true);
                  }
              }
              else
              {
                  if (!IsPostBack)
                  {
                      if (this.Visible)
                      {
                          ReservationController reservationController = new ReservationController();
                          List<BookingDetailsEntity> bookingDetailsList =
                              reservationController.FetchBookingSummary(
                                  ReservationNumberSessionWrapper.ReservationNumber);

                          LogRateMismatchInfo(bookingDetailsList);

                          BookingReceiptInformation.PopulateUi(bookingDetailsList);
                      }
                  }

                  if (Request.QueryString["command"] != null)
                  {
                      prntWrapper.Attributes.Add("class", "mgnLft20 prntWrapper");
                      if (Request.QueryString["isPDF"] != null && Request.QueryString["isPDF"] == "true")
                      {
                          scandicLogo.Visible = true;

                      }
                  }
              }
            Core.AppLogger.LogInfoMessage("BookingReceiptASCX:Page_Load() EndTime:::" +
                                                          DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
        }

        #endregion

        private void LogRateMismatchInfo(List<BookingDetailsEntity> bookingDetailsList)
        {
            try
            {

                foreach (BookingDetailsEntity bkngDetails in bookingDetailsList)
                {
                    RateCategory rateCategory = null;

                    //rateCategory is required to find wheather the rate present in current bookingDetailsEntity is a save rate or not.
                    //below logic should only be executed if the rateCategory is a save rateCategory.
                    if (bkngDetails.HotelRoomRate != null)
                        rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bkngDetails.HotelRoomRate.RatePlanCode);

                    if (rateCategory != null && RoomRateUtil.IsSaveRateCategory(rateCategory.RateCategoryId))
                    {
                        double depositAmount = bkngDetails.HotelRoomRate.DepositAmount != null ? bkngDetails.HotelRoomRate.DepositAmount.Rate : 0.0;
                        double totalRate = bkngDetails.HotelRoomRate.TotalRate != null ? bkngDetails.HotelRoomRate.TotalRate.Rate : 0.0;
                        double currentBalance = bkngDetails.HotelRoomRate.CurrentBalance != null ? bkngDetails.HotelRoomRate.CurrentBalance.Rate : 0.0;
                        double diffInPayment = currentBalance + totalRate;

                        if (depositAmount != totalRate || diffInPayment != 0.0)
                        {
                            string messageToLog = string.Empty;

                            if (bkngDetails.HotelSearch != null && bkngDetails.HotelSearch.SearchedFor != null)
                                messageToLog = string.Format("HotelId:{0} | HotelName:{1} | ResvNumber:{2} | DepositAmount:{3}| TotalRate:{4} | CurrentBalance:{5}",
                                                    bkngDetails.HotelSearch.SearchedFor.SearchCode, bkngDetails.HotelSearch.SearchedFor.SearchString, bkngDetails.ReservationNumber,
                                                    depositAmount, totalRate, currentBalance);

                            else
                                messageToLog = string.Format("ResvNumber:{0} | DepositAmount:{1}| TotalRate:{2} | CurrentBalance:{3}",
                                                  bkngDetails.ReservationNumber, depositAmount, totalRate, currentBalance);

                            AppLogger.LogReservationRatesInfoLogger(messageToLog);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, ex.Message);
            }

        }

    }
}