//  Description					: Code Behind class for Refine Booking Search Control	  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Girish Krishnanta                                   	  //
/// Author email id				:                           							  //
/// Creation Date				: 13th December  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
#endregion

#region Scandic Namespaces
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Entity;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.ExceptionManager;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    public partial class RefineBooking : EPiServer.UserControlBase
    {
        #region Page Events
        /// <summary>
        /// This method populates the control with hotelsearch information from sesssion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetDropDowns();                
                HotelSearchEntity hotelSearch = SessionWrapper.SearchCriteria as HotelSearchEntity;
                txtArrivalDate.Value = Core.DateUtil.DateToDDMMYYYYString(hotelSearch.ArrivalDate);
                txtDepartureDate.Value = Core.DateUtil.DateToDDMMYYYYString(hotelSearch.DepartureDate);
                txtNoOfNights.Value = hotelSearch.NoOfNights.ToString();
                ddlAdultsPerRoom.Text  = hotelSearch.AdultsPerRoom.ToString();
                ddlChildPerRoom.Text = hotelSearch.ChildrenPerRoom.ToString();            
            }
        }

        /// <summary>
        /// This methods update the session information with user inputs and 
        /// redirect accordingly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Search_Click(object sender, EventArgs e)
        {
            try
            {
                SessionWrapper.CleanSessionSearchResults();
                HotelSearchEntity hotelSearchEntity = GetHotelSearchEntity();
                SessionWrapper.SearchCriteria = hotelSearchEntity;
                if (hotelSearchEntity.SearchedFor != null)
                {
                    Response.Redirect("ModifySelectRate.aspx", false);
                }
                else
                {
                    ShowErrorMessage(WebUtil.GetTranslatedText
                        ("/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name"));
                }
            }
            catch (Exception GenEx)
            {
                ExceptionManager.Utils.LogException(GenEx, AppConstants.GENERAL_EXCEPTION, string.Empty);                                
                WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                            WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
            }
        }
        #endregion PageEvents

        #region Private Methods

        /// <summary>
        /// Get the hotel search information from the session and updates it 
        /// with the user inputs
        /// </summary>
        /// <returns></returns>
        private HotelSearchEntity GetHotelSearchEntity()
        {
            HotelSearchEntity hotelSearch = SessionWrapper.SearchCriteria as HotelSearchEntity;
            if (hotelSearch != null)
            {
                hotelSearch.ArrivalDate = Core.DateUtil.StringToDDMMYYYDate(txtArrivalDate.Value);
                hotelSearch.DepartureDate = Core.DateUtil.StringToDDMMYYYDate(txtDepartureDate.Value);
                hotelSearch.NoOfNights = int.Parse(txtNoOfNights.Value);
                hotelSearch.AdultsPerRoom = int.Parse(ddlAdultsPerRoom.Text);
                hotelSearch.ChildrenPerRoom = int.Parse(ddlChildPerRoom.Text);
            }
            return hotelSearch;
        }

        /// <summary>
        /// Display the Error message
        /// </summary>
        /// <param name="errorMessage"></param>
        private void ShowErrorMessage(string errorMessage)
        {
            clientErrorDiv.Attributes.Add("class", "errorText");

            string start = "<div id='errorReport' class='formGroupError'><div class='redAlertIcon'>"
                + WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") 
                + "</div><ul class='circleList'>";
            string end = "</ul></div>";
            clientErrorDiv.InnerHtml = start + errorMessage + end;
        }

        /// <summary>
        /// Populate the Drop Down
        /// </summary>
        private void SetDropDowns()
        {
            /*******************************************************************************************
            * Load Comboboxes on form
            *******************************************************************************************/
            ddlAdultsPerRoom.Items.Add(AppConstants.ONE);
            ddlAdultsPerRoom.Items.Add(AppConstants.TWO);
            ddlAdultsPerRoom.Items.Add(AppConstants.THREE);
            ddlAdultsPerRoom.Items.Add(AppConstants.FOUR);
            ddlAdultsPerRoom.Items.Add(AppConstants.FIVE);
            ddlAdultsPerRoom.Items.Add(AppConstants.SIX);

            ddlChildPerRoom.Items.Add(AppConstants.ZERO);
            ddlChildPerRoom.Items.Add(AppConstants.ONE);
            ddlChildPerRoom.Items.Add(AppConstants.TWO);
            ddlChildPerRoom.Items.Add(AppConstants.THREE);
            ddlChildPerRoom.Items.Add(AppConstants.FOUR);
            ddlChildPerRoom.Items.Add(AppConstants.FIVE);
        }
        #endregion Private Methods             
    }
}