using System;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// EmailConfirmation
    /// </summary>
    public partial class EmailConfirmation : EPiServer.UserControlBase
    {

        /// <summary>
        /// Initialize Header of the Email confirmation user control
        /// </summary>
        /// <param name="headerText">Contains  the text to be displayed</param>
        public void AssignHeader(string headerText)
        {
            header.InnerText = headerText;
        }

        /// <summary>
        /// Initialize Header of the Email confirmation user control
        /// </summary>
        /// <param name="messageText">Contains the text to be displayed as message</param>
        public void AssignMessage(string messageText)
        {
            message.InnerText = messageText;
        }

        /// <summary>
        /// Initialize Link text for the Email confirmation user control.
        /// </summary>
        /// <param name="linkText">Contains the text to be displayed as link</param>
        public void AssignLink(string linkText)
        {
            lnkbutton.Text = linkText;
        }

        /// <summary>
        /// lnkbutton_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbutton_Click(object sender, EventArgs e)
        {
            string redirectUrl = string.Empty;
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.OFFERS_OVERVIEW), false);
        }
    }
}