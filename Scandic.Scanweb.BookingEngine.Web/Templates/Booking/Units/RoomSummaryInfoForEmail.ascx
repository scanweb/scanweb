<%@ Control Language="C#" AutoEventWireup="true" Codebehind="RoomSummaryInfoForEmail.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.RoomSummaryInfoForEmail" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>

<div id="roomSummaryInfo" runat="server">
    <table width="505px" border="0" cellspacing="0" cellpadding="0" style="margin-left: 3px;
        margin-right: 3px;">
        <!--<tr>
            <td align="left" valign="top" style="margin-left: 3px;">
                <asp:Image ID="greyBoxTop" runat="server" Style="display: block;" Width="504" Height="2" />
            </td>
        </tr>-->
        <tr>
            <td align="left" valign="top">
                <table width="505px" border="0" cellspacing="0" cellpadding="0" style="background-color: #f0f0f0;">
                    <tr runat="server">
                        <td align="left" valign="top" width="85" style="padding-bottom: 12px; padding-left: 15px;
                            padding-top: 5px; color: #002052;">
                            <strong><label id="roomLabel" runat="server"></label></strong>
                        </td>
                        <td align="left" valign="top" style="vertical-align: top; padding-top: 5px; width: 110px;">
                            <label id="reservationLeglabel" runat="server"></label>
                        </td>
                        <td align="left" valign="top" width="130" style="padding-left: 15px; padding-top: 5px;">
                            <strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/sortRate") %></strong>
                        </td>
                        <td align="left" valign="top" style="padding-left: 15px; padding-top: 5px;">
                            <label id="roomRateCategory" runat="server">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-bottom: 12px; padding-left: 15px">
                            <strong> <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/RoomType") %>:</strong>
                         </td>
                         <td style="vertical-align: top;" valign="top">
                            <span id="roomTypeID" runat="server"></span>
                         </td>
                        <td align="left" valign="top" style="padding-left: 15px">
                        <strong>
                            <label id="priceInfo" runat="server">                            
                            </label>
                         </strong>
                         </td>
                        <td align="left" valign="top" style="padding-bottom: 12px; padding-left: 15px">
                            <span style="font-size: 15px;" id="priceString" runat="server"></span> 
                            <span id="lblChargedAtHotel" runat="server"></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="margin-left: 3px; margin-right: 3px;">
              <asp:Image id="grayBoxBottom" runat="server" style="display: block;" width="504" height="2" />
                     
           </td>
        </tr>
    </table>
</div>
