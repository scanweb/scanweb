<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProgressBar.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ProgressBar" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%--<input type="hidden" id="ClientPage" runat="server"/>--%>
<div id="stepIndecator" class="mgnTop30" >
	<ul id ="breadCrumb" runat="server">
		<li id="breadCrumbSelectHotel" runat="server"><a runat="server" id="ptOne"><span id ="Span1" runat ="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selecthotel") %></span></a></li>
		<li id="breadCrumbSelectRate" runat="server"><a id="ptTwo" runat="server"><span id="Span2" runat="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selectrate") %> </span></a></li>
		<li  id="breadCrumbBookingDetails" runat="server"><a runat="server" id="ptThree"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/bookingdetails") %></span></a></li>
		<li id ="breadCrumbPayment" runat="server" visible="false"><a runat="server" id="ptPayment"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/payment") %></span></a></li>
		<li id ="breadCrumbConfirmation" runat="server"><a class="last" runat="server" id="ptFour"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/confirmation") %></span></a></li>
	</ul>
</div>