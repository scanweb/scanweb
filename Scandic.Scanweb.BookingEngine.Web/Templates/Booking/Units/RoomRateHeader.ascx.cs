using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for ReservationInformationContainer
    /// </summary>
    public partial class RoomRateHeader : EPiServer.UserControlBase
    {
        private string currentLanguage = Scandic.Scanweb.BookingEngine.Web.Utility.GetCurrentLanguage();

        /// <summary>
        /// SetHeader
        /// </summary>
        /// <param name="rateCategoryHeaderList"></param>
        /// <param name="roomRatesTable"></param>
        public void SetHeader(List<RateCategoryHeaderDisplay> rateCategoryHeaderList, ref Table roomRatesTable)
        {
            RearrangeCoulmnNumber(rateCategoryHeaderList);

            TableHeaderRow headerRow = new TableHeaderRow();
            AddHeaderRow(rateCategoryHeaderList, ref headerRow);
            roomRatesTable.Rows.Add(headerRow);
        }

        /// <summary>
        /// This method will rearrange the coulmn number.
        /// </summary>
        /// <param name="headerList">Header list for different category</param>
        private void RearrangeCoulmnNumber(List<RateCategoryHeaderDisplay> headerList)
        {
            string totalCoulmnAccepted = ConfigurationManager.AppSettings.Get("SelectRate.NoOfRateCoulmn");
            bool isCoulmnNumberValidFormat = false;
            int totalCoulmnConfigured = 0;
            if (!string.IsNullOrEmpty(totalCoulmnAccepted))
            {
                isCoulmnNumberValidFormat = int.TryParse(totalCoulmnAccepted, out totalCoulmnConfigured);
            }
            int headerCount = headerList.Count;
            if (IsPromoSearch() && headerList.Count >= 3)
            {
                foreach (var item in headerList)
                {
                    if (item.RateCategoryColor.ToUpper() != AppConstants.EarlyColor &&
                        item.RateCategoryColor.ToUpper() != AppConstants.FlexColor)
                        item.CoulmnNumber = 1;
                }
            }
            headerList.Sort(new SortHeaderDisplay());
            for (int count = 0; count < headerCount; count++)
            {
                headerList[count].CoulmnNumber = totalCoulmnConfigured;
                totalCoulmnConfigured--;
            }

            headerList.Sort(new RateCategoryCoulmnComparer());
        }

        /// <summary>
        /// To check if public rate search is on for promo search
        /// </summary>
        /// <returns></returns>
        private static bool IsPromoSearch()
        {
            bool bPromoSearch = false;
            if (Convert.ToBoolean(AppConstants.ENABLE_PUBLIC_RATE_SEARCH_FOR_PROMOTIONS))
            {
                if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REGULAR &&
                    !string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode))
                {
                    bPromoSearch = true;
                }
            }
            return bPromoSearch;
        }

        /// <summary>
        /// Truncate
        /// </summary>
        /// <param name="source"></param>
        /// <param name="length"></param>
        /// <returns>Truncated string</returns>
        public static string Truncate(string source, int length)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length);
            }
            return source;
        }

        /// <summary>
        /// This method will dynamically find out the the links and assign them value.
        /// It is intelligent enough to find out which header to display where
        /// </summary>
        /// <param name="headerList">List of header display object</param>
        /// <param name="headerRow"></param>
        private void AddHeaderRow(List<RateCategoryHeaderDisplay> headerList, ref TableHeaderRow headerRow)
        {
            const string COULMN = "rateCategoryColumn";
            string totalCoulmnAccepted = ConfigurationManager.AppSettings.Get("SelectRate.NoOfRateCoulmn");
            bool isCoulmnNumberValidFormat = false;
            int totalCoulmnConfigured = 0;
            if (!string.IsNullOrEmpty(totalCoulmnAccepted))
            {
                isCoulmnNumberValidFormat = int.TryParse(totalCoulmnAccepted, out totalCoulmnConfigured);
            }

            TableHeaderCell staticTableCell = new TableHeaderCell();
            StringBuilder staticCellText = new StringBuilder();
            if (currentLanguage == CurrentPageLanguageConstant.LANGAUGE_RUSSIAN || Utility.CheckifFallback())
            {
                staticCellText.Append("<h2 class=\"stoolHeadingAlternate\">");
            }
            else
            {
                staticCellText.Append("<h2 class=\"stoolHeading\">");
            }
            staticCellText.Append(WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RoomType"));
            staticCellText.Append("</h2>");
            staticTableCell.Text = staticCellText.ToString();

            staticTableCell.ID = "rateCategoryColumnStatic";
            staticTableCell.Attributes.Add("align", "left");
            staticTableCell.Attributes.Add("class", "first sprite");
            staticTableCell.Attributes.Add("valign", "middle");

            headerRow.Cells.Add(staticTableCell);
            bool isBlockCodeBooking = Utility.IsBlockCodeBooking;
            int headerCount = headerList.Count;
            for (int count = 0; count < headerCount; count++)
            {
                TableHeaderCell newTableCell = new TableHeaderCell();

                HtmlAnchor tooltip = new HtmlAnchor();
                tooltip.Attributes.Add("class", "help scansprite");
                tooltip.Attributes.Add("title", string.Empty);
                tooltip.Attributes.Add("rel", "#corporate");
                tooltip.HRef = string.Empty;
                newTableCell.Controls.Add(tooltip);
                HtmlGenericControl RateCategoryName = new HtmlGenericControl("span");
                HtmlGenericControl DynamicRateHeader1 = new HtmlGenericControl("span");
                HtmlGenericControl DynamicRateHeader2 = new HtmlGenericControl("span");
                HtmlGenericControl DynamicRateHeader3 = new HtmlGenericControl("span");
                SearchType searchType = SearchCriteriaSessionWrapper.SearchCriteria.SearchingType;
                RateCategoryName.InnerHtml = headerList[count].Title;
                string headerText1 = string.Empty;
                string headerText2 = string.Empty;
                string headerText3 = string.Empty;
                switch (searchType)
                {
                    case SearchType.REGULAR:
                    case SearchType.VOUCHER:
                    case SearchType.BONUSCHEQUE:
                    case SearchType.REDEMPTION:
                    case SearchType.CORPORATE:
                        {
                            if (searchType == SearchType.CORPORATE &&
                                    (isBlockCodeBooking ||
                                        (!headerList[count].RateCategoryColor.ToUpper().Equals("ORANGE") && !headerList[count].RateCategoryColor.ToUpper().Equals("BLUE"))
                                    )
                               )
                            {
                                RateCategoryName.InnerHtml = headerList[count].Title;
                            }
                            else
                            {

                                if (!string.IsNullOrEmpty(headerList[count].Headers[0]) ||
                                    !string.IsNullOrEmpty(headerList[count].Headers[1])
                                    || !string.IsNullOrEmpty(headerList[count].Headers[2]))
                                {
                                    headerText1 = headerList[count].Headers[0];
                                    DynamicRateHeader1.InnerHtml = string.IsNullOrEmpty(headerText1) ? string.Empty : headerText1.ToString();

                                    headerText2 = headerList[count].Headers[1];
                                    DynamicRateHeader2.InnerHtml = string.IsNullOrEmpty(headerText2) ? string.Empty : headerText2.ToString();

                                    headerText3 = headerList[count].Headers[2];
                                    DynamicRateHeader3.InnerHtml = string.IsNullOrEmpty(headerText3) ? string.Empty : headerText3.ToString();
                                }
                                else
                                {
                                    RateCategoryName.InnerHtml = headerList[count].Title;
                                }
                            }
                            break;
                        }
                    //case SearchType.VOUCHER:
                    //    {
                    //        if (!string.IsNullOrEmpty(headerList[count].Headers[0]) ||
                    //            !string.IsNullOrEmpty(headerList[count].Headers[1])
                    //            || !string.IsNullOrEmpty(headerList[count].Headers[2]))
                    //        {
                    //            headerText1 = headerList[count].Headers[0];
                    //            DynamicRateHeader1.InnerHtml = headerText1.ToString();

                    //            headerText2 = headerList[count].Headers[1];
                    //            DynamicRateHeader2.InnerHtml = headerText2.ToString();

                    //            headerText3 = headerList[count].Headers[2];
                    //            DynamicRateHeader3.InnerHtml = headerText3.ToString();
                    //        }
                    //        else
                    //        {
                    //            RateCategoryName.InnerHtml = headerList[count].Title;
                    //        }
                    //        break;
                    //    }
                    //case SearchType.CORPORATE:
                    //    {
                    //        if (Utility.IsBlockCodeBooking)
                    //        {
                    //            RateCategoryName.InnerHtml = headerList[count].Title;
                    //        }
                    //        else
                    //        {
                    //            //if (headerList[count].RateCategoryColor.ToUpper().Equals("ORANGE") ||
                    //            //    headerList[count].RateCategoryColor.ToUpper().Equals("BLUE"))
                    //            //{
                    //                if (!string.IsNullOrEmpty(headerList[count].Headers[0]) ||
                    //                    !string.IsNullOrEmpty(headerList[count].Headers[1])
                    //                    || !string.IsNullOrEmpty(headerList[count].Headers[2]))
                    //                {
                    //                    headerText1 = headerList[count].Headers[0];
                    //                    DynamicRateHeader1.InnerHtml = headerText1.ToString();
                    //                    headerText2 = headerList[count].Headers[1];
                    //                    DynamicRateHeader2.InnerHtml = headerText2.ToString();

                    //                    headerText3 = headerList[count].Headers[2];
                    //                    DynamicRateHeader3.InnerHtml = headerText3.ToString();
                    //                }
                    //                else
                    //                {
                    //                    RateCategoryName.InnerHtml = headerList[count].Title;
                    //                }
                    //            //}
                    //            //else
                    //            //    RateCategoryName.InnerHtml = headerList[count].Title;
                    //        }
                    //        break;
                    //    }
                    //case SearchType.BONUSCHEQUE:
                    //    {
                    //        if (!string.IsNullOrEmpty(headerList[count].Headers[0]) ||
                    //            !string.IsNullOrEmpty(headerList[count].Headers[1])
                    //            || !string.IsNullOrEmpty(headerList[count].Headers[2]))
                    //        {
                    //            headerText1 = headerList[count].Headers[0];
                    //            DynamicRateHeader1.InnerHtml = headerText1.ToString();

                    //            headerText2 = headerList[count].Headers[1];
                    //            DynamicRateHeader2.InnerHtml = headerText2.ToString();

                    //            headerText3 = headerList[count].Headers[2];
                    //            DynamicRateHeader3.InnerHtml = headerText3.ToString();
                    //        }
                    //        else
                    //        {
                    //            RateCategoryName.InnerHtml = headerList[count].Title;
                    //        }
                    //        break;
                    //    }
                    //case SearchType.REDEMPTION:
                    //    {
                    //        if (!string.IsNullOrEmpty(headerList[count].Headers[0]) ||
                    //            !string.IsNullOrEmpty(headerList[count].Headers[1])
                    //            || !string.IsNullOrEmpty(headerList[count].Headers[2]))
                    //        {
                    //            headerText1 = headerList[count].Headers[0];
                    //            DynamicRateHeader1.InnerHtml = headerText1.ToString();

                    //            headerText2 = headerList[count].Headers[1];
                    //            DynamicRateHeader2.InnerHtml = headerText2.ToString();

                    //            headerText3 = headerList[count].Headers[2];
                    //            DynamicRateHeader3.InnerHtml = headerText3.ToString();
                    //        }
                    //        else
                    //        {
                    //            RateCategoryName.InnerHtml = headerList[count].Title;
                    //        }
                    //        break;
                    //    }
                    default:
                        break;
                }
                if ((headerText1 == string.Empty) && (headerText2 == string.Empty) && (headerText3 == string.Empty))
                {
                    newTableCell.Controls.Add(RateCategoryName);
                }
                if (!string.IsNullOrEmpty(DynamicRateHeader1.InnerHtml))
                {
                    newTableCell.Controls.Add(DynamicRateHeader1);
                }
                if (!string.IsNullOrEmpty(DynamicRateHeader2.InnerHtml))
                {
                    newTableCell.Controls.Add(DynamicRateHeader2);
                }
                if (!string.IsNullOrEmpty(DynamicRateHeader3.InnerHtml))
                {
                    newTableCell.Controls.Add(DynamicRateHeader3);
                }

                HtmlGenericControl earlyRate = new HtmlGenericControl("span");
                earlyRate.Attributes.Add("Class", "m15_ToolTip");
                earlyRate.Attributes.Add("id", "corporate");
                StringBuilder InnerDiv = new StringBuilder();
                InnerDiv.Append("<span class=\"hd ratesSprite\"></span>");
                InnerDiv.Append("<span class=\"cnt\">");
                string aboutOurRateDesc = CurrentPage["AboutOurRateDescription"] as string ?? string.Empty;
                string abtOurRateDivOpenTag = "<div class='selectRoomTTSection'>";
                string closeDivTag = "</div>";
                InnerDiv.Append(abtOurRateDivOpenTag);
                InnerDiv.Append(aboutOurRateDesc);
                InnerDiv.Append(closeDivTag);
                string headerDivOpenTag = "<div class='selectRoomTTSection selectRoomTTSectionDesc'>";
                InnerDiv.Append(headerDivOpenTag);
                InnerDiv.Append(headerList[count].Description);
                InnerDiv.Append(closeDivTag);

                InnerDiv.Append("</span>");
                InnerDiv.Append("<span class=\"ft ratesSprite\"></span>");
                earlyRate.InnerHtml = InnerDiv.ToString();
                tooltip.Controls.Add(earlyRate);

                newTableCell.ID = "rateCategoryColumn" + count.ToString();
                string rateCategoryColor = headerList[count].RateCategoryColor;
                string defaultColorString = ConfigurationManager.AppSettings.Get("SelectRate.RateCategoryDefaultColor");

                if (!string.IsNullOrEmpty(rateCategoryColor))
                    newTableCell.Attributes.Add("class", rateCategoryColor);
                else if ((defaultColorString != null))
                    newTableCell.Attributes.Add("class", defaultColorString);
                else
                    newTableCell.Attributes.Add("class", string.Empty);

                headerRow.Cells.Add(newTableCell);
            }

        }


    }

    /// <summary>
    /// Rate categories are not displayed as per the configurations
    /// </summary>
    public class RateCategoryCoulmnComparer : IComparer<RateCategoryHeaderDisplay>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RateCategoryHeaderDisplay first, RateCategoryHeaderDisplay second)
        {
            return first.CoulmnNumber.CompareTo(second.CoulmnNumber);
        }
    }
}