<%@ Control Language="C#" AutoEventWireup="true" Codebehind="BookingModuleBig.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.BookingModuleBig" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%@ Register Src="DestinationSearch.ascx" TagName="DestinationSearch" TagPrefix="uc1" %>
<%@ Register Src="Calendar.ascx" TagName="Calendar" TagPrefix="uc2" %>
<div class="bookingContent">
    <!-- Get the language code and load the corresponding image. -->
    <% string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();%>
    <%-- <select visible="false" id="ddlBookingType" name="ddlBookingType" onchange="showTabContent(('Tab'+this.value));" runat="server">
	</select>--%>
    <input id="childrenCriteriaHidden" type="hidden" class="hiddenTxt" runat="server" />
    <input id="bedTypeCollection" type="hidden" class="hiddenTxt" runat="server" />
    <!-- Accordian content Booking Details Starts here -->
    <div class="expand" id="tabContainer1">
        <div class="hdFrst scansprite" id="Tab1">
            <div class="hdRtFrst scansprite">
            <% if (Utility.GetCurrentLanguage().Equals("ru-RU") || Utility.CheckifFallback())
               {%>
                       <h3 class="stoolHeadingAlternate">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookaHotel") %>
                    </h3>
                
                    <% }
               else
               {%> 
				            <h3 class="stoolHeading">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookaHotel") %>
                </h3>
                    <% } %>
                     
               <%-- <h3 class="stoolHeading">
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookaHotel")%>
                </h3>--%>
                <div class="hdRtCnt">
                </div>
            </div>
        </div>
        <div class="mainContent" style="display: block;">
            <div id="RegClientErrorDiv" class="errorText" runat="server" EnableViewState="False">
            </div>
            <div class="roundMe grayBox stayWhr">
                <p>
                    <label for="stay" class="fltLft">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/WhereToStay") %>
                    </label>
                    <%--<a class="fltRt linkHover map-view" id="searchOnMap" runat="server">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SearchBymap") %>
                    </a>--%>
                </p>
                <input id="txtHotelName" name="city" tabindex="1" value="" type="text" class="stayInputLft widthChng input input defaultColor"
                    autocomplete="off" runat="server" />
                <div id="autosuggest" class="autosuggest">
                    <ul>
                    </ul>
                    <iframe src="javascript:false;" frameborder="0" scrolling="no"></iframe>
                </div>
            </div>
            <div onkeypress="return WebForm_FireDefaultButton(event, '<%= btnSearch.ClientID %>')">
            <div class="roundMe grayBox stayWhn">
                <p>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/When") %>
                </p>
                <div class="colm3 padRt">
                    <label for="chekIn">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin") %>
                    </label>
                    <input type="text" name="chek" id="txtArrivalDate" class="dateRange txtArrivalDate"
                        runat="server" />
                    <br class="clear" />
                </div>
                <div class="colm1 padRt">
                    <label for="nights" class="midLab">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Nights") %>
                    </label>
                    <!--Release R 2.0| Change ddlnoOfNights to txtnoOfNights(Shameem)-->
                    <asp:TextBox ID="txtnoOfNights" MaxLength="2" runat="server" Style="width: 25px;"></asp:TextBox>
                    <%-- <asp:DropDownList ID="ddlnoOfNights" runat="server">
                    </asp:DropDownList>--%>
                </div>
                <div class="colm3 widthChng">
                    <label for="chekOut">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut") %>
                    </label>
                    <input type="text" name="chekOut" runat="server" id="txtDepartureDate" class="dateRange txtArrivalDate" />
                    <br class="clear" />
                </div>
                <br class="clear" />
            </div>
            <div class="hwMany fltLft">
                <p class="fltLft">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/HowManyRooms") %>
                </p>
                <asp:DropDownList ID="ddlNoOfRoomsReg" TabIndex="6" runat="server" CssClass="roomSelect"
                    rel="#regularBking">
                </asp:DropDownList>
            </div>
            <div class="roomOutput" id="regularBking">
                <div class="roundMe grayBox howManyRoom" style="display: block;" id="idRoom1" runat="server">
                    <div class="colmMerg fltLft roomOutput">
                        <div class="colm3 padRt">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/room1") %>
                            </strong>
                        </div>
                        <div class="colm4">
                            <div class="colm1 padRt">
                                <label class="midLab" for="rooms">
                                    &nbsp;
                                </label>
                            </div>
                            <div class="colm1 widthLs fltLft">
                                <label for="adult">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                </label>
                                <asp:DropDownList ID="ddlAdultsPerRoom1" TabIndex="7" runat="server" rel="adult">
                                </asp:DropDownList>
                                <label for="adult"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %> 13+</label>
                            </div>
                            <div class="colm1 widthMr fltRt">
                                <label for="child">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children") %>
                                </label>
                                <asp:DropDownList ID="ddlChildPerRoom1" TabIndex="8" runat="server" CssClass="child chafltRt"
                                    rel="choutput1">
                                </asp:DropDownList>
                                <label for="child">0-12</label>
                            </div>
                        </div>
                        <div class="childSelct choutput1" id="idRoom1ChildLabel" runat="server">
                            <div class="bord fltLft roundMe grayBoxHome">
                                <!-- Add new for Children bed-->
                                <div class="fltLft chdSelTtl">
                                    <div style="position: absolute; left: 5px; top: 72px;">
                                       <strong style="float:left;">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                        </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>" class="help spriteIcon toolTipMe"></span>
                                    </div>
                                </div>
                                <!--End here-->
                                <div class="fltLft chdSelTtl">
                                    <p class="fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                        </strong>
                                    </p>
                                </div>
                                <div class="selHdr1">
                                    <strong>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType") %>
                                    </strong>
                                </div>
                                <div class="colm5 padTop" id="idRoom1Child1Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child1") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom1Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild114">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput11">
                                        <asp:DropDownList ID="bedTypeforRoom1Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild114">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom1Child2Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child2") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom1Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild124">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput12">
                                        <asp:DropDownList ID="bedTypeforRoom1Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild124">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom1Child3Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child3") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom1Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild134">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput13">
                                        <asp:DropDownList ID="bedTypeforRoom1Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild134">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom1Child4Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child4") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom1Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild144">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput14">
                                        <asp:DropDownList ID="bedTypeforRoom1Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild144">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom1Child5Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child5") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom1Child5" TabIndex="7" runat="server" CssClass="fltLft subchild"
                                        rel="subchild154">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput15">
                                        <asp:DropDownList ID="bedTypeforRoom1Child5" TabIndex="7" runat="server" CssClass="selBedTyp subchild154">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="roundMe grayBox howManyRoom" id="idRoom2" runat="server">
                    <div class="colmMerg fltLft roomOutput">
                        <div class="colm3 padRt">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/room2") %>
                            </strong>
                        </div>
                        <div class="colm4">
                            <div class="colm1 padRt">
                                <label class="midLab" for="rooms">
                                    &nbsp;
                                </label>
                            </div>
                            <div class="colm1 widthLs fltLft">
                                <label for="adult">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                </label>
                                <asp:DropDownList ID="ddlAdultsPerRoom2" TabIndex="6" runat="server" rel="adult">
                                </asp:DropDownList>
                                <label for="adult">Age 13+</label>
                            </div>
                            <div class="colm1 widthMr fltRt">
                                <label for="child">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children") %>
                                </label>
                                <asp:DropDownList ID="ddlChildPerRoom2" TabIndex="6" runat="server" CssClass="child chafltRt"
                                    rel="choutput2">
                                </asp:DropDownList>
                                <label for="child">0-12</label>
                            </div>
                        </div>
                        <div class="childSelct choutput2" id="idRoom2ChildLabel" runat="server">
                            <div class="bord fltLft roundMe grayBoxHome">
                                <!-- Add new for Children bed-->
                                <div class="fltLft chdSelTtl">
                                    <div style="position: absolute; left: 5px; top: 72px;">
                                       <strong style="float:left;">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                        </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>" class="help spriteIcon toolTipMe"></span>
                                    </div>
                                </div>
                                <!-- End here-->
                                <div class="fltLft chdSelTtl">
                                    <p class="fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                        </strong>
                                    </p>
                                </div>
                                <div class="selHdr1">
                                    <strong>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType") %>
                                    </strong>
                                </div>
                                <div class="colm5 padTop" id="idRoom2Child1Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child1") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom2Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild214">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput21">
                                        <asp:DropDownList ID="bedTypeforRoom2Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild214">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom2Child2Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child2") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom2Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild224">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput22">
                                        <asp:DropDownList ID="bedTypeforRoom2Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild224">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom2Child3Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child3") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom2Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild234">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput23">
                                        <asp:DropDownList ID="bedTypeforRoom2Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild234">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom2Child4Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child4") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom2Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild244">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput24">
                                        <asp:DropDownList ID="bedTypeforRoom2Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild244">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom2Child5Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child5") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom2Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild254">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput24">
                                        <asp:DropDownList ID="bedTypeforRoom2Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchild254">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="roundMe grayBox howManyRoom" id="idRoom3" runat="server">
                    <div class="colmMerg fltLft roomOutput">
                        <div class="colm3 padRt">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/room3") %>
                            </strong>
                        </div>
                        <div class="colm4">
                            <div class="colm1 padRt">
                                <label class="midLab" for="rooms">
                                    &nbsp;
                                </label>
                            </div>
                            <div class="colm1 widthLs fltLft">
                                <label for="adult">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                </label>
                                <asp:DropDownList ID="ddlAdultsPerRoom3" TabIndex="6" runat="server" rel="adult">
                                </asp:DropDownList>
                                <label for="adult">Age 13+</label>
                            </div>
                            <div class="colm1 widthMr fltRt">
                                <label for="child">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children") %>
                                </label>
                                <asp:DropDownList ID="ddlChildPerRoom3" TabIndex="6" runat="server" CssClass="child chafltRt"
                                    rel="choutput3">
                                </asp:DropDownList>
                                <label for="child">0-12</label>
                            </div>
                        </div>
                        <div class="childSelct choutput3" id="idRoom3ChildLabel" runat="server">
                            <div class="bord fltLft roundMe grayBoxHome">
                                <!-- Add new for Children bed-->
                                <div class="fltLft chdSelTtl">
                                    <div style="position: absolute; left: 5px; top: 72px;">
                                        <strong style="float:left;">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                        </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>" class="help spriteIcon toolTipMe"></span>
                                    </div>
                                </div>
                                <!-- End here-->
                                <div class="fltLft chdSelTtl">
                                    <p class="fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                        </strong>
                                    </p>
                                </div>
                                <div class="selHdr1">
                                    <strong>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType") %>
                                    </strong>
                                </div>
                                <div class="colm5 padTop" id="idRoom3Child1Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child1") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom3Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild314">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput31">
                                        <asp:DropDownList ID="bedTypeforRoom3Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild314">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom3Child2Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child2") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom3Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild324">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput32">
                                        <asp:DropDownList ID="bedTypeforRoom3Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild324">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom3Child3Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child3") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom3Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild334">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput33">
                                        <asp:DropDownList ID="bedTypeforRoom3Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild334">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom3Child4Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child4") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom3Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild344">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput34">
                                        <asp:DropDownList ID="bedTypeforRoom3Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild344">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom3Child5Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child5") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom3Child5" TabIndex="7" runat="server" CssClass="fltLft subchild"
                                        rel="subchild354">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput35">
                                        <asp:DropDownList ID="bedTypeforRoom3Child5" TabIndex="7" runat="server" CssClass="selBedTyp subchild354">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="roundMe grayBox howManyRoom" id="idRoom4" runat="server">
                    <div class="colmMerg fltLft roomOutput">
                        <div class="colm3 padRt">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/room4") %>
                            </strong>
                        </div>
                        <div class="colm4">
                            <div class="colm1 padRt">
                                <label class="midLab" for="rooms">
                                    &nbsp;
                                </label>
                            </div>
                            <div class="colm1 widthLs fltLft">
                                <label for="adult">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                </label>
                                <asp:DropDownList ID="ddlAdultsPerRoom4" TabIndex="6" runat="server" name="adult">
                                </asp:DropDownList>
                                <label for="adult">Age 13+</label>
                            </div>
                            <div class="colm1 widthMr fltRt">
                                <label for="child">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children") %>
                                </label>
                                <asp:DropDownList ID="ddlChildPerRoom4" TabIndex="6" runat="server" CssClass="child chafltRt"
                                    rel="choutput4">
                                </asp:DropDownList>
                                <label for="child">0-12</label>
                            </div>
                        </div>
                        <div class="childSelct choutput4" id="idRoom4ChildLabel" runat="server">
                            <div class="bord fltLft roundMe grayBoxHome">
                                <!-- Add new for Children bed-->
                                <div class="fltLft chdSelTtl">
                                    <div style="position: absolute; left: 5px; top: 72px;">
                                        <strong style="float:left;">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                        </strong>
                                        <span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>" class="help spriteIcon toolTipMe"></span>
                                    </div>
                                </div>
                                <!-- End here-->
                                <div class="fltLft chdSelTtl">
                                    <p class="fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                        </strong>
                                    </p>
                                </div>
                                <div class="selHdr1">
                                    <strong>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType") %>
                                    </strong>
                                </div>
                                <div class="colm5 padTop" id="idRoom4Child1Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child1") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom4Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild414">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput41">
                                        <asp:DropDownList ID="bedTypeforRoom4Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild414">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom4Child2Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child2") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom4Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild424">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput42">
                                        <asp:DropDownList ID="bedTypeforRoom4Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild424">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom4Child3Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child3") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom4Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild434">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput43">
                                        <asp:DropDownList ID="bedTypeforRoom4Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild434">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom4Child4Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child4") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom4Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchild444">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput44">
                                        <asp:DropDownList ID="bedTypeforRoom4Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild444">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idRoom4Child5Ddl" runat="server">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child5") %>
                                    </label>
                                    <asp:DropDownList ID="childAgeforRoom4Child5" TabIndex="7" runat="server" CssClass="fltLft subchild"
                                        rel="subchild454">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput45">
                                        <asp:DropDownList ID="bedTypeforRoom4Child5" TabIndex="7" runat="server" CssClass="selBedTyp subchild454">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="whitBox">
                <div class="whitBoxLft">
                    <p>
                        <label for="bokCod" class="fltLft">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterBookingCode1") %>
                        </label>
                        <span title=" <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingCodeToolTip") %>"
                            class="help spriteIcon toolTipMe"></span>
                    </p>
                    <input type="text" name="txtRegularCode" runat="server" id="txtRegularCode" class="normalTextCont input defaultColor" />
                    <div class="chkBox">
                        <input type="checkbox" name="disFrndRoom" id="rememberMe" runat="server" />
                        <label for="disFrndRoom">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RememberBookingCode") %>
                        </label>
                    </div>
                </div>
                <div class="whitBoxRt fltRt">
                    <div class="actionBtn fltRt">
                        <%--<a class="buttonInner sprite" href="t02a_select_hotel-list_view.shtml" title="Search"><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Search")%></a>--%>
                        <asp:LinkButton ID="btnSearch" TabIndex="8" runat="server" CssClass="buttonInner"
                            OnClick="btnSearch_Click"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Search") %></span></asp:LinkButton>
                        <asp:LinkButton ID="spnSearch" runat="server" CssClass="buttonRt scansprite" OnClick="btnSearch_Click"></asp:LinkButton>
                    </div>
                </div>
                <br class="clear" />
            </div>
        </div>
    </div>
    </div>
    <!-- Accordian content Booking Details Ends here -->
    <!-- Accordian content Redeem Bonus cheque starts here -->
    <div class="expand" id="tabContainer2">
        <div class="hdDeflt scansprite" id="Tab2">
            <div class="hdRtDeflt scansprite">
             <% if (Utility.GetCurrentLanguage().Equals("ru-RU") || Utility.CheckifFallback())
                {%>
                       <h3 class="stoolHeadingAlternate">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RedeemYourBonusCheques") %>
                    </h3>
                
                    <% }
                else
                {%> 
				            <h3 class="stoolHeading">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RedeemYourBonusCheques") %>
                </h3>
                    <% } %>
                <%--<h3 class="stoolHeading">
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RedeemYourBonusCheques")%>
                </h3>--%>
            </div>
        </div>
        <div class="mainContent">
            <div id="BCClientErrorDiv" class="errorText" runat="server" EnableViewState="False">
            </div>
            <!-- BugFix:430942:Added to show error on bonous check booking -->
            <div id="clientErrorDiv" runat="server">
            </div>
            <div class="roundMe grayBox stayWhr">
                <p>
                    <label for="stay" class="fltLft">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/WhereToStay") %>
                    </label>
                    <%--<a class="fltRt linkHover map-view" id="searchOnMapBNC" runat="server">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SearchBymap") %>
                    </a>--%>
                </p>
                <input id="txtHotelNameBNC" runat="server" type="text" class="stayInputLft input widthChng demoSuggest input defaultColor"
                    name="city" />
                <div id="autosuggest1" class="autosuggest">
                    <ul>
                    </ul>
                    <iframe src="javascript:false;" frameborder="0" scrolling="no"></iframe>
                </div>
            </div>
            <div class="stayInptRt">
            </div>
            <div onkeypress="return WebForm_FireDefaultButton(event, '<%= btnBonusChequeSearch.ClientID %>')">
            <div class="roundMe grayBox stayWhn">
                <p>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/When") %>
                </p>
                <div class="colm3 padRt">
                    <label for="chekIn">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin") %>
                    </label>
                    <input type="text" name="chek" id="txtBCArrivalDate" class="dateRange txtArrivalDate"
                        runat="server" />
                    <br class="clear" />
                </div>
                <div class="colm1 padRt">
                    <label for="nights" class="midLab">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Nights") %>
                    </label>
                    <!--Release R 2.0| Change ddlBonousChequeNoofNights to txtRewardNightNoOfNights(Shameem)-->
                    <asp:TextBox ID="txtBonousChequeNoofNights" MaxLength="2" runat="server" Style="width: 25px;"></asp:TextBox>
                    <%-- <asp:DropDownList ID="ddlBonousChequeNoofNights" runat="server">
                    </asp:DropDownList>--%>
                </div>
                <div class="colm3 widthChng">
                    <label for="chekOut">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut") %>
                    </label>
                    <input type="text" name="chekOut" id="txtBCDepartureDate" runat="server" class="dateRange txtDepartureDate" />
                    <br class="clear" />
                </div>
                <br class="clear" />
            </div>
            <div class="hwMany fltLft">
                <p class="fltLft">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/HowManyRooms") %>
                </p>
                <asp:DropDownList ID="ddlNoOfRoomsBC" TabIndex="6" runat="server" rel="#redeem" CssClass="roomSelect">
                </asp:DropDownList>
            </div>
            <div class="roomOutput" id="redeem">
                <div class="roundMe grayBox howManyRoom" style="display: block;" id="idBCRoom1" runat="server">
                    <div class="colmMerg fltLft roomOutput">
                        <div class="colm3 padRt">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room1") %>
                            </strong>
                        </div>
                        <div class="colm4">
                            <div class="colm1 padRt">
                                <label class="midLab" for="rooms">
                                    &nbsp;
                                </label>
                            </div>
                            <div class="colm1 widthLs fltLft">
                                <label for="adult">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                </label>
                                <asp:DropDownList ID="ddlBCAdultsPerRoom1" TabIndex="7" runat="server" rel="adult">
                                </asp:DropDownList>
                                <label for="adult">Age 13+</label>
                            </div>
                            <div class="colm1 widthMr fltRt">
                                <label for="child">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children") %>
                                </label>
                                <asp:DropDownList ID="ddlBCChildPerRoom1" TabIndex="8" runat="server" CssClass="child chafltRt"
                                    rel="choutputBC1">
                                </asp:DropDownList>
                                <label for="child">0-12</label>
                            </div>
                        </div>
                        <div class="childSelct choutputBC1" id="idBCRoom1ChildLabel" runat="server">
                            <div class="bord fltLft roundMe grayBoxHome">
                                <!-- Add new for Children bed-->
                                <div class="fltLft chdSelTtl">
                                    <div style="position: absolute; left: 5px; top: 72px;">
                                       <strong style="float:left;">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                        </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>" class="help spriteIcon toolTipMe"></span>
                                    </div>
                                </div>
                                <!--End here-->
                                <div class="fltLft chdSelTtl">
                                    <p class="fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                        </strong>
                                    </p>
                                </div>
                                <div class="selHdr1">
                                    <strong>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType") %>
                                    </strong>
                                </div>
                                <div class="colm5 padTop" id="idBCRoom1Child1Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child1") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom1Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC114">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput11">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom1Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC114">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom1Child2Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child2") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom1Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC124">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput12">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom1Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC124">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom1Child3Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child3") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom1Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC134">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput13">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom1Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC134">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom1Child4Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child4") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom1Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC144">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput14">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom1Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC144">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom1Child5Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child5") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom1Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC154">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput15">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom1Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC154">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="roundMe grayBox howManyRoom" id="idBCRoom2" runat="server">
                    <div class="colmMerg fltLft roomOutput">
                        <div class="colm3 padRt">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room2") %>
                            </strong>
                        </div>
                        <div class="colm4">
                            <div class="colm1 padRt">
                                <label class="midLab" for="rooms">
                                    &nbsp;
                                </label>
                            </div>
                            <div class="colm1 widthLs fltLft">
                                <label for="adult">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                </label>
                                <asp:DropDownList ID="ddlBCAdultsPerRoom2" TabIndex="6" runat="server" rel="adult">
                                </asp:DropDownList>
                                <label for="adult">Age 13+</label>
                            </div>
                            <div class="colm1 widthMr fltRt">
                                <label for="child">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children") %>
                                </label>
                                <asp:DropDownList ID="ddlBCChildPerRoom2" TabIndex="6" runat="server" CssClass="child chafltRt"
                                    rel="choutputBC2">
                                </asp:DropDownList>
                                <label for="child">0-12</label>
                            </div>
                        </div>
                        <div class="childSelct choutputBC2" id="idBCRoom2ChildLabel" runat="server">
                            <div class="bord fltLft roundMe grayBoxHome">
                                <!-- Add new for Children bed-->
                                <div class="fltLft chdSelTtl">
                                    <div style="position: absolute; left: 5px; top: 72px;">
                                      <strong style="float:left;">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                        </strong>
                                        <span title=" <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"
                                            class="help spriteIcon toolTipMe"></span>
                                    </div>
                                </div>
                                <!--End here-->
                                <div class="fltLft chdSelTtl">
                                    <p class="fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                        </strong>
                                    </p>
                                </div>
                                <div class="selHdr1">
                                    <strong>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType") %>
                                    </strong>
                                </div>
                                <div class="colm5 padTop" id="idBCRoom2Child1Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child1") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom2Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC214">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput21">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom2Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC214">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom2Child2Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child2") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom2Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC224">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput22">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom2Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC224">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom2Child3Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child3") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom2Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC234">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput23">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom2Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC234">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom2Child4Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child4") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom2Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC244">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput24">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom2Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC244">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom2Child5Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child5") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom2Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC254">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput25">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom2Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC254">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="roundMe grayBox howManyRoom" id="idBCRoom3" runat="server">
                    <div class="colmMerg fltLft roomOutput">
                        <div class="colm3 padRt">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room3") %>
                            </strong>
                        </div>
                        <div class="colm4">
                            <div class="colm1 padRt">
                                <label class="midLab" for="rooms">
                                    &nbsp;
                                </label>
                            </div>
                            <div class="colm1 widthLs fltLft">
                                <label for="adult">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                </label>
                                <asp:DropDownList ID="ddlBCAdultsPerRoom3" TabIndex="6" runat="server" rel="adult">
                                </asp:DropDownList>
                                <label for="adult">Age 13+</label>
                            </div>
                            <div class="colm1 widthMr fltRt">
                                <label for="child">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children") %>
                                </label>
                                <asp:DropDownList ID="ddlBCChildPerRoom3" TabIndex="6" runat="server" CssClass="child chafltRt"
                                    rel="choutputBC3">
                                </asp:DropDownList>
                                <label for="child">0-12</label>
                            </div>
                        </div>
                        <div class="childSelct choutputBC3" id="idBCRoom3ChildLabel" runat="server">
                            <div class="bord fltLft roundMe grayBoxHome">
                                <!-- Add new for Children bed-->
                                <div class="fltLft chdSelTtl">
                                    <div style="position: absolute; left: 5px; top: 72px;">
                                      <strong style="float:left;">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                        </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>" class="help spriteIcon toolTipMe"></span>
                                    </div>
                                </div>
                                <!--End here-->
                                <div class="fltLft chdSelTtl">
                                    <p class="fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                        </strong>
                                    </p>
                                </div>
                                <div class="selHdr1">
                                    <strong>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType") %>
                                    </strong>
                                </div>
                                <div class="colm5 padTop" id="idBCRoom3Child1Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child1") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom3Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC314">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput31">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom3Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC314">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom3Child2Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child2") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom3Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC324">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput32">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom3Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC324">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom3Child3Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child3") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom3Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC334">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput33">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom3Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC334">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom3Child4Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child4") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom3Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC344">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput34">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom3Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC344">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom3Child5Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child5") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom3Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC354">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput35">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom3Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC354">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="roundMe grayBox howManyRoom" id="idBCRoom4" runat="server">
                    <div class="colmMerg fltLft roomOutput">
                        <div class="colm3 padRt">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room4") %>
                            </strong>
                        </div>
                        <div class="colm4">
                            <div class="colm1 padRt">
                                <label class="midLab" for="rooms">
                                    &nbsp;
                                </label>
                            </div>
                            <div class="colm1 widthLs fltLft">
                                <label for="adult">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                </label>
                                <asp:DropDownList ID="ddlBCAdultsPerRoom4" TabIndex="6" runat="server" name="adult">
                                </asp:DropDownList>
                                <label for="adult">Age 13+</label>
                            </div>
                            <div class="colm1 widthMr fltRt">
                                <label for="child">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children") %>
                                </label>
                                <asp:DropDownList ID="ddlBCChildPerRoom4" TabIndex="6" runat="server" CssClass="child chafltRt"
                                    rel="choutputBC4">
                                </asp:DropDownList>
                                <label for="child">0-12</label>
                            </div>
                        </div>
                        <div class="childSelct choutputBC4" id="idBCRoom4ChildLabel" runat="server">
                            <div class="bord fltLft roundMe grayBoxHome">
                                <!-- Add new for Children bed-->
                                <div class="fltLft chdSelTtl">
                                    <div style="position: absolute; left: 5px; top: 72px;">
                                        <strong style="float:left;">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                        </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>" class="help spriteIcon toolTipMe"></span>
                                    </div>
                                </div>
                                <!--End here-->
                                <div class="fltLft chdSelTtl">
                                    <p class="fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                        </strong>
                                    </p>
                                </div>
                                <div class="selHdr1">
                                    <strong>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType") %>
                                    </strong>
                                </div>
                                <div class="colm5 padTop" id="idBCRoom4Child1Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child1") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom4Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC414">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput41">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom4Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC414">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom4Child2Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child2") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom4Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC424">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput42">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom4Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC424">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom4Child3Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child3") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom4Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC434">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput43">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom4Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC434">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom4Child4Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child4") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom4Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC444">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput44">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom4Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC444">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="colm5" id="idBCRoom4Child5Ddl" runat="server" style="display:none;">
                                    <label class="fltLft">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child5") %>
                                    </label>
                                    <asp:DropDownList ID="ddlBCChildAgeforRoom4Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                        rel="subchildBC454">
                                    </asp:DropDownList>
                                    <div class="widthMr fltLft bedoutput45">
                                        <asp:DropDownList ID="ddlBCBedTypeforRoom4Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC454">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="whitBoxInfoAlert">
                <div class="whitBoxLft">
                    <div class="hd sprite">
                        &nbsp;</div>
                    <div class="cnt sprite">
                        <p>
                            <span class="spriteIcon"></span>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/bonuschequeteaser") %>
                        </p>
                    </div>
                    <div class="ft sprite">
                        &nbsp;</div>
                </div>
                <div class="whitBoxRt noPad fltRt">
                    <div class="actionBtn fltRt">
                        <asp:LinkButton ID="btnBonusChequeSearch" TabIndex="8" runat="server" CssClass="buttonInner"
                            OnClick="btnSearch_Click"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Search") %></span></asp:LinkButton>
                        <asp:LinkButton ID="spnBonusChequeSearch" runat="server" CssClass="buttonRt scansprite" OnClick="btnSearch_Click"></asp:LinkButton>
                    </div>
                </div>
                <br class="clear" />
            </div>
        </div>
    </div>
    </div>
    <!-- Accordian content Redeem ends here -->
    <!-- Accordian content Claim reward Nights starts here -->
    <div class="expand" id="tabContainer3">
        <div class="hdDeflt scansprite" id="Tab3">
            <div class="hdRtDeflt scansprite">
            <% if (Utility.GetCurrentLanguage().Equals("ru-RU") || Utility.CheckifFallback())
               {%>
                       <h3 class="stoolHeadingAlternate">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ClaimRewardNight") %>
                    </h3>
                
                    <% }
               else
               {%> 
				            <h3 class="stoolHeading">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ClaimRewardNight") %>
                </h3>
                    <% } %>
                <%--<h3 class="stoolHeading">
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ClaimRewardNight")%>
                </h3>--%>
            </div>
        </div>
        <div class="mainContent">
            <div id="RNClientErrorDiv" class="errorText" runat="server" EnableViewState="False">
            </div>
            <asp:PlaceHolder ID="rewardNightPlaceHolder" Visible="false" runat="server">
                <p class="pad20 marB20">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/loginrequired") %>
                </p>
                <div class="roundMe grayBox claimBox" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnLogin.ClientID %>')">
                    <label class="fltLft">
                        <span class="fltLft"><strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/PleaseLogIn") %>
                        </strong></span>
                    </label>
                    <div class="clear">
                    </div>
                    <div class="colmLft">
                    <input type="text" name="memberPrefix" readonly="readonly" class="input input defaultColor input defaultColor rewardNightMemberLogin userPrefixMed userPrefix40" id="txtUserNamePrefix" runat="server" />
                        <input type="text" name="member" class="input input defaultColor input defaultColor rewardNightMemberLogin w75" tabindex="300" id="txtUserName" runat="server" />
                    </div>
                    <div class="colmRt fltRt">
                        <%--<input type="password" value="" class="password-password" tabindex="301" id="txtPassword" runat="server" maxlength="20" name="ctl00$MenuLoginStatus$txtLoyaltyPassword" style="display: none;float:left;"/>--%>
                        <asp:TextBox CssClass="password-password fltLft" ID="txtPassword" TextMode="Password"
                            MaxLength="20" runat="server" />
                            <%--R2.2 Artf1148124 tabindex of following password control should be eqaul to the remember me check box Ashish--%>
                        <input type="text" id="inputtxtPassword" tabindex="125" value='<%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/Password") %>'
                            class="defaultColor password-clear fltLft" />
                    </div>
                    <div class="chkBox">
                        <input type="checkbox" name="disFrndRoom" class="chkBx" id="RememberUserIdPwd" value=""
                            runat="server" />
                        <label for="rememberMe">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/RememberMe") %>                           
                        </label>
                         <span title=" <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/RememberMeToolTip") %>"
                                class="help spriteIcon toolTipMe"></span>
                    </div>
                    <div class="whitBoxRt fltRt">
                        <div class="usrBtn fltRt">
                            <%--<a class="buttonInner sprite" href="#" title="Login">
                                <%=WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/loginmessage")%>
                            </a>--%>
                            <asp:LinkButton ID="btnLogin" runat="server" CssClass="buttonInner"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/login") %></span></asp:LinkButton>
                            <asp:LinkButton ID="spnLogin" runat="server" CssClass="buttonRt"></asp:LinkButton>
                            
                        </div>
                    </div>
                    <div id="RwrdNgtMedLoginProgressDiv" runat="server" style="display: none">
                         <span class="fltLft rwrdNgtMedLoginPrg">
                            <img src='<%=ResolveUrl("~/Templates/Booking/Styles/Default/Images/rotatingclockani.gif")%>'
                                alt="image" align="middle" width="25px" height="25px" />
                            <span>
                                <%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/Loading")%>
                            </span>
                        </span>
                    </div>
                    <br class="clear" />
                </div>
                <div class="forgotLinks">
                    <a href="#" class="fltLft linkHover" onclick="openPopupWin('<%= GlobalUtil.GetHelpPage(Utility.GetForgottenMembershipNoPageReference()) %>','width=400,height=300,scrollbars=yes,resizable=yes');return false;">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/forgottenmembershipno") %>
                    </a>
                    <asp:LinkButton ID="forgotPassword" runat="server" CssClass="fltLft padTop0 linkHover" OnClick="ForgottenPassword_Click"><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/forgottenpasswordmessage") %></asp:LinkButton>
                </div>
                <br class="clear" />
            </asp:PlaceHolder>
            <!--This placeholder will show after log in in Reward night module-->
            <asp:PlaceHolder ID="bookingPlaceHolder" Visible="false" runat="server">
                <div class="roundMe grayBox stayWhr">
                    <p>
                        <label for="stay" class="fltLft">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/WhereToStay") %>
                        </label>
                        <%--<a class="fltRt linkHover map-view" id="searchOnMapRewardNight" runat="server">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SearchBymap") %>
                        </a>--%>
                    </p>
                    <%--<input type="text" value=''
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName")%>' rel="Enter hotel name or destination" class="stayInputLft widthChng demoSuggest input" name="city"/>--%>
                    <input id="txtHotelNameRewardNights" name="city" tabindex="1" value="" type="text"
                        class="stayInputLft widthChng input input defaultColor " autocomplete="off"
                        runat="server" /><br />
                    <!--Auto sugestion Starts--->
                    <!-- CSS hack for Mac Safari and Safari version 3 browser -->
                    <style>
              #autosuggest ul{
              margin-top: 35px;
              overflow: scroll;
              }
              /* Mac Safari hack - the below css will be ignored by Mac safari less than version3*/
              #autosuggest ul{
              overflow-x: hidden;
              overflow-y: auto;
              margin-top: 0px;#
              }
              /* Safari 3 hack -the below css  works only on safari 3 browser */
              @media screen and (-webkit-min-device-pixel-ratio:0) {
              #autosuggest { margin-top: 35px;}
              }
            </style>
                    <!-- END - CSS hack for Mac Safari and Safari version 3 browser -->
                    <div id="autosuggest2" class="autosuggest">
                        <ul>
                        </ul>
                        <iframe src="javascript:false;" frameborder="0" scrolling="no"></iframe>
                    </div>
                </div>
                <div onkeypress="return WebForm_FireDefaultButton(event, '<%= btnRewardNightSearch.ClientID %>')">
                <div class="roundMe grayBox stayWhn">
                    <p>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/When") %>
                    </p>
                    <div class="colm3 padRt">
                        <label for="chekIn">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin") %>
                        </label>
                        <input type="text" name="chek" id="txtRNArrivalDate" class="dateRange txtArrivalDate"
                            runat="server" />
                        <br class="clear" />
                    </div>
                    <div class="colm1 padRt">
                        <label for="nights" class="midLab">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Nights") %>
                        </label>
                        <!--Release R 2.0| Change ddlBonousChequeNoofNights to txtRewardNightNoOfNights(Shameem)-->
                        <asp:TextBox ID="txtRewardNightNoOfNights" MaxLength="2" runat="server" Style="width: 25px;"></asp:TextBox>
                        <%--  <asp:DropDownList ID="ddlRewardNightNoOfNights" runat="server">
                        </asp:DropDownList>--%>
                    </div>
                    <div class="colm3 widthChng">
                        <label for="chekOut">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut") %>
                        </label>
                        <input type="text" name="chekOut" runat="server" id="txtRNDepartureDate" class="dateRange txtDepartureDate" />
                        <br class="clear" />
                    </div>
                    <br class="clear" />
                </div>
                <div class="hwMany fltLft">
                    <p class="fltLft">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/HowManyRooms") %>
                    </p>
                    <asp:DropDownList ID="ddlNoOfRoomsRed" TabIndex="6" runat="server" CssClass="roomSelect"
                        rel="#rewardBking">
                    </asp:DropDownList>
                </div>
                <div class="roomOutput" id="rewardBking">
                    <div class="roundMe grayBox howManyRoom" style="display: block;" id="idRNRoom1" runat="server">
                        <div class="colmMerg fltLft roomOutput">
                            <div class="colm3 padRt">
                                <strong>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room1") %>
                                </strong>
                            </div>
                            <div class="colm4">
                                <div class="colm1 padRt">
                                    <label class="midLab" for="rooms">
                                        &nbsp;
                                    </label>
                                </div>
                                <div class="colm1 widthLs fltLft">
                                    <label for="adult">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                    </label>
                                    <asp:DropDownList ID="ddlRNAdultsPerRoom1" TabIndex="7" runat="server" rel="adult">
                                    </asp:DropDownList>
                                    <label for="adult">Age 13+</label>
                                </div>
                                <div class="colm1 widthMr fltRt">
                                    <label for="child">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children") %>
                                    </label>
                                    <asp:DropDownList ID="ddlRNChildPerRoom1" TabIndex="8" runat="server" CssClass="child chafltRt"
                                        rel="choutputRN1">
                                    </asp:DropDownList>
                                    <label for="child">0-12</label>
                                </div>
                            </div>
                            <div class="childSelct choutputRN1" id="idRNRoom1ChildLabel" runat="server">
                                <div class="bord fltLft roundMe grayBoxHome">
                                    <!-- Add new for Children bed-->
                                    <div class="fltLft chdSelTtl">
                                        <div style="position: absolute; left: 5px; top: 72px;">
                                           <strong style="float:left;">
                                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                            </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>" class="help spriteIcon toolTipMe"></span>
                                        </div>
                                    </div>
                                    <!--NEW-->
                                    <div class="fltLft chdSelTtl">
                                        <p class="fltLft">
                                            <strong>
                                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                            </strong>
                                        </p>
                                    </div>
                                    <div class="selHdr1">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType") %>
                                        </strong>
                                    </div>
                                    <div class="colm5 padTop" id="idRNRoom1Child1Ddl" runat="server" style="display:none;">
                                        <label class="fltLft">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child1") %>
                                        </label>
                                        <asp:DropDownList ID="ddlRNChildAgeforRoom1Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                            rel="subchildRN114">
                                        </asp:DropDownList>
                                        <div class="widthMr fltLft bedoutput11">
                                            <asp:DropDownList ID="ddlRNBedTypeforRoom1Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchildRN114">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="colm5" id="idRNRoom1Child2Ddl" runat="server" style="display:none;">
                                        <label class="fltLft">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child2") %>
                                        </label>
                                        <asp:DropDownList ID="ddlRNChildAgeforRoom1Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                            rel="subchildRN124">
                                        </asp:DropDownList>
                                        <div class="widthMr fltLft bedoutput12">
                                            <asp:DropDownList ID="ddlRNBedTypeforRoom1Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchildRN124">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="colm5" id="idRNRoom1Child3Ddl" runat="server" style="display:none;">
                                        <label class="fltLft">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child3") %>
                                        </label>
                                        <asp:DropDownList ID="ddlRNChildAgeforRoom1Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                            rel="subchildRN134">
                                        </asp:DropDownList>
                                        <div class="widthMr fltLft bedoutput13">
                                            <asp:DropDownList ID="ddlRNBedTypeforRoom1Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchildRN134">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="colm5" id="idRNRoom1Child4Ddl" runat="server" style="display:none;">
                                        <label class="fltLft">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child4") %>
                                        </label>
                                        <asp:DropDownList ID="ddlRNChildAgeforRoom1Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                            rel="subchildRN144">
                                        </asp:DropDownList>
                                        <div class="widthMr fltLft bedoutput14">
                                            <asp:DropDownList ID="ddlRNBedTypeforRoom1Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchildRN144">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="colm5" id="idRNRoom1Child5Ddl" runat="server" style="display:none;">
                                        <label class="fltLft">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child5") %>
                                        </label>
                                        <asp:DropDownList ID="ddlRNChildAgeforRoom1Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                            rel="subchildRN154">
                                        </asp:DropDownList>
                                        <div class="widthMr fltLft bedoutput15">
                                            <asp:DropDownList ID="ddlRNBedTypeforRoom1Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchildRN154">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
                <div class="whitBoxInfoAlert">
                   <!-- <div class="whitBoxLft">
                        <div class="hd sprite">
                            &nbsp;</div>
                        <div class="cnt sprite">
                            <p>
                                <span class="spriteIcon"></span>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/bonuschequeteaser") %>
                            </p>
                        </div>
                        <div class="ft sprite">
                            &nbsp;</div>
                    </div>-->
                    <div class="whitBoxRt noPad fltRt">
                        <div class="actionBtn fltRt">
                            <asp:LinkButton ID="btnRewardNightSearch" runat="server" CssClass="buttonInner"
                                OnClick="btnSearch_Click"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Search") %></span></asp:LinkButton>
                            <asp:LinkButton ID="spnRewardNightSearch" runat="server" CssClass="buttonRt scansprite"
                                OnClick="btnSearch_Click"></asp:LinkButton>
                            
                        </div>
                    </div>
                    <br class="clear" />
                </div>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
    <!-- Accordian content Claim rewards ends here -->
    <!-- Accordian content Cancel booking starts here -->
    <div class="expand">
        <div class="hdDeflt scansprite" id="Tab4">
            <div class="hdRtDeflt scansprite">
            <% if (Utility.GetCurrentLanguage().Equals("ru-RU") || Utility.CheckifFallback())
               {%>
                       <h3 class="stoolHeadingAlternate">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ModifyOrCancelBooking") %>
                    </h3>
                
                    <% }
               else
               {%> 
				            <h3 class="stoolHeading">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ModifyOrCancelBooking") %>
                </h3>
                    <% } %>
                <%--<h3 class="stoolHeading">
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ModifyOrCancelBooking")%>
                </h3>--%>
            </div>
        </div>
        <div class="mainContent cancelInfo">
            <div id="clientErrorDivBD" class="errorText" runat="server">
            </div>
            <p class="marB20">
                <label id="lblModifyCancelInfo" runat="server">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ModifyCancelInfo") %>
                </label>
            </p>
            <div class="roundMe grayBox claimBox" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnFetchBooking.ClientID %>')">
                <label class="padBtm5 fltLft">
                    <strong style="float:left;">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/PleaseEnterDetails") %>
                    </strong><span title=" <%= WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/CancelModifyBookingToolTip") %>"
                        class="help spriteIcon toolTipMe"></span>
                </label>
                <div class="clear">
                </div>
                <div class="colmLft">
                    <input type="text" name="BookingNumber" id="txtBookingNumber" class="input defaultColor input defaultColor"
                        runat="server" />
                </div>
                <div class="colmRt fltRt">
                    <input type="text" name="Surname" id="txtSurname" class="input defaultColor input defaultColor"
                        maxlength="40" runat="server" />
                </div>
                <br class="clear" />
            </div>
            <div class="whitBoxInfoAlert">
                <div class="whitBoxRt fltRt">
                    <div class="actionBtn fltRt">
                        <asp:LinkButton ID="btnFetchBooking" CssClass="buttonInner" OnClick="btnFetchBooking_Click"
                            runat="server">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/Search") %>
                        </asp:LinkButton>
                        <asp:LinkButton ID="spnFetchBooking" runat="server" CssClass="buttonRt scansprite" OnClick="btnFetchBooking_Click"></asp:LinkButton>
                    </div>
                    <br class="clear" />
                </div>
                <br class="clear" />
            </div>
            <p class="marT18">
                <label id="lblContactCustomerService" runat="server">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ContactCustomerService") %>
                </label>
            </p>
            <%--<div class="ft sprite">
            </div>--%>
        </div>
    </div>
    <!-- Accordian content Cancel booking ends here -->
    <!--Hidden Fields used for search-->
    <%--    <div id="clientErrorDiv" runat="server">
    </div>--%>
    <input type="hidden" id="siteLang" name="siteLang" value="<%= siteLanguage %>" />
    <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
    
    <!--Rel 2.2.7: AMS Patch 4| artf1216848 : Scanweb - Incorrect error when searching a destination that is not choosen from list 
    <input type="hidden" id="customerMessage" name="customerMessage" runat="server" />    -->
    
    <input type="hidden" id="destinationError" name="destinationError" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name") %>' />
    <input type="hidden" id="duplicateDestinationError" name="duplicateDestinationError"
        value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/duplicate_destination") %>' />
    <input type="hidden" id="arrivaldateError" name="arrivaldateError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/arrival_date") %>' />
    <input type="hidden" id="departuredateError" name="departuredateError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/departure_date") %>' />
    <input type="hidden" id="NoOfNightError" name="NoOfNightError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/no_of_nights") %>' />
    <input type="hidden" id="invalidGuestNumberError" name="invalidGuestNumberError"
        value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/user_name") %>' />
    <input type="hidden" id="invalidGuestPINError" name="invalidGuestPINError" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/password") %>' />
    <input type="hidden" id="invalidGuestLoginError" name="invalidGuestLoginError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/user_name_password") %>' />
    <input type="hidden" id="invalidSpecialCodeError" name="invalidSpecialCodeError"
        value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/special_booking_code") %>' />
    <input type="hidden" id="invalidNegotiatedCode" name="invalidNegotiatedCode" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/negotiated_rate_code") %>' />
    <input type="hidden" id="invalidVoucherCode" name="invalidVoucherCode" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/voucher_booking_code") %>' />
    <input type="hidden" id="specialCodeToDNumber" name="specialCodeToDNumber" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/invalidSpecialCode") %>' />
    <input type="hidden" id="specialCodeToTravelAgent" name="specialCodeToTravelAgent"
        value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/businesserror/invalidSpecialCodeTravelAgent") %>' />
    <input type="hidden" id="invalidBookingNumber" name="invalidBookingNumber" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidBookingNumber") %>' />
    <input type="hidden" id="invalidSurname" name="invalidSurname" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidSurname") %>' />
    <!-- searchMode value can be changed based on the login -->
    <input type="hidden" id="guestLogin" value="false" runat="server" />
    <input type="hidden" id="sT" runat="server" />
    <input type="hidden" id="selectedDestId" runat="server" />
    <%--<input type="hidden" id="selectedDestId1" runat="server" />--%>
    <input type="hidden" id="loginSrchID" runat="server" />
    <input type="hidden" id="loginSrchPageID" runat="server" />
    <input type="hidden" id="childAgeNotSelectedError" name="childrenAgeNotSelectedError"
        value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidAge") %>" />
    <input type="hidden" id="ChildBedTypeNotSelectedError" name="ChildBedTypeNotSelectedError"
        value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidBedType") %>" />
    <input type="hidden" id="childInAdultBedandAdultCountError" name="childInAdultBedandAdultCountError"
        value="<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalidbedtypechoosen") %>" />
    <input type="hidden" id="childInAdultsBedType" name="childInAdultsBedType" value="<%=
                WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/sharingbed") %>" />
    <input type="hidden" id="hdnlastNameTranslated" name="hdnlastNameTranslated" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/Surname") %>" />
    <input type="hidden" id="invalidReservationNumber" name="invalidReservationNumber" runat="server" value="false"/>
    <!--Priya Singh: Error msg displayed when invalid block code entered.
    Artifact artf1217694 : Scanweb - Input control missing for incorrect block code  -->
    <input type="hidden" id="invalidBlockCode" name="invalidBlockCode" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/InvalidBlockCode") %>" />
    <!--Boxi : Added input control for special charecter validation in Booking Code field -->
    <input type="hidden" id="invalidBlockCodeChars" name="invalidBlockCodeChars" value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/noSpecialCaracters") %>" />
    <input type="hidden" id="defaultBookingCode" name="defaultBookingCode" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingCode") %>" />
    <input type="hidden" id="BookingCodeIata" name="BookingCodeIata" />
    <input type="hidden" id="IATANumber" name="IATANumber"/>
    <input type="hidden" id="InvalidIATAnumber" name="InvalidIATAnumber" value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/IataSpecialCaracters") %>" />
    <!--Hidden Fields end here-->
    <!--Destination Search use for createing Hotel List-->
    <div id="destinationSearch">
        <uc1:DestinationSearch id="DestinationSearch1" runat="server" />
    </div>
    <!--Add calander---->
    <div id="emptyDiv">
    </div>
    <div id="hiddenCalendar" class="calendarContainer">
        <table width="186" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td align="left" valign="top">
                    <div id="calendarLayer">
                        &nbsp;</div>
                    <div id="calendarBase">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!--create Calander-->
    <div id="calendar">
        <uc2:Calendar id="Calendar1" runat="server" />
    </div>
</div>

<script type="text/javascript" language="javascript">
    var requestUrl = <%= "\"" + GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") + "\"" %>;
    ShowAppropriateAccordian('#SecondaryBodyArea',$fn(_endsWith("sT")).value);
//    var currentaccordian = '<%= BookingTab.Tab1 %>';    
    associateControlBasedOnAccordianSelected();
    initBB();
    //R2.0 Bug id:409102-Ashish
     scandic.agSelct();     
     //End R2.0 Bug id:409102-Ashish
     
var errorInSettingBedTypeRGB = "<%=errorInBedTypeSelectionInRGB%>";
var errorInSettingBedTypeBCB = "<%=errorInBedTypeSelectionInBCB%>";
var errorInSettingBedTypeRNB = "<%=errorInBedTypeSelectionInRNB%>";
var errorPriorDateTypeRGB = "<%=errorPriorDateInRGB%>";
 var errorPriorDateTypeBCB = "<%=errorPriorDateInBCB%>";
 var errorPriorDateTypeRNB = "<%=errorPriorDateInRNB%>";

$(function(){
    if(errorInSettingBedTypeRGB == "True"){   
   
       // Get the No of Rooms
       var noOfRoom = $('#tabContainer1 .roomSelect').find(":selected").val();

       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlChildPerRoom1")).id).find(":selected").val();
       var noOfChild2 = $("#"+ $fn(_endsWith("ddlChildPerRoom2")).id).find(":selected").val();
       var noOfChild3 = $("#"+ $fn(_endsWith("ddlChildPerRoom3")).id).find(":selected").val();
       var noOfChild4 = $("#"+ $fn(_endsWith("ddlChildPerRoom4")).id).find(":selected").val();         
       selectBedTypeError(noOfRoom, noOfChild1, noOfChild2, noOfChild3, noOfChild4, "choutput", "DFT");
    }
    else if(errorInSettingBedTypeBCB == "True"){
       // Get the No of Rooms
       var noOfRoom = $('#tabContainer2 .roomSelect').find(":selected").val();

       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom1")).id).find(":selected").val();
       var noOfChild2 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom2")).id).find(":selected").val();
       var noOfChild3 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom3")).id).find(":selected").val();
       var noOfChild4 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom4")).id).find(":selected").val();         
       selectBedTypeError(noOfRoom, noOfChild1, noOfChild2, noOfChild3, noOfChild4, "choutputBC", "DFT");
    }
    else if(errorInSettingBedTypeRNB == "True"){
  // Get the No of Rooms
       var noOfRoom = $('#tabContainer3 .roomSelect').find(":selected").val();

       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlRNChildPerRoom1")).id).find(":selected").val();
               
       selectBedTypeError(noOfRoom, noOfChild1, "", "", "", "choutputRN", "DFT");
    }	
	
	if(errorPriorDateTypeRGB == "True"){   
   
       // Get the No of Rooms
       var noOfRoom = $('#tabContainer1 .roomSelect').find(":selected").val();

       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlChildPerRoom1")).id).find(":selected").val();
       var noOfChild2 = $("#"+ $fn(_endsWith("ddlChildPerRoom2")).id).find(":selected").val();
       var noOfChild3 = $("#"+ $fn(_endsWith("ddlChildPerRoom3")).id).find(":selected").val();
       var noOfChild4 = $("#"+ $fn(_endsWith("ddlChildPerRoom4")).id).find(":selected").val();         
       setChildrenDetails(noOfRoom, noOfChild1, noOfChild2, noOfChild3, noOfChild4, "choutput", "DFT");
    }
    else if(errorPriorDateTypeBCB == "True"){
       // Get the No of Rooms
       var noOfRoom = $('#tabContainer2 .roomSelect').find(":selected").val();

       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom1")).id).find(":selected").val();
       var noOfChild2 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom2")).id).find(":selected").val();
       var noOfChild3 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom3")).id).find(":selected").val();
       var noOfChild4 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom4")).id).find(":selected").val();         
       setChildrenDetails(noOfRoom, noOfChild1, noOfChild2, noOfChild3, noOfChild4, "choutputBC", "DFT");
    }
    else if(errorPriorDateTypeRNB == "True"){
  // Get the No of Rooms
       var noOfRoom = $('#tabContainer3 .roomSelect').find(":selected").val();

       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlRNChildPerRoom1")).id).find(":selected").val();
               
       setChildrenDetails(noOfRoom, noOfChild1, "", "", "", "choutputRN", "DFT");
    }
});
     
     $(document).ready(function() {
        appendPrefix("txtUserNamePrefix");
         $("input[id$='txtUserName']").bind('focusout',TrimWhiteSpace);
	 });




function TrimWhiteSpace() {

	    trimWhiteSpaces("txtUserName");
        replaceDuplicatePrefix("txtUserName");
	}
     
     
</script>
