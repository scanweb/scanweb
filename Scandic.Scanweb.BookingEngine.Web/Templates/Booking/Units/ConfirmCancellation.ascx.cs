//  Description					: Code Behind class for ModifyBookingDates Control			  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Girish Krishnan                                	  //
/// Author email id				:                           							  //
/// Creation Date				: 10th December  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////
#region System Namespaces
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
#endregion

#region Scandic Namespaces
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.BookingEngine.Entity;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.ExceptionManager;
#endregion
namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    public partial class ConfirmCancellation : EPiServer.UserControlBase
    {
        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Populate the UI
                PopulateUI();
            }
        }

        protected void btnCancelBooking_Click(object sender, EventArgs args)
        {
            try
            {
                //  Create an instance of resrevation controller
                ReservationController reservationController = new ReservationController();

                BookingDetailsEntity bookingDetails = SessionWrapper.BookingDetails;
                if (bookingDetails != null && (bookingDetails.HotelSearch != null))
                {
                    CancelDetailsEntity cancelDetailsEntity = null;

                    cancelDetailsEntity = new CancelDetailsEntity();
                    cancelDetailsEntity.HotelCode = bookingDetails.HotelSearch.SelectedHotelCode;
                    cancelDetailsEntity.ReservationNumber = SessionWrapper.ReservationNumber;                    
                    cancelDetailsEntity.ChainCode = AppConstants.CHAIN_CODE;
                    cancelDetailsEntity.CancelType = CancelTermType.Cancel;
                    if (reservationController.CancelBooking(cancelDetailsEntity))
                    {
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.CANCELLED_BOOKING), false);
                    }
                }
                else
                {
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH), false);
                }
            }
            catch (OWSException owsException)
            {
            }
            catch (Exception owsException)
            { 
            }
        }

        protected void btnNo_Click(object sender, EventArgs args)
        {
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_CHANGE_DATES), false);
        
        }

        #endregion Page Events

        #region Private Methods


        /// <summary>
        /// Populate the UI
        /// </summary>
        private void PopulateUI()
        {
            try
            {
                //Retrieve the Booking Details from the sesssion
                BookingDetailsEntity bookingDetails = SessionWrapper.BookingDetails;
                if (bookingDetails != null)
                {
                    HotelSearchEntity hotelSearchEntity = bookingDetails.HotelSearch;
                    HotelRoomRateEntity hotelRoomRateEntity = bookingDetails.HotelRoomRate;
                    GuestInformationEntity guestInfoEntity = bookingDetails.GuestInformation;
                    if ((hotelSearchEntity != null) && (hotelRoomRateEntity != null) && (guestInfoEntity != null))
                    {
                        //Populate the Reservation Info Container
                        PopulateReservationInfoContainer(hotelSearchEntity, hotelRoomRateEntity, guestInfoEntity);
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionManager.Utils.LogTrace(exception.Message);
                ExceptionManager.Utils.LogTrace(exception.StackTrace);
                WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                            WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
            }
        }


        /// <summary>
        /// Populate the Reservation Info Container
        /// </summary>
        /// <param name="hotelSearchEntity">
        /// HotelSearchEntity
        /// </param>
        /// <param name="hotelRoomRateEntity">
        /// HotelRoomRateEntity
        /// </param>
        /// <param name="guestInfoEntity">
        /// GuestInformationEntity
        /// </param>
        private void PopulateReservationInfoContainer(HotelSearchEntity hotelSearchEntity,
            HotelRoomRateEntity hotelRoomRateEntity, GuestInformationEntity guestInfoEntity)
        {
            //BaseRoomRateDetails roomRateDetails = null;
            HotelDestination hotelDestination = null;
            if ((hotelSearchEntity != null) && (hotelRoomRateEntity != null))
            {
                AvailabilityController availabilityController = new AvailabilityController();
                hotelDestination = availabilityController.GetHotelDestinationEntity(hotelSearchEntity.SelectedHotelCode);

                HtmlAnchor hotelAnchor = new HtmlAnchor();
                hotelAnchor.Attributes.Add("onclick", "showDesc(this,'blkHotelDetails'); return false");
                hotelAnchor.Attributes.Add("href", "#");
                hotelAnchor.ID = "Hotel";
                ReservationInfo.CityOrHotelNameControl.Controls.Add(hotelAnchor);
                if (hotelDestination != null)
                {
                    hotelAnchor.InnerText = hotelDestination.Name;
                    ReservationInfo.HotelInfoBlock = hotelDestination;
                }
                ReservationInfo.NoOfRooms = hotelSearchEntity.RoomsPerNight;
                ReservationInfo.ArrivalDate = hotelSearchEntity.ArrivalDate;
                ReservationInfo.DepartureDate = hotelSearchEntity.DepartureDate;
                ReservationInfo.NoOfAdults = hotelSearchEntity.AdultsPerRoom;
                ReservationInfo.NoOfChildren = hotelSearchEntity.ChildrenPerRoom;
                ReservationInfo.NoOfDays = hotelSearchEntity.NoOfNights;

                //Display the Reservation Message
                string username = guestInfoEntity.FirstName + " " + guestInfoEntity.LastName;
                string reservationMessage = WebUtil.GetTranslatedText(TranslatedTextConstansts.YOUR_BOOKING_RESERVATION_MESSAGE);
                reservationMessage = string.Format(reservationMessage, username, SessionWrapper.ReservationNumber);
                ReservationInfo.ReservationNumberMessage = reservationMessage;

                string hotelCountryCode = hotelSearchEntity.HotelCountryCode;
                TimeSpan timeSpan = hotelSearchEntity.ArrivalDate.Subtract(hotelSearchEntity.DepartureDate);
                double basePoints = 0;
                double totalPoints = 0;

                basePoints = hotelRoomRateEntity.Points;
                if (hotelRoomRateEntity.TotalRate != null)
                {
                    totalPoints = hotelRoomRateEntity.TotalRate.Rate;
                }

                //Get the Selected Rate Category Name
                string ratePlanCode = hotelRoomRateEntity.RatePlanCode;
                string selectedRateCategoryName = string.Empty;
                if ((ratePlanCode != null) && (ratePlanCode != string.Empty))
                {
                    Rate rate = RoomRateUtil.GetRate(ratePlanCode);
                    if (rate != null)
                    {
                        selectedRateCategoryName = rate.RateCategoryName;
                    }
                }

                string baseRateString = Utility.GetRoomRateString(selectedRateCategoryName, hotelRoomRateEntity.Rate, basePoints, hotelSearchEntity.SearchingType, hotelSearchEntity.ArrivalDate.Year, hotelCountryCode);
                string totalRateString = string.Empty;
                if (hotelRoomRateEntity.TotalRate != null)
                {
                    totalRateString = Utility.GetRoomRateString(null, hotelRoomRateEntity.TotalRate, totalPoints, hotelSearchEntity.SearchingType, hotelSearchEntity.ArrivalDate.Year, hotelCountryCode);
                }
                ReservationInfo.SetRoomRate(baseRateString);
                ReservationInfo.SetTotalRoomRate(totalRateString);
            }
        }
        #endregion
    }
}