//  Description					: SelectRate                                              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using System.Web;


namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for SelectRate
    /// </summary>
    public partial class SelectRate : EPiServer.UserControlBase, INavigationTraking
    {
        public string SELECT_RATE_ALL_ROOM_TYPES =
            WebUtil.GetTranslatedText(TranslatedTextConstansts.SELECT_RATE_ALL_ROOM_TYPES);

        public string SELECT_RATE_SPECIFIC_ROOM_TYPES =
            WebUtil.GetTranslatedText(TranslatedTextConstansts.SELECT_RATE_SPECIFIC_ROOM_TYPES);

        private bool isSessionValid = true;
        public string roomNumber = null;

        public string RoomUnavailableErrorMessage
        {
            get
            {
                return operaErrorMsg.InnerHtml;
            }
            set
            {
                operaErrorMsg.InnerHtml = value;
            }
        }

        #region Page_Load

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SearchCriteriaSessionWrapper.IsPromocodeInvalidForHotel)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "page_load", WebUtil.ShowSearchHotelWidgetOnPageLoad());
                if (!string.IsNullOrEmpty(WebUtil.Booking_Promo_Code))
                    DisplayErrorMessage(string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/noroomratewithdnumber"), WebUtil.Booking_Promo_Code));
                else
                    DisplayErrorMessage(string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/noroomratewithdnumber"), ""));
            }
            if (SearchCriteriaSessionWrapper.SearchCriteria == null)
            {
                StringBuilder setIsSearchCriteriaValid = new StringBuilder();
                setIsSearchCriteriaValid.Append("<script type=\"text/javascript\">");
                setIsSearchCriteriaValid.Append("isSearchCriteriaValid = false;");
                setIsSearchCriteriaValid.Append("</script>");
                Page.ClientScript.RegisterStartupScript(this.GetType(), "page_load", setIsSearchCriteriaValid.ToString());
            }
            if (HttpContext.Current.Request.UrlReferrer != null)
            {
                if (HttpContext.Current.Request.UrlReferrer.ToString().Contains("Reservation/Select-Hotel/") && Reservation2SessionWrapper.AvailabilityCalendarAccessed)
                    Reservation2SessionWrapper.AvailabilityCalendarAccessed = false;
            }

            AppLogger.LogInfoMessage("Select rate:ascx StartTime:::" +
                                                          DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));

            //Merchandising:R3:Display ordinary rates for unavailable promo rates 
            availabilityCalendarControl.Visible = true;
            RoomTab.Visible = true;

            perNight.Text = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rdoPernight");
            perStay.Text = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rdoperStay");

            blkRoomDetailContainer.Controls.Clear();
            ProgressBar.SetProgressBarSelection(EpiServerPageConstants.SELECT_RATE_PAGE);
            HotelName.SetPageType(EpiServerPageConstants.SELECT_RATE_PAGE);
            RoomTab.SetPageType(EpiServerPageConstants.SELECT_RATE_PAGE);
            isSessionValid = WebUtil.IsSessionValid();
            if (Visible && isSessionValid)
            {
                HotelDestination hotelDestination = null;
                try
                {
                    if ((txtRoomCategoryID.Value.Length > 0) && (txtRateCategory.Value.Length > 0) &&
                        (txtRoomNumber.Value.Length) > 0)
                    {
                        roomNumber = txtRoomNumber.Value;

                        OnSelectRoomRate(null, null);
                    }

                    if (!string.IsNullOrEmpty(txtPerStay.Value))
                    {
                        Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage = Convert.ToBoolean(txtPerStay.Value);
                    }
                    perNight.Checked = !Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage;
                    perStay.Checked = Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage;

                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                    if (hotelSearch != null)
                    {
                        if (!IsPostBack)
                        {
                            if (Reservation2SessionWrapper.AvailabilityCalendar != null && Reservation2SessionWrapper.AvailabilityCalendar.SearchState == SearchState.COMPLETED && !Reservation2SessionWrapper.AvailabilityCalendarAccessed)
                            {
                                Reservation2SessionWrapper.AvailabilityCalendar = null;
                            }

                            if (!Reservation2SessionWrapper.AvailabilityCalendarAccessed)
                            {
                                CreateAvailabilityCalender(hotelSearch);
                            }
                        }

                        DisplaySpecialAlert(hotelSearch);

                        OnOffPerStayPerNight(hotelSearch);

                        hotelDestination = ContentDataAccess.GetHotelByOperaID(hotelSearch.SelectedHotelCode);

                        if (null != hotelDestination)
                        {
                            if (hotelDestination.IsAvailableInBooking)
                            {
                                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                                {
                                    Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage = true;
                                    DisplayRates(true, false);
                                }
                                else
                                {
                                    DisplayRates(Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage, true);
                                }
                            }
                            else
                            {
                                HideMainBody();
                                DisplayErrorMessage(
                                    WebUtil.GetTranslatedText(
                                        "/scanweb/bookingengine/errormessages/businesserror/customerMessage"));
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            if (!ContentDataAccess.IsEditOrPreviewMode)
                            {
                                HttpContext.Current.Response.Redirect(string.Format("{0}?SessionExpired=true", HttpContext.Current.Request.RawUrl));
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                                HttpContext.Current.Response.End();
                            }
                        }
                        catch
                        {
                        }
                    }
                }
                catch (OWSException owsException)
                {
                    AppLogger.LogInfoMessage(AppConstants.OWS_EXCEPTION + " \n OWS Error Message: " +
                                             owsException.Message +
                                             "\n OWS Stack trace: " + owsException.StackTrace);
                    HandleOwsExceptions(owsException, hotelDestination);

                }
                catch (CMSException cmsException)
                {
                    HandleCmsExceptions(cmsException, hotelDestination);
                }
                catch (ContentDataAccessException cdaException)
                {
                    if (string.Equals(cdaException.ErrorCode, AppConstants.BONUS_CHEQUE_NOT_FOUND,
                                      StringComparison.InvariantCultureIgnoreCase))
                    {
                        HideMainBody();
                        DisplayErrorMessage(
                            WebUtil.GetTranslatedText(
                                "/scanweb/bookingengine/errormessages/businesserror/bonuschequenotfound"));
                        
                    }
                    else
                        WebUtil.ApplicationErrorLog(cdaException);
                }
                catch (Exception exception)
                {
                    WebUtil.ApplicationErrorLog(exception);
                }
            }
            AppLogger.LogInfoMessage("Select rate:ascx EndTime:::"
                                                           + DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
        }

        #endregion Page_Load

        private void OnOffPerStayPerNight(HotelSearchEntity hotelSearch)
        {
            switch (hotelSearch.SearchingType)
            {
                case SearchType.VOUCHER:
                case SearchType.REDEMPTION:
                    {
                        perStay.Visible = true;
                        perStay.Checked = true;
                        perNight.Visible = false;
                        perNight.Checked = false;
                    }
                    break;
                default:
                    {
                        perStay.Visible = true;
                    }
                    break;
            }
        }

        private void DisplaySpecialAlert(HotelSearchEntity hotelSearch)
        {
            string spAlert = ContentDataAccess.GetSpecialAlert(hotelSearch.SelectedHotelCode, false);
            if (!string.IsNullOrEmpty(spAlert))
            {
                divSpAlertWrap.Visible = true;
                divSpAlert.InnerHtml = spAlert;
            }
            else
            {
                divSpAlertWrap.Visible = false;
            }

        }

        private void HandleOwsExceptions(OWSException owsException, HotelDestination hotelDestination)
        {
            switch (owsException.ErrCode)
            {
                case OWSExceptionConstants.PROMOTION_CODE_INVALID:
                    {
                        SearchCriteriaSessionWrapper.IsPromocodeInvalidForHotel = true;
                        HideMainBody();
                        HandleInvalidPromoCodes(owsException, hotelDestination);
                        break;
                    }
                case OWSExceptionConstants.PROPERTY_NOT_AVAILABLE:
                case OWSExceptionConstants.INVALID_PROPERTY:
                case OWSExceptionConstants.PROPERTY_RESTRICTED:
                case OWSExceptionConstants.PRIOR_STAY:
                    {
                        if (hotelDestination != null)
                        {
                            DisplayAlternateHotels(hotelDestination);
                        }
                        break;
                    }
                default:
                    if (owsException.ErrMessage != null &&
                        owsException.ErrMessage == "No RateCodes/Rooms Available:")
                    {
                        DisplayAlternateHotels(hotelDestination);
                    }
                    else
                    {
                        WebUtil.ShowApplicationError(
                            WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                            WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                    }
                    break;
            }
        }

        private void HandleInvalidPromoCodes(OWSException owsException, HotelDestination hotelDestination)
        {
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            DisplayErrorMsgForInvalidPromoCode(owsException, hotelSearch);
            InitiateSearchIfOwsExceptions(hotelSearch, hotelDestination);
        }

        private void DisplayErrorMsgForInvalidPromoCode(OWSException owsException, HotelSearchEntity hotelSearch)
        {
            //hotelSearch.SearchingType = Utility.IsBlockCodeBooking ? SearchType.BLOCKCODE : hotelSearch.SearchingType;
            var searchingType = Utility.IsBlockCodeBooking ? SearchType.BLOCKCODE : hotelSearch.SearchingType;

            //switch (hotelSearch.SearchingType)
            switch (searchingType)
            {
                case SearchType.REGULAR:
                    {
                        errorLabel.InnerText = WebUtil.GetTranslatedText(owsException.TranslatePath);
                        errorAlertDiv.Visible = true;
                        btnBack.Visible = true;

                    }
                    break;
                case SearchType.VOUCHER:
                case SearchType.BLOCKCODE:
                case SearchType.CORPORATE:
                    {
                        errorLabel.InnerText =
                            WebUtil.GetTranslatedText(TranslatedTextConstansts.VOUCHER_CODE_INVALID);

                    }
                    break;
            }

        }

        private void InitiateSearchIfOwsExceptions(HotelSearchEntity hotelSearch, HotelDestination hotelDestination)
        {
            switch (SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.UserSearchType)
            {
                case SearchedForEntity.LocationSearchType.City:
                    {
                        NormalHotelSearch(hotelSearch);
                    }
                    break;
                case SearchedForEntity.LocationSearchType.Hotel:
                    {
                        //Merchandising:R3:Display ordinary rates for unavailable promo rates 
                        if (null != hotelDestination && hotelSearch != null)
                        {
                            if ((hotelSearch.CampaignCode != null && hotelSearch.SearchingType == SearchType.REGULAR))
                                NormalHotelSearch(hotelSearch);
                            else
                                DisplayAlternateHotels(hotelDestination);
                        }
                    }
                    break;
            }
        }
        private void HandleCmsExceptions(CMSException cmsExp, HotelDestination hotelDestination)
        {
            switch (cmsExp.ErrCode)
            {
                case CMSExceptionConstants.BLOCK_NOT_CONFIGURED:
                    {
                        HideMainBody();
                        HotelRoomRateSessionWrapper.ListHotelRoomRate = null;
                        AlternateHotelsSessionWrapper.DisplayNoHotelsAvailable = true;
                        errorLabel.InnerText = WebUtil.GetTranslatedText(cmsExp.TranslatePath);
                        if (null != hotelDestination)
                        {
                            DisplayAlternateHotels(hotelDestination);
                        }
                        break;
                    }
            }
        }

        /// <summary>
        /// Event handler for show room types per condition.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowRoomTypesPerCondition(object sender, EventArgs e)
        {
            bool showPricePerStay = true;
            RadioButton selectedRadio = (sender) as RadioButton;
            if (selectedRadio.ClientID.ToLower().EndsWith("pernight"))
                showPricePerStay = false;
            Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage = showPricePerStay;

            ClearSelectedRoomAndRates();
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE), false);
        }

        /// <summary>
        /// Clears the selected room and rates.
        /// </summary>
        private void ClearSelectedRoomAndRates()
        {
            Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
            if (selectedRoomAndRatesHashTable != null)
            {
                selectedRoomAndRatesHashTable.Clear();
                selectedRoomAndRatesHashTable = null;
            }

            UnBlockOnRdBtnChange();
        }

        /// <summary>
        /// Function Thatb calls webutil function to unblock the rooms
        /// </summary>
        private void UnBlockOnRdBtnChange()
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if (search != null && search.ListRooms != null && search.ListRooms.Count > 0)
            {
                for (int iListRoom = 0; iListRoom < search.ListRooms.Count; iListRoom++)
                {
                    if (search.ListRooms[iListRoom].IsBlocked &&
                        !string.IsNullOrEmpty(search.ListRooms[iListRoom].RoomBlockReservationNumber))
                    {
                        WebUtil.UnblockRoom(search.ListRooms[iListRoom].RoomBlockReservationNumber);
                        search.ListRooms[iListRoom].RoomBlockReservationNumber = string.Empty;
                        search.ListRooms[iListRoom].IsBlocked = false;
                    }
                }
            }
        }

        /// <summary>
        /// Perform Regular Search
        /// </summary>
        /// <param name="hotelSearch"></param>
        private void NormalHotelSearch(HotelSearchEntity hotelSearch)
        {
            hotelSearch.SearchingType = SearchType.REGULAR;
            //hotelSearch.CampaignCode = string.Empty;
            WebUtil.Booking_Promo_Code = hotelSearch.CampaignCode;
            hotelSearch.CampaignCode = null;
            SearchCriteriaSessionWrapper.SearchCriteria = hotelSearch;
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE), false);
        }

        private IList<BaseRoomRateDetails> GetPublicRatesForPromotionalSearch(HotelSearchEntity hotelSearch)
        {
            IList<BaseRoomRateDetails> listpublicRateDetails = SelectRoomUtil.MultiRoomAvailSearchOrPrefetch(hotelSearch);
            return listpublicRateDetails;
        }

        private void DisplayRates(bool showPricePerStay, bool showAllRoomTypes)
        {
            bool SelectRateLogin = true;
            string strCampaignCode = null;
            if (String.IsNullOrEmpty(Request.QueryString["SR-LogIn"]).Equals(false))
                SelectRateLogin = false;
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("Select rate:MultiRoomAvailSearchOrPrefetch() StartTime:::"
                                                          + DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            IList<BaseRoomRateDetails> listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
            if ((!IsPostBack || listRoomRateDetails == null) && SelectRateLogin)
            {
                listRoomRateDetails = SelectRoomUtil.MultiRoomAvailSearchOrPrefetch(hotelSearch);

                if (Convert.ToBoolean(AppConstants.ENABLE_PUBLIC_RATE_SEARCH_FOR_PROMOTIONS))
                {
                    if (hotelSearch.SearchingType == SearchType.REGULAR && !string.IsNullOrEmpty(hotelSearch.CampaignCode))
                    {
                        strCampaignCode = hotelSearch.CampaignCode;
                        hotelSearch.CampaignCode = null;
                        AppLogger.LogInfoMessage("Promo Search: An extra call to fetch the Public Rates for Promotional Search");
                        IList<BaseRoomRateDetails> listHotelPublicRoomRateDetails = GetPublicRatesForPromotionalSearch(hotelSearch);
                        hotelSearch.CampaignCode = strCampaignCode;
                        listRoomRateDetails = SelectRoomUtil.MergePublicRatesWithPromotionalRates(listRoomRateDetails, listHotelPublicRoomRateDetails);
                    }
                }
                HotelRoomRateSessionWrapper.ListHotelRoomRate = listRoomRateDetails;

            }
            AppLogger.LogInfoMessage("Select rate:MultiRoomAvailSearchOrPrefetch() EndTime:::"
                                                          + DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("Select rate:DisplayRates():UI StartTime:::"
                                                          + DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));

            List<RoomCategoryEntity> roomCategoryListforPopup = new List<RoomCategoryEntity>();
            if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
            {
                for (int roomNumber = 0; roomNumber < listRoomRateDetails.Count; roomNumber++)
                {
                    BaseRoomRateDetails roomRateDetails = listRoomRateDetails[roomNumber];
                    if ((Utility.IsBlockCodeBooking) && (roomRateDetails.RateCategories == null)
                        && (roomRateDetails.RateRoomCategories == null) ||
                        (roomRateDetails.RateRoomCategories.Count == 0))
                    {
                        HotelDestination hotelDestination = roomRateDetails.HotelDestination;
                        DisplayAlternateHotels(hotelDestination);
                        return;
                    }
                    else if ((!Utility.IsBlockCodeBooking) && ((roomRateDetails.RateCategories == null
                                                                || (roomRateDetails.RateCategories.Count == 0)
                                                                || (roomRateDetails.RateRoomCategories == null))))
                    {
                        HotelDestination hotelDestination = roomRateDetails.HotelDestination;
                        DisplayAlternateHotels(hotelDestination);
                        return;
                    }
                    else
                    {
                        BaseRateDisplay rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);

                        bool defaultRoomTypesAvailable =
                            RoomRateDisplayUtil.RoomTypeAvailable(rateDisplay.RateRoomCategories,
                                                                  SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode);

                        List<RoomCategoryEntity> roomCategoryList = rateDisplay.RateRoomCategories;

                        RoomCategoryEntity configuredRoomCategory = null;
                        if (roomCategoryList != null)
                        {
                            for (int roomCategoryCount = 0;
                                 roomCategoryCount < roomCategoryList.Count;
                                 roomCategoryCount++)
                            {
                                RoomCategoryEntity roomCategory = roomCategoryList[roomCategoryCount];

                                if (rateDisplay.HotelDestination != null &&
                                    rateDisplay.HotelDestination.HotelRoomDescriptions != null
                                    && rateDisplay.HotelDestination.HotelRoomDescriptions.Count > 0)
                                {
                                    bool roomCategoryFound = false;
                                    foreach (
                                        HotelRoomDescription hotelRoomDescription in
                                            rateDisplay.HotelDestination.HotelRoomDescriptions)
                                    {
                                        if (
                                            roomCategoryList[roomCategoryCount].Id.Equals(
                                                hotelRoomDescription.HotelRoomCategory.RoomCategoryId))
                                        {
                                            configuredRoomCategory = new RoomCategoryEntity();
                                            configuredRoomCategory.Id = roomCategoryList[roomCategoryCount].Id;

                                            if (
                                                !string.IsNullOrEmpty(
                                                    hotelRoomDescription.HotelRoomCategory.RoomCategoryDescription))
                                                configuredRoomCategory.Description =
                                                    hotelRoomDescription.HotelRoomCategory.RoomCategoryDescription;
                                            else
                                                configuredRoomCategory.Description =
                                                    roomCategoryList[roomCategoryCount].Description;
                                            configuredRoomCategory.Url = hotelRoomDescription.HotelRoomCategory.ImageURL;
                                            roomCategoryFound = true;
                                            break;
                                        }
                                    }

                                    if (!roomCategoryFound)
                                    {
                                        configuredRoomCategory = new RoomCategoryEntity();
                                        configuredRoomCategory.Id = roomCategoryList[roomCategoryCount].Id;
                                        configuredRoomCategory.Description =
                                            roomCategoryList[roomCategoryCount].Description;
                                    }
                                }

                                if (rateDisplay != null && rateDisplay.HotelDestination != null &&
                                    rateDisplay.HotelDestination.HotelRoomDescriptions != null
                                    && rateDisplay.HotelDestination.HotelRoomDescriptions.Count == 0)
                                {
                                    bool roomCategoryFound = false;
                                    for (int i = 0; i <= roomCategoryList.Count; i++)
                                    {
                                        configuredRoomCategory = new RoomCategoryEntity();
                                        configuredRoomCategory.Id = roomCategoryList[roomCategoryCount].Id;
                                        configuredRoomCategory.Description =
                                            roomCategoryList[roomCategoryCount].Description;
                                        roomCategoryFound = true;
                                        break;
                                    }
                                    if (!roomCategoryFound)
                                    {
                                        configuredRoomCategory = new RoomCategoryEntity();
                                        configuredRoomCategory.Id = roomCategoryList[roomCategoryCount].Id;
                                        configuredRoomCategory.Description =
                                            roomCategoryList[roomCategoryCount].Description;
                                    }
                                }

                                if (configuredRoomCategory != null)
                                {
                                    configuredRoomCategory.Name = roomCategoryList[roomCategoryCount].Name;
                                }

                                if ((configuredRoomCategory != null) &&
                                    (!roomCategoryListforPopup.Contains(configuredRoomCategory)))
                                    roomCategoryListforPopup.Add(configuredRoomCategory);
                            }
                        }

                        ProcessRoomAndRateResults(rateDisplay, defaultRoomTypesAvailable,
                                                   showPricePerStay, showAllRoomTypes, roomNumber);

                    }

                    ProcessRoomRateDetails(hotelSearch, roomRateDetails);

                }
                ProcessRoomDescriptionPopups(roomCategoryListforPopup);
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("Select rate:DisplayRates():UI EndTime:::" +
                                                          DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
        }

        private void ProcessRoomAndRateResults(BaseRateDisplay rateDisplay, bool defaultRoomTypesAvailable, bool showPricePerStay, bool showAllRoomTypes, int roomNumber)
        {
            RoomTypeRateContainer roomTypeRateControl;
            roomTypeRateControl = LoadControl("RoomTypeRateContainer.ascx") as RoomTypeRateContainer;

            if (roomTypeRateControl != null)
            {
                roomTypeRateControl.SetRateDisplay(rateDisplay, defaultRoomTypesAvailable,
                                                   showPricePerStay, showAllRoomTypes, roomNumber);
                HtmlGenericControl dynDiv = new HtmlGenericControl("DIV");
                dynDiv.ID = "blkRoomTableContainer" + roomNumber.ToString(CultureInfo.InvariantCulture);
                dynDiv.Controls.Add(roomTypeRateControl);
                dynDiv.Attributes.Add("runat", "server");
                if (roomNumber == 0)
                {
                    dynDiv.Attributes.Add("class", "tabCntnt default room" + (roomNumber + 1));
                }
                else
                {
                    dynDiv.Attributes.Add("class", "tabCntnt room" + (roomNumber + 1));
                }

                blkRoomDetailContainer.Controls.Add(dynDiv);
            }
        }

        private void ProcessRoomDescriptionPopups(List<RoomCategoryEntity> roomCategoryListforPopup)
        {
            if (roomCategoryListforPopup == null || roomCategoryListforPopup.Count <= 0) return;

            foreach (RoomCategoryEntity configuredRoomCategory in roomCategoryListforPopup)
            {
                RoomDescriptionPopup roomDescriptionPopup;
                roomDescriptionPopup = LoadControl("RoomDescriptionPopup.ascx") as RoomDescriptionPopup;
                if (configuredRoomCategory == null || roomDescriptionPopup == null) continue;
                roomDescriptionPopup.SetRoomCategoryDetails(configuredRoomCategory);
                RoomCategoryContainer.Controls.Add(roomDescriptionPopup);
            }
        }

        private void ProcessRoomRateDetails(HotelSearchEntity hotelSearch, BaseRoomRateDetails roomRateDetails)
        {
            switch (hotelSearch.SearchingType)
            {
                case SearchType.CORPORATE:
                    {
                        ProcessCorporateSearch(roomRateDetails);
                        break;
                    }
                case SearchType.REGULAR:
                    {
                        if (null != hotelSearch.CampaignCode)
                        {
                            PromotionRoomRateDetails promoRoomRate = roomRateDetails as PromotionRoomRateDetails;
                            if (promoRoomRate != null && !promoRoomRate.HasPromtionRates)
                            {
                                HotelDestination hotelDestination = roomRateDetails.HotelDestination;
                                DisplayAlternateHotels(hotelDestination);
                                return;
                            }
                        }
                        break;
                    }
                case SearchType.VOUCHER:
                    {
                        VoucherRoomRateDetails voucherRoomRate = roomRateDetails as VoucherRoomRateDetails;
                        if (voucherRoomRate != null && (!voucherRoomRate.HasPromtionRates || !voucherRoomRate.RoomHasPromtionRates))
                        {
                            HotelDestination hotelDestination = roomRateDetails.HotelDestination;
                            DisplayAlternateHotels(hotelDestination);
                            return;
                        }
                        break;
                    }
            }
        }
        private void ProcessCorporateSearch(BaseRoomRateDetails roomRateDetails)
        {
            if (!Utility.IsBlockCodeBooking)
            {
                NegotiatedRoomRateDetails negotiatedRoomRate = roomRateDetails as NegotiatedRoomRateDetails;
                if ((null != negotiatedRoomRate) && !(negotiatedRoomRate.HasNegotiatedRates))
                {
                    string errMsg = string.Empty;
                    if (SearchCriteriaSessionWrapper.SearchCriteria != null &&
                        !string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode))
                        errMsg = string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/noroomratewithdnumber"),
                            SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                    else
                        errMsg = string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/noroomratewithdnumber"), "");

                    DisplayErrorMessage(errMsg);
                }
            }
            else
            {
                BlockCodeRoomRateDetails blockCodeRoomRate =
                    roomRateDetails as BlockCodeRoomRateDetails;
                if ((blockCodeRoomRate != null) && (!blockCodeRoomRate.IsBlockRates))
                {
                    if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                    {
                        SearchCriteriaSessionWrapper.SearchCriteria.IsBlockCodeRate = false;
                    }
                    string errMsg = string.Empty;
                    if (SearchCriteriaSessionWrapper.SearchCriteria != null &&
                        !string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode))
                        errMsg = string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/noroomratewithdnumber"),
                            SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                    else
                        errMsg = string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/noroomratewithdnumber"), "");

                    DisplayErrorMessage(errMsg);

                }
                else
                {
                    if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                    {
                        SearchCriteriaSessionWrapper.SearchCriteria.IsBlockCodeRate = true;
                    }
                }
            }
        }
        #region Alternate Hotels

        /// <summary>
        /// Display Alternate Hotels
        /// </summary>
        /// <param name="hotelDestination">
        /// HotelDestination
        /// </param>
        private void DisplayAlternateHotels(HotelDestination hotelDestination)
        {
            SearchedHotelCityNameSessionWrapper.SearchedHotelCityName = hotelDestination.HotelAddress.City;

            //Merchandising:R3:Display ordinary rates for unavailable promo rates 
            string redirectURL = string.Empty;
            string errorMsg = string.Empty;
            if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REGULAR
                && SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode != null)
            {
                if (!AlternateHotelsSessionWrapper.IsHotelAlternateFlow)
                {
                    redirectURL = RoomRateDisplayUtil.GetAlternateHotels(hotelDestination);
                    AlternateHotelsSessionWrapper.IsHotelAlternateFlow = false;
                }
            }
            else
            {
                redirectURL = RoomRateDisplayUtil.GetAlternateHotels(hotelDestination);
            }

            if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REGULAR
                && SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode != null)
            {
                if (SelectHotelUtil.TotalPromotionRateAvailableHotels() <= 0)
                {
                    redirectURL = string.Empty;
                    if (HotelRoomRateSessionWrapper.ListHotelRoomRate == null)
                    {
                        if (SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                            errorMsg = WebUtil.GetTranslatedText(AppConstants.ALTERNATE_HOTELS_PROMO_MESSAGE3);
                        else if (SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City)
                            errorMsg = WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE3);

                        availabilityCalendarControl.Visible = false;
                        RoomTab.Visible = false;
                    }
                }
            }
            if ((redirectURL != null) && (redirectURL != string.Empty))
                Response.Redirect(redirectURL, false);
            else if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REGULAR
                && SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode != null)
            {
                if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null)
                {
                    if (SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                        errorMsg = string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/noroomratewithdnumber"),
                            SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                    else if (SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City)
                        errorMsg = string.Format(WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE2),
                            SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                }
            }
            if (!string.IsNullOrEmpty(errorMsg))
                DisplayErrorMessage(WebUtil.GetTranslatedText(errorMsg));
        }

        #endregion Alternate Hotels

        /// <summary>
        /// OnSelectRoomRate event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnSelectRoomRate(object sender, EventArgs e)
        {
            SelectedRoomAndRateEntity selectedRoomRate =
                SelectRoomUtil.SetRoomAndRateSelection(txtRoomCategoryID.Value, txtRateCategory.Value,
                                                       txtRoomNumber.Value);
            UserNavTracker.TrackAction(this, "Select Rate");
            //WebUtil.ApplicationErrorLog(new Exception("Error in SelectRate"));
            //Response.End();
            string errorMsg = string.Empty;
            operaErrorMsg.InnerText = string.Empty;
            int selectedRoomIndex = -1;
            int.TryParse(roomNumber, out selectedRoomIndex);
            if (SearchCriteriaSessionWrapper.SearchCriteria != null &&
                SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REDEMPTION)
            {
                if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.ListRooms != null
                    && SearchCriteriaSessionWrapper.SearchCriteria.ListRooms.Count >= selectedRoomIndex)
                {
                    SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[selectedRoomIndex].IsBlocked = true;
                }
            }
            else
            {
                errorMsg = SelectRoomUtil.BlockRoom(selectedRoomRate, roomNumber);
            }
            if (!string.IsNullOrEmpty(errorMsg) || !string.IsNullOrEmpty(operaErrorMsg.InnerText))
            {
                errorDiv.Visible = true;

                if (operaErrorMsg.InnerText.Contains(errorMsg))
                {
                    operaErrorMsg.InnerHtml = errorMsg;
                }
                else
                {
                    operaErrorMsg.InnerHtml += errorMsg;
                }
                Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                if (selectedRoomAndRatesHashTable != null &&
                    selectedRoomAndRatesHashTable.ContainsKey(selectedRoomIndex))
                {
                    selectedRoomAndRatesHashTable.Remove(selectedRoomIndex);
                }
            }
            else
            {
                errorAlertDiv.Visible = false;
            }

            txtRoomCategoryID.Value = string.Empty;
            txtRateCategory.Value = string.Empty;
            txtRoomNumber.Value = string.Empty;

            string relativeUrl = string.Empty;
            relativeUrl = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE);
            string urlToRedirect = WebUtil.GetSecureUrl(relativeUrl);
            AppLogger.LogInfoMessage(urlToRedirect);
        }

        /// <summary>
        /// Hides the main body.
        /// </summary>
        private void HideMainBody()
        {
            divSelectRateMainBody.Visible = false;
        }


        /// <summary>
        /// Displays the error message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void DisplayErrorMessage(string message)
        {
            errorAlertDiv.Visible = true;
            this.errorLabel.InnerHtml = message;
        }

        /// <summary>
        /// Creates the availability calender.
        /// </summary>
        /// <param name="search">The search.</param>
        public void CreateAvailabilityCalender(HotelSearchEntity search)
        {
            AvailabilityCalendarEntity availabilityCalendar = Reservation2SessionWrapper.AvailabilityCalendar;
            if (availabilityCalendar == null)
            {
                availabilityCalendar = SetAvailCalendarEntities(search);
            }

            AvailabilityController availController = new AvailabilityController();
            availController.MultiroomMultidateCalendarAvailSearch(search, availabilityCalendar);
        }

        /// <summary>
        /// Sets the avail calendar entities.
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        private AvailabilityCalendarEntity SetAvailCalendarEntities(HotelSearchEntity search)
        {
            AvailabilityCalendarEntity availabilityCalendar = null;

            if (search != null)
            {
                availabilityCalendar = Reservation2SessionWrapper.AvailabilityCalendar;
                if (availabilityCalendar == null)
                {
                    availabilityCalendar = new AvailabilityCalendarEntity();
                    availabilityCalendar.LeftCarouselIndex = 0;
                    availabilityCalendar.RightCarouselIndex = 0;
                    Reservation2SessionWrapper.AvailabilityCalendar = availabilityCalendar;
                    leftVisibleCarouselIndex.Value = "0";
                    Reservation2SessionWrapper.LeftVisibleCarouselIndex = 0;
                }

                availabilityCalendar.SearchState = SearchState.NONE;
                availabilityCalendar.ListAvailCalendarItems = new List<AvailCalendarItemEntity>();

                DateTime arrivalDateLimit = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                DateTime departureDateLimit = DateTime.Today.AddYears(AppConstants.NO_OF_YEARS_IN_AVAILCALENDAR);

                DateTime iteratorArrivalDate = search.ArrivalDate.AddDays(-(double)(AppConstants.CAROUSEL_COUNT / 2));
                DateTime iteratorDepartureDate = search.DepartureDate.AddDays(-(double)(AppConstants.CAROUSEL_COUNT / 2));
                leftVisibleCarouselIndex.Value = AppConstants.DEFAULT_LEFT_VISIBLECAROUSAL_INDEX.ToString();
                Reservation2SessionWrapper.LeftVisibleCarouselIndex = AppConstants.DEFAULT_LEFT_VISIBLECAROUSAL_INDEX;
                if (iteratorArrivalDate < arrivalDateLimit)
                {
                    iteratorArrivalDate = arrivalDateLimit;
                    iteratorDepartureDate = iteratorArrivalDate.AddDays(search.NoOfNights);
                    leftVisibleCarouselIndex.Value = "0";
                    Reservation2SessionWrapper.LeftVisibleCarouselIndex = 0;
                    HtmlInputHidden firstBatch = this.Parent.FindControl("firstBatch") as HtmlInputHidden;
                    if (firstBatch != null)
                    {
                        firstBatch.Value = "true";
                    }
                    Reservation2SessionWrapper.IsCalendarFirstBatch = true;
                }

                for (int iterator = 0;
                     iterator < AppConstants.CAROUSEL_COUNT;
                     iterator++, availabilityCalendar.RightCarouselIndex++)
                {
                    if (search.ArrivalDate.Equals(iteratorArrivalDate))
                    {
                        selectedCalendarItemIndex.Value = Convert.ToString(iterator);
                        Reservation2SessionWrapper.SelectedCalendarItemIndex = iterator;

                        int step = iterator / AppConstants.VISIBLE_CAROUSEL_COUNT;
                        leftVisibleCarouselIndex.Value = Convert.ToString(step * AppConstants.VISIBLE_CAROUSEL_COUNT);
                        Reservation2SessionWrapper.LeftVisibleCarouselIndex =
                            Convert.ToInt32(step * AppConstants.VISIBLE_CAROUSEL_COUNT);
                    }
                    if (iteratorArrivalDate >= departureDateLimit)
                        break;
                    AvailCalendarItemEntity calendarItem = new AvailCalendarItemEntity();
                    calendarItem.ArrivalDate = iteratorArrivalDate;
                    calendarItem.DepartureDate = iteratorDepartureDate;
                    availabilityCalendar.ListAvailCalendarItems.Add(calendarItem);

                    iteratorArrivalDate = iteratorArrivalDate.AddDays(AppConstants.CALENDARITEM_GAP_DAYS);
                    iteratorDepartureDate = iteratorDepartureDate.AddDays(AppConstants.CALENDARITEM_GAP_DAYS);
                }
                availabilityCalendar.RightCarouselIndex--;
            }
            return availabilityCalendar;
        }

        #region INavigationTraking Members

        public List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> parameters = new List<KeyValueParam>();
            parameters.Add(new KeyValueParam("Room Category ID", txtRoomCategoryID.Value));
            parameters.Add(new KeyValueParam("Rate Category", txtRateCategory.Value));
            parameters.Add(new KeyValueParam("Room Number", txtRoomNumber.Value));
            parameters.Add(new KeyValueParam("Per Stay", txtPerStay.Value));
            return parameters;
        }

        #endregion
    }
}