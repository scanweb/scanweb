<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingReceipt.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.BookingReceipt" %>
<%@ Register Src="~/Templates/Booking/Units/BookingReceiptInformation.ascx" TagName="BookingReceiptInformation"
    TagPrefix="Booking" %>
<div class="BE">
    <div id="scandicLogo" runat="server" visible="false" class="scandicPdfLogo">
    </div>
    <div id="BookingConfirmation">
        <div id="BookingDetails" class="BookingConfirmation">
            <div id="prntWrapper" runat="server">
                <div class="clearAll">
                </div>
                <Booking:BookingReceiptInformation ID="BookingReceiptInformation" runat="server">
                </Booking:BookingReceiptInformation>
               
            </div>
        </div>
    </div>
</div>
