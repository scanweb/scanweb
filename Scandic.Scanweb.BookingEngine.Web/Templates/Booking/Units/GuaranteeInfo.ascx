<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GuaranteeInfo.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.GuaranteeInfo" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<input type="hidden" id="previousCardNumber" name="orginalCardNumber" runat="server" />
    <div class="depositInfo reterm fltLft modifyflow" id="HideUnhideGuranteeDiv" runat="server"
        visible="false" >
        <h3 id="headerGuarantee" runat="server">
            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteedeposite") %>
        </h3>
        <p class="gType" id="blkHoldRoom" runat="server" visible="false" >
                <!--<input type="radio" id="rdoHoldRoom" runat="server" name="depinfo"
                    value="holdOn" class="radio chkEnroll" />-->
                <span id="spanHoldRoom" runat="server"></span>
        </p>
        <p class="gType" id="lblGuranteewithRadio" runat="server">
            
                <!--  <input type="radio" name="depinfo" value="creditInfo" class="radio chkEnroll"
                    id="rdoLateArrivalGurantee" runat="server" />-->
                <span id="txtGuranteeWithRadio" class="lblGuarantee" runat="server"></span>
        </p>
        <p>
         <label id="lblGuranteewithoutRadio" visible="false" runat="server">
                    <span id="txtGuranteeWithoutRadio" runat="server"></span>
         </label>
        </p>    
    <!--    <div class="ccInfoDetails fltLft" id="HideUnhideCreditCardDiv" runat="server" >
            <div class="ccInfo">
                <input type="text" rel="Name on card" class="input defaultColor" name="cc_name" id="txtCardHolder" runat="server" />
                <input type="text" rel="Credit card number" class="input defaultColor" name="cc_num" id="txtCardNumber" runat="server" />
                <asp:DropDownList ID="ddlCardType" runat="server">
                </asp:DropDownList>
            </div>
            <div class="ccDate fltLft">
                <div class="formFld fltLft">
                    <label class="fltLft" for="date">
                        <%=
                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/CardExpDate") %>
                    </label>
                    <asp:DropDownList ID="ddlExpiryMonth" runat="server">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlExpiryYear" runat="server">
                    </asp:DropDownList>
                </div>
                <img height="57" width="114" alt="" class="fltRt" src="/Templates/Booking/Styles/Default/Images/verisign.gif" />
            </div>
            <%--//RK: Reservation 2.0 | ID#465382 | show the update check box for updating credit card details--%>
            <div id="divCreditCardUpdate" runat="server" visible="false">
                <p>
                    <input type="checkbox" id="updateCreditCard"/>
                     <label>
                      <%=
                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/UseSameCard") %>
                       </label>
                </p>
            </div>
        </div>-->
        
    </div>
   