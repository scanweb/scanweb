<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailConfirmation.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.EmailConfirmation" %>
<div id="Loyalty" class="BE">
	<!-- LastCard -->
	<div id="LostCard">
<div class="box-top-grey"><span class="title" id="header" runat="server"></span></div>
		<div class="box-middle">
			<div class="content">
				<div class="txtModule" id="message" runat="server">
				<p></p>
				</div>
				
				<!-- Footer -->
				<div id="FooterContainer">
				<p><span class="link"><asp:LinkButton ID="lnkbutton" runat="server" OnClick="lnkbutton_Click"></asp:LinkButton></span></p>
				</div>
				<!-- Footer -->
			</div>
		</div>
		<div class="box-bottom">&nbsp;</div>
	</div>
	<!-- /LastCard -->
</div>
			
	
			