//  Description					: Code Behind class for ModifyBookingDates Control		  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Girish Krishnan                                	      //
//  Author email id				:                           							  //
//  Creation Date				: 14th December  2007									  //
//	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces
using System;
using System.Collections.Generic;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for CancelledBooking
    /// </summary>
    public partial class CancelledBooking : EPiServer.UserControlBase
    {
        public string requestURL;

        /// <summary>
        /// Page Load Method of CancelledBooking Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (BookingEngineSessionWrapper.AllCancelledDetails != null)
            {
                LnkBtnPrint.Attributes.Add("onclick", "javascript:OpenPringDialog();return false;");
                spnBtnPrint.Attributes.Add("onclick", "javascript:OpenPringDialog();return false;");
                LnkBtnPrint.Visible = false;

                pfLnkBtnPrint.Attributes.Add("onclick", "javascript:OpenPringDialog();return false;");
                pfSpnBtnPrint.Attributes.Add("onclick", "javascript:OpenPringDialog();return false;");
                pfLnkBtnPrint.Visible = false;

                requestURL = GlobalUtil.GetUrlToPage(EpiServerPageConstants.CANCEL_PRINTER_FRIENDLY_CONFIRMATION);
                if ((!String.IsNullOrEmpty(requestURL)) && (requestURL.LastIndexOf('/') == (requestURL.Length - 1)))
                {
                    requestURL = requestURL.Substring(0, requestURL.Length - 1);
                }
                requestURL = requestURL + "?command=CancelPageprint&AllROOMSTOBESHOWN=true";
                bool isSessionValid = WebUtil.IsSessionValid();

                HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab1;

                if (!IsPostBack && isSessionValid)
                {
                    PopulateUI();

                    ReservationNumberSessionWrapper.TransactionCount++;

                    if (Request.QueryString["command"] != null)
                    {
                        if (Request.QueryString["command"].ToString() == "CancelPageprint")
                        {
                            infoAlertBox.Visible = false;
                            confirmAlertMod.Visible = false;
                            lblRoom1CancelInfo.Visible = true;
                            LnkBtnPrint.Visible = false;
                            pfLnkBtnPrint.Visible = true;
                            lnkBtnPrinterfriendly.Visible = false;
                            PrintDiv.Visible = true;
                            pfPrintDiv.Visible = true;
                            printerFriendlySection.Visible = true;
                            string isAllRoom = System.Web.HttpContext.Current.Request.QueryString["AllROOMSTOBESHOWN"];
                            int totalCancelledBooking = BookingEngineSessionWrapper.AllCancelledDetails.Count;
                            if (isAllRoom == "true")
                            {
                                for (int count = 0; count < totalCancelledBooking; count++)
                                {
                                    switch (count)
                                    {
                                        case 0:
                                            roomCncl1.Visible = true;
                                            pfRoomCncl1.Visible = true;
                                            break;
                                        case 1:
                                            roomCncl2.Visible = true;
                                            pfRoomCncl2.Visible = true;
                                            break;
                                        case 2:
                                            roomCncl3.Visible = true;
                                            pfRoomCncl3.Visible = true;
                                            break;
                                        case 3:
                                            roomCncl4.Visible = true;
                                            pfRoomCncl4.Visible = true;
                                            break;
                                    }
                                }
                            }
                            if (isAllRoom == "false")
                            {
                                string cnt = System.Web.HttpContext.Current.Request.QueryString["count"];
                                if (cnt == "0")
                                {
                                    roomCncl1.Visible = true;
                                    pfRoomCncl1.Visible = true;
                                }
                                else if (cnt == "1")
                                {
                                    roomCncl2.Visible = true;
                                    pfRoomCncl2.Visible = true;
                                }
                                else if (cnt == "2")
                                {
                                    roomCncl3.Visible = true;
                                    pfRoomCncl3.Visible = true;
                                }
                                else if (cnt == "3")
                                {
                                    roomCncl4.Visible = true;
                                    pfRoomCncl4.Visible = true;
                                }
                            }
                        }
                    }

                    if (Request.QueryString["isPDF"] != null)
                    {
                        if (Convert.ToBoolean(Request.QueryString["isPDF"]) == true)
                        {
                            roomCncl1.Visible = false;
                            roomCncl2.Visible = false;
                            roomCncl3.Visible = false;
                            roomCncl4.Visible = false;

                            PrintDiv.Visible = false;
                            lnkBtnPrinterfriendly.Visible = false;
                            pfLnkBtnPrint.Visible = false;
                            printerFriendlySection.Visible = false;
                            scandicLogo.Visible = true;
                        }
                    }
                    if (Request.QueryString["isPDF"] != null && Request.QueryString["AllROOMSTOBESHOWN"] != null)
                    {
                        infoAlertBox.Visible = false;
                        confirmAlertMod.Visible = false;
                        if (Convert.ToBoolean(Request.QueryString["isPDF"]) == true &&
                            Convert.ToBoolean(Request.QueryString["AllROOMSTOBESHOWN"]) == false)
                        {
                            MainReservationBox.Visible = false;
                            lnkBtnPrinterfriendly.Visible = false;
                        }
                    }
                    if (Request.QueryString["isPDF"] != null && Convert.ToBoolean(Request.QueryString["isPDF"]) == false)
                    {
                        pfLnkBtnPrint.Visible = false;
                        pfSpnBtnPrint.Visible = false;
                        pfscandicLogo.Visible = true;
                    }
                }
            }
            else
            {
                Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE));
            }
        }

        #region Private Methods

        /// <summary>
        /// Populate the UI
        /// </summary>
        private void PopulateUI()
        {
            try
            {
                lblGuaranteeInfo.InnerHtml = CurrentPage["GuaranteeInformation"] != null
                                                 ? CurrentPage["GuaranteeInformation"] as string
                                                 : string.Empty;
                pfLblGuaranteeInfo.InnerHtml = CurrentPage["GuaranteeInformation"] != null
                                                   ? CurrentPage["GuaranteeInformation"] as string
                                                   : string.Empty;
                PopulateCancellationInformation();
                PopulateReservationInfoContainer();
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }
        }

        /// <summary>
        /// Populate Cancellation Information
        /// </summary>
        private void PopulateCancellationInformation()
        {
            string firstName = string.Empty;
            string lastName = string.Empty;
            string title = string.Empty;
            string EmailAddress = string.Empty;

            List<CancelDetailsEntity> cancellationList = BookingEngineSessionWrapper.AllCancelledDetails;

            if (cancellationList != null && cancellationList.Count > 0)
            {
                int totalCancelledBooking = cancellationList.Count;

                string isAllRoom = System.Web.HttpContext.Current.Request.QueryString["AllROOMSTOBESHOWN"];
                string Namelabel = "<strong>" +
                                   WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/Name") +
                                   "</strong>";
                if (isAllRoom == "false")
                {
                    string cnt = System.Web.HttpContext.Current.Request.QueryString["count"];

                    CancelDetailsEntity cancelEntity1 = cancellationList[Convert.ToInt16(cnt)];
                    BookingDetailsEntity bookingDetails1 =
                        Utility.GetBookingDetail(cancelEntity1.LegNumber);

                    if (cancelEntity1 != null && bookingDetails1 != null)
                    {
                        if (bookingDetails1.GuestInformation != null && !string.IsNullOrEmpty(bookingDetails1.GuestInformation.NativeFirstName) &&
                           !string.IsNullOrEmpty(bookingDetails1.GuestInformation.NativeLastName))
                        {
                            firstName = bookingDetails1.GuestInformation.NativeFirstName;
                            lastName = bookingDetails1.GuestInformation.NativeLastName;
                            title = bookingDetails1.GuestInformation.Title;
                        }
                        else
                        {
                            firstName = bookingDetails1.GuestInformation.FirstName;
                            lastName = bookingDetails1.GuestInformation.LastName;
                            title = bookingDetails1.GuestInformation.Title;
                        }
                        if (cnt == "0")
                        {
                            reservationNo1.InnerHtml = "<strong>" +
                                                       WebUtil.GetTranslatedText(
                                                           "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                       AppConstants.SPACE + cancelEntity1.ReservationNumber +
                                                       AppConstants.HYPHEN + cancelEntity1.LegNumber + "</strong>";
                            cancelNo1.InnerHtml = "<strong>" +
                                                  WebUtil.GetTranslatedText(
                                                      "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                  AppConstants.SPACE + cancelEntity1.CancelNumber + AppConstants.SPACE +
                                                  "</strong>";
                            name1.InnerHtml = Utility.GetNameWithTitle(title, firstName, lastName);

                            email1.InnerHtml = "<strong>" +
                                               WebUtil.GetTranslatedText(
                                                   "/bookingengine/booking/missingpoint/addressemail") + " :" +
                                               AppConstants.SPACE +
                                               bookingDetails1.GuestInformation.EmailDetails.EmailID + "</strong>";
                            pfReservationNo1.InnerHtml = "<strong>" +
                                                         WebUtil.GetTranslatedText(
                                                             "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                         AppConstants.SPACE + cancelEntity1.ReservationNumber +
                                                         AppConstants.HYPHEN + cancelEntity1.LegNumber + "</strong>";
                            pfCancelNo1.InnerHtml = "<strong>" +
                                                    WebUtil.GetTranslatedText(
                                                        "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                    AppConstants.SPACE + cancelEntity1.CancelNumber + AppConstants.SPACE +
                                                    "</strong>";
                            pfName1.InnerHtml = "<strong>" + AppConstants.SPACE + Namelabel + " :" + AppConstants.SPACE +
                                                firstName + AppConstants.SPACE +
                                                lastName + AppConstants.SPACE +
                                                "</strong>";
                            pfEmail1.InnerHtml = "<strong>" +
                                                 WebUtil.GetTranslatedText(
                                                     "/bookingengine/booking/missingpoint/addressemail") + " :" +
                                                 AppConstants.SPACE +
                                                 bookingDetails1.GuestInformation.EmailDetails.EmailID + "</strong>";


                            if (!string.IsNullOrEmpty(cancelEntity1.SMSNumber))
                            {
                                mobileNumber1.Visible = true;
                                mobileNumber1.InnerHtml = "<strong>" +
                                                          WebUtil.GetTranslatedText(
                                                              "/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent") +
                                                          " :" + AppConstants.SPACE + cancelEntity1.SMSNumber +
                                                          "</strong>";
                            }
                        }
                        else if (cnt == "1")
                        {
                            reservationNo2.InnerHtml = "<strong>" +
                                                       WebUtil.GetTranslatedText(
                                                           "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                       AppConstants.SPACE + cancelEntity1.ReservationNumber +
                                                       AppConstants.HYPHEN + cancelEntity1.LegNumber + "</strong>";
                            cancelNo2.InnerHtml = "<strong>" +
                                                  WebUtil.GetTranslatedText(
                                                      "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                  AppConstants.SPACE + cancelEntity1.CancelNumber + AppConstants.SPACE +
                                                  "</strong>";
                            name2.InnerHtml = Utility.GetNameWithTitle(title, firstName, lastName);
                            email2.InnerHtml = "<strong>" +
                                               WebUtil.GetTranslatedText(
                                                   "/bookingengine/booking/missingpoint/addressemail") + " :" +
                                               AppConstants.SPACE +
                                               bookingDetails1.GuestInformation.EmailDetails.EmailID + "</strong>";

                            pfReservationNo2.InnerHtml = "<strong>" +
                                                         WebUtil.GetTranslatedText(
                                                             "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                         AppConstants.SPACE + cancelEntity1.ReservationNumber +
                                                         AppConstants.HYPHEN + cancelEntity1.LegNumber + "</strong>";
                            pfCancelNo2.InnerHtml = "<strong>" +
                                                    WebUtil.GetTranslatedText(
                                                        "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                    AppConstants.SPACE + cancelEntity1.CancelNumber + AppConstants.SPACE +
                                                    "</strong>";
                            pfName2.InnerHtml = "<strong>" + AppConstants.SPACE + Namelabel + " :" + AppConstants.SPACE + firstName + 
                                                AppConstants.SPACE + lastName + AppConstants.SPACE + "</strong>";
                            pfEmail2.InnerHtml = "<strong>" +
                                                 WebUtil.GetTranslatedText(
                                                     "/bookingengine/booking/missingpoint/addressemail") + " :" +
                                                 AppConstants.SPACE +
                                                 bookingDetails1.GuestInformation.EmailDetails.EmailID + "</strong>";
                            if (!string.IsNullOrEmpty(cancelEntity1.SMSNumber))
                            {
                                mobileNumber2.Visible = true;
                                mobileNumber2.InnerHtml = "<strong>" +
                                                          WebUtil.GetTranslatedText(
                                                              "/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent") +
                                                          " :" + AppConstants.SPACE + cancelEntity1.SMSNumber +
                                                          "</strong>";
                            }
                        }
                        else if (cnt == "2")
                        {
                            reservationNo3.InnerHtml = "<strong>" +
                                                       WebUtil.GetTranslatedText(
                                                           "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                       AppConstants.SPACE + cancelEntity1.ReservationNumber +
                                                       AppConstants.HYPHEN + cancelEntity1.LegNumber + "</strong>";
                            cancelNo3.InnerHtml = "<strong>" +
                                                  WebUtil.GetTranslatedText(
                                                      "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                  AppConstants.SPACE + cancelEntity1.CancelNumber + AppConstants.SPACE +
                                                  "</strong>";
                            name3.InnerHtml = Utility.GetNameWithTitle(title, firstName, lastName);
                            email3.InnerHtml = "<strong>" +
                                               WebUtil.GetTranslatedText(
                                                   "/bookingengine/booking/missingpoint/addressemail") + " :" +
                                               AppConstants.SPACE +
                                               bookingDetails1.GuestInformation.EmailDetails.EmailID + "</strong>";


                            pfReservationNo3.InnerHtml = "<strong>" +
                                                         WebUtil.GetTranslatedText(
                                                             "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                         AppConstants.SPACE + cancelEntity1.ReservationNumber +
                                                         AppConstants.HYPHEN + cancelEntity1.LegNumber + "</strong>";
                            pfCancelNo3.InnerHtml = "<strong>" +
                                                    WebUtil.GetTranslatedText(
                                                        "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                    AppConstants.SPACE + cancelEntity1.CancelNumber + AppConstants.SPACE +
                                                    "</strong>";
                            pfName3.InnerHtml = "<strong>" + AppConstants.SPACE + Namelabel + " :" + AppConstants.SPACE + firstName + 
                                                AppConstants.SPACE + lastName + AppConstants.SPACE + "</strong>";
                            pfEmail3.InnerHtml = "<strong>" +
                                                 WebUtil.GetTranslatedText(
                                                     "/bookingengine/booking/missingpoint/addressemail") + " :" +
                                                 AppConstants.SPACE +
                                                 bookingDetails1.GuestInformation.EmailDetails.EmailID + "</strong>";

                            if (!string.IsNullOrEmpty(cancelEntity1.SMSNumber))
                            {
                                mobileNumber3.Visible = true;
                                mobileNumber3.InnerHtml = "<strong>" +
                                                          WebUtil.GetTranslatedText(
                                                              "/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent") +
                                                          " :" + AppConstants.SPACE + cancelEntity1.SMSNumber +
                                                          "</strong>";
                            }
                        }
                        else if (cnt == "3")
                        {
                            reservationNo4.InnerHtml = "<strong>" +
                                                       WebUtil.GetTranslatedText(
                                                           "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                       AppConstants.SPACE + cancelEntity1.ReservationNumber +
                                                       AppConstants.HYPHEN + cancelEntity1.LegNumber + "</strong>";
                            cancelNo4.InnerHtml = "<strong>" +
                                                  WebUtil.GetTranslatedText(
                                                      "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                  AppConstants.SPACE + cancelEntity1.CancelNumber + AppConstants.SPACE +
                                                  "</strong>";
                            name4.InnerHtml = Utility.GetNameWithTitle(title, firstName, lastName);
                            email4.InnerHtml = "<strong>" +
                                               WebUtil.GetTranslatedText(
                                                   "/bookingengine/booking/missingpoint/addressemail") + " :" +
                                               AppConstants.SPACE +
                                               bookingDetails1.GuestInformation.EmailDetails.EmailID + "</strong>";


                            pfReservationNo4.InnerHtml = "<strong>" +
                                                         WebUtil.GetTranslatedText(
                                                             "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                         AppConstants.SPACE + cancelEntity1.ReservationNumber +
                                                         AppConstants.HYPHEN + cancelEntity1.LegNumber + "</strong>";
                            pfCancelNo4.InnerHtml = "<strong>" +
                                                    WebUtil.GetTranslatedText(
                                                        "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                    AppConstants.SPACE + cancelEntity1.CancelNumber + AppConstants.SPACE +
                                                    "</strong>";
                            pfName4.InnerHtml = "<strong>" + AppConstants.SPACE + Namelabel + " :" + AppConstants.SPACE + firstName + 
                                                AppConstants.SPACE + lastName + AppConstants.SPACE + "</strong>";
                            pfEmail4.InnerHtml = "<strong>" +
                                                 WebUtil.GetTranslatedText(
                                                     "/bookingengine/booking/missingpoint/addressemail") + " :" +
                                                 AppConstants.SPACE +
                                                 bookingDetails1.GuestInformation.EmailDetails.EmailID + "</strong>";

                            if (!string.IsNullOrEmpty(cancelEntity1.SMSNumber))
                            {
                                mobileNumber4.Visible = true;
                                mobileNumber4.InnerHtml = "<strong>" +
                                                          WebUtil.GetTranslatedText(
                                                              "/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent") +
                                                          " :" + AppConstants.SPACE + cancelEntity1.SMSNumber +
                                                          "</strong>";
                            }
                        }
                    }
                }


                for (int count = 0; count < totalCancelledBooking; count++)
                {
                    CancelDetailsEntity cancelEntity = cancellationList[count];
                    string mobileNumber = string.Empty;
                    if (cancelEntity != null)
                    {
                        BookingDetailsEntity bookingDetails =
                            Utility.GetBookingDetail(cancelEntity.LegNumber);
                        if (bookingDetails != null && bookingDetails.GuestInformation != null)
                        {
                            title = bookingDetails.GuestInformation.Title;
                            if (!string.IsNullOrEmpty(bookingDetails.GuestInformation.NativeFirstName) 
                                && !string.IsNullOrEmpty(bookingDetails.GuestInformation.NativeLastName))
                            {
                                firstName = bookingDetails.GuestInformation.NativeFirstName;
                                lastName = bookingDetails.GuestInformation.NativeLastName;
                            }
                            else
                            {
                                firstName = bookingDetails.GuestInformation.FirstName;
                                lastName = bookingDetails.GuestInformation.LastName;
                            }
                            string cancellationMessage = string.Empty;
                            cancellationMessage = "<strong>" +
                                                  WebUtil.GetTranslatedText(
                                                      "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                  AppConstants.SPACE + cancelEntity.ReservationNumber +
                                                  AppConstants.HYPHEN + cancelEntity.LegNumber + "</strong>";
                            cancellationMessage = String.Concat(cancellationMessage, " ", Namelabel, ": ",
                                                                Utility.GetNameWithTitle(title, firstName, lastName));
                            cancellationMessage += "<strong>" +
                                                   WebUtil.GetTranslatedText(
                                                       "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                   AppConstants.SPACE + cancelEntity.CancelNumber + "</strong>";
                            if (bookingDetails.GuestInformation.EmailDetails != null &&
                                bookingDetails.GuestInformation.EmailDetails.EmailID != null)
                            {
                                EmailAddress = bookingDetails.GuestInformation.EmailDetails.EmailID;
                            }
                            if ((cancelEntity != null) && (!string.IsNullOrEmpty(cancelEntity.SMSNumber)))
                            {
                                mobileNumber = cancelEntity.SMSNumber;
                            }
                            DisplayCancellationMessage(cancellationMessage, EmailAddress, mobileNumber, count + 1);

                            if (isAllRoom == "true")
                            {
                                switch (count)
                                {
                                    case 0:
                                        reservationNo1.InnerHtml = "<strong>" +
                                                                   WebUtil.GetTranslatedText(
                                                                       "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                                   AppConstants.SPACE + cancelEntity.ReservationNumber +
                                                                   AppConstants.HYPHEN + cancelEntity.LegNumber +
                                                                   "</strong>";
                                        cancelNo1.InnerHtml = "<strong>" +
                                                              WebUtil.GetTranslatedText(
                                                                  "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                              AppConstants.SPACE + cancelEntity.CancelNumber +
                                                              AppConstants.SPACE + "</strong>";
                                        name1.InnerHtml = Utility.GetNameWithTitle(title, firstName, lastName);
                                        email1.InnerHtml = "<strong>" +
                                                           WebUtil.GetTranslatedText(
                                                               "/bookingengine/booking/missingpoint/addressemail") +
                                                           " :" + AppConstants.SPACE +
                                                           bookingDetails.GuestInformation.EmailDetails.EmailID +
                                                           "</strong>";

                                        pfReservationNo1.InnerHtml = "<strong>" +
                                                                     WebUtil.GetTranslatedText(
                                                                         "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                                     AppConstants.SPACE + cancelEntity.ReservationNumber +
                                                                     AppConstants.HYPHEN + cancelEntity.LegNumber +
                                                                     "</strong>";
                                        pfCancelNo1.InnerHtml = "<strong>" +
                                                                WebUtil.GetTranslatedText(
                                                                    "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                                AppConstants.SPACE + cancelEntity.CancelNumber +
                                                                AppConstants.SPACE + "</strong>";
                                        pfName1.InnerHtml = "<strong>" + AppConstants.SPACE + Namelabel + " :" +
                                                            AppConstants.SPACE + firstName + AppConstants.SPACE + lastName +
                                                            AppConstants.SPACE + "</strong>";
                                        pfEmail1.InnerHtml = "<strong>" +
                                                             WebUtil.GetTranslatedText(
                                                                 "/bookingengine/booking/missingpoint/addressemail") +
                                                             " :" + AppConstants.SPACE +
                                                             bookingDetails.GuestInformation.EmailDetails.EmailID +
                                                             "</strong>";

                                        if (!string.IsNullOrEmpty(cancelEntity.SMSNumber))
                                        {
                                            mobileNumber1.Visible = true;
                                            mobileNumber1.InnerHtml = "<strong>" +
                                                                      WebUtil.GetTranslatedText(
                                                                          "/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent") +
                                                                      " :" + AppConstants.SPACE + cancelEntity.SMSNumber +
                                                                      "</strong>";
                                        }
                                        break;
                                    case 1:
                                        reservationNo2.InnerHtml = "<strong>" +
                                                                   WebUtil.GetTranslatedText(
                                                                       "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                                   AppConstants.SPACE + cancelEntity.ReservationNumber +
                                                                   AppConstants.HYPHEN + cancelEntity.LegNumber +
                                                                   "</strong>";
                                        cancelNo2.InnerHtml = "<strong>" +
                                                              WebUtil.GetTranslatedText(
                                                                  "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                              AppConstants.SPACE + cancelEntity.CancelNumber +
                                                              AppConstants.SPACE + "</strong>";
                                        name2.InnerHtml = Utility.GetNameWithTitle(title, firstName, lastName);

                                        email2.InnerHtml = "<strong>" +
                                                           WebUtil.GetTranslatedText(
                                                               "/bookingengine/booking/missingpoint/addressemail") +
                                                           " :" + AppConstants.SPACE +
                                                           bookingDetails.GuestInformation.EmailDetails.EmailID +
                                                           "</strong>";

                                        pfReservationNo2.InnerHtml = "<strong>" +
                                                                     WebUtil.GetTranslatedText(
                                                                         "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                                     AppConstants.SPACE + cancelEntity.ReservationNumber +
                                                                     AppConstants.HYPHEN + cancelEntity.LegNumber +
                                                                     "</strong>";
                                        pfCancelNo2.InnerHtml = "<strong>" +
                                                                WebUtil.GetTranslatedText(
                                                                    "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                                AppConstants.SPACE + cancelEntity.CancelNumber +
                                                                AppConstants.SPACE + "</strong>";
                                        pfName2.InnerHtml = "<strong>" + AppConstants.SPACE + Namelabel + " :" +
                                                            AppConstants.SPACE + firstName + AppConstants.SPACE + lastName +
                                                            AppConstants.SPACE + "</strong>";
                                        pfEmail2.InnerHtml = "<strong>" +
                                                             WebUtil.GetTranslatedText(
                                                                 "/bookingengine/booking/missingpoint/addressemail") +
                                                             " :" + AppConstants.SPACE +
                                                             bookingDetails.GuestInformation.EmailDetails.EmailID +
                                                             "</strong>";

                                        if (!string.IsNullOrEmpty(cancelEntity.SMSNumber))
                                        {
                                            mobileNumber2.Visible = true;
                                            mobileNumber2.InnerHtml = "<strong>" +
                                                                      WebUtil.GetTranslatedText(
                                                                          "/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent") +
                                                                      " :" + AppConstants.SPACE + cancelEntity.SMSNumber +
                                                                      "</strong>";
                                        }
                                        break;
                                    case 2:
                                        reservationNo3.InnerHtml = "<strong>" +
                                                                   WebUtil.GetTranslatedText(
                                                                       "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                                   AppConstants.SPACE + cancelEntity.ReservationNumber +
                                                                   AppConstants.HYPHEN + cancelEntity.LegNumber +
                                                                   "</strong>";
                                        cancelNo3.InnerHtml = "<strong>" +
                                                              WebUtil.GetTranslatedText(
                                                                  "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                              AppConstants.SPACE + cancelEntity.CancelNumber +
                                                              AppConstants.SPACE + "</strong>";
                                        name3.InnerHtml = Utility.GetNameWithTitle(title, firstName, lastName);

                                        email3.InnerHtml = "<strong>" +
                                                           WebUtil.GetTranslatedText(
                                                               "/bookingengine/booking/missingpoint/addressemail") +
                                                           " :" + AppConstants.SPACE +
                                                           bookingDetails.GuestInformation.EmailDetails.EmailID +
                                                           "</strong>";

                                        pfReservationNo3.InnerHtml = "<strong>" +
                                                                     WebUtil.GetTranslatedText(
                                                                         "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                                     AppConstants.SPACE + cancelEntity.ReservationNumber +
                                                                     AppConstants.HYPHEN + cancelEntity.LegNumber +
                                                                     "</strong>";
                                        pfCancelNo3.InnerHtml = "<strong>" +
                                                                WebUtil.GetTranslatedText(
                                                                    "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                                AppConstants.SPACE + cancelEntity.CancelNumber +
                                                                AppConstants.SPACE + "</strong>";
                                        pfName3.InnerHtml = "<strong>" + AppConstants.SPACE + Namelabel + " :" +
                                                            AppConstants.SPACE + firstName + AppConstants.SPACE + lastName +
                                                            AppConstants.SPACE + "</strong>";
                                        pfEmail3.InnerHtml = "<strong>" +
                                                             WebUtil.GetTranslatedText(
                                                                 "/bookingengine/booking/missingpoint/addressemail") +
                                                             " :" + AppConstants.SPACE +
                                                             bookingDetails.GuestInformation.EmailDetails.EmailID +
                                                             "</strong>";

                                        if (!string.IsNullOrEmpty(cancelEntity.SMSNumber))
                                        {
                                            mobileNumber3.Visible = true;
                                            mobileNumber3.InnerHtml = "<strong>" +
                                                                      WebUtil.GetTranslatedText(
                                                                          "/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent") +
                                                                      " :" + AppConstants.SPACE + cancelEntity.SMSNumber +
                                                                      "</strong>";
                                        }
                                        break;
                                    case 3:
                                        reservationNo4.InnerHtml = "<strong>" +
                                                                   WebUtil.GetTranslatedText(
                                                                       "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                                   AppConstants.SPACE + cancelEntity.ReservationNumber +
                                                                   AppConstants.HYPHEN + cancelEntity.LegNumber +
                                                                   "</strong>";
                                        cancelNo4.InnerHtml = "<strong>" +
                                                              WebUtil.GetTranslatedText(
                                                                  "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                              AppConstants.SPACE + cancelEntity.CancelNumber +
                                                              AppConstants.SPACE + "</strong>";
                                        name4.InnerHtml = Utility.GetNameWithTitle(title, firstName, lastName);

                                        email4.InnerHtml = "<strong>" +
                                                           WebUtil.GetTranslatedText(
                                                               "/bookingengine/booking/missingpoint/addressemail") +
                                                           " :" + AppConstants.SPACE +
                                                           bookingDetails.GuestInformation.EmailDetails.EmailID +
                                                           "</strong>";

                                        pfReservationNo4.InnerHtml = "<strong>" +
                                                                     WebUtil.GetTranslatedText(
                                                                         "/bookingengine/booking/bookingdetail/reservationNumber") +
                                                                     AppConstants.SPACE + cancelEntity.ReservationNumber +
                                                                     AppConstants.HYPHEN + cancelEntity.LegNumber +
                                                                     "</strong>";
                                        pfCancelNo4.InnerHtml = "<strong>" +
                                                                WebUtil.GetTranslatedText(
                                                                    "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                                AppConstants.SPACE + cancelEntity.CancelNumber +
                                                                AppConstants.SPACE + "</strong>";
                                        pfName4.InnerHtml = "<strong>" + AppConstants.SPACE + Namelabel + " :" +
                                                            AppConstants.SPACE + firstName + AppConstants.SPACE + lastName +
                                                            AppConstants.SPACE + "</strong>";
                                        pfEmail4.InnerHtml = "<strong>" +
                                                             WebUtil.GetTranslatedText(
                                                                 "/bookingengine/booking/missingpoint/addressemail") +
                                                             " :" + AppConstants.SPACE +
                                                             bookingDetails.GuestInformation.EmailDetails.EmailID +
                                                             "</strong>";

                                        if (!string.IsNullOrEmpty(cancelEntity.SMSNumber))
                                        {
                                            mobileNumber4.Visible = true;
                                            mobileNumber4.InnerHtml = "<strong>" +
                                                                      WebUtil.GetTranslatedText(
                                                                          "/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent") +
                                                                      " :" + AppConstants.SPACE + cancelEntity.SMSNumber +
                                                                      "</strong>";
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Display the Cancellation Messages
        /// </summary>
        /// <param name="cancellationMessage">
        /// Message to be displayed
        /// </param>
        /// <param name="legNumber">
        /// Leg Number of the cancelled booking
        /// </param>
        private void DisplayCancellationMessage(string cancellationMessage, string EmailAddress, string MobileNumber,
                                                int count)
        {
            switch (count)
            {
                case AppConstants.MAX_BOOKING_ROOMS_ALLOWED:
                    {
                        lblEmailRoom4.InnerHtml = EmailAddress;
                        Room4CancellationMessage.Visible = true;
                        if (!string.IsNullOrEmpty(MobileNumber))
                        {
                            SMSRoom4Div.Visible = true;
                            lblSMSRoom4.InnerHtml = MobileNumber;
                        }
                        lblRoom4CancelInfo.InnerHtml = cancellationMessage;
                        break;
                    }
                case AppConstants.MAX_BOOKING_ROOMS_ALLOWED - 1:
                    {
                        lblEmailRoom3.InnerHtml = EmailAddress;
                        Room3CancellationMessage.Visible = true;
                        if (!string.IsNullOrEmpty(MobileNumber))
                        {
                            SMSRoom3Div.Visible = true;
                            lblSMSRoom3.InnerHtml = MobileNumber;
                        }
                        lblRoom3CancelInfo.InnerHtml = cancellationMessage;
                        break;
                    }
                case (AppConstants.MAX_BOOKING_ROOMS_ALLOWED - 2):
                    {
                        lblEmailRoom2.InnerHtml = EmailAddress;
                        Room2CancellationMessage.Visible = true;
                        if (!string.IsNullOrEmpty(MobileNumber))
                        {
                            SMSRoom2Div.Visible = true;
                            lblSMSRoom2.InnerHtml = MobileNumber;
                        }
                        
                        lblRoom2CancelInfo.InnerHtml = cancellationMessage;
                        break;
                    }
                case (AppConstants.MAX_BOOKING_ROOMS_ALLOWED - 3):
                    {
                        Room1CancellationMessage.Visible = true;
                        lblEmailRoom1.InnerHtml = EmailAddress;
                        if (!string.IsNullOrEmpty(MobileNumber))
                        {
                            SMSRoom1Div.Visible = true;
                            lblSMSRoom1.InnerHtml = MobileNumber;
                        }

                        lblRoom1CancelInfo.InnerHtml = cancellationMessage;
                        break;
                    }
            }
        }

        /// <summary>
        /// Populate the Reservation Info Container
        /// </summary>
        /// <param name="hotelSearchEntity">
        /// HotelSearchEntity
        /// </param>
        /// <param name="hotelRoomRateEntity">
        /// HotelRoomRateEntity
        /// </param>
        /// <param name="guestInfoEntity">
        /// GuestInformationEntity
        /// </param>
        private void PopulateReservationInfoContainer()
        {
            BookingDetailsEntity bookingDetails = BookingEngineSessionWrapper.BookingDetails;
            if (bookingDetails != null)
            {
                HotelSearchEntity hotelSearchEntity = bookingDetails.HotelSearch;
                HotelRoomRateEntity hotelRoomRateEntity = bookingDetails.HotelRoomRate;
                GuestInformationEntity guestInfoEntity = bookingDetails.GuestInformation;
                if ((hotelSearchEntity != null) && (hotelRoomRateEntity != null) && (guestInfoEntity != null))
                {
                    HotelDestination hotelDestination = null;
                    AvailabilityController availabilityController = new AvailabilityController();
                    hotelDestination =
                        availabilityController.GetHotelDestinationEntity(hotelSearchEntity.SelectedHotelCode);
                    if (hotelDestination != null)
                    {
                        ReservationInfo.IsCancelledBooking = true;
                        ReservationInfo.CityOrHotelName = hotelDestination.Name;
                        pfReservationInfo.CityOrHotelName = hotelDestination.Name;
                        ReservationInfo.HotelUrl = hotelDestination.HotelPageURL;
                        pfReservationInfo.HotelUrl = hotelDestination.HotelPageURL;

                        if (Request.QueryString["command"] != null)
                        {
                            if (Request.QueryString["command"].ToString() ==
                                QueryStringConstants.QUERY_STRING_CANCEL_PRINT.ToString())
                            {
                                ReservationInfo.HotelNameLabel = hotelDestination.Name;
                                pfReservationInfo.HotelNameLabel = hotelDestination.Name;
                            }
                        }
                    }
                    RoomCategory roomCategory = RoomRateUtil.GetRoomCategory(hotelRoomRateEntity.RoomtypeCode);
                    if (roomCategory != null)
                    {
                        ReservationInfo.NoOfRooms = ReservationInfo.GetTotalBookedRoomsCancellation().ToString();
                        pfReservationInfo.NoOfRooms = ReservationInfo.GetTotalBookedRoomsCancellation().ToString();
                    }
                    ReservationInfo.ArrivalDate = hotelSearchEntity.ArrivalDate;
                    ReservationInfo.DepartureDate = hotelSearchEntity.DepartureDate;

                    pfReservationInfo.ArrivalDate = hotelSearchEntity.ArrivalDate;
                    pfReservationInfo.DepartureDate = hotelSearchEntity.DepartureDate;

                    ReservationInfo.NoOfAdults = Utility.GetTotalAdultsCancellation();
                    ReservationInfo.NoOfChildren = Utility.GetTotalChildrenCancellation();

                    pfReservationInfo.NoOfAdults = Utility.GetTotalAdultsCancellation();
                    pfReservationInfo.NoOfChildren = Utility.GetTotalChildrenCancellation();

                    ReservationInfo.NoOfNights = hotelSearchEntity.NoOfNights;
                    pfReservationInfo.NoOfNights = hotelSearchEntity.NoOfNights;

                    string reservationMessage =
                        WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/reservationDetails");
                    ReservationInfo.IsPrevBooking = false;
                    pfReservationInfo.IsPrevBooking = false;
                    string hotelCountryCode = hotelSearchEntity.HotelCountryCode;
                    TimeSpan timeSpan = hotelSearchEntity.ArrivalDate.Subtract(hotelSearchEntity.DepartureDate);
                    double basePoints = 0;
                    double totalPoints = 0;
                    
                    if (hotelSearchEntity.SearchingType == SearchType.REDEMPTION &&
                        BookingEngineSessionWrapper.ActiveBookingDetails != null && BookingEngineSessionWrapper.ActiveBookingDetails.Count > 0)
                        basePoints = BookingEngineSessionWrapper.AllBookingDetails[0].HotelRoomRate.Points;
                    if (hotelRoomRateEntity.TotalRate != null)
                    {
                        totalPoints = hotelRoomRateEntity.TotalRate.Rate;
                    }

                    string ratePlanCode = hotelRoomRateEntity.RatePlanCode;
                    string selectedRateCategoryName = string.Empty;
                    if (!string.IsNullOrEmpty(ratePlanCode))
                    {
                        Rate rate = RoomRateUtil.GetRate(ratePlanCode);
                        if (rate != null)
                        {
                            selectedRateCategoryName = rate.RateCategoryName;
                        }
                    }

                    double dblTotalRate = Utility.CalculateTotalRateInCaseOfCancelFlow();

                    string baseRateString = string.Empty;
                    if (hotelSearchEntity.SearchingType == SearchType.REDEMPTION)
                    {
                        basePoints = dblTotalRate;
                        baseRateString = Utility.GetRoomRateString(selectedRateCategoryName, hotelRoomRateEntity.Rate,
                                                                   basePoints,
                                                                   hotelSearchEntity.SearchingType,
                                                                   hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                                                   hotelSearchEntity.NoOfNights,
                                                                   hotelSearchEntity.RoomsPerNight);
                    }
                    else if ((Utility.IsBlockCodeBooking)
                             && (string.IsNullOrEmpty(hotelRoomRateEntity.RatePlanCode))
                             && (!string.IsNullOrEmpty(hotelRoomRateEntity.RoomtypeCode)))
                    {
                        baseRateString = Utility.GetRoomRateString(hotelRoomRateEntity.Rate);
                    }
                    else
                    {
                        baseRateString = Utility.GetRoomRateString(selectedRateCategoryName, hotelRoomRateEntity.Rate,
                                                                   basePoints, hotelSearchEntity.SearchingType,
                                                                   hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                                                   AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                    }

                    string totalRateString = string.Empty;
                    int noOfRooms = Convert.ToInt32(ReservationInfo.NoOfRooms);
                    if (hotelSearchEntity.SearchingType == SearchType.REDEMPTION)
                    {
                        totalRateString = Utility.GetRoomRateString(null, null, basePoints,
                                                                    hotelSearchEntity.SearchingType,
                                                                    hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                                                    hotelSearchEntity.NoOfNights, noOfRooms);
                    }
                    else if (hotelSearchEntity.SearchingType.Equals(SearchType.BONUSCHEQUE))
                    {
                        if (hotelRoomRateEntity.TotalRate != null)
                        {
                            RateEntity tempRateEntity = new RateEntity(dblTotalRate,
                                                                       hotelRoomRateEntity.TotalRate.CurrencyCode);
                            totalRateString =
                                WebUtil.GetBonusChequeRateString("/bookingengine/booking/selectrate/bonuschequerate",
                                                                 hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                                                 tempRateEntity.Rate,
                                                                 hotelRoomRateEntity.Rate.CurrencyCode,
                                                                 hotelSearchEntity.NoOfNights, noOfRooms);
                        }
                    }
                    else
                    {
                        if (hotelRoomRateEntity.TotalRate != null)
                        {
                            RateEntity tempRateEntity = new RateEntity(dblTotalRate,
                                                                       hotelRoomRateEntity.TotalRate.CurrencyCode);
                            totalRateString = Utility.GetRoomRateString(null, tempRateEntity, totalPoints,
                                                                        hotelSearchEntity.SearchingType,
                                                                        hotelSearchEntity.ArrivalDate.Year,
                                                                        hotelCountryCode, hotelSearchEntity.NoOfNights,
                                                                        noOfRooms);
                        }
                    }
                    ReservationInfo.SetTotalRoomRate(totalRateString);
                    pfReservationInfo.SetTotalRoomRate(totalRateString);
                }
            }
            List<BookingDetailsEntity> bookingDtls = new List<BookingDetailsEntity>();

            if (BookingEngineSessionWrapper.AllBookingDetails != null)
            {
                if (BookingEngineSessionWrapper.AllCancelledDetails != null)
                {
                    foreach (CancelDetailsEntity cnclEntity in BookingEngineSessionWrapper.AllCancelledDetails)
                    {
                        foreach (BookingDetailsEntity bkngEntity in BookingEngineSessionWrapper.AllBookingDetails)
                        {
                            if (cnclEntity.ReservationNumber == bkngEntity.ReservationNumber &&
                                cnclEntity.LegNumber == bkngEntity.LegNumber)
                            {
                                bookingDtls.Add(bkngEntity);
                                break;
                            }
                        }
                    }
                }
            }
            ReservationInfo.SetRoomDetails(bookingDtls);
            ReservationInfo.PopulateCancellationNumbers();

            pfReservationInfo.SetRoomDetails(bookingDtls);
            pfReservationInfo.PopulateCancellationNumbers();
        }
        #endregion
    }
}