//  Description					: Code Behind class for BookingSearch Control			  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Girish Krishnan                                   	  //
//  Author email id				:                           							  //
//  Creation Date				: 17th December  2007									  //
//	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System namespaces
using System;
using System.Configuration;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

#endregion System namespaces

#region Scandic Namespace
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.BookingEngine.Controller;
using System.Collections;
using System.Text;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for GuestInformation
    /// </summary>
    public partial class GuestInformation : EPiServer.UserControlBase
    {
        //Header css is bydefault lightHeading.
        private string headingCss = "lightHeading";

        public string requestURL;
        public string receiptURL;

        public string HeadingCss
        {
            get { return headingCss; }
            set { headingCss = value; }
        }

        private bool hideLinks;
        public bool HideLinks
        {
            set
            {
                hideLinks = value;
                if (hideLinks)
                {
                    linkFullDescription.Visible = false;
                    linkFindAHotel.Visible = false;
                }
            }
        }
        public string TwitterText { get; set; }

        private HotelDestination hotelDestination;
        public HotelDestination HotelDestination
        {
            get
            {
                return hotelDestination;
            }
            set
            {
                hotelDestination = value;
            }
        }

        public string HostName
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Request.Url.Host != null)
                {
                    return HttpContext.Current.Request.Url.Host;
                }
                return string.Empty;
            }
        }

        public string HotelURL { get; set; }

        private PaymentController _paymentController = null;

        /// <summary>
        /// Gets PaymentController
        /// </summary>
        private PaymentController MPaymentController
        {
            get
            {
                if (_paymentController == null)
                {
                    IContentDataAccessManager contentDataAccessManager = new ContentDataAccessManager();
                    IAdvancedReservationDomain advancedReservationDomain = new AdvancedReservationDomain();
                    ILoyaltyDomain loyaltyDomain = new LoyaltyDomain();
                    IReservationDomain reservationDomain = new ReservationDomain();
                    _paymentController = new PaymentController(contentDataAccessManager, advancedReservationDomain, loyaltyDomain, reservationDomain);
                }
                return _paymentController;
            }
        }

        private NameController nameController = null;

        /// <summary>
        /// Gets PaymentController
        /// </summary>
        private NameController m_NameController
        {
            get { return nameController ?? (nameController = new NameController()); }
        }

        private IContentDataAccessManager contentDataAccessManager = null;

        /// <summary>
        /// Gets contentDataAccessManager
        /// </summary>
        private IContentDataAccessManager m_ContentDataAccessManager
        {
            get
            {
                if (contentDataAccessManager == null)
                {
                    return new ContentDataAccessManager();
                }

                return contentDataAccessManager;
            }
        }
        #region Page Events
        /// <summary>
        /// Event Handler for the Page Load event of GuestInformationn User Control
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="e">
        /// Arguments for the event
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("GuestInformation.Ascx:Page_Load() StartTime");
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
            if (!IsPostBack)
            {
                if (this.Visible)
                {
                    string spAlert = ContentDataAccess.GetSpecialAlert(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode, false);
                    if (!string.IsNullOrEmpty(spAlert))
                    {
                        divSpAlertWrap.Visible = true;
                        divSpAlert.InnerHtml = spAlert;
                    }
                    else
                    {
                        divSpAlertWrap.Visible = false;
                    }


                    if (hotelSearch != null && !string.IsNullOrEmpty(hotelSearch.SelectedHotelCode))
                    {
                        HotelDestination hotelDestination =
                            ContentDataAccess.GetHotelByOperaID(hotelSearch.SelectedHotelCode);
                        if (hotelDestination != null)
                        {
                            this.HotelDestination = hotelDestination;
                            string hotelDescription = Utility.Escape(hotelDestination.HotelDescription);
                            this.HotelDestination.HotelDescription = Utility.TruncateAndAppendDots(hotelDescription, 400);
                            string hotelURL = !string.IsNullOrEmpty(this.hotelDestination.ShortURL)
                                                  ? this.hotelDestination.ShortURL
                                                  : (!string.IsNullOrEmpty(this.hotelDestination.HotelPageURL)
                                                         ? this.hotelDestination.HotelPageURL
                                                         : string.Empty);
                            if (!string.IsNullOrEmpty(hotelURL))
                            {
                                hotelURL = string.Format("{0} {1}/{2}", WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/checkout"),
                                    this.HostName, hotelURL);
                            }

                            this.TwitterText = string.Format("{0} {1}.{2}", WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/twittertext"),
                                   HotelDestination.Name, hotelURL);

                        }
                    }
                }
                string reservationNumber = ReservationInfo != null ? ReservationInfo.ReservationNumber : string.Empty;
                string zanoxUrLscript = Utility.ZanoxTracking(Request.Cookies[ConfigurationManager.AppSettings["SCampaigns"].ToString()],
                                      reservationNumber, SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode,
                                      ((HtmlGenericControl)ReservationInfo.FindControl("lblTotalRoomRateValue")).InnerText, HotelDestination.OperaDestinationId);

                if (!string.IsNullOrEmpty(zanoxUrLscript))
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ZanoxTracker", HttpUtility.HtmlDecode(zanoxUrLscript), false);

                HttpCookie TrivagoCampaigns = Request.Cookies[ConfigurationManager.AppSettings["SCampaigns"].ToString()];

                string referrer = Request.UrlReferrer == null ? "" : Request.UrlReferrer.AbsolutePath;
                bool isMobileRequest = false;
                if (referrer.Contains("/mobile"))
                    isMobileRequest = true;

                if (TrivagoCampaigns != null && string.Equals(TrivagoCampaigns.Value, ConfigurationManager.AppSettings["AffiliatedToTrivago"],
                    StringComparison.InvariantCultureIgnoreCase) && !isMobileRequest && hotelSearch != null)
                {
                    TrivagoPixelTracking(hotelSearch, reservationNumber);
                }
            }

            LnkBtnPrint.Attributes.Add("onclick", "javascript:OpenPringDialog();return false;");
            spnBtnPrint.Attributes.Add("onclick", "javascript:OpenPringDialog();return false;");

            pfLnkBtnPrint.Attributes.Add("onclick", "javascript:OpenPringDialog();return false;");
            pfSpnBtnPrint.Attributes.Add("onclick", "javascript:OpenPringDialog();return false;");

            LnkBtnPrint.Visible = false;
            PrintDiv.Visible = false;
            aboutOurRates.Visible = false;

            requestURL = GlobalUtil.GetUrlToPage(EpiServerPageConstants.PRINTER_FRIENDLY_CONFIRMATION);

            if ((!String.IsNullOrEmpty(requestURL)) && (requestURL.LastIndexOf('/') == (requestURL.Length - 1)))
            {
                requestURL = requestURL.Substring(0, requestURL.Length - 1);
            }
            requestURL = requestURL + "?command=print";

            if (!m_ContentDataAccessManager.GetPaymentFallback(hotelSearch.SelectedHotelCode))
            {

                receiptURL = GlobalUtil.GetUrlToPage(EpiServerPageConstants.RECEIPT);

                if ((!String.IsNullOrEmpty(receiptURL)) && (receiptURL.LastIndexOf('/') == (receiptURL.Length - 1)))
                    receiptURL = receiptURL.Substring(0, receiptURL.Length - 1);

                receiptURL = receiptURL + "?command=print";
            }

            if (BookingEngineSessionWrapper.IsModifyBooking)
            {
                this.modifyBookingMessage.InnerHtml = "<h3>" + WebUtil.GetTranslatedText(TranslatedTextConstansts.MODIFICATION_CONFIRMAION_MESSAGE) + "</h3><br/>";
                this.modifyBookingMessage.Visible = true;
            }
            else
            {
                this.modifyBookingMessage.InnerHtml = "<h3>" + WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/ThankYouHeader") + "</h3>";
                this.modifyBookingMessage.Visible = true;
            }

            if (Request.QueryString["command"] != null)
            {
                if (Request.QueryString["command"].ToString() == "print")
                {
                    printerFriendlySection.Visible = true;

                    EmailPlaceHolder.Visible = false;
                    LnkBtnPrint.Visible = false;
                    lnkBtnPrinterfriendly.Visible = false;
                    divanatherHotel.Visible = false;
                    ThankYouheader.Visible = false;
                    pfPrintDiv.Visible = true;
                    HideLinks = true;
                    aboutOurRates.Visible = false;
                    pfAboutRate.Visible = true;
                    saveCreditCardSection.Visible = false;
                }
                else
                    saveCreditCardSection.Visible = CanCCSectionDisplayed(hotelSearch.SelectedHotelCode);
            }
            else
                saveCreditCardSection.Visible = CanCCSectionDisplayed(hotelSearch.SelectedHotelCode);

            if (Request.QueryString["isPDF"] != null)
            {
                if (Convert.ToBoolean(Request.QueryString["isPDF"]) == true)
                {
                    PrintDiv.Visible = false;
                    divanatherHotel.Visible = false;
                    lnkBtnPrinterfriendly.Visible = false;

                    EmailPlaceHolder.Visible = false;
                    lnkBtnPrinterfriendly.Visible = false;
                    divanatherHotel.Visible = false;
                    ThankYouheader.Visible = false;
                    HideLinks = true;
                    aboutOurRates.Visible = true;
                    pfLnkBtnPrint.Visible = false;
                    pfSpnBtnPrint.Visible = false;
                }
            }

            if (Request.QueryString["roomNumber"] != null)
            {
                masterReserMod21.Visible = false;
                pfMasterReserMod21.Visible = false;
            }
            btnBookAnotherHotel.Attributes.Add("onclick", "javascript:AddPropertiesforAddAnathorHotel();");
            spnBookAnotherHotel.Attributes.Add("onclick", "javascript:AddPropertiesforAddAnathorHotel();");

            btnBookAnotherHotel.Attributes.Add("href", "/");
            spnBookAnotherHotel.Attributes.Add("href", "/");
            //btnAddAnother.Attributes.Add("onclick", "javascript:AddPropertiesforAddAnathorHotel();");
            //spnAddAnother.Attributes.Add("onclick", "javascript:AddPropertiesforAddAnathorHotel();");
            bool isAllRoomsTobeShown = true;
            int roomNumberToBeDisplayed = -1;
            if (System.Web.HttpContext.Current.Request.QueryString["AllROOMSTOBESHOWN"] != null)
            {
                isAllRoomsTobeShown = Convert.ToBoolean((System.Web.HttpContext.Current.Request.QueryString["AllROOMSTOBESHOWN"]));
            }
            if (System.Web.HttpContext.Current.Request.QueryString["roomNumber"] != null)
            {
                roomNumberToBeDisplayed = Convert.ToInt32((System.Web.HttpContext.Current.Request.QueryString["roomNumber"]));
            }
            SetAboutOurRatesDetails(isAllRoomsTobeShown, roomNumberToBeDisplayed);
            if (Request.QueryString["isPDF"] != null && Convert.ToBoolean(Request.QueryString["isPDF"]) == true)
            {
                if (Convert.ToBoolean(Request.QueryString["isPDF"]) == true)
                {
                    showImportantAndDidUKnwInfo.Attributes["style"] = "display:block;";
                    SetDidYouKnowAndOtherImportantInfoTextForEmailTemplate(PageIdentifier.BookingPageIdentifier);
                }
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("GuestInformation.Ascx:Page_Load() EndTime");
        }

        private void TrivagoPixelTracking(HotelSearchEntity hotelSearch, string reservationNumber)
        {
            string partnerID = Convert.ToString(ConfigurationManager.AppSettings["TrivagoPartnerID"]);

            string bucketID = string.Empty;
            string trivagoDomainID = string.Empty;
            if (Request.Cookies["TrivagoDomain"] != null)
                trivagoDomainID = Convert.ToString(Request.Cookies["TrivagoDomain"].Value);

            if (Request.Cookies["TrivagoBucketID"] != null)
                bucketID = Convert.ToString(Request.Cookies["TrivagoBucketID"].Value);

            string trivagoUrlscript = Utility.TrivagoTracking(partnerID, hotelSearch.SelectedHotelCode, hotelSearch.ArrivalDate, hotelSearch.DepartureDate,
                ((HtmlGenericControl)ReservationInfo.FindControl("lblTotalRoomRateValue")).InnerText, reservationNumber, trivagoDomainID, bucketID);

            if (!string.IsNullOrEmpty(trivagoUrlscript))
            {
                HtmlImage img = new HtmlImage();
                img.Height = 1;
                img.Width = 1;
                img.Style.Add("border-style", "none");
                img.Src = trivagoUrlscript;
                TrivagoPlaceHolder.Visible = true;
                TrivagoPlaceHolder.Controls.Add(img);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (BookingEngineSessionWrapper.IsModifyComboBooking)
            {
                ReservationInfo.DisplayReceiptLink = false;
            }
        }


        /// <summary>
        /// returns whether the credit card section can be displayed or not
        /// </summary>
        /// <returns></returns>
        private bool CanCCSectionDisplayed(string hotelOperaID)
        {
            return !string.IsNullOrEmpty(Reservation2SessionWrapper.PaymentTransactionId) && m_NameController.CanCCSectionDisplayed()
                   && !m_ContentDataAccessManager.GetPaymentFallback(hotelOperaID);
        }

        /// <summary>
        /// Sets the did you know and other important info text for email template.
        /// </summary>
        /// <param name="bookingType">Type of the booking.</param>
        /// <returns></returns>
        private void SetDidYouKnowAndOtherImportantInfoTextForEmailTemplate(string bookingType)
        {
            Dictionary<string, string> returnValue = null;
            returnValue = ContentDataAccess.GetConfirmationEmailTextFormCMS(bookingType);
            if (returnValue != null)
            {
                DidYouKnowHeader.InnerHtml = ((returnValue.ContainsKey("DIDYOUKNOWTEXTHEADER")) ? returnValue["DIDYOUKNOWTEXTHEADER"].ToString() : string.Empty);
                DidYouKnow.InnerHtml = ((returnValue.ContainsKey("DIDYOUKNOWTEXT")) ? returnValue["DIDYOUKNOWTEXT"].ToString() : string.Empty);
                OtherImportantInformationHeader.InnerHtml = ((returnValue.ContainsKey("OTHERIMPORTANTINFORMATIONTEXTHEADER")) ? returnValue["OTHERIMPORTANTINFORMATIONTEXTHEADER"].ToString() : string.Empty);
                OtherImportantInformation.InnerHtml = ((returnValue.ContainsKey("OTHERIMPORTANTINFORMATIONTEXT")) ? returnValue["OTHERIMPORTANTINFORMATIONTEXT"].ToString() : string.Empty);
                if (DidYouKnow.InnerHtml == string.Empty)
                    didYouKnowRow.Visible = false;
                if (OtherImportantInformation.InnerHtml == string.Empty)
                    otherImportantInfoRow.Visible = false;

            }
        }

        private void SetAboutOurRatesDetails(bool isAllRoomsTobeShown, int roomNumberToBeDisplayed)
        {
            string authority = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            Hashtable selectedRoomRatesHashTable = BookingEngineSessionWrapper.SelectedRoomAndRatesHashTable;
            SelectedRoomAndRateEntity selectedRoomRates = null;
            List<RateDescriptionforEMAIL> uniqueRateDescriptionBoxes = new List<RateDescriptionforEMAIL>();
            RateCategory rate = null;
            Block block = null;
            string RatePlanCode = string.Empty;
            List<string> uniqueCategories = new List<string>();
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
            if (selectedRoomRatesHashTable != null && hotelSearch != null)
            {
                if (isAllRoomsTobeShown)
                {
                    for (int i = 0; i < selectedRoomRatesHashTable.Count; i++)
                    {
                        selectedRoomRates = selectedRoomRatesHashTable[i] as SelectedRoomAndRateEntity;
                        if (selectedRoomRates != null && selectedRoomRates.RoomRates != null)
                        {
                            if (Utility.IsBlockCodeBooking)
                            {
                                block = Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                                if (!uniqueCategories.Contains(block.BlockID))
                                {
                                    uniqueCategories.Add(block.BlockID);
                                    RateDescriptionforEMAIL rateDescriptionForBlock = LoadControl("/Templates/Booking/Units/RateDescriptionforEMAIL.ascx") as RateDescriptionforEMAIL;
                                    IList<BaseRoomRateDetails> listRoomRateDetails = null;
                                    if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null)
                                        listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                                    if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                                    {
                                        for (int CategoryCount = 0; CategoryCount < listRoomRateDetails.Count; CategoryCount++)
                                        {
                                            BaseRoomRateDetails roomRateDetails = listRoomRateDetails[CategoryCount];
                                            BaseRateDisplay rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                                            foreach (RateCategoryHeaderDisplay rateHeaderDisplay in rateDisplay.RateCategoriesDisplay)
                                            {
                                                if (rateHeaderDisplay.Title == block.BlockName)
                                                {
                                                    rateDescriptionForBlock.RateCaegoryDetails(rateHeaderDisplay);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        rateDescriptionForBlock.RateCategoryDetails(block);
                                    }
                                    uniqueRateDescriptionBoxes.Add(rateDescriptionForBlock);
                                }

                            }
                            else if (selectedRoomRates.RoomRates.Count > 0 && selectedRoomRates.RoomRates[0].RatePlanCode != null)
                            {

                                RatePlanCode = selectedRoomRates.RoomRates[0].RatePlanCode;
                                rate = RoomRateUtil.GetRateCategoryByRatePlanCode(RatePlanCode);
                                if (!uniqueCategories.Contains(rate.RateCategoryId))
                                {
                                    uniqueCategories.Add(rate.RateCategoryId);
                                    RateDescriptionforEMAIL rateDescription = LoadControl("/Templates/Booking/Units/RateDescriptionforEMAIL.ascx") as RateDescriptionforEMAIL;

                                    IList<BaseRoomRateDetails> listRoomRateDetails = null;
                                    if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null)
                                        listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                                    if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                                    {
                                        for (int CategoryCount = 0; CategoryCount < listRoomRateDetails.Count; CategoryCount++)
                                        {
                                            BaseRoomRateDetails roomRateDetails = listRoomRateDetails[CategoryCount];
                                            BaseRateDisplay rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                                            foreach (RateCategoryHeaderDisplay rateHeaderDisplay in rateDisplay.RateCategoriesDisplay)
                                            {
                                                if (rateHeaderDisplay.Id == rate.RateCategoryId)
                                                {
                                                    rateDescription.RateCaegoryDetails(rateHeaderDisplay);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        rateDescription.RateCaegoryDetails(rate);
                                    }
                                    uniqueRateDescriptionBoxes.Add(rateDescription);
                                }
                            }
                        }
                    }
                }
                else
                {
                    selectedRoomRates = selectedRoomRatesHashTable[roomNumberToBeDisplayed] as SelectedRoomAndRateEntity;
                    if (selectedRoomRates != null && selectedRoomRates.RoomRates != null)
                    {
                        if (Utility.IsBlockCodeBooking)
                        {
                            block = Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                            if (!uniqueCategories.Contains(block.BlockID))
                            {
                                uniqueCategories.Add(block.BlockID);
                                RateDescriptionforEMAIL rateDescriptionForBlock = LoadControl("/Templates/Booking/Units/RateDescriptionforEMAIL.ascx") as RateDescriptionforEMAIL;

                                IList<BaseRoomRateDetails> listRoomRateDetails = null;
                                if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null)
                                    listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                                if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                                {
                                    for (int CategoryCount = 0; CategoryCount < listRoomRateDetails.Count; CategoryCount++)
                                    {
                                        BaseRoomRateDetails roomRateDetails = listRoomRateDetails[CategoryCount];
                                        BaseRateDisplay rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                                        foreach (RateCategoryHeaderDisplay rateHeaderDisplay in rateDisplay.RateCategoriesDisplay)
                                        {
                                            if (rateHeaderDisplay.Title == block.BlockName)
                                            {
                                                rateDescriptionForBlock.RateCaegoryDetails(rateHeaderDisplay);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    rateDescriptionForBlock.RateCategoryDetails(block);
                                }
                                uniqueRateDescriptionBoxes.Add(rateDescriptionForBlock);
                            }

                        }
                        else if (selectedRoomRates.RoomRates.Count > 0 && selectedRoomRates.RoomRates[0].RatePlanCode != null)
                        {
                            RatePlanCode = selectedRoomRates.RoomRates[0].RatePlanCode;

                            rate = RoomRateUtil.GetRateCategoryByRatePlanCode(RatePlanCode);
                            if (!uniqueCategories.Contains(rate.RateCategoryId))
                            {
                                uniqueCategories.Add(rate.RateCategoryId);
                                RateDescriptionforEMAIL rateDescription = LoadControl("/Templates/Booking/Units/RateDescriptionforEMAIL.ascx") as RateDescriptionforEMAIL;

                                IList<BaseRoomRateDetails> listRoomRateDetails = null;
                                if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null)
                                    listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                                if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                                {
                                    for (int CategoryCount = 0; CategoryCount < listRoomRateDetails.Count; CategoryCount++)
                                    {
                                        BaseRoomRateDetails roomRateDetails = listRoomRateDetails[CategoryCount];
                                        BaseRateDisplay rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                                        foreach (RateCategoryHeaderDisplay rateHeaderDisplay in rateDisplay.RateCategoriesDisplay)
                                        {
                                            if (rateHeaderDisplay.Id == rate.RateCategoryId)
                                            {
                                                rateDescription.RateCaegoryDetails(rateHeaderDisplay);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    rateDescription.RateCaegoryDetails(rate);
                                }
                                uniqueRateDescriptionBoxes.Add(rateDescription);
                            }
                        }
                    }
                }
            }
            foreach (RateDescriptionforEMAIL rateDescription in uniqueRateDescriptionBoxes)
            {
                AboutRate.Controls.Add(rateDescription);
                if (Request.QueryString["command"] != null && Request.QueryString["command"].ToString() == "print")
                {
                    pfAboutRate.Controls.Add(rateDescription);
                }
            }
            if (Reservation2SessionWrapper.AboutOurRateHeading != null)
            {
                aboutOurRateHeading.Text = Reservation2SessionWrapper.AboutOurRateHeading;
                if (Request.QueryString["command"] != null && Request.QueryString["command"].ToString() == "print")
                {
                    pfAboutOurRateHeading.InnerHtml = Reservation2SessionWrapper.AboutOurRateHeading;
                }
            }
            if (Reservation2SessionWrapper.AboutOurRateDescription != null)
            {
                aboutOurRateDescription.Text = Reservation2SessionWrapper.AboutOurRateDescription;
                if (Request.QueryString["command"] != null && Request.QueryString["command"].ToString() == "print")
                {
                    pfAboutOurRateDescription.InnerHtml = Reservation2SessionWrapper.AboutOurRateDescription;
                }
            }

        }
        protected void lnkBtnPrinterfriendly_Click(object sender, EventArgs e)
        {
            Response.Redirect("/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx");
        }
        public string GetPrinterFriendlyURL()
        {
            return "/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx";
        }
        #endregion Page Events

        #region Public Methods
        /// <summary>
        /// Populate the UI with the confirmation details
        /// </summary>
        public void PopulateUI(SortedList<string, GuestInformationEntity> guestsList, string selectedCardType)
        {
            int legNumber = 1;
            GuestInformationEntity legGuestInformation = Utility.GetLowestLegNumber(guestsList, out legNumber);
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
            Hashtable hashSelectedRoomRateEntity = BookingEngineSessionWrapper.SelectedRoomAndRatesHashTable;
            if ((hotelSearch != null) && (hotelSearch.ListRooms != null) && (hotelSearch.ListRooms.Count > 0))
            {
                FillGuestInfoWithChilds(ref guestsList, hotelSearch);
                legGuestInformation.ChildrensDetails = hotelSearch.ListRooms[0].ChildrenDetails;
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("GuestInformarmation.ASCX:PopulateReservationInfoContainer() PopulateMain Reservation box StartTime:::" + DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
            PopulateReservationInfoContainer(hotelSearch, hashSelectedRoomRateEntity);
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("GuestInformarmation.ASCX:PopulateReservationInfoContainer() PopulateMain Reservation box EndTime:::" + DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));

            if (hotelSearch != null && hashSelectedRoomRateEntity != null)
            {
                SelectedRoomAndRateEntity selectedRoomRates = (SelectedRoomAndRateEntity)hashSelectedRoomRateEntity[0];
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("GuestInformarmation.ASCX:PopulateReservationInfoContainer() Populate Individual Room box StartTime:::" + DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
            PopulateRoomInfoContainer(guestsList);
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("GuestInformarmation.ASCX:PopulateReservationInfoContainer() Populate Individual Room box EndTime:::" + DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
            DisplayNotifications(guestsList, hashSelectedRoomRateEntity);

            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("GuestInformation.Ascx:DisplayHotelInformation() StartTime");
            DisplayHotelInformation(hotelSearch);
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("GuestInformation.Ascx:DisplayHotelInformation() EndTime");
            SetSessionForSogeti();

            SetConfirmationMessage(hashSelectedRoomRateEntity);

            if (WebUtil.IsAnyRoomHavingSaveCategory(hashSelectedRoomRateEntity) && !String.IsNullOrEmpty(Reservation2SessionWrapper.PaymentTransactionId))
            {
                lnkBtnPrintReceipt.Visible = true;

                ReservationNumberSessionWrapper.TransactionCount++;
            }
        }

        /// <summary>
        /// Sets confirmation messages based on the room type
        /// </summary>
        private void SetConfirmationMessage(Hashtable hashSelectedRoomRateEntity)
        {
            lblConfirmationMessage.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/EmailConfirmation");
            if (WebUtil.IsAnyRoomHavingSaveCategory(hashSelectedRoomRateEntity) && !String.IsNullOrEmpty(Reservation2SessionWrapper.PaymentTransactionId))
            {
                lblReceiptMessage.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/EmailConfirmationAndReceipt");
                divConfirmationReceiptAlert.Visible = true;
                divConfirmationAlert.Visible = hashSelectedRoomRateEntity.Count > 1;
            }
            else
            {
                divConfirmationReceiptAlert.Visible = false;
                confirmationMessageSection.Visible = true;
            }
            smsConfirmationSection.Visible = BookingEngineSessionWrapper.IsSmsRequired;
        }

        /// <summary>
        /// Populates the confirmation room information container with guestlist details
        /// Author: RK
        /// </summary>
        /// <param name="guestsList"></param>
        private void PopulateRoomInfoContainer(SortedList<string, GuestInformationEntity> guestsList)
        {
            int roomNumber = 0;
            GuranteeType gType = guestsList.Values[0].GuranteeInformation.GuranteeType;
            bool showIndividualGuarantee = false;
            string bookingType = PageIdentifier.BookingPageIdentifier;
            if (BookingEngineSessionWrapper.IsModifyBooking)
                bookingType = PageIdentifier.ModifyBookingPageIdentifier;
            Dictionary<string, string> returnValue = null;
            returnValue = ContentDataAccess.GetConfirmationEmailTextFormCMS(bookingType);
            foreach (string key in guestsList.Keys)
            {
                if (gType != guestsList[key].GuranteeInformation.GuranteeType)
                {
                    showIndividualGuarantee = true;
                    break;
                }
            }

            Hashtable selectedRoomRatesHashTable = BookingEngineSessionWrapper.SelectedRoomAndRatesHashTable;
            if (selectedRoomRatesHashTable != null && selectedRoomRatesHashTable.Count > 1)
            {
                SelectedRoomAndRateEntity selectedRoomRate = selectedRoomRatesHashTable[0] as SelectedRoomAndRateEntity;
                for (int iterator = 1; iterator < selectedRoomRatesHashTable.Count; iterator++)
                {
                    SelectedRoomAndRateEntity currentroomRate = selectedRoomRatesHashTable[iterator] as SelectedRoomAndRateEntity;
                    if (selectedRoomRate.RateCategoryID != currentroomRate.RateCategoryID)
                    {
                        showIndividualGuarantee = true;
                        break;
                    }
                }
            }
            if (Request.QueryString["roomNumber"] != null)
            {
                int roomNo = Convert.ToInt32(Request.QueryString["roomNumber"]);
                ConfirmationRoomInfoContainer ConfirmRoomInfo = FindControl("ConfirmRoomInfo" + roomNo) as ConfirmationRoomInfoContainer;
                ConfirmationRoomInfoContainer pfConfirmRoomInfo = FindControl("pfConfirmRoomInfo" + roomNo) as ConfirmationRoomInfoContainer;
                ConfirmRoomInfo.Visible = true;
                pfConfirmRoomInfo.Visible = true;
                pfConfirmRoomInfo.FindControl("pfDivCheckInandCheckOutDateContainer").Visible = true;
                GuestInformationEntity guest = null;
                guest = guestsList[guestsList.Keys[roomNo]];
                ConfirmRoomInfo.PopulateGuestInfo(guest, showIndividualGuarantee, roomNo, returnValue);
                pfConfirmRoomInfo.PopulateGuestInfo(guest, showIndividualGuarantee, roomNo, returnValue);
            }
            else
            {
                foreach (string key in guestsList.Keys)
                {
                    ConfirmationRoomInfoContainer ConfirmRoomInfo = FindControl("ConfirmRoomInfo" + roomNumber) as ConfirmationRoomInfoContainer;
                    ConfirmationRoomInfoContainer pfConfirmRoomInfo = FindControl("pfConfirmRoomInfo" + roomNumber) as ConfirmationRoomInfoContainer;
                    ConfirmRoomInfo.Visible = true;
                    pfConfirmRoomInfo.Visible = true;
                    ConfirmRoomInfo.PopulateGuestInfo(guestsList[key], showIndividualGuarantee, roomNumber, returnValue);
                    pfConfirmRoomInfo.PopulateGuestInfo(guestsList[key], showIndividualGuarantee, roomNumber, returnValue);
                    if (Request.QueryString["command"] != null)
                    {
                        if ((Request.QueryString["command"].ToString() == QueryStringConstants.QUERY_STRING_PRINT)
                            || (Request.QueryString["command"].ToString() == QueryStringConstants.QUERY_STRING_CANCEL_PRINT))
                        {
                            ConfirmRoomInfo.hideLinksInPdf = guestsList[key].EmailDetails.EmailID;
                        }
                    }
                    roomNumber++;
                }
            }
            if (!showIndividualGuarantee)
            {
                if (selectedRoomRatesHashTable != null && selectedRoomRatesHashTable.Count > 0)
                {
                    SelectedRoomAndRateEntity selectedRoomandRate = selectedRoomRatesHashTable[0] as SelectedRoomAndRateEntity;
                    guaranteeInformation.Attributes["style"] = "display : block;";
                    pfGuaranteeInformation.Attributes["style"] = "display : block;";
                    lblGuaranteeOption.InnerHtml = Utility.GetGuaranteeText(guestsList.Values[0].GuranteeInformation, selectedRoomandRate, roomNumber);
                    pfLblGuaranteeOption.InnerHtml = Utility.GetGuaranteeText(guestsList.Values[0].GuranteeInformation, selectedRoomandRate, roomNumber);
                    if (returnValue != null)
                    {
                        lblGuaranteeCommonHeader.Text = ((returnValue.ContainsKey("GUARANTEEANDCANCELLATIONHEADER")) ? returnValue["GUARANTEEANDCANCELLATIONHEADER"].ToString() : string.Empty);
                        pfLblGuaranteeCommonHeader.InnerText = ((returnValue.ContainsKey("GUARANTEEANDCANCELLATIONHEADER")) ? returnValue["GUARANTEEANDCANCELLATIONHEADER"].ToString() : string.Empty);
                    }
                }

            }

        }

        private void FillGuestInfoWithChilds(ref SortedList<string, GuestInformationEntity> guestsList, HotelSearchEntity hotelSearch)
        {
            for (int iCount = 0; iCount < guestsList.Count; iCount++)
            {
                if (iCount <= hotelSearch.ListRooms.Count - 1)
                    guestsList.Values[iCount].ChildrensDetails = hotelSearch.ListRooms[iCount].ChildrenDetails;
            }
        }
        #endregion
        #region Private Methods


        /// <summary>
        /// Function to calculate the total amount for the
        /// hotel room booking
        /// </summary>
        /// <param name="listRoomRate">Entity that contain info of the rate per room</param>
        /// <returns></returns>     
        private string CalcTotalAmount(List<RoomRateEntity> listRoomRate)
        {
            double dblTotalAmount = 0;
            for (int iRateCnt = 0; iRateCnt < listRoomRate.Count; iRateCnt++)
            {
                dblTotalAmount += listRoomRate[iRateCnt].BaseRate.Rate;
            }
            return Convert.ToString(dblTotalAmount);
        }
        /// <summary>
        /// Function to calculate the no of adults and childs
        /// </summary>
        /// <param name="HotelRoomList">Entity that contain the info about the adults and childs</param>
        /// <returns>string seperated by ',' to seprate no of adults and child</returns>
        private string CalcNoOfAdultsAndChild(IList<HotelSearchRoomEntity> HotelRoomList)
        {
            string NoOfAdultAndChild = "";
            int NoAdult = 0;
            int NoChild = 0;
            for (int iRoomListCount = 0; iRoomListCount < HotelRoomList.Count; iRoomListCount++)
            {
                NoAdult += HotelRoomList[iRoomListCount].AdultsPerRoom;
                NoChild += HotelRoomList[iRoomListCount].ChildrenPerRoom;
            }
            NoOfAdultAndChild = NoAdult + "," + NoChild;
            return NoOfAdultAndChild;
        }

        /// <summary>
        /// Function to set the Other Control and Special Request
        /// Information 
        /// </summary>
        /// <param name="htmlOthGenControl">Html Control to whom other info is to set</param>
        /// <param name="htmlSplGenControl">Control to whom special info is to set</param>
        /// <param name="guestInfoEntity">Entity from where other and special request control is to populate</param>       
        private void DisplaySpecialRequestDetailsFrRooms(HtmlGenericControl htmlOthGenControl, HtmlGenericControl htmlSplGenControl, GuestInformationEntity guestInfoEntity)
        {
            if (guestInfoEntity != null)
            {
                //Display the Special Requests                
                if (guestInfoEntity.SpecialRequests != null)
                {
                    string otherPeferencesList = string.Empty;
                    string specialRequestList = string.Empty;
                    int totalSpecialRequest = guestInfoEntity.SpecialRequests.Length;
                    for (int spRequestCount = 0; spRequestCount < totalSpecialRequest; spRequestCount++)
                    {
                        switch (guestInfoEntity.SpecialRequests[spRequestCount].RequestValue)
                        {
                            //Display Other Preferences
                            case UserPreferenceConstants.LOWERFLOOR:
                                {
                                    otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.LOWERFLOOR);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.HIGHFLOOR:
                                {
                                    otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.HIGHERFLOOR);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.AWAYFROMELEVATOR:
                                {
                                    otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.AWAYFROMELEVATOR);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.NEARELEVATOR:
                                {
                                    otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.NEARELEVATOR);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.NOSMOKING:
                                {
                                    otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.NOSMOKING);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.SMOKING:
                                {
                                    otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.SMOKING);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.ACCESSIBLEROOM:
                                {
                                    otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.ACCESSABLEROOM);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.ALLERGYROOM:
                                {
                                    otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.ALLERYROOM);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.PETFRIENDLYROOM:
                                {
                                    specialRequestList += WebUtil.GetTranslatedText(TranslatedTextConstansts.PETFRIENDLYROOM);
                                    specialRequestList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.EARLYCHECKIN:
                                {
                                    specialRequestList += WebUtil.GetTranslatedText(TranslatedTextConstansts.EARLYCHECKIN);
                                    specialRequestList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.LATECHECKOUT:
                                {
                                    specialRequestList += WebUtil.GetTranslatedText(TranslatedTextConstansts.LATECHECKOUT);
                                    specialRequestList += " <br />";
                                    break;
                                }
                            default:
                                break;
                        }
                    }
                    htmlOthGenControl.InnerHtml = otherPeferencesList;
                    htmlSplGenControl.InnerHtml = specialRequestList;
                }
            }
        }

        /// <summary>
        /// Function To add Childs per Room informations to Respective
        /// Controls
        /// </summary>
        /// <param name="controlId">Div To Which Child Control Is to Attach </param>
        /// <param name="intChildCount">Index for the Reqired List<ChildrensDetailsEntity> Entity</param>
        private void AddChildDivToTheRoomDiv(string controlId, IList<ChildEntity> lstChildEntity)
        {
            //Check do child Entity Contains Enough Data to Manipulate
            if ((lstChildEntity != null) && (lstChildEntity.Count > 0))
            {
                HtmlGenericControl htmControl = this.FindControl(controlId) as HtmlGenericControl;
                HtmlGenericControl htmlContLi = new HtmlGenericControl("li");
                for (int iChildCount = 0; iChildCount < lstChildEntity.Count; iChildCount++)
                {
                    int iCount = iChildCount;
                    HtmlGenericControl htmlContSpan = new HtmlGenericControl("span");
                    htmlContSpan.InnerText = "Child " + (iCount + 1) + " " + lstChildEntity[iChildCount].AccommodationString;
                    htmlContLi.Controls.Add(htmlContSpan);
                }
                htmControl.Controls.Add(htmlContLi);
            }
        }

        /// <summary>
        /// Displays the hotel information.
        /// Release R2.0 | Confirmation Page map | Displays the hotel information box
        /// </summary>
        /// <param name="hotelSearch">The hotel search.</param>
        private void DisplayHotelInformation(HotelSearchEntity hotelSearch)
        {
            if (hotelSearch != null)
            {
                string hotelID = hotelSearch.SelectedHotelCode;
                if (!string.IsNullOrEmpty(hotelID))
                {
                    HotelDestination hotelDestination = ContentDataAccess.GetHotelByOperaID(hotelID);
                    if (hotelDestination != null)
                    {
                        // Populate the map header as the hotel name
                        mapHeaderHotelname.InnerText = hotelDestination.Name;
                        pfHeaderHotelname.InnerHtml = hotelDestination.Name;

                        // Set show on map link
                        if (!string.IsNullOrEmpty(hotelDestination.Name))
                        {
                            //AMS Patch7| Artifact artf1268464 : Hotel name not shown on google map (link to google map from the confirmation page) 
                            //To Fix this issue: Replacing the "Hotel Name" from QueryString["MapID"] by "Hotel ID"
                            //string findAHotelPageRelativeUrl = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SEARCH_USING_MAP_PAGE) + "?MapID=" + hotelDestination.Name;
                            string findAHotelPageRelativeUrl = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SEARCH_USING_MAP_PAGE) + "?MapID=" + hotelDestination.Name;
                            linkFindAHotel.HRef = findAHotelPageRelativeUrl;
                        }

                        // Fill location and contact info
                        ShowHotelLocationAndContactInfo(hotelDestination);

                        // Fill facilities info
                        ShowHotelFacilitiesInfo(hotelDestination);

                        // Fill href for the Full Description Link
                        if (!string.IsNullOrEmpty(hotelDestination.Name) && !string.IsNullOrEmpty(hotelDestination.HotelPageURL))
                        {
                            //AMS Patch7| Artifact artf1268464 : Hotel name not shown on google map (link to google map from the confirmation page) 
                            //To Fix this issue: Replacing the "Hotel Name" from QueryString["MapID"] by "Hotel ID"
                            //linkFullDescription.HRef = hotelDestination.HotelPageURL + "?GMapID=" + hotelDestination.Name;
                            linkFullDescription.HRef = hotelDestination.HotelPageURL + "?GMapID=" + hotelDestination.Id;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Function for Setting the data in the Hotel Description Div Just
        /// Below to the Google Map
        /// Release R2.0 | Confirmation Page map | Displays the location and contact info of the hotel
        /// </summary>
        /// <param name="hotelDestination">The hotel destination.</param>
        private void ShowHotelLocationAndContactInfo(HotelDestination hotelDestination)
        {
            if (hotelDestination != null)
            {
                if (hotelDestination.HotelAddress != null)
                {
                    // Archana | PostalCity Changes for XMLAPI
                    StringBuilder streetAddress = new StringBuilder();
                    string address = hotelDestination.HotelAddress.StreetAddress;
                    string zip = hotelDestination.HotelAddress.PostCode;
                    string postalCity = hotelDestination.HotelAddress.PostalCity;
                    string city = hotelDestination.HotelAddress.City;
                    string country = hotelDestination.HotelAddress.Country;
                    if (!string.IsNullOrEmpty(address))
                    {
                        //artf1155879 | Confirmation page - hotel information |Rajneesh
                        //streetAddress.Append(address);
                        streetAddress.Append(address.Trim());
                        if (!string.IsNullOrEmpty(zip) || !string.IsNullOrEmpty(city) || !string.IsNullOrEmpty(country))
                            streetAddress.Append(", ");
                    }
                    if (!string.IsNullOrEmpty(zip))
                    {
                        //artf1155879 | Confirmation page - hotel information |Rajneesh
                        //streetAddress.Append(zip);
                        streetAddress.Append(zip.Trim());
                        if (!string.IsNullOrEmpty(city) || !string.IsNullOrEmpty(country))
                            streetAddress.Append(", ");
                    }
                    // Archana | PostalCity Changes for XMLAPI
                    if (city.Trim() == postalCity.Trim())
                    {
                        streetAddress.Append(postalCity.Trim());
                        streetAddress.Append(",");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(postalCity))
                        {
                            //artf1155879 | Confirmation page - hotel information |Rajneesh
                            //streetAddress.Append(zip);
                            streetAddress.Append(postalCity.Trim());
                            if (!string.IsNullOrEmpty(postalCity) || !string.IsNullOrEmpty(country))
                                streetAddress.Append(", ");
                        }
                        if (!string.IsNullOrEmpty(city))
                        {
                            //artf1155879 | Confirmation page - hotel information |Rajneesh
                            //streetAddress.Append(city);
                            streetAddress.Append(city.Trim());
                            if (!string.IsNullOrEmpty(country))
                                streetAddress.Append(", ");
                        }
                    }
                    if (!string.IsNullOrEmpty(country))
                    {
                        //artf1155879 | Confirmation page - hotel information |Rajneesh
                        //streetAddress.Append(country);
                        streetAddress.Append(country.Trim());
                    }

                    //if (streetAddress.Length == 0)
                    //{
                    //    streetAddress.Append("&nbsp;");
                    //}

                    string fullAddress = streetAddress.ToString();
                    if (!string.IsNullOrEmpty(fullAddress))
                    {
                        //linkHotelAddress.InnerText = fullAddress;
                        //if (!string.IsNullOrEmpty(hotelDestination.Name) && !string.IsNullOrEmpty(hotelDestination.HotelPageURL))
                        //{
                        //    linkHotelAddress.HRef = hotelDestination.HotelPageURL + "?GMapID=" + hotelDestination.Name;
                        //}
                        lblHotelAddress.InnerText = fullAddress;
                        pfLblHotelAddress.InnerText = fullAddress;
                    }
                    else
                    {
                        //linkHotelAddress.Attributes.Add("style", "display:none");
                        lblHotelAddress.Attributes.Add("style", "display:none");
                        pfLblHotelAddress.Attributes.Add("style", "display:none");
                    }
                }

                if (!string.IsNullOrEmpty(hotelDestination.Telephone))
                {
                    lblHotelPhoneNum.InnerText = hotelDestination.Telephone;
                    pfLblHotelPhoneNum.InnerText = hotelDestination.Telephone;
                }
                else
                {
                    phoneContainer.Attributes.Add("style", "display:none");
                    pfPhoneContainer.Attributes.Add("style", "display:none");
                }

                if (!string.IsNullOrEmpty(hotelDestination.Fax))
                {
                    lblHotelFaxNum.InnerText = hotelDestination.Fax;
                    pfLblHotelFaxNum.InnerText = hotelDestination.Fax;
                }
                else
                {
                    faxContainer.Attributes.Add("style", "display:none");
                    pfFaxContainer.Attributes.Add("style", "display:none");
                }
                //Defect fix - Bhavya - to display label instead of link in printerfriendly page
                if (!string.IsNullOrEmpty(hotelDestination.Email))
                {
                    if (Request.QueryString["command"] != null)
                    {
                        if (Request.QueryString["command"].ToString() == "print")
                        {
                            lblHotelEmail.InnerText = hotelDestination.Email;
                            lblHotelEmail.Visible = true;
                            linkHotelEmail.Visible = false;
                            PrintDiv.Visible = true;

                            pfLblHotelEmail.InnerText = hotelDestination.Email;
                            pfLblHotelEmail.Visible = true;
                        }
                        else if (Request.QueryString["command"].ToString() == "isPDF")
                        {
                            linkHotelEmail.HRef = "mailto:" + hotelDestination.Email;
                            lblHotelEmail.InnerText = hotelDestination.Email;
                            lblHotelEmail.Visible = true;
                            linkHotelEmail.Visible = false;
                            PrintDiv.Visible = true;
                        }
                    }
                    else
                    {
                        linkHotelEmail.HRef = "mailto:" + hotelDestination.Email;
                        linkHotelEmail.InnerText = hotelDestination.Email;
                        lblHotelEmail.Visible = false;
                        linkHotelEmail.Visible = true;
                        PrintDiv.Visible = false;


                        pfLblHotelEmail.InnerText = hotelDestination.Email;
                        pfLblHotelEmail.Visible = true;
                    }
                }
                else
                {
                    emailContainer.Attributes.Add("style", "display:none");
                    pfEmailContainer.Attributes.Add("style", "display:none");
                }

                if (!string.IsNullOrEmpty(hotelDestination.CustomerCarePhone))
                {
                    lblCustomerCareNumber.InnerText = hotelDestination.CustomerCarePhone;
                    pfLblCustomerCareNumber.InnerText = hotelDestination.CustomerCarePhone;
                }
                else
                {
                    customerCareNumberContainer.Attributes.Add("style", "display:none");
                    pfCustomerCareNumberContainer.Attributes.Add("style", "display:none");
                }

                lblLatitude.InnerText = hotelDestination.Coordinate.Y.ToString();
                lblLongitude.InnerText = hotelDestination.Coordinate.X.ToString();

                pfLblLatitude.InnerText = hotelDestination.Coordinate.Y.ToString();
                pfLblLongitude.InnerText = hotelDestination.Coordinate.X.ToString();
            }
        }

        /// <summary>
        /// Shows the hotel facilities info.
        /// Release R2.0 | Confirmation Page map | Displays the hotel facilities info
        /// </summary>
        /// <param name="hotelDestination">The hotel destination.</param>
        private void ShowHotelFacilitiesInfo(HotelDestination hotelDestination)
        {
            if (hotelDestination != null)
            {
                string yes = Translate("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Yes");
                string no = Translate("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/No");
                string km = Translate("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Km");

                if (hotelDestination.NoOfRooms != null)
                {
                    lblNoOfRooms.InnerText = hotelDestination.NoOfRooms.ToString();
                    pfLblNoOfRooms.InnerText = hotelDestination.NoOfRooms.ToString();
                }
                else
                {
                    noOfRoomsContainer.Attributes.Add("style", "display:none");
                    pfNoOfRoomsContainer.Attributes.Add("style", "display:none");
                }

                if (hotelDestination.NoOfNonSmokingRooms != null)
                {
                    lblNoOfNonSmokingRooms.InnerText = hotelDestination.NoOfNonSmokingRooms.ToString();
                    pfLblNoOfNonSmokingRooms.InnerText = hotelDestination.NoOfNonSmokingRooms.ToString();
                }
                else
                {
                    noOfNonSmokingRoomsContainer.Attributes.Add("style", "display:none");
                    pfNoOfNonSmokingRoomsContainer.Attributes.Add("style", "display:none");
                }

                lblRoomsForDisabledAvailable.InnerText = hotelDestination.IsRoomsForDisabledAvailable ? yes : no;
                lblRelaxCenterAvailable.InnerText = hotelDestination.IsRelaxCenterAvailable ? yes : no;
                lblRestaurantBar.InnerText = hotelDestination.IsRestaurantAndBarAvailable ? yes : no;
                lblGarage.InnerText = hotelDestination.IsGarageAvailable ? yes : no;
                lblOutdoorParking.InnerText = hotelDestination.IsOutDoorParkingAvailable ? yes : no;
                lblShop.InnerText = hotelDestination.IsShopAvailable ? yes : no;
                lblMeetingFacilities.InnerText = hotelDestination.IsMeetingRoomAvailable ? yes : no;
                lblWirelessInternet.InnerText = hotelDestination.IsWiFiAvailable ? yes : no;

                pfLblRoomsForDisabledAvailable.InnerText = hotelDestination.IsRoomsForDisabledAvailable ? yes : no;
                pfLblRelaxCenterAvailable.InnerText = hotelDestination.IsRelaxCenterAvailable ? yes : no;
                pfLblRestaurantBar.InnerText = hotelDestination.IsRestaurantAndBarAvailable ? yes : no;
                pfLblGarage.InnerText = hotelDestination.IsGarageAvailable ? yes : no;
                pfLblOutdoorParking.InnerText = hotelDestination.IsOutDoorParkingAvailable ? yes : no;
                pfLblShop.InnerText = hotelDestination.IsShopAvailable ? yes : no;
                pfLblMeetingFacilities.InnerText = hotelDestination.IsMeetingRoomAvailable ? yes : no;
                pfLblWirelessInternet.InnerText = hotelDestination.IsWiFiAvailable ? yes : no;


                if (!string.IsNullOrEmpty(hotelDestination.EcoLabeled))
                {
                    lblEcoLabeled.InnerText = hotelDestination.EcoLabeled;
                    pfLblEcoLabeled.InnerText = hotelDestination.EcoLabeled;
                }
                else
                {
                    ecoLabeledContainer.Attributes.Add("style", "display:none");
                    pfEcoLabeledContainer.Attributes.Add("style", "display:none");
                }

                lblDistanceCentre.InnerText = hotelDestination.CityCenterDistance.ToString() + " " + km;
                pfLblDistanceCentre.InnerText = hotelDestination.CityCenterDistance.ToString() + " " + km;

                if (!string.IsNullOrEmpty(hotelDestination.Airport1Name))
                {
                    lblAirportName.InnerText = hotelDestination.Airport1Name;
                    lblDistanceAirport.InnerText = hotelDestination.Airport1Distance.ToString() + " " + km;

                    pfLblAirportName.InnerText = hotelDestination.Airport1Name;
                    pfLblDistanceAirport.InnerText = hotelDestination.Airport1Distance.ToString() + " " + km;
                }
                else
                {
                    airportDistanceContainer.Attributes.Add("style", "display:none");
                    pfAirportDistanceContainer.Attributes.Add("style", "display:none");
                }

                if (hotelDestination.TrainStationDistance != null)
                {
                    lblDistanceTrain.InnerText = hotelDestination.TrainStationDistance.ToString() + " " + km;
                    pfLblDistanceTrain.InnerText = hotelDestination.TrainStationDistance.ToString() + " " + km;
                }
                else
                {
                    trainStationDistContainer.Attributes.Add("style", "display:none");
                    pfTrainStationDistContainer.Attributes.Add("style", "display:none");
                }

            }
        }

        /// <summary>
        /// Set the parameters for Sogeti
        /// </summary>
        private void SetSessionForSogeti()
        {
            SogetiVariableSessionWrapper.ReservationNumberForThirdParty = ReservationNumberSessionWrapper.ReservationNumber;
            GuestInformationEntity guestInfoEntity = GuestBookingInformationSessionWrapper.GuestBookingInformation;
            if (guestInfoEntity != null)
            {
                SogetiVariableSessionWrapper.PostCodeForThirdParty = guestInfoEntity.PostCode;
            }
            LoyaltyDetailsEntity loyaltyDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
            if (loyaltyDetails != null)
            {
                SogetiVariableSessionWrapper.MemberShipLevelForThirdParty = loyaltyDetails.MembershipLevel;
            }
            SogetiVariableSessionWrapper.TotalAmountForThirdParty = GenericSessionVariableSessionWrapper.TotalAmout;
        }

        private void DisplayNotifications(SortedList<string, GuestInformationEntity> guestsList, Hashtable hashSelectedRoomRateEntity)
        {
            List<string> listofFailedEmailRecipient = ErrorsSessionWrapper.FailedEmailRecipient;
            int guestIterator = 0;

            if (guestsList != null && guestsList.Count > 0)
            {
                int totalGuests = guestsList.Count;
                if (listofFailedEmailRecipient != null && listofFailedEmailRecipient.Count == guestsList.Keys.Count)
                {
                    confirmAlertMod.Visible = false;
                }
                else
                {
                    foreach (string key in guestsList.Keys)
                    {
                        GuestInformationEntity guestInfoEntity = guestsList[key];
                        if (guestInfoEntity != null && guestInfoEntity.EmailDetails != null &&
                                (listofFailedEmailRecipient == null ||
                                    (listofFailedEmailRecipient != null &&
                                        !(listofFailedEmailRecipient.Contains(guestInfoEntity.EmailDetails.EmailID))
                                    )
                                )
                            )
                        {
                            //first user gets email message also
                            if (WebUtil.IsAnyRoomHavingSaveCategory(hashSelectedRoomRateEntity))
                            {
                                if (guestIterator == 0)
                                {
                                    lblReceiptEmail.InnerHtml += guestInfoEntity.EmailDetails.EmailID;
                                }
                                else
                                {
                                    lblEmailConfirmation.InnerHtml += guestInfoEntity.EmailDetails.EmailID + ", ";
                                }
                            }
                            else
                            {
                                lblEmailConfirmation.InnerHtml += guestInfoEntity.EmailDetails.EmailID + ", ";
                            }
                        }

                        guestIterator++;
                    }

                    string emailConfirmation = lblEmailConfirmation.InnerHtml.Trim();
                    lblEmailConfirmation.InnerHtml = emailConfirmation.Trim(',');

                    string emailReceipt = string.Empty;
                    if (!String.IsNullOrEmpty(Reservation2SessionWrapper.PaymentTransactionId))
                    {
                        emailReceipt = lblReceiptEmail.InnerHtml.Trim();
                        lblReceiptEmail.InnerHtml = emailReceipt.Trim(',');
                    }

                    if (string.IsNullOrEmpty(emailConfirmation) && string.IsNullOrEmpty(emailReceipt))
                    {
                        confirmAlertMod.Visible = false;
                    }
                }
            }

            if (BookingEngineSessionWrapper.IsSmsRequired)
            {
                lblSMSConfirmation.Visible = true;

                List<string> listOfFailedSMSRecepient = ErrorsSessionWrapper.FailedSMSRecepient;
                if (listOfFailedSMSRecepient == null || listOfFailedSMSRecepient.Count <= 0)
                {
                    if (guestsList != null && guestsList.Count > 0)
                    {
                        //guestIterator = 0;
                        int totalGuests = guestsList.Count;

                        foreach (string key in guestsList.Keys)
                        {
                            GuestInformationEntity guestInfoEntity = guestsList[key];
                            if ((guestInfoEntity != null) && (guestInfoEntity.Mobile != null) &&
                                (!string.IsNullOrEmpty(guestInfoEntity.Mobile.Number)) && guestInfoEntity.IsSMSChecked)
                            {
                                lblSMSConfirmation.InnerHtml += guestInfoEntity.Mobile.Number + ", ";
                            }
                        }

                        string confSMS = lblSMSConfirmation.InnerHtml.Trim();
                        lblSMSConfirmation.InnerHtml = confSMS.Trim(',');
                    }
                }
            }
        }

        /// <summary>
        /// This method populates the Reservation Information.
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="selectedRoomRateshashtable"></param>
        private void PopulateReservationInfoContainer(HotelSearchEntity hotelSearch, Hashtable selectedRoomRateshashtable)
        {
            int noOfAdults = 0;
            int noOfChildren = 0;
            string totalRateString = string.Empty;
            if (hotelSearch != null && hotelSearch.ListRooms.Count > 0)
            {
                for (int roomIterator = 0; roomIterator < hotelSearch.ListRooms.Count; roomIterator++)
                {
                    noOfAdults += hotelSearch.ListRooms[roomIterator].AdultsPerRoom;
                    noOfChildren += hotelSearch.ListRooms[roomIterator].ChildrenPerRoom;
                }
            }
            if (selectedRoomRateshashtable != null && selectedRoomRateshashtable.Count > 0)
            {
                List<RoomRateEntity> selectedRoomRateEntityList = new List<RoomRateEntity>();
                SelectedRoomAndRateEntity selectedRoomRates = null;
                RoomRateEntity roomRateList = null;
                double totalRate = 0;
                string hotelCountryCode = string.Empty;
                double basePoints = 0;
                double totalPoints = 0;

                bool isEarly = false;
                bool isFlex = false;

                for (int iterator = 0; iterator < selectedRoomRateshashtable.Keys.Count; iterator++)
                {
                    selectedRoomRates = null;
                    if (selectedRoomRateshashtable.ContainsKey(iterator))
                    {
                        selectedRoomRates = selectedRoomRateshashtable[iterator] as SelectedRoomAndRateEntity;
                    }
                    AppLogger.LogInfoMessage("START : In BookingConfirmation page ::GuestInformationController:: PopulateReservationInformationContainer()");
                    ReservationInfo.ArrivalDate = hotelSearch.ArrivalDate;
                    ReservationInfo.DepartureDate = hotelSearch.DepartureDate;
                    ReservationInfo.NoOfNights = hotelSearch.NoOfNights;
                    pfReservationInfo.ArrivalDate = hotelSearch.ArrivalDate;
                    pfReservationInfo.DepartureDate = hotelSearch.DepartureDate;
                    pfReservationInfo.NoOfNights = hotelSearch.NoOfNights;
                    ReservationInfo.IsPrevBooking = false;

                    if (hotelSearch.ChildrenPerRoom > 0)
                    {
                        if ((!BookingEngineSessionWrapper.IsModifyBooking) || ((BookingEngineSessionWrapper.IsModifyBooking) && (!BookingEngineSessionWrapper.DirectlyModifyContactDetails)))
                        {
                            if (hotelSearch.ChildrenPerRoom > 0)
                            {
                                string cot = string.Empty, extraBed = string.Empty, sharingBed = string.Empty;
                                Utility.GetLocaleSpecificChildrenAccomodationTypes(ref cot, ref extraBed, ref sharingBed);
                            }
                        }
                    }
                    SelectedRoomAndRateEntity clonedSelectedRoomRates = SearchCriteriaSessionWrapper.CloneSelectedRoomAndRateEntity;

                    if (selectedRoomRates != null)
                    {
                        SelectedRoomAndRateEntity cloneSelectedRoomRatesEntity = selectedRoomRates.Clone();
                        SearchCriteriaSessionWrapper.CloneSelectedRoomAndRateEntity = cloneSelectedRoomRatesEntity;
                    }
                    else
                    {
                        selectedRoomRates = SearchCriteriaSessionWrapper.CloneSelectedRoomAndRateEntity;
                    }

                    if (selectedRoomRates != null)
                    {

                        Rate rate = RoomRateUtil.GetRate(selectedRoomRates.RoomRates[0].RatePlanCode);

                        if (rate != null)
                        {

                            if (rate.IsCancellable)
                            {
                                isFlex = true;

                            }
                            else
                            {
                                isEarly = true;

                            }

                        }

                    }

                    if (selectedRoomRates != null)
                    {
                        if ((hotelSearch != null) && (!BookingEngineSessionWrapper.DirectlyModifyContactDetails))
                        {
                            HotelDestination hotelDestination = HotelRoomRateSessionWrapper.ListHotelRoomRate[0].HotelDestination;
                            if (hotelDestination != null)
                            {
                                ReservationInfo.CityOrHotelName = hotelDestination.Name;
                                ReservationInfo.HotelUrl = hotelDestination.HotelPageURL;
                                if (Request.QueryString["command"] != null)
                                {
                                    if ((Request.QueryString["command"].ToString() == QueryStringConstants.QUERY_STRING_PRINT)
                                        || (Request.QueryString["command"].ToString() == QueryStringConstants.QUERY_STRING_CANCEL_PRINT))
                                    {
                                        ReservationInfo.HotelNameLabel = hotelDestination.Name;
                                        pfReservationInfo.HotelNameLabel = hotelDestination.Name;
                                    }
                                }
                            }
                            hotelCountryCode = HotelRoomRateSessionWrapper.ListHotelRoomRate[0].CountryCode;

                            TimeSpan timeSpan = hotelSearch.DepartureDate.Subtract(hotelSearch.ArrivalDate);

                            if (null != selectedRoomRates.GetCommonBasePoints())
                            {
                                basePoints = selectedRoomRates.GetCommonBasePoints().PointsRequired;
                                totalPoints = selectedRoomRates.GetCommonBasePoints().PointsRequired * timeSpan.Days;
                            }
                            ReservationInfo.NoOfRooms = Convert.ToString(hotelSearch.RoomsPerNight);
                            pfReservationInfo.NoOfRooms = Convert.ToString(hotelSearch.RoomsPerNight);
                            string selectedRateCategoryName = string.Empty;
                            string baseRateString = string.Empty;
                            if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                            {
                                baseRateString = Utility.GetRoomRateString(selectedRateCategoryName, selectedRoomRates.GetCommonRate(),
                                    totalPoints, hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year, hotelCountryCode, hotelSearch.NoOfNights, hotelSearch.RoomsPerNight);
                            }
                            else if ((Utility.IsBlockCodeBooking)
                                && string.IsNullOrEmpty(selectedRoomRates.RoomRates[0].RatePlanCode)
                                && !string.IsNullOrEmpty(selectedRoomRates.RoomRates[0].RoomTypeCode))
                            {
                                totalRateString = Utility.GetRoomRateString(selectedRoomRates.GetCommonRate());
                            }
                            else
                            {
                                totalRateString = Utility.GetRoomRateString(selectedRateCategoryName, selectedRoomRates.GetCommonRate(),
                                    basePoints, hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year, hotelCountryCode, AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                            }
                            if (hotelSearch.SearchingType.Equals(SearchType.REGULAR) || hotelSearch.SearchingType.Equals(SearchType.BONUSCHEQUE)
                                || hotelSearch.SearchingType.Equals(SearchType.CORPORATE) || hotelSearch.SearchingType.Equals(SearchType.VOUCHER))
                            {
                                totalRate += Convert.ToDouble(selectedRoomRates.RoomRates[0].TotalRate.Rate);
                            }

                            if (selectedRoomRates.RoomRates.Count > 0)
                            {
                                if (BookingEngineSessionWrapper.DirectlyModifyContactDetails)
                                {
                                    AvailabilityController availabilityController = new AvailabilityController();
                                    ReservationInfo.CityOrHotelName = hotelDestination.Name;
                                    ReservationInfo.HotelUrl = hotelDestination.HotelPageURL;
                                }
                                roomRateList = selectedRoomRates.RoomRates[0] as RoomRateEntity;
                                selectedRoomRateEntityList.Add(roomRateList);
                            }
                        }
                    }

                    if (hotelSearch != null && BookingEngineSessionWrapper.DirectlyModifyContactDetails)
                    {
                        if (selectedRoomRates.RoomRates.Count > 0)
                        {
                            AvailabilityController availabilityController = new AvailabilityController();
                            HotelDestination hotelDestination = availabilityController.GetHotelDestinationEntity(SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.SearchCode);
                            if (hotelDestination != null)
                            {
                                ReservationInfo.CityOrHotelName = hotelDestination.Name;
                                ReservationInfo.HotelUrl = hotelDestination.HotelPageURL;
                                if (Request.QueryString["command"] != null)
                                {
                                    if ((Request.QueryString["command"].ToString() == QueryStringConstants.QUERY_STRING_PRINT)
                                        || (Request.QueryString["command"].ToString() == QueryStringConstants.QUERY_STRING_CANCEL_PRINT))
                                    {
                                        ReservationInfo.HotelNameLabel = hotelDestination.Name;
                                        pfReservationInfo.HotelNameLabel = hotelDestination.Name;
                                    }
                                }
                            }

                            TimeSpan timeSpan = hotelSearch.DepartureDate.Subtract(hotelSearch.ArrivalDate);

                            if (null != selectedRoomRates.GetCommonBasePoints())
                            {
                                basePoints = selectedRoomRates.GetCommonBasePoints().PointsRequired;
                                totalPoints = selectedRoomRates.GetCommonBasePoints().PointsRequired * timeSpan.Days;
                            }
                            ReservationInfo.NoOfRooms = Convert.ToString(hotelSearch.RoomsPerNight);
                            pfReservationInfo.NoOfRooms = Convert.ToString(hotelSearch.RoomsPerNight);

                            if (hotelSearch.SearchingType.Equals(SearchType.REGULAR) || hotelSearch.SearchingType.Equals(SearchType.BONUSCHEQUE)
                                || hotelSearch.SearchingType.Equals(SearchType.CORPORATE) || hotelSearch.SearchingType.Equals(SearchType.VOUCHER))
                            {
                                if (string.IsNullOrEmpty(hotelCountryCode) && hotelSearch != null && hotelSearch.HotelCountryCode != null)
                                    hotelCountryCode = hotelSearch.HotelCountryCode;
                            }

                            string selectedRateCategoryName = string.Empty;
                            string baseRateString = string.Empty;
                            if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                            {
                                baseRateString = Utility.GetRoomRateString(selectedRateCategoryName, selectedRoomRates.GetCommonRate(),
                                    totalPoints, hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year, hotelCountryCode, hotelSearch.NoOfNights, hotelSearch.RoomsPerNight);
                            }
                            else if ((Utility.IsBlockCodeBooking)
                                && string.IsNullOrEmpty(selectedRoomRates.RoomRates[0].RatePlanCode)
                                && !string.IsNullOrEmpty(selectedRoomRates.RoomRates[0].RoomTypeCode))
                            {
                                totalRateString = Utility.GetRoomRateString(selectedRoomRates.GetCommonRate());
                            }
                            else
                            {
                                totalRateString = Utility.GetRoomRateString(selectedRateCategoryName, selectedRoomRates.GetCommonRate(),
                                    basePoints, hotelSearch.SearchingType, hotelSearch.ArrivalDate.Year, hotelCountryCode, AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                            }
                            if (hotelSearch.SearchingType.Equals(SearchType.REGULAR) || hotelSearch.SearchingType.Equals(SearchType.CORPORATE)
                                || hotelSearch.SearchingType.Equals(SearchType.BONUSCHEQUE) || hotelSearch.SearchingType.Equals(SearchType.VOUCHER))
                            {
                                totalRate += Convert.ToDouble(selectedRoomRates.RoomRates[0].TotalRate.Rate);
                            }
                            roomRateList = selectedRoomRates.RoomRates[0] as RoomRateEntity;
                            selectedRoomRateEntityList.Add(roomRateList);
                        }
                    }

                    if (hotelSearch.SearchingType.Equals(SearchType.BONUSCHEQUE))
                    {
                        totalRateString = WebUtil.GetBonusChequeRateString("/bookingengine/booking/selectrate/bonuschequerate", hotelSearch.ArrivalDate.Year, hotelCountryCode, totalRate,
                            selectedRoomRates.RoomRates[0].BaseRate.CurrencyCode, hotelSearch.NoOfNights, hotelSearch.RoomsPerNight);

                    }
                    else if (hotelSearch.SearchingType.Equals(SearchType.REDEMPTION))
                    {

                        totalRateString = Utility.GetRoomRateString(null, null, totalPoints, hotelSearch.SearchingType,
                                              hotelSearch.ArrivalDate.Year, hotelCountryCode, hotelSearch.NoOfNights, hotelSearch.RoomsPerNight);

                    }
                    else
                    {
                        RateEntity rateEntity = rateEntity = new RateEntity(totalRate, selectedRoomRates.RoomRates[0].BaseRate.CurrencyCode);
                        totalRateString = Utility.GetRoomRateString(null, rateEntity, totalPoints, hotelSearch.SearchingType,
                                   hotelSearch.ArrivalDate.Year, hotelCountryCode, hotelSearch.NoOfNights, hotelSearch.RoomsPerNight);
                    }
                }

                if (isEarly != isFlex && isEarly == true)
                {
                    pfGuaranteeOptionDiv.Attributes.Add("class", "");

                }

                GenericSessionVariableSessionWrapper.TotalAmout = totalRateString;
                ReservationInfo.SetTotalRoomRate(totalRateString);
                ReservationInfo.NoOfAdults = noOfAdults;
                ReservationInfo.NoOfChildren = noOfChildren;

                pfReservationInfo.SetTotalRoomRate(totalRateString);
                pfReservationInfo.NoOfAdults = noOfAdults;
                pfReservationInfo.NoOfChildren = noOfChildren;
                if (selectedRoomRateEntityList.Count > 0)
                {
                    ReservationInfo.SetRoomDetails(selectedRoomRateEntityList);
                    pfReservationInfo.SetRoomDetails(selectedRoomRateEntityList);
                }
            }
        }
        #endregion
    }
}