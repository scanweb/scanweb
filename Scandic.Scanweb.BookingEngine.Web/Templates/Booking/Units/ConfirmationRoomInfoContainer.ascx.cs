using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using System.Web;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// ConfirmationRoomInfoContainer
    /// </summary>
    public partial class ConfirmationRoomInfoContainer : EPiServer.UserControlBase
    {
        public string hideLinksInPdf
        {
            set
            {
                spanEmail.Visible = false;
                spanEmailAnch.Visible = false;
                spanEmailPrnt.InnerHtml = value;
            }
        }      
        public string ShowDnumber = string.Empty;

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("ConfirmationRoomInfoContainer:Page_Load() Begin");
            printHeader.Visible = false;
            prntHeading.Visible = false;

            if (Request.QueryString["command"] != null)
            {
                if (Request.QueryString["command"].ToString() == "print")
                {
                    lblAnchRoomRate.Visible = false;
                    Header.Visible = false;
                    printHeader.Visible = true;
                    prntHeading.Visible = true;
                    Header.Attributes.Add("class", "");
                    divSingleRoom1.Attributes.Add("class", "roomDetails expand roomDetailsPrnt");
                }
                if (Request.QueryString["command"].ToString() == "isPDF")
                {
                    string pdfFooterClassAttributes = pdfFooter.Attributes["class"];
                    pdfFooter.Attributes.Add("class", "ft sprite pdfFooter");
                    lblAnchRoomRate.Visible = false;
                    Header.Visible = false;
                    printHeader.Visible = true;
                    prntHeading.Visible = true;
                    Header.Attributes.Add("class", "");
                    divSingleRoom1.Attributes.Add("class", "roomDetails expand roomDetailsPrnt");
                }
                if (Request.QueryString["isCancel"] != null && Request.QueryString["isCancel"].ToString() == "true")
                {
                    printHeader.Visible = false;
                    prntHeading.Visible = false;
                }
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("ConfirmationRoomInfoContainer:Page_Load() End");
        }

        /// <summary>
        /// GetDnumber
        /// </summary>
        /// <returns>Dnumber</returns>
        public string GetDnumber()
        {
            if (SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode != null &&
                (!string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode.ToString())))
            {
                return SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        private string GetViewModifyDLRedirectLink(string reservationID, string last_Name)
        {
            return WebUtil.GetViewModifyDLRedirectLink(HttpContext.Current.Request.Url.Host, AppConstants.DLREDIRECT,
               AppConstants.DEEPLINK_VIEW_MODIFY_RESERVATION_ID, reservationID,
               AppConstants.DEEPLINK_VIEW_MODIFY_LAST_NAME, last_Name);
        }


        /// <summary>
        /// PopulateGuestInfo
        /// </summary>
        /// <param name="guestInfoEntity"></param>
        /// <param name="showIndividualGuarantee"></param>
        /// <param name="roomNumber"></param>
        /// <param name="returnValue"></param>
        public void PopulateGuestInfo(GuestInformationEntity guestInfoEntity, bool showIndividualGuarantee,
                                      int roomNumber, Dictionary<string, string> returnValue)
        {
            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
            {
                pfSpnCheckInDate.InnerText = WebUtil.GetDayFromDate(SearchCriteriaSessionWrapper.SearchCriteria.ArrivalDate) +
                                             AppConstants.SPACE +
                                             DateUtil.DateToDDMMYYYYWords(SearchCriteriaSessionWrapper.SearchCriteria.ArrivalDate);
                pfSpnCheckOutDate.InnerText = WebUtil.GetDayFromDate(SearchCriteriaSessionWrapper.SearchCriteria.DepartureDate) +
                                              AppConstants.SPACE +
                                              DateUtil.DateToDDMMYYYYWords(SearchCriteriaSessionWrapper.SearchCriteria.DepartureDate);
            }
            else
            {
                pfSpnCheckInDate.InnerText =
                    WebUtil.GetDayFromDate(BookingEngineSessionWrapper.BookingDetails.HotelSearch.ArrivalDate) + AppConstants.SPACE +
                    DateUtil.DateToDDMMYYYYWords(BookingEngineSessionWrapper.BookingDetails.HotelSearch.ArrivalDate);
                pfSpnCheckOutDate.InnerText =
                    WebUtil.GetDayFromDate(BookingEngineSessionWrapper.BookingDetails.HotelSearch.DepartureDate) + AppConstants.SPACE +
                    DateUtil.DateToDDMMYYYYWords(BookingEngineSessionWrapper.BookingDetails.HotelSearch.DepartureDate);
            }

            //To display Deeplink for Other Rooms 
            viewModifyDLRedirectPDFLinkForOtherRooms.Visible = false;
            if (System.Web.HttpContext.Current.Request.QueryString["AllROOMSTOBESHOWN"] != null)
            {
                bool isAllRoomsTobeShown = Convert.ToBoolean((System.Web.HttpContext.Current.Request.QueryString["AllROOMSTOBESHOWN"]));
                if (!isAllRoomsTobeShown)
                {
                    viewModifyDLRedirectPDFLinkForOtherRooms.Visible = true;
                    string resNo = guestInfoEntity.ReservationNumber + AppConstants.HYPHEN + guestInfoEntity.LegNumber;
                    ancViewModifyDLRedirectLinkOtherRoom.InnerHtml = GetViewModifyDLRedirectLink(resNo, guestInfoEntity.LastName);
                    ancViewModifyDLRedirectLinkOtherRoom.HRef = "http://" + GetViewModifyDLRedirectLink(resNo, guestInfoEntity.LastName);
                }
            }

            DnumberLabel.Visible = false;
            if (guestInfoEntity.IsValidDNumber)
                ShowDnumber = guestInfoEntity.D_Number;
            if (!string.IsNullOrEmpty(ShowDnumber)) DnumberLabel.Visible = true;
            if (string.IsNullOrEmpty(ShowDnumber) || SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.VOUCHER)
            {
                DNumberContainer.Visible = true;
            }
            if (Request.QueryString["command"] != null && Request.QueryString["command"].ToString() == "print" &&
                !string.IsNullOrEmpty(ShowDnumber))
            {
                pfDNumberContainer.Visible = true;
            }

            if (!showIndividualGuarantee)
            {
                guaranteeInformation.Attributes["style"] = "display : none";
                pfguaranteeInformation.Attributes["style"] = "display:none";
            }
            System.Globalization.TextInfo tInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
            
            spanMainGuestReservationNumber.InnerHtml =
                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room" + (roomNumber + 1) + "") +
                AppConstants.SPACE + AppConstants.HYPHEN + AppConstants.SPACE +
                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/reservationNumber") + "<strong>" +
                AppConstants.SPACE + guestInfoEntity.ReservationNumber + "</strong>"
                + "<strong>" + AppConstants.HYPHEN + guestInfoEntity.LegNumber + "</strong>";
            divSingleRoom1.Visible = true;
            lblMainGuestReservationNumber.InnerHtml =
                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room" + (roomNumber + 1) + "") +
                AppConstants.SPACE + AppConstants.HYPHEN + AppConstants.SPACE +
                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/reservationNumber") + "<strong>" +
                AppConstants.SPACE + guestInfoEntity.ReservationNumber + "</strong>"
                + "<strong>" + AppConstants.HYPHEN + guestInfoEntity.LegNumber + "</strong>";

            pfLblMainGuestReservationNumber.InnerHtml = WebUtil.GetTranslatedText(
                "/bookingengine/booking/bookingdetail/room" + (roomNumber + 1) + "")
                                                        + AppConstants.SPACE + AppConstants.HYPHEN + AppConstants.SPACE +
                                                        WebUtil.GetTranslatedText(
                                                            "/bookingengine/booking/bookingdetail/reservationNumber")
                                                        + AppConstants.SPACE + guestInfoEntity.ReservationNumber +
                                                        AppConstants.HYPHEN + guestInfoEntity.LegNumber;
            lblAnchMainGuestRNo.Attributes.Add("title",
                                               WebUtil.GetTranslatedText(
                                                   "/bookingengine/booking/searchhotel/bookingREservationRoomToolTip"));

            lblCountry.InnerText = guestInfoEntity.Country;

            lblTitle.InnerText = Utility.GetNameTitle(guestInfoEntity.Title).Trim();
            lblFName.InnerText = tInfo.ToTitleCase(guestInfoEntity.FirstName);
            lblLName.InnerText = tInfo.ToTitleCase(guestInfoEntity.LastName);

            lblCountry.InnerText = guestInfoEntity.Country;


            pfLblFName.InnerText = tInfo.ToTitleCase(guestInfoEntity.FirstName) + AppConstants.SPACE +
                                   tInfo.ToTitleCase(guestInfoEntity.LastName);

            
            if ((pfLblFName.InnerText).Length > 22)
            {
                pfLblFName.InnerHtml = Utility.FormatFieldsToDisplay(pfLblFName.InnerText, 22);
            }
            OrderedDictionary countryCodes = DropDownService.GetCountryCodes();
            if (countryCodes != null)
            {
                pfLblCountry.InnerText = countryCodes[guestInfoEntity.Country] as string;
            }
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
            if (hotelSearch != null && hotelSearch.ListRooms.Count > roomNumber)
            {
                lblAdults.InnerText = hotelSearch.ListRooms[roomNumber].AdultsPerRoom.ToString();

                lblChild.InnerText = hotelSearch.ListRooms[roomNumber].ChildrenPerRoom.ToString();

                pfLblAdults.InnerText = hotelSearch.ListRooms[roomNumber].AdultsPerRoom.ToString();
                pfLblChild.InnerText = hotelSearch.ListRooms[roomNumber].ChildrenPerRoom.ToString();
            }

            SelectedRoomAndRateEntity selectedRoomRates = null;
            Hashtable selectedRoomRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
            if (selectedRoomRatesHashTable != null && selectedRoomRatesHashTable.ContainsKey(roomNumber))
            {
                selectedRoomRates = selectedRoomRatesHashTable[roomNumber] as SelectedRoomAndRateEntity;
            }

            if ((guestInfoEntity.ChildrensDetails != null) && (guestInfoEntity.ChildrensDetails.ListChildren.Count != 0))
            {
                AddChildDivToTheRoomDiv("lblDynChilds", guestInfoEntity.ChildrensDetails.ListChildren);
                AddChildDivToTheRoomDiv("pfLblDynChilds", guestInfoEntity.ChildrensDetails.ListChildren);
            }

            if ((guestInfoEntity.ChildrensDetails != null) && (guestInfoEntity.ChildrensDetails.ListChildren.Count != 0))
            {
                lblChildsbedtype.Visible = true;
                pfpLblChildsbedtype.Visible = true;
            }
            else
            {
                lblChildsbedtype.Visible = false;
                pfpLblChildsbedtype.Visible = false;
            }

            if (selectedRoomRates != null)
            {
                lblRoomType.InnerText = tInfo.ToTitleCase(selectedRoomRates.SelectedRoomCategoryName);
                pfLblRoomType.InnerText = tInfo.ToTitleCase(selectedRoomRates.SelectedRoomCategoryName);
                if (Utility.IsBlockCodeBooking)
                {
                    Block block = null;
                    if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                    {
                        block =
                            Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(
                                SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                    }
                    else
                    {
                        block =
                            Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(
                                BookingEngineSessionWrapper.BookingDetails.HotelSearch.CampaignCode);
                    }
                    if (block != null)
                    {
                        lblRoomRate.InnerHtml = Utility.FormatFieldsToDisplay(block.BlockCategoryName, 25);
                        pfLblRoomRate.InnerHtml = Utility.FormatFieldsToDisplay(block.BlockCategoryName, 20);
                        string blockDescription = block.BlockDescription;
                        lblAnchRoomRate.Attributes.Add("title", blockDescription);
                    }
                }
                else if (selectedRoomRates.RoomRates.Count > 0 && selectedRoomRates.RoomRates[0].RatePlanCode != null)
                {
                    string RatePlanCode = selectedRoomRates.RoomRates[0].RatePlanCode;
                    Rate rate = RoomRateUtil.GetRate(RatePlanCode);
                    if (rate != null)
                    {
                        lblRoomRate.InnerHtml = Utility.FormatFieldsToDisplay(rate.RateCategoryName, 25);
                        pfLblRoomRate.InnerHtml = Utility.FormatFieldsToDisplay(rate.RateCategoryName, 20);
                    }

                    if (!string.IsNullOrEmpty(RatePlanCode))
                    {
                        RateCategory rateCategory =
                            RoomRateUtil.GetRateCategoryByRatePlanCode(selectedRoomRates.RoomRates[0].RatePlanCode);
                        if (rateCategory != null)
                        {
                            string rateCategoryDescription = rateCategory.RateCategoryDescription;
                            lblAnchRoomRate.Attributes.Add("title", rateCategoryDescription);
                        }
                    }
                }
                if (hotelSearch.SearchingType == SearchType.CORPORATE)
                {
                    BaseRateDisplay rateDisplay = null;
                    bool isTrue = false;
                    IList<BaseRoomRateDetails> listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;

                    if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                    {
                        if (Utility.IsBlockCodeBooking)
                        {
                            foreach (BaseRoomRateDetails roomDtls in listRoomRateDetails)
                            {
                                if (roomDtls != null)
                                {
                                    if (roomDtls.RateCategories != null && roomDtls.RateCategories.Count == 0)
                                    {
                                        rateDisplay = new BlockCodeRoomRateDisplay(roomDtls as BlockCodeRoomRateDetails);
                                        foreach (
                                            RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                                        {
                                            lblRoomRate.InnerHtml = Utility.FormatFieldsToDisplay(hdrDisplay.Title, 25);
                                            pfLblRoomRate.InnerHtml = Utility.FormatFieldsToDisplay(hdrDisplay.Title, 20);
                                        }
                                    }
                                    else
                                    {
                                        foreach (RateCategory rateCtgry in roomDtls.RateCategories)
                                        {
                                            if ((rateCtgry != null) && (selectedRoomRates.RateCategoryID == rateCtgry.RateCategoryId))
                                            {
                                                rateDisplay =
                                                    new BlockCodeRoomRateDisplay(roomDtls as BlockCodeRoomRateDetails);
                                                isTrue = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (isTrue)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            foreach (BaseRoomRateDetails roomDtls in listRoomRateDetails)
                            {
                                if (roomDtls != null)
                                {
                                    foreach (RateCategory rateCtgry in roomDtls.RateCategories)
                                    {
                                        rateDisplay =
                                            new NegotiatedRoomRateDisplay(roomDtls as NegotiatedRoomRateDetails);
                                        if ((rateCtgry != null) && (selectedRoomRates.RateCategoryID == rateCtgry.RateCategoryId))
                                        {
                                            isTrue = true;
                                            break;
                                        }
                                    }
                                }
                                if (isTrue)
                                {
                                    break;
                                }
                            }
                        }
                    }

                    if (rateDisplay != null)
                    {
                        foreach (RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                        {
                            if (selectedRoomRates != null && selectedRoomRates.RateCategoryID != null &&
                                selectedRoomRates.RateCategoryID == hdrDisplay.Id)
                            {
                                lblRoomRate.InnerHtml = Utility.FormatFieldsToDisplay(hdrDisplay.Title, 25);
                                pfLblRoomRate.InnerHtml = Utility.FormatFieldsToDisplay(hdrDisplay.Title, 20);
                            }
                        }
                    }
                    else if (!Utility.IsBlockCodeBooking && BookingEngineSessionWrapper.BookingDetails != null 
                                            && BookingEngineSessionWrapper.BookingDetails.GuestInformation!= null )
                    {
                       var roomRatePlan = BookingEngineSessionWrapper.BookingDetails.GuestInformation.CompanyName;
                       lblRoomRate.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 25);
                       pfLblRoomRate.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 20);
                    }
                    
                }
                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                {
                    lblPernightstay.InnerText =
                        WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RatePerStay");
                    pfLblPernightstay.InnerText =
                        WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RatePerStay");
                }
                else
                {
                    lblPernightstay.InnerText =
                        WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RoomRateFirstNight");
                    pfLblPernightstay.InnerText =
                        WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RoomRateFirstNight");
                }
                switch (hotelSearch.SearchingType)
                {
                    case SearchType.REDEMPTION:
                        if (selectedRoomRates != null && selectedRoomRates.RoomRates != null && selectedRoomRates.RoomRates.Count > 0 &&
                           selectedRoomRates.RoomRates[0].PointsDetails != null)
                        {
                            lblGuestRatePerNight.InnerText =
                                WebUtil.GetRedeemString(selectedRoomRates.RoomRates[0].PointsDetails.PointsRequired *
                                                        hotelSearch.NoOfNights);
                            pfLblGuestRatePerNight.InnerText =
                                WebUtil.GetRedeemString(selectedRoomRates.RoomRates[0].PointsDetails.PointsRequired *
                                                        hotelSearch.NoOfNights);
                        }
                        break;
                    case SearchType.BONUSCHEQUE:

                        RateEntity rateEntity = null;
                        AvailabilityController availabilityController = new AvailabilityController();
                        string hotelCountryCode = string.Empty;

                        if (hotelSearch != null)
                        {
                            if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null && HotelRoomRateSessionWrapper.ListHotelRoomRate.Count > 0)
                                hotelCountryCode = HotelRoomRateSessionWrapper.ListHotelRoomRate[0].CountryCode;
                            else
                                hotelCountryCode = availabilityController.GetHotelCountryCode(hotelSearch,
                                                                                          hotelSearch.ListRooms[0]);
                        }
                        rateEntity = new RateEntity(selectedRoomRates.RoomRates[0].BaseRate.Rate,
                                                    selectedRoomRates.RoomRates[0].BaseRate.CurrencyCode);
                        lblGuestRatePerNight.InnerText
                            = Utility.GetRoomRateString(null, rateEntity, 0, hotelSearch.SearchingType,
                                                        hotelSearch.ArrivalDate.Year, hotelCountryCode,
                                                        AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                        pfLblGuestRatePerNight.InnerText
                            = Utility.GetRoomRateString(null, rateEntity, 0, hotelSearch.SearchingType,
                                                        hotelSearch.ArrivalDate.Year, hotelCountryCode,
                                                        AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                        break;
                    case SearchType.VOUCHER:
                        lblGuestRatePerNight.InnerText =
                            WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher");
                        pfLblGuestRatePerNight.InnerText =
                            WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher");
                        break;

                    default:
                        if (BookingEngineSessionWrapper.HideARBPrice)
                        {
                            lblGuestRatePerNight.InnerText = Utility.GetPrepaidString();
                            pfLblGuestRatePerNight.InnerText = Utility.GetPrepaidString();
                        }
                        else
                        {
                            lblGuestRatePerNight.InnerText = selectedRoomRates.RoomRates[0].BaseRate.Rate + AppConstants.SPACE + selectedRoomRates.RoomRates[0].BaseRate.CurrencyCode;
                            pfLblGuestRatePerNight.InnerText = selectedRoomRates.RoomRates[0].BaseRate.Rate + AppConstants.SPACE + selectedRoomRates.RoomRates[0].BaseRate.CurrencyCode;
                        }
                        break;
                }
            }
            lblBedType.InnerText = guestInfoEntity.BedTypePreference;
            pfLblBedType.InnerText = guestInfoEntity.BedTypePreference;

            if (guestInfoEntity.SpecialRequests != null && guestInfoEntity.SpecialRequests.Length > 0)
            {
                DisplaySpecialRequestDetailsFrRooms(lblGuestOtherPref, lblGuestSpecialReq, guestInfoEntity);
                DisplaySpecialRequestDetailsFrRooms(pfLblGuestOtherPref, pfLblGuestSpecialReq, guestInfoEntity);
            }
            else

            {
                OthersRequestCtrl.Style.Add("display", "none");
                SpecialRequestCtrl.Style.Add("display", "none");
                pfOthersRequestCtrl.Style.Add("display", "none");
                pfSpecialRequestCtrl.Style.Add("display", "none");
            }
            if (!string.IsNullOrEmpty(guestInfoEntity.AdditionalRequest))
            {
                addRequestHdr.Visible = true;
                pfAddRequestHdr.Visible = true;
                DisplayAdditionalRequest(addRequestText, guestInfoEntity);
                pfAddRequestText.InnerHtml = guestInfoEntity.AdditionalRequest;
            }
            else
            {
                addRequestHdr.Visible = false;
                pfAddRequestHdr.Visible = false;
            }
            if (guestInfoEntity.Mobile != null)
            {
                lblMobileValue.InnerText = guestInfoEntity.Mobile.Number;
                pfLblMobileValue.InnerText = guestInfoEntity.Mobile.Number;
            }
            if (guestInfoEntity.EmailDetails != null)
            {
                lblEmail.InnerText = guestInfoEntity.EmailDetails.EmailID;

                if (guestInfoEntity.EmailDetails.EmailID.Length > 22)
                {
                    StringBuilder emailStr = new System.Text.StringBuilder();
                    string finalEmailStr = guestInfoEntity.EmailDetails.EmailID;
                    int lenStr = (finalEmailStr.Length/22);
                    if (lenStr > 1)
                    {
                        int stInd;
                        for (int i = 0; i < lenStr; i++)
                        {
                            stInd = i*22;
                            if ((stInd + 22) <= finalEmailStr.Length)
                            {
                                emailStr.Append(finalEmailStr.Substring(stInd, 22));
                                emailStr.Append("<br/>");
                            }
                            else
                            {
                                emailStr.Append(finalEmailStr.Substring(stInd));
                            }
                        }
                        if ((finalEmailStr.Length) > (lenStr*22))
                        {
                            emailStr.Append(finalEmailStr.Substring(lenStr*22));
                            emailStr.Append("<br/>");
                        }
                        finalEmailStr = emailStr.ToString();
                    }
                    else
                    {
                        emailStr.Append(finalEmailStr.Substring(0, 22));
                        emailStr.Append("<br/>");
                        emailStr.Append(finalEmailStr.Substring(22));
                        finalEmailStr = emailStr.ToString();
                    }
                    pfLblEmail.InnerHtml = finalEmailStr;
                }
                else
                    pfLblEmail.InnerText = guestInfoEntity.EmailDetails.EmailID;
                lblEmail.Attributes.Add("href", "mailto:" + guestInfoEntity.EmailDetails.EmailID);
                lblAnchEmail.Attributes.Add("title", lblEmail.InnerText);
                lblAnchEmail.Attributes.Add("href", "mailto:" + lblEmail.InnerText);
            }
            countryCodes = DropDownService.GetCountryCodes();
            if (countryCodes != null)
            {
                lblCountry.InnerText = countryCodes[guestInfoEntity.Country] as string;
            }
            if (guestInfoEntity.GuestAccountNumber == string.Empty || guestInfoEntity.GuestAccountNumber == null)
            {
                divFGPNumber.Visible = false;
                pfDivFGPNumber.Visible = false;
            }
            else if (guestInfoEntity.IsValidFGPNumber)
            {
                divFGPNumber.Attributes["style"] = "display : block; padding-bottom: 10px;";
                lblFGPNumber.InnerText = guestInfoEntity.GuestAccountNumber;

                pfDivFGPNumber.Attributes["style"] = "display : block; padding-bottom: 10px;";
                pfLblFGPNumber.InnerText = guestInfoEntity.GuestAccountNumber;
                if (lblFGPNumber.InnerText == null)
                {
                    divFGPNumber.Visible = false;
                    pfDivFGPNumber.Visible = false;
                }
                else
                {
                    divFGPNumber.Visible = true;
                    pfDivFGPNumber.Visible = true;
                }
            }
            if (returnValue != null)
            {
                lblGuaranteeOptionHeader.Text = ((returnValue.ContainsKey("GUARANTEEANDCANCELLATIONHEADER"))
                                                     ? returnValue["GUARANTEEANDCANCELLATIONHEADER"].ToString()
                                                     : string.Empty);
                pflblGuaranteeOptionHeader.Text = ((returnValue.ContainsKey("GUARANTEEANDCANCELLATIONHEADER"))
                                                       ? returnValue["GUARANTEEANDCANCELLATIONHEADER"].ToString()
                                                       : string.Empty);
            }
            lblGuaranteeOption.InnerHtml = Utility.GetGuaranteeText(guestInfoEntity.GuranteeInformation,
                                                                    selectedRoomRates, roomNumber + 1);
            pflblGuaranteeOption.InnerHtml = Utility.GetGuaranteeText(guestInfoEntity.GuranteeInformation,
                                                                      selectedRoomRates, roomNumber + 1);
            if (guestInfoEntity.GuranteeInformation != null)
            {
                if (GuranteeType.CREDITCARD == guestInfoEntity.GuranteeInformation.GuranteeType)
                {
                    if (guestInfoEntity.GuranteeInformation.CreditCard != null)
                    {
                        if (selectedRoomRates != null && RoomRateUtil.IsSaveRateCategory(selectedRoomRates.RateCategoryID))
                        {
                            pfdivCreditCardInfo.Visible = false;
                            creditCardSectionForConfPage.Visible = false;
                        }
                        else
                        {
                            pfdivCreditCardInfo.Visible = true;
                            creditCardSectionForConfPage.Visible = true;
                       
                            string cardnumber = guestInfoEntity.GuranteeInformation.CreditCard.CardNumber;
                            
                            if (cardnumber.Length >= AppConstants.CREDIT_CARD_DISPLAY_CHARS)
                            {
                                if (!cardnumber.Contains(AppConstants.CARD_MASK_CHAR))
                                {
                                    lblCreditCardNumber.InnerText = AppConstants.CARD_MASK +
                                                                    cardnumber.Substring((cardnumber.Length -
                                                                                          AppConstants.
                                                                                              CREDIT_CARD_DISPLAY_CHARS));

                                    pflblCreditCardNumber.InnerText = AppConstants.CARD_MASK +
                                                                      cardnumber.Substring((cardnumber.Length -
                                                                                            AppConstants.
                                                                                                CREDIT_CARD_DISPLAY_CHARS));
                                }
                                else
                                {
                                    lblCreditCardNumber.InnerText = cardnumber;
                                    pflblCreditCardNumber.InnerText = cardnumber;
                                }
                            }
                            else
                            {
                                lblCreditCardNumber.InnerText = AppConstants.CARD_MASK + cardnumber;

                                pflblCreditCardNumber.InnerText = AppConstants.CARD_MASK + cardnumber;
                            }
                            
                            lblCreditCardType.InnerText = guestInfoEntity.GuranteeInformation.CreditCard.CardType;
                            
                            lblExpiryDate.InnerText =
                                guestInfoEntity.GuranteeInformation.CreditCard.ExpiryDate.ToDateTime().ToString("MM/yyyy");

                            pflblCreditCardType.InnerText = guestInfoEntity.GuranteeInformation.CreditCard.CardType;

                            if (IsPanHashCreditCard(guestInfoEntity.GuranteeInformation.CreditCard.CardType))
                            {
                                ccNameSection.Visible = false;
                                pfCCNameSection.Visible = false;
                            }
                            else
                            {
                                lblCreditCardName.InnerText = guestInfoEntity.GuranteeInformation.CreditCard.NameOnCard;
                                pflblCreditCardName.InnerText = guestInfoEntity.GuranteeInformation.CreditCard.NameOnCard;
                            }

                            pflblExpiryDate.InnerText =
                                guestInfoEntity.GuranteeInformation.CreditCard.ExpiryDate.ToDateTime().ToString("MM/yyyy");
                        }

                        if (SearchCriteriaSessionWrapper.SearchCriteria != null &&
                            SearchCriteriaSessionWrapper.SearchCriteria.SearchingType != SearchType.REDEMPTION)
                        {
                            divCreditCardNum.Visible = true;
                            divExpiryDate.Visible = true;
                            divCreditCardType.Visible = true;
                            divCreditCardName.Visible = true;

                            pfdivCreditCardInfo.Visible = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// checks whether the credit card is pan hash type or not
        /// </summary>
        /// <returns></returns>
        private bool IsPanHashCreditCard(string cardType)
        {
            if (!string.IsNullOrEmpty(cardType) && !string.IsNullOrEmpty(AppConstants.PREPAID_CCTYPE_PREFIX))
            {
                return cardType.StartsWith(AppConstants.PREPAID_CCTYPE_PREFIX);
            }
            return false;
        }

        ///<summary>
        ///Function to set the Other Control and Special Request
        ///Information 
        ///</summary>
        ///<param name="htmlOthGenControl">Html Control to whom other info is to set</param>
        ///<param name="htmlSplGenControl">Control to whom special info is to set</param>
        ///<param name="guestInfoEntity">Entity from where other and special request control is to populate</param>       
        private void DisplaySpecialRequestDetailsFrRooms(HtmlGenericControl htmlOthGenControl,
                                                         HtmlGenericControl htmlSplGenControl,
                                                         GuestInformationEntity guestInfoEntity)
        {
            if (guestInfoEntity != null)
            {                
                if (guestInfoEntity.SpecialRequests != null)
                {
                    string otherPeferencesList = string.Empty;
                    string specialRequestList = string.Empty;
                    int totalSpecialRequest = guestInfoEntity.SpecialRequests.Length;
                    for (int spRequestCount = 0; spRequestCount < totalSpecialRequest; spRequestCount++)
                    {
                        switch (guestInfoEntity.SpecialRequests[spRequestCount].RequestValue)
                        {
                            case UserPreferenceConstants.LOWERFLOOR:
                                {
                                    System.Text.StringBuilder str = new System.Text.StringBuilder();
                                    str.Append("<li>");
                                    str.Append("<span>");
                                    str.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.LOWERFLOOR));
                                    str.Append("</span>");
                                    str.Append("</li>");
                                    otherPeferencesList += str.ToString();
                                    break;
                                }
                            case UserPreferenceConstants.HIGHFLOOR:
                                {
                                    System.Text.StringBuilder str = new System.Text.StringBuilder();
                                    str.Append("<li>");
                                    str.Append("<span>");
                                    str.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.HIGHERFLOOR));
                                    str.Append("</span>");
                                    str.Append("</li>");
                                    otherPeferencesList += str.ToString();
                                    break;
                                }
                            case UserPreferenceConstants.AWAYFROMELEVATOR:
                                {
                                    System.Text.StringBuilder str = new System.Text.StringBuilder();
                                    str.Append("<li>");
                                    str.Append("<span>");
                                    str.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.AWAYFROMELEVATOR));
                                    str.Append("</span>");
                                    str.Append("</li>");
                                    otherPeferencesList += str.ToString();
                                    break;
                                }
                            case UserPreferenceConstants.NEARELEVATOR:
                                {
                                    System.Text.StringBuilder str = new System.Text.StringBuilder();
                                    str.Append("<li>");
                                    str.Append("<span>");
                                    str.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.NEARELEVATOR));
                                    str.Append("</span>");
                                    str.Append("</li>");
                                    otherPeferencesList += str.ToString();
                                    break;
                                }
                            case UserPreferenceConstants.NOSMOKING:
                                {
                                    System.Text.StringBuilder str = new System.Text.StringBuilder();
                                    str.Append("<li>");
                                    str.Append("<span>");
                                    str.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.NOSMOKING));
                                    str.Append("</span>");
                                    str.Append("</li>");
                                    otherPeferencesList += str.ToString();
                                    break;
                                }
                            case UserPreferenceConstants.SMOKING:
                                {
                                    System.Text.StringBuilder str = new System.Text.StringBuilder();
                                    str.Append("<li>");
                                    str.Append("<span>");
                                    str.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.SMOKING));
                                    str.Append("</span>");
                                    str.Append("</li>");
                                    otherPeferencesList += str.ToString();
                                    break;
                                }
                            case UserPreferenceConstants.ACCESSIBLEROOM:
                                {
                                    System.Text.StringBuilder str = new System.Text.StringBuilder();
                                    str.Append("<li>");
                                    str.Append("<span>");
                                    str.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ACCESSABLEROOM));
                                    str.Append(": ");
                                    str.Append("</span>");
                                    str.Append("</br>");
                                    str.Append("<span>");
                                    str.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ACCESSABLEROOMDESC));
                                    str.Append("</span>");
                                    str.Append("</li>");
                                    otherPeferencesList += str.ToString();
                                    break;
                                }
                            case UserPreferenceConstants.ALLERGYROOM:
                                {
                                    System.Text.StringBuilder str = new System.Text.StringBuilder();
                                    str.Append("<li>");
                                    str.Append("<span>");
                                    str.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ALLERYROOM));
                                    str.Append(": ");
                                    str.Append("</span>");
                                    str.Append("</br>");
                                    str.Append("<span>");
                                    str.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ALLERYROOMDESC));
                                    str.Append("</span>");
                                    str.Append("</li>");
                                    otherPeferencesList += str.ToString();
                                    break;
                                }
                            case UserPreferenceConstants.PETFRIENDLYROOM:
                                {
                                    System.Text.StringBuilder str = new System.Text.StringBuilder();
                                    str.Append("<li>");
                                    str.Append("<span>");
                                    str.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.PETFRIENDLYROOM));
                                    str.Append(": ");
                                    str.Append("</span>");
                                    str.Append("</br>");
                                    str.Append("<span>");
                                    str.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.PETFRIENDLYROOMDESC));
                                    str.Append("</span>");
                                    str.Append("</li>");
                                    otherPeferencesList += str.ToString();
                                    break;
                                }
                            case UserPreferenceConstants.EARLYCHECKIN:
                                {
                                    System.Text.StringBuilder str2 = new System.Text.StringBuilder();
                                    str2.Append("<li>");
                                    str2.Append("<span>");
                                    str2.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.EARLYCHECKIN));
                                    str2.Append(": ");
                                    str2.Append("</span>");
                                    str2.Append("</br>");
                                    str2.Append("<span>");
                                    str2.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.EARLYCHECKINDESC));
                                    str2.Append("</span>");
                                    str2.Append("</li>");
                                    specialRequestList += str2.ToString();
                                    break;
                                }
                            case UserPreferenceConstants.LATECHECKOUT:
                                {
                                    System.Text.StringBuilder str3 = new System.Text.StringBuilder();
                                    str3.Append("<li>");
                                    str3.Append("<span>");
                                    str3.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.LATECHECKOUT));
                                    str3.Append(": ");
                                    str3.Append("</span>");
                                    str3.Append("</br>");
                                    str3.Append("<span>");
                                    str3.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.LATECHECKOUTDESC));
                                    str3.Append("</span>");
                                    str3.Append("</li>");
                                    specialRequestList += str3.ToString();
                                    break;
                                }
                            default:
                                break;
                        }
                    }
                    OthersRequestCtrl.Style.Add("display", "none");
                    SpecialRequestCtrl.Style.Add("display", "none");

                    pfOthersRequestCtrl.Style.Add("display", "none");
                    pfSpecialRequestCtrl.Style.Add("display", "none");

                    specialRequestSection.Visible = true;
                    if ((string.IsNullOrEmpty(otherPeferencesList)) && (string.IsNullOrEmpty(specialRequestList)))
                    {
                        specialRequestSection.Visible = false;
                    }
                    if (!string.IsNullOrEmpty(otherPeferencesList))
                    {
                        if (htmlOthGenControl.ID != "pfLblGuestOtherPref")
                        {
                            otherPeferencesList = "<ul class = conf_List>" + otherPeferencesList + "</ul>";
                        }
                        OthersRequestCtrl.Style.Add("display", "block");

                        htmlOthGenControl.InnerHtml = otherPeferencesList;
                        pfOthersRequestCtrl.Style.Add("display", "block");
                    }

                    if (!string.IsNullOrEmpty(specialRequestList))
                    {
                        if (htmlSplGenControl.ID != "pfLblGuestSpecialReq")
                        {
                            specialRequestList = "<ul class = conf_List>" + specialRequestList + "</ul>";
                        }
                        SpecialRequestCtrl.Style.Add("display", "block");
                        htmlSplGenControl.InnerHtml = specialRequestList;
                        pfSpecialRequestCtrl.Style.Add("display", "block");
                    }
                }
            }
        }

        /// <summary>
        /// DisplayAdditionalRequest
        /// </summary>
        /// <param name="htmlAddlControl"></param>
        /// <param name="guestInfoEntity"></param>
        public void DisplayAdditionalRequest(HtmlGenericControl htmlAddlControl, GuestInformationEntity guestInfoEntity)
        {
            System.Text.StringBuilder str3 = new System.Text.StringBuilder();
            str3.Append("<li>");
            str3.Append("<span>");
            str3.Append(guestInfoEntity.AdditionalRequest);
            str3.Append("</span>");
            str3.Append(" </li>");
            htmlAddlControl.InnerHtml = "<ul class = conf_List>" + str3.ToString() + "</ul>";
        }

        /// <summary>
        /// Function To add Childs per Room informations to Respective
        /// Controls
        /// </summary>
        /// <param name="controlId">Div To Which Child Control Is to Attach </param>
        /// <param name="lstChildEntity"></param>
        private void AddChildDivToTheRoomDiv(string controlId, IList<ChildEntity> lstChildEntity)
        {
            if ((lstChildEntity != null) && (lstChildEntity.Count > 0))
            {
                HtmlGenericControl htmControl = this.FindControl(controlId) as HtmlGenericControl;
                StringBuilder newstringBuilder = new StringBuilder();
                for (int iChildCount = 0; iChildCount < lstChildEntity.Count; iChildCount++)
                {
                    int iCount = iChildCount;
                    string childsBedType = string.Empty;
                    if (string.IsNullOrEmpty(lstChildEntity[iChildCount].AccommodationString))
                    {
                        childsBedType = getBedDescOfChild(lstChildEntity[iChildCount].ChildAccommodationType);
                        newstringBuilder.Append("<li><strong>" +
                                                WebUtil.GetTranslatedText(
                                                    "/bookingengine/booking/childrensdetails/child") + " " +
                                                (iCount + 1) + "</strong>" + " " + childsBedType + "</li>");
                    }
                    else
                    {
                        newstringBuilder.Append("<li><strong>" +
                                                WebUtil.GetTranslatedText(
                                                    "/bookingengine/booking/childrensdetails/child") + " " +
                                                (iCount + 1) + "</strong>" + " " +
                                                lstChildEntity[iChildCount].AccommodationString + "</li>");
                    }
                }
                htmControl.InnerHtml = newstringBuilder.ToString();
            }
        }

        /// <summary>
        /// getBedDescOfChild
        /// </summary>
        /// <param name="childAccommodationType"></param>
        /// <returns>BedDescOfChild</returns>
        private string getBedDescOfChild(ChildAccommodationType childAccommodationType)
        {
            string childsBedType = string.Empty;
            switch (childAccommodationType)
            {
                case ChildAccommodationType.CIPB:
                    {
                        childsBedType =
                            WebUtil.GetTranslatedText(
                                "/bookingengine/booking/childrensdetails/accommodationtypes/sharingbed");
                        break;
                    }
                case ChildAccommodationType.CRIB:
                    {
                        childsBedType =
                            WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/crib");
                        break;
                    }
                case ChildAccommodationType.XBED:
                    {
                        childsBedType =
                            WebUtil.GetTranslatedText(
                                "/bookingengine/booking/childrensdetails/accommodationtypes/extrabed");
                        break;
                    }
                default:
                    {
                        childsBedType = string.Empty;
                        break;
                    }
            }
            return childsBedType;
        }

        /// <summary>
        /// This method populates the Room information in PDF in cancel Flow
        /// </summary>
        /// <param name="CancellationRoomInfo"></param>
        public void PopulateRoomInformation(Dictionary<string, string> CancellationRoomInfo)
        {
            if (!string.IsNullOrEmpty(CancellationRoomInfo[CommunicationTemplateConstants.LEGNUMBER]))
            {
                int roomNumber = Convert.ToInt32(CancellationRoomInfo[CommunicationTemplateConstants.LEGNUMBER]);
            }

            string ReservationNumber = CancellationRoomInfo[CommunicationTemplateConstants.CONFIRMATION_NUMBER];
            if (CancellationRoomInfo != null &&
                CancellationRoomInfo.ContainsKey(CommunicationTemplateConstants.RATE_CATEGORY))
            {
                lblRoomRate.InnerHtml =
                    Utility.FormatFieldsToDisplay(CancellationRoomInfo[CommunicationTemplateConstants.RATE_CATEGORY], 25);
                pfLblRoomRate.InnerHtml =
                    Utility.FormatFieldsToDisplay(CancellationRoomInfo[CommunicationTemplateConstants.RATE_CATEGORY], 20);
            }

            if (BookingEngineSessionWrapper.HideARBPrice)
            {
                lblGuestRatePerNight.InnerText = Utility.GetPrepaidString();
                pfLblGuestRatePerNight.InnerText = Utility.GetPrepaidString();
            }
            else
            {
                lblGuestRatePerNight.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.ROOM_RATE];
                pfLblGuestRatePerNight.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.ROOM_RATE];
            }
            
            DNumberContainer.Visible = false;
            pfDNumberContainer.Visible = false;

            if (CancellationRoomInfo.ContainsKey(CommunicationTemplateConstants.DNUMBER))
            {
                if (!string.IsNullOrEmpty(CancellationRoomInfo[CommunicationTemplateConstants.DNUMBER]))
                {
                    ShowDnumber = CancellationRoomInfo[CommunicationTemplateConstants.DNUMBER];
                    DNumberContainer.Visible = true;
                    pfDNumberContainer.Visible = true;
                }
            }
            divCancelFlowRoomHeader.Visible = true;
            reservationNumber.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.CONFIRMATION_NUMBER];
            cancellationNumber.InnerText =
                CancellationRoomInfo[CommunicationTemplateConstants.CANCEL_CONFIRMATION_NUMBER];
            Header.Visible = false;
            divCancelFlowRoomHeaderBorder.Visible = true;
            pdfRoomTypeInfo.Visible = false;
            divRoomPreferences.Visible = false;
            divUserStayInfoContainer.Visible = true;
            lblAnchRoomRate.Visible = false;
            spnArrivalDate.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.ARRIVAL_DATE];
            spnDepartureDate.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.DEPARTURE_DATE];
            spnHotelName.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.CITY_HOTEL];
            lblTitle.InnerText = Utility.GetNameTitle(CancellationRoomInfo[CommunicationTemplateConstants.TITLE]).Trim();

            lblFName.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.FIRST_NAME];
            lblLName.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.LASTNAME];

            lblCountry.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.COUNTRY];
            lblEmail.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.EMAIL];
            lblMobileValue.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.TELEPHONE1];

            pfDivCancelFlowRoomHeader.Visible = true;
            pfReservationNumber.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.CONFIRMATION_NUMBER];
            pfCancellationNumber.InnerText =
                CancellationRoomInfo[CommunicationTemplateConstants.CANCEL_CONFIRMATION_NUMBER];
            divUserStayInfoContainer.Visible = true;
            pfSpnArrivalDate.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.ARRIVAL_DATE];
            pfSpnDepartureDate.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.DEPARTURE_DATE];
            pfSpnHotelName.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.CITY_HOTEL];
            pfLblFName.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.FIRST_NAME];
            pfLblLName.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.LASTNAME];

            pfLblCountry.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.COUNTRY];
            pfLblEmail.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.EMAIL];
            pfLblMobileValue.InnerText = CancellationRoomInfo[CommunicationTemplateConstants.TELEPHONE1];

            if (CancellationRoomInfo.ContainsKey(CommunicationTemplateConstants.MEMBERSHIPNUMBER))
            {
                lblFGPNumber.InnerText =
                    CancellationRoomInfo[CommunicationTemplateConstants.MEMBERSHIPNUMBER].ToString();
                divFGPNumber.Attributes.Add("style", "display:block");
                pfLblFGPNumber.InnerText =
                    CancellationRoomInfo[CommunicationTemplateConstants.MEMBERSHIPNUMBER].ToString();
                pfDivFGPNumber.Attributes.Add("style", "display:block");
            }

            divSingleRoom1.Visible = true;
        }
    }
}