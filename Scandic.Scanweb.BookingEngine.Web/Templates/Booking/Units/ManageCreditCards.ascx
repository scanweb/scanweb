﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageCreditCards.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ManageCreditCards" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%--<link rel="stylesheet" type="text/css" href="/Templates/Booking/Styles/Default/reservation2.0.css" />--%>
 <div class="formGroup mrgTop19">
 <p class="signupformHeaders">
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/ManageCCHeader") %>
 </p>
<p>
    <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/ManageCCDescription") %>
</p>
<!--This section dynamically populated from the code-->
<div id="creditCardsSection" runat="server">
</div>
<div class="clear">
                &nbsp;</div>
<div id="ccNotDeletedOverlay" class="jqmWindow hide">
    <h2 class="overlayheader">
        <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/CCNotDeleted") %>
        <span>
            <img src="/templates/Booking/Styles/Default/Images/cc_fail_icon.png" width="32"
                height="33" alt="confirm" /></span>
    </h2>
    <div class="overlaymessage deleteCCDesc">
        <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/CCNotDeletedDesc") %>
    </div>
    <div class="saveCCMessageOK">
        <span class="jqmClose jqmClose_saveCreditCard">
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/CCNotSavedOK")%>
        </span>
    </div>
</div>


<div id="ccDeleteConfirmOverlay" class="jqmWindow hide">
    <h2 class="overlayheader">
        <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/CCDeleteConfirm") %>
    </h2>
    <div class="overlaymessage deleteCCDesc deleteCCConfirmOverlay">
        <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/CCDeleteConfirmQuestion") %>
    </div>
    <div class="saveCCMessageOK deleteCCConfirmCancel" >
        <span class="jqmClose jqmClose_saveCreditCard" id="btnDeleteCancel">
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/CCDeleteConfirmCancel")%>
        </span>
    </div>
    <div class="saveCCMessageOK deleteCCConfirmOK"  >
        <span class="jqmClose jqmClose_saveCreditCard" id="btnDeleteOK">
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/Yes")%>
        </span>
    </div>

</div>
</div>
<script type="text/javascript" language="javascript">
    var buttonPressed = '';
    var mcreditCardOperaID = null;
    
     $(document).ready(function() {
        $("#ccDeleteConfirmOverlay").jqm();
        $("#ccNotDeletedOverlay").jqm();
      });
      
      $("#btnDeleteOK").click(function(){
        DeleteCC();
      });
      
    function DeleteCreditCard(rowToBeDeleted, creditCardOperaID) {
        mcreditCardOperaID = creditCardOperaID;
        
        if (creditCardOperaID == null) return;
        $('#StageAreaWide #MainBodyArea').css('position','static');
        $("#ccDeleteConfirmOverlay").jqmShow();
    }

    function DeleteCC() {
     $.ajax({
            url: "<%= GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") %>",
            type: "POST",
            data: { methodToCall: 'DeleteCreditCard', creditCardOperaID: mcreditCardOperaID },
            cache: false,
            success: function(result) {
                if (result != 'ERROR') {
                    $("#" + "<%=creditCardsSection.ClientID %>").html(result);
                }
                else {
                    $("#ccNotDeletedOverlay").jqmShow();
                }
            },
            error: function(result) {
                $("#ccNotDeletedOverlay").jqmShow();
            }
        });
    }
</script>

