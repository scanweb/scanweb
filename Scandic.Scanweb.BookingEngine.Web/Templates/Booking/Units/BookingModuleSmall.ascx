<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingModuleBig.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.BookingModuleBig" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%@ Register Src="DestinationSearch.ascx" TagName="DestinationSearch" TagPrefix="uc1" %>
<%@ Register Src="Calendar.ascx" TagName="Calendar" TagPrefix="uc2" %>
<!-- Booking Area Big -->
<div class="singleCol">
    <div id="yourStayMod05">
        <div class="bookingContent regular margin0">
            <!-- Accordian content Booking Details Starts here -->
            <div class="expand" id="tabContainer1">
                <div class="hdFrst scansprite" id="Tab1">
                    <div class="hdRtFrst scansprite">
                        <% if (Utility.GetCurrentLanguage().Equals("ru-RU") || Utility.CheckifFallback())
                       {%>
                        <h3 class="stoolHeadingAlternate">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookaHotel") %>
                        </h3>
                        <% }
                       else
                       {%>
                        <h3 class="stoolHeading">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookaHotel") %>
                        </h3>
                        <% } %>
                        <%-- <h3 class="stoolHeading">
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookaHotel")%>
                        </h3>--%>
                    </div>
                </div>
                <div class="mainContent cnt fltLft modifyDetail" style="display: block;">
                    <div id="RegClientErrorDiv" class="errorText" runat="server" enableviewstate="False">
                    </div>
                    <div class="calCont">
                        <p class="margin0 BBSmallLabel">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/WhereToStay") %>
                            </strong>
                        </p>
                        <div class="bookingCode">
                            <input id="txtHotelName" tabindex="1" type="text" class="input defaultColor" name="city"
                                autocomplete="off" runat="server" />
                            <div id="autosuggest" class="autosuggest">
                                <ul>
                                </ul>
                                <iframe src="javascript:false;" frameborder="0" scrolling="no"></iframe>
                            </div>
                            <br class="clear" />
                        </div>
                        <div onkeypress="return WebForm_FireDefaultButton(event, '<%= btnSearch.ClientID %>')">
                            <div class="colm3 formColmChn">
                                <label for="cal">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin") %>
                                </label>
                                <input class="dateRange txtArrivalDate" name="cal" tabindex="203" type="text" id="txtArrivalDate"
                                    runat="server" />
                                <br class="clear" />
                            </div>
                            <div class="colm1 formColmChn">
                                <label for="nights">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Nights") %>
                                </label>
                                <!--Release R 2.0| Change ddlnoOfNights to txtnoOfNights(Shameem)-->
                                <asp:TextBox ID="txtnoOfNights" MaxLength="2" runat="server" CssClass="ignore" Style="width: 25px;"></asp:TextBox>
                                <%--<asp:DropDownList ID="ddlnoOfNights" runat="server">
                            </asp:DropDownList>--%>
                                <br class="clear" />
                            </div>
                            <br class="clear" />
                        </div>
                    </div>
                    <div onkeypress="return WebForm_FireDefaultButton(event, '<%= btnSearch.ClientID %>')">
                        <div class="calCont">
                            <div class="colm3 formColmChn">
                                <label for="cal">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut") %>
                                </label>
                                <input class="dateRange txtDepartureDate" name="cal" tabindex="204" type="text" id="txtDepartureDate"
                                    runat="server" />
                                <br class="clear" />
                            </div>
                            <br class="clear" />
                        </div>
                        <br class="clear" />
                        <p>
                            <span class="roomsTxt">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/HowManyRooms") %>
                            </span>
                        </p>
                        <div class="colm1 formColmChn M05Bpad padBtm5">
                            <asp:DropDownList ID="ddlNoOfRoomsReg" TabIndex="6" runat="server" CssClass="roomSelect"
                                rel="#regularBking">
                            </asp:DropDownList>
                        </div>
                        <!-- Copied from Booking module big -->
                        <div class="roomOutput" id="regularBking">
                            <div class="roundMe grayBox howManyRoom M05B" style="display: block;" id="idRoom1"
                                runat="server">
                                <div class="colmMerg fltLft roomOutput">
                                    <div class="colm formColmChn fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room1") %>
                                        </strong>
                                    </div>
                                    <div class="colm1 widthLs fltLft">
                                        <label for="adult">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                        </label>
                                        <asp:DropDownList ID="ddlAdultsPerRoom1" TabIndex="7" runat="server" rel="adult">
                                        </asp:DropDownList>
                                        <label for="adult" style="width: 58px;">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                            13+</label>
                                    </div>
                                    <div class="colm1 widthMr fltRt">
                                        <label for="child">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenSmallTxt") %>
                                        </label>
                                        <asp:DropDownList ID="ddlChildPerRoom1" TabIndex="8" runat="server" CssClass="child chafltRt"
                                            rel="choutput1">
                                        </asp:DropDownList>
                                        <label for="child" style="width: 50px;">
                                            0-12</label>
                                    </div>
                                    <div class="childSelct choutput1" id="idRoom1ChildLabel" runat="server">
                                        <div class="bord fltLft roundMe grayBoxStay">
                                            <div class="fltLft chdSelTtl">
                                                <!-- BHAVYA-->
                                                <!--<div style="width: 100px;"><!-- BHAVYA-->
                                                <div style="position: absolute; left: 5px; top: 2px;">
                                                    <strong class="fltLft">
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                                    </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"
                                                        class="help spriteIcon toolTipMe"></span>
                                                </div>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="fltLft chdSelTtl">
                                                <p class="fltLft">
                                                    <strong>
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AgeandSelectBedType") %>
                                                    </strong>
                                                </p>
                                            </div>
                                            <div class="selHdr1">
                                                <strong>
                                                    <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>--%>
                                                </strong>
                                            </div>
                                            <!-- BHAVYA div alignment corrected -->
                                            <div class="colm5 padTop" id="idRoom1Child1Ddl" runat="server">
                                                <label class="fltLft">
                                                    1)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom1Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild114">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput11">
                                                    <asp:DropDownList ID="bedTypeforRoom1Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild114">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom1Child2Ddl" runat="server">
                                                <label class="fltLft">
                                                    2)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom1Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild124">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput12">
                                                    <asp:DropDownList ID="bedTypeforRoom1Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild124">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom1Child3Ddl" runat="server">
                                                <label class="fltLft">
                                                    3)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom1Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild134">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput13">
                                                    <asp:DropDownList ID="bedTypeforRoom1Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild134">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom1Child4Ddl" runat="server">
                                                <label class="fltLft">
                                                    4)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom1Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild144">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput14">
                                                    <asp:DropDownList ID="bedTypeforRoom1Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild144">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom1Child5Ddl" runat="server">
                                                <label class="fltLft">
                                                    5)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom1Child5" TabIndex="7" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild154">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput15">
                                                    <asp:DropDownList ID="bedTypeforRoom1Child5" TabIndex="7" runat="server" CssClass="selBedTyp subchild154">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--<div style="display: inline-block; left: 190px; position: relative; top: -137px;"
                                    class="selHdr1">
                                    <strong>
                                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>
                                    </strong>
                                </div>--%>
                                </div>
                            </div>
                            <div class="roundMe grayBox howManyRoom M05B" id="idRoom2" runat="server">
                                <div class="colmMerg fltLft roomOutput">
                                    <div class="colm formColmChn fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room2") %>
                                        </strong>
                                    </div>
                                    <div class="colm1 widthLs fltLft">
                                        <label for="adult">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                        </label>
                                        <asp:DropDownList ID="ddlAdultsPerRoom2" TabIndex="6" runat="server" rel="adult">
                                        </asp:DropDownList>
                                        <label for="adult" style="width: 58px;">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                            13+</label>
                                    </div>
                                    <div class="colm1 widthMr fltRt">
                                        <label for="child">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenSmallTxt") %>
                                        </label>
                                        <asp:DropDownList ID="ddlChildPerRoom2" TabIndex="6" runat="server" CssClass="child chafltRt"
                                            rel="choutput2">
                                        </asp:DropDownList>
                                        <label for="child" style="width: 50px;">
                                            0-12</label>
                                    </div>
                                    <div class="childSelct choutput2" id="idRoom2ChildLabel" runat="server">
                                        <div class="bord fltLft roundMe grayBoxStay">
                                            <div class="fltLft chdSelTtl">
                                                <!-- BHAVYA-->
                                                <%-- <div style="width: 100px;">--%>
                                                <!-- BHAVYA-->
                                                <div style="position: absolute; left: 5px; top: 2px;">
                                                    <strong class="fltLft">
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                                    </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"
                                                        class="help spriteIcon toolTipMe"></span>
                                                </div>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="fltLft chdSelTtl">
                                                <p class="fltLft">
                                                    <strong>
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AgeandSelectBedType") %>
                                                    </strong>
                                                </p>
                                            </div>
                                            <div class="selHdr1">
                                                <strong>
                                                    <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>--%>
                                                </strong>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="colm5 padTop" id="idRoom2Child1Ddl" runat="server">
                                                <label class="fltLft">
                                                    1)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom2Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild214">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput21">
                                                    <asp:DropDownList ID="bedTypeforRoom2Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild214">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom2Child2Ddl" runat="server">
                                                <label class="fltLft">
                                                    2)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom2Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild224">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput22">
                                                    <asp:DropDownList ID="bedTypeforRoom2Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild224">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom2Child3Ddl" runat="server">
                                                <label class="fltLft">
                                                    3)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom2Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild234">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput23">
                                                    <asp:DropDownList ID="bedTypeforRoom2Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild234">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom2Child4Ddl" runat="server">
                                                <label class="fltLft">
                                                    4)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom2Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild244">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput24">
                                                    <asp:DropDownList ID="bedTypeforRoom2Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild244">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom2Child5Ddl" runat="server">
                                                <label class="fltLft">
                                                    5)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom2Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild254">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput24">
                                                    <asp:DropDownList ID="bedTypeforRoom2Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchild254">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="roundMe grayBox howManyRoom M05B" id="idRoom3" runat="server">
                                <div class="colmMerg fltLft roomOutput">
                                    <div class="colm formColmChn fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room3") %>
                                        </strong>
                                    </div>
                                    <div class="colm1 widthLs fltLft">
                                        <label for="adult">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                        </label>
                                        <asp:DropDownList ID="ddlAdultsPerRoom3" TabIndex="6" runat="server" rel="adult">
                                        </asp:DropDownList>
                                        <label for="adult" style="width: 58px;">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                            13+</label>
                                    </div>
                                    <div class="colm1 widthMr fltRt">
                                        <label for="child">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenSmallTxt") %>
                                        </label>
                                        <asp:DropDownList ID="ddlChildPerRoom3" TabIndex="6" runat="server" CssClass="child chafltRt"
                                            rel="choutput3">
                                        </asp:DropDownList>
                                        <label for="child" style="width: 50px;">
                                            0-12</label>
                                    </div>
                                    <div class="childSelct choutput3" id="idRoom3ChildLabel" runat="server">
                                        <div class="bord fltLft roundMe grayBoxStay">
                                            <div class="fltLft chdSelTtl">
                                                <!-- BHAVYA-->
                                                <%--<div style="width: 100px;">--%>
                                                <!-- BHAVYA-->
                                                <div style="position: absolute; left: 5px; top: 2px;">
                                                    <strong class="fltLft">
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                                    </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"
                                                        class="help spriteIcon toolTipMe"></span>
                                                </div>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="fltLft chdSelTtl">
                                                <p class="fltLft">
                                                    <strong>
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AgeandSelectBedType") %>
                                                    </strong>
                                                </p>
                                            </div>
                                            <div class="selHdr1">
                                                <strong>
                                                    <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>--%>
                                                </strong>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="colm5 padTop" id="idRoom3Child1Ddl" runat="server">
                                                <label class="fltLft">
                                                    1)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom3Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild314">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput31">
                                                    <asp:DropDownList ID="bedTypeforRoom3Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild314">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom3Child2Ddl" runat="server">
                                                <label class="fltLft">
                                                    2)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom3Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild324">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput32">
                                                    <asp:DropDownList ID="bedTypeforRoom3Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild324">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom3Child3Ddl" runat="server">
                                                <label class="fltLft">
                                                    3)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom3Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild334">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput33">
                                                    <asp:DropDownList ID="bedTypeforRoom3Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild334">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom3Child4Ddl" runat="server">
                                                <label class="fltLft">
                                                    4)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom3Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild344">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput34">
                                                    <asp:DropDownList ID="bedTypeforRoom3Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild344">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom3Child5Ddl" runat="server">
                                                <label class="fltLft">
                                                    5)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom3Child5" TabIndex="7" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild354">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput35">
                                                    <asp:DropDownList ID="bedTypeforRoom3Child5" TabIndex="7" runat="server" CssClass="selBedTyp subchild354">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Bhavya-->
                                    <%--      <div style="display: inline-block; left: 190px; position: relative; top: -137px;"
                                    class="selHdr3">
                                    <strong>
                                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>
                                    </strong>
                                </div>--%>
                                    <!--Bhavya-->
                                </div>
                            </div>
                            <div class="roundMe grayBox howManyRoom M05B" id="idRoom4" runat="server">
                                <div class="colmMerg fltLft roomOutput">
                                    <div class="colm formColmChn fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room4") %>
                                        </strong>
                                    </div>
                                    <div class="colm1 widthLs fltLft">
                                        <label for="adult">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                        </label>
                                        <asp:DropDownList ID="ddlAdultsPerRoom4" TabIndex="6" runat="server" name="adult">
                                        </asp:DropDownList>
                                        <label for="adult" style="width: 58px;">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                            13+</label>
                                    </div>
                                    <div class="colm1 widthMr fltRt">
                                        <label for="child">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenSmallTxt") %>
                                        </label>
                                        <asp:DropDownList ID="ddlChildPerRoom4" TabIndex="6" runat="server" CssClass="child chafltRt"
                                            rel="choutput4">
                                        </asp:DropDownList>
                                        <label for="child" style="width: 50px;">
                                            0-12</label>
                                    </div>
                                    <div class="childSelct choutput4" id="idRoom4ChildLabel" runat="server">
                                        <div class="bord fltLft roundMe grayBoxStay">
                                            <div class="fltLft chdSelTtl">
                                                <!-- BHAVYA-->
                                                <%--<div style="width: 100px;">--%>
                                                <!-- BHAVYA-->
                                                <div style="position: absolute; left: 5px; top: 2px;">
                                                    <strong class="fltLft">
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                                    </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"
                                                        class="help spriteIcon toolTipMe"></span>
                                                </div>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="fltLft chdSelTtl">
                                                <p class="fltLft">
                                                    <strong>
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AgeandSelectBedType") %>
                                                    </strong>
                                                </p>
                                            </div>
                                            <div class="selHdr1">
                                                <strong>
                                                    <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>--%>
                                                </strong>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="colm5 padTop" id="idRoom4Child1Ddl" runat="server">
                                                <label class="fltLft">
                                                    1)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom4Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild414">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput41">
                                                    <asp:DropDownList ID="bedTypeforRoom4Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild414">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom4Child2Ddl" runat="server">
                                                <label class="fltLft">
                                                    2)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom4Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild424">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput42">
                                                    <asp:DropDownList ID="bedTypeforRoom4Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild424">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom4Child3Ddl" runat="server">
                                                <label class="fltLft">
                                                    3)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom4Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild434">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput43">
                                                    <asp:DropDownList ID="bedTypeforRoom4Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild434">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom4Child4Ddl" runat="server">
                                                <label class="fltLft">
                                                    4)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom4Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild444">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput44">
                                                    <asp:DropDownList ID="bedTypeforRoom4Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild444">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idRoom4Child5Ddl" runat="server">
                                                <label class="fltLft">
                                                    5)
                                                </label>
                                                <asp:DropDownList ID="childAgeforRoom4Child5" TabIndex="7" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild454">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput45">
                                                    <asp:DropDownList ID="bedTypeforRoom4Child5" TabIndex="7" runat="server" CssClass="selBedTyp subchild454">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Bhavya-->
                                    <%--         <div style="display: inline-block; left: 190px; position: relative; top: -137px;"
                                    class="selHdr4">
                                    <strong>
                                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>
                                    </strong>
                                </div>--%>
                                    <!--Bhavya-->
                                </div>
                            </div>
                        </div>
                        <!-- Earlier code-->
                        <p class="fltLft mrgBtm10">
                            <span class="fltLft">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterBookingCode1") %>
                            </span><span class="help spriteIcon toolTipMe" title=" <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingCodeToolTip") %>">
                            </span>
                        </p>
                        <div class="bookingCode">
                            <input type="text" name="txtRegularCode" runat="server" id="txtRegularCode" value=""
                                class="roundMe input defaultColor" />
                        </div>
                        <div class="chkBox">
                            <input type="checkbox" name="disFrndRoom" id="rememberMe" value="" runat="server" />
                            <label for="disFrndRoom">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RememberBookingCode") %>
                            </label>
                        </div>
                        <br class="clear" />
                        <div class="HR mrgBtm10">
                        </div>
                        <div class="ftButtons fieldOption">
                            <div class="actionBtn fltRt">
                                <asp:LinkButton ID="btnSearch" runat="server" CssClass="buttonInner" OnClick="btnSearch_Click"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Search") %></asp:LinkButton>
                                <asp:LinkButton ID="spnSearch" runat="server" CssClass="buttonRt scansprite" OnClick="btnSearch_Click"></asp:LinkButton>
                            </div>
                        </div>
                        <br class="clear" />
                    </div>
                </div>
            </div>
            <!-- Accordian content Booking Details Ends here -->
            <!-- Accordian content BC Redeem starts here -->
            <div class="expand" id="tabContainer2">
                <div class="hdDeflt scansprite" id="Tab2">
                    <div class="hdRtDeflt scansprite">
                        <% if (Utility.GetCurrentLanguage().Equals("ru-RU") || Utility.CheckifFallback())
                        {%>
                        <h3 class="stoolHeadingAlternate">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RedeemYourBonusCheques") %>
                        </h3>
                        <% }
                        else
                        {%>
                        <h3 class="stoolHeading">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RedeemYourBonusCheques") %>
                        </h3>
                        <% } %>
                        <%--<h3 class="stoolHeading">
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RedeemYourBonusCheques")%>
                        </h3>--%>
                    </div>
                </div>
                <div class="mainContent cnt fltLft modifyDetail" style="display: none;">
                    <div id="BCClientErrorDiv" class="errorText" runat="server" enableviewstate="False">
                    </div>
                    <div class="calCont">
                        <p class="margin0 BBSmallLabel">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/WhereToStay") %>
                            </strong>
                        </p>
                        <div class="bookingCode">
                            <input id="txtHotelNameBNC" runat="server" type="text" name="city" class="input defaultColor"
                                autocomplete="off" />
                            <div id="autosuggest1" class="autosuggest">
                                <ul>
                                </ul>
                                <iframe src="javascript:false;" frameborder="0" scrolling="no"></iframe>
                            </div>
                            <br class="clear" />
                        </div>
                        <div onkeypress="return WebForm_FireDefaultButton(event, '<%= btnBonusChequeSearch.ClientID %>')">
                            <div class="colm3 formColmChn">
                                <label for="cal">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin") %>
                                </label>
                                <input type="text" name="chek" id="txtBCArrivalDate" class="dateRange txtArrivalDate"
                                    runat="server" />
                                <br class="clear" />
                            </div>
                            <div class="colm1 formColmChn">
                                <label for="nights">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Nights") %>
                                </label>
                                <!--Release R 2.0| Change ddlBonousChequeNoofNights to txtBonousChequeNoofNights(Shameem)-->
                                <asp:TextBox ID="txtBonousChequeNoofNights" MaxLength="2" runat="server" Style="width: 25px;"></asp:TextBox>
                                <%--<asp:DropDownList ID="ddlBonousChequeNoofNights" runat="server">
                            </asp:DropDownList>--%>
                                <br class="clear" />
                            </div>
                            <br class="clear" />
                        </div>
                    </div>
                    <div onkeypress="return WebForm_FireDefaultButton(event, '<%= btnBonusChequeSearch.ClientID %>')">
                        <div class="calCont">
                            <div class="colm3 formColmChn">
                                <label for="cal">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut") %>
                                </label>
                                <input type="text" name="cal" id="txtBCDepartureDate" runat="server" class="dateRange txtDepartureDate" />
                                <br class="clear" />
                            </div>
                            <br class="clear" />
                        </div>
                        <br class="clear" />
                        <p>
                            <span class="roomsTxt">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/HowManyRooms") %>
                            </span>
                        </p>
                        <div class="colm1 formColmChn M05Bpad padBtm5">
                            <asp:DropDownList ID="ddlNoOfRoomsBC" TabIndex="6" runat="server" CssClass="roomSelect"
                                rel="#redeem">
                            </asp:DropDownList>
                        </div>
                        <div class="roomOutput" id="redeem">
                            <div class="roundMe grayBox howManyRoom M05B" id="idBCRoom1" runat="server" style="display: block;">
                                <div class="colmMerg fltLft roomOutput">
                                    <div class="colm formColmChn fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room1") %>
                                        </strong>
                                    </div>
                                    <div class="colm1 widthLs fltLft">
                                        <label for="adult">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                        </label>
                                        <asp:DropDownList ID="ddlBCAdultsPerRoom1" TabIndex="7" runat="server" rel="adult">
                                        </asp:DropDownList>
                                        <label for="adult">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                            13+</label>
                                    </div>
                                    <div class="colm1 widthMr fltRt">
                                        <label for="child">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenSmallTxt") %>
                                        </label>
                                        <asp:DropDownList ID="ddlBCChildPerRoom1" TabIndex="8" runat="server" CssClass="child chafltRt"
                                            rel="choutputBC1">
                                        </asp:DropDownList>
                                        <label for="child">
                                            0-12</label>
                                    </div>
                                    <div class="childSelct choutputBC1" id="idBCRoom1ChildLabel" runat="server">
                                        <div class="bord fltLft roundMe grayBoxStay">
                                            <div class="fltLft chdSelTtl">
                                                <!-- BHAVYA-->
                                                <%-- <div style="width: 100px;">--%>
                                                <!-- BHAVYA-->
                                                <div style="position: absolute; left: 5px; top: 2px;">
                                                    <strong class="fltLft">
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                                    </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"
                                                        class="help spriteIcon toolTipMe"></span>
                                                </div>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="fltLft chdSelTtl">
                                                <p class="fltLft">
                                                    <strong>
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AgeandSelectBedType") %>
                                                    </strong>
                                                </p>
                                            </div>
                                            <div class="selHdr1">
                                                <strong>
                                                    <%-- <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>--%>
                                                </strong>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="colm5 padTop" id="idBCRoom1Child1Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    1)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom1Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC114">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput11">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom1Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC114">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom1Child2Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    2)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom1Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC124">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput12">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom1Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC124">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom1Child3Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    3)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom1Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC134">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput13">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom1Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC134">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom1Child4Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    4)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom1Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild1BC144">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput14">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom1Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild1BC144">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom1Child5Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    5)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom1Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild1BC154">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput15">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom1Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchild1BC154">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Bhavya-->
                                    <%--         <div style="display: inline-block; left: 190px; position: relative; top: -137px;"
                                    class="selHdr4">
                                    <strong>
                                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>
                                    </strong>
                                </div>--%>
                                    <!--Bhavya-->
                                </div>
                            </div>
                            <div class="roundMe grayBox howManyRoom M05B" id="idBCRoom2" runat="server">
                                <div class="colmMerg fltLft roomOutput">
                                    <div class="colm formColmChn fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room2") %>
                                        </strong>
                                    </div>
                                    <div class="colm1 widthLs fltLft">
                                        <label for="adult">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                        </label>
                                        <asp:DropDownList ID="ddlBCAdultsPerRoom2" TabIndex="6" runat="server" rel="adult">
                                        </asp:DropDownList>
                                        <label for="adult">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                            13+</label>
                                    </div>
                                    <div class="colm1 widthMr fltRt">
                                        <label for="child">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenSmallTxt") %>
                                        </label>
                                        <asp:DropDownList ID="ddlBCChildPerRoom2" TabIndex="6" runat="server" CssClass="child chafltRt"
                                            rel="choutputBC2">
                                        </asp:DropDownList>
                                        <label for="child">
                                            0-12</label>
                                    </div>
                                    <div class="childSelct choutputBC2" id="idBCRoom2ChildLabel" runat="server">
                                        <div class="bord fltLft roundMe grayBoxStay">
                                            <div class="fltLft chdSelTtl">
                                                <!-- BHAVYA-->
                                                <%-- <div style="width: 100px;">--%>
                                                <!-- BHAVYA-->
                                                <div style="position: absolute; left: 5px; top: 2px;">
                                                    <strong class="fltLft">
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                                    </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"
                                                        class="help spriteIcon toolTipMe"></span>
                                                </div>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="fltLft chdSelTtl">
                                                <p class="fltLft">
                                                    <strong>
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AgeandSelectBedType") %>
                                                    </strong>
                                                </p>
                                            </div>
                                            <div class="selHdr1">
                                                <strong>
                                                    <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>--%>
                                                </strong>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="colm5 padTop" id="idBCRoom2Child1Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    1)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom2Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC214">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput21">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom2Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC214">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom2Child2Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    2)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom2Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC224">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput22">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom2Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC224">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom2Child3Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    3)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom2Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC234">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput23">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom2Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC234">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom2Child4Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    4)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom2Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC244">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput24">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom2Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC244">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom2Child5Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    5)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom2Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC254">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput25">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom2Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC254">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Bhavya-->
                                    <%--         <div style="display: inline-block; left: 190px; position: relative; top: -137px;"
                                    class="selHdr4">
                                    <strong>
                                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>
                                    </strong>
                                </div>--%>
                                    <!--Bhavya-->
                                </div>
                            </div>
                            <div class="roundMe grayBox howManyRoom M05B" id="idBCRoom3" runat="server">
                                <div class="colmMerg fltLft roomOutput">
                                    <div class="colm formColmChn fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room3") %>
                                        </strong>
                                    </div>
                                    <div class="colm1 widthLs fltLft">
                                        <label for="adult">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                        </label>
                                        <asp:DropDownList ID="ddlBCAdultsPerRoom3" TabIndex="6" runat="server" rel="adult">
                                        </asp:DropDownList>
                                        <label for="adult">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                            13+</label>
                                    </div>
                                    <div class="colm1 widthMr fltRt">
                                        <label for="child">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenSmallTxt") %>
                                        </label>
                                        <asp:DropDownList ID="ddlBCChildPerRoom3" TabIndex="6" runat="server" CssClass="child chafltRt"
                                            rel="choutputBC3">
                                        </asp:DropDownList>
                                        <label for="child">
                                            0-12</label>
                                    </div>
                                    <div class="childSelct choutputBC3" id="idBCRoom3ChildLabel" runat="server">
                                        <div class="bord fltLft roundMe grayBoxStay">
                                            <div class="fltLft chdSelTtl">
                                                <!-- BHAVYA-->
                                                <%-- <div style="width: 100px;">--%>
                                                <!-- BHAVYA-->
                                                <div style="position: absolute; left: 5px; top: 2px;">
                                                    <strong class="fltLft">
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                                    </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"
                                                        class="help spriteIcon toolTipMe"></span>
                                                </div>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="fltLft chdSelTtl">
                                                <p class="fltLft">
                                                    <strong>
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AgeandSelectBedType") %>
                                                    </strong>
                                                </p>
                                            </div>
                                            <div class="selHdr1">
                                                <strong>
                                                    <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>--%>
                                                </strong>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="colm5 padTop" id="idBCRoom3Child1Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    1)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom3Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC314">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput31">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom3Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC314">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom3Child2Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    2)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom3Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC324">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput32">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom3Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC324">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom3Child3Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    3)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom3Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC334">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput33">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom3Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC334">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom3Child4Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    4)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom3Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC344">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput34">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom3Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC344">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom3Child5Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    5)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom3Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC354">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput35">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom3Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC354">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Bhavya-->
                                    <%--         <div style="display: inline-block; left: 190px; position: relative; top: -137px;"
                                    class="selHdr4">
                                    <strong>
                                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>
                                    </strong>
                                </div>--%>
                                    <!--Bhavya-->
                                </div>
                            </div>
                            <div class="roundMe grayBox howManyRoom M05B" id="idBCRoom4" runat="server">
                                <div class="colmMerg fltLft roomOutput">
                                    <div class="colm formColmChn fltLft">
                                        <strong>
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room4") %>
                                        </strong>
                                    </div>
                                    <div class="colm1 widthLs fltLft">
                                        <label for="adult">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                        </label>
                                        <asp:DropDownList ID="ddlBCAdultsPerRoom4" TabIndex="6" runat="server" name="adult">
                                        </asp:DropDownList>
                                        <label for="adult">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                            13+</label>
                                    </div>
                                    <div class="colm1 widthMr fltRt">
                                        <label for="child">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenSmallTxt") %>
                                        </label>
                                        <asp:DropDownList ID="ddlBCChildPerRoom4" TabIndex="6" runat="server" CssClass="child chafltRt"
                                            rel="choutputBC4">
                                        </asp:DropDownList>
                                        <label for="child">
                                            0-12</label>
                                    </div>
                                    <div class="childSelct choutputBC4" id="idBCRoom4ChildLabel" runat="server">
                                        <div class="bord fltLft roundMe grayBoxStay">
                                            <div class="fltLft chdSelTtl">
                                                <!-- BHAVYA-->
                                                <%-- <div style="width: 100px;">--%>
                                                <!-- BHAVYA-->
                                                <div style="position: absolute; left: 5px; top: 2px;">
                                                    <strong class="fltLft">
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                                    </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"
                                                        class="help spriteIcon toolTipMe"></span>
                                                </div>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="fltLft chdSelTtl">
                                                <p class="fltLft">
                                                    <strong>
                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AgeandSelectBedType") %>
                                                    </strong>
                                                </p>
                                            </div>
                                            <div class="selHdr1">
                                                <strong>
                                                    <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>--%>
                                                </strong>
                                            </div>
                                            <!-- BHAVYA-->
                                            <div class="colm5 padTop" id="idBCRoom4Child1Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    1)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom4Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC414">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput41">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom4Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC414">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom4Child2Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    2)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom4Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC424">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput42">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom4Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC424">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom4Child3Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    3)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom4Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC434">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput43">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom4Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC434">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom4Child4Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    4)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom4Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC444">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput44">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom4Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC444">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5" id="idBCRoom4Child5Ddl" runat="server" style="display: none;">
                                                <label class="fltLft">
                                                    5)
                                                </label>
                                                <asp:DropDownList ID="ddlBCChildAgeforRoom4Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchildBC454">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput45">
                                                    <asp:DropDownList ID="ddlBCBedTypeforRoom4Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchildBC454">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br class="clear" />
                        <div class="HR mrgBtm10">
                        </div>
                        <div id="yourStayAlertBox">
                            <div class="threeCol alertContainer">
                                <div class="hd sprite">
                                    &nbsp;</div>
                                <div class="cnt">
                                    <p>
                                        <span class="spriteIcon"></span>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/bctext") %>
                                    </p>
                                </div>
                                <div class="ft sprite">
                                </div>
                            </div>
                        </div>
                        <br class="clear" />
                        <div class="ftButtons">
                            <div class="actionBtn fltRt">
                                <asp:LinkButton ID="btnBonusChequeSearch" runat="server" CssClass="buttonInner" OnClick="btnSearch_Click"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Search") %></asp:LinkButton>
                                <asp:LinkButton ID="spnBonusChequeSearch" runat="server" CssClass="buttonRt scansprite"
                                    OnClick="btnSearch_Click"></asp:LinkButton>
                            </div>
                        </div>
                        <br class="clear" />
                    </div>
                </div>
            </div>
            <!-- Accordian content BC Redeem ends here -->
            <!-- Accordian content Claim rewards starts here -->
            <div class="expand" id="tabContainer3">
                <div class="hdDeflt scansprite" id="Tab3">
                    <div class="hdRtDeflt scansprite">
                        <% if (Utility.GetCurrentLanguage().Equals("ru-RU") || Utility.CheckifFallback())
                       {%>
                        <h3 class="stoolHeadingAlternate">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ClaimRewardNight") %>
                        </h3>
                        <% }
                       else
                       {%>
                        <h3 class="stoolHeading">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ClaimRewardNight") %>
                        </h3>
                        <% } %>
                        <%-- <h3 class="stoolHeading">
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ClaimRewardNight")%>
                        </h3>--%>
                    </div>
                </div>
                <div class="mainContent">
                    <div id="RNClientErrorDiv" class="errorText" runat="server" enableviewstate="False">
                    </div>
                    <asp:PlaceHolder ID="rewardNightPlaceHolder" Visible="false" runat="server">
                        <p class="pad20 marB20">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/loginrequired") %>
                        </p>
                        <div class="roundMe grayBox claimBox" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnLogin.ClientID %>')">
                            <label class="fltLft">
                                <span class="fltLft">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/PleaseLogIn") %>
                                </span>
                            </label>
                            <div class="colmLft">
                                <input type="text" name="memberPrefix" class="input rewardNightMemberLogin userPrefix40"
                                    id="txtUserNamePrefix" runat="server" readonly="readonly" />
                                <input type="text" name="member" class="input rewardNightMemberLogin w110" tabindex="300"
                                    id="txtUserName" runat="server" />
                                <asp:TextBox ID="txtPassword" TabIndex="2" TextMode="password" runat="server" MaxLength="20"
                                    CssClass="password-password" />
                                <%--R2.2 Artf1148124 tabindex of following password control should be eqaul to the remember me check box Ashish--%>
                                <input type="text" id="inputtxtPassword" tabindex="125" value='<%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/Password") %>'
                                    class="defaultColor password-clear" />
                            </div>
                            <div class="chkBox">
                                <input type="checkbox" value="" id="RememberUserIdPwd" class="chkBx" name="disFrndRoom"
                                    runat="server" />
                                <label for="rememberMe" class="fltLft">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/RememberMe") %>
                                </label>
                                <span title=" <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/RememberMeToolTip") %>"
                                    class="help spriteIcon toolTipMe"></span>
                            </div>
                            <div class="whitBoxRtUsr noPad fltRt">
                                <div class="usrBtn">
                                    <asp:LinkButton ID="btnLogin" runat="server" CssClass="buttonInner"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/login") %></asp:LinkButton>
                                    <asp:LinkButton ID="spnLogin" runat="server" CssClass="buttonRt"></asp:LinkButton>
                                </div>
                            </div>
                            <div id="RwrdNgtSmallLoginProgressDiv" runat="server" style="display: none">
                                 <span class="fltLft rwrdNgtSmlLoginPrg">
                                    <img src='<%=ResolveUrl("~/Templates/Booking/Styles/Default/Images/rotatingclockani.gif")%>'
                                        alt="image" align="middle" width="25px" height="25px" />
                                    <span>
                                        <%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/Loading")%>
                                    </span>
                                </span>
                            </div>
                            <br class="clear" />
                        </div>
                        <div>
                            <a href="#" class="fltLft linkHover" onclick="openPopupWin('<%= GlobalUtil.GetHelpPage(Utility.GetForgottenMembershipNoPageReference()) %>','width=400,height=300,scrollbars=yes,resizable=yes');return false;">
                                <div class="IconLink">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/forgottenmembershipno") %></div>
                            </a>
                            <asp:LinkButton ID="forgotPassword" runat="server" CssClass="fltLft padTop0 linkHover"
                                OnClick="ForgottenPassword_Click"><div class="IconLink"><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/forgottenpasswordmessage") %></div></asp:LinkButton>
                        </div>
                        <br class="clear" />
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="bookingPlaceHolder" Visible="false" runat="server">
                        <p class="margin0 BBSmallLabel">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/WhereToStay") %>
                            </strong>
                        </p>
                        <%--<input type="text" value=''
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName")%>' rel="Enter hotel name or destination" class="stayInputLft widthChng demoSuggest input" name="city"/>--%>
                        <div class="bookingCode">
                            <input id="txtHotelNameRewardNights" name="city" tabindex="1" value="" type="text"
                                class="stayInputLft widthChng input" autocomplete="off" runat="server" /><br />
                            <!--Auto sugestion Starts--->
                            <!-- CSS hack for Mac Safari and Safari version 3 browser -->
                            <style>
              #autosuggest ul{
              margin-top: 35px;
              overflow: scroll;
              }
              /* Mac Safari hack - the below css will be ignored by Mac safari less than version3*/
              #autosuggest ul{
              overflow-x: hidden;
              overflow-y: auto;
              margin-top: 0px;#
              }
              /* Safari 3 hack -the below css  works only on safari 3 browser */
              @media screen and (-webkit-min-device-pixel-ratio:0) {
              #autosuggest { margin-top: 35px;}
              }
            </style>
                            <!-- END - CSS hack for Mac Safari and Safari version 3 browser -->
                            <div id="autosuggest2" class="autosuggest">
                                <ul>
                                </ul>
                                <iframe src="javascript:false;" frameborder="0" scrolling="no"></iframe>
                            </div>
                        </div>
                        <div onkeypress="return WebForm_FireDefaultButton(event, '<%= btnRewardNightSearch.ClientID %>')">
                            <div class="roundMe grayBox stayWhn">
                                <div class="colm3 formColmChn">
                                    <label for="cal">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin") %>
                                    </label>
                                    <input type="text" name="cal" id="txtRNArrivalDate" class="dateRange txtArrivalDate"
                                        runat="server" />
                                    <br class="clear" />
                                </div>
                                <div class="colm1 formColmChn">
                                    <label for="nights">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Nights") %>
                                    </label>
                                    <!--Release R 2.0| Change ddlRewardNightNoOfNights to txtRewardNightNoOfNights(Shameem)-->
                                    <asp:TextBox ID="txtRewardNightNoOfNights" MaxLength="2" runat="server" Style="width: 25px;"></asp:TextBox>
                                    <%--   <asp:DropDownList ID="ddlRewardNightNoOfNights" runat="server">
                                </asp:DropDownList>--%>
                                    <br class="clear" />
                                </div>
                                <br class="clear" />
                                <div class="colm3 formColmChn">
                                    <label for="cal">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut") %>
                                    </label>
                                    <input type="text" name="cal" runat="server" id="txtRNDepartureDate" class="dateRange txtDepartureDate" />
                                    <br class="clear" />
                                </div>
                                <br class="clear" />
                            </div>
                            <div class="hwMany fltLft">
                                <p class="fltLft">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/HowManyRooms") %>
                                </p>
                                <asp:DropDownList ID="ddlNoOfRoomsRed" TabIndex="6" runat="server" CssClass="roomSelect"
                                    rel="#rewardBking">
                                </asp:DropDownList>
                            </div>
                            <div class="roomOutput" id="rewardBking">
                                <div class="roundMe grayBox howManyRoom M05B" style="display: block;" id="idRNRoom1"
                                    runat="server">
                                    <div class="colmMerg fltLft roomOutput">
                                        <div class="colm formColmChn fltLft">
                                            <strong>
                                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room1") %>
                                            </strong>
                                        </div>
                                        <div class="colm1 widthLs fltLft">
                                            <label for="adult">
                                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                                            </label>
                                            <asp:DropDownList ID="ddlRNAdultsPerRoom1" TabIndex="7" runat="server" rel="adult">
                                            </asp:DropDownList>
                                            <label for="adult">
                                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %>
                                                13+</label>
                                        </div>
                                        <div class="colm1 widthMr fltRt">
                                            <label for="child">
                                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenSmallTxt") %>
                                            </label>
                                            <asp:DropDownList ID="ddlRNChildPerRoom1" TabIndex="8" runat="server" CssClass="child chafltRt"
                                                rel="choutputRN1">
                                            </asp:DropDownList>
                                            <label for="child">
                                                0-12</label>
                                        </div>
                                        <div class="childSelct choutputRN1" id="idRNRoom1ChildLabel" runat="server">
                                            <div class="bord fltLft roundMe grayBoxHome">
                                                <div class="fltLft chdSelTtl">
                                                    <!-- BHAVYA-->
                                                    <%-- <div style="width: 100px;">--%>
                                                    <!-- BHAVYA-->
                                                    <div style="position: absolute; left: 5px; top: 2px;">
                                                        <strong class="fltLft">
                                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                                        </strong><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"
                                                            class="help spriteIcon toolTipMe"></span>
                                                    </div>
                                                </div>
                                                <!-- BHAVYA-->
                                                <div class="fltLft chdSelTtl">
                                                    <p class="fltLft">
                                                        <strong>
                                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AgeandSelectBedType") %>
                                                        </strong>
                                                    </p>
                                                </div>
                                                <div class="selHdr1">
                                                    <strong>
                                                        <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>--%>
                                                    </strong>
                                                </div>
                                                <!-- BHAVYA-->
                                                <div class="colm5 padTop" id="idRNRoom1Child1Ddl" runat="server" style="display: none;">
                                                    <label class="fltLft">
                                                        1)
                                                    </label>
                                                    <asp:DropDownList ID="ddlRNChildAgeforRoom1Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                        rel="subchildRN114">
                                                    </asp:DropDownList>
                                                    <div class="fltLft bedoutput11">
                                                        <asp:DropDownList ID="ddlRNBedTypeforRoom1Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchildRN114">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="colm5" id="idRNRoom1Child2Ddl" runat="server" style="display: none;">
                                                    <label class="fltLft">
                                                        2)
                                                    </label>
                                                    <asp:DropDownList ID="ddlRNChildAgeforRoom1Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                        rel="subchildRN124">
                                                    </asp:DropDownList>
                                                    <div class="fltLft bedoutput12">
                                                        <asp:DropDownList ID="ddlRNBedTypeforRoom1Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchildRN124">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="colm5" id="idRNRoom1Child3Ddl" runat="server" style="display: none;">
                                                    <label class="fltLft">
                                                        3)
                                                    </label>
                                                    <asp:DropDownList ID="ddlRNChildAgeforRoom1Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                        rel="subchildRN134">
                                                    </asp:DropDownList>
                                                    <div class="fltLft bedoutput13">
                                                        <asp:DropDownList ID="ddlRNBedTypeforRoom1Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchildRN134">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="colm5" id="idRNRoom1Child4Ddl" runat="server" style="display: none;">
                                                    <label class="fltLft">
                                                        4)
                                                    </label>
                                                    <asp:DropDownList ID="ddlRNChildAgeforRoom1Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                        rel="subchildRN144">
                                                    </asp:DropDownList>
                                                    <div class="fltLft bedoutput14">
                                                        <asp:DropDownList ID="ddlRNBedTypeforRoom1Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchildRN144">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="colm5" id="idRNRoom1Child5Ddl" runat="server" style="display: none;">
                                                    <label class="fltLft">
                                                        5)
                                                    </label>
                                                    <asp:DropDownList ID="ddlRNChildAgeforRoom1Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                        rel="subchildRN154">
                                                    </asp:DropDownList>
                                                    <div class="fltLft bedoutput15">
                                                        <asp:DropDownList ID="ddlRNBedTypeforRoom1Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchildRN154">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Bhavya-->
                                        <%--       <div style="display: inline-block; left: 190px; position: relative; top: -137px;"
                                        class="selHdr1">
                                        <strong>
                                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>
                                        </strong>
                                    </div>--%>
                                        <!--Bhavya-->
                                    </div>
                                </div>
                                <%--<div class="roundMe grayBox howManyRoom M05B">
                                <div class="colmMerg fltLft roomOutput">
                                    <div class="colm formColmChn fltLft">
                                        <label for="noRooms">
                                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room2")%>
                                        </label>
                                    </div>                                    
                                    <div class="colm1 widthLs fltLft">
                                        <label for="adult">
                                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults")%>
                                        </label>
                                        <asp:DropDownList ID="ddlRNAdultsPerRoom2" TabIndex="6" runat="server" rel="adult">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="colm1 widthMr fltRt">
                                        <label for="child">
                                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children")%>
                                        </label>
                                        <asp:DropDownList ID="ddlRNChildPerRoom2" TabIndex="6" runat="server" CssClass="child chafltRt"
                                            rel="choutputRN2">
                                        </asp:DropDownList>
                                    </div>                                    
                                    <div class="childSelct choutputRN2">
                                        <div class="bord fltLft roundMe grayBoxHome">                                            
                                                <div style="width: 100px;">
                                                    <strong>
                                                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds")%>
                                                    </strong>
                                                </div>                                            
                                            <div class="colm5 padTop">
                                                <label class="fltLft">
                                                    1)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom2Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild214">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput21">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom2Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild214">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5">
                                                <label class="fltLft">
                                                    2)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom2Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild224">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput22">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom2Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild224">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5">
                                                <label class="fltLft">
                                                    3)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom2Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild234">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput23">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom2Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild234">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5">
                                                <label class="fltLft">
                                                    4)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom2Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild244">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput24">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom2Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild244">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5">
                                                <label class="fltLft">
                                                    5)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom2Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild254">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput25">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom2Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchild254">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="roundMe grayBox howManyRoom M05B">
                                <div class="colmMerg fltLft roomOutput">
                                    <div class="colm formColmChn fltLft">
                                        <label for="noRooms">
                                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room3")%>
                                        </label>
                                    </div>                                        
                                    <div class="colm1 widthLs fltLft">
                                        <label for="adult">
                                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults")%>
                                        </label>
                                        <asp:DropDownList ID="ddlRNAdultsPerRoom3" TabIndex="6" runat="server" rel="adult">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="colm1 widthMr fltRt">
                                        <label for="child">
                                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children")%>
                                        </label>
                                        <asp:DropDownList ID="ddlRNChildPerRoom3" TabIndex="6" runat="server" CssClass="child chafltRt"
                                            rel="choutputRN3">
                                        </asp:DropDownList>
                                    </div>                                    
                                    <div class="childSelct choutputRN3">
                                        <div class="bord fltLft roundMe grayBoxStay">                                            
                                            <div style="width: 100px;">
                                                <strong>
                                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds")%>
                                                </strong>
                                            </div>                                            
                                            <div class="colm5 padTop">
                                                <label class="fltLft">
                                                    1)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom3Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild314">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput31">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom3Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild314">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5">
                                                <label class="fltLft">
                                                    2)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom3Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild324">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput32">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom3Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild324">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5">
                                                <label class="fltLft">
                                                    3)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom3Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild334">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput33">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom3Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild334">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5">
                                                <label class="fltLft">
                                                    4)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom3Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild344">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput34">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom3Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild344">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5">
                                                <label class="fltLft">
                                                    5)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom3Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild354">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput35">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom3Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchild354">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="roundMe grayBox howManyRoom M05B">
                                <div class="colmMerg fltLft roomOutput">
                                    <div class="colm formColmChn fltLft">
                                        <label for="noRooms">
                                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room4")%>
                                        </label>
                                    </div>                                    
                                    <div class="colm1 widthLs fltLft">
                                        <label for="adult">
                                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults")%>
                                        </label>
                                        <asp:DropDownList ID="ddlRNAdultsPerRoom4" TabIndex="6" runat="server" rel="adult">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="colm1 widthMr fltRt">
                                        <label for="child">
                                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children")%>
                                        </label>
                                        <asp:DropDownList ID="ddlRNChildPerRoom4" TabIndex="6" runat="server" CssClass="child chafltRt"
                                            rel="choutputRN4">
                                        </asp:DropDownList>
                                    </div>                                    
                                    <div class="childSelct choutputRN4">
                                        <div class="bord fltLft roundMe grayBoxStay">                                            
                                            <div style="width: 100px;">
                                                <strong>
                                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds")%>
                                                </strong>
                                            </div>                                            
                                            <div class="colm5 padTop">
                                                <label class="fltLft">
                                                    1)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom4Child1" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild414">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput41">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom4Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild414">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5">
                                                <label class="fltLft">
                                                    2)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom4Child2" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild424">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput42">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom4Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild424">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5">
                                                <label class="fltLft">
                                                    3)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom4Child3" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild434">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput43">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom4Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild434">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5">
                                                <label class="fltLft">
                                                    4)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom4Child4" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild444">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput44">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom4Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild444">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="colm5">
                                                <label class="fltLft">
                                                    5)
                                                </label>
                                                <asp:DropDownList ID="ddlRNChildAgeforRoom4Child5" TabIndex="6" runat="server" CssClass="fltLft subchild"
                                                    rel="subchild454">
                                                </asp:DropDownList>
                                                <div class="fltLft bedoutput45">
                                                    <asp:DropDownList ID="ddlRNBedTypeforRoom4Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchild454">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                            </div>
                            <br class="clearAll" />
                            <div class="ftButtons" style="margin-top: 10px;">
                                <div class="actionBtn fltRt">
                                    <asp:LinkButton ID="btnRewardNightSearch" runat="server" CssClass="buttonInner" OnClick="btnSearch_Click"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Search") %></asp:LinkButton>
                                    <asp:LinkButton ID="spnRewardNightSearch" runat="server" CssClass="buttonRt scansprite"
                                        OnClick="btnSearch_Click"></asp:LinkButton>
                                </div>
                                <br class="clear" />
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
            <!-- Accordian content Claim rewards ends here -->
            <!-- Accordian content Cancel booking starts here -->
            <div class="expand">
                <div class="hdDeflt scansprite" id="Tab4">
                    <div class="hdRtDeflt scansprite">
                        <% if (Utility.GetCurrentLanguage().Equals("ru-RU") || Utility.CheckifFallback())
                       {%>
                        <h3 class="stoolHeadingAlternate">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ModifyOrCancelBooking") %>
                        </h3>
                        <% }
                       else
                       {%>
                        <h3 class="stoolHeading">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ModifyOrCancelBooking") %>
                        </h3>
                        <% } %>
                        <%-- <h3 class="stoolHeading">
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ModifyOrCancelBooking")%>
                        </h3>--%>
                    </div>
                </div>
                <div class="mainContent cancelInfo" style="display: none;">
                    <div id="clientErrorDivBD" class="errorText" runat="server">
                    </div>
                    <p class="pad20 marB20">
                        <label id="lblModifyCancelInfo" runat="server">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ModifyCancelInfo") %>
                        </label>
                    </p>
                    <div class="roundMe grayBox claimBox" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnFetchBooking.ClientID %>')">
                        <label class="fltLft">
                            <span class="fltLft" style="width: auto;">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/PleaseEnterDetails") %>
                                <span title=" <%= WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/CancelModifyBookingToolTip") %>"
                                    class="help spriteIcon toolTipMe" style="width: auto; display: inline-block;
                                    float: none; top: 3px;"></span></span>
                        </label>
                        <div class="colmLft">
                            <input type="text" name="member" id="txtBookingNumber" class="input defaultColor"
                                rel="Reservation Number" runat="server" />
                            <input type="text" name="pass" id="txtSurname" class="input defaultColor" rel="Last name"
                                maxlength="40" runat="server" />
                        </div>
                        <br class="clear" />
                    </div>
                    <div class="whitBoxRt fltRt">
                        <div class="actionBtn fltRt">
                            <asp:LinkButton ID="btnFetchBooking" CssClass="buttonInner" OnClick="btnFetchBooking_Click"
                                runat="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/Search") %>
                            </asp:LinkButton>
                            <asp:LinkButton ID="spnFetchBooking" CssClass="buttonRt scansprite" OnClick="btnFetchBooking_Click"
                                runat="server"></asp:LinkButton>
                        </div>
                        <div class="clear">
                        </div>
                        <p class="marT18">
                            <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ContactCustomerService")%>--%>
                            <label id="lblContactCustomerService" runat="server">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ContactCustomerService") %>
                            </label>
                        </p>
                        <div class="gradientft sprite">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Accordian content Cancel booking ends here -->
        </div>
    </div>
</div>
<div id="destinationSearch">
    <uc1:DestinationSearch id="DestinationSearch1" runat="server" />
</div>
<div id="calendar">
    <uc2:Calendar id="Calendar1" runat="server" />
</div>
<% string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();%>
<div id="clientErrorDiv" runat="server">
</div>
<div class="errorDivClient">
    <input type="hidden" id="siteLang" name="siteLang" value="<%= siteLanguage %>" />
    <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
    <input type="hidden" id="destinationError" name="destinationError" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name") %>' />
    <input type="hidden" id="duplicateDestinationError" name="duplicateDestinationError"
        value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/duplicate_destination") %>' />
    <input type="hidden" id="arrivaldateError" name="arrivaldateError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/arrival_date") %>' />
    <input type="hidden" id="departuredateError" name="departuredateError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/departure_date") %>' />
    <input type="hidden" id="NoOfNightError" name="NoOfNightError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/no_of_nights") %>' />
    <input type="hidden" id="invalidGuestNumberError" name="invalidGuestNumberError"
        value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/user_name") %>' />
    <input type="hidden" id="invalidGuestPINError" name="invalidGuestPINError" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/password") %>' />
    <input type="hidden" id="invalidGuestLoginError" name="invalidGuestLoginError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/user_name_password") %>' />
    <input type="hidden" id="invalidSpecialCodeError" name="invalidSpecialCodeError"
        value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/special_booking_code") %>' />
    <input type="hidden" id="invalidNegotiatedCode" name="invalidNegotiatedCode" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/negotiated_rate_code") %>' />
    <input type="hidden" id="invalidVoucherCode" name="invalidVoucherCode" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/voucher_booking_code") %>' />
    <input type="hidden" id="specialCodeToDNumber" name="specialCodeToDNumber" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/invalidSpecialCode") %>' />
    <input type="hidden" id="specialCodeToTravelAgent" name="specialCodeToTravelAgent"
        value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/businesserror/invalidSpecialCodeTravelAgent") %>' />
    <!--Rel 2.2.7: AMS Patch 4| artf1216848 : Scanweb - Incorrect error when searching a destination that is not choosen from list 
    <input type="hidden" id="customerMessage" name="customerMessage" runat="server" />    -->
    <input type="hidden" id="invalidBookingNumber" name="invalidBookingNumber" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidBookingNumber") %>' />
    <input type="hidden" id="invalidSurname" name="invalidSurname" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidSurname") %>' />
    <!-- searchMode value can be changed based on the login -->
    <input type="hidden" id="guestLogin" value="false" runat="server" />
    <input type="hidden" id="sT" runat="server" />
    <input type="hidden" id="selectedDestId" runat="server" />
    <input type="hidden" id="loginSrchID" runat="server" />
    <input type="hidden" id="loginSrchPageID" runat="server" />
    <input id="childrenCriteriaHidden" type="hidden" class="hiddenTxt" runat="server" />
    <input id="bedTypeCollection" type="hidden" class="hiddenTxt" runat="server" />
    <input type="hidden" id="childAgeNotSelectedError" name="childrenAgeNotSelectedError"
        value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidAge") %>" />
    <input type="hidden" id="ChildBedTypeNotSelectedError" name="ChildBedTypeNotSelectedError"
        value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidBedType") %>" />
    <input type="hidden" id="childrenAgeBedTypeError" name="childrenAgeBedTypeError"
        value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name") %>' />
    <input type="hidden" id="childInAdultBedandAdultCountError" name="childInAdultBedandAdultCountError"
        value="<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalidbedtypechoosen") %>" />
    <input type="hidden" id="childInAdultsBedType" name="childInAdultsBedType" value="<%=
                WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/sharingbed") %>" />
    <input type="hidden" id="hdnlastNameTranslated" name="hdnlastNameTranslated" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/Surname") %>" />
    <input type="hidden" id="invalidReservationNumber" name="invalidReservationNumber"
        runat="server" value="false" />
    <!--artf1150499 | Select age doesn't fit in small booking module | Rajneesh -->
    <input type="hidden" id="isBookingModuleSmall" runat="server" />
    <!--Priya Singh: Error msg displayed when invalid block code entered.
    Artifact artf1217694 : Scanweb - Input control missing for incorrect block code  -->
    <input type="hidden" id="invalidBlockCode" name="invalidBlockCode" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/InvalidBlockCode") %>" />
    <!--Boxi : Added input control for special charecter validation in Booking Code field -->
    <input type="hidden" id="invalidBlockCodeChars" name="invalidBlockCodeChars" value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/noSpecialCaracters") %>" />
    <input type="hidden" id="defaultBookingCode" name="defaultBookingCode" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingCode") %>" />
    <input type="hidden" id="BookingCodeIata" name="BookingCodeIata" />
    <input type="hidden" id="IATANumber" name="IATANumber" />
    <input type="hidden" id="InvalidIATAnumber" name="InvalidIATAnumber" value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/IataSpecialCaracters") %>" />
</div>

<script type="text/javascript" language="javascript">

    var requestUrl = <%= "\"" + GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") + "\"" %>;
    var selectedAccordian = $fn(_endsWith("sT")).value;
    ShowAppropriateAccordian('#yourStayMod05',$fn(_endsWith("sT")).value);
    initBMS();
    $fn(_endsWith("sT")).value = selectedAccordian;
    associateControlBasedOnAccordianSelected();
    scandic.agSelct(); 

var errorInSettingBedTypeRGB = "<%=errorInBedTypeSelectionInRGB%>";
var errorInSettingBedTypeBCB = "<%=errorInBedTypeSelectionInBCB%>";
var errorInSettingBedTypeRNB = "<%=errorInBedTypeSelectionInRNB%>";
var errorPriorDateTypeRGB = "<%=errorPriorDateInRGB%>";
var errorPriorDateTypeBCB = "<%=errorPriorDateInBCB%>";
var errorPriorDateTypeRNB = "<%=errorPriorDateInRNB%>";

 
$(function(){
    if(errorInSettingBedTypeRGB == "True"){   
   
       // Get the No of Rooms
       var noOfRoom = $('#tabContainer1 .roomSelect').find(":selected").val();

       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlChildPerRoom1")).id).find(":selected").val();
       var noOfChild2 = $("#"+ $fn(_endsWith("ddlChildPerRoom2")).id).find(":selected").val();
       var noOfChild3 = $("#"+ $fn(_endsWith("ddlChildPerRoom3")).id).find(":selected").val();
       var noOfChild4 = $("#"+ $fn(_endsWith("ddlChildPerRoom4")).id).find(":selected").val();         
       selectBedTypeError(noOfRoom, noOfChild1, noOfChild2, noOfChild3, noOfChild4, "choutput", "DFT");
    }
    else if(errorInSettingBedTypeBCB == "True"){
       // Get the No of Rooms
       var noOfRoom = $('#tabContainer2 .roomSelect').find(":selected").val();

       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom1")).id).find(":selected").val();
       var noOfChild2 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom2")).id).find(":selected").val();
       var noOfChild3 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom3")).id).find(":selected").val();
       var noOfChild4 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom4")).id).find(":selected").val();         
       selectBedTypeError(noOfRoom, noOfChild1, noOfChild2, noOfChild3, noOfChild4, "choutputBC", "DFT");
    }
    else if(errorInSettingBedTypeRNB == "True"){
  // Get the No of Rooms
       var noOfRoom = $('#tabContainer3 .roomSelect').find(":selected").val();

       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlRNChildPerRoom1")).id).find(":selected").val();
               
       selectBedTypeError(noOfRoom, noOfChild1, "", "", "", "choutputRN", "DFT");
    }	
	
	if(errorPriorDateTypeRGB == "True"){   
   
       // Get the No of Rooms
       var noOfRoom = $('#tabContainer1 .roomSelect').find(":selected").val();

       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlChildPerRoom1")).id).find(":selected").val();
       var noOfChild2 = $("#"+ $fn(_endsWith("ddlChildPerRoom2")).id).find(":selected").val();
       var noOfChild3 = $("#"+ $fn(_endsWith("ddlChildPerRoom3")).id).find(":selected").val();
       var noOfChild4 = $("#"+ $fn(_endsWith("ddlChildPerRoom4")).id).find(":selected").val();         
       setChildrenDetails(noOfRoom, noOfChild1, noOfChild2, noOfChild3, noOfChild4, "choutput", "DFT");
    }
    else if(errorPriorDateTypeBCB == "True"){
       // Get the No of Rooms
       var noOfRoom = $('#tabContainer2 .roomSelect').find(":selected").val();

       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom1")).id).find(":selected").val();
       var noOfChild2 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom2")).id).find(":selected").val();
       var noOfChild3 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom3")).id).find(":selected").val();
       var noOfChild4 = $("#"+ $fn(_endsWith("ddlBCChildPerRoom4")).id).find(":selected").val();         
       setChildrenDetails(noOfRoom, noOfChild1, noOfChild2, noOfChild3, noOfChild4, "choutputBC", "DFT");
    }
    else if(errorPriorDateTypeRNB == "True"){
  // Get the No of Rooms
       var noOfRoom = $('#tabContainer3 .roomSelect').find(":selected").val();

       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlRNChildPerRoom1")).id).find(":selected").val();
               
       setChildrenDetails(noOfRoom, noOfChild1, "", "", "", "choutputRN", "DFT");
    }
});
     
    $(document).ready(function() {
        appendPrefix("txtUserNamePrefix");
        $("input[id$='txtUserName']").bind('focusout',TrimWhiteSpaceSmall);
	 });




function TrimWhiteSpaceSmall() {

	    trimWhiteSpaces("txtUserName");
        replaceDuplicatePrefix("txtUserName");
	}
     
</script>

