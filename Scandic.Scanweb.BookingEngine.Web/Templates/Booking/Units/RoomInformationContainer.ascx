    <%@ Control Language="C#" AutoEventWireup="true" Codebehind="RoomInformationContainer.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.RoomInformationContainer" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%--<div class="<%=TopCss%>">--%>
<div class="bookingDetail">
<input type="hidden" id="previousCardNumber" name="orginalCardNumber" runat="server" />
    <div class="greyRd">        
        <div class="cnt bookingDetailBorder" id="roomInfoDiv" runat="server">
            <div class="guestInfo">
                <h3><%= RoomHeader %></h3>
                <p><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/MandatoryFieldsInfoMessage") %></p>
                <div id="Room1">
                <label id="roomInfoText" runat="server" visible="false"></label>
                    <div class="guestcol">
                           <div runat="server" id="divNameTitle" class="clear">                                 
                                <p>
                                    <label for="<%= ddlTitle.ClientID %>" class="formLabel"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nametitle") %></label>
                                     <asp:DropDownList CssClass="frmTitleSelect" ID="ddlTitle" runat="server" />
                                </p>
                            </div>
                            <p>
                                <label for="<%= txtFNameRoom.ClientID %>" class="formLabel"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPFName") %> <span class="importantMark">&#42;</span></label>
                                <span id="lblRoomFName">
                                    <label id="lblRoomFNameHdr" runat="server" visible="false"></label>
                                </span>
                                <input type="text" id="txtFNameRoom" runat="server" name="Fname" maxlength="30" class="text-input defaultColor distinctusername" />
                                <strong><span id="lblFNameRoom" visible="false" runat="server"></span></strong>
                            </p>
                            <p>
                                <label for="<%= txtLNameRoom.ClientID %>" class="formLabel"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPLName") %> <span class="importantMark">&#42;</span></label>
                                <span id="lblRoomLName">
                                    <label id="lblRoomLNameHdr" runat="server" visible="false"></label>
                                </span>
                                <input type="text" name="txtLname"  id="txtLNameRoom" runat="server" maxlength="30" class="text-input defaultColor  distinctusername " />
                                <strong><span id="lblLNameRoom" visible="false" runat="server"></span></strong>
                            </p>   
                            <p class="fltLft clboth emailfield">
                                <label for="<%= txtEmail.ClientID %>" class="formLabel"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPEmail") %> <span class="importantMark">&#42;</span><a tabindex="-1" class="help spriteIcon toolTipMe fltLft" title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPEmailToolTip") %>" href="#"></a></label>
                                <input type="text" name="txtEmailRoom" id="txtEmail" runat="server"  class="text-input defaultColor" />
                                
                            </p>                         
                            <p>
                                <label for="<%= ddlCountry.ClientID %>" class="formLabel"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/countryname") %> <span class="importantMark">&#42;</span></label>
                                <asp:DropDownList CssClass="frmSelectBig frmSelectBignew" ID="ddlCountry" runat="server">
                                </asp:DropDownList>
                            </p>
                           
                            <p class="extrWidthBxMember fltLft">
                                <span class="formFieldCol" style="width:100%;">
                                    <label for="<%= ddlMobileNumber.ClientID %>" class="formLabel"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/CountryCode") %> <span class="importantMark">&#42;</span></label>
                                    <asp:DropDownList CssClass="frmSelect frmSelectBignew" ID="ddlMobileNumber" runat="server">
                                    </asp:DropDownList>
                                </span>
                             </p>
                             <p class="extrWidthBxMember fltLft"> 
                                <span class="formFieldCol" style="width:100%;">
                                    <label for="<%= txtTelephone.ClientID %>" class="formLabel"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/PrimaryPhNo") %> <span class="importantMark">&#42;</span></label>
                                    <input type="text" id="txtTelephone" name="txtTelephone" runat="server" maxlength="20" class="text-input defaultColor tphone" />
                                    
                                </span>
                            </p>
                            <p class="extrWidthBxMember fltLft">
                                 <input type="checkbox" class="frmInputChkBox smsbox" tabindex="340" id="SMSconfirmation" name="SMSconfirmation"  runat = "server" />
                                 <label><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/smsconfirmation")%></label>
                                    
                            </p>
                            <p class="extrWidthBxMember fltLft" id="memNo">
                                <label for="<%= txtMembershipNumberRoom.ClientID %>" class="formLabel"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/MemFrqGstPrg") %><a href="#" tabindex="-1" title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/MemFrqGstPrgToolTip") %>" class="help spriteIcon toolTipMe fltLft"></a></label>
                                <!--Defect fix - 487882 - Bhavya - same fgp number validation-->
                                <input type="text" id="txtMembershipNumberRoom" maxlength="20" name="mem_no" runat="server"/>
                                
                            </p>
                            <p class="extrWidthBxMember fltLft">
                                 <label for="bedtype" class="formLabel"> <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/bedtypepreference") %> <span class="importantMark">&#42;</span><a href="#" tabindex="-1" title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/BedtypeTooltip") %>" class="help spriteIcon toolTipMe fltLft"></a></label>                             
                                <asp:DropDownList CssClass="frmSelectBig frmSelectBignew selBedType" ID="ddlBedTypePreference"  runat="server">
                                </asp:DropDownList>                             
                            </p>
                            <br class="clear" />
                            <div class="memberNo fltLft">
                                <!-- R2.0 | Bhavya | Connect Bonuscheque Bookings with DNumber -->
                                <div id ="divDNumber" runat="server" visible="false" style="*margin-top:7px;">
                                <span id="lblD_Number">
                                    <label class="formLabel"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/DNumber") %></label>
                            
                                </span>
                                <input type="text" name="txtD_Number" class="frmInputText text-input" id="txtD_Number" runat="server" maxlength="200" />
                                 </div>
                            <!-- R1.3 | Bhavya | Connect Bonuscheque Bookings with DNumber ends here --> 
                            <div class="roomDetails room inSideContainer">
                                <h3 class="splreqirment"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/specialrequirements")%></h3>
                                                                
                                <!-- Start of new code -->
                                <h2 class="trigger" runat="server" id="otherRequestsHeading">
                                
                                    
                                  <a runat="server" id="spnAddSpecialrequests" style="cursor:pointer" class="secure"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/OtherReq")%> - </a>
                                    
                                    <%--<span runat="server" id="spnAddSpecialrequests" name="spnAddSpecialrequests" class="secure scansprite"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/OtherReq")%> - </span>--%>
                                    <span><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/ShowAndHideOption")%></span>
                                    <%--<span class="showMe"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/Show")%></span>
                                    <span class="hideMe"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/Hide")%></span>--%>
                                </h2>
                                <ul class="roomPref" id="roomPref" runat="server">
                                    <li>
                                        
                                    </li>
                                </ul>
                                <div class="otherRequest" id="otherRequestContents" runat="server">
                                    <div class="noroundMe">
                                        <h4><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/RoomLocation")%></h4>
                                        <ul class="bedTypeList">
                                            <li>                                             
                                                <span><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/roomfloor")%></span>
                                                <asp:DropDownList ID="ddlRoomFloor" runat="server"></asp:DropDownList>  
                                                <%--<select>
                                                <option><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nopreference")%></option>
                                                <option><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/lowerfloor")%></option>
                                                <option><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/highfloor")%></option>
                                                </select>--%>
                                            </li>
                                            <li>
                                                <span><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/elevator")%></span>
                                                <asp:DropDownList ID="ddlElevator" runat="server"></asp:DropDownList>  
                                               <%-- <select>
                                                <option><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nopreference")%></option>
                                                <option><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/awayfromelevator")%></option>
                                                <option><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nearelevator")%></option>
                                                </select>--%>
                                            </li>
                                            <li>
                                                <span><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SmokingNSmoking")%></span>
                                                <asp:DropDownList ID="ddlSmoking" runat="server"></asp:DropDownList>  
                                                <%--<select>
                                                <option><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nopreference")%></option>
                                                <option><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nosmoking")%></option>
                                                <option><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/smoking")%></option>
                                                </select>--%>
                                            </li>
                                        </ul>
                                    </div>
                                     <div class="noroundMe">
                                        <h4><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SplNeed")%></h4>
                                        <ul class="bedTypeList otherRequestList">
                                            <li>
                                                <input type="checkbox" name="AccessableRoom" id="chkAccessableRoom" class="chkBx fltLft" runat="server" /><span class="fltLft"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/accessableroom") %></span>
                                                <div class="additional-info">	
	                                                <span class="additional-info-txt"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/accessableroomhelptext") %></span>
                                                </div>                                                
                                            </li>
                                            <li>
                                                <input type="checkbox" name="AllergyRooms" class="chkBx fltLft" id="chkAllergyRooms" runat="server" />
                                                <span class="fltLft"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/allergyroom") %></span>
                                                <div class="additional-info">	
	                                                <span class="additional-info-txt"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/allergyroomhelptext") %></span>
                                                </div>                                                  
                                            </li>
                                            <li style="width: 300px;">
                                                <input type="checkbox" name="Petfriendlyroom" runat="server" id="ChkPetfriendlyroom" class="chkBx fltLft" /><span class="fltLft"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/PetFriendlyRoom") %></span>
                                                <div class="additional-info">	
	                                                <span class="additional-info-txt"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/PetFriendlyRoomhelptext") %></span>
                                                </div>                                                
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="noroundMe">
                                        <h4><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/specialservicerequests")%></h4>
                                        <ul class="bedTypeList otherRequestList specialService">
                                            <li>
                                                <input type="checkbox" name="EarlyCheckIn" id="chkEarlyCheckIn" runat="server" class="chkBx fltLft" />
                                                <span class="fltLft"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/earlycheckin") %></span>
                                                <div class="additional-info">	
	                                                <span class="additional-info-txt"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/earlycheckinhelptext") %></span>
                                                </div>                                                
                                            </li>
                                            <li style="width: 260px;">
                                                <input type="checkbox" name="LateCheckOut" class="chkBx fltLft" id="chkLateCheckOut" runat="server" />
                                                <span class="fltLft"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/latecheckout") %></span>
                                                <div class="additional-info">	
	                                                <span class="additional-info-txt"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/latecheckouthelptext") %></span>
                                                </div> 
                                                
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="noroundMe">
                                        <h4><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/AdditionalReq")%></h4>
                                        <textarea name="addReq" id="additionalRequest" runat="server" maxlength="255" class="textareaclassbd"></textarea>
                                    </div>
                                </div>
                                <!-- end of otherRequest -->
                            </div>
                            <!-- end of RoomDetails room inSideContainer -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start from Here Gaurantee Info -->
            <div class="depositInfo reterm fltLft" id="HideUnhideGuranteeDiv" runat="server" visible="false">
                <h3><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteedeposite") %></h3>
                <!--Vrushali | fix for artf1180532 : Design issue -->
                <asp:Label ID="guaranteeDepositAppendForComboReservation" runat="server"></asp:Label>
                <p class="gType" id="blkHoldRoom" runat="server" visible="false">
                        <!-- <input type="radio" id="rdoHoldRoom" runat="server" checked="true" tabindex="111" name="depinfo" value="holdOn" class="radio chkEnroll" />-->
                        <span id="spanHoldRoom" runat="server"></span>
                        <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/gaurenteediv")%>--%>
                        <%--<a href="#" title="Hold my booking until 6PM on the day of check in" class="help spriteIcon toolTipMe">--%>
                    
                </p>
                <p class="gType fieldOption" id="lblGuranteewithRadio" runat="server">
                        <input type="checkbox" name="depinfo" value="creditInfo" class="radio chkEnroll" tabindex="112" id="rdoLateArrivalGurantee" runat="server" />
                        <span id="txtGuranteeWithRadio" class="lblGuarantee" runat="server"></span>
                        <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/gaurenteeCreditCard")%>--%>
                        <%--<a href="#" title="Guarantee my booking" class="help spriteIcon toolTipMe"></a>--%>
                </p>
                <p>
                    <label id="lblGuranteewithoutRadio" visible="false" runat="server">
                        <span id="txtGuranteeWithoutRadio" runat="server"></span>
                    </label>
                   
                    
                </p>    
                <div class="ccInfoDetails fltLft" id="HideUnhideCreditCardDiv" runat="server">
                 <label id="lblGuranteewithRadioCCDivInfo" visible = "true" runat="server">
                        <span><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/cancellationexpand") %></span> <br/><br/>
                 </label>
                    <div class="ccInfo">
                        <span class="formFieldCol">
                            <label class="formLabel" for="<%=txtCardHolder.ClientID %>"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/NameOnCard")%> <span class="importantMark">&#42;</span></label>
                            <!--AMS Patch7:artf1263082 : Scanweb - Creditcard information not included in printer friendly and PDF confirmation-->
                            <input type="text" rel="Card holder" class="defaultColor" name="cc_name" id="txtCardHolder" maxlength="30" runat="server" />
                        </span>
                        <span class="formFieldCol">
                            <label class="formLabel" for="<%=ddlCardType.ClientID %>"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/cardtype")%> <span class="importantMark">&#42;</span></label>
                            <asp:DropDownList ID="ddlCardType" runat="server">
                            </asp:DropDownList>
                        </span>
                        <span class="formFieldCol">
                            <label class="formLabel" for="<%=txtCardNumber.ClientID %>"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/NumberOnCard") %> <span class="importantMark">&#42;</span></label>
                            <input type="text"  rel="Credit card number" class="defaultColor" name="cc_num" id="txtCardNumber" runat="server" maxlength="20"/>
                        </span>
                    </div>
                    <div class="ccDate fltLft">
                        <div class="formFld fltLft">
                            <label class="fltLft formLabel" for="<%=ddlExpiryMonth.ClientID %>">
                                <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/CardExpDate")%> <span class="importantMark">&#42;</span>
                            </label>
                             <span class="formFieldCol">
                            <asp:DropDownList ID="ddlExpiryMonth"  runat="server">
                            </asp:DropDownList>
                            </span>
                             <span class="formFieldCol">
                            <asp:DropDownList ID="ddlExpiryYear"  runat="server">
                            </asp:DropDownList>
                            </span>
                        </div>
                        <img height="57" width="114" alt="" class="fltRt ccimgclas" src="<%=WebUtil.GetLogoPath()%>" />
                    </div>
                    <div class="clearAll"><!-- Clearing Float --></div>
                     <!-- R2.2.5 Credit Card Info -->
            		<div class="ccExtraInfoWrapper">
                        <div class="ccExtraInfo">
	                        <div class="content"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Dankort") %></div>
                        </div>
                    </div>
                    <div class="clearAll"><!-- Clearing Float --></div>
                    <%--//RK: Reservation 2.0 | ID#465382 | show the update check box for updating credit card details--%>
                    <div id="divCreditCardUpdate" runat="server" visible="false">
                        <p>
                        <%--//R2.2 | artf1167496 | Box around box on confirmation page --%>
                            <input type="checkbox" id="updateCreditCard" runat="server"  class ="noBorder"/>
                            <label><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/UseSameCard")%></label>
                        </p>
                    </div>
                </div>
                
                 <div class="ccInfoDetails fltLft" id="panHashCreditCardDiv" runat="server">
                    <div class="ccInfo">
                        <div class="formFld fltLft">
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/PanhashCCLabel") %> <span class="importantMark">&#42;</span><span class="formFieldCol">
                                <asp:DropDownList ID="ddlPanHashCreditCards" runat="server"  CssClass="panHashDropDown">
                                </asp:DropDownList>
                            </span>
                        </div>
                        <img height="57" width="114" alt="" class="fltRt ccimgclas" src="<%=WebUtil.GetLogoPath()%>" />
                    </div>
                    <div class="clearAll">
                        <!-- Clearing Float -->
                    </div>
                    <!-- R2.2.5 Credit Card Info -->
                    <div class="ccExtraInfoWrapper">
                        <div class="ccExtraInfo">
                            <div class="content">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Dankort") %></div>
                        </div>
                    </div>
                    <div class="clearAll">
                        <!-- Clearing Float -->
                    </div>
                    <div id="divCreditCardUpdatePanHash" runat="server" visible="false">
                        <p>
                            <input type="checkbox" id="updateCreditCardPanHash" runat="server" class="noBorder" />
                            <label>
                                <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/UseSameCard")%></label>
                        </p>
                    </div>
                </div>
                
            </div>
            <div class="clear"></div>
        </div>        
    </div>
</div>
<%--//RK: Reservation 2.0 | ID#465382 | Method to update credit card details--%>
<script type="text/javascript">
    updateCreditCardDetails();
    updateCreditCardPanHashDetails();

    $('.otherRequest textarea').keyup(function() {

        var max = parseInt($(this).attr('maxlength'));

        if ($(this).val().length > max) {
            $(this).val($(this).val().substr(0, max));
            //$(this).val().substr(0,max)+'...';

        }

        // $(this).parent().find('.roundMe').html('You have ' + (max - $(this).val().length) + ' characters remaining');
    });


    var fgpfield = $fn(_endsWith("txtMembershipNumberRoom"));
    $(fgpfield).bind('keyup', function() {
        var fgpdiv = $fn(_endsWith("lblFGPDiv"));
        var fgpvalue = $fn(_endsWith("txtMembershipNumberRoom")).value;
        if (fgpvalue == "") {
            fgpdiv.style.display = 'block';
        } else {
            fgpdiv.style.display = 'none';
        }
    });

    $(document).ready(function() {
       // appendPrefix("txtMembershipNumberRoom");
      $("input[id$='txtMembershipNumberRoom']").bind('focusout', TrimWhiteSpace);
    });

    function TrimWhiteSpace() {

        trimWhiteSpaces("txtMembershipNumberRoom");

    }

//    $("input[id$='rdoLateArrivalGurantee']").bind("change", function() {
//        if (this.checked) {
//            $("div[id$='HideUnhideCreditCardDiv']").show();
//        } else {
//            $("div[id$='HideUnhideCreditCardDiv']").hide();
//        }

    //});
    
    
    
</script>
