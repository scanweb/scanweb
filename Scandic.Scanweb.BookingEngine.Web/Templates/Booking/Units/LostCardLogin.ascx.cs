////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: Lost card with login user control code behind   		  //
//                                User request for lost card after login in from this     //
//                                Screen.                                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Raj Kishore Marandi	                                  //
//  Author email id				:                           							  //
//  Creation Date				: 6th December 2007									      //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
// Revision History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code behind of LostCardLogin control.
    /// </summary>
    public partial class LostCardLogin : EPiServer.UserControlBase
    {
        #region Protected Method

        #region Page_Load

        /// <summary>
        /// Fired when page is loaded.
        /// Responsible for setting labels of lost card with login user control.
        /// Responsible for setting labels of lost card confirmation user control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible && WebUtil.IsSessionValid())
            {
                if (UserLoggedInSessionWrapper.UserLoggedIn == true)
                {
                    if (!IsPostBack)
                    {
                        SetLabels();
                        SetLostCardEmailConfirmationUserControl();
                        LCWLEmailConfirmation.Visible = false;
                    }
                }
                else
                {
                    AppLogger.LogInfoMessage(
                        "User is already logged out - Still accessing Lost Card Login Functionality.");
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.LOGIN_FOR_DEEPLINK), false);
                }
            }
        }

        #endregion Page_Load

        #region ReportButton_Click

        /// <summary>
        /// This method is fired when report button is clicked from UI.
        /// Responsible for Sending Email,hiding lost card user control and displaying lost card confirmation user control on successful sending of Email.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReportButton_Click(object sender, EventArgs e)
        {
            try
            {
                SendMailToUser();
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
            try
            {
                SendMail();
                GuestLostCard.Visible = false;
                LCWLEmailConfirmation.Visible = true;
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
        }

        #endregion ReportButton_Click

        #endregion Protected Method

        #region Private Method

        #region SetLabels

        /// <summary>
        /// Method to Set the labels of lost card with login User control.
        /// </summary>
        private void SetLabels()
        {
            LoyaltyDetailsEntity loyaltyDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails as LoyaltyDetailsEntity;
            if (loyaltyDetails != null)
            {
                txtMembershipNumber.Value = loyaltyDetails.UserName;
                if (loyaltyDetails.AddressLine1 != null && loyaltyDetails.AddressLine2 != null)
                {
                    spanStreet.InnerText = loyaltyDetails.AddressLine1 + "," + loyaltyDetails.AddressLine2;
                }
                else if (loyaltyDetails.AddressLine1 == null && loyaltyDetails.AddressLine2 == null)
                {
                    spanStreet.InnerText = "";
                }
                else
                {
                    spanStreet.InnerText = loyaltyDetails.AddressLine1 + loyaltyDetails.AddressLine2;
                }
                spanPostCode.InnerText = loyaltyDetails.PostCode;
                spanCity.InnerText = loyaltyDetails.City;

                string countryName = string.Empty;
                if (loyaltyDetails.Country != null)
                {
                    countryName = Utility.GetCountryName(loyaltyDetails.Country);
                }
                spanCountry.InnerText = countryName;
                if (loyaltyDetails.Telephone1 != null)
                {
                    string formatedPhone = Utility.FormatePhoneToDisplay(loyaltyDetails.Telephone1);
                    spanTelephone.InnerText = formatedPhone;
                }
            }
        }

        #endregion SetLabels

        #region SetLostCardEmailConfirmationUserControl

        /// <summary>
        /// This Method is responsible for setting labels of lost card confirmation user control.
        /// </summary>
        private void SetLostCardEmailConfirmationUserControl()
        {
            LCWLEmailConfirmation.AssignHeader(
                WebUtil.GetTranslatedText(
                    "/bookingengine/booking/lostcardwithoutlogin/lostcardwithoutloginmessageheader"));
            LCWLEmailConfirmation.AssignMessage(
                WebUtil.GetTranslatedText(
                    "/bookingengine/booking/lostcardwithoutlogin/lostcardwithoutloginmessgaedescription"));
            LCWLEmailConfirmation.AssignLink(
                WebUtil.GetTranslatedText(
                    "/bookingengine/booking/lostcardwithoutlogin/lostcardwithoutloginspecialoffers"));
        }

        #endregion SetLostCardEmailConfirmationUserControl

        #region SendMail

        /// <summary>
        /// Method Responsible for sending the email message to Scandic.
        /// </summary>
        private void SendMail()
        {
            try
            {
                LoyaltyDetailsEntity loyaltyInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails as LoyaltyDetailsEntity;
                Dictionary<string, string> emailConfirmationMap = GetLostCardEmailConfirmationDetails();
                EmailEntity emailEntity = CommunicationUtility.GetLostCardConfirmationEmail(emailConfirmationMap);

                CommunicationService.SendMail(emailEntity, null);
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx, "Email could not send in Lost Card With Out Login screen.");
            }
        }

        #endregion SendMail

        #region SendMailToUser

        /// <summary>
        /// Method Responsible for sending the email message to User.
        /// </summary>
        private void SendMailToUser()
        {
            try
            {
                LoyaltyDetailsEntity loyaltyInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails as LoyaltyDetailsEntity;
                Dictionary<string, string> emailConfirmationMap = GetLostCardEmailConfirmationToUserDetails();
                EmailEntity emailEntity = CommunicationUtility.GetLostCardConfirmationEmailToUser(emailConfirmationMap,
                                                                                                  loyaltyInfoDetails.
                                                                                                      Email);
               
                CommunicationService.SendMail(emailEntity, null);
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx, "Email could not send in Lost Card With Out Login screen.");
            }
        }

        #endregion SendMailToUser

        #region GetLostCardEmailConfirmationDetails

        /// <summary>
        /// Collect the Lost Card Confirmation Information to be send as Email
        /// </summary>
        /// <returns>OrderedDictionary</returns>
        private Dictionary<string, string> GetLostCardEmailConfirmationDetails()
        {
            Dictionary<string, string> LostCardEmailConfirmationMap = new Dictionary<string, string>();
            string valueNotPresent = CommunicationTemplateConstants.BLANK_SPACES;
            LoyaltyDetailsEntity loyaltyDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails as LoyaltyDetailsEntity;
            if (loyaltyDetails != null)
            {
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.TITLE] =
                    Utility.GetNameTitle(loyaltyDetails.Title).Trim();
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.FIRST_NAME] = (loyaltyDetails.FirstName ==
                                                                                           null
                                                                                               ? valueNotPresent
                                                                                               : loyaltyDetails.
                                                                                                     FirstName);
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.LASTNAME] = (loyaltyDetails.SurName == null
                                                                                             ? valueNotPresent
                                                                                             : loyaltyDetails.SurName);
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.EMAIL] = (loyaltyDetails.Email == null
                                                                                          ? valueNotPresent
                                                                                          : loyaltyDetails.Email);
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.CITY] = (loyaltyDetails.City == null
                                                                                         ? valueNotPresent
                                                                                         : loyaltyDetails.City);
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.ADDRESS_LINE1] =
                    (loyaltyDetails.AddressLine1 == null ? valueNotPresent : loyaltyDetails.AddressLine1);
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.ADDRESS_LINE2] =
                    (loyaltyDetails.AddressLine2 == null ? valueNotPresent : loyaltyDetails.AddressLine2);
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.TELEPHONE1] = (loyaltyDetails.Telephone1 ==
                                                                                           null
                                                                                               ? valueNotPresent
                                                                                               : loyaltyDetails.
                                                                                                     Telephone1);
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.COUNTRY] = (loyaltyDetails.Country == null
                                                                                            ? valueNotPresent
                                                                                            : loyaltyDetails.Country);
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.POST_CODE] = (loyaltyDetails.PostCode ==
                                                                                          null
                                                                                              ? valueNotPresent
                                                                                              : loyaltyDetails.PostCode);
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.MEMBERSHIPNUMBER] =
                    (loyaltyDetails.UserName == null ? valueNotPresent : loyaltyDetails.UserName);
            }

            return LostCardEmailConfirmationMap;
        }

        #endregion GetLostCardEmailConfirmationDetails

        #region GetLostCardEmailConfirmationToUserDetails

        /// <summary>
        /// Collect the Lost Card Confirmation Information to be send as Email to user
        /// </summary>
        /// <returns>Dictionary</returns>
        private Dictionary<string, string> GetLostCardEmailConfirmationToUserDetails()
        {
            Dictionary<string, string> LostCardEmailConfirmationMap = new Dictionary<string, string>();
            string valueNotPresent = AppConstants.SPACE;
            LoyaltyDetailsEntity loyaltyDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails as LoyaltyDetailsEntity;
            if (loyaltyDetails != null)
            {
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.TITLE] =
                    Utility.GetNameTitle(loyaltyDetails.Title).Trim();
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.FIRST_NAME] = (loyaltyDetails.FirstName ==
                                                                                           null
                                                                                               ? valueNotPresent
                                                                                               : loyaltyDetails.
                                                                                                     FirstName);
                LostCardEmailConfirmationMap[CommunicationTemplateConstants.LOGOPATH] = AppConstants.LOGOPATH;
            }

            return LostCardEmailConfirmationMap;
        }

        #endregion GetLostCardEmailConfirmationToUserDetails

        #endregion Private Method
    }
}