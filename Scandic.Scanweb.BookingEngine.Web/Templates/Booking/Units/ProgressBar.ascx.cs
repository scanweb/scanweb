//  Description					: ProgressBar                                             //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code behind of ProgressBar control
    /// </summary>
    public partial class ProgressBar : System.Web.UI.UserControl
    {
        private bool isConfirmation = false;

        /// <summary>
        /// Remove the attributes need to set for default settings
        /// </summary>
        private void RemoveAttributes()
        {
            ptOne.Attributes.Remove("class");
            ptTwo.Attributes.Remove("class");
            ptThree.Attributes.Remove("class");
            ptFour.Attributes.Remove("class");

            ptOne.Attributes.Remove("href");
            ptTwo.Attributes.Remove("href");
            ptThree.Attributes.Remove("href");
            ptFour.Attributes.Remove("href");
        }

        /// <summary>
        /// Sets progress bar selection
        /// </summary>
        /// <param name="pageType"></param>
        internal void SetProgressBarSelection(string pageType)
        {
            isConfirmation = false;
            switch (pageType)
            {
                case EpiServerPageConstants.BOOKING_CONFIRMATION_PAGE:
                    {
                        isConfirmation = true;
                        RemoveAttributes();

                        ptOne.Attributes.Add("style", "display:block");
                        ptFour.Attributes.Add("class", "last activeComon");

                        ModifyFlow();

                        if (GenericSessionVariableSessionWrapper.IsPrepaidBooking)
                        {
                            breadCrumbPayment.Visible = true;
                            ptPayment.Attributes.Remove("href");
                            breadCrumbSelectHotel.Attributes.Add("class", "payment");
                            breadCrumbSelectRate.Attributes.Add("class", "payment");
                            breadCrumbBookingDetails.Attributes.Add("class", "payment");
                            breadCrumbPayment.Attributes.Add("class", "payment");
                            breadCrumbConfirmation.Attributes.Add("class", "payment");
                        }
                        if (BookingEngineSessionWrapper.IsModifyComboBooking)
                        {
                            breadCrumbPayment.Visible = false;
                        }
                    }
                    break;
                case EpiServerPageConstants.SELECT_HOTEL_PAGE:
                    {
                        RemoveAttributes();

                        ptOne.Attributes.Add("class", "activeHotel");

                        ptOne.Attributes.Remove("href");
                        ptTwo.Attributes.Remove("href");
                        ptThree.Attributes.Remove("href");
                        ptFour.Attributes.Remove("href");

                        ModifyFlow();
                    }
                    break;
                case EpiServerPageConstants.SELECT_RATE_PAGE:
                    {
                        RemoveAttributes();

                        ptTwo.Attributes.Add("class", "activeComon");

                        if (SearchCriteriaSessionWrapper.SearchCriteria != null &&
                            SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor != null && 
                            SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.UserSearchType ==
                            SearchedForEntity.LocationSearchType.City)
                        {
                            ptOne.Attributes.Add("href", "/Reservation/Select-Hotel/");
                            ptOne.Attributes.Add("class", "bookingBreadcrumb");
                        }
                        ptTwo.Attributes.Remove("href");
                        ptThree.Attributes.Remove("href");
                        ptFour.Attributes.Remove("href");

                        ModifyFlow();
                    }
                    break;
                case EpiServerPageConstants.BOOKING_DETAILS_PAGE:
                    {
                        RemoveAttributes();

                        ptThree.Attributes.Add("class", "activeComon");

                        if (SearchCriteriaSessionWrapper.SearchCriteria != null &&
                            SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor != null && 
                            SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.UserSearchType ==
                            SearchedForEntity.LocationSearchType.City)
                        {
                            ptOne.Attributes.Add("href", "/Reservation/Select-Hotel/");
                            ptOne.Attributes.Add("class", "bookingBreadcrumb");
                        }

                        ptTwo.Attributes.Add("href", "/Reservation/Select-Rate/");

                        if (Reservation2SessionWrapper.IsModifyFlow)
                        {
                            ptTwo.Attributes.Add("href", "/Reservation/Modify-Select-Rate/");
                            HygieneSessionWrapper.IsModifySelectRateFromBreadCrumb = true;
                        }

                        ptFour.Attributes.Remove("href");
                        ptTwo.Attributes.Add("class", "bookingBreadcrumb");

                        ModifyFlow();

                        if (GenericSessionVariableSessionWrapper.IsPrepaidBooking)
                        {
                            breadCrumbPayment.Visible = true;
                            breadCrumb.Attributes.Add("class", "mgnTop30");
                            ptPayment.Attributes.Remove("href");
                            breadCrumbSelectHotel.Attributes.Add("class", "payment");
                            breadCrumbSelectRate.Attributes.Add("class", "payment");
                            breadCrumbBookingDetails.Attributes.Add("class", "payment");
                            breadCrumbPayment.Attributes.Add("class", "payment");
                            breadCrumbConfirmation.Attributes.Add("class", "payment");
                        }
                        if (BookingEngineSessionWrapper.IsModifyComboBooking)
                        {
                            breadCrumbPayment.Visible = false;
                        }
                    }
                    break;
                case EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE:
                    {
                        RemoveAttributes();
                        ptTwo.Attributes.Add("class", "activeComon");
                        ptOne.Attributes.Add("style", "display:none");
                    }
                    break;               
            }
        }

        /// <summary>
        /// Changes on Modify or cancel flow 
        /// </summary>
        private void ModifyFlow()
        {
            if (BookingEngineSessionWrapper.IsModifyBooking)
            {
                if (!isConfirmation)
                {
                    ptOne.Attributes.Add("style", "display:none");
                }
                if (BookingEngineSessionWrapper.DirectlyModifyContactDetails)
                {
                    ptTwo.Attributes.Remove("href");
                    ptTwo.Attributes.Remove("class");
                }
            }
            else if (Reservation2SessionWrapper.IsModifyFlow)
            {
                if (!isConfirmation)
                {
                    ptOne.Attributes.Add("style", "display:none");
                }
            }
            else
            {
                ptOne.Attributes.Add("style", "display:block");
            }
        }
    }
}