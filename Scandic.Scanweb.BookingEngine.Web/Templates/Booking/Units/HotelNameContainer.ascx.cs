//  Description					:   HotelNameContainer                                    //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#region Scandic Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// This class contains all members of HotelNameContainer
    /// </summary>
    public partial class HotelNameContainer : EPiServer.UserControlBase
    {
        private string pageType = string.Empty;

        /// <summary>
        /// Sets page type
        /// </summary>
        /// <param name="pageType"></param>
        public void SetPageType(string pageType)
        {
            this.pageType = pageType;
        }


        public string SELECTED_HOTEL_NAME = string.Empty;

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
            {
                    string searchString = SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor != null ? SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.SearchString : string.Empty;

                    if (pageType == EpiServerPageConstants.SELECT_RATE_PAGE)
                        SELECTED_HOTEL_NAME = string.Format("{0}{1}{2}", WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectRoom"), AppConstants.SPACE, searchString);

                    else if (pageType == EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE)
                        SELECTED_HOTEL_NAME = string.Format("{0}{1}{2}", WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ModifyRoom"), AppConstants.SPACE, searchString);
            }
        }
    }
}