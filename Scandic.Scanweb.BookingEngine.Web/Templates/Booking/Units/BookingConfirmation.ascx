<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingConfirmation.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.BookingConfirmation" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register Src="~/Templates/Booking/Units/UsefulLinksContainer.ascx" TagName="UsefulLinks" TagPrefix="Booking" %>
<%--<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx" TagName="ReservationInformation" TagPrefix="Booking" %>
--%>
<%@ Register Src="~/Templates/Booking/Units/GuestInformation.ascx" TagName="GuestInformation" TagPrefix="Booking" %>
<%@ Register TagPrefix="Scanweb" TagName="ProgressBar" Src="~/Templates/Booking/Units/ProgressBar.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%@ Import Namespace="Scandic.Scanweb.Entity" %>
<!-- Step4: Booking Confirmation -->
<asp:PlaceHolder ID="progressBarPlaceHolder" runat="server">

<Scanweb:ProgressBar id="ProgressBar" runat="server" />
</asp:PlaceHolder>
<div class="BE">
    <div id= "scandicLogo" runat="server" visible="false" class="scandicPdfLogo">
        <%--<asp:Image ID="logoImage" runat="server" Visible= "true" Style="margin: 10px;" Width="149" Height="32" alt="Scandic" />--%>
    </div>
    <div id="BookingConfirmation">
	<div id="BookingDetails" class="BookingConfirmation">
	<div id="prntWrapper" runat="server">
			<!-- Progress Tab -->
		<div id="progTab">
		    <!-- If the booking is done with out kids i.e. no of children = 0, 
		    then the div with "pt_4" would be displayed. 
		    Or else if the no of children > 0, then the div with "pt_5" would be displayed. -->
	        <% if (!Utility.IsFamilyBooking())
               {%>
	        <div id="pt_4" class="lastCurve">
			  <%--  <ul>
				    <li class="pt_num firstTick">&nbsp;</li>
				    <li class="text visited"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selecthotel")%></li>
				    <li class="pt_arrow1 lastvisitedArrow">&nbsp;</li>
					
				    <li  class="pt_num tick">&nbsp;</li>
				    <li class="text visited"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selectrate")%></li>
				    <li class="pt_arrow1 lastvisitedArrow">&nbsp;</li>
					
				    <li  class="pt_num tick">&nbsp;</li>
				    <li class="text visited"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/bookingdetails")%></li>
				    <li class="pt_arrow1 visitedArrow">&nbsp;</li>
					
				    <li  class="pt_num four">&nbsp;</li>
				    <li class="text active"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/confirmation")%></li>
				    <li class="pt_last">&nbsp;</li>
			    </ul>--%>
		    </div>
	        <% }
               else
               { %>		    
		  <%--  <div id="pt_5" class="lastCurve">
				    <ul>
				        <li class="pt_num firstTick">&nbsp;</li>
					    <li class="text visited"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/childrendetail")%></li>
					    <li class="pt_arrow1 lastvisitedArrow">&nbsp;</li>
    					
					    <li class="pt_num tick">&nbsp;</li>
					    <li class="text visited"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selecthotel")%></li>
					    <li class="pt_arrow1 lastvisitedArrow">&nbsp;</li>
    					
					    <li class="pt_num tick">&nbsp;</li>
					    <li class="text visited"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selectrate")%></li>
					    <li class="pt_arrow1 lastvisitedArrow">&nbsp;</li>
    						
					    <li class="pt_num tick">&nbsp;</li>
					    <li class="text visited"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/bookingdetails")%></li>
					    <li class="pt_arrow1 visitedArrow">&nbsp;</li>
    						
					    <li class="pt_num five">&nbsp;</li>
					    <li class="text active"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/confirmation")%></li>
					    <li class="pt_last">&nbsp;</li>
				    </ul>
                </div>--%>
            <% } %>            
            
			<div class="clear">&nbsp;</div>
		</div>
		<!-- /Progress Tab -->
		<%--<Booking:ReservationInformation ID="ReservationInfo" ShowHelpLink ="false"  runat="server">
	    </Booking:ReservationInformation>--%>           
                 
                 <!-- BEGIN New Opera Message -->
                <div class="clearAll"><!-- Clearing Float --></div>
                <div id="clientErrorDivBC" class="infoAlertBox infoAlertBoxRed" runat="server" visible="false">
                    <div class="threeCol alertContainer">
                        <div class="hd sprite">&#160;</div>
                        <div class="cnt sprite">
                           <span class="alertIconRed">&#160;</span>
                           <div id="Div1" class="descContainer" runat="server">
                                <label class="titleRed"><%= WebUtil.GetTranslatedText("/bookingengine/booking/Rate48HRS/PleaseNote") %></label>
                                <label id="operaErrorMsg" runat="server" class="infoAlertBoxDesc"></label>
                           </div>
                           <div class="clearAll">&#160;</div>
                        </div>
                        <div class="ft sprite">&#160;</div>
                    </div>
                 </div>
                 <!-- END New Opera Message --> 
                 
		<div class="infoAlertBox" id="invalidFGPBox" runat="server" visible="false">
                <div class="threeCol alertContainer">
                    <div class="hd sprite">
                        &nbsp;</div>
                    <div class="cnt sprite">
                        <p class="scansprite">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/InvalidFGPNumber") %>
                            <br />
                            <span id="lblInvalidFGPNum" runat="server"></span>
                        </p>
                    </div>
                    <div class="ft sprite">
                    </div>
                </div>
            </div>	
		<Booking:GuestInformation ID="GuestInformation" runat="server">
	    </Booking:GuestInformation>
			</div>	
	</div>
	<!-- /hotel Info -->
</div>
</div>