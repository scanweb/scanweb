////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: Code behind class for LoginStatus user control    	  //
//								  This user control is used when user wants to log to     //
//                                system using user name and password.                    //  
//                                This will populate the session and push the user name   //
//                                to the session.                                         //   
//----------------------------------------------------------------------------------------//
//  Author						: Raj Kishore Marandi	                                  //
//  Author email id				:                           							  //
//  Creation Date				: 13th November  2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revision History				: -NA-												  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Core;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code behind of LoginStatus control.
    /// </summary>
    public partial class LoginStatus : EPiServer.UserControlBase
    {
        protected string LoggedinUserNameTop = string.Empty;

        # region PROTECTED METHOD

        #region PAGE LOAD

        /// <summary>
        /// This is fired when page is loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                loyaltyLoginErrDiv.InnerHtml = string.Empty;
                if (LoginErrorCodeSessionWrapper.LoginPopupErrorCode > 0 && LoginErrorCodeSessionWrapper.LoginPopupErrorCode != 6
                    && LoginErrorCodeSessionWrapper.LoginPopupErrorCode != 7 && LoginErrorCodeSessionWrapper.LoginPopupErrorCode != 8)
                {
                    Utility.ShowErrorMessageForAccordian(loyaltyLoginErrDiv, WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/LoginErrorMessage"));
                    Page.ClientScript.RegisterStartupScript(GetType(), "Login", "Login_Box.style.display = 'block';", true);
                    LoginErrorCodeSessionWrapper.LoginPopupErrorCode = 0;
                }

                UpdateLoginStatus(true);
                string pagename = CurrentPage.PageName.Replace("'", "\\'");
                lnkBtnJoinHeader.Attributes.Add("onclick", "AddSiteCatalystForJoin('" + pagename + "');");
                spnBtnJoinHeader.Attributes.Add("onclick", "AddSiteCatalystForJoin('" + pagename + "');");
                LoginLink.Attributes.Add("onclick", "javascript:return checkLoginOrLogOut(this, 'Login_Box');");
                spnLink.Attributes.Add("onclick", "javascript:return checkLoginOrLogOut(this, 'Login_Box');");
                loggedinUserName.Attributes.Add("onclick", "javascript:showLogoutBox(); return false;");
                LoginCloseLink.Attributes.Add("onclick", "javascript:showLoginControl(); return false;");
                btnLogIn.Attributes.Add("onclick", "javascript:AddSiteCatalystForLogin('" + pagename + "');return performValidation(PAGE_LOGIN_STATUS);");

                btnLogIn.PostBackUrl = WebUtil.LoginValidatorUrl();

                if (!IsPostBack)
                {
                    RememberUserIdPwd.Checked = true;
                    if (!UserLoggedInSessionWrapper.UserLoggedIn)
                    {
                        if (WebUtil.IsCookieExists(AppConstants.DONTREMEMBERME_COOKIE))
                            RememberUserIdPwd.Checked = false;
                        else
                            WebUtil.SetCookieToTextBox(txtLoyaltyUsernamePrefix, txtLoyaltyUsername, txtLoyaltyPassword, RememberUserIdPwd);
                    }
                    
                    if (CurrentPage.PageLink.ID != Utility.GetLoginForDeepLinkPageId())
                        Utility.StoreLastVisitedPage(CurrentPage.PageLink.ID, Request.RawUrl);

                    LoginLink.Attributes.Add("onclick", "javascript:return checkLoginOrLogOut(this, 'Login_Box');");
                    spnLink.Attributes.Add("onclick", "javascript:return checkLoginOrLogOut(this, 'Login_Box');");
                    LoginCloseLink.Attributes.Add("onclick", "javascript:showLoginControl(); return false;");
                    btnLogIn.Attributes.Add("onclick", "javascript:AddSiteCatalystForLogin('" + pagename + "');return performValidation(PAGE_LOGIN_STATUS);");

                    LoginSourceModule currentModule = LoginSourceModule.LOGIN_POPUP_MODULE;

                    if ((Request != null && Request.UrlReferrer != null && Request.RawUrl != null) && (Request.UrlReferrer.ToString().ToLower().Contains(AppConstants.SELECT_RATE) || Request.UrlReferrer.ToString().ToLower().Contains(AppConstants.SELECT_HOTEL) ||
                        Request.UrlReferrer.ToString().ToLower().Contains(AppConstants.BOOKING_DETAILS) || Request.RawUrl.ToString().ToLower().Contains(AppConstants.SELECT_RATE) || Request.RawUrl.ToString().ToLower().Contains(AppConstants.SELECT_HOTEL)
                        || Request.RawUrl.ToString().ToLower().Contains(AppConstants.BOOKING_DETAILS)))
                    {
                        currentModule = LoginSourceModule.BOOKING_FLOW_MODULE;
                    }

                    Utility.SetLoginDemographics(txtLoyaltyUsernamePrefix.Name, txtLoyaltyUsername.Name, txtLoyaltyPassword.UniqueID,
                                                 RememberUserIdPwd.Name, currentModule, loginPopUpPageID.Name);

                    loginPopUpID.Value = LoginSourceModule.LOGIN_POPUP_MODULE.ToString();
                }

                SetLoginPopUpCSS();
            }
        }

        private void SetLoginPopUpCSS()
        {
            string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToLower();
            if (0 == string.Compare(siteLanguage, LanguageConstant.LANGUAGE_FINNISH, true))
            {
                textCont.Attributes["class"] = String.Concat("textCont", LanguageConstant.LANGUAGE_FINNISH.ToLower());
                loginContainer.Attributes["class"] = String.Concat("loginFlyout roundMe fltLft loginCnt", LanguageConstant.LANGUAGE_FINNISH.ToLower());
            }
            if (0 == string.Compare(siteLanguage, LanguageConstant.LANGUAGE_DANISH, true))
            {
                textCont.Attributes["class"] = String.Concat("textCont", LanguageConstant.LANGUAGE_DANISH.ToLower());
                loginContainer.Attributes["class"] = String.Concat("loginFlyout roundMe fltLft loginCnt", LanguageConstant.LANGUAGE_DANISH.ToLower());
            }
            if (0 == string.Compare(siteLanguage, LanguageConstant.LANGUAGE_SWEDISH, true))
            {
                textCont.Attributes["class"] = String.Concat("textCont", LanguageConstant.LANGUAGE_SWEDISH.ToLower());
                loginContainer.Attributes["class"] = String.Concat("loginFlyout roundMe fltLft loginCnt", LanguageConstant.LANGUAGE_SWEDISH.ToLower());
            }
            if (0 == string.Compare(siteLanguage, LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION, true))
            {
                textCont.Attributes["class"] = String.Concat("textCont", LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION.ToLower());
                loginContainer.Attributes["class"] = String.Concat("loginFlyout roundMe fltLft loginCnt", LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION.ToLower());
            }
            if (0 == string.Compare(siteLanguage, LanguageConstant.LANGUAGE_GERMAN, true))
            {
                textCont.Attributes["class"] = String.Concat("textCont", LanguageConstant.LANGUAGE_GERMAN.ToLower());
                loginContainer.Attributes["class"] = String.Concat("loginFlyout roundMe fltLft loginCnt", LanguageConstant.LANGUAGE_GERMAN.ToLower());
            }
            if (0 == string.Compare(siteLanguage, LanguageConstant.LANGUAGE_RUSSIA, true))
            {
                textCont.Attributes["class"] = String.Concat("textCont", LanguageConstant.LANGUAGE_RUSSIA.ToLower());
                loginContainer.Attributes["class"] = String.Concat("loginFlyout roundMe fltLft loginCnt", LanguageConstant.LANGUAGE_RUSSIA.ToLower());
            }
        }


        #endregion PAGE LOAD

        #region LogOut

        /// <summary>
        /// This method will clear up the session and make the user log off. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LogOut(object sender, EventArgs e)
        {
            WebUtil.ClearBlockedRooms();
            UserNavTracker.ClearTrackedData();
            Response.Redirect(WebUtil.LoginValidatorUrl(), false);
        }

        /// <summary>
        /// this method will redirect the user to My pages page after login.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MyPages_Click(object sender, EventArgs e)
        {
            string redirectPageURL = GlobalUtil.GetUrlToPage(EpiServerPageConstants.MYACCOUNT_BOOKING_PAGE);
            Response.Redirect(redirectPageURL);
        }

        #endregion LogOut

        #endregion PROTECTED METHOD

        #region PRIVATE METHOD

        #region UpdateLoginStatus

        /// <summary>
        /// This method should be public. It is being called from other places too.
        /// </summary>
        /// <returns></returns>
        public void UpdateLoginStatus(bool makeOperaCall)
        {
            if (UserLoggedInSessionWrapper.UserLoggedIn)
            {
                LoyaltyDetailsEntity loyalityInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
                if (loyalityInfoDetails != null)
                {
                    if (makeOperaCall && CurrentPage != null && (string.Equals(CurrentPage.PageTypeName, PageIdentifier.ReservationConfirmation, StringComparison.InvariantCultureIgnoreCase) ||
                        string.Equals(CurrentPage.PageTypeName, PageIdentifier.ModifyReservationCancelledBooking, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        double noOfPoints = ReturnPoints(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID);
                        LoyaltyDetailsSessionWrapper.LoyaltyDetails.CurrentPoints = noOfPoints;
                    }
                    LoginLink.Text = WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/logoutmessage");
                    LoginForBestDeals.Visible = false;
                    JoinHeader.Visible = false;
                    textCont.Style.Add("background", "none");
                    spnLink.Style.Add("display", "none");
                    LoginLink.Visible = false;
                    DisplayLoggedinUserValues();
                }
            }
            else
            {
                placeholderPoint.InnerHtml = "&nbsp;";
                LoginLink.Visible = true;
                LogoutLink.Visible = false;
                LoginLink.Text = WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/loginmessage");
                LoginForBestDeals.Visible = true;
                JoinHeader.Visible = true;
            }
        }

        #endregion UpdateLoginStatus

        # region ReturnPoints

        public void DisplayLoggedinUserValues()
        {
            EditProfile.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.MY_PROFILE_PAGE);

            LogoutLink.Visible = true;
            LogoutLink.Text = WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/logoutmessage");
            LogoutLink.Attributes.Add("onclick", "javascript:return checkLoginOrLogOut(this, 'Logout_Box');");

            MyPages.Visible = true;
            MyPages.Text = WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/MyPages");

            EditProfile.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/EditMyProfile");
            if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null) // Refactor - Added
            {
                double noOfPoints = LoyaltyDetailsSessionWrapper.LoyaltyDetails.CurrentPoints;
                string pointString = WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/pointsheading");
                string generatedString = string.Format(pointString, noOfPoints);

                divPoints.Visible = true;
                divPoints.InnerHtml = generatedString;

                loggedinUserName.Visible = true;
                spnLoggedInUserEmail.InnerText = LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email;

                //Added as part of SCANAM-537
                if (!string.IsNullOrEmpty(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NativeFirstName) &&
                   !string.IsNullOrEmpty(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NativeLastName))
                {
                    LoggedinUserNameTop = LoyaltyDetailsSessionWrapper.LoyaltyDetails.NativeFirstName + " " + LoyaltyDetailsSessionWrapper.LoyaltyDetails.NativeLastName;
                    spnUserNameLogout.InnerText = LoyaltyDetailsSessionWrapper.LoyaltyDetails.NativeFirstName + " " + LoyaltyDetailsSessionWrapper.LoyaltyDetails.NativeLastName;
                }
                else
                {
                    LoggedinUserNameTop = LoyaltyDetailsSessionWrapper.LoyaltyDetails.FirstName + " " + LoyaltyDetailsSessionWrapper.LoyaltyDetails.SurName;
                    spnUserNameLogout.InnerText = LoyaltyDetailsSessionWrapper.LoyaltyDetails.FirstName + " " + LoyaltyDetailsSessionWrapper.LoyaltyDetails.SurName;
                }
            }
        }

        /// <summary>
        /// This will fetch points credit by Loyalty user.
        /// </summary>
        /// <returns></returns>
        private double ReturnPoints(string nameId)
        {
            double availablePoints = 0;

            NameController nameController = new NameController();
            availablePoints = nameController.ReturnPoints(nameId);

            return availablePoints;
        }

        # endregion ReturnPoints

        #region ForgottenPassword_Click

        /// <summary>
        /// This will redirect to forgotten password page where user is able to retrieve forgotten password.
        /// </summary>
        /// <param name="sender">Sender name</param>
        /// <param name="e">Event Arguments</param>
        protected void ForgottenPassword_Click(object sender, EventArgs e)
        {
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.FORGOTTEN_PASSWORD), false);
        }

        /// <summary>
        /// EnrollLoyalty click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnrollLoyalty_Click(object sender, EventArgs e)
        {
            Reservation2SessionWrapper.IsUserClickTopRightButtonForRegistration = true;
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.ENROLL_LOYALTY), false);
        }

        #endregion ForgottenPassword_Click

        #endregion PRIVATE METHOD

        #region INavigationTraking Members
        /*
        public System.Collections.Generic.List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> paramters = new List<KeyValueParam>();
            paramters.Add(new KeyValueParam(TrackerConstants.LOG_TYPE, actionName));
            return paramters;
        }
		*/
        #endregion
    }
}