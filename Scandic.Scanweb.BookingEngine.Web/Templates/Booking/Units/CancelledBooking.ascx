<%@ Control Language="C#" AutoEventWireup="true" Codebehind="CancelledBooking.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.CancelledBooking" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx"
    TagName="ReservationInformation" TagPrefix="Booking" %>
<%--<div id="Reservation" class="BE">
	<!-- Step2: Select Rate -->
	<div id="ModifyOrCancel" class="CancelConfirmation">
		<Booking:ReservationInformation ID="ReservationInfo" ShowHelpLink ="false" runat="server">
        </Booking:ReservationInformation>
	</div>		
	<div id="divCancellationInfo" runat="server">
	<div id="resInfo" class="CancelConfirmation">
	    <div class="resInfo-top">&nbsp;</div>
        <div class="resNumber">
          <p><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/cancellationDetails")%></p>
        </div>
        <div class="resInfo-inner">
          <div id="Availability">
            <div class="RoomCancelConfirmation">
              <p><label id="lblRoom1CancelInfo" visible="false" runat="server"></label></p>
              <p><label id="lblRoom2CancelInfo" visible="false" runat="server"></label></p>
              <p><label id="lblRoom3CancelInfo" visible="false" runat="server"></label></p>
              <p><label id="lblRoom4CancelInfo" visible="false" runat="server"></label></p>
            </div>
            <div class="clear">&nbsp;</div>
          </div>
        </div>
        <div class="resInfo-bottom">&nbsp;</div>
	</div>
	</div>
</div>--%>
<link rel="stylesheet" type="text/css" href="/Templates/Booking/Styles/Default/reservation2.0.css" />
<% if ((Request.QueryString["command"] == null) || ((Request.QueryString["command"] != null) && (Request.QueryString["command"].ToString() == "isPDF")))
   { %>
<div id="StageAreaWideFull">
<input type="hidden" id="printerFriendly" runat="server" />
<div id= "scandicLogo" runat="server" visible="false" class="scandicPdfLogo">
   <%--  <asp:Image ID="logoImage" runat="server" Style="margin: 10px;" Width="149" Height="32"
                                alt="Scandic" Visible= "true" />--%>
    </div>
    <div id="reservation2Container">
        <div class="tripleCol mgnLft20 flushMarginRight">
            <div id="Header">
            <h2 class="borderH2"><%= WebUtil.GetTranslatedText("/bookingengine/booking/cancelconfirmation/subject") %></h2>
               <%-- <a href="#" onclick="javascript:print();" title='<%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/Print")%>'
                    class="print spriteIcon">
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/Print")%>
                </a>--%>
                <%--<asp:LinkButton CssClass="print spriteIcon" ID="lnkBtnPrinterfriendly" runat="server" ><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/Print")%></asp:LinkButton>--%>
                 <a id="lnkBtnPrinterfriendly" runat="server" href="#" target="_top" tabindex="15" class="print spriteIcon pntfdlylink"
                        onmousedown="javascript:handleRightMouseClick(event);" onmouseup="javascript:handleRightMouseClick(event);"
                        onclick="javascript:getPrinter(canccelbookingprinterFrindlyURL);return false;">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/Print") %>
                    </a>
                <div class="clearAll"><!-- Clearing Float --></div>
                <div class="actionBtn fltRt cancelConfirmPrint confirmationPrint" id="PrintDiv" visible="false" runat="server">
						  <asp:LinkButton ID="LnkBtnPrint" runat="server" CssClass="buttonInner cPrint" ><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/print") %></asp:LinkButton>
						  <asp:LinkButton ID="spnBtnPrint" runat="server" CssClass="buttonRt scansprite" ></asp:LinkButton>
				</div>
                <div class="clearAll"><!-- Clearing Float --></div>
            </div>
            <div id="roomCncl1" runat="server" visible="false" style="border:1px solid #ccc;" class="errorMsgCancelConf">
                <p>
                    <span id="reservationNo1"  runat="server"></span>
                    <span id="name1" runat="server"></span>
                    <span id="cancelNo1" runat="server"></span>
                 </p>
                 <p id="email1" runat="server"></p>
                  <p id="mobileNumber1" runat="server" visible="false"></p>
            </div>
             <div id="roomCncl2" runat="server" visible="false" style="border:1px solid #ccc;" class="errorMsgCancelConf">
                <p>
                    <span id="reservationNo2"  runat="server"></span>
                    <span id="name2" runat="server"></span>                    
                    <span id="cancelNo2" runat="server"></span>
                </p>
                <p id="email2" runat="server"></p>   
                 <p id="mobileNumber2" runat="server" visible="false"></p>             
            </div>
             <div id="roomCncl3" runat="server" visible="false" style="border:1px solid #ccc;" class="errorMsgCancelConf">
                <p>
                    <span id="reservationNo3"  runat="server"></span>
                    <span id="name3" runat="server"></span>
                    <span id="cancelNo3" runat="server"></span>
                </p>
                <p id="email3" runat="server"></p>
                <p id="mobileNumber3" runat="server" visible="false"></p> 
            </div>
             <div id="roomCncl4" runat="server" visible="false" style="border:1px solid #ccc;" class="errorMsgCancelConf">
                <p>
                    <span id="reservationNo4"  runat="server"></span>
                    <span id="name4" runat="server"></span>
                    <span id="cancelNo4" runat="server"></span>
                </p>
                <p id="email4" runat="server"></p>
                  <p id="mobileNumber4" runat="server" visible="false"></p>
            </div>
            <div class="infoAlertBox padBtm5" id="infoAlertBox" runat="server">
                <div class="threeCol alertContainer">
                    <div class="hd sprite">
                        &nbsp;</div>
                    <div class="cnt sprite">
                        <p class="spriteIcon canclInfo">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/BookingCancelled") %>
                            </strong>
                        </p>
                    </div>
                    <div class="ft sprite">
                    </div>
                </div>
            </div>
            <div class="clear">
            </div>
            <div id="confirmAlertMod" runat="server" class="confirmAlertMod">
                <div class="threeCol alertContainer">
                    <div class="hd sprite">&nbsp;</div>
                    <div class="cnt sprite">
                        <div class="cancelConfirmationAlert">
                            <!-- Room 1 Alert Message -->
                            <div class="canConfrmAlertSec canConfrmAlertSecFirst" id="Room1CancellationMessage" runat="server" visible="false">
                                <!-- eMail Message -->
                                <div class="cancelCofirmEmail">
                                    <label id="lblRoom1CancelInfo" runat="server"></label>
                                    <br />
                                    <strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/EmailConfirmation") %></strong>&#160;
                                    <span id="lblEmailRoom1" runat="server"></span>
                                </div>
                                
                                <!-- SMS Message -->
                                <div class="cancelConfirmSms" id="SMSRoom1Div" visible="false" runat="server">
                                    <strong id="smsLabel1" runat="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent") %></strong>&#160;
                                    <span id="lblSMSRoom1" runat="server"></span>
                                </div>
                            </div>
                            
                            <!-- Room 2 Alert Message -->
                            <div class="canConfrmAlertSec" id="Room2CancellationMessage" runat="server" visible="false">
                                <!-- eMail Message -->
                                <div class="cancelCofirmEmail">
                                    <label id="lblRoom2CancelInfo" runat="server"></label>
                                    <br />
                                    <strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/EmailConfirmation") %></strong>&#160;
                                    <span id="lblEmailRoom2" runat="server"></span>
                                </div>
                                
                                <!-- SMS Message -->
                                <div class="cancelConfirmSms" id="SMSRoom2Div" visible="false" runat="server">
                                    <strong id="smsLabel2" runat="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent") %></strong>&#160;
                                    <span id="lblSMSRoom2" runat="server"></span>
                                </div>
                            </div> 
                            
                            <!-- Room 3 Alert Message -->
                            <div class="canConfrmAlertSec" id="Room3CancellationMessage" runat="server" visible="false">
                                <!-- eMail Message -->
                                <div class="cancelCofirmEmail">
                                    <label id="lblRoom3CancelInfo" runat="server"></label>
                                    <br />
                                    <strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/EmailConfirmation") %></strong>&#160;
                                    <span id="lblEmailRoom3" runat="server"></span>
                                </div>
                                
                                <!-- SMS Message -->
                                <div class="cancelConfirmSms" id="SMSRoom3Div" visible="false" runat="server">
                                    <strong id="smsLabel3" runat="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent") %></strong>&#160;
                                    <span id="lblSMSRoom3" runat="server"></span>
                                </div>
                            </div>

                            <!-- Room 4 Alert Message -->
                            <div class="canConfrmAlertSec" id="Room4CancellationMessage" runat="server" visible="false">
                                <!-- eMail Message -->
                                <div class="cancelCofirmEmail">
                                    <label id="lblRoom4CancelInfo" runat="server"></label>
                                    <br />
                                    <strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/EmailConfirmation") %></strong>&#160;
                                    <span id="lblEmailRoom4" runat="server"></span>
                                </div>
                                
                                <!-- SMS Message -->
                                <div class="cancelConfirmSms" id="SMSRoom4Div" visible="false" runat="server">
                                    <strong id="smsLabel4" runat="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent") %></strong>&#160;
                                    <span id="lblSMSRoom4" runat="server"></span>
                                </div>
                            </div>                                                           
                                                       
                        </div>
                     <%--   <p style="padding-left: 0;">
                            <%--<span id="Room1CancellationMessage" runat="server" visible="false">
                                <label id="lblRoom1CancelInfo" class="eMsgCancelContainer" runat="server"></label>
                                <strong><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/EmailConfirmation")%></strong>&#160;
                                <span id="lblEmailRoom1" runat="server"></span>
                                <br />
                                 <%--Vrushali | artf1164014 : Info after confirmed cancellation
                                <%--<strong id="smsLabel1" runat="server" visible="false"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent")%></strong>&#160;
                                <span id="lblSMSRoom1" runat="server" visible="false"></span>
                                <br />
                            </span>
                            <span id="Room2CancellationMessage" runat="server" visible="false">
                                <br />
                                <label id="lblRoom2CancelInfo" class="eMsgCancelContainer" runat="server"></label>
                                <strong class="fltLft"><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/EmailConfirmation")%></strong>&#160;
                                <span id="lblEmailRoom2" runat="server"></span>
                                <br />
                                <%--Vrushali | artf1164014 : Info after confirmed cancellation
                                <strong id="smsLabel2" runat="server" visible="false"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent")%></strong>&#160;
                                <span id="lblSMSRoom2" runat="server" visible="false"></span>
                                <br />
                            </span>
                            <span id="Room3CancellationMessage" runat="server" visible="false">
                                <br />
                                <label id="lblRoom3CancelInfo" class="eMsgCancelContainer" runat="server"></label>
                                <!--R2.0 /BugID:513022/Rajneesh
	                            Description:Promo Flex Bookng| Sv local| Cancel Conf Msg  not proper.
	                            Modification:Added one <br> tag below
	                             -->	
                                <strong><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/EmailConfirmation")%></strong>&#160;
                                <span id="lblEmailRoom3" runat="server"></span>
                                <br />
                                <%--Vrushali | artf1164014 : Info after confirmed cancellation
                                <strong id="smsLabel3" runat="server" visible="false"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent")%></strong>&#160;
                                <span id="lblSMSRoom3" runat="server" visible="false"></span>
                                <br />
                                
                            </span>
                            <span id="Room4CancellationMessage" runat="server" visible="false">
                                <br />
                                <label id="lblRoom4CancelInfo" class="eMsgCancelContainer" runat="server"></label>
                                <!--R2.0 /BugID:513022/Rajneesh
	                            Description:Promo Flex Bookng| Sv local| Cancel Conf Msg  not proper.
	                            Modification:Added one <br> tag below
	                             -->                                
                                <strong class="fltLft"><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/EmailConfirmation")%></strong>&#160;
                                <span id="lblEmailRoom4" runat="server"></span>
                                <br />
                                <%--Vrushali | artf1164014 : Info after confirmed cancellation
                                <strong id="smsLabel4" runat="server" visible="false"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent")%></strong>&#160;
                                <span id="lblSMSRoom4" runat="server" visible="false"></span>
                                <br />
                            </span>
                        </p>--%>
                    </div>
                    <div class="ft sprite"></div>
                </div>
            </div>
            <div id="MainReservationBox" runat="server">
                <div id="masterReserMod21" class="masterReserMod21">
                    <div class="greyRd">
                        <%--<div class="hd sprite">
                        </div>
                        <p class="fltLft lblReservationHeader">
                            <strong><span id="lblReservationHeader" runat="server"></span></strong><a class="help spriteIcon toolTipMe"
                                id="reservationNumberHelpIcon" href="#" runat="server"></a>
                            <span id="lblReservationNumber" runat="server"></span>
                        </p>--%>
                            <Booking:ReservationInformation ID="ReservationInfo" ShowHelpLink="false" runat="server">
                            </Booking:ReservationInformation>
                        <div class="ft">
                            &nbsp;</div>
                    </div>
                </div>
            </div>
            <div class="guaranteTypeInfo">
               <%-- <h3>
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformation")%>
                </h3>
                <ul>
                    <li>
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformationLine1")%>
                    </li>
                    <li>
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformationLine2")%>
                    </li>
                    <li>
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformationLine3")%>
                        <a href="#" title="www.scandichotels.com">
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/ScandicSite")%>
                        </a>
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformationLine4")%>
                    </li>
                </ul>--%>
                 <label id="lblGuaranteeInfo" runat="server"></label>
            </div>
        </div>
    </div>
</div>

 <% } %>
 
 <% if (Request.QueryString["command"] != null && Request.QueryString["command"].ToString() == "CancelPageprint")
    {%>
 <div id= "pfscandicLogo" runat="server" visible="false" class="scandicPdfLogo" style="margin:20px 10px 0;">
   <%--  <asp:Image ID="logoImage" runat="server" Style="margin: 10px;" Width="149" Height="32"
                                alt="Scandic" Visible= "true" />--%>
                                
    </div>
   <div id="printerFriendlySection" runat="server" visible="false">
    <div id="confirmationPrint">
       <div id="pfPrintDiv" visible="false" runat="server">
           <!-- Print Button -->
           <div class="actionBtn fltRt cancelConfirmPrint confirmationPrint">
			      <asp:LinkButton ID="pfLnkBtnPrint" runat="server" CssClass="buttonInner cPrint" ><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/print") %></asp:LinkButton>
			      <asp:LinkButton ID="pfSpnBtnPrint" runat="server" CssClass="buttonRt scansprite" ></asp:LinkButton>
		    </div>
		    <div class="clearFix">&#160;</div>
	   </div>
       
       <!-- Title -->
        <h1><%= WebUtil.GetTranslatedText("/bookingengine/booking/cancelconfirmation/subject") %></h1>
	    <div class="printerCancellationDetails">       
    	   <div class="section" id="pfRoomCncl1" runat="server" visible="false">
                <div>
                    <span id="pfReservationNo1"  runat="server"></span>
                    <span class="printCancelName" id="pfName1" runat="server"></span>
                    <span id="pfCancelNo1" runat="server"></span>
                </div>
                <div id="pfEmail1" runat="server"></div>
                <div id="pfMobileNumber1" runat="server" visible="false"></div>
            </div>
            
             <div class="section" id="pfRoomCncl2" runat="server" visible="false">
                <div>
                    <span id="pfReservationNo2"  runat="server"></span>
                    <span id="pfName2" runat="server"></span>                    
                    <span id="pfCancelNo2" runat="server"></span>
                </div>
                 <div id="pfEmail2" runat="server"></div>   
                 <div id="pfMobileNumber2" runat="server" visible="false"></div>             
            </div>
            
             <div class="section" id="pfRoomCncl3" runat="server" visible="false">
                <div>
                    <span id="pfReservationNo3"  runat="server"></span>
                    <span id="pfName3" runat="server"></span>
                    <span id="pfCancelNo3" runat="server"></span>
                </div>
                <div id="pfEmail3" runat="server"></div>
                <div id="pfMobileNumber3" runat="server" visible="false"></div> 
            </div>
             <div class="section" id="pfRoomCncl4" runat="server" visible="false">
                <div>
                    <span id="pfReservationNo4"  runat="server"></span>
                    <span id="pfName4" runat="server"></span>
                    <span id="pfCancelNo4" runat="server"></span>
                </div>
                <div id="pfEmail4" runat="server"></div>
                <div id="pfMobileNumber4" runat="server" visible="false"></div>
            </div>
        </div>
            
            <div id="pfMainReservationBox" runat="server">
                <div id="pfMasterReserMod21" class="masterReserMod21">
                    <Booking:ReservationInformation ID="pfReservationInfo" ShowHelpLink="false" runat="server">
                    </Booking:ReservationInformation>
                </div>
            </div>
       
            <div id="pfguaranteTypeInfo">
                <label id="pfLblGuaranteeInfo" runat="server"></label>
            </div>
    </div>
   </div>
   
   <% } %>
<script language="javascript" type="text/javascript">

//artf1167462 : Printer friendly version :Rajneesh
//Client side variable to store the printer friendly URL.This variable is passed in the getPrinter function
var canccelbookingprinterFrindlyURL = '<%= requestURL %>';  
  
</script>
