<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ForgottenPassword.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ForgottenPassword" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register Src="EmailConfirmation.ascx" TagName="EmailConfirmation" TagPrefix="uc1" %>
<script language="javascript" type="text/javascript">
    
    $(document).ready(function() {
        appendPrefix("txtForgotPrefix");
        $("input[id$='txtMembershipNumber']").bind('focusout', trimwhitespaceFP);
    });

	function trimwhitespaceFP() {
	    trimWhiteSpaces("txtMembershipNumber");
	    replaceDuplicatePrefix("txtMembershipNumber");
	}    

</script>
<div id="Loyalty" class="BE">
    <!-- LastCard -->
    <div id="ForgotPassword" runat="server">
        <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
        <input type="hidden" id="invalidprogNo" name="invalidprogNo" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/guestprogrammenumber") %>' />
        <input type="hidden" id="invalidQuestion" name="invalidQuestion" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/question") %>' />
        <input type="hidden" id="invalidAnswer" name="invalidAnswer" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/answer") %>' />
        <div class="box-top-grey">
            <span class="title">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/forgottenpin/messageheader") %>
            </span>
        </div>
        <div class="box-middle">
            <div class="content"  onkeypress="return WebForm_FireDefaultButton(event, '<%= LinkButtonSend.ClientID %>')">
                <div id="ForgotPasswordErrorDiv" runat="server">
                </div>
                <p>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/forgottenpin/messagedescription") %>
                </p>
                <div class="columnOne">
                    <p class="formRow">
                        <span id="spanFPFrequentGuestProgramme">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/forgottenpin/frequentguestprogram") %>
                            </label>
                        </span>
                        <br />
                        <input type="text" name="txtForgotPrefix" class="frmForgotPwdPrefixText" id="txtForgotPrefix" runat="server" maxlength="10" 
                        readonly="readonly" />
                        <input type="text" name="txtMembershipNumber" class="frmForgotPwdInputText" id="txtMembershipNumber" runat="server" />
                    </p>
                </div>
                <!-- /details -->
                <!-- Footer -->
                <div id="FooterContainer" class="noimage">
                    <div class="alignLeft">
                        <span>&nbsp; </span>
                    </div>
                    <div class="alignRight">
                        <div class="actionBtn fltRt">
                            <asp:LinkButton ID="LinkButtonSend" class="buttonInner" OnClick="ButtonSend_Click"
                                runat="server"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/forgottenpin/logonbutton") %></span></asp:LinkButton>
                                
                                <asp:LinkButton ID="spnButtonSend" class="buttonRt scansprite" OnClick="ButtonSend_Click"
                                runat="server"></asp:LinkButton>                            
                        </div>
                    </div>
                    <div class="clear">
                        &nbsp;</div>
                </div>
                <!-- Footer -->
            </div>
        </div>
        <div class="box-bottom">
            &nbsp;</div>
    </div>
    <uc1:EmailConfirmation ID="ForgottenPasswordEmailConfirmation" runat="server" />
</div>
<!-- /LastCard -->