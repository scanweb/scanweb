// <copyright file="ChildrensAccommodationRules.ascx.cs" company="Sapient">
// Copyright (c) 2008 All Right Reserved
// </copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>16-April-2009</date>
// <version>1.0(Scanweb 1.6)</version>
// <summary>Contains code behind logic for the control ChildrensAccommodationRules.ascx</summary>

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    #region System NameSpaces
    using System;
    using System.Data;
    using System.Configuration;
    using System.Collections;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Web.UI.HtmlControls;
    #endregion // System NameSpaces

    /// <summary>
    /// This class will show the Children's accommodation
    /// criteria
    /// </summary>
    public partial class ChildrensAccommodationRules : System.Web.UI.UserControl
    {
        #region Protected Events
        /// <summary>
        /// Page_Load event of the ChildrensAccomodationRules Control
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event params</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion // Protected Events
    }
}