<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginStatus.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.LoginStatus" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Controller" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>

<script language="javascript" type="text/javascript">
    
    $(document).ready(function() {
        appendPrefix("txtLoyaltyUsernamePrefix");
		$("input[id$='txtLoyaltyUsername']").bind('focusout', trimwhitespace);
    });

	function trimwhitespace() {

            trimWhiteSpaces("txtLoyaltyUsername");
	    	replaceDuplicatePrefix("txtLoyaltyUsername");
	}
    
    
function checkLoginOrLogOut(param1, param2)
{ 
    var isLogin = <%= UserLoggedInSessionWrapper.UserLoggedIn.ToString().ToLower() %>;
    
    // BUG FIX: artf695610 | Login Pop Up is not shown for the user who is on Become a Member screen    
    if(isLogin == true)
    {
        return true;
    }
    else
    {
        hideLoginControl(param1, param2);
        return false;        
    }
}
</script>

<style type="text/css">
    div.BE a.buttonInner:focus
    {
        outline: 1px dotted Grey;        
    }
    #Login_Box .loginFlyout .loginFlyContLft input.chkBx:focus
    {
        outline: 1px dotted Grey;
    }
   /*styles added for logout corner by aleem*/
.logout-user { text-decoration: underline!important; float: right; font-weight: normal; font-size: 1.3em; margin: 15px 10px 0 0; }
.logout-user-drop {width: 20px; display: inline-block; margin-left: 5px; background-position: 0px -248px!important; height: 20px; float: right; }
.user-points { font-weight: bold; font-size: 1.1em; margin-right: 34px; float: right; clear: right; text-align:right; width:auto; }
.logout-flyout {background-color: rgb(255, 255, 255)!important; float: right!important; right: 10px; top: -18px; overflow: hidden; width: 225px!important;}
.font-large {font-size: 1.2em;}
.user-email { position: relative; top: -5px; color: rgb(153, 153, 153); font-size: 1.1em; }
.actBtns { margin-top: 4px; }
.actBtns .blutActBtn { margin-top: 2px; }
.blueBtn .buttonInner { background-color: #218DA3; }
.blueBtn .buttonInner:hover { background-color: #87B3B0; }
.greenBtn .buttonInner { background-color: #87B33A; }
.greenBtn .buttonInner:hover { background-color: #7AC465; }
.loginFlyout a {cursor: pointer;  }
.actBtns .buttonInner { padding: 2px 15px 4px;  }
#LogoutControl .loginFlyout hr { width: 100%;  }
#LogoutControl .loginFlyout { padding: 10px;  }
#loginClosed .wrapper { margin-right: 5px; }
</style>
<!-- As a part of adding new labels to fields we have removed the "input" class from
all the text form field (<input />) -->
<div id="Login" class="BE" onload="clearhistory()">
    
    <div id="LoginControl" class="LoginControl">
        <div class="LogoutUser" style="overflow: hidden;">
            <a href="#" runat="server" id="loggedinUserName" class="logout-user" visible="false">
            <%= LoggedinUserNameTop.Trim() %><span class="scansprite logout-user-drop"></span></a>
            <div class="user-points" id="divPoints" runat="server" visible="false">
            </div>
        </div>
        <div class="login_box" id="LogoutControl">
            <div style="" id="Logout_Box" class="loginFlyout roundMe fltLft loginCnt logout-flyout">
               <strong><span class="font-large" style="" id="spnUserNameLogout" runat="server">
                    </span></strong>
                <br>
                <span class="user-email" style="" id="spnLoggedInUserEmail" runat="server"></span>
                <br>
                <a class="font-large" id="EditProfile" runat="server"></a>
                <div class="clearAll"></div>
                <hr class="fltLft">
                <div style="" class="notMemb actBtns">
                    <div style="" class="blutActBtn fltLft blueBtn">
                        <asp:LinkButton runat="server" ID="MyPages" OnClick="MyPages_Click" class="buttonInner fltLft"></asp:LinkButton>
                    </div>
                    <div style="" class="blutActBtn fltRt greenBtn">
                        <asp:LinkButton runat="server" ID="LogoutLink" OnClick="LogOut" class="buttonInner fltLft"></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div id="loginClosed">
            <div class="hdLoginHm fltLft">
                <div class="cnt fltLft">
                    <div id="textCont" class="textCont" runat="server">
                        <div class="text fltLft" id="LoginForBestDeals" runat="server">
                            <strong class="fltLft">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/LoginToGetBestDeals") %>
                            </strong>
                        </div>
                        <div class="wrapper fltRt">
                            <div class="loginActBtn fltRt">
                                <!-- removing the sprite class and placing sprite scandic for branding project  -->
                                <asp:LinkButton runat="server" ID="LoginLink" OnClick="LogOut" class="buttonInner fltLft"></asp:LinkButton>
                                <asp:LinkButton runat="server" ID="spnLink" OnClick="LogOut" class="buttonRt scansprite fltLft"></asp:LinkButton>
                            </div>
                            <div class="blutActBtn fltLft mrgRt8" id="JoinHeader" runat="server">
                                <asp:LinkButton ID="lnkBtnJoinHeader" runat="server" OnClick="EnrollLoyalty_Click"
                                    CssClass="buttonInner fltLft">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/Join") %>
                                </asp:LinkButton>
                                <asp:LinkButton ID="spnBtnJoinHeader" runat="server" OnClick="EnrollLoyalty_Click"
                                    CssClass="buttonRt fltLft">                        
                                </asp:LinkButton>
                            </div>
                            <%--<div class="loginActBtn fltLft">
						                <a title="Login" href="javascript:__doPostBack('ctl00$MenuLoginStatus$LoginLink','')" id="ctl00_MenuLoginStatus_LoginLink" onclick="javascript:return checkLoginOrLogOut(this, 'Login_Box');" class="buttonInner sprite fltLft">Login</a><span class="buttonRt sprite fltLft"></span>
					                </div>--%>
                        </div>
                    </div>
                </div>
            </div>
            <!--
	             <ul class="list" id="ForgotClosedList" runat="server">
	                <li><a href="#" onclick="openPopupWin('<%= GlobalUtil.GetHelpPage(Utility.GetForgottenMembershipNoPageReference()) %>','width=400,height=300,scrollbars=yes,resizable=yes');return false;"><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/forgottenmembershipno") %></a></li>
                    <li><asp:LinkButton ID="ForgottenPasswordClosed" runat="server" OnClick="ForgottenPassword_Click" ><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/forgottenpasswordmessage") %></asp:LinkButton></li>
		        </ul>-->
        </div>
        <!--<div class="pointsInfo"><span id="placeholderPoint" runat="server"></span></div>-->
    </div>
    <div id="Login_Box" class="login_box">
        <div id="loginContainer" class="loginFlyout roundMe fltLft loginCnt" runat="server">
            <asp:LinkButton runat="server" ID="LoginCloseLink" OnClick="LogOut" class="scansprite blkClose">&nbsp;</asp:LinkButton>
            <p>
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/loginmessage") %></p>
            <br />
            <div class="clearAll">
            </div>
            <div id="loginArea" class="loginFlyContLft fltLft">
                <div class="userPass fltLft increasemrgbtm" id="userCredentials" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnLogIn.ClientID %>')">
                    <div>
                        <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
                        <input type="hidden" id="LoyaltyUserPasswordInvalid" name="LoyaltyUserPasswordInvalid"
                            value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/password") %>' />
                        <input type="hidden" id="LoyaltyUserNameInvalid" name="LoyaltyUserNameInvalid" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/user_name") %>' />
                        <input type="hidden" id="loginPopUpID" runat="server" />
                        <input type="hidden" id="loginPopUpPageID" runat="server" />
                    </div>
                    <div id="loyaltyLoginErrDiv" class="errorText" runat="server">
                    </div>
                    <div class="clearAll">
                    </div>
                    <div class="formFieldCol">
                        <label class="formLabel" for="<%= txtLoyaltyUsername.ClientID %>">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/MembershipNo") %></label>

                        <input type="text" class="fltLft defaultColor ignore userPrefix40" id="txtLoyaltyUsernamePrefix" 
                             name="txtLoyaltyUsernamePrefix" maxlength="10" runat="server" readonly="readonly" />

                        <input type="text" class="fltLft defaultColor ignore w85" id="txtLoyaltyUsername" tabindex="2"
                            name="txtLoyaltyUsername" maxlength="80" runat="server" />
                    </div>
                    <div class="loginPasswordWrapper">
                        <label class="formLabel" for="<%= txtLoyaltyPassword.ClientID %>">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/Password") %></label>
                        <asp:TextBox CssClass="defaultColor fltLft ignore" ID="txtLoyaltyPassword" TabIndex="4"
                            TextMode="Password" MaxLength="20" runat="server" />
                    </div>
                    <div id="LoginButtonDiv" class="usrBtn fltLft chromefixclearleft">
                        <label class="formLabel" for="">
                            &#160;</label>
                        <asp:LinkButton runat="server" ID="btnLogIn" class="buttonInner" TabIndex="6"><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/loginmessage") %></asp:LinkButton>
                    </div>
                </div>
                <div class="forgotCntnt fltLft ie7widtclass">
                    <a href="#" onclick="openPopupWin('<%= GlobalUtil.GetHelpPage(Utility.GetForgottenMembershipNoPageReference()) %>','width=400,height=300,scrollbars=yes,resizable=yes');return false;">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/forgottenmembershipno") %>
                    </a>
                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="ForgottenPassword_Click"><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/forgottenpasswordmessage") %></asp:LinkButton>
                </div>
                <div class="rememberMe fltRt chrmhelpiconfix">
                    <span class="fltLft" style="margin: 2px 3px 0 0; *margin: 0px 3px 0 0;">
                        <input type="checkbox" id="RememberUserIdPwd" name="RememberUserIdPwd" class="chkBx"
                            tabindex="5" runat="server" /></span>
                    <%--R2.0:artf1149134: Bhavya: Help text for Remember me is corrected to pick from right xml tag--%>
                    <span class="fltLft rememberText">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/RememberMe") %></span>
                    <span class="help spriteIcon toolTipMe ie9tip" title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/RememberMeToolTip") %>">
                    </span>
                </div>
                <div  id="LoginProgressDiv" runat="server" style="display: none">
                    <span class="fltLft lgnBoxProc">
                        <img src='<%=ResolveUrl("~/Templates/Booking/Styles/Default/Images/rotatingclockani.gif")%>'
                        alt="image" align="middle" width="25px" height="25px" />
                        <span>
                            <%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/Loading")%>
                        </span>
                    </span>
                </div>
            </div>
            
            <hr class="fltLft" />
            <div class="notMemb fltLft">
                <div class="blutActBtn fltRt">
                    <asp:LinkButton ID="lnkBtnJoin" runat="server" OnClick="EnrollLoyalty_Click" CssClass="buttonInner">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/Join") %>
                    </asp:LinkButton>
                    <asp:LinkButton ID="spnBtnJoin" runat="server" OnClick="EnrollLoyalty_Click" CssClass="buttonRt">                        
                    </asp:LinkButton>
                </div>
                <span class="fltRt">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/NotYetAMember") %>
                </span>
            </div>
        </div>
    </div>
</div>
