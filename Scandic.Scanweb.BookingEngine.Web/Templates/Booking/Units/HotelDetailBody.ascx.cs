//  Description					:   HotelDetailBody                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Linq;
using System.Text;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code behind of HotelDetailBody control
    /// </summary>
    public partial class HotelDetailBody : EPiServer.UserControlBase
    {
        /// <summary>
        /// Gets Name
        /// </summary>
        protected string Name { get; private set; }

        /// <summary>
        /// Gets Address
        /// </summary>
        protected string Address { get; private set; }

        /// <summary>
        /// Gets Teaser
        /// </summary>
        protected string Teaser { get; private set; }

        /// <summary>
        /// Gets HotelURL
        /// </summary>
        protected string HotelURL { get; private set; }

        /// <summary>
        /// Gets 
        /// </summary>
        protected string ImageURL { get; private set; }

        /// <summary>
        /// Gets ImageAlt
        /// </summary>
        protected string ImageAlt { get; private set; }

        /// <summary>
        /// Gets HotelID
        /// </summary>
        protected string HotelID { get; private set; }

        /// <summary>
        /// Gets PernightValue
        /// </summary>
        protected string PernightValue { get; set; }

        /// <summary>
        /// Gets HotelSearch
        /// </summary>
        public string HotelSearch { get; set; }

        /// <summary>
        /// Gets 
        /// </summary>
        protected string HotelName { get; private set; }

        public string stringFormat { get; set; }
        public string convertedRateString { get; set; }
        public string parseRateValue { get; set; }
        public string parseRateCurrency { get; set; }
        public string bonusCheckString { get; set; }

        public string TripAdvisorWidget { get; set; }
        public int TALocationID { get; set; }
        public bool HideHotelTARatings { get; set; }
        /// <summary>
        /// Gets Hotel
        /// </summary>
        public IHotelDisplayInformation Hotel
        {
            set
            {
                Address = value.Address;
                Name = value.HotelName;
                Teaser = value.Teaser;
                HotelURL = value.HotelURL;
                ImageURL = value.ImageSrc;
                ImageAlt = value.ImageAltText;
                HotelID = value.ID;
                PernightValue = value.Price;
                HotelSearch = value.HotelSearch;
                if (!string.IsNullOrEmpty(value.RateString))
                {
                    lblRateString.InnerText = value.RateString;
                }

                if (!string.IsNullOrEmpty(value.FromText))
                {
                    spnFromText.InnerText = value.FromText;
                }

                //Defect Id-430452 (Change the font of Bonus Cheque rate displayed in Select Hotel page.) -parvathi 
                if (!string.IsNullOrEmpty(value.Price))
                {
                    ExchangeRateManager exchangeRateManager = new ExchangeRateManager();
                    rateDisp.InnerText = value.Price;
                    stringFormat = "{0} {1}";
                    string disclaimerText = string.Empty;
                    string approxText = string.Empty;
                    disclaimerText = String.Format("{0}{1}{2}{3}{4}", AppConstants.BOLDSTART_TAG,
                                        WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/disclaimertextline1"),
                                        AppConstants.BOLDEND_TAG, AppConstants.BREAK_TAG,
                                        WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/disclaimertextline2"));
                    approxText = String.Format("{0}{1}", WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/about"), AppConstants.SPACE);
                    if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.BONUSCHEQUE)
                    {
                        parseRateValue = value.Price.Split(Convert.ToChar(AppConstants.PLUS))[1].Trim().Split(Convert.ToChar(AppConstants.SPACE))[0];
                        parseRateCurrency = value.Price.Split(Convert.ToChar(AppConstants.PLUS))[1].Trim().Split(Convert.ToChar(AppConstants.SPACE))[1];
                        convertedRateString = exchangeRateManager.GetExchangeRateString(stringFormat, parseRateCurrency, parseRateValue);
                        if (convertedRateString != string.Empty)
                        {
                            bonusCheckString = value.Price.Split(Convert.ToChar(AppConstants.PLUS))[0];
                            altRateDisplay.Attributes.Add("title", disclaimerText);
                            altRateDisplay.InnerText = String.Format("{0}{1}{2}{3}{4}", approxText, bonusCheckString, AppConstants.PLUS, AppConstants.SPACE, convertedRateString);
                        }
                        else
                        {
                            altRateDisplay.Visible = false;
                        }
                    }
                    else
                    {
                        convertedRateString = exchangeRateManager.GetExchangeRateString(stringFormat, value.Price.Split(Convert.ToChar(AppConstants.SPACE))[1],
                                                                            value.Price.Split(Convert.ToChar(AppConstants.SPACE))[0]);
                        if (convertedRateString != string.Empty)
                        {
                            altRateDisplay.Attributes.Add("title", disclaimerText);
                            altRateDisplay.InnerText = String.Format("{0}{1}", approxText, convertedRateString);
                        }
                        else
                        {
                            altRateDisplay.Visible = false;
                        }

                    }
                    if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.BONUSCHEQUE ||
                        SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REDEMPTION)
                        rateWrapper.Attributes.Add("class", "ratesFont");

                    else
                        rateWrapper.Attributes.Add("class", "rates");
                }


                if (!string.IsNullOrEmpty(value.ImageSrc))
                {
                    ImageURL = value.ImageSrc;
                }


                if (!string.IsNullOrEmpty(value.Distance))
                {
                    lblDistance.InnerHtml = "<strong>" +
                                            WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/distance") +
                                            " : </strong>" + value.Distance;
                    lblDistance.Visible = true;
                }

                if (!string.IsNullOrEmpty(value.Direction))
                {
                    lblDirection.InnerHtml = "<strong>" +
                                             WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/direction") +
                                             " : </strong>" + value.Direction;
                    lblDirection.Visible = true;
                }

                if (!string.IsNullOrEmpty(value.DrivingTime))
                {
                    lblDrivingTime.InnerHtml = "<strong>" +
                                               WebUtil.GetTranslatedText(
                                                   "/bookingengine/booking/selecthotel/drivingtime") + " : </strong>" +
                                               value.DrivingTime;
                    lblDrivingTime.Visible = true;
                }

                if (!string.IsNullOrEmpty(value.CityCenterDistance))
                {
                    lblCityCenterDistance.InnerHtml = value.CityCenterDistance;
                    lblCityCenterDistance.Visible = true;
                }
                if (!string.IsNullOrEmpty(value.HotelURL))
                {
                    lnkAddress.Attributes.Add("href", value.HotelURL);
                    lnkFullDescription.Attributes.Add("href", value.HotelURL);
                }

                lnkImageGallery.Attributes.Add("rel",
                                               GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") +
                                               "?methodToCall=GetImageGallery&HotelID=" + value.ID + "&HotelName=" +
                                               value.HotelName);

                if (null != value.RateTitle)
                {
                    RateTitle.Visible = true;

                    ////Merchandising:R3:Display ordinary rates for unavailable promo rates 
                    //string strRateTitle = string.Empty;
                    //if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode != null
                    //    && SearchCriteriaSessionWrapper.SearchCriteria.SearchingType != SearchType.REGULAR)
                    //    strRateTitle = Utility.TruncateAndAppendDots(value.RateTitle, 14);
                    //else
                    string strRateTitle = string.Empty;
                    strRateTitle = value.RateTitle;

                    lblRateTitle.InnerText = strRateTitle;
                    string classname = ratesHolder.Attributes["class"];
                    string formattedClassName = classname + " " + "ratesDNumber";
                    ratesHolder.Attributes.Add("class", formattedClassName);
                }

                if (!string.IsNullOrEmpty(value.NoDiscount))
                {
                    NoDiscount.Visible = true;
                    lblNoDiscount.InnerText = value.NoDiscount;
                }
                TALocationID = value.TALocationID;

                if (!string.IsNullOrEmpty(value.TripAdvisorWidget))
                    TripAdvisorWidget = value.TripAdvisorWidget;

                HideHotelTARatings = value.HideHotelTARatings;
            }
        }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            imgHotel.Src = ImageURL;
            imgHotel.Alt = ImageAlt;
            if (Visible)
            {
                string spAlert = ContentDataAccess.GetSpecialAlert(HotelID, false);
                if (!string.IsNullOrEmpty(spAlert))
                {
                    divSpAlertWrap.Visible = true;
                    divSpAlert.InnerHtml = spAlert;
                }
                else
                {
                    divSpAlertWrap.Visible = false;
                }

                if (!WebUtil.HideTARating(false, this.HideHotelTARatings))
                    divTripAdvisorRating.InnerHtml = WebUtil.CreateTripAdvisorRatingWidget(Convert.ToInt32(TALocationID));
                else
                    divTripAdvisorRating.InnerHtml = "";
            }
        }

        /// <summary>
        /// Checks whether the unblock is required or not.
        /// </summary>
        /// <returns></returns>
        public string IsUblockRequired()
        {
            return Convert.ToString(WebUtil.IsUblockRequired(HotelID)).ToLower();
        }

    }
}