<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChildrensAccommodationRules.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ChildrensAccommodationRules" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>

<!-- Main container div -->
<div class="BE" id="Reservation">

    <!-- Children occupancy rules -->
    <div class="occupancyHolder">
        <p><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/occupancycalc")%></p>
        <dl>
            <dd class="age"><strong><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/age")%></strong></dd>
            <dd class="TOA"><strong><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/typeofaccommodation")%></strong></dd>
            <dd class="occupancy"><strong><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/occupancy")%></strong></dd>
        </dl>

        <dl>
            <dd class="age"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/agerange1")%></dd>
            <dd class="TOA"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/cot")%></dd>
            <dd class="occupancy"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/occupy")%></dd>
        </dl>

        <dl>
            <dd class="age"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/agerange1")%></dd>
            <dd class="TOA"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/childinadultsbed")%></dd>
            <dd class="occupancy"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/nooccupy")%></dd>
        </dl>
        
        <dl>
            <dd class="age"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/agerange2")%></dd>
            <dd class="TOA"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/extrabed")%></dd>
            <dd class="occupancy"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/occupy")%></dd>
        </dl>
        
        <dl>
            <dd class="age"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/agerange2")%></dd>
            <dd class="TOA"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/childinadultsbed")%></dd>
            <dd class="occupancy"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/nooccupy")%></dd>
        </dl>
        
        <dl>
            <dd class="age"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/agerange3")%></dd>
            <dd class="TOA"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/extrabed")%></dd>
            <dd class="occupancy"><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/occupy")%></dd>
        </dl>
        <dl class="noteText">
		    <dd><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/childperadult")%></dd>
		</dl>
    </div>
    <!-- /Children occupancy rules -->
</div>
<!-- /Main container div -->