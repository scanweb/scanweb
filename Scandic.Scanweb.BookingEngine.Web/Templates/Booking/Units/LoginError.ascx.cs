////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: Code behind class for Error Login user control    	  //
//								  This user control is used when user enters wrong user   //
//                                Name and password.This control does same functionality  //  
//                                as loginstatus.ascx except this control gives user the  //  
//                                opportunity for new registration.It gives second        //
//                                opportunity for entering user name and password.        //
//----------------------------------------------------------------------------------------//
//  Author						: Raj Kishore Marandi	                                  //
//  Author email id				:                           							  //
//  Creation Date				: 13th November  2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revision History				: -NA-											      //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespace
using System;
using EPiServer.Core;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.Core;

#endregion System Namespace

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// This class is responsible for authenticate user and fetching nameid using userid. 
    /// Using this nameid it gets user details and populate the session for further use.
    /// </summary>
    public partial class LoginError : EPiServer.UserControlBase
    {
        #region PROTECTED METHOD

        #region PageLoad

        /// <summary>
        /// Fired when page is loaded.
        /// </summary>
        /// <param name="sender">Contains Sender name</param>
        /// <param name="e">Contains event Arguments</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                LoginErrorDiv.InnerText = "";
                if (!IsPostBack)
                {
                    RememberUserIdPwd.Checked = true;
                    if (!UserLoggedInSessionWrapper.UserLoggedIn)
                    {
                        if (WebUtil.IsCookieExists(AppConstants.DONTREMEMBERME_COOKIE))
                            RememberUserIdPwd.Checked = false;
                        else
                            WebUtil.SetCookieToTextBox(txtLoginErrorUserNamePrefix, txtLoginErrorUserName, txtLoginErrorPassword, RememberUserIdPwd);
                    }
                    btnLogonButton.PostBackUrl = WebUtil.LoginValidatorUrl();
                    spnLogonButton.PostBackUrl = WebUtil.LoginValidatorUrl();
                    btnLogonButton.Attributes.Add("onclick", "javascript:return performValidation(PAGE_LOGIN_ERROR);");
                    spnLogonButton.Attributes.Add("onclick", "javascript:return performValidation(PAGE_LOGIN_ERROR);");

                    var loginSourceModule = CurrentPage.PageLink.ID ==
                                                          Utility.GetLoginForDeepLinkPageId()
                                                              ? LoginSourceModule.LOGIN_FOR_DEEPLINK
                                                              : LoginSourceModule.LOGIN_ERROR_MODULE;
                    Utility.SetLoginDemographics(txtLoginErrorUserNamePrefix.Name, txtLoginErrorUserName.Name, txtLoginErrorPassword.UniqueID,
                                             RememberUserIdPwd.Name, loginSourceModule, loginErrPageID.Name);

                    loginErrID.Value = CurrentPage.PageLink.ID ==
                                                              Utility.GetLoginForDeepLinkPageId()
                                                              ? LoginSourceModule.LOGIN_FOR_DEEPLINK.ToString()
                                                              : LoginSourceModule.LOGIN_ERROR_MODULE.ToString();

                    if (LoginErrorCodeSessionWrapper.LoginPopupErrorCode == 6 || LoginErrorCodeSessionWrapper.LoginPopupErrorCode == 8)
                    {
                        Utility.ShowErrorMessageForAccordian(LoginErrorDiv,
                            WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/LoginErrorMessage"));
                        LoginErrorCodeSessionWrapper.LoginPopupErrorCode = 0;
                    }
                }

                PageData pageData = GetPage(PageReference.RootPage);
                int frequentGuestProgrammeID =
                    ((PageReference)pageData[EpiServerPageConstants.FREQUENT_GUEST_PROGRAMME]).ID;
                if (CurrentPage.PageLink.ID == frequentGuestProgrammeID)
                    ExistingMember.Visible = true;
            }
        }

        #endregion PageLoad

        #region Forgotten Password Link Clicked

        /// <summary>
        /// Fired when Forgotten password link is clicked.
        /// It will redirect to Forgotten password page where user can retrieve forgotten password.
        /// </summary>
        /// <param name="sender">Sender name</param>
        /// <param name="e">Event arguments</param>
        protected void btnForgotenPassword_Click(object sender, EventArgs e)
        {
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.FORGOTTEN_PASSWORD), false);
        }

        #endregion Forgotten Password Link Clicked

        #endregion PROTECTED METHOD
    }
}
