<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InviteAFriend.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.InviteAFriend" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register Src="EmailConfirmation.ascx" TagName="EmailConfirmation" TagPrefix="uc1" %>
<script type="text/javascript" language="javascript">

function addFriend()
{   
    var firstName   = "<%= WebUtil.GetTranslatedText("/bookingengine/booking/inviteafriend/friendname") %>";
    var emailId     = "<%= WebUtil.GetTranslatedText("/bookingengine/booking/inviteafriend/emailaddress") %>";
    var timeNow     = Math.random();
    var str         = "";
    
    str             = str + "<div class = 'columnOne'>";
    str             = str + "<p class = 'formRow'>";
    str             = str + "<span id = 'spanCol1" + timeNow + "'>";
    str             = str + "<label id = 'lblCol1" + timeNow +"'>" + firstName + "</label>";
    str             = str + "</span>";
    str             = str + "<br />";
    str             = str + "<input type = 'text' name = 'Fname' id = 'txtCol1" + timeNow + "'class = 'frmInputText' maxlength='40' />";
    str             = str + "</p>";
    str             = str + "</div>";
    
    str             = str + "<div class = 'columnTwo'>";
    str             = str + "<p class = 'formRow'>";
    str             = str + "<span id = 'spanCol2" + timeNow + "'>";
    str             = str + "<label id = 'lblCol2" + timeNow + "'>" + emailId + "</label>";
    str             = str + "</span>";
    str             = str + "<br />";
    str             = str + "<input type = 'text' name = 'Email2' id = 'txtCol2" + timeNow + "'class = 'frmInputText' maxlength='4000'/>";
    str             = str + "</p>";
    str             = str + "</div>";
    
    str             = str + "<div class = 'clear'>&nbsp;</div>";
	
	$fn(_endsWith('friendsDetails')).innerHTML = $fn(_endsWith('friendsDetails')).innerHTML + str;
}

</script>

<div id="Loyalty" class="BE">
	<!-- LastCard -->
	<div id="InviteAFriendDiv" runat="server">
	<div id="InviteFriend">
		<div class="box-top-grey"><span class="title"><%= WebUtil.GetTranslatedText("/bookingengine/booking/inviteafriend/header") %></span></div>
		<div class="box-middle">
			<div class="content">
			<div id="ErrorInvAFrnd" class="errorText"  runat="server"></div>
			            <input type="hidden" id="InviteAFriendErrMsgTitle" name="InviteAFriendErrMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
			            <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
			            <input type="hidden" id="InvalidFriendName" name="InvalidFriendName" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/friend_name") %>' />
			            <input type="hidden" id="InvalidEmail" name="InvalidEmail" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/email_address") %>' />
	                    <input type="hidden" id="storage" name="storage" runat="server"/>
				<div id="friendsDetails" runat="server">
				    <div class="columnOne">
							<p class="formRow">
							    <span id="spanCol1">
								<label id="lblCol1"><%= WebUtil.GetTranslatedText("/bookingengine/booking/inviteafriend/friendname") %></label>
								</span>
								<br />
								<input type="text" id="txtCol1" name="txtCol1" class="frmInputText" maxlength="40" />
							</p>
						</div>
						<div class="columnTwo">
							<p class="formRow">
							    <span id="spanCol2">
								<label id="lblCol2"><%= WebUtil.GetTranslatedText("/bookingengine/booking/inviteafriend/emailaddress") %></label>
								</span>
								<br />
								<input type="text" id="txtCol2" name="txtCol2" class="frmInputText" maxlength="4000"/>
							</p>
						</div>
						<div class="clear">&nbsp;</div>
						</div>
					<div id="addFriend" runat="server">
						<p><span class="showLink">
						<a href="#" onclick="addFriend();return false;"><%= WebUtil.GetTranslatedText("/bookingengine/booking/inviteafriend/addanotherfriend") %></a> 
						   </span></p>
					</div>
					<div><%= WebUtil.GetTranslatedText("/bookingengine/booking/inviteafriend/textareaheader") %></div>
					<div class="message">
						<p class="formRow">
							<textarea rows="5" class="frmInputTextAreaBig" id="txtMessage" name="txtMessage" runat="server"></textarea>
						</p>
					</div>
					<!-- /details -->
				<!-- Footer -->
				<div id="FooterContainer">
					<div class="alignRight">
						<span class="btnSubmit">
						<asp:LinkButton ID="btnInviteFriend" runat="server" OnClick="btnInviteFriend_Click"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/inviteafriend/button") %></span></asp:LinkButton>
						</span>
					</div>
					<div class="clear">&nbsp;</div>
				</div>
				<!-- Footer -->
			</div>
		</div>
		<div class="box-bottom">
            
	</div>
	<!-- /LastCard -->
	</div>
	  </div>
	<uc1:EmailConfirmation ID="EmailConfirmationMsg" runat="server" />
            &nbsp;
          
</div>