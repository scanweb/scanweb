<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingKids.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.BookingKids" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register Src="~/Templates/Booking/Units/UsefulLinksContainer.ascx" TagName="UsefulLinks" TagPrefix="Booking" %>
<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx" TagName="ReservationInformation" TagPrefix="Booking" %>

<!-- Main container div -->
<div class="BE">
    
    <!-- Children's details container div -->
    <div id="ChildrensDetails">
        
        <!-- Hidden inputs -->
        <input id="childrenInfoHidden" type="hidden" value="" class="hiddenTxt" runat="server"/>
        <input id="childrenCriteriaHidden" type="hidden" value="" class="hiddenTxt" runat="server"/>
        <input id="noOfChildrenHidden" type="hidden" value="" class="hiddenTxt" runat="server"/>
        <input id="noOfAdults" type="hidden" value="" class="hiddenTxt" runat="server"/>
        <input id="errMessage" name="errMessage" type="hidden" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalidbedtypechoosen") %>' />
        <input id="CIPBString" name="CIPBString" type="hidden" value="<%=
                WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/sharingbed") %>"/>
        <input id="InvalidKidsAge" name="InvalidKidsAge" type="hidden" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidAge") %>'/>
        <input id="InvalidBedType" name="InvalidBedType" type="hidden" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidBedType") %>'/>
        
        <!-- /Hidden inputs -->
     
        <!-- Progress Tab -->
        <div id="progTab">
          <div id="pt_1" >
            <ul>
              <li class="pt_num one">&nbsp;</li>
              <li class="text active"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/childrendetail") %> </li>
              <li class="pt_arrow1 activeArrow">&nbsp;</li>
              
              <li  class="pt_num two">&nbsp; </li>
              <li class="text"> <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selecthotel") %> </li>
              <li class="pt_arrow1"> &nbsp;</li>
              
              <li  class="pt_num three">&nbsp; </li>
              <li class="text"> <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selectrate") %> </li>
              <li class="pt_arrow1"> &nbsp;</li>
              
              <li  class="pt_num four">&nbsp; </li>
              <li class="text"> <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/bookingdetails") %> </li>
              <li class="pt_arrow1"> &nbsp;</li>
              
              <li  class="pt_num five">&nbsp; </li>
              <li class="text"> <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/confirmation") %></li>
              <li class="pt_last"> &nbsp;</li>
            </ul>
          </div>
          <div class="clear">&nbsp;</div>
        </div>
        <!-- /Progress Tab -->
        
        <!-- Reservation information and useful links controls -->
        <Booking:ReservationInformation ID="ReservationInfo" runat="server">
        </Booking:ReservationInformation>
        <Booking:UsefulLinks ID="UsefulLinks" runat="server">
        </Booking:UsefulLinks>
        <!-- /Reservation information and useful links controls -->
        
        <!-- Childrens room requirements plain text -->
        <div><h2 class="lightHeading"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/childrendetail") %></h2></div>
        <!-- /Childrens room requirements plain text -->
        <!-- Div to show Error messages -->
        <div id="kidsErrorDiv" runat="server"></div>
        <!-- /Div to show Error messages -->
        <!-- Container on which all the children's accommodation types has been chosen dynamically -->
        <div id="selectContainer">
        <table class="kidsDDHolder" runat="server" id="tableChildrenAcc" border="0" cellpadding="5" cellspacing="0"></table>
        </div>
        <!-- Children's accommodation rules displaying control -->
        <div>
            <%= CurrentPage["ChildrenOccupancyRule"] %>
        </div>
        <!-- /Children's accommodation rules displaying control -->
        <!-- /Container on which all the children's accommodation types has been chosen dynamically -->

        <div class="btnHolder">
        <div id="Div1" class="errorText"></div>
            <!-- Continue button -->
            <!--Naresh AMS Patch7 artf1258221 : Scanweb - Issue regarding "back"-function -->
            <div class="buttonContainer">
		        <span class="btnSubmit">
		            <a id="btnBack" href="javascript:history.go(-1)" runat="server"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/back") %></span></a>
		        </span>
	        </div>
	        <!-- /Continue button -->
        	
        	<!-- Back button -->
            <div class="formRowButton">
                <span class="btnSubmit">
                <asp:LinkButton ID="BtnContinueKidsPage" OnClick="OnContinue" runat="server"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/search") %></span></asp:LinkButton>
                </span>
            </div>
            <!-- /Back button -->
        </div>

        <!-- Childish attractions -->
        <div class="childAttrHolder">
            <EPiServer:Property PropertyName="ChildAttraction" ID="ChildAttraction" Visible="true" runat="server" />
        </div>
        <!-- /Childish attractions -->
    </div>
    <!-- /Children's details container div -->
</div>
<!-- /Main container div -->