<%@ Control Language="C#" AutoEventWireup="true" Codebehind="SelectRate.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.SelectRate" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<!--%@ Register Src="~/Templates/Booking/Units/UsefulLinksContainer.ascx" TagName="UsefulLinks" TagPrefix="Booking" %-->
<!--%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx" TagName="ReservationInformation" TagPrefix="Booking" %-->
<%--This is commented as its found that its not being used in the page - Bhavya --%>
<%--<%@ Register Src="~/Templates/Booking/Units/DateCarsouel.ascx" TagName="DateCarsouel"
    TagPrefix="Booking" %>--%>
<%@ Register Src="~/Templates/Booking/Units/RoomTabControl.ascx" TagName="RoomTab"
    TagPrefix="Booking" %>
<%@ Register TagPrefix="Scanweb" TagName="ProgressBar" Src="~/Templates/Booking/Units/ProgressBar.ascx" %>
<%@ Register Src="~/Templates/Booking/Units/RoomDetailContainer.ascx" TagName="RoomDetailContainer"
    TagPrefix="Booking" %>
<%@ Register Src="~/Templates/Booking/Units/HotelNameContainer.ascx" TagName="HotelNameContainer"
    TagPrefix="Booking" %>
<!-- Shameem: 22 june : Add user contrl to show Image and description for HOtel -->
<%@ Register Src="~/Templates/Booking/Units/RoomDescriptionPopup.ascx" TagName="PopupRoomDescription"
    TagPrefix="Booking" %>
<!-- Release:Reservation R2.0 - Bhavya - AvalibilityCalenderControl - Generates the Availability calenderItem  controls -->  
<%@ Register Src ="~/Templates/Booking/Units/AvailabilityCalendar.ascx" TagName ="AvailabilityCalendar" TagPrefix ="SelectRate" %>



<Scanweb:ProgressBar id="ProgressBar" runat="server" />
<div class="BE">
    <!-- Step2: Select Rate -->
    <div id="SelectRate">
        <!-- Progress Tab -->
        <div id="progTab">
            <!-- If the booking is done with out kids i.e. no of children = 0, 
	    then the div with "pt_1" would be displayed. 
	    Or else if the no of children > 0, then the div with "pt_2" would be displayed. -->
            <% if (!Utility.IsFamilyBooking())
               {%>
            <div id="pt_2">
                <%-- <ul>
                    <li id="lnkSelectHotel1" class="pt_num firstTick" runat="server">&nbsp; </li>
                    <li id="lnkSelectHotel2" class="text visited" runat="server"><a href="<%=GlobalUtil.GetUrlToPage("ReservationSelectHotelPage")%>">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selecthotel")%>
                    </a></li>
                    <li id="lnkSelectHotel3" class="pt_arrow1 visitedArrow" runat="server">&nbsp;</li>
                    <li id="lblSelectHotel1" class="pt_num firstTick_Disabled" runat="server" visible="false">
                        &nbsp; </li>
                    <li id="lblSelectHotel2" class="text" runat="server" visible="false">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selecthotel")%>
                    </li>
                    <li id="lblSelectHotel3" class="pt_arrow1 disabledArrow" runat="server" visible="false">
                        &nbsp;</li>
                    <li class="pt_num two">&nbsp; </li>
                    <li class="text active">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selectrate")%>
                    </li>
                    <li class="pt_arrow1 activeArrow">&nbsp;</li>
                    <li class="pt_num three">&nbsp;</li>
                    <li class="text">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/bookingdetails")%>
                    </li>
                    <li class="pt_arrow1">&nbsp;</li>
                    <li class="pt_num four">&nbsp;</li>
                    <li class="text">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/confirmation")%>
                    </li>
                    <li class="pt_last">&nbsp;</li>
                </ul>--%>
            </div>
            <% }
               else
               { %>
            <div id="pt_3">
                <ul>
                    <%--<li class="pt_num firstTick">&nbsp;</li>
                    <li class="text visited"><a href="<%=GlobalUtil.GetUrlToPage("ReservationChildrensDetailsPage")%>">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/childrendetail")%>
                    </a></li>
                    <li class="pt_arrow1 lastvisitedArrow">&nbsp;</li>
                    <li class="pt_num tick">&nbsp;</li>
                    <li class="text visited"><a href="<%=GlobalUtil.GetUrlToPage("ReservationSelectHotelPage")%>">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selecthotel")%>
                    </a></li>
                    <li class="pt_arrow1 visitedArrow">&nbsp;</li>
                    <li class="pt_num three">&nbsp; </li>
                    <li class="text active">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selectrate")%>
                    </li>
                    <li class="pt_arrow1 activeArrow">&nbsp;</li>
                    <li class="pt_num four">&nbsp;</li>
                    <li class="text">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/bookingdetails")%>
                    </li>
                    <li class="pt_arrow1">&nbsp;</li>
                    <li class="pt_num five">&nbsp;</li>
                    <li class="text">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/confirmation")%>
                    </li>
                    <li class="pt_last">&nbsp;</li>--%>
                </ul>
            </div>
            <% } %>
            <%--  <div class="clear">
                &nbsp;</div>--%>
        </div>
        <!--Progress Tab -->
        <!-- Details -->
        <input type="hidden" id="txtShowAll" value="FALSE" runat="server" />
        <input type="hidden" id="txtShowAllRoomTypes" value="" runat="server" />
        <input type="hidden" id="txtPerStay" value="" runat="server" />
        <input type="hidden" id="txtRoomCategoryID" runat="server" />
        <input type="hidden" id="txtRateCategory" runat="server" />
        <input type="hidden" id="txtRoomNumber" runat="server" class="sessionRoomNumber" />
        <input type="hidden" id="currentSelectedTab" runat="server" />
        <input type="hidden" id="boolFirstTimeCalendar" runat="server" />
        <input type="hidden" id="stepCount" runat="server" />
        <input type="hidden" id="visibleCount" runat="server" />
        <input type="hidden" id="leftVisibleCarouselIndex" runat="server" />
        <input type="hidden" id="selectedCalendarItemIndex" runat="server" class="selectedCalendarItemIndex"/>        
        <input type="hidden" id="firstBatch" runat="server" class="firstBatch" />
        <input type="hidden" id="lastBatch" runat="server" class="lastBatch" />

        <div style="display: none;" id="Roomflag">
        </div>
        <div id="Header">
            <div class="hdrCnt fltLft">
                <Booking:HotelNameContainer ID="HotelName" runat="server">
                </Booking:HotelNameContainer>
                <div class="formCnt fltRt clearMgnTop">
                    <strong class="fltLft">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/prices") %>
                    </strong>
                    <asp:RadioButton ID="perNight" Text="" TabIndex="10" GroupName="groupPrices" runat="server"
                        CssClass="radio fltLft" AutoPostBack="true" EnableViewState="true" OnClick="javascript:setPerNightSelectedOnSelectRatePage(false);"
                        OnCheckedChanged="ShowRoomTypesPerCondition" />
                    <asp:RadioButton ID="perStay" Text="" Checked="False" TabIndex="12" GroupName="groupPrices"
                        runat="server" CssClass="radio fltLft" AutoPostBack="true" EnableViewState="true" OnClick="javascript:setPerNightSelectedOnSelectRatePage(true);"
                        OnCheckedChanged="ShowRoomTypesPerCondition" />
                        <span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/PopupPerNightPerStay") %>" class="help spriteIcon toolTipMe fltLft" style="position:relative;"></span>
                </div>  
                 
                   <!-- BEGIN New Opera Message -->
                <div class="clearAll"><!-- Clearing Float --></div>
                <div id="errorDiv" class="infoAlertBox infoAlertBoxRed" runat="server" visible="false">
                    <div class="threeCol alertContainer">
                        <div class="hd sprite">&#160;</div>
                        <div class="cnt sprite">
                           <span class="alertIconRed">&#160;</span>
                           <div class="descContainer" id="Div2" runat="server">
                                <label class="titleRed"><%= WebUtil.GetTranslatedText("/bookingengine/booking/Rate48HRS/PleaseNote") %></label>
                                <label id="operaErrorMsg" runat="server" class="infoAlertBoxDesc"></label>
                           </div>
                           <div class="clearAll">&#160;</div>
                        </div>
                        <div class="ft sprite">&#160;</div>
                    </div>
                 </div>
                 <!-- END New Opera Message -->             
                 
                              
            </div>
				<!-- BEGIN Special Alert -->
				<div id="divSpAlertWrap" class="selectratealertwrapper" runat="server">
					<div class="splalertcontent" runat="server" id ="divSpAlert">
					</div>
				</div>
				<!-- END Special Alert -->
        </div>
        
        <!--artf1148604 : Wrong error message text and formatting :Rajneesh -->
       <%-- <div class="infoAlertBox" visible="false" runat="server" id="divSelectRatePageErrorMessage">
        <div class="threeCol alertContainer">
         
            <div class="hd sprite">
                &nbsp;</div>
            <div class="cnt sprite">
                <p class="scansprite">                                       
                    <label id="lblWrongCodeError" runat="server"/>
                </p>
            </div>
            <div class="ft sprite">
            </div>
        </div>
    </div>--%>      
    <div class="infoAlertBox infoAlertBoxRed" id="errorAlertDiv" runat="server" Visible="false">
                    <div class="threeCol alertContainer">
                        <div class="hd sprite">&nbsp;</div>
                        <div class="cnt sprite">
                           <span class="alertIconRed">&nbsp;</span>
                           <div id="ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_Div1" class="descContainer">
                                <label class="titleRed"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/PleaseNote") %></label>
                                <label id="errorLabel" runat="server" class="infoAlertBoxDesc"></label>
                                    
    </div>       
                           <div class="clearAll">&nbsp;</div>
                        </div>
                        <div class="ft sprite">&nbsp;</div>
                    </div>               
        
        </div> 

        
        
        
        <div id="divSelectRateMainBody" runat="server">
<%--          <Booking:DateCarsouel ID="DateCarsouel" runat="server">
            </Booking:DateCarsouel>--%>
            <!-- Release:Reservation R2.0 - Bhavya - AvalibilityCalenderControl - Generates the Availability calenderItem  controls -->  
           <div style="display: block;" class="calendarLoadIndicator" id="CalendarProgressDiv">
                <img src="<%= ResolveUrl("~/templates/Scanweb/Styles/Default/Images/ajax-loader_big.gif") %>" alt="image" align="top" />
                <span class="ploadingText"><%=
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Units/Static/FindHotelSearch/Loading") %></span>
            </div>
            <div style="display: none;" id="CalendarData">
                <SelectRate:AvailabilityCalendar ID="availabilityCalendarControl" runat="server"></SelectRate:AvailabilityCalendar>           
            </div>

            <div class="selectRoom" id="TabStructure">
                <Booking:RoomTab ID="RoomTab" runat="server">
                </Booking:RoomTab>
                <div class="RRWrapper">
                    <div id="blkRoomDetailContainer" runat="server">
                    </div>
                    <div class="topCntOverLay">
                        <div class="ratesOverLayCnt">
                        </div>
                        <div class="ratesOverLay roundMe">
                            <div class="hd sprite">
                            </div>
                            <div class="cnt">
                                <p id="selectedRoom">
                                </p>
                                <div class="HR">
                                </div>
                                <p>
                                    <strong id="nextRoom"></strong>
                                </p>
                            </div>
                            <div class="ft sprite">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- header -->
                <!-- header -->
                <!-- Room Detail Container -->
            </div>
            <!-- header -->
            <!-- header -->
            <!-- Room Detail Container -->
            <!-- /Room Detail Container -->
            <!-- Room Detail Links -->
            <!-- /Room Detail Links-->
            <!-- Footer -->
            <!-- Footer -->
        </div>
    </div>
    <!-- \Step2: Select Rate -->
    <!-- To Display Lightbox for Moadal Pop up window -->
    <div class="jqmWindow dialog imgGalPopUp" id="SelectedRoom" runat="server">
        <div class="hd sprite">
        </div>
        <div class="cnt roomDescription">
            <a href="#" class="jqmClose scansprite blkClose"></a>
            <div class="LightBoxCntnt" id="RoomCategoryContainer" runat="server">
            </div>
        </div>
        <div class="ft sprite">
        </div>
    </div>
    <!--Naresh AMS Patch7 artf1258221 : Scanweb - Issue regarding "back"-function -->
     <div class="bkingDetBtmLinks">	
	<div class="goBackLink fltLft">
			<a id="btnBack" href="javascript:history.go(-1)" runat="server" visible="false"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/goback") %></a>
		</div>
	</div>
    <!--End of Add Room description poput user control -->
</div>
<!-- /Reservation Div -->

<script type="text/javascript" language="javascript">
    window.scrollBy(0,800); // horizontal and vertical scroll increments 	
    var requestUrl = <%= "\"" + GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") + "\"" %>;

    $(document).ready(function() {
        FetchAvailabilityCalenders(false);
        var tab = '<%=roomNumber%>';
        //defect id: artf1150702 added delay switch
        selectRatemoveRoom(tab, true,true);
    	
    	var roomShowTypeArray = $fn(_endsWith("txtShowAllRoomTypes")).value.toString().split(',');
		
		for( var roomCount=0; roomCount < roomShowTypeArray.length - 1; roomCount++) {
			$(roomShowTypeArray[roomCount] + " .HiddenRows").show();
			$(roomShowTypeArray[roomCount] + ".usrBtn a[rel='showAll']").hide();
			}
    });
    
    window.onunload = function(){};
</script>

