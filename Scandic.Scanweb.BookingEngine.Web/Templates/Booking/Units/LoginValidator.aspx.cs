//  Description					: LoginValidator                                          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using Scandic.Scanweb.Core.Encryption;
using Scandic.Scanweb.Core.Encryption.Interface;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code behind of LoginValidator page.
    /// </summary>
    public partial class LoginValidator : System.Web.UI.Page, INavigationTraking
    {
        LoginSourceModule currentModule = LoginSourceModule.NONE;
        #region Page_Load

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            NameController nameController = new NameController();
            string prefix = string.Empty;
            string userName = string.Empty;
            string passwd = string.Empty;
            string doStoreCookie;
            string redirectPageURL = string.Empty;

            try
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    if (UserLoggedInSessionWrapper.UserLoggedIn)
                    {
                        #region Logout Operation
                        nameController.Logout();
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.LOGOUT_CONFIRMATION), false);
                        Reservation2SessionWrapper.IsLoginFromBookingPageLoginOrTopLogin = false;
                        Reservation2SessionWrapper.BookingModuleActiveTab = false;
                        SearchCriteriaSessionWrapper.SearchCriteria = null;
                        #endregion Logout Operation
                    }
                    else
                    {
                        #region Login Operation
                        if ((LoginDemographicsSessionWrapper.LoginDemographics == null) || (LoginDemographicsSessionWrapper.LoginDemographics.Count <= 0))
                        {
                            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.LOGIN_ERROR), false);
                        }
                        else
                        {
                            currentModule = GetModuleWhereLoginPerformed(LoginDemographicsSessionWrapper.LoginDemographics);

                            #region Redirecting the user to required page post logon.

                            if (currentModule != LoginSourceModule.NONE)
                            {
                                redirectPageURL = GetRedirectPageURL(currentModule);
                                prefix = Request.Form[LoginDemographicsSessionWrapper.LoginDemographics[currentModule].UserNameControlPrefix];
                                userName = prefix + Request.Form[LoginDemographicsSessionWrapper.LoginDemographics[currentModule].UserNameControl];
                                passwd = WebUtil.GetActualPassword(Request.Form[LoginDemographicsSessionWrapper.LoginDemographics[currentModule].PasswordControl], userName);
                                UserNavTracker.TrackAction(this, "Login");

                                doStoreCookie = Request.Form[LoginDemographicsSessionWrapper.LoginDemographics[currentModule].RememberMeChkBox];
                                bool status = false;
                                if (doStoreCookie != null && doStoreCookie == "ON".ToLower())
                                {
                                    status = true;
                                }
                                WebUtil.RememberUserIdPassword(userName, passwd, status);

                                nameController.Login(userName, passwd);
                                if (currentModule == LoginSourceModule.LOGIN_ERROR_MODULE)
                                    Reservation2SessionWrapper.IsLoginFromFGP = true;
                                else
                                    Reservation2SessionWrapper.IsLoginFromUpperRight = true;

                                LoginErrorCodeSessionWrapper.LoginErrorCode = 0;

                                if (redirectPageURL.Equals("/Reservation/Select-Rate/"))
                                    Response.Redirect(redirectPageURL + "?SR-LogIn=false", false);
                                else
                                    Response.Redirect(redirectPageURL, false);
                            }
                            if (currentModule == LoginSourceModule.BOOKING_SEARCH_MODULE)
                                Reservation2SessionWrapper.BookingModuleActiveTab = true;

                            if (currentModule == LoginSourceModule.BOOKING_DETAIL_MODULE)
                            {
                                Reservation2SessionWrapper.IsLoginFromBookingPageLoginOrTopLogin = true;
                            }
                            if (currentModule == LoginSourceModule.NONE)
                            {
                                Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.LOGIN_ERROR), false);
                            }
                            #endregion Redirecting the user to required page post logon.
                        }

                        #endregion Login Operation
                    }
                }
            }
            catch (OWSException owsEx)
            {
                UserLoggedInSessionWrapper.UserLoggedIn = false;
                //**AppLogger.LogCustomException(owsEx, AppConstants.OWS_EXCEPTION);
                if (currentModule == LoginSourceModule.BOOKING_DETAIL_MODULE)
                {
                    LoginErrorCodeSessionWrapper.LoginErrorCode = 2;
                    Response.Redirect(redirectPageURL);
                }
                else if (currentModule == LoginSourceModule.BOOKING_FLOW_MODULE)
                {
                    LoginErrorCodeSessionWrapper.LoginPopupErrorCode = 2;
                    Response.Redirect(redirectPageURL);
                }
                else if (currentModule == LoginSourceModule.LOGIN_POPUP_MODULE)
                {
                    LoginErrorCodeSessionWrapper.LoginPopupErrorCode = 2;
                    Response.Redirect(LastVisitedPageSessionWrapper.LastVisitedPage.PageUrl);
                }
                else if (currentModule == LoginSourceModule.BOOKING_SEARCH_MODULE)
                {
                    LoginErrorCodeSessionWrapper.LoginPopupErrorCode = 7; //internal Error code if logging in from hotel search screens for redemption booking
                    Response.Redirect(LastVisitedPageSessionWrapper.LastVisitedPage.PageUrl);
                }
                else if (currentModule == LoginSourceModule.LOGIN_ERROR_MODULE)
                {
                    LoginErrorCodeSessionWrapper.LoginPopupErrorCode = 8;  //internal Error code if logging in from FG page
                    Response.Redirect(LastVisitedPageSessionWrapper.LastVisitedPage.PageUrl);
                }
                else
                {
                    LoginErrorCodeSessionWrapper.LoginPopupErrorCode = 6;
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.LOGIN_ERROR), false);
                }
            }
            catch (Exception genEx)
            {
                UserLoggedInSessionWrapper.UserLoggedIn = false;
                AppLogger.LogFatalException(genEx);
                if (currentModule == LoginSourceModule.BOOKING_DETAIL_MODULE)
                {
                    LoginErrorCodeSessionWrapper.LoginErrorCode = 3;
                    Response.Redirect(redirectPageURL);
                }
                else if (currentModule == LoginSourceModule.BOOKING_FLOW_MODULE)
                {
                    LoginErrorCodeSessionWrapper.LoginPopupErrorCode = 3;
                    Response.Redirect(redirectPageURL);
                }
                else
                {
                    WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                                 WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                }
            }
            finally
            {
                if (Response.Cookies["IsLoggedInUser"] != null)
                {
                    Response.Cookies["IsLoggedInUser"].Value = UserLoggedInSessionWrapper.UserLoggedIn.ToString();
                }
                else
                {
                    HttpCookie httpCookie = new HttpCookie("IsLoggedInUser", UserLoggedInSessionWrapper.UserLoggedIn.ToString());
                    httpCookie.Expires = DateTime.MaxValue;
                    Response.Cookies.Add(httpCookie);
                }
            }
        }

        #endregion Page_Load


        #region GetModuleWhereLoginPerformed

        /// <summary>
        /// Find out the module name where the login action is performed. 
        /// For Example: on the home page, user can login from Booking module 
        /// or from Login Status. Hence to understand where the login 
        /// is done we call this function.
        /// </summary>
        /// <returns></returns>
        private LoginSourceModule GetModuleWhereLoginPerformed(
            Dictionary<LoginSourceModule, LoginDemographicEntity> loginDemographics)
        {
            LoginSourceModule currentModule = LoginSourceModule.NONE;

            try
            {
                int totalLoginModuleAvailable = loginDemographics.Count;
                foreach (LoginSourceModule s in loginDemographics.Keys)
                {
                    string id = loginDemographics[s].PageIdentifier;
                    if (!string.IsNullOrEmpty(Request.Form[id]))
                    {
                        switch (Request.Form[id])
                        {
                            case "LOGIN_POPUP_MODULE":
                                currentModule = LoginSourceModule.LOGIN_POPUP_MODULE;
                                break;
                            case "BOOKING_DETAIL_MODULE":
                                currentModule = LoginSourceModule.BOOKING_DETAIL_MODULE;
                                break;
                            case "BOOKING_SEARCH_MODULE":
                                currentModule = LoginSourceModule.BOOKING_SEARCH_MODULE;
                                break;
                            case "BOOKING_FLOW_MODULE":
                                currentModule = LoginSourceModule.BOOKING_FLOW_MODULE;
                                break;
                            case "LOGIN_ERROR_MODULE":
                                currentModule = LoginSourceModule.LOGIN_ERROR_MODULE;
                                break;
                            case "LOGIN_FOR_DEEPLINK":
                                currentModule = LoginSourceModule.LOGIN_FOR_DEEPLINK;
                                break;
                        }
                        break;
                    }
                }

                if ((currentModule == LoginSourceModule.LOGIN_POPUP_MODULE) && (Request != null && Request.UrlReferrer != null) && (Request.UrlReferrer.ToString().ToLower().Contains("select-rate") || Request.UrlReferrer.ToString().ToLower().Contains("select-hotel") ||
                   Request.UrlReferrer.ToString().ToLower().Contains("booking-detail")))
                {
                    currentModule = LoginSourceModule.BOOKING_FLOW_MODULE;
                }

            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx);
            }

            return currentModule;
        }

        #endregion GetModuleWhereLoginPerformed

        #region GetRedirectPageURL

        /// <summary>
        /// To generate the URL of the page to which an user required to be redirected 
        /// after successful login to the site.
        /// </summary>
        /// <param name="loginSourceModule">Where fromt he login is performed by the user.</param>
        /// <returns></returns>
        private string GetRedirectPageURL(LoginSourceModule loginSourceModule)
        {
            string redirectPageURL = GlobalUtil.GetUrlToPage(EpiServerPageConstants.MYACCOUNT_BOOKING_PAGE);
            int lastVisitedPageId;
            string lastVisitedPageUrl = string.Empty;
            try
            {
                if (LastVisitedPageSessionWrapper.LastVisitedPage != null)
                {
                    lastVisitedPageId = LastVisitedPageSessionWrapper.LastVisitedPage.PageId;
                    lastVisitedPageUrl = LastVisitedPageSessionWrapper.LastVisitedPage.PageUrl;
                    switch (loginSourceModule)
                    {
                        case LoginSourceModule.BOOKING_SEARCH_MODULE:

                            switch (lastVisitedPageId)
                            {
                                case PageId.LOGIN_ERROR_PAGE:
                                case PageId.LOGOUT_CONFIRMATION_PAGE:
                                case PageId.ENROLL_PAGE:
                                case PageId.LOST_CARD_NOT_LOGGEDIN:
                                    redirectPageURL =
                                        GlobalUtil.GetUrlToPage(EpiServerPageConstants.MYACCOUNT_BOOKING_PAGE);
                                    break;
                                default:
                                    redirectPageURL = lastVisitedPageUrl;
                                    break;
                            }
                            break;
                        case LoginSourceModule.LOGIN_ERROR_MODULE:
                            redirectPageURL = GlobalUtil.GetUrlToPage(EpiServerPageConstants.MYACCOUNT_BOOKING_PAGE);
                            break;
                        case LoginSourceModule.LOGIN_FOR_DEEPLINK:
                            redirectPageURL = lastVisitedPageUrl;
                            break;
                        case LoginSourceModule.BOOKING_FLOW_MODULE:
                            redirectPageURL = lastVisitedPageUrl;
                            break;
                        case LoginSourceModule.BOOKING_DETAIL_MODULE:
                            redirectPageURL = lastVisitedPageUrl;
                            break;
                        case LoginSourceModule.LOGIN_POPUP_MODULE:
                            switch (lastVisitedPageId)
                            {
                                case PageId.HOME_PAGE:
                                case PageId.ENROLL_PAGE:
                                case PageId.LOGIN_ERROR_PAGE:
                                case PageId.LOGOUT_CONFIRMATION_PAGE:
                                case PageId.LOST_CARD_NOT_LOGGEDIN:
                                case PageId.FORGOTTEN_PASSWORD_PAGE:
                                    redirectPageURL =
                                        GlobalUtil.GetUrlToPage(EpiServerPageConstants.MYACCOUNT_BOOKING_PAGE);
                                    break;

                                case PageId.SELECT_HOTEL:
                                case PageId.SELECT_RATE:
                                case PageId.BOOKING_DETAIL:
                                case PageId.MODIFY_SELECT_RATE:
                                case PageId.MODIFY_BOOKING_DETAIL:
                                    redirectPageURL = lastVisitedPageUrl;
                                    break;
                                default:
                                    redirectPageURL = lastVisitedPageUrl;
                                    break;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Could generate redirect page URL post Login.");
            }

            return redirectPageURL;
        }

        #endregion GetRedirectPageURL

        #region INavigationTraking Members

        public List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> paramters = new List<KeyValueParam>();
            paramters.Add(new KeyValueParam("User Name", Request.Form[LoginDemographicsSessionWrapper.LoginDemographics[currentModule].UserNameControl]));
            return paramters;
        }

        #endregion
    }
}