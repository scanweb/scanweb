<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RefineBooking.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.RefineBooking" %>
<%@ Register Src="DestinationSearch.ascx" TagName="DestinationSearch" TagPrefix="uc1" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<div id="BookingEngine" class="BE">
<!-- Booking Area Big -->
	<div id="BookingAreaMedium">
	<div id="BookingArea-inner">
		<!-- Booking header -->
		<div id="BookingHeader">
			<div id="imgContainer" >
				<h1><%=WebUtil.GetTranslatedText("/bookingengine/booking/ModifySelectRate/refineyourbooking")%></h1>			
				<select visible="false" id="ddlBookingType" name="ddlBookingType" onchange="hideClientError(); showTabContent(('Tab'+this.value));"  runat="server">
				</select>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<!-- \Booking header -->
		<div id="BookingContent">
			<!-- Tab Contents -->
			<div id="TabContents">
					<div id="clientErrorDiv" runat="server"></div>					

					<div class="errorDivClient">
					    <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading")%>' />
					    <input type="hidden" id="arrivaldateError" name="arrivaldateError" value='<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/arrival_date")%>' />
						<input type="hidden" id="departuredateError" name="departuredateError" value='<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/departure_date")%>' />
						<input type="hidden" id="NoOfNightError" name="NoOfNightError" value='<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/no_of_nights")%>' />
														
						
					</div>
					<div class="formInputs">
					  <!-- Common Controls -->
					  <div id="commonControls">
						<div class="formGroupOne">
							<div class="formColumn1">
							    <div id="destCont">
									
								<div id="autosuggest" class="autosuggest"><ul></ul></div>
								</div>
							</div>
							<div class="clear">&nbsp;</div>
					        <p class="formRowColumn1">
						        <span id="atCont">
						        <label><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/arrivaldate")%></label>
						        <br />
						        <input class="inputTextSmall" type="text" id="txtArrivalDate" onchange="javascript:validateArrivalDate();"  onkeydown="if(event.keyCode==9 || event.keyCode==13) closeCal('y'); else return false;" onfocus="javascript:setCalFieldNOpenCal1($(_endsWith(ArrivalDate)).value, $(_endsWith(DepartureDate)).value, '1');"  maxlength="10" runat="server"/>
						        </span>
					        </p>
					        <p class="formRowColumnSpl">
						        <span id="dtCont">
						        <label><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/departuredate")%></label>
						        <br />
						        <input class="inputTextSmall" type="text" id="txtDepartureDate" onchange="javascript:validateDepartureDate();" onkeydown="if(event.keyCode==9 || event.keyCode==13) closeCal('y'); else return false;" onfocus="javascript:setCalFieldNOpenCal2($(_endsWith(ArrivalDate)).value, $(_endsWith(DepartureDate)).value, '2');" runat="server"/>
						        </span>
					        </p>
						    </div>
						
						<div class="clear">&nbsp;</div>
						<p class="formRowColumn1">
							<span id="nnCont">
							<label><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/noofnights")%></label>
							<br />
							<input class="inputTextSmall" type="text" id="txtNoOfNights" size="3" maxlength="2" value="1" onchange="javascript:validateNoOfNightOnChange();" runat=server/>
							</span>
						</p>
						<!--<p class="formRowColumn2">
							<label><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/noofrooms")%></label>
							<br />
							<label id="lblNoOfRooms">1</label>
						</p>-->
						<p class="clear">&nbsp;</p>
						<p class="formRowColumn1">
							<label><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults")%></label>
							<asp:DropDownList ID="ddlAdultsPerRoom" runat="server" CssClass="selBoxMedium">
                            </asp:DropDownList>
						</p>
						<p class="formRowColumn2">
							<label><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children")%></label>
							<asp:DropDownList ID="ddlChildPerRoom" runat="server" CssClass="selBoxMedium">
                            </asp:DropDownList>
						</p>
						<p class="clear">&nbsp;</p>
					</div>
					  <!-- /Common Controls -->
					</div>
										
					<div id="commonFooter" class="formFooter">
					<div class="formRow"></div>
						<div class="formRowButton">
							<span class="btnSubmit" >
							    <asp:LinkButton ID="LinkButton2"  OnClick="Search_Click" runat="server"><span><%=WebUtil.GetTranslatedText("/bookingengine/booking/ModifySelectRate/searchone")%></span></asp:LinkButton>
							</span>
						</div>
						<div class="clear">&nbsp;</div>
					</div>
			</div>
			<!-- /Tab Contents -->
		</div>
	</div>
</div>
<!-- Booking Area Big -->
<div id="emptyDiv"></div>
<div id="hiddenCalendar" class="calendarContainer">
	<table width="186" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td align="left" valign="top"><div id="calendarLayer">&nbsp;</div>
				<div id="calendarBase"></div></td>
		</tr>
	</table>
</div>
<div class="clear">&nbsp;</div>
</div>

<div id="destinationSearch">
    <uc1:DestinationSearch id="DestinationSearch1" runat="server" />    
</div>

<script language="javascript">
       initRefineBooking();
</script>