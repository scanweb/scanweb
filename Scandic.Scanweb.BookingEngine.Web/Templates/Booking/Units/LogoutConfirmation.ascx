<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginError.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.LoginError" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<div id="Loyalty" class="BE" onload="clearhistory()">
    <!-- LastCard -->
    <div id="ForgotPassword">
        <div class="box-top-grey">
            <span class="title">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/loginheadermessage") %>
            </span>
        </div>
        <div class="box-middle">
            <div class="content" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnLogonButton.ClientID %>')">
                <div id="LoginErrorDiv" class="errorText" runat="server">
                </div>
                <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
                <input type="hidden" id="LoginErrorPasswordInvalid" name="LoginErrorPasswordInvalid"
                    value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/password") %>' />
                <input type="hidden" id="LoginErrorUserNameInvalid" name="LoginErrorUserNameInvalid"
                    value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/user_name") %>' />
                <input type="hidden" id="loginErrID" runat="server" />
                <input type="hidden" id="loginErrPageID" runat="server" />
                <p>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/errormessage") %>
                </p>
                <div class="columnOne">
                    <p class="formRow">
                        <span id="spanLEUserName">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/usernamemessage") %>
                            </label>
                        </span>
                        <br />
                        <input type="text" class="frmInputTextPrefix userPrefix50" id="txtLoginErrorUserNamePrefix"
                            name="txtLoginErrorUserNamePrefix" maxlength="10" runat="server" readonly="readonly" />
                        <input type="text" name="txtLoginErrorUserName" id="txtLoginErrorUserName" runat="server"
                            class="frmInputText w140" />
                    </p>
                </div>
                <div class="columnTwo">
                    <p class="formRow">
                        <span id="spanLEPassword">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/passwordmessage") %>
                            </label>
                        </span>
                        <br />
                        <asp:TextBox CssClass="frmInputText" TextMode="Password" ID="txtLoginErrorPassword"
                            MaxLength="20" runat="server" />
                        <br class="clear" />
                    </p>
                </div>
                <div class="rememberMe fltRt chrmhelpiconfix">
                    <span class="fltLft" style="margin: 2px 3px 0 0; *margin: 0px 3px 0 0;">
                        <input type="checkbox" id="RememberUserIdPwd" name="RememberUserIdPwd" class="chkBx"
                            runat="server" /></span>
                    <%--R2.0:artf1149134: Bhavya: Help text for Remember me is corrected to pick from right xml tag--%>
                    <span class="fltLft rememberText">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/RememberMe") %></span>
                    <span id="remembrMeTryAgain" class="help spriteIcon toolTipMe" title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/RememberMeToolTip") %>"
                        style="z-index: 998"></span>
                </div>
                <div class="clear">
                    &nbsp;</div>
                <div  id="LogoutConfirmProgressDiv" runat="server" style="display: none">
                     <span class="fltLft lgoutBoxProc">
                        <img src='<%=ResolveUrl("~/Templates/Booking/Styles/Default/Images/rotatingclockani.gif")%>'
                            alt="image" align="middle" width="25px" height="25px" />
                        <span>
                            <%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/Loading")%>
                        </span>
                    </span>
                </div>
                
                <!-- /details -->
                <!-- Footer -->
                <div id="FooterContainer">
                    <div class="alignLeft">
                        <span class="link">
                            <%--<asp:LinkButton ID="btnNewRegistration" runat="server" OnClick="btnNewRegistration_Click" ><span><%=WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/newregistrationmessage")%></span></asp:LinkButton>--%>
                        </span>
                    </div>
                    <div class="alignRight">
                        <div class="usrBtn fltRt lout">
                            <asp:LinkButton ID="btnLogonButton" class="buttonInner" runat="server"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/logonbuttonmessage") %></span></asp:LinkButton>
                            <asp:LinkButton ID="spnLogonButton" class="buttonRt scansprite" runat="server" Style="width: 8px;"></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="clear">
                    &nbsp;</div>
            </div>
            <!-- Footer -->
        </div>
    </div>
    <div class="box-bottom">
        &nbsp;</div>
</div>

<script type="text/javascript" language="javascript">  
    $(document).ready(function() {
    appendPrefix("txtLoginErrorUserNamePrefix");
    $("input[id$='txtLoginErrorUserName']").bind('focusout', TrimWhiteSpace);
    });

    function TrimWhiteSpace() {
      
        trimWhiteSpaces("txtLoginErrorUserName");
        	replaceDuplicatePrefix("txtLoginErrorUserName");

    }
    
</script>

<!-- /LastCard -->
