<%@ Control Language="C#" AutoEventWireup="true" Codebehind="LoginError.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.LoginError" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>

<div id="Loyalty" class="BE">
    <!-- LastCard -->
    <div id="LoginScreen">
        <div class="box-top">
            &nbsp;</div>
        <div class="box-middle">
            <div class="content" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnLogonButton.ClientID %>')"> 
                            <h2 runat="server" visible="false" id="ExistingMember" class="darkHeading">
                 <%= WebUtil.GetTranslatedText("/bookingengine/booking/campaignLanding/existingMember") %>
                </h2>
                <p class="errorText">
                </p>
                <div id="LoginErrorDiv" class="errorText" runat="server">
                </div>
                <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
                <input type="hidden" id="LoginErrorPasswordInvalid" name="LoginErrorPasswordInvalid"
                    value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/password") %>' />
                <input type="hidden" id="LoginErrorUserNameInvalid" name="LoginErrorUserNameInvalid"
                    value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/user_name") %>' />
                <input type="hidden" id="loginErrID" runat="server" />
                <input type="hidden" id="loginErrPageID" runat="server" />

                <div>
                    <p class="formRow">
                        <span id="spanLEUserName">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/usernamemessage") %>
                            </label>
                        </span>
                        <br />

                        <input type="text" class="frmInputTextPrefix userPrefix50" id="txtLoginErrorUserNamePrefix" name="txtLoginErrorUserNamePrefix" 
                        maxlength="10" runat="server" readonly="readonly" />

                        <input type="text" class="frmInputText w138" id="txtLoginErrorUserName" name="txtLoginErrorUserName" tabindex="500"
                            maxlength="80" runat="server" />
                    </p>
                    <p class="formRow">
                        <span id="spanLEPassword">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/passwordmessage") %>
                            </label>
                        </span>
                        <br />
                        <asp:TextBox CssClass="frmInputText" TextMode="Password" ID="txtLoginErrorPassword"
                            MaxLength="20" runat="server" TabIndex="501" />
                    </p>
                </div>
               <div class="rememberMe fltRt chrmhelpiconfix">
                <span class="fltLft" style="margin:2px 3px 0 0;*margin:0px 3px 0 0;"><input type="checkbox" tabindex="503" id="RememberUserIdPwd" name="RememberUserIdPwd"
                        class="chkBx" runat="server" /></span>
                        <%--R2.0:artf1149134: Bhavya: Help text for Remember me is corrected to pick from right xml tag--%>
                   
                       <span class="fltLft rememberText"><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/RememberMe") %></span>
                    <span id="remembrMeLoginError" class="help spriteIcon toolTipMe" title=" <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/RememberMeToolTip") %>" style="z-index:998">
                       </span>
                </div>
                <div class="clear">&nbsp;</div>
                    <div id="LoginErrorProgressDiv" runat="server" style="display: none">
                    <span class="fltLft lgnErrorProg">
                        <img src='<%=ResolveUrl("~/Templates/Booking/Styles/Default/Images/rotatingclockani.gif")%>'
                            alt="image" align="middle" width="25px" height="25px" />
                        <span>
                            <%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/Loading")%>
                        </span>
                        <div class="clear">&nbsp;</div>
                    </span>
                </div>
                <!-- /details -->
                <!-- Footer -->
                <div id="FooterContainer">
                    <div class="alignLeft">
                        <span class="link">
                            <asp:LinkButton ID="btnForgotenPassword" runat="server" OnClick="btnForgotenPassword_Click">
		        <span><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/forgottenpasswordmessage") %></span>
                            </asp:LinkButton></span>
                        <!-- START: R1.5 | artf816158 : FGP - Forgotten PIN - Add forgotten memberhipno link -->
                        <span class="link"><a tabindex="305" href="#" onclick="openPopupWin('<%= GlobalUtil.GetHelpPage(Utility.GetForgottenMembershipNoPageReference()) %>','width=400,height=300,scrollbars=yes,resizable=yes');return false;">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/forgottenmembershipno") %>
                        </a></span>
                        <!-- END: R1.5 | artf816158 : FGP - Forgotten PIN - Add forgotten memberhipno link -->
                        <%--<span class="link"><asp:LinkButton ID="btnNewRegistration" runat="server" OnClick="btnNewRegistration_Click"><span><%=WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/newregistrationmessage")%></span></asp:LinkButton></span>--%>
                    </div>
                    <div class="alignRight">
                        <div class="usrBtn fltRt lout">
                            <asp:LinkButton ID="btnLogonButton" class="buttonInner" runat="server" TabIndex="504"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/logonbuttonmessage") %></span></asp:LinkButton>
                            <asp:LinkButton ID="spnLogonButton" class="buttonRt scansprite" runat="server" style="width:5px;"></asp:LinkButton>
                            
                        </div>
                    </div>
                    <div class="clear">
                        &nbsp;</div>
                </div>
                <!-- Footer -->
            </div>
        </div>
        <div class="box-bottom">
            &nbsp;</div>
    </div>
    <!-- /LastCard -->
</div>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        appendPrefix("txtLoginErrorUserNamePrefix");
        $("input[id$='txtLoginErrorUserName']").bind('focusout', TrimWhiteSpaceError);
    });

    function TrimWhiteSpaceError() {

        trimWhiteSpaces("txtLoginErrorUserName");
        replaceDuplicatePrefix("txtLoginErrorUserName");

    }
    
</script>