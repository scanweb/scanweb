//  Description					: Code Behind class for ModifyBookingDetails Control      //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Shankar Dasgupta                                   	  //
/// Author email id				:                           							  //
/// Creation Date				: 14th December  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: Bhavya Shekar - ReservationInfo Property name modified												  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces

using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion

    #region Scandic Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for ModifyBookingDetails
    /// </summary>
    public partial class ModifyBookingDetails : EPiServer.UserControlBase
    {
        private bool isSessionValid = true;

        /// <summary>
        /// Page Load Method of Modify Booking Details User Details
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="e">
        /// Arguments for the Event
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            AppLogger.LogInfoMessage("ModifyBookingDetails :Page Load ");
            BookingEngineSessionWrapper.IsModifyBooking = true;
            ModifyBookingContactDetails.PageType = EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS;
            isSessionValid = WebUtil.IsSessionValid();
            ReservationInfo.PopulateReservationInfoContainer(true);
            if (this.Visible && isSessionValid)
            {
                HotelSearchEntity hotelSearch = null;
                if (!BookingEngineSessionWrapper.DirectlyModifyContactDetails)
                {
                    hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                }
                else
                {
                    if (BookingEngineSessionWrapper.BookingDetails != null)
                    {
                        hotelSearch = BookingEngineSessionWrapper.BookingDetails.HotelSearch;
                    }
                }
                if (hotelSearch != null)
                {
                    hotelSearch.IsModifyComboBooking = BookingEngineSessionWrapper.IsModifyComboBooking;
                }
                SearchCriteriaSessionWrapper.SearchCriteria = hotelSearch;

                if (hotelSearch != null)
                {


                    if (BookingEngineSessionWrapper.DirectlyModifyContactDetails)
                    {
                        ModifyBookingContactDetails.EnableBedTypePreference = false;

                        if (!Utility.IsFamilyBooking())
                        {
                        }
                        else
                        {
                            //lnkChildrenDetails1.Visible = false;
                            //lnkChildrenDetails2.Visible = false;
                            //lnkChildrenDetails3.Visible = false;

                            //lnkSelectRateKids1.Visible = false;
                            //lnkSelectRateKids2.Visible = false;
                            //lnkSelectRateKids3.Visible = false;

                            //lblChildrenDetails1.Visible = true;
                            //lblChildrenDetails2.Visible = true;
                            //lblChildrenDetails3.Visible = true;

                            //lblSelectRateKids1.Visible = true;
                            //lblSelectRateKids2.Visible = true;
                            //lblSelectRateKids3.Visible = true;
                        }
                    }
                    else
                    {
                        if (Utility.IsFamilyBooking())
                        {
                        }
                    }
                }
                else
                {                  
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ProgressBar.SetProgressBarSelection(EpiServerPageConstants.BOOKING_DETAILS_PAGE);
            ShowProgressBar();
            ReservationInfo.ShowHideChargeInfoTexts(false);
        }

        #region Private Methods

        #region ShowProgressBar

        /// <summary>
        /// This used to show the progress baar based on the type of booking made.
        /// </summary>
        private void ShowProgressBar()
        {
            if (Utility.IsFamilyBooking())
            {
            }
            else
            {
            }
        }

        #endregion ShowProgressBar

        #endregion

        

        
    }
}