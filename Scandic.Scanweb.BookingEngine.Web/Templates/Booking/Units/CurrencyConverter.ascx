<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CurrencyConverter.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.CurrencyConverter" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head1">
<link rel="stylesheet" type="text/css" media="screen" href="/Templates/Scanweb/Styles/Default/Styles.css">
<link rel="stylesheet" type="text/css" media="screen" href="/Templates/Scanweb/Styles/Default/Popup.css">
<title><%=WebUtil.GetTranslatedText("/bookingengine/booking/currencyConverter/CurrencyConverterText")%></title>
</head>
<body>
<form name="Form1" method="post" action="#" id="Form1" runat="server">
 <div class="PopUp">
    <div class="PopUpTop">
      <div class="ClosePopUp"> <a class="CloseWindowPic" onclick="self.close();"> Close window </a> </div>

    </div>
    <div class="HeaderLeft"></div>
    <div class="PopUpHeader">
      <h1><%=WebUtil.GetTranslatedText("/bookingengine/booking/currencyConverter/CurrencyConverterHeader")%></h1>
    </div>
    <div class="PopUpBody">
      <p><%=WebUtil.GetTranslatedText("/bookingengine/booking/currencyConverter/CurrencyConverterText")%>
      </p>
      <div id="CurrencyConverterPopup">
        <div class="CurrencyConverterPopupTop">&nbsp;</div>
        <div class="CurrencyConverterPopupContent">
            <div class="formRow">
            <label>
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/currencyConverter/change")%>
            </label><br />
            <asp:TextBox TabIndex="1" ID="txtCurrencyChange" runat="server"></asp:TextBox><br />      
            </div>            
            <div class="formRow">
            <label>
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/currencyConverter/BaseCurrency")%>
            </label><br />
            <asp:DropDownList TabIndex="2" ID="ddlSourceCurrency" runat="server">
            </asp:DropDownList><br />   
            </div>
            <div class="formRow">
            <label>
              <%=WebUtil.GetTranslatedText("/bookingengine/booking/currencyConverter/OtherCurrency")%>
              </label><br />
            <asp:DropDownList TabIndex="3" ID="ddlDestinationCurrency" runat="server">
            </asp:DropDownList><br />  
            </div>                        
            <div class="footerDiv">                         
                <div  class="ButtonLink">
                <div>
                <asp:LinkButton TabIndex="4" ID="btnConvert" Text="Convert" OnClick="btnConvertCurrency_Click" runat="server"></asp:LinkButton>                
            </div>
        </div>
        <br style="clear: both" />       
      </div>      
    </div>
     <div class="CurrencyConverterPopupBottom">&nbsp;</div>
      <p><asp:Label ID="lblConvertedAmount" runat="server"></asp:Label> <asp:Label ID="lblConvertedValue" runat="server" ></asp:Label> </p>
  </div>
  </div>
  </div>
</form>
</body>
</html>

