<%@ Control Language="C#" AutoEventWireup="true" Codebehind="RateDescriptionforEMAIL.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.RateDescriptionforEMAIL" %>
<%--<asp:label id="rateInfo" runat="server">--%>
<tr>
    <td style="padding-top: 15px; padding-bottom: 15px;">
		<div style="margin-bottom: 10px;"><strong><asp:Label ID="rateHeading" runat="server" ></asp:Label></strong></div>
       <div id="flexBGColor" runat="server">&#160;</div>
    </td>
</tr>
<tr>
    <td>
    <asp:Literal ID="aboutRateDesc" runat="server">
    </asp:Literal>
    <%--Book right up to and including the day of arrival. Change or cancel the reservation
        until 18:00 local time on the day of arrival. Payment in conjunction with stay.
        Breakfast is included. VAT included. Book right up to and including the day of arrival.--%>
    </td>
</tr>
<%--</asp:label>--%>
