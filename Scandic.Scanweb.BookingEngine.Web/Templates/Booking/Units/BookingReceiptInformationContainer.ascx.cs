//  Description					:   BookingReceiptInformationContainer                    //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for ReservationInformationContainer
    /// </summary>
    public partial class BookingReceiptInformationContainer : EPiServer.UserControlBase
    {
        private const string RECEIPT_DATE_EN_FORMAT = "ddd dd MMMM yyyy";
        private const string CULTURE_EN = "en-US";
        private const string ENGLISH_LANGUAGE_CODE = "en";

        /// <summary>
        /// Sets hotel name label
        /// </summary>
        public string HotelNameLabel
        {
            set
            {
                pfLblHotelName.InnerHtml = value;
            }
        }

        /// <summary>
        /// Sets ArrivalDate
        /// </summary>
        public DateTime ArrivalDate
        {
            set
            {
                string arrivalDateNode = WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/selecthotel/arrivaldatevalue");
                string fromDate = WebUtil.GetDayFromDate(value) + AppConstants.SPACE + Utility.GetFormattedDate(value);
                string arrivalDateString = string.Format(arrivalDateNode, fromDate);

                if (AppConstants.RECEIPT_IN_ENGLISH_ONLY)
                    pfLblCheckIn.InnerHtml = value.ToString(RECEIPT_DATE_EN_FORMAT, CultureInfo.CreateSpecificCulture(CULTURE_EN));
                else
                    pfLblCheckIn.InnerHtml = arrivalDateString;
            }
        }

        /// <summary>
        /// Sets DepartureDate
        /// </summary>
        public DateTime DepartureDate
        {
            set
            {
                string departureDateNode =
                    WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/selecthotel/departuredatevalue");
                string toDate = WebUtil.GetDayFromDate(value) + AppConstants.SPACE + Utility.GetFormattedDate(value);
                string departureDateString = string.Format(departureDateNode, toDate);

                if (AppConstants.RECEIPT_IN_ENGLISH_ONLY)
                    pfLblCheckOut.InnerText = value.ToString(RECEIPT_DATE_EN_FORMAT, CultureInfo.CreateSpecificCulture(CULTURE_EN));
                else
                    pfLblCheckOut.InnerText = departureDateString;
            }
        }

        private string _noOfRooms = string.Empty;

        /// <summary>
        /// Sets/Gets  NoOfRooms
        /// </summary>
        public string NoOfRooms
        {
            get { return _noOfRooms; }
            set
            {
                pfLblNoOfRooms.InnerHtml = value;
            }
        }

        private string _noofAdults = String.Empty;

        /// <summary>
        /// Sets/Gets NoOfAdults
        /// </summary>
        public int NoOfAdults
        {
            set
            {
                _noofAdults = value.ToString();
            }
            get
            {
                int adultnumber;
                int.TryParse(_noofAdults, out adultnumber);
                return adultnumber;
            }
        }

        private string _noofChildren = String.Empty;

        /// <summary>
        /// Sets/Gets NoOfChildren
        /// </summary>
        public int NoOfChildren
        {
            set
            {
                _noofChildren = value.ToString();
            }
            get
            {
                int childnumber;
                int.TryParse(_noofChildren, out childnumber);
                return childnumber;
            }
        }

        /// <summary>
        /// Sets NoOfNights
        /// </summary>
        public int NoOfNights
        {
            set
            {
                pfLblNoOfNights.InnerHtml = value.ToString();
            }
        }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["command"] != null)
            {
                if ((Request.QueryString["command"] == QueryStringConstants.QUERY_STRING_CANCEL_PRINT)
                    || (Request.QueryString["command"] == QueryStringConstants.QUERY_STRING_PRINT))
                {
                    divPrinterFriendly.Visible = true;
                }
            }
        }

        /// <summary>
        /// This method populates Room details for the specified Reservation
        /// </summary>
        /// <param name="bookingDetailsList">
        /// list of Booking Details 
        /// </param>
        public void SetRoomDetails(List<BookingDetailsEntity> bookingDetailsList)
        {
            if (bookingDetailsList == null)
                return;

            for (int roomIterator = 0; roomIterator < bookingDetailsList.Count; roomIterator++)
            {
                string roomRatePlan = string.Empty;
                string pricePerNight = string.Empty;
                string rateCategoryDescription = string.Empty;
                HotelSearchEntity hotelSearch = bookingDetailsList[roomIterator].HotelSearch;

                switch (roomIterator)
                {
                    case 0:
                        {
                            RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetailsList[roomIterator].HotelRoomRate.RatePlanCode);

                            if (!RoomRateUtil.IsSaveRateCategory(rateCategory.RateCategoryId))
                            {
                                pfDivSummRoom1.Visible = false;
                                break;
                            }

                            pfDivSummRoom1.Visible = true;

                            if (bookingDetailsList[roomIterator] != null)
                            {
                                RoomCategory roomCtgry = RoomRateUtil.GetRoomCategory(bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode);
                                if (roomCtgry != null)
                                {
                                    pfLblRoomCatName1.InnerHtml = roomCtgry.RoomCategoryName;
                                }

                                SetRoomRateDetails(bookingDetailsList[roomIterator], out roomRatePlan, out pricePerNight,
                                                   out rateCategoryDescription);
                                if (!string.IsNullOrEmpty(roomRatePlan))
                                {
                                    pfLblRatePlan1.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 20);
                                }
                                DisplayReservationLegNumber(bookingDetailsList, roomIterator, pfRoom1No);
                            }
                            break;
                        }
                    case 1:
                        {
                            RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetailsList[roomIterator].HotelRoomRate.RatePlanCode);

                            if (!RoomRateUtil.IsSaveRateCategory(rateCategory.RateCategoryId))
                            {
                                pfDivSummRoom2.Visible = false;
                                break;
                            }

                            pfDivSummRoom2.Visible = true;

                            if (bookingDetailsList[roomIterator] != null)
                            {
                                if (RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode) != null &&
                                    RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode).RoomCategoryName != null)

                                    SetRoomRateDetails(bookingDetailsList[roomIterator], out roomRatePlan, out pricePerNight,
                                                       out rateCategoryDescription);

                                pfLblRoomCatName2.InnerText = RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode).RoomCategoryName;

                                if (!string.IsNullOrEmpty(roomRatePlan))
                                {
                                    pfLblRatePlan2.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 20);
                                }
                                DisplayReservationLegNumber(bookingDetailsList, roomIterator, pfRoom2No);
                            }

                            break;
                        }
                    case 2:
                        {
                            RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetailsList[roomIterator].HotelRoomRate.RatePlanCode);

                            if (!RoomRateUtil.IsSaveRateCategory(rateCategory.RateCategoryId))
                            {
                                pfDivSummRoom3.Visible = false;
                                break;
                            }

                            pfDivSummRoom3.Visible = true;
                            if (bookingDetailsList[roomIterator] != null)
                            {
                                SetRoomRateDetails(bookingDetailsList[roomIterator], out roomRatePlan, out pricePerNight,
                                                   out rateCategoryDescription);

                                pfLblRoomCatName3.InnerText = RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode).RoomCategoryName;

                                if (!string.IsNullOrEmpty(roomRatePlan))
                                {
                                    pfLblRatePlan3.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 20);
                                }
                                DisplayReservationLegNumber(bookingDetailsList, roomIterator, pfRoom3No);
                            }
                            break;
                        }
                    case 3:
                        {
                            RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetailsList[roomIterator].HotelRoomRate.RatePlanCode);

                            if (!RoomRateUtil.IsSaveRateCategory(rateCategory.RateCategoryId))
                            {
                                pfDivSummRoom4.Visible = false;
                                break;
                            }

                            pfDivSummRoom4.Visible = true;
                            if (bookingDetailsList[roomIterator] != null)
                            {
                                SetRoomRateDetails(bookingDetailsList[roomIterator], out roomRatePlan, out pricePerNight, out rateCategoryDescription);

                                pfLblRoomCatName4.InnerText = RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode).RoomCategoryName;

                                if (!string.IsNullOrEmpty(roomRatePlan))
                                {
                                    pfLblRatePlan4.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 20);
                                }
                                DisplayReservationLegNumber(bookingDetailsList, roomIterator, pfRoom4No);
                            }
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// This method returns the RoomRatePlan, PricePerNight and room category for the specified room.
        /// </summary>
        /// <param name="bookingDetail">Booking details for the specified room</param>
        /// <param name="roomRatePlan"></param>
        /// <param name="pricePerNight"></param>
        /// <param name="rateCategoryDescription"></param>
        private void SetRoomRateDetails(BookingDetailsEntity bookingDetail, out string roomRatePlan,
                                        out string pricePerNight, out string rateCategoryDescription)
        {
            Rate rate = null;
            RateCategory rateCategory = null;
            roomRatePlan = string.Empty;
            pricePerNight = string.Empty;
            rateCategoryDescription = string.Empty;
            Dictionary<string, RateCategory> ratePlanMap = null;
            BaseRateDisplay rateDisplay = null;
            bool isTrue = false;

            switch (bookingDetail.HotelSearch.SearchingType)
            {
                case SearchType.VOUCHER:
                    {
                        roomRatePlan = WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/searchhotel/voucher");
                        pricePerNight = WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/searchhotel/voucher");
                        rateCategory =
                            RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetail.HotelRoomRate.RatePlanCode);
                        if (rateCategory != null)
                            rateCategoryDescription = rateCategory.RateCategoryDescription;
                        break;
                    }
                case SearchType.BONUSCHEQUE:
                    rate = RoomRateUtil.GetRate(bookingDetail.HotelRoomRate.RatePlanCode);
                    if (rate != null)
                    {
                        roomRatePlan = rate.RateCategoryName;
                    }
                    rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetail.HotelRoomRate.RatePlanCode);
                    if (rateCategory != null)
                    {
                        rateCategoryDescription = rateCategory.RateCategoryDescription;
                    }

                    if (Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage)
                        pricePerNight =
                            WebUtil.GetBonusChequeRateString("/bookingengine/booking/selectrate/bonuschequerate",
                                                             bookingDetail.HotelSearch.ArrivalDate.Year,
                                                             bookingDetail.HotelSearch.HotelCountryCode,
                                                             bookingDetail.HotelRoomRate.TotalRate.Rate,
                                                             bookingDetail.HotelRoomRate.Rate.CurrencyCode,
                                                             RoomRateDisplayUtil.GetNoOfNights(),
                                                             RoomRateDisplayUtil.GetNoOfRooms());
                    else
                        pricePerNight =
                            WebUtil.GetBonusChequeRateString("/bookingengine/booking/selectrate/bonuschequerate",
                                                             bookingDetail.HotelSearch.ArrivalDate.Year,
                                                             bookingDetail.HotelSearch.HotelCountryCode,
                                                             bookingDetail.HotelRoomRate.Rate.Rate,
                                                             bookingDetail.HotelRoomRate.Rate.CurrencyCode,
                                                             AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                    break;
                case SearchType.REDEMPTION:
                    rate = RoomRateUtil.GetRate(bookingDetail.HotelRoomRate.RatePlanCode);
                    if (rate != null)
                    {
                        roomRatePlan = rate.RateCategoryName;
                    }
                    rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetail.HotelRoomRate.RatePlanCode);
                    if (rateCategory != null)
                    {
                        rateCategoryDescription = rateCategory.RateCategoryDescription;
                    }

                    pricePerNight = WebUtil.GetRedeemString(bookingDetail.HotelRoomRate.Points);

                    break;

                case SearchType.REGULAR:
                    if (bookingDetail.HotelRoomRate != null && bookingDetail.HotelRoomRate.RatePlanCode != null)
                    {
                        ratePlanMap = ContentDataAccess.GetRateTypeCategoryMap(true);
                        if (ratePlanMap != null)
                        {
                            if (ratePlanMap.ContainsKey(bookingDetail.HotelRoomRate.RatePlanCode))
                                rateCategory = ratePlanMap[bookingDetail.HotelRoomRate.RatePlanCode];
                            else
                                AppLogger.LogFatalException(new Exception(string.Format("Missing RatePlan code: {0}", bookingDetail.HotelRoomRate.RatePlanCode)));
                        }
                        if (rateCategory != null)
                        {
                            roomRatePlan = rateCategory.RateCategoryName;
                            rateCategoryDescription = rateCategory.RateCategoryDescription;
                        }
                    }
                    pricePerNight = bookingDetail.HotelRoomRate.Rate.Rate.ToString() + AppConstants.SPACE +
                                    bookingDetail.HotelRoomRate.Rate.CurrencyCode;
                    break;

                default:
                    ratePlanMap = ContentDataAccess.GetRateTypeCategoryMap(true);
                    if (ratePlanMap != null)
                    {
                        if (ratePlanMap.ContainsKey(bookingDetail.HotelRoomRate.RatePlanCode))
                            rateCategory = ratePlanMap[bookingDetail.HotelRoomRate.RatePlanCode];
                        else
                            AppLogger.LogFatalException(new Exception(string.Format("Missing RatePlan code: {0}", bookingDetail.HotelRoomRate.RatePlanCode)));
                    }
                    if (rateCategory != null)
                        roomRatePlan = rateCategory.RateCategoryName;
                    if (Utility.IsBlockCodeBooking)
                    {
                        Block block = null;
                        if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                        {
                            block =
                                Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(
                                    SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                        }
                        else
                        {
                            block =
                                Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(
                                    BookingEngineSessionWrapper.BookingDetails.HotelSearch.CampaignCode);
                        }
                        if (block != null)
                        {
                            rateCategoryDescription = block.BlockDescription;
                        }
                    }

                    IList<BaseRoomRateDetails> listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;

                    if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                    {
                        if (Utility.IsBlockCodeBooking)
                        {
                            foreach (BaseRoomRateDetails roomDtls in listRoomRateDetails)
                            {
                                if (roomDtls != null)
                                {
                                    if (roomDtls.RateCategories != null && roomDtls.RateCategories.Count == 0)
                                    {
                                        if (roomDtls as BlockCodeRoomRateDetails != null)
                                        {
                                            rateDisplay = new BlockCodeRoomRateDisplay(roomDtls as BlockCodeRoomRateDetails);
                                            foreach (RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                                            {
                                                roomRatePlan = hdrDisplay.Title;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (RateCategory rateCtgry in roomDtls.RateCategories)
                                        {
                                            if (rateCategory.RateCategoryId == rateCtgry.RateCategoryId)
                                            {
                                                if (roomDtls as BlockCodeRoomRateDetails != null)
                                                {
                                                    rateDisplay = new BlockCodeRoomRateDisplay(roomDtls as BlockCodeRoomRateDetails);
                                                    isTrue = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (isTrue)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            foreach (BaseRoomRateDetails roomDtls in listRoomRateDetails)
                            {
                                if (roomDtls != null)
                                {
                                    foreach (RateCategory rateCtgry in roomDtls.RateCategories)
                                    {
                                        if (rateCategory.RateCategoryId == rateCtgry.RateCategoryId)
                                        {
                                            rateDisplay = new NegotiatedRoomRateDisplay(roomDtls as NegotiatedRoomRateDetails);
                                            isTrue = true;
                                            break;
                                        }
                                    }
                                }
                                if (isTrue)
                                {
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Utility.IsBlockCodeBooking)
                        {
                            roomRatePlan = bookingDetail.HotelSearch.CampaignCode.ToString();
                        }
                    }

                    if (rateDisplay != null)
                    {
                        foreach (RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                        {
                            if (rateCategory != null && rateCategory.RateCategoryId != null &&
                                rateCategory.RateCategoryId == hdrDisplay.Id)
                            {
                                if (string.IsNullOrEmpty(roomRatePlan))
                                    roomRatePlan = hdrDisplay.Title;
                            }
                        }
                    }
                    pricePerNight = bookingDetail.HotelRoomRate.Rate.Rate.ToString() + AppConstants.SPACE +
                                    bookingDetail.HotelRoomRate.Rate.CurrencyCode;
                    break;
            }
        }

        private void DisplayReservationLegNumber(List<BookingDetailsEntity> bookingDetailsList, int roomIterator,
                                                 HtmlGenericControl cntrl)
        {
            cntrl.Visible = true;
            BookingDetailsEntity bookingEntity = bookingDetailsList[roomIterator];
            if (bookingEntity != null)
            {
                cntrl.InnerText = "<" + bookingEntity.ReservationNumber + "-" + bookingEntity.LegNumber + ">";
            }
        }

        /// <summary>
        /// Label display based on per stay and per night
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="rateRoom"></param>
        /// <param name="perRoom"></param>
        private void RoomRateLabelDisplay(HotelSearchEntity hotelSearch, HtmlGenericControl rateRoom, HtmlGenericControl perRoom)
        {
            if (hotelSearch != null)
            {
                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                {
                    rateRoom.InnerText = WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/searchhotel/RatePerStay");
                    perRoom.InnerText = WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/searchhotel/PerStay");
                }
                else
                {
                    rateRoom.InnerText =
                        WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/searchhotel/RoomRateFirstNight");
                    perRoom.InnerText = WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/searchhotel/PerRoom");
                }

                perRoom.Attributes["style"] = "display : none";
            }
        }


        /// <summary>
        /// Populates receipts data
        /// </summary>
        public void PopulateReceiptinfo(List<BookingDetailsEntity> bookingDetailsList)
        {
            GuestInformationEntity guestInfoEntity = bookingDetailsList[0].GuestInformation;

            if (guestInfoEntity == null) return;
            TextInfo tInfo = CultureInfo.CurrentCulture.TextInfo;
            pfName.InnerText = tInfo.ToTitleCase(guestInfoEntity.FirstName) + AppConstants.SPACE + tInfo.ToTitleCase(guestInfoEntity.LastName);

            if ((pfName.InnerText).Length > 22)
                pfName.InnerHtml = Utility.FormatFieldsToDisplay(pfName.InnerText, 22);

            if (AppConstants.RECEIPT_IN_ENGLISH_ONLY)
                pfCountry.InnerText = Utility.GetCountryName(guestInfoEntity.Country, LanguageConstant.LANGUAGE_ENGLISH.ToLower());
            else
                pfCountry.InnerText = Utility.GetCountryName(guestInfoEntity.Country);

            pfEmail.InnerText = guestInfoEntity.EmailDetails != null ? guestInfoEntity.EmailDetails.EmailID : string.Empty;
            pfMobilePhone.InnerText = guestInfoEntity.Mobile != null ? guestInfoEntity.Mobile.Number : string.Empty;

            foreach (BookingDetailsEntity bookingDetails in bookingDetailsList)
            {
                if (bookingDetails.ReservationCreateDate != null)
                {
                    CreateDate.InnerText = bookingDetails.ReservationCreateDate.ToString("yyyy-MM-dd");
                }

                RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetails.HotelRoomRate.RatePlanCode);

                if (RoomRateUtil.IsSaveRateCategory(rateCategory.RateCategoryId))
                {
                    guestInfoEntity = bookingDetails.GuestInformation;
                }
            }

            if (guestInfoEntity.GuranteeInformation != null)
            {
                if (guestInfoEntity.GuranteeInformation.GuranteeType == GuranteeType.PREPAID
                    || guestInfoEntity.GuranteeInformation.GuranteeType == GuranteeType.CREDITCARD)
                {
                    if (guestInfoEntity.GuranteeInformation.CreditCard != null)
                    {
                        pfCardNumber.InnerText = guestInfoEntity.GuranteeInformation.CreditCard.CardNumber;
                    }
                }
            }

            double subTotalValue = 0;
            double totalValue = 0;
            int roomIterator = 0;
            string currencyCode = string.Empty;
            string hotelCountryCode = string.Empty;
            Dictionary<string, RateEntity> vatDetails = new Dictionary<string, RateEntity>();

            if (bookingDetailsList != null)
            {
                foreach (BookingDetailsEntity bookingDetails in bookingDetailsList)
                {
                    HotelSearchEntity hotelSearchEntity = bookingDetails.HotelSearch;
                    HotelRoomRateEntity hotelRoomRateEntity = bookingDetails.HotelRoomRate;
                    RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetails.HotelRoomRate.RatePlanCode);
                    roomIterator++;
                    if (!RoomRateUtil.IsSaveRateCategory(rateCategory.RateCategoryId)) continue;

                    subTotalValue += hotelRoomRateEntity.SubTotal.Rate;
                    totalValue += hotelRoomRateEntity.TotalRate.Rate;

                    string[] bookingReceiptTaxEntriesToHide = AppConstants.BOOKING_RECEIPT_TAX_ENTRIES_TO_HIDE.Split(',');
                    Dictionary<string, string> dicTax = new Dictionary<string, string>();
                    foreach (var item in bookingReceiptTaxEntriesToHide)
                    {
                        dicTax.Add(item, item);
                    }

                    if (hotelRoomRateEntity.Taxes != null)
                    {
                        foreach (ChargeEntity charge in hotelRoomRateEntity.Taxes.Charges)
                        {
                            if (!dicTax.ContainsKey(charge.Description))
                            {
                                if (vatDetails.ContainsKey(charge.Description))
                                    vatDetails[charge.Description].Rate = vatDetails[charge.Description].Rate + charge.Amount.Rate;
                                else
                                    vatDetails.Add(charge.Description, new RateEntity(charge.Amount.Rate, charge.Amount.CurrencyCode));
                            }
                        }
                    }
                }

                if (bookingDetailsList.Count > 0)
                {
                    currencyCode = bookingDetailsList[0].HotelRoomRate.SubTotal.CurrencyCode;
                    hotelCountryCode = bookingDetailsList[0].HotelSearch.HotelCountryCode;
                }
                SetTotalValuesTableContent(totalValue, subTotalValue, currencyCode, vatDetails, hotelCountryCode);
            }

            string pageIdentifier = string.Empty;

            if (BookingEngineSessionWrapper.IsModifyBooking)
                pageIdentifier = PageIdentifier.ModifyBookingPageIdentifier;
            else
                pageIdentifier = PageIdentifier.BookingPageIdentifier;

            if (AppConstants.RECEIPT_IN_ENGLISH_ONLY)
                pfDivGuaranteeInformation.InnerHtml = ContentDataAccess.GetGuaranteeTextForReceipt(pageIdentifier, LanguageConstant.LANGUAGE_ENGLISH.ToLower());
            else
                pfDivGuaranteeInformation.InnerHtml = ContentDataAccess.GetGuaranteeTextForReceipt(pageIdentifier, string.Empty);

        }

        /// <summary>
        /// Sets vat values and sub total values
        /// </summary>
        /// <param name="subTotalValue"></param>
        /// <param name="currencyCode"></param>
        /// <param name="vatDetails"></param>
        private void SetTotalValuesTableContent(double totalValue, double subTotalValue, string currencyCode, Dictionary<string, RateEntity> vatDetails, string hotelCountryCode)
        {
            const string VAT_TEXT = "VAT";
            string taxValueString = string.Empty;

            TableRow tr = new TableRow();
            TableCell tc = new TableCell();
            bool hideVat = ContentDataAccess.GetVATVisibilityForCountry(hotelCountryCode);
            if (!hideVat)
            {
                //add sub total
                tc = new TableCell();
                tc.CssClass = "receiptSubTotalLabel";
                tc.Text = WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/SubTotal");
                tr.Cells.Add(tc);

                tc = new TableCell();
                tc.Text = string.Concat(subTotalValue, AppConstants.SPACE, currencyCode);
                tc.CssClass = "receiptSubTotal";
                tr.Cells.Add(tc);
                totalValuesTable.Rows.Add(tr);

                //add VAT values
                if (vatDetails.Count > 0)
                {
                    foreach (KeyValuePair<string, RateEntity> keyValueItem in vatDetails)
                    {
                        //if VAT value is zero then don't display 
                        if (keyValueItem.Value.Rate == 0) continue;

                        string vatDescription = keyValueItem.Key;
                        RateEntity vatItem = keyValueItem.Value;

                        if (!string.IsNullOrEmpty(vatDescription)) vatDescription = vatDescription.ToUpper();

                        if (vatDescription.IndexOf(VAT_TEXT) > -1)
                        {
                            vatDescription = vatDescription.Substring(vatDescription.IndexOf(VAT_TEXT));
                            vatDescription = vatDescription.Replace(VAT_TEXT, WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/VAT"));
                        }

                        tr = new TableRow();

                        tc = new TableCell();
                        tc.CssClass = "receiptVATLabel";
                        tc.Text = vatDescription;
                        tr.Cells.Add(tc);

                        taxValueString = string.Concat(vatItem.Rate, AppConstants.SPACE, vatItem.CurrencyCode);

                        tc = new TableCell();
                        tc.CssClass = "receiptVAT";
                        tc.Text = taxValueString;
                        tr.Cells.Add(tc);
                        totalValuesTable.Rows.Add(tr);
                    }
                }
            }
            //add total values
            tr = new TableRow();
            tc = new TableCell();
            tc.CssClass = "receiptTotalLabel";
            tc.Text = WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/Total");
            tr.Cells.Add(tc);

            tc = new TableCell();
            tc.CssClass = "receiptTotal";
            tc.Text = string.Concat(totalValue, AppConstants.SPACE, currencyCode);
            tr.Cells.Add(tc);
            totalValuesTable.Rows.Add(tr);

            if (hideVat)
            {
                tr = new TableRow();
                tc = new TableCell();
                tc.ColumnSpan = 2;
                tc.Text = WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/searchhotel/InclTaxesFees");
                tr.Cells.Add(tc);
                totalValuesTable.Rows.Add(tr);
            }

        }
    }
}
