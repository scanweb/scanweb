<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateProfile.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.UpdateProfile" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%@ Import Namespace="Scandic.Scanweb.Entity" %>
<%@ Register Src="UpdateMessageHeader.ascx" TagName="UpdateMessageHeader" TagPrefix="uc1" %>
<%@ Register Src="~/Templates/Booking/Units/ManageCreditCards.ascx" TagName="ManageCreditCards"
    TagPrefix="Scandic" %>
                <p class="formRow">
                <span>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/updateprofile/updatemessage") %>
                </span>
            </p>
<div id="Loyalty" class="BE updateProfilePage">
    <!-- Enroll Loyalty -->
    <div id="EnrollLoyalty" onkeypress="return WebForm_FireDefaultButton(event, '<%= BtnSend.ClientID %>')">
        <div id="GuestInfo" class="formGroup">
            <% string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();%>
            <!-- Error -->
            <div id="clientErrorDivUP" class="errorText" runat="server">
            </div>
            <div class="errorDivClient">
                <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
                <input type="hidden" id="invalidaddressLine1" name="invalidaddressLine1" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/address") %>' />
                <input type="hidden" id="invalidaddressLine2" name="invalidaddressLine2" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/address") %>' />
                <input type="hidden" id="invalidCityTown" name="invalidCityTown" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/city_town_information") %>' />
                <input type="hidden" id="invalidPostcode" name="invalidPostcode" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/postal_code") %>' />
                <input type="hidden" id="invalidTelephone1" name="invalidTelephone1" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/landline_telephone") %>' />
                <input type="hidden" id="invalidTelephone2" name="invalidTelephone2" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/mobile_telephone") %>' />
                <input type="hidden" id="onlyNumber" name="onlyNumber" value="<%=
				WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/onlyNumber")%>" />
                <input type="hidden" id="invalidEmail" name="invalidEmail" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/email_address") %>' />
                <input type="hidden" id="invalidPartnerProgram" name="invalidPartnerProgram" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/partner_program") %>' />
                <input type="hidden" id="invalidAccountNo" name="invalidAccountNo" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/account_no") %>' />
                <input type="hidden" id="invalidCardHolder" name="invalidCardHolder" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/card_holder_name") %>' />
                <input type="hidden" id="invalidCountryCode" name="invalidCountryCode" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalid_Country_Code") %>' />
                <input type="hidden" id="invalidCardNumber" name="invalidCardNumber" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_credit_card_number") %>' />
                <input type="hidden" id="invalidCardType" name="invalidCardType" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_credit_card_type") %>' />
                <input type="hidden" id="invalidExpiryDate" name="invalidExpiryDate" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/expiry_date") %>' />
                <input type="hidden" id="invalidHomePhoneCode" name="invalidHomePhoneCode" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_Home_Phone_Code") %>' />
                <input type="hidden" id="invalidMobilePhoneCode" name="invalidMobilePhoneCode" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_Mobile_Phone_Code") %>' />
                <input type="hidden" id="invalidPassword" name="invalidPassword" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/password") %>' />
                <input type="hidden" id="invalidNewPassword" name="invalidNewPassword" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/new_password") %>' />
                <input type="hidden" id="invalidRetypePassword" name="invalidRetypePassword" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/not_same_password") %>' />
                <input type="hidden" id="invalidNewPasswordDollarSign" name="invalidNewPasswordDollarSign"
                    value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/dollarsign_not_allowed_in_password") %>' />
                <!--Validate for nordic characters Artifact artf1072335 : Scanweb - workaround for double byte chars in password and answer fields -->
                <input type="hidden" id="invalidPasswordNordicChar" name="invalidPasswordNordicChar"
                    value="<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/nordicchar_not_allowed_in_password") %>" />
                <input type="hidden" id="invalidSecretAnswerNordicChar" name="invalidSecretAnswerNordicChar"
                    value="<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/nordicchar_not_allowed_in_secretanswer") %>" />
                <input type="hidden" id="invalidDateofBirth" name="invalidDateofBirth" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/dateofbirth") %>' />
                <input id="invalidSecurityAnswer" name="invalidSecurityAnswer" type="hidden" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_security_answer") %>' />
                <!-- Defect Fix - Artifact artf652899(R1.3) -->
                <input id="invalidSecurityQuestion" name="invalidSecurityQuestion" type="hidden"
                    value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_security_question") %>' />
                <input type="hidden" id="invalidAgeConfirmation" name="invalidAgeConfirmation" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_age_confirmation") %>' />
                <input type="hidden" id="invalidTermsConfirmation" name="invalidTermsConfirmation"
                    value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_terms_confirmation_enroll") %>' />
                <!--R1.5 | Artifact artf809436 : FGP - Update profile CC field -->
                <input type="hidden" id="previousCardNumber" name="orginalCardNumber" runat="server" />
                <!--START:Release 1.5 | artf809605 | FGP - add more than one partner preference -->
                <input type="hidden" id="selectedPartnerProgramValues" name="selectedPartnerProgramValues"
                    runat="server" />
                <input type="hidden" id="optionStringControl" name="optionStringControl" runat="server" />
                <input type="hidden" id="uniquePartnerProg" name="uniquePartnerProg" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/uniquePartnerProg") %>' />
                <input type="hidden" id="choosePreferred" name="choosePreferred" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/choosePreferred") %>' />
                <input type="hidden" id="prefPartnerText" name="prefPartnerText" value='<%=
                WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/preferredPartnerProgramme") %>' />
                <input type="hidden" id="optPartnerText" name="optPartnerText" value='<%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/optionalPartnerProgram") %>' />
                <input type="hidden" id="prefPartnerCount" name="prefPartnerCount" runat="server" />
                <input type="hidden" id="invalidMobile" name="invalidMobile" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/valid_mobile_number") %>'/>
                  <input type="hidden" value="Select an option" name="CheckboxMultiple" id="CheckboxMultiple">
                        <input type="hidden" value="Sorry, no special characters are allowed" name="restrictSplChars" id="restrictSplChars">
                        
                <!--END:Release 1.5 | artf809605 | FGP - add more than one partner preference -->
            </div>
            <!-- Error -->
            <!-- Update Message Header-->
            <uc1:UpdateMessageHeader id="UpdateProfileMessageHeader" runat="server">
            </uc1:UpdateMessageHeader>
            <!-- Update Message Header-->
            <!-- header -->

            <div class="helpLink">
                <h2>
                    <span class="link"><a href="#" onclick="openPopupWin(NeedHelpUrl,'width=550,height=420,scrollbars=yes,resizable=yes'); return false;">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/needhelp") %>
                    </a></span>
                </h2>
            </div>
            <p class="signupformHeaders">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/contactInformation") %>
                       </p>
               <%-- <% if (siteLanguage == "FI")
                   { %>
                <div class="Addr">
                    <h2>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/address") %>
                    </h2>
                </div>
                <!--div class="helpLink">
                            <h2>
                                <span class="link">
                                    <a href="#" onclick="OpenPopUpwindow(NeedHelpUrl,'200','580'); return false;">
                                        <=WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/needhelp")%>
                                    </a> 
                                </span>
                            </h2>
                        </div-->
                <% }
                   else
                   { %>
                <div class="Addr">
                    <h2>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/address") %>
                    </h2>
                </div>
                <!--div class="helpLink">
                            <h2>
                                <span class="link"><a href="#" onclick="OpenPopUpwindow(NeedHelpUrl,'200','580'); return false;"><=WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/needhelp")%></a>
                                </span>
                            </h2>
                        </div-->
                <% } %>--%>
                <div class="clear">
                    &nbsp;</div>
            <!-- /header -->
            <!-- details -->
            <div class="columnOne mrgTop19">
                <p class="formRow">
                    <span id="lblFname">
                        <label id="lblFirstNameHdr" runat="server">
                        </label>
                    </span>
                    <br />
                    <strong><span id="lblFirstName" runat="server"></span></strong>
                </p>
                  <p class="formRow selectFieldParagraph">
                    <label for="Day">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/updateprofile/dateofbirth") %>
                    </label>
                    <br />
                    <strong><span id="lblDateofBirthDay" runat="server" /></strong><strong><span id="lblDateofBirthMonth"
                        runat="server" /></strong><strong><span id="lblDateofBirthYear" runat="server" />
                    </strong>
                    <%--<br class="clear" />--%>
                </p>
               
              
               
            </div>
            <div class="columnTwo mrgTop19">
                <p class="formRow">
                    <span id="lblLname">
                        <label id="lblLastNameHdr" runat="server">
                        </label>
                    </span>
                    <br />
                    <strong><span id="lblLastName" runat="server"></span></strong>
                </p>
                 <p class="formRow radioBtnParagraph">
                    <label>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/gender") %>
                    </label>
                    <br />
                    <strong><span id="lblGender" runat="server"></span></strong>
                </p>
                
            </div>
            <div class="clear">
                &nbsp;</div>
            <!-- /details -->
        
        <div class="formGroup">
        <p class="formRow">
                    <span id="lblAddressLine1">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/addressline1") %>
                        </label>
                    </span>
                    <br />
                    <input type="text" name="txtAddressLine1" class="frmInputText" id="txtAddressLine1"
                        tabindex="306" runat="server" maxlength="80" />
                </p>
                <p class="formRow">
                    <span id="lblAddressLine2">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/addressline2") %>
                        </label>
                    </span>
                    <br />
                    <input type="text" name="txtAddressLine2" class="frmInputText" id="txtAddressLine2"
                        tabindex="307" runat="server" maxlength="80" />
                </p>
                <p class="formRow">
                    <span id="lblCityTown">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/towncity") %>
                        </label>
                    </span>
                    <br />
                    <input type="text" name="txtCityOrTown" class="frmInputText" id="txtCityOrTown" runat="server"
                        tabindex="308" maxlength="40" />
                </p>
                <p class="formRow">
                    <span id="lblPostcode">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/postcode") %>
                        </label>
                    </span>
                    <br />
                    <input type="text" name="txtPostCode" class="frmInputText" id="txtPostCode" runat="server"
                        tabindex="309" maxlength="9" />
                </p>
                <p class="formRow">
                    <span id="lblCountry">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/country") %>
                        </label>
                    </span>
                    <br />
                    <asp:DropDownList CssClass="frmSelectBig" ID="ddlCountry" runat="server" TabIndex="310">
                    </asp:DropDownList>
                </p>
                 <p class="formRow">
                    <span id="lblEmail">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/email") %>
                        </label>
                    </span>
                    <br />
                    <input type="text" name="txtEmail1" class="frmInputText" id="txtEmail1" runat="server"
                        tabindex="311" maxlength="200" />
                </p>
                <p class="formRow">
                    <span id="lblUpdateMobilePhone">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/mobilephone") %>
                        </label>
                    </span>
                    <br />
                    <asp:DropDownList CssClass="frmSelect" ID="ddlTel2" runat="server" TabIndex="312">
                    </asp:DropDownList>
                    <input type="text" name="txtTelephone2" class="frmInputTextSmall" id="txtTelephone2"
                        tabindex="313" runat="server" maxlength="20" />
                </p>
                <p class="formRow">
                    <span class="fltLft">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/prefferedlanguage") %>
                        </label>
                        <br />
                        <asp:DropDownList ID="ddlPreferredLang" onchange="ShowOrHideTitle(this);" runat="server" TabIndex="321" CssClass="frmSelectBig prefLang">
                        </asp:DropDownList>
                    </span>
                    <span class="fltLft">
                        <label id="lblNameTitle" runat="server">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nametitle") %>
                        </label>
                        <br />
                        <span id="dvNameTitle" runat="server">
                             <asp:DropDownList CssClass="frmTitleSelect" ID="ddlTitle" runat="server" />
                        </span>
                    </span>
                    <br class="clear" />
                </p>
                
        </div>
        <div class="clear">
                &nbsp;</div>
        </div>
        <Scandic:ManageCreditCards ID="ManageCreditCards" runat="server">
        </Scandic:ManageCreditCards>
        
        <!--Added by Vaibhav: Merch R4-Itr 2-->
        <div id="MyInterests" runat="server" class="formGroup mrgTop19" style="display: none;">
            <p class="signupformHeaders">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/myinterests") %>
                    </p>                  
                
            </div>
            <label>
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/myinterestshelp") %></label>
            <div class="chkmyinterestContainer">
                <asp:CheckBoxList runat="server" ID="chkMyInterestList" CssClass="chkMyInterest" TabIndex="323">
                </asp:CheckBoxList>
                
            </div>
  <div class="clear">
                &nbsp;</div>
        <!-- SpclReq -->
        <div id="SpclReq" class="formGroup mrgTop19">
            <!-- header -->
            <div class="formGroup mrgTop19">
             <p class="signupformHeaders">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/specialrequirements") %>
                   </p>
                   </div>
            <label>
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/specialrequirementsinfotext") %></label>
            <!-- /header -->
            <div class="RoomPref">
                <label>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/roompreferences") %>
                </label>
                <br class="clear" />
                <!--Following radio button is Added for no preference -->
                <label class="lblRadio">
                    <input name="FloorPreference" type="radio" class="frmInputRadio" tabindex="324" checked="true"
                        value="" id="rdoNoPreferenceFloor" runat="server" />
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nopreference") %>
                </label>
                <label class="lblRadio">
                    <input name="FloorPreference" type="radio" class="frmInputRadio" value="" tabindex="324"
                        id="rdoLowerFloor" runat="server" />
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/lowerfloor") %>
                </label>
                <label class="lblRadio">
                    <input name="FloorPreference" type="radio" class="frmInputRadio" value="" tabindex="324"
                        id="rdoHighFloor" runat="server" />
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/highfloor") %>
                </label>
                <br class="clear" />
                <!--Following radio button is Added for no preference -->
                <label class="lblRadio">
                    <input name="ElevatorPreference" type="radio" class="frmInputRadio" tabindex="325"
                        checked="true" value="" id="rdoNoPreferenceElevator" runat="server" />
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nopreference") %>
                </label>
                <label class="lblRadio">
                    <input name="ElevatorPreference" type="radio" class="frmInputRadio" tabindex="325"
                        value="" id="rdoAwayFromElevator" runat="server" />
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/awayfromelevator") %>
                </label>
                <label class="lblRadio">
                    <input name="ElevatorPreference" type="radio" class="frmInputRadio" tabindex="325"
                        value="" id="rdoNearElevator" runat="server" />
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nearelevator") %>
                </label>
                <br class="clear" />
                <label class="lblRadio">
                    <input name="SmokingPreference" type="radio" class="frmInputRadio" tabindex="326"
                        value="" checked="true" id="rdoNoPreferenceSmocking" runat="server" />
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nopreference") %>
                </label>
                <label class="lblRadio">
                    <input name="SmokingPreference" type="radio" class="frmInputRadio" tabindex="326"
                        value="" id="rdoNoSmoking" runat="server" />
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nosmoking") %>
                </label>
                <label class="lblRadio">
                    <input name="SmokingPreference" type="radio" class="frmInputRadio" tabindex="326"
                        value="" id="rdoSmoking" runat="server" />
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/smoking") %>
                </label>
                <br class="clear" />
                <div class="accessible-room-wrapper">
                    <input name="chkAccessableRoom" type="checkbox" class="frmInputChkBox" value="" tabindex="328"
                        id="chkAccessableRoom" runat="server" />
                    <label class="lblChkBox checkAccessRoom">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/accessableroom") %>
                    </label>
                    <div class="additional-info">	
	                    <span class="additional-info-txt"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/accessableroomhelptext") %></span>
                    </div>
                </div>
                <div class="allergy-room-wrapper">
                    <input name="chkAllergyRooms" type="checkbox" class="frmInputChkBox" value="" tabindex="329"
                        id="chkAllergyRooms" runat="server" />
                    <label class="lblChkBox checkAccessRoom">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/allergyroom") %>
                    </label>
                    <div class="additional-info">	
	                    <span class="additional-info-txt"><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/allergyroomhelptext") %></span>
                    </div> 
                 </div>
               <%-- <br class="clear" />--%>
            </div>
           
        </div>
         <div class="clear">
                &nbsp;</div>
        <div id="partnerPref" class="formGroup">
            <!-- header -->
            <div class="formGroup mrgTop19">
             <p class="signupformHeaders">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/partnerpreference") %>
                   </p>
                   </div>
            <label>
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/partnerpreferenceinfotext") %></label>
            <!-- /header -->
            <!-- details -->
            <!-- STRAT:Release 1.5 | artf809605 | FGP - add more than one partner preference -->
            <div id="preferredPartnerPgm" runat="server">
            </div>
            <div class="clear">
                &nbsp;</div>
            <div class="alignRight">
                <div class="actionBtn fltRt">
                    <span id="addPrefPgmLink"><a href="javascript:void(0);" class="buttonInner" tabindex="365"
                        onclick="addClone();"><span>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/addmorepreference") %></span>
                    </a><a href="javascript:void(0);" class="buttonRt scansprite" tabindex="365" onclick="addClone();">
                    </a></span>
                </div>
            </div>
            <div class="clear">
                &nbsp;</div>
            <!-- /details -->
        </div>
        <div id="password" class="formGroup">
            <!-- header -->
             <div class="formGroup mrgTop19">
             <p class="signupformHeaders">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/passwordheader") %>
                   </p></div>
            <!-- /header -->
            <!-- details -->
            <div class="formGroup">
                <p class="formRow">
                    <span id="lblPassword">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/updateprofile/oldpassword") %>
                        </label>
                    </span>
                    <br />
                    <input type="password" name="txtUpdatePassword" tabindex="370" class="frmInputText ignore"
                        id="txtUpdatePassword" runat="server" maxlength="20" />
                </p>
                <p class="formRow">
                    <span id="lblRetypePassword">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/updateprofile/newpassword") %>
                        </label>
                    </span>
                    <br />
                    <input type="password" name="txtUpdateReTypePassword" tabindex="371" class="frmInputText ignore"
                        id="txtUpdateReTypePassword" runat="server" maxlength="20" />
                </p>
                <p class="formRow">
                    <span id="lblRetypeNewPassword">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/updateprofile/retypenewpassword") %>
                        </label>
                    </span>
                    <br />
                    <input type="password" name="txtUpdateReTypeNewPassword" tabindex="372" class="frmInputText"
                        id="txtUpdateReTypeNewPassword" runat="server" maxlength="20" />
                </p>
            </div>
            <!-- /details -->
            <div class="clear">
                &nbsp;</div>
        </div>
        <!-- Comments -->
        <div id="Terms" class="formGroup">
            <!-- header -->
            <div class="formGroup mrgTop19">
             <p class="signupformHeaders">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/termsconditionupdateprofile") %>
                   </p></div>
            <!-- /header -->
            <!-- details -->
            <div style="width: 100%">
                <%-- <p class="formRow">
                    <input id="chkReceiveScandicInfo" type="checkbox" class="frmCheckBox" value="" tabindex="380" name="chkReceiveScandicInfo"
                        runat="server" />
                    <span id="lblAgeconfirmation">
                    <label class="frmCheckBox">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/receivescandicinfolatest") %>
                    </label>
                    </span>
                    <br class="clear" />
                </p>--%>
                <p class="formRow">
                    <input id="chkReceivePartnersInfo" type="checkbox" class="frmCheckBox" value="" tabindex="381"
                        name="chkReceivePartnersInfo" runat="server" />
                    <span id="lblTermsconfirmation">
                        <label class="frmCheckBox">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/receivepartnersinfo") %>
                        </label>
                    </span>
                    <br class="clear" />
                </p>
            </div>
            <%--    <div class="columnTwo">
                <p class="formRow">
                    <span>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/pleasecheck") %>
                    </span>
                </p>
                <p class="formRow">
                    <input name="chkAgeconfirmation" type="checkbox" class="frmCheckBox" value="" tabindex="382" id="chkAgeconfirmation" />
                    <span id="lblAgeconfirmation">
                        <label class="frmCheckBox">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/confirmage") %>
                        </label>
                    </span>
                    <br class="clear" />
                </p>
                <p class="formRow">
                    <input name="chkTermsconfirmation" type="checkbox" class="frmCheckBox" value="" tabindex="383" id="chkTermsconfirmation" />
                    <span id="lblTermsconfirmation">
                        <label class="frmCheckBox">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/ireadterms") %>
                            <a href="#" tabindex="384" onclick="openPopupWin('<%= GlobalUtil.GetUrlToPage(EpiServerPageConstants.ENROLL_TERMS_AND_CONDITION) %>','width=800,height=600,scrollbars=yes,resizable=yes'); return false;">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/termscondition") %>
                            </a>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/andagree") %>
                        </label>
                    </span>
                    <br class="clear" />
                </p>
            </div>--%>
            <!-- /details -->
            <div class="clear">
                &nbsp;</div>
        </div>
        <!-- Comments -->
       
        <div class="clear">
            &nbsp;</div>
        <!-- /loyaltyEnroll -->
    </div>
    <!-- /Loyalty -->
</div>
 <!-- Footer -->
        <div id="FooterContainer" class="mrgTop19 updateProfileSavebtn">
            <div class="buttonContainer alignRight">
                <div class="actionBtn fltRt">
                    <%--<asp:LinkButton TabIndex="386" ID="BtnSend" class="buttonInner" runat="server" OnClick="BtnSave_Click"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/save") %></span>
                    </asp:LinkButton>
                    <asp:LinkButton TabIndex="386" ID="spnSend" class="buttonRt scansprite" runat="server"
                        OnClick="BtnSave_Click"></asp:LinkButton>--%>
                         <asp:Button ID="BtnSend" runat="server" tabindex="386" OnClick="BtnSave_Click" class="buttonInner brandSearchBtn scansprite"/>
                </div>
            </div>
            <div class="clear">
                &nbsp;</div>
        </div>
        <!-- Footer -->

<script type="text/javascript" language="javascript">
    var NeedHelpUrl = <%= "\"" + GlobalUtil.GetUrlToPage("BookingHelpPage") + "\"" %>;
    //var TermsAndConditionUrl = <%= "\"" + GlobalUtil.GetUrlToPage("EnrollTermsAndConditionsPage") + "\"" %>;
    //initUP();  
    
    //Call it initially to populate the during first loading.
    
    //START:Release 1.5 | artf809605 | FGP - add more than one partner preference
    setupDropDown();
    function addClone(){
        var preferenceNo = $('.thisReplicates');
	    var id = preferenceNo.length;
	    var optionString = $fn(_endsWith("optionStringControl")).value;
	    var maxCount = $fn(_endsWith("prefPartnerCount")).value;
	    document.getElementById("addPrefPgmLink").innerHTML = '<a href="javascript:void(0);" tabindex="365" onclick="addClone();" class="buttonInner"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/addmorepreference") %></span></a><a onclick="addClone();" tabindex="365" class="buttonRt scansprite" href="javascript:void(0);"></a>'; // R 1.6.1 - MOVED FROM THE BELOW BLOCK TO HERE
	    if(id < maxCount)
	    {
	        pushEntityToHiddenField(document.getElementsByName("selectPgm"));
	        //Find out how many objects are there in originalArray.According to that populate the fields.
	        // $fn(_endsWith("preferredPartnerPgm")).innerHTML += '<div class="thisReplicates"><div class="columnOneA"><p class="formRow"><span id="lblPartnerProgram'+id+'"><label>'+ '<%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/optionalPartnerProgram") %>'+' '+id+'</label></span> <br /><select name="selectPgm" id="selectPgm'+id+'" tabindex="'+(343+id) +'" class="frmSelectBig">'+ optionString +'</select><br class="clear" /></p></div><div class="columnTwoA"><p class="formRow"> <span id="lblAccountNo'+id+'"><label><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/accountnumber") %>'+ ' '+id+'</label></span><br /><input name="memberNo" type="text" id="memberNo'+id+'" class="frmInputText1" tabindex="'+(343+id) +'" maxlength="20" /></p></div><div class="columnThree"><a href="javascript:void(0);" onclick="deletePref('+id+');"><span>'+ '<%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/delete") %>' +'</span></a></div><div class="clear"></div><hr /></div>'; // R 1.6.1 - ADDED <span> for consistancy
	         $fn(_endsWith("preferredPartnerPgm")).innerHTML += '<div class="thisReplicates"><div class="fltLft"><div class="prefPartnerLablelft fltLft"><span id="lblPartnerProgram'+id+'"><label>'+ '<%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/optionalPartnerProgram") %>'+' '+id+'</label></span></div><div class="prefPartnerLablert fltLft"><span id="lblAccountNo'+id+'"><label><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/accountnumber") %>'+ ' '+id+'</label></span></div></div><div class="columnOneA"><p class="formRow"><select name="selectPgm" id="selectPgm'+id+'" tabindex="'+(343+id) +'" class="hiddenClass frmSelectBig">'+ optionString +'</select><br class="clear" /></p></div><div class="columnTwoA"><p class="formRow"> <input name="memberNo" type="text" id="memberNo'+id+'" class="hiddenClass frmInputText1" tabindex="'+(343+id) +'" maxlength="20" /></p></div><div class="columnThree"><a href="javascript:void(0);" onclick="deletePref('+id+');"><span>'+ '<%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/delete") %>' +'</span></a></div><div class="clear"></div><hr /></div>'; // R 1.6.1 - ADDED <span> for consistancy
	        setupDropDown();
	    }
	}
	function deletePref(id){
	    var delObj = parseInt(id);
	    var divArray = $('.thisReplicates');
	    var divString = "";
	    var innercount = 0;
	    var deleteString = "";
	    //R1.5 | artf837530: Add partner preference||Clicking on delete link all other the 
	    //partner preferences details become blank
	    //delete the values from hidden field.
	    pushEntityToHiddenFieldAfterDel(document.getElementsByName("selectPgm"), id);
	    var optionString = $fn(_endsWith("optionStringControl")).value;
	    for(var count=0;count<divArray.length;count++){
	        var partnerProgram = "";
	        var labelCount = "";
	        if(count != id){
	            if(0 !=innercount){
	                partnerProgram = $fn(_endsWith("optPartnerText")).value;
	                labelCount = innercount ;
	                deleteString = '<div class="columnThree"><a href="javascript:void(0);"  onclick="deletePref('+innercount+');"><span>'+'<%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/delete") %>'+'</span></a></div><div class="clear"></div>'; }  // R 1.6.1 - ADDED <span> for consistancy
	            else{
	               partnerProgram = $fn(_endsWith("prefPartnerText")).value;
	               labelCount = "";
	               deleteString = '<div class="columnThree"><a href="javascript:void(0);"  onclick="deletePref('+innercount+');"><span>'+'<%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/delete") %>'+'</span></a></div><div class="clear"></div>';}
	           // divString += '<div class="thisReplicates"><div class="columnOneA"><p class="formRow"><span id="lblPartnerProgram'+innercount+'"><label>'+ partnerProgram +' '+labelCount+'</label></span> <br /><select name="selectPgm" id="selectPgm'+innercount+'" tabindex="'+(343+innercount) +'" class="frmSelectBig">'+ optionString +'</select><br class="clear" /></p></div><div class="columnTwoA"><p class="formRow"> <span id="lblAccountNo'+innercount+'"><label><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/accountnumber") %>'+ ' '+labelCount+'</label></span><br /><input name="memberNo" type="text" id="memberNo'+innercount+'" tabindex="'+(343+innercount) +'" class="frmInputText1" maxlength="20" /></p></div>'+ deleteString +'<hr /></div>';
	           divString += '<div class="thisReplicates"><div class="fltLft"><div class="prefPartnerLablelft fltLft"><span id="lblPartnerProgram'+innercount+'"><label>'+ partnerProgram +' '+labelCount+'</label></span></div><div class="prefPartnerLablert fltLft"><span id="lblAccountNo'+innercount+'"><label><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/accountnumber") %>'+ ' '+labelCount+'</label></span></div></div><div class="columnOneA"><p class="formRow"> <select name="selectPgm" id="selectPgm'+innercount+'" tabindex="'+(343+innercount) +'" class="frmSelectBig">'+ optionString +'</select><br class="clear" /></p></div><div class="columnTwoA"><p class="formRow"> <input name="memberNo" type="text" id="memberNo'+innercount+'" tabindex="'+(343+innercount) +'" class="frmInputText1" maxlength="20" /></p></div>'+ deleteString +'<hr /></div>';
	            innercount++;}}
	    $fn(_endsWith("preferredPartnerPgm")).innerHTML = null;
	    $fn(_endsWith("preferredPartnerPgm")).innerHTML = divString;
	    setupDropDown();
	    pushEntityToHiddenField(document.getElementsByName("selectPgm"));
    }
    
	function setupDropDown(){
	    //Get the drop downs.
	    var val = $fn(_endsWith("selectedPartnerProgramValues"));
	    var eachPartnerProgram = val.value.split('%');

	    var selectCtrl = document.getElementsByName("selectPgm");
	    var textCtrl = document.getElementsByName("memberNo");
	    for(var conotrolCount=0;conotrolCount<eachPartnerProgram.length-1;conotrolCount++){
		    var eachValue = eachPartnerProgram[conotrolCount].split(',');
	        selectCtrl[conotrolCount].value = eachValue[0];
	        textCtrl[conotrolCount].value = eachValue[1];}
			
		//R1.5 | artf837495: Add Partner Preference||Error message is displayed if use leave partner preference blank
		pushEntityToHiddenField(document.getElementsByName("selectPgm"));
    }
   
    //R1.5 | artf837530: Add partner preference||Clicking on delete link all other the 
	//partner preferences details become blank
    function pushEntityToHiddenFieldAfterDel(selectCtrl, id)
    {
	    var textCtrl = document.getElementsByName("memberNo");
	    var val="";
	    for(var count=0;count<selectCtrl.length;count++)
	    {
	        if(count != id)
	        {
		        val += selectCtrl[count].options[selectCtrl[count].selectedIndex].value+','+ textCtrl[count].value +'%';
		    }
	    }
	    $fn(_endsWith("selectedPartnerProgramValues")).value = val;
    }
</script>

<!--[if IE 7]>
<script type="text/javascript" language="javascript">

    $(document).ready(function() {
        $('table.chkMyInterest').each(function() {
            $(this).replaceWith($(this).html()
                .replace(/<tbody/gi, "<div class='myinterestheader'")
                .replace(/<tr/gi, "<div class='myinterest'")
                .replace(/<\/tr>/gi, "</div>")
                .replace(/<td/gi, "<span")
                .replace(/<\/td>/gi, "</span>")
                .replace(/<\/tbody/gi, "<\/div")
            );
        });
    });


</script>
<![endif]-->

<script type="text/javascript">
    $(document).ready(function() {
        if ($('.chkmyinterestContainer').children().length > 0) {
            var parentDiv = $fn(_endsWith("MyInterests"));
            $(parentDiv).show();
            var selLanguage = $('#<%= ddlPreferredLang.ClientID %>').val();
            if (selLanguage == 'GER' || selLanguage == "GER") {
                $("[id$=dvNameTitle]").show();
                $("[id$=lblNameTitle]").show();
            }
            else {
                $("[id$=dvNameTitle]").hide();
                $("[id$=lblNameTitle]").hide();
            }
        }
    });     
</script>
<script type='text/javascript'>
    $(function() {
        $('.hiddenClass').live("change", function() {
            pushEntityToHiddenField(document.getElementsByName("selectPgm"));
        });
    });

</script>
<script type="text/javascript" src="/Templates/Booking/Javascript/UpdateProfile.js?v=<%=CmsUtil.GetJSVersion()%>"></script>