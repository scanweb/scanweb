//  Description					: RoomTabControl                                          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code behind of RoomTabControl
    /// </summary>
    public partial class RoomTabControl : System.Web.UI.UserControl
    {
        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                AddRoomTabLinks();
            }
            catch (ContentDataAccessException ex)
            {
                if (!string.Equals(ex.ErrorCode, AppConstants.BONUS_CHEQUE_NOT_FOUND))
               { throw ex; }
            }
        }

        private string pageType = string.Empty;

        /// <summary>
        /// Sets page type.
        /// </summary>
        /// <param name="pageType"></param>
        public void SetPageType(string pageType)
        {
            this.pageType = pageType;
        }

        private void AddRoomTabLinks()
        {
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            if (hotelSearch != null)
            {

                //int noOfRooms = hotelSearch.RoomsPerNight; 

                IList<BaseRoomRateDetails> listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                if ((listRoomRateDetails != null) && (listRoomRateDetails.Count > 0)) 
                {

                    for (int i = 0; i < listRoomRateDetails.Count; i++)
                    {
                        BaseRoomRateDetails currentRoomRateDetails = listRoomRateDetails[i];

                        if (hotelSearch.SearchingType == SearchType.BONUSCHEQUE)
                            BonusChequeRoomRateDisplay.IsRoomsTab = true;

                        BaseRateDisplay rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(currentRoomRateDetails);
                        BonusChequeRoomRateDisplay.IsRoomsTab = false;
                        string liInnerText = string.Empty;
                        string roomNumber = (i + 1).ToString();
                        string roomString = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room") + " " + roomNumber + "&nbsp "; //Shalini CR Fix to add space

                        HtmlGenericControl listControl = new HtmlGenericControl("li");
                        string rateCellDisplay = string.Empty;
                        if (BookingEngineSessionWrapper.HideARBPrice)
                        {
                            rateCellDisplay = Utility.GetPrepaidString();
                        }
                        else
                        {
                            if (hotelSearch.SearchingType != SearchType.BONUSCHEQUE)
                                rateCellDisplay = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from");
                            string notModifiable = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/roomNotModifiableTextForRoomTab");
                            if ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation))
                            {
                                if ((hotelSearch.ListRooms) != null && (hotelSearch.ListRooms.Count > i))
                                {
                                    if (!(hotelSearch.ListRooms[i].IsRoomModifiable))
                                        rateCellDisplay = notModifiable;
                                }
                            }
                            if (rateCellDisplay != notModifiable)
                            {
                                if (Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage)
                                    rateCellDisplay = rateCellDisplay + rateDisplay.MinRatePerStayForEachRoom;
                                else
                                    rateCellDisplay = rateCellDisplay + rateDisplay.MinRateForEachRoom;
                            }
                        }
                        liInnerText = "<a href=\"#\" rel=\".room" + roomNumber + "\" class=\"tabs\"><span>" + roomString + "</span><span class=\"desc\">" + rateCellDisplay + "</span></a>";
                        //Vrushali | Res2.1 | CR - Modify combo booking.
                        if (Convert.ToInt32(roomNumber) == 1)
                        {
                            liInnerText = "<a href=\"#\" rel=\".room" + roomNumber + "\" class=\"tabs\"><span>" + roomString + "</span><span class=\"desc\">" + rateCellDisplay + "</span></a>";

                        }
                        if ((Convert.ToInt32(roomNumber) == 1) && (hotelSearch.ListRooms) != null && (hotelSearch.ListRooms.Count > i) && !(hotelSearch.ListRooms[i].IsRoomModifiable))
                        {

                            liInnerText = "<a href=\"#\" rel=\".room" + roomNumber + "\" class=\"tabs notModifiable\"><span>" + roomString + "</span><span class=\"desc\">" + rateCellDisplay + "</span></a>";
                        }
                        else if ((Convert.ToInt32(roomNumber) > 1) && (hotelSearch.ListRooms) != null && (hotelSearch.ListRooms.Count > i))
                        {
                            if (!(hotelSearch.ListRooms[i].IsRoomModifiable))
                                liInnerText = "<a href=\"#\" rel=\".room" + roomNumber + "\" class=\"tabs notModifiable\"><span>" + roomString + "</span><span class=\"desc\">" + rateCellDisplay + "</span></a>";
                            else
                                liInnerText = "<a href=\"#\" rel=\".room" + roomNumber + "\" class=\"tabs\"><span>" + roomString + "</span><span class=\"desc\">" + rateCellDisplay + "</span></a>";
                        }

                        if (listRoomRateDetails.Count == 1)
                        {
                            roomListContainer.Visible = false;
                            tabWrapperContainer.Visible = false;
                        }
                        else
                        {
                            roomListContainer.Visible = true;
                        }
                        listControl.InnerHtml = liInnerText;
                        roomListContainer.Controls.Add(listControl);
                    }

                }

            }

        }
    }
}