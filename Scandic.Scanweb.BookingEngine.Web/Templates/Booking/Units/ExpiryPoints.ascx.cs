//  Description					: Expiry points display on FGP page	                       //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Ashish Singh                                            //
/// Author email id				:                           							  //
/// Creation Date				: 17 th Jan 2011         								  //
///	Version	#					: 2.1													  //
///---------------------------------------------------------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////

#region Using

using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;

#endregion Using

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// ExpiryPoints
    /// </summary>
    public partial class ExpiryPoints : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            DrawPointOfExpiryTable();
        }

        /// <summary>
        /// Creates the Expiry Point Table with respect to the logged in member, earned point and their expiry date
        /// </summary>
        private void DrawPointOfExpiryTable()
        {
            try
            {
                string nameId = string.Empty;
                if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null &&
                    (!string.IsNullOrEmpty(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID)))
                    nameId = LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID;
                ArrayList ExpiryPointOfMember = new ArrayList();
                if (!string.IsNullOrEmpty(nameId))
                {
                    NameController nameController = new NameController();
                    ExpiryPointOfMember = nameController.FetchGuestCardListExpPoint(nameId,
                                                                                    AppConstants.
                                                                                        SCANDIC_LOYALTY_MEMBERSHIPTYPE);
                }
                if (ExpiryPointOfMember != null && ExpiryPointOfMember.Count > 0)
                {
                    HtmlTableRow rowExpiryPointDate;
                    HtmlTableCell cellExpiryDate;
                    HtmlTableCell cellExpiryPoint;
                    SortedDictionary<DateTime, double> dictPointDateExpiry = new SortedDictionary<DateTime, double>();
                    SortedDictionary<DateTime, double> dictPointDateExpiryVal = new SortedDictionary<DateTime, double>();
                    for (int memeberShipCount = 0; memeberShipCount < ExpiryPointOfMember.Count; memeberShipCount++)
                    {
                        MembershipEntity memberEntity = ExpiryPointOfMember[memeberShipCount] as MembershipEntity;

                        foreach (ExpiryPointEntity expEntity in memberEntity.ExpiryPointCollection)
                        {
                            dictPointDateExpiry.Add(expEntity.ExpiryDate, expEntity.ExpiryPoint);
                        }
                    }
                    dictPointDateExpiry = CheckAndRemovePreviousYearEntry(dictPointDateExpiry);
                    if (dictPointDateExpiry.Count > 0)
                    {
                        expiringPoints.Visible = true;
                        foreach (KeyValuePair<DateTime, double> pairPointDateExpiry in dictPointDateExpiry)
                        {
                            rowExpiryPointDate = new HtmlTableRow();
                            cellExpiryDate = new HtmlTableCell();
                            cellExpiryPoint = new HtmlTableCell();
                            cellExpiryPoint.Attributes.Add("class", "cPad last");
                            cellExpiryDate.Attributes.Add("class", "first");
                            HtmlGenericControl htmlSpanForPoint = new HtmlGenericControl();
                            HtmlGenericControl htmlSpanForDate = new HtmlGenericControl();
                            htmlSpanForPoint.InnerHtml = pairPointDateExpiry.Value.ToString();
                            cellExpiryPoint.Controls.Add(htmlSpanForPoint);
                            string[] dateIteration = (pairPointDateExpiry.Key.ToString("dd MMMM yyyy")).Split(' ');
                            string completeDate = dateIteration[0] + AppConstants.SPACE +
                                                  Convert.ToString(UppercaseFirst(dateIteration[1]).Substring(0,3)) +
                                                  AppConstants.SPACE + dateIteration[2];
                            htmlSpanForDate.InnerHtml = completeDate;
                            cellExpiryDate.Controls.Add(htmlSpanForDate);
                            rowExpiryPointDate.Cells.Add(cellExpiryDate);
                            rowExpiryPointDate.Cells.Add(cellExpiryPoint);
                            tablePointsExpiringDate.Rows.Add(rowExpiryPointDate);
                        }
                    }
                    else
                    {
                        expiringPoints.Visible = false;
                    }
                }
            }
            catch (OWSException owsException)
            {
                AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);
                string exceptionMessage = WebUtil.GetTranslatedText(owsException.TranslatePath);
                if (owsException.Message == AppConstants.SYSTEM_ERROR)
                {
                    WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                                 WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogCustomException(ex, ex.Message);
            }
        }

        /// <summary>
        /// UppercaseFirst
        /// </summary>
        /// <param name="monthVal"></param>
        /// <returns>uppercased first</returns>
        public string UppercaseFirst(string monthVal)
        {
            if (string.IsNullOrEmpty(monthVal))
            {
                return string.Empty;
            }
            return char.ToUpper(monthVal[0]) + monthVal.Substring(1);
        }

        /// <summary>
        /// Removing previous year entry from the dictionary
        /// </summary>
        /// <param name="dictPointDateExpiry">Raw data dictionary</param>
        /// <returns>dictionary have data now now and onwards year data</returns>
        private SortedDictionary<DateTime, double> CheckAndRemovePreviousYearEntry(
            SortedDictionary<DateTime, double> dictPointDateExpiry)
        {
            SortedDictionary<DateTime, double> dictRemovedPrevYr = new SortedDictionary<DateTime, double>();
            if (dictPointDateExpiry.Count > 0)
            {
                foreach (KeyValuePair<DateTime, double> pairPointDateExpiry in dictPointDateExpiry)
                {
                    if (Convert.ToDateTime(pairPointDateExpiry.Key) >= DateTime.Today)
                        dictRemovedPrevYr.Add(pairPointDateExpiry.Key, pairPointDateExpiry.Value);
                }
                return dictRemovedPrevYr;
            }
            else
                return dictPointDateExpiry;
        }
    }
}