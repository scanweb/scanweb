<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ModifyBookingSelectRate.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ModifyBookingSelectRate" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx"
    TagName="NewBookingReservationInformation" TagPrefix="Booking" %>
<%--<%@ Register Src="~/Templates/Booking/Units/DateCarsouel.ascx" TagName="DateCarsouel"
    TagPrefix="Booking" %>--%>
<%@ Register Src ="~/Templates/Booking/Units/AvailabilityCalendar.ascx" TagName ="AvailabilityCalendar" TagPrefix ="SelectRate" %>    
<%@ Register Src="~/Templates/Booking/Units/RoomTabControl.ascx" TagName="RoomTab"
    TagPrefix="Booking" %>
<%@ Register TagPrefix="Scanweb" TagName="ProgressBar" Src="~/Templates/Booking/Units/ProgressBar.ascx" %>
<%@ Register Src="~/Templates/Booking/Units/RoomDetailContainer.ascx" TagName="RoomDetailContainer"
    TagPrefix="Booking" %>
<%@ Register Src="~/Templates/Booking/Units/HotelNameContainer.ascx" TagName="HotelNameContainer"
    TagPrefix="Booking" %>
<!-- Shameem: 22 june : Add user contrl to show Image and description for HOtel -->
<%@ Register Src="~/Templates/Booking/Units/RoomDescriptionPopup.ascx" TagName="PopupRoomDescription"
    TagPrefix="Booking" %>
<Scanweb:ProgressBar id="ProgressBar" runat="server" />
<div class="BE">
    <!-- Step2: Select Rate -->
    <div id="SelectRate">
        <!--Progress Tab -->
        <!-- Details -->
        <input type="hidden" id="txtShowAll" value="FALSE" runat="server" />
        <input type="hidden" id="txtPerStay" value="" runat="server" />
        <input type="hidden" id="txtRoomCategoryID" runat="server" />
        <input type="hidden" id="txtRateCategory" runat="server" />
        <input type="hidden" id="txtRoomNumber" runat="server" class="sessionRoomNumber" />
        <input type="hidden" id="currentSelectedTab" runat="server" />
        <input type="hidden" id="txtNoroomsAvailable" value="FALSE" runat="server" />
        <input type="hidden" id="visibleCount" runat="server" />
        <input type="hidden" id="leftVisibleCarouselIndex" runat="server" />
        <input type="hidden" id="selectedCalendarItemIndex" runat="server" class="selectedCalendarItemIndex"/>        
        <input type="hidden" id="firstBatch" runat="server" class="firstBatch" />
        <input type="hidden" id="lastBatch" runat="server" class="lastBatch" />
        <div style="display: none;" id="Roomflag">
        </div>
        <div class="roomDetails collapse">
            <%--<div class="hdLft sprite">
                <h2 class="hdRt sprite">
                  <label id="reservationHeader" runat="server"></label>
                  <a href="#" title="Room1 - Reservation number"
                        class="help spriteIcon toolTipMe"></a>
                   </h2>
            </div>--%>
            <Booking:NewBookingReservationInformation ID="ReservationInfo" runat="server">
            </Booking:NewBookingReservationInformation>
            <div class="ft sprite" style="display:none;">&nbsp;</div>
        </div>
        <div id="Header">
            <div class="hdrCnt fltLft">
                <Booking:HotelNameContainer ID="HotelName" runat="server">
                </Booking:HotelNameContainer>
                <div class="formCnt fltRt clearMgnTop">
                    <strong class="fltLft">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/prices") %>
                    </strong>
                    <asp:RadioButton ID="perNight" Text="Per Night" GroupName="groupPrices" runat="server"
                        CssClass="radio fltLft" AutoPostBack="true" EnableViewState="true" OnClick="javascript:setPerNightSelectedOnSelectRatePage(false);"
                        OnCheckedChanged="ShowRoomTypesPerCondition" />
                    <asp:RadioButton ID="perStay" Text="Per Stay" Checked="False" GroupName="groupPrices"
                        runat="server" CssClass="radio fltLft" AutoPostBack="true" EnableViewState="true" OnClick="javascript:setPerNightSelectedOnSelectRatePage(true);"
                        OnCheckedChanged="ShowRoomTypesPerCondition" />
                     <span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/PopupPerNightPerStay") %>" class="help spriteIcon toolTipMe fltLft" style="position:relative;"></span>
                </div>
                
                  <!-- BEGIN New Opera Message -->
                <div class="clearAll"><!-- Clearing Float --></div>
                <div id="errorDiv" class="infoAlertBox infoAlertBoxRed" runat="server" visible="false">
                    <div class="threeCol alertContainer">
                        <div class="hd sprite">&#160;</div>
                        <div class="cnt sprite">
                           <span class="alertIconRed">&#160;</span>
                           <div class="descContainer" runat="server">
                                <label class="titleRed"><%= WebUtil.GetTranslatedText("/bookingengine/booking/Rate48HRS/PleaseNote") %></label>
                                <label id="operaErrorMsg" runat="server" class="infoAlertBoxDesc"></label>
                           </div>
                           <div class="clearAll">&#160;</div>
                        </div>
                        <div class="ft sprite">&#160;</div>
                    </div>
                 </div>
                 <!-- END New Opera Message --> 
            </div>
            	<!-- BEGIN Special Alert -->				
				<div id="divSpAlertWrap" class="selectratealertwrapper mdfaltfix" runat="server">
					<div class="splalertcontent" runat="server" id ="divSpAlert"></div>
				</div>
				<!-- END Special Alert -->
        </div>
        <div id="divRoomUnavailable" runat="server" visible="false">
            <p class="errorText" id="roomUnavailableMessage" runat="server">
            </p>
        </div>
            
        <!--artf1151262 : Error message is not dispalying in case of modify booking with an invalid booking code :Rajneesh -->
        <%--<div class="infoAlertBox" visible="false" runat="server" id="divSelectRatePageErrorMessage">
        <div class="threeCol alertContainer">
         
            <div class="hd sprite">
                &nbsp;</div>
            <div class="cnt sprite">
                <p class="scansprite">                                       
                    <label id="lblWrongCodeError" runat="server"/>
                </p>
            </div>
            <div class="ft sprite">
            </div>
        </div>
    </div>  --%>
    <div class="infoAlertBox infoAlertBoxRed" id="infoErrorDiv" runat="server" Visible="false">
                    <div class="threeCol alertContainer">
                        <div class="hd sprite">&nbsp;</div>
                        <div class="cnt sprite">
                           <span class="alertIconRed">&nbsp;</span>
                           <div id="ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_Div1" class="descContainer">
                                <label class="titleRed">Please note</label>
                                <label id="errorLabel" runat="server" class="infoAlertBoxDesc"></label>
                                    
                           </div>
                           <div class="clearAll">&nbsp;</div>
                        </div>
                        <div class="ft sprite">&nbsp;</div>
                    </div>               
                 
        </div> 
    
                 
   

        <div id="divSelectRateMainBody" runat="server">
            <div style="display: block;" class="calendarLoadIndicator" id="CalendarProgressDiv">
                <img src="<%= ResolveUrl("~/templates/Scanweb/Styles/Default/Images/ajax-loader_big.gif") %>" alt="image" align="top" />
                <span class="ploadingText"><%=
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Units/Static/FindHotelSearch/Loading") %></span>
            </div>
            <div style="display: none;" id="CalendarData">
                <SelectRate:AvailabilityCalendar ID="availabilityCalendarControl" runat="server"></SelectRate:AvailabilityCalendar>
            </div>    
            <div class="selectRoom" id="TabStructure">
                <Booking:RoomTab ID="RoomTab" runat="server">
                </Booking:RoomTab>
                <div class="RRWrapper">
                    <div id="blkRoomDetailContainer" runat="server">
                    </div>
                    <div class="topCntOverLay">
                        <div class="ratesOverLayCnt">
                        </div>
                        <div class="ratesOverLay roundMe">
                            <div class="hd sprite">
                            </div>
                            <div class="cnt">
                                <p id="selectedRoom">
                                </p>
                                <div class="HR">
                                </div>
                                <p>
                                    <strong id="nextRoom"></strong>
                                </p>
                            </div>
                            <div class="ft sprite">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- header -->
                <!-- header -->
                <!-- Room Detail Container -->
            </div>
            <!-- header -->
            <!-- header -->
            <!-- Room Detail Container -->
            <!-- /Room Detail Container -->
            <!-- Room Detail Links -->
            <!-- /Room Detail Links-->
            <!-- Footer -->
            <!-- Footer -->
        </div>
    </div>
    <!-- \Step2: Select Rate -->
    <!-- To Display Lightbox for Moadal Pop up window -->
    <div class="jqmWindow dialog imgGalPopUp" id="Div1" runat="server">
        <div class="hd sprite">
        </div>
        <div class="cnt">
            <a href="#" class="jqmClose scansprite blkClose"></a>
            <div class="LightBoxCntnt" id="RoomCategoryContainer" runat="server">
            </div>
        </div>
        <div class="ft sprite">
        </div>
    </div>
    <!--End of Add Room description poput user control -->
</div>
<!-- /Reservation Div -->


<script type="text/javascript" language="javascript">
window.scrollBy(0,800); 
 var requestUrl = <%= "\"" + GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") + "\"" %>;
FetchAvailabilityCalenders(false);
var tab = '<%= roomNumber %>';
var isComboReservation='<%= isComboReservation %>';
//defect id: artf1150702 added delay switch
selectRatemoveRoom(tab, true,true);
$('#CalendarProgressDiv').hide();
$('#CalendarData').show();
</script>
