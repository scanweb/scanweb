//  Description					:   ReservationInformationContainer                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System.Web;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for ReservationInformationContainer
    /// </summary>
    public partial class ReservationInformationContainer : EPiServer.UserControlBase
    {
        /// <summary>
        /// Receipt page url
        /// </summary>
        public string receiptURL;

        /// <summary>
        /// Computes the total of rooms booked on the active booking
        /// </summary>
        /// <returns></returns>
        public int GetTotalBookedRooms()
        {
            return BookingEngineSessionWrapper.ActiveBookingDetails.Count;
        }

        /// <summary>
        /// Computes the total of rooms booked on the active booking
        /// </summary>
        /// <returns></returns>
        public int GetTotalBookedRoomsCancellation()
        {
            return BookingEngineSessionWrapper.AllCancelledDetails.Count;
        }

        /// <summary>
        /// This propery will resolve the url for the help link
        /// </summary>
        public string HelpPageUrl
        {
            get
            {
                PageReference helpPage = CurrentPage["Help Text"] as PageReference;
                if (helpPage == null)
                {
                    helpPage = CurrentPage["HelpText"] as PageReference;
                }
                return GlobalUtil.GetHelpPage(helpPage);
            }
        }

        /// <summary>
        /// Sets hotel name label
        /// </summary>
        public string HotelNameLabel
        {
            set
            {
                spnHotelName.InnerHtml = value;
                spnHotelName.Visible = true;
                lblspHotelName.Visible = false;

                pfLblHotelName.InnerHtml = value;
            }
        }

        /// <summary>
        /// Sets City or hotel name.
        /// </summary>
        public string CityOrHotelName
        {
            set
            {
                lblHotelName.InnerHtml = value;
                spnHotelName.Visible = false;
                lblspHotelName.Visible = true;
            }
        }

        /// <summary>
        /// Sets HotelUrl
        /// </summary>
        public string HotelUrl
        {
            set { lblHotelName.Attributes.Add("href", value); }
        }

        public string ReservationNumber
        {

            get { return ReservationNumberSessionWrapper.ReservationNumber; }
        }

        /// <summary>
        /// Sets ArrivalDate
        /// </summary>
        public DateTime ArrivalDate
        {
            set
            {
                string arrivalDateNode = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/arrivaldatevalue");
                string fromDate = WebUtil.GetDayFromDate(value) + AppConstants.SPACE + Utility.GetFormattedDate(value);
                string arrivalDateString = string.Format(arrivalDateNode, fromDate);
                lblCheckIn.InnerHtml = arrivalDateString;
                pfLblCheckIn.InnerHtml = arrivalDateString;
            }
        }

        /// <summary>
        /// Sets DepartureDate
        /// </summary>
        public DateTime DepartureDate
        {
            set
            {
                string departureDateNode =
                    WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/departuredatevalue");
                string toDate = WebUtil.GetDayFromDate(value) + AppConstants.SPACE + Utility.GetFormattedDate(value);
                string departureDateString = string.Format(departureDateNode, toDate);
                lblCheckOut.InnerHtml = departureDateString;
                pfLblCheckOut.InnerText = departureDateString;
            }
        }

        private string noOfRooms = string.Empty;

        /// <summary>
        /// Sets/Gets  NoOfRooms
        /// </summary>
        public string NoOfRooms
        {
            get { return noOfRooms; }
            set
            {
                string noofRoomsNode = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/noofroomsvalue");
                noOfRooms = value.ToString();
                lblNoOfRoomsValue.InnerHtml = noOfRooms;
                pfLblNoOfRooms.InnerHtml = noOfRooms;
            }
        }

        public string noofAdults = String.Empty;

        /// <summary>
        /// Sets/Gets NoOfAdults
        /// </summary>
        public int NoOfAdults
        {
            set
            {
                string noofAdultsNode = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/noofadultvalue");
                noofAdults = value.ToString();

                lblNoOfAdultsValue.InnerHtml = noofAdults;
            }
            get
            {
                int adultnumber;
                int.TryParse(noofAdults, out adultnumber);
                return adultnumber;
            }
        }

        public string noofChildren = String.Empty;

        /// <summary>
        /// Sets/Gets NoOfChildren
        /// </summary>
        public int NoOfChildren
        {
            set
            {
                string noofChildrenNode =
                    WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/noofchildrenvalue");
                noofChildren = value.ToString();

                lblNoOfChildrenValue.InnerHtml = noofChildren;
            }
            get
            {
                int childnumber;
                int.TryParse(noofChildren, out childnumber);
                return childnumber;
            }
        }

        /// <summary>
        /// Sets NoOfNights
        /// </summary>
        public int NoOfNights
        {
            set
            {
                string noOfNightsNode = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/noofdaysvalue");
                string noOfNights = value.ToString();
                lblNoOfNightsValue.InnerHtml = noOfNights;
                pfLblNoOfNights.InnerHtml = noOfNights;
            }
        }

        /// <summary>
        /// Shows/Hides receipt link
        /// </summary>
        public bool DisplayReceiptLink
        {
            set { lnkBtnPrintReceipt.Visible = value; }
        }


        private string TotalRate
        {
            set
            {

                string rateText = string.Empty;

                if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchType.VOUCHER == SearchCriteriaSessionWrapper.SearchCriteria.SearchingType)
                    rateText = Utility.GetNumberofVouchers();
                else
                    rateText = SmallFontToBonusCheque(value);

                lblTotalRoomRateValue.InnerHtml = rateText;
                lblTotalRoomRateValue.Visible = true;

                pfLblTotalRoomRateValue.InnerHtml = rateText;
            }
        }

        /// <summary>
        /// Function that convert the currency code to light font.
        /// </summary>
        /// <param name="value">string</param>
        /// <returns></returns>
        private string SmallFontToBonusCheque(string value)
        {
            string rateText = string.Empty;
            if (((SearchCriteriaSessionWrapper.SearchCriteria != null &&
                  SearchType.BONUSCHEQUE == SearchCriteriaSessionWrapper.SearchCriteria.SearchingType)
                 || (BookingEngineSessionWrapper.BookingDetails != null && BookingEngineSessionWrapper.BookingDetails.HotelSearch != null &&
                     SearchType.BONUSCHEQUE == BookingEngineSessionWrapper.BookingDetails.HotelSearch.SearchingType))
                && value != null)
            {
                string[] rateString = value.Split('+');
                if (rateString.Length > 1)
                {
                    lblBonusString.Visible = true;
                    pfLblBonusString.Visible = true;
                    string[] splitedtext = rateString[1].Trim().Split(Convert.ToChar(AppConstants.SPACE));
                    rateText = rateString[0] + AppConstants.SPACE + "+" + AppConstants.SPACE + splitedtext[0];
                    lblBonusString.InnerHtml = splitedtext[1];
                    pfLblBonusString.InnerHtml = splitedtext[1];
                }
            }
            else
            {
                lblBonusString.Visible = false;
                pfLblBonusString.Visible = false;
            }
            if (string.IsNullOrEmpty(rateText))
                return value;
            else
                return rateText;
        }

        /// <summary>
        /// Total Rate
        /// </summary>
        public void SetTotalRoomRate(string totalRateString)
        {
            if (BookingEngineSessionWrapper.HideARBPrice)
            {
                TotalRate = Utility.GetPrepaidString();
                pCartTerms.InnerText = string.Empty;
                pfCartTerms.InnerHtml = string.Empty;
            }
            else
            {
                TotalRate = totalRateString;
                pCartTerms.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/InclTaxesFees");
                pfCartTerms.InnerHtml = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/InclTaxesFees");
            }
        }

        /// <summary>
        /// Display message in the error message block
        /// </summary>
        /// <param name="message">
        /// Message to be displayed
        /// </param>
        /// <returns></returns>
        public void DisplayErrorMessage(string message)
        {
            divNoAvailability.Visible = true;
            divNoAvailabilityMessage.InnerHtml = message;
        }

        /// <summary>
        /// Display message in the error message block
        /// </summary>
        /// <param name="message">
        /// Message to be displayed
        /// </param>
        /// <returns></returns>
        public void DisplayErrorMessage(string message, string searchedHotelName)
        {
            divNoAvailability.Visible = true;
            message = string.Format(message, searchedHotelName);
            divNoAvailabilityMessage.InnerHtml = message;
        }

        private bool showHelpLink = true;

        /// <summary>
        /// This is used to Hide/Display Help link
        /// </summary>
        public bool ShowHelpLink
        {
            get { return showHelpLink; }
            set { showHelpLink = value; }
        }

        /// <summary>
        /// This is used to return css string which will be used to display/ Hide
        /// the Help link
        /// </summary>
        /// <returns>string</returns>
        public string HelpVisibility()
        {
            if (ShowHelpLink)
                return "display:block;";
            else
                return "display:none;";
        }

        /// <summary>
        /// Indicates if the booking information to be shown is for previous booking
        /// This changes the header layout to be shown on page
        /// Author: Ruman Khan
        /// Date: Aug 05 2010, Reservation 2.0
        /// </summary>
        private bool isPrevBooking = false;

        /// <summary>
        /// Sets IsPrevBooking
        /// </summary>
        public bool IsPrevBooking
        {
            set { isPrevBooking = value; }
        }

        /// <summary>
        /// Indicates if the modify contact details link should be shown or not
        /// Author: Ruman Khan
        /// Date: Aug 05 2010, Reservation 2.0
        /// </summary>
        private bool canModifyContactDetails = false;

        /// <summary>
        /// Sets CanModifyContactDetails
        /// </summary>
        public bool CanModifyContactDetails
        {
            set { canModifyContactDetails = value; }
        }

        private bool rateDoesNotExistsInCMS = false;

        /// <summary>
        /// Sets RateDoesNotExistsInCMS
        /// </summary>
        public bool RateDoesNotExistsInCMS
        {
            set { rateDoesNotExistsInCMS = value; }
        }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            lnkReservationToolTip.Attributes["style"] = "display:none";
            lnkReservationToolTip.Attributes.Add("title",
                                                 WebUtil.GetTranslatedText(
                                                     "/bookingengine/booking/searchhotel/bookingREservationNoToolTip"));

            if (isPrevBooking)
            {
                divPrevBookingHeader.Attributes["style"] = "display : block;";
                divRegularBookingHeader.Attributes["style"] = "display : none;";
                if (BookingEngineSessionWrapper.BookingDetails != null)
                {
                    prevBookingResNumber.InnerText =
                        WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingResNoModifySelectRatePage") +
                        BookingEngineSessionWrapper.BookingDetails.ReservationNumber;
                }
            }
            else
            {
                regularBookingHeader.InnerText =
                    WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingReservationNumber");
                regularBookingResNumber.InnerText = ReservationNumberSessionWrapper.ReservationNumber;
            }
            lnkModifyContactDetails.Visible = canModifyContactDetails;
            if (Request.QueryString["command"] != null)
            {
                if ((Request.QueryString["command"].ToString() == QueryStringConstants.QUERY_STRING_CANCEL_PRINT)
                    || (Request.QueryString["command"].ToString() == QueryStringConstants.QUERY_STRING_PRINT))
                {
                    regularBookingHeader.InnerText =
                        WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingReservationNumber") + ":";

                    divPrinterFriendly.Visible = true;
                }
            }
            if (Request.QueryString["isPDF"] != null && Convert.ToBoolean(Request.QueryString["isPDF"]) == true)
            {
                lblAnchSummRatePlan1.Visible = false;
                helpIcon.Visible = false;
                lblAnchSummRatePlan3.Visible = false;
                lblAnchSummRatePlan2.Visible = false;
                lblAnchSummRatePlan1.Visible = false;
                lblAnchSummRatePlan4.Visible = false;
                divCnt.Attributes["class"] = "bookingDetailPDfBorder";

                viewModifyDLRedirectPDFLink.Visible = true;
                ancViewModifyDLRedirectLink.HRef = "http://" + GetViewModifyDLRedirectLink();
                ancViewModifyDLRedirectLink.InnerHtml = GetViewModifyDLRedirectLink();
            }
            else
            {
                divCnt.Attributes["class"] = "cnt bookingDetailBorder";
            }
            if (_isCancelledBooking)
                lnkBtnPrintReceipt.Visible = false;
            else if (BookingEngineSessionWrapper.ActiveBookingDetails != null)
                lnkBtnPrintReceipt.Visible = WebUtil.IsAnyRoomHavingSaveCategory(BookingEngineSessionWrapper.ActiveBookingDetails);
            else
                lnkBtnPrintReceipt.Visible = false;

            receiptURL = GlobalUtil.GetUrlToPage(EpiServerPageConstants.RECEIPT);

            if ((!String.IsNullOrEmpty(receiptURL)) && (receiptURL.LastIndexOf('/') == (receiptURL.Length - 1)))
                receiptURL = receiptURL.Substring(0, receiptURL.Length - 1);

            receiptURL = receiptURL + "?command=print";
            if (rateDoesNotExistsInCMS && !Utility.IsBlockCodeBooking)
                divTotalRate.Visible = false;
        }
        private bool _isCancelledBooking = false;
        public bool IsCancelledBooking
        {
            set { _isCancelledBooking = value; }
        }

        /// <summary>
        /// Redirect the User to modify contact details page. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkModifyContactDetails_Click(object sender, EventArgs e)
        {
            BookingEngineSessionWrapper.DirectlyModifyContactDetails = true;
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS));
        }

        /// <summary>
        /// This method populates Room details for the specified Reservation
        /// </summary>
        /// <param name="bookingDetailsList">
        /// list of Booking Details 
        /// </param>
        public void SetRoomDetails(List<BookingDetailsEntity> bookingDetailsList)
        {
            if (bookingDetailsList == null)
                return;
            for (int roomIterator = 0; roomIterator < bookingDetailsList.Count; roomIterator++)
            {
                string roomRatePlan = string.Empty;
                string pricePerNight = string.Empty;
                string rateCategoryDescription = string.Empty;
                HotelSearchEntity hotelSearch = bookingDetailsList[roomIterator].HotelSearch;
                switch (roomIterator)
                {
                    case 0:
                        {
                            divSummRoom1.Visible = true;
                            pfDivSummRoom1.Visible = true;

                            if (bookingDetailsList[roomIterator] != null)
                            {
                                RoomRateLabelDisplay(hotelSearch, rateRoom1, perRoom1);
                                RoomRateLabelDisplay(hotelSearch, pfRateRoom1, pfPerRoom1);

                                RoomCategory roomCtgry =
                                    RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode);

                                WebUtil.SetChargedAtHotelLabel(roomIterator, lblChargeInfoRoom1, false, false);
                                WebUtil.SetChargedAtHotelLabel(roomIterator, pflblChargedAtHotelRoom1, true, false);

                                if (roomCtgry != null)
                                {
                                    lblRoomCatName1.InnerText = roomCtgry.RoomCategoryName;
                                    pfLblRoomCatName1.InnerHtml = roomCtgry.RoomCategoryName;
                                }

                                SetRoomRateDetails(bookingDetailsList[roomIterator], out roomRatePlan, out pricePerNight,
                                                   out rateCategoryDescription);
                                if (!string.IsNullOrEmpty(roomRatePlan))
                                {

                                    lblRatePlan1.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 25);
                                    pfLblRatePlan1.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 20);

                                }
                                
                                if (BookingEngineSessionWrapper.HideARBPrice)
                                {
                                    lblPricePerNight1.InnerText = Utility.GetPrepaidString();
                                    pfLblPricePerNight1.InnerText = Utility.GetPrepaidString();
                                }
                                else if (!string.IsNullOrEmpty(pricePerNight))
                                {
                                    lblPricePerNight1.InnerText = pricePerNight;
                                    pfLblPricePerNight1.InnerHtml = pricePerNight;
                                }
                                if (rateDoesNotExistsInCMS && !Utility.IsBlockCodeBooking)
                                    divPriceDetails1.Visible = false;
                                    
                                if (!string.IsNullOrEmpty(rateCategoryDescription))
                                {
                                    lblAnchSummRatePlan1.Attributes.Add("title", rateCategoryDescription);
                                }
                                else
                                {
                                    lblAnchSummRatePlan1.Visible = false;
                                }
                                DisplayReservationLegNumber(bookingDetailsList, roomIterator, room1No);
                                DisplayReservationLegNumber(bookingDetailsList, roomIterator, pfRoom1No);
                            }
                            break;
                        }
                    case 1:
                        {
                            divSummRoom2.Visible = true;
                            pfDivSummRoom2.Visible = true;

                            if (bookingDetailsList[roomIterator] != null)
                            {
                                RoomRateLabelDisplay(hotelSearch, rateRoom2, perRoom2);
                                RoomRateLabelDisplay(hotelSearch, pfRateRoom2, pfPerRoom2);

                                WebUtil.SetChargedAtHotelLabel(roomIterator, lblChargeInfoRoom2, false, false);
                                WebUtil.SetChargedAtHotelLabel(roomIterator, pflblChargedAtHotelRoom2, true, false);

                                if (
                                    RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode) != null &&
                                    RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode).RoomCategoryName !=
                                    null)
                                    lblRoomCatName2.InnerText =
                                        RoomRateUtil.GetRoomCategory(
                                            bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode).
                                            RoomCategoryName;
                                SetRoomRateDetails(bookingDetailsList[roomIterator], out roomRatePlan, out pricePerNight,
                                                   out rateCategoryDescription);
                                if (
                                    RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode) != null &&
                                    RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode).RoomCategoryName !=
                                    null)
                                    pfLblRoomCatName2.InnerText =
                                        RoomRateUtil.GetRoomCategory(
                                            bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode).RoomCategoryName;

                                if (!string.IsNullOrEmpty(roomRatePlan))
                                {
                                    lblRatePlan2.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 25);
                                    pfLblRatePlan2.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 20);
                                }
                                
                                if (BookingEngineSessionWrapper.HideARBPrice)
                                {
                                    lblPricePerNight2.InnerText = Utility.GetPrepaidString();
                                    pfLblPricePerNight2.InnerText = Utility.GetPrepaidString();
                                }
                                else if (!string.IsNullOrEmpty(pricePerNight))
                                {
                                    lblPricePerNight2.InnerText = pricePerNight;
                                    pfLblPricePerNight2.InnerHtml = pricePerNight;
                                }
                                if (rateDoesNotExistsInCMS && !Utility.IsBlockCodeBooking)
                                    divPriceDetails2.Visible = false;

                                //RK: Modified to limit the content lengh in tool tip
                                /*
                                if (rateCategoryDescription.Length > 20)
                                {
                                    rateCategoryDescription = rateCategoryDescription.Remove(17) + "...";
                                }*/
                                if (!string.IsNullOrEmpty(rateCategoryDescription))
                                {
                                    lblAnchSummRatePlan2.Attributes.Add("title", rateCategoryDescription);
                                }
                                else
                                {
                                    lblAnchSummRatePlan2.Visible = false;
                                }

                                DisplayReservationLegNumber(bookingDetailsList, roomIterator, room2No);
                                DisplayReservationLegNumber(bookingDetailsList, roomIterator, pfRoom2No);
                            }

                            break;
                        }
                    case 2:
                        {
                            divSummRomm3.Visible = true;
                            pfDivSummRoom3.Visible = true;
                            if (bookingDetailsList[roomIterator] != null)
                            {
                                WebUtil.SetChargedAtHotelLabel(roomIterator, lblChargeInfoRoom3, false, false);
                                WebUtil.SetChargedAtHotelLabel(roomIterator, pflblChargedAtHotelRoom3, true, false);

                                RoomRateLabelDisplay(hotelSearch, rateRoom3, perRoom3);
                                RoomRateLabelDisplay(hotelSearch, pfRateRoom3, pfPerRoom3);

                                lblRoomCatName3.InnerText =
                                    RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode).RoomCategoryName;
                                SetRoomRateDetails(bookingDetailsList[roomIterator], out roomRatePlan, out pricePerNight,
                                                   out rateCategoryDescription);

                                pfLblRoomCatName3.InnerText =
                                    RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode).RoomCategoryName;

                                if (!string.IsNullOrEmpty(roomRatePlan))
                                {
                                    lblRatePlan3.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 25);
                                    pfLblRatePlan3.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 20);
                                }
                                
                                if (BookingEngineSessionWrapper.HideARBPrice)
                                {
                                    lblPricePerNight3.InnerText = Utility.GetPrepaidString();
                                    pfLblPricePerNight3.InnerText = Utility.GetPrepaidString();
                                }
                                else if (!string.IsNullOrEmpty(pricePerNight))
                                {
                                    lblPricePerNight3.InnerText = pricePerNight;
                                    pfLblPricePerNight3.InnerHtml = pricePerNight;
                                }
                                if (rateDoesNotExistsInCMS && !Utility.IsBlockCodeBooking)
                                    divPriceDetails3.Visible = false;

                                if (!string.IsNullOrEmpty(rateCategoryDescription))
                                {
                                    lblAnchSummRatePlan3.Attributes.Add("title", rateCategoryDescription);
                                }
                                else
                                {
                                    lblAnchSummRatePlan3.Visible = false;
                                }

                                DisplayReservationLegNumber(bookingDetailsList, roomIterator, room3No);
                                DisplayReservationLegNumber(bookingDetailsList, roomIterator, pfRoom3No);
                            }
                            break;
                        }
                    case 3:
                        {
                            divSummRoom4.Visible = true;
                            pfDivSummRoom4.Visible = true;

                            if (bookingDetailsList[roomIterator] != null)
                            {
                                WebUtil.SetChargedAtHotelLabel(roomIterator, lblChargeInfoRoom4, false, false);
                                WebUtil.SetChargedAtHotelLabel(roomIterator, pflblChargedAtHotelRoom4, true, false);

                                RoomRateLabelDisplay(hotelSearch, rateRoom4, perRoom4);
                                RoomRateLabelDisplay(hotelSearch, pfRateRoom4, pfPerRoom4);

                                lblRoomCatName4.InnerText =
                                    RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode).RoomCategoryName;
                                SetRoomRateDetails(bookingDetailsList[roomIterator], out roomRatePlan, out pricePerNight,
                                                   out rateCategoryDescription);

                                pfLblRoomCatName4.InnerText =
                                    RoomRateUtil.GetRoomCategory(
                                        bookingDetailsList[roomIterator].HotelRoomRate.RoomtypeCode).RoomCategoryName;

                                if (!string.IsNullOrEmpty(roomRatePlan))
                                {

                                    lblRatePlan4.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 25);
                                    pfLblRatePlan4.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 20);

                                }
                                
                                if (BookingEngineSessionWrapper.HideARBPrice)
                                {
                                    lblPricePerNight4.InnerText = Utility.GetPrepaidString();
                                    pfLblPricePerNight4.InnerText = Utility.GetPrepaidString();
                                }
                                else if (!string.IsNullOrEmpty(pricePerNight))
                                {
                                    lblPricePerNight4.InnerText = pricePerNight;
                                    pfLblPricePerNight4.InnerHtml = pricePerNight;
                                }
                                if (rateDoesNotExistsInCMS && !Utility.IsBlockCodeBooking)
                                    divPriceDetails4.Visible = false;

                                if (!string.IsNullOrEmpty(rateCategoryDescription))
                                {
                                    lblAnchSummRatePlan4.Attributes.Add("title", rateCategoryDescription);
                                }
                                else
                                {
                                    lblAnchSummRatePlan4.Visible = false;
                                }

                                DisplayReservationLegNumber(bookingDetailsList, roomIterator, room4No);
                                DisplayReservationLegNumber(bookingDetailsList, roomIterator, pfRoom4No);
                            }
                            break;
                        }
                }
            }
        }


        private void DisplayReservationLegNumber(List<BookingDetailsEntity> bookingDetailsList, int roomIterator,
                                                 HtmlGenericControl cntrl)
        {
            cntrl.Visible = true;
            BookingDetailsEntity bookingEntity = bookingDetailsList[roomIterator];
            if (bookingEntity != null)
            {
                cntrl.InnerText = "<" + bookingEntity.ReservationNumber + "-" + bookingEntity.LegNumber + ">";
            }
        }


        /// <summary>
        /// This method returns the RoomRatePlan, PricePerNight and room category for the specified room.
        /// </summary>
        /// <param name="bookingDetail">
        /// Booking details for the specified room
        /// </param>
        /// <param name="roomRatePlan">
        /// holds the room rate plan
        /// </param>
        /// <param name="pricePerNight">
        /// holds the pricePernight for the specified room
        /// </param>
        /// <param name="rateCategoryDescription">
        /// Holds the Room Rate Category Description.
        /// </param>
        private void SetRoomRateDetails(BookingDetailsEntity bookingDetail, out string roomRatePlan,
                                         out string pricePerNight, out string rateCategoryDescription)
        {
            Rate rate = null;
            RateCategory rateCategory = null;
            roomRatePlan = string.Empty;
            pricePerNight = string.Empty;
            rateCategoryDescription = string.Empty;

            BaseRateDisplay rateDisplay = null;
            bool isTrue = false;

            switch (bookingDetail.HotelSearch.SearchingType)
            {
                case SearchType.VOUCHER:
                    {
                        roomRatePlan = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher");
                        pricePerNight = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher");
                        rateCategory =
                            RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetail.HotelRoomRate.RatePlanCode);
                        if(rateCategory !=null)
                            rateCategoryDescription = rateCategory.RateCategoryDescription;
                        break;
                    }
                case SearchType.BONUSCHEQUE:
                    rate = RoomRateUtil.GetRate(bookingDetail.HotelRoomRate.RatePlanCode);
                    if (rate != null)
                    {
                        roomRatePlan = rate.RateCategoryName;
                    }
                    rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetail.HotelRoomRate.RatePlanCode);
                    if (rateCategory != null)
                    {
                        rateCategoryDescription = rateCategory.RateCategoryDescription;
                    }

                    if (Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage)
                        pricePerNight =
                            WebUtil.GetBonusChequeRateString("/bookingengine/booking/selectrate/bonuschequerate",
                                                             bookingDetail.HotelSearch.ArrivalDate.Year,
                                                             bookingDetail.HotelSearch.HotelCountryCode,
                                                             bookingDetail.HotelRoomRate.TotalRate.Rate,
                                                             bookingDetail.HotelRoomRate.Rate.CurrencyCode,
                                                             RoomRateDisplayUtil.GetNoOfNights(),
                                                             RoomRateDisplayUtil.GetNoOfRooms());
                    else
                        pricePerNight =
                            WebUtil.GetBonusChequeRateString("/bookingengine/booking/selectrate/bonuschequerate",
                                                             bookingDetail.HotelSearch.ArrivalDate.Year,
                                                             bookingDetail.HotelSearch.HotelCountryCode,
                                                             bookingDetail.HotelRoomRate.Rate.Rate,
                                                             bookingDetail.HotelRoomRate.Rate.CurrencyCode,
                                                             AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                    break;
                case SearchType.REDEMPTION:
                    rate = RoomRateUtil.GetRate(bookingDetail.HotelRoomRate.RatePlanCode);
                    if (rate != null)
                    {
                        roomRatePlan = rate.RateCategoryName;
                    }
                    rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetail.HotelRoomRate.RatePlanCode);
                    if (rateCategory != null)
                    {
                        rateCategoryDescription = rateCategory.RateCategoryDescription;
                    }

                    pricePerNight = WebUtil.GetRedeemString(bookingDetail.HotelRoomRate.Points);

                    break;

                case SearchType.REGULAR:
                    rate = RoomRateUtil.GetRate(bookingDetail.HotelRoomRate.RatePlanCode);
                    if (rate != null)
                    {
                        roomRatePlan = rate.RateCategoryName;
                    }
                    if (bookingDetail.HotelRoomRate != null && bookingDetail.HotelRoomRate.RatePlanCode != null)
                    {
                        rateCategory =
                            RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetail.HotelRoomRate.RatePlanCode);
                        if (rateCategory != null)
                        {
                            rateCategoryDescription = rateCategory.RateCategoryDescription;
                        }
                    }
                    pricePerNight = string.Format("{0}{1}{2}",
                                    WebUtil.GetPricePerNight(bookingDetail.HotelRoomRate.TotalRate.Rate, bookingDetail.HotelSearch.NoOfNights),
                                    AppConstants.SPACE, bookingDetail.HotelRoomRate.TotalRate.CurrencyCode);
                    break;

                default:

                    rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bookingDetail.HotelRoomRate.RatePlanCode);
                    if (rateCategory != null)
                    {
                        if (string.Equals(rateCategory.RateCategoryColor, "BLUE", StringComparison.InvariantCultureIgnoreCase) || string.Equals(rateCategory.RateCategoryColor, "ORANGE", StringComparison.InvariantCultureIgnoreCase))
                        {
                            roomRatePlan = rateCategory.RateCategoryName;
                        }
                        rateCategoryDescription = rateCategory.RateCategoryDescription;
                    }
                    if (Utility.IsBlockCodeBooking)
                    {
                        Block block = null;
                        if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                        {
                            block = Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(
                                    SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                        }
                        else
                        {
                            block = Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(
                                    BookingEngineSessionWrapper.BookingDetails.HotelSearch.CampaignCode);
                        }
                        if (block != null)
                        {
                            rateCategoryDescription = block.BlockDescription;
                        }
                    }
                    else
                    {
                        if (rateCategory == null || !string.Equals(rateCategory.RateCategoryColor, "BLUE", StringComparison.InvariantCultureIgnoreCase) && !string.Equals(rateCategory.RateCategoryColor, "ORANGE", StringComparison.InvariantCultureIgnoreCase))
                        {
                            roomRatePlan = bookingDetail.GuestInformation.CompanyName;
                        }
                    }

                    IList<BaseRoomRateDetails> listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                    if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                    {
                        if (Utility.IsBlockCodeBooking)
                        {
                            foreach (BaseRoomRateDetails roomDtls in listRoomRateDetails)
                            {
                                if (roomDtls != null)
                                {
                                    if (roomDtls.RateCategories != null && roomDtls.RateCategories.Count == 0)
                                    {
                                        if (roomDtls as BlockCodeRoomRateDetails != null)
                                        {
                                            rateDisplay = new BlockCodeRoomRateDisplay(roomDtls as BlockCodeRoomRateDetails);
                                            foreach (RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                                            {
                                                roomRatePlan = hdrDisplay.Title;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (RateCategory rateCtgry in roomDtls.RateCategories)
                                        {
                                            if (rateCategory.RateCategoryId == rateCtgry.RateCategoryId)
                                            {
                                                if (roomDtls as BlockCodeRoomRateDetails != null)
                                                {
                                                    rateDisplay = new BlockCodeRoomRateDisplay(roomDtls as BlockCodeRoomRateDetails);
                                                    isTrue = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (isTrue)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            foreach (BaseRoomRateDetails roomDtls in listRoomRateDetails)
                            {
                                if (roomDtls != null)
                                {
                                    foreach (RateCategory rateCtgry in roomDtls.RateCategories)
                                    {
                                        if ((rateCtgry != null) && (rateCategory.RateCategoryId == rateCtgry.RateCategoryId))
                                        {
                                            rateDisplay = new NegotiatedRoomRateDisplay(roomDtls as NegotiatedRoomRateDetails);
                                            isTrue = true;
                                            break;
                                        }
                                    }
                                }
                                if (isTrue)
                                {
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Utility.IsBlockCodeBooking)
                        {
                            roomRatePlan = bookingDetail.HotelSearch.CampaignCode.ToString();
                        }
                    }

                    if (rateDisplay != null)
                    {
                        foreach (RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                        {
                            if (rateCategory != null && rateCategory.RateCategoryId != null &&
                                rateCategory.RateCategoryId == hdrDisplay.Id)
                            {
                                roomRatePlan = hdrDisplay.Title;
                            }
                        }
                    }

                    pricePerNight = string.Format("{0}{1}{2}",
                                   WebUtil.GetPricePerNight(bookingDetail.HotelRoomRate.TotalRate.Rate, bookingDetail.HotelSearch.NoOfNights),
                                   AppConstants.SPACE, bookingDetail.HotelRoomRate.TotalRate.CurrencyCode);
                    break;
            }
        }

        /// <summary>
        /// This is an overloaded method to populate the RoomRateDetails for the specified reservation.
        /// </summary>
        /// <param name="roomRateEntityList"></param>
        public void SetRoomDetails(List<RoomRateEntity> roomRateEntityList)
        {
            for (int roomIterator = 0; roomIterator < roomRateEntityList.Count; roomIterator++)
            {
                string roomRatePlan = string.Empty;
                string pricePerNight = string.Empty;
                string rateCategoryDescription = string.Empty;
                HotelSearchEntity hotelSearch = hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;

                switch (roomIterator)
                {
                    case 0:
                        {
                            divSummRoom1.Visible = true;
                            pfDivSummRoom1.Visible = true;
                            if (roomRateEntityList[roomIterator] != null)
                            {
                                RoomRateLabelDisplay(hotelSearch, rateRoom1, perRoom1);
                                RoomRateLabelDisplay(hotelSearch, pfRateRoom1, pfPerRoom1);
                                string roomCategoryText = RoomRateUtil.GetRoomCategory(roomRateEntityList[roomIterator].RoomTypeCode).RoomCategoryName;
                                lblRoomCatName1.InnerText = roomCategoryText;
                                SetRoomRateDetails(roomRateEntityList[roomIterator], out roomRatePlan, out pricePerNight, out rateCategoryDescription);
                                pfLblRoomCatName1.InnerText = roomCategoryText;

                                WebUtil.SetChargedAtHotelLabel(roomIterator, lblChargeInfoRoom1, false, false);
                                WebUtil.SetChargedAtHotelLabel(roomIterator, pflblChargedAtHotelRoom1, true, false);

                                if (!string.IsNullOrEmpty(roomRatePlan))
                                {
                                    lblRatePlan1.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 25);
                                    pfLblRatePlan1.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 20);
                                }
                                if (BookingEngineSessionWrapper.HideARBPrice)
                                {
                                    lblPricePerNight1.InnerText = Utility.GetPrepaidString();
                                    pfLblPricePerNight1.InnerText = Utility.GetPrepaidString();
                                }
                                else if (!string.IsNullOrEmpty(pricePerNight))
                                {
                                    lblPricePerNight1.InnerText = pricePerNight;
                                    pfLblPricePerNight1.InnerText = pricePerNight;
                                }

                                if (!string.IsNullOrEmpty(rateCategoryDescription))
                                {
                                    lblAnchSummRatePlan1.Attributes.Add("title", rateCategoryDescription);
                                }
                                else
                                {
                                    lblAnchSummRatePlan1.Visible = false;
                                }
                            }
                            DisplayReservationInBookingFlow(roomIterator, room1No);
                            DisplayReservationInBookingFlow(roomIterator, pfRoom1No);
                            break;
                        }
                    case 1:
                        {
                            divSummRoom2.Visible = true;
                            pfDivSummRoom2.Visible = true;
                            if (roomRateEntityList[roomIterator] != null)
                            {
                                RoomRateLabelDisplay(hotelSearch, rateRoom2, perRoom2);
                                RoomRateLabelDisplay(hotelSearch, pfRateRoom2, pfPerRoom2);
                                string roomCategoryText = RoomRateUtil.GetRoomCategory(roomRateEntityList[roomIterator].RoomTypeCode).RoomCategoryName;
                                lblRoomCatName2.InnerText = roomCategoryText;
                                SetRoomRateDetails(roomRateEntityList[roomIterator], out roomRatePlan, out pricePerNight, out rateCategoryDescription);
                                pfLblRoomCatName2.InnerText = roomCategoryText;

                                WebUtil.SetChargedAtHotelLabel(roomIterator, lblChargeInfoRoom2, false, false);
                                WebUtil.SetChargedAtHotelLabel(roomIterator, pflblChargedAtHotelRoom2, true, false);

                                if (!string.IsNullOrEmpty(roomRatePlan))
                                {
                                    lblRatePlan2.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 25);
                                    pfLblRatePlan2.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 20);
                                }
                                if (BookingEngineSessionWrapper.HideARBPrice)
                                {
                                    lblPricePerNight2.InnerText = Utility.GetPrepaidString();
                                    pfLblPricePerNight2.InnerText = Utility.GetPrepaidString();
                                }
                                else if (!string.IsNullOrEmpty(pricePerNight))
                                {
                                    lblPricePerNight2.InnerText = pricePerNight;
                                    pfLblPricePerNight2.InnerText = pricePerNight;
                                }

                                if (!string.IsNullOrEmpty(rateCategoryDescription))
                                {
                                    lblAnchSummRatePlan2.Attributes.Add("title", rateCategoryDescription);
                                }
                                else
                                {
                                    lblAnchSummRatePlan2.Visible = false;
                                }
                            }
                            DisplayReservationInBookingFlow(roomIterator, room2No);
                            DisplayReservationInBookingFlow(roomIterator, pfRoom2No);
                            break;
                        }
                    case 2:
                        {
                            divSummRomm3.Visible = true;
                            pfDivSummRoom3.Visible = true;
                            if (roomRateEntityList[roomIterator] != null)
                            {
                                RoomRateLabelDisplay(hotelSearch, rateRoom3, perRoom3);
                                RoomRateLabelDisplay(hotelSearch, pfRateRoom3, pfPerRoom3);
                                string roomCategoryText = RoomRateUtil.GetRoomCategory(roomRateEntityList[roomIterator].RoomTypeCode).RoomCategoryName;
                                lblRoomCatName3.InnerText = roomCategoryText;
                                SetRoomRateDetails(roomRateEntityList[roomIterator], out roomRatePlan, out pricePerNight, out rateCategoryDescription);
                                pfLblRoomCatName3.InnerText = roomCategoryText;

                                WebUtil.SetChargedAtHotelLabel(roomIterator, lblChargeInfoRoom3, false, false);
                                WebUtil.SetChargedAtHotelLabel(roomIterator, pflblChargedAtHotelRoom3, true, false);

                                if (!string.IsNullOrEmpty(roomRatePlan))
                                {
                                    lblRatePlan3.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 25);
                                    pfLblRatePlan3.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 20);
                                }
                                if (BookingEngineSessionWrapper.HideARBPrice)
                                {
                                    lblPricePerNight3.InnerText = Utility.GetPrepaidString();
                                    pfLblPricePerNight3.InnerText = Utility.GetPrepaidString();
                                }
                                else if (!string.IsNullOrEmpty(pricePerNight))
                                {
                                    lblPricePerNight3.InnerText = pricePerNight;
                                    pfLblPricePerNight3.InnerText = pricePerNight;
                                }
                                if (!string.IsNullOrEmpty(rateCategoryDescription))
                                {
                                    lblAnchSummRatePlan3.Attributes.Add("title", rateCategoryDescription);
                                }
                                else
                                {
                                    lblAnchSummRatePlan3.Visible = false;
                                }
                            }
                            DisplayReservationInBookingFlow(roomIterator, room3No);
                            DisplayReservationInBookingFlow(roomIterator, pfRoom3No);
                            break;
                        }
                    case 3:
                        {
                            divSummRoom4.Visible = true;
                            pfDivSummRoom4.Visible = true;
                            if (roomRateEntityList[roomIterator] != null)
                            {
                                RoomRateLabelDisplay(hotelSearch, rateRoom4, perRoom4);
                                RoomRateLabelDisplay(hotelSearch, pfRateRoom4, pfPerRoom4);
                                string roomCategoryText = RoomRateUtil.GetRoomCategory(roomRateEntityList[roomIterator].RoomTypeCode).RoomCategoryName;
                                lblRoomCatName4.InnerText = roomCategoryText;
                                SetRoomRateDetails(roomRateEntityList[roomIterator], out roomRatePlan, out pricePerNight, out rateCategoryDescription);
                                pfLblRoomCatName4.InnerText = roomCategoryText;

                                WebUtil.SetChargedAtHotelLabel(roomIterator, lblChargeInfoRoom4, false, false);
                                WebUtil.SetChargedAtHotelLabel(roomIterator, pflblChargedAtHotelRoom4, true, false);

                                if (!string.IsNullOrEmpty(roomRatePlan))
                                {
                                    lblRatePlan4.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 25);
                                    pfLblRatePlan4.InnerHtml = Utility.FormatFieldsToDisplay(roomRatePlan, 20);
                                }
                                if (BookingEngineSessionWrapper.HideARBPrice)
                                {
                                    lblPricePerNight4.InnerText = Utility.GetPrepaidString();
                                    pfLblPricePerNight4.InnerText = Utility.GetPrepaidString();
                                }
                                else if (!string.IsNullOrEmpty(pricePerNight))
                                {
                                    lblPricePerNight4.InnerText = pricePerNight;
                                    pfLblPricePerNight4.InnerText = pricePerNight;
                                }
                                if (!string.IsNullOrEmpty(rateCategoryDescription))
                                {
                                    lblAnchSummRatePlan4.Attributes.Add("title", rateCategoryDescription);
                                }
                                else
                                {
                                    lblAnchSummRatePlan4.Visible = false;
                                }
                            }
                            DisplayReservationInBookingFlow(roomIterator, room4No);
                            DisplayReservationInBookingFlow(roomIterator, pfRoom4No);
                            break;
                        }
                }
            }
        }

        private void DisplayReservationInBookingFlow(int roomIterator, HtmlGenericControl cntrl)
        {
            if (GuestBookingInformationSessionWrapper.AllGuestsBookingInformations != null)
            {
                int i = 0;
                foreach (GuestInformationEntity guest in GuestBookingInformationSessionWrapper.AllGuestsBookingInformations.Values)
                {
                    if (i == roomIterator)
                    {
                        cntrl.InnerText = string.Format("{0}" + guest.ReservationNumber + "{1}" + guest.LegNumber + "{2}", "<", "-", ">");
                        break;
                    }
                    i++;
                }
            }
        }


        /// <summary>
        /// Label display based on per stay and per night
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="rateRoom"></param>
        /// <param name="perRoom"></param>
        private void RoomRateLabelDisplay(HotelSearchEntity hotelSearch, HtmlGenericControl rateRoom, HtmlGenericControl perRoom)
        {
            if (hotelSearch != null)
            {
                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                {
                    rateRoom.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RatePerStay");
                    perRoom.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/PerStay");
                }
                else
                {
                    rateRoom.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RoomRateFirstNight");
                    perRoom.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/PerRoom");
                }
                perRoom.Attributes["style"] = "display : none";
            }
        }

        /// <summary>        
        /// This is an overloaded method that returns the RoomRatePlan, PricePerNight and room category for the specified room. 
        /// </summary>
        /// <param name="roomRateEntity"></param>
        /// <param name="roomRatePlan"></param>
        /// <param name="pricePerNight"></param>
        /// <param name="rateCategoryDescription"></param>
        private void SetRoomRateDetails(RoomRateEntity roomRateEntity, out string roomRatePlan, out string pricePerNight,
                                        out string rateCategoryDescription)
        {
            Rate rate = null;
            RateCategory rateCategory = null;
            roomRatePlan = string.Empty;
            pricePerNight = string.Empty;
            rateCategoryDescription = string.Empty;
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
            string hotelCountryCode = "";
            if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null && HotelRoomRateSessionWrapper.ListHotelRoomRate.Count > 0)
                hotelCountryCode = HotelRoomRateSessionWrapper.ListHotelRoomRate[0].CountryCode;
            if (string.IsNullOrEmpty(hotelCountryCode))
                hotelCountryCode = hotelSearch.HotelCountryCode;

            BaseRateDisplay rateDisplay = null;
            bool isTrue = false;

            switch (hotelSearch.SearchingType)
            {
                case SearchType.VOUCHER:
                    {
                        roomRatePlan = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher");
                        pricePerNight = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher");
                        rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(roomRateEntity.RatePlanCode);
                        if(rateCategory != null)
                            rateCategoryDescription = rateCategory.RateCategoryDescription;
                        break;
                    }
                case SearchType.BONUSCHEQUE:
                    rate = RoomRateUtil.GetRate(roomRateEntity.RatePlanCode);
                    if (rate != null)
                    {
                        roomRatePlan = rate.RateCategoryName;
                    }
                    rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(roomRateEntity.RatePlanCode);
                    if (rateCategory != null)
                    {
                        rateCategoryDescription = rateCategory.RateCategoryDescription;
                    }
                    pricePerNight = WebUtil.GetBonusChequeRateString(
                        "/bookingengine/booking/selectrate/bonuschequerate",
                        hotelSearch.ArrivalDate.Year,
                        hotelCountryCode,
                        roomRateEntity.BaseRate.Rate, roomRateEntity.BaseRate.CurrencyCode, AppConstants.PER_NIGHT,
                        AppConstants.PER_ROOM);
                    break;
                case SearchType.REDEMPTION:

                    rate = RoomRateUtil.GetRate(roomRateEntity.RatePlanCode);
                    if (rate != null)
                    {
                        roomRatePlan = rate.RateCategoryName;
                    }
                    rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(roomRateEntity.RatePlanCode);
                    if (rateCategory != null)
                    {
                        rateCategoryDescription = rateCategory.RateCategoryDescription;
                    }
                    if (roomRateEntity != null && roomRateEntity.PointsDetails != null)
                        pricePerNight = WebUtil.GetRedeemString(roomRateEntity.PointsDetails.PointsRequired);

                    break;
                case SearchType.REGULAR:
                    rate = RoomRateUtil.GetRate(roomRateEntity.RatePlanCode);
                    if (rate != null)
                    {
                        roomRatePlan = rate.RateCategoryName;
                    }
                    if (roomRateEntity != null && roomRateEntity.RatePlanCode != null)
                    {
                        rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(roomRateEntity.RatePlanCode);
                        if (rateCategory != null)
                        {
                            rateCategoryDescription = rateCategory.RateCategoryDescription;
                        }
                    }
                    pricePerNight = string.Format("{0}{1}{2}",
                                    WebUtil.GetPricePerNight(roomRateEntity.TotalRate.Rate, hotelSearch.NoOfNights),
                                    AppConstants.SPACE, roomRateEntity.TotalRate.CurrencyCode);
                    break;

                default:

                    rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(roomRateEntity.RatePlanCode);
                    if (rateCategory != null)
                    {
                        roomRatePlan = rateCategory.RateCategoryName;
                        rateCategoryDescription = rateCategory.RateCategoryDescription;
                    }
                    if (Utility.IsBlockCodeBooking)
                    {
                        Block block = null;
                        if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                        {
                            block = Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(
                                    SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                        }
                        else
                        {
                            block = Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(
                                    BookingEngineSessionWrapper.BookingDetails.HotelSearch.CampaignCode);
                        }
                        if (block != null)
                        {
                            rateCategoryDescription = block.BlockDescription;
                        }
                    }
                    if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null && HotelRoomRateSessionWrapper.ListHotelRoomRate.Count > 0)
                    {
                        if (Utility.IsBlockCodeBooking)
                        {
                            foreach (BaseRoomRateDetails roomDtls in HotelRoomRateSessionWrapper.ListHotelRoomRate)
                            {
                                if (roomDtls != null)
                                {
                                    if (roomDtls.RateCategories != null && roomDtls.RateCategories.Count == 0)
                                    {
                                        rateDisplay = new BlockCodeRoomRateDisplay(roomDtls as BlockCodeRoomRateDetails);
                                        foreach (RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                                        {
                                            roomRatePlan = hdrDisplay.Title;
                                        }
                                    }
                                    else
                                    {
                                        foreach (RateCategory rateCtgry in roomDtls.RateCategories)
                                        {
                                            rateDisplay = new BlockCodeRoomRateDisplay(roomDtls as BlockCodeRoomRateDetails);
                                            if ((rateCtgry != null) && (rateCategory.RateCategoryId == rateCtgry.RateCategoryId))
                                            {
                                                isTrue = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (isTrue)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            foreach (BaseRoomRateDetails roomDtls in HotelRoomRateSessionWrapper.ListHotelRoomRate)
                            {
                                if (roomDtls != null && roomDtls is NegotiatedRoomRateDetails)
                                {
                                    foreach (RateCategory rateCtgry in roomDtls.RateCategories)
                                    {
                                        if ((rateCtgry != null) && (rateCategory.RateCategoryId == rateCtgry.RateCategoryId))
                                        {
                                            rateDisplay = new NegotiatedRoomRateDisplay(roomDtls as NegotiatedRoomRateDetails);
                                            isTrue = true;
                                            break;
                                        }
                                    }
                                }
                                else if (roomDtls != null && roomDtls is RedemptionRoomRateDetails)
                                {
                                    foreach (RateCategory rateCtgry in roomDtls.RateCategories)
                                    {
                                        if ((rateCtgry != null) && (rateCategory.RateCategoryId == rateCtgry.RateCategoryId))
                                        {
                                            rateDisplay = new RedemptionRoomRateDisplay(roomDtls as RedemptionRoomRateDetails);
                                            isTrue = true;
                                            break;
                                        }
                                    }
                                }

                                if (isTrue)
                                {
                                    break;
                                }
                            }
                        }
                    }

                    if (rateDisplay != null)
                    {
                        foreach (RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                        {
                            if (rateCategory != null && rateCategory.RateCategoryId != null &&
                                rateCategory.RateCategoryId == hdrDisplay.Id)
                            {
                                roomRatePlan = hdrDisplay.Title;
                            }
                        }
                    }
                    else
                    {
                        if (hotelSearch.SearchingType == SearchType.CORPORATE && BookingEngineSessionWrapper.BookingDetails != null)
                        {                           
                            if (Utility.IsBlockCodeBooking)
                            {
                                roomRatePlan = BookingEngineSessionWrapper.BookingDetails.HotelSearch != null ? 
                                    BookingEngineSessionWrapper.BookingDetails.HotelSearch.CampaignCode : "";
                            }
                            else
                            {
                                roomRatePlan = BookingEngineSessionWrapper.BookingDetails.GuestInformation != null ?
                                    BookingEngineSessionWrapper.BookingDetails.GuestInformation.CompanyName : "";
                            }                            
                        }
                    }

                    pricePerNight = string.Format("{0}{1}{2}",
									WebUtil.GetPricePerNight(roomRateEntity.TotalRate.Rate, hotelSearch.NoOfNights),
                            	    AppConstants.SPACE, roomRateEntity.TotalRate.CurrencyCode);


                    break;
            }
        }

        private string pageType = string.Empty;

        /// <summary>
        /// Sets page type
        /// </summary>
        /// <param name="pageType"></param>
        public void SetPageType(string pageType)
        {
            this.pageType = pageType;
        }

        /// <summary>
        /// Populates the reservation info container.
        /// </summary>
        /// <param name="isPreviousBooking">
        /// Indicates if the content is for showing previous booking information
        /// </param>
        public void PopulateReservationInfoContainer(bool isPreviousBooking)
        {
            if (isPreviousBooking)
            {
                divPrevBookingHeader.Attributes["style"] = "display : block;";
                divRegularBookingHeader.Attributes["style"] = "display : none;";
                if (BookingEngineSessionWrapper.BookingDetails != null)
                {
                    prevBookingResNumber.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingResNoModifySelectRatePage") +
                        BookingEngineSessionWrapper.BookingDetails.ReservationNumber;
                }
            }
            else
            {
                regularBookingHeader.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingReservationNumber");
                regularBookingResNumber.InnerText = ReservationNumberSessionWrapper.ReservationNumber;
            }

            List<BookingDetailsEntity> bookingDetailsList = BookingEngineSessionWrapper.ActiveBookingDetails;

            if (bookingDetailsList == null || bookingDetailsList.Count <= 0)
            {
                this.ShowHelpLink = false;
            }
            BookingDetailsEntity bookingDetails = BookingEngineSessionWrapper.BookingDetails;

            if (bookingDetails != null)
            {
                HotelSearchEntity hotelSearchEntity = bookingDetails.HotelSearch;
                HotelRoomRateEntity hotelRoomRateEntity = bookingDetails.HotelRoomRate;
                GuestInformationEntity guestInfoEntity = bookingDetails.GuestInformation;

                HotelDestination hotelDestination = null;
                if ((hotelSearchEntity != null) && (hotelRoomRateEntity != null) && (guestInfoEntity != null))
                {
                    AvailabilityController availabilityController = new AvailabilityController();
                    hotelDestination =
                        availabilityController.GetHotelDestinationEntity(hotelSearchEntity.SelectedHotelCode);

                    if (hotelDestination != null)
                    {
                        this.CityOrHotelName = hotelDestination.Name;

                        pfLblHotelName.InnerText = hotelDestination.Name;
                        this.HotelUrl = hotelDestination.HotelPageURL;

                        hotelSearchEntity.SearchedFor.SearchString = hotelDestination.Name;
                    }
                    RoomCategory roomCategory = RoomRateUtil.GetRoomCategory(hotelRoomRateEntity.RoomtypeCode);
                    if (roomCategory != null)
                    {
                        this.NoOfRooms = bookingDetailsList.Count.ToString();
                    }
                    this.ArrivalDate = hotelSearchEntity.ArrivalDate;
                    this.DepartureDate = hotelSearchEntity.DepartureDate;
                    this.NoOfAdults = Utility.GetTotalAdults();
                    this.NoOfChildren = Utility.GetTotalChildren();


                    this.NoOfNights = hotelSearchEntity.NoOfNights;
                    string username = guestInfoEntity.FirstName + " " + guestInfoEntity.LastName;

                    username = guestInfoEntity.Title + " " + username;

                    string reservationMessage = string.Empty;
                    if (!BookingEngineSessionWrapper.IsModifyingLegBooking)
                    {
                        reservationMessage =
                            WebUtil.GetTranslatedText(
                                TranslatedTextConstansts.YOUR_BOOKING_RESERVATION_MESSAGE_WITHOUT_NUMBER);
                        reservationMessage = string.Format(reservationMessage, bookingDetails.ReservationNumber);
                    }
                    else
                    {
                        reservationMessage =
                            WebUtil.GetTranslatedText(
                                "/bookingengine/booking/searchhotel/BookingResNoModifySelectRatePage");

                        string resNumber =
                            WebUtil.GetTranslatedText(
                                "/bookingengine/booking/searchhotel/BookingResNoModifySelectRatePage") +
                            bookingDetails.ReservationNumber + AppConstants.HYPHEN + bookingDetails.LegNumber;
                        reservationMessage = string.Format(reservationMessage, resNumber);
                    }

                    TimeSpan timeSpan = hotelSearchEntity.ArrivalDate.Subtract(hotelSearchEntity.DepartureDate);
                    double basePoints = 0;
                    double totalPoints = 0;
                    basePoints = hotelRoomRateEntity.Points;

                    try
                    {
                        if (hotelSearchEntity.NoOfNights > 1)
                            basePoints = hotelRoomRateEntity.Points / hotelSearchEntity.NoOfNights;
                    }
                    catch (Exception ex)
                    {
                        AppLogger.LogFatalException(ex, "Unable to calculate base points.");
                    }

                    if (hotelRoomRateEntity.TotalRate != null)
                    {
                        totalPoints = hotelRoomRateEntity.TotalRate.Rate;
                    }

                    string ratePlanCode = hotelRoomRateEntity.RatePlanCode;
                    string selectedRateCategoryName = string.Empty;

                    if (!string.IsNullOrEmpty(ratePlanCode))
                    {
                        Rate rate = RoomRateUtil.GetRate(ratePlanCode);
                        if (rate != null)
                        {
                            selectedRateCategoryName = rate.RateCategoryName;
                        }
                    }
                    string hotelCountryCode = hotelSearchEntity.HotelCountryCode;
                    if (hotelCountryCode == null || hotelCountryCode == string.Empty)
                    {
                        if (hotelSearchEntity.SearchingType == SearchType.BONUSCHEQUE)
                        {
                            HotelSearchEntity cloneHotelSearch = hotelSearchEntity.Clone();
                            if (cloneHotelSearch != null)
                            {
                                cloneHotelSearch.ArrivalDate = DateTime.Now;
                                cloneHotelSearch.DepartureDate = DateTime.Now.AddDays(1);
                                hotelCountryCode = availabilityController.GetHotelCountryCode(cloneHotelSearch,
                                                                                              cloneHotelSearch.ListRooms
                                                                                                  [0]);
                                hotelSearchEntity.HotelCountryCode = hotelCountryCode;
                            }
                        }
                    }

                    string baseRateString = string.Empty;
                    if (hotelSearchEntity.SearchingType == SearchType.REDEMPTION)
                    {
                        baseRateString = Utility.GetRoomRateString(selectedRateCategoryName, hotelRoomRateEntity.Rate,
                                                                   hotelRoomRateEntity.Points,
                                                                   hotelSearchEntity.SearchingType,
                                                                   hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                                                   hotelSearchEntity.NoOfNights,
                                                                   hotelSearchEntity.RoomsPerNight);
                    }

                    else if ((Utility.IsBlockCodeBooking) && (string.IsNullOrEmpty(hotelRoomRateEntity.RatePlanCode))
                             && (!string.IsNullOrEmpty(hotelRoomRateEntity.RoomtypeCode)))
                    {
                        baseRateString = Utility.GetRoomRateString(hotelRoomRateEntity.Rate);
                    }
                    else
                    {
                        baseRateString = Utility.GetRoomRateString(selectedRateCategoryName, hotelRoomRateEntity.Rate,
                                                                   basePoints, hotelSearchEntity.SearchingType,
                                                                   hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                                                   AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                    }

                    string totalRateString = string.Empty;
                    double totalRateToAdd = 0;
                    RateEntity rateEntity = null;
                    string CurrencyCodeValue = string.Empty;

                    HotelRoomRateEntity hotelRoomRate = null;
                    if (BookingEngineSessionWrapper.BookingDetails != null)
                    {
                        string legNumber = BookingEngineSessionWrapper.BookingDetails.LegNumber;
                        BookingDetailsEntity bookDetEntity = null;
                        bookDetEntity = BookingEngineSessionWrapper.GetLegBookingDetails(legNumber,
                                                                            BookingEngineSessionWrapper.BookingDetails.
                                                                                ReservationNumber);
                        hotelRoomRate = bookDetEntity.HotelRoomRate;
                        totalRateToAdd += hotelRoomRate.TotalRate.Rate;
                        CurrencyCodeValue = hotelRoomRate.TotalRate.CurrencyCode;
                    }

                    totalRateToAdd = Utility.CalculateTotalRateInCaseOfModifyFlow();

                    rateEntity = new RateEntity(totalRateToAdd, CurrencyCodeValue);
                    switch (hotelSearchEntity.SearchingType)
                    {
                        case SearchType.REDEMPTION:
                            {
                                totalRateString = Utility.GetRoomRateString(null, null, hotelRoomRateEntity.Points,
                                                                            hotelSearchEntity.SearchingType,
                                                                            hotelSearchEntity.ArrivalDate.Year,
                                                                            hotelCountryCode,
                                                                            hotelSearchEntity.NoOfNights,
                                                                            hotelSearchEntity.RoomsPerNight);
                                break;
                            }
                        case SearchType.VOUCHER:
                            {
                                totalRateString = Utility.GetRoomRateString(null, null, totalPoints,
                                                                            hotelSearchEntity.SearchingType,
                                                                            hotelSearchEntity.ArrivalDate.Year,
                                                                            hotelCountryCode,
                                                                            hotelSearchEntity.NoOfNights,
                                                                            hotelSearchEntity.RoomsPerNight);
                                break;
                            }
                        default:
                            {
                                totalRateString = Utility.GetRoomRateString(null, rateEntity, totalPoints,
                                                                            hotelSearchEntity.SearchingType,
                                                                            hotelSearchEntity.ArrivalDate.Year,
                                                                            hotelCountryCode,
                                                                            hotelSearchEntity.NoOfNights,
                                                                            hotelSearchEntity.RoomsPerNight);
                                break;
                            }
                    }
                    this.SetTotalRoomRate(totalRateString);
                    if (bookingDetailsList != null && bookingDetailsList.Count > 0)
                        this.SetRoomDetails(bookingDetailsList);
                }
            }
        }

        private string GetViewModifyDLRedirectLink()
        {
            string last_Name = string.Empty;
            if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REDEMPTION)
                last_Name = GuestBookingInformationSessionWrapper.FetchedGuestInformation.LastName;
            else
                last_Name = GuestBookingInformationSessionWrapper.GuestBookingInformation.LastName;

            return WebUtil.GetViewModifyDLRedirectLink(HttpContext.Current.Request.Url.Host, AppConstants.DLREDIRECT,
                AppConstants.DEEPLINK_VIEW_MODIFY_RESERVATION_ID, ReservationNumberSessionWrapper.ReservationNumber,
                AppConstants.DEEPLINK_VIEW_MODIFY_LAST_NAME, last_Name);

        }

        /// <summary>
        /// Populates cancellation numbers
        /// </summary>
        public void PopulateCancellationNumbers()
        {
            if (BookingEngineSessionWrapper.AllCancelledDetails != null && BookingEngineSessionWrapper.AllCancelledDetails.Count > 0)
            {
                cancelNumbers.Visible = true;
                int i = 0;
                foreach (CancelDetailsEntity cnclEntity in BookingEngineSessionWrapper.AllCancelledDetails)
                {
                    cnclnums.InnerText += cnclEntity.CancelNumber + ", ";
                    pfCnclnums.InnerText += cnclEntity.CancelNumber + ", ";
                    i++;
                }


                if (!string.IsNullOrEmpty(cnclnums.InnerText))
                {
                    cnclnums.InnerText = cnclnums.InnerText.Remove(cnclnums.InnerText.Length - 2);
                    pfCnclnums.InnerText = pfCnclnums.InnerText.Remove(pfCnclnums.InnerText.Length - 2);

                    if (i > 1)
                    {
                        headerCncl.InnerText =
                            WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/cancellationNumbers");
                        pfHeaderCncl.InnerText =
                            WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/cancellationNumbers");
                    }
                    else if (i == 1)
                    {
                        headerCncl.InnerText =
                            WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/cancellationNumber") +
                            ":";
                        pfHeaderCncl.InnerText =
                            WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/cancellationNumber") +
                            ":";
                    }
                }
            }
        }

        /// <summary>
        /// Populate cancellation numbers in modify
        /// </summary>
        public void PopulateCancellationNumbersInModify()
        {
            cancelNumbers.Visible = true;
            int i = 0;
            foreach (BookingDetailsEntity bkngDetls in BookingEngineSessionWrapper.AllBookingDetails)
            {
                if (bkngDetls.CancelDetails != null)
                {
                    cnclnums.InnerText += bkngDetls.CancelDetails.CancelNumber + ", ";
                    pfCnclnums.InnerText += bkngDetls.CancelDetails.CancelNumber + ", ";
                    i++;
                }
            }


            if (!string.IsNullOrEmpty(cnclnums.InnerText))
            {
                cnclnums.InnerText = cnclnums.InnerText.Remove(cnclnums.InnerText.Length - 2);
                pfCnclnums.InnerText = cnclnums.InnerText.Remove(cnclnums.InnerText.Length - 2);

                if (i > 1)
                {
                    headerCncl.InnerText =
                        WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/cancellationNumbers");
                    pfHeaderCncl.InnerText =
                        WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/cancellationNumbers");
                }
                else if (i == 1)
                {
                    headerCncl.InnerText =
                        WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/cancellationNumber") + ":";
                    pfHeaderCncl.InnerText =
                        WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/cancellationNumber") + ":";
                }
            }
        }

        /// <summary>
        /// Shows or hides charge info texts.
        /// </summary>
        /// <param name="display"></param>
        public void ShowHideChargeInfoTexts(bool display)
        {
            lblChargeInfoRoom1.Visible = display;
            lblChargeInfoRoom2.Visible = display;
            lblChargeInfoRoom3.Visible = display;
            lblChargeInfoRoom4.Visible = display;
        }

        #region INavigationTraking Members

        public List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> parameters = new List<KeyValueParam>();
            parameters.Add(new KeyValueParam("Action", actionName));
            parameters.Add(new KeyValueParam("Reservation No", (isPrevBooking)
                                                                ? BookingEngineSessionWrapper.BookingDetails.ReservationNumber
                                                                : ReservationNumberSessionWrapper.ReservationNumber));
            return parameters;
        }

        #endregion
    }
}
