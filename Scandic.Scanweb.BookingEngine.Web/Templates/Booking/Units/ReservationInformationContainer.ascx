<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReservationInformationContainer.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ReservationInformationContainer" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<!-- Reservation information -->
<%--<div id="resInfo">		
			<div class="resInfo-top" >&nbsp;</div>
			<div id="divReservationNumber" runat="server" class="resNumber" visible="false">
			    <p id="lblReservationNumber" runat="server" visible="false"></p>
		    </div>
			<div class="resInfo-inner">
			<div id="HelpContent" style="<%=HelpVisibility()%>" >
			    <p class="popupLink"> 
			        <a href='#' onclick="openPopupWin('<%=HelpPageUrl%>','width=800,height=600,scrollbars=yes,resizable=yes');return false;"><%=WebUtil.GetTranslatedText("/bookingengine/booking/common/help")%></a>
			    </p>
			</div>
            <div class="clear">&nbsp;</div>
			<div class="toggleLink" id = "divExpandableBooking" runat="server" visible = "false">
                <p>  <span class="collapseLink">
                    <a href="#" id="ExpandableBlock" runat="server">
                        <label id="lblExpandableBookingMsg" runat="server"></label>
                    </a>
                </span>
                </p>
            </div>
            <div class="clear">&nbsp;</div>
			<div id ="PrevBookingInfo" class="bookingInfo" runat="server">
                <div id="divNoAvailability" runat="server">
                    <p class="errorText"  id= "divNoAvailabilityMessage" runat="server"></p>
                </div>
			    
			    <div id="divReservationMessage" runat="server" visible="false">
				    <p id="lblReservationMessage" runat="server" visible="false"></p>
			    </div>
			    <div id="divLongTextMessage" runat="server" visible="false">
				    <p id="lblLongTextMessage" runat="server" visible="false"></p><br />
			    </div>
				    <div id="divAvailability" visible="true" runat="server">
					    <div class="colOne">
						    <p>
							    <span><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/hotelorcity")%></span><br />
							    <span class="showLink"><strong id="lblCityorHotelName" runat="server"></strong></span>
						    </p>
						    <p>
							    <span><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/noofrooms")%></span><br />
							    <span><strong id="lblNoOfRoomsValue" runat="server"></strong></span>
						    </p>
						    <p id="lblRoomAndRateInfo" runat="server" visible="false">
							    <span><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/roomrate")%></span><br />
							    <span><strong id ="lblRoomAndRateInfoValue" runat="server" visible="true"></strong></span>
						    </p>
						    <!--Scanweb 1.6 - START-->
						    <%--<p id="lblChildrensAges" runat="server" visible="false">
				                <span><%=WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/childrensages")%></span><br />
				                <span><strong id="lblChildrensAgesValue" runat="server"></strong></span>
			                </p>--%>
<!-- Reservation information - Bhavya - Cancel -->
<%--<div id="masterReserMod21">
    <div class="greyRd">
        <div class="hd sprite">
        </div>--%>
<%
     if ((Request.QueryString["command"] == null) ||
         ((Request.QueryString["command"] != null) && (Request.QueryString["command"].ToString() == "isPDF")))
     {%>
<div class="hdLft scansprite" id="divPrevBookingHeader" runat="server" style="display: none;">
    <h2 class="hdRt">
        <label id="prevBookingResNumber" runat="server" style="float: left;">
        </label>
        <span class="help scansprite toolTipMe prevbook" id="lnkReservationToolTip" runat="server"
            style="display: none; float: none;"></span>
        <%--<a class="help spriteIcon toolTipMe" id="lnkReservationToolTip">&nbsp;</a>--%>
    </h2>
</div>
<div class="cnt modifyRoom">
    <div class="greyRd">
        <div class="cnt bookingDetailBorder" id="divCnt" runat="server">
            <div id="divRegularBookingHeader" runat="server" style="display: block;" class="modifyRoomContainer">
                <div class="hd">
                </div>
                <p class="fltLft lblReservationHeader">
                    <strong><span id="regularBookingHeader" runat="server"></span></strong><span runat="server"
                        id="helpIcon"><span class="help spriteIcon  toolTipMe" style="float: none;" title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/bookingREservationNoToolTip") %>">
                        </span></span>
                    <%-- <a class="help spriteIcon toolTipMe" id="reservationNumberHelpIcon" href="#" runat="server"></a>--%>
                    <%-- <a id = "" class="help spriteIcon toolTipMe" title="Booking Reservation number" href="#"></a>--%>
                    <span id="regularBookingResNumber" runat="server"></span>
                </p>
                <div class="clearAll">
                    <!-- Clearing Float -->
                </div>
                <p class="fltLft" id="cancelNumbers" runat="server" visible="false">
                    <strong id="headerCncl" runat="server"></strong><strong id="cnclnums" runat="server">
                    </strong>
                </p>
                <p class="fltRt" sizcache="0" sizset="56">
                    <a id="lnkBtnPrintReceipt" runat="server" href="#" target="_top" class="print spriteIcon pntfdlylink"
                        visible="true" onmousedown="javascript:handleRightMouseClick(event);" onmouseup="javascript:handleRightMouseClick(event);"
                        onclick="javascript:getPrinter(receiptURL);return false;" style="float: right;">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/PrintReceipt")%>
                    </a>
                    <br />
                    <asp:LinkButton class="modLink" ID="lnkModifyContactDetails" OnClick="lnkModifyContactDetails_Click"
                        runat="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/ModifyContactDetails") %></asp:LinkButton>
                </p>
                <div class="clear">
                </div>
                <hr />
            </div>

            <script language="javascript" type="text/javascript">
                var receiptURL = '<%= receiptURL%>';
            </script>

            <!--START: error Text -->
            <div id="divNoAvailability" runat="server">
                <p class="errorText" id="divNoAvailabilityMessage" runat="server">
                </p>
            </div>
            <!--END: error Text -->
            <div class="detailsColmLeft">
                <p>
                    <strong>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/namehotelheader") %>
                    </strong><span id="lblspHotelName" runat="server"><a tabindex="21" id="lblHotelName"
                        runat="server"></a></span><span id="spnHotelName" runat="server"></span>
                    <%--<asp:LinkButton TabIndex="3" ID="lnkBtnHotelName" runat="server"></asp:LinkButton>	--%>
                </p>
                <p>
                    <strong>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin") %>
                    </strong><span id="lblCheckIn" runat="server"></span>
                </p>
                <p>
                    <strong>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut") %>
                    </strong><span id="lblCheckOut" runat="server"></span>
                </p>
            </div>
            <div class="detailsColmRight">
                <p>
                    <strong>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/noofnights") %>
                    </strong><span id="lblNoOfNightsValue" runat="server"></span>
                </p>
                <p>
                    <strong>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/noofrooms") %>
                    </strong><span id="lblNoOfRoomsValue" runat="server"></span>
                </p>
                <!--artf1150474|Rajneesh
	             Description:Spelling mistakes on the e-mail confirmation
	             Modification:Added one else-If statement to handle the condition:Multiple Adults,No Child selection
	              -->
                <p class="clear">
                    <strong>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Occupants") %>
                    </strong><span id="lblNoOfAdultsValue" runat="server" style="display: none"></span>
                    <span id="lblNoOfChildrenValue" runat="server" style="display: none"></span>
                    <% if (Convert.ToInt32(NoOfAdults) > 1 && Convert.ToInt32(NoOfChildren) > 0)
                       { %>
                    <%= NoOfAdults %>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>,
                    <% } %>
                    <%
                       else if (Convert.ToInt32(NoOfAdults) == 1 && Convert.ToInt32(NoOfChildren) > 0)
                       {%>
                    <%= NoOfAdults %>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Adult") %>,
                    <% } %>
                    <%
                       else if (Convert.ToInt32(NoOfAdults) > 1 && Convert.ToInt32(NoOfChildren) == 0)
                       {%>
                    <%= NoOfAdults %>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                    <% } %>
                    <%
                       else
                       {%>
                    <%= NoOfAdults %>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Adult") %>
                    <% } %>
                    <% if (Convert.ToInt32(NoOfChildren) > 1)
               { %>
                    <%= NoOfChildren %>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/children") %>
                    <% } %>
                    <%
               else if (Convert.ToInt32(NoOfChildren) == 1)
               {%>
                    <%= NoOfChildren %>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child") %>
                    <% } %>
                </p>
            </div>
            <div id="divSummRoom1" runat="server" visible="false" class="roundMe grayBox boxMargBtm"
                style="-moz-border-radius-topleft: 5px; -moz-border-radius-topright: 5px; -moz-border-radius-bottomleft: 5px;
                -moz-border-radius-bottomright: 5px;">
                <div class="detailsColmLeft">
                    <p class="fltLft textClr">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room1") %>
                        </strong><span id="room1No" runat="server"></span>
                    </p>
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/RoomType") %>
                        </strong><span id="lblRoomCatName1" runat="server"></span>
                    </p>
                </div>
                <div class="detailsColmRight" id="divPriceDetails1" runat="server">
                    <!--<p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/sortRate") %>
                        </strong><span style="display: block; float: left; width: 168px;position:relative;top:4px;"><span id="lblRatePlan1_bak" runat="server"></span>
                        <span style="position:relative;top:-4px;"><a class="help spriteIcon toolTipMe" id="lblAnchSummRatePlan1_bak" runat="server">&nbsp;</a></span>
                         </span>
                    </p>-->
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/sortRate") %>
                        </strong><span style="float: left; width: 180px;"><span id="lblRatePlan1" runat="server"
                            style="float: left;"></span><a class="help spriteIcon toolTipMe" id="lblAnchSummRatePlan1"
                                runat="server"></a></span>
                    </p>
                    <p class="fltLft">
                        <strong id="rateRoom1" runat="server" style="width: 140px" />
                        <!-- Release2.0|BugID:513272|Rajneesh-->
                        <span class="roomPerRateNight"><span id="lblPricePerNight1" runat="server" /><span
                            id="perRoom1" runat="server" /><span id="lblChargeInfoRoom1" runat="server"></span>
                        </span>
                    </p>
                </div>
                <br class="clear" />
            </div>
            <div id="divSummRoom2" runat="server" visible="false" class="roundMe grayBox boxMargBtm"
                style="-moz-border-radius-topleft: 5px; -moz-border-radius-topright: 5px; -moz-border-radius-bottomleft: 5px;
                -moz-border-radius-bottomright: 5px;">
                <div class="detailsColmLeft">
                    <p class="fltLft textClr">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room2") %>
                        </strong><span id="room2No" runat="server"></span>
                    </p>
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/RoomType") %>
                        </strong><span id="lblRoomCatName2" runat="server"></span>
                    </p>
                </div>
                <div class="detailsColmRight" id="divPriceDetails2" runat="server">
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/sortRate") %>
                        </strong><span style="float: left; width: 180px;"><span id="lblRatePlan2" runat="server"
                            style="float: left;"></span><a class="help spriteIcon toolTipMe" id="lblAnchSummRatePlan2"
                                runat="server"></a></span>
                    </p>
                    <p class="fltLft">
                        <strong id="rateRoom2" runat="server" style="width: 140px" /><span class="roomPerRateNight">
                            <span id="lblPricePerNight2" runat="server" /><span id="perRoom2" runat="server" />
                            <span id="lblChargeInfoRoom2" runat="server" /></span>
                    </p>
                    <%--<p class="fltLft"><strong>Price per night</strong><span>1990 SEK per room</span></p>--%>
                </div>
                <br class="clear" />
            </div>
            <div id="divSummRomm3" runat="server" visible="false" class="roundMe grayBox boxMargBtm"
                style="-moz-border-radius-topleft: 5px; -moz-border-radius-topright: 5px; -moz-border-radius-bottomleft: 5px;
                -moz-border-radius-bottomright: 5px;">
                <div class="detailsColmLeft">
                    <p class="fltLft textClr">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room3") %>
                        </strong><span id="room3No" runat="server"></span>
                    </p>
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/RoomType") %>
                        </strong><span id="lblRoomCatName3" runat="server"></span>
                    </p>
                </div>
                <div class="detailsColmRight" id="divPriceDetails3" runat="server">
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/sortRate") %>
                        </strong><span style="float: left; width: 180px;"><span id="lblRatePlan3" runat="server"
                            style="float: left;"></span><a class="help spriteIcon toolTipMe" id="lblAnchSummRatePlan3"
                                runat="server"></a></span>
                    </p>
                    <p class="fltLft">
                        <strong id="rateRoom3" runat="server" style="width: 140px"></strong><span class="roomPerRateNight">
                            <span id="lblPricePerNight3" runat="server" /><span id="perRoom3" runat="server" />
                            <span id="lblChargeInfoRoom3" runat="server" /></span>
                    </p>
                    <%--<p class="fltLft"><strong>Price per night</strong><span>1990 SEK per room</span></p>--%>
                </div>
                <br class="clear" />
            </div>
            <div id="divSummRoom4" runat="server" visible="false" class="roundMe grayBox boxMargBtm"
                style="-moz-border-radius-topleft: 5px; -moz-border-radius-topright: 5px; -moz-border-radius-bottomleft: 5px;
                -moz-border-radius-bottomright: 5px;">
                <div class="detailsColmLeft">
                    <p class="fltLft textClr">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room4") %>
                        </strong><span id="room4No" runat="server"></span>
                    </p>
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/RoomType") %>
                        </strong><span id="lblRoomCatName4" runat="server"></span>
                    </p>
                </div>
                <div class="detailsColmRight" id="divPriceDetails4" runat="server">
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/sortRate") %>
                        </strong><span style="float: left; width: 180px;"><span id="lblRatePlan4" runat="server"
                            style="float: left;"></span><a class="help spriteIcon toolTipMe" id="lblAnchSummRatePlan4"
                                runat="server"></a></span>
                    </p>
                    <p class="fltLft">
                        <strong id="rateRoom4" runat="server" style="width: 140px"></strong><span class="roomPerRateNight">
                            <span id="lblPricePerNight4" runat="server" /><span id="perRoom4" runat="server" />
                            <span id="lblChargeInfoRoom4" runat="server" /></span>
                    </p>
                </div>
                <br class="clear" />
            </div>
            <div class="clearAll">
                &#160;</div>
            <div class="totalConf fltRt" id="divTotalRate" runat="server">
                <p>
                    <small>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Total") %>
                    </small><strong><span id="lblTotalRoomRateValue" runat="server"></span></strong>
                    <strong id="lblBonusString" runat="server" visible="false"></strong>
                </p>
                <!--REEP CR-->
                <!-- 524072: On reservations, there needs to be stated that Total rate is incl. taxes and fees. Just as in the Your stay module.-->
                <div class="colm2 bookDetail">
                    <p id="pCartTerms" runat="server">
                    </p>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
</div>
<% } %>
<%
     if (Request.QueryString["command"] != null &&
         ((Request.QueryString["command"].ToString() == "print") ||
          (Request.QueryString["command"].ToString() == "CancelPageprint")))
     {%>
<!-- Page Title -->
<div id="divPrinterFriendly" visible="false" runat="server">
    <div class="section">
        <h3>
            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingReservationNumber") %>
            <%= ReservationNumberSessionWrapper.ReservationNumber %></h3>
        <h3>
            <span id="pfHeaderCncl" runat="server"></span><span id="pfCnclnums" runat="server">
            </span>
        </h3>
        <div id="viewModifyDLRedirectPDFLink" visible="false" runat="server">
            <div class="row clearFix">
                <table cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/ViewModifyReservationMessage")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a id="ancViewModifyDLRedirectLink" runat="server"></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Hotel Information -->
        <div class="row clearFix">
            <table cellspacing="0">
                <tbody>
                    <tr>
                        <td class="dataCol1">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/namehotelheader") %>
                        </td>
                        <td class="dataCol2">
                            <span id="pfLblHotelName" runat="server"></span>
                        </td>
                        <td class="dataCol3">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/noofnights") %>
                        </td>
                        <td class="dataCol4">
                            <span id="pfLblNoOfNights" runat="server"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="dataCol1">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin") %>
                        </td>
                        <td class="dataCol2">
                            <span id="pfLblCheckIn" runat="server"></span>
                        </td>
                        <td class="dataCol3">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/noofrooms") %>
                        </td>
                        <td class="dataCol4">
                            <span id="pfLblNoOfRooms" runat="server"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="dataCol1">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut") %>
                        </td>
                        <td class="dataCol2">
                            <span id="pfLblCheckOut" runat="server"></span>
                        </td>
                        <td class="dataCol3">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Occupants") %>
                        </td>
                        <td class="dataCol4">
                            <span id="pfLblNoOfAdultsValue" runat="server" style="display: none"></span><span
                                id="pfLblNoOfChildrenValue" runat="server" style="display: none"></span>
                            <% if (Convert.ToInt32(NoOfAdults) > 1 && Convert.ToInt32(NoOfChildren) > 0)
                               { %>
                            <%= NoOfAdults %>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>,
                            <% } %>
                            <%
                               else if (Convert.ToInt32(NoOfAdults) == 1 && Convert.ToInt32(NoOfChildren) > 0)
                               {%>
                            <%= NoOfAdults %>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Adult") %>,
                            <% } %>
                            <%
                               else if (Convert.ToInt32(NoOfAdults) > 1 && Convert.ToInt32(NoOfChildren) == 0)
                               {%>
                            <%= NoOfAdults %>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                            <% } %>
                            <%
                               else
                               {%>
                            <%= NoOfAdults %>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Adult") %>
                            <% } %>
                            <% if (Convert.ToInt32(NoOfChildren) > 1)
                                   { %>
                            <%= NoOfChildren %>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/children") %>
                            <% } %>
                            <%
                                   else if (Convert.ToInt32(NoOfChildren) == 1)
                                   {%>
                            <%= NoOfChildren %>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child") %>
                            <% } %>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- Room1 Information -->
        <div id="pfDivSummRoom1" runat="server" visible="false">
            <div class="row clearFix">
                <table cellspacing="0">
                    <tbody>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room1") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfRoom1No" runat="server"></span>
                            </td>
                            <td class="dataCol3">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/sortRate") %>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblRatePlan1" runat="server"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/RoomType") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfLblRoomCatName1" runat="server"></span>
                            </td>
                            <td class="dataCol3">
                                <span id="pfRateRoom1" runat="server"></span>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblPricePerNight1" runat="server"><span id="pfPerRoom1" runat="server"></span>
                                </span><span id="pflblChargedAtHotelRoom1" runat="server"></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Room2 Information -->
        <div id="pfDivSummRoom2" runat="server" visible="false">
            <div class="row clearFix">
                <table cellspacing="0">
                    <tbody>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room2") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfRoom2No" runat="server"></span>
                            </td>
                            <td class="dataCol3">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/sortRate") %>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblRatePlan2" runat="server"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/RoomType") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfLblRoomCatName2" runat="server"></span>
                            </td>
                            <td class="dataCol3">
                                <span id="pfRateRoom2" runat="server"></span>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblPricePerNight2" runat="server"><span id="pfPerRoom2" runat="server"></span>
                                </span><strong><span id="pflblChargedAtHotelRoom2" runat="server"></span></strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Room3 Information -->
        <div id="pfDivSummRoom3" runat="server" visible="false">
            <div class="row clearFix">
                <table cellspacing="0">
                    <tbody>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room3") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfRoom3No" runat="server"></span>
                            </td>
                            <td class="dataCol3">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/sortRate") %>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblRatePlan3" runat="server"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/RoomType") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfLblRoomCatName3" runat="server"></span>
                            </td>
                            <td class="dataCol3">
                                <span id="pfRateRoom3" runat="server"></span>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblPricePerNight3" runat="server"><span id="pfPerRoom3" runat="server"></span>
                                </span><strong><span id="pflblChargedAtHotelRoom3" runat="server"></span></strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Room4 Information -->
        <div id="pfDivSummRoom4" runat="server" visible="false">
            <div class="row clearFix">
                <table cellspacing="0">
                    <tbody>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room4") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfRoom4No" runat="server"></span>
                            </td>
                            <td class="dataCol3">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/sortRate") %>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblRatePlan4" runat="server"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="dataCol1">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/RoomType") %>
                            </td>
                            <td class="dataCol2">
                                <span id="pfLblRoomCatName4" runat="server"></span>
                            </td>
                            <td class="dataCol3">
                                <span id="pfRateRoom4" runat="server"></span>
                            </td>
                            <td class="dataCol4">
                                <span id="pfLblPricePerNight4" runat="server"><span id="pfPerRoom4" runat="server"></span>
                                </span><strong><span id="pflblChargedAtHotelRoom4" runat="server"></span></strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Total -->
        <div class="row totalWrap clearFix">
            <div class="total">
                <p>
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Total")%>
                    <span id="pfLblTotalRoomRateValue" runat="server"></span><strong id="pfLblBonusString"
                        runat="server" visible="false"></strong>
                </p>
                <p class="incl" id="pfCartTerms" runat="server">
                </p>
            </div>
        </div>
    </div>
</div>
<% } %>
<%-- <div class="ft sprite">
        </div>
    </div>
</div>--%>
<!-- Reservation information - Bhavya - Cancel End-->
