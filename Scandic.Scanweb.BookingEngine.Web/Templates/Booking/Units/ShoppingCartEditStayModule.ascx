<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ShoppingCartEditStayModule.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ShoppingCartEditStayModule" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Controller" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%--<%@ Register TagPrefix="Booking" TagName="FlyOut" Src="~/Templates/Booking/Units/BookingModuleFlyout.ascx" %>--%>
<%@ Register Src="Calendar.ascx" TagName="Calendar" TagPrefix="uc2" %>
<%@ Register Src="DestinationSearch.ascx" TagName="DestinationSearch" TagPrefix="uc1" %>


<div class="stoolHeader">
   <%-- <div class="hd sprite">
    </div> Commented this code to remove the background image --%>
   <%-- <div class="cnt">
        <h2 class="stoolHeading">
            <asp:Label runat="server" ID="lblHeaderText">
            </asp:Label>
        </h2>
    </div>--%>
    <div class="cnt">
    <% if (Utility.GetCurrentLanguage().Equals("ru-RU") || Utility.CheckifFallback())
       {%>
                       <h2 class="stoolHeadingAlternate">               
                    <% }
       else
       {%> 
				            <h2 class="stoolHeading">                   
                    <% } %>
                    <asp:Label runat="server" ID="lblHeaderText">
                    </asp:Label>
                    </h2>
            
    </div>
    
    
    <%--  <div class="ft sprite">
        &nbsp;</div>ommented this code to remove the background image --%>
</div>
<div id="editStay" runat="server" class="mainContent cnt fltLft modifyDetail" style="display: none;">
    <% string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();%>
    <input id="childrenCriteriaHidden" type="hidden" value="" class="hiddenTxt" runat="server" />
    <input id="bedTypeCollection" type="hidden" class="hiddenTxt" runat="server" />
    <input type="hidden" id="siteLang" name="siteLang" value="<%= siteLanguage %>" />
    <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />   
  <!--SiteExpansion | artf1243679 : Not able to edit the booking with children-->
    <input type="hidden" id="defaultBookingCode" name="defaultBookingCode" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingCode") %>" />
    <!--Rel 2.2.7: AMS Patch 4| artf1216848 : Scanweb - Incorrect error when searching a destination that is not choosen from list 
    <input type="hidden" id="customerMessage" name="customerMessage" runat="server" />    -->
    
    <input type="hidden" id="destinationError" name="destinationError" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name") %>' />
    <input type="hidden" id="duplicateDestinationError" name="duplicateDestinationError"
        value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/duplicate_destination") %>' />
    <input type="hidden" id="arrivaldateError" name="arrivaldateError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/arrival_date") %>' />
    <input type="hidden" id="departuredateError" name="departuredateError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/departure_date") %>' />
    <input type="hidden" id="NoOfNightError" name="NoOfNightError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/no_of_nights") %>' />
    <input type="hidden" id="invalidGuestNumberError" name="invalidGuestNumberError"
        value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/user_name") %>' />
    <input type="hidden" id="invalidGuestPINError" name="invalidGuestPINError" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/password") %>' />
    <input type="hidden" id="invalidGuestLoginError" name="invalidGuestLoginError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/user_name_password") %>' />
    <input type="hidden" id="invalidSpecialCodeError" name="invalidSpecialCodeError"
        value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/special_booking_code") %>' />
    <input type="hidden" id="invalidNegotiatedCode" name="invalidNegotiatedCode" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/negotiated_rate_code") %>' />
    <input type="hidden" id="invalidVoucherCode" name="invalidVoucherCode" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/voucher_booking_code") %>' />
    <input type="hidden" id="specialCodeToDNumber" name="specialCodeToDNumber" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/invalidSpecialCode") %>' />
    <input type="hidden" id="specialCodeToTravelAgent" name="specialCodeToTravelAgent"
        value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/businesserror/invalidSpecialCodeTravelAgent") %>' />
    <!-- searchMode value can be changed based on the login -->
    <input type="hidden" id="guestLogin" value="false" runat="server" />
    <input type="hidden" id="sT" value='<%=  BookingTab.Tab1 %>' runat="server" />
    <input type="hidden" id="hdnModifyOrCancelPageName" runat="server" />
    <input type="hidden" id="selectedDestId" runat="server" />
    <%--<input type="hidden" id="selectedDestId1" runat="server" />--%>
    <input type="hidden" id="loginSrchID" runat="server" />
    <input type="hidden" id="loginSrchPageID" runat="server" />
    <input type="hidden" id="noOfRoomsSelected" runat="server" value="0" />
    <input type="hidden" id="childAgeNotSelectedError" name="childrenAgeNotSelectedError" value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidAge") %>"/>
    <input type="hidden" id="ChildBedTypeNotSelectedError" name="ChildBedTypeNotSelectedError" value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidBedType") %>"/>
    <input type="hidden" id="childInAdultBedandAdultCountError" name="childInAdultBedandAdultCountError" value="<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalidbedtypechoosen") %>" />
    <input type="hidden" id="childInAdultsBedType" name="childInAdultsBedType" value="<%=
                WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/sharingbed") %>" />
    
    <input type="hidden" id="fillThis" name="fillThis" value="<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/fillThis") %>" />
    <input type="hidden" id="validDestination" name="validDestination" value="<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name_valid") %>" />
    <input type="hidden" id="noOfRoomsSelFormVal" class="noOfRoomsSelected" runat="server" value="1" />
    <!--Priya Singh: Error msg displayed when invalid block code entered.
    Artifact artf1217694 : Scanweb - Input control missing for incorrect block code  -->
    <input type="hidden" id="invalidBlockCode" name="invalidBlockCode" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/InvalidBlockCode") %>" />
    <!--Boxi : Added input control for special charecter validation in Booking Code field -->
    <input type="hidden" id="invalidBlockCodeChars" name="invalidBlockCodeChars" value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/noSpecialCaracters") %>" />    
    <input type="hidden" id="BookingCodeIata" name="BookingCodeIata" />  
    <input type="hidden" id="IATANumber" name="IATANumber" value="" />
    <input type="hidden" id="InvalidIATAnumber" name="InvalidIATAnumber" value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/IataSpecialCaracters") %>" />
    <div id="clientErrorDiv" runat="server" style="display:none" ></div>
    <%--RK: Reservation 2.0 | artf1149137 | Made the divs diplay none --%>
    <div id="RegClientErrorDiv" class="errorText setWidth" runat="server" style="display:none" EnableViewState="False">
            </div>
    <div id="tabContainer1">
    <div id="shoppingEditStay" runat="server">
    <div class="calCont" id="disableFlds" runat="server">
        <p style="font-size: 12px;">
            <asp:Label runat="server" ID="lblHotelText"></asp:Label>
        </p>
        <p class="margin0 BBSmallLabel">
            <strong>
                <asp:Label runat="server" ID="lblWhereToStay"></asp:Label>
            </strong>
        </p>
        <div class="bookingCode" id="hotelSearchBox" runat="server">
            <input id="txtHotelName" tabindex="201" type="text" name="city"
                autocomplete="off" runat="server" class="stayInputLft widthChng validate[required] validate[hotelValidation] text-input input defaultColor" />
           
			 <div id="autosuggest" class="autosuggest">
                <ul>
                </ul>
                <iframe src="javascript:false;" frameborder="0" scrolling="no"></iframe>
            </div>
            <br class="clear" />
        </div>
        <div class="colm3 formColmChn" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnSearch.ClientID %>')">
            <label for="cal">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin1") %>
            </label>
            <input class="dateRange txtArrivalDate" name="cal" tabindex="203" type="text" id="txtArrivalDate"
                runat="server" />
            <br class="clear" />
        </div>
        <div class="colm1 formColmChn" id="noOfNights" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnSearch.ClientID %>')">
            <label for="nights" class="BBSmallLabel">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/noofdays") %>
            </label>
              <asp:TextBox ID="txtnoOfNights" MaxLength="2" runat="server" style="width:25px;"></asp:TextBox>
         <%--   <asp:DropDownList ID="ddlnoOfNights" runat="server">
            </asp:DropDownList>--%>
            <br class="clear" />
        </div>
        <br class="clear" />
    </div>
    <div class="calCont" id="disableFldsDep" runat="server" onkeypress="return WebForm_FireDefaultButton(event, btnSearch)">
        <div class="colm3 formColmChn">
            <label for="cal">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut1") %>
            </label>
            <input class="dateRange txtDepartureDate" name="cal" tabindex="204" type="text" id="txtDepartureDate"
                runat="server" />
            <br class="clear" />
        </div>
        <br class="clear" />
    </div>
    <br class="clear" />
    <asp:Panel ID="noOfRooms" runat="server" BorderStyle="none" DefaultButton="btnSearch">
        <p>
            <strong>
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/noofrooms1") %>
            </strong>            
        </p>
        <div class="colm1 formColmChn M05Bpad padBtm5">
            <asp:DropDownList ID="ddlNoOfRoomsReg" TabIndex="6" runat="server" CssClass="roomSelect"
                rel="#regularBking">
            </asp:DropDownList>
        </div>
    </asp:Panel>
    <!-- Copied from Booking module big -->
    <div class="roomOutput" id="regularBking" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnSearch.ClientID %>')">
        <div id="room1" runat="server" class="roundMe grayBox howManyRoom M05B" style="display: block;">
        <input type="hidden" runat="server" ID="IsRoomModifiable1" Value="" />
            <div class="colmMerg fltLft roomOutput">
                <div class="colm formColmChn fltLft">
                    <strong>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room1") %>
                    </strong>
                </div>
                <div class="colm1 widthLs fltLft">
                    <label for="adult">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                    </label>
                    <asp:DropDownList ID="ddlAdultsPerRoom1" TabIndex="7" runat="server" rel="adult">
                    </asp:DropDownList>
                    <label for="adult" style="width:58px;"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %> 13+</label>
                </div>
                <div class="colm1 widthMr fltRt">
                    <label for="child">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children") %>
                    </label>
                    <asp:DropDownList ID="ddlChildPerRoom1" TabIndex="8" runat="server" CssClass="child chafltRt"
                        rel="choutput1">
                    </asp:DropDownList>
                    <label for="child" style="width:50px;">0-12</label>
                </div>
                <div class="childSelct choutput1" runat="server" id="childrenBedTypesDropDownForRoom1">
                
                    <div class="fltLft roundMe grayBoxStay bord">
                        <div>
                            <strong style="float:left;">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                                </strong><span class="help spriteIcon toolTipMe ie9tip" title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"></span>
                        </div>
                        <div class="fltLft chdSelTtl">
                            <p class="fltLft">
                                <strong>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AgeandSelectBedType") %>
                                </strong>
                            </p>
                        </div>
                       <div class="selHdr1 customToolTipContainer">
                            <strong>
                                <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>--%>
                            </strong>
                            <div class="customToolTip">
                                <div class="ttTop"><span>&nbsp;</span></div>
                                <div class="ttMainContentWrapper">
                                    <div class="ttContentWrapper">
                                        <div class="ttContent">
                                            <%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalidbedtypechoosen") %>
                                        </div>
                                    </div>
                                </div>
                                <div class="ttBottom"><span>&nbsp;</span></div>
                                <span class="ttpointer">&nbsp;</span>
                            </div>                              
                        </div>
                        <div class="colm5 padTop" id="divForchildAgeforRoom1Child1" runat="server">
                            <label class="fltLft">
                                1)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom1Child1" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild114">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput11" id="divForbedTypeforRoom1Child1">
                                <asp:DropDownList ID="bedTypeforRoom1Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild114 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom1Child2" runat="server">
                            <label class="fltLft">
                                2)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom1Child2" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild124">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput12"  id="divForbedTypeforRoom1Child2" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom1Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild124 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom1Child3" runat="server">
                            <label class="fltLft">
                                3)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom1Child3" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild134">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput13" id="divForbedTypeforRoom1Child3" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom1Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild134 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom1Child4" runat="server">
                            <label class="fltLft">
                                4)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom1Child4" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild144">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput14" id="divForbedTypeforRoom1Child4" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom1Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild144 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom1Child5" runat="server">
                            <label class="fltLft">
                                5)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom1Child5" TabIndex="7" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild154">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput15" id="divForbedTypeforRoom1Child5" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom1Child5" TabIndex="7" runat="server" CssClass="selBedTyp subchild154 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="room2" runat="server" class="roundMe grayBox howManyRoom M05B">
        <input type="hidden" runat="server" ID="IsRoomModifiable2" Value="" />
            <div class="colmMerg fltLft roomOutput">
                <div class="colm formColmChn fltLft">
                    <strong>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room2") %>
                    </strong>
                </div>
                <div class="colm1 widthLs fltLft">
                    <label for="adult">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                    </label>
                    <asp:DropDownList ID="ddlAdultsPerRoom2" TabIndex="6" runat="server" rel="adult">
                    </asp:DropDownList>
                    <label for="adult" style="width:58px;"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %> 13+</label>
                </div>
                <div class="colm1 widthMr fltRt">
                    <label for="child">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children") %>
                    </label>
                    <asp:DropDownList ID="ddlChildPerRoom2" TabIndex="6" runat="server" CssClass="child chafltRt"
                        rel="choutput2">
                    </asp:DropDownList>
                    <label for="child" style="width:50px;">0-12</label>
                </div>
                <div class="childSelct choutput2" runat="server" id="childrenBedTypesDropDownForRoom2">
                    <div class="bord fltLft roundMe grayBoxStay">
                        <div>
                            <strong style="float:left;">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                            </strong>
                            <span class="help spriteIcon toolTipMe ie9tip" title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"></span>
                        </div>
                        <div class="fltLft chdSelTtl">
                            <p class="fltLft">
                                <strong>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AgeandSelectBedType") %>
                                </strong>
                            </p>
                        </div>
                       <div class="selHdr1 customToolTipContainer">
                            <strong>
                                <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>--%>
                            </strong>
                            <div class="customToolTip">
                                <div class="ttTop"><span>&nbsp;</span></div>
                                <div class="ttMainContentWrapper">
                                    <div class="ttContentWrapper">
                                        <div class="ttContent">
                                            <%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalidbedtypechoosen") %>
                                        </div>
                                    </div>
                                </div>
                                <div class="ttBottom"><span>&nbsp;</span></div>
                                <span class="ttpointer">&nbsp;</span>
                            </div>                              
                        </div>
                        <div class="colm5 padTop" id="divForchildAgeforRoom2Child1" runat="server">
                            <label class="fltLft">
                                1)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom2Child1" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild214">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput21" id="divForbedTypeforRoom2Child1" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom2Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild214 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom2Child2" runat="server">
                            <label class="fltLft">
                                2)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom2Child2" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild224">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput22" id="divForbedTypeforRoom2Child2" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom2Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild224 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom2Child3" runat="server">
                            <label class="fltLft">
                                3)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom2Child3" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild234">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput23" id="divForbedTypeforRoom2Child3" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom2Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild234 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom2Child4" runat="server">
                            <label class="fltLft">
                                4)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom2Child4" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild244">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput24" id="divForbedTypeforRoom2Child4" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom2Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild244 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom2Child5" runat="server">
                            <label class="fltLft">
                                5)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom2Child5" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild254">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput24" id="divForbedTypeforRoom2Child5" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom2Child5" TabIndex="6" runat="server" CssClass="selBedTyp subchild254 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="room3" runat="server" class="roundMe grayBox howManyRoom M05B">
        <input type="hidden" runat="server" ID="IsRoomModifiable3" Value="" />
            <div class="colmMerg fltLft roomOutput">
                <div class="colm formColmChn fltLft">
                    <strong>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room3") %>
                    </strong>
                </div>
                <div class="colm1 widthLs fltLft">
                    <label for="adult">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                    </label>
                    <asp:DropDownList ID="ddlAdultsPerRoom3" TabIndex="6" runat="server" rel="adult">
                    </asp:DropDownList>
                    <label for="adult" style="width:58px;"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %> 13+</label>
                </div>
                <div class="colm1 widthMr fltRt">
                    <label for="child">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenSmallTxt") %>
                    </label>
                    <asp:DropDownList ID="ddlChildPerRoom3" TabIndex="6" runat="server" CssClass="child chafltRt"
                        rel="choutput3">
                    </asp:DropDownList>
                    <label for="child" style="width:50px;">0-12</label>
                </div>
                <div class="childSelct choutput3" runat="server" id="childrenBedTypesDropDownForRoom3">
                    <div class="bord fltLft roundMe grayBoxStay">
                        <div>
                            <strong style="float:left;">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                            </strong><span class="help spriteIcon toolTipMe ie9tip" title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"></span>
                        </div>
                        <div class="fltLft chdSelTtl">
                            <p class="fltLft">
                                <strong>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AgeandSelectBedType") %>
                                </strong>
                            </p>
                        </div>
                       <div class="selHdr1 customToolTipContainer">
                            <strong>
                                <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>--%>
                            </strong>
                            <div class="customToolTip">
                                <div class="ttTop"><span>&nbsp;</span></div>
                                <div class="ttMainContentWrapper">
                                    <div class="ttContentWrapper">
                                        <div class="ttContent">
                                            <%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalidbedtypechoosen") %>
                                        </div>
                                    </div>
                                </div>
                                <div class="ttBottom"><span>&nbsp;</span></div>
                                <span class="ttpointer">&nbsp;</span>
                            </div>                              
                        </div>
                        <div class="colm5 padTop" id="divForchildAgeforRoom3Child1" runat="server">
                            <label class="fltLft">
                                1)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom3Child1" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild314">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput31" id="divForbedTypeforRoom3Child1" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom3Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild314 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom3Child2" runat="server">
                            <label class="fltLft">
                                2)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom3Child2" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild324">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput32" id="divForbedTypeforRoom3Child2" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom3Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild324 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom3Child3" runat="server">
                            <label class="fltLft">
                                3)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom3Child3" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild334">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput33" id="divForbedTypeforRoom3Child3" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom3Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild334 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom3Child4" runat="server">
                            <label class="fltLft">
                                4)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom3Child4" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild344">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput34" id="divForbedTypeforRoom3Child4" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom3Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild344 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom3Child5" runat="server">
                            <label class="fltLft">
                                5)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom3Child5" TabIndex="7" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild354">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput35" id="divForbedTypeforRoom3Child5" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom3Child5" TabIndex="7" runat="server" CssClass="selBedTyp subchild354 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="room4" runat="server" class="roundMe grayBox howManyRoom M05B">
        <input type="hidden" runat="server" ID="IsRoomModifiable4" Value="" />
            <div class="colmMerg fltLft roomOutput">
                <div class="colm formColmChn fltLft">
                    <strong>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room4") %>
                    </strong>
                </div>
                <div class="colm1 widthLs fltLft">
                    <label for="adult">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %>
                    </label>
                    <asp:DropDownList ID="ddlAdultsPerRoom4" TabIndex="6" runat="server" name="adult">
                    </asp:DropDownList>
                    <label for="adult" style="width:58px;"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %> 13+</label>
                </div>
                <div class="colm1 widthMr fltRt">
                    <label for="child">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenSmallTxt") %>
                    </label>
                    <asp:DropDownList ID="ddlChildPerRoom4" TabIndex="6" runat="server" CssClass="child chafltRt"
                        rel="choutput4">
                    </asp:DropDownList>
                    <label for="child" style="width:50px;">0-12</label>
                </div>
                <div class="childSelct choutput4" runat="server" id="childrenBedTypesDropDownForRoom4">
                    <div class="bord fltLft roundMe grayBoxStay">
                        <div>
                            <strong style="float:left;">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %>
                            </strong><span class="help spriteIcon toolTipMe ie9tip" title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>"></span>
                        </div>
                        <div class="fltLft chdSelTtl">
                            <p class="fltLft">
                                <strong>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AgeandSelectBedType") %>
                                </strong>
                            </p>
                        </div>
                       <div class="selHdr1 customToolTipContainer">
                            <strong>
                                <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%>--%>
                            </strong>
                            <div class="customToolTip">
                                <div class="ttTop"><span>&nbsp;</span></div>
                                <div class="ttMainContentWrapper">
                                    <div class="ttContentWrapper">
                                        <div class="ttContent">
                                            <%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalidbedtypechoosen") %>
                                        </div>
                                    </div>
                                </div>
                                <div class="ttBottom"><span>&nbsp;</span></div>
                                <span class="ttpointer">&nbsp;</span>
                            </div>                              
                        </div>
                        <div class="colm5 padTop" id="divForchildAgeforRoom4Child1" runat="server">
                            <label class="fltLft">
                                1)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom4Child1" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild414">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput41" id="divForbedTypeforRoom4Child1" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom4Child1" TabIndex="6" runat="server" CssClass="selBedTyp subchild414 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom4Child2" runat="server">
                            <label class="fltLft">
                                2)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom4Child2" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild424">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput42" id="divForbedTypeforRoom4Child2" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom4Child2" TabIndex="6" runat="server" CssClass="selBedTyp subchild424 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom4Child3" runat="server">
                            <label class="fltLft">
                                3)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom4Child3" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild434">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput43" id="divForbedTypeforRoom4Child3" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom4Child3" TabIndex="6" runat="server" CssClass="selBedTyp subchild434 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom4Child4" runat="server">
                            <label class="fltLft">
                                4)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom4Child4" TabIndex="6" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild444">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput44" id="divForbedTypeforRoom4Child4" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom4Child4" TabIndex="6" runat="server" CssClass="selBedTyp subchild444 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="colm5" id="divForchildAgeforRoom4Child5" runat="server">
                            <label class="fltLft">
                                5)
                            </label>
                            <asp:DropDownList ID="childAgeforRoom4Child5" TabIndex="7" runat="server" CssClass="validate[required] fltLft subchild"
                                rel="subchild454">
                            </asp:DropDownList>
                            <div class="widthMr fltLft bedoutput45" id="divForbedTypeforRoom4Child5" runat="server">
                                <asp:DropDownList ID="bedTypeforRoom4Child5" TabIndex="7" runat="server" CssClass="selBedTyp subchild454 validate[childInAdultBed]">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Earlier code-->
 
    <asp:Panel BorderStyle="none" ID="pnlBookingCode" runat="server" DefaultButton="btnSearch">
        <p class="mrgBtm10">
            <span class="fltLft">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterBookingCode1") %>
            </span>
            <span title=" <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BCodeOnShopingCardToolTip") %>"
                            class="help spriteIcon toolTipMe ie9tip" id="shopcartedtbkcodehelp"></span>
        </p>
        <div class="bookingCode">
            <input type="text" name="txtRegularCode" runat="server" id="txtRegularCode" value=""
                class="roundMe input defaultColor" />
        </div>
    </asp:Panel>
    
    <div id="addRoomBlock" runat="server" class="addRoomBlock" visible="false">
        <div class="addRoomLink" style="display: block;">
            <a class="addRoom" rel="addRoom" href="#"><%= WebUtil.GetTranslatedText("/bookingengine/booking/ModifyStay/AddAnotherRoom") %></a>
        </div>
    </div>
    <div id="yourStayAlertBox">
            <div class="threeCol alertContainer">
                <div class="hd sprite">
                    &nbsp;</div>
                <div class="cnt">
                    <p>
                        <span class="spriteIcon"></span><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/YourStayAlertBox") %>
                    </p>
                </div>
                <div class="ft sprite">
                </div>
            </div>
        </div>
    <%-- <div class="chkBox">
        <input type="checkbox" name="disFrndRoom" id="rememberMe" value="" runat="server" />
        <label for="disFrndRoom">
            <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RememberBookingCode")%>--
        </label>
    </div>--%>
    <br class="clear" />
    <div class="ftButtons editShopingCartBtn">
    <div class="HR mrgBtm10"></div>    
        <asp:HyperLink rel="gobackDet" href="#" ID="gobackAnchor" runat="server" class="goBack" >
            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Cancel") %>
        </asp:HyperLink>
        <div class="actionBtn fltRt" id="SubmitButtonDiv">
            <asp:LinkButton ID="btnSearch" TabIndex="160" runat="server" CssClass="submit buttonInner" 
                OnClick="btnSearch_Click"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Continue") %></asp:LinkButton>
            <asp:LinkButton ID="spnSearch" TabIndex="160" runat="server" CssClass="buttonRt scansprite" OnClick="btnSearch_Click"></asp:LinkButton>            
        </div>
        <div class="clear">
        </div>
    </div>
    <br class="clear" />
    <!--Destination Search use for createing Hotel List-->
    <div id="destinationSearch">
        <uc1:DestinationSearch id="DestinationSearch1" runat="server" />
    </div>
    <!--Add calander---->
    <div id="emptyDiv">
    </div>
    <div id="hiddenCalendar" class="calendarContainer">
        <table width="186" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td align="left" valign="top">
                    <div id="calendarLayer">
                        &nbsp;</div>
                    <div id="calendarBase">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!--create Calander-->
    <div id="calendar">
        <uc2:Calendar id="Calendar1" runat="server" />
    </div>
</div>
    </div>
</div>

<script type="text/javascript" language="javascript">
	$(document).ready(function() {   

            $("select[id*='childAgeforRoom']").bind("change", function() { 
            if (this.selectedIndex == 0) 
                $(this).next().children(":first").hide(); 
            else 
                $(this).next().children(":first").show(); 
            }); 
		
	$("select[id*='childAgeforRoom']").each(function() { 
            if (this.selectedIndex == 0) 
                $(this).next().children(":first").hide(); 
            else 
                $(this).next().children(":first").show(); 
            }); 
        });
    var requestUrl = <%= "\"" + GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") + "\"" %>;    
    initBMS();
    ArrDate_Id = _endsWith("txtArrivalDate");
    DepDate_Id = _endsWith("txtDepartureDate");
    Nights_Id = _endsWith("txtnoOfNights");
    //Vrushali | fix 3372992 | Not able to modify a combo reservation with children bed type 'Extra bed'. | P2
    isModifyFlowComboReservation = "<%= HygieneSessionWrapper.IsComboReservation %>";
   
    //Vrushali- This call is required to retain the child selection in shopping cart. Please don't remove.
    scandic.agSelct();

    //Vrushali | Workaround suggesed by Elin for artf1194231: Wrong bedtype registrered in OPERA
    //artf1193637: Scanweb - Possible exceeding MAX OCC when booking with children
    // Hiding the Sections during Bed Type Error
    var errorInSettingBedType = "<%= errorInBedTypeSelectionInEditStay %>";
    $(function(){
        if(errorInSettingBedType == "True"){
            var chDet = $("#yourStayMod05 div.regular div.changedDetail");
	        var moBrd = $("#yourStayMod05 div.regular div.broadBrd");
	        chDet.hide();
	        moBrd.hide();
	       
	       // Hiding the Booking Data and Room Details
	       $('.tripleCol').attr("style", "min-height: 100px;");
	       $("#CalendarData").hide();
	       $("#stepIndecator").hide();
	       $(".tripleCol .BE").hide();
	       $(".goBack").hide();	      
	 
	    }
	});


$(function(){
   if(errorInSettingBedType == "True"){   
       // Get the No of Rooms
       var noOfRoom = $('#yourStayMod05 .roomSelect').find(":selected").val();
       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlChildPerRoom1")).id).find(":selected").val();
       var noOfChild2 = $("#"+ $fn(_endsWith("ddlChildPerRoom2")).id).find(":selected").val();
       var noOfChild3 = $("#"+ $fn(_endsWith("ddlChildPerRoom3")).id).find(":selected").val();
       var noOfChild4 = $("#"+ $fn(_endsWith("ddlChildPerRoom4")).id).find(":selected").val();         
       selectBedTypeError(noOfRoom, noOfChild1, noOfChild2, noOfChild3, noOfChild4, "choutput", "Age");
    }
    
  
});

var errorPriorDate = "<%=errorPriorDate%>";
    $(function(){
        if(errorPriorDate == "True"){
            var chDet = $("#yourStayMod05 div.regular div.changedDetail");
	        var moBrd = $("#yourStayMod05 div.regular div.broadBrd");
	        chDet.hide();
	        moBrd.hide();    
	 
	    }
	});
    
$(function(){
   if(errorPriorDate == "True"){   
       // Get the No of Rooms
       var noOfRoom = $('#yourStayMod05 .roomSelect').find(":selected").val();
       // Get the No of Childrens for each Room
       var noOfChild1 = $("#"+ $fn(_endsWith("ddlChildPerRoom1")).id).find(":selected").val();
       var noOfChild2 = $("#"+ $fn(_endsWith("ddlChildPerRoom2")).id).find(":selected").val();
       var noOfChild3 = $("#"+ $fn(_endsWith("ddlChildPerRoom3")).id).find(":selected").val();
       var noOfChild4 = $("#"+ $fn(_endsWith("ddlChildPerRoom4")).id).find(":selected").val();         
       setChildrenDetails(noOfRoom, noOfChild1, noOfChild2, noOfChild3, noOfChild4, "choutput", "Age");
    }
});
</script>

