//  Description					:   BookingModuleBig                                      //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using System.Linq;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code behind of BookingModuleBig control.
    /// </summary>
    public partial class BookingModuleBig : EPiServer.UserControlBase, INavigationTraking
    {

        #region Private Members
        private AvailabilityController availabilityController = null;
        private bool isDeeplinkViewModify = false;
        /// <summary>
        /// Gets m_AvailabilityController
        /// </summary>
        private AvailabilityController m_AvailabilityController
        {
            get
            {
                if (availabilityController == null)
                {
                    availabilityController = new AvailabilityController();
                }
                return availabilityController;
            }
        }
        #endregion

        public bool errorInBedTypeSelectionInRGB = false;
        public bool errorInBedTypeSelectionInBCB = false;
        public bool errorInBedTypeSelectionInRNB = false;
        List<string> allowedControls = new List<string> { "btnSearch", "spnSearch", "btnBonusChequeSearch", "spnBonusChequeSearch", "btnRewardNightSearch", "spnRewardNightSearch" };
        public bool errorPriorDateInRGB = false;
        public bool errorPriorDateInBCB = false;
        public bool errorPriorDateInRNB = false;

        #region Protected Methods

        protected override void OnInit(EventArgs e)
        {
            SetDefaultTextToTextBoxes();
            SetDropDowns();
            SetFields();

            Reservation2SessionWrapper.IsCurrentBookingsPage = false;

            if (BookingEngineSessionWrapper.BookingCodeIATA != null && BookingEngineSessionWrapper.BookingCodeIATA != string.Empty)
                txtRegularCode.Value = BookingEngineSessionWrapper.BookingCodeIATA;
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            Reservation2SessionWrapper.CurrentLanguage = CurrentPage.LanguageID;
            ControlCollection controls = Controls;
            Reservation2SessionWrapper.IsModifyFlow = false;
            txtPassword.Attributes.Add("fieldName", "EnrollPassword");
            if (Request.Form != null && Request.Form.AllKeys != null && Request.Form.AllKeys.Count() > 0
                && !string.IsNullOrEmpty(Request.Form["viewModifyDeeplinkReservationID"])
                && !string.IsNullOrEmpty(Request.Form["viewModifyDeeplinkLastName"]))
            {
                isDeeplinkViewModify = true;
                btnFetchBooking_Click(null, null);
                return;
            }

            if (this.Visible)
            {
                string strHotelOverviewURL = GlobalUtil.GetUrlToPage("HotelOverviewPage");
                //if (searchOnMap != null) searchOnMap.HRef = strHotelOverviewURL + "?initialview=map";
                //if (searchOnMapRewardNight != null) searchOnMapRewardNight.HRef = strHotelOverviewURL + "?initialview=map";
                //if (searchOnMapBNC != null) searchOnMapBNC.HRef = strHotelOverviewURL + "?initialview=map";

                string mode = Request.QueryString[QueryStringConstants.QUERY_STRING_MODE];

                if (string.Equals(Request.QueryString[QueryStringConstants.QUERY_STRING_MODE], "rn", StringComparison.InvariantCultureIgnoreCase))
                {
                    sT.Value = BookingTab.Tab3;
                }

                if (UserLoggedInSessionWrapper.UserLoggedIn && Reservation2SessionWrapper.IsCurrentBookingsPage &&
                    (!string.Equals(Request.QueryString[QueryStringConstants.QUERY_STRING_MODE], "rn", StringComparison.InvariantCultureIgnoreCase)))
                    sT.Value = BookingTab.Tab1;

                if (UserLoggedInSessionWrapper.UserLoggedIn && string.Equals(Convert.ToString(CurrentPage[AppConstants.PAGE_SEGMENT_KEY]), AppConstants.REWARD_NIGHT_SEGMENT, StringComparison.InvariantCultureIgnoreCase))
                {
                    sT.Value = BookingTab.Tab3;
                }
                if (string.Equals(Convert.ToString(CurrentPage[AppConstants.PAGE_SEGMENT_KEY]), AppConstants.USE_POINTS_SEGMENT, StringComparison.InvariantCultureIgnoreCase))
                {
                    sT.Value = BookingTab.Tab3;
                }
                if (!string.Equals(CurrentPage.URLSegment, "Confirmation", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (Request.QueryString["command"] != null)
                    {
                        if ((Request.QueryString["command"].ToString() != QueryStringConstants.QUERY_STRING_CANCEL_PRINT)
                            || (Request.QueryString["command"].ToString() != QueryStringConstants.QUERY_STRING_PRINT))
                        {
                            GuestBookingInformationSessionWrapper.AllGuestsBookingInformations = null;
                        }
                    }
                }
                PageData pageData = GetPage(PageReference.RootPage);
                int homePageLinkID = ((PageReference)pageData[EpiServerPageConstants.HOME_PAGE]).ID;
                int frequentGuestProgrammeID =
                    ((PageReference)pageData[EpiServerPageConstants.FREQUENT_GUEST_PROGRAMME]).ID;
                int offersListPageID = ((PageReference)pageData[EpiServerPageConstants.OFFERS_LIST_PAGE]).ID;
                int offersOverviewPageID = ((PageReference)pageData[EpiServerPageConstants.OFFERS_OVERVIEW]).ID;
                int fgpLogOutPageID = ((PageReference)pageData[EpiServerPageConstants.LOGOUT_CONFIRMATION]).ID;
                if (CurrentPage.PageLink.ID == homePageLinkID || CurrentPage.PageLink.ID == frequentGuestProgrammeID
                    || CurrentPage.PageLink.ID == offersListPageID || CurrentPage.PageLink.ID == offersOverviewPageID
                    || CurrentPage.PageLink.ID == fgpLogOutPageID ||
                    CurrentPage.ParentLink.ID == frequentGuestProgrammeID)
                {
                    GuestBookingInformationSessionWrapper.AllGuestsBookingInformations = null;
                }

                SetLoginStatus();
                LoginStatus ctrl = (LoginStatus)this.Page.Master.FindControl("MenuLoginStatus");
                if (ctrl != null)
                {
                    ctrl.UpdateLoginStatus(false);
                }
                if (!Page.IsPostBack)
                {
                    if (UserLoggedInSessionWrapper.UserLoggedIn)
                    {
                        this.bookingPlaceHolder.Visible = true;
                    }
                    else
                    {
                        RememberUserIdPwd.Checked = true;
                        if (WebUtil.IsCookieExists(AppConstants.DONTREMEMBERME_COOKIE))
                            RememberUserIdPwd.Checked = false;
                        else
                            WebUtil.SetCookieToTextBox(txtUserNamePrefix, txtUserName, txtPassword, RememberUserIdPwd);
                        this.rewardNightPlaceHolder.Visible = true;
                        btnLogin.PostBackUrl = WebUtil.LoginValidatorUrl();
                        spnLogin.PostBackUrl = WebUtil.LoginValidatorUrl();

                        Utility.SetLoginDemographics(txtUserNamePrefix.Name, txtUserName.Name, txtPassword.UniqueID, RememberUserIdPwd.Name,
                                                     LoginSourceModule.BOOKING_SEARCH_MODULE, loginSrchPageID.Name);
                        btnLogin.Attributes.Add("onclick", "javascript:ValueToTab();return performValidation(PAGE_BOOKING_MODULE_BIG);");
                        spnLogin.Attributes.Add("onclick", "javascript:ValueToTab();return performValidation(PAGE_BOOKING_MODULE_BIG);");
                        btnLogin.Text = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/login");
                        txtPassword.Text = WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/Password");
                        txtPassword.Attributes["rel"] = WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/Password");
                        if (LoginErrorCodeSessionWrapper.LoginPopupErrorCode == 7)
                        {
                            sT.Value = BookingTab.Tab3;
                            Utility.ShowErrorMessageForAccordian(RNClientErrorDiv, WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/LoginErrorMessage"));
                            LoginErrorCodeSessionWrapper.LoginPopupErrorCode = 0;
                        }
                    }

                    loginSrchID.Value = LoginSourceModule.BOOKING_SEARCH_MODULE.ToString();

                    btnSearch.Attributes.Add("onclick", "javascript:return performValidation(PAGE_BOOKING_MODULE_BIG);");
                    spnSearch.Attributes.Add("onclick", "javascript:return performValidation(PAGE_BOOKING_MODULE_BIG);");
                    btnSearch.Text = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Search");

                    btnSearch.CommandArgument = BookingTab.Tab1;
                    spnSearch.CommandArgument = BookingTab.Tab1;

                    btnBonusChequeSearch.Attributes.Add("onclick",
                                                        "javascript:return performValidation(PAGE_BOOKING_MODULE_BIG);");
                    spnBonusChequeSearch.Attributes.Add("onclick",
                                                        "javascript:return performValidation(PAGE_BOOKING_MODULE_BIG);");
                    btnBonusChequeSearch.Text = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Search");

                    btnBonusChequeSearch.CommandArgument = BookingTab.Tab2;
                    spnBonusChequeSearch.CommandArgument = BookingTab.Tab2;

                    btnRewardNightSearch.Attributes.Add("onclick",
                                                        "javascript:ValueToTab();return performValidation(PAGE_BOOKING_MODULE_BIG);");
                    spnRewardNightSearch.Attributes.Add("onclick",
                                                        "javascript:ValueToTab();return performValidation(PAGE_BOOKING_MODULE_BIG);");
                    btnRewardNightSearch.Text = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Search");

                    btnRewardNightSearch.CommandArgument = BookingTab.Tab3;
                    spnRewardNightSearch.CommandArgument = BookingTab.Tab3;

                    childrenCriteriaHidden.Value = WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/childcriteria");

                    btnFetchBooking.CommandArgument = BookingTab.Tab4;
                    btnFetchBooking.Attributes.Add("onclick", "javascript:return performValidation(PAGE_BOOKING_SEARCH);");
                    spnFetchBooking.CommandArgument = BookingTab.Tab4;
                    spnFetchBooking.Attributes.Add("onclick", "javascript:return performValidation(PAGE_BOOKING_SEARCH);");
                    btnFetchBooking.Text = WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/Search");

                    SetTabIndexControl();
                    btnSearch.Text = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Search");

                    if(string.Equals(Request.QueryString["ErrCode"], "D1", StringComparison.InvariantCultureIgnoreCase))
                        Utility.ShowErrorMessageForAccordian(RegClientErrorDiv, WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/UnauthorizedAccessToFamilyAndFriendsDNumber"));
                }
            }
        }

        /// <summary>
        /// Set the tabIndex to the controls
        /// </summary>
        private void SetTabIndexControl()
        {
            Int16 tabIndex = 14;
            for (int ctrlCount = 0; ctrlCount < this.Controls.Count; ctrlCount++)
            {
                if (this.Controls[ctrlCount].Controls.Count > 0)
                {
                    Utility.FetchInnerControlSetTabIndex(this.Controls[ctrlCount], ref tabIndex);
                }
                else
                {
                    Utility.SetTabIndex(this.Controls[ctrlCount], ref tabIndex);
                }
            }
        }

        /// <summary>
        /// Handled during click of search button in regular search and bonus check booking.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            WebUtil.ValidateCampaignCodeForFamilyAndFriendsDNumber(txtRegularCode.Value);
            
            try
            {
                LinkButton sndrBtn = sender as LinkButton;
                bool isRegularBookingdatesValid = Core.DateUtil.StringToDDMMYYYDate(txtArrivalDate.Value) < Core.DateUtil.StringToDDMMYYYDate(txtDepartureDate.Value);
                bool isBonuscheckBookingdatesValid = Core.DateUtil.StringToDDMMYYYDate(txtBCArrivalDate.Value) < Core.DateUtil.StringToDDMMYYYDate(txtBCDepartureDate.Value);
                bool isRewardnightsBookingdatesValid = Core.DateUtil.StringToDDMMYYYDate(txtRNArrivalDate.Value) < Core.DateUtil.StringToDDMMYYYDate(txtRNDepartureDate.Value);
                if (
                       (isRegularBookingdatesValid && (sndrBtn != null && sndrBtn.CommandArgument == BookingTab.Tab1) && WebUtil.IsValidDates(txtArrivalDate.Value, txtDepartureDate.Value)) ||
                       (isBonuscheckBookingdatesValid && (sndrBtn != null && sndrBtn.CommandArgument == BookingTab.Tab2) && WebUtil.IsValidDates(txtBCArrivalDate.Value, txtBCDepartureDate.Value)) ||
                       (isRewardnightsBookingdatesValid && (sndrBtn != null && sndrBtn.CommandArgument == BookingTab.Tab3) && WebUtil.IsValidDates(txtRNArrivalDate.Value, txtRNDepartureDate.Value))
                   )
                {
                    Reservation2SessionWrapper.BookingModuleActiveTab = false;

                    CleanSessionSearchResultsSessionWrapper.CleanSessionSearchResults();

                    CleanSessionSearchResultsSessionWrapper.ClearComboReservationBooleans();
                    Reservation2SessionWrapper.AvailabilityCalendarAccessed = false;
                    GenericSessionVariableSessionWrapper.IsPrepaidBooking = false;
                    HygieneSessionWrapper.IsPANHashCreditCardsAvailable = false;
                    BookingEngineSessionWrapper.IsModifyComboBooking = false;
                    Reservation2SessionWrapper.PaymentGuestInformation = null;
                    Reservation2SessionWrapper.PaymentHotelRoomRateInformation = null;
                    Reservation2SessionWrapper.PaymentIsSessionBooking = false;
                    Reservation2SessionWrapper.PaymentOrderAmount = null;
                    HygieneSessionWrapper.IsEarlyBookingUsingStoredPanHashCC = false;
                    Reservation2SessionWrapper.PaymentTransactionId = null;
                    GenericSessionVariableSessionWrapper.IsPrepaidBooking = false;
                    SearchCriteriaSessionWrapper.IsPromocodeInvalidForHotel = false;
                    BookingEngineSessionWrapper.IATAProfileCode = string.Empty;
                    BookingEngineSessionWrapper.BookingCodeIATA = string.Empty;
                    WebUtil.ClearBlockedRooms();

                    bool errorSettingChildBedType = false;
                    string currentBookingTab = string.Empty;
                    List<ChildrensDetailsEntity> childrenDetailsallRooms = null;
                    HotelSearchEntity hotelSearchEntity = null;
                    LinkButton senderButton = sender as LinkButton;
                    if (null != senderButton)
                    {
                        switch (senderButton.CommandArgument)
                        {
                            case BookingTab.Tab1:
                                {
                                    //Merchandising:R3:Display ordinary rates for unavailable promo rates 
                                    HotelResultsSessionWrapper.HotelDetails = null;
                                    AlternateHotelsSessionWrapper.IsHotelAlternateFlow = false;
                                    AlternateCityHotelsSearchSessionWrapper.TotalRegionalAltCityHotelCount = -1;
                                    AlternateCityHotelsSearchSessionWrapper.CityAltHotelDetails = null;
                                    AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone = false;
                                    TotalGeneralAvailableHotelsSessionWrapper.PromoGeneralAvailableHotels = 0;

                                    hotelSearchEntity = GetHotelSearchEntity(BookingTab.Tab1);
                                    childrenDetailsallRooms = CreateChildrenDetailsforAllRooms("bedTypeforRoom", "childAgeforRoom",
                                        "ddlNoOfRoomsReg", "ddlChildPerRoom", "ddlAdultsPerRoom", hotelSearchEntity, ref errorSettingChildBedType);
                                    CheckChidrenBedTypesSetProperly(ref errorSettingChildBedType, hotelSearchEntity);
                                    currentBookingTab = BookingTab.Tab1;
                                }
                                break;
                            case BookingTab.Tab2:
                                {
                                    hotelSearchEntity = GetHotelSearchEntity(BookingTab.Tab2);
                                    childrenDetailsallRooms = CreateChildrenDetailsforAllRooms("ddlBCBedTypeforRoom", "ddlBCChildAgeforRoom",
                                        "ddlNoOfRoomsBC", "ddlBCChildPerRoom", "ddlBCAdultsPerRoom", hotelSearchEntity, ref errorSettingChildBedType);
                                    CheckChidrenBedTypesSetProperly(ref errorSettingChildBedType, hotelSearchEntity);
                                    currentBookingTab = BookingTab.Tab2;
                                }
                                break;
                            case BookingTab.Tab3:
                                {
                                    hotelSearchEntity = GetHotelSearchEntity(BookingTab.Tab3);
                                    childrenDetailsallRooms = CreateChildrenDetailsforAllRooms("ddlRNBedTypeforRoom", "ddlRNChildAgeforRoom",
                                        "ddlNoOfRoomsRed", "ddlRNChildPerRoom", "ddlRNAdultsPerRoom", hotelSearchEntity, ref errorSettingChildBedType);
                                    CheckChidrenBedTypesSetProperly(ref errorSettingChildBedType, hotelSearchEntity);
                                    currentBookingTab = BookingTab.Tab3;
                                }
                                break;
                        }
                    }

                    SearchCriteriaSessionWrapper.SearchCriteria = hotelSearchEntity;
                    UserNavTracker.ClearTrackedData();
                    UserNavTracker.TrackAction(this, "Search");
                    //WebUtil.ApplicationErrorLog(new Exception("Error in search"));
                    //Response.End();
                    if (hotelSearchEntity != null && hotelSearchEntity.SearchedFor != null)
                        SearchDestinationSessionWrapper.SearchDestination = hotelSearchEntity.SearchedFor.SearchString;
                    SearchDestinationSessionWrapper.SearchDestinationUpdated = false;

                    if (childrenDetailsallRooms != null && childrenDetailsallRooms.Count > 0)
                    {
                        hotelSearchEntity.ChildrenOccupancyPerRoom =
                            Utility.GetNoOfChildrenToBeAccommodated(childrenDetailsallRooms[0]);
                    }

                    if (!errorSettingChildBedType)
                    {
                        string urlToRedirect = Utility.RedirectBookingFlow(BookingEnginePosition.MAIN_SEARCH_PAGE);
                        if (!string.IsNullOrEmpty(urlToRedirect))
                        {
                            Response.Redirect(urlToRedirect, false);
                        }
                        else
                        {
                            Utility.ShowErrorMessage(clientErrorDiv,
                                                     WebUtil.GetTranslatedText(
                                                         "/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name"));
                        }
                    }
                    else
                    {
                        string errorMessage = string.Empty;
                        switch (currentBookingTab)
                        {
                            case BookingTab.Tab1:
                                {
                                   
                                    errorMessage = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidAge") +
                                                   WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidBedType");
                                    Utility.ShowErrorMessageForAccordian(RegClientErrorDiv, errorMessage);
                                    if (hotelSearchEntity != null)
                                        HideChildernDetailsWhenBedTypeNotSet(hotelSearchEntity, BookingTab.Tab1);
                                    errorInBedTypeSelectionInRGB = true;
                                }
                                break;
                            case BookingTab.Tab2:
                                {
                                    errorMessage = WebUtil.GetTranslatedText(
                                        "/scanweb/bookingengine/errormessages/requiredfielderror/invalidAge")
                                                   +
                                                   WebUtil.GetTranslatedText(
                                                       "/scanweb/bookingengine/errormessages/requiredfielderror/invalidBedType");
                                    Utility.ShowErrorMessageForAccordian(BCClientErrorDiv, errorMessage);
                                    if (hotelSearchEntity != null)
                                        HideChildernDetailsWhenBedTypeNotSet(hotelSearchEntity, BookingTab.Tab2);
                                    errorInBedTypeSelectionInBCB = true;
                                }
                                break;
                            case BookingTab.Tab3:
                                {
                                    errorMessage = WebUtil.GetTranslatedText(
                                        "/scanweb/bookingengine/errormessages/requiredfielderror/invalidAge")
                                                   +
                                                   WebUtil.GetTranslatedText(
                                                       "/scanweb/bookingengine/errormessages/requiredfielderror/invalidBedType");
                                    Utility.ShowErrorMessageForAccordian(RNClientErrorDiv, errorMessage);
                                    if (hotelSearchEntity != null)
                                        HideChildernDetailsWhenBedTypeNotSet(hotelSearchEntity, BookingTab.Tab3);
                                    errorInBedTypeSelectionInRNB = true;
                                }
                                break;
                        }
                    }
                }
                else
                {
                    string errorMsg = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidDate");
                    if (!isRegularBookingdatesValid || !isBonuscheckBookingdatesValid || !isRewardnightsBookingdatesValid)
                        errorMsg = WebUtil.GetTranslatedText(TranslatedTextConstansts.END_DATE_INVALID);
                    if (sndrBtn != null)
                    {
                        if (sndrBtn.CommandArgument == BookingTab.Tab1)
                        {
                            sT.Value = BookingTab.Tab1;
                            Utility.ShowErrorMessageForAccordian(RegClientErrorDiv, errorMsg);
                            errorPriorDateInRGB = true;
                        }
                        else if (sndrBtn.CommandArgument == BookingTab.Tab2)
                        {
                            sT.Value = BookingTab.Tab2;
                            Utility.ShowErrorMessageForAccordian(BCClientErrorDiv, errorMsg);
                            errorPriorDateInBCB = true;
                        }
                        else if (sndrBtn.CommandArgument == BookingTab.Tab3)
                        {
                            sT.Value = BookingTab.Tab3;
                            Utility.ShowErrorMessageForAccordian(RNClientErrorDiv, errorMsg);
                            errorPriorDateInRNB = true;
                        }
                    }
                }
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }

        }

        /// <summary>
        /// Called when we fetch the reserved booking.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnFetchBooking_Click(object sender, EventArgs e)
        {
            try
            {
                bool isSessionBooking = false;
                HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
                Reservation2SessionWrapper.BookingModuleActiveTab = false;
                Utility.CleanSession();
                UserNavTracker.TrackAction(this, "FetchBooking");
                //WebUtil.ApplicationErrorLog(new Exception("Fetch Bookings"));
                //Response.End();
                Reservation2SessionWrapper.AvailabilityCalendarAccessed = false;
                BookingEngineSessionWrapper.IsModifyBooking = true;

                Reservation2SessionWrapper.IsModifyFlow = true;

                string bookingNumber = string.Empty;
                string surName = string.Empty;
                if (isDeeplinkViewModify)
                {
                    bookingNumber = Request.Form["viewModifyDeeplinkReservationID"];
                    surName = Request.Form["viewModifyDeeplinkLastName"];
                    txtBookingNumber.Value = bookingNumber;
                    txtSurname.Value = surName;
                }
                else
                {
                    bookingNumber = txtBookingNumber.Value.Trim();
                    surName = txtSurname.Value.Trim();
                }
                sT.Value = BookingTab.Tab4;

                ReservationController reservationController = new ReservationController();
                List<BookingDetailsEntity> bookingEntities = null;
                if (IsMasterConfirmationNumber(bookingNumber))
                {
                    bookingEntities = reservationController.FetchBookingSummary(bookingNumber);
                    BookingEngineSessionWrapper.IsModifyingLegBooking = false;
                }
                else
                {
                    string legNumber = string.Empty;
                    bookingNumber = SplitBookingAndLegNumber(bookingNumber, out legNumber);
                    BookingDetailsEntity bookingEntity = reservationController.FetchBooking(bookingNumber, legNumber);
                    if (bookingEntity != null)
                    {
                        bookingEntities = new List<BookingDetailsEntity>();
                        bookingEntities.Add(bookingEntity);
                        BookingEngineSessionWrapper.IsModifyingLegBooking = true;
                    }
                }
                foreach (BookingDetailsEntity bookingEntity in bookingEntities)
                {
                    if (bookingEntity.HotelRoomRate != null && bookingEntity.HotelRoomRate.IsSessionBooking == true)
                    {
                        isSessionBooking = true;
                        break;
                    }
                }
                if (bookingEntities != null && bookingEntities.Count > 0)
                {
                    int cancelledBookingsCount = 0;
                    for (int i = 0; i < bookingEntities.Count; i++)
                    {
                        BookingDetailsEntity currentBookingDetailsEntity = bookingEntities[i];
                        if (currentBookingDetailsEntity.CancelDetails != null &&
                            currentBookingDetailsEntity.CancelDetails.CancelDateSpecified)
                        {
                            cancelledBookingsCount = cancelledBookingsCount + 1;
                        }
                    }

                    string translatedDefaultSurName =
                        WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/Surname");
                    if ((string.IsNullOrEmpty(surName)) ||
                        (surName.ToUpper().Equals(translatedDefaultSurName.ToUpper())))
                    {
                        clientErrorDivBD.InnerHtml =
                            WebUtil.GetTranslatedText(
                                "/scanweb/bookingengine/errormessages/requiredfielderror/invalidSurname");
                        invalidReservationNumber.Value = "true";
                    }
                    else if (!CheckName(bookingEntities, surName))
                    {
                        clientErrorDivBD.InnerHtml =
                            WebUtil.GetTranslatedText(TranslatedTextConstansts.INVALID_SURNAME);
                        invalidReservationNumber.Value = "true";
                    }
                    else if (isSessionBooking)
                    {
                        clientErrorDivBD.InnerHtml = WebUtil.GetTranslatedText(TranslatedTextConstansts.INVALID_BOOKING_NUMBER);
                        invalidReservationNumber.Value = "true";
                    }
                    else
                    {
                        BookingEngineSessionWrapper.AllBookingDetails = bookingEntities;
                        BookingEngineSessionWrapper.BookingDetails = bookingEntities[0];
                        ReservationNumberSessionWrapper.ReservationNumber = bookingNumber;
                        if (Utility.IsBlockCodeBooking)
                        {
                            Utility.OverrideCancelableDateIfBlockCode(bookingEntities);
                            if (null != bookingEntities[0].HotelSearch)
                            {
                                BookingEngineSessionWrapper.HideARBPrice = Utility.GetHideARBPrice(bookingEntities[0].HotelSearch.CampaignCode);
                            }
                        }
                        Utility.SetBookingEntityIsModifiableAndIsComboReservation();

                        BookingEngineSessionWrapper.PreviousPageURL = GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE);
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_CHANGE_DATES),
                                          false);
                    }
                }
                else
                {
                    Scanweb.Core.AppLogger.LogInfoMessage(
                        string.Format(
                            "No Booking Details information is found against the specified {0} booking confirmation number in the method - btnFetchBooking_Click",
                            bookingNumber));
                    WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                                 WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                }
                if (bookingEntities != null)
                {
                    BookingEngineSessionWrapper.IsModifyComboBooking =
                        WebUtil.IsAnyRoomHavingSaveCategory(bookingEntities);
                }
            }
            catch (OWSException owsException)
            {
                if (!string.Equals(owsException.ErrMessage, "BOOKING NOT FOUND", StringComparison.InvariantCultureIgnoreCase))
                    Scanweb.Core.AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);
                switch (owsException.ErrCode)
                {
                    case OWSExceptionConstants.BOOKING_NOT_FOUND:
                    case OWSExceptionConstants.SEE_CONTACT_DETAILS_ON_CHAIN_INFO:
                        {
                            clientErrorDivBD.InnerHtml =
                                WebUtil.GetTranslatedText(TranslatedTextConstansts.INVALID_BOOKING_NUMBER);
                            invalidReservationNumber.Value = "true";
                            break;
                        }
                    default:
                        {
                            clientErrorDivBD.InnerHtml = owsException.Message;
                            break;
                        }
                }
                
                if (owsException.Message == AppConstants.SYSTEM_ERROR)
                {
                    WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                                 WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                }
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }
        }

        /// <summary>
        /// This will redirect to forgotten password page where user is able to retrieve forgotten password.
        /// </summary>
        /// <param name="sender">Sender name</param>
        /// <param name="e">Event Arguments</param>
        protected void ForgottenPassword_Click(object sender, EventArgs e)
        {
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.FORGOTTEN_PASSWORD), false);
        }

        #endregion Protected Methods

        /// <summary>
        /// This method will assign the default text to the text boxes.
        /// </summary>
        private void SetDefaultTextToTextBoxes()
        {
            txtHotelName.Value = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName");
            txtHotelNameBNC.Value = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName");
            txtHotelNameRewardNights.Value =
                WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName");
            ;
            txtBookingNumber.Value = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ReservationNumber");
            txtSurname.Value = WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/Surname");
            txtUserName.Value = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/username");
            txtPassword.Text = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/password");
            txtRegularCode.Value = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingCode");
            txtRegularCode.Attributes["rel"] =
                WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingCode");
            ;
            txtHotelName.Attributes["rel"] =
                WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName");
            txtHotelNameBNC.Attributes["rel"] =
                WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName");
            txtHotelNameRewardNights.Attributes["rel"] =
                WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName");
            txtUserName.Attributes["rel"] = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/username");
            txtBookingNumber.Attributes["rel"] =
                WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ReservationNumber");
            txtSurname.Attributes["rel"] = WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/Surname");

        }

        private bool m_IsBookingModuleReset;

        /// <summary>
        /// Flag to indicate if Booking module is to be displayed with Default View
        /// </summary>
        public bool IsBookingModuleReset
        {
            set { m_IsBookingModuleReset = value; }
        }

        /// <summary>
        /// Creates children details entity
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="listHotelSearchRoomEntity">The list hotel search room entity.</param>
        /// <param name="bedTypes">The bed types.</param>
        /// <param name="childrenDetailsallRooms">The children detailsall rooms.</param>
        /// <param name="childPerRoomControlName">Name of the child per room control.</param>
        /// <param name="adultPerRoomControlName">Name of the adult per room control.</param>
        /// <param name="childAgeForRoomsControlName">Name of the child age for rooms control.</param>
        /// <param name="bedTypeControlName">Name of the bed type control.</param>
        /// <param name="controlAndBedType">Type of the control and bed.</param>
        /// 
        /// IF ANY CHANGES ARE MADE TO THIS METHOD, MAKE APPROPRIATE CHANGES IN THE BELOW MENTIONED FILE
        /// \Scandic.Scanweb.BookingEngine.Web\Templates\Booking\Units\ShoppingCartEditStayModule.ascx.cs
        /// 
        /// 
        private void CreateChildrenDetails(int count, List<HotelSearchRoomEntity> listHotelSearchRoomEntity
            , string[] bedTypes, List<ChildrensDetailsEntity> childrenDetailsallRooms,
            string childPerRoomControlName, string adultPerRoomControlName, string childAgeForRoomsControlName,
            string bedTypeControlName, Dictionary<string, string> controlAndBedType, ref bool errorSettingChildBedType)
        {
            HotelSearchRoomEntity hotelSearchRoomEntity = new HotelSearchRoomEntity();
            DropDownList noOfChilddropdown = this.FindControl(childPerRoomControlName + count) as DropDownList;
            int childrenCount = Convert.ToInt32(noOfChilddropdown.SelectedValue);
            DropDownList noOfAdultsDropdown = this.FindControl(adultPerRoomControlName + count) as DropDownList;
            int adultsCount = Convert.ToInt32(noOfAdultsDropdown.SelectedValue);
            hotelSearchRoomEntity.AdultsPerRoom = adultsCount;
            hotelSearchRoomEntity.ChildrenPerRoom = childrenCount;
            List<ChildEntity> listChildren = CreateChildEntityForRooms(childAgeForRoomsControlName, bedTypeControlName,
                controlAndBedType, bedTypes, count, childrenCount, ref errorSettingChildBedType);
            if ((listChildren != null) && (listChildren.Count > 0))
            {
                ChildrensDetailsEntity childrensDetailsEntity = new ChildrensDetailsEntity();
                childrensDetailsEntity.SetChildrensEntity((uint)childrenCount, listChildren);
                hotelSearchRoomEntity.ChildrenDetails = childrensDetailsEntity;
                childrenDetailsallRooms.Add(childrensDetailsEntity);
                hotelSearchRoomEntity.ChildrenOccupancyPerRoom = Utility.GetNoOfChildrenToBeAccommodated(childrensDetailsEntity);
            }
            listHotelSearchRoomEntity.Add(hotelSearchRoomEntity);
        }

        /// <summary>
        /// Create Children details object for all rooms selected in search page.
        /// </summary>
        /// <param name="bedTypeControlName">Name of the bed type control.</param>
        /// <param name="childAgeforRoomsControlName">Name of the child agefor rooms control.</param>
        /// <param name="noOfRoomsControlName">Name of the no of rooms control.</param>
        /// <param name="childPerRoomControlName">Name of the child per room control.</param>
        /// <param name="adultsPerRoomControlName">Name of the adults per room control.</param>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>
        /// <returns></returns>
        /// 
        /// IF ANY CHANGES ARE MADE TO THIS METHOD, MAKE APPROPRIATE CHANGES IN THE BELOW MENTIONED FILE
        /// \Scandic.Scanweb.BookingEngine.Web\Templates\Booking\Units\ShoppingCartEditStayModule.ascx.cs
        /// 
        /// 
        private List<ChildrensDetailsEntity> CreateChildrenDetailsforAllRooms(string bedTypeControlName,
            string childAgeforRoomsControlName, string noOfRoomsControlName, string childPerRoomControlName, string adultsPerRoomControlName,
            HotelSearchEntity hotelSearchEntity, ref bool errorSettingChildBedType)
        {
            List<HotelSearchRoomEntity> listHotelSearchRoomEntity = new List<HotelSearchRoomEntity>();
            string cot = string.Empty, extraBed = string.Empty, sharingBed = string.Empty;
            Utility.GetLocaleSpecificChildrenAccomodationTypes(ref cot, ref extraBed, ref sharingBed);
            string[] bedTypes = new string[3]
                                    {
                                        cot, extraBed, sharingBed
                                    };

            Dictionary<string, string> controlAndBedType = FetchBedTypeSelection();
            List<ChildrensDetailsEntity> childrenDetailsallRooms = new List<ChildrensDetailsEntity>();
            DropDownList noOfRoomsControl = this.FindControl(noOfRoomsControlName) as DropDownList;
            int noOfRooms = Convert.ToInt32(noOfRoomsControl.SelectedValue);

            for (int count = 1; count <= noOfRooms; count++)
            {
                CreateChildrenDetails(count, listHotelSearchRoomEntity, bedTypes, childrenDetailsallRooms,
                                      childPerRoomControlName, adultsPerRoomControlName, childAgeforRoomsControlName,
                        bedTypeControlName, controlAndBedType, ref errorSettingChildBedType);
            }
            hotelSearchEntity.ListRooms = listHotelSearchRoomEntity;
            return childrenDetailsallRooms;
        }

        /// <summary>
        /// Reads the hidden field populated by the javascript and creates a dictionary
        /// which will contain the dropdown name and value in it.
        /// </summary>
        /// <returns>Dictionary containing list of bed type dropdown name and selected value in it.</returns>
        /// <remarks>Release 2.0</remarks>
        private Dictionary<string, string> FetchBedTypeSelection()
        {
            string childrenBedTypes = bedTypeCollection.Value;
            string[] eachBedType = childrenBedTypes.Split(new char[] { '|' });
            Dictionary<string, string> controlAndBedType = new Dictionary<string, string>();
            int eachBedTypeCount = eachBedType.Length;
            for (int count = 0; count < eachBedTypeCount - 1; count++)
            {
                string[] bedTypeControlAndType = eachBedType[count].Split(new char[] { ',' });
                controlAndBedType.Add(bedTypeControlAndType[0], bedTypeControlAndType[1]);
            }
            return controlAndBedType;
        }

        /// <summary>
        /// Creates children entity from dropdowns.
        /// </summary>
        /// <param name="childAgeControlPrefix">The child age control prefix.</param>
        /// <param name="bedTypeControlPrefix">The bed type control prefix.</param>
        /// <param name="controlAndBedType">Type of the control and bed.</param>
        /// <param name="bedTypes">Collection of bed types</param>
        /// <param name="roomNo">The room no.</param>
        /// <param name="childrenCountPerRoom">The children count per room.</param>
        /// <returns>List of child entity</returns>
        /// <remarks></remarks>
        private List<ChildEntity> CreateChildEntityForRooms(string childAgeControlPrefix, string bedTypeControlPrefix,
            Dictionary<string, string> controlAndBedType, string[] bedTypes, int roomNo, int childrenCountPerRoom, ref bool errorSettingChildBedType)
        {
            List<ChildEntity> listChildren = new List<ChildEntity>();
            ChildEntity childEntity = null;
            StringBuilder controlSufix;
            for (int count = 1; count <= childrenCountPerRoom; count++)
            {
                controlSufix = new StringBuilder();
                controlSufix.Append(roomNo);
                controlSufix.Append("Child");
                controlSufix.Append(count);
                DropDownList ageDropdown =
                    this.FindControl(childAgeControlPrefix + controlSufix.ToString()) as DropDownList;
                string key = bedTypeControlPrefix + controlSufix.ToString();
                if ((null != ageDropdown) && (!string.IsNullOrEmpty(key)))
                {
                    if (ageDropdown.SelectedIndex != 0)
                    {
                        string bedType = controlAndBedType.ContainsKey(key) ? controlAndBedType[key] : string.Empty;

                        childEntity = CreateChildEntity(ageDropdown.SelectedValue, bedType,
                                                        bedTypes[0], bedTypes[1], bedTypes[2]);
                        listChildren.Add(childEntity);
                    }

                    else if (ageDropdown.SelectedIndex == 0)
                    {
                        errorSettingChildBedType = true;
                        break;
                    }
                }
            }
            return listChildren;
        }

        /// <summary>
        /// Create child enity object for each room
        /// </summary>
        /// <param name="childAgeforRoom1Child1"></param>
        /// <param name="bedTypeforRoom1Child1"></param>
        /// <param name="cot"></param>
        /// <param name="extraBed"></param>
        /// <param name="sharingBed"></param>
        /// <returns></returns>
        private ChildEntity CreateChildEntity(string childAge, string bedType, string cot, string extraBed,
                                              string sharingBed)
        {
            ChildEntity childEntity = new ChildEntity();
            childEntity.Age = Convert.ToInt16(childAge);
            if (string.Compare(bedType, cot, false) == 0)
            {
                childEntity.ChildAccommodationType = ChildAccommodationType.CRIB;
                childEntity.AccommodationString = cot;
            }
            else if (string.Compare(bedType, sharingBed, false) == 0)
            {
                childEntity.ChildAccommodationType = ChildAccommodationType.CIPB;
                childEntity.AccommodationString = sharingBed;
            }
            else if (string.Compare(bedType, extraBed, false) == 0)
            {
                childEntity.ChildAccommodationType = ChildAccommodationType.XBED;
                childEntity.AccommodationString = extraBed;
            }
            return childEntity;
        }

        /// <summary>
        /// Verifying if the user is already logged to the system
        /// </summary>
        private void SetLoginStatus()
        {
            if (UserLoggedInSessionWrapper.UserLoggedIn)
            {
                guestLogin.Value = "true";
            }
            else
            {
                guestLogin.Value = "false";
            }
        }

        /// <summary>
        /// Create Search Enity object for Regular Booking module
        /// </summary>
        /// <param name="Tab1"></param>
        /// <returns></returns>
        /// 
        /// IF ANY CHANGES ARE MADE TO THIS METHOD, MAKE APPROPRIATE CHANGES IN THE BELOW MENTIONED FILE
        /// \Scandic.Scanweb.BookingEngine.Web\Templates\Booking\Units\ShoppingCartEditStayModule.ascx.cs
        /// 
        /// 
        private HotelSearchEntity GetHotelSearchEntity(string Tab)
        {
            string relText = string.Empty;
            if (txtRegularCode.Attributes["rel"] != null)
            {
                relText = txtRegularCode.Attributes["rel"].ToString();
            }
            txtRegularCode.Value = txtRegularCode.Value != relText ? txtRegularCode.Value.Trim() : string.Empty;

            if (txtRegularCode != null && txtRegularCode.Value.Contains("/"))
            {
                string[] regularCodes = txtRegularCode.Value.Split('/');
                BookingEngineSessionWrapper.IATAProfileCode = regularCodes[1];
                BookingEngineSessionWrapper.BookingCodeIATA = txtRegularCode.Value;
                txtRegularCode.Value = regularCodes[0];
            }
            int iataCode = -1;
            int.TryParse(Convert.ToString(txtRegularCode.Value), out iataCode);
            if (iataCode > 0)
            {
                BookingEngineSessionWrapper.IATAProfileCode = txtRegularCode.Value;
                txtRegularCode.Value = string.Empty;
            }
            HotelSearchEntity hotelSearch = new HotelSearchEntity();
            SearchedForEntity searchedFor = null;
            string[] destCodeArr;
            switch (Tab)
            {
                case BookingTab.Tab1:
                    {
                        if (!string.IsNullOrEmpty(selectedDestId.Value) &&
                            !(WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName").Equals(
                                txtHotelName.Value)))
                        {
                            searchedFor = new SearchedForEntity(txtHotelName.Value.Trim());
                            destCodeArr = selectedDestId.Value.ToUpper().Split(':');
                            if (destCodeArr[0] == "HOTEL")
                                searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.Hotel;
                            else
                            {
                                searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.City;
                                Reservation2SessionWrapper.SearchedCityName = ((txtHotelName != null) &&
                                                                   (txtHotelName.Value != null))
                                                                      ? (txtHotelName.Value.ToString())
                                                                      : string.Empty;
                            }
                            searchedFor.SearchCode = destCodeArr[1];
                            if (searchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                                hotelSearch.SelectedHotelCode = searchedFor.SearchCode;

                            hotelSearch.SearchedFor = searchedFor;
                        }
                        else
                        {
                            hotelSearch.SearchedFor = searchedFor;
                        }

                        hotelSearch.ArrivalDate = Core.DateUtil.StringToDDMMYYYDate(txtArrivalDate.Value);
                        hotelSearch.DepartureDate = Core.DateUtil.StringToDDMMYYYDate(txtDepartureDate.Value);
                        hotelSearch.NoOfNights = Core.DateUtil.DateDifference(hotelSearch.DepartureDate,
                                                                              hotelSearch.ArrivalDate);
                        hotelSearch.AdultsPerRoom = int.Parse(ddlAdultsPerRoom1.Text);
                        hotelSearch.ChildrenPerRoom = int.Parse(ddlChildPerRoom1.Text);
                        hotelSearch.RoomsPerNight = int.Parse(ddlNoOfRoomsReg.Text); // added by vrushali.

                        if (BookingEngineSessionWrapper.IATAProfileCode != null || BookingEngineSessionWrapper.IATAProfileCode != string.Empty)
                            hotelSearch.IataProfileCode = BookingEngineSessionWrapper.IATAProfileCode;
                    }
                    break;
                case BookingTab.Tab2:
                    {
                        if (!string.IsNullOrEmpty(selectedDestId.Value)
                            &&
                            !(WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName").Equals(
                                txtHotelNameBNC.Value)))
                        {
                            searchedFor = new SearchedForEntity(txtHotelNameBNC.Value.Trim());
                            destCodeArr = selectedDestId.Value.ToUpper().Split(':');
                            if (destCodeArr[0] == "HOTEL")
                                searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.Hotel;
                            else
                            {
                                searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.City;
                                Reservation2SessionWrapper.SearchedCityName = ((txtHotelNameBNC != null) &&
                                                                   (txtHotelNameBNC.Value != null))
                                                                      ? (txtHotelNameBNC.Value.ToString())
                                                                      : string.Empty;
                            }
                            searchedFor.SearchCode = destCodeArr[1];
                            if (searchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                                hotelSearch.SelectedHotelCode = searchedFor.SearchCode;

                            hotelSearch.SearchedFor = searchedFor;
                        }
                        else
                        {
                            hotelSearch.SearchedFor = searchedFor;
                        }
                        hotelSearch.ArrivalDate = Core.DateUtil.StringToDDMMYYYDate(txtBCArrivalDate.Value);
                        hotelSearch.DepartureDate = Core.DateUtil.StringToDDMMYYYDate(txtBCDepartureDate.Value);
                        hotelSearch.NoOfNights = Core.DateUtil.DateDifference(hotelSearch.DepartureDate,
                                                                              hotelSearch.ArrivalDate);
                        hotelSearch.AdultsPerRoom = int.Parse(ddlBCAdultsPerRoom1.Text);
                        hotelSearch.ChildrenPerRoom = int.Parse(ddlBCChildPerRoom1.Text);
                        hotelSearch.RoomsPerNight = int.Parse(ddlNoOfRoomsBC.Text);
                    }
                    break;
                case BookingTab.Tab3:
                    {
                        if (!string.IsNullOrEmpty(selectedDestId.Value)
                            &&
                            !(WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName").Equals(
                                txtHotelNameRewardNights.Value)))
                        {
                            searchedFor = new SearchedForEntity(txtHotelNameRewardNights.Value.Trim());
                            destCodeArr = selectedDestId.Value.ToUpper().Split(':');
                            if (destCodeArr[0] == "HOTEL")
                                searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.Hotel;
                            else
                            {
                                searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.City;
                                Reservation2SessionWrapper.SearchedCityName = ((txtHotelNameRewardNights != null)
                                                                   && (txtHotelNameRewardNights.Value != null))
                                                                      ? (txtHotelNameRewardNights.Value.ToString())
                                                                      : string.Empty;
                            }
                            searchedFor.SearchCode = destCodeArr[1];
                            if (searchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                                hotelSearch.SelectedHotelCode = searchedFor.SearchCode;

                            hotelSearch.SearchedFor = searchedFor;
                        }
                        else
                        {
                            hotelSearch.SearchedFor = searchedFor;
                        }
                        hotelSearch.ArrivalDate = Core.DateUtil.StringToDDMMYYYDate(txtRNArrivalDate.Value);
                        hotelSearch.DepartureDate = Core.DateUtil.StringToDDMMYYYDate(txtRNDepartureDate.Value);
                        hotelSearch.NoOfNights = Core.DateUtil.DateDifference(hotelSearch.DepartureDate,
                                                                              hotelSearch.ArrivalDate);
                        hotelSearch.AdultsPerRoom = int.Parse(ddlRNAdultsPerRoom1.Text);
                        hotelSearch.ChildrenPerRoom = int.Parse(ddlRNChildPerRoom1.Text);
                        hotelSearch.RoomsPerNight = Int32.Parse(AppConstants.ONE);
                    }
                    break;
            }
            if (searchedFor != null && searchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City)
                searchedFor.CitySearchString = searchedFor.SearchString;

            hotelSearch.SearchingType = GetSearchType(Tab);

            if ((UserLoggedInSessionWrapper.UserLoggedIn) &&
                ((hotelSearch.SearchingType == SearchType.REGULAR) ||
                 (hotelSearch.SearchingType == SearchType.BONUSCHEQUE)
                 || (hotelSearch.SearchingType == SearchType.CORPORATE)))
            {
                LoyaltyDetailsEntity loyaltyDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
                if (null != loyaltyDetails)
                {
                    long membershipNumber;
                    bool parseStatus = long.TryParse(loyaltyDetails.MembershipID, out membershipNumber);
                    if (parseStatus)
                    {
                        hotelSearch.MembershipID = membershipNumber;
                    }
                }
            }


            switch (hotelSearch.SearchingType)
            {
                case SearchType.REGULAR:
                    {
                        if (!string.IsNullOrEmpty(txtRegularCode.Value.Trim()))
                        {
                            hotelSearch.CampaignCode = txtRegularCode.Value.ToUpper().Trim();
                            if (rememberMe.Checked)
                            {
                                SaveBookingCodeToCookies(hotelSearch.CampaignCode);
                            }
                            else
                            {
                                DeleteBookingCodeFromCookies();
                            }
                        }
                        break;
                    }
                case SearchType.CORPORATE:
                    {
                        hotelSearch.CampaignCode = Utility.FormatCode(txtRegularCode.Value);
                        hotelSearch.QualifyingType = Utility.GetQualifyingType(hotelSearch.CampaignCode);
                        if (rememberMe.Checked)
                        {
                            SaveBookingCodeToCookies(hotelSearch.CampaignCode);
                        }
                        else
                        {
                            DeleteBookingCodeFromCookies();
                        }
                        break;
                    }
                case SearchType.VOUCHER:
                    {
                        hotelSearch.CampaignCode = txtRegularCode.Value.ToUpper().Trim();
                        if (rememberMe.Checked)
                        {
                            SaveBookingCodeToCookies(hotelSearch.CampaignCode);
                        }
                        else
                        {
                            DeleteBookingCodeFromCookies();
                        }
                        break;
                    }
            }
            return hotelSearch;
        }


        /// <summary>
        /// Find out the dropdown and populate the values.
        /// </summary>
        /// <param name="controlName">Control name to be populated</param>
        /// <param name="startValue">Start value in Dropdown</param>
        /// <param name="endValue">End value in Dropdown</param>
        /// <param name="defaultValue">If any default value then this will appear first instead of start value</param>
        /// <remarks></remarks>
        private void CreateDropDown(string controlName, int startValue, int endValue, string defaultValue)
        {
            DropDownList dropDownList = this.FindControl(controlName) as DropDownList;
            if (null != dropDownList)
            {
                ListItem item;
                if (!string.IsNullOrEmpty(defaultValue))
                {
                    item = new ListItem(defaultValue, "DFT");
                    dropDownList.Items.Add(item);
                }
                for (int constantCount = startValue; constantCount <= endValue; constantCount++)
                {
                    item = new ListItem(constantCount.ToString(), constantCount.ToString());
                    dropDownList.Items.Add(item);
                }
            }
        }

        /// <summary>
        /// This method sets the Dropdowns appropriately as required for this page
        /// The list of values are as defined in the Form and Page Behaviour document
        /// </summary>
        private void SetDropDowns()
        {
            txtnoOfNights.Text = "1";
            txtBonousChequeNoofNights.Text = "1";
            txtRewardNightNoOfNights.Text = "1";
            CreateDropDown("ddlNoOfRoomsReg", 1, 4, "");
            CreateDropDown("ddlNoOfRoomsBC", 1, 4, "");
            CreateDropDown("ddlNoOfRoomsRed", 1, 1, "");
            for (int count = 1; count <= 4; count++)
            {
                CreateDropDown("ddlAdultsPerRoom" + count, 1, 6, "");
                CreateDropDown("ddlBCAdultsPerRoom" + count, 1, 6, "");
                CreateDropDown("ddlRNAdultsPerRoom" + count, 1, 6, "");
            }
            for (int count = 1; count <= 4; count++)
            {
                CreateDropDown("ddlChildPerRoom" + count, 0, 5, "");
                CreateDropDown("ddlBCChildPerRoom" + count, 0, 5, "");
                CreateDropDown("ddlRNChildPerRoom" + count, 0, 5, "");
            }
            int minAge = Convert.ToInt32(ConfigurationManager.AppSettings["Booking.Children.MinAge"]);
            int maxAge = Convert.ToInt32(ConfigurationManager.AppSettings["Booking.Children.MaxAge"]);

            for (int count = 1; count <= 4; count++)
            {
                for (int innerCount = 1; innerCount <= 5; innerCount++)
                {
                    if (this.FindControl("isBookingModuleSmall") != null)
                    {
                        CreateDropDown("childAgeforRoom" + count + "Child" + innerCount, minAge, maxAge,
                                       WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age"));
                        CreateDropDown("ddlBCChildAgeforRoom" + count + "Child" + innerCount, minAge, maxAge,
                                       WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age"));
                        CreateDropDown("ddlRNChildAgeforRoom" + count + "Child" + innerCount, minAge, maxAge,
                                       WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age"));
                    }
                    else
                    {
                        CreateDropDown("childAgeforRoom" + count + "Child" + innerCount, minAge, maxAge,
                                       WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Selectage"));
                        CreateDropDown("ddlBCChildAgeforRoom" + count + "Child" + innerCount, minAge, maxAge,
                                       WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Selectage"));
                        CreateDropDown("ddlRNChildAgeforRoom" + count + "Child" + innerCount, minAge, maxAge,
                                       WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Selectage"));
                    }
                }
            }

            string[] bedTypes = new string[3]
                                    {
                                        WebUtil.GetTranslatedText(
                                            "/bookingengine/booking/childrensdetails/accommodationtypes/sharingbed"),
                                        WebUtil.GetTranslatedText(
                                            "/bookingengine/booking/childrensdetails/accommodationtypes/crib"),
                                        WebUtil.GetTranslatedText(
                                            "/bookingengine/booking/childrensdetails/accommodationtypes/extrabed")
                                    };

            ListItem tab1 = new ListItem(WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/booking"), "1");
            ListItem tab2 = new ListItem(WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/bonus"), "2");
            ListItem tab3 = new ListItem(WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/redemption"), "3");
        }

        /// <summary>
        /// This method sets the search fields by reading the values 
        /// from the session's SearchCriteria
        /// if the SearchCriteria is not in session only the arrival date, departure date
        /// and the search type tab is set to Tab1
        /// </summary>
        private void SetFields()
        {
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
            if (null != searchCriteria && !m_IsBookingModuleReset)
            {
                string searchType = searchCriteria.SearchingType.ToString();
                string campaignCode = searchCriteria.CampaignCode;

                Regex partnerProfileCodeValidator = new Regex(@"LSH[0-9]");
                if (!string.IsNullOrEmpty(campaignCode))
                {
                    if (partnerProfileCodeValidator.IsMatch(campaignCode))
                    {
                        campaignCode = string.Empty;
                    }
                }

                int iataCode = -1;
                int.TryParse(campaignCode, out iataCode);
                if (iataCode > 0)
                    campaignCode = string.Empty;

                if (searchCriteria.SearchedFor != null && searchCriteria.SearchedFor.SearchString != null)
                    SetHotelNameInAllAccordians(searchCriteria.SearchedFor.SearchString.ToString());

                if (searchCriteria.SearchedFor == null && HttpContext.Current.Session["deepLinkDates"] == "true")
                {
                    SetDate(searchCriteria.ArrivalDate, searchCriteria.DepartureDate);
                    HttpContext.Current.Session["deepLinkDates"] = "false";
                }
                else if (searchCriteria.SearchedFor == null)
                {
                    SetDate(DateTime.Now, DateTime.Now.AddDays(1));
                }
                else
                {
                    SetDate(searchCriteria.ArrivalDate, searchCriteria.DepartureDate);
                }

                if (searchType == SearchType.REGULAR.ToString() || searchType == SearchType.CORPORATE.ToString() ||
                    searchType == SearchType.VOUCHER.ToString())
                {
                    ddlAdultsPerRoom1.SelectedValue = searchCriteria.AdultsPerRoom.ToString();
                    sT.Value = GetSelectedTab(searchCriteria.SearchingType);
                }
                if (searchType == SearchType.REDEMPTION.ToString())
                {
                    SetDate(DateTime.Now, DateTime.Now.AddDays(1));
                    sT.Value = BookingTab.Tab3;
                    HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab3;

                    if (UserLoggedInSessionWrapper.UserLoggedIn == true)
                    {
                        guestLogin.Value = "true";
                    }
                }

                if (searchType == SearchType.MODIFY.ToString())
                {
                    sT.Value = BookingTab.Tab4;
                    HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab4;

                    if (UserLoggedInSessionWrapper.UserLoggedIn == true)
                    {
                        guestLogin.Value = "true";
                    }
                }


                else if (searchType == SearchType.REGULAR.ToString())
                {
                    sT.Value = BookingTab.Tab1;
                    HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab1;

                    if (!string.IsNullOrEmpty(campaignCode))
                    {
                        txtRegularCode.Value = searchCriteria.CampaignCode.ToString();
                    }
                    ReadCookie();
                    if (searchCriteria.SearchedFor != null)
                        txtHotelName.Value = searchCriteria.SearchedFor.SearchString;

                    OpenDviesFor_Regular_Corporate_VoucherBooking();
                }
                else if (searchType == SearchType.CORPORATE.ToString())
                {
                    sT.Value = BookingTab.Tab1;
                    HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab1;

                    if (!string.IsNullOrEmpty(campaignCode))
                    {
                        txtRegularCode.Value = searchCriteria.CampaignCode.ToString();
                    }
                    ReadCookie();
                    OpenDviesFor_Regular_Corporate_VoucherBooking();
                }
                else if (searchType == SearchType.VOUCHER.ToString())
                {
                    sT.Value = BookingTab.Tab1;
                    HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab1;

                    if (!string.IsNullOrEmpty(campaignCode))
                    {
                        txtRegularCode.Value = searchCriteria.CampaignCode.ToString();
                    }
                    ReadCookie();
                    OpenDviesFor_Regular_Corporate_VoucherBooking();
                }
                else if (searchType == SearchType.BONUSCHEQUE.ToString())
                {
                    SetDateForBCandRedemption(searchCriteria.ArrivalDate, searchCriteria.DepartureDate, searchType);
                    ddlBCAdultsPerRoom1.SelectedValue = searchCriteria.AdultsPerRoom.ToString();
                    OpeningDivsForBonousCheck();
                    if ((searchCriteria.SearchedFor != null) &&
                        (!string.IsNullOrEmpty(searchCriteria.SearchedFor.SearchString)))
                        txtHotelNameBNC.Value = searchCriteria.SearchedFor.SearchString.ToString();
                    ddlNoOfRoomsBC.SelectedValue = Convert.ToString(searchCriteria.RoomsPerNight);
                    txtBonousChequeNoofNights.Text = Convert.ToString(searchCriteria.NoOfNights);
                    sT.Value = BookingTab.Tab2;
                    HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab2;
                }
                if (searchCriteria.SearchedFor != null)
                {
                    if (searchCriteria.SearchingType.ToString() == SearchType.BONUSCHEQUE.ToString())
                    {
                        if (searchCriteria.SelectedHotelCode != null)
                        {
                            string selectedHotel =
                                m_AvailabilityController.GetHotelNameFromCMS(searchCriteria.SelectedHotelCode);
                            if (selectedHotel != null && selectedHotel != string.Empty)
                            {
                                txtHotelNameBNC.Value = selectedHotel;
                            }
                        }
                    }
                    else if (searchCriteria.SearchingType.ToString() == SearchType.REDEMPTION.ToString())
                    {
                        SetDateForBCandRedemption(searchCriteria.ArrivalDate, searchCriteria.DepartureDate, searchType);
                        txtRNArrivalDate.Value = Core.DateUtil.DateToDDMMYYYYString(searchCriteria.ArrivalDate);
                        txtRNDepartureDate.Value = Core.DateUtil.DateToDDMMYYYYString(searchCriteria.DepartureDate);
                        ddlRNAdultsPerRoom1.SelectedValue = searchCriteria.AdultsPerRoom.ToString();
                        OpeningDivsForRedumptionBooking();
                        if ((searchCriteria.SearchedFor != null) &&
                            (!string.IsNullOrEmpty(searchCriteria.SearchedFor.SearchString)))
                            txtHotelNameRewardNights.Value = searchCriteria.SearchedFor.SearchString.ToString();
                        ddlNoOfRoomsRed.SelectedValue = Convert.ToString(searchCriteria.RoomsPerNight);
                        sT.Value = BookingTab.Tab3;
                        HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab3;
                    }
                    else
                    {
                        if (searchCriteria.SelectedHotelCode != null)
                        {
                            string selectedHotel =
                                m_AvailabilityController.GetHotelNameFromCMS(searchCriteria.SelectedHotelCode);
                            if (!string.IsNullOrEmpty(selectedHotel))
                            {
                                txtHotelName.Value = selectedHotel;
                                selectedDestId.Value = "HOTEL:" + searchCriteria.SelectedHotelCode;
                            }
                        }
                        else
                        {
                            txtHotelName.Value = searchCriteria.SearchedFor.SearchString;
                            if (searchCriteria.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                                selectedDestId.Value = "HOTEL:" + searchCriteria.SearchedFor.SearchCode;
                            else
                                selectedDestId.Value = "CITY:" + searchCriteria.SearchedFor.SearchCode;
                        }
                    }
                }
            }
            else
            {
                SetDate(DateTime.Now, DateTime.Now.AddDays(1));
                if (string.IsNullOrEmpty(sT.Value))
                {
                    sT.Value = BookingTab.Tab1;
                }
                if (Reservation2SessionWrapper.BookingModuleActiveTab)
                {
                    sT.Value = BookingTab.Tab3;
                }

                PopulateBookingCodeFromCookies();
            }
        }

        /// <summary>
        /// Fill the Dropdown values on Confirmation page on Add anohter hotel link.
        /// </summary>
        private void OpenDviesFor_Regular_Corporate_VoucherBooking()
        {
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
            OpeningDivs();
            if (searchCriteria != null)
            {
                ddlNoOfRoomsReg.SelectedValue = Convert.ToString(searchCriteria.RoomsPerNight);
                txtnoOfNights.Text = Convert.ToString(searchCriteria.NoOfNights);
            }
        }

        /// <summary>
        /// Reads the Search Type string from "sT" field and accordingly returns the SearchType enum value
        /// if none of the Tab values is returned due to some issue or tampering of the text field
        /// it will be considered as the Regular availability search
        /// 
        /// This will take the values like "Tab1", "Tab2" etc., and return the SearchType enum value
        /// </summary>
        /// <param name="searhType"></param>
        /// <returns></returns>
        private SearchType GetSearchType(string searchType)
        {
            SearchType returnValue = SearchType.REGULAR;

            switch (searchType)
            {
                case BookingTab.Tab1:
                    {
                        if (string.IsNullOrEmpty(txtRegularCode.Value.Trim()))
                        {
                            returnValue = SearchType.REGULAR;
                        }
                        else
                        {
                            returnValue = GetBookingType();
                        }
                        break;
                    }
                case BookingTab.Tab2:
                    {
                        returnValue = SearchType.BONUSCHEQUE;
                        break;
                    }
                case BookingTab.Tab3:
                    {
                        returnValue = SearchType.REDEMPTION;
                        break;
                    }
                default:
                    {
                        returnValue = SearchType.REGULAR;
                        break;
                    }
            }
            return returnValue;
        }

        /// <summary>
        /// After merging of booking tabs Regular tab is used to identify the booking type.
        /// D - If code starts with D then it is Corporate Booking.
        /// L - If code starts with L then it is Travel agent booking.
        /// B - If code starts with B then it is block code booking.
        /// VO- IF code starts with VO then it is Voucher booking.
        /// </summary>
        /// <returns>Search Type</returns>
        /// <remarks>R1.5.3 | Artifact artf871521 : Merge the Booking tabs.</remarks>
        private SearchType GetBookingType()
        {
            SearchType returnValue = SearchType.REGULAR;
            string bookingCode = txtRegularCode.Value.ToUpper().Trim();
            if (bookingCode != WebUtil.GetTranslatedText("/scanweb/bookingengine/booking/searchhotel/BookingCode"))
            {
                if (bookingCode.StartsWith("D") || bookingCode.StartsWith("L") || bookingCode.StartsWith("B"))
                {
                    returnValue = SearchType.CORPORATE;
                }
                else if (bookingCode.StartsWith("VO"))
                {
                    returnValue = SearchType.VOUCHER;
                }
            }
            return returnValue;
        }

        /// <summary>
        /// It is the inverse method of the GetSearchType.
        /// 
        /// This method will return the value "Tab1", "Tab2"........ based on 
        /// the Search type passed.
        /// 
        /// The value Tab1, Tab2 is required to be set to the input field "sT"
        /// based on which the corresponding tab is selected on the search page
        /// </summary>
        /// <param name="searchType"></param>
        /// <returns></returns>
        private string GetSelectedTab(SearchType searchType)
        {
            if (SearchType.REGULAR == searchType) return BookingTab.Tab1;
            else if (SearchType.CORPORATE == searchType) return BookingTab.Tab1;
            else if (SearchType.BONUSCHEQUE == searchType) return BookingTab.Tab2;
            else if (SearchType.REDEMPTION == searchType) return BookingTab.Tab3;
            else if (SearchType.VOUCHER == searchType) return BookingTab.Tab1;
            else if (SearchType.MODIFY == searchType) return BookingTab.Tab4;
            else return BookingTab.Tab1;
        }

        #region Booking Code Cookie

        /// <summary>
        /// Populate D-number from Cookies
        /// </summary>
        private void PopulateBookingCodeFromCookies()
        {
            if (AppConstants.SCANDIC_BOOKING_CODE_COOKIES)
            {
                HttpCookie bookingCodeCookie = Request.Cookies[AppConstants.BOOOKING_CODE_COOKIE];
                if ((bookingCodeCookie != null) && (!string.IsNullOrEmpty(bookingCodeCookie.Value)))
                {
                    sT.Value = BookingTab.Tab1;
                    txtRegularCode.Value = bookingCodeCookie.Value;
                    rememberMe.Checked = true;
                }
            }
        }

        /// <summary>
        /// Delete D-number from Cookies
        /// </summary>
        private void DeleteBookingCodeFromCookies()
        {
            if (AppConstants.SCANDIC_BOOKING_CODE_COOKIES)
            {
                HttpCookie bookingCookie = Request.Cookies[AppConstants.BOOOKING_CODE_COOKIE];
                if (bookingCookie != null)
                {
                    Response.Cookies[AppConstants.BOOOKING_CODE_COOKIE].Expires = DateTime.Now.AddYears(-30);
                }
            }
        }

        /// <summary>
        /// Save D-number to Cookies
        /// </summary>
        private void SaveBookingCodeToCookies(string bookingCode)
        {
            if (AppConstants.SCANDIC_BOOKING_CODE_COOKIES)
            {
                HttpCookie dnumberCookie = Request.Cookies[AppConstants.BOOOKING_CODE_COOKIE];
                if (dnumberCookie == null)
                {
                    dnumberCookie = new HttpCookie(AppConstants.BOOOKING_CODE_COOKIE);
                }
                dnumberCookie.Value = bookingCode;
                dnumberCookie.Expires = DateTime.Now.AddYears(AppConstants.SCANDIC_BOOKING_CODE_EXPIRYYEAR);
                Response.Cookies.Add(dnumberCookie);
            }
        }

        /// <summary>
        /// Read cookie & set the check box "Remember me" value
        /// </summary>
        private void ReadCookie()
        {
            if (Request.Cookies[AppConstants.BOOOKING_CODE_COOKIE] != null)
            {
                rememberMe.Checked = true;
            }
            else
            {
                rememberMe.Checked = false;
            }
        }

        #endregion Booking Code Cookie

        #region Accordian Cancel or Modify Your Booking related methods

        /// <summary>
        /// Checks wheather this is master booking number or individual booking number.
        /// </summary>
        /// <example>
        /// 12345-1 is considered as individual booking number where as
        /// 12345(with out -) is considered as master booking number.
        /// </example>
        /// <param name="confirmationNumber">
        /// The confirmation number for which bookig details to be retrieved.
        /// </param>
        /// <returns>
        /// True - if the confirmation number is master booking.
        /// False - if the confirmation number is individual booking.
        /// </returns>
        private bool IsMasterConfirmationNumber(string confirmationNumber)
        {
            bool status = false;

            if (!confirmationNumber.Contains(AppConstants.HYPHEN))
            {
                status = true;
            }
            return status;
        }

        /// <summary>
        /// This method will cross verify the booking number and last name with all the booking available.
        /// If collection of booking contains the last name given by user then returns true else false.
        /// </summary>
        /// <param name="bookingDetailsEntity">Collection of booking.</param>
        /// <returns>
        /// True or False.
        /// </returns>
        private bool CheckName(List<BookingDetailsEntity> bookingDetailsEntity, string lastName)
        {
            bool doesContainName = false;
            int bookingEntityLength = bookingDetailsEntity.Count;
            for (int counter = 0; counter < bookingEntityLength; counter++)
            {
                if ((bookingDetailsEntity[counter].GuestInformation != null) && !string.IsNullOrEmpty(lastName) 
                    && ((!string.IsNullOrEmpty(bookingDetailsEntity[counter].GuestInformation.LastName) && string.Equals(lastName, bookingDetailsEntity[counter].GuestInformation.LastName.Trim(), StringComparison.InvariantCultureIgnoreCase)) 
                    || (!string.IsNullOrEmpty(bookingDetailsEntity[counter].GuestInformation.NativeLastName) && string.Equals(lastName, bookingDetailsEntity[counter].GuestInformation.NativeLastName.Trim(),StringComparison.InvariantCultureIgnoreCase))))
                {
                    doesContainName = true;
                    break;
                }
            }
            return doesContainName;
        }

        /// <summary>
        /// Split the Booking Number into MasterBooking Number and Leg-Number
        /// </summary>
        /// <param name="bookingNumber">BookingNumber to be split</param>
        /// <param name="legNumber">out LegNumber</param>
        /// <returns>Booking Number</returns>
        private string SplitBookingAndLegNumber(string bookingNumber, out string legNumber)
        {
            string[] splitedString = new string[2];
            splitedString = bookingNumber.Split(char.Parse(AppConstants.HYPHEN));
            bookingNumber = splitedString[0].Trim();
            legNumber = splitedString[1].Trim();

            return bookingNumber;
        }

        private void SetDate(DateTime arrivalDate, DateTime departureDate)
        {
            string arrDate = WebUtil.GetDayFromDate(arrivalDate) + AppConstants.SPACE +
                             Core.DateUtil.DateToDDMMYYYYString(arrivalDate);
            string depDate = WebUtil.GetDayFromDate(departureDate) + AppConstants.SPACE +
                             Core.DateUtil.DateToDDMMYYYYString(departureDate);
            txtArrivalDate.Value = arrDate;
            txtBCArrivalDate.Value = arrDate;
            txtRNArrivalDate.Value = arrDate;
            txtDepartureDate.Value = depDate;
            txtBCDepartureDate.Value = depDate;
            txtRNDepartureDate.Value = depDate;
        }

        private void SetHotelNameInAllAccordians(string strHotelName)
        {
            txtHotelName.Value = strHotelName;
            txtHotelNameBNC.Value = strHotelName;
            txtHotelNameRewardNights.Value = strHotelName;
        }

        /// <summary>
        /// This will set the arival date and departure date for all tab in booking module big.
        /// </summary>
        /// <param name="arrivalDate"></param>
        /// <param name="departureDate"></param>
        /// <param name="searchType"></param>
        private void SetDateForBCandRedemption(DateTime arrivalDate, DateTime departureDate, string searchType)
        {
            string arrDate = WebUtil.GetDayFromDate(arrivalDate) + AppConstants.SPACE +
                             Core.DateUtil.DateToDDMMYYYYString(arrivalDate);
            string depDate = WebUtil.GetDayFromDate(departureDate) + AppConstants.SPACE +
                             Core.DateUtil.DateToDDMMYYYYString(departureDate);
            if (searchType == SearchType.BONUSCHEQUE.ToString())
            {
                txtBCArrivalDate.Value = arrDate;
                txtBCDepartureDate.Value = depDate;

                txtRNArrivalDate.Value = arrDate;
                txtRNDepartureDate.Value = depDate;

                txtArrivalDate.Value = arrDate;
                txtDepartureDate.Value = depDate;
            }
            else if (searchType == SearchType.REDEMPTION.ToString())
            {
                txtRNArrivalDate.Value = arrDate;
                txtRNDepartureDate.Value = depDate;

                txtBCArrivalDate.Value = arrDate;
                txtBCDepartureDate.Value = depDate;

                txtArrivalDate.Value = arrDate;
                txtDepartureDate.Value = depDate;
            }
        }

        #endregion Private Methods

        #region Code for add Another Hotel

        /// <summary>
        /// Ashish Setting bed type to the DDL Child Bed respective of age
        /// </summary>
        /// <param name="ddlChildBed">child's bed ddl</param>
        /// <param name="childsAge">age of child</param>
        private void CreateDropDownList(DropDownList ddlChildBed, int childsAge, string itemToSetInDdl)
        {
            string strDataToBind = SetChildsBedType(childsAge);
            string[] strSplitted = strDataToBind.Split(',');
            int indexToSet = 0;
            ListItem item = null;
            for (int iItem = 0; iItem < strSplitted.Length; iItem++)
            {
                if (!string.IsNullOrEmpty(itemToSetInDdl))
                {
                    if (itemToSetInDdl.Trim().ToUpper() == strSplitted[iItem].ToString().Trim().ToUpper())
                    {
                        indexToSet = iItem;
                    }
                }
                item = new ListItem(strSplitted[iItem].ToString(), strSplitted[iItem].ToString());
                ddlChildBed.Items.Add(item);
            }
            ddlChildBed.SelectedIndex = indexToSet;
        }

        /// <summary>
        /// Ashish Function that returns the respective string bed type to set in ddl of child bed type
        /// </summary>
        /// <param name="childsAge">Age of child</param>
        /// <returns>return bed type</returns>
        private string SetChildsBedType(int childsAge)
        {
            string strToReturn = string.Empty;
            ArrayList aList1 = new ArrayList();
            ArrayList aList2 = new ArrayList();
            ArrayList aList3 = new ArrayList();
            string str = WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/childcriteria");
            string[] strComma = str.Split(',');
            for (int iSep = 0; iSep < strComma.Length; iSep++)
            {
                string[] strUnderScore = strComma[iSep].Split('_');
                string[] strAgeBed = strUnderScore[0].Split('|');
                aList1.Add(strAgeBed[0]);
                aList2.Add(strAgeBed[1]);
                if (strUnderScore.Length > 1)
                    aList3.Add(strUnderScore[1]);
            }
            int returnInt = IndexNeedToPick(aList1, childsAge);
            if (aList3.Count > returnInt)
                strToReturn = Convert.ToString(aList2[returnInt] + "," + aList3[returnInt]);
            else
                strToReturn = Convert.ToString(aList2[returnInt]);
            return strToReturn;
        }

        /// <summary>
        /// Getting Index For Array List of Child Bed TYpe
        /// </summary>
        /// <param name="aList1"></param>
        /// <param name="childsAge"></param>
        /// <returns></returns>
        private int IndexNeedToPick(ArrayList aList1, int childsAge)
        {
            int returnInt = -1;
            for (int indexNeed = 0; indexNeed < aList1.Count; indexNeed++)
            {
                string[] strAges = Convert.ToString(aList1[indexNeed]).Split('-');
                if ((childsAge >= Convert.ToInt32(strAges[0])) && (childsAge <= Convert.ToInt32(strAges[1])))
                {
                    returnInt = indexNeed;
                }
            }
            return returnInt;
        }

        /// <summary>
        /// Function use to Opening the divs for Regular Booking respective to the data in entity
        /// </summary>
        private void OpeningDivs()
        {
            GenericFunctionForRegularAndBonousCheck("idRoom", "childAgeforRoom", "bedTypeforRoom", "ddlAdultsPerRoom",
                                                    "ddlChildPerRoom");
        }

        /// <summary>
        /// generic Function for Bonus check booking and regular
        /// </summary>
        /// <param name="strIdRoom">id for room </param>
        /// <param name="strIdChildAge">id for ddl child age</param>
        /// <param name="StrIdChildBedType">id for ddl child bed type</param>
        /// <param name="strAdultsPerRoom">id for ddl adults per room</param>
        /// <param name="strChildPerRoom">id for dd child per room</param>
        private void GenericFunctionForRegularAndBonousCheck(string strIdRoom, string strIdChildAge,
                                                             string StrIdChildBedType,
                                                             string strAdultsPerRoom, string strChildPerRoom)
        {
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
            if (searchCriteria != null && searchCriteria.ListRooms != null)
            {
                for (int iCount = 0; iCount < searchCriteria.ListRooms.Count; iCount++)
                {
                    int iDiv = iCount;
                    iDiv++;
                    HtmlGenericControl htmlRoomDiv = this.FindControl(strIdRoom + iDiv) as HtmlGenericControl;
                    if (htmlRoomDiv != null)
                    {
                        if ((searchCriteria.ListRooms[iCount].ChildrenDetails != null) &&
                            (searchCriteria.ListRooms[iCount].ChildrenDetails.ListChildren != null))
                        {
                            for (int ichild = 0;
                                 ichild < searchCriteria.ListRooms[iCount].ChildrenDetails.ListChildren.Count;
                                 ichild++)
                            {
                                int iChildCount = ichild;
                                iChildCount++;
                                HtmlGenericControl htmlChildLabelDiv =
                                    this.FindControl(strIdRoom + iDiv + "ChildLabel") as HtmlGenericControl;
                                HtmlGenericControl htmlChildDdlDiv =
                                    this.FindControl(strIdRoom + iDiv + "Child" + iChildCount + "Ddl") as
                                    HtmlGenericControl;
                                htmlChildLabelDiv.Style.Add("display", "block");
                                htmlChildDdlDiv.Style.Add("display", "block");
                                DropDownList ddlChildAge =
                                    this.FindControl(strIdChildAge + iDiv + "Child" + iChildCount) as DropDownList;
                                if (
                                    searchCriteria.ListRooms[iCount].ChildrenDetails.ListChildren[ichild].
                                        ChildAccommodationType ==
                                    ChildAccommodationType.SelectBedType &&
                                    searchCriteria.ListRooms[iCount].ChildrenDetails.ListChildren[ichild].Age == 0)
                                {
                                    ddlChildAge.SelectedValue =
                                        WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Selectage");
                                }
                                else
                                {
                                    ddlChildAge.SelectedValue =
                                        Convert.ToString(
                                            searchCriteria.ListRooms[iCount].ChildrenDetails.ListChildren[ichild].Age);
                                }

                                DropDownList ddlChildBed =
                                    this.FindControl(StrIdChildBedType + iDiv + "Child" + iChildCount) as DropDownList;
                                CreateDropDownList(ddlChildBed,
                                                   searchCriteria.ListRooms[iCount].ChildrenDetails.ListChildren[ichild]
                                                       .Age,
                                                   searchCriteria.ListRooms[iCount].ChildrenDetails.ListChildren[ichild]
                                                       .AccommodationString);

                                ddlChildAge.Visible = true;
                                ddlChildAge.Style.Add("display", "block");
                                ddlChildBed.Visible = true;
                                ddlChildBed.Style.Add("display", "block");
                            }
                        }
                        //setting data to each room total no of child and adult ddlChildPerRoom1
                        DropDownList ddlAdultsPerRoom = this.FindControl(strAdultsPerRoom + iDiv) as DropDownList;
                        ddlAdultsPerRoom.SelectedValue = Convert.ToString(searchCriteria.ListRooms[iCount].AdultsPerRoom);
                        DropDownList ddlChildPerRoom = this.FindControl(strChildPerRoom + iDiv) as DropDownList;
                        ddlChildPerRoom.SelectedValue =
                            Convert.ToString(searchCriteria.ListRooms[iCount].ChildrenPerRoom);
                        htmlRoomDiv.Style.Add("display", "block");
                    }
                }
            }
        }

        /// <summary>
        /// Function use to Opening the divs for bonus check respective to the data in entity
        /// </summary>
        private void OpeningDivsForBonousCheck()
        {
            GenericFunctionForRegularAndBonousCheck("idBCRoom", "ddlBCChildAgeforRoom", "ddlBCBedTypeforRoom",
                                                    "ddlBCAdultsPerRoom", "ddlBCChildPerRoom");
        }

        /// <summary>
        ///Shameem added below method to populate the Reward Night booking modeul on Confirmation page.
        /// </summary>
        private void OpeningDivsForRedumptionBooking()
        {
            GenericFunctionForRegularAndBonousCheck("idRNRoom", "ddlRNChildAgeforRoom", "ddlRNBedTypeforRoom",
                                                    "ddlRNAdultsPerRoom", "ddlRNChildPerRoom");
        }

        #endregion Code for add Another Hotel

        #region functions to hide and show rooms in case of bed type error.

        /// <summary>
        /// Checks the chidren bed types set properly.
        /// </summary>
        /// <param name="errorSettingChildBedType">if set to <c>true</c> [error setting child bed type].</param>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>
        private void CheckChidrenBedTypesSetProperly(ref bool errorSettingChildBedType,
                                                     HotelSearchEntity hotelSearchEntity)
        {
            if ((hotelSearchEntity != null) && (hotelSearchEntity.ListRooms != null))
            {
                for (int i = 0; i < hotelSearchEntity.ListRooms.Count; i++)
                {
                    if ((hotelSearchEntity.ListRooms[i] != null) &&
                        ((hotelSearchEntity.ListRooms[i].ChildrenDetails) != null) &&
                        (hotelSearchEntity.ListRooms[i].ChildrenDetails.ListChildren != null) &&
                        (hotelSearchEntity.ListRooms[i].ChildrenDetails.ListChildren.Count > 0) &&
                        (bedTypeCollection.Value == string.Empty))
                    {
                        errorSettingChildBedType = true;
                        break;
                    }
                }
            }
        }


        private void HideChildernDetailsWhenBedTypeNotSet(HotelSearchEntity hotelSearchEntity, string currentBookingTab)
        {
            if ((hotelSearchEntity != null) && (hotelSearchEntity.ListRooms != null))
            {
                if (currentBookingTab == BookingTab.Tab1)
                {
                    HideAllRooms("idRoom");
                    ShowRelevantNoOfRooms("idRoom", hotelSearchEntity);
                }
                else if (currentBookingTab == BookingTab.Tab2)
                {
                    HideAllRooms("idBCRoom");
                    ShowRelevantNoOfRooms("idBCRoom", hotelSearchEntity);
                }
                else if (currentBookingTab == BookingTab.Tab3)
                {
                    HideAllRooms("idRNRoom");
                    ShowRelevantNoOfRooms("idRNRoom", hotelSearchEntity);
                }
            }
        }

        private void HideAllRooms(string roomLabel)
        {
            for (int totalRoomCount = 1; totalRoomCount <= 4; totalRoomCount++)
            {
                HtmlGenericControl divForRoom = this.FindControl(roomLabel + totalRoomCount) as HtmlGenericControl;
                if (divForRoom != null)
                    divForRoom.Style.Add("display", "none");
            }
        }

        private void ShowRelevantNoOfRooms(string roomLabel, HotelSearchEntity hotelSearchEntity)
        {
            if ((hotelSearchEntity != null) && (hotelSearchEntity.ListRooms != null))
            {
                for (int i = 0; i < hotelSearchEntity.ListRooms.Count; i++)
                {
                    int roomCount = i + 1;
                    HtmlGenericControl divForRoom = this.FindControl(roomLabel + roomCount) as HtmlGenericControl;
                    if (divForRoom != null)
                        divForRoom.Style.Add("display", "block");
                }
            }
        }

        #endregion

        #region INavigationTraking Members

        public List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> parameters = new List<KeyValueParam>();
            if (string.Equals(actionName, "Search", StringComparison.InvariantCultureIgnoreCase))
            {
                parameters.Add(new KeyValueParam("Search Type", actionName));
                var searchEntity = SearchCriteriaSessionWrapper.SearchCriteria;
                var counter = default(int);

                if (searchEntity.SearchedFor != null)
                {
                    parameters.Add(new KeyValueParam("Search String", searchEntity.SearchedFor.SearchString));
                    parameters.Add(new KeyValueParam("Search Code", searchEntity.SearchedFor.SearchCode));
                    parameters.Add(new KeyValueParam("User Search Type", searchEntity.SearchedFor.UserSearchType.ToString()));
                }
                parameters.Add(new KeyValueParam("Search Type", searchEntity.SearchingType.ToString()));
                parameters.Add(new KeyValueParam("Membership ID", searchEntity.MembershipID.ToString()));
                parameters.Add(new KeyValueParam("Arrival Date", searchEntity.ArrivalDate.ToString()));
                parameters.Add(new KeyValueParam("Departure Date: ", searchEntity.DepartureDate.ToString()));

                parameters.Add(new KeyValueParam("No Of Nights: ", searchEntity.NoOfNights.ToString()));
                parameters.Add(new KeyValueParam("Rooms Per Night: ", searchEntity.RoomsPerNight.ToString()));
                parameters.Add(new KeyValueParam("Adults Per Room: ", searchEntity.AdultsPerRoom.ToString()));
                parameters.Add(new KeyValueParam("Children Per Room: ", searchEntity.ChildrenPerRoom.ToString()));
                parameters.Add(new KeyValueParam("Campaign Code", searchEntity.CampaignCode));

                parameters.Add(new KeyValueParam("Children Occupancy Per Room: ",
                                                searchEntity.ChildrenOccupancyPerRoom.ToString()));

                parameters.AddRange((from room in searchEntity.ListRooms
                                     select new KeyValueParam(string.Format("Adults in Room{0}", ++counter),
                                                             room.AdultsPerRoom.ToString())).ToList());
                counter = default(int);
                parameters.AddRange((from room in searchEntity.ListRooms
                                     select new KeyValueParam(string.Format("Children in Room{0}", ++counter),
                                                             room.ChildrenPerRoom.ToString())).ToList());

                counter = default(int);
                parameters.AddRange((from room in searchEntity.ListRooms
                                     select new KeyValueParam(string.Format("Children Occupancy in Room{0}", ++counter),
                                                             room.ChildrenOccupancyPerRoom.ToString())).ToList());

                counter = searchEntity.ListRooms.Count;
                for (int i = 0; i < counter; i++)
                {
                    if (searchEntity.ListRooms[i].ChildrenDetails != null)
                    {
                        foreach (ChildEntity child in searchEntity.ListRooms[i].ChildrenDetails.ListChildren)
                        {
                            parameters.Add(new KeyValueParam(string.Format("Children Accomodation Type {0}", i),
                                                child.ChildAccommodationType.ToString()));
                        }
                    }
                }

                //Need to Verify at Runtime
                /*
                counter = default(int);
                parameters.AddRange((from room in searchEntity.ListRooms
                                     from child in room.ChildrenDetails.ListChildren
                                     select new KeyValueParam(string.Format("Age of Child{0} of Room{1}: {2}",  counter++),
                                                             child.Age.ToString())).ToList());

                counter = default(int);
                parameters.AddRange((from room in searchEntity.ListRooms
                                     from child in room.ChildrenDetails.ListChildren
                                     select new KeyValueParam(string.Format("Bed type of Child{0} of Room{1}: {2}{3}", counter++),
                                                             child.ChildAccommodationType.ToString())).ToList());

                counter = default(int);
                parameters.AddRange((from room in searchEntity.ListRooms
                                     from child in room.ChildrenDetails.ListChildren
                                     select new KeyValueParam(string.Format("Accomodation string of Child{0} of Room{1}: {2}{3}", counter++),
                                                             child.AccommodationString)).ToList());

                    */
            }
            else if (actionName.Equals("FetchBooking", StringComparison.InvariantCultureIgnoreCase))
            {
                parameters.Add(new KeyValueParam("Search Type", actionName));
                parameters.Add(new KeyValueParam("Membership Id", txtBookingNumber.Value));
                parameters.Add(new KeyValueParam("Last Name", txtSurname.Value));

            }
            /*
            counter = default(int);
            parameters.AddRange((from room in searchEntity.ListRooms
                                 from childer in room.ChildrenDetails.ListChildren
                                 select new KeyValueParam(string.Format("Age of Child{0} of Room{1}", counter++), 
                                            room.ChildrenOccupancyPerRoom.ToString())).ToList());

            /*
            if (listRooms != null && listRooms.Count > 0)
            {
                for (int i = 0; i < listRooms.Count; i++)
                {
                    parameters.Add(new KeyValueParamFormat("Adults in Room{0}: {1}{2}", i + 1, listRooms[i].AdultsPerRoom.ToString(),
                                    Environment.NewLine);
                    parameters.Add(new KeyValueParamFormat("Children in Room{0}: {1}{2}", i + 1, listRooms[i].ChildrenPerRoom.ToString(),
                                    Environment.NewLine);
                    parameters.Add(new KeyValueParamFormat("Children Occupancy in Room{0}: {1}{2}", i + 1,
                                    listRooms[i].ChildrenOccupancyPerRoom.ToString(), Environment.NewLine);

                    if (listRooms[i].ChildrenDetails != null)
                    {
                        parameters.Add(new KeyValueParamFormat("Adults per room in Children Details of Room{0}: {1}{2}", i + 1,
                                        listRooms[i].ChildrenDetails.AdultsPerRoom.ToString(), Environment.NewLine);
                        parameters.Add(new KeyValueParamFormat("Children details text in Children Details of Room{0}: {1}{2}", i + 1,
                                        listRooms[i].ChildrenDetails.ChildrensDetailsInText, Environment.NewLine);

                        if (listRooms[i].ChildrenDetails.ListChildren != null &&
                            listRooms[i].ChildrenDetails.ListChildren.Count > 0)
                        {
                            for (int j = 0; j < listRooms[i].ChildrenDetails.ListChildren.Count; j++)
                            {
                                parameters.Add(new KeyValueParamFormat("Age of Child{0} of Room{1}: {2}{3}", j + 1, i + 1,
                                                listRooms[i].ChildrenDetails.ListChildren[j].Age.ToString(),
                                                Environment.NewLine);
                                parameters.Add(new KeyValueParamFormat("Bed type of Child{0} of Room{1}: {2}{3}", j + 1, i + 1,
                                                listRooms[i].ChildrenDetails.ListChildren[j].ChildAccommodationType.
                                                    ToString(), Environment.NewLine);
                                parameters.Add(new KeyValueParamFormat("Accomodation string of Child{0} of Room{1}: {2}{3}", j + 1, i + 1,
                                                listRooms[i].ChildrenDetails.ListChildren[j].AccommodationString,
                                                Environment.NewLine);
                            }
                        }
                    }
                }
            }
            
            */

            return parameters;
        }

        #endregion
    }
}