<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EnrollGuestProgram.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.EnrollGuestProgram" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Controller" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.Entity" %>
<div id="Loyalty" class="BE signupformFGP">
    <!-- Enroll Loyalty -->
    <div id="EnrollLoyalty" onkeypress="return WebForm_FireDefaultButton(event, '<%= BtnSend.ClientID %>')">
        <div id="GuestInfo" class="formGroup">
            <% string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();%>
            <!-- Error -->
            <div id="clientErrorDivEL" class="errorText" runat="server">
            </div>
            <div class="errorDivClient">
                <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
                <input type="hidden" id="invalidFirstName" name="invalidFirstName" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/first_name") %>' />
                <input type="hidden" id="invalidLastName" name="invalidLastName" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/last_name") %>' />
                <input type="hidden" id="invalidaddressLine1" name="invalidaddressLine1" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/address") %>' />
                <input type="hidden" id="invalidaddressLine2" name="invalidaddressLine2" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/address") %>' />
                <input type="hidden" id="invalidCityTown" name="invalidCityTown" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/city_town_information") %>' />
                <input type="hidden" id="invalidPostcode" name="invalidPostcode" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/postal_code") %>' />
                <input type="hidden" id="invalidTelephone1" name="invalidTelephone1" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/landline_telephone") %>' />
                <input type="hidden" id="invalidTelephone2" name="invalidTelephone2" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/mobile_telephone") %>' />
                <input type="hidden" id="invalidMobile" name="invalidMobile" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/valid_mobile_number") %>' />
                <input type="hidden" id="invalidEmail" name="invalidEmail" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/email_address") %>' />
                <input type="hidden" id="invalidPartnerProgram" name="invalidPartnerProgram" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/partner_program") %>' />
                <input type="hidden" id="invalidAccountNo" name="invalidAccountNo" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/account_no") %>' />
                <input type="hidden" id="invalidCardHolder" name="invalidCardHolder" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/card_holder_name") %>' />
                <input type="hidden" id="invalidCountryCode" name="invalidCountryCode" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalid_Country_Code") %>' />
                <input type="hidden" id="invalidCardNumber" name="invalidCardNumber" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_credit_card_number") %>' />
                <input type="hidden" id="invalidCardType" name="invalidCardType" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_credit_card_type") %>' />
                <input type="hidden" id="invalidExpiryDate" name="invalidExpiryDate" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/expiry_date") %>' />
                <input type="hidden" id="invalidHomePhoneCode" name="invalidHomePhoneCode" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_Home_Phone_Code") %>' />
                <input type="hidden" id="invalidMobilePhoneCode" name="invalidMobilePhoneCode" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_Mobile_Phone_Code") %>' />
                <input type="hidden" id="invalidPassword" name="invalidPassword" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/password") %>' />
                <input type="hidden" id="invalidRetypePassword" name="invalidRetypePassword" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/retype_password") %>' />
                <input type="hidden" id="invalidPasswordMatch" name="invalidPasswordMatch" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/not_same_password") %>' />
                <input type="hidden" id="invalidPasswordDollarSign" name="invalidPasswordDollarSign"
                    value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/dollarsign_not_allowed_in_password") %>' />
                <!--Validate for nordic characters Artifact artf1072335 : Scanweb - workaround for double byte chars in password and answer fields -->
                <input type="hidden" id="invalidPasswordNordicChar" name="invalidPasswordNordicChar"
                    value="<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/nordicchar_not_allowed_in_password") %>" />
                <input type="hidden" id="invalidSecretAnswerNordicChar" name="invalidSecretAnswerNordicChar"
                    value="<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/nordicchar_not_allowed_in_secretanswer") %>" />
                <input type="hidden" id="invalidDateofBirth" name="invalidDateofBirth" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/dateofbirth") %>' />
                <input id="invalidSecurityAnswer" name="invalidSecurityAnswer" type="hidden" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_security_answer") %>' />
                <!-- Defect Fix - Artifact artf652899(R1.3) -->
                <input id="invalidSecurityQuestion" name="invalidSecurityQuestion" type="hidden"
                    value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_security_question") %>' />
                <input type="hidden" id="invalidAgeConfirmation" name="invalidAgeConfirmation" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_age_confirmation") %>' />
                <input type="hidden" id="invalidTermsConfirmation" name="invalidTermsConfirmation"
                    value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_terms_confirmation_enroll") %>' />
                <!--Release 1.5 | artf809466 | FGP � Fast Track Enrolment-->
                <input type="hidden" id="invalidCampaignCode" name="invalidCampaignCode" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_campaign_code") %>' />
                <input type="hidden" id="addressID" name="addressID" runat="server" />
                 <!-- <input type="hidden" value="Select an option" name="CheckboxMultiple" id="CheckboxMultiple">
                <input type="hidden" value="Sorry, no special characters are allowed" name="restrictSplChars"
                    id="restrictSplChars">-->
                     <input type="hidden" id="restrictSplChars" name="restrictSplChars" value="<%=WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/noSpecialCaracters")%>" />
                <!-- START:Release 1.5 | artf809605 | FGP - add more than one partner preference -->
                <input type="hidden" id="partnerProgramValues" name="partnerProgramValues" runat="server" />
                <input type="hidden" id="selectedPartnerProgramValues" name="selectedPartnerProgramValues"
                    runat="server" />
                <input type="hidden" id="uniquePartnerProg" name="uniquePartnerProg" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/uniquePartnerProg") %>' />
                <input type="hidden" id="choosePreferred" name="choosePreferred" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/choosePreferred") %>' />
                <input type="hidden" id="prefPartnerText" name="prefPartnerText" value='<%=
                WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/preferredPartnerProgramme") %>' />
                <input type="hidden" id="optPartnerText" name="optPartnerText" value='<%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/optionalPartnerProgram") %>' />
                <input type="hidden" id="prefPartnerCount" name="prefPartnerCount" runat="server" />
                <!-- END:Release 1.5 | artf809605 | FGP - add more than one partner preference -->
                <input type="hidden" id="hdnCampaignCode" runat="server" />
            </div>
            <!-- Error -->
            <!-- header -->
            <!-- Start here -->
            <% if (FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.EnrolmentThroughCampaignLandingPage)
           {%>
            <div id="campaignCodeDiv" class="formGroup" runat="server">
                <!-- header -->
                <div class="helpLink">
                    <h2>
                        <span class="link"><a href="#" onclick="openPopupWin(NeedHelpUrl,'width=550,height=420,scrollbars=yes,resizable=yes'); return false;">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/needhelp") %></a>
                        </span>
                    </h2>
                </div>
                <div class="darkHeading">
                    <div class="columnOne">
                        <h2>
                            <span>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/campaignLanding/campaignHeading") %></span></h2>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <!-- /header -->
                <!-- details -->
                <p class="formRow">
                    <span id="spanCampaignCode">
                        <label for="campainCode">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/campaignLanding/campaignCode") %></label>
                    </span>
                    <br />
                    <input id="txtCampaignCode" class="frmInputText" type="text" maxlength="20" name="txtCampaignCode"
                        runat="server" />
                </p>
                <!-- details -->
                <div class="clear">
                </div>
            </div>
            <!-- END here -->
            <p class="signupformHeaders">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/contactInformation") %>
            </p>
            <%--<div class="Addr"><h2>
        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/address") %></h2></div>--%>
            <% }
           else
           { %>
            <div class="helpLink">
                <h2>
                    <span class="link"><a href="#" onclick="openPopupWin(NeedHelpUrl,'width=420,height=550,scrollbars=yes,resizable=yes'); return false;">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/needhelp") %>
                    </a></span>
                </h2>
            </div>
            <p class="signupformHeaders">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/contactInformation") %></p>
            <%--            <% if (siteLanguage == "FI")
               { %>		
                <div class="Addr"><h2><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/address") %></h2></div> 
                <!--div class="helpLink"><h2><span class="link"><a href="#" onclick="OpenPopUpwindow(NeedHelpUrl,'200','580'); return false;"><=WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/needhelp")%></a></span></h2></div-->
            <% }
               else
               { %>  
                <div class="Addr"><h2>
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/address") %></h2></div> 
                <!--div class="helpLink"> 
                <h2><span class="link">
                        <a href="#" onclick="OpenPopUpwindow(NeedHelpUrl,'200','580'); return false;">                        
                        <=WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/needhelp")%>
                        </a>
                </span></h2> 
                </div-->
            <% } %>--%>
            <% } %>
            <!-- /header -->
            <!-- details -->
            <div class="columnOne mrgTop19">
                <div class="mendatoryFields">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/MandatoryFieldsInfoMessage") %></div>
                <p class="formRow">
                    <span id="lblFname">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/firstname") %></label>
                    </span>
                    <br />
                    <input type="text" id="txtFName" name="txtFName" class="frmInputText" runat="server"
                        maxlength="30" tabindex="311" />
                </p>
                <p class="formRow">
                    <span id="lblLname">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/lastname") %></label>
                    </span>
                    <br />
                    <input type="text" name="txtLName" class="frmInputText" id="txtLName" runat="server"
                        maxlength="30" tabindex="312" />
                </p>
                <p class="formRow">
                    <span id="lblAddressLine1">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/addressline1") %></label>
                    </span>
                    <br />
                    <input type="text" name="txtAddressLine1" class="frmInputText" id="txtAddressLine1"
                        runat="server" maxlength="80" tabindex="313" />
                </p>
                <p class="formRow">
                    <span id="lblAddressLine2">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/addressline2") %></label>
                    </span>
                    <br />
                    <input type="text" name="txtAddressLine2" class="frmInputText" id="txtAddressLine2"
                        runat="server" maxlength="80" tabindex="314" />
                </p>
                <p class="formRow">
                    <span id="lblCityTown">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/towncity") %></label>
                    </span>
                    <br />
                    <input type="text" name="txtCityOrTown" class="frmInputText" id="txtCityOrTown" runat="server"
                        maxlength="40" tabindex="315" />
                </p>
                <p class="formRow">
                    <span id="lblPostcode">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/postcode") %></label>
                    </span>
                    <br />
                    <input type="text" name="txtPostCode" class="frmInputText" id="txtPostCode" runat="server"
                        maxlength="9" tabindex="316" />
                </p>
                <p class="formRow">
                    <span id="lblCountry">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/country") %></label>
                    </span>
                    <br />
                    <asp:DropDownList CssClass="frmSelectBig" ID="ddlCountry" runat="server" TabIndex="317">
                    </asp:DropDownList>
                </p>
                <p class="formRow">
                    <span id="lblEmail">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/email") %></label>
                    </span>
                    <br />
                    <input type="text" name="txtEmail1" class="frmInputText" id="txtEmail1" runat="server"
                        tabindex="318" maxlength="200" />
                </p>
                <p class="formRow">
                    <span id="lblMobilePhone">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/mobilephone") %></label>
                    </span>
                    <br />
                    <asp:DropDownList CssClass="frmSelect" ID="ddlTel2" runat="server" TabIndex="319">
                    </asp:DropDownList>
                    <input type="text" name="txtTelephone2" class="frmInputTextSmall" id="txtTelephone2"
                        runat="server" maxlength="20" tabindex="320" />
                </p>
                <p class="formRow radioBtnParagraph">
                    <label>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/gender") %></label>
                    <br />
                    <span class="radioBtnHolder">
                        <label class="secondaryLabel">
                            <input name="Gender" type="radio" class="frmInputRadio" checked="true" id="rdoMale"
                                runat="server" tabindex="321" />
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/male") %></label>
                        <label class="secondaryLabel">
                            <input name="Gender" type="radio" class="frmInputRadio" id="rdoFemale" runat="server"
                                tabindex="322" />
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/female") %></label></span><br
                                class="clear" />
                </p>
                <p class="formRow selectFieldParagraph">
                    <span id="lblDateOfBirth">
                        <label for="Day">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/dateofbirth") %></label>
                    </span>
                    <br />
                    <span class="selDay">
                        <asp:DropDownList ID="ddlDOBDay" runat="server" CssClass="frmSelect" TabIndex="323">
                        </asp:DropDownList>
                    </span><span class="selMonth">
                        <asp:DropDownList ID="ddlDOBMonth" runat="server" CssClass="frmSelect" TabIndex="324">
                        </asp:DropDownList>
                    </span><span class="selYear">
                        <asp:DropDownList ID="ddlDOBYear" runat="server" CssClass="frmSelect" TabIndex="325">
                        </asp:DropDownList>
                    </span>
                    <br class="clear" />
                </p>
                <p class="formRow">
                    <span class=fltLft">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/prefferedlanguage") %>
                        </label>
                         <br />
                         <asp:DropDownList ID="ddlPreferredLang"  onchange="ShowOrHideTitle(this);" runat="server" CssClass="frmSelectBig prefLang"
                            TabIndex="330">
                        </asp:DropDownList>
                     </span>
                    <span class="fltLft">
                        <label id="lblNameTitle" runat="server">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nametitle") %>
                        </label>
                           <br />                 <!-- title field add only for German Language !-->
                        <span id="dvNameTitle" runat="server" class="enrlDvNameTitle">
                            <asp:DropDownList CssClass="frmTitleSelect" ID="ddlTitle" runat="server" />
                        </span>
                    </span>
                    <br class="clear" />
                </p>
                
                
                <div class="clear">
                    &nbsp;</div>
                <!--<p class="formRow">
            <span id="lblEnrollLandline1">
                <label><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/landline") %></label>
            </span>
            <br />
            <asp:DropDownList CssClass="frmSelect" ID="ddlTel11" runat="server" tabindex="311">
			</asp:DropDownList>
			<input type="text"	name="txtTelephone11" class="frmInputTextSmall" id="txtTelephone11" runat="server" maxlength="20" tabindex="312"/>
          </p>-->
            </div>
            <div class="columnTwo">
            </div>
            <div class="clear">
                &nbsp;</div>
            <!-- /details -->
        </div>
        <p class="signupformHeaders">
            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/passwordheader") %></p>
        <div class="columnOne mrgTop19">
            <p class="formRow">
                <span id="lblPassword">
                    <label>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/password") %>*</label>
                </span>
                <br />
                <input type="password" name="txtEnrollPassword" class="frmInputText" id="txtEnrollPassword"
                    runat="server" maxlength="20" tabindex="326" />
            </p>
            <p class="formRow">
                <span id="lblRetypePassword">
                    <label>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/retypepassword") %>*</label>
                </span>
                <br />
                <input type="password" name="txtEnrollReTypePassword" class="frmInputText" id="txtEnrollReTypePassword"
                    runat="server" maxlength="20" tabindex="327" />
            </p>
        </div>
        <div class="clear">
            &nbsp;</div>
        <!-- Comments -->
        <div id="Terms" class="formGroup mrgTop19">
            <!-- header -->
            <p class="signupformHeaders">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/termscondition") %>
            </p>
            <!-- /header -->
            <!-- details -->
             <div class="mrgTop19" id="tandc">
              <p class="formRow">
                <input id="chkReceiveScandicInfo" name="chkReceiveScandicInfo" type="checkbox" class="frmCheckBox" value="" runat="server" tabindex="328"/>
                <label class="frmCheckBox signupTandC">                
                                <span>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/ireadterms") %>
                                      <a href="#" onclick="openPopupWin('<%= GlobalUtil.GetUrlToPage(EpiServerPageConstants.ENROLL_TERMS_AND_CONDITION) %>','width=800,height=600,scrollbars=yes,resizable=yes'); return false;">
                                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/receivescandicinfolatest") %>
                                      </a>
								        <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/receivesscandicinfolatestmore") %>
                                </span>                           
                             </label>
                <br class="clear" />
              </p>
              <p class="formRow">
                <input id="chkReceivePartnersInfo" name="chkReceivePartnersInfo" type="checkbox" class="frmCheckBox" value="" runat="server" tabindex="329"/>
                
                <label class="frmCheckBox signupTandC"><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/receivepartnersinfo") %></label>
               
                <br class="clear" />
              </p>
        </div>
            <%-- <div class="columnTwo">
          <p class="formRow"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/pleasecheck") %></span></p>
          <p class="formRow">
            <input name="chkAgeconfirmation" type="checkbox" class="frmCheckBox" value="" id="chkAgeconfirmation" tabindex="330"/>
            <span id="lblAgeconfirmation">
                <label class="frmCheckBox"><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/confirmage") %>
                </label>
            </span>
            <br class="clear" />
            
          </p>
          <p class="formRow">
            <input name="chkTermsconfirmation" type="checkbox" class="frmCheckBox" value="" id="chkTermsconfirmation" tabindex="331"/>
            <span id="lblTermsconfirmation">
                <label class="frmCheckBox"><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/ireadterms") %><a href="#" tabindex="332" onclick="openPopupWin('<%= GlobalUtil.GetUrlToPage(EpiServerPageConstants.ENROLL_TERMS_AND_CONDITION) %>','width=800,height=600,scrollbars=yes,resizable=yes'); return false;">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/termscondition") %>
                </a>
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/andagree") %>
                </label>
            </span>
            <br class="clear" />
          </p>
        </div>--%>
            <!-- /details -->
            <div class="clear">
                &nbsp;</div>
        </div>
        <!-- Comments -->
        <div class="clear">
            &nbsp;</div>
        <!-- /loyaltyEnroll -->
    </div>
    <div class="clear">
        &nbsp;</div>
    <!-- /Loyalty -->
</div>
<!-- Footer -->
<div id="FooterContainer" class="mrgTop19">
    <div class="buttonContainer alignRight" id="SendButtonDiv">
        <div class="actionBtn fltRt">
            <%--<asp:LinkButton ID="BtnSend" tabindex="375" runat="server" OnClick="BtnSend_Click" class="buttonInner"><%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/send") %></asp:LinkButton>
						<asp:LinkButton ID="spnSend" tabindex="375" runat="server" OnClick="BtnSend_Click" class="buttonRt scansprite"></asp:LinkButton>--%>
            <asp:Button ID="BtnSend" runat="server" TabIndex="333" OnClick="BtnSend_Click" class="buttonInner brandSearchBtn scansprite" />
        </div>
    </div>
    <!--R1.7.3 [Opera V5 patch upgrade] | artf1056125
            : the div is added to hide the send button so that the user can not make multiple registrations. -->
    <div class="booking-progress" id="SendingProgressDiv" runat="server" style="display: none">
        <img src="<%= ResolveUrl("~/Templates/Booking/Styles/Default/Images/rotatingclockani.gif") %>"
            align="middle" width="25px" height="25px" />
        <span>
            <%=
                WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/CreateProfileInProgressMessage") %></span>
    </div>
    <div class="clear">
        &nbsp;</div>
</div>
<!-- Footer -->

<script type="text/javascript" language="javascript">
  
     //R1.7.3 [Opera V5 patch upgrade] | artf1056125 : the variable is added to hide the send button so that the user can not make multiple registrations. 
    var isSendClicked = false;
    
    $fn(_endsWith("SendButtonDiv")).style.display = "block";
    $fn(_endsWith("SendingProgressDiv")).style.display = "none";    
    
    var NeedHelpUrl = <%= "\"" + GlobalUtil.GetUrlToPage("JoinScandicFriendsHelpPage") + "\"" %>;
    //var TermsAndConditionUrl = <%= "\"" + GlobalUtil.GetUrlToPage("EnrollTermsAndConditionsPage") + "\"" %>;
    //initEL();    
    //START:Release 1.5 | artf809605 | FGP - add more than one partner preference
    var optionString = "";
    createOptionString();
    function createOptionString()
    {
        var originalArray = $fn(_endsWith("partnerProgramValues")).value.split("%");	    
	    for(var partnerCount=0;partnerCount<originalArray.length-1;partnerCount++)
	    {
	        var val = originalArray[partnerCount].split(",");
	        optionString += '<option value ="' + val[0] +'">' + val[1] + '</option>';	        
	    }
    }

    function pushEntityToHiddenFieldAfterDel(selectCtrl, id)
    {
	    var textCtrl = document.getElementsByName("memberNo");
	    var val="";
	    for(var count=0;count<selectCtrl.length;count++)
	    {
	        if(count != id)
	        {
		        val += selectCtrl[count].options[selectCtrl[count].selectedIndex].value+','+ textCtrl[count].value +'%';
		    }
	    }
	    $fn(_endsWith("selectedPartnerProgramValues")).value = val;
    }
	
	//END:Release 1.5 | artf809605 | FGP - add more than one partner preference
</script>

<script type="text/javascript" src="/Templates/Booking/Javascript/EnrollValidation.js?v=<%=CmsUtil.GetJSVersion()%>"></script>

