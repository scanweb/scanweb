//  Description					: Code Behind class for BookingSearch  Control			  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Shankar Dasgupta                                   	  //
/// Author email id				:                           							  //
/// Creation Date				: 07th December  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System NameSpace

using System;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;

#endregion

    #region Scandic Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for BookingSearch
    /// </summary>
    public partial class BookingSearch : EPiServer.UserControlBase
    {
        /// <summary>
        /// Event Handler for Search Button click
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="args">
        /// Arguments for the Event
        /// </param>
        public void BtnSearch_Click(object sender, EventArgs args)
        {
            try
            {
                Utility.CleanSession();
                BookingEngineSessionWrapper.IsModifyBooking = true;
                string bookingNumber = txtBookingNumber.Value.Trim();
                string surName = txtSurname.Value.Trim();
                ReservationController reservationController = new ReservationController();
                List<BookingDetailsEntity> bookingEntities = null;
                if (IsMasterConfirmationNumber(bookingNumber))
                {
                    bookingEntities = reservationController.FetchBookingSummary(bookingNumber);
                    BookingEngineSessionWrapper.IsModifyingLegBooking = false;
                }
                else
                {
                    string legNumber = string.Empty;
                    bookingNumber = SplitBookingAndLegNumber(bookingNumber, out legNumber);
                    BookingDetailsEntity bookingEntity = reservationController.FetchBooking(bookingNumber, legNumber);
                    if (bookingEntity != null)
                    {
                        bookingEntities = new List<BookingDetailsEntity>();
                        bookingEntities.Add(bookingEntity);
                        BookingEngineSessionWrapper.IsModifyingLegBooking = true;
                    }
                }

                if (bookingEntities != null && bookingEntities.Count > 0)
                {
                    if (!CheckName(bookingEntities, surName.ToLower()))
                    {
                        clientErrorDivBD.InnerHtml =
                            WebUtil.GetTranslatedText(TranslatedTextConstansts.INVALID_SURNAME);
                    }
                    else
                    {
                        BookingEngineSessionWrapper.AllBookingDetails = bookingEntities;
                        BookingEngineSessionWrapper.BookingDetails = bookingEntities[0];
                        ReservationNumberSessionWrapper.ReservationNumber = bookingNumber;
                        if (Utility.IsBlockCodeBooking)
                        {
                            Utility.OverrideCancelableDateIfBlockCode(bookingEntities);
                        }
                        BookingEngineSessionWrapper.PreviousPageURL =
                            GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH);
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_CHANGE_DATES),
                                          false);
                    }
                }
                else
                {
                    Scanweb.Core.AppLogger.LogInfoMessage
                        (string.Format
                             ("No Booking Details information is found against the specified {0} booking confirmation number",
                              bookingNumber));
                    WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                                 WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                }
            }
            catch (OWSException owsException)
            {
                Scanweb.Core.AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);
                switch (owsException.ErrCode)
                {
                    case OWSExceptionConstants.BOOKING_NOT_FOUND:
                    case OWSExceptionConstants.SEE_CONTACT_DETAILS_ON_CHAIN_INFO:
                        {
                            clientErrorDivBD.InnerHtml =
                                WebUtil.GetTranslatedText(TranslatedTextConstansts.INVALID_BOOKING_NUMBER);
                            break;
                        }
                    default:
                        {
                            clientErrorDivBD.InnerHtml = owsException.Message;
                            break;
                        }
                }
                string exceptionMessage = WebUtil.GetTranslatedText(owsException.TranslatePath);
                if (owsException.Message == AppConstants.SYSTEM_ERROR)
                {
                    WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                                 WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                }
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }
        }

        #region Protected Methods

        /// <summary>
        /// Page Load Method of BookingSearch Control
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="e">
        /// Arguments for the Event
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BtnSearch.Attributes.Add("onclick", "javascript:return performValidation(PAGE_BOOKING_SEARCH);");
            }
        }

        #endregion Protected Methods

        #region Private Methods

        /// <summary>
        /// Checks wheather this is master booking number or individual booking number.
        /// </summary>
        /// <example>
        /// 12345-1 is considered as individual booking number where as
        /// 12345(with out -) is considered as master booking number.
        /// </example>
        /// <param name="confirmationNumber">
        /// The confirmation number for which bookig details to be retrieved.
        /// </param>
        /// <returns>
        /// True - if the confirmation number is master booking.
        /// False - if the confirmation number is individual booking.
        /// </returns>
        private bool IsMasterConfirmationNumber(string confirmationNumber)
        {
            bool status = false;

            if (!confirmationNumber.Contains(AppConstants.HYPHEN))
            {
                status = true;
            }
            return status;
        }

        /// <summary>
        /// This method will cross verify the booking number and last name with all the booking available.
        /// If collection of booking contains the last name given by user then returns true else false.
        /// </summary>
        /// <param name="bookingDetailsEntity">Collection of booking.</param>
        /// <param name="lastName">Last name</param>
        /// <returns>
        /// True or False.
        /// </returns>
        private bool CheckName(List<BookingDetailsEntity> bookingDetailsEntity, string lastName)
        {
            bool doesContainName = false;
            int bookingEntityLength = bookingDetailsEntity.Count;
            for (int counter = 0; counter < bookingEntityLength; counter++)
            {
                if ((bookingDetailsEntity[counter].GuestInformation != null)
                    && (lastName == bookingDetailsEntity[counter].GuestInformation.LastName.ToLower()))
                {
                    doesContainName = true;
                    break;
                }
            }
            return doesContainName;
        }

        /// <summary>
        /// Split the Booking Number into MasterBooking Number and Leg-Number
        /// </summary>
        /// <param name="bookingNumber">BookingNumber to be split</param>
        /// <param name="legNumber">out LegNumber</param>
        /// <returns>Booking Number</returns>
        private string SplitBookingAndLegNumber(string bookingNumber, out string legNumber)
        {
            string[] splitedString = new string[2];
            splitedString = bookingNumber.Split(char.Parse(AppConstants.HYPHEN));
            bookingNumber = splitedString[0].Trim();
            legNumber = splitedString[1].Trim();

            return bookingNumber;
        }

        #endregion Private Methods
    }
}