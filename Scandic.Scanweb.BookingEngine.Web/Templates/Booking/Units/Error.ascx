<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Error.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.Error" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<div id="ErrorPage" class="BE">
	<div class="formGroupError">
		<div class="redAlertIcon"><%= WebUtil.GetTranslatedText("/bookingengine/booking/errorpage/apperrhead") %></div>
		<div class="errorMsg">
		    <div id = "errMsgContent" runat="server"></div>
			</div>
		<div class="columnOne">
			<p class="link">
				<a href="javascript:history.go(-1)"><%= WebUtil.GetTranslatedText("/bookingengine/booking/errorpage/previousscreen") %></a>
			</p>
			<div class="hr">&nbsp;</div>
			<p class="link">
				<a href="/."><%= WebUtil.GetTranslatedText("/bookingengine/booking/errorpage/redirecttohome") %></a>
			</p>
		</div>
	</div>
</div>
    