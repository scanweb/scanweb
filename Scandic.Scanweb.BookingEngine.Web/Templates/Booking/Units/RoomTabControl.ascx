<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RoomTabControl.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.RoomTabControl" %>

<div id="tabWrapperContainer" runat="server">
    <div class="scandicTabs scandicTabsDouble" id="tabWrapper">
        <ul class="tabsCnt" runat="server" id="roomListContainer">
</ul>
</div>
</div>
	