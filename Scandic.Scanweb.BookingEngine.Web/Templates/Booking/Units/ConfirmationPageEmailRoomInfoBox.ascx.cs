using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Confirmation Page Email Room Info Box
    /// </summary>
    public partial class ConfirmationPageEmailRoomInfoBox : System.Web.UI.UserControl
    {
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("ConfirmationPageEmailRoomInfoBox:Page_Load() Begin");
            setImageSource();
            SetFieldsInEmailRoomInfoBox();
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("ConfirmationPageEmailRoomInfoBox:Page_Load() End");
        }

        #region Private Methods

        /// <summary>
        /// set Image Source
        /// </summary>
        private void setImageSource()
        {
            string authority = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            singleBorderBoxTop.ImageUrl = authority + ResolveUrl
                                                          ("/templates/Scanweb/Images/Images_Email/singleBrdrBox_top.gif");
            singleBorderBoxBottom.ImageUrl = authority + ResolveUrl
                                                             ("/templates/Scanweb/Images/Images_Email/singleBrdrBox_btm.gif");
        }

        /// <summary>
        /// Sets the fields in email room info box.
        /// </summary>
        private void SetFieldsInEmailRoomInfoBox()
        {
            if (!string.IsNullOrEmpty(dNumberValue))
            {
                dNumberSpan.InnerHtml = dNumberValue;
                dNumberTR.Visible = true;
            }
            else
            {
                dNumberTR.Visible = false;
            }

            reservationNumber.InnerHtml = propreservationNumber;
            reservationNumberHeader.InnerHtml = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingReservationNumber") + ": ";
            roomTypeName.InnerHtml = propRoomTypeName;
            rateTypeID.InnerHtml = Utility.FormatFieldsToDisplay(propRateTypeID, 20);
            priceInfo.InnerHtml = propPriceInfo;

            rateString.InnerHtml = propRateString;
            adultAndChildrenDetail.InnerHtml = propAdultAndChildrenDetail;
            adultBedType.InnerHtml = propAdultBedType;
            if (propChildrenBedTypeDetails != string.Empty)
                childBedType.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildBedType");
            else
                childBedType.InnerText = string.Empty;

            childernBedType.InnerHtml = propChildrenBedTypeDetails;
            if (!string.IsNullOrEmpty(propOtherPreferences))
            {
                otherPreferences.InnerHtml = propOtherPreferences;
                otherPreferencesRow.Visible = true;
            }
            else
            {
                otherPreferencesRow.Visible = false;
            }
            if (!string.IsNullOrEmpty(propSpecialServiceRequest))
            {
                specialServiceRequest.InnerHtml = propSpecialServiceRequest;
                otherSpecialServiceRow.Visible = true;
            }
            else
            {
                otherSpecialServiceRow.Visible = false;
            }
            nameOfPerson.InnerHtml = propName;

            //nameOfCityTown.InnerHtml = propCityOrTown;
            nameOfCountry.InnerHtml = propCountry;
            emailAddress.InnerHtml = propEmailAddress;
            phoneNumber.InnerHtml = propPhoneNumber;
            hotelName.InnerHtml = propHotelName;
            checkInDate.InnerHtml = propArrivalDate;
            checkOutDate.InnerHtml = propDepartureDate;
            if (!string.IsNullOrEmpty(propCancellationNumber))
            {
                cancellationNumberHeader.InnerHtml = WebUtil.GetTranslatedText
                                                         ("/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                     ": ";
                cancellationNumber.InnerHtml = propCancellationNumber;
            }

            if (!string.IsNullOrEmpty(propFGPNumber))
            {
                lblFGPNumber.InnerHtml = propFGPNumber;
                divFGPNumber.Attributes["style"] = "display : block;";
            }

            if ((!string.IsNullOrEmpty(propCreditCardNumber)) && (!string.IsNullOrEmpty(propExpiryDate))
                 && (!string.IsNullOrEmpty(propCreditCardType)))
            {
                if (SearchCriteriaSessionWrapper.SearchCriteria != null &&
                    SearchCriteriaSessionWrapper.SearchCriteria.SearchingType != SearchType.REDEMPTION)
                {
                    divCreditCardNum.Visible = true;
                    divExpiryDate.Visible = true;
                    divCardType.Visible = true;

                    lblCreditCardNumber.InnerHtml = propCreditCardNumber;
                    lblExpiryDate.InnerHtml = propExpiryDate;
                    lblCardType.InnerHtml = propCreditCardType;
                }

                if (!string.IsNullOrEmpty(propCreditCardHolder))
                {
                    divCardHolderName.Visible = true;
                    lblCardHolderName.InnerHtml = propCreditCardHolder;
                }
            }
            if (propShowGuaranteeForEachRoom)
            {
                GuaranteeInfoHeader.Text = GuaranteeTextHeader;
                liGuaranteeInformation.InnerHtml = propGuaranteeInformation;
                DifferentGuaranteeInformationText.Visible = true;
            }
            if (!showRoomTypeRow)
                roomTypeRow.Visible = false;
            if (!showAdultAndChildInfoTable)
                adultAndChildInfoTable.Visible = false;
            if (!showContactInfoTable)
                contactInfoTable.Visible = false;

            if (!string.IsNullOrEmpty(propAdditionalRequest))
            {
                addReq.InnerHtml = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/AdditionalReq");
                AditionalServiceRow.Visible = true;
            }
            else
            {
                addReq.InnerHtml = string.Empty;
                AditionalServiceRow.Visible = false;
            }

            additionalRequest.InnerHtml = propAdditionalRequest;
            deeplinkPerRoom.Visible = ShowDeepLinkForOtherRooms;
            ancViewModifyDLRedirectLink.InnerHtml = deepLinkUrl;
            ancViewModifyDLRedirectLink.HRef = "http://" + deepLinkUrl;

        }

        #endregion

        #region Properties

        private string roomIdentifier = string.Empty;

        /// <summary>
        /// Gets or sets the room identifier.
        /// </summary>
        /// <value>The room identifier.</value>
        public string RoomIdentifier
        {
            get { return roomIdentifier; }
            set { roomIdentifier = value; }
        }

        private string propHotelName = string.Empty;

        /// <summary>
        /// Gets or sets the name of the hotel.
        /// </summary>
        /// <value>The name of the hotel.</value>
        public string HotelName
        {
            get { return propHotelName; }
            set { propHotelName = value; }
        }

        private string propArrivalDate = string.Empty;

        /// <summary>
        /// Gets or sets the arrival date.
        /// </summary>
        /// <value>The arrival date.</value>
        public string ArrivalDate
        {
            get { return propArrivalDate; }
            set { propArrivalDate = value; }
        }

        private string propDepartureDate = string.Empty;

        /// <summary>
        /// Gets or sets the departur date.
        /// </summary>
        /// <value>The departur date.</value>
        public string DeparturDate
        {
            get { return propDepartureDate; }
            set { propDepartureDate = value; }
        }

        private string propreservationNumber = string.Empty;

        /// <summary>
        /// Gets or sets the reservation number.
        /// </summary>
        /// <value>The reservation number.</value>
        public string ReservationNumber
        {
            get { return propreservationNumber; }
            set { propreservationNumber = value; }
        }

        private string propRoomTypeName = string.Empty;

        /// <summary>
        /// Gets or sets the name of the room type.
        /// </summary>
        /// <value>The name of the room type.</value>
        public string RoomTypeName
        {
            get { return propRoomTypeName; }
            set { propRoomTypeName = value; }
        }

        private string dNumberValue = string.Empty;

        public string DNumberValue
        {
            get { return dNumberValue; }
            set { dNumberValue = value; }
        }

        private string propRateTypeID = string.Empty;

        /// <summary>
        /// Gets or sets the rate type ID.
        /// </summary>
        /// <value>The rate type ID.</value>
        public string RateTypeID
        {
            get { return propRateTypeID; }
            set { propRateTypeID = value; }
        }

        private string propPriceInfo = string.Empty;

        /// <summary>
        /// Gets or sets the price info.
        /// </summary>
        /// <value>The price info.</value>
        public string PriceInfo
        {
            get { return propPriceInfo; }
            set { propPriceInfo = value; }
        }

        private string propRateString = string.Empty;

        /// <summary>
        /// Gets or sets the rate string.
        /// </summary>
        /// <value>The rate string.</value>
        public string RateString
        {
            get { return propRateString; }
            set { propRateString = value; }
        }

        private string propAdultAndChildrenDetail = string.Empty;

        /// <summary>
        /// Gets or sets the adult and childern detail.
        /// </summary>
        /// <value>The adult and childern detail.</value>
        public string AdultAndChildernDetail
        {
            get { return propAdultAndChildrenDetail; }
            set { propAdultAndChildrenDetail = value; }
        }

        private string propAdultBedType = string.Empty;

        /// <summary>
        /// Gets or sets the type of the adult bed.
        /// </summary>
        /// <value>The type of the adult bed.</value>
        public string AdultBedType
        {
            get { return propAdultBedType; }
            set { propAdultBedType = value; }
        }

        private string propChildrenBedTypeDetails = string.Empty;

        /// <summary>
        /// Gets or sets the childern bed type details.
        /// </summary>
        /// <value>The childern bed type details.</value>
        public string ChildernBedTypeDetails
        {
            get { return propChildrenBedTypeDetails; }
            set { propChildrenBedTypeDetails = value; }
        }

        private string propOtherPreferences = string.Empty;

        /// <summary>
        /// Gets or sets the other preferences.
        /// </summary>
        /// <value>The other preferences.</value>
        public string OtherPreferences
        {
            get { return propOtherPreferences; }
            set { propOtherPreferences = value; }
        }

        private string propSpecialServiceRequest = string.Empty;

        /// <summary>
        /// Gets or sets the special service request.
        /// </summary>
        /// <value>The special service request.</value>
        public string SpecialServiceRequest
        {
            get { return propSpecialServiceRequest; }
            set { propSpecialServiceRequest = value; }
        }

        /// <summary>
        /// Additional Request
        /// </summary>
        private string propAdditionalRequest = string.Empty;

        /// <summary>
        /// Gets or sets the special service request.
        /// </summary>
        /// <value>The special service request.</value>
        public string AdditionalRequest
        {
            get { return propAdditionalRequest; }
            set { propAdditionalRequest = value; }
        }

        private string propContactPerson = string.Empty;

        /// <summary>
        /// Gets or sets the contact person.
        /// </summary>
        /// <value>The contact person.</value>
        public string ContactPerson
        {
            get { return propContactPerson; }
            set { propContactPerson = value; }
        }

        private string propName = string.Empty;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return propName; }
            set { propName = value; }
        }

        private string propTitle = string.Empty;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Title
        {
            get { return propTitle; }
            set { propTitle = value; }
        }

        private string propCityOrTown = string.Empty;

        /// <summary>
        /// Gets or sets the city or town.
        /// </summary>
        /// <value>The city or town.</value>
        public string CityOrTown
        {
            get { return propCityOrTown; }
            set { propCityOrTown = value; }
        }

        private string propCountry = string.Empty;

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        public string Country
        {
            get { return propCountry; }
            set { propCountry = value; }
        }

        private string propEmailAddress = string.Empty;

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        /// <value>The email address.</value>
        public string EmailAddress
        {
            get { return propEmailAddress; }
            set { propEmailAddress = value; }
        }

        private string propPhoneNumber = string.Empty;

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        /// <value>The phone number.</value>
        public string PhoneNumber
        {
            get { return propPhoneNumber; }
            set { propPhoneNumber = value; }
        }

        private string propFGPNumber = string.Empty;

        /// <summary>
        /// Gets or sets the FGP number.
        /// </summary>
        /// <value>The FGP number.</value>
        public string FGPNumber
        {
            get { return propFGPNumber; }
            set { propFGPNumber = value; }
        }

        private string propCreditCardNumber = string.Empty;

        /// <summary>
        /// Gets or sets the FGP number.
        /// </summary>
        /// <value>The FGP number.</value>
        public string CreditCardNumber
        {
            get { return propCreditCardNumber; }
            set { propCreditCardNumber = value; }
        }

        private string propExpiryDate = string.Empty;

        /// <summary>
        /// Gets or sets the expiry date.
        /// </summary>
        /// <value>The expiry date.</value>
        public string ExpiryDate
        {
            get { return propExpiryDate; }
            set { propExpiryDate = value; }
        }

        private string propCreditCardHolder = string.Empty;

        /// <summary>
        /// Gets or sets the Credit Card Holder Name.
        /// </summary>
        /// <value>The expiry date.</value>
        public string CreditCardHolder
        {
            get { return propCreditCardHolder; }
            set { propCreditCardHolder = value; }
        }

        private string propCreditCardType = string.Empty;

        /// <summary>
        /// Gets or sets the Credit Card Type.
        /// </summary>
        /// <value>The expiry date.</value>
        public string CreditCardType
        {
            get { return propCreditCardType; }
            set { propCreditCardType = value; }
        }

        private string propGuaranteeInformation = string.Empty;


        /// <summary>
        /// Gets or sets the guarantee information for each room.
        /// </summary>
        /// <value>The guarantee information for each room.</value>
        public string GuaranteeInformationForEachRoom
        {
            get { return propGuaranteeInformation; }
            set { propGuaranteeInformation = value; }
        }

        private bool propShowGuaranteeForEachRoom = false;

        /// <summary>
        /// [ERROR: Unknown property access] a value indicating whether [show guarantee for each room].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [show guarantee for each room]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowGuaranteeForEachRoom
        {
            get { return propShowGuaranteeForEachRoom; }
            set { propShowGuaranteeForEachRoom = value; }
        }

        private string propCancellationNumber = string.Empty;

        /// <summary>
        /// Gets or sets the cancellation number.
        /// </summary>
        /// <value>The cancellation number.</value>
        public string CancellationNumber
        {
            get { return propCancellationNumber; }
            set { propCancellationNumber = value; }
        }

        private bool showRoomTypeRow = true;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is room type row to be shown.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is room type row to be shown; otherwise, <c>false</c>.
        /// </value>
        public bool IsRoomTypeRowToBeShown
        {
            get { return showRoomTypeRow; }
            set { showRoomTypeRow = value; }
        }

        private bool showAdultAndChildInfoTable = true;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is adult and child info table to be shown.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is adult and child info table to be shown; otherwise, <c>false</c>.
        /// </value>
        public bool IsAdultAndChildInfoTableToBeShown
        {
            get { return showAdultAndChildInfoTable; }
            set { showAdultAndChildInfoTable = value; }
        }

        private bool showContactInfoTable = true;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is show contact info table.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is show contact info table; otherwise, <c>false</c>.
        /// </value>
        public bool IsShowContactInfoTable
        {
            get { return showContactInfoTable; }
            set { showContactInfoTable = value; }
        }


        private string guaranteeTextHeader;
        /// <summary>
        /// This property holds the gaurantee text header entered in CMS
        /// </summary>
        public string GuaranteeTextHeader
        {
            get { return guaranteeTextHeader; }
            set { guaranteeTextHeader = value; }
        }

        public bool CardHolderName
        {
            get { return this.divCardHolderName.Visible; }
            set { this.divCardHolderName.Visible = value; }
        }

        private string deepLinkUrl = string.Empty;
        public string DeepLinkUrl
        {
            get { return deepLinkUrl; }
            set { deepLinkUrl = value; }
        }

        public bool ShowDeepLinkForOtherRooms
        {
            get;
            set;
        }

        #endregion
    }
}