//  Description					: Code Behind for Update Header Message			          //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Priya Singh 	                                          //
/// Author email id				:                           							  //
/// Creation Date				: 27th November  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for UpdateMessageHeader
    /// </summary>
    public partial class UpdateMessageHeader : EPiServer.UserControlBase
    {
    }
}