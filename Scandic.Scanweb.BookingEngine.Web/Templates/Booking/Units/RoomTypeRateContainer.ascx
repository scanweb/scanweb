<%@ Control Language="C#" AutoEventWireup="true" Codebehind="RoomTypeRateContainer.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.RoomTypeRateContainer" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<div id="roomTabWrapper" runat="server">
<%--<div class="tabHd">
</div>
<div class="tabCnt height15">
</div>
<div class="tabFt">
</div>
--%></div>
<div id="blkRoomRateTypeContainer" runat="server" class="tableData">
</div>
<!-- /Room Detail Container -->
<!-- Room Detail Links -->

 
<div class="" id="tabBtn" runat="server">
<!--artf1148359|Summary:Alt text|Modification:Updated Title Property|Rajneesh -->
<a class="buttonInner showallbutton" href="#" tabindex="41" rel="showAll"><%= SELECT_RATE_ALL_ROOM_TYPES %></a>
<a class="buttonInner hideall" href="#" tabindex="42" rel="hideSome" style="display:none;"><%= SELECT_RATE_SPECIFIC_ROOM_TYPES %></a>
<a class="buttonRt" href="#"></a>
<input type="hidden" id="roomID" name="roomID" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room") %>" />
<input type="hidden" id="selectedID" name="roomID" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/selectedMsg") %>" />
<input type="hidden" id="nextRoomMessage" name="roomID" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nextRateMsg") %>" />
<input type="hidden" id="lastRateHeaderMsg" name="roomID" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/lastRateHeader") %>" />
<input type="hidden" id="lastRoomMessage" name="roomID" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/lastRateMsg") %>" />
</div>



<!-- /Room Detail Links-->
