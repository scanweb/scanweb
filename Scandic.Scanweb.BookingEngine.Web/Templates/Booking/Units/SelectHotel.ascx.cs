using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System.Configuration;
using System.Linq;
using Scandic.Scanweb.ExceptionManager;
namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for SelectHotel
    /// </summary>
    public partial class SelectHotel : EPiServer.UserControlBase, INavigationTraking
    {
        #region Private Variables
        private bool isSessionValid = true;

        private const string SORT_ALPHA_PATH = "/bookingengine/booking/selecthotel/sortAlphabetic";
        private const string SORT_RATE_PATH = "/bookingengine/booking/selecthotel/sortRate";
        private const string SORT_CORPORATE_PATH = "/bookingengine/booking/selecthotel/sortCorporate";
        private const string SORT_POINTS_PATH = "/bookingengine/booking/selecthotel/sortPoints";
        private const string SORT_CITY_CENTRE_PATH = "/bookingengine/booking/selecthotel/sortCiteCentre";
        private const string SORT_ALTERNATE_HOTEL_DISTANCE = "/bookingengine/booking/selecthotel/alternateHotelsDistance";
        private const string SORT_TRIPADVISOR = "/bookingengine/booking/selecthotel/sortTripAdvisor";
        private const string SORT_DISCOUNTTYPE = "/bookingengine/booking/selecthotel/sortDiscountType";
        protected bool paginationEnabled = false;
        #endregion


        /// <summary>
        /// Page Load method of Select Hotel
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            paginationEnabled = ConfigurationManager.AppSettings["SelectHotel.PaginationEnabled"] != null ? Convert.ToBoolean(ConfigurationManager.AppSettings["SelectHotel.PaginationEnabled"]) : true;
            operaErr.Value = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/OperaNotConnected");
            rdoperNight.Text = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rdoPernight");
            rdoperStay.Text = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rdoperStay");
            ProgressBar.SetProgressBarSelection(EpiServerPageConstants.SELECT_HOTEL_PAGE);
            isSessionValid = WebUtil.IsSessionValid();

            if (!paginationEnabled)
            {
                ddlHotelsPerPage.Visible = false;
                lblView.Visible = false;
            }
            else
            {
                ddlHotelsPerPage.Visible = true;
                lblView.Visible = true;
            }

            //Merchandising:R3:Display ordinary rates for unavailable promo rates 
            AlternateHotelsSessionWrapper.IsHotelAlternateFlow = false;

            if (isSessionValid)
            {
                if ((Session != null && Session.Count <= 4) || SearchCriteriaSessionWrapper.SearchCriteria == null)
                {
                    try
                    {
                        HttpContext.Current.Response.Redirect(string.Format("{0}?SessionExpired=true", HttpContext.Current.Request.RawUrl));
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        HttpContext.Current.Response.End();
                    }
                    catch
                    {
                    }
                }
                ProcessControlLoad();
                CheckSelectedHotelRequest();
            }
            
        }

        private void CheckSelectedHotelRequest()
        {
            var hotelId = Request.QueryString["SelectedHotelId"];
            if (!string.IsNullOrEmpty(hotelId))
            {
                UserNavTracker.TrackAction(this, "Select Hotel");
                //WebUtil.ApplicationErrorLog(new Exception("Error in Select Hotel"));
                //Response.End();
                SelectedHotel(hotelId);

            }

        }

        /// <summary>
        /// This will rearrange the pagination.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ReArrange_pagination(object sender, EventArgs e)
        {
            headerDtMsg.InnerHtml = txtAvailabilityMsg2.Value;

            infoAlert.Attributes.Add("style", "display:block");
            if (!string.IsNullOrEmpty(txtPageNo.Value))
                txtPageNo.Value = AppConstants.ONE;
            DisplayHotels(rdoperStay.Checked);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "RemoveOverlayOnReload();", true);
        }

        /// <summary>
        /// This method will sort the hotels based the sort option chosen by the
        /// user. 
        /// It uses the Comparers for different kinds of sorting
        /// The page number to be displayed is set to 1 as we should be displying the first page
        /// when user does the sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Sort_Hotels(object sender, EventArgs e)
        {
            headerDtMsg.InnerHtml = txtAvailabilityMsg2.Value;

            infoAlert.Attributes.Add("style", "display:block");
            if (rdoperStay.Checked)
            {
                rdoperStay.Checked = false;
                rdoperNight.Checked = true;
            }
            txtPageNo.Value = AppConstants.ONE;
            DisplayHotels(rdoperStay.Checked);
            Page.ClientScript.RegisterStartupScript(GetType(), "alert", "RemoveOverlayOnReload();", true);
        }
        protected void SortHotels()
        {
            List<IHotelDetails> hotels = HotelResultsSessionWrapper.HotelResults;
            if ((hotels != null) && (hotels.Count > 0))
            {
                string sortByOption = ddlSortHotel.SelectedValue;
                switch (GetSortyByOption(sortByOption))
                {
                    case SortType.ALPHABETIC:
                        {
                            SelectHotelUtil.SortHotelsByName(hotels);
                            break;
                        }
                    case SortType.RATE:
                        {
                            hotels.Sort(new RateComparer());
                            SelectHotelUtil.SortByMaxRate(hotels);
                            break;
                        }
                    case SortType.CORPORATE:
                        {
                            hotels.Sort(new CorporateComparer());
                            break;
                        }
                    case SortType.CITYCENTREDISTANCE:
                        {
                            SelectHotelUtil.SortCityCenterDistance(hotels);
                            break;
                        }
                    case SortType.POINTS:
                        {
                            hotels.Sort(new PointComparer());
                            break;
                        }
                    case SortType.ALTERNATEHOTELSDISTANCE:
                        {
                            HotelResultsSessionWrapper.HotelResults = hotels.OrderBy(a => a.AlternateHotelsDistance).ThenBy(a => a.MinRate != null ? a.MinRate.Rate : double.MaxValue).ToList();
                            break;
                        }
                    case SortType.DISTANCETOCITYCENTREOFSEARCHEDHOTEL:
                        {
                            SelectHotelUtil.SortByDistanceToCityCenterOfSearchedHotel(hotels);
                            break;
                        }
                    case SortType.TRIPADVISOR:
                        {
                            /*hotels.Sort(new TripAdvisorRatingComparer());*/
                            SelectHotelUtil.SortByTARatingMaxRate(hotels);
                            break;
                        }
                    case SortType.DISCOUNTTYPE:
                        SelectHotelUtil.SortByDiscountTypeAndRate(hotels);
                        //hotels.Sort(new PromoComparer());
                        break;
                }
            }
        }

        #region Select_Hotel

        /// <summary>
        /// This is the event fired when user clicks on the Select Hotel button
        /// The value set in the txtHotelId will be saved to the Search criteria
        /// and user is redirected to the Select Rate Page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Select_Hotel(object sender, EventArgs e)
        {
            string selectedHotelId = txtHotelId.Value;
            SelectedHotel(selectedHotelId);
        }

        private void SelectedHotel(string hotelId)
        {
            if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor != null)
            {
                SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode = hotelId;
                SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.SearchCode = hotelId;
                SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.SearchString =
                  SelectHotelUtil._AvailabilityController.GetHotelNameFromCMS(hotelId);

                AlternateHotelsSessionWrapper.IsHotelAlternateFlow = true;//Merchandising:R3:Display ordinary rates for unavailable promo rates 

                Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE), false);
            }
        }

        #endregion Select_Hotel

        #region Select_Pagination

        /// <summary>
        /// This is the event fired when user clicks on the pagination links.
        /// And the page is loaded with the next set of hotels.
        /// When user is going to select rate page from select hotel page and then coming back to the select rate page using 
        /// browser back button then the pagination link was not working properly.Now clicking on the pagination link it was taking to the 
        /// select rate page instead of taking to the next set of the hotels.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Select_Pagination(object sender, EventArgs e)
        {
            headerDtMsg.InnerHtml = txtAvailabilityMsg2.Value;

            infoAlert.Attributes.Add("style", "display:block");
            DisplayHotels(rdoperStay.Checked);
        }

        #endregion Select_Pagination

        /// <summary>
        /// Add method to set data perNight on radio button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void StayPerNight(object sender, EventArgs e)
        {
            headerDtMsg.InnerHtml = txtAvailabilityMsg2.Value;
            DisplayHotels(false);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "RemoveOverlayOnReload();", true);
        }

        /// <summary>
        /// Add method to set the data for perStay on radio button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void Gmaptab_Click(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Hello');", true);

        }

        /// <summary>
        /// Add method to set the data for perStay on radio button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PerStay(object sender, EventArgs e)
        {
            headerDtMsg.InnerHtml = txtAvailabilityMsg2.Value;
            DisplayHotels(true);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "RemoveOverlayOnReload();", true);
        }

        /// <summary>
        /// This method will be called when user choses the Show all from the select hotel page
        /// The page no is set to 1 so when user hides the show all hotels the first page needs
        /// to be displayed to the user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowAll(object sender, EventArgs e)
        {
            txtPageNo.Value = AppConstants.ONE;
            DisplayHotels(rdoperStay.Checked);
        }
        private static List<CityDestination> FetchAllDestinationsList()
        {
            List<CityDestination> cityList = new List<CityDestination>();
            cityList = ContentDataAccess.GetCityAndHotelForAutoSuggest(false);
            return cityList;
        }

        /// <summary>
        /// This method will set the dropdowns for the sort by dropdown box
        /// The sort by options are set as per the type of search.
        /// </summary>
        protected void SetSortDropDowns()
        {
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;

            if (AlternateHotelsSessionWrapper.DisplayAlternateHotelsAvailable)
            {
                ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_CITY_CENTRE_PATH),
                                                    SortType.DISTANCETOCITYCENTREOFSEARCHEDHOTEL.ToString()));
                ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_ALTERNATE_HOTEL_DISTANCE),
                                                    SortType.ALTERNATEHOTELSDISTANCE.ToString()));
            }
            else
            {
                ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_CITY_CENTRE_PATH),
                                                    SortType.CITYCENTREDISTANCE.ToString()));
            }

            ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_ALPHA_PATH), SortType.ALPHABETIC.ToString()));
            if (hotelSearch != null)
            {
                switch (hotelSearch.SearchingType)
                {
                    case SearchType.REGULAR:
                        ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_RATE_PATH), SortType.RATE.ToString()));
                        break;
                    case SearchType.REDEMPTION:
                        ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_POINTS_PATH), SortType.POINTS.ToString()));
                        break;
                    case SearchType.CORPORATE:
                        ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_CORPORATE_PATH), SortType.CORPORATE.ToString()));
                        ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_RATE_PATH), SortType.RATE.ToString()));
                        break;
                    case SearchType.BONUSCHEQUE:
                        ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_RATE_PATH), SortType.RATE.ToString()));
                        break;
                }
            }

            if (hotelSearch != null && hotelSearch.SearchedFor != null && !string.IsNullOrEmpty(hotelSearch.SearchedFor.SearchCode))
            {
                bool IsCityOperaID = false;
                var SearchedCityOperaID = from city in FetchAllDestinationsList()
                                          where string.Equals(city.OperaDestinationId, hotelSearch.SearchedFor.SearchCode, StringComparison.InvariantCultureIgnoreCase)
                                          select city.OperaDestinationId;
                if (SearchedCityOperaID != null && SearchedCityOperaID.ToList().Count > 0)
                {
                    IsCityOperaID = true;
                }
                AvailabilityController availabilityController = new AvailabilityController();
                bool hideCityTARatings = false;
                if (AlternateHotelsSessionWrapper.DisplayAlternateHotelsAvailable && HotelResultsSessionWrapper.HotelResults != null
                    && HotelResultsSessionWrapper.HotelResults.Count > 0 && HotelResultsSessionWrapper.HotelResults[0].HotelDestination != null && !IsCityOperaID)
                {
                    PageData pd = ContentDataAccess.GetPageDataByOperaID(hotelSearch.SearchedFor.SearchCode, hotelSearch.SearchedFor.SearchString);
                    if (pd != null)
                    {
                        string cityOperaID = string.Empty;
                        PageData cityPageData = ContentDataAccess.GetPageData((ContentDataAccess.GetPageData(pd.ParentLink.ID)).ParentLink.ID);
                        if (cityPageData != null)
                            cityOperaID = Convert.ToString(cityPageData["OperaID"]);
                        hideCityTARatings = availabilityController.GetCityTripAdvisorFlag(cityOperaID);
                        if (!Convert.ToBoolean(hideCityTARatings))
                        {
                            ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_TRIPADVISOR), SortType.TRIPADVISOR.ToString()));
                        }
                    }
                }
                else
                {
                    //to make sure that this piece of code only gets fire for City Search.
                    if (hotelSearch.SelectedHotelCode == null && hotelSearch.SearchedFor != null && hotelSearch.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City)
                    {
                        hideCityTARatings = availabilityController.GetCityTripAdvisorFlag(hotelSearch.SearchedFor.SearchCode);
                        if (!Convert.ToBoolean(hideCityTARatings))
                        {
                            ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_TRIPADVISOR), SortType.TRIPADVISOR.ToString()));
                        }
                    }
                }
            }

            if (hotelSearch != null && hotelSearch.SearchingType == SearchType.REGULAR && !string.IsNullOrEmpty(hotelSearch.CampaignCode))
            {
                ddlSortHotel.Items.Insert(0, (new ListItem(WebUtil.GetTranslatedText(SORT_DISCOUNTTYPE), SortType.DISCOUNTTYPE.ToString())));
            }

            int perPageHotelCount = (int)CurrentPage["PerPageHotelCount"];
            if (perPageHotelCount <= 0)
            {
                perPageHotelCount = AppConstants.HOTELS_PER_PAGE;
            }
            for (int count = 1; count <= perPageHotelCount; count++)
            {
                if (count == 1)
                    ddlHotelsPerPage.Items.Add(
                        new ListItem(count + " " + WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/OneHotelPerPageText"), count.ToString()));
                else
                    ddlHotelsPerPage.Items.Add(
                        new ListItem(count + " " + WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/HotelsPerPageText"), count.ToString()));
            }
            ddlHotelsPerPage.Items.Add(new ListItem(WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/showallhotels"), "Show All"));

            if (!paginationEnabled)
            {
                ddlHotelsPerPage.SelectedValue = "Show All";
                footerContainer.Style.Add("display", "none");
            }
            else
            {
                ddlHotelsPerPage.SelectedIndex = perPageHotelCount - 1;
            }
        }

        /// <summary>
        /// This method will be used in the ascx file to read the HotelSearchEntity
        /// will read the HotelSearch Entity from sessionWrapper and return the value
        /// </summary>
        /// <returns>HotelSearchEntity</returns>
        protected HotelSearchEntity HotelSearchEntity()
        {
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            return hotelSearch;
        }

        /// <summary>
        /// This method will return the list of hotels to be displayed to the user
        /// If user has selected the ShowAll option from Select Hotel page then all
        /// hotels are returned. 
        /// If the user has selected different page from the select Hotel, then corresponding
        /// list of hotels to be displayed fo the page selected will be displayed
        /// If the user has done a sorting the hotels are already sorted before this method is called
        /// so the corresponding hotels for the page 1 is returned if user does a sorting
        /// </summary>
        /// <returns>The list of hotels to be displayed to the user</returns>
        public int GetPageSize()
        {
            bool paginationEnabled = ConfigurationManager.AppSettings["SelectHotel.PaginationEnabled"] != null ? Convert.ToBoolean(ConfigurationManager.AppSettings["SelectHotel.PaginationEnabled"]) : true;

            List<IHotelDetails> hotels = SelectHotelUtil.GetAllHotels();
            if (!paginationEnabled)
                return 1;
            int pagenum = 0;
            bool ispageselected = int.TryParse(ddlHotelsPerPage.SelectedValue, out pagenum);

            if (hotels != null && hotels.Count > 0)
            {
                if (ispageselected)
                    return pagenum;
                else
                    return hotels.Count;
            }
            else
            {
                if (ispageselected)
                    return pagenum;
                else
                    return 1;
            }
        }

        /// <summary>
        /// HotelsToBeDisplayed
        /// </summary>
        /// <returns>List of IHotelDetails</returns>
        protected List<IHotelDetails> HotelsToBeDisplayed()
        {
            List<IHotelDetails> hotels = SelectHotelUtil.GetAllHotels();
            int hotelsPerPage;
            bool isPageNumberSelected = int.TryParse(ddlHotelsPerPage.SelectedValue, out hotelsPerPage);
            if ((!string.IsNullOrEmpty(txtShowAll.Value)) && (txtShowAll.Value.ToUpper() == "FALSE")
                && (hotels != null) && (isPageNumberSelected))
            {
                int pageNo = int.Parse(txtPageNo.Value);
                int startIndex = ((pageNo - 1) * hotelsPerPage);
                int resultCount = hotelsPerPage;

                if ((startIndex + hotelsPerPage - 1) >= hotels.Count)
                {
                    resultCount = hotels.Count - startIndex;
                }
                return hotels.GetRange(startIndex, resultCount);
            }
            else
            {
                return hotels;
            }
        }


        /// <summary>
        /// This displays all the hotels in select hotel page.
        /// </summary>
        ///<param name="isPerStay">To pass if the display of hotels is "Per Stay" or "Per Night".Pass true for perstay</param>
        protected void DisplayHotels(bool isPerStay)
        {
            if (!SelectHotelUtil.IsSearchToBeDone())// && !HotelResultsSessionWrapper.IsPromoNotValidForCity)
            {
                allHotels.Controls.Clear();
                SortHotels();
                List<IHotelDetails> hotels = HotelsToBeDisplayed();
                HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                if (hotelSearch != null)
                {
                    SearchType searchType = hotelSearch.SearchingType;
                    bool promoSearchShowHotelsWithPublicRates = false;
                    bool hasHotelsWithPromotionRates = SelectHotelUtil.HasHotelsWithPromotionRates();
                    switch (searchType)
                    {
                        case SearchType.CORPORATE:
                            if ((!Utility.IsBlockCodeBooking) &&
                                (!SelectHotelUtil.HasHotelsWithNegotiatedRates()))
                            {
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "page_load", WebUtil.ShowSearchHotelWidgetOnPageLoad());
                                string errMsg = string.Empty;
                                if (!string.IsNullOrEmpty(hotelSearch.CampaignCode))
                                    errMsg = string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/nohotelswithdnumber"), hotelSearch.CampaignCode);
                                else
                                    errMsg = string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/nohotelswithdnumber"), string.Empty);
                                errorLabel.InnerHtml = errMsg;
                                ddlSortHotel.Items.Remove(ddlSortHotel.Items.FindByValue("DISCOUNTTYPE"));
                                errorAlertDiv.Style.Add("display", "block");
                            }
                            break;
                        case SearchType.VOUCHER:
                            if (!hasHotelsWithPromotionRates)
                            {
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "page_load", WebUtil.ShowSearchHotelWidgetOnPageLoad());
                                promoSearchShowHotelsWithPublicRates = true;
                                string errMsg = string.Empty;
                                if (!string.IsNullOrEmpty(hotelSearch.CampaignCode))
                                    errMsg = string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/nohotelswithvouchercode"), hotelSearch.CampaignCode);
                                else
                                    errMsg = string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/nohotelswithvouchercode"), string.Empty);

                                errorLabel.InnerHtml = errMsg;
                                errorAlertDiv.Style.Add("display", "block");
                                SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = null;
                                SearchCriteriaSessionWrapper.SearchCriteria.SearchingType = SearchType.REGULAR;
                            }
                            break;
                    }

                    if (hotels != null && hotels.Count > 0)
                    {
                        foreach (IHotelDetails hotel in hotels)
                        {
                            IHotelDisplayInformation hotelDisplay =
                                SelectHotelUtil.GetHotelDisplayObject(searchType, hotel, hotelSearch.CampaignCode,
                                                                      promoSearchShowHotelsWithPublicRates, isPerStay);
                            if (hotelDisplay != null)
                            {
                                TripAdvisorPlaceEntity TAEntity = WebUtil.GetTAReviewEntityForHotel(hotelDisplay.TALocationID);

                                hotel.HideHotelTARating = hotel.HotelDestination != null ? hotel.HotelDestination.HideHotelTARating : false;
                                if (!hotel.HideHotelTARating)
                                    hotel.TripAdvisorHotelRating = TAEntity != null ? TAEntity.AverageRating : 0.0;
                                else
                                    hotel.TripAdvisorHotelRating = 0.0;

                                HotelDetailBody hotelBody = (HotelDetailBody)LoadControl("HotelDetailBody.ascx");
                                string sortByOption = ddlSortHotel.SelectedValue;
                                if (GetSortyByOption(sortByOption) == SortType.CITYCENTREDISTANCE)
                                {
                                    string CityCenterDistanceString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/citycenterdistance");
                                    hotelDisplay.CityCenterDistance = string.Format(CityCenterDistanceString,
                                                                                    hotel.HotelDestination.CityCenterDistance,
                                                                                    hotel.HotelDestination.HotelAddress.City);
                                }
                                if (GetSortyByOption(sortByOption) == SortType.DISTANCETOCITYCENTREOFSEARCHEDHOTEL)
                                {
                                    string CityCenterDistanceString = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/citycenterdistance");
                                    hotelDisplay.CityCenterDistance = string.Format(CityCenterDistanceString,
                                                                                    hotel.DistanceToCityCentreOfSearchedHotel,
                                                                                    SearchedHotelCityNameSessionWrapper.SearchedHotelCityName);
                                }
                                hotelBody.Hotel = hotelDisplay;
                                allHotels.Controls.Add(hotelBody);
                            }
                        }
                        if (AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                        {
                            if (hotelSearch.CampaignCode != null && hotelSearch.SearchingType == SearchType.REGULAR
                                && hotelSearch.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City)
                            {
                                if (SelectHotelUtil.TotalPromotionRateAvailableHotels() > 0)
                                    errorLabel.InnerHtml = string.Format(WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE1),
                                        hotelSearch.CampaignCode);
                                else
                                    errorLabel.InnerHtml = string.Format(WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE2),
                                        hotelSearch.CampaignCode);

                                Page.ClientScript.RegisterStartupScript(this.GetType(), "page_load", WebUtil.ShowSearchHotelWidgetOnPageLoad());

                            }
                            else
                                errorLabel.InnerHtml = string.Format(WebUtil.GetTranslatedText(AppConstants.ALT_CITY_HOTELS_MSG),
                                                  hotelSearch.SearchedFor.SearchString);
                        }
                    }
                }
            }
            ListItem rateItem = new ListItem(WebUtil.GetTranslatedText(SORT_RATE_PATH), SortType.RATE.ToString());
            if (!SelectHotelUtil.HasHotelsWithPromotionRates() &&
                (!ddlSortHotel.Items.Contains(rateItem)) &&
                (SearchCriteriaSessionWrapper.SearchCriteria != null) &&
                (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType != SearchType.REDEMPTION))
            {
                if (null == ddlSortHotel.Items)
                {
                    ddlSortHotel.Items.Insert(0, new ListItem(WebUtil.GetTranslatedText(SORT_RATE_PATH), SortType.RATE.ToString()));
                }
                else
                {
                    ddlSortHotel.Items.Insert(1, new ListItem(WebUtil.GetTranslatedText(SORT_RATE_PATH), SortType.RATE.ToString()));
                }
            }

            blkSearchProgress.Visible = false;
            blkPaginationProgress.Visible = false;
        }

        /// <summary>
        /// This method will return the pagination strings based on the number of hotels to be
        /// displayed per page which is configured in CMS and total number of hotels returned from the search.
        /// 
        /// Will create the list of strings for example "Previous | 1-4 | 5-8 | 9-12 | 13 | Next" if the pagination size is 4
        /// and will only return the strings which are in the total size of the hotels
        /// </summary>
        /// <returns>The pagination strings for example 1-3, 4-6, 7-9 ...</returns>
        protected List<string> GetPaginationDetails()
        {
            int pageSize = 0;
            bool ispageselected = int.TryParse(ddlHotelsPerPage.SelectedValue, out pageSize);
            if (ispageselected)
                pageSize = int.Parse(ddlHotelsPerPage.SelectedValue);
            else
                pageSize = 1;
            //int pageSize = int.Parse(ddlHotelsPerPage.SelectedValue);
            int totalPages;
            List<IHotelDetails> hotels = SelectHotelUtil.GetAllHotels();
            int totalHotels = hotels.Count;
            if (hotels != null)
            {
                totalPages = TotalPages();

                List<string> pageDetails = new List<string>();
                int startVal = 1;
                if (totalPages == 1)
                {
                    int endVal = startVal + hotels.Count - 1;
                    pageDetails.Add(startVal + AppConstants.HYPHEN + endVal);
                    return pageDetails;
                }
                else
                {
                    for (int i = 0; i < totalPages; i++)
                    {
                        int endVal = startVal + pageSize - 1;

                        if (endVal > totalHotels)
                            endVal = totalHotels;

                        if (startVal == endVal)
                            pageDetails.Add(startVal + string.Empty);
                        else
                            pageDetails.Add(startVal + AppConstants.HYPHEN + endVal);
                        startVal = endVal + 1;
                    }
                    return pageDetails;
                }
            }
            return null;
        }

        /// <summary>
        /// The total number of pages as per the pagination set in the Appsettings.config
        /// and total hotels returned by the search
        /// </summary>
        /// <returns>The total number of pages for the search</returns>
        protected int TotalPages()
        {
            List<IHotelDetails> hotels = SelectHotelUtil.GetAllHotels();
            int pageSize = GetPageSize();
            int totalHotels;
            int totalPages = 1;

            if (hotels != null)
            {
                totalHotels = hotels.Count;
                if (totalHotels > pageSize)
                    totalPages = (int)Math.Ceiling((double)totalHotels / pageSize);
            }
            return totalPages;
        }

        /// <summary>
        /// This method will return the total number of hotels returned from the search
        /// and will be used int he ascx page
        /// </summary>
        /// <returns></returns>
        protected int TotalHotels()
        {
            List<IHotelDetails> hotels = HotelResultsSessionWrapper.HotelResults;
            if (hotels != null) return hotels.Count;
            else return 0;
        }


        #region Private Methods

        /// <summary>
        /// This method will read the value of the sort by option drop down of the Select Hotel page
        /// and returns the enumerated value the SortType
        /// </summary>
        /// <param name="sortBySelected">The sorty by option selected by the user</param>
        /// <returns>Returns the SortType option</returns>
        private SortType GetSortyByOption(string sortBySelected)
        {
            SortType sortType = SortType.NONE;
            try
            {
                sortType = (SortType)Enum.Parse(typeof(SortType), sortBySelected, true);
            }
            catch (Exception)
            {
            }
            return sortType;
        }

        /// <summary>
        /// Process initial load operations required for the control.
        /// </summary>
        private void ProcessControlLoad()
        {
            try
            {

                if (null != SearchCriteriaSessionWrapper.SearchCriteria)
                {
                    if (SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor != null &&
                        !string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.SearchString) &&
                        SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City)
                    {
                        if (!string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode))
                            cityName.Text = SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.CitySearchString;
                        else
                            cityName.Text = SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.SearchString;
                    }
                    if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType.Equals(SearchType.REDEMPTION) ||
                        SearchCriteriaSessionWrapper.SearchCriteria.SearchingType.Equals(SearchType.VOUCHER))
                    {
                        rdoperNight.Visible = false;
                        rdoperNight.Checked = false;
                        rdoperStay.Visible = true;
                        rdoperStay.Checked = true;
                        rdoPerNightMap.Visible = false;
                        rdoPerStayMap.Visible = true;
                        rdoPerStayMap.Checked = true;
                        lblPerNightMap.Visible = false;
                    }
                    else
                    {
                        lblPerNightMap.Visible = true;
                        rdoPerNightMap.Visible = true;
                        rdoPerNightMap.Checked = true;
                        rdoPerStayMap.Visible = true;
                    }
                }
                if (this.Visible && isSessionValid)
                {
                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                    if (!IsPostBack)
                    {
                        SetSortDropDowns();
                        if (hotelSearch != null)
                        {
                            if (AlternateHotelsSessionWrapper.DisplayAlternateHotelsAvailable && hotelSearch.SearchedFor != null)
                            {
                                string searchedHotelName = hotelSearch.SearchedFor.SearchString;

                                //Merchandising:R3:Display ordinary rates for unavailable promo rates 
                                string alternateHotelMessage = string.Empty;
                                if (hotelSearch.CampaignCode != null && hotelSearch.SearchingType == SearchType.REGULAR)
                                {
                                    if (HotelResultsSessionWrapper.HotelDetails == null &&
                                        hotelSearch.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                                        alternateHotelMessage = WebUtil.GetTranslatedText(AppConstants.ALTERNATE_HOTELS_PROMO_MESSAGE4);
                                    else if (HotelResultsSessionWrapper.HotelDetails != null &&
                                             hotelSearch.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                                        alternateHotelMessage =
                                            string.Format(WebUtil.GetTranslatedText(AppConstants.ALTERNATE_HOTELS_PROMO_MESSAGE1), hotelSearch.CampaignCode);
                                    else
                                        alternateHotelMessage =
                                            string.Format(WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE1), hotelSearch.CampaignCode);
                                }
                                else
                                    alternateHotelMessage = WebUtil.GetTranslatedText(TranslatedTextConstansts.ALTERNATE_HOTELS_MESSAGE);

                                Page.ClientScript.RegisterStartupScript(this.GetType(), "page_load", WebUtil.ShowSearchHotelWidgetOnPageLoad());

                                alternateHotelMessage = string.Format(alternateHotelMessage, searchedHotelName);
                                errorLabel.InnerHtml = alternateHotelMessage;
                                errorAlertDiv.Style.Add("display", "block");
                            }
                            else if (AlternateHotelsSessionWrapper.DisplayNoHotelsAvailable)
                            {
                                errorLabel.InnerText = WebUtil.GetTranslatedText(TranslatedTextConstansts.NO_HOTELS_AVAILABLE_MESSAGE);
                                errorAlertDiv.Style.Add("display", "block");
                                subHeader.Visible = false;
                                footerContainer.Visible = false;
                                TabStructure.Visible = false;
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "page_load", WebUtil.ShowSearchHotelWidgetOnPageLoad());
                            }
                        }
                    }

                    string eventTarget = Page.Request.Form["__EVENTTARGET"];
                    if (string.IsNullOrEmpty(eventTarget))
                        if (AlternateHotelsSessionWrapper.DisplayAlternateHotelsAvailable)
                        {
                            List<IHotelDetails> hotels = HotelResultsSessionWrapper.HotelResults;
                            //Merchandising:R3:Display ordinary rates for unavailable promo rates 
                            if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REGULAR && SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode != null)
                            {
                                //hotels.Sort(new PromoComparer());
                                SelectHotelUtil.SortByDiscountTypeAndRate(hotels);
                                ddlSortHotel.SelectedValue = SortType.DISCOUNTTYPE.ToString();
                            }
                            else
                            {
                                hotels.Sort(new AlternateHotelsDistanceComparer());
                                ddlSortHotel.SelectedValue = SortType.ALTERNATEHOTELSDISTANCE.ToString();
                            }
                        }

                    DisplayHotels(rdoperStay.Checked);

                    if (string.IsNullOrEmpty(txtPageNo.Value))
                        txtPageNo.Value = AppConstants.ONE;
                    if (false == SelectHotelUtil.IsSearchToBeDone() &&
                        HotelResultsSessionWrapper.HotelResults != null &&
                        HotelResultsSessionWrapper.HotelResults.Count <= 0)
                    {
                        //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
                        if (SearchCriteriaSessionWrapper.SearchCriteria != null &&
                            SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode != null &&
                            SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REGULAR
                            &&
                            SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor != null &&
                            SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.UserSearchType ==
                            SearchedForEntity.LocationSearchType.City)
                            errorLabel.InnerText = (WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE3));
                        else
                            errorLabel.InnerText = WebUtil.GetTranslatedText(AppConstants.NO_HOTELS_FOUND);

                        Page.ClientScript.RegisterStartupScript(this.GetType(), "page_load", WebUtil.ShowSearchHotelWidgetOnPageLoad());
                        errorAlertDiv.Style.Add("display", "block");
                        subHeader.Visible = false;
                        allHotels.Visible = false;
                        footerContainer.Visible = false;
                        TabStructure.Visible = false;
                    }
                    string srchMsg = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/availablehotels");
                    string currentLanguage = Utility.GetCurrentLanguage();
                    if (currentLanguage != null && currentLanguage.IndexOf("de", StringComparison.Ordinal) >= 0)
                    {
                        string[] srchMsgArray = srchMsg.Split(',');
                        string srchPreMsg = srchMsgArray[0];
                        string srchPostMsg = srchMsgArray[1];
                        string[] srchPostMsgArray = srchPostMsg.Split(' ');
                        txtAvailabilityMsg1.Value = string.Format("{0}, {1} {2} {3} {4} {5} {6}", srchPreMsg,
                                                                  srchPostMsgArray[1],
                                                                  srchPostMsgArray[2], srchPostMsgArray[3],
                                                                  srchPostMsgArray[4], cityName.Text,
                                                                  srchPostMsgArray[5]);
                    }
                    else
                        txtAvailabilityMsg1.Value = string.Format("{0} {1}", srchMsg, cityName.Text);
                }
            }
            catch (ContentDataAccessException cdaException)
            {
                if (string.Equals(cdaException.ErrorCode, AppConstants.BONUS_CHEQUE_NOT_FOUND,
                                  StringComparison.InvariantCultureIgnoreCase))
                {
                    errorLabel.InnerText = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/bonuschequenotfound");
                    errorAlertDiv.Style.Add("display", "block");
                    subHeader.Visible = false;
                    footerContainer.Visible = false;
                    TabStructure.Visible = false;
                }
                else
                    WebUtil.ApplicationErrorLog(cdaException);
            }
        }


        #endregion

        #region INavigationTraking Members

        public List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> paremters = new List<KeyValueParam>(1);
            paremters.Add(new KeyValueParam("Selected Hotel Id", Request.QueryString["SelectedHotelId"]));
            return paremters;
        }

        #endregion
    }
}