<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfirmCancellation.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ConfirmCancellation" %>
<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx" TagName="ReservationInformation" TagPrefix="Booking" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<div id="Reservation" class="BE">
	<!-- Step2: Select Rate -->
	<div id="ModifyOrCancel">
	<!--	<form name="ModifyBookingForm">-->
	        <!--<p>Are you sure you would like to cancel the following booking?</p>-->
	        <p><%=WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelConfirmationText")%></p>
			<Booking:ReservationInformation ID="ReservationInfo" runat="server">
            </Booking:ReservationInformation>
			<!-- LinksCont -->
			<div id="LinksCont">
				<div class="alignLeft">
					<span class="btnSubmit">
						<asp:LinkButton ID="btnNo" runat="server" OnClick="btnNo_Click"><span><%=WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/No")%></span></asp:LinkButton>	
					</span>
				</div>
				<div class="alignRight">
					<span class="btnSubmit">
						<!--<a href="#"><span>Cancel booking</span></a>-->
						<asp:LinkButton ID="btnCancelBooking" runat="server" OnClick="btnCancelBooking_Click"><span><%=WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelBooking")%></span></asp:LinkButton>	
					</span>
				</div>
				<div class="clear">&nbsp;</div>
			</div>
	</div>	
</div>


