<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingReceiptInformation.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.BookingReceiptInformation" %>
<%@ Register Src="~/Templates/Booking/Units/BookingReceiptInformationContainer.ascx"
    TagName="BookingReceiptInfoContainer" TagPrefix="Booking" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<link rel="stylesheet" type="text/css" media="print" href="/Templates/Scanweb/Styles/Default/Print.css" />
<link rel="stylesheet" type="text/css" media="print" href="/Templates/Booking/Styles/Default/print.css" />
<link rel="stylesheet" type="text/css" href="/Templates/Booking/Styles/Default/reservation2.0.css" />

<% if (Request.QueryString["command"] != null && Request.QueryString["command"] == "print")
   { %>
<div id="printerFriendlySection" runat="server" visible="false">
    <div id="confirmationPrint" class="confirmationPrintReceipt">
        <div class="receiptCopyWatermark" id="watermarkImage" runat="server">
        <img src"/templates/Booking/Styles/Default/images/reservation2.0/copy-watermark.png" />
        </div>
        <div class="clearFix" id="pfPrintDiv" runat="server">
            <!-- Print Button -->
            <div class="actionBtn fltRt">
                <asp:LinkButton ID="pfLnkBtnPrint" runat="server" CssClass="buttonInner cPrint"><%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/bookingdetail/print")%></asp:LinkButton>
                <asp:LinkButton ID="pfSpnBtnPrint" runat="server" CssClass="buttonRt scansprite"></asp:LinkButton>
            </div>
        </div>
        <!-- Title -->
        <h1>
            <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/CreditCardReceipt")%>
        </h1>
        <div id="creditCardInfoSection" runat="server" >
            <table class="receiptDescSection">
                <tr>
                    <td style="text-align: center; width: 50px">
                        <div id="divPleaseNoteLogoBk" class="receiptPleaseNoteLogo receiptPleaseNoteLogoBk" runat="server" >
                        </div>
                        <div id="divPleaseNoteLogo" runat="server">
                        <img class="receiptPleaseNoteLogo" src"/templates/Booking/Styles/Default/images/reservation2.0/pleaseNote.gif" />
                        </div>
                    </td>
                    <td>
                        <p class="receiptDescription">
                            <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/CreditCardReceiptInfo")%>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
        <div id="pfMasterReserMod21" runat="server">
            <Booking:BookingReceiptInfoContainer ID="pfReservationInfo" runat="server" />
        </div>
        
        <br />
        <!-- Merchant Information -->
        <div class="section" runat="server" id="divMerchantInformation">
            <table>
                <tr>
                    <td class="dataCol1">
                        <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/CompanyName")%>
                    </td>
                   
                    <td class="dataCol2 merchantTd">
                        <span id="lblPDFCompanyName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="dataCol1">
                        <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/OrganisationNumber")%>
                    </td>
                    
                     <td class="dataCol2 merchantTd">
                        <span id="lblPDFOrganisationNumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="dataCol1">
                        <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/Hotel")%>
                    </td>
                  
                    <td class="dataCol2 merchantTd">
                        <span id="lblPDFHotel" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="dataCol1">
                        <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/VisitingAddress")%>
                    </td>
                 
                    <td class="dataCol2 merchantTd">
                        <span id="lblPDFVisitingAddress" runat="server" />
                    </td>
                </tr>  
                    <tr>
                    <td class="dataCol1">
                        <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/Phone")%>
                    </td>                   
                    <td class="dataCol2 merchantTd">
                        <span id="lblPDFPhone" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="dataCol1">
                        <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/Fax")%>
                    </td>
                
                    <td class="dataCol2 merchantTd">
                        <span id="lblPDFFax" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="dataCol1">
                        <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/Email")%>
                    </td>
                  
                    <td class="dataCol2 merchantTd">
                        <span id="lblPDFEmail" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="dataCol1">
                        <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/HomePage")%>
                    </td>
                
                    <td class="dataCol2 merchantTd" id="lblPDFHomePage"  runat="server" >                        
                    </td>
                </tr>               
                <tr>
                    <td class="dataCol1">
                        <%=WebUtil.GetTranslatedTextForReceipt("/bookingengine/booking/BookingReceipt/MerchantID")%>
                    </td>
             
                    <td class="dataCol2 merchantTd">
                        <span id="lblPDFMerchantID" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<%} %>
