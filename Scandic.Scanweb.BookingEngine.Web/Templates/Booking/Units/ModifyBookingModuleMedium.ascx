<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModifyBookingModuleMedium.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ModifyBookingModuleMedium" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register Src="DestinationSearch.ascx" TagName="DestinationSearch" TagPrefix="uc1" %>
<%@ Register Src="Calendar.ascx" TagName="Calendar" TagPrefix="uc2" %>
<div id="BookingEngine" class="BE">
<!-- Booking Area Big -->
	<div id="BookingAreaMedium">
	<div id="BookingArea-inner">
		<!-- Booking header -->
		<div id="BookingHeader">
			<!-- Get the language code and load the corresponding image. -->
		    <% string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();%>
			<div id="imgContainer" class="RefineSearch_<%= siteLanguage %>">
				<h1><%= WebUtil.GetTranslatedText("/bookingengine/booking/ModifySelectRate/refineyourbooking") %></h1>			
				<select visible="false" id="ddlBookingType" name="ddlBookingType" onchange="hideClientError(); showTabContent(('Tab'+this.value));"  runat="server">
				</select>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<!-- \Booking header -->
		<div id="BookingContent">
			<!-- Tab Contents -->
			<div id="TabContents">
					<div id="clientErrorDiv" runat="server"></div>					

					<div class="errorDivClient">
					    <input type="hidden" id="siteLang" name="siteLang" value="<%= siteLanguage %>" />
					    <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
					    <input type="hidden" id="arrivaldateError" name="arrivaldateError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/arrival_date") %>' />
						<input type="hidden" id="departuredateError" name="departuredateError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/departure_date") %>' />
						<input type="hidden" id="NoOfNightError" name="NoOfNightError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/no_of_nights") %>' />
														
						
					</div>
					<div class="formInputs">
					  <!-- Common Controls -->
					  <div id="commonControls">
						<div class="formGroupOne">
							<div class="formColumn1">
							    <div id="destCont">&nbsp;</div>
							</div>
							<div class="clear">&nbsp;</div>
					        <p class="formRowColumn1">
						        <span id="atCont">
						        <label><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/arrivaldate") %></label>
						        <br />
						        <input class="inputTextSmall dateRange" tabindex="3" type="text" id="txtArrivalDate" runat="server"/>
						        </span>
					        </p>
					        <p class="formRowColumnSpl">
						        <span id="dtCont">
						        <label><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/departuredate") %></label>
						        <br />
						        <input class="inputTextSmall dateRange" tabindex="4" type="text" id="txtDepartureDate" runat="server"/>
						        </span>
					        </p>
						    </div>
						
						<div class="clear">&nbsp;</div>
						<p class="formRowColumn1">
							<span id="nnCont">
							<label><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/noofnights") %></label>
							<br />
							<input class="inputTextSmall" tabindex="5" type="text" id="txtNoOfNights" size="3" maxlength="2" value="1" runat=server/>
							</span>
						</p>
						<div id="MultiLeg">
						<p class="formRowColumn2">
							<label><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/noofrooms") %></label>
							<br />
							 <asp:DropDownList ID="ddlNoOfRooms" tabindex="6" runat="server" CssClass="selBoxMedium">
                            </asp:DropDownList>
						</p>
						</div>
						<div id="SingleLeg" style="display:none">
						<p class="formRowColumn2">
							<label><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/noofrooms") %></label>
							<br />
							<label id="lblNoOfRooms">1</label>
						</p>
						</div>
						<p class="clear">&nbsp;</p>
						<p class="formRowColumn1">
							<label><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %></label>
							<asp:DropDownList ID="ddlAdultsPerRoom" runat="server" CssClass="selBoxMedium">
                            </asp:DropDownList>
						</p>
						<p class="formRowColumn2">
							<label><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children") %></label>
							<asp:DropDownList ID="ddlChildPerRoom" runat="server" CssClass="selBoxMedium">
                            </asp:DropDownList>
						</p>
						<p class="clear">&nbsp;</p>
					</div>
					  <!-- /Common Controls -->
					</div>
										
					<div id="commonFooter" class="formFooter">
					<div class="formRow"></div>
						<div class="formRowButton">
							<span class="btnSubmit" >
							    <asp:LinkButton ID="LinkButton2"  OnClick="Search_Click" runat="server"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/ModifySelectRate/searchone") %></span></asp:LinkButton>
							</span>
						</div>
						<div class="clear">&nbsp;</div>
					</div>
			</div>
			<!-- /Tab Contents -->
		</div>
	</div>
</div>
<!-- Booking Area Big -->
<div id="emptyDiv"></div>
<div id="hiddenCalendar" class="calendarContainer">
	<table width="186" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td align="left" valign="top"><div id="calendarLayer">&nbsp;</div>
				<div id="calendarBase"></div></td>
		</tr>
	</table>
</div>
<div class="clear">&nbsp;</div>
</div>

<div id="destinationSearch">
    <uc1:DestinationSearch id="DestinationSearch1" runat="server" />    
</div>

<div id="calendar">
    <uc2:Calendar id="Calendar1" runat="server"/>
</div>

<script type="text/javascript" language="javascript">
       initRefineBooking();
</script>