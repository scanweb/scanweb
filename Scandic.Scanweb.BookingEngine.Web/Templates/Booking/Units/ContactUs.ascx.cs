#region Description

////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: Code Behind For ContactUs user control    			  //
//								  Basic Functionality of this code base is populate       //
//                                hotel contact information,and collecting information    //
//                                of user input and send it.                              //    
//----------------------------------------------------------------------------------------//
/// Author						: Raj Kishore Marandi	                                  //
/// Author email id				:                           							  //
/// Creation Date				: 19th November  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revision History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#endregion Description

#region SYSTEM NAMESPACE

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion SYATEM NAMESPACE

    #region EpiServer NAMESPACE

    #endregion EpiServer NAMESPACE

    #region SCANDIC NAMESPACE

#endregion SCANDIC NAMESPACE

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// ContactUs
    /// </summary>
    public partial class ContactUs : EPiServer.UserControlBase
    {
        #region PROTECTED METHODS

        #region PAGE LOAD

        /// <summary>
        /// Page Load method which will initialize EmailConfirmation User Control and sets user information on contact us user control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible && WebUtil.IsSessionValid())
            {
                if (UserLoggedInSessionWrapper.UserLoggedIn == true)
                {
                    ContactUsEmailConfirmation.Visible = false;
                    if (!IsPostBack)
                    {
                        try
                        {
                            InitializeEmailConfirmationUserControl();
                            PopulateUserInformation();
                            LinkButtonSend.Attributes.Add("onclick",
                                                          "javascript:return performValidation(PAGE_CONTACT_US);");
                            spnButtonSend.Attributes.Add("onclick",
                                                         "javascript:return performValidation(PAGE_CONTACT_US);");
                        }
                        catch (Exception genEx)
                        {
                            WebUtil.ApplicationErrorLog(genEx);
                        }
                    }
                }
                else
                {
                    AppLogger.LogInfoMessage("User is already logged out - Still accessing Contact Us Functionality.");
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.LOGIN_FOR_DEEPLINK), false);
                }
            }
        }

        #endregion PAGE LOAD

        #region CLICK SEND BUTTON

        /// <summary>
        /// Fired when the send button is Clicked.It sends Email message.
        /// It hides Contactus user control and display Email Confirmation on successful send of Email.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        protected void ButtonSend_Click(object sender, EventArgs e)
        {
            try
            {
                SendEmailMessage();
                ContactusDiv.Visible = false;
                ContactUsEmailConfirmation.Visible = true;
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }
        }

        #endregion CLICK SEND BUTTON

        #endregion PROTECTED METHODS

        #region PRIVATE METHODS

        #region InitializeEmailConfirmationUserControl

        /// <summary>
        /// It initialize Email confirmation user control.
        /// </summary>
        private void InitializeEmailConfirmationUserControl()
        {
            ContactUsEmailConfirmation.AssignHeader(
                WebUtil.GetTranslatedText("/bookingengine/booking/contactus/contactusmessageheader"));
            ContactUsEmailConfirmation.AssignMessage(
                WebUtil.GetTranslatedText("/bookingengine/booking/contactus/contactusmessagedescription"));
            ContactUsEmailConfirmation.AssignLink(
                WebUtil.GetTranslatedText("/bookingengine/booking/contactus/contactusspecialoffers"));
        }

        #endregion InitializeEmailConfirmationUserControl

        #region PopulateUserInformation

        /// <summary>
        /// Populate User Information e.g street address
        /// post code,city etc.. on Contact US user control.
        /// Invoked by page load event call
        /// </summary>
        private void PopulateUserInformation()
        {
            LoyaltyDetailsEntity loyalityInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails as LoyaltyDetailsEntity;

            if (loyalityInfoDetails != null)
            {

                txtName.Value = loyalityInfoDetails.FirstName + " " + loyalityInfoDetails.SurName;
                txtEmailAddress.Value = (loyalityInfoDetails.Email != null ? loyalityInfoDetails.Email : string.Empty);
                lblCity.InnerText = (loyalityInfoDetails.City != null ? loyalityInfoDetails.City : string.Empty);
                string countryName = string.Empty;
                if (loyalityInfoDetails.Country != null)
                {
                    countryName = Utility.GetCountryName(loyalityInfoDetails.Country);
                }
                lblCountry.InnerText = countryName;
                if (loyalityInfoDetails.Fax == null)
                {
                    faxHeading.Visible = false;
                }
                else
                {
                    lblFaxNumber.InnerText = loyalityInfoDetails.Fax;
                }

                lblPhoneNumber.InnerText = (loyalityInfoDetails.Telephone1 != null
                                                ? loyalityInfoDetails.Telephone1
                                                : string.Empty);
                lblPostCode.InnerText = (loyalityInfoDetails.PostCode != null
                                             ? loyalityInfoDetails.PostCode
                                             : string.Empty);
                if (loyalityInfoDetails.AddressLine2 != null)
                {
                    lblStreet.InnerText = (loyalityInfoDetails.AddressLine1 != null
                                               ? loyalityInfoDetails.AddressLine1
                                               : string.Empty) + "," + loyalityInfoDetails.AddressLine2;
                }
                else
                {
                    lblStreet.InnerText = loyalityInfoDetails.AddressLine1;
                }
                PopulateSubjects();
            }
        }

        #endregion PopulateUserInformation

        #region PopulateSubjects

        /// <summary>
        /// Method to Populate Subject
        /// Combo Box on Contact US user control.
        /// </summary>
        private void PopulateSubjects()
        {
            OrderedDictionary subjectMap = DropDownService.GetSubject();
            if (subjectMap != null)
            {
                foreach (string key in subjectMap.Keys)
                {
                    ddlSubject.Items.Add(new ListItem(subjectMap[key].ToString(), key));
                }
                ListItem selectedDefaultItem = ddlSubject.Items.FindByValue(AppConstants.DEFAULT_VALUE_CONST);
                selectedDefaultItem.Selected = true;
            }
        }

        #endregion PopulateSubjects

        #region GetContact Us SC Confirmation Details

        /// <summary>
        /// Collect the Missing point Confirmation Information to be send as Email
        /// </summary>
        /// <returns>OrderedDictionary</returns>
        private Dictionary<string, string> GetContactUsEmailSCConfirmationDetails()
        {
            string valueNotPresent = AppConstants.SPACE;
            Dictionary<string, string> ContactUsEmailConfirmationMap = new Dictionary<string, string>();
            LoyaltyDetailsEntity loyaltyInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails as LoyaltyDetailsEntity;

            if (loyaltyInfoDetails != null)
            {
                ContactUsEmailConfirmationMap[CommunicationTemplateConstants.FIRST_NAME] =
                    (loyaltyInfoDetails.FirstName == null ? valueNotPresent : loyaltyInfoDetails.FirstName);
                ContactUsEmailConfirmationMap[CommunicationTemplateConstants.LASTNAME] = (loyaltyInfoDetails.SurName ==
                                                                                          null
                                                                                              ? valueNotPresent
                                                                                              : loyaltyInfoDetails.
                                                                                                    SurName);
                ContactUsEmailConfirmationMap[CommunicationTemplateConstants.CITY] = (loyaltyInfoDetails.City == null
                                                                                          ? valueNotPresent
                                                                                          : loyaltyInfoDetails.City);
                ContactUsEmailConfirmationMap[CommunicationTemplateConstants.ADDRESS_LINE1] =
                    (loyaltyInfoDetails.AddressLine1 == null ? valueNotPresent : loyaltyInfoDetails.AddressLine1);
                ContactUsEmailConfirmationMap[CommunicationTemplateConstants.ADDRESS_LINE2] =
                    (loyaltyInfoDetails.AddressLine2 == null ? valueNotPresent : loyaltyInfoDetails.AddressLine2);
                ContactUsEmailConfirmationMap[CommunicationTemplateConstants.EMAIL] = (loyaltyInfoDetails.Email == null
                                                                                           ? valueNotPresent
                                                                                           : loyaltyInfoDetails.Email);
                ContactUsEmailConfirmationMap[CommunicationTemplateConstants.POST_CODE] =
                    (loyaltyInfoDetails.PostCode == null ? valueNotPresent : loyaltyInfoDetails.PostCode);
                ContactUsEmailConfirmationMap[CommunicationTemplateConstants.MESSAGE] = txtComposedMessage.Value;
                ContactUsEmailConfirmationMap[CommunicationTemplateConstants.SUBJECT] = ddlSubject.SelectedItem.Text;
                ContactUsEmailConfirmationMap[CommunicationTemplateConstants.MEMBERSHIPNUMBER] =
                    loyaltyInfoDetails.UserName;
                if (loyaltyInfoDetails.Fax != null)
                {
                    ContactUsEmailConfirmationMap[CommunicationTemplateConstants.FAX] = loyaltyInfoDetails.Fax;
                }
                else
                {
                    ContactUsEmailConfirmationMap[CommunicationTemplateConstants.NO_FAX] = AppConstants.SPACE;
                }
                if (loyaltyInfoDetails.Telephone1 != null)
                {
                    ContactUsEmailConfirmationMap[CommunicationTemplateConstants.TELEPHONE1] =
                        loyaltyInfoDetails.Telephone1;
                }
                else
                {
                    ContactUsEmailConfirmationMap[CommunicationTemplateConstants.NO_PHONE] = AppConstants.SPACE;
                }
            }

            return ContactUsEmailConfirmationMap;
        }

        #endregion GetContact Us SC Confirmation Details
      
        #region SendEmailMessage

        /// <summary>
        /// This will send email to the 
        /// </summary>
        private void SendEmailMessage()
        {
            LoyaltyDetailsEntity loyaltyInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails as LoyaltyDetailsEntity;
            try
            {
                Dictionary<string, string> emailConfirmationMap = GetContactUsEmailSCConfirmationDetails();
                EmailEntity emailEntity = CommunicationUtility.GetContactUsSCConfirmationEmail(emailConfirmationMap,
                                                                                               loyaltyInfoDetails.Email,
                                                                                               chkCopy.Checked);
                CommunicationService.SendMail(emailEntity, null);
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx);
            }
        }

        #endregion SendEmailMessage

        #endregion PRIVATE METHODS
    }
}