//  Description					: Code Behind class for about our rates Control          //
//	This will hold controls for displaying room rates.  						         //
//----------------------------------------------------------------------------------------//
/// Author						: Ranajit Nayak                                   	      //
/// Author email id				:                           							  //
/// Creation Date				: 24rd June  2010									      //
///	Version	#					: 2.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.ExceptionManager;

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code behind of AboutOurRates control.
    /// </summary>
    public partial class AboutOurRates : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ArrayList addedRateCategoriesList = new ArrayList();
                List<RateCategory> rateList = new List<RateCategory>();
                IList<BaseRoomRateDetails> listRoomRateDetails = null;
                if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null)
                    listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                List<string> uniqueCategories = new List<string>();
                List<RateDescription> uniqueRateDescriptionBoxes = new List<RateDescription>();

                PageData pageData = GetPage(PageReference.RootPage);
                PageReference SelectRatePageData = (PageReference) pageData[EpiServerPageConstants.SELECT_RATE_PAGE];
                PageData selectRatePage = GetPage(SelectRatePageData);
                aboutOurRateHeading.Text = selectRatePage["AboutOurRatesHeading"] as string ?? string.Empty;
                Reservation2SessionWrapper.AboutOurRateHeading = selectRatePage["AboutOurRatesHeading"] as string ??
                                                                 string.Empty;
                aboutOurRateDescription.Text = selectRatePage["AboutOurRateDescription"] as string ?? string.Empty;
                Reservation2SessionWrapper.AboutOurRateDescription =
                    selectRatePage["AboutOurRateDescription"] as string ??
                    string.Empty;

                if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                {
                    for (int CategoryCount = 0; CategoryCount < listRoomRateDetails.Count; CategoryCount++)
                    {
                        BaseRoomRateDetails roomRateDetails = listRoomRateDetails[CategoryCount];
                        BaseRateDisplay rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                        RearrangeCoulmnNumber(rateDisplay.RateCategoriesDisplay);
                        foreach (RateCategoryHeaderDisplay rate in rateDisplay.RateCategoriesDisplay)
                        {
                            if (!uniqueCategories.Contains(rate.Id))
                            {
                                uniqueCategories.Add(rate.Id);
                                RateDescription rateDescription = LoadControl("RateDescription.ascx") as RateDescription;
                                rateDescription.RateCaegoryDetails(rate, CategoryCount);
                                uniqueRateDescriptionBoxes.Add(rateDescription);
                            }
                        }
                    }

                    foreach (RateDescription rateDescription in uniqueRateDescriptionBoxes)
                    {
                        AboutRate.Controls.Add(rateDescription);
                    }
                }
                if (listRoomRateDetails == null)
                {
                    Hashtable selectedRoomRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                    SelectedRoomAndRateEntity selectedRoomRates = null;
                    Block block = null;
                    RateCategory rate = null;
                    string RatePlanCode = string.Empty;
                    List<RateDescriptionforEMAIL> uniqueModifyDatesRateDescriptionBoxes =
                        new List<RateDescriptionforEMAIL>();
                    if (selectedRoomRatesHashTable != null)
                    {
                        for (int i = 0; i < selectedRoomRatesHashTable.Count; i++)
                        {
                            selectedRoomRates = selectedRoomRatesHashTable[i] as SelectedRoomAndRateEntity;
                            if (selectedRoomRates != null && selectedRoomRates.RoomRates != null)
                            {
                                if (Utility.IsBlockCodeBooking)
                                {
                                    block =
                                        Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(
                                            SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                                    if ((block != null) && (!uniqueCategories.Contains(block.BlockID)))
                                    {
                                        uniqueCategories.Add(block.BlockID);
                                        RateDescriptionforEMAIL rateDescriptionForBlock =
                                            LoadControl("/Templates/Booking/Units/RateDescriptionforEMAIL.ascx") as
                                            RateDescriptionforEMAIL;
                                        rateDescriptionForBlock.RateCategoryDetails(block);
                                        uniqueModifyDatesRateDescriptionBoxes.Add(rateDescriptionForBlock);
                                    }
                                }
                                else if (selectedRoomRates.RoomRates.Count > 0 &&
                                         selectedRoomRates.RoomRates[0].RatePlanCode != null)
                                {
                                    RatePlanCode = selectedRoomRates.RoomRates[0].RatePlanCode;
                                    rate = RoomRateUtil.GetRateCategoryByRatePlanCode(RatePlanCode);
                                    if (!uniqueCategories.Contains(rate.RateCategoryId))
                                    {
                                        uniqueCategories.Add(rate.RateCategoryId);
                                        RateDescriptionforEMAIL rateDescription =
                                            LoadControl("/Templates/Booking/Units/RateDescriptionforEMAIL.ascx") as
                                            RateDescriptionforEMAIL;
                                        rateDescription.RateCaegoryDetails(rate);
                                        uniqueModifyDatesRateDescriptionBoxes.Add(rateDescription);
                                    }
                                }
                            }
                        }
                    }

                    foreach (RateDescriptionforEMAIL rateDescription in uniqueModifyDatesRateDescriptionBoxes)
                    {
                        AboutRate.Controls.Add(rateDescription);
                    }
                    if (Reservation2SessionWrapper.AboutOurRateHeading != null)
                        aboutOurRateHeading.Text = Reservation2SessionWrapper.AboutOurRateHeading;
                    if (Reservation2SessionWrapper.AboutOurRateDescription != null)
                        aboutOurRateDescription.Text = Reservation2SessionWrapper.AboutOurRateDescription;
                }
            }
            catch (ContentDataAccessException ex)
            {
                if (!string.Equals(ex.ErrorCode, AppConstants.BONUS_CHEQUE_NOT_FOUND))
               { throw ex; }
            }
        }

        /// <summary>
        /// This method will rearrange the coulmn number.
        /// </summary>
        /// <param name="headerList">Header list for different category</param>
        /// <remarks>Release 1.8.1 | Hygiene release | Rate structure display</remarks>
        private void RearrangeCoulmnNumber(List<RateCategoryHeaderDisplay> headerList)
        {
            string totalCoulmnAccepted = ConfigurationManager.AppSettings.Get("SelectRate.NoOfRateCoulmn");
            bool isCoulmnNumberValidFormat = false;
            int totalCoulmnConfigured = 0;
            if (!string.IsNullOrEmpty(totalCoulmnAccepted))
            {
                isCoulmnNumberValidFormat = int.TryParse(totalCoulmnAccepted, out totalCoulmnConfigured);
            }
            int headerCount = headerList.Count;
            headerList.Sort(new SortHeaderDisplay());
            for (int count = 0; count < headerCount; count++)
            {
                headerList[count].CoulmnNumber = totalCoulmnConfigured;
                totalCoulmnConfigured--;
            }
            headerList.Sort(new RateCategoryCoulmnComparer());
        }
    }
}