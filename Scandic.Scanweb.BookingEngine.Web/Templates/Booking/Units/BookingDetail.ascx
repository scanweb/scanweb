<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingDetail.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.BookingDetail" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register Src="~/Templates/Booking/Units/UsefulLinksContainer.ascx" TagName="UsefulLinks" TagPrefix="Booking" %>
<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx" TagName="ReservationInformation" TagPrefix="Booking" %>
<%@ Register Src="~/Templates/Booking/Units/UserBookingContactDetails.ascx" TagName="BookingContactDetails" TagPrefix="Booking" %>
<%@ Register TagPrefix="Scanweb" TagName="ProgressBar" Src="~/Templates/Booking/Units/ProgressBar.ascx" %>
<script language="javascript" type="text/javascript">    
</script>
<div class="BE">
<!-- Progress Tab -->
		
		    <!-- If the booking is done with out kids i.e. no of children = 0, 
		    then the div with "pt_4" would be displayed. 
		    Or else if the no of children > 0, then the div with "pt_5" would be displayed. -->
		    <% if (!Utility.IsFamilyBooking())
               {%>
		    <%--<div id="pt_3" >
			    <ul>
				    <li class="pt_num firstTick">&nbsp;</li>
				    <li class="text visited"><a href="<%=GlobalUtil.GetUrlToPage("ReservationSelectHotelPage")%>"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selecthotel")%></a></li>
				    <li class="pt_arrow1 lastvisitedArrow">&nbsp;</li>
				    
				    <li class="pt_num tick">&nbsp;</li>
				    <li class="text visited"><a href="<%=GlobalUtil.GetUrlToPage("ReservationSelectRatePage")%>"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selectrate")%></a></li>
				    <li class="pt_arrow1 visitedArrow">&nbsp;</li>
				    
				    <li class="pt_num three">&nbsp;</li>
				    <li class="text active"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/bookingdetails")%></li>
				    <li class="pt_arrow1 activeArrow">&nbsp;</li>
				    
				    <li class="pt_num four">&nbsp;</li>
				    <li class="text"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/confirmation")%></li>
				    <li class="pt_last">&nbsp;</li>
			    </ul>
			</div>--%>
		    <% }
               else
               { %>
			<%--<div id="pt_4">
				<ul>				    
                    <li class="pt_num firstTick">&nbsp;</li>
                    <li class="text visited"><a href="<%=GlobalUtil.GetUrlToPage("ReservationChildrensDetailsPage")%>"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/childrendetail")%></a></li>
                    <li class="pt_arrow1 lastvisitedArrow">&nbsp;</li>
										
                    <li class="pt_num tick">&nbsp;</li>
                    <li class="text visited"><a href="<%=GlobalUtil.GetUrlToPage("ReservationSelectHotelPage")%>"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selecthotel")%></a></li>
                    <li class="pt_arrow1 lastvisitedArrow">&nbsp;</li>          
                                        
					<li class="pt_num tick">&nbsp;</li>
					<li class="text visited"><a href="<%=GlobalUtil.GetUrlToPage("ReservationSelectRatePage")%>"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selectrate")%></a></li>
					<li class="pt_arrow1 visitedArrow">&nbsp;</li>
					
					<li class="pt_num four">&nbsp;</li>
					<li class="text active"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/bookingdetails")%></li>
					<li class="pt_arrow1 activeArrow">&nbsp;</li>
					
					<li class="pt_num five">&nbsp;</li>
					<li class="text"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/confirmation")%></li>
					<li class="pt_last">&nbsp;</li>
				</ul>
			</div>--%>
			<% } %>
			
	<!-- Step3: Enter Booking Details -->
		
			
				<div class="tripleCol flushMarginRight">	
	<div id="BookingDetails">
	
        <%--<Booking:ReservationInformation ID="ReservationInfo" runat="server">
        </Booking:ReservationInformation>--%>
       <%-- <Booking:UsefulLinks ID="UsefulLinks" runat="server">
        </Booking:UsefulLinks>   --%>       
        <Scanweb:ProgressBar id="ProgressBar" runat="server" />    
        <Booking:BookingContactDetails ID="BookingContactDetails" runat="server">
        </Booking:BookingContactDetails>        
    </div>
    </div>
    
    
	<!-- /hotel Info -->

</div>

