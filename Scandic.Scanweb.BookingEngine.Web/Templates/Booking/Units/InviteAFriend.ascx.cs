#region Description

////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: Code behind for Invite a friend functionality			  //
//                                This is after login functionality.User can suggest name //
//                                of the multiple friends and their email id.This is the  // 
//                                additional value added service provided by Scandic      //
//                                hotels.Mainly this functionality collect names and email//
//                                id of friends and send it to Scandic hotel.             //  
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Raj Kishore Marandi	                                  //
//  Author email id				:                           							  //
//  Creation Date				: 27th December  2007									  //
// 	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
// Revision History				: -NA-												      //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#endregion Description

#region System NameSpace

using System;
using System.Collections.Generic;
using System.Web.UI;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion System NameSpace

    #region Scandic Namespace

    #endregion Scandic Namespace

    #region EpiServer NameSpace

#endregion EpiServer NameSpace

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// This class is responsible for collecting information about a user's friend and their emailid.
    /// After collecting this send all these information to Scandic.
    /// After successful sending of this email confirmation message will be displayed.  
    /// </summary>
    public partial class InviteAFriend : EPiServer.UserControlBase
    {
        #region Protected Method

        #region Page_Load

        /// <summary>
        /// Page load event of this user control.
        /// Responsible for 
        ///     1.Setting all the information fields for Emil Confirmation functionality.
        ///     2.Sets the attribute of inviteFriend button.
        ///     3.Generate Two text boxes and two labels initially when control is loaded.
        /// All above are done when page is first loaded.It wont repeat on subsequent page load.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Event Argument</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (UserLoggedInSessionWrapper.UserLoggedIn == true)
                {
                    try
                    {
                        if (!IsPostBack)
                        {
                            EmailConfirmationMsg.Visible = false;
                            SetEmailConfirmationMessage();
                            btnInviteFriend.Attributes.Add("onclick",
                                                           "javascript:return performValidation(PAGE_INVITE_A_FRIEND);");
                        }
                    }
                    catch (Exception ex)
                    {
                        Scanweb.Core.AppLogger.LogFatalException(ex,
                                                                 "Page Load - InviteAFriend functionality has Error.. ");
                    }
                }
                else
                {
                    Scanweb.Core.AppLogger.LogInfoMessage(
                        "User is already logged out - Still accessing Invite A Friend Functionality.");
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.LOGIN_FOR_DEEPLINK), false);
                }
            }
        }

        #endregion Page_Load

        #region btnInviteFriend_Click

        /// <summary>
        /// This method is fired when user is  finished filling all the fields and click on it.
        /// Responsible for 
        ///     1.Finding out the generated string stored in the hidden field named "store". 
        ///     2.Splitting out the names and emilids from that string and sending the email.
        /// 
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Event Arguments</param>
        protected void btnInviteFriend_Click(object sender, EventArgs e)
        {
            try
            {
                Control cl = this.FindControl("storage");
                string collectedValues = ((System.Web.UI.HtmlControls.HtmlInputHidden) cl).Value;

                string[] eachValue = collectedValues.Split(new Char[] {';'});
                Dictionary<string, string> nameEmailPair;
                Dictionary<int, Dictionary<string, string>> masterList =
                    new Dictionary<int, Dictionary<string, string>>();
                for (int counter = 0; counter < eachValue.Length; counter++)
                {
                    string[] pair = eachValue[counter].Split(new char[] {','});
                    nameEmailPair = new Dictionary<string, string>();
                    nameEmailPair.Add(pair[0], pair[1]);
                    masterList.Add(counter, nameEmailPair);
                }
                try
                {
                    SendEmail(masterList);
                    InviteAFriendDiv.Visible = false;
                    EmailConfirmationMsg.Visible = true;
                }
                catch (Exception exMembership)
                {
                    WebUtil.ApplicationErrorLog(exMembership);
                }
                try
                {
                    SendEmailToUser();
                }
                catch (Exception exUser)
                {
                    WebUtil.ApplicationErrorLog(exUser);
                }
            }
            catch (Exception genEx)
            {
                Scanweb.Core.AppLogger.LogFatalException(genEx, "Invite A Friend - Inivite button click has problem..");
            }
        }

        #endregion btnInviteFriend_Click

        #endregion Protected Method

        #region Private Method

        #region SetEmailConfirmationMessage

        /// <summary>
        /// This will set the email Confirmation control.This method is called when fresh page is loaded for first time.
        /// </summary>
        private void SetEmailConfirmationMessage()
        {
            EmailConfirmationMsg.AssignHeader(
                WebUtil.GetTranslatedText("/bookingengine/booking/inviteafriend/inviteafriendmessageheader"));
            EmailConfirmationMsg.AssignMessage(
                WebUtil.GetTranslatedText("/bookingengine/booking/inviteafriend/inviteafriendmessgedescription"));
            EmailConfirmationMsg.AssignLink(
                WebUtil.GetTranslatedText("/bookingengine/booking/inviteafriend/inviteafriendpecialoffers"));
        }

        #endregion SetEmailConfirmationMessage

        #region SendEmail

        /// <summary>
        /// This will send the Email to Scandic hotels.
        /// Responsible for 
        ///     1.Fetching To address and CC address of Scandic.
        ///     2.Fetching email template.
        ///     3.Sending Email.
        /// </summary>
        /// <param name="masterList"></param>
        private void SendEmail(Dictionary<int, Dictionary<string, string>> masterList)
        {
            LoyaltyDetailsEntity loyalityInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;

            try
            {
                Dictionary<string, string> emailConfirmationMap = GetIniviteAFriendConfirmationDetails();
                EmailEntity emailEntity = CommunicationUtility.GetInviteAFriendConfirmationEmail(emailConfirmationMap,
                                                                                                 masterList);

                CommunicationService.SendMail(emailEntity, null);
            }
            catch (Exception genEx)
            {
                Scanweb.Core.AppLogger.LogFatalException(genEx, "Email could not send in Invite A Friend screen.");
            }
        }

        #endregion SendEmail

        #region GetIniviteAFriendConfirmationDetails

        /// <summary>
        /// Collect the InviteAFriend Map Information to be send as Email
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> GetIniviteAFriendConfirmationDetails()
        {
            string valueNotPresent = CommunicationTemplateConstants.BLANK_SPACES;
            Dictionary<string, string> InviteAFriendConfirmationMap = new Dictionary<string, string>();

            LoyaltyDetailsEntity loyaltyInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails as LoyaltyDetailsEntity;

            if (loyaltyInfoDetails != null)
            {
                InviteAFriendConfirmationMap[CommunicationTemplateConstants.FIRST_NAME] =
                    (loyaltyInfoDetails.FirstName == null ? valueNotPresent : loyaltyInfoDetails.FirstName);
                InviteAFriendConfirmationMap[CommunicationTemplateConstants.LASTNAME] =
                    (loyaltyInfoDetails.SurName == null ? valueNotPresent : loyaltyInfoDetails.SurName);
                InviteAFriendConfirmationMap[CommunicationTemplateConstants.EMAIL] =
                    (loyaltyInfoDetails.Email == null ? valueNotPresent : loyaltyInfoDetails.Email);
                InviteAFriendConfirmationMap[CommunicationTemplateConstants.CITY] =
                    (loyaltyInfoDetails.City == null ? valueNotPresent : loyaltyInfoDetails.City);
                InviteAFriendConfirmationMap[CommunicationTemplateConstants.MESSAGE] =
                    (txtMessage.Value == string.Empty ? valueNotPresent : txtMessage.Value);
                InviteAFriendConfirmationMap[CommunicationTemplateConstants.ADDRESS_LINE1] =
                    (loyaltyInfoDetails.AddressLine1 == null ? valueNotPresent : loyaltyInfoDetails.AddressLine1);
                InviteAFriendConfirmationMap[CommunicationTemplateConstants.ADDRESS_LINE2] =
                    (loyaltyInfoDetails.AddressLine2 == null ? valueNotPresent : loyaltyInfoDetails.AddressLine2);
                InviteAFriendConfirmationMap[CommunicationTemplateConstants.POST_CODE] =
                    (loyaltyInfoDetails.PostCode == null ? valueNotPresent : loyaltyInfoDetails.PostCode);
                InviteAFriendConfirmationMap[CommunicationTemplateConstants.MEMBERSHIPNUMBER] =
                    loyaltyInfoDetails.UserName;
            }

            return InviteAFriendConfirmationMap;
        }

        #endregion GetIniviteAFriendConfirmationDetails

        #region SendEmailToUser

        // This will send the mail to user who has requested for the friends name. 
        private void SendEmailToUser()
        {
            LoyaltyDetailsEntity loyalityInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;

            try
            {
                Dictionary<string, string> emailConfirmationMap = GetIniviteAFriendToUserConfirmationDetails();
                EmailEntity emailToUserEntity =
                    CommunicationUtility.GetInviteAFriendToUserConfirmationEmail(emailConfirmationMap,
                                                                                 loyalityInfoDetails.Email);

                CommunicationService.SendMail(emailToUserEntity, null);
            }
            catch (Exception genEx)
            {
                Scanweb.Core.AppLogger.LogFatalException(genEx, "Email could not send in Invite A Friend screen.");
            }
        }

        #endregion SendEmailToUser

        #region GetIniviteAFriendToUserConfirmationDetails

        private Dictionary<string, string> GetIniviteAFriendToUserConfirmationDetails()
        {
            LoyaltyDetailsEntity loyaltyInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails as LoyaltyDetailsEntity;
            Dictionary<string, string> InviteAFriendToUserConfirmationMap = new Dictionary<string, string>();
            if (loyaltyInfoDetails != null)
            {
                string valueNotPresent = AppConstants.SPACE;
                InviteAFriendToUserConfirmationMap[CommunicationTemplateConstants.FIRST_NAME] =
                    (loyaltyInfoDetails.FirstName == null ? valueNotPresent : loyaltyInfoDetails.FirstName);
                InviteAFriendToUserConfirmationMap[CommunicationTemplateConstants.LOGOPATH] = AppConstants.LOGOPATH;
            }
            return InviteAFriendToUserConfirmationMap;
        }

        #endregion GetIniviteAFriendToUserConfirmationDetails

        #endregion Private Method
    }
}