<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateCarsouel.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.DateCarsouel" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<div id="my_carousel">

<!-- RS2.0:Translation: 22 July: Setting for One only as its all hard coded -->
<div class="scansprite btnprev"><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/Previous5days") %></strong></div>
<div class="scansprite btnnext"><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/Next5days") %></strong></div>
<!--START: carsouel dates -->
	<ul id="carouselData">
		<li>
			<div class="hd ratesSprite"></div>
			<div class="cnt">
				<p>Tue 18/05/2010- Thu 20/01/2010</p>
				<hr />
				<!-- RS2.0:Translation: 22 July: Setting for One only as its all hard coded -->
				<strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from") %> 1550 SEK</strong>
			</div>
			<div class="ft ratesSprite"></div>
		</li>
		<li>
			<div class="hd ratesSprite"></div>
			<div class="cnt">
				<p>Thu 20/05/2010- Fri 21/05/2010</p>
				<hr />
				<strong>From 1300 SEK</strong>
			</div>
			<div class="ft ratesSprite"></div>
		</li>
		<li>
			<div class="hd ratesSprite"></div>
			<div class="cnt">
				<p>Sat 22/05/2010- Mon 24/05/2010</p>
				<hr />
				<strong>From 1350 SEK</strong>
			</div>
			<div class="ft ratesSprite"></div>
		</li>
		<li class="active">
			<div class="hd ratesSprite"></div>
			<div class="cnt">
				<p>Tue 25/05/2010- Wed 26/05/2010</p>
				<hr />
				<strong>From 1550 SEK</strong>
			</div>
			<div class="ft ratesSprite"></div>
		</li>
		<li>
			<div class="hd ratesSprite"></div>
			<div class="cnt">
				<p>Thu 27/01/2010- Fri 28/05/2010</p>
				<hr />
				<strong>From 1550 SEK</strong>
			</div>
			<div class="ft ratesSprite"></div>
		</li>

		<li>
			<div class="hd ratesSprite"></div>
			<div class="cnt">
				<p>Fri 28/01/2010- Sun 30/01/2010</p>
				<hr />
				<strong>From 1450 SEK</strong>
			</div>
			<div class="ft ratesSprite"></div>
		</li>
		<li>
			<div class="hd ratesSprite"></div>
			<div class="cnt">
				<p>Sun 30/05/2010- Mon 31/05/2010</p>
				<hr />
				<strong>From 1350 SEK</strong>
			</div>
			<div class="ft ratesSprite"></div>
		</li>
		<li>
			<div class="hd ratesSprite"></div>
			<div class="cnt">
				<p>Thu 03/06/2010- Sat 05/06/2010</p>
				<hr />
				<strong>From 1350 SEK</strong>
			</div>
			<div class="ft ratesSprite"></div>
		</li>
		<li>
			<div class="hd ratesSprite"></div>
			<div class="cnt">
				<p>Sat 05/06/2010- Sun 06/06/2010</p>
				<hr />
				<strong>From 1450 SEK</strong>
			</div>
			<div class="ft ratesSprite"></div>
		</li>
		<li>
			<div class="hd ratesSprite"></div>
			<div class="cnt">
				<p>Tue 08/06/2010- Wed 09/06/2010</p>
				<hr />
				<strong>From 1450 SEK</strong>
			</div>
			<div class="ft ratesSprite"></div>
		</li>
	</ul>
<!--END: carsouel dates -->
</div>





