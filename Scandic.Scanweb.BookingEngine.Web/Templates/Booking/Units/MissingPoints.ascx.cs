#region Description

////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: Missing Points user control code behind   			  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Raj Kishore Marandi	                                  //
/// Author email id				:                           							  //
/// Creation Date				: 17th November  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revision History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#endregion Description

#region  System Namespaces

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion System Namespaces

    #region Scandic Namespaces

    #endregion Scandic Namespaces

    #region EpiServer NameSpace

#endregion EpiServer NameSpace

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class of Missing Points User Control.
    /// This is responsible for user control Population and fetching User information from UI.
    /// Also responsible for Creating Email template and send the mail to Scandic.
    /// </summary>
    public partial class MissingPoints : EPiServer.UserControlBase, INavigationTraking
    {
        #region Private Members
        private AvailabilityController availabilityController = null;

        /// <summary>
        /// Gets m_AvailabilityController
        /// </summary>
        private AvailabilityController m_AvailabilityController
        {
            get
            {
                if (availabilityController == null)
                {
                    availabilityController = new AvailabilityController();
                }
                return availabilityController;
            }
        }
        #endregion 

        #region Protected Methods

        #region Page Load

        /// <summary>
        /// Page load for the user control.
        /// Responsible for initializing UI,
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible && WebUtil.IsSessionValid())
            {
                if (UserLoggedInSessionWrapper.UserLoggedIn == true)
                {
                    EmailConfirmationMsg.Visible = false;
                    if (!IsPostBack)
                    {
                        try
                        {
                            SetEmailConfirmationMsg();
                            InitializeLabels();
                            InitializeDate();
                            LinkButtonReport.Attributes.Add("onclick", "javascript:return pushEntityToHiddenField();");
                            spnButtonReport.Attributes.Add("onclick", "javascript:return pushEntityToHiddenField();");
                        }
                        catch (Exception ex)
                        {
                            WebUtil.ApplicationErrorLog(ex);
                        }
                    }

                    if (Utility.GetCurrentLanguage().ToUpper() == LanguageConstant.LANGUAGE_GERMAN)
                        dvNameTitle.Visible = true;
                    else
                        dvNameTitle.Visible = false;
                }
                else
                {
                    AppLogger.LogInfoMessage(
                        "User is already logged out - Still accessing MissingPoint Point Functionality.");
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.LOGIN_FOR_DEEPLINK), false);
                }
            }
        }

        #endregion Page Load

        #region Report Button Click

        /// <summary>
        /// This method is fired when report button is clicked.
        /// Responsible for initiating sending mail.
        /// This method calls ReportMissingPoints.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Report_Click(object sender, EventArgs e)
        {
            try
            {
                UserNavTracker.TrackAction(this, "Send Missing Points");
                //WebUtil.ApplicationErrorLog(new Exception("Missing Points"));
                //Response.End();
                SendConfirmationToUser();
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }

            try
            {
                ReportMissingPoints();
                MissingPointsDiv.Visible = false;
                EmailConfirmationMsg.Visible = true;
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }
        }

        #endregion Report Button Click

        #endregion Protected Methods

        #region Private Methods

        #region SetEmailConfirmationMsg

        /// <summary>
        /// Private Method to Initialize User control Labels.
        /// </summary>
        private void SetEmailConfirmationMsg()
        {
            EmailConfirmationMsg.AssignHeader(
                WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/missingpointmessageheader"));
            EmailConfirmationMsg.AssignMessage(
                WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/missingpointmessgaedescription"));
            EmailConfirmationMsg.AssignLink(
                WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/missingpointspecialoffers"));
        }

        #endregion SetEmailConfirmationMsg
       
        #region InitializeDate

        /// <summary>
        /// InitializeDate
        /// </summary>
        private void InitializeDate()
        {
            DateTime now = DateTime.Now;
        }

        #endregion InitializeDate

        #region Initialize Labels

        /// <summary>
        /// Method to Initialize the labels in the User control.
        /// </summary>
        private void InitializeLabels()
        {
            LoyaltyDetailsEntity loyalityInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails as LoyaltyDetailsEntity;
            if (loyalityInfoDetails != null)
            {
                NameTitle.InnerText = Utility.GetNameTitle(loyalityInfoDetails.Title);
                //
                FirstName.InnerText = (loyalityInfoDetails.FirstName == null
                                           ? string.Empty
                                           : loyalityInfoDetails.FirstName);
                SurName.InnerText = (loyalityInfoDetails.SurName == null ? string.Empty : loyalityInfoDetails.SurName);
                MailAddress.InnerText = (loyalityInfoDetails.Email == null ? string.Empty : loyalityInfoDetails.Email);
                if (loyalityInfoDetails.Telephone1 != null)
                {
                    string formatedPhone = Utility.FormatePhoneToDisplay(loyalityInfoDetails.Telephone1);
                    Telephone.InnerText = formatedPhone;
                }
            }
        }

        #endregion Initilize Labels

        #region SendConfirmationToUser

        /// <summary>
        /// SendConfirmationToUser
        /// </summary>
        private void SendConfirmationToUser()
        {
            LoyaltyDetailsEntity loyalityInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;

            try
            {
                Dictionary<string, string> emailConfirmationMap = GetMissingPointEmailConfirmationDetails();
                EmailEntity emailEntity = CommunicationUtility.GetMissingPointConfirmationEmail(emailConfirmationMap,
                                                                                                loyalityInfoDetails.
                                                                                                    Email);
                CommunicationService.SendMail(emailEntity, null);
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx);
            }
        }

        #endregion SendConfirmationToUser

        #region Report Missing Point

        /// <summary>
        /// This will send the user information to Scandic.
        /// Invoked by Report Button Submit
        /// </summary>
        private void ReportMissingPoints()
        {
            try
            {
                int collectionCount = 0;
                Dictionary<string, string> emailConfirmationMap =
                    GetMissingPointSCEmailConfirmationDetails(ref collectionCount);
                EmailEntity emailEntity = CommunicationUtility.GetMissingPointSCConfirmationEmail(emailConfirmationMap,
                                                                                                  collectionCount);
                CommunicationService.SendMail(emailEntity, null);
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx);
            }
        }

        #endregion Report Missing Point

        #region GetMissing Point SC Confirmation Details

        /// <summary>
        /// Collect the Missing point Confirmation  Map Information to be send as Email to Scandic
        /// Invoked by Report Button Submit
        /// </summary>
        /// <param name="collectionCount"></param>
        /// <returns>Missing point Confirmation  Details</returns>
        private Dictionary<string, string> GetMissingPointSCEmailConfirmationDetails(ref int collectionCount)
        {
            string valueNotPresent = AppConstants.SPACE;
            Dictionary<string, string> MissingEmailConfirmationMap = new Dictionary<string, string>();
            LoyaltyDetailsEntity loyaltyInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails as LoyaltyDetailsEntity;

            string selectedCityAndHotelCollection = selectedCityAndHotel.Value;
            if (loyaltyInfoDetails != null)
            {
                MissingEmailConfirmationMap[CommunicationTemplateConstants.TITLE] =
                    Utility.GetNameTitle(loyaltyInfoDetails.Title);
                MissingEmailConfirmationMap[CommunicationTemplateConstants.FIRST_NAME] =
                    (loyaltyInfoDetails.FirstName == null ? valueNotPresent : loyaltyInfoDetails.FirstName);
                MissingEmailConfirmationMap[CommunicationTemplateConstants.LASTNAME] = (loyaltyInfoDetails.SurName ==
                                                                                        null
                                                                                            ? valueNotPresent
                                                                                            : loyaltyInfoDetails.SurName);
                MissingEmailConfirmationMap[CommunicationTemplateConstants.EMAIL] = (loyaltyInfoDetails.Email == null
                                                                                         ? valueNotPresent
                                                                                         : loyaltyInfoDetails.Email);
                MissingEmailConfirmationMap[CommunicationTemplateConstants.TELEPHONE1] =
                    (loyaltyInfoDetails.Telephone1 == null ? valueNotPresent : loyaltyInfoDetails.Telephone1);
                MissingEmailConfirmationMap[CommunicationTemplateConstants.MEMBERSHIPNUMBER] =
                    loyaltyInfoDetails.UserName;


                char[] cityHotelSeperator = new char[1];
                cityHotelSeperator[0] = '%';
                string[] eachCollection = selectedCityAndHotelCollection.Split(cityHotelSeperator,
                                                                               StringSplitOptions.RemoveEmptyEntries);

                char[] valueSeperator = new char[1];
                valueSeperator[0] = ',';

                collectionCount = eachCollection.Length;
                for (int count = 0; count < collectionCount; count++)
                {
                    string[] values = eachCollection[count].Split(valueSeperator, StringSplitOptions.RemoveEmptyEntries);
                    if (5 == values.Length)
                    {
                        MissingEmailConfirmationMap[CommunicationTemplateConstants.CONFIRMATION_NUMBER + count] =
                            values[0];
                        MissingEmailConfirmationMap[CommunicationTemplateConstants.ARRIVAL_DATE + count] = values[1];
                        MissingEmailConfirmationMap[CommunicationTemplateConstants.DEPARTURE_DATE + count] = values[2];
                        MissingEmailConfirmationMap[CommunicationTemplateConstants.CITY + count] = values[3];
                        MissingEmailConfirmationMap[CommunicationTemplateConstants.HOTEL + count] = values[4];
                    }
                    else
                    {
                        MissingEmailConfirmationMap[CommunicationTemplateConstants.CONFIRMATION_NUMBER + count] =
                            AppConstants.SPACE;
                        MissingEmailConfirmationMap[CommunicationTemplateConstants.ARRIVAL_DATE + count] = values[0];
                        MissingEmailConfirmationMap[CommunicationTemplateConstants.DEPARTURE_DATE + count] = values[1];
                        MissingEmailConfirmationMap[CommunicationTemplateConstants.CITY + count] = values[2];
                        MissingEmailConfirmationMap[CommunicationTemplateConstants.HOTEL + count] = values[3];
                    }
                }  
            }

            return MissingEmailConfirmationMap;
        }

        #endregion GetMissing Point SC Confirmation Details

        #region GetMissingPointEmailConfirmationDetails

        /// <summary>
        /// GetMissingPointEmailConfirmationDetails
        /// </summary>
        /// <returns>MissingPointEmailConfirmationDetails</returns>
        private Dictionary<string, string> GetMissingPointEmailConfirmationDetails()
        {
            string valueNotPresent = WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/fieldnotavailable");
            Dictionary<string, string> MissingEmailConfirmationMap = new Dictionary<string, string>();
            LoyaltyDetailsEntity loyaltyInfoDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails as LoyaltyDetailsEntity;

            if (loyaltyInfoDetails != null)
            {
                MissingEmailConfirmationMap[CommunicationTemplateConstants.TITLE] =
                    Utility.GetNameTitle(loyaltyInfoDetails.Title);
                //
                MissingEmailConfirmationMap[CommunicationTemplateConstants.FIRST_NAME] =
                    (loyaltyInfoDetails.FirstName == null ? valueNotPresent : loyaltyInfoDetails.FirstName);
                MissingEmailConfirmationMap[CommunicationTemplateConstants.LOGOPATH] = AppConstants.LOGOPATH;
            }

            return MissingEmailConfirmationMap;
        }

        #endregion GetMissingPointSCEmailConfirmationDetails

        /// <summary>
        /// CustomizedFetchAllDestination
        /// </summary>
        /// <returns>List of CityDestination</returns>
        protected List<CityDestination> CustomizedFetchAllDestination()
        {
            List<CityDestination> destinationsToBeRemovedFromTheLastSixMonthscityList =new List<CityDestination>();
            List<CityDestination> destinationsFromTheLastSixMonthscityList = m_AvailabilityController.FetchDestinationsFromTheLastSixMonthscityList();
            foreach (var cityDestination in destinationsFromTheLastSixMonthscityList)
            {
                if (cityDestination.SearchedHotels.Count <= 0)
                    destinationsToBeRemovedFromTheLastSixMonthscityList.Add(cityDestination);
            }
            return destinationsFromTheLastSixMonthscityList.Except(destinationsToBeRemovedFromTheLastSixMonthscityList).ToList();
        }

        #endregion Private Methods

        #region INavigationTraking Members

        public List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> paramters = new List<KeyValueParam>();
            paramters.Add(new KeyValueParam("Selected Missing Points", selectedCityAndHotel.Value));
            return paramters;
        }

        #endregion
    }

    #region HotelInfo Comparers

    /// <summary>
    /// HotelInfoComparer
    /// </summary>
    public class HotelInfoComparer : IComparer<HotelDestination>
    {

        private static IFormatProvider cultureInfo =
            new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);

        private static string currentLocale = cultureInfo.ToString();
        private CompareInfo ci = new CultureInfo(currentLocale).CompareInfo;

        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(HotelDestination first, HotelDestination second)
        {
            return ci.Compare(first.Name, second.Name, CompareOptions.IgnoreCase);
        }
    }

    #endregion HotelInfo Comparers
}