<%@ Control Language="C#" AutoEventWireup="true" Codebehind="LostCardNoLogin.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.LostCardNoLogin" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register Src="EmailConfirmation.ascx" TagName="EmailConfirmation" TagPrefix="ECUC" %>
<div id="Loyalty" class="BE">
    <div id="LostCarWithOutLogInDiv" runat="server">
        <!-- LastCard -->
        <div class="box-top-grey">
            <span class="title">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/header") %>
            </span>
        </div>
        <div>
            <input type="hidden" id="messageSummary" name="messageSummary" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/lostcardheading") %>' />
            <input type="hidden" id="lcwolFirstname" name="lcwolFirstname" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/first_name") %>' />
            <input type="hidden" id="lcwolLastname" name="lcwolLastname" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/last_name") %>' />
            <input type="hidden" id="lcwolAddressline1" name="lcwolAddressline1" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/address") %>' />
            <!-- Added hidden field for AddressLine2-->
            <input type="hidden" id="lcwolAddressline2" name="lcwolAddressline2" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/addressline2") %>' />
            <input type="hidden" id="lcwolPostcode" name="lcwolPostcode" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/postal_code") %>' />
            <input type="hidden" id="lcwolCitytown" name="lcwolCitytown" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/city_town_information") %>' />
            <input type="hidden" id="lcwolMembershipNumber" name="lcwolMembershipNumber" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalidmembershipnumber") %>' />
            <input type="hidden" id="lcwolCountry" name="lcwolCountry" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/country") %>' />
            <input type="hidden" id="lcwolPhone" name="lcwolPhone" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/phone") %>' />
            <!--R1.3 | CR16 | Loyalty Enhancement 
                    Added  error message for telephone code validation-->
            <input type="hidden" id="lcwolPhoneCode" name="lcwolPhoneCode" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/phone_code") %>' />
            <input type="hidden" id="lcwolEmail" name="lcwolEmail" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/email_address") %>' />
        </div>
        <div class="box-middle">
            <div class="content">
                <div id="errMessageDivLostcardWithoutlogin">
                </div>
                <div class="columnOne">
                <div runat="server" id="divNameTitle">   
                    <p class="formRow">
                        <span id="lblNameTitle">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/Title") %> <!--Add Title field-->
                            </label>
                        </span>
                        <br />
                        <asp:DropDownList CssClass="frmSelect" ID="ddlTitle" runat="server" />
                    </p>
                   </div>
                    <p class="formRow">
                        <span id="spanLCFirstName">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/namefirst") %>
                            </label>
                        </span>
                        <br />
                        <input type="text" id="txtFirstName" runat="server" class="frmInputText" maxlength="40" />
                    </p>
                    <p class="formRow" id="P1" runat="server">
                        <span id="spanLCLastName">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/lastname") %>
                            </label>
                        </span>
                        <br />
                        <input type="text" runat="server" id="txtLastName" class="frmInputText" maxlength="80" />
                    </p>
                    <p class="formRow">
                        <span id="spanLCEMail">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/email") %>
                            </label>
                        </span>
                        <br />
                        <input type="text" id="txtEMail" runat="server" class="frmInputText" maxlength="200" />
                    </p>
                    <p class="formRow">
                        <span id="spanLCPhone">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/phonenumber") %>
                            </label>
                        </span>
                        <br />
                        <span class="selCode">
                            <asp:DropDownList CssClass="frmSelect fieldSelect" ID="ddlTelephone" runat="server">
                            </asp:DropDownList>
                        </span>
                        <input type="text" id="txtHomePhone" runat="server" class="frmInputTextSmall" maxlength="2000" />
                        <br class="clear" />
                    </p>
                    <p class="formRow">
                        <span id="spanLCGuestNo">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/program") %>
                            </label>
                        </span>
                        <br />
                        <input type="text" id="txtGuestNumber" runat="server" class="frmInputText" maxlength="50" />
                    </p>
                </div>
                <div class="columnTwo">
                    <p class="formRow">
                        <span id="spanLCAddressline1">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/addressline1") %>
                            </label>
                        </span>
                        <br />
                        <input type="text" id="txtAddressLine1" runat="server" class="frmInputText" maxlength="80" />
                    </p>
                    <p class="formRow">
                        <span id="spanLCAddressline2">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/addressline2") %>
                            </label>
                        </span>
                        <br />
                        <input type="text" id="txtAddressLine2" runat="server" class="frmInputText" maxlength="80" />
                    </p>
                    <p class="formRow">
                        <span id="spanLCPostcode">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/postcode") %>
                            </label>
                        </span>
                        <br />
                        <input type="text" id="txtPostCode" runat="server" class="frmInputText" maxlength="15" />
                    </p>
                    <p class="formRow">
                        <span id="spanLCCity">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/city") %>
                            </label>
                        </span>
                        <br />
                        <input type="text" id="txtCity" runat="server" class="frmInputText" maxlength="40" />
                    </p>
                    <p class="formRow">
                        <span id="spanLCCountry">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/country") %>
                            </label>
                        </span>
                        <br />
                        <asp:DropDownList CssClass="frmSelectBig fieldSelect" ID="ddlCountry" runat="server">
                        </asp:DropDownList>
                        <br class="clear" />
                    </p>
                </div>
                <div class="clear">
                    &nbsp;</div>
                <!-- /details -->
                <!-- Footer -->
                <div id="FooterContainer">
                    <div class="alignRight">
                        <div class="actionBtn fltRt">
                            <asp:LinkButton ID="LinkButtonReport" class="buttonInner" OnClick="ReportButton_Click"
                                runat="server"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/buttonmessage") %></span></asp:LinkButton>
                                <asp:LinkButton ID="spnButtonReport" class="buttonRt scansprite" OnClick="ReportButton_Click"
                                runat="server"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="clear">
                        &nbsp;</div>
                </div>
                <!-- Footer -->
            </div>
        </div>
        <div class="box-bottom">
            &nbsp;</div>
    </div>
    <ECUC:EmailConfirmation ID="LCWLEmailConfirmation" runat="server" />
</div>
