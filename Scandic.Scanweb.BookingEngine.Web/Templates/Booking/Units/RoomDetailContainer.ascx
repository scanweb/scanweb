<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RoomDetailContainer.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.RoomDetailContainer" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>

<div class="tableData" id="roomAndRateTable" runat="server">
      <div id="tableDataInnerDiv" runat="server">
               <table border="0" cellspacing="0" cellpadding="0" id="m15TabularData" runat="server">
					<tr style="position:relative;">
						<th align="left" class="first sprite" valign="middle"><h2 class="stoolHeading"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/RomTypes") %></h2></th>
						
						<th align="left" class="corpRate col2">
							<h3><span class="fltLft">Corporate rate sample header goes here please have a look</span><a href="#" title="#corpRate" class="help spriteIcon toolTipMe"></a></h3>
							
							<!--Tool Tips To be included -->
						</th>
						<th align="left" class="earlyRate col3">
							<h3><span class="fltLft">Early rate</span><a href="#" title="#earlyRate" class="help spriteIcon toolTipMe"></a></h3>
							<<!--Tool Tips To be included -->
						</th>
						<th align="left" class="flexRate col4">
							<h3><span class="fltLft">Flex rate</span><a href="#" title="#flexRate" class="help spriteIcon toolTipMe"></a></h3>
							
							<!--Tool Tips To be included -->
						</th>
					</tr>
					<tr>
						<td class="first" rel=""><h3>cabin </h3><a href="#" title="cabin" class="overlay jqModal spriteIcon"></a>
						<p>Work or play in your 19sqm contempory room, in a local scandinavian style</p>
						<br class="clear" />
						</td>
								
						<td class="corpRate" rel="actCorpRate"><input type="radio" name="radio" id="er_rate" class="room1" value="1450"/><label for="er_rate">1450 SEK</label></td>		
						<td class="earlyRate" rel="actEarlyRate"><input type="radio" name="radio" id="fx_rate" class="room1" value="1550"/><label for="fx_rate">1550 SEK</label></td>
						<td class="flexRate" rel="actFlexRate"><input type="radio" name="radio" id="Radio1" class="room1" value="1550"/><label for="fx_rate">1550 SEK</label></td>
					</tr>
					
					<tr>
						<td class="first"><h3>Suite</h3><a href="#" title="Suite" class="overlay jqModal spriteIcon"></a><p>Work or play in your 19sqm contempory room, in a local scandinavian style</p><br class="clear" /></td>
								
						<td class="corpRate"  rel="actCorpRate"><input type="radio" name="radio" id="er_rate1" class="room1" value="1450"/><label for="er_rate1">1450 SEK</label></td>		
						<td class="earlyRate" rel="actEarlyRate"><input type="radio" name="radio" id="fx_rate1" class="room1" value="1550"/><label for="fx_rate1">1550 SEK</label></td>
						<td class="flexRate" rel="actFlexRate"><input type="radio" name="radio" id="Radio2" class="room1" value="1550"/><label for="fx_rate1">1550 SEK</label></td>
					</tr>
					
					<tr>
						<td class="first"><h3>Superior</h3><a href="#" title="Superior" class="overlay jqModal spriteIcon"></a><p>Work or play in your 19sqm contempory room, in a local scandinavian style</p><br class="clear" /></td>
							
						<td class="corpRate" rel="actCorpRate"><input type="radio" name="radio" id="er_rate2" class="room1" value="1450"/><label for="er_rate2">1450 SEK</label></td>		
						<td class="earlyRate" rel="actEarlyRate"><input type="radio" name="radio" id="fx_rate2" class="room1" value="1550"/><label for="fx_rate2">1550 SEK</label></td>
						<td class="flexRate" rel="actFlexRate"><input type="radio" name="radio" id="Radio3" class="room1" value="1550"/><label for="fx_rate2">1550 SEK</label></td>

					</tr>
					<tr>
						<td class="first"><h3>Amazing</h3><a href="#" title="Amazing" class="overlay jqModal spriteIcon"></a><p>Work or play in your 19sqm contempory room, in a local scandinavian style</p><br class="clear" /></td>
							
						<td class="corpRate" rel="actCorpRate"><input type="radio" name="radio" id="er_rate3" class="room1" value="1450"/><label for="er_rate3">1450 SEK</label></td>		
						<td class="earlyRate" rel="actEarlyRate"><input type="radio" name="radio" id="fx_rate3" class="room1" value="1550"/><label for="fx_rate3">1550 SEK</label></td>
						<td class="flexRate" rel="actFlexRate"><input type="radio" name="radio" id="Radio4" class="room1" value="1550"/><label for="fx_rate3">1550 SEK</label></td>
					</tr>
					<tr>
						<td class="first"><h3>Super deluxe</h3><a href="#" title="Super deluxe" class="overlay jqModal spriteIcon"></a><p>Work or play in your 19sqm contempory room, in a local scandinavian style</p><br class="clear" /></td>
								
						<td class="corpRate" rel="actCorpRate"><input type="radio" name="radio" id="er_rate4" class="room1" value="1450"/><label for="er_rate4">1450 SEK</label></td>		
						<td class="earlyRate" rel="actEarlyRate"><input type="radio" name="radio" id="fx_rate4" class="room1" value="1550"/><label for="fx_rate4">1550 SEK</label></td>
						<td class="flexRate" rel="actFlexRate"><input type="radio" name="radio" id="Radio5" class="room1" value="1550"/><label for="fx_rate4">1550 SEK</label></td>
					</tr>
					<tr>
						<td class="first"><h3>Another category</h3><a href="#" title="Another category" class="overlay jqModal spriteIcon"></a><p>Work or play in your 19sqm contempory room, in a local scandinavian style</p><br class="clear" /></td>
							
						<td class="corpRate" rel="actCorpRate"><input type="radio" name="radio" id="er_rate5" class="room1" value="1450"/><label for="er_rate5">1450 SEK</label></td>		
						<td class="earlyRate" rel="actEarlyRate"><input type="radio" name="radio" id="fx_rate5" class="room1" value="1550"/><label for="fx_rate5">1550 SEK</label></td>
						<td class="flexRate" rel="actFlexRate"><input type="radio" name="radio" id="Radio6" class="room1" value="1550"/><label for="fx_rate5">1550 SEK</label></td>
					</tr>
					<tbody class="HiddenRows">
						<tr>
						<td class="first"><h3>Another category</h3><a href="#" title="Another category" class="overlay jqModal spriteIcon"></a><p>Work or play in your 19sqm contempory room, in a local scandinavian style</p><br class="clear" /></td>
							
						<td class="corpRate" rel="actCorpRate"><input type="radio" name="radio" id="Radio7" class="room1" value="1450"/><label for="er_rate5">1450 SEK</label></td>		
						<td class="earlyRate" rel="actEarlyRate"><input type="radio" name="radio" id="Radio8" class="room1" value="1550"/><label for="fx_rate5">1550 SEK</label></td>
						<td class="flexRate" rel="actFlexRate"><input type="radio" name="radio" id="Radio9" class="room1" value="1550"/><label for="fx_rate5">1550 SEK</label></td>
					</tr>
						<tr>
						<td class="first"><h3>Another category</h3><a href="#" title="Another category" class="overlay jqModal spriteIcon"></a><p>Work or play in your 19sqm contempory room, in a local scandinavian style</p><br class="clear" /></td>
							
						<td class="corpRate" rel="actCorpRate"><input type="radio" name="radio" id="Radio10" class="room1" value="1450"/><label for="er_rate5">1450 SEK</label></td>		
						<td class="earlyRate" rel="actEarlyRate"><input type="radio" name="radio" id="Radio11" class="room1" value="1550"/><label for="fx_rate5">1550 SEK</label></td>
						<td class="flexRate" rel="actFlexRate"><input type="radio" name="radio" id="Radio12" class="room1" value="1550"/><label for="fx_rate5">1550 SEK</label></td>
					</tr>
					
					</tbody>
			</table>
			<div class="topCnt">
				<div class="ratesOverLayCnt"></div>
					<div class="ratesOverLay roundMe">
						<div class="hd sprite"></div>
						<div class="cnt">
							<p><strong>Please select room</strong></p>
							<div class="HR"></div>
							<!--<p><strong>Room 3: Please select room type and rate type</strong></p>-->
						</div>
						<div class="ft sprite"></div>
					</div>
			</div>
			<div class="usrBtn">
				<a class="buttonInner sprite" href="#" rel="showAll"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/selectrate/allroomtypes") %></a>
				<a class="buttonInner sprite" href="#" rel="hideSome" style="display:none;">Hide</a>
				<a class="buttonRt sprite" href="#"></a>				
			</div>			
   </div>
</div>	