//  Description					: Code Behind class for ModifyBookingDates Control			  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Shankar Dasgupta                                   	  //
//  Author email id				:                           							  //
//  Creation Date				: 10th December  2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces
using System;
using System.Collections.Generic;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System.Web.UI.HtmlControls;

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for ModifyBookingDates
    /// </summary>
    public partial class ModifyBookingDates : EPiServer.UserControlBase
    {
        #region Private Variable

        private bool isSessionValid = true;

        private bool isEarly = false;
        private bool isFlex = false;

        /// <summary>
        /// Check if the any of the booking in the list is already canceled
        /// </summary>
        private bool isAlreadyCancelled = false;

        /// <summary>
        /// Check if the any of the booking in the list has the cancel dates over
        /// </summary>
        private bool isCancelDatesOver = false;

        #endregion

        private bool canModifyContactDetails = true;

        public bool CanModifyContactDetails
        {
            set { canModifyContactDetails = value; }
        }

        private bool ratePlanCodeDoesnotExists = false;

        public bool RatePlanCodeDoesnotExists
        {
            set { ratePlanCodeDoesnotExists = value; }
        }

        /// <summary>
        /// Page Load Method of ModifyBookingDates.
        /// Its sets the drop down and populates the UI 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            isSessionValid = WebUtil.IsSessionValid();
            BookingEngineSessionWrapper.DirectlyModifyContactDetails = false;
            BookingEngineSessionWrapper.IsPrepaidBlockBooking = false;
            if ((String.IsNullOrEmpty(Reservation2SessionWrapper.AboutOurRateHeading)) ||
                (String.IsNullOrEmpty(Reservation2SessionWrapper.AboutOurRateDescription)))
            {
                PageData pageData = GetPage(PageReference.RootPage);
                PageReference SelectRatePageData = (PageReference) pageData[EpiServerPageConstants.SELECT_RATE_PAGE];
                PageData selectRatePage = GetPage(SelectRatePageData);
                if (String.IsNullOrEmpty(Reservation2SessionWrapper.AboutOurRateHeading))
                    Reservation2SessionWrapper.AboutOurRateHeading = selectRatePage["AboutOurRatesHeading"] as string ??
                                                         string.Empty;
                if (String.IsNullOrEmpty(Reservation2SessionWrapper.AboutOurRateDescription))
                    Reservation2SessionWrapper.AboutOurRateDescription =
                        selectRatePage["AboutOurRateDescription"] as string ?? string.Empty;
            }

            if (isSessionValid)
            {
                if (!IsPostBack)
                {
                    ReservationInfo.CanModifyContactDetails = canModifyContactDetails;
                    ReservationInfo.RateDoesNotExistsInCMS = ratePlanCodeDoesnotExists;
                    List<BookingDetailsEntity> activeBookings = BookingEngineSessionWrapper.ActiveBookingDetails;
                    var isPastBooking = (activeBookings != null &&
                                         (activeBookings.Count > 0) &&
                                         activeBookings[0].HotelSearch.ArrivalDate < DateTime.Today);

                    if (IsFieldsNeedToBeSetup() || isPastBooking)
                    {
                        PopulateUI();
                    }
                    else
                    {
                    }
                    PopulateReservationInfoContainer();
                }
            }
            if (HygieneSessionWrapper.IsComboReservation)
            {
                lblInformativeTextModify.InnerText =
                    WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/InformativeTextModifyCancelMsz");
            }
        }

        /// <summary>
        /// lnkModifyContactDetails_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkModifyContactDetails_Click(object sender, EventArgs e)
        {
            BookingEngineSessionWrapper.DirectlyModifyContactDetails = true;
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS));
        }

        /// <summary>
        /// Event Handler for Cancel Button Click
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="args">Arguments for the event</param>
        protected void btnCancelBooking_Click(object sender, EventArgs args)
        {
            AppLogger.LogInfoMessage("ModifyBooking Dates: btnCancelBooking_Click");
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.CONFIRM_CANCELLATION), false);
        }

        /// <summary>
        /// Event Handler for Cancel Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void lnkModifyContact_Click(object sender, EventArgs args)
        {
            AppLogger.LogInfoMessage("ModifyBooking Dates: lnkModifyContact_Click");
            BookingEngineSessionWrapper.DirectlyModifyContactDetails = true;
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS), false);
        }

        /// <summary>
        /// Populate the UI
        /// </summary>
        private void PopulateUI()
        {
            try
            {
                DisplayCMSText();
                FindAllActiveBookings();

                List<BookingDetailsEntity> activeBookings = BookingEngineSessionWrapper.ActiveBookingDetails;
                bool isModifyCancel = false;
                bool isRateNull = false;
                if (activeBookings.Count == 1)
                {
                    if (activeBookings[0].HotelRoomRate != null && activeBookings[0].HotelSearch != null)
                    {
                        if (Utility.IsBlockCodeBooking)
                        {
                            string bookingStatus = string.Empty;
                            isModifyCancel = Utility.BlockCodeBooking(isModifyCancel, activeBookings[0], out bookingStatus);
                            divBookingStatus.InnerHtml = bookingStatus;
                            ReservationInfo.CanModifyContactDetails = isModifyCancel;
                        }
                        else
                        {
                            CheckBookingStatus
                                (activeBookings[0].HotelRoomRate.RatePlanCode, activeBookings[0].HotelSearch);
                            isModifyCancel = true;
                            Rate rate = RoomRateUtil.GetRate(activeBookings[0].HotelRoomRate.RatePlanCode);
                            if (rate != null &&
                                !rate.IsModifiable &&
                                activeBookings[0].HotelSearch.SearchingType != SearchType.REDEMPTION)
                            {
                                isModifyCancel = false;
                            }
                            else if (rate == null)
                            {
                                isRateNull = true;
                                isModifyCancel = false;
                            }
                        }

                        if (isModifyCancel)
                        {
                            btnCancelBooking.Visible = true;
                            spanbtnCancelBooking.Visible = false;
                        }
                        else
                        {
                            btnCancelBooking.Visible = false;
                            spanbtnCancelBooking.Visible = false;
                        }
                    }
                }
                else
                {
                    foreach (BookingDetailsEntity booking in activeBookings)
                    {
                        if (booking.HotelRoomRate != null && booking.HotelSearch != null)
                        {
                            if (Utility.IsBlockCodeBooking)
                            {
                                string bookingStatus = string.Empty;
                                isModifyCancel = Utility.BlockCodeBooking(isModifyCancel, booking,out bookingStatus);
                                divBookingStatus.InnerHtml = bookingStatus;
                                ReservationInfo.CanModifyContactDetails = isModifyCancel;
                            }
                            else
                            {
                                Rate rate = RoomRateUtil.GetRate(booking.HotelRoomRate.RatePlanCode);

                                if (rate != null)
                                {
                                    if (rate.IsCancellable || rate.IsCancellable)
                                    {
                                        isModifyCancel = true;
                                    }
                                    else
                                    {
                                        divBookingStatus.InnerHtml =
                                            WebUtil.GetTranslatedText
                                                (TranslatedTextConstansts.BOOKING_NOT_CANCELABLE) + AppConstants.SPACE;
                                    }
                                    if (rate.IsCancellable)
                                    {
                                        isFlex = true;
                                    }
                                    else
                                    {
                                        isEarly = true;
                                    }
                                }
                                else
                                {
                                    isRateNull = true;
                                    isModifyCancel = false;
                                }
                            }
                        }
                    }
                    if (isModifyCancel)
                    {
                        btnCancelBooking.Visible = true;
                        spanbtnCancelBooking.Visible = true;
                    }
                    else
                    {
                        btnCancelBooking.Visible = false;
                        spanbtnCancelBooking.Visible = false;
                    }
                }
                divHelpDescriptionText.Visible = true;
                if (isEarly != isFlex && isEarly == true)
                {
                    this.lblHelpDescriptionText.InnerHtml =
                        WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/EarlyHelpDescription");
                }
                else if ((isEarly != isFlex && isFlex == true))
                {
                    this.lblHelpDescriptionText.InnerHtml =
                        WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/FlexHelpDescription");
                }
                else if (Utility.IsBlockCodeBooking && isModifyCancel)
                {
                    divHelpDescriptionText.Visible = false;
                }
                else if (isEarly == isFlex && isFlex == true)
                {
                    this.lblHelpDescriptionText.InnerHtml =
                        WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/ComboHelpDescription");
                }

                else
                {
                    string bookingStatus = divBookingStatus.InnerHtml.Trim();
                    if (bookingStatus == "\r\n")
                        bookingStatus.Replace("\r\n", "");
                    if (!string.IsNullOrEmpty(bookingStatus))
                    {
                        this.lblHelpDescriptionText.InnerHtml = bookingStatus;
                    }
                    else
                    {
                        divHelpDescriptionText.Visible = false;
                    }
                }
                if (activeBookings != null &&
                    (activeBookings.Count > 0) && activeBookings[0].HotelSearch.ArrivalDate < DateTime.Today)
                {
                    this.lblHelpDescriptionText.InnerHtml =
                        WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/PastBookingMsg");
                    btnCancelBooking.Visible = false;
                    spanbtnCancelBooking.Visible = false;
                }
                if (isRateNull)
                {
                    btnCancelBooking.Visible = false;
                    spanbtnCancelBooking.Visible = false;
                    divHelpDescriptionText.Visible = true;
                    this.lblHelpDescriptionText.InnerHtml =
                        WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/RateCodeNotinCMS");
                }

                if (Utility.IsBlockCodeBooking && BookingEngineSessionWrapper.IsPrepaidBlockBooking)
                {
                    btnCancelBooking.Visible = true;
                    spanbtnCancelBooking.Visible = true;
                        divHelpDescriptionText.Visible = true;
                    this.lblHelpDescriptionText.InnerHtml =
                       WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/FlexHelpDescription");

                }
                if (activeBookings != null && activeBookings.Count > 0)
                {
                    foreach (BookingDetailsEntity booking in activeBookings)
                    {
                        if (booking.HotelRoomRate != null && booking.HotelRoomRate.IsSessionBooking == true)
                        {
                            btnCancelBooking.Visible = false;
                            spanbtnCancelBooking.Visible = false;
                            divHelpDescriptionText.Visible = false;
                            errorAlertDiv.Visible = true;
                            this.errorLabel.InnerHtml = WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/SessionBookingHelpDescription");
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }
        }

        

        /// <summary>
        /// Display the Informative text from CMS
        /// </summary>
        private void DisplayCMSText()
        {
            lblInformativeText.InnerHtml =
                CurrentPage["MultilegInformativeText"] != null
                    ? CurrentPage["MultilegInformativeText"] as string
                    : string.Empty;
            lblGuaranteeInfo.InnerHtml =
                CurrentPage["GuaranteeInformation"] != null
                    ? CurrentPage["GuaranteeInformation"] as string
                    : string.Empty;
        }

        /// <summary>
        /// Find all the active booing
        /// </summary>
        public void FindAllActiveBookings()
        {
            List<BookingDetailsEntity> bookingDetailsList = BookingEngineSessionWrapper.AllBookingDetails;
            int cancelledCount = 0;
            if (bookingDetailsList != null && bookingDetailsList.Count > 0)
            {
                DisplayBookingStatus(cancelledCount, bookingDetailsList);
                foreach (BookingDetailsEntity bookingDetail in bookingDetailsList)
                {
                    if (!bookingDetail.IsCancelledByUser)
                    {
                        BookingEngineSessionWrapper.BookingDetails = bookingDetail;
                        break;
                    }
                }
                SetActualRoomsPerNight();
            }
        }
       
        /// <summary>
        /// Set the Actual Rooms Per Night
        /// </summary>
        private void SetActualRoomsPerNight()
        {
            int totalBooking = 0;
            List<BookingDetailsEntity> listBookingDetails = BookingEngineSessionWrapper.ActiveBookingDetails;
            List<BookingDetailsEntity> listAllBookingDetails = BookingEngineSessionWrapper.AllBookingDetails;
            if (listBookingDetails != null && listBookingDetails.Count > 0)
            {
                totalBooking = listBookingDetails.Count;
                for (int count = 0; count < totalBooking; count++)
                {
                    BookingDetailsEntity bookingDetails = listBookingDetails[count];
                    if (bookingDetails.HotelSearch != null)
                    {
                        bookingDetails.HotelSearch.RoomsPerNight = totalBooking;                        
                    }
                }
            }

            else if (listAllBookingDetails != null && listAllBookingDetails.Count > 0)
            {
                totalBooking = listAllBookingDetails.Count;
                for (int count = 0; count < totalBooking; count++)
                {
                    BookingDetailsEntity bookingDetails = listAllBookingDetails[count];
                    //Set the Rooms per night to the actual number of active bookings
                    if (bookingDetails.HotelSearch != null)
                    {
                        bookingDetails.HotelSearch.RoomsPerNight = totalBooking;
                    }
                }
            }
            if (BookingEngineSessionWrapper.BookingDetails != null && BookingEngineSessionWrapper.BookingDetails.HotelSearch != null)
            {
                BookingEngineSessionWrapper.BookingDetails.HotelSearch.RoomsPerNight = totalBooking;
            }
        }

        /// <summary>
        /// Display the status of the booking
        /// </summary>
        /// <param name="cancelledCount">Cancelled Count</param>
        /// <param name="bookingDetailsList">List of Bookings</param>
        private void DisplayBookingStatus(int cancelledCount, List<BookingDetailsEntity> bookingDetailsList)
        {
            if (bookingDetailsList.FindAll
                    (delegate(BookingDetailsEntity bd) { return bd.IsCancelledByUser; }).Count ==
                bookingDetailsList.Count)
            {
                divBookingStatus.InnerHtml = WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_ALREADY_CANCELED);
                btnCancelBooking.Visible = false;
                spanbtnCancelBooking.Visible = false;
            }
            else if (bookingDetailsList.FindAll
                         (delegate(BookingDetailsEntity bd) { return bd.IsCancelledBySytem; }).Count ==
                     bookingDetailsList.Count)
            {
                if (Utility.IsBlockCodeBooking)
                {
                    divBookingStatus.InnerHtml = WebUtil.GetTranslatedText(TranslatedTextConstansts.BLOCK_NOT_CANCELABLE);
                }
                else
                {
                    divBookingStatus.InnerHtml =
                        WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_NOT_CANCELABLE);
                }
                btnCancelBooking.Visible = false;
                spanbtnCancelBooking.Visible = false;
            }
            else if ((bookingDetailsList.FindAll
                          (delegate(BookingDetailsEntity bd) { return bd.IsCancelledBySytem; }).Count > 0)
                     ||
                     (bookingDetailsList.FindAll(
                         delegate(BookingDetailsEntity bd) { return bd.HotelSearch.CancelPolicy == AppConstants.NON_CANCELLABLE_RESERVATION; }).Count
                      == BookingEngineSessionWrapper.ActiveBookingDetails.Count)
                )
            {
                if (Utility.IsBlockCodeBooking)
                {
                    divBookingStatus.InnerHtml = WebUtil.GetTranslatedText(TranslatedTextConstansts.BLOCK_NOT_CANCELABLE);
                }
                else
                {
                    divBookingStatus.InnerHtml =
                        WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_NOT_CANCELABLE);
                }
                btnCancelBooking.Visible = false;
                spanbtnCancelBooking.Visible = false;
            }
        }
        
        /// <summary>
        /// Check the Status of Booking
        /// </summary>
        /// <param name="ratePlanCode">
        /// RatePlanCode
        /// </param>
        /// <param name="hotelSearchEntity">
        /// HotelSearchEntity
        /// </param>
        private void CheckBookingStatus(string ratePlanCode, HotelSearchEntity hotelSearchEntity)
        {
            if (!string.IsNullOrEmpty(ratePlanCode))
            {
                Rate rate = RoomRateUtil.GetRate(ratePlanCode);

                if (rate != null)
                {
                    if (!rate.IsCancellable)
                    {
                        btnCancelBooking.Visible = false;
                        spanbtnCancelBooking.Visible = false;
                        isEarly = true;
                        divBookingStatus.InnerHtml =
                            WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_NOT_CANCELABLE) +
                            AppConstants.SPACE;
                    }
                    else
                    {
                        btnCancelBooking.Visible = true;
                        spanbtnCancelBooking.Visible = true;
                        isFlex = true;
                    }
                    if (!rate.IsModifiable)
                    {
                        btnCancelBooking.Visible = false;
                        spanbtnCancelBooking.Visible = false;
                        divBookingStatus.InnerHtml +=
                            WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_NOT_MODIFIABLE);
                    }
                    else
                    {
                        btnCancelBooking.Visible = true;
                        spanbtnCancelBooking.Visible = true;
                    }
                }
            }
            if (!isAlreadyCancelled && hotelSearchEntity.SearchingType == SearchType.REDEMPTION)
            {
                btnCancelBooking.Visible = true;
                spanbtnCancelBooking.Visible = true;
                divBookingStatus.InnerHtml = WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_NOT_MODIFIABLE);
            }
        }

        /// <summary>
        /// Populate the Reservation Info Container
        /// </summary>
        private void PopulateReservationInfoContainer()
        {
            List<BookingDetailsEntity> bookingDetailsList = BookingEngineSessionWrapper.ActiveBookingDetails;
            if (bookingDetailsList == null || bookingDetailsList.Count <= 0)
            {
                ReservationInfo.ShowHelpLink = false;
            }
            BookingDetailsEntity bookingDetails = BookingEngineSessionWrapper.BookingDetails;
            ReservationInfo.IsPrevBooking = false;


            if (bookingDetails != null)
            {
                HotelSearchEntity hotelSearchEntity = bookingDetails.HotelSearch;
                HotelRoomRateEntity hotelRoomRateEntity = bookingDetails.HotelRoomRate;
                GuestInformationEntity guestInfoEntity = bookingDetails.GuestInformation;
                HotelDestination hotelDestination = null;
                if ((hotelSearchEntity != null) && (hotelRoomRateEntity != null) && (guestInfoEntity != null))
                {
                    AvailabilityController availabilityController = new AvailabilityController();
                    hotelDestination =
                        availabilityController.GetHotelDestinationEntity(hotelSearchEntity.SelectedHotelCode);
                    if (hotelDestination != null)
                    {
                        ReservationInfo.CityOrHotelName = hotelDestination.Name;
                        ReservationInfo.HotelUrl = hotelDestination.HotelPageURL;
                        hotelSearchEntity.SearchedFor.SearchString = hotelDestination.Name;
                    }
                    RoomCategory roomCategory = RoomRateUtil.GetRoomCategory(hotelRoomRateEntity.RoomtypeCode);
                    if (roomCategory != null)
                    {
                        ReservationInfo.NoOfRooms = bookingDetailsList.Count.ToString();
                    }
                    ReservationInfo.ArrivalDate = hotelSearchEntity.ArrivalDate;
                    ReservationInfo.DepartureDate = hotelSearchEntity.DepartureDate;
                    ReservationInfo.NoOfAdults = Utility.GetTotalAdults();
                    ReservationInfo.NoOfChildren = Utility.GetTotalChildren();
                    ReservationInfo.NoOfNights = hotelSearchEntity.NoOfNights;
                    string username = guestInfoEntity.FirstName + " " + guestInfoEntity.LastName;

                    string reservationMessage = string.Empty;
                    if (!BookingEngineSessionWrapper.IsModifyingLegBooking)
                    {
                        reservationMessage =
                            WebUtil.GetTranslatedText
                                (TranslatedTextConstansts.YOUR_BOOKING_RESERVATION_MESSAGE_WITHOUT_NUMBER);
                        reservationMessage = string.Format(reservationMessage, bookingDetails.ReservationNumber);
                    }
                    else
                    {
                        reservationMessage =
                            WebUtil.GetTranslatedText(TranslatedTextConstansts.YOUR_BOOKING_RESERVATION_MESSAGE);
                        string resNumber =
                            bookingDetails.ReservationNumber + AppConstants.HYPHEN + bookingDetails.LegNumber;
                        reservationMessage = string.Format(reservationMessage, username, resNumber);
                    }

                    TimeSpan timeSpan = hotelSearchEntity.ArrivalDate.Subtract(hotelSearchEntity.DepartureDate);
                    double basePoints = 0;
                    double totalPoints = 0;
                    basePoints = hotelRoomRateEntity.Points;
                    try
                    {
                        if (hotelSearchEntity.NoOfNights > 1)
                            basePoints = hotelRoomRateEntity.Points/hotelSearchEntity.NoOfNights;
                    }
                    catch (Exception ex)
                    {
                        AppLogger.LogFatalException(ex, "Unable to calculate base points.");
                    }

                    if (hotelRoomRateEntity.TotalRate != null)
                    {
                        totalPoints = hotelRoomRateEntity.TotalRate.Rate;
                    }
                    string ratePlanCode = hotelRoomRateEntity.RatePlanCode;
                    string selectedRateCategoryName = string.Empty;

                    if (!string.IsNullOrEmpty(ratePlanCode))
                    {
                        Rate rate = RoomRateUtil.GetRate(ratePlanCode);
                        if (rate != null)
                        {
                            selectedRateCategoryName = rate.RateCategoryName;
                        }
                    }
                    string hotelCountryCode = hotelSearchEntity.HotelCountryCode;
                    if (hotelCountryCode == null || hotelCountryCode == string.Empty)
                    {
                        if (hotelSearchEntity.SearchingType == SearchType.BONUSCHEQUE)
                        {
                            HotelSearchEntity cloneHotelSearch = hotelSearchEntity.Clone();
                            if (cloneHotelSearch != null)
                            {
                                cloneHotelSearch.ArrivalDate = DateTime.Now;
                                cloneHotelSearch.DepartureDate = DateTime.Now.AddDays(1);
                                hotelCountryCode =
                                    availabilityController.GetHotelCountryCode
                                        (cloneHotelSearch, cloneHotelSearch.ListRooms[0]);
                                hotelSearchEntity.HotelCountryCode = hotelCountryCode;
                            }
                        }
                    }
                    string baseRateString = string.Empty;
                    if (hotelSearchEntity.SearchingType == SearchType.REDEMPTION)
                    {
                        baseRateString =
                            Utility.GetRoomRateString
                                (selectedRateCategoryName, hotelRoomRateEntity.Rate, hotelRoomRateEntity.Points,
                                 hotelSearchEntity.SearchingType, hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                 hotelSearchEntity.NoOfNights, hotelSearchEntity.RoomsPerNight);
                    }
                    else if ((Utility.IsBlockCodeBooking) && (string.IsNullOrEmpty(hotelRoomRateEntity.RatePlanCode))
                             && (!string.IsNullOrEmpty(hotelRoomRateEntity.RoomtypeCode)))
                    {
                        baseRateString = Utility.GetRoomRateString(hotelRoomRateEntity.Rate);
                    }
                    else
                    {
                        baseRateString =
                            Utility.GetRoomRateString
                                (selectedRateCategoryName, hotelRoomRateEntity.Rate, basePoints,
                                 hotelSearchEntity.SearchingType, hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                 AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                    }
                    double dblTotalRate = Utility.CalculateTotalRateInCaseOfModifyFlow();
                    string totalRateString = string.Empty;
                    switch (hotelSearchEntity.SearchingType)
                    {
                        case SearchType.REDEMPTION:
                            {
                                totalRateString =
                                    Utility.GetRoomRateString
                                        (null, null, hotelRoomRateEntity.Points, hotelSearchEntity.SearchingType,
                                         hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                         hotelSearchEntity.NoOfNights,
                                         hotelSearchEntity.RoomsPerNight);
                                break;
                            }
                        case SearchType.VOUCHER:
                            {
                                totalRateString =
                                    Utility.GetRoomRateString(null, null, totalPoints, hotelSearchEntity.SearchingType,
                                                              hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                                              hotelSearchEntity.NoOfNights,
                                                              hotelSearchEntity.RoomsPerNight);
                                break;
                            }
                        default:
                            {
                                RateEntity tempRateEntity =
                                    new RateEntity(dblTotalRate, hotelRoomRateEntity.TotalRate.CurrencyCode);
                                totalRateString =
                                    Utility.GetRoomRateString(null, tempRateEntity, totalPoints,
                                                              hotelSearchEntity.SearchingType,
                                                              hotelSearchEntity.ArrivalDate.Year,
                                                              hotelCountryCode, hotelSearchEntity.NoOfNights,
                                                              hotelSearchEntity.RoomsPerNight);
                                break;
                            }
                    }
                    ReservationInfo.SetTotalRoomRate(totalRateString);
                    if (bookingDetailsList != null && bookingDetailsList.Count > 0)
                        ReservationInfo.SetRoomDetails(bookingDetailsList);
                    if (bookingDetails.IsCancelledByUser || bookingDetails.IsCancelledBySytem)
                    {
                        ModifyInCancel(hotelSearchEntity, hotelRoomRateEntity, totalPoints, hotelCountryCode);
                    }
                }
            }
        }


        /// <summary>
        /// Modify in Cancelled Booking 
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <param name="hotelRoomRateEntity"></param>
        /// <param name="totalPoints"></param>
        /// <param name="hotelCountryCode"></param>
        private void ModifyInCancel
            (HotelSearchEntity hotelSearchEntity, HotelRoomRateEntity hotelRoomRateEntity, double totalPoints,
             string hotelCountryCode)
        {
            int noOfChildren = 0;
            int noOfAdults = 0;
            foreach (BookingDetailsEntity bkng in BookingEngineSessionWrapper.AllBookingDetails)
            {
                noOfChildren += bkng.HotelSearch.ChildrenPerRoom;
                noOfAdults += bkng.HotelSearch.AdultsPerRoom;
            }
            ReservationInfo.NoOfAdults = noOfAdults;
            ReservationInfo.NoOfChildren = noOfChildren;

            double dblTotalRate = Utility.CalculateTotalRateInCaseOfModifyCancel();
            string totalRateString = string.Empty;
            switch (hotelSearchEntity.SearchingType)
            {
                case SearchType.REDEMPTION:
                    {
                        totalRateString =
                            Utility.GetRoomRateString
                                (null, null, hotelRoomRateEntity.Points, hotelSearchEntity.SearchingType,
                                 hotelSearchEntity.ArrivalDate.Year, hotelCountryCode, hotelSearchEntity.NoOfNights,
                                 hotelSearchEntity.RoomsPerNight);
                        break;
                    }
                case SearchType.VOUCHER:
                    {
                        totalRateString =
                            Utility.GetRoomRateString(null, null, totalPoints, hotelSearchEntity.SearchingType,
                                                      hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                                      hotelSearchEntity.NoOfNights,
                                                      hotelSearchEntity.RoomsPerNight);
                        break;
                    }
                default:
                    {
                        RateEntity tempRateEntity =
                            new RateEntity(dblTotalRate, hotelRoomRateEntity.TotalRate.CurrencyCode);
                        totalRateString =
                            Utility.GetRoomRateString(null, tempRateEntity, totalPoints, hotelSearchEntity.SearchingType,
                                                      hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                                      hotelSearchEntity.NoOfNights,
                                                      hotelSearchEntity.RoomsPerNight);
                        break;
                    }
            }
            ReservationInfo.SetTotalRoomRate(totalRateString);
            ReservationInfo.SetRoomDetails(BookingEngineSessionWrapper.AllBookingDetails);

            if (BookingEngineSessionWrapper.AllBookingDetails != null)
            {
                ReservationInfo.NoOfRooms = BookingEngineSessionWrapper.AllBookingDetails.Count.ToString();
                ReservationInfo.PopulateCancellationNumbersInModify();
            }
        }

        /// <summary>
        /// Returns false when block code booking and block is not configured in the CMS.
        /// </summary>
        /// <returns>False only when block booking and Block code is not configured in CMS</returns>
        private bool IsFieldsNeedToBeSetup()
        {
            bool returnValue = true;
            if (BookingEngineSessionWrapper.BookingDetails != null && BookingEngineSessionWrapper.BookingDetails.HotelSearch != null)
            {
                if (Utility.IsBlockCodeBooking)
                {
                    Block block =
                        ContentDataAccess.GetBlockCodePages(BookingEngineSessionWrapper.BookingDetails.HotelSearch.CampaignCode);
                    if (null == block)
                    {
                        returnValue = false;
                    }
                }
            }
            return returnValue;
        }
    }
}