using System;
using System.Configuration;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    public partial class RateDescriptionforEMAIL : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        //Vrushali | Res 2.0 | This overloaded method is added to fix the bug.
        //470132- Client Feedback|Maintain same order of rate category in �About our rate� section
        /// <summary>
        /// Rates the caegory details.
        /// </summary>
        /// <param name="rateCategory">The rate category.</param>
        /// <param name="categoryCount">The category count.</param>
        public void RateCaegoryDetails(RateCategoryHeaderDisplay rateCategory)
        {
            if (rateCategory != null)
            {
                string rateCategoryColor = rateCategory.RateCategoryColor;
                string defaultColorString = ConfigurationManager.AppSettings.Get("SelectRate.RateCategoryDefaultColor");
                string color = string.Empty;

                if (!string.IsNullOrEmpty(rateCategoryColor))
                {
                    color = "height: 5px; background-color: " + rateCategoryColor + ";";
                    if (rateCategoryColor == "Pink" || rateCategoryColor == "PINK" || rateCategoryColor == "pink")
                    {
                        //$(this).css("background","#c36").removeClass('grayMe');
                        color = "height: 5px; background-color: #cc3366;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else if (rateCategoryColor == "Grey" || rateCategoryColor == "GREY" || rateCategoryColor == "grey")
                        //Res 2.0 - Parvathi : artf1163673: Gray line missing on Redemtion conf e-mail
                    {
                        color = "height: 5px; background-color: #828282;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else if (rateCategoryColor == "Blue" || rateCategoryColor == "BLUE" ||
                             rateCategoryColor == "blue")
                        //Res 2.0 - Parvathi : artf1163673: Gray line missing on Redemtion conf e-mail
                    {
                        color = "height: 5px; background-color: #218da3;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else if (rateCategoryColor == "Orange" || rateCategoryColor == "ORANGE" ||
                             rateCategoryColor == "orange")
                        //Res 2.0 - Parvathi : artf1163673: Gray line missing on Redemtion conf e-mail
                    {
                        color = "height: 5px; background-color: #d17a33;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else if (rateCategoryColor == "Brown" || rateCategoryColor == "BROWN" ||
                             rateCategoryColor == "brown")
                        //Res 2.0 - Parvathi : artf1163673: Gray line missing on Redemtion conf e-mail
                    {
                        color = "height: 5px; background-color: #614132;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else
                        flexBGColor.Attributes["style"] = color;
                }
                else if ((defaultColorString != null))
                {
                    color = "height: 5px; background-color: " + defaultColorString + ";";
                    flexBGColor.Attributes["style"] = color;
                }
                else
                {
                    flexBGColor.Attributes["style"] = string.Empty;
                }

                SearchType searchType = SearchCriteriaSessionWrapper.SearchCriteria.SearchingType;

                //Vrushali | artf1165343: Missing hyphen for rate names in select rate page.
                // As per the comments given by Erika, rate Heading in About Our Rate should always come from name field in CMS.
                // Heading1, Heading2, Heading3 are used only in rate columns in select rate page.

                rateHeading.Text = rateCategory.Title;

                //string headerText1 = string.Empty;
                //string headerText2 = string.Empty;

                //string headerText3 = string.Empty;

                //switch (searchType)
                //{
                //    case SearchType.REGULAR:
                //        {
                //            if (!string.IsNullOrEmpty(rateCategory.Headers[0]) || !string.IsNullOrEmpty(rateCategory.Headers[1])
                //            || !string.IsNullOrEmpty(rateCategory.Headers[2]))
                //            {
                //                headerText1 = Truncate(rateCategory.Headers[0], 14);
                //                headerText2 = Truncate(rateCategory.Headers[1], 14);
                //                headerText3 = Truncate(rateCategory.Headers[2], 14);
                //                rateHeading.Text = headerText1 + " " + headerText2 + " " + headerText3;
                //            }
                //            else
                //            {
                //                rateHeading.Text = rateCategory.Title;
                //            }
                //            break;
                //        }
                //    case SearchType.VOUCHER:
                //        {
                //            if (!string.IsNullOrEmpty(rateCategory.Headers[0]) || !string.IsNullOrEmpty(rateCategory.Headers[1])
                //            || !string.IsNullOrEmpty(rateCategory.Headers[2]))
                //            {
                //                headerText1 = Truncate(rateCategory.Headers[0], 14);
                //                //DynamicRateHeader1.InnerHtml = headerText1.ToString();

                //                headerText2 = Truncate(rateCategory.Headers[1], 14);
                //                //DynamicRateHeader2.InnerHtml = headerText2.ToString();

                //                headerText3 = Truncate(rateCategory.Headers[2], 14);
                //                //DynamicRateHeader3.InnerHtml = headerText3.ToString();
                //                rateHeading.Text = headerText1 + " " + headerText2 + " " + headerText3;
                //            }
                //            else
                //            {
                //                rateHeading.Text = rateCategory.Title;
                //            }
                //            break;
                //        }
                //    case SearchType.CORPORATE:
                //        {
                //            if (Utility.IsBlockCodeBooking)
                //            {
                //                rateHeading.Text = rateCategory.Title;
                //            }
                //            else
                //            {
                //                if (rateCategory.Id.ToUpper().Equals("EARLY") || rateCategory.Id.ToUpper().Equals("FLEX"))
                //                {
                //                    if (!string.IsNullOrEmpty(rateCategory.Headers[0]) || !string.IsNullOrEmpty(rateCategory.Headers[1])
                //                    || !string.IsNullOrEmpty(rateCategory.Headers[2]))
                //                    {
                //                        headerText1 = Truncate(rateCategory.Headers[0], 14);
                //                        //DynamicRateHeader1.InnerHtml = headerText1.ToString();

                //                        headerText2 = Truncate(rateCategory.Headers[1], 14);
                //                        //DynamicRateHeader2.InnerHtml = headerText2.ToString();

                //                        headerText3 = Truncate(rateCategory.Headers[2], 14);
                //                        //DynamicRateHeader3.InnerHtml = headerText3.ToString();
                //                        rateHeading.Text = headerText1 + " " + headerText2 + " " + headerText3;
                //                    }
                //                    else
                //                    {
                //                        rateHeading.Text = rateCategory.Title;
                //                    }
                //                }
                //                else
                //                    rateHeading.Text = rateCategory.Title;
                //            }
                //            break;
                //        }                      
                //    case SearchType.BONUSCHEQUE:
                //        {
                //            if (!string.IsNullOrEmpty(rateCategory.Headers[0]) || !string.IsNullOrEmpty(rateCategory.Headers[1])
                //            || !string.IsNullOrEmpty(rateCategory.Headers[2]))
                //            {
                //                headerText1 = Truncate(rateCategory.Headers[0], 14);
                //                //DynamicRateHeader1.InnerHtml = headerText1.ToString();

                //                headerText2 = Truncate(rateCategory.Headers[1], 14);
                //                //DynamicRateHeader2.InnerHtml = headerText2.ToString();

                //                headerText3 = Truncate(rateCategory.Headers[2], 14);
                //                //DynamicRateHeader3.InnerHtml = headerText3.ToString();
                //                rateHeading.Text = headerText1 + " " + headerText2 + " " + headerText3;
                //            }
                //            else
                //            {
                //                rateHeading.Text = rateCategory.Title;
                //            }
                //            break;
                //        }
                //    case SearchType.REDEMPTION:
                //        {
                //            if (!string.IsNullOrEmpty(rateCategory.Headers[0]) || !string.IsNullOrEmpty(rateCategory.Headers[1])
                //            || !string.IsNullOrEmpty(rateCategory.Headers[2]))
                //            {
                //                headerText1 = Truncate(rateCategory.Headers[0], 14);
                //                //DynamicRateHeader1.InnerHtml = headerText1.ToString();

                //                headerText2 = Truncate(rateCategory.Headers[1], 14);
                //                //DynamicRateHeader2.InnerHtml = headerText2.ToString();

                //                headerText3 = Truncate(rateCategory.Headers[2], 14);
                //                //DynamicRateHeader3.InnerHtml = headerText3.ToString();
                //                rateHeading.Text = headerText1 + " " + headerText2 + " " + headerText3;

                //            }
                //            else
                //            {
                //                rateHeading.Text = rateCategory.Title;
                //            }
                //            break;
                //        }
                //    default:
                //        break;
                //}
                //25th June:r2.0: Set rate heading and Rate description on About our rates control.
                //rateHeading.Text = rateCategory.Title;

                //"Page type ""Rate Category"" has in the documentation a new, large text field named ""About Our Rate"", which can be formatted.  
                //This field is actually redundant and we only need the formatting options to be added to the existing large text field ""Rate Category Description""."
                //[Done bcos of the above request for r2.0 UAT[aboutOurRate is same as RateCategoryDescription
                //aboutRateDesc.Text = rateCategory.AboutOurRate;

                if (Request.QueryString["command"] != null && Request.QueryString["command"].ToString() == "print")
                {
                    aboutRateDesc.Text = "<div class='roomRateDesc'>" + rateCategory.Description + "</div>";
                }
                else
                {
                    aboutRateDesc.Text = rateCategory.Description;
                }
            }
        }

        /// <summary>
        /// set rate description and Rate heading in Ratedescription control
        /// </summary>
        /// <param name="rateCategory"></param>
        public void RateCaegoryDetails(RateCategory rateCategory)
        {
            if (rateCategory != null)
            {
                //25th June : R2.0: Add the class attribute dyanamicaly
                //rateInfo.Attributes.Add("class", "room" + (categoryCount + 1));
                //h4Heading.Attributes.Add("class", "ratesSprite " + rateCategory.RateCategoryId);
                string rateCategoryColor = rateCategory.RateCategoryColor;
                string defaultColorString = ConfigurationManager.AppSettings.Get("SelectRate.RateCategoryDefaultColor");

                string color = string.Empty;

                if (!string.IsNullOrEmpty(rateCategoryColor))
                {
                    color = "height: 5px; background-color: " + rateCategoryColor + ";";
                    if (rateCategoryColor == "Pink" || rateCategoryColor == "PINK" || rateCategoryColor == "pink")
                    {
                        //$(this).css("background","#c36").removeClass('grayMe');
                        color = "height: 5px; background-color: #cc3366;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else if (rateCategoryColor == "Grey" || rateCategoryColor == "GREY" || rateCategoryColor == "grey")
                        //Res 2.0 - Parvathi : artf1163673: Gray line missing on Redemtion conf e-mail
                    {
                        color = "height: 5px; background-color: #828282;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else if (rateCategoryColor == "Blue" || rateCategoryColor == "BLUE" ||
                             rateCategoryColor == "blue")
                        //Res 2.0 - Parvathi : artf1163673: Gray line missing on Redemtion conf e-mail
                    {
                        color = "height: 5px; background-color: #218da3;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else if (rateCategoryColor == "Orange" || rateCategoryColor == "ORANGE" ||
                             rateCategoryColor == "orange")
                        //Res 2.0 - Parvathi : artf1163673: Gray line missing on Redemtion conf e-mail
                    {
                        color = "height: 5px; background-color: #d17a33;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else if (rateCategoryColor == "Brown" || rateCategoryColor == "BROWN" ||
                             rateCategoryColor == "brown")
                        //Res 2.0 - Parvathi : artf1163673: Gray line missing on Redemtion conf e-mail
                    {
                        color = "height: 5px; background-color: #614132;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else
                        flexBGColor.Attributes["style"] = color;
                }
                else if ((defaultColorString != null))
                {
                    color = "height: 5px; background-color: " + defaultColorString + ";";
                    flexBGColor.Attributes["style"] = color;
                }
                else
                {
                    flexBGColor.Attributes["style"] = string.Empty;
                }
                SearchType searchType = SearchCriteriaSessionWrapper.SearchCriteria.SearchingType;

                //Vrushali | artf1165343: Missing hyphen for rate names in select rate page.
                // As per the comments given by Erika, rate Heading in About Our Rate should always come from name field in CMS.
                // Heading1, Heading2, Heading3 are used only in rate columns in select rate page.

                rateHeading.Text = rateCategory.RateCategoryName;

                //string headerText1 = string.Empty;
                //string headerText2 = string.Empty;
                //string headerText3 = string.Empty;
                //switch (searchType)
                //{
                //    case SearchType.REGULAR:
                //        {
                //            if (!string.IsNullOrEmpty(rateCategory.HeaderList[0]) || !string.IsNullOrEmpty(rateCategory.HeaderList[1])
                //            || !string.IsNullOrEmpty(rateCategory.HeaderList[2]))
                //            {
                //                headerText1 = Truncate(rateCategory.HeaderList[0], 14);
                //                //DynamicRateHeader1.InnerHtml = headerText1.ToString();

                //                headerText2 = Truncate(rateCategory.HeaderList[1], 14);
                //                //DynamicRateHeader2.InnerHtml = headerText2.ToString();

                //                headerText3 = Truncate(rateCategory.HeaderList[2], 14);
                //                //DynamicRateHeader3.InnerHtml = headerText3.ToString();
                //                rateHeading.Text = headerText1 + " " + headerText2 + " " + headerText3;
                //            }
                //            else
                //            {
                //                rateHeading.Text = rateCategory.RateCategoryName;
                //            }
                //            break;
                //        }
                //    case SearchType.VOUCHER:
                //        {
                //            if (!string.IsNullOrEmpty(rateCategory.HeaderList[0]) || !string.IsNullOrEmpty(rateCategory.HeaderList[1])
                //            || !string.IsNullOrEmpty(rateCategory.HeaderList[2]))
                //            {
                //                headerText1 = Truncate(rateCategory.HeaderList[0], 14);
                //                //DynamicRateHeader1.InnerHtml = headerText1.ToString();

                //                headerText2 = Truncate(rateCategory.HeaderList[1], 14);
                //                //DynamicRateHeader2.InnerHtml = headerText2.ToString();

                //                headerText3 = Truncate(rateCategory.HeaderList[2], 14);
                //                //DynamicRateHeader3.InnerHtml = headerText3.ToString();
                //                rateHeading.Text = headerText1 + " " + headerText2 + " " + headerText3;
                //            }
                //            else
                //            {
                //                rateHeading.Text = rateCategory.RateCategoryName;
                //            }
                //            break;
                //        }
                //    case SearchType.CORPORATE:
                //        {
                //            if (Utility.IsBlockCodeBooking)
                //            {
                //                rateHeading.Text = rateCategory.RateCategoryName;
                //            }
                //            else
                //            {
                //                if (rateCategory.RateCategoryId.ToUpper().Equals("EARLY") || rateCategory.RateCategoryId.ToUpper().Equals("FLEX"))
                //                {
                //                    if (!string.IsNullOrEmpty(rateCategory.HeaderList[0]) || !string.IsNullOrEmpty(rateCategory.HeaderList[1])
                //                    || !string.IsNullOrEmpty(rateCategory.HeaderList[2]))
                //                    {
                //                        headerText1 = Truncate(rateCategory.HeaderList[0], 14);
                //                        //DynamicRateHeader1.InnerHtml = headerText1.ToString();

                //                        headerText2 = Truncate(rateCategory.HeaderList[1], 14);
                //                        //DynamicRateHeader2.InnerHtml = headerText2.ToString();

                //                        headerText3 = Truncate(rateCategory.HeaderList[2], 14);
                //                        //DynamicRateHeader3.InnerHtml = headerText3.ToString();
                //                        rateHeading.Text = headerText1 + " " + headerText2 + " " + headerText3;
                //                    }
                //                    else
                //                    {
                //                        rateHeading.Text = rateCategory.RateCategoryName;
                //                    }
                //                }
                //                else
                //                    rateHeading.Text = rateCategory.RateCategoryName;
                //            }
                //            break;
                //        }
                //    case SearchType.BONUSCHEQUE:
                //        {
                //            if (!string.IsNullOrEmpty(rateCategory.HeaderList[0]) || !string.IsNullOrEmpty(rateCategory.HeaderList[1])
                //            || !string.IsNullOrEmpty(rateCategory.HeaderList[2]))
                //            {
                //                headerText1 = Truncate(rateCategory.HeaderList[0], 14);
                //                //DynamicRateHeader1.InnerHtml = headerText1.ToString();

                //                headerText2 = Truncate(rateCategory.HeaderList[1], 14);
                //                //DynamicRateHeader2.InnerHtml = headerText2.ToString();

                //                headerText3 = Truncate(rateCategory.HeaderList[2], 14);
                //                //DynamicRateHeader3.InnerHtml = headerText3.ToString();
                //                rateHeading.Text = headerText1 + " " + headerText2 + " " + headerText3;
                //            }
                //            else
                //            {
                //                rateHeading.Text = rateCategory.RateCategoryName;
                //            }
                //            break;
                //        }
                //    case SearchType.REDEMPTION:
                //        {
                //            if (!string.IsNullOrEmpty(rateCategory.HeaderList[0]) || !string.IsNullOrEmpty(rateCategory.HeaderList[1])
                //            || !string.IsNullOrEmpty(rateCategory.HeaderList[2]))
                //            {
                //                headerText1 = Truncate(rateCategory.HeaderList[0], 14);
                //                //DynamicRateHeader1.InnerHtml = headerText1.ToString();

                //                headerText2 = Truncate(rateCategory.HeaderList[1], 14);
                //                //DynamicRateHeader2.InnerHtml = headerText2.ToString();

                //                headerText3 = Truncate(rateCategory.HeaderList[2], 14);
                //                //DynamicRateHeader3.InnerHtml = headerText3.ToString();
                //                rateHeading.Text = headerText1 + " " + headerText2 + " " + headerText3;

                //            }
                //            else
                //            {
                //                rateHeading.Text = rateCategory.RateCategoryName;
                //            }
                //            break;
                //        }
                //    default:
                //        break;
                //}
                //25th June:r2.0: Set rate heading and Rate description on About our rates control.
                //rateHeading.Text = rateCategory.RateCategoryName;

                //"Page type ""Rate Category"" has in the documentation a new, large text field named ""About Our Rate"", which can be formatted.  
                //This field is actually redundant and we only need the formatting options to be added to the existing large text field ""Rate Category Description""."
                //[Done bcos of the above request for r2.0 UAT[aboutOurRate is same as RateCategoryDescription
                aboutRateDesc.Text = rateCategory.RateCategoryDescription;
            }
        }

        //Release R 2.0 | Reservation 2.0 | Dynamic header in Select rate page
        // Add a new method to set the max lenth of 14th charector.(shameem)
        public static string Truncate(string source, int length)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length);
            }
            return source;
        }

        public void RateCategoryDetails(Block block)
        {
            if (block != null)
            {
                //25th June : R2.0: Add the class attribute dyanamicaly
                //rateInfo.Attributes.Add("class", "room" + (categoryCount + 1));
                //h4Heading.Attributes.Add("class", "ratesSprite " + rateCategory.RateCategoryId);
                string categoryColor = block.BlockCategoryColor;
                string defaultColorString = ConfigurationManager.AppSettings.Get("SelectRate.RateCategoryDefaultColor");
                string color = string.Empty;

                if (!string.IsNullOrEmpty(categoryColor))
                {
                    color = "height: 5px; background-color: " + categoryColor + ";";
                    if (categoryColor == "Pink" || categoryColor == "PINK" || categoryColor == "pink")
                    {
                        //$(this).css("background","#c36").removeClass('grayMe');
                        color = "height: 5px; background-color: #cc3366;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else if (categoryColor == "Grey" || categoryColor == "GREY" || categoryColor == "grey")
                        //Res 2.0 - Parvathi : artf1163673: Gray line missing on Redemtion conf e-mail
                    {
                        color = "height: 5px; background-color: #828282;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else if (categoryColor == "Blue" || categoryColor == "BLUE" || categoryColor == "blue")
                        //Res 2.0 - Parvathi : artf1163673: Gray line missing on Redemtion conf e-mail
                    {
                        color = "height: 5px; background-color: #218da3;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else if (categoryColor == "Orange" || categoryColor == "ORANGE" || categoryColor == "orange")
                        //Res 2.0 - Parvathi : artf1163673: Gray line missing on Redemtion conf e-mail
                    {
                        color = "height: 5px; background-color: #d17a33;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    else if (categoryColor == "Brown" || categoryColor == "BROWN" || categoryColor == "brown")
                        //Res 2.0 - Parvathi : artf1163673: Gray line missing on Redemtion conf e-mail
                    {
                        color = "height: 5px; background-color: #614132;";
                        flexBGColor.Attributes["style"] = color;
                    }
                    flexBGColor.Attributes["style"] = color;
                }
                else if ((defaultColorString != null))
                {
                    color = "height: 5px; background-color: " + defaultColorString + ";";
                    flexBGColor.Attributes["style"] = color;
                }
                else
                {
                    flexBGColor.Attributes["style"] = string.Empty;
                }

                //25th June:r2.0: Set rate heading and Rate description on About our rates control.
                rateHeading.Text = block.BlockCategoryName;

                //"Page type ""Rate Category"" has in the documentation a new, large text field named ""About Our Rate"", which can be formatted.  
                //This field is actually redundant and we only need the formatting options to be added to the existing large text field ""Rate Category Description""."
                //[Done bcos of the above request for r2.0 UAT[aboutOurRate is same as RateCategoryDescription
                //aboutRateDesc.Text = block.AboutOurRate;

                aboutRateDesc.Text = block.BlockDescription;
            }
        }
    }
}