using System;
using System.Text;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Entity;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// CustomerServiceContactUs
    /// </summary>
    public partial class CustomerServiceContactUs : EPiServer.UserControlBase
    {
        private string contactUsQueriesXML = string.Empty;

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                contactUsQueriesXML = CurrentPage["MultiRecipientEmailAddresses"] as string;

                if (!IsPostBack)
                {
                    PopulateForm();
                    btnSubmit.Attributes.Add("onclick",
                                             "javascript:return performValidation(PAGE_CUSTOMER_SERVICE_CONTACT_US);");
                    spnSubmit.Attributes.Add("onclick",
                                             "javascript:return performValidation(PAGE_CUSTOMER_SERVICE_CONTACT_US);");
                    ddlNatureOfQuery.Attributes.Add("onchange", "javascript:queryTypeValidation();");
                }
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
        }

        /// <summary>
        /// btnSubmit_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string senderEmail = string.Empty;
            string[] recipientEmail = new string[1];
            string emailSubject = string.Empty;
            string emailBody = string.Empty;

            try
            {
                clientErrorDivCSCU.InnerText = string.Empty;
                if (contactUsQueriesXML != string.Empty)
                {
                    CustomerServiceContactUsEntity customerServiceContactUsEntity =
                        new CustomerServiceContactUsEntity(contactUsQueriesXML);
                    if (customerServiceContactUsEntity != null)
                    {
                        senderEmail = getSender();
                        recipientEmail[0] = getRecipient(customerServiceContactUsEntity);

                        if (recipientEmail[0] == string.Empty)
                        {
                            clientErrorDivCSCU.InnerText =
                                WebUtil.GetTranslatedText(
                                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalidHotel");
                            EnableDisableHotelList();
                        }
                        else
                        {
                            emailSubject = ddlNatureOfQuery.SelectedItem.Text;
                            emailBody = getEmailBody();
                            EmailEntity emailEntity = new EmailEntity();
                            emailEntity.Sender = senderEmail;
                            emailEntity.Recipient = recipientEmail;
                            emailEntity.Subject = emailSubject;
                            emailEntity.Body = emailBody;
                            CommunicationService.SendMail(emailEntity, null);
                            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.CS_CONTACTUS_THANKYOU),
                                              false);
                        }
                    }
                }
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
        }

        #region Private Methods

        #region getSender

        /// <summary>
        /// This fetches the email id of the sender from which email 
        /// can be send to recipient.
        /// </summary>
        /// <returns>Sender</returns>
        private string getSender()
        {
            string sender = string.Empty;

            try
            {
                sender = CurrentPage["SenderEmailAddress"] as string;
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx);
            }
            return sender;
        }

        #endregion getSender

        #region getRecipient

        /// <summary>
        /// This will determine to which email id the email can be sent.
        /// </summary>
        /// <returns></returns>
        private string getRecipient(CustomerServiceContactUsEntity CSContactUsEntity)
        {
            string typeOfQuery = string.Empty;
            string typeOfQueryEmail = string.Empty;
            string hotelName = string.Empty;
            string hotelNameEmail = string.Empty;
            string recipientEmail = string.Empty;

            try
            {
                if (CSContactUsEntity != null)
                {
                    typeOfQuery = ddlNatureOfQuery.SelectedItem.Text;
                    hotelName = ddlHotelList.SelectedItem.Text;
                    foreach (ContactUsType contactUsType in CSContactUsEntity.NatureOfQueries)
                    {
                        if (contactUsType.DisplayText.ToLower() == typeOfQuery.ToLower())
                        {
                            typeOfQueryEmail = contactUsType.Email;
                            break;
                        }
                    }
                    if (typeOfQueryEmail != string.Empty)
                    {
                        recipientEmail = typeOfQueryEmail;
                    }
                    else
                    {
                        foreach (ContactUsType contactUsType in CSContactUsEntity.Hotels)
                        {
                            if (contactUsType.DisplayText.ToLower() == hotelName.ToLower())
                            {
                                hotelNameEmail = contactUsType.Email;
                                break;
                            }
                        }

                        recipientEmail = hotelNameEmail;
                    }
                }
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx);
            }
            return recipientEmail;
        }

        #endregion getRecipient

        #region getEmailBody

        /// <summary>
        /// Generating the email body with the contents being entered by the user on the form.
        /// </summary>
        /// <returns></returns>
        private string getEmailBody()
        {
            StringBuilder emailBody = new StringBuilder();

            try
            {
                emailBody.Append(WebUtil.GetTranslatedText("Hotels: "));
                emailBody.Append(ddlHotelList.SelectedItem.Text);
                emailBody.Append("<BR>");

                emailBody.Append(WebUtil.GetTranslatedText("comments: "));
                emailBody.Append(txtComments.Value);
                emailBody.Append("<BR>");

                emailBody.Append(WebUtil.GetTranslatedText("name: "));
                emailBody.Append(txtCustomerName.Value);
                emailBody.Append("<BR>");

                emailBody.Append(WebUtil.GetTranslatedText("membership_number: "));
                emailBody.Append(txtMembershipNo.Value);
                emailBody.Append("<BR>");

                emailBody.Append(WebUtil.GetTranslatedText("telephone: "));
                emailBody.Append(txtTelephoneNo.Value);
                emailBody.Append("<BR>");

                emailBody.Append(WebUtil.GetTranslatedText("e_mail: "));
                emailBody.Append(txtEmailId.Value);
                emailBody.Append("<BR>");
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx);
            }
            return emailBody.ToString();
        }

        #endregion getEmailBody

        #region PopulateForm

        /// <summary>
        /// This will initialise the form with all the controls prepopulated.
        /// </summary>
        private void PopulateForm()
        {
            try
            {
                txtComments.Value = string.Empty;
                if (contactUsQueriesXML != string.Empty)
                {
                    CustomerServiceContactUsEntity customerServiceContactUsEntity =
                        new CustomerServiceContactUsEntity(contactUsQueriesXML);
                    foreach (ContactUsType contactUsType in customerServiceContactUsEntity.NatureOfQueries)
                    {
                        ddlNatureOfQuery.Items.Add(contactUsType.DisplayText);
                    }

                    foreach (ContactUsType contactUsType in customerServiceContactUsEntity.Hotels)
                    {
                        ddlHotelList.Items.Add(contactUsType.DisplayText);
                    }
                    ddlHotelList.Enabled = false;
                }
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx);
            }
        }

        #endregion PopulateForm

        #region EnableDisableHotelList

        /// <summary>
        /// Based on the selected nature of query, hotellist should be either disabled or enabled.
        /// </summary>
        private void EnableDisableHotelList()
        {
            if ((ddlNatureOfQuery.SelectedIndex == 0) ||
                (ddlNatureOfQuery.SelectedIndex == 1) ||
                (ddlNatureOfQuery.SelectedIndex == 3) ||
                (ddlNatureOfQuery.SelectedIndex == 4) ||
                (ddlNatureOfQuery.SelectedIndex == 5))
            {
                ddlHotelList.SelectedIndex = 0;
                ddlHotelList.Enabled = false;
            }
            else
            {
                ddlHotelList.Enabled = true;
            }
        }

        #endregion EnableDisableHotelList

        #endregion Private Methods
    }
}