<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Calendar.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.Calendar" %>
<%@ Import Namespace="System.Collections.Generic" %>

<script type="text/javascript" language="JavaScript">
// If the object array is not available in java script.Please don't add white space as it will go to client machine.
if(!masterCalendarWeekList)
{
    <%
        // Gets the list of week statring dates between the date range with respective week numbers.
        SortedList<DateTime, int> weekGroupList = GetWeekNumbers();
        foreach (DateTime key in weekGroupList.Keys)
        {
            // Storing the collection in javascript object
%>populateCalendarWeeks('<%= key.Month + "/" + key.Day + "/" + key.Year %>', '<%= weekGroupList[key] %>');<% }%>
}        
</script>