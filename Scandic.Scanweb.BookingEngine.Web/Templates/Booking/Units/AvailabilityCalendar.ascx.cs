//  Description					: Code Behind class for AvalibilityCalendar control       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Bhavya Shekar                              	          //
//  Author email id				:                           							  //
//  Creation Date				: 27th July  2010									      //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System NameSpaces

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion

    #region Scandic NameSpaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// This control displays the availability of the selected hotel for a date range 
    /// </summary>
    public partial class AvailabilityCalendar : EPiServer.UserControlBase
    {
        #region Member Variables

        private int CAROUSEL_COUNT = ConfigurationManager.AppSettings["SelectRate.AvailabilityCalendarItemsCount"] !=
                                     null
                                         ? Convert.ToInt32(
                                             ConfigurationManager.AppSettings[
                                                 "SelectRate.AvailabilityCalendarItemsCount"])
                                         : AppConstants.CAROUSEL_COUNT;

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation))
                {
                    modifyFlowTab.Attributes.Add("style", "display:none");
                }
                else
                {
                    GenerateAvalibilityCalendarItems();
                }
                HtmlInputHidden selectedCalendarItemIndex =
                    this.Parent.FindControl("selectedCalendarItemIndex") as HtmlInputHidden;
                if (selectedCalendarItemIndex != null)
                {
                    selectedCalendarItemIndex.Value = Reservation2SessionWrapper.SelectedCalendarItemIndex.ToString();
                }
                HtmlInputHidden firstBatch = this.Parent.FindControl("firstBatch") as HtmlInputHidden;
                if (firstBatch != null)
                {
                    firstBatch.Value = Reservation2SessionWrapper.IsCalendarFirstBatch.ToString().ToLower();
                }
                HtmlInputHidden lastBatch = this.Parent.FindControl("lastBatch") as HtmlInputHidden;
                if (lastBatch != null)
                {
                    lastBatch.Value = Reservation2SessionWrapper.IsCalendarLastBatch.ToString().ToLower();
                }
            }

            HtmlInputHidden leftVisibleCarouselIndex =
                this.Parent.FindControl("leftVisibleCarouselIndex") as HtmlInputHidden;
            if (leftVisibleCarouselIndex != null)
            {
                leftVisibleCarouselIndex.Value = Reservation2SessionWrapper.LeftVisibleCarouselIndex.ToString();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnOnNextCalendarRange control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnOnNextCalendarRange_Click(object sender, EventArgs e)
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            AvailabilityCalendarEntity availabilityCalendar = Reservation2SessionWrapper.AvailabilityCalendar;

            if (search != null && availabilityCalendar != null && availabilityCalendar.ListAvailCalendarItems != null
                && availabilityCalendar.ListAvailCalendarItems.Count > 0)
            {
                DateTime iteratorArrivalDate, iteratorDepartureDate;
                DateTime departureDateLimit = DateTime.Today.AddYears(AppConstants.NO_OF_YEARS_IN_AVAILCALENDAR);

                availabilityCalendar.LeftCarouselIndex = ++availabilityCalendar.RightCarouselIndex;

                if (availabilityCalendar.LeftCarouselIndex == availabilityCalendar.ListAvailCalendarItems.Count
                    ||
                    availabilityCalendar.LeftCarouselIndex + AppConstants.CAROUSEL_COUNT >
                    availabilityCalendar.ListAvailCalendarItems.Count)
                {
                    iteratorArrivalDate =
                        availabilityCalendar.ListAvailCalendarItems[
                            availabilityCalendar.ListAvailCalendarItems.Count - 1].ArrivalDate.AddDays(
                                AppConstants.CALENDARITEM_GAP_DAYS);
                    iteratorDepartureDate =
                        availabilityCalendar.ListAvailCalendarItems[
                            availabilityCalendar.ListAvailCalendarItems.Count - 1].DepartureDate.AddDays(
                                AppConstants.CALENDARITEM_GAP_DAYS);

                    for (int i = 0; i < CAROUSEL_COUNT; i++, availabilityCalendar.RightCarouselIndex++)
                    {
                        if (iteratorArrivalDate >= departureDateLimit)
                        {
                            availabilityCalendar.RightCarouselIndex = availabilityCalendar.ListAvailCalendarItems.Count;
                            availabilityCalendar.LeftCarouselIndex = availabilityCalendar.RightCarouselIndex -
                                                                     CAROUSEL_COUNT;
                            HtmlInputHidden lastBatch = this.Parent.FindControl("lastBatch") as HtmlInputHidden;
                            if (lastBatch != null)
                            {
                                lastBatch.Value = "true";
                            }
                            Reservation2SessionWrapper.IsCalendarLastBatch = true;
                            break;
                        }

                        AvailCalendarItemEntity calendarItem = new AvailCalendarItemEntity();
                        calendarItem.ArrivalDate = iteratorArrivalDate;
                        calendarItem.DepartureDate = iteratorDepartureDate;
                        availabilityCalendar.ListAvailCalendarItems.Add(calendarItem);

                        iteratorArrivalDate = iteratorArrivalDate.AddDays(AppConstants.CALENDARITEM_GAP_DAYS);
                        iteratorDepartureDate = iteratorDepartureDate.AddDays(AppConstants.CALENDARITEM_GAP_DAYS);
                    }
                    availabilityCalendar.RightCarouselIndex--;
                    List<AvailCalendarItemEntity> listAvailCalendarItems =
                        availabilityCalendar.ListAvailCalendarItems as List<AvailCalendarItemEntity>;
                    listAvailCalendarItems.Sort(new ArrivalDateComparer());

                    AvailabilityController availController = new AvailabilityController();
                    availController.MultiroomMultidateCalendarAvailSearch(search, availabilityCalendar);
                }
                else if (availabilityCalendar.LeftCarouselIndex < availabilityCalendar.ListAvailCalendarItems.Count)
                {
                    if ((availabilityCalendar.LeftCarouselIndex + CAROUSEL_COUNT - 1) <
                        availabilityCalendar.ListAvailCalendarItems.Count)
                    {
                        availabilityCalendar.RightCarouselIndex = availabilityCalendar.LeftCarouselIndex +
                                                                  CAROUSEL_COUNT - 1;
                    }
                    else
                    {
                        availabilityCalendar.RightCarouselIndex = availabilityCalendar.ListAvailCalendarItems.Count - 1;
                    }
                }

                Reservation2SessionWrapper.LeftVisibleCarouselIndex = 0;
                Reservation2SessionWrapper.SelectedCalendarItemIndex = -1;

                HtmlInputHidden firstBatch = this.Parent.FindControl("firstBatch") as HtmlInputHidden;
                if (firstBatch != null)
                {
                    firstBatch.Value = "false";
                }
                Reservation2SessionWrapper.IsCalendarFirstBatch = false;
                Reservation2SessionWrapper.AvailabilityCalendarAccessed = true;

                if (CurrentPage.PageName.Equals("Modify select rate", StringComparison.CurrentCultureIgnoreCase))
                {
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE), false);
                }
                else
                {
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE), false);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnOnPrevCalendarRange control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnOnPrevCalendarRange_Click(object sender, EventArgs e)
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            AvailabilityCalendarEntity availabilityCalendar = Reservation2SessionWrapper.AvailabilityCalendar;

            if (search != null && availabilityCalendar != null && availabilityCalendar.ListAvailCalendarItems != null
                && availabilityCalendar.ListAvailCalendarItems.Count > 0)
            {
                DateTime arrivalDateLimit = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                availabilityCalendar.RightCarouselIndex = --availabilityCalendar.LeftCarouselIndex;

                DateTime iteratorArrivalDate, iteratorDepartureDate;
                if (availabilityCalendar.RightCarouselIndex == -1)
                {
                    availabilityCalendar.LeftCarouselIndex = 0;
                    availabilityCalendar.RightCarouselIndex = 0;

                    iteratorArrivalDate =
                        availabilityCalendar.ListAvailCalendarItems[0].ArrivalDate.AddDays(
                            -AppConstants.CALENDARITEM_GAP_DAYS);
                    iteratorDepartureDate =
                        availabilityCalendar.ListAvailCalendarItems[0].DepartureDate.AddDays(
                            -AppConstants.CALENDARITEM_GAP_DAYS);

                    for (int i = 0; i < CAROUSEL_COUNT; i++, availabilityCalendar.RightCarouselIndex++)
                    {
                        if (iteratorArrivalDate < arrivalDateLimit)
                        {
                            availabilityCalendar.LeftCarouselIndex = 0;
                            availabilityCalendar.RightCarouselIndex = availabilityCalendar.LeftCarouselIndex +
                                                                      CAROUSEL_COUNT;
                            HtmlInputHidden firstBatch = this.Parent.FindControl("firstBatch") as HtmlInputHidden;
                            if (firstBatch != null)
                            {
                                firstBatch.Value = "true";
                            }
                            Reservation2SessionWrapper.IsCalendarFirstBatch = true;
                            break;
                        }

                        AvailCalendarItemEntity calendarItem = new AvailCalendarItemEntity();
                        calendarItem.ArrivalDate = iteratorArrivalDate;
                        calendarItem.DepartureDate = iteratorDepartureDate;
                        availabilityCalendar.ListAvailCalendarItems.Add(calendarItem);

                        iteratorArrivalDate = iteratorArrivalDate.AddDays(-AppConstants.CALENDARITEM_GAP_DAYS);
                        iteratorDepartureDate = iteratorDepartureDate.AddDays(-AppConstants.CALENDARITEM_GAP_DAYS);
                    }
                    availabilityCalendar.RightCarouselIndex--;
                    List<AvailCalendarItemEntity> listAvailCalendarItems =
                        availabilityCalendar.ListAvailCalendarItems as List<AvailCalendarItemEntity>;
                    listAvailCalendarItems.Sort(new ArrivalDateComparer());

                    AvailabilityController availController = new AvailabilityController();
                    availController.MultiroomMultidateCalendarAvailSearch(search, availabilityCalendar);
                }
                else if (availabilityCalendar.LeftCarouselIndex > 0)
                {
                    if ((availabilityCalendar.RightCarouselIndex - CAROUSEL_COUNT + 1) > 0)
                    {
                        availabilityCalendar.LeftCarouselIndex = availabilityCalendar.RightCarouselIndex -
                                                                 CAROUSEL_COUNT + 1;
                    }
                    else
                    {
                        availabilityCalendar.LeftCarouselIndex = 0;
                    }
                }
                Reservation2SessionWrapper.LeftVisibleCarouselIndex = CAROUSEL_COUNT - AppConstants.VISIBLE_CAROUSEL_COUNT;
                Reservation2SessionWrapper.SelectedCalendarItemIndex = -1;

                HtmlInputHidden lastBatch = this.Parent.FindControl("lastBatch") as HtmlInputHidden;
                if (lastBatch != null)
                {
                    lastBatch.Value = "false";
                }
                Reservation2SessionWrapper.IsCalendarLastBatch = false;
                Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE), false);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Generates the avalibility calendar items.
        /// </summary>
        private void GenerateAvalibilityCalendarItems()
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            AvailabilityCalendarEntity availabilityCalendar = Reservation2SessionWrapper.AvailabilityCalendar;
            HtmlInputHidden selectedCalenderItem = null;

            selectedCalenderItem = this.Parent.FindControl("selectedCalendarItemIndex") as HtmlInputHidden;
            if (selectedCalenderItem != null)
            {
                selectedCalenderItem.Value = "-1";
                Reservation2SessionWrapper.SelectedCalendarItemIndex = -1;
            }

            if (availabilityCalendar != null && availabilityCalendar.ListAvailCalendarItems != null
                && availabilityCalendar.ListAvailCalendarItems.Count > 0)
            {
                int createdAvailCalendarItemsIndex = 0;
                carouselData.Controls.Clear();
                int temp = 0;
                DateTime ?tempDate = null;
                IList<AvailCalendarItemEntity> availCalendarItemsList =
                    Reservation2SessionWrapper.AvailabilityCalendar.ListAvailCalendarItems;
                for (int iterator = availabilityCalendar.LeftCarouselIndex;
                     iterator <= availabilityCalendar.RightCarouselIndex;
                     iterator++, createdAvailCalendarItemsIndex++)
                {
                    if (temp.Equals(0))
                    {
                        tempDate = availCalendarItemsList[iterator].ArrivalDate;
                        temp++;
                    }
                    StringBuilder sbArrivalDateformatted = new StringBuilder();
                    StringBuilder sbDepartureDateformatted = new StringBuilder();
                    if (iterator < 0)
                    {
                        iterator = 0;
                    }
                    if (iterator >= availCalendarItemsList.Count)
                    {
                        iterator = availCalendarItemsList.Count - 1;
                    }

                    HtmlGenericControl liToAdd = new HtmlGenericControl("li");
                    string liID = "li" + iterator;
                    liToAdd.ID = liID;
                    carouselData.Controls.Add(liToAdd);
                    AvalibilityCalendarItem availabilityCalenderItem =
                        LoadControl("AvalibilityCalendarItem.ascx") as AvalibilityCalendarItem;
                    DateTime arrivalDate = availCalendarItemsList[iterator].ArrivalDate;
                    DateTime departureDate = availCalendarItemsList[iterator].DepartureDate;

                    if (search != null && search.ArrivalDate.Equals(arrivalDate) && selectedCalenderItem != null)
                    {
                        selectedCalenderItem.Value = Convert.ToString(iterator);
                        Reservation2SessionWrapper.SelectedCalendarItemIndex = iterator;
                    }

                    sbArrivalDateformatted.Append(WebUtil.GetDayFromDate(arrivalDate));
                    sbArrivalDateformatted.Append(AppConstants.SPACE);
                    sbArrivalDateformatted.Append(Core.DateUtil.DateToDDMMYYYYString(arrivalDate));

                    sbDepartureDateformatted.Append(WebUtil.GetDayFromDate(departureDate));
                    sbDepartureDateformatted.Append(AppConstants.SPACE);
                    sbDepartureDateformatted.Append(Core.DateUtil.DateToDDMMYYYYString(departureDate));
                    if (availabilityCalenderItem != null)
                    {
                        availabilityCalenderItem.ArrivalDate = sbArrivalDateformatted.ToString();
                        availabilityCalenderItem.DepartureDate = sbDepartureDateformatted.ToString();
                        availabilityCalenderItem.LiPositionIndexInList = createdAvailCalendarItemsIndex;
                    }
                    if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.BONUSCHEQUE)
                    {
                        carouselData.Attributes.Add("class", "carouselData bcList");
                    }
                    else
                    {
                        carouselData.Attributes.Add("class", "carouselData");
                    }
                    if (availabilityCalenderItem != null)
                    liToAdd.Controls.Add(availabilityCalenderItem);
                }
                if (tempDate !=null && tempDate <= DateTime.Today)
                    btnPrevBatch.Enabled = false;
				
            }
        }

        #endregion
    }

    /// <summary>
    /// This class implements the Icomparer interface to customise the sorting in AvailCalendarItemEntity based on Arrival Date.
    /// </summary>
    internal class ArrivalDateComparer : IComparer<AvailCalendarItemEntity>
    {
        /// <summary>
        /// ICompare Implementation
        /// </summary>
        /// <param name="first">left side to compare</param>
        /// <param name="second">right side to compare</param>
        /// <returns>
        /// 0 if eqal -ve if first LESS THAN second +ve if first GREATER THAN second
        /// </returns>
        public int Compare(AvailCalendarItemEntity first, AvailCalendarItemEntity second)
        {
            if (first.ArrivalDate < second.ArrivalDate)
                return -1;
            else if (first.ArrivalDate == second.ArrivalDate)
                return 0;
            else
                return 1;
        }
    }
}