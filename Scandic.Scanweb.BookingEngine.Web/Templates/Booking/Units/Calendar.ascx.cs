////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: Template Class for maintaing the templates              //
//                                for various communications                              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Himansu Senapati                                  	  //
//  Author email id				:                           							  //
//  Creation Date				: 03rd October  2008									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
// Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using Scandic.Scanweb.Core.Core;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code behind of Calendar control.
    /// </summary>
    public partial class Calendar : System.Web.UI.UserControl
    {
        #region member variables

        /// <summary>
        /// Variables declared for storing cached week numbers for each locale.
        /// </summary>
        private const string WEEKNUMBERS_CACHE_KEY_EN = "BEWEEKNUMBERS_EN{0}";

        private const string WEEKNUMBERS_CACHE_KEY_NON_EN = "BEWEEKNUMBERS_NON_EN{0}";

        private static GregorianCalendar calendar = new GregorianCalendar();

        #endregion member variables

        #region GetWeekNumbers

        /// <summary>
        /// This will be called from .ascx on load of the page. This returns 
        /// the entire week numbers for the date range required by the website.
        /// Later this list will be stored in the javascript object array for subsequent use.
        /// </summary>
        /// <returns></returns>
        public static SortedList<DateTime, int> GetWeekNumbers()
        {
            string cacheKey = string.Empty;
            DateTime currentDate = DateTime.Now;
            int totalDays = 0;

            SortedList<DateTime, int> weekNumbersList = new SortedList<DateTime, int>();

            SortedList<DateTime, int> weekGroupList = new SortedList<DateTime, int>();

            if (GetCurrentLocale() != WEEKNUMBERS_CACHE_KEY_EN)
            {
                cacheKey = WEEKNUMBERS_CACHE_KEY_NON_EN;
            }

            if (cacheKey != string.Empty)
            {
                weekGroupList = ReadObjectFromCache(cacheKey);

                if (weekGroupList == null)
                {
                    DateTime startDate = GenerateStartDate(currentDate);

                    DateTime endDate = GenerateEndDate(currentDate);

                    TimeSpan dateDiff = endDate.Subtract(startDate);
                    totalDays = dateDiff.Days;

                    for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                    {
                        int weekNumber = GetWeekOfDate(date);
                        weekNumbersList[date] = weekNumber;
                    }

                    weekGroupList = ArrangeWeekNumber(weekNumbersList);

                    StoreObjectInCache(cacheKey, weekGroupList);
                }
            }

            return weekGroupList;
        }

        #endregion GetWeekNumbers

        #region Private Methods

        #region GenerateStartDate

        /// <summary>
        /// Start date from when the week numbers to be generated.
        /// </summary>
        /// <param name="currentDate"></param>
        /// <returns></returns>
        private static DateTime GenerateStartDate(DateTime currentDate)
        {
            DateTime startDate = new DateTime(2008, 1, 1);

            return startDate;
        }

        #endregion GenerateStartDate

        #region GenerateEndDate

        /// <summary>
        /// End date till when the week numbers to be generated.
        /// </summary>
        /// <param name="currentDate"></param>
        /// <returns></returns>
        private static DateTime GenerateEndDate(DateTime currentDate)
        {
            DateTime endDate = new DateTime(currentDate.AddYears(2).Year, 12, 31);

            return endDate;
        }

        #endregion GenerateEndDate

        #region ArrangeWeekNumber

        /// <summary>
        /// This arranges the week numbers group by each week. Hence it returns a 
        /// list of items containing the start day of each week and corresponding week number.
        /// </summary>
        /// <param name="weekNumbersList"></param>
        private static SortedList<DateTime, int> ArrangeWeekNumber(SortedList<DateTime, int> weekNumbersList)
        {
            SortedList<DateTime, int> weekGroupList = new SortedList<DateTime, int>();

            foreach (DateTime date in weekNumbersList.Keys)
            {
                int weekNumber = weekNumbersList[date];

                if (CanAddWeekNumbers(weekGroupList, date, weekNumber))
                {
                    weekGroupList[date] = weekNumber;
                }
            }

            return weekGroupList;
        }

        #endregion ArrangeWeekNumber

        #region CanAddWeekNumbers

        /// <summary>
        /// It returns "TRUE" or "FALSE" if a date and corresponding week number can be added.
        /// </summary>
        /// <param name="date"></param>
        /// <param name="weekNumber"></param>
        /// <returns></returns>
        private static bool CanAddWeekNumbers(SortedList<DateTime, int> weekGroupList, DateTime date, int weekNumber)
        {
            bool returnValue = true;

            int index = GetLastIndex(weekGroupList, weekNumber);

            if (index != -1)
            {
                DateTime alreadyPresentDate = weekGroupList.Keys[index];

                if (alreadyPresentDate.Year.Equals(date.Year) &&
                    alreadyPresentDate.Month.Equals(date.Month))
                {
                    returnValue = false;
                }
            }

            return returnValue;
        }

        #endregion CanAddWeekNumbers

        #region GetLastIndex

        /// <summary>
        /// Finds the index of the last occurance of the week number.
        /// </summary>
        /// <param name="weekNumber"></param>
        /// <returns></returns>
        private static int GetLastIndex(SortedList<DateTime, int> weekGroupList, int weekNumber)
        {
            int index = -1;

            for (int ctr = weekGroupList.Count - 1; ctr >= 0; ctr--)
            {
                if (weekNumber == weekGroupList.Values[ctr])
                {
                    index = ctr;
                    break;
                }
            }

            return index;
        }

        #endregion GetLastIndex

        #region GetWeekOfDate

        /// <summary>
        /// Finds out the week number of the date passed as parameter.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private static int GetWeekOfDate(DateTime date)
        {
            int weekNo = 0;
            const int DECEMBER = 12;

            DayOfWeek firstDayOfWeek = DayOfWeek.Monday;

            CalendarWeekRule calendarWeekRule = CalendarWeekRule.FirstFourDayWeek;

            weekNo = calendar.GetWeekOfYear(date, calendarWeekRule, firstDayOfWeek);

            if (date.Month == DECEMBER)
            {
                if ((date.Day == 29 && (date.DayOfWeek.Equals(DayOfWeek.Monday))) ||
                    (date.Day == 30 &&
                     (date.DayOfWeek.Equals(DayOfWeek.Monday) || date.DayOfWeek.Equals(DayOfWeek.Tuesday))) ||
                    (date.Day == 31 &&
                     (date.DayOfWeek.Equals(DayOfWeek.Monday) || date.DayOfWeek.Equals(DayOfWeek.Tuesday) ||
                      date.DayOfWeek.Equals(DayOfWeek.Wednesday))))

                {
                    weekNo = 1;
                }
            }

            return weekNo;
        }

        #endregion GetWeekOfDate

        #region GetCurrentLocale

        /// <summary>
        /// Returns he current locale 
        /// </summary>
        /// <returns></returns>
        private static string GetCurrentLocale()
        {
            IFormatProvider cultureInfo = new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
            string currentLocale = cultureInfo.ToString().Substring(0, 2).ToUpper();

            return currentLocale;
        }

        #endregion GetCurrentLocale

        #region StoreObjectInCache

        /// <summary>
        /// Stores the object in the cache
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="objectToStore"></param>
        private static void StoreObjectInCache(string cacheKey, object objectToStore)
        {
            DateTime cacheExpirationDate = DateTime.Now.AddDays(1);
            ScanwebCacheManager.Instance.AddToCache(cacheKey,objectToStore,24);
        }

        #endregion StoreObjectInCache

        #region ReadObjectFromCache

        /// <summary>
        /// Reads the value fo object from cache.
        /// </summary>
        /// <param name="cacheKey"></param>
        private static SortedList<DateTime, int> ReadObjectFromCache(string cacheKey)
        {
            SortedList<DateTime, int> weekGroupList = new SortedList<DateTime, int>();

            weekGroupList = ScanwebCacheManager.Instance.LookInCache<SortedList<DateTime, int>>(cacheKey);

            return weekGroupList;
        }

        #endregion ReadObjectFromCache

        #endregion Private Methods
    }
}