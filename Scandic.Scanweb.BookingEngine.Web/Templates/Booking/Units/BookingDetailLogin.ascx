<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingDetailLogin.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.BookingDetailLogin" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<div id="bookingDetailsLogin">
    <div class="LoginHeader">
        <style type="text/css">
            input.chkBx:focus
            {
                outline: 1px dotted Grey;
            }
        </style>
        <input type="hidden" id="loginBookingDetail" runat="server" />
        <input type="hidden" id="loginBookingDetailPage" runat="server" />
        <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
        <input type="hidden" id="BDLoginPasswordInvalid" name="BDLoginPasswordInvalid" value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/password") %>' />
        <input type="hidden" id="BDLoginUserNameInvalid" name="BDLoginUserNameInvalid" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/user_name") %>' />
        <div class="hdLft">
            <div class="usrBtn usrActBtn fltLft usrBtnBookingHeader">
                <a <%--title="Login"--%> href="#" class="buttonInner" tabindex="15" rel="loginOn">
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/loginmessagebd")%></a>
                <span class="buttonRt">&nbsp;</span>
            </div>
        </div>
        <div class="loginFlyout roundMe fltLft loginCnt" style="display: none;" id="loginInfo"
            runat="server">
            <div id="BDLoginErrorDiv" runat="server">
                <span class="redAlertIcon">
                    <%=
                WebUtil.GetTranslatedText(
                    "/bookingengine/booking/loyaltylogin/LoginErrorMessage") %></span>
            </div>
            <div id="LoginErrorDiv" runat="server">
            </div>
            <a href="#" <%--title="Close"--%> class="scansprite blkClose" rel="loginClose" tabindex="27">
                &nbsp;</a>
            <div class="loginFlyContLft fltLft">
                <div class="userPass fltLft" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnBDLogIn.ClientID %>')">
                    <div class="memNo">
                        <label class="formLabel" for="<%= txtBDLoginUserName.ClientID %>">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/usernamemessage") %></label>
                        <input type="text" class="fltLft defaultColor ignore userPrefix45" id="txtBDLoginUserNamePrefix"
                            name="txtBDLoginUserNamePrefix" readonly="readonly" maxlength="10" runat="server" />
                        <input type="text" class="fltLft defaultColor ignore w240" id="txtBDLoginUserName"
                            tabindex="18" name="txtBDLoginUserName" maxlength="80" runat="server" />
                    </div>
                    <div class="passwordWrapper">
                        <label class="formLabel" for="<%= txtBDLoginPassword.ClientID %>">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/Password") %></label>
                        <asp:TextBox CssClass="frmInputText ignore" ID="txtBDLoginPassword" TabIndex="19"
                            TextMode="Password" MaxLength="20" runat="server" />
                        <%--<input type="text" id="passwordtext" runat="server" class="frmInputText defaultColor pwdTxt" />--%>
                        <span class="fltLft" style="margin-right: 3px; margin-top: 7px;">
                            <input type="checkbox" tabindex="20" id="RememberUserIdPwd" name="RememberUserIdPwd"
                                class="chkBx mdcheckbox" runat="server" /></span>
                        <%--R2.0:artf1149134: Bhavya: Help text for Remember me is corrected to pick from right xml tag--%>
                        <span class="fltLft" style="margin-top: 7px;">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/RememberMe")%></span>
                        <span class="help spriteIcon toolTipMe" title="<%=WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/RememberMeToolTip")%>"
                            style="margin-top: 7px; float: left;"></span>
                    </div>
                    <div class="usrBtn fltLft">
                        <label class="formLabel" for="">
                            &#160;</label>
                        <asp:LinkButton ID="btnBDLogIn" runat="server" class="buttonInner" value="Login"
                            TabIndex="21">
                        </asp:LinkButton>
                        <%--<asp:LinkButton ID="spnBDLogIn" runat="server" class="buttonRt"> vipul hiding this as not neded
                    </asp:LinkButton>--%>
                    </div>
                    <div class="fltLft bookDetLgnPro" id="BookingDetailsLoginProgressDiv" runat="server" style="display: none">
                        <span class="fltLft bookDetLgnPro">
                            <img src='<%=ResolveUrl("~/Templates/Booking/Styles/Default/Images/rotatingclockani.gif")%>'
                                alt="image" align="middle" width="25px" height="25px" />
                            <span>
                                <%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/Loading")%>
                            </span></span>
                    </div>
                </div>
                <div class="forgotCntnt fltLft">
                    <a href="#" tabindex="23" onclick="openPopupWin('<%= GlobalUtil.GetHelpPage(Utility.GetForgottenMembershipNoPageReference()) %>','width=400,height=300,scrollbars=yes,resizable=yes');return false;">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/forgottenmembershipno") %>
                    </a>
                    <asp:LinkButton TabIndex="25" ID="ForgottenPassword" runat="server" OnClick="ForgottenPassword_Click"><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/forgottenpasswordmessage") %></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        appendPrefix("txtBDLoginUserNamePrefix");

    });
    $("input[id$='txtBDLoginUserName']").bind('focusout', TrimWhiteSpace);
    function TrimWhiteSpace() {


        trimWhiteSpaces("txtBDLoginUserName");
        replaceDuplicatePrefix("txtBDLoginUserName");
    }
    
</script>

