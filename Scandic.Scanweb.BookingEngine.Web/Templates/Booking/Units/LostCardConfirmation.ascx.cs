#region Description

////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: Lost card Confirmation user control code behind   	  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Raj Kishore Marandi	                                  //
/// Author email id				:                           							  //
/// Creation Date				: 7th December 2007									      //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#endregion Description

#region  System Namespaces

using System;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Entity;

#endregion System Namespaces

    #region EpiServer NameSpace

    #endregion EpiServer NameSpace

    #region Scandic Namespaces

#endregion Scandic Namespaces

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// LostCardConfirmation
    /// </summary>
    public partial class LostCardConfirmation : EPiServer.UserControlBase
    {
        /// <summary>
        /// Initialize Header of the Lost card confirmation user control
        /// </summary>
        /// <param name="headerText">Contains  the text to be displayed</param>
        public void AssignHeader(string headerText)
        {
            header.InnerText = headerText;
        }

        /// <summary>
        /// Initialize Header of the Lost card confirmation user control
        /// </summary>
        /// <param name="messageText">Contains the text to be displayed as message</param>
        public void AssignMessage(string messageText)
        {
            message.InnerText = messageText;
        }

        /// <summary>
        /// Initialize Link text for the Lost card  confrirmation uesr control.
        /// </summary>
        /// <param name="linkText">Contains the text to be displayed as link</param>
        public void AssignLink(string linkText)
        {
            lnkbutton.Text = linkText;
        }

        /// <summary>
        /// This wil redirect to offers Overview page on click of link button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbutton_Click(object sender, EventArgs e)
        {
            string redirectUrl = string.Empty;
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.OFFERS_OVERVIEW), false);
        }
    }
}