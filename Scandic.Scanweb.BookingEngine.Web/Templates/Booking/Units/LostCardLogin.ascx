<%@ Control Language="C#" AutoEventWireup="true" Codebehind="LostCardLogin.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.LostCardLogin" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register Src="EmailConfirmation.ascx" TagName="EmailConfirmation" TagPrefix="uc2" %>
<div id="Loyalty" class="BE">
    <!-- LastCard -->
    <div id="GuestLostCard" runat="server">
        <div class="txtModule">
            <div>
                <h2 class="darkHeading">
                    <span>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardlogin/messageheader") %>
                    </span>
                </h2>
            </div>
            <div class="txtBlockOne">
                <span id="spanStreet" runat="server"></span>
                <br />
                <span id="spanPostCode" runat="server"></span>
                <br />
                <span id="spanCity" runat="server"></span>,&nbsp;<span id="spanCountry" runat="server"></span></div>
            <!-- R1.3 | CR16 | Loyalty Enhancement 
     Added  Sapn to display telephone number-->
            <div>
                <span>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardlogin/telephone") %>
                </span><span id="spanTelephone" runat="server"></span>
            </div>
            <div class="txtBlockTwo">
            </div>
            <div class="clear">
                &nbsp;</div>
        </div>
        <div class="box-top-grey">
            <span class="title">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardlogin/lostcardheader") %>
            </span>
        </div>
        <div class="box-middle">
            <div class="content">
                <div class="columnOne">
                    <p class="formRow">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardlogin/membership") %>
                        </label>
                        <br />
                        <input type="text" name="txtMembershipNumber" class="frmInputText" id="txtMembershipNumber"
                            runat="server" readonly="readonly" />
                    </p>
                </div>
                <div class="columnTwo">
                </div>
                <div class="clear">
                    &nbsp;</div>
                <!-- /details -->
                <!-- Footer -->
                <div id="FooterContainer">
                    <div class="alignRight">
                        <div class="actionBtn fltRt">
                            <asp:LinkButton ID="LinkButtonReport" class="buttonInner" OnClick="ReportButton_Click"
                                runat="server">
          <span><%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/buttonmessage") %></span>
                            </asp:LinkButton>
                            <asp:LinkButton ID="spnButtonReport" class="buttonRt scansprite" OnClick="ReportButton_Click"
                                runat="server">          
                            </asp:LinkButton>
                        </div>
                    </div>
                    <div class="clear">
                        &nbsp;</div>
                </div>
                <!-- Footer -->
            </div>
        </div>
        <div class="box-bottom">
        </div>
    </div>
    <uc2:EmailConfirmation ID="LCWLEmailConfirmation" runat="server" />
    <!-- /LastCard -->
</div>
