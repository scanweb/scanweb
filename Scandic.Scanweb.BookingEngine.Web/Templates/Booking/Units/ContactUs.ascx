<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ContactUs.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ContactUs" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register Src="EmailConfirmation.ascx" TagName="EmailConfirmation" TagPrefix="uc1" %>
<div id="Loyalty" class="BE">
    <!-- LastCard -->
    <div id="ContactusDiv" runat="server">
        <div id="Contactus">
            <div>
                <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
                <input type="hidden" id="contactMessageError" name="contactMessageError" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/contactusvalidationerror") %>' />
            </div>
            <div class="txtModule">
                <div>
                    <h3 class="darkHeading">
                        <span>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/contactus/scandiccontactheader") %>
                        </span>
                    </h3>
                </div>
                <div id="ContactusErrorDiv" runat="server">
                </div>
                <div class="txtBlockOne">
                    <span>
                        <label id="lblStreet" runat="server">
                        </label>
                        <br />
                        <label id="lblPostCode" runat="server">
                        </label>
                        <br />
                        <label id="lblCity" runat="server">
                        </label>
                        ,&nbsp;<label id="lblCountry" runat="server"></label>
                    </span>
                </div>
                <div class="txtBlockTwo">
                    <span>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/contactus/scandicphonenumber") %>
                        <br />
                        <label id="lblPhoneNumber" runat="server">
                        </label>
                        <br />
                        <label id="faxHeading" runat="server">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/contactus/scandicfaxnumber") %>
                            <br />
                        </label>
                        <label id="lblFaxNumber" runat="server">
                        </label>
                    </span>
                </div>
                <div class="clear">
                    &nbsp;</div>
            </div>
            <div class="box-top-grey">
                <span class="title">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/contactus/header") %>
                </span>
            </div>
            <div class="box-middle">
                <div class="content">
                    <div class="columnOne">
                        <p class="formRow">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/contactus/name") %>
                            </label>
                            <br />
                            <input type="text" readonly="readonly" id="txtName" name="txtName" class="txtName"
                                maxlength="40" runat="server" />
                        </p>
                        <p class="formRow">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/contactus/Email") %>
                            </label>
                            <br />
                            <input type="text" readonly="readonly" id="txtEmailAddress" name="txtEmailAddress"
                                class="txtEmailAddress" runat="server" />
                        </p>
                    </div>
                    <div class="columnTwo">
                        <p class="formRow">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/contactus/subject") %>
                            </label>
                            <br />
                            <asp:DropDownList ID="ddlSubject" runat="server" CssClass="selBoxMedium" Width="200px">
                            </asp:DropDownList>
                            <br class="clear" />
                        </p>
                        <br />
                        <p class="formRow">
                            <input name="SubscribeNewsletter" type="checkbox" class="frmCheckBox" value="" id="chkCopy"
                                runat="server" checked="CHECKED" />
                            <label class="frmCheckBox">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/contactus/copy") %>
                            </label>
                            <br class="clear" />
                        </p>
                    </div>
                    <div>
                        <p class="formRow">
                            <textarea rows="5" id="txtComposedMessage" class="frmInputTextAreaBig" runat="server"></textarea>
                        </p>
                    </div>
                    <div class="clear">
                        &nbsp;</div>
                    <!-- /details -->
                    <!-- Footer -->
                    <div id="FooterContainer">
                        <div class="alignRight">
                            <div class="actionBtn fltRt">
                                <asp:LinkButton ID="LinkButtonSend" class="buttonInner" OnClick="ButtonSend_Click"
                                    runat="server"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/contactus/buttonsend") %></span></asp:LinkButton>
                                    <asp:LinkButton ID="spnButtonSend" class="buttonRt scansprite" OnClick="ButtonSend_Click"
                                    runat="server"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                    </div>
                    <!-- Footer -->
                </div>
            </div>
            <div class="box-bottom">
                &nbsp;</div>
        </div>
    </div>
    <!-- /LastCard -->
    <uc1:EmailConfirmation ID="ContactUsEmailConfirmation" runat="server" />
</div>
