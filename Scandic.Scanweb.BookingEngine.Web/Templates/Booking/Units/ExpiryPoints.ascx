<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExpiryPoints.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ExpiryPoints" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register TagPrefix="Scanweb" TagName="RoomInformation" Src="~/Templates/Booking/Units/RoomInformationContainer.ascx" %>

<div id="expiringPoints" runat="server" visible="false">
<div class="bbox">
    <div class="box-head expPts-box-head">
		<h2 class="bbox-title expPts-title"><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/ExpiringPoint") %></strong></h2>
		<span class="help scansprite toolTipMe expPts" title="Your points are valid for a minimum of 3 years and always expire at the end of a year. Points with the shortest validity date are deducted first."></span>
	</div>
	<div class="box-content">
		<table runat="server" id="tablePointsExpiringDate" width="100%" cellspacing="0" cellpadding="0" border="0" style="*width:433px!important;*margin-left:8px;*padding-left:0px;" class="tblSubHead mp-tbl">
			<tbody>
				<tr>
					<th> <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/ExpirationDate") %></th>
					<th class="cl"> <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/pointsheader") %></th>
				</tr>				
			</tbody>
		</table>
	</div>
</div>
        </div>