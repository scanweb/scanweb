<%@ Control Language="C#" AutoEventWireup="true" Codebehind="AccountOverview.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.AccountOverview" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%@ Register TagPrefix="Scanweb" TagName="PointExpInformation" Src="~/Templates/Booking/Units/ExpiryPoints.ascx" %>
<!--[if IE]> 
<style type="text/css">
#AccountOverview span.ttpointer{margin-top: -5px;}
</style>
<![endif]-->
<div id="Loyalty" class="BE">
    <!-- AccountOverview -->
    <div id="errMsgTop" class="errorText" visible="false" runat="server" />
    <div id="AccountOverview">
        <div class="txtModule">
            <strong class="acctHeading"></strong>
            <div class="box-top"></div>
            <div class="box-middle">               
                <div class="content">
                <ul class="actBlc">
                    <li class="Pts acctSlct">
                        <div><%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/YourPoint") %></div>
                        <strong id="totalPoints" runat="server"></strong>
                    </li>
                    <li class="Blc">
                        <div><%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/membershipNumber") %></div>
                        <strong id="membershipNo" runat="server"></strong>
                    </li>
                    <li class="lv">
                        <span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/hlpLevel") %>" class="help scansprite toolTipMe"></span>
                        <div><%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/levelStatus") %></div>
                        <strong id="memberLevel" runat="server"></strong>
                    </li>
                </ul>
                <div class="clear actBlcClr">&nbsp;</div>
            </div>
                </div>
            <div class="box-bottom"></div>
            </div>
        <!--Start : R1.5 | Artifact artf809442 : FGP - Display expiry date for FGP points  -->
        <div id="expiringPointsdiv" runat="server">
        
        <Scanweb:PointExpInformation  ID="PointOfExpControl" runat="server"> </Scanweb:PointExpInformation>
        </div>
        <!--End : R1.5 | Artifact artf809442 : FGP - Display expiry date for FGP points  -->
        <div id="Transactions">
            <div class="box-top">
                &nbsp;</div>
            <div class="box-middle">
                <!-- Content -->
                <div class="content" id="TransactionGrid" runat="server">
                    <!-- header -->
                    <div class="tableHoldingHead"><strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/transactionMainheader") %></strong></div>
                    <div class="subHeadingTop"></div>
                    <div class="subHeading" id="idSubHeading" runat="server">    
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/YourQlNight") %><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/hlpBook") %>" class="help scansprite toolTipMe" style="float:none;"></span><br /><strong id="strngNights" runat="server"> </strong>            
                    </div>
                    <div id="errMsg" class="errorText" runat="server" />
                    <div id="NoTransaction" runat="server" visible="false" class="subHeading"></div>
                    <div class="subHeadingBtm"></div> 
                    <div id="divTransaction" runat="server">
                        <!-- /header -->
                        <table runat="server" id="tableTransactions" width="434" border="0" class="tblTrans" cellpadding="0" cellspacing="0">
                        <tr>
                            <th><%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/transactionheader") %></th>
                            <th><%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/dateheader") %></th>
                            <th class="tRtg"><%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/pointsheader") %></th>
                        </tr>
                        </table>
                        <!-- Footer -->
                        <!--div class="clear">&nbsp;</div-->
                        <div id="FooterContainer">
                            <div class="timeFrame">
                                <span id="timeFrameFrom" runat="server" class="text"></span><span class="selMonth">
                                    <select id="ddlMonthFrom" runat="server">
                                    </select>
                                </span><span id="timeFrameTo" runat="Server" class="selMonth"></span><span class="selMonth">
                                    <select id="ddlMonthTo" runat="server">
                                    </select>
                                </span>
                               <%-- <div class="clear">
                                    &nbsp;</div>--%>
                            </div>
                            <div class="clear">&nbsp;</div>
                                
                            <div class="actionBtn fltRt">
                                
                                    <asp:LinkButton ID="linkButtonView" CssClass="buttonInner accview" OnClick="ButtonView_Click" runat="server">
                                        <span id="viewButtonCaption" runat="server"></span>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="linkBtnRt" CssClass="buttonRt scansprite accviewrt" OnClick="ButtonView_Click" runat="server"></asp:LinkButton>
                            </div>
                            <div class="clear">&nbsp;</div>
                        </div>
                                     
                                
                            </div>
                      <!-- Footer -->
                            
                        <div id="idAlertBox" class="alrtBox" runat="server">
                            <div class="whitBoxLft">    
                                <div class="hd"></div>
                                    <div class="cnt">
                                        <p><span class="spriteIcon"></span><%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/fiveDays") %></p>
                                    </div>
                                <div class="ft"></div>
                        </div>                    
                    </div>
                </div>
                <!-- /Content -->
            </div>
            <div class="box-bottom">
                &nbsp;</div>
            <div class="clear">
                &nbsp;</div>
        </div>
        <div id="PointsDetails" runat="Server" visible="false">
            <div class="box-top">
                &nbsp;</div>
            <div class="box-middle">
                <!-- Content -->
                <div class="content">
                    <!-- Header -->
                    <div class="subHeader">
                        <span class="column1">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/pointsexpiringheader") %>
                        </span><span class="column2">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/pointsexpiringdate") %>
                        </span>
                        <div class="clear">
                            &nbsp;</div>
                    </div>
                    <!-- /Header -->
                    <div class="row-details">
                        <!-- DEV NOTE:  						
						//IF no date to extract show below code						
						
						<p>You have made no transactions</p>						
						
						//ELSE show show below code
						-->
                        <span class="column1"><span class="num">1500</span><%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/pointsexpiringpoints") %>
                        </span><span class="column2">31, December 2007</span>
                        <div class="clear">
                            &nbsp;</div>
                    </div>
                </div>
                <!-- /Content -->
            </div>
            <div class="box-bottom">
                &nbsp;</div>
            <div class="clear">
                <input id="hdnMembershipNameID" style="width: 14px" type="hidden" runat="server" />
                <input id="hdnMembershipOperaID" style="width: 14px" type="hidden" runat="server" /></div>
        </div>
    </div>
    <!-- /AccountOverview -->
</div>
