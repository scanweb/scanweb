using System.Collections.Generic;
using System.Text;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// BookingModuleFlyout
    /// </summary>
    public partial class BookingModuleFlyout : EPiServer.UserControlBase
    {
        /// <summary>
        /// Create city destination.
        /// </summary>
        /// <param name="isTopDestination">If true then it's a top destination else All destination</param>
        /// <returns>string having html formatted destination</returns>
        protected string GetDestinations(bool isTopDestination)
        {
            List<DestinationInfo> topDestinationList = ContentDataAccess.GetAllDestinations(isTopDestination);

            StringBuilder topDestination = new StringBuilder();
            if (topDestinationList != null && topDestinationList.Count > 0)
            {
                if (isTopDestination)
                {
                    topDestination.Append("<span>");
                    topDestination.Append(WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/TopDestinations"));
                    topDestination.Append("</sapn>");
                }
                else
                {
                    topDestination.Append("<span>");
                    topDestination.Append(WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AllDestinations"));
                    topDestination.Append("</span>");
                }
                topDestination.Append("<ul>");
                topDestinationList.Sort(new HotelDestinationComparer());
                foreach (DestinationInfo topDestinationListItem in topDestinationList)
                {
                    topDestination.Append("<li><a href=\"");
                    topDestination.Append("#\"");
                    topDestination.Append("rel=\"CITY:");
                    topDestination.Append(topDestinationListItem.OperaID);
                    topDestination.Append("\">");
                    topDestination.Append(topDestinationListItem.DestinationName);
                    topDestination.Append("</a></li>");
                }

                topDestination.Append("</ul>");
            }
            else
            {
                if (isTopDestination)
                {
                    topLinks.Visible = false;
                }
                else
                {
                    linkList.Visible = false;
                }
            }
            return topDestination.ToString();
        }
    }
}