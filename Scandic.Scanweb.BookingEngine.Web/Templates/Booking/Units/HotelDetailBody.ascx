<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelDetailBody.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.HotelDetailBody" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<!-- hotel detail -->
<div class="hotelInfoCnt">
    <div class="imageHolder">
        <div class="hd sprite">
        </div>
        <div class="cnt">
            <img src="<%= hotelURL %>" id="imgHotel" width="226" height="148" alt="<%= imageAlt %>"
                runat="server" /></div>
        <div class="ft sprite">
        </div>
    </div>
    <div class="desc">
        <h2>
            <%= Name %></h2>
        <p>
            <span><a id="lnkAddress" runat="server">
                <%= Address %></a></span></p>
        <p>
            <%= Teaser %></p>
        <ul class="links">
            <li><a id="lnkFullDescription" class="goto scansprite" runat="server">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/FullDescription") %></a></li>
            <li><a class="overlay jqModal scansprite" id="lnkImageGallery" runat="server" href="#">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/imagegallery") %>
            </a></li>
        </ul>
    </div>
    <!-- BEGIN Hotel Rates -->
    <div class="ratesHolderContainer" id="ratesHolder" runat="server">
	                <p id="lblCityCenterDistance" class="cityCenterDist" visible="false" runat="server">
                </p>
        <div class="ratesHolderTop">
            &#160;</div>
        <div class="ratesHolder">
            <div class="rates">
                <div id="RateTitle" runat="server" visible="false">
                    <div id="lblRateTitle" runat="server" class="ratetitleclass">
                    </div>
                </div>
                <div id="NoDiscount" runat="server" visible="false">
                    <div id="lblNoDiscount" runat="server" class="ratetitleclass">
                    </div>
                </div>
                <%--   <p><span><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from")%></span></p>--%>
                <p>
                    <span id="spnFromText" runat="server"></span>
                </p>
                <p id="rateWrapper" runat="server">
                    <strong runat="server" id="rateDisp"></strong>
                </p>
                <span id="altRateDisplay" class="alternateCurrencySelectHotel" runat="server"></span>
                <hr class="HR" />
                <p runat="server" id="lblRateString">
                </p>

            </div>
            <p id="lblDistance" visible="false" runat="server">
            </p>
            <p id="lblDrivingTime" visible="false" runat="server">
            </p>
            <p id="lblDirection" visible="false" runat="server">
            </p>
            <!-- Action Button Container -->
            <!--  commenting this code for trip advisor  
               <div class="actionBtnContainer">
	            <div class="actionBtn">
		            <a href="javascript:UpdateSHListSelection();RedirectToSelectRate('<%= HotelID %>',<%= IsUblockRequired() %>);" class="buttonInner" ><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/roomsandrates") %></a>
		            <a href="javascript:UpdateSHListSelection();RedirectToSelectRate('<%= HotelID %>',<%= IsUblockRequired() %>);" class="buttonRt scansprite noText" ></a>
	            </div>
	            </div>
            -->
        </div>
        <div class="ratesHolderBottom">
            &#160;</div>
    </div>
    <div class="tripadvisorWrapper">
        <div id="divTripAdvisorRating" runat="server" class="divTripAdvisorRating">
        </div>
        <div class="roomsAndrates">
            <div class="actionBtnContainer">
                <div class="actionBtn">
                    <a href="javascript:UpdateSHListSelection();RedirectToSelectRate('<%= HotelID %>',<%= IsUblockRequired() %>);"
                        class="buttonInner">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/roomsandrates") %></a>
                    <a href="javascript:UpdateSHListSelection();RedirectToSelectRate('<%= HotelID %>',<%= IsUblockRequired() %>);"
                        class="buttonRt scansprite noText"></a>
                </div>
            </div>
        </div>
    </div>
    <!-- END Hotel Rates -->
    <!-- BEGIN Special Alert -->
    <div id="divSpAlertWrap" class="selecthotelwrpalert" runat="server">
        <div class="splalertcontentratepage" id="divSpAlert" runat="server">
        </div>
    </div>
    <!-- END Special Alert -->
</div>
<!-- /hotel detail -->
