//  Description					:   RoomTypeRateContainer                                 //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces

using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion

    #region Scandic Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for RoomTypeRateContainer
    /// </summary>
    public partial class RoomTypeRateContainer : EPiServer.UserControlBase
    {
        public string SELECT_RATE_ALL_ROOM_TYPES =
            WebUtil.GetTranslatedText(TranslatedTextConstansts.SELECT_RATE_ALL_ROOM_TYPES);

        public string SELECT_RATE_SPECIFIC_ROOM_TYPES =
            WebUtil.GetTranslatedText(TranslatedTextConstansts.SELECT_RATE_SPECIFIC_ROOM_TYPES);

        /// <summary>
        /// Set the RoomTypeRate control
        /// </summary>
        /// <param name="rateDisplay">
        /// BaseRateDisplay
        /// </param>
        /// <param name="defaultRoomTypesAvailable">
        /// Boolean indicates whether all the default room types were available
        /// </param>
        /// <param name="showPricePerStay">
        /// Boolean indicates whether the price per stay / per night needs to be displayed
        /// </param>
        /// <param name="showAllRoomTypes">
        /// Boolean indicates to show all Room Types
        /// </param>
        public void SetRateDisplay(BaseRateDisplay rateDisplay,
                                   bool defaultRoomTypesAvailable, bool showPricePerStay, bool showAllRoomTypes,
                                   int roomNumber)
        {
            DisplayRateTable(rateDisplay, defaultRoomTypesAvailable, showPricePerStay, showAllRoomTypes, roomNumber);
            tabBtn.Attributes.Add("rel", ".table" + roomNumber.ToString());
            tabBtn.Attributes.Add("class", "usrBtn table" + roomNumber.ToString());
            tabBtn.Visible = RoomRateDisplayUtil.HasNonDefaultRoomAvailable(rateDisplay.RateRoomCategories,
                                                                            SearchCriteriaSessionWrapper.SearchCriteria.
                                                                                SelectedHotelCode);
            tabBtn.Attributes.Add("class", "usrBtn table" + roomNumber);
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            if (hotelSearch != null && hotelSearch.RoomsPerNight == 1)
            {
                roomTabWrapper.Visible = false;
            }
            else
            {
                roomTabWrapper.Visible = true;
            }
        }

        #region Private Methods

        private void DisplayRateTable(BaseRateDisplay rateDisplay,
                                      bool defaultRoomTypesAvailable, bool showPricePerStay, bool showAllRoomTypes,
                                      int roomNumber)
        {
            Table roomRatesTable = new Table();
            int radioButtonCount = 0;
            roomRatesTable.CssClass = "m15TabularData table" + roomNumber.ToString();
            roomRatesTable.Attributes.Add("cellspacing", "1");
            roomRatesTable.ID = "tabularData" + roomNumber.ToString();
            DisplayHeader(rateDisplay, ref roomRatesTable, roomNumber);
            DisplayRoomRateRows(rateDisplay, defaultRoomTypesAvailable, showPricePerStay, showAllRoomTypes,
                                ref roomRatesTable, roomNumber, ref radioButtonCount);
            HtmlGenericControl tabledataDiv = new HtmlGenericControl("div");
            tabledataDiv.ID = "m15TabularData";
            tabledataDiv.Controls.Add(roomRatesTable);
            blkRoomRateTypeContainer.Controls.Add(tabledataDiv);
        }


        /// <summary>
        /// Display the Header of the Rate Types
        /// </summary>
        /// <param name="rateDisplay">
        /// BaseRateDisplay
        /// </param>
        private void DisplayHeader(BaseRateDisplay rateDisplay, ref Table roomRatesTable, int roomNumber)
        {
            RoomRateHeader roomRateHeader = LoadControl("RoomRateHeader.ascx") as RoomRateHeader;
            if ((roomRateHeader != null) && (rateDisplay != null))
            {
                roomRateHeader.SetHeader(rateDisplay.RateCategoriesDisplay, ref roomRatesTable);
                blkRoomRateTypeContainer.Controls.Add(roomRateHeader);
            }
        }

        /// <summary>
        /// Display the RateRows
        /// </summary>
        /// <param name="rateDisplay">
        /// BaseRateDisplay
        /// </param>
        /// <param name="defaultRoomTypesAvailable">
        /// Boolean indicates whether all the default room types were available
        /// </param>
        /// <param name="showPricePerStay">
        /// Boolean indicates whether the price per stay / per night needs to be displayed
        /// </param>
        /// <param name="showAllRoomTypes">
        /// Boolean indicates whether to show all room categories
        /// </param>
        private void DisplayRoomRateRows(BaseRateDisplay rateDisplay, bool defaultRoomTypesAvailable,
                                         bool showPricePerStay, bool showAllRoomTypes, ref Table roomRatesTable,
                                         int roomNumber, ref int radioButtonCount)
        {
            bool noDefaultRoomTypesAvailable = false;
            BlockCodeRoomRateDisplay newBlockCodeRoomRateDisplay = null;
            try
            {
                newBlockCodeRoomRateDisplay = rateDisplay as BlockCodeRoomRateDisplay;
            }
            catch
            {
            }

            #region Sort by rate order

            double minRate = double.MaxValue;
            string minRateCategoryName = string.Empty;
            List<RoomCategoryEntity> roomCategoryList = rateDisplay.RateRoomCategories;
            bool isPerstay = false;
            isPerstay = Convert.ToBoolean(Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage);
            Dictionary<string, double> minRateForEachCategory = new Dictionary<string, double>();
            for (int iterator = 0; iterator < roomCategoryList.Count; iterator++)
            {
                foreach (string rateCategoryKey in roomCategoryList[iterator].RateCategories.Keys)
                {
                    double minRateforCategory = GetMinRateInRoomCategory(roomCategoryList, rateCategoryKey, isPerstay);
                    if (minRateforCategory < minRate)
                    {
                        minRate = minRateforCategory;
                        minRateCategoryName = rateCategoryKey;
                    }
                }
            }
            Reservation2SessionWrapper.MinRateCategoryName = minRateCategoryName;

            List<string> rateCategoryIds = new List<string>();
            for (int i = 0; i < rateDisplay.RateCategoriesDisplay.Count; i++)
            {
                rateCategoryIds.Add(rateDisplay.RateCategoriesDisplay[i].Id);
            }
            RoomCategoryEntityComparer.RateCategoryIds = rateCategoryIds;

            roomCategoryList.Sort(new RoomCategoryEntityComparer());

            #endregion

            if (roomCategoryList != null)
            {
                List<string> uniqueCategoryList = new List<string>();
                Hashtable defaultRoomlist = new Hashtable();
                List<RoomCategoryEntity> defaultRoomCategories = new List<RoomCategoryEntity>();
                List<RoomCategoryEntity> regularRoomCategories = new List<RoomCategoryEntity>();

                for (int roomCategoryCount = 0; roomCategoryCount < roomCategoryList.Count; roomCategoryCount++)
                {
                    RoomCategoryEntity roomCategory = roomCategoryList[roomCategoryCount];

                    if (roomCategoryCount == 0)
                    {
                        defaultRoomCategories.Add(roomCategory);
                    }
                    else if (
                        (Core.StringUtil.IsStringInArray(
                            RoomRateDisplayUtil.GetDefaultRoomTypeIds(
                                SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode), roomCategory.Id)))
                    {
                        if (defaultRoomCategories.Count <= 5)
                        {
                            defaultRoomCategories.Add(roomCategory);
                        }
                        else
                        {
                            regularRoomCategories.Add(roomCategory);
                        }
                    }
                    else
                    {
                        if (roomCategoryCount > 0)
                        {
                            regularRoomCategories.Add(roomCategory);
                        }
                    }
                }
                if (defaultRoomCategories.Count <= 1)
                {
                    defaultRoomCategories.Clear();
                    regularRoomCategories.Clear();
                    for (int roomCategoryCount = 0; roomCategoryCount < roomCategoryList.Count; roomCategoryCount++)
                    {
                        RoomCategoryEntity roomCategory = roomCategoryList[roomCategoryCount];
                        if (roomCategoryCount == 0)
                        {
                            defaultRoomCategories.Add(roomCategory);
                            if (
                                !Core.StringUtil.IsStringInArray(
                                    RoomRateDisplayUtil.GetDefaultRoomTypeIds(
                                        SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode), roomCategory.Id))
                            {
                                noDefaultRoomTypesAvailable = true;
                            }
                        }
                        else if (
                            !Core.StringUtil.IsStringInArray(
                                RoomRateDisplayUtil.GetDefaultRoomTypeIds(
                                    SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode), roomCategory.Id))
                        {
                            if (defaultRoomCategories.Count < 6 && noDefaultRoomTypesAvailable)
                            {
                                defaultRoomCategories.Add(roomCategory);
                                noDefaultRoomTypesAvailable = true;
                            }
                            else
                            {
                                regularRoomCategories.Add(roomCategory);
                            }
                        }
                    }
                }
                int remainingNoofRooms = 0;
                int roomCount = 0;
                int additionaldefaultrooms = 0;
                int remainingRoomCount = 0;

                if (roomCategoryList.Count > Convert.ToInt32(AppConstants.SIX))
                    remainingNoofRooms = roomCategoryList.Count - Convert.ToInt32(AppConstants.SIX);
                else
                    remainingNoofRooms = roomCategoryList.Count - defaultRoomCategories.Count;

                RoomCategoryEntity roomCategoryEntity = null;
                int counter = 0;
                for (int roomCategoryCount = 0; roomCategoryCount < defaultRoomCategories.Count; roomCategoryCount++)
                {
                    roomCategoryEntity = defaultRoomCategories[roomCategoryCount];
                    counter = GetIndexFromSessionList(roomCategoryEntity, counter, roomNumber);
                    if (roomCategoryCount == 0)
                    {
                        CreateDefaultRoomRateRows(true, counter, roomCategoryEntity, rateDisplay,
                                                  defaultRoomTypesAvailable, showPricePerStay, showAllRoomTypes,
                                                  ref roomRatesTable, roomNumber, ref radioButtonCount,
                                                  noDefaultRoomTypesAvailable);
                    }
                    else
                    {
                        CreateDefaultRoomRateRows(false, counter, roomCategoryEntity, rateDisplay,
                                                  defaultRoomTypesAvailable,
                                                  showPricePerStay, showAllRoomTypes, ref roomRatesTable, roomNumber,
                                                  ref radioButtonCount,
                                                  noDefaultRoomTypesAvailable);
                    }
                }
                roomCount = defaultRoomCategories.Count;

                if (regularRoomCategories.Count > 0)
                {
                    if (defaultRoomCategories.Count < Convert.ToInt32(AppConstants.SIX) && roomCategoryList.Count > Convert.ToInt32(AppConstants.SIX))
                    {
                        additionaldefaultrooms = Convert.ToInt32(AppConstants.SIX) - defaultRoomCategories.Count;
                        for (int roomCategoryCount = 0; roomCategoryCount < additionaldefaultrooms; roomCategoryCount++)
                        {
                            roomCategoryEntity = regularRoomCategories[roomCategoryCount];
                            counter = GetIndexFromSessionList(roomCategoryEntity, counter, roomNumber);
                            CreateDefaultRoomRateRows(false, counter, roomCategoryEntity, rateDisplay,
                                                      defaultRoomTypesAvailable,
                                                      showPricePerStay, showAllRoomTypes, ref roomRatesTable, roomNumber,
                                                      ref radioButtonCount,
                                                      noDefaultRoomTypesAvailable);
                        }
                    }
                    for (int roomCategoryCount = 0; roomCategoryCount < remainingNoofRooms; roomCategoryCount++)
                    {
                        if (additionaldefaultrooms > 0)
                        {
                            remainingRoomCount = roomCategoryCount + additionaldefaultrooms;
                        }
                        else
                        {
                            remainingRoomCount = roomCategoryCount;
                        }
                        roomCategoryEntity = regularRoomCategories[remainingRoomCount];

                        counter = GetIndexFromSessionList(roomCategoryEntity, counter, roomNumber);

                        if (defaultRoomCategories.Count == 0 && roomCategoryCount == 0)
                        {
                            CreateDefaultRoomRateRows(true, counter, roomCategoryEntity, rateDisplay,
                                                      defaultRoomTypesAvailable, showPricePerStay, showAllRoomTypes,
                                                      ref roomRatesTable, roomNumber,
                                                      ref radioButtonCount, noDefaultRoomTypesAvailable);
                        }
                        else
                        {
                            CreateDefaultRoomRateRows(false, counter, roomCategoryEntity, rateDisplay,
                                                      defaultRoomTypesAvailable,
                                                      showPricePerStay, showAllRoomTypes, ref roomRatesTable, roomNumber,
                                                      ref radioButtonCount,
                                                      noDefaultRoomTypesAvailable);
                        }
                    }
                }
                if ((remainingNoofRooms == 0) || (roomCategoryList.Count == 1))
                {
                    tabBtn.Style.Add("display", "none");
                }
                else
                {
                    tabBtn.Style.Add("display", "block");
                }
            }
        }

        /// <summary>
        /// To get the matching index from SessionWrapper.ListHotelRoomRate as sorted list index is different
        /// </summary>
        /// <param name="roomCategoryEntity"></param>
        /// <param name="counter"></param>
        /// <returns></returns>
        private static int GetIndexFromSessionList(RoomCategoryEntity roomCategoryEntity, int counter, int roomNumber)
        {
            counter = 0;
            if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null)
            {
                foreach (RoomCategoryEntity roomCtgry in HotelRoomRateSessionWrapper.ListHotelRoomRate[roomNumber].RateRoomCategories)
                {
                    if (roomCategoryEntity == roomCtgry)
                    {
                        break;
                    }
                    counter++;
                }
            }
            return counter;
        }


        private void CreateDefaultRoomRateRows(bool isLowestCategory, int roomCategoryCount,
                                               RoomCategoryEntity roomCategory, BaseRateDisplay rateDisplay,
                                               bool defaultRoomTypesAvailable,
                                               bool showPricePerStay, bool showAllRoomTypes, ref Table roomRatesTable,
                                               int roomNumber, ref int radioButtonCount,
                                               bool noDefaultRoomTypesAvailable)
        {
            RoomRateRowContainer rateRowContainer = null;
            List<RoomCategoryEntity> roomCategoryList = rateDisplay.RateRoomCategories;
            bool isDefaultRow = false;
            if (defaultRoomTypesAvailable)
            {
                isDefaultRow = false;

                if (
                    (Core.StringUtil.IsStringInArray(
                        RoomRateDisplayUtil.GetDefaultRoomTypeIds(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode),
                        roomCategory.Id)))
                {
                    if (RoomRateDisplayUtil.HasMulitpleRoomCategories(roomCategory.Id, roomCategoryList) &&
                        (CheckForMinRateRoomCategory(roomCategory, roomCategoryList, showPricePerStay)))
                        isDefaultRow = true;
                    else if (!(RoomRateDisplayUtil.HasMulitpleRoomCategories(roomCategory.Id, roomCategoryList)))
                        isDefaultRow = true;
                }
            }
            if (noDefaultRoomTypesAvailable)
            {
                isDefaultRow = true;
            }
            if (isLowestCategory)
                isDefaultRow = true;

            string roomTeaserText = string.Empty;
            string roomCategoryTooltip = string.Empty;
            if (rateDisplay.HotelDestination != null && rateDisplay.HotelDestination.HotelRoomDescriptions != null
                && rateDisplay.HotelDestination.HotelRoomDescriptions.Count > 0)
            {
                foreach (HotelRoomDescription hotelRoomDescription in rateDisplay.HotelDestination.HotelRoomDescriptions
                    )
                {
                    if (roomCategory.Id.Equals(hotelRoomDescription.HotelRoomCategory.RoomCategoryId))
                    {
                        if (hotelRoomDescription.HotelRoomCategory.RoomCategoryteserText.Length > 256)
                        {
                            roomTeaserText = hotelRoomDescription.HotelRoomCategory.RoomCategoryteserText.Remove(256) +
                                             "...";
                            ;
                        }
                        else
                            roomTeaserText = hotelRoomDescription.HotelRoomCategory.RoomCategoryteserText;
                    }
                }
            }

            roomCategoryTooltip = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RoomToolTip");

            rateRowContainer = CreateRoomRateRow(rateDisplay, showPricePerStay, roomCategoryCount, roomCategory,
                                                 ref roomRatesTable, roomNumber, isDefaultRow, roomTeaserText,
                                                 roomCategoryTooltip, ref radioButtonCount);

            if ((null != rateRowContainer) &&
                (RoomRateDisplayUtil.HasMulitpleRoomCategories(roomCategory.Id, roomCategoryList)))
            {
                if ((showAllRoomTypes) || (!defaultRoomTypesAvailable))
                {
                    if (!CheckForMinRateRoomCategory(roomCategory, roomCategoryList, showPricePerStay))
                    {
                        rateRowContainer.DisplayBedTypes(roomCategory);
                    }
                }
            }
        }


        /// <summary>
        /// Get the sub room category having the same room category ID, from the main list
        /// </summary>
        /// <param name="roomCategoryList">Main room category list</param>
        /// <param name="roomCategory">Multiple room category that need to be identified from the main 
        /// list with the same id as passed room category id</param>
        /// <returns>Returns the list of RoomCategoryEntity having the same room category id</returns>
        private List<RoomCategoryEntity> GetMatchingRoomCategoryList(List<RoomCategoryEntity> roomCategoryList,
                                                                     RoomCategoryEntity roomCategory)
        {
            List<RoomCategoryEntity> subRoomCategoryList = new List<RoomCategoryEntity>();

            for (int ctr = 0; ctr < roomCategoryList.Count; ctr++)
            {
                if (roomCategory.Id == roomCategoryList[ctr].Id)
                {
                    subRoomCategoryList.Add(roomCategoryList[ctr]);
                }
            }

            return subRoomCategoryList;
        }

        /// <summary>
        /// Method to find out the min rate from the list against the given rate category
        /// </summary>
        /// <param name="roomCategoryList">Room category list to be compaired to get the min rate.</param>
        /// <param name="rateCategoryKey">Rate category to be compaired to get the min rate</param>
        /// <param name="showPricePerStay">Boolean indicates whether to compare base rate or total rate</param>
        /// <returns>Returns the min rate from the list.</returns>
        private double GetMinRateInRoomCategory(List<RoomCategoryEntity> roomCategoryList, string rateCategoryKey,
                                                bool showPricePerStay)
        {
            double minRateValue = double.MaxValue;
            double rateToCheck = 0;

            for (int ctr = 0; ctr < roomCategoryList.Count; ctr++)
            {
                try
                {
                    List<RoomRateEntity> roomRateEntityList = roomCategoryList[ctr].RateCategories[rateCategoryKey];

                    if (!showPricePerStay)
                    {
                        if (roomRateEntityList[0] != null && roomRateEntityList[0].BaseRate != null)
                            rateToCheck = roomRateEntityList[0].BaseRate.Rate;
                    }
                    else
                    {
                        if (roomRateEntityList[0] != null && roomRateEntityList[0].TotalRate != null)
                            rateToCheck = roomRateEntityList[0].TotalRate.Rate;
                    }

                    if (minRateValue > rateToCheck)
                    {
                        minRateValue = rateToCheck;
                    }
                }
                catch (KeyNotFoundException knfe)
                {
                }
            }

            return minRateValue;
        }


        /// <summary>
        /// Method to find out the minimun rate, from all the rate categories in corresponding room categories.
        /// </summary>
        /// <param name="roomCategory">Room category exists multiple times in a list</param>
        /// <param name="roomCategoryList">Room category list to be compaired with for min rate of given room category</param>
        /// <param name="showPricePerStay">Boolean indicates whether to compare base rate or total rate</param>
        /// <returns>Returns true, if the min rate category with min rate exists in the passed room category</returns>
        private bool CheckForMinRateRoomCategory(RoomCategoryEntity roomCategory,
                                                 List<RoomCategoryEntity> roomCategoryList,
                                                 bool showPricePerStay)
        {
            bool returnValue = false;
            double minRateValue = 0;
            double rateToCheck = 0;

            List<RoomCategoryEntity> subRoomCategoryList = GetMatchingRoomCategoryList(roomCategoryList, roomCategory);

            Dictionary<string, double> minRateForEachCategory = new Dictionary<string, double>();

            if ((subRoomCategoryList.Count > 0) && (roomCategory.RateCategories.Count > 0))
            {
                int subRoomCategoryListLength = subRoomCategoryList.Count;
                minRateValue = double.MaxValue;

                foreach (string rateCategoryKey in roomCategory.RateCategories.Keys)
                {
                    minRateValue = GetMinRateInRoomCategory(subRoomCategoryList, rateCategoryKey, showPricePerStay);

                    minRateForEachCategory.Add(rateCategoryKey, minRateValue);
                }
            }

            minRateValue = double.MaxValue;
            foreach (string key in minRateForEachCategory.Keys)
            {
                if (minRateValue > minRateForEachCategory[key])
                    minRateValue = minRateForEachCategory[key];
            }

            foreach (string keyRateCategory in roomCategory.RateCategories.Keys)
            {
                if (minRateValue == minRateForEachCategory[keyRateCategory])
                {
                    if (showPricePerStay)
                    {
                        rateToCheck = roomCategory.RateCategories[keyRateCategory][0].TotalRate.Rate;
                    }
                    else
                    {
                        rateToCheck = roomCategory.RateCategories[keyRateCategory][0].BaseRate.Rate;
                    }

                    if (rateToCheck == minRateValue)
                    {
                        returnValue = true;
                        break;
                    }
                }
            }

            return returnValue;
        }


        /// <summary>
        /// Creates the Room Rate Row and Add it to the main RoomRate Type container
        /// </summary>
        /// <param name="rateDisplay"></param>
        /// <param name="showPricePerStay"></param>
        /// <param name="roomCategoryCount"></param>
        /// <param name="roomCategory"></param>
        /// <returns></returns>
        private RoomRateRowContainer CreateRoomRateRow(BaseRateDisplay rateDisplay, bool showPricePerStay,
                                                       int roomCategoryCount, RoomCategoryEntity roomCategory,
                                                       ref Table roomRatesTable, int roomNumber,
                                                       bool isDefaultRow, string roomTeaserText,
                                                       string roomCategoryTooltip, ref int radioButtonCount)
        {
            RoomRateRowContainer rateRowContainer =
                LoadControl("RoomRateRowContainer.ascx") as RoomRateRowContainer;
            if (rateRowContainer != null)
            {
                TableRow roomRateRow = new TableRow();
                roomRateRow.Controls.Clear();
                if (!isDefaultRow || roomRatesTable.Rows.Count > 6)
                    roomRateRow.CssClass = "HiddenRows";
                rateRowContainer.SetRoomRateDisplay(rateDisplay, roomCategoryCount, roomCategory,
                                                    rateDisplay.RateCategoriesDisplay, showPricePerStay, ref roomRateRow,
                                                    roomNumber,
                                                    roomTeaserText, roomCategoryTooltip, ref radioButtonCount);
                roomRatesTable.Rows.Add(roomRateRow);
            }
            return rateRowContainer;
        }

        /// <summary>
        /// Check if multiple room categories of <code>roomCategory</code> are existing in the 
        /// <code>roomCategoryList</code> by checking the <code>RoomCategoryEntity.Id</code> value
        /// with the values in the list.
        /// 
        /// If more than one category with id that of roomCategory is existing true will be returned else false is returned
        /// </summary>
        /// <param name="roomCategory">RoomCategoryEntity which need to be checked against with</param>
        /// <param name="roomCategoryList">The list of RoomCategoryEntity in which needs to checked against</param>
        /// <returns></returns>
        public bool HasMulitpleRoomCategories(RoomCategoryEntity roomCategory, List<RoomCategoryEntity> roomCategoryList)
        {
            string roomCategoryId = roomCategory.Id;
            int noOfRoomCategories = 0;

            foreach (RoomCategoryEntity roomCategoryEntity in roomCategoryList)
            {
                if (roomCategoryId == roomCategoryEntity.Id)
                {
                    noOfRoomCategories++;
                }
            }

            return (noOfRoomCategories > 1);
        }

        #endregion
    }
}