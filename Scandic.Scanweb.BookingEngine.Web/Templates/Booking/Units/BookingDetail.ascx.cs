//  Description					: Code Behind class for BookingDetails Control			  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Shankar Dasgupta                                   	  //
/// Author email id				:                           							  //
/// Creation Date				: 05th November  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: Modified by Shameem for Reservation 2.0 [Booking details//
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespace

using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Entity;

#endregion

    #region Scandic Namespace

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    ///  Code Behind class for ModifyBookingDates
    /// </summary>
    public partial class BookingDetail : EPiServer.UserControlBase
    {
        private bool isSessionValid = true;

        #region Protected Methods        

        /// <summary>
        /// Page Load Method of BookingDetails Control
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="e">
        /// Arguments for the Event
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            BookingEngineSessionWrapper.IsModifyBooking = false;
            isSessionValid = WebUtil.IsSessionValid();
           
            if (isSessionValid)
            {
                Scanweb.Core.AppLogger.LogInfoMessage("START : In BookingDetail page :: Page_Load() : ");

                if (this.Visible)
                {
                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
                    if (hotelSearch != null)
                    {
                    }
                    else
                    {
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE));
                    }
                }

                Scanweb.Core.AppLogger.LogInfoMessage("END : In BookingDetail page :: Page_Load() : ");
            }
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            ProgressBar.SetProgressBarSelection(EpiServerPageConstants.BOOKING_DETAILS_PAGE);
        }

        #endregion
    }
}