<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectHotel.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.SelectHotel" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Controller" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%@ Register Src="~/Templates/Booking/Units/UsefulLinksContainer.ascx" TagName="UsefulLinks" TagPrefix="Booking" %>
<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx" TagName="ReservationInformation" TagPrefix="Booking" %>
<%@ Register TagPrefix="Scanweb" TagName="SelectHotelGoogleMapIframe" Src="~/Templates/Scanweb/Units/Static/Booking/SelectHotelGoogleMapIframe.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="ProgressBar" Src="~/Templates/Booking/Units/ProgressBar.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<%--<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Scanweb/Javascript/SiteCatalyst/s_code.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>
--%>
<script type="text/javascript">
    function setsource() {
        //to load the google maps API async; not on page load.
        var googleMapIframe = document.getElementById('SelectHotelGoogleMapIframe');
        //the source of the iframe is define at the click of the map tab
        googleMapIframe.src = '<%=ResolveUrl("~/Templates/Scanweb/Pages/Booking/BookingGoogleMapPage.aspx")%>';
        //fresh the iframe
        RefreshGoogleMapIframe();
    } 
</script>

<script type="text/javascript">

    $(document).ready(function() {

        $('.alternateCurrencySelectHotel').live('mouseover',
		function() {
		    tip = $(this).attr('title');
		    setToolTipWidth = "350px";
		    $(this).append(
							'<div class="ttContainer" style="width:' + setToolTipWidth + '">'
								+ '<div class="ttTop"><span>&#160;</span></div>'
								+ '<div class="ttMainContentWrapper">'
								+ '<div class="ttContentWrapper">'
								+ '<div class="ttContent">'
									+ tip
								+ '</div>'
								+ '</div>'
								+ '</div>'
								+ '<div class="ttBottom"><span>&#160;</span></div>'
								+ '<span class="ttpointer">&#160;</span>'
							+ '</div>'
						);
		    $(this).attr('title', '');
		    //  $('.ratesHolderContainer').css('overflow', 'visible');
		    $(this).children('.ttContainer').show();
		});
        $('.alternateCurrencySelectHotel').live('mouseout', function() {
            //$('.ratesHolderContainer').css('overflow', 'hidden');
            $('.ttContainer').fadeOut(100);
            $(this).children().remove();
            if (tip == undefined) {
                tip = $(this).attr('title');
            } else {
                $(this).attr('title', tip);
            } 
        });
    });
</script>

<!--START: Progress bar -->
  <Scanweb:ProgressBar id="ProgressBar" runat="server" />
<!--END: Progress bar -->
<div id="ErrMsg" style="font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;font-size:small;color:Red;font-weight:bolder"></div>

<div class="BE">
<!-- Step1: Select hotel -->
<div>
    <!-- Reservation 2.0 Progress Tab -->
    <div id="progTab">
    </div>
    <!-- /Progress Tab -->
    
    <!--Reservation 2.0-->
    <div id="Header" class="borderH2">
    
	<h2><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/HotelResults") %><span>&nbsp</span><asp:Literal ID="cityName" runat="server"></asp:Literal></h2>

    </div>
    <div class="infoAlertBox" id="alertDiv" runat="server" style="display:none;"> 
	    <div class="threeCol alertContainer">
		<div class="hd sprite">&nbsp;</div>
		<div class="cnt sprite">		    
			<p class="scansprite" id= "divNoAvailabilityMessage" runat="server"></p> 
		</div>
		<div class="ft sprite"></div>
	    </div>
    </div>
    <div class="infoAlertBox infoAlertBoxRed" id="errorDiv" runat="server" style="display:none;"></div>
    <div class="infoAlertBox infoAlertBoxRed" id="errorAlertDiv" runat="server" style="display:none;">
                    <div class="threeCol alertContainer">
                        <div class="hd sprite">&nbsp;</div>
                        <div class="cnt sprite">
                           <span class="alertIconRed">&nbsp;</span>
                           <div id="ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_Div1" class="descContainer">
                                <label class="titleRed"><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/PleaseNote") %></label>
                                <label id="errorLabel" runat="server" class="infoAlertBoxDesc"></label>
                                    
                           </div>
                           <div class="clearAll">&nbsp;</div>
                        </div>
                        <div class="ft sprite">&nbsp;</div>
                    </div>               
                 
        </div>
    <div class="clearAll"><!-- Clearing Float --></div>
    
    <!--Reservation2.0:-Tab for List and Map-->
    <div id="TabStructure" runat="server">
    <!-- Scandic Tabs -->
    <div id="tabWrapper" class="scandicTabs">
        <ul>
            <li><a id="listViewTab" href="#" rel=".listView" class="active tabs"><span><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/List")%></span></a></li>
            <li><a id="mapTab" href="#" onclick="setsource()" class="tabs" rel=".map"><span><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/Map")%></span></a></li>
        </ul>
        <div class="clearAll"><!-- Clearing All --></div>
	</div>
	
	<div class="tabCntnt default listView">
		<!-- <div class="tabHd"></div>-->
		<div class="tabCnt">
			<div class="formCnt chooseStayTypeWrap" id="chooseStayWrapper">

			<div class="formLeftCnt">
				<label for="name"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/SortBy") %></label>
				<asp:DropDownList CssClass="altHotelSortBy" TabIndex="10" ID="ddlSortHotel"  OnTextChanged ="Sort_Hotels" OnSelectedIndexChanged="Sort_Hotels" runat="server" AutoPostBack="True">
                </asp:DropDownList>
				<label id="lblView" runat="server" for="view"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/View") %></label>
				<asp:DropDownList ID="ddlHotelsPerPage" TabIndex="13" cssClass="altHotelView" OnTextChanged ="ReArrange_pagination" OnSelectedIndexChanged="ReArrange_pagination" runat="server" AutoPostBack="True">
                </asp:DropDownList>
			</div>
			<div class="chooseStayType">
			<div style="float:left;line-height:20px;">
			<strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/prices") %></strong>
             <asp:RadioButton ID="rdoperNight" TabIndex="16" Text="" Checked="True" GroupName="groupPrices"
                        runat="server" CssClass="radio" AutoPostBack="true" EnableViewState="true" OnCheckedChanged="StayPerNight" />
                    <asp:RadioButton ID="rdoperStay" TabIndex="19" Text="" Checked="False" GroupName="groupPrices"
                        runat="server" CssClass="radio" AutoPostBack="true" EnableViewState="true"  OnCheckedChanged="PerStay" />   
						</div>
                       <span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/PopupPerNightPerStay") %>" class="help spriteIcon toolTipMe"></span>
		    </div>
            </div>
        </div>
        <div class="tabFt"></div>
        <div class="m07HotelInfo">                
				<!-- header -->
            <div id="subHeader" class="subHeader" runat="server">
                <div class="availDate"><span id ="headerDtMsg" runat="server"></span></div>
              
                <input type="hidden" id="txtPageNo" value="1" runat="server"/>
                <input type="hidden" id="txtShowAll"  value="FALSE" runat="server"/>
                <input type="hidden" id="txtAvailabilityMsg1"  value="" runat="server"/>
                <input type="hidden" id="txtAvailabilityMsg2"  value="" runat="server"/>
                <input type="hidden" id="hdnAlert1"  value="" runat="server"/>
                <input type="hidden" id="operaErr" value="" runat="server" />
                
            </div>
	        <!-- /header -->
	        <!-- Details -->
	        <div id="hotelDetailContainer">
	        <div id="allHotels" runat="server">
            </div>
            <!-- availabilty search pogress -->
            <div id="blkSearchProgress" class="hotelDetailSearch" runat="server">
                <img src="<%= ResolveUrl("~/Templates/Booking/Styles/Default/Images/searching.gif") %>" align="middle" width="83" height="27" />
                <p class="teaser"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/loadingmessage") %></p>
                <div class="clear">&nbsp;</div>
            </div>
            <!-- /availabilty search pogress -->
	        </div>
	        <!-- /Details -->

	<!-- Footer -->
	<div id="hotelFooterContainer">
	    <%
	        int totalPages = TotalPages();
            if (totalPages > 1 || SelectHotelUtil.IsSearchToBeDone())
            {
%>
	    <div id="footerContainer" runat="server">
		<!-- page listing -->
		<div class="pageListingContainer">		
			<div id="pageListingContainer">		
			<div class="showAll">
				 <% if ("FALSE" == txtShowAll.Value)
                    {%>
                        <span class="showLink">
                      
                       
				        </span>
                    <% }
                    else
                    {%> 
				            <span class="hideLink">
				           
			               </span>
                    <% } %>
				
			</div>
			<% if(paginationEnabled){ %>
			<div class="pagination">
			
			<div id="pageListing" class="pageListing">
			    <%
			        if (!SelectHotelUtil.IsSearchToBeDone() && "FALSE" == txtShowAll.Value)
                    {
                        int currentPage = int.Parse(txtPageNo.Value);
                        int totalPages = TotalPages();
%>
                    <span class="prevLink">
                <%
                    if (currentPage != 1)
                    {
%>
                    <a href="javascript:SelectPagination(<%= (currentPage - 1) %>);"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/previous") %></a>
				<%
                    }
                    else
                    {
%>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/previous") %>
                 <%
                    }
%>
                </span>
                <%
                    List<string> pageDetails = GetPaginationDetails();
                    for (int i = 0; i < pageDetails.Count; i++)
                    {
%>
                    | <span class="pageItem">
                    <%
                        if (currentPage != i + 1)
                        {
%>
                        <a href="javascript:SelectPagination(<%= i + 1 %>)";><%= pageDetails[i] %></a>
                    <%
                        }
                        else
                        {
%>
                        <span class="pageItemSelected"><%= pageDetails[i] %></span>
                    <%
                        }
%>
                     </span>
                 <% } %>
                 |
                 <span class="nextLink">
                 <% if (currentPage != totalPages)
                    { %>
                    <a href="javascript:SelectPagination(<%= (currentPage + 1) %>);"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/next") %></a>
                 <% }
                    else
                    { %>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/next") %>
                 <% } %>
                </span>
				<% } %>
			</div>	
			</div>		
			<%} %>
			</div>
		</div>
		
		</div>
	    <%
            }
%>

	</div>
	<!-- Footer -->
	
	<!--START:info container -->
	<div class="infoAlertBox" style="display:none" id="infoAlert" runat="server">
	    <div class="threeCol alertContainer">
		    <div class="hd sprite">&nbsp;</div>
		    <div class="cnt sprite">
			   <p class="scansprite" id="alert2"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/AlertText") %>
			   </p>
		    </div>
		    <div class="ft sprite"></div>
	    </div>
    </div>

	<!-- /Button -->
	<!-- availabilty search pogress -->
	<div id="blkPaginationProgress" class="paginationSearch" runat="server">
		<img src="<%= ResolveUrl("~/Templates/Booking/Styles/Default/Images/searching.gif") %>" align="middle"  width="83" height="27" />	
		<div class="clear">&nbsp;</div>
	</div>
	<!-- /availabilty search pogress -->
	
</div>
<input id="txtHotelId" runat="server" type="hidden" />
<asp:LinkButton ID="SelectOneHotel" OnClick="Select_Hotel" runat="server"></asp:LinkButton>
<asp:LinkButton ID="SelectPagination" OnClick="Select_Pagination" runat="server"></asp:LinkButton>
</div>
<!--Reservation 2.0:Map tab starts here-->
	<div class="tabCntnt map">	
		<div class="tabCnt">
			<div class="formCnt mapRadioCnt">
			<div style="float:left;line-height:20px;">
				<strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/prices") %></strong>
				<span class="radio">
				<input name="rdoPriceType" id="rdoPerNightMap" runat="server" type="radio" onclick="SwitchUnitText('rdoPerNightMap')" disabled="disabled" />
				<label for="night" id="lblPerNightMap" runat="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rdoPernight") %></label>
                </span>
                <span class="radio">
                <input name="rdoPriceType" id="rdoPerStayMap" runat="server" type="radio" onclick="SwitchUnitText('rdoPerStayMap')" disabled="disabled"/>
				<label for="stay" id="lblPerStayMap" runat="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rdoperStay") %></label>				 
				</span>
				</div>
				<span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/PopupPerNightPerStay") %>" class="help spriteIcon toolTipMe"></span>
			</div>			
		</div>
		<div class="tabFt"></div>
		<!--Reservation2.0:Add GooleMap control-->
		<div id="GoogleMap" style="position:relative;">
            <Scanweb:SelectHotelGoogleMapIframe id="GoogleMapIframe1"  runat="server" />
            <%--<Scanweb:SelectHotelGoogleMap ID="SelectHotelGoogleMap1" runat="server" />--%>

            <div class="jqmWindow" id="progressBar">
                <div id="BookingProgressDiv" class="loadingIcon">
                    <img src="<%= ResolveUrl("~/templates/Scanweb/Styles/Default/Images/loadingIcon.gif") %>" align="top" />
                        <strong class="loadingText">
                            <%=
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Units/Static/FindHotelSearch/Loading") %>
                        </strong>
                </div>
            </div>
        </div>
		
		<!--Reservation 2.0:add alert information below google map-->
		<div class="infoAlertBox">
	    <div class="threeCol alertContainer">
		<div class="hd sprite">&nbsp;</div>
		<div class="cnt sprite">
			<p class="scansprite"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/AlertText") %>
			</p>
		</div>
		<div class="ft sprite"></div>
	    </div>
        </div>
	</div>
	
</div>
 <div class="bkingDetBtmLinks">	
	<div class="goBackLink fltLft">
			<a id="btnBack" href="javascript:history.go(-1)" runat="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/goback") %></a>
		</div>
	</div>
	</div>
</div>
<!--Image Gallery Implementation-->
<div class="jqmWindow dialog imgGalPopUp">
	<div class="hd sprite"></div>
	<div class="cnt">
     <div id="imgGallery" class="scandicTabs">
        <ul>
		    <li><a class="active" href="#" rel=".images"><span><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/MoreImagesPopUp/Images") %></span></a></li>
    	    <li><a href="#" rel=".galleryView"><span><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/MoreImagesPopUp/Image360") %></span></a></li>
        </ul>
        <div class="clearAll"><!-- Clearing All --></div>
	</div>
	 
	<div id ="imgGal" runat="server" class="bedTypeListclass">
	</div>
	</div>
	<div class="ft sprite"></div>
</div>
<script type="text/javascript"  language="javascript">
    //For server
    var destSearchStartTime = 0;
    var destSearchTimeOut;
    var requestUrl = <%= "\"" + GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") + "\"" %>;
    var pRpN = <%= "'" + WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/perroompernight") + "'" %>;
    var sH =  <%= "'" + WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selectahotel") + "'" %>  ;
    var prv = <%= "'" + WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/previous") + "'" %>
    var nxt = <%= "'" + WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/next") + "'" %>
    var pageSize = <%= GetPageSize() %>;
    if(document.getElementById('ctl00_FullBodyRegion_MainBodyRegion_ctl00_ddlSortHotel')!=null)
        var index =document.getElementById('ctl00_FullBodyRegion_MainBodyRegion_ctl00_ddlSortHotel').selectedIndex;
    if(index == 0)
    {
        <% if (!IsPostBack)
           { %>   
        destSearchTimeOut = <%= ConfigurationManager.AppSettings["destSearchTimeOutInSeconds"]%>;
        destSearchStartTime = new Date().getTime();	 
        fetchHotels(false);
        <% } %>
    }
</script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/plugin/imageResizer.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>

<script type="text/javascript" language="javascript">
    <%
        if (SelectHotelUtil.IsSearchToBeDone() && (!AlternateHotelsSessionWrapper.DisplayAlternateHotelsAvailable) &&
            ((!AlternateHotelsSessionWrapper.DisplayNoHotelsAvailable)))
        {%>
    showHotelDetailSearch();
    
    //This will display "Loading..." overlay only the first time,not in the pagination
    AddMyOverlay();
    
    <% } %>
    DispErrMsg()
    //Contain value of result AjaxCall.stillHotelToFetchResult()
    var fetchStatus = true;
    function IsIframeLoadComplete()
    {
        var iframe =document.getElementById('SelectHotelGoogleMapIframe');
        if (iframe.document.readyState == 'complete')
        {
            RemoveMyOverlay();
            return;
        }
        window.setTimeout('IsIframeLoadComplete();', 100);      
    }
    
    function SwitchUnitText(rateType)
    {
        if (window.frames.SelectHotelGoogleMapIframe && window.frames.SelectHotelGoogleMapIframe.SwitchUnitText)
        {
            window.frames.SelectHotelGoogleMapIframe.SwitchUnitText(rateType);
        }
        else
        {
            var googleMapIframe = document.getElementById('SelectHotelGoogleMapIframe');
            if(googleMapIframe && googleMapIframe.contentWindow && googleMapIframe.contentWindow.SwitchUnitText)
            {
                googleMapIframe.contentWindow.SwitchUnitText(rateType);
            }
        }
    }
    function DispErrMsg()
    {
        var control = $("label[id$='errorLabel']");
	    if((null != control) && ('' != control.innerHTML))
	    {
		    var divCnt =$fn(_endsWith("errorDiv")); 
		    if(null != divCnt)
		    {
			    divCnt.style.display = 'block';
		    }
	    }
    }
    
    function ChangeRateTitle(val)
    {
        $('.rateText').html(val);
    }
    //Class for KeyValue object
    function KeyValue(key,value)
    {
        this.Key = key;
        this.Value = value;
    }
    //Assign the rate and corresponding text.    
    function AssignRate(array,text)
    {
        var count = array.Length;
        for(var c=0;c<count;c++)
        {
            $(array[c].Key).innerHtml  = array[c].Value;
        }
    }
    
 
</script>
<script type="text/javascript" language="javascript">
    function RemoveOverlayOnReload() {
        //Remove the load indicator in the select a hotel page.			
        RemoveMyOverlay();
        var rdoPerNight = $fn(_endsWith('rdoPerNightMap'));
        rdoPerNight.disabled = false;
        var rdoPerStay = $fn(_endsWith('rdoPerStayMap'));
        rdoPerStay.disabled = false;
    }

</script>


<div class="jqmWindow imgGalPopUp" id="tripAdvisoModal">
	<div class="cnt">
		<a href="#" class="jqmClose scansprite blkClose iframeClose"></a>
		<div id="tripAdvisorIframe">
		</div>
	</div>
</div>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#tripAdvisoModal').jqm({ trigger: 'a#lnkReviewCount' });
     });

 </script>
<!-- Step1: Select hotel -->