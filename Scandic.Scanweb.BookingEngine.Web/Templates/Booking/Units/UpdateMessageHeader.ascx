<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateMessageHeader.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.UpdateMessageHeader" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<!--NOTE: Dev - This block will be shown when the form data has been updated succesfully -->
<div id="Loyalty" class="BE">
<div id="SuccessResponse" runat ="server">
    <div class="feedbackMsg">
	    <div class="box-top-grey"><span class="titleTxt">
            <label><%= WebUtil.GetTranslatedText("/bookingengine/booking/updatemessageheader/messageheader") %>
            </label></span>
	    </div>
	    <div class="box-middle">
		    <div class="content">
			    <span>
			    <%= WebUtil.GetTranslatedText("/bookingengine/booking/updatemessageheader/messagebody") %>
			    </span>
		    </div>
	    </div>
	    <div class="box-bottom">&nbsp;</div>
    </div>
    </div>
</div>
<div class="clear">&nbsp;</div>
