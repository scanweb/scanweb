<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModifyBookingDetails.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ModifyBookingDetails" %>
<%@ Register Src="~/Templates/Booking/Units/UsefulLinksContainer.ascx" TagName="UsefulLinks" TagPrefix="ModifyBooking" %>
<%--<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx" TagName="PreviousBookingReservationInformation" TagPrefix="ModifyBooking" %>
<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx" TagName="NewBookingReservationInformation" TagPrefix="ModifyBooking" %>--%>
<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx"
    TagName="PrevBookingReservationInformation" TagPrefix="Booking" %>
    <%@ Register TagPrefix="Scanweb" TagName="ProgressBar" Src="~/Templates/Booking/Units/ProgressBar.ascx" %>
<%@ Register Src="~/Templates/Booking/Units/UserBookingContactDetails.ascx" TagName="BookingContactDetails" TagPrefix="ModifyBooking" %>

<script language="javascript" type="text/javascript">    
</script>
<div class="BE">
        <!-- Progress Tab -->
		
	
	<!-- Step3: Enter Booking Details -->
	<div id="BookingDetails">	
        <%--<ModifyBooking:PreviousBookingReservationInformation ID="prevBookingReservationInfo" runat="server">
        </ModifyBooking:PreviousBookingReservationInformation>
        <ModifyBooking:NewBookingReservationInformation  ID="newBookingReservationInfo" ShowHelpLink ="false" runat="server"  >
        </ModifyBooking:NewBookingReservationInformation>--%>
        <Scanweb:ProgressBar id="ProgressBar" runat="server" />
        <%--RK: Reservation 2.0 | Added new reservation container--%>
        <div class="roomDetails collapse">
            <%--<div class="hdLft sprite">
                <h2 class="hdRt sprite">
                  <label id="reservationHeader" runat="server"></label>
                  <a href="#" title="Room1 - Reservation number"
                        class="help spriteIcon toolTipMe"></a>
                   </h2>
            </div>--%>
            <Booking:PrevBookingReservationInformation ID="ReservationInfo" runat="server">
            </Booking:PrevBookingReservationInformation>
            <div class="ft sprite">&nbsp;</div>
        </div>
        <ModifyBooking:UsefulLinks ID="UsefulLinks" runat="server">
        </ModifyBooking:UsefulLinks>
        
        <ModifyBooking:BookingContactDetails ID="ModifyBookingContactDetails" runat="server">
        </ModifyBooking:BookingContactDetails>  
    </div>
	<!-- /hotel Info -->
</div>

