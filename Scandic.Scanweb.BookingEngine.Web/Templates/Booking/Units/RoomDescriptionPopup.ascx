<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RoomDescriptionPopup.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.RoomDescriptionPopup" %>
<div id="RoomDescription" runat="server">
    <asp:PlaceHolder ID="RoomCategoryPlaceHolder" runat="server">
        <h3 class="roomHeading" ><asp:Literal ID="RoomCategoryName" runat="server" /></h3>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="MainImagePlaceHolder" Visible="false" runat="server">
        <asp:Image CssClass="roomBanner" ID="MainImage" runat="server"/>    
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="TextPropertyPlaceHolder" runat="server">
        <div class="roomContent" id="roomContent" runat="server">
          <asp:Literal ID="TextProperty" runat="server"></asp:Literal>
        </div>
    </asp:PlaceHolder>
</div>			
