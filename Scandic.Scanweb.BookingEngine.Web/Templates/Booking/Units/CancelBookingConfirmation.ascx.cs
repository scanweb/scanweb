//  Description					: Code Behind class for ModifyBookingDates Control		  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Girish Krishnan                                	      //
//  Author email id				:                           							  //
//  Creation Date				: 10th December  2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for CancelBookingConfirmation
    /// </summary>
    public partial class CancelBookingConfirmation : EPiServer.UserControlBase, INavigationTraking
    {
        private delegate void CreatePDF(List<EmailEntity> email, List<string> reservations);

        private bool isSessionValid = true;
        private bool isCommonGuarantee = false;

        #region Page Events

        /// <summary>   
        /// Page Load Method for ConfirmCancellation Control
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="e">
        /// Event Arguments
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            isSessionValid = WebUtil.IsSessionValid();
            btnCancelBooking.Attributes.Add("onclick",
                                            "javascript:return performValidation(PAGE_CANCEL_BOOKING_CONFIRMATION);");
            spnCancelBooking.Attributes.Add("onclick",
                                            "javascript:return performValidation(PAGE_CANCEL_BOOKING_CONFIRMATION);");
            spnCancelBooking.Visible = false;

            //LoginStatus ctrl = (LoginStatus)this.Page.Master.FindControl("MenuLoginStatus");
            //if (ctrl != null)
            //{
            //    ctrl.UpdateLoginStatus(true);
            //}

            if (!IsPostBack && isSessionValid)
            {
                List<BookingDetailsEntity> bookingEntities = BookingEngineSessionWrapper.ActiveBookingDetails;
                if (
                    bookingEntities != null &&
                    bookingEntities.Count > 0 &&
                    bookingEntities[0] != null &&
                    bookingEntities[0].GuestInformation != null &&
                    bookingEntities[0].GuestInformation.IsRewardNightGift &&
                    bookingEntities[0].RewardNightGiftGuestInfo != null
                    )
                {
                    UpdateActualGuestInfoForRewardNightGift(bookingEntities);
                }

                List<BookingDetailsEntity> bookingDetailsList = BookingEngineSessionWrapper.ActiveBookingDetails;
                System.Collections.Generic.Dictionary<string, string> ratePlans = new Dictionary<string, string>();
                if (bookingDetailsList != null && bookingDetailsList.Count > 0)
                {
                    for (int i = 0; i < bookingDetailsList.Count; i++)
                    {
                        string ratePlanCode = bookingDetailsList[i].HotelRoomRate.RatePlanCode;
                        RateCategory rate = RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlanCode);

                        if (rate != null && !ratePlans.ContainsValue(rate.RateCategoryId))
                        {
                            ratePlans.Add(ratePlanCode, rate.RateCategoryId);
                        }
                    }
                    Reservation2SessionWrapper.GuaranteeInfoRate = ratePlans;
                }

                PopulateUI();
                PopulateEachBooking();
                transactionCount.Value = ReservationNumberSessionWrapper.TransactionCount.ToString();

                if ((Reservation2SessionWrapper.GuaranteeInfoRate != null && Reservation2SessionWrapper.GuaranteeInfoRate.Count == 1) &&
                    (BookingEngineSessionWrapper.ActiveBookingDetails.Count >= 1))
                {
                    isCommonGuarantee = true;
                    DisplayGuaranteeInformation(4);
                }
            }
        }

        /// <summary>
        /// Updates the actual guest info for reward night gift.
        /// </summary>
        /// <param name="bookingEntities">The booking entities.</param>
        private void UpdateActualGuestInfoForRewardNightGift(List<BookingDetailsEntity> bookingEntities)
        {
            if (
                bookingEntities != null &&
                bookingEntities[0] != null &&
                bookingEntities[0].GuestInformation != null &&
                bookingEntities[0].GuestInformation.IsRewardNightGift &&
                bookingEntities[0].RewardNightGiftGuestInfo != null
                )
            {
                bookingEntities[0].GuestInformation.FirstName = bookingEntities[0].RewardNightGiftGuestInfo.FirstName;
                bookingEntities[0].GuestInformation.LastName = bookingEntities[0].RewardNightGiftGuestInfo.LastName;
                bookingEntities[0].GuestInformation.City = bookingEntities[0].RewardNightGiftGuestInfo.City;
                bookingEntities[0].GuestInformation.Country = bookingEntities[0].RewardNightGiftGuestInfo.Country;
                if(bookingEntities[0].GuestInformation.Mobile != null)
                    bookingEntities[0].GuestInformation.Mobile.Number = bookingEntities[0].RewardNightGiftGuestInfo.MobileNumber;

                // Saving the email of the FGP user for sending cancel confirmation email.
                if (bookingEntities[0].GuestInformation.EmailDetails != null)
                {
                    bookingEntities[0].RewardNightGiftGuestInfo.EmailAddressFgpUser = bookingEntities[0].GuestInformation.EmailDetails.EmailID;
                    bookingEntities[0].GuestInformation.EmailDetails.EmailID = bookingEntities[0].RewardNightGiftGuestInfo.EmailAddress;
                }
            }
        }

        /// <summary>
        /// This method will populate each booking.
        /// </summary>
        private void PopulateEachBooking()
        {
            List<BookingDetailsEntity> bookingDetailsList = BookingEngineSessionWrapper.ActiveBookingDetails;
            if (bookingDetailsList != null && bookingDetailsList.Count > 0)
            {
                int totalBooking = bookingDetailsList.Count;
                if (
                    bookingDetailsList.FindAll(delegate(BookingDetailsEntity bd) { return bd.IsCancelledBySytem; }).
                        Count > 0)
                    MasterCheckBox.Disabled = true;
                int cancellableBookingCount =
                    bookingDetailsList.FindAll(
                        delegate(BookingDetailsEntity bd) { return bd.HotelSearch.CancelPolicy == AppConstants.NON_CANCELLABLE_RESERVATION; }).Count;
                if (totalBooking - cancellableBookingCount <= 1)
                {
                    SelectAllCheck.Visible = false;
                    cnclConfirm.Visible = false;
                }
                else if (cancellableBookingCount >= 1)
                {
                    SelectAllCheck.Visible = false;
                    cnclConfirm.Visible = false;
                }

                for (int count = 0; count < totalBooking; count++)
                {
                    GuestInformationEntity guestInformationEntity = bookingDetailsList[count].GuestInformation;
                    if (guestInformationEntity != null)
                    {
                        string username = string.Empty;
                        if (!string.IsNullOrEmpty(guestInformationEntity.NativeFirstName) && !string.IsNullOrEmpty(guestInformationEntity.NativeLastName))
                        {
                            username = Utility.GetNameWithTitle(guestInformationEntity.Title,
                                                                       guestInformationEntity.NativeFirstName,
                                                                       guestInformationEntity.NativeLastName);
                        }
                        else
                        {
                            username = Utility.GetNameWithTitle(guestInformationEntity.Title,
                                                                       guestInformationEntity.FirstName,
                                                                       guestInformationEntity.LastName);
                        }
                        string legNumber = bookingDetailsList[count].LegNumber;

                        string reservationMessage =
                            WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/reservationMessage");
                        reservationMessage = bookingDetailsList[count].ReservationNumber + AppConstants.HYPHEN +
                                             legNumber;
                        bool isEarlyBooking = bookingDetailsList[count].HotelSearch.CancelPolicy ==
                                              AppConstants.NON_CANCELLABLE_RESERVATION
                                                  ? true
                                                  : false;
                        DisplayRoomInformation(guestInformationEntity, reservationMessage, username, count + 1,
                                               isEarlyBooking);
                    }
                }
            }
        }

        /// <summary>
        /// DisplayRoomInformation
        /// </summary>
        /// <param name="guestInformationEntity"></param>
        /// <param name="reservationMessage"></param>
        private void DisplayRoomInformation(GuestInformationEntity guestInformationEntity,
                                            string reservationMessage, string userName, int numberOfBookings,
                                            bool isEarlyBooking)
        {
            switch (numberOfBookings)
            {
                case AppConstants.MAX_BOOKING_ROOMS_ALLOWED:
                    {
                        divRoom4.Visible = true;
                        divLegNumberRoom4.Value = guestInformationEntity.LegNumber;
                        lblReservationNumberRoom4.InnerHtml = reservationMessage;
                        if (guestInformationEntity.EmailDetails != null)
                        {
                            lblEmailRoom4.InnerHtml = Utility.FormateFieldsToDisplayEmail(guestInformationEntity.EmailDetails.EmailID);
                        }
                        SetPhoneCodesDropDown(ddlPhoneCodeRoom4);
                        if (guestInformationEntity.Mobile != null)
                        {
                            string phoneNumber = string.Empty;
                            string phoneCode = Utility.GetPhoneCodesNumber(guestInformationEntity.Mobile.Number,
                                                                           out phoneNumber);
                            txtMobileNumberRoom4.Value = phoneNumber;

                            SetSelectedPhoneCode(ddlPhoneCodeRoom4, phoneCode);
                        }
                        if (!string.IsNullOrEmpty(userName))
                        {
                            lblContactPersonRoom4.InnerHtml = userName;
                        }
                        if (isEarlyBooking)
                        {
                            Room4chkCancel.Enabled = false;
                            txtMobileNumberRoom4.Disabled = true;
                            ddlPhoneCodeRoom4.Enabled = false;
                            chkSMSRoom4.Enabled = false;
                            divRoom4.Attributes.Add("class", string.Format("{0} {1}", divRoom4.Attributes["class"], AppConstants.DisableMeCSSClass));
                        }
                        if ((Reservation2SessionWrapper.GuaranteeInfoRate != null && Reservation2SessionWrapper.GuaranteeInfoRate.Count > 1) &&
                            (BookingEngineSessionWrapper.ActiveBookingDetails.Count > 1))
                        {
                            DisplayGuaranteeInformation(3);
                        }

                        break;
                    }
                case (AppConstants.MAX_BOOKING_ROOMS_ALLOWED - 1):
                    {
                        divRoom3.Visible = true;
                        divLegNumberRoom3.Value = guestInformationEntity.LegNumber;
                        lblReservationNumberRoom3.InnerHtml = reservationMessage;
                        if (guestInformationEntity.EmailDetails != null)
                        {
                            lblEmailRoom3.InnerHtml = Utility.FormateFieldsToDisplayEmail(guestInformationEntity.EmailDetails.EmailID);
                        }
                        SetPhoneCodesDropDown(ddlPhoneCodeRoom3);
                        if (guestInformationEntity.Mobile != null)
                        {
                            string phoneNumber = string.Empty;
                            string phoneCode = Utility.GetPhoneCodesNumber(guestInformationEntity.Mobile.Number,
                                                                           out phoneNumber);
                            txtMobileNumberRoom3.Value = phoneNumber;

                            SetSelectedPhoneCode(ddlPhoneCodeRoom3, phoneCode);
                        }
                        if (!string.IsNullOrEmpty(userName))
                        {
                            lblContactPersonRoom3.InnerHtml = userName;
                        }
                        if (isEarlyBooking)
                        {
                            Room3chkCancel.Enabled = false;
                            txtMobileNumberRoom3.Disabled = true;
                            ddlPhoneCodeRoom3.Enabled = false;
                            chkSMSRoom3.Enabled = false;
                            divRoom3.Attributes.Add("class", string.Format("{0} {1}", divRoom3.Attributes["class"], AppConstants.DisableMeCSSClass));
                        }

                        if ((Reservation2SessionWrapper.GuaranteeInfoRate != null && Reservation2SessionWrapper.GuaranteeInfoRate.Count > 1) &&
                            (BookingEngineSessionWrapper.ActiveBookingDetails.Count > 1))
                        {
                            DisplayGuaranteeInformation(2);
                        }

                        break;
                    }
                case (AppConstants.MAX_BOOKING_ROOMS_ALLOWED - 2):
                    {
                        divRoom2.Visible = true;
                        divLegNumberRoom2.Value = guestInformationEntity.LegNumber;
                        lblReservationNumberRoom2.InnerHtml = reservationMessage;
                        if (guestInformationEntity.EmailDetails != null)
                        {
                            lblEmailRoom2.InnerHtml = Utility.FormateFieldsToDisplayEmail(guestInformationEntity.EmailDetails.EmailID);
                        }
                        SetPhoneCodesDropDown(ddlPhoneCodeRoom2);
                        if (guestInformationEntity.Mobile != null)
                        {
                            string phoneNumber = string.Empty;
                            string phoneCode = Utility.GetPhoneCodesNumber(guestInformationEntity.Mobile.Number,
                                                                           out phoneNumber);
                            txtMobileNumberRoom2.Value = phoneNumber;

                            SetSelectedPhoneCode(ddlPhoneCodeRoom2, phoneCode);
                        }
                        if (!string.IsNullOrEmpty(userName))
                        {
                            lblContactPersonRoom2.InnerHtml = userName;
                        }
                        if (isEarlyBooking)
                        {
                            Room2chkCancel.Enabled = false;
                            txtMobileNumberRoom2.Disabled = true;
                            ddlPhoneCodeRoom2.Enabled = false;
                            chkSMSRoom2.Enabled = false;
                            divRoom2.Attributes.Add("class", string.Format("{0} {1}", divRoom2.Attributes["class"], AppConstants.DisableMeCSSClass));
                        }

                        if ((Reservation2SessionWrapper.GuaranteeInfoRate != null && Reservation2SessionWrapper.GuaranteeInfoRate.Count > 1) &&
                            (BookingEngineSessionWrapper.ActiveBookingDetails.Count > 1))
                        {
                            DisplayGuaranteeInformation(1);
                        }
                        break;
                    }
                case (AppConstants.MAX_BOOKING_ROOMS_ALLOWED - 3):
                    {
                        divRoom1.Visible = true;
                        divLegNumberRoom1.Value = guestInformationEntity.LegNumber;
                        lblReservationNumberRoom1.InnerHtml = reservationMessage;
                        if (guestInformationEntity.EmailDetails != null)
                        {
                            lblEmailRoom1.InnerHtml = guestInformationEntity.EmailDetails.EmailID;
                                //Utility.FormateFieldsToDisplayEmail(
                                 //   guestInformationEntity.EmailDetails.EmailID.ToString());
                        }
                        SetPhoneCodesDropDown(ddlPhoneCodeRoom1);
                        if (guestInformationEntity.Mobile != null)
                        {
                            string phoneNumber = string.Empty;
                            string phoneCode = Utility.GetPhoneCodesNumber(guestInformationEntity.Mobile.Number,
                                                                           out phoneNumber);
                            txtMobileNumberRoom1.Value = phoneNumber;

                            SetSelectedPhoneCode(ddlPhoneCodeRoom1, phoneCode);
                        }
                        if (!string.IsNullOrEmpty(userName))
                        {
                            lblContactPersonRoom1.InnerHtml = userName;
                        }
                        if (isEarlyBooking)
                        {
                            Room1chkCancel.Enabled = false;
                            txtMobileNumberRoom1.Disabled = true;
                            ddlPhoneCodeRoom1.Enabled = false;
                            chkSMSRoom1.Enabled = false;
                            divRoom1.Attributes.Add("class", string.Format("{0} {1}", divRoom1.Attributes["class"], AppConstants.DisableMeCSSClass));
                        }

                        if ((Reservation2SessionWrapper.GuaranteeInfoRate != null && Reservation2SessionWrapper.GuaranteeInfoRate.Count > 1) &&
                            (BookingEngineSessionWrapper.ActiveBookingDetails.Count > 1))
                        {
                            DisplayGuaranteeInformation(0);
                        }

                        break;
                    }
            }
        }

        private void DisplayGuaranteeInformation(int roomNo)
        {
            GuaranteeInfo guaranteeInfo = FindControl("GuaranteeInformation" + roomNo) as GuaranteeInfo;
            if (guaranteeInfo != null)
            {
                if (isCommonGuarantee)
                {
                    roomNo = 0;
                }

                guaranteeInfo.Visible = true;
                guaranteeInfo.FindControl("HideUnhideGuranteeDiv").Visible = true;

                guaranteeInfo.DataSource = BookingEngineSessionWrapper.ActiveBookingDetails[roomNo].GuestInformation;
                guaranteeInfo.RoomIdentifier = 0;

                string rateCode = BookingEngineSessionWrapper.ActiveBookingDetails[roomNo].HotelRoomRate.RatePlanCode;
                guaranteeInfo.DisplayHoldOptionIfRequired(Reservation2SessionWrapper.GuaranteeInfoRate[rateCode], false);
                guaranteeInfo.DisplayControls(
                    Convert.ToString(
                        BookingEngineSessionWrapper.ActiveBookingDetails[roomNo].GuestInformation.GuranteeInformation.GuranteeType));
            }
        }

        /// <summary>
        /// This method will populate only the Reservation information container.
        /// </summary>
        private void PopulateUI()
        {
            List<BookingDetailsEntity> bookingDetailsList = BookingEngineSessionWrapper.ActiveBookingDetails;

            if (bookingDetailsList != null && bookingDetailsList.Count > 0)
            {
                BookingDetailsEntity bookingDetails = bookingDetailsList[0];
                if (bookingDetails != null)
                {
                    HotelSearchEntity hotelSearchEntity = bookingDetails.HotelSearch;
                    HotelRoomRateEntity hotelRoomRateEntity = bookingDetails.HotelRoomRate;
                    GuestInformationEntity guestInfoEntity = bookingDetails.GuestInformation;
                    if ((hotelSearchEntity != null) && (hotelRoomRateEntity != null) && (guestInfoEntity != null))
                    {
                        HotelDestination hotelDestination = null;
                        if ((hotelSearchEntity != null) && (hotelRoomRateEntity != null))
                        {
                            AvailabilityController availabilityController = new AvailabilityController();
                            hotelDestination =
                                availabilityController.GetHotelDestinationEntity(hotelSearchEntity.SelectedHotelCode);
                            if (hotelDestination != null)
                            {
                                ReservationInfo.CityOrHotelName = hotelDestination.Name;
                                ReservationInfo.HotelUrl = hotelDestination.HotelPageURL;
                            }
                            RoomCategory roomCategory = RoomRateUtil.GetRoomCategory(hotelRoomRateEntity.RoomtypeCode);
                            if (roomCategory != null)
                            {
                                ReservationInfo.NoOfRooms = ReservationInfo.GetTotalBookedRooms().ToString();
                            }
                            ReservationInfo.ArrivalDate = hotelSearchEntity.ArrivalDate;
                            ReservationInfo.DepartureDate = hotelSearchEntity.DepartureDate;
                            ReservationInfo.NoOfAdults = Utility.GetTotalAdults();
                            ReservationInfo.NoOfChildren = Utility.GetTotalChildren();
                            ReservationInfo.NoOfNights = hotelSearchEntity.NoOfNights;

                            string username = Utility.GetNameWithTitle(guestInfoEntity.Title, guestInfoEntity.FirstName,
                                                                       guestInfoEntity.LastName);
                            string reservationMessage = string.Empty;
                            if (!BookingEngineSessionWrapper.IsModifyingLegBooking)
                            {
                                reservationMessage =
                                    WebUtil.GetTranslatedText(
                                        TranslatedTextConstansts.YOUR_BOOKING_RESERVATION_MESSAGE_WITHOUT_NUMBER);
                                reservationMessage = string.Format(reservationMessage, bookingDetails.ReservationNumber);
                            }
                            else
                            {
                                reservationMessage =
                                    WebUtil.GetTranslatedText(TranslatedTextConstansts.YOUR_BOOKING_RESERVATION_MESSAGE);
                                string resNumber = bookingDetails.ReservationNumber + AppConstants.HYPHEN +
                                                   bookingDetails.LegNumber;
                                reservationMessage = string.Format(reservationMessage, username, resNumber);
                            }
                            ReservationInfo.IsPrevBooking = false;
                            string hotelCountryCode = hotelSearchEntity.HotelCountryCode;
                            TimeSpan timeSpan = hotelSearchEntity.ArrivalDate.Subtract(hotelSearchEntity.DepartureDate);
                            double basePoints = 0;
                            double totalPoints = 0;

                            basePoints = hotelRoomRateEntity.Points;
                            if (hotelRoomRateEntity.TotalRate != null)
                            {
                                totalPoints = hotelRoomRateEntity.TotalRate.Rate;
                            }

                            string ratePlanCode = hotelRoomRateEntity.RatePlanCode;
                            string selectedRateCategoryName = string.Empty;
                            if ((ratePlanCode != null) && (ratePlanCode != string.Empty))
                            {
                                Rate rate = RoomRateUtil.GetRate(ratePlanCode);
                                if (rate != null)
                                {
                                    selectedRateCategoryName = rate.RateCategoryName;
                                }
                            }

                            string baseRateString = string.Empty;
                            if (hotelSearchEntity.SearchingType == SearchType.REDEMPTION)
                            {
                                baseRateString = Utility.GetRoomRateString(selectedRateCategoryName,
                                                                           hotelRoomRateEntity.Rate, basePoints,
                                                                           hotelSearchEntity.SearchingType,
                                                                           hotelSearchEntity.ArrivalDate.Year,
                                                                           hotelCountryCode,
                                                                           hotelSearchEntity.NoOfNights,
                                                                           hotelSearchEntity.RoomsPerNight);
                            }
                            else if ((Utility.IsBlockCodeBooking)
                                     && (string.IsNullOrEmpty(hotelRoomRateEntity.RatePlanCode))
                                     && (!string.IsNullOrEmpty(hotelRoomRateEntity.RoomtypeCode)))
                            {
                                baseRateString = Utility.GetRoomRateString(hotelRoomRateEntity.Rate);
                            }
                            else
                            {
                                baseRateString = Utility.GetRoomRateString(selectedRateCategoryName,
                                                                           hotelRoomRateEntity.Rate,
                                                                           basePoints, hotelSearchEntity.SearchingType,
                                                                           hotelSearchEntity.ArrivalDate.Year,
                                                                           hotelCountryCode, AppConstants.PER_NIGHT,
                                                                           AppConstants.PER_ROOM);
                            }
							if (BookingEngineSessionWrapper.HideARBPrice)
                            {
                                baseRateString = Utility.GetPrepaidString();
                            }
                            double dblTotalRate = Utility.CalculateTotalRateInCaseOfModifyFlow();
                            string totalRateString = string.Empty;
                            if (hotelRoomRateEntity.TotalRate != null)
                            {
                                RateEntity tempRateEntity = new RateEntity(dblTotalRate,
                                                                           hotelRoomRateEntity.TotalRate.CurrencyCode);
                                totalRateString = Utility.GetRoomRateString(null, tempRateEntity, totalPoints,
                                                                            hotelSearchEntity.SearchingType,
                                                                            hotelSearchEntity.ArrivalDate.Year,
                                                                            hotelCountryCode,
                                                                            hotelSearchEntity.NoOfNights,
                                                                            hotelSearchEntity.RoomsPerNight);
                            }
                            if (hotelSearchEntity.SearchingType == SearchType.REDEMPTION)
                            {
                                totalRateString = Utility.GetRoomRateString(null, null, basePoints,
                                                                            hotelSearchEntity.SearchingType,
                                                                            hotelSearchEntity.ArrivalDate.Year,
                                                                            hotelCountryCode,
                                                                            hotelSearchEntity.NoOfNights,
                                                                            hotelSearchEntity.RoomsPerNight);
                            }
                            ReservationInfo.SetTotalRoomRate(totalRateString);
                        }
                    }
                }
                ReservationInfo.SetRoomDetails(bookingDetailsList);
            }
        }

        /// <summary>
        /// Event Handler for Cancel Booking button
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="args">
        /// Argument for the Event
        /// </param>
        protected void btnCancelBooking_Click(object sender, EventArgs args)
        {
            try
            {
                if (transactionCount.Value == ReservationNumberSessionWrapper.TransactionCount.ToString())
                {
                    List<BookingDetailsEntity> bookingDetailsList = BookingEngineSessionWrapper.ActiveBookingDetails;
                    List<CancelDetailsEntity> cancelDetailsList = new List<CancelDetailsEntity>();
                    if (bookingDetailsList != null && bookingDetailsList.Count > 0)
                    {
                        BookingDetailsEntity bookingEntity = bookingDetailsList[0];
                        if (divRoom1.Visible && Room1chkCancel.Checked)
                        {
                            CancelDetailsEntity cancelEntity = new CancelDetailsEntity();
                            cancelEntity.ReservationNumber = bookingEntity.ReservationNumber;
                            cancelEntity.LegNumber = divLegNumberRoom1.Value;
                            cancelEntity.HotelCode = bookingEntity.HotelSearch.SelectedHotelCode;
                            cancelEntity.ChainCode = AppConstants.CHAIN_CODE;
                            cancelEntity.CancelType = CancelTermType.Cancel;
                            if (chkSMSRoom1.Checked)
                            {
                                cancelEntity.SMSNumber = ddlPhoneCodeRoom1.SelectedValue + txtMobileNumberRoom1.Value;
                            }
                            cancelDetailsList.Add(cancelEntity);
                        }
                        if (divRoom2.Visible && Room2chkCancel.Checked)
                        {
                            CancelDetailsEntity cancelEntity = new CancelDetailsEntity();
                            cancelEntity.ReservationNumber = bookingEntity.ReservationNumber;
                            cancelEntity.LegNumber = divLegNumberRoom2.Value;
                            cancelEntity.HotelCode = bookingEntity.HotelSearch.SelectedHotelCode;
                            cancelEntity.ChainCode = AppConstants.CHAIN_CODE;
                            cancelEntity.CancelType = CancelTermType.Cancel;
                            if (chkSMSRoom2.Checked)
                            {
                                cancelEntity.SMSNumber = ddlPhoneCodeRoom2.SelectedValue + txtMobileNumberRoom2.Value;
                            }
                            cancelDetailsList.Add(cancelEntity);
                        }
                        if (divRoom3.Visible && Room3chkCancel.Checked)
                        {
                            CancelDetailsEntity cancelEntity = new CancelDetailsEntity();
                            cancelEntity.ReservationNumber = bookingEntity.ReservationNumber;
                            cancelEntity.LegNumber = divLegNumberRoom3.Value;
                            cancelEntity.HotelCode = bookingEntity.HotelSearch.SelectedHotelCode;
                            cancelEntity.ChainCode = AppConstants.CHAIN_CODE;
                            cancelEntity.CancelType = CancelTermType.Cancel;
                            if (chkSMSRoom3.Checked)
                            {
                                cancelEntity.SMSNumber = ddlPhoneCodeRoom3.SelectedValue + txtMobileNumberRoom3.Value;
                            }
                            cancelDetailsList.Add(cancelEntity);
                        }
                        if (divRoom4.Visible && Room4chkCancel.Checked)
                        {
                            CancelDetailsEntity cancelEntity = new CancelDetailsEntity();
                            cancelEntity.ReservationNumber = bookingEntity.ReservationNumber;
                            cancelEntity.LegNumber = divLegNumberRoom4.Value;
                            cancelEntity.HotelCode = bookingEntity.HotelSearch.SelectedHotelCode;
                            cancelEntity.ChainCode = AppConstants.CHAIN_CODE;
                            cancelEntity.CancelType = CancelTermType.Cancel;
                            if (chkSMSRoom4.Checked)
                            {
                                cancelEntity.SMSNumber = ddlPhoneCodeRoom4.SelectedValue + txtMobileNumberRoom4.Value;
                            }
                            cancelDetailsList.Add(cancelEntity);
                        }
                    }
                    else
                    {
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH),
                                          false);
                    }
                   ReservationController reservationCtrl = new ReservationController();
                    int totalBooking = cancelDetailsList.Count;
                    for (int cancelCount = 0; cancelCount < totalBooking; cancelCount++)
                    {
                        CancelDetailsEntity cancelEntity = cancelDetailsList[cancelCount];
                        reservationCtrl.CancelBooking(ref cancelEntity);

                        foreach (BookingDetailsEntity booking in bookingDetailsList)
                        {
                            if (booking.LegNumber == cancelEntity.LegNumber)
                            {
                                booking.IsCancelledByUser = true;
                            }
                        }
                    }

                    BookingEngineSessionWrapper.AllCancelledDetails = cancelDetailsList;

                    FindtheActiveBookings();
                    UserNavTracker.TrackAction(this, "CancelBooking");
                    SentCancelConfirmationToGuest(cancelDetailsList);
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.CANCELLED_BOOKING), false);
                }
                else
                {
                    cancelError.Visible = true;
                    divSelectRatePageErrorMessage.Attributes.Add("style", "display:none");
                    operaErrorMsg.InnerHtml =
                        WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_ALREADY_CANCELED);
                }
            }
            catch (OWSException owsException)
            {
                AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);
                switch (owsException.ErrCode)
                {
                    case OWSExceptionConstants.BOOKING_LOCKED_BY_STAFF_MEMBER:
                        {
                            cancelError.Visible = true;
                            divSelectRatePageErrorMessage.Attributes.Add("style", "display:none");
                            operaErrorMsg.InnerHtml =
                                WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_LOCKED_BY_STAFF_MEMBER);
                            break;
                        }
                    case OWSExceptionConstants.BOOKING_ALREADY_CANCELED:
                        {
                            cancelError.Visible = true;
                            divSelectRatePageErrorMessage.Attributes.Add("style", "display:none");
                            operaErrorMsg.InnerHtml =
                                WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_ALREADY_CANCELED);
                            break;
                        }
                    case OWSExceptionConstants.CANCELLATION_TOO_LATE:
                        {
                            cancelError.Visible = true;
                            divSelectRatePageErrorMessage.Attributes.Add("style", "display:none");
                            operaErrorMsg.InnerHtml =
                                WebUtil.GetTranslatedText(TranslatedTextConstansts.CANCALLATION_TOO_LATE);
                            break;
                        }
                    default:
                        {
                            cancelError.Visible = true;
                            divSelectRatePageErrorMessage.Attributes.Add("style", "display:none");
                            operaErrorMsg.InnerHtml = owsException.ErrMessage;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }
        }

        private void FindtheActiveBookings()
        {
            List<BookingDetailsEntity> bookingList = BookingEngineSessionWrapper.AllBookingDetails;
            List<CancelDetailsEntity> cancelledList = BookingEngineSessionWrapper.AllCancelledDetails;

            List<BookingDetailsEntity> activeBooking = new List<BookingDetailsEntity>();
        }

        /// <summary>
        /// Event Handler for No Cancellation button click
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="args">
        /// Event Arguments
        /// </param>
        protected void btnNo_Click(object sender, EventArgs args)
        {
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_CHANGE_DATES), false);
        }

        #endregion Page Events

        #region Private Methods

        /// <summary>
        /// Sent Cancel Booking Confirmation to Guest
        /// </summary>
        private void SentCancelConfirmationToGuest(List<CancelDetailsEntity> cancelEntityList)
        {
            SendMailConfirmationToGuestsUsingHTMLString(cancelEntityList);
            SentSMSCancellationConfirmation(cancelEntityList);
        }

        /// <summary>
        /// Send SMS Cancellation Confirmation
        /// </summary>
        private void SentSMSCancellationConfirmation(List<CancelDetailsEntity> cancelList)
        {
            if (cancelList != null && cancelList.Count > 0)
            {
                int totalCancel = cancelList.Count;
                for (int count = 0; count < totalCancel; count++)
                {
                    CancelDetailsEntity cancelEntity = null;

                    try
                    {
                        cancelEntity = cancelList[count];
                        if ((cancelEntity != null) && (!string.IsNullOrEmpty(cancelEntity.SMSNumber)) &&
                            (cancelEntity.CancelationStatus))
                        {
                            BookingDetailsEntity bookingEntity = Utility.GetBookingDetail(cancelEntity.LegNumber);
                            if (bookingEntity != null)
                            {
                                Dictionary<string, string> smsCancelBookingMap =
                                    CollectSMSBookingCancellationDetails(bookingEntity);
                                smsCancelBookingMap[CommunicationTemplateConstants.BOOKING_CANCALLATION_NUMBER] =
                                    cancelEntity.CancelNumber;
                                SMSEntity smsEntity = CommunicationUtility.GetBookingCancellationSMS(smsCancelBookingMap);
                                if (smsEntity != null)
                                {
                                    smsEntity.PhoneNumber = cancelEntity.SMSNumber;
                                    CommunicationService.SendSMS(smsEntity);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        AppLogger.LogFatalException(ex,
                                                    "Failed to send cancel confirmation SMS to the Guest for booking: " +
                                                    cancelEntity.ReservationNumber + AppConstants.HYPHEN +
                                                    cancelEntity.LegNumber);
                    }
                }
            }
        }

        /// <summary>
        /// Sends the mail confirmation to guests using HTML string.
        /// </summary>
        /// <param name="cancelList">The cancel list.</param>
        private void SendMailConfirmationToGuestsUsingHTMLString(List<CancelDetailsEntity> cancelList)
        {
            string bookingType = PageIdentifier.CancelBookingPageIdentifier;
            ErrorsSessionWrapper.ClearFailedEmailRecipient();
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;

            string templateFileName = string.Empty;
            if (cancelList != null)
            {
                bool isSentToMainGuest = false;
                bool isSentPDF = false;
                bool isSentPDFforMain = false;
                List<EmailEntity> emails = new List<EmailEntity>();
                List<string> reservations = new List<string>();
                for (int count = 0; count < cancelList.Count; count++)
                {
                    CancelDetailsEntity cancelEntity = cancelList[count];
                    int currentLegNumber = Convert.ToInt32(cancelEntity.LegNumber) - 1;
                    string htmlStringForEmail = string.Empty;
                    string htmlStringforPDF = string.Empty;
                    EmailEntity emailObject = null;
                    string cancelNumber = string.Empty;
                    
                    if (!isSentToMainGuest)
                    {
                        GenerateConfirmationPageHtmlString(
                            @"/templates/Scanweb/Pages/ConfirmationEmailPage.aspx?AllROOMSTOBESHOWN=true&&command=CancelPageprint&BOOKINGTYPE=" +
                            bookingType, ref htmlStringForEmail);
                        GenerateConfirmationPageHtmlString(
                            @"/templates/Scanweb/Pages/ModifyCancelBooking/CancellBookingPrinterFreindly.aspx?isCancel=true&AllROOMSTOBESHOWN=true&isPDF=false&command=CancelPageprint",
                            ref htmlStringforPDF);

                        if (string.IsNullOrEmpty(htmlStringforPDF))
                        {
                            GenerateConfirmationPageHtmlString(
                                @"/templates/Scanweb/Pages/ModifyCancelBooking/CancellBookingPrinterFreindly.aspx?isCancel=true&AllROOMSTOBESHOWN=true&isPDF=false&command=CancelPageprint",
                                ref htmlStringforPDF);
                        }
                        templateFileName =
                            Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
                        templateFileName = templateFileName.Remove(templateFileName.IndexOf("\\bin"));


                        string alterhtmlStringForPDF = htmlStringforPDF.Replace("/Templates",
                                                                                templateFileName + "/Templates");

                        SendMailForMainGuest("1", htmlStringForEmail + "^" + alterhtmlStringForPDF, ref emailObject,
                                             ref cancelNumber);
                        emails.Add(emailObject);
                        reservations.Add(cancelNumber);
                        isSentToMainGuest = true;
                    }
                    if (cancelList[count].LegNumber != 1.ToString())
                    {
                        htmlStringforPDF = string.Empty;
                        string alterhtmlStringForLegPDF = string.Empty;
                        GenerateConfirmationPageHtmlString(
                            @"/templates/Scanweb/Pages/ConfirmationEmailPage.aspx?AllROOMSTOBESHOWN=false&&command=CancelPageprint&ROOMNUMBERTOBEDISPLAYED=" +
                            currentLegNumber + "&BOOKINGTYPE=" + bookingType, ref htmlStringForEmail);
                        GenerateConfirmationPageHtmlString(
                            @"/templates/Scanweb/Pages/ModifyCancelBooking/CancellBookingPrinterFreindly.aspx?isCancel=true&AllROOMSTOBESHOWN=false&isPDF=false&command=CancelPageprint&count=" +
                            count + "&CurrentLegNumber=" + currentLegNumber, ref htmlStringforPDF);
                        if (string.IsNullOrEmpty(htmlStringforPDF))
                        {
                            GenerateConfirmationPageHtmlString(
                                @"/templates/Scanweb/Pages/ModifyCancelBooking/CancellBookingPrinterFreindly.aspx?isCancel=true&AllROOMSTOBESHOWN=false&isPDF=false&command=CancelPageprint&count=" +
                                count + "&CurrentLegNumber=" + currentLegNumber, ref htmlStringforPDF);
                        }
                        templateFileName =
                            Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
                        templateFileName = templateFileName.Remove(templateFileName.IndexOf("\\bin"));


                        alterhtmlStringForLegPDF = htmlStringforPDF.Replace("/Templates",
                                                                            templateFileName + "/Templates");

                        SendMailForEachCancelledRoom(ref emailObject, count, cancelList,
                                                     htmlStringForEmail + "^" + alterhtmlStringForLegPDF,
                                                     ref cancelNumber);
                        emails.Add(emailObject);
                        reservations.Add(cancelNumber);
                    }
                }
                SendMail(emails, reservations);
            }
        }


        /// <summary>
        /// Sends the mail for each cancelled room.
        /// </summary>
        /// <param name="count">The count.</param>
        private void SendMailForEachCancelledRoom(ref EmailEntity emailObject, int count,
                                                  List<CancelDetailsEntity> cancelList, string htmlStringForEmail,
                                                  ref string cancelNumber)
        {
            CancelDetailsEntity cancelEntity = null;
            string[] emailTextAndPDF = null;
            string bookingType = PageIdentifier.CancelBookingPageIdentifier;
            try
            {
                cancelEntity = cancelList[count];

                if (cancelEntity != null)
                {
                    if (cancelEntity.CancelationStatus)
                    {
                        BookingDetailsEntity bookingEntity = Utility.GetBookingDetail(cancelEntity.LegNumber);
                        if (bookingEntity != null)
                        {
                            EmailEntity emailEntity = new EmailEntity();
                            if (emailEntity != null)
                            {
                                GuestInformationEntity guestInfoEntity = bookingEntity.GuestInformation;
                                if (guestInfoEntity != null && guestInfoEntity.EmailDetails != null)
                                {
                                    string[] emailList = null;
                                    if (guestInfoEntity.IsRewardNightGift &&
                                        bookingEntity.RewardNightGiftGuestInfo != null)
                                    {
                                        emailList = new string[]
                                                        {
                                                            guestInfoEntity.EmailDetails.EmailID,
                                                            bookingEntity.RewardNightGiftGuestInfo.EmailAddressFgpUser
                                                        };
                                    }
                                    else
                                    {
                                        emailList = new string[] {guestInfoEntity.EmailDetails.EmailID};
                                    }
                                    emailEntity.Recipient = emailList;
                                    emailEntity.Subject =
                                        WebUtil.GetTranslatedText("/bookingengine/booking/cancelconfirmation/subject");
                                    emailEntity.Sender =
                                        WebUtil.GetTranslatedText("/bookingengine/booking/cancelconfirmation/fromEmail");
                                    string reservationNumberForCancel = bookingEntity.ReservationNumber +
                                                                        AppConstants.HYPHEN + bookingEntity.LegNumber;
                                    emailTextAndPDF = htmlStringForEmail.Split('^');
                                    if (emailTextAndPDF != null)
                                    {
                                        if (emailTextAndPDF[0] != null)
                                            emailEntity.Body = emailTextAndPDF[0];
                                        if (emailTextAndPDF[1] != null)
                                            emailEntity.TextEmailBody = emailTextAndPDF[1];
                                    }
                                    emailObject = emailEntity;
                                    cancelNumber = reservationNumberForCancel;
                                }
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Failed to send cancel confirmation email to the Guest for booking: " +
                                            cancelEntity.ReservationNumber + AppConstants.HYPHEN +
                                            cancelEntity.LegNumber);
            }
        }

        /// <summary>
        /// SendEmail And CreatePDF for all Guest.
        /// </summary>
        /// <param name="emails">emails list</param>
        /// <param name="reservations">reservations list</param>
        /// <returns></returns>
        private void SendEmailAndCreatePDF(List<EmailEntity> emails, List<string> reservations)
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendEmailAndCreatePDF() Start Time");
            int counter = 0;
            foreach (EmailEntity email in emails)
            {
                bool sendStatus = CommunicationService.SendMail(email,
                                                                reservations[counter]);

                if (!sendStatus)
                {
                    if (email.Recipient != null)
                    {
                        if (email.Recipient.Length > 0)
                        {
                            for (int i = 0; i < email.Recipient.Length; i++)
                            {
                                ErrorsSessionWrapper.AddFailedEmailRecipient(email.Recipient[i].ToString());
                            }
                        }
                    }
                }
                counter++;
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendEmailAndCreatePDF() End Time");
        }

        /// <summary>
        /// This method will create PDF files asynchronously.
        /// </summary>
        /// <param name="emails">emails list</param>
        /// <param name="reservations">reservations list</param>
        public void SendMail(List<EmailEntity> emails, List<string> reservations)
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendMail() Start Time");
            CreatePDF createPDFDeligate = new CreatePDF(SendEmailAndCreatePDF);

            IAsyncResult result = createPDFDeligate.BeginInvoke(emails, reservations, null, null);

            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendMail() End Time");
        }

        /// <summary>
        /// Sends the mail for each cancelled room.
        /// </summary>
        /// <param name="count">The count.</param>
        private void SendMailForMainGuest(string legNumber, string htmlStringForEmail, ref EmailEntity emailObject,
                                          ref string cancelNumber)
        {
            string[] emailTextAndPDF = null;
            string bookingType = PageIdentifier.CancelBookingPageIdentifier;
            BookingDetailsEntity bookingEntity = Utility.GetBookingDetail(legNumber);

            if (bookingEntity == null)
            {
                if (BookingEngineSessionWrapper.IsModifyingLegBooking)
                {
                    ReservationController reservationController = new ReservationController();
                    bookingEntity =
                        reservationController.FetchBooking(BookingEngineSessionWrapper.AllBookingDetails[0].ReservationNumber,
                                                           legNumber);
                }
            }
            try
            {
                if (bookingEntity != null)
                {
                    EmailEntity emailEntity = new EmailEntity();
                    if (emailEntity != null)
                    {
                        GuestInformationEntity guestInfoEntity = bookingEntity.GuestInformation;
                        if (guestInfoEntity != null && guestInfoEntity.EmailDetails != null)
                        {
                            string[] emailList = null;
                            if (guestInfoEntity.IsRewardNightGift && bookingEntity.RewardNightGiftGuestInfo != null)
                            {
                                emailList = new string[]
                                                {
                                                    guestInfoEntity.EmailDetails.EmailID,
                                                    bookingEntity.RewardNightGiftGuestInfo.EmailAddressFgpUser
                                                };
                            }
                            else
                            {
                                emailList = new string[] {guestInfoEntity.EmailDetails.EmailID};
                            }
                            emailEntity.Recipient = emailList;
                            emailEntity.Subject =
                                WebUtil.GetTranslatedText("/bookingengine/booking/cancelconfirmation/subject");
                            emailEntity.Sender =
                                WebUtil.GetTranslatedText("/bookingengine/booking/cancelconfirmation/fromEmail");
                            string reservationNumberForCancel = bookingEntity.ReservationNumber + AppConstants.HYPHEN +
                                                                bookingEntity.LegNumber;
                            emailTextAndPDF = htmlStringForEmail.Split('^');
                            if (emailTextAndPDF != null)
                            {
                                if (emailTextAndPDF[0] != null)
                                    emailEntity.Body = emailTextAndPDF[0];
                                if (emailTextAndPDF[1] != null)
                                    emailEntity.TextEmailBody = emailTextAndPDF[1];
                            }
                            emailObject = emailEntity;
                            cancelNumber = reservationNumberForCancel;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Failed to send cancel confirmation email to the Guest for booking: " +
                                            bookingEntity.ReservationNumber + AppConstants.HYPHEN +
                                            bookingEntity.LegNumber);
            }
        }

        #region gerenarting html preview of Confirmation page

        /// <summary>
        /// Generates the confirmation page HTML string.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="htmlString">The HTML string.</param>
        private void GenerateConfirmationPageHtmlString(string url, ref string htmlString)
        {
            #region Very IMP code To render the preview page

            try
            {
                StringWriter sw = new StringWriter();
                Server.Execute(url, sw);
                htmlString = (sw.ToString()).Trim().Replace("\r\n", "").Replace("\"", "'");
            }
            catch (Exception ex)
            {
            }

            #endregion
        }

        #endregion

        /// <summary>
        /// Collect the SMS Booking Cancellation Details to be displayed in the SMS Template
        /// </summary>
        /// <param name="bookingDetails">
        /// Dictionary of SMS Details
        /// </param>
        private Dictionary<string, string> CollectSMSBookingCancellationDetails(BookingDetailsEntity bookingDetails)
        {
            Dictionary<string, string> cancelBookingMap = null;
            if (bookingDetails != null)
            {
                cancelBookingMap = new Dictionary<string, string>();
               
                GuestInformationEntity guestInfo = bookingDetails.GuestInformation;
                if (guestInfo != null)
                {
                    cancelBookingMap[CommunicationTemplateConstants.TITLE] =
                        Utility.GetNameTitle(guestInfo.Title).Trim();
                    cancelBookingMap[CommunicationTemplateConstants.FIRST_NAME] =
                        (string.IsNullOrEmpty(guestInfo.FirstName)) ? string.Empty : guestInfo.FirstName;
                    cancelBookingMap[CommunicationTemplateConstants.LASTNAME] =
                        (string.IsNullOrEmpty(guestInfo.LastName)) ? string.Empty : guestInfo.LastName;
                }

                HotelSearchEntity hotelSearch = bookingDetails.HotelSearch;
                if (hotelSearch != null)
                {
                    AvailabilityController availabilityController = new AvailabilityController();
                    HotelDestination hotelDestination =
                        availabilityController.GetHotelDestinationEntity(hotelSearch.SelectedHotelCode);
                    cancelBookingMap[CommunicationTemplateConstants.CITY_HOTEL] =
                        (hotelDestination != null) ? (hotelDestination.Name) : string.Empty;
                    cancelBookingMap[CommunicationTemplateConstants.HOTELADDRESS] =
                        (hotelDestination != null) ? (hotelDestination.HotelAddress.StreetAddress) : string.Empty;
                    cancelBookingMap[CommunicationTemplateConstants.HOTELTELEPHONE] =
                        (hotelDestination != null) ? (hotelDestination.Telephone) : string.Empty;

                    IFormatProvider cultureInfo = new CultureInfo("en-US");
                    cancelBookingMap[CommunicationTemplateConstants.ARRIVAL_DATE] =
                        hotelSearch.ArrivalDate.ToString("dd MMM yy", cultureInfo);
                    cancelBookingMap[CommunicationTemplateConstants.DEPARTURE_DATE] =
                        hotelSearch.DepartureDate.ToString("dd MMM yy", cultureInfo);
                }
            }
            return cancelBookingMap;
        }


        /// <summary>
        /// Set the Phone Code
        /// </summary>
        /// <param name="phoneCode"></param>
        public void SetSelectedPhoneCode(DropDownList ddlPhoneCode, string phoneCode)
        {
            if (ddlPhoneCode.Items != null)
            {
                ListItem selectedPhoneCode = ddlPhoneCode.Items.FindByValue(phoneCode);
                if (selectedPhoneCode != null)
                {
                    if (ddlPhoneCode.SelectedItem != null)
                    {
                        ddlPhoneCode.SelectedItem.Selected = false;
                        selectedPhoneCode.Selected = true;
                    }
                }
            }
        }

        /// <summary>
        /// Set the Phone Drop Down
        /// </summary>
        private void SetPhoneCodesDropDown(DropDownList ddlPhoneCode)
        {
            OrderedDictionary phoneCodesMap = DropDownService.GetTelephoneCodes();
            if (phoneCodesMap != null)
            {
                ddlPhoneCode.Items.Clear();
                foreach (string key in phoneCodesMap.Keys)
                {
                    if (key == AppConstants.DEFAULT_VALUE_CONST || key == AppConstants.DEFAULT_SEPARATOR_CONST)
                    {
                        ddlPhoneCode.Items.Add(new ListItem(phoneCodesMap[key].ToString(), key));
                    }
                    else
                    {
                        ddlPhoneCode.Items.Add(new ListItem(key, key));
                    }
                }
            }
            SetSelectedPhoneCode(ddlPhoneCode, AppConstants.DEFAULT_VALUE_CONST);
        }

        #endregion

        #region INavigationTraking Members

        public List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> parameters = new List<KeyValueParam>();
            List<CancelDetailsEntity> cancelDetalsList = BookingEngineSessionWrapper.AllCancelledDetails;

            parameters.Add(new KeyValueParam("Booking Number", cancelDetalsList[0].ReservationNumber));

            parameters.AddRange((from booking in cancelDetalsList
                                 select new KeyValueParam("Leg Number", booking.LegNumber)).ToList<KeyValueParam>());
            parameters.AddRange((from booking in cancelDetalsList
                                 select new KeyValueParam(string.Format("Hote Code for Leg Number{0}", booking.LegNumber), booking.HotelCode)).ToList<KeyValueParam>());
            parameters.AddRange((from booking in cancelDetalsList
                                 select new KeyValueParam(string.Format("SMS Number for Leg Number{0}", booking.LegNumber), booking.SMSNumber)).ToList<KeyValueParam>());
            return parameters;

        }

        #endregion
    }
}