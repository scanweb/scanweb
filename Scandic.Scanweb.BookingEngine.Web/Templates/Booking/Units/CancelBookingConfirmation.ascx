<%@ Control Language="C#" AutoEventWireup="true" Codebehind="CancelBookingConfirmation.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.CancelBookingConfirmation" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx"
    TagName="ReservationInformation" TagPrefix="Booking" %>
    <%@ Register Src="~/Templates/Booking/Units/guaranteeInfo.ascx" TagName="GuaranteeInformation" TagPrefix="Guarantee" %>

<script type="text/javascript" language="javascript">       
    initCancelBookingConfirmation();      
    function UnCheckSelectAll()
    {
        var selectAll = $fn(_endsWith("MasterCheckBox"));
        if(selectAll != null)
            selectAll.checked = false;
    }
    
    function CheckUncheckAll()
    {
        var controlcancel = new Array();
        var controlsc;
        controlcancel = document.getElementsByTagName('input');
        for(controlsc = 0;controlsc < controlcancel.length;controlsc++)
        {
	        if(controlcancel[controlsc].getAttribute('type') == 'checkbox')
            {
                if(controlcancel[controlsc].id != 'undefined' || controlcancel[controlsc].id != '')
                { 
                    var str = controlcancel[controlsc].id.toString();
			        if($fn(_endsWith('MasterCheckBox')).checked == true)
			        {
				        if(str.search('chkCancel') != -1)
				        {
					        //RK: Reservation 2.0 | Defect#461122 | User able to cancel early booking | Cancel booking page
					        $fn(_endsWith(str)).checked = $fn(_endsWith(str)).disabled ? false : true;
				        }
			        }
			        else
			        {
				        if(str.search('chkCancel') != -1)
				        {
					        $fn(_endsWith(str)).checked = false;
				        }
        				
			        }
		        }
		    }
        }
    }
</script>

<!-- Bhavya Cancel Story Start -->
<div id="masterReserMod21" class="masterReserMod21">
	<div id="Header">
	<h2 class="borderH2"><%= WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/CancelButtonText") %></h2>
	</div>
	<div class="txtContnr fltLft">
		<p><%=
                WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelConfirmationText") %></p>
	</div>
	<!--artf1150983: Error message on cancel a reservation -->
	<div class="cancelFlow">
	 <div class="infoAlertBox" id="divSelectRatePageErrorMessage" style="display:none" runat="server">
        <div class="threeCol alertContainer">
            <div class="hd sprite">
                &nbsp;</div>
            <div class="cnt sprite">
				<!--AMS Patch7 | Shalini | artf1285662       Scanweb - Icon does not show correct on the cancellation page -->
                <p class="scansprite nobullet" id="cnclAltert" runat="server">
                </p>
            </div>
            <div class="ft sprite">
            </div>
        </div>
    </div>
     <!--artf1150983: Error message on cancel a reservation -->
    </div>	      
                 
                 <!-- BEGIN New Opera Message -->
                <div class="clearAll"><!-- Clearing Float --></div>
                <div id="cancelError" class="infoAlertBox infoAlertBoxRed" runat="server" visible="false">
                    <div class="threeCol alertContainer">
                        <div class="hd sprite">&#160;</div>
                        <div class="cnt sprite">
                           <span class="alertIconRed">&#160;</span>
                           <div id="Div2" class="descContainer" runat="server">
                                <label class="titleRed"><%= WebUtil.GetTranslatedText("/bookingengine/booking/Rate48HRS/PleaseNote") %></label>
                                <label id="operaErrorMsg" runat="server" class="infoAlertBoxDesc"></label>
                           </div>
                           <div class="clearAll">&#160;</div>
                        </div>
                        <div class="ft sprite">&#160;</div>
                    </div>
                 </div>
                 <!-- END New Opera Message --> 
	        
	        <div class="errorDivClient clear">
	   <!-- <div id="clientErrorDivCB" class="errorText" runat="server"></div> -->						
	        <input type="hidden" id="invalidMobileCodeOrNumber" name="invalidMobileCodeOrNumber" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_mobile_code_or_number") %>'/>
            <input type="hidden" id="invalidCancellation" name="invalidCancellation"  value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalid_cancellation") %>' />
            <input type="hidden" id="invalidNonNumericNumber" name="invalidNonNumericNumber" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_mobile_code_or_number") %>'/>
            <input type="hidden" id="transactionCount" name="transactionCount" runat="server" />
	        </div>
	<div class="clear">&nbsp;</div>
 
	<div class="greyRd">
		<div class="hd">&nbsp;</div>
		<div class="cnt bookingDetailBorder"  onkeypress="return WebForm_FireDefaultButton(event, '<%= btnCancelBooking.ClientID %>')">
			<p id="cnclConfirm" runat="server">
			    <strong><%=
                WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelConfirmationMessage") %></strong>
			    <hr />
			</p>
			
			<div class="detailsColmLeft" id="SelectAllCheck" runat="server">
				<p class="mrgBtm10">				 
                    <input id="MasterCheckBox" name="cnclRm" type="checkbox" class="chkBx chkBxAll" value="" tabindex="10" onclick="CheckUncheckAll();" runat="server"/>
                    <label for="SelectAll"><%= WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/SelectAll") %>
                    </label></p>
				<!--<p class="padLft"><input type="checkbox" class="chkBx" name="smsCnf" id="smsCnf" /><label for="smsCnf">Send SMS confirmation</label></p>
				<p class="padLft"><select><option value="+46">+46</option></select><input type="text" value="Phone no." name="phone"  /></p>-->
            </div>
            <br class="clear" />
            <p class="fltLft" style="padding-top: 3px;">
                <strong>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelIndividualRooms") %>
                </strong>
                <br /><br />
            </p>
            <br class="clear" />
            <!-- Start Room 1 -->
            <div class="roundMe grayBox boxMargBtm" id="divRoom1" runat="server" visible="false">
                <input id="divLegNumberRoom1" runat="server" visible="false" />
                <div class="detailsColmLeft">
                    <p class="mrgBtm10">
                        <asp:CheckBox ID="Room1chkCancel" class="frmInputChkBox" TabIndex="13" runat="server" onclick="UnCheckSelectAll()" />
                     <!--END REEP | artf1285662 Scanweb - Icon does not show correct on the cancellation page | CHECKBOX BORDER-->
                <label for="chkCancelRoom1">
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelRoom1")%>
                            &lt;<label id="lblReservationNumberRoom1" runat="server"></label>&gt;</label></p>
                    <p class="padLft mrgBtm5">
                        
                        <asp:CheckBox ID="chkSMSRoom1" TabIndex="18" class="frmInputChkBox" runat="server" />
                        
                        <label id="lblSMSRoom1" for="chkSMSRoom1"><%=WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelSMSConfirmation")%></label></p>
                    <p class="padLft">
                        <asp:DropDownList ID="ddlPhoneCodeRoom1" TabIndex="19" CssClass="phoneDropdown" runat="server">
                        </asp:DropDownList>
                        <input type="text" class="frmInputText" id="txtMobileNumberRoom1" tabindex="20" runat="server"
                            maxlength="20" /></p>
                </div>
                <div class="detailsColmRight">
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/ContactPerson") %>
                        </strong><span>
                            <label id="lblContactPersonRoom1" runat="server">
                            </label>
                        </span>
                    </p>
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/Email") %>
                        </strong><span>
                            <label id="lblEmailRoom1" runat="server">
                            </label>
                        </span>
                    </p>
                </div>
                <br class="clear" />
                <Guarantee:GuaranteeInformation ID="GuaranteeInformation0" runat="server">
                </Guarantee:GuaranteeInformation>
            </div>
            <!-- Start Room 2 -->
            <div class="roundMe grayBox boxMargBtm" id="divRoom2" runat="server" visible="false">
                <input id="divLegNumberRoom2" runat="server" visible="false" />
                <div class="detailsColmLeft">
                    <p class="mrgBtm10">
                   
                        <asp:CheckBox ID="Room2chkCancel" TabIndex="21" class="frmInputChkBox" runat="server" onclick="UnCheckSelectAll()" />
                   
                        <label for="chkCancelRoom2">
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelRoom2")%>
                            &lt;<label id="lblReservationNumberRoom2" runat="server"></label>&gt;</label></p>
                    <p class="padLft mrgBtm5">
                        
                        <asp:CheckBox ID="chkSMSRoom2" TabIndex="24" class="frmInputChkBox" runat="server" />

                        <label id="lblSMSRoom2" for="chkSMSRoom2"><%=WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelSMSConfirmation")%></label></p>
                    <p class="padLft">
                        <asp:DropDownList ID="ddlPhoneCodeRoom2" TabIndex="25" CssClass="phoneDropdown" runat="server">
                        </asp:DropDownList>
                        <input type="text" class="frmInputText" TabIndex="26" id="txtMobileNumberRoom2" runat="server"
                            maxlength="20" /></p>
                </div>
                <div class="detailsColmRight">
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/ContactPerson") %>
                        </strong><span>
                            <label id="lblContactPersonRoom2" runat="server">
                            </label>
                        </span>
                    </p>
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/Email") %>
                        </strong><span>
                            <label id="lblEmailRoom2" runat="server">
                            </label>
                        </span>
                    </p>
                </div>
                <br class="clear" />
                 <Guarantee:GuaranteeInformation ID="GuaranteeInformation1" runat="server">
                </Guarantee:GuaranteeInformation>
            </div>
            <!-- Start Room 3 -->
            <div class="roundMe grayBox boxMargBtm" id="divRoom3" runat="server" visible="false">
                <input id="divLegNumberRoom3" runat="server" visible="false" />
                <div class="detailsColmLeft">
                    <p class="mrgBtm10">
                   
                        <asp:CheckBox ID="Room3chkCancel" class="frmInputChkBox" TabIndex="27" runat="server" onclick="UnCheckSelectAll()"/>

                        <label for="chkCancelRoom3">
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelRoom3")%>
                            &lt;<label id="lblReservationNumberRoom3" runat="server"></label>&gt;</label></p>
                    <p class="padLft mrgBtm5">
                        <asp:CheckBox ID="chkSMSRoom3" TabIndex="30" class="frmInputChkBox" runat="server" /><label id="lblSMSRoom3" for="chkSMSRoom3">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelSMSConfirmation") %>
                        </label>
                    </p>
                    <p class="padLft">
                        <asp:DropDownList ID="ddlPhoneCodeRoom3" TabIndex="31" CssClass="phoneDropdown" runat="server">
                        </asp:DropDownList>
                        <input type="text" class="frmInputText" TabIndex="32" id="txtMobileNumberRoom3" runat="server"
                            maxlength="20" /></p>
                </div>
                <div class="detailsColmRight">
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/ContactPerson") %>
                        </strong><span>
                            <label id="lblContactPersonRoom3" runat="server">
                            </label>
                        </span>
                    </p>
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/Email") %>
                        </strong><span>
                            <label id="lblEmailRoom3" runat="server">
                            </label>
                        </span>
                    </p>
                </div>
                <br class="clear" />
                <Guarantee:GuaranteeInformation ID="GuaranteeInformation2" runat="server">
                </Guarantee:GuaranteeInformation>
            </div>
            <!-- Start Room 4 -->
            <div class="roundMe grayBox boxMargBtm" id="divRoom4" runat="server" visible="false">
                <input id="divLegNumberRoom4" runat="server" visible="false" />
                <div class="detailsColmLeft">
                    <p class="mrgBtm10">
                     
                        <asp:CheckBox ID="Room4chkCancel" TabIndex="33" class="frmInputChkBox" runat="server" onclick="UnCheckSelectAll()"/>
                     
                        <label for="Room4chkCancel">
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelRoom4")%>
                            &lt;<label id="lblReservationNumberRoom4" runat="server"></label>&gt;</label></p>
                  <p class="padLft mrgBtm5">
                    
                        <asp:CheckBox ID="chkSMSRoom4" TabIndex="37"
                         class="frmInputChkBox" runat="server" />
                    
                        <label id="lblSMSRoom4" for="chkSMSRoom4"><%=WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelSMSConfirmation")%></label></p>
                    <p class="padLft">
                        <asp:DropDownList ID="ddlPhoneCodeRoom4" TabIndex="38" CssClass="phoneDropdown" runat="server">
                        </asp:DropDownList>
                        <input type="text" TabIndex="39" class="frmInputText" id="txtMobileNumberRoom4" runat="server"
                            maxlength="20" /></p>
                </div>
                <div class="detailsColmRight">
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/ContactPerson") %>
                        </strong><span>
                            <label id="lblContactPersonRoom4" runat="server">
                            </label>
                        </span>
                    </p>
                    <p class="fltLft">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/Email") %>
                        </strong><span>
                            <label id="lblEmailRoom4" runat="server">
                            </label>
                        </span>
                    </p>
                </div>
                <br class="clear" />
                <Guarantee:GuaranteeInformation ID="GuaranteeInformation3" runat="server">
                </Guarantee:GuaranteeInformation>
            </div>
            <!-- /Room  Cancellation-->
            <div class="canclGoBk fltLft">
                <div class="goBackLink fltLft">
                    <asp:LinkButton ID="btnNo" TabIndex="60" runat="server" OnClick="btnNo_Click"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/GoBack") %></span></asp:LinkButton>
                </div>
            </div>
            <div class="canclGoBk fltRt">
                <div class="cancelBtn">
                    <asp:LinkButton ID="btnCancelBooking" TabIndex="50" runat="server" OnClick="btnCancelBooking_Click"
                        CssClass="buttonInner"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/ConfirmCancellation/CancelBooking") %></span></asp:LinkButton>
                        <asp:LinkButton ID="spnCancelBooking" runat="server" OnClick="btnCancelBooking_Click"
                        CssClass="buttonRt"></asp:LinkButton>
                </div>
            </div>
            <br class="clear" />
            <div class="clear">
            </div>
        </div>
        <div class="ft">
            &nbsp;</div>
    </div>
</div>
<div class="clear">&nbsp;
</div>
<div id="Div1" class="masterReserMod21">
    <div class="greyRd">
        <%--<div class="hd sprite">
        </div>
        <p class="fltLft lblReservationHeader">
            <strong><span id="lblReservationHeader" runat="server"></span></strong><a class="help spriteIcon toolTipMe"
                id="reservationNumberHelpIcon" href="#" runat="server"></a>
            <span id="lblReservationNumber" runat="server"></span>
        </p>--%>
        <Booking:ReservationInformation ID="ReservationInfo" runat="server">
        </Booking:ReservationInformation>
        <div class="clear">
        </div>
        <div class="ft">
            &nbsp;</div>
    </div>
</div>
<div class="guaranteTypeInfo">
    <%--<h3>
        <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformation")%>
    </h3>
    <ul>
        <li>
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformationLine1")%>
        </li>
        <li>
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformationLine2")%>
        </li>
        <li>
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformationLine3")%>
            <a href="#" title="www.scandichotels.com">
                <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/ScandicSite")%>
            </a>
            <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformationLine4")%>
        </li>
    </ul>--%>
    <Guarantee:GuaranteeInformation ID="GuaranteeInformation4" runat="server">
                </Guarantee:GuaranteeInformation>
</div>
<!-- Bhavya Cancel Story Start -->
