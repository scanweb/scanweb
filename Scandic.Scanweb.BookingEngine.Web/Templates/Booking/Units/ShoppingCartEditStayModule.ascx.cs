using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Shopping Cart Edit Stay Module
    /// </summary>
    public partial class ShoppingCartEditStayModule : System.Web.UI.UserControl
    {
        private string pageType;

        private string BOOKING_CODE_DEFAULT_TEXT =
            WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingCode");

        public bool errorInBedTypeSelectionInEditStay;
        public bool errorPriorDate;
        private AvailabilityController availabilityController = null;

        /// <summary>
        /// Gets m_AvailabilityController
        /// </summary>
        private AvailabilityController m_AvailabilityController
        {
            get
            {
                if (availabilityController == null)
                {
                    availabilityController = new AvailabilityController();
                }
                return availabilityController;
            }
        }

        /// <summary>
        /// PageType to be used for the edit shopping cart.Based on the pagetype, the related
        //controls will be loaded
        /// </summary>
        public string PageType
        {
            set { this.pageType = value; }
            get { return this.pageType; }
        }

        protected override void OnInit(EventArgs e)
        {
            SetDefaultTextToTextBoxes();
            SetDropDowns();
            //SetFields();

            if (!BookingEngineSessionWrapper.IsModifyBooking && BookingEngineSessionWrapper.BookingCodeIATA != null && BookingEngineSessionWrapper.BookingCodeIATA != string.Empty)
                txtRegularCode.Value = BookingEngineSessionWrapper.BookingCodeIATA;
        }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (this.Visible)
                {
                    Reservation2SessionWrapper.IsCurrentBookingsPage = false;
                    LoginStatus ctrl = (LoginStatus)this.Page.Master.FindControl("MenuLoginStatus");
                    if (ctrl != null)
                    {
                        ctrl.UpdateLoginStatus(false);
                    }
                    if (!Page.IsPostBack)
                    {
                        if (BookingEngineSessionWrapper.IsModifyBooking)
                        {
                            if
                                (BookingEngineSessionWrapper.BookingDetails != null &&
                                 BookingEngineSessionWrapper.BookingDetails.HotelSearch != null &&
                                 BookingEngineSessionWrapper.BookingDetails.HotelSearch.SearchingType != null)
                            {
                                if (BookingEngineSessionWrapper.BookingDetails.HotelSearch.SearchingType == SearchType.BONUSCHEQUE)
                                    pnlBookingCode.Style.Add("display", "none");
                            }
                        }
                        else
                        {
                            if
                                (SearchCriteriaSessionWrapper.SearchCriteria != null &&
                                 SearchCriteriaSessionWrapper.SearchCriteria.SearchingType != null)
                            {
                                if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.BONUSCHEQUE)
                                    pnlBookingCode.Style.Add("display", "none");
                            }
                        }
                        if (pageType == null || pageType == string.Empty)
                        {
                            pageType = EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH;
                        }
                        SetFieldVisibility();
                        //SetDefaultTextToTextBoxes();

                        //SetDropDowns();

                        SetFields();


                        loginSrchID.Value = LoginSourceModule.BOOKING_SEARCH_MODULE.ToString();

                        btnSearch.CommandArgument = BookingTab.Tab1;
                        spnSearch.CommandArgument = BookingTab.Tab1;
                        childrenCriteriaHidden.Value =
                            WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/childcriteria");
                    }
                    else
                    {
                        if (pageType == EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE)
                        {
                            if
                                (SearchCriteriaSessionWrapper.SearchCriteria != null &&
                                 SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor != null &&
                                 SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.SearchString != null)
                                this.txtHotelName.Value = SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.SearchString;
                        }
                    }

                    SetHeaderText();

                    SetTabIndexControl();
                }
            }
            catch (OWSException owsException)
            {
                AppLogger.LogInfoMessage
                    (AppConstants.OWS_EXCEPTION + " \n OWS Error Message: " + owsException.Message +
                     "\n OWS Stack trace: " + owsException.StackTrace);
            }
            catch (CMSException bex)
            {
            }
            catch (Exception BusEx)
            {
            }
        }

        /// <summary>
        /// Set the tabIndex to the controls
        /// </summary>
        private void SetTabIndexControl()
        {
            Int16 tabIndex = 15;
            for (int ctrlCount = 0; ctrlCount < this.Controls.Count; ctrlCount++)
            {
                if (this.Controls[ctrlCount].Controls.Count > 0)
                {
                    Utility.FetchInnerControlSetTabIndex(this.Controls[ctrlCount], ref tabIndex);
                }
                else
                {
                    Utility.SetTabIndex(this.Controls[ctrlCount], ref tabIndex);
                }
            }
        }

        /// <summary>
        /// Set the Header Text of ShoppingCartEditStay
        /// </summary>
        private void SetHeaderText()
        {
            switch (pageType)
            {
                case EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH:
                    lblHeaderText.Text = WebUtil.GetTranslatedText("/bookingengine/booking/ModifyStay/HeaderText");
                    break;
                case EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS:
                    lblHeaderText.Text = WebUtil.GetTranslatedText("/bookingengine/booking/ModifyStay/ModifyBooking");
                    break;
                case EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE:
                    lblHeaderText.Text = WebUtil.GetTranslatedText("/bookingengine/booking/ModifyStay/ModifyBooking");
                    break;
                default:
                    lblHeaderText.Text = WebUtil.GetTranslatedText("/bookingengine/booking/ModifyStay/YourStay");
                    break;
            }
        }

        /// <summary>
        /// SetFieldVisibility
        /// </summary>
        private void SetFieldVisibility()
        {
            switch (pageType)
            {
                case EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH:

                    lblWhereToStay.Text =
                        m_AvailabilityController.GetHotelNameFromCMS
                            (BookingEngineSessionWrapper.BookingDetails.HotelSearch.SelectedHotelCode);
                    lblHotelText.Text = WebUtil.GetTranslatedText("/bookingengine/booking/ModifyStay/HotelText");
                    editStay.Style.Add("display", "block");
                    gobackAnchor.Visible = false;
                    this.Visible = true;
                    hotelSearchBox.Visible = false;
                    noOfRooms.Visible = false;
                    pnlBookingCode.Visible = true;
                    btnSearch.Attributes.Add
                        ("onclick", "javascript:return performValidation(PAGE_MODIFY_SHOPPING_CART);");
                    spnSearch.Attributes.Add
                        ("onclick", "javascript:return performValidation(PAGE_MODIFY_SHOPPING_CART);");
                    break;
                case EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS:
                    lblWhereToStay.Text =
                        ((SearchCriteriaSessionWrapper.SearchCriteria != null)
                             ? SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.SearchString
                             : string.Empty);
                    editStay.Style.Add("display", "none");
                    btnSearch.Attributes.Add
                        ("onclick", "javascript:return performValidation(PAGE_MODIFY_SHOPPING_CART);");
                    spnSearch.Attributes.Add
                        ("onclick", "javascript:return performValidation(PAGE_MODIFY_SHOPPING_CART);");
                    break;
                case EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE:
                    lblWhereToStay.Text = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/WhereToStay1");
                    hotelSearchBox.Visible = true;
                    lblHotelText.Visible = false;
                    txtHotelName.Attributes["disabled"] = "true";
                    pnlBookingCode.Visible = true;
                    noOfRooms.Visible = true;
                    btnSearch.Attributes.Add
                        ("onclick", "javascript:return performValidation(PAGE_MODIFY_SHOPPING_CART);");
                    spnSearch.Attributes.Add
                        ("onclick", "javascript:return performValidation(PAGE_MODIFY_SHOPPING_CART);");
                    break;
                default:
                    lblWhereToStay.Text = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/WhereToStay1");
                    txtHotelName.Visible = true;
                    lblHotelText.Visible = false;
                    if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                    {
                        if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REDEMPTION
                            || SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.BONUSCHEQUE)
                        {
                            pnlBookingCode.Visible = false;
                        }
                        else
                        {
                            pnlBookingCode.Visible = true;
                        }
                    }
                    noOfRooms.Visible = true;
                    btnSearch.Attributes.Add("onclick", "javascript:return performValidation(PAGE_SHOPPING_CART);");
                    spnSearch.Attributes.Add("onclick", "javascript:return performValidation(PAGE_SHOPPING_CART);");
                    break;
            }
            if (false == SelectHotelUtil.IsSearchToBeDone() &&
                HotelResultsSessionWrapper.HotelResults != null && HotelResultsSessionWrapper.HotelResults.Count <= 0)
            {
                editStay.Style.Add("display", "block");
            }
        }


        /// <summary>
        /// Handled during click of search button in regular search and bonus check booking.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            WebUtil.ValidateCampaignCodeForFamilyAndFriendsDNumber(txtRegularCode.Value);

            try
            {
                Reservation2SessionWrapper.AvailabilityCalendarAccessed = false;
                SearchCriteriaSessionWrapper.IsPromocodeInvalidForHotel = false;
                BookingEngineSessionWrapper.IATAProfileCode = string.Empty;
                BookingEngineSessionWrapper.BookingCodeIATA = string.Empty;
                bool isRegularBookingdatesValid = Core.DateUtil.StringToDDMMYYYDate(txtArrivalDate.Value) < Core.DateUtil.StringToDDMMYYYDate(txtDepartureDate.Value);
                if (isRegularBookingdatesValid && WebUtil.IsValidDates(txtArrivalDate.Value, txtDepartureDate.Value))
                {

                    Reservation2SessionWrapper.IsEditYourStaySearchTriggered = true;


                    Reservation2SessionWrapper.SearchedCityName =
                        ((txtHotelName != null) &&
                         (txtHotelName.Value != null))
                            ? (txtHotelName.Value.ToString())
                            : string.Empty;

                    int confirmedRoomCount = 0;
                    CleanSessionSearchResultsSessionWrapper.CleanSessionSearchResults();
                    WebUtil.ClearBlockedRooms();
                    bool errorSettingChildBedType = false;
                    string currentBookingTab = string.Empty;
                    List<ChildrensDetailsEntity> childrenDetailsallRooms = null;
                    HotelSearchEntity hotelSearchEntity = null;
                    bool SessionPresentEarlier = true;
                    if (SearchCriteriaSessionWrapper.SearchCriteria == null)
                    {
                        SessionPresentEarlier = false;
                        SearchCriteriaSessionWrapper.SearchCriteria = NoSessionGetHotelSearchEntity(BookingTab.Tab1);
                        HotelSearchEntity NoSessionhotelSearchEntity = SearchCriteriaSessionWrapper.SearchCriteria;
                        childrenDetailsallRooms = NoSessionCreateChildrenDetailsforAllRooms("bedTypeforRoom", "childAgeforRoom",
                                        "ddlNoOfRoomsReg", "ddlChildPerRoom", "ddlAdultsPerRoom", NoSessionhotelSearchEntity, ref errorSettingChildBedType);
                        CheckChidrenBedTypesSetProperly(ref errorSettingChildBedType, NoSessionhotelSearchEntity);
                        CheckChidrenBedTypesSetProperly(ref errorSettingChildBedType, NoSessionhotelSearchEntity);

                        currentBookingTab = BookingTab.Tab1;
                    }
                    if
                        ((SearchCriteriaSessionWrapper.SearchCriteria != null) &&
                         (SearchCriteriaSessionWrapper.SearchCriteria.ListRooms != null) &&
                         (SearchCriteriaSessionWrapper.SearchCriteria.ListRooms.Count > 0))
                    {
                        foreach (HotelSearchRoomEntity search in SearchCriteriaSessionWrapper.SearchCriteria.ListRooms)
                        {
                            if (search != null)
                                confirmedRoomCount =
                                    search.IsRoomConfirmed == true ? (confirmedRoomCount + 1) : confirmedRoomCount;
                        }
                    }

                    if (txtRegularCode != null && txtRegularCode.Value.Contains("/"))
                    {
                        string[] regularCodes = txtRegularCode.Value.Split('/');
                        BookingEngineSessionWrapper.IATAProfileCode = regularCodes[1];
                        BookingEngineSessionWrapper.BookingCodeIATA = txtRegularCode.Value;
                        txtRegularCode.Value = regularCodes[0];
                    }
                    int iataCode = -1;
                    int.TryParse(Convert.ToString(txtRegularCode.Value), out iataCode);
                    if (iataCode > 0)
                    {
                        BookingEngineSessionWrapper.IATAProfileCode = txtRegularCode.Value;
                        txtRegularCode.Value = string.Empty;
                    }


                    if (SessionPresentEarlier)
                    {

                        LinkButton senderButton = sender as LinkButton;
                        if (null != senderButton)
                        {
                            switch (senderButton.CommandArgument)
                            {
                                case BookingTab.Tab1:
                                    {

                                        //Merchandising:R3:Display ordinary rates for unavailable promo rates 
                                        HotelResultsSessionWrapper.HotelDetails = null;
                                        AlternateHotelsSessionWrapper.IsHotelAlternateFlow = false;
                                        AlternateCityHotelsSearchSessionWrapper.TotalRegionalAltCityHotelCount = -1;
                                        AlternateCityHotelsSearchSessionWrapper.CityAltHotelDetails = null;
                                        AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone = false;
                                        TotalGeneralAvailableHotelsSessionWrapper.PromoGeneralAvailableHotels = 0;

                                        hotelSearchEntity = GetHotelSearchEntity(BookingTab.Tab1);

                                        childrenDetailsallRooms = CreateChildrenDetailsforAllRooms("bedTypeforRoom",
                                                                                                   "childAgeforRoom",
                                                                                                   "ddlNoOfRoomsReg",
                                                                                                   "ddlChildPerRoom",
                                                                                                   "ddlAdultsPerRoom",
                                                                                                   hotelSearchEntity,
                                                                                                   confirmedRoomCount,
                                                                                                   "IsRoomModifiable",
                                                                                                   ref
                                                                                                   errorSettingChildBedType);
                                        CheckChidrenBedTypesSetProperly(ref errorSettingChildBedType, hotelSearchEntity);
                                        currentBookingTab = BookingTab.Tab1;
                                        if (BookingEngineSessionWrapper.IATAProfileCode != null || BookingEngineSessionWrapper.IATAProfileCode != string.Empty)
                                            hotelSearchEntity.IataProfileCode = BookingEngineSessionWrapper.IATAProfileCode;
                                    }
                                    break;
                            }
                        }

                        SearchCriteriaSessionWrapper.SearchCriteria = hotelSearchEntity;
                    }
                    SearchDestinationSessionWrapper.SearchDestination = string.Empty;
                    if (hotelSearchEntity != null && hotelSearchEntity.SearchedFor != null)
                    {
                        SearchDestinationSessionWrapper.SearchDestination = hotelSearchEntity.SearchedFor.SearchString;
                    }
                    SearchDestinationSessionWrapper.SearchDestinationUpdated = false;
                    if (hotelSearchEntity != null)
                    {
                        hotelSearchEntity.IsModifyComboBooking = BookingEngineSessionWrapper.IsModifyComboBooking;
                    }
                    if (hotelSearchEntity != null && childrenDetailsallRooms != null && childrenDetailsallRooms.Count > 0)
                    {
                        hotelSearchEntity.ChildrenOccupancyPerRoom =
                            Utility.GetNoOfChildrenToBeAccommodated(childrenDetailsallRooms[0]);
                    }

                    if (!errorSettingChildBedType)
                    {
                        if (pageType == EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH ||
                            pageType == EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE)
                        {
                            Response.Redirect
                                (GlobalUtil.GetUrlToPage
                                     (EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE), false);
                        }
                        else
                        {
                            string urlToRedirect = Utility.RedirectBookingFlow(BookingEnginePosition.MAIN_SEARCH_PAGE);
                            if (!string.IsNullOrEmpty(urlToRedirect))
                            {
                                Response.Redirect(urlToRedirect, false);
                            }
                            else
                            {
                                clientErrorDiv.Attributes["style"] = "display:block";
                                Utility.ShowErrorMessage(clientErrorDiv,
                                                         WebUtil.GetTranslatedText
                                                             ("/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name"));
                            }
                        }
                    }
                    else
                    {
                        if (currentBookingTab == BookingTab.Tab1)
                        {
                            editStay.Attributes["style"] = "display:block";
                            RegClientErrorDiv.Attributes["style"] = "display:block";
                            string errorMessage =
                                WebUtil.GetTranslatedText
                                    ("/scanweb/bookingengine/errormessages/requiredfielderror/invalidAge")
                                + WebUtil.GetTranslatedText
                                      ("/scanweb/bookingengine/errormessages/requiredfielderror/invalidBedType");
                            Utility.ShowErrorMessageForAccordian(RegClientErrorDiv, errorMessage);
                            if (hotelSearchEntity != null)
                                HideChildernDetailsWhenBedTypeNotSet(hotelSearchEntity, currentBookingTab);
                        }
                    }
                }
                else
                {
                    errorPriorDate = true;
                    editStay.Attributes["style"] = "display:block";
                    RegClientErrorDiv.Attributes["style"] = "display:block";
                    string errorMessage = WebUtil.GetTranslatedText(
                        "/scanweb/bookingengine/errormessages/requiredfielderror/invalidDate");
                    if (!isRegularBookingdatesValid)
                        errorMessage = WebUtil.GetTranslatedText(TranslatedTextConstansts.END_DATE_INVALID);
                    Utility.ShowErrorMessageForAccordian(RegClientErrorDiv, errorMessage);
                }
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
        }

        /// <summary>
        /// This method will assign the default text to the text boxes.
        /// </summary>
        private void SetDefaultTextToTextBoxes()
        {
            txtHotelName.Value = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName");
            txtHotelName.Attributes["rel"] =
                WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName");
            txtRegularCode.Value = BOOKING_CODE_DEFAULT_TEXT;
            txtRegularCode.Attributes["rel"] =
                WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/BookingCode");
        }

        /// <summary>
        /// Creates children details entity
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="listHotelSearchRoomEntity">The list hotel search room entity.</param>
        /// <param name="bedTypes">The bed types.</param>
        /// <param name="childrenDetailsallRooms">The children detailsall rooms.</param>
        /// <param name="childPerRoomControlName">Name of the child per room control.</param>
        /// <param name="adultPerRoomControlName">Name of the adult per room control.</param>
        /// <param name="childAgeForRoomsControlName">Name of the child age for rooms control.</param>
        /// <param name="bedTypeControlName">Name of the bed type control.</param>
        /// <param name="controlAndBedType">Type of the control and bed.</param>
        private void CreateChildrenDetails(int count, List<HotelSearchRoomEntity> listHotelSearchRoomEntity
            , string[] bedTypes, List<ChildrensDetailsEntity> childrenDetailsallRooms,
            string childPerRoomControlName, string adultPerRoomControlName, string childAgeForRoomsControlName,
            string bedTypeControlName, Dictionary<string, string> controlAndBedType, string isRoomModifiableControlName, ref bool errorSettingChildBedType)
        {
            HotelSearchRoomEntity hotelSearchRoomEntity = new HotelSearchRoomEntity();
            DropDownList noOfChilddropdown = this.FindControl(childPerRoomControlName + count) as DropDownList;
            int childrenCount = Convert.ToInt32(noOfChilddropdown.SelectedValue);
            DropDownList noOfAdultsDropdown = this.FindControl(adultPerRoomControlName + count) as DropDownList;
            int adultsCount = Convert.ToInt32(noOfAdultsDropdown.SelectedValue);
            hotelSearchRoomEntity.AdultsPerRoom = adultsCount;
            hotelSearchRoomEntity.ChildrenPerRoom = childrenCount;
            List<ChildEntity> listChildren = CreateChildEntityForRooms(childAgeForRoomsControlName, bedTypeControlName,
                controlAndBedType, bedTypes, count, childrenCount, ref errorSettingChildBedType);
            //Vrushali | Res2.1 | CR - Modify combo booking.
            if ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation))
            {
                HtmlInputHidden isRoomModifiableControl =
                    this.FindControl(isRoomModifiableControlName + count) as HtmlInputHidden;
                hotelSearchRoomEntity.IsRoomModifiable =
                    ((isRoomModifiableControl != null) &&
                     (!string.IsNullOrEmpty(isRoomModifiableControl.Value)))
                        ? Convert.ToBoolean(isRoomModifiableControl.Value)
                        : true;
                if (!hotelSearchRoomEntity.IsRoomModifiable)
                {
                    hotelSearchRoomEntity.IsSearchDone = true;
                    hotelSearchRoomEntity.IsBlocked = true;
                }
                else
                    hotelSearchRoomEntity.IsSearchDone = false;
            }
            if ((listChildren != null) && (listChildren.Count > 0))
            {
                ChildrensDetailsEntity childrensDetailsEntity = new ChildrensDetailsEntity();
                childrensDetailsEntity.SetChildrensEntity((uint)childrenCount, listChildren);
                hotelSearchRoomEntity.ChildrenDetails = childrensDetailsEntity;
                childrenDetailsallRooms.Add(childrensDetailsEntity);
                hotelSearchRoomEntity.ChildrenOccupancyPerRoom =
                    Utility.GetNoOfChildrenToBeAccommodated(childrensDetailsEntity);
            }
            listHotelSearchRoomEntity.Add(hotelSearchRoomEntity);
        }

        private void NoSessionCreateChildrenDetails(int count, List<HotelSearchRoomEntity> listHotelSearchRoomEntity
           , string[] bedTypes, List<ChildrensDetailsEntity> childrenDetailsallRooms,
           string childPerRoomControlName, string adultPerRoomControlName, string childAgeForRoomsControlName,
           string bedTypeControlName, Dictionary<string, string> controlAndBedType, ref bool errorSettingChildBedType)
        {
            HotelSearchRoomEntity hotelSearchRoomEntity = new HotelSearchRoomEntity();
            DropDownList noOfChilddropdown = this.FindControl(childPerRoomControlName + count) as DropDownList;
            int childrenCount = Convert.ToInt32(noOfChilddropdown.SelectedValue);
            DropDownList noOfAdultsDropdown = this.FindControl(adultPerRoomControlName + count) as DropDownList;
            int adultsCount = Convert.ToInt32(noOfAdultsDropdown.SelectedValue);
            hotelSearchRoomEntity.AdultsPerRoom = adultsCount;
            hotelSearchRoomEntity.ChildrenPerRoom = childrenCount;
            List<ChildEntity> listChildren = CreateChildEntityForRooms(childAgeForRoomsControlName, bedTypeControlName,
                controlAndBedType, bedTypes, count, childrenCount, ref errorSettingChildBedType);
            if ((listChildren != null) && (listChildren.Count > 0))
            {
                ChildrensDetailsEntity childrensDetailsEntity = new ChildrensDetailsEntity();
                childrensDetailsEntity.SetChildrensEntity((uint)childrenCount, listChildren);
                hotelSearchRoomEntity.ChildrenDetails = childrensDetailsEntity;
                childrenDetailsallRooms.Add(childrensDetailsEntity);
                hotelSearchRoomEntity.ChildrenOccupancyPerRoom = Utility.GetNoOfChildrenToBeAccommodated(childrensDetailsEntity);
            }
            listHotelSearchRoomEntity.Add(hotelSearchRoomEntity);
        }
        /// <summary>
        /// Create Children details object for all rooms selected in search page.
        /// </summary>
        /// <param name="bedTypeControlName">Name of the bed type control.</param>
        /// <param name="childAgeforRoomsControlName">Name of the child agefor rooms control.</param>
        /// <param name="noOfRoomsControlName">Name of the no of rooms control.</param>
        /// <param name="childPerRoomControlName">Name of the child per room control.</param>
        /// <param name="adultsPerRoomControlName">Name of the adults per room control.</param>
        /// <param name="hotelSearchEntity">The hotel search entity.</param>
        /// <returns>Childrens Details Entity List</returns>
        private List<ChildrensDetailsEntity> CreateChildrenDetailsforAllRooms(string bedTypeControlName,
            string childAgeforRoomsControlName, string noOfRoomsControlName, string childPerRoomControlName, string adultsPerRoomControlName,
            HotelSearchEntity hotelSearchEntity, int confirmedRoomCount, string isModifiableControlName, ref bool errorSettingChildBedType)
        {
            List<HotelSearchRoomEntity> listHotelSearchRoomEntity = new List<HotelSearchRoomEntity>();
            string cot = string.Empty, extraBed = string.Empty, sharingBed = string.Empty;
            Utility.GetLocaleSpecificChildrenAccomodationTypes(ref cot, ref extraBed, ref sharingBed);
            string[] bedTypes = new string[3]
                                    {
                                        cot, extraBed, sharingBed
                                    };

            Dictionary<string, string> controlAndBedType = FetchBedTypeSelection();
            List<ChildrensDetailsEntity> childrenDetailsallRooms = new List<ChildrensDetailsEntity>();
            int noOfRooms = Convert.ToInt32(noOfRoomsSelected.Value);

            for (int count = 1; count <= noOfRooms; count++)
            {
                CreateChildrenDetails(count, listHotelSearchRoomEntity, bedTypes, childrenDetailsallRooms,
                    childPerRoomControlName, adultsPerRoomControlName, childAgeforRoomsControlName,
                   bedTypeControlName, controlAndBedType, isModifiableControlName, ref errorSettingChildBedType);
                //RK: Making sure the isConfirmedflag is set for the already booked rooms
                if (count <= confirmedRoomCount)
                {
                    listHotelSearchRoomEntity[count - 1].IsRoomConfirmed = true;
                }
            }
            hotelSearchEntity.ListRooms = listHotelSearchRoomEntity;
            return childrenDetailsallRooms;
        }
        private List<ChildrensDetailsEntity> NoSessionCreateChildrenDetailsforAllRooms(string bedTypeControlName,
            string childAgeforRoomsControlName, string noOfRoomsControlName, string childPerRoomControlName, string adultsPerRoomControlName,
            HotelSearchEntity hotelSearchEntity, ref bool errorSettingChildBedType)
        {
            List<HotelSearchRoomEntity> listHotelSearchRoomEntity = new List<HotelSearchRoomEntity>();
            string cot = string.Empty, extraBed = string.Empty, sharingBed = string.Empty;
            Utility.GetLocaleSpecificChildrenAccomodationTypes(ref cot, ref extraBed, ref sharingBed);
            string[] bedTypes = new string[3]
                                    {
                                        cot, extraBed, sharingBed
                                    };

            Dictionary<string, string> controlAndBedType = FetchBedTypeSelection();
            List<ChildrensDetailsEntity> childrenDetailsallRooms = new List<ChildrensDetailsEntity>();
            DropDownList noOfRoomsControl = this.FindControl(noOfRoomsControlName) as DropDownList;
            int noOfRooms = Convert.ToInt32(noOfRoomsControl.SelectedValue);

            for (int count = 1; count <= noOfRooms; count++)
            {
                NoSessionCreateChildrenDetails(count, listHotelSearchRoomEntity, bedTypes, childrenDetailsallRooms,
                                      childPerRoomControlName, adultsPerRoomControlName, childAgeforRoomsControlName,
                        bedTypeControlName, controlAndBedType, ref errorSettingChildBedType);
            }
            hotelSearchEntity.ListRooms = listHotelSearchRoomEntity;
            return childrenDetailsallRooms;
        }

        /// <summary>
        /// Reads the hidden field populated by the javascript and creates a dictionary
        /// which will contain the dropdown name and value in it.
        /// </summary>
        /// <returns>Dictionary containing list of bed type dropdown name and selected value in it.</returns>
        private Dictionary<string, string> FetchBedTypeSelection()
        {
            string childrenBedTypes = bedTypeCollection.Value;
            string[] eachBedType = childrenBedTypes.Split(new char[] { '|' });
            Dictionary<string, string> controlAndBedType = new Dictionary<string, string>();
            int eachBedTypeCount = eachBedType.Length;
            for (int count = 0; count < eachBedTypeCount - 1; count++)
            {
                string[] bedTypeControlAndType = eachBedType[count].Split(new char[] { ',' });
                controlAndBedType.Add(bedTypeControlAndType[0], bedTypeControlAndType[1]);
            }
            return controlAndBedType;
        }

        /// <summary>
        /// Creates children entity from dropdowns.
        /// </summary>
        /// <param name="childAgeControlPrefix">The child age control prefix.</param>
        /// <param name="bedTypeControlPrefix">The bed type control prefix.</param>
        /// <param name="controlAndBedType">Type of the control and bed.</param>
        /// <param name="bedTypes">Collection of bed types</param>
        /// <param name="roomNo">The room no.</param>
        /// <param name="childrenCountPerRoom">The children count per room.</param>
        /// <returns>List of child entity</returns>
        /// <remarks></remarks>
        private List<ChildEntity> CreateChildEntityForRooms(string childAgeControlPrefix, string bedTypeControlPrefix,
            Dictionary<string, string> controlAndBedType, string[] bedTypes, int roomNo, int childrenCountPerRoom, ref bool errorSettingChildBedType)
        {
            List<ChildEntity> listChildren = new List<ChildEntity>();
            ChildEntity childEntity = null;
            StringBuilder controlSufix;
            for (int count = 1; count <= childrenCountPerRoom; count++)
            {
                controlSufix = new StringBuilder();
                controlSufix.Append(roomNo);
                controlSufix.Append("Child");
                controlSufix.Append(count);
                DropDownList ageDropdown =
                    this.FindControl(childAgeControlPrefix + controlSufix.ToString()) as DropDownList;
                string key = bedTypeControlPrefix + controlSufix.ToString();
                if ((null != ageDropdown) && (!string.IsNullOrEmpty(key)))
                {
                    if (ageDropdown.SelectedIndex != 0)
                    {
                        string bedType = controlAndBedType.ContainsKey(key) ? controlAndBedType[key] : string.Empty;

                        childEntity = CreateChildEntity(ageDropdown.SelectedValue, bedType,
                                                        bedTypes[0], bedTypes[1], bedTypes[2]);
                        listChildren.Add(childEntity);
                    }
                    //server side check is added for children age.
                    else if (ageDropdown.SelectedIndex == 0)
                    {
                        errorSettingChildBedType = true;
                        break;
                    }
                }
            }
            return listChildren;
        }

        /// <summary>
        /// Create child enity object for each room
        /// </summary>
        /// <param name="childAge"></param>
        /// <param name="bedType"></param>
        /// <param name="cot"></param>
        /// <param name="extraBed"></param>
        /// <param name="sharingBed"></param>
        /// <returns>Child Entity</returns>
        private ChildEntity CreateChildEntity
            (string childAge, string bedType, string cot, string extraBed, string sharingBed)
        {
            ChildEntity childEntity = new ChildEntity();
            childEntity.Age = Convert.ToInt16(childAge);
            if (string.Compare(bedType, cot, false) == 0)
            {
                childEntity.ChildAccommodationType = ChildAccommodationType.CRIB;
                childEntity.AccommodationString = cot;
            }
            else if (string.Compare(bedType, sharingBed, false) == 0)
            {
                childEntity.ChildAccommodationType = ChildAccommodationType.CIPB;
                childEntity.AccommodationString = sharingBed;
            }
            else if (string.Compare(bedType, extraBed, false) == 0)
            {
                childEntity.ChildAccommodationType = ChildAccommodationType.XBED;
                childEntity.AccommodationString = extraBed;
            }
            return childEntity;
        }


        /// <summary>
        /// Create Search Enity object for Regular Booking module
        /// </summary>
        /// <param name="Tab"></param>
        /// <returns>Hotel Search Entity</returns>
        private HotelSearchEntity GetHotelSearchEntity(string Tab)
        {
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
            HotelSearchEntity hotelSearch = new HotelSearchEntity();
            SearchedForEntity searchedFor = null;
            string[] destCodeArr;
            string searchedCityName = string.Empty;
            switch (Tab)
            {
                case BookingTab.Tab1:
                    {
                        if (!string.IsNullOrEmpty(selectedDestId.Value) &&
                            !(WebUtil.GetTranslatedText
                                 ("/bookingengine/booking/searchhotel/EnterHotelName").Equals(txtHotelName.Value)))
                        {
                            searchedCityName = txtHotelName.Value;
                            searchedFor = new SearchedForEntity(searchedCityName);

                            destCodeArr = selectedDestId.Value.ToUpper().Split(':');
                            if (destCodeArr[0] == "HOTEL")
                            {
                                searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.Hotel;
                                searchedFor.SearchString = m_AvailabilityController.GetHotelNameFromCMS(destCodeArr[1]);
                            }
                            else
                            {
                                searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.City;
                                Reservation2SessionWrapper.SearchedCityName = searchedCityName;
                            }
                            searchedFor.SearchCode = destCodeArr[1];
                            if (searchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                                hotelSearch.SelectedHotelCode = searchedFor.SearchCode;

                            hotelSearch.SearchedFor = searchedFor;
                        }
                        else
                        {
                            hotelSearch.SearchedFor = searchedFor;
                        }

                        hotelSearch.ArrivalDate = Core.DateUtil.StringToDDMMYYYDate(txtArrivalDate.Value);
                        hotelSearch.DepartureDate = Core.DateUtil.StringToDDMMYYYDate(txtDepartureDate.Value);
                        hotelSearch.NoOfNights =
                            Core.DateUtil.DateDifference(hotelSearch.DepartureDate, hotelSearch.ArrivalDate);
                        hotelSearch.AdultsPerRoom = int.Parse(ddlAdultsPerRoom1.Text);
                        hotelSearch.ChildrenPerRoom = int.Parse(ddlChildPerRoom1.Text);
                        hotelSearch.RoomsPerNight = Convert.ToInt32(noOfRoomsSelected.Value);
                    }
                    break;
            }
            if (!txtRegularCode.Value.ToUpper().Equals(BOOKING_CODE_DEFAULT_TEXT.ToUpper()))
            {
                if (!string.IsNullOrEmpty(txtRegularCode.Value.Trim()))
                {
                    hotelSearch.SearchingType = GetSearchType(Tab);
                    SearchCriteriaSessionWrapper.SearchCriteria.SearchingType = hotelSearch.SearchingType;
                }
            }
            if (((SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REGULAR) ||
                 (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.VOUCHER) ||
                 (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.CORPORATE)) &&
                (string.IsNullOrEmpty(txtRegularCode.Value.Trim()) ||
                 (txtRegularCode.Value.ToUpper().Equals(BOOKING_CODE_DEFAULT_TEXT.ToUpper()))))
            {
                SearchCriteriaSessionWrapper.SearchCriteria.SearchingType = SearchType.REGULAR;
            }

            hotelSearch.SearchingType = SearchCriteriaSessionWrapper.SearchCriteria.SearchingType;
            if (hotelSearch.SearchingType == SearchType.REDEMPTION)
            {
                hotelSearch.RoomsPerNight = Int32.Parse(AppConstants.ONE);
            }


            switch (hotelSearch.SearchingType)
            {
                case SearchType.REGULAR:
                    {
                        if (!string.IsNullOrEmpty(txtRegularCode.Value.Trim()) &&
                            !(txtRegularCode.Value.ToUpper().Equals(BOOKING_CODE_DEFAULT_TEXT.ToUpper())))
                        {
                            hotelSearch.CampaignCode = txtRegularCode.Value.ToUpper().Trim();
                        }
                        break;
                    }
                case SearchType.CORPORATE:
                    {
                        if (!string.IsNullOrEmpty(txtRegularCode.Value.Trim()) &&
                            !(txtRegularCode.Value.ToUpper().Equals(BOOKING_CODE_DEFAULT_TEXT.ToUpper())))
                        {
                            hotelSearch.CampaignCode = Utility.FormatCode(txtRegularCode.Value);
                            hotelSearch.QualifyingType = Utility.GetQualifyingType(hotelSearch.CampaignCode);
                        }

                        break;
                    }
                case SearchType.VOUCHER:
                    {
                        if (!string.IsNullOrEmpty(txtRegularCode.Value.Trim()) &&
                            !(txtRegularCode.Value.ToUpper().Equals(BOOKING_CODE_DEFAULT_TEXT.ToUpper())))
                        {
                            hotelSearch.CampaignCode = txtRegularCode.Value.ToUpper().Trim();
                        }
                        break;
                    }
            }
            if ((UserLoggedInSessionWrapper.UserLoggedIn) &&
                ((hotelSearch.SearchingType == SearchType.REGULAR) ||
                 (hotelSearch.SearchingType == SearchType.BONUSCHEQUE)
                 || (hotelSearch.SearchingType == SearchType.CORPORATE)))
            {
                LoyaltyDetailsEntity loyaltyDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
                if (null != loyaltyDetails)
                {
                    long membershipNumber;
                    bool parseStatus = long.TryParse(loyaltyDetails.MembershipID, out membershipNumber);
                    if (parseStatus)
                    {
                        hotelSearch.MembershipID = membershipNumber;
                    }
                }
            }


            return hotelSearch;
        }
        /// <summary>
        /// NoSessionGetHotelSearchEntity
        /// </summary>
        /// <param name="Tab"></param>
        /// <returns></returns>
        private HotelSearchEntity NoSessionGetHotelSearchEntity(string Tab)
        {
            string relText = string.Empty;
            if (txtRegularCode.Attributes["rel"] != null)
            {
                relText = txtRegularCode.Attributes["rel"].ToString();
            }
            txtRegularCode.Value = txtRegularCode.Value != relText ? txtRegularCode.Value.Trim() : string.Empty;

            HotelSearchEntity hotelSearch = new HotelSearchEntity();
            SearchedForEntity searchedFor = null;
            string[] destCodeArr;

            if (!string.IsNullOrEmpty(selectedDestId.Value) &&
                !(WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName").Equals(
                    txtHotelName.Value)))
            {
                searchedFor = new SearchedForEntity(txtHotelName.Value.Trim());
                destCodeArr = selectedDestId.Value.ToUpper().Split(':');
                if (destCodeArr[0] == "HOTEL")
                    searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.Hotel;
                else
                {
                    searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.City;
                    Reservation2SessionWrapper.SearchedCityName = ((txtHotelName != null) &&
                                                       (txtHotelName.Value != null))
                                                          ? (txtHotelName.Value.ToString())
                                                          : string.Empty;
                }
                searchedFor.SearchCode = destCodeArr[1];
                if (searchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                    hotelSearch.SelectedHotelCode = searchedFor.SearchCode;

                hotelSearch.SearchedFor = searchedFor;
            }
            else
            {
                hotelSearch.SearchedFor = searchedFor;
            }

            hotelSearch.ArrivalDate = Core.DateUtil.StringToDDMMYYYDate(txtArrivalDate.Value);
            hotelSearch.DepartureDate = Core.DateUtil.StringToDDMMYYYDate(txtDepartureDate.Value);
            hotelSearch.NoOfNights = Core.DateUtil.DateDifference(hotelSearch.DepartureDate,
                                                                  hotelSearch.ArrivalDate);
            hotelSearch.AdultsPerRoom = int.Parse(ddlAdultsPerRoom1.Text);
            hotelSearch.ChildrenPerRoom = int.Parse(ddlChildPerRoom1.Text);
            hotelSearch.RoomsPerNight = int.Parse(ddlNoOfRoomsReg.Text); // added by vrushali.

            hotelSearch.SearchingType = GetSearchType(Tab);

            if ((UserLoggedInSessionWrapper.UserLoggedIn) &&
                ((hotelSearch.SearchingType == SearchType.REGULAR) ||
                 (hotelSearch.SearchingType == SearchType.BONUSCHEQUE)
                 || (hotelSearch.SearchingType == SearchType.CORPORATE)))
            {
                LoyaltyDetailsEntity loyaltyDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
                if (null != loyaltyDetails)
                {
                    long membershipNumber;
                    bool parseStatus = long.TryParse(loyaltyDetails.MembershipID, out membershipNumber);
                    if (parseStatus)
                    {
                        hotelSearch.MembershipID = membershipNumber;
                    }
                }
            }
            return hotelSearch;
        }
        /// <summary>
        /// Find out the dropdown and populate the values.
        /// </summary>
        /// <param name="controlName">Control name to be populated</param>
        /// <param name="startValue">Start value in Dropdown</param>
        /// <param name="endValue">End value in Dropdown</param>
        /// <param name="defaultValue">If any default value then this will appear first instead of start value</param>
        private void CreateDropDown(string controlName, int startValue, int endValue, string defaultValue)
        {
            DropDownList dropDownList = this.FindControl(controlName) as DropDownList;
            if (null != dropDownList)
            {
                ListItem item;
                if (!string.IsNullOrEmpty(defaultValue))
                {
                    item = new ListItem(defaultValue, defaultValue);
                    dropDownList.Items.Add(item);
                }
                for (int constantCount = startValue; constantCount <= endValue; constantCount++)
                {
                    item = new ListItem(constantCount.ToString(), constantCount.ToString());
                    dropDownList.Items.Add(item);
                }
            }
        }

        /// <summary>
        /// This method sets the Dropdowns appropriately as required for this page
        /// The list of values are as defined in the Form and Page Behaviour document
        /// </summary>
        private void SetDropDowns()
        {
            txtnoOfNights.Text = "1";
            int startRoomNo = 0;
            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
            {
                foreach (HotelSearchRoomEntity search in SearchCriteriaSessionWrapper.SearchCriteria.ListRooms)
                {
                    startRoomNo = search.IsRoomConfirmed == true ? (startRoomNo + 1) : startRoomNo;
                }
            }
            startRoomNo = startRoomNo > 0 ? startRoomNo : 1;
            CreateDropDown("ddlNoOfRoomsReg", startRoomNo, 4, "");
            for (int count = 1; count <= 4; count++)
            {
                CreateDropDown("ddlAdultsPerRoom" + count, 1, 6, "");
            }
            for (int count = 1; count <= 4; count++)
            {
                CreateDropDown("ddlChildPerRoom" + count, 0, 5, "");
            }
            int minAge = Convert.ToInt32(ConfigurationManager.AppSettings["Booking.Children.MinAge"]);
            int maxAge = Convert.ToInt32(ConfigurationManager.AppSettings["Booking.Children.MaxAge"]);
            for (int count = 1; count <= 4; count++)
            {
                for (int innerCount = 1; innerCount <= 5; innerCount++)
                {
                    CreateDropDown("childAgeforRoom" + count + "Child" + innerCount, minAge, maxAge,
                                   WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age"));
                }
            }

            string[] bedTypes = new string[3]
                                    {
                                        WebUtil.GetTranslatedText(
                                            "/bookingengine/booking/childrensdetails/accommodationtypes/sharingbed"),
                                        WebUtil.GetTranslatedText(
                                            "/bookingengine/booking/childrensdetails/accommodationtypes/crib"),
                                        WebUtil.GetTranslatedText(
                                            "/bookingengine/booking/childrensdetails/accommodationtypes/extrabed")
                                    };
        }

        /// <summary>
        /// This method sets the search fields by reading the values 
        /// from the session's SearchCriteria
        /// if the SearchCriteria is not in session only the arrival date, departure date
        /// and the search type tab is set to Tab1
        /// </summary>
        private void SetFields()
        {
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
            if (pageType == EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH &&
                BookingEngineSessionWrapper.BookingDetails != null
                && searchCriteria == null
                )
            {
                Hashtable selectedRoomAndRates = new Hashtable();
                searchCriteria = BookingEngineSessionWrapper.BookingDetails.HotelSearch;
                List<BookingDetailsEntity> bookingList = BookingEngineSessionWrapper.ActiveBookingDetails;
                List<HotelSearchRoomEntity> listOfRooms = new List<HotelSearchRoomEntity>();
                SortedList<string, GuestInformationEntity> allGuestsInfo =
                    new SortedList<string, GuestInformationEntity>();
                int roomIndex = 0;

                foreach (BookingDetailsEntity bookingEntity in bookingList)
                {
                    if (bookingEntity.HotelSearch.ListRooms != null && bookingEntity.HotelSearch.ListRooms.Count > 0)
                    {
                        listOfRooms.Add(bookingEntity.HotelSearch.ListRooms[0]);

                        SelectedRoomAndRateEntity selectedRoomRate = new SelectedRoomAndRateEntity();
                        RoomRateEntity roomRate = new RoomRateEntity(bookingEntity.HotelRoomRate.RoomtypeCode,
                                                                     bookingEntity.HotelRoomRate.RatePlanCode);
                        roomRate.BaseRate = bookingEntity.HotelRoomRate.Rate;
                        roomRate.IsSpecialRate = bookingEntity.HotelRoomRate.IsSpecialRate;
                        roomRate.TotalRate = bookingEntity.HotelRoomRate.TotalRate;
                        selectedRoomRate.RoomRates = new List<RoomRateEntity>();
                        selectedRoomRate.RoomRates.Add(roomRate);

                        RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bookingEntity.HotelRoomRate.RatePlanCode);
                        RoomCategory roomCategory = RoomRateUtil.GetRoomCategory(bookingEntity.HotelRoomRate.RoomtypeCode);
                        if (Utility.IsBlockCodeBooking)
                        {
                            selectedRoomRate.RateCategoryID = AppConstants.BLOCK_CODE_QUALIFYING_TYPE;
                        }
                        else
                        {
                            selectedRoomRate.RateCategoryID = rateCategory != null ? rateCategory.RateCategoryId : null;
                        }

                        if (roomCategory != null && selectedRoomRate.SelectedRoomCategoryEntity == null)
                        {
                            RoomCategoryEntity roomCategoryEntity = new RoomCategoryEntity();
                            roomCategoryEntity.Id = roomCategory.RoomCategoryId;
                            roomCategoryEntity.Name = roomCategory.RoomCategoryName;
                            roomCategoryEntity.Url = roomCategory.RoomCategoryURL;
                            roomCategoryEntity.Description = roomCategory.RoomCategoryDescription;
                            selectedRoomRate.SelectedRoomCategoryEntity = roomCategoryEntity;
                        }

                        selectedRoomRate.RoomCategoryID = roomCategory != null ? roomCategory.RoomCategoryId : null;
                        selectedRoomRate.SelectedRoomCategoryName = roomCategory != null ? roomCategory.RoomCategoryName : null;

                        selectedRoomAndRates.Add(roomIndex, selectedRoomRate);
                        roomIndex++;
                        allGuestsInfo[bookingEntity.GuestInformation.ReservationNumber + AppConstants.HYPHEN + bookingEntity.GuestInformation.LegNumber] = bookingEntity.GuestInformation;
                    }
                }

                if (!BookingEngineSessionWrapper.DirectlyModifyContactDetails && HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable == null)
                {
                    HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable = selectedRoomAndRates;
                }
                GuestBookingInformationSessionWrapper.AllGuestsBookingInformations = allGuestsInfo;

                searchCriteria.ListRooms = listOfRooms;
                if (Reservation2SessionWrapper.IsModifyFlow)
                {
                    addRoomBlock.Visible = true;
                }
            }

            if (null != searchCriteria)
            {
                string searchType = searchCriteria.SearchingType.ToString();
                string campaignCode = searchCriteria.CampaignCode;
                Regex partnerProfileCodeValidator = new Regex(@"LSH[0-9]");
                if (!string.IsNullOrEmpty(campaignCode))
                {
                    if (partnerProfileCodeValidator.IsMatch(campaignCode))
                    {
                        campaignCode = string.Empty;
                    }
                }

                int iataCode = -1;
                int.TryParse(campaignCode, out iataCode);
                if (iataCode > 0)
                    campaignCode = string.Empty;

                SetDate(searchCriteria.ArrivalDate, searchCriteria.DepartureDate);
                if (searchType == SearchType.REGULAR.ToString() || searchType == SearchType.CORPORATE.ToString()
                    || searchType == SearchType.VOUCHER.ToString() || searchType == SearchType.BONUSCHEQUE.ToString()
                    || searchType == SearchType.REDEMPTION.ToString())
                {
                    //SetDate(searchCriteria.ArrivalDate, searchCriteria.DepartureDate);
                    txtnoOfNights.Text =
                        Core.DateUtil.DateDifference
                            (searchCriteria.DepartureDate, searchCriteria.ArrivalDate).ToString();
                    if ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation))
                        txtnoOfNights.Enabled = false;
                    ddlNoOfRoomsReg.SelectedValue =
                        searchCriteria.ListRooms != null ? searchCriteria.ListRooms.Count.ToString() : string.Empty;
                    ddlAdultsPerRoom1.SelectedValue = searchCriteria.AdultsPerRoom.ToString();
                    noOfRoomsSelected.Value =
                        searchCriteria.ListRooms != null ? searchCriteria.ListRooms.Count.ToString() : "0";

                    if (searchCriteria.ListRooms != null && searchCriteria.ListRooms.Count > 0)
                    {
                        for (int i = 0; i < searchCriteria.ListRooms.Count; i++)
                        {
                            HtmlInputHidden hiddenControl = null;
                            if ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation))
                            {
                                if (BookingEngineSessionWrapper.ActiveBookingDetails.Count == searchCriteria.ListRooms.Count)
                                {
                                    if
                                        (BookingEngineSessionWrapper.ActiveBookingDetails != null &&
                                         BookingEngineSessionWrapper.ActiveBookingDetails.Count > 0 &&
                                         BookingEngineSessionWrapper.ActiveBookingDetails[i] != null)
                                    {
                                        hiddenControl =
                                            this.FindControl("IsRoomModifiable" + (i + 1)) as HtmlInputHidden;
                                        hiddenControl.Value =
                                            BookingEngineSessionWrapper.ActiveBookingDetails[i].IsCurrentBookingModifiable.ToString();
                                        searchCriteria.ListRooms[i].IsRoomModifiable =
                                            BookingEngineSessionWrapper.ActiveBookingDetails[i].IsCurrentBookingModifiable;
                                    }
                                }
                                else if (searchCriteria.ListRooms.Count > BookingEngineSessionWrapper.ActiveBookingDetails.Count)
                                {
                                    hiddenControl = this.FindControl("IsRoomModifiable" + (i + 1)) as HtmlInputHidden;
                                    hiddenControl.Value = searchCriteria.ListRooms[i].IsRoomModifiable.ToString();
                                }
                            }
                            HtmlGenericControl ctrl = this.FindControl("room" + (i + 1)) as HtmlGenericControl;
                            ctrl.Attributes["style"] = "display : block;";
                            if ((hiddenControl != null) && String.Equals(hiddenControl.Value, "false", StringComparison.InvariantCultureIgnoreCase))
                                ctrl.Attributes.Add("class", "roundMe grayBox howManyRoom M05B disableMe");

                            if (addRoomBlock.Visible && i == 3)
                            {
                                addRoomBlock.Visible = false;
                            }

                            HotelSearchRoomEntity room = searchCriteria.ListRooms[i];
                            DropDownList dropDownList = null;
                            dropDownList = this.FindControl("ddlAdultsPerRoom" + (i + 1)) as DropDownList;
                            if (dropDownList != null)
                            {
                                dropDownList.SelectedValue = room.AdultsPerRoom.ToString();
                            }
                            if ((hiddenControl != null) && String.Equals(hiddenControl.Value, "false", StringComparison.InvariantCultureIgnoreCase))
                                dropDownList.Enabled = false;

                            dropDownList = this.FindControl("ddlChildPerRoom" + (i + 1)) as DropDownList;
                            if (dropDownList != null)
                            {
                                dropDownList.SelectedValue = room.ChildrenPerRoom.ToString();
                            }
                            if ((hiddenControl != null) && String.Equals(hiddenControl.Value, "false", StringComparison.InvariantCultureIgnoreCase))
                                dropDownList.Enabled = false;

                            if
                                (room.ChildrenDetails != null &&
                                 room.ChildrenDetails.ListChildren != null &&
                                 room.ChildrenDetails.ListChildren.Count > 0)
                            {
                                for (int j = 0; j < room.ChildrenDetails.ListChildren.Count; j++)
                                {
                                    ChildEntity child = room.ChildrenDetails.ListChildren[j];
                                    dropDownList =
                                        this.FindControl("childAgeforRoom" + (i + 1) + "Child" + (j + 1)) as
                                        DropDownList;
                                    if (dropDownList != null)
                                    {
                                        if ((Reservation2SessionWrapper.IsModifyFlow)
                                        &&
                                        ((hiddenControl == null) || (String.Equals(hiddenControl.Value, "true", StringComparison.InvariantCultureIgnoreCase))))
                                            dropDownList.SelectedValue = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age");
                                        else
                                            dropDownList.SelectedValue = child.Age.ToString();
                                        dropDownList.Attributes["style"] = "display : block;";
                                        if
                                            ((hiddenControl != null) &&
                                             String.Equals(hiddenControl.Value, "false", StringComparison.InvariantCultureIgnoreCase))
                                            dropDownList.Enabled = false;
                                    }

                                    SetChildrenBedTypeDetails(i, j, room, child, hiddenControl);
                                }
                            }
                        }
                    }
                    ddlAdultsPerRoom1.SelectedValue = searchCriteria.AdultsPerRoom.ToString();
                    sT.Value = GetSelectedTab(searchCriteria.SearchingType);
                }
                if (searchType == SearchType.REDEMPTION.ToString())
                {
                    sT.Value = BookingTab.Tab3;
                    HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab3;

                    if (UserLoggedInSessionWrapper.UserLoggedIn == true)
                    {
                        guestLogin.Value = "true";
                    }
                    ddlNoOfRoomsReg.Enabled = false;

                }
                else if (searchType == SearchType.REGULAR.ToString())
                {
                    sT.Value = BookingTab.Tab1;
                    HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab1;
                    if (!string.IsNullOrEmpty(campaignCode))
                    {
                        txtRegularCode.Value = searchCriteria.CampaignCode.ToString();
                    }
                }
                else if (searchType == SearchType.CORPORATE.ToString())
                {
                    sT.Value = BookingTab.Tab1;
                    HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab1;
                    if (!string.IsNullOrEmpty(campaignCode))
                    {
                        txtRegularCode.Value = searchCriteria.CampaignCode.ToString();
                    }
                }
                else if (searchType == SearchType.VOUCHER.ToString())
                {
                    sT.Value = BookingTab.Tab1;
                    HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab1;
                    if (!string.IsNullOrEmpty(campaignCode))
                    {
                        txtRegularCode.Value = searchCriteria.CampaignCode.ToString();
                    }
                }
                else if (searchType == SearchType.BONUSCHEQUE.ToString())
                {
                    hdnModifyOrCancelPageName.Value = "ModifyOrCancelPage";
                }

                if (searchCriteria.SearchedFor != null)
                {
                    if (searchCriteria.SelectedHotelCode != null)
                    {
                        string selectedHotel =
                            m_AvailabilityController.GetHotelNameFromCMS(searchCriteria.SelectedHotelCode);
                        if (selectedHotel != null && selectedHotel != string.Empty)
                        {
                            txtHotelName.Value = selectedHotel;
                            selectedDestId.Value = "HOTEL:" + searchCriteria.SelectedHotelCode;
                        }
                    }
                    else
                    {
                        txtHotelName.Value = searchCriteria.SearchedFor.SearchString;
                        if (searchCriteria.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.Hotel)
                            selectedDestId.Value = "HOTEL:" + searchCriteria.SearchedFor.SearchCode;
                        else
                            selectedDestId.Value = "CITY:" + searchCriteria.SearchedFor.SearchCode;
                    }
                    if ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation))
                        txtHotelName.Disabled = true;
                }
            }
            else
            {
                SetDate(DateTime.Now, DateTime.Now.AddDays(1));
                sT.Value = BookingTab.Tab1;
            }
            if (SearchCriteriaSessionWrapper.SearchCriteria == null)
            {
                SearchCriteriaSessionWrapper.SearchCriteria = searchCriteria;
            }
        }

        /// <summary>
        /// Reads the Search Type string from "sT" field and accordingly returns the SearchType enum value
        /// if none of the Tab values is returned due to some issue or tampering of the text field
        /// it will be considered as the Regular availability search
        /// This will take the values like "Tab1", "Tab2" etc., and return the SearchType enum value
        /// </summary>
        /// <param name="searchType"></param>
        /// <returns>Search Type</returns>
        private SearchType GetSearchType(string searchType)
        {
            SearchType returnValue = SearchType.REGULAR;
            switch (searchType)
            {
                case BookingTab.Tab1:
                    {
                        if (string.IsNullOrEmpty(txtRegularCode.Value.Trim()))
                        {
                            returnValue = SearchType.REGULAR;
                        }
                        else
                        {
                            returnValue = GetBookingType();
                        }
                        break;
                    }
                case BookingTab.Tab2:
                    {
                        returnValue = SearchType.BONUSCHEQUE;
                        break;
                    }
                case BookingTab.Tab3:
                    {
                        returnValue = SearchType.REDEMPTION;
                        break;
                    }
                default:
                    {
                        returnValue = SearchType.REGULAR;
                        break;
                    }
            }
            return returnValue;
        }

        /// <summary>
        /// After merging of booking tabs Regular tab is used to identify the booking type.
        /// D - If code starts with D then it is Corporate Booking.
        /// L - If code starts with L then it is Travel agent booking.
        /// B - If code starts with B then it is block code booking.
        /// VO- IF code starts with VO then it is Voucher booking.
        /// </summary>
        /// <returns>Search Type</returns>
        private SearchType GetBookingType()
        {
            SearchType returnValue = SearchType.REGULAR;
            string bookingCode = txtRegularCode.Value.ToUpper().Trim();
            if (bookingCode != WebUtil.GetTranslatedText("/scanweb/bookingengine/booking/searchhotel/BookingCode"))
            {
                if (bookingCode.StartsWith("D") || bookingCode.StartsWith("L") || bookingCode.StartsWith("B"))
                {
                    returnValue = SearchType.CORPORATE;
                }
                else if (bookingCode.StartsWith("VO"))
                {
                    returnValue = SearchType.VOUCHER;
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Get Selected Tab
        /// </summary>
        /// <param name="searchType"></param>
        /// <returns>Selected Tab</returns>
        private string GetSelectedTab(SearchType searchType)
        {
            if (SearchType.REGULAR == searchType) return BookingTab.Tab1;
            else if (SearchType.CORPORATE == searchType) return BookingTab.Tab1;
            else if (SearchType.BONUSCHEQUE == searchType) return BookingTab.Tab2;
            else if (SearchType.REDEMPTION == searchType) return BookingTab.Tab3;
            else if (SearchType.VOUCHER == searchType) return BookingTab.Tab1;
            else return BookingTab.Tab1;
        }

        /// <summary>
        /// SetDate
        /// </summary>
        /// <param name="arrivalDate"></param>
        /// <param name="departureDate"></param>
        private void SetDate(DateTime arrivalDate, DateTime departureDate)
        {
            string arrDate = WebUtil.GetDayFromDate(arrivalDate) + AppConstants.SPACE +
                             Core.DateUtil.DateToDDMMYYYYString(arrivalDate);
            string depDate = WebUtil.GetDayFromDate(departureDate) + AppConstants.SPACE +
                             Core.DateUtil.DateToDDMMYYYYString(departureDate);

            txtArrivalDate.Value = arrDate;
            txtDepartureDate.Value = depDate;
            if ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation))
            {
                txtArrivalDate.Disabled = true;
                txtDepartureDate.Disabled = true;
                txtArrivalDate.Attributes.Remove("class");
                txtArrivalDate.Attributes.Add("class", "txtArrivalDate hasDatepicker");
                txtDepartureDate.Attributes.Remove("class");
                txtDepartureDate.Attributes.Add("class", "txtArrivalDate hasDatepicker");
                disableFlds.Attributes.Add("class", "calCont disableTxtFlds");
                disableFldsDep.Attributes.Add("class", "calCont disableTxtFlds");
            }
        }

        /// <summary>
        /// Sets the children bed type details.
        /// </summary>
        /// <param name="roomNumberCnt">The room number CNT.</param>
        /// <param name="childNumberCnt">The child number CNT.</param>
        /// <param name="room">The room.</param>
        /// <param name="child">The child.</param>
        private void SetChildrenBedTypeDetails
            (int roomNumberCnt, int childNumberCnt, HotelSearchRoomEntity room, ChildEntity child,
             HtmlInputHidden hiddenControl)
        {
            string bedTypes = WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/childcriteria");
            char[] tobeSplitWithComma = new char[] { ',' };
            char[] tobeSplitWithPipe = new char[] { '|' };
            char[] tobeSplitWithHighpen = new char[] { '-' };
            string[] bedTypesEntity = bedTypes.Split(tobeSplitWithComma);
            SortedList<string, string> allItemsList = new SortedList<string, string>();
            string selectedKey = string.Empty;
            string selectedString = string.Empty;
            if (bedTypesEntity != null && bedTypesEntity.Length > 0)
            {
                for (int i = 0; i < bedTypesEntity.Length; i++)
                {
                    string[] eachBedType = bedTypesEntity[i].Split(tobeSplitWithPipe);
                    if (eachBedType != null && eachBedType.Length > 0)
                        allItemsList.Add(eachBedType[0], eachBedType[1]);
                }
            }

            foreach (string key in allItemsList.Keys)
            {
                string[] ageRange = key.Split(tobeSplitWithHighpen);

                if (ageRange != null && ageRange.Length > 0)
                {
                    int minRange = Convert.ToInt32(ageRange[0]);
                    int maxRange = Convert.ToInt32(ageRange[ageRange.Length - 1]);
                    if ((minRange <= child.Age) && (child.Age <= maxRange))
                        selectedKey = key;
                }
            }

            string cot = string.Empty, extraBed = string.Empty, sharingBed = string.Empty;
            Utility.GetLocaleSpecificChildrenAccomodationTypes(ref cot, ref extraBed, ref sharingBed);
            string accommodationString =
                room.ChildrenDetails.GetChildrensAccommodationInString(sharingBed, cot, extraBed, childNumberCnt);

            if (!string.IsNullOrEmpty(selectedKey))
                selectedString = allItemsList[selectedKey];

            if (!string.IsNullOrEmpty(selectedString))
            {
                char[] tobeSplitWithUnderScore = new char[] { '_' };
                string[] itemsArray = selectedString.Split(tobeSplitWithUnderScore);
                if (itemsArray != null && itemsArray.Length > 0)
                {
                    DropDownList dropDownList =
                        this.FindControl("bedTypeforRoom" + (roomNumberCnt + 1) + "Child" + (childNumberCnt + 1)) as
                        DropDownList;
                    if (dropDownList != null)
                    {
                        dropDownList.Items.Clear();
                        for (int i = 0; i < itemsArray.Length; i++)
                        {
                            ListItem newListItem = new ListItem(itemsArray[i]);
                            dropDownList.Items.Add(newListItem);
                        }
                        dropDownList.SelectedValue = accommodationString;
                        if ((hiddenControl != null) && String.Equals(hiddenControl.Value, "false", StringComparison.InvariantCultureIgnoreCase))
                            dropDownList.Enabled = false;
                    }

                    HtmlGenericControl divForRoom =
                        this.FindControl("childrenBedTypesDropDownForRoom" + (roomNumberCnt + 1)) as HtmlGenericControl;
                    if (divForRoom != null)
                        divForRoom.Attributes.Add("style", "display:block");

                    HtmlGenericControl parentDiv =
                        this.FindControl("divForchildAgeforRoom" + (roomNumberCnt + 1) + "Child" +
                                         (childNumberCnt + 1)) as HtmlGenericControl;
                    if (parentDiv != null)
                        parentDiv.Attributes.Add("style", "display:block");
                }
            }
        }

        #region functions to hide and show rooms in case of bed type error.

        /// <summary>
        /// Check Chidren Bed Types Set Properly
        /// </summary>
        /// <param name="errorSettingChildBedType"></param>
        /// <param name="hotelSearchEntity"></param>
        private void CheckChidrenBedTypesSetProperly(ref bool errorSettingChildBedType,
                                                     HotelSearchEntity hotelSearchEntity)
        {
            if ((hotelSearchEntity != null) && (hotelSearchEntity.ListRooms != null))
            {
                for (int i = 0; i < hotelSearchEntity.ListRooms.Count; i++)
                {
                    if ((hotelSearchEntity.ListRooms[i] != null) &&
                        ((hotelSearchEntity.ListRooms[i].ChildrenDetails) != null) &&
                        (hotelSearchEntity.ListRooms[i].ChildrenDetails.ListChildren != null) &&
                        (hotelSearchEntity.ListRooms[i].ChildrenDetails.ListChildren.Count > 0) &&
                        (bedTypeCollection.Value == string.Empty))
                    {
                        errorSettingChildBedType = true;
                        break;
                    }
                }
            }
        }


        /// <summary>
        /// Hide Childern Details When Bed Type Not Set
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <param name="currentBookingTab"></param>
        private void HideChildernDetailsWhenBedTypeNotSet(HotelSearchEntity hotelSearchEntity, string currentBookingTab)
        {
            if ((hotelSearchEntity != null) && (hotelSearchEntity.ListRooms != null))
            {
                if (currentBookingTab == BookingTab.Tab1)
                {
                    HideAllRooms("room");
                    ShowRelevantNoOfRooms("room", hotelSearchEntity);
                }
            }
        }

        /// <summary>
        /// HideAllRooms
        /// </summary>
        /// <param name="roomLabel"></param>
        private void HideAllRooms(string roomLabel)
        {
            for (int totalRoomCount = 1; totalRoomCount <= 4; totalRoomCount++)
            {
                HtmlGenericControl divForRoom = this.FindControl(roomLabel + totalRoomCount) as HtmlGenericControl;
                if (divForRoom != null)
                    divForRoom.Style.Add("display", "none");
            }
        }

        /// <summary>
        /// ShowRelevantNoOfRooms
        /// </summary>
        /// <param name="roomLabel"></param>
        /// <param name="hotelSearchEntity"></param>
        private void ShowRelevantNoOfRooms(string roomLabel, HotelSearchEntity hotelSearchEntity)
        {
            if ((hotelSearchEntity != null) && (hotelSearchEntity.ListRooms != null))
            {
                for (int i = 0; i < hotelSearchEntity.ListRooms.Count; i++)
                {
                    int roomCount = i + 1;
                    HtmlGenericControl divForRoom = this.FindControl(roomLabel + roomCount) as HtmlGenericControl;
                    if (divForRoom != null)
                        divForRoom.Style.Add("display", "block");
                }
            }
        }

        #endregion
    }
}