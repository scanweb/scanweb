<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingSearch.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.BookingSearch" %>
<%@ Import Namespace="EPiServer.Core" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<div class="BE">
	<!-- Step2: Select Rate -->
	<div id="ModifyOrCancel">
	    <!-- <form name="ModifyBookingForm"> -->
		<div id="Box">
			<div class="box-top">&nbsp;</div>
			<div class="box-inner">
			    <div id="GuestDetails">
                    <!--Dev NOTE:  Error mesage div -->						
				    <div id="clientErrorDivBD" class="errorText" runat="server"></div>						
				    <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
				    <input type="hidden" id="invalidBookingNumber" name="invalidBookingNumber" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidBookingNumber") %>' />
				    <input type="hidden" id="invalidSurname" name="invalidSurname" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidSurname") %>' />				
                    <!--End NOTE -->	
                    <div id="HelpContent">
                        <p class="popupLink"> 
			                <a href='#' onclick="openPopupWin('<%= GlobalUtil.GetHelpPage(CurrentPage["Help Text"] as PageReference) %>','width=800,height=600,scrollbars=yes,resizable=yes');return false;"><%= WebUtil.GetTranslatedText("/bookingengine/booking/common/help") %></a>
			            </p>
			        </div>
				    <div class="columnOne">
					    <p class="formRow">
					    <span id="lblBookingNumber">
						    <label><%= WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/BookingNumber") %></label>
						    <br />
					    </span>
						    <input tabindex ="1" type="text" id="txtBookingNumber" name="BookingNumber" runat="server" class="frmInputText" maxlength="10" />
					    </p>
				    </div>
				    <div class="columnTwo">
					    <p class="formRow">
					    <span id="lblBookingSurname">
						    <label><%= WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/Surname") %></label>
						    <br />
					    </span>
						    <input tabindex="2" type="text" id="txtSurname" name="Surname" runat="server" class="frmInputText" maxlength="40" />
					    </p>
				    </div>
				    <div class="clear">&nbsp;</div>
				    <!-- Buttons -->
				    <div id="FooterContainer">
					    <div class="alignRight">								
						    <span class="btnSubmit">
				            <asp:LinkButton TabIndex="3" ID="BtnSearch" runat="server" OnClick="BtnSearch_Click"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/Search") %></span></asp:LinkButton>						    
				            </span>
					    </div>
					    <div class="clear">&nbsp;</div>
				    </div>
				    <div class="clear">&nbsp;</div>
				    <!-- Buttons -->
				</div>
			</div>				
			<div class="box-bottom">&nbsp;</div>
		</div>
	    <!--	</form>-->
    </div>			
	<!-- /Reservation Div -->
</div>

