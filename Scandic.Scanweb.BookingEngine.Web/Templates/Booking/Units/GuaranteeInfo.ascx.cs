using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System.Linq;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// GuaranteeInfo
    /// </summary>
    public partial class GuaranteeInfo : EPiServer.UserControlBase
    {
        #region Variables

        private bool isBlockDisplay = false;

        private string nameOnCreditCard =
            WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/NameOnCard");

        private string creditCardNumber =
            WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/NumberOnCard");

        #endregion

        #region Properties

        private int roomIdentifier;

        public int RoomIdentifier
        {
            get { return roomIdentifier; }
            set { roomIdentifier = value; }
        }

        private GuestInformationEntity dataSource;

        public GuestInformationEntity DataSource
        {
            set { dataSource = value; }
        }

        #endregion Properties

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Display Credit card Information
        /// </summary>
        public void DisplayCreditInfo()
        {
            txtCardHolder.Value = nameOnCreditCard;
            txtCardNumber.Value = creditCardNumber;

            bool flagGaurenteeType = false;
            if ((null != dataSource) && dataSource.GuranteeInformation != null)
            {
                if (dataSource.GuranteeInformation.GuranteeType == GuranteeType.HOLD)
                {
                    flagGaurenteeType = true;
                }
            }

            if ((null != dataSource) &&
                (null != dataSource.GuranteeInformation) && (null != dataSource.GuranteeInformation.CreditCard))
            {
                CreditCardEntity creditCard = dataSource.GuranteeInformation.CreditCard;
                if (null != creditCard.NameOnCard)
                    txtCardHolder.Value = creditCard.NameOnCard;

                if (null != creditCard.CardNumber)
                    txtCardNumber.Value = creditCard.CardNumber;

                if (flagGaurenteeType)
                    rdoLateArrivalGurantee.Checked = false;
                else
                    rdoLateArrivalGurantee.Checked = true;


                string cardNumber = creditCard.CardNumber;
                if (cardNumber.Length >= AppConstants.CREDIT_CARD_DISPLAY_CHARS)
                    txtCardNumber.Value =
                        AppConstants.CARD_MASK + cardNumber.Substring
                                                     ((cardNumber.Length - AppConstants.CREDIT_CARD_DISPLAY_CHARS));
                else
                    txtCardNumber.Value = AppConstants.CARD_MASK + cardNumber;


                if (null != creditCard.ExpiryDate)
                {
                    ddlExpiryMonth.SelectedIndex = (creditCard.ExpiryDate.Month - 1) > 0
                                                       ? (creditCard.ExpiryDate.Month - 1)
                                                       : 0;
                    ddlExpiryYear.SelectedValue = creditCard.ExpiryDate.Year.ToString();
                }
                if (null != creditCard.CardType)
                {
                    ddlCardType.SelectedValue = creditCard.CardType;
                }
                HideUnhideCreditCardDiv.Visible = true;
            }
            else
            {
                HideUnhideCreditCardDiv.Visible = false;
            }

            PrePopulateCreditInfo(flagGaurenteeType);

            if (null != BookingEngineSessionWrapper.ActiveBookingDetails &&
                roomIdentifier < BookingEngineSessionWrapper.ActiveBookingDetails.Count)
                DisplayCreditcardOptionAfterSixPM(BookingEngineSessionWrapper.ActiveBookingDetails[roomIdentifier].HotelSearch);
        }

        /// <summary>
        /// Pre populate credit card information 
        /// </summary>
        /// <param name="flagForGaurenteeType"></param>
        public void PrePopulateCreditInfo(bool flagForGaurenteeType)
        {
            NameController nameController = new NameController();
            CreditCardEntity creditCardDetails = null;

            if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null)
            {
                creditCardDetails = nameController.FetchCreditCard(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID, false);
            }

            if (creditCardDetails != null)
            {
                if (flagForGaurenteeType)
                    rdoLateArrivalGurantee.Checked = false;
                else
                    rdoLateArrivalGurantee.Checked = true;

                txtCardHolder.Value = creditCardDetails.NameOnCard;

                string creditCardNumber = creditCardDetails.CardNumber;

                previousCardNumber.Value = creditCardNumber;
                if (creditCardNumber.Length >= AppConstants.CREDIT_CARD_DISPLAY_CHARS)
                    txtCardNumber.Value = AppConstants.CARD_MASK +
                                          creditCardNumber.Substring((creditCardNumber.Length -
                                                                      AppConstants.CREDIT_CARD_DISPLAY_CHARS));
                else
                    txtCardNumber.Value = AppConstants.CARD_MASK + creditCardNumber;

                ListItem selectedCard = ddlCardType.Items.FindByValue(creditCardDetails.CardType);
                if (selectedCard != null)
                {
                    ddlCardType.SelectedItem.Selected = false;
                    selectedCard.Selected = true;
                }
                ListItem selectedMonth = ddlExpiryMonth.Items.FindByValue(creditCardDetails.ExpiryDate.Month.ToString());
                if (selectedMonth != null)
                    selectedMonth.Selected = true;

                ListItem selectedYear = ddlExpiryYear.Items.FindByValue(creditCardDetails.ExpiryDate.Year.ToString());
                if (selectedYear != null)
                    selectedYear.Selected = true;
            }
        }

        /// <summary>
        /// This will display only Credit card option after 6PM if user book a hotel after 6PM on DOA.
        /// </summary>
        /// <param name="hotelSearch">HotelSearchEntity</param>
        public void DisplayCreditcardOptionAfterSixPM(HotelSearchEntity hotelSearch)
        {
            const int SIXPMTIME = 18;
            TimeSpan timeSpan = new TimeSpan();
            HotelDestination hotelDetails = null;
            if (!string.IsNullOrEmpty(BookingEngineSessionWrapper.ActiveBookingDetails[roomIdentifier].HotelSearch.SelectedHotelCode))
                hotelDetails =
                    ContentDataAccess.GetHotelByOperaID
                        (BookingEngineSessionWrapper.ActiveBookingDetails[roomIdentifier].HotelSearch.SelectedHotelCode);

            DateTime currentTimeforHotel = new DateTime();
            if (hotelDetails != null)
            {
                if (hotelDetails.Coordinate != null)
                {
                    currentTimeforHotel =
                        WebUtil.GetCurrentTimeforHotel(hotelDetails.Coordinate.Y, hotelDetails.Coordinate.X);
                    if (currentTimeforHotel != DateTime.MinValue)
                    {
                        int result = DateTime.Compare(hotelSearch.ArrivalDate.Date, currentTimeforHotel.Date);
                        if (result == 0)
                        {
                            timeSpan = currentTimeforHotel.TimeOfDay;
                            if (timeSpan.Hours > SIXPMTIME || (timeSpan.Hours == SIXPMTIME && timeSpan.Minutes > 0))
                            {
                                SetAfterSixPMBookingText();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This will set correct description if user books after 6PM on the same day of arrival.
        /// </summary>
        private void SetAfterSixPMBookingText()
        {
            blkHoldRoom.Visible = false;
            lblGuranteewithRadio.Visible = true;
            rdoLateArrivalGurantee.Disabled = true;
            lblGuranteewithoutRadio.Visible = false;
            HygieneSessionWrapper.Is6PMBooking = true;
            System.Web.UI.HtmlControls.HtmlGenericControl displayTextLabel = new HtmlGenericControl();
            displayTextLabel = lblGuranteewithRadio;
            displayTextLabel.InnerHtml = (CurrentPage["GTDCC"] != null)
                                             ? CurrentPage["GTDCC"].ToString()
                                             : string.Empty;
        }

        /// <summary>
        /// Generic Function to Displays the guarantee information based on the rate category.
        /// </summary>
        /// <param name="rateCategoryID">Rate category id</param>
        public void DisplayHoldOptionIfRequired(string rateCategoryID, bool paymentFallback)
        {
            HideUnhideCreditCardDiv.Attributes.Add("class", "ccInfoDetails fltLft" + " Room" + roomIdentifier);
            rdoHoldRoom.Attributes.Add("rel", "Room" + roomIdentifier);
            rdoLateArrivalGurantee.Attributes.Add("rel", "Room" + roomIdentifier);

            string campaignCode = string.Empty;

            if ((null != BookingEngineSessionWrapper.ActiveBookingDetails[roomIdentifier]) &&
                null != BookingEngineSessionWrapper.ActiveBookingDetails[roomIdentifier].HotelSearch &&
                (!string.IsNullOrEmpty(BookingEngineSessionWrapper.ActiveBookingDetails[roomIdentifier].HotelSearch.CampaignCode)))
                campaignCode = BookingEngineSessionWrapper.ActiveBookingDetails[roomIdentifier].HotelSearch.CampaignCode;

            bool noRateCodeButDisplayGuarantee;
            string creditCardGuranteeType;

            if (IsTruelyPrepaid(rateCategoryID, campaignCode, out noRateCodeButDisplayGuarantee,
                                out creditCardGuranteeType))
                HideUnhideGuranteeDiv.Visible = false;
            else
                DisplayGuranteeDiv(rateCategoryID, noRateCodeButDisplayGuarantee, creditCardGuranteeType, paymentFallback);
        }

        /// <summary>
        /// This will find out if the guarantee information need to be displayed or not.
        /// Along with it finds if no rate code associated and credit card guarantee type text.
        /// </summary>
        /// <param name="rateCategoryId">Rate category id if associated with this booking.</param>
        /// <param name="blockCode">Block code</param>
        /// <param name="noRateCodeButDisplayGuarantee">True if no rate code associated but display 
        /// the guarantee information.</param>
        /// <param name="creditCardGuranteeType">Credit card guarantee type text</param>
        /// <returns>True if this is block code booking and if block code does not need the 
        /// credit card information</returns>
        public bool IsTruelyPrepaid(string rateCategoryId, string blockCode,
                                    out bool noRateCodeButDisplayGuarantee, out string creditCardGuranteeType)
        {
            bool returnVaue = false;
            noRateCodeButDisplayGuarantee = false;
            creditCardGuranteeType = string.Empty;
            if (Utility.IsBlockCodeBooking)
            {
                if (rateCategoryId == AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
                {
                    Block blockPage = ContentDataAccess.GetBlockCodePages(blockCode);
                    if (null != blockPage)
                    {
                        if (blockPage.CaptureGuarantee)
                        {
                            noRateCodeButDisplayGuarantee = true;
                            if (!string.IsNullOrEmpty(blockPage.GuranteeType))
                            {
                                creditCardGuranteeType = blockPage.GuranteeType;
                            }
                        }
                        else
                        {
                            returnVaue = true;
                        }
                    }
                }
            }
            return returnVaue;
        }

        /// <summary>
        /// This will display the GuranteeDiv.Which will contain the fields for the Gurantee infromations.
        /// </summary>
        /// <param name="rateCategoryID">RateCategoryId</param>
        /// <param name="noRateCodeButDisplayGuarantee">Wheather to display gurantee information to the user.</param>
        /// <param name="creditCardGuranteeType">Type of the credit card gurantee.</param>
        public void DisplayGuranteeDiv(string rateCategoryID, bool noRateCodeButDisplayGuarantee,
                                       string creditCardGuranteeType, bool paymentFallback)
        {
            RateCategory rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(rateCategoryID);
            Block block = null;
            if (null == rateCategory)
            {
                block =
                    ContentDataAccess.GetBlockCodePages
                        (BookingEngineSessionWrapper.ActiveBookingDetails[roomIdentifier].HotelSearch.CampaignCode);
            }
            if (((null != block) && (block.SixPMHoldAvailable)) ||
                ((null != rateCategory) && (rateCategory.HoldGuranteeAvailable)))
            {
                blkHoldRoom.Visible = true;
                lblGuranteewithRadio.Visible = true;
                lblGuranteewithoutRadio.Visible = false;
                isBlockDisplay = true;
            }
            else
            {
                lblGuranteewithRadio.Visible = false;
                lblGuranteewithoutRadio.Visible = true;
                isBlockDisplay = false;
            }
            if (BookingEngineSessionWrapper.ActiveBookingDetails[roomIdentifier].HotelSearch.SearchingType.Equals
                (SearchType.REDEMPTION))
            {
                blkHoldRoom.Visible = false;
                HideUnhideCreditCardDiv.Visible = false;
                lblGuranteewithRadio.Visible = false;
                lblGuranteewithoutRadio.Visible = true;
                string redemptionGuranteeText =
                    (null != CurrentPage["RewardNights"])
                        ? CurrentPage["RewardNights"].ToString()
                        : string.Empty;
                lblGuranteewithoutRadio.InnerHtml = redemptionGuranteeText;
            }
            else
            {
                DisplayGuranteeText(rateCategory, noRateCodeButDisplayGuarantee, creditCardGuranteeType, paymentFallback);
            }
        }

        /// <summary>
        /// Set the drop down list of the credit card types
        /// </summary>
        public void SetCreditCardDropDown()
        {
            OrderedDictionary creditCardTypesMap = DropDownService.GetCreditCardsCodes();
            if (creditCardTypesMap != null)
            {
                foreach (string key in creditCardTypesMap.Keys)
                {
                    ddlCardType.Items.Add(new ListItem(creditCardTypesMap[key].ToString(), key));
                }
            }
            ListItem selectedDefaultItem = ddlCardType.Items.FindByValue(AppConstants.DEFAULT_VALUE_CONST);
            if (selectedDefaultItem != null)
            {
                ddlCardType.ClearSelection();
                selectedDefaultItem.Selected = true;
            }
        }

        /// <summary>
        /// Populate the Expiry Date drop down list(non generic)
        /// </summary>
        public void SetExpiryDateList()
        {
            for (int monthCount = 1; monthCount <= AppConstants.TOTALMONTHS; monthCount++)
            {
                IFormatProvider cultureInfo =
                    new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
                string monthName = (new DateTime(1, monthCount, 1)).ToString("MMM", cultureInfo);
                ddlExpiryMonth.Items.Add(new ListItem(monthName, monthCount.ToString()));
            }
            int currentYear = DateTime.Today.Year;
            int currentMonth = DateTime.Today.Month;
            if (currentMonth == 12)
                currentYear = currentYear + 1;
            for (int yearCount = 0; yearCount < AppConstants.TOTALYEARS; yearCount++)
            {
                ddlExpiryYear.Items.Add(new ListItem((currentYear + yearCount).ToString()));
            }
        }

        /// <summary>
        /// Displays the gurantee text.
        /// </summary>
        /// <param name="rateCategory">The rate category.</param>
        /// <param name="noRateCodeButDisplayGuarantee">if set to <c>true</c> [no rate code but display guarantee].</param>
        /// <param name="creditCardGuranteeType">Type of the credit card gurantee.</param>
        public void DisplayGuranteeText
            (RateCategory rateCategory, bool noRateCodeButDisplayGuarantee, string creditCardGuranteeType, bool paymentFallback)
        {
            string[] prepaidGuranteeTypes = AppConstants.GUARANTEE_TYPE_PRE_PAID;
            string spanHoldText = (CurrentPage["GTDCC1800"] != null)
                                      ? CurrentPage["GTDCC1800"].ToString()
                                      : string.Empty;

            spanHoldText = spanHoldText.Replace("<DIV>", "");
            spanHoldText = spanHoldText.Replace("</DIV>", "");
            spanHoldRoom.InnerHtml = spanHoldText;
            System.Web.UI.HtmlControls.HtmlGenericControl displayTextLabel = new HtmlGenericControl();
            if (lblGuranteewithRadio.Visible == true || isBlockDisplay)
                displayTextLabel = txtGuranteeWithRadio;
            else if (lblGuranteewithoutRadio.Visible == true || !isBlockDisplay)
                displayTextLabel = txtGuranteeWithoutRadio;

            if (!noRateCodeButDisplayGuarantee && rateCategory != null)
            {
                creditCardGuranteeType = rateCategory.CreditCardGuranteeType;
                if (!paymentFallback && (prepaidGuranteeTypes != null) && prepaidGuranteeTypes.Contains(rateCategory.CreditCardGuranteeType))
                {
                    creditCardGuranteeType = string.Format("{0}-Prepaid", "rateCategory.CreditCardGuranteeType");
                }
            }
            string displayTextLabelText = (CurrentPage[creditCardGuranteeType] != null)
                                              ? CurrentPage[creditCardGuranteeType].ToString()
                                              : string.Empty;
            displayTextLabelText = displayTextLabelText.Replace("<DIV>", "");
            displayTextLabelText = displayTextLabelText.Replace("</DIV>", "");
            displayTextLabel.InnerHtml = displayTextLabelText;
        }

        /// <summary>
        /// Hide or Show details
        /// </summary>
        /// <param name="type"></param>
        public void DisplayControls(string type)
        {
            if (type == "HOLD")
            {
                spanHoldRoom.Visible = true;
                txtGuranteeWithRadio.Visible = false;
                if (string.IsNullOrEmpty(spanHoldRoom.InnerHtml))
                {
                    headerGuarantee.Visible = false;
                }
            }
            else
            {
                txtGuranteeWithRadio.Visible = true;
                spanHoldRoom.Visible = false;

                if (string.IsNullOrEmpty(txtGuranteeWithRadio.InnerHtml))
                {
                    headerGuarantee.Visible = false;
                }
            }

            if (lblGuranteewithoutRadio.Visible)
            {
                if (!string.IsNullOrEmpty(txtGuranteeWithoutRadio.InnerHtml))
                {
                    headerGuarantee.Visible = true;
                }

                if (
                    BookingEngineSessionWrapper.ActiveBookingDetails[roomIdentifier].HotelSearch.SearchingType.Equals(
                        SearchType.REDEMPTION))
                {
                    if (!string.IsNullOrEmpty(lblGuranteewithoutRadio.InnerHtml))
                    {
                        headerGuarantee.Visible = true;
                    }
                }
            }
        }
    }
}