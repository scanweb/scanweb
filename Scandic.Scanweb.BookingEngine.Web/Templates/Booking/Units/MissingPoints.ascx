<%@ Control Language="C#" AutoEventWireup="true" Codebehind="MissingPoints.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.MissingPoints" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.Core" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Register Src="BookingModuleSmall.ascx" TagName="BookingModuleSmall" TagPrefix="uc2" %>
<%@ Register Src="EmailConfirmation.ascx" TagName="EmailConfirmation" TagPrefix="uc1" %>
<%@ Register Src="Calendar.ascx" TagName="Calendar" TagPrefix="uc3" %>
<div id="Loyalty" class="BE">
    <!-- LastCard -->
    <div id="MissingPointsDiv" runat="server">
        <div class="box-top-grey">
            <span class="title">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/Details") %>
            </span>
        </div>
        <div class="box-middle">
            <div class="content" onkeypress="return WebForm_FireDefaultButton(event, 'addaStay')">
                <div id="clientErrorDiv1">
                </div>
                <div id="MissingpointsErrorDiv" runat="server">
                </div>
                <div class="errorDivClient">
                    <input type="hidden" id="MissingpointsErrMsgTitle" name="MissingpointsErrMsgTitle"
                        value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
                    <input type="hidden" id="MissingpointsReservationError" name="MissingpointsReservationError"
                        value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/reservationnumber") %>' />
                    <input type="hidden" id="MissingpointsCityError" name="MissingpointsCityError" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/validcityname") %>' />
                    <input type="hidden" id="MissingpointsHotelError" name="MissingpointsHotelError"
                        value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/validhotelname") %>' />
                    <input type="hidden" id="arrivaldateError1" name="arrivaldateError1" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/arrival_date") %>' />
                    <input type="hidden" id="departuredateError1" name="departuredateError1" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/departure_date") %>' />
                    <input type="hidden" id="SelectAHotelText" name="SelectAHotelText" value='<%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/selecthotel") %>' />
                    <input type="hidden" id="SelectOneMissingPoint" name="SelectAtLeastMissingPoint"
                        value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/selectatleastonemissingpoints") %>' />
                    <input type="hidden" id="DuplicateStay" name="DuplicateStay" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/duplicatestay") %>' />
                    <input type="hidden" id="MaxStayExceed" name="MaxStayExceed" value='<%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/maxstayexceed") %>' />
                    <input type="hidden" id="selectedCityAndHotel" name="selectedCityAndHotel" value=""
                        runat="server" />
                </div>
                <div class="columnOne">
          <!-- title field add only for German Language !-->
                <div id="dvNameTitle" runat="server" >
                       <p class="formRow">
                                <span id="lblNameTitle">
                                    <label><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nametitle") %></label> 
                                </span>
                                <br />
                                    <span id="NameTitle" runat="server" />
                       </p>
                    </div>
                    <p class="formRow">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/namefirst") %>
                        </label>
                        <br />
                        <span class="value" id="FirstName" runat="server"></span>
                    </p>
                    <p class="formRow">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/addressemail") %>
                        </label>
                        <br />
                        <span class="value" id="MailAddress" runat="Server"></span>
                    </p>
                </div>
                <div class="columnTwo">
                    <p class="formRow">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/surname") %>
                        </label>
                        <br />
                        <span class="value" id="SurName" runat="Server"></span>
                    </p>
                    <!-- R1.3 | CR16 | Loyalty Enhancement 
                             Added  Sapn to display telephone number-->
                    <p class="formRow">
                        <label>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/telephone") %>
                        </label>
                        <br />
                        <span class="value" id="Telephone" runat="Server"></span>
                    </p>
                </div>
                <div class="clear">
                    &nbsp;</div>
                <div class="columnOne">
                    <p class="formRow">
                        <span id="spanReservation">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/reservationnumber") %>
                            </label>
                        </span>
                        <br />
                        <input type="text" name="txtReservation" class="frmInputText" id="txtReservation"
                            runat="server" maxlength="50" />
                    </p>
                    <p class="formRow">
                        <span id="spanCity">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/namecity") %>
                            </label>
                        </span>
                        <br />
                        <%--<asp:DropDownList ID="ddlCity" CssClass="frmSelectBig" runat="server" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                            &nbsp;
                            <span><br />--%>
                        <select id="ddlCity" name="ddlCity" class="frmSelectBig" onchange="javascript:getHotelName();return false;">
                        </select>
                        &nbsp;
                        <%--</span>--%>
                    </p>
                </div>
                <div class="columnTwo">
                    <p class="formRowColumn1">
                        <span id="spanArrivalDate1">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/dateaarrival") %>
                            </label>
                        </span>
                        <br />
                        <input class="inputTextSmall mp_dateRange" type="text" id="txtArrivalDate1" runat="server" />
                    </p>
                    <p class="formRowColumn2">
                        <span id="spanDepartureDate1">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/datedeparture") %>
                            </label>
                        </span>
                        <br />
                        <input class="inputTextSmall mp_dateRange" type="text" id="txtDepartureDate1" runat="server" />
                    </p>
                    <p class="formRow">
                        <span id="spanHotel">
                            <label>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/namehotel") %>
                            </label>
                        </span>
                        <br />
                        <%--<asp:DropDownList ID="ddlHotel" CssClass="frmSelectBig" runat="server"></asp:DropDownList>							
							<span><br />--%>
                        <select id="ddlHotel" class="frmSelectBig" name="ddlHotel">
                        </select>
                        <%--</span>--%>
                        <br class="clear" />
                    </p>
                </div>
                <div class="clear">
                    &nbsp;</div>
                <!-- /details -->
                <!-- Footer -->
                <div id="FooterContainer">
                    <div class="alignRight" id="btnREport" runat="Server">
                        <div class="actionBtn fltRt">
                            <%--<asp:LinkButton ID="LinkButtonReport" OnClick="Report_Click" runat="server"><span><%=WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/send")%></span></asp:LinkButton>--%>
                            <a href="javascript:void(0);" class="buttonInner" onclick="addRow();" id="addaStay">
                                <span>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/addastay") %>
                                </span></a>
                                <a href="javascript:void(0);" class="buttonRt scansprite" onclick="addRow();" id="addaStay2">
                                </a>
                        </div>
                    </div>
                    <div class="clear">
                        &nbsp;</div>
                </div>
                <div id="stayListForMissingPoint">
                    <div id="stayDetails">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="stayDetailsTable">
                            <thead>
                                <tr>
                                    <th>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/deletebuttontext") %>
                                    </th>
                                    <th>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/reservationnumber") %>
                                    </th>
                                    <th>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/dateaarrivalheader") %>
                                    </th>
                                    <th>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/datedepartureheader") %>
                                    </th>
                                    <th>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/namecityheader") %>
                                    </th>
                                    <th>
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/namehotelheader") %>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="FooterContainer">
                        <!--div class="alignLeft"><span class="btnSubmit"> <a href="javascript:void(0);" onclick="delRow()"><span>Delete selected</span></a></span></div-->
                        <div class="alignRight">
                            <div class="actionBtn fltRt">
                                <asp:LinkButton ID="LinkButtonReport" class="buttonInner sendbt" OnClick="Report_Click"
                                    runat="server"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/send") %></span></asp:LinkButton>                                    
                                    <asp:LinkButton ID="spnButtonReport" class="buttonRt scansprite sendbtrt" OnClick="Report_Click"
                                    runat="server"></asp:LinkButton>                                
                            </div>
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                    </div>
                </div>
                <!-- Footer -->
            </div>
        </div>
        <div class="box-bottom">
            &nbsp;</div>
    </div>
    <!-- /LastCard -->
</div>
<uc1:EmailConfirmation ID="EmailConfirmationMsg" runat="server" />
<div id="calendar">
    <uc3:Calendar id="Calendar1" runat="server" />
</div>

<script language="javascript" type="text/javascript">
    
    initCal();
    setCityDropDown();
    
    var allDestination;
    var arrivalDt, departureDt;
    var staysForMissingPoint = new StaysForMissingPoint();
    
    function setCityDropDown(){
        var cityDropDown = $fn(_endsWith("ddlCity"));
        addOptionsToDropDown('<%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/selectcity") %>',"DFT",cityDropDown);
        <%
                List<CityDestination> cityList = CustomizedFetchAllDestination();
                for (int count = 0; count < cityList.Count; count++)
                {
        %>
            allDestination += '<%= cityList[count].OperaDestinationId %>' + ":";            
            addOptionsToDropDown('<%= cityList[count].Name %>','<%= cityList[count].OperaDestinationId %>',cityDropDown);
            <%
                List<HotelDestination> hotelList = cityList[count].SearchedHotels;
                for (int hotelCount = 0; hotelCount < hotelList.Count; hotelCount++)
                {
            %>
                allDestination += '<%= hotelList[hotelCount].Name %>';
            <%
                if (hotelCount != hotelList.Count - 1)
                {
            %>
                    allDestination += ",";
                <%
                }
                }
                %>
            allDestination += "%";
        <%
                }
        %>
        if(($fn(_endsWith("txtArrivalDate1")) != null) &&  ($fn(_endsWith("txtDepartureDate1")) != null))
        {
            arrivalDt=$fn(_endsWith("txtArrivalDate1")).value;
            departureDt=$fn(_endsWith("txtDepartureDate1")).value;
        }
    }
    
    function getHotelName(){
        $fn(_endsWith("ddlHotel")).innerHTML = null;
        var cityDropDown = $fn(_endsWith("ddlCity"));
        if(cityDropDown.value != "DFT")
        {
            addOptionsToDropDown($fn(_endsWith("SelectAHotelText")).value,"DFT",$fn(_endsWith("ddlHotel")));
            var index = allDestination.indexOf(cityDropDown.value) + cityDropDown.value.length + 1;
            var allHotels = allDestination.slice(index,allDestination.indexOf("%",index)).split(",");        
            for(var maxCount=0;maxCount<allHotels.length;maxCount++)
            {
                addOptionsToDropDown(allHotels[maxCount],maxCount,$fn(_endsWith("ddlHotel")));
            }
        }
    }
    
    function addOptionsToDropDown(text,value,dropDown){
        if(dropDown != null){
            var option = document.createElement("OPTION");
            option.text = text;
            option.value = value;
            dropDown.options.add(option);
        }
    }
    
    function pushEntityToHiddenField(){
    
        /*var newString="";
        var theTable = document.getElementById("stayDetailsTable");
        //Some browser returns innerHTML with all tags like <td> ..as <TD> so to avoid that replacing here.
        var text = theTable.innerHTML.replace(/<TD>/g,"<td>").replace(/<\/TD>/g,"</td>").replace(/<TR>/g,"<tr>").replace(/<\/TR>/g,"</tr>");        
        var tableRows = text.slice(text.indexOf("</tr>")+5,text.lastIndexOf("</tr>"));
        var eachEntity = tableRows.split("</tr>");
        for(var count=0;count<eachEntity.length;count++)
        {
            var individualEntity = eachEntity[count].substr(eachEntity[count].indexOf("</td>")+5);
            individualEntity = individualEntity.replace(/<\/td>/g,",");
            individualEntity = individualEntity.replace(/<td>/g,"");
            newString += individualEntity;
            newString += "%";
        }
        $fn(_endsWith("selectedCityAndHotel")).value = newString;*/
        
        $fn(_endsWith("selectedCityAndHotel")).value = staysForMissingPoint.getStaysInString();
    }
    
    function addRow() {
    
    if(performValidation(PAGE_MISSING_POINTS)){
        var theTable = document.getElementById("stayDetailsTable");
	    var rowNo = theTable.rows.length;
	    //R1.5 artf825182: Missing points||Suggestion There should be a limit for the user to add stay in missing points.
	    var maxStay = <%= AppConstants.MAX_ADD_A_STAY_ALLOWED %>;
	    if(rowNo <= maxStay)
	    {
	        var newRow = theTable.insertRow(rowNo);
            var newCell;
	        var bookingNum = $fn(_endsWith("txtReservation")).value;
	        var arrivalDt = $fn(_endsWith("txtArrivalDate1")).value;
	        var departureDt = $fn(_endsWith("txtDepartureDate1")).value;
	        var city = $fn(_endsWith("ddlCity")).options[$fn(_endsWith("ddlCity")).selectedIndex].text;
	        var hotel = $fn(_endsWith("ddlHotel")).options[$fn(_endsWith("ddlHotel")).selectedIndex].text;
	        document.getElementById("stayListForMissingPoint").style.display = "block";	
            newCell = newRow.insertCell(0);
	        //newCell.innerHTML = '<input type="checkbox" value="delete'+rowNo+'" name="chk" id="chk'+rowNo+'" />';
	        newCell.innerHTML = '<a href="javascript:void(0);" onclick="deleteRow(this.parentNode.parentNode.rowIndex)"><%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/deletebuttontext") %></a>';
	        newCell.className = "deleteRow";
	        newCell = newRow.insertCell(1);
	        newCell.innerHTML=bookingNum;
	        newCell = newRow.insertCell(2);
	        newCell.innerHTML=arrivalDt;
	        newCell = newRow.insertCell(3);
	        newCell.innerHTML=departureDt;
	        newCell = newRow.insertCell(4);
	        newCell.innerHTML=city;
	        newCell = newRow.insertCell(5);
	        newCell.innerHTML=hotel;
	        
	        ////////////////////////////////////////////////////////////
	        // While adding a stay, it should also add tot he collection in the javascript. 
	        var newStay = new MissingPointStay(bookingNum, arrivalDt, departureDt, city, hotel);
	        staysForMissingPoint.addStay(newStay);
	        ////////////////////////////////////////////////////////////
	        
	        $("#stayDetailsTable tr:even").addClass("altRow");
	        resetFields();
	    }
	    var rowNo = theTable.rows.length;
	    if(rowNo > maxStay)
	    {
	       document.getElementById("addaStay").style.display = "none";
	       document.getElementById("addaStay2").style.display = "none";
	       
	       $fn("clientErrorDiv1").style.visibility  = isVisible;
	       $fn("clientErrorDiv1").style.display     = "block";
	       $fn("clientErrorDiv1").className         = "errorText";
	       $fn("clientErrorDiv1").innerHTML         = "<BR/>" + $fn("MaxStayExceed").value + "<BR/><BR/>";
	    }
	}
}

/*R1.5 | artf809435: FGP - add more than one stay at Missing Points */
function resetFields(){
    $fn(_endsWith("txtReservation")).value="";
    $fn(_endsWith("txtArrivalDate1")).value=arrivalDt;
    $fn(_endsWith("txtDepartureDate1")).value=departureDt;
    $fn(_endsWith("ddlCity")).value = "DFT";
    $fn(_endsWith("ddlHotel")).innerHTML = null;
}

function deleteRow(i){
    var theTable = document.getElementById('stayDetailsTable');
	var rowNo = theTable.rows.length
	if(rowNo == 2){
		document.getElementById("stayListForMissingPoint").style.display = "none";	
		theTable.deleteRow(i);
	}else {
		theTable.deleteRow(i);
		$("#stayDetailsTable tr").removeClass("altRow");
	}
	
	////////////////////////////////////////////////////////////
	// While deleting any stay fromt eh UI, it should also delete the same stay in the collection.
    staysForMissingPoint.deleteStay(i-1);
    ////////////////////////////////////////////////////////////
	    
	$("#stayDetailsTable tr:even").addClass("altRow");
	
	var maxStay = <%= AppConstants.MAX_ADD_A_STAY_ALLOWED %>;
	var rowNo = theTable.rows.length;
	if(rowNo <= maxStay)
	  {	  
	    document.getElementById("addaStay").style.display = "";
	    document.getElementById("addaStay2").style.display = "";
	    
	    $fn("clientErrorDiv1").style.visibility = isHidden;
	    $fn("clientErrorDiv1").style.display    = "none";
	    $fn("clientErrorDiv1").className        = null;
	    $fn("clientErrorDiv1").innerHTML        = null;
	 }
}
</script>

