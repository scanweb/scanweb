#region Description

////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: Lost Card with out Login user control code behind       //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Raj Kishore Marandi	                                  //
/// Author email id				:                           							  //
/// Creation Date				: 6th December  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revision History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#endregion Description

#region  System Namespaces

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion System Namespaces

    #region Scandic Namespaces

    #endregion Scandic Namespaces

    #region EpiServer NameSpace

#endregion EpiServer NameSpace

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// LostCardNoLogin
    /// </summary>
    public partial class LostCardNoLogin : EPiServer.UserControlBase
    {
        #region Protected Methods

        #region PageLoad

        /// <summary>
        /// Fired when page load is done.
        /// Responsible for setting country dropdown,phone code drop down.
        /// Responsible for setting labels of Lost card confirmation user control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                try
                {
                    if (!IsPostBack)
                    {
                        LCWLEmailConfirmation.Visible = false;
                        SetCountryDropDown();
                        SetPhoneCodesDropDown();
                        SetEmailConfirmationUserControl();
                        LinkButtonReport.Attributes.Add("onclick",
                                                        "javascript:return performValidation(PAGE_LOSTCARD_NOLOGIN);");
                        spnButtonReport.Attributes.Add("onclick",
                                                       "javascript:return performValidation(PAGE_LOSTCARD_NOLOGIN);");
                        if (Utility.GetCurrentLanguage().ToUpper() == LanguageConstant.LANGUAGE_GERMAN)
                        {
                            divNameTitle.Visible = true;
                            InformationController informationCtrl = new InformationController();
                            Dictionary<string, string> nameTitleMapOpera =
                                informationCtrl.GetConfiguredTitles(LanguageConstant.LANGUAGE_GERMAN_CODE);
                            ListItem selectItem = new ListItem(AppConstants.TITLE_DEFAULT_VALUE,
                                                               AppConstants.TITLE_DEFAULT_VALUE);
                            ddlTitle.Items.Add(selectItem);

                            foreach (string key in nameTitleMapOpera.Keys)
                            {
                                ddlTitle.Items.Add(new ListItem(nameTitleMapOpera[key].ToString(), key));
                            }
                            ListItem selectedDefaultItem = ddlTitle.Items.FindByValue(AppConstants.TITLE_DEFAULT_VALUE);
                            if (selectedDefaultItem != null)
                            {
                                ddlTitle.SelectedItem.Selected = false;
                                selectedDefaultItem.Selected = true;
                            }
                        }
                        else
                        {
                            divNameTitle.Visible = false;
                        }
                    }
                }
                catch (Exception genEx)
                {
                    Scanweb.Core.AppLogger.LogFatalException(genEx,
                                                             "Page Load - Lost card No Login could not executed properly..");
                }
            }
        }

        #endregion PageLoad

        #region ReportButtonClick

        /// <summary>
        ///    Method fired when Report button is fired.
        ///    This Method Sends mail.
        ///    This Method Hides Lost Card User Control and display Confirmation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReportButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEMail.Value != string.Empty)
                {
                    sendEmailToUser();
                }
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
            try
            {
                SendEmail();
                LCWLEmailConfirmation.Visible = true;
                LostCarWithOutLogInDiv.Visible = false;
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
        }

        #endregion ReportButtonClick

        #endregion Protected Methods

        #region Private Methods

        #region SetEmailConfirmationUserControl

        /// <summary>
        /// This method sets the Email confirmation user control to be send after successful email send.
        /// </summary>
        private void SetEmailConfirmationUserControl()
        {
            LCWLEmailConfirmation.AssignHeader(
                WebUtil.GetTranslatedText(
                    "/bookingengine/booking/lostcardwithoutlogin/lostcardwithoutloginmessageheader"));
            LCWLEmailConfirmation.AssignMessage(
                WebUtil.GetTranslatedText(
                    "/bookingengine/booking/lostcardwithoutlogin/lostcardwithoutloginmessgaedescription"));
            LCWLEmailConfirmation.AssignLink(
                WebUtil.GetTranslatedText(
                    "/bookingengine/booking/lostcardwithoutlogin/lostcardwithoutloginspecialoffers"));
        }

        #endregion SetEmailConfirmationUserControl

        #region SendMail

        /// <summary>
        /// This is the functionality for sending email message.
        /// </summary>
        private void SendEmail()
        {
            try
            {
                Dictionary<string, string> emailConfirmationMap = GetLostCardNoLogInSCEmailConfirmationDetails();
                EmailEntity emailEntity =
                    CommunicationUtility.GetLostCardNoLogInSCConfirmationEmail(emailConfirmationMap);
                CommunicationService.SendMail(emailEntity, null);
            }
            catch (Exception genEx)
            {
                Scanweb.Core.AppLogger.LogFatalException(genEx);
            }
        }

        #endregion SendMail

        #region sendEmailToUser

        /// <summary>
        /// sendEmailToUser
        /// </summary>
        private void sendEmailToUser()
        {
            try
            {
                Dictionary<string, string> emailConfirmationMap = GetLostCardNoLogInEmailConfirmationDetails();
                EmailEntity emailEntity = CommunicationUtility.GetLostCardNoLogInConfirmationEmail(
                    emailConfirmationMap, txtEMail.Value.Trim());
                CommunicationService.SendMail(emailEntity, null);
            }
            catch (Exception genEx)
            {
                Scanweb.Core.AppLogger.LogFatalException(genEx);
            }
        }

        #endregion sendEmailToUser

        #region GetLostCardNoLogInEmailConfirmationDetails

        /// <summary>
        /// GetLostCardNoLogInEmailConfirmationDetails
        /// </summary>
        /// <returns>LostCardNoLogInEmailConfirmationDetails</returns>
        private Dictionary<string, string> GetLostCardNoLogInEmailConfirmationDetails()
        {
            Dictionary<string, string> LostCardNoLogInEmailConfirmationMap = new Dictionary<string, string>();
            string valueNotPresent = WebUtil.GetTranslatedText("/bookingengine/booking/contactus/fieldnotavailable");
            LostCardNoLogInEmailConfirmationMap[CommunicationTemplateConstants.TITLE] =
                Utility.GetNameTitle(ddlTitle.Text);
            //
            LostCardNoLogInEmailConfirmationMap[CommunicationTemplateConstants.FIRST_NAME] = (txtFirstName.Value == null
                                                                                                  ? valueNotPresent
                                                                                                  : txtFirstName.Value);
            LostCardNoLogInEmailConfirmationMap[CommunicationTemplateConstants.LOGOPATH] = AppConstants.LOGOPATH;
            return LostCardNoLogInEmailConfirmationMap;
        }

        #endregion GetLostCardNoLogInEmailConfirmationDetails

        #region GetLostCardNoLogInSC Confirmation Details

        /// <summary>
        /// Collect the Lost Card Confirmation Information to be send as Email
        /// </summary>
        /// <returns>OrderedDictionary</returns>
        private Dictionary<string, string> GetLostCardNoLogInSCEmailConfirmationDetails()
        {
            Dictionary<string, string> LostCardNoLogInSCEmailConfirmationMap = new Dictionary<string, string>();
            string valueNotPresent = CommunicationTemplateConstants.BLANK_SPACES;
            LostCardNoLogInSCEmailConfirmationMap[CommunicationTemplateConstants.TITLE] =
                Utility.GetNameTitle(ddlTitle.Text);
            LostCardNoLogInSCEmailConfirmationMap[CommunicationTemplateConstants.FIRST_NAME] = (txtFirstName.Value ==
                                                                                                string.Empty
                                                                                                    ? valueNotPresent
                                                                                                    : txtFirstName.Value);
            LostCardNoLogInSCEmailConfirmationMap[CommunicationTemplateConstants.LASTNAME] = (txtLastName.Value ==
                                                                                              string.Empty
                                                                                                  ? valueNotPresent
                                                                                                  : txtLastName.Value);
            LostCardNoLogInSCEmailConfirmationMap[CommunicationTemplateConstants.EMAIL] = (txtEMail.Value ==
                                                                                           string.Empty
                                                                                               ? valueNotPresent
                                                                                               : txtEMail.Value);
            LostCardNoLogInSCEmailConfirmationMap[CommunicationTemplateConstants.CITY] = (txtCity.Value == string.Empty
                                                                                              ? valueNotPresent
                                                                                              : txtCity.Value);
            LostCardNoLogInSCEmailConfirmationMap[CommunicationTemplateConstants.ADDRESS_LINE1] =
                (txtAddressLine1.Value == string.Empty ? valueNotPresent : txtAddressLine1.Value);
            LostCardNoLogInSCEmailConfirmationMap[CommunicationTemplateConstants.ADDRESS_LINE2] =
                (txtAddressLine2.Value == string.Empty ? valueNotPresent : txtAddressLine2.Value);
            LostCardNoLogInSCEmailConfirmationMap[CommunicationTemplateConstants.TELEPHONE1] =
                (ddlTelephone.SelectedItem.Text == string.Empty ? string.Empty : ddlTelephone.SelectedItem.Text) +
                (txtHomePhone.Value == null ? valueNotPresent : txtHomePhone.Value);
            LostCardNoLogInSCEmailConfirmationMap[CommunicationTemplateConstants.COUNTRY] =
                (ddlCountry.SelectedItem.Text == string.Empty ? valueNotPresent : ddlCountry.SelectedItem.Text);
            LostCardNoLogInSCEmailConfirmationMap[CommunicationTemplateConstants.POST_CODE] = (txtPostCode.Value ==
                                                                                               string.Empty
                                                                                                   ? valueNotPresent
                                                                                                   : txtPostCode.Value);
 
            if (txtGuestNumber.Value != string.Empty)
            {
                LostCardNoLogInSCEmailConfirmationMap[CommunicationTemplateConstants.CARD_NUMBER] =
                    txtGuestNumber.Value.Trim();
            }
            else
            {
                LostCardNoLogInSCEmailConfirmationMap[CommunicationTemplateConstants.NO_GUSTPRG_NUMBER] =
                    AppConstants.SPACE;
            }

            return LostCardNoLogInSCEmailConfirmationMap;
        }

        #endregion GetLostCardNoLogInSC Confirmation Details

        #region SetCountryDropDown

        /// <summary>
        /// Set the drop down list of Countries
        /// </summary>
        private void SetCountryDropDown()
        {
            OrderedDictionary countryCodeMap = DropDownService.GetCountryCodes();
            if (countryCodeMap != null)
            {
                foreach (string key in countryCodeMap.Keys)
                {
                    ddlCountry.Items.Add(new ListItem(countryCodeMap[key].ToString(), key));
                }
            }
            ListItem selectedDefaultItem = ddlCountry.Items.FindByValue(AppConstants.DEFAULT_VALUE_CONST);
            if (selectedDefaultItem != null)
            {
                ddlCountry.SelectedItem.Selected = false;
                selectedDefaultItem.Selected = true;
            }
        }

        #endregion SetCountryDropDown

        #region SetPhoneCodeDropDown

        /// <summary>
        /// Set the drop down list of Phone Codes
        /// </summary>
        private void SetPhoneCodesDropDown()
        {
            OrderedDictionary phoneCodesMap = DropDownService.GetTelephoneCodes();
            if (phoneCodesMap != null)
            {
                foreach (string key in phoneCodesMap.Keys)
                {
                    if (key == AppConstants.DEFAULT_VALUE_CONST || key == AppConstants.DEFAULT_SEPARATOR_CONST)
                    {
                        ddlTelephone.Items.Add(new ListItem(phoneCodesMap[key].ToString(), key));
                    }
                    else
                    {
                        ddlTelephone.Items.Add(new ListItem(key, key));
                    }
                }
            }
            ListItem telephoneSelectedDefaultItem = ddlTelephone.Items.FindByValue(AppConstants.DEFAULT_VALUE_CONST);
            if (telephoneSelectedDefaultItem != null)
            {
                ddlTelephone.SelectedItem.Selected = false;
                telephoneSelectedDefaultItem.Selected = true;
            }
        }

        #endregion SetPhoneCodeDropDown

        #endregion Private Methods
    }
}