//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Carousel;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;

namespace Scandic.Scanweb.BookingEngine.Web
{
    /// <summary>
    /// Code behind of AjaxServer page.
    /// </summary>
    public partial class AjaxServer : EPiServer.TemplatePage
    {
        #region Declarations

        private const string GET_AVAILABLE_HOTELS = "GetAvailableHotels";
        private const string GET_MARKERS_DATA_XML = "GetMarkersDataXml";
        private const string UPDATE_IE6MESSAGE = "updateIE6MessageStatus";
        private const string SELECT_HOTEL_TAB = "TabInSelectHotel";
        private const string GET_AVAILABILITY_CALENDARS = "GetAvailabilityCalendars";
        private const string GET_IMAGE_GALLERY = "GetImageGallery";
        private const string CLEAR_BLOCKEDROOMS = "ClearBlockedRooms";
        private const string GET_CURRENT_BOOKINGS = "GetCurrentBookings";
        private const string GET_CURRENT_BOOKINGS_URL = "GetCurrentBookingsRedirectUrl";
        private const string CREATE_CAROUSEL = "CreateCarousel";
        private const string GET_REDEMPTION_POINTS = "GetRedemptionPoints";
        private const string SAVE_CREDIT_CARD = "SaveCreditCard";
        private const string DELETE_CREDIT_CARD = "DeleteCreditCard";
        private const string GET_DYNAMIC_CONTENT = "GetDynamicContent";
        private const string CHECK_RESERVATION_STATUS = "CheckReservationStatus";

        private NameController nameController = null;

        /// <summary>
        /// Gets NameController
        /// </summary>
        private NameController mNameController
        {
            get
            {
                if (nameController == null)
                {
                    nameController = new NameController();
                }
                return nameController;
            }
        }

        #endregion Declarations

        public bool isOperaConnected = true;

        #region Page_Load

        /// <summary>
        /// This method will be called each time an ajax call made from the AjaxCall.js
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool FirstTimeCall = false;
            int BookingIndex = 0;
            string SearchType = string.Empty;
            string SearchName = string.Empty;
            string MinPoints = string.Empty;
            string MaxPoints = string.Empty;
            string SortingBasedOn = string.Empty;
            string Sorting = string.Empty;
            string ConfirmationId = string.Empty;
            if (!IsPostBack)
            {
                string methodToCall = string.Empty;
                string responseXml = string.Empty;
                string contentType = string.Empty;
                string hotelID = string.Empty;
                string hotelName = string.Empty;

                methodToCall = Convert.ToString(Request.Form["methodToCall"]);
                if (Request.HttpMethod.Equals("GET") && (Request.QueryString.Count > 0))
                {
                    methodToCall = Convert.ToString(Request.QueryString["methodToCall"]);
                    hotelID = Convert.ToString(Request.QueryString["hotelID"]);
                    hotelName = Convert.ToString(Request.QueryString["HotelName"]);
                }
                switch (methodToCall)
                {
                    case GET_AVAILABLE_HOTELS:
                        {
                            FirstTimeCall = Convert.ToBoolean(Request.Form["FirstTimeCall"]);
                            AppLogger.LogInfoMessage(string.Format("The value of First Time Call in Ajax Server: {0}",
                                                                  FirstTimeCall.ToString(CultureInfo.InvariantCulture)));

                            if (RedemptionHotelsAvailableSessionWrapper.NoRedemptionHotelsAvailable)
                            {
                                TotalRegionalAvailableHotelsSessionWrapper.TotalRegionalAvailableHotels = 0;
                                TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels = 0;
                            }
                            responseXml = GetAvailableHotels(FirstTimeCall);
                            contentType = "text/html";
                        }
                        break;

                    case GET_MARKERS_DATA_XML:
                        {
                            if (FindAHotelSessionVariablesSessionWrapper.MarkerData != null)
                            {
                                responseXml = FindAHotelSessionVariablesSessionWrapper.MarkerData.OuterXml;
                            }
                            contentType = "text/xml";
                        }
                        break;
                    case UPDATE_IE6MESSAGE:
                        {
                            HygieneSessionWrapper.IsIE6PopupClosed = true;
                        }
                        break;
                    case SELECT_HOTEL_TAB:
                        {
                            if (!string.IsNullOrEmpty(Request.Form["stillHotelToFetch"] as string))
                            {
                                responseXml = "<value>";
                                responseXml += SelectHotelUtil.IsSearchToBeDone().ToString();
                                responseXml += "</value>";
                                contentType = "text/xml";
                            }
                            else
                            {
                                string selectedTabId = Request.Form["SelectedTab"] as string;
                                string selectedDiv = Request.Form["SelectedDiv"] as string;
                                Reservation2SessionWrapper.SelectedTabInSelectHotel = selectedTabId + "|" + selectedDiv;
                            }
                            break;
                        }
                    case GET_AVAILABILITY_CALENDARS:
                        FirstTimeCall = Convert.ToBoolean(Request.Form["FirstTimeCall"]);
                        Scanweb.Core.AppLogger.LogInfoMessage("The value of First Time Call in Ajax Server: " +
                                                              FirstTimeCall.ToString());

                        if (Reservation2SessionWrapper.AvailabilityCalendar != null &&
                            Reservation2SessionWrapper.AvailabilityCalendar.ListAvailCalendarItems != null
                            && Reservation2SessionWrapper.AvailabilityCalendar.ListAvailCalendarItems.Count > 0)
                        {
                            responseXml = GetAvailabilityCalendars(FirstTimeCall);
                            contentType = "text/xml";
                        }
                        break;
                    case GET_IMAGE_GALLERY:
                        {
                            responseXml = GetImageDetailsForHotel(hotelID, hotelName);
                            contentType = "text/html";
                        }
                        break;
                    case CLEAR_BLOCKEDROOMS:
                        {
                            responseXml = ClearBlockedRooms();
                            contentType = "text/html";
                        }
                        break;
                    case GET_CURRENT_BOOKINGS:
                        {
                            contentType = "application/json";
                            responseXml = GetCurrentBookingsJsonResponse();
                        }
                        break;
                    case GET_CURRENT_BOOKINGS_URL:
                        {
                            ConfirmationId = Request.Form["ConfirmationId"];
                            contentType = "text/html";
                            responseXml = GetCurrentBookingsRedirectUrl(ConfirmationId);
                        }
                        break;
                    case CREATE_CAROUSEL:
                        {
                            contentType = "application/json";


                            string CurrentPageTypeID = Convert.ToString(Request.Form["CurrentPageTypeID"]);
                            int CurrentPageLinkID = Convert.ToInt32(Request.Form["CurrentPageLinkID"]);


                            PageData CurentPageData = new PageData();
                            CurentPageData = ContentDataAccess.GetPageData(CurrentPageLinkID);

                            var genericobj = CarouselFactory.CreateInstance(CurrentPageTypeID).GetCarouselData(CurentPageData);

                            string responsJSON = new JavaScriptSerializer().Serialize(genericobj);
                            responseXml = responsJSON;

                            //contentType = "application/json";
                        }
                        break;
                    case GET_REDEMPTION_POINTS:
                        {
                            contentType = "application/json";
                            FirstTimeCall = Convert.ToBoolean(Request.Form["FirstTimeCall"]);
                            SearchType = Request.Form["SearchType"];
                            SearchName = Request.Form["SearchName"];
                            MinPoints = Request.Form["MinPoints"];
                            MaxPoints = Request.Form["MaxPoints"];
                            SortingBasedOn = Request.Form["SortingBasedOn"];
                            Sorting = Request.Form["sorting"];
                            responseXml = GetRedemptionPoints(FirstTimeCall, SearchType, SearchName, MinPoints, MaxPoints, Sorting, SortingBasedOn);
                        }
                        break;
                    case SAVE_CREDIT_CARD:
                        {
                            responseXml = SaveCreditCard();
                            contentType = "text/html";
                        }
                        break;
                    case DELETE_CREDIT_CARD:
                        {
                            responseXml = string.Empty;

                            if (!string.IsNullOrEmpty(Request.Form["creditCardOperaId"]))
                                responseXml = DeleteCreditCard(Convert.ToInt64(Request.Form["creditCardOperaId"]));

                            contentType = "text/html";
                        }
                        break;
                    case GET_DYNAMIC_CONTENT:
                        {
                            responseXml = GetDynamicContent();
                            contentType = "text/html";
                        }
                        break;
                    case CHECK_RESERVATION_STATUS:
                        {
                            responseXml = CheckReservationStatus();
                            contentType = "text/html";
                        }
                        break;
                }
                WriteToResponse(responseXml, contentType);
            }
        }
        #endregion Page_Load

        private string GetRedemptionPoints(bool firstTimeCall, string searchType, string searchName, string minPoints, string maxPoints, string sorting, string sortingBasedOn)
        {
            var json = string.Empty;
            try
            {
                if (firstTimeCall)
                {
                    searchType = Convert.ToString(HotelRedemptionPoints.SuggestionSearch.COUNTRY);
                    switch (CMSSessionWrapper.CurrentLanguage)
                    {
                        case "en":
                        case "se":
                            searchName = ConfigurationManager.AppSettings["CountryCodeSE"];
                            break;
                        case "no":
                            searchName = ConfigurationManager.AppSettings["CountryCodeNO"];
                            break;
                        case "da":
                            searchName = ConfigurationManager.AppSettings["CountryCodeDK"];
                            break;
                        case "fi":
                        case "ru-RU":
                            searchName = ConfigurationManager.AppSettings["CountryCodeFI"];
                            break;
                        case "de":
                            searchName = ConfigurationManager.AppSettings["CountryCodeDE"];
                            break;
                        default:
                            searchName = ConfigurationManager.AppSettings["CountryCodeSE"];
                            break;

                    }
                }

                HotelRedemptionPoints points = new HotelRedemptionPoints();
                var serializer = new JavaScriptSerializer();
                json = serializer.Serialize(points.GetHotelRemptionPoints(searchType, searchName, minPoints, maxPoints, sorting, sortingBasedOn));

                if (string.IsNullOrEmpty(json) || json == "[]")
                    json = "<empty>" + WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsSearchError") + "<BR />" + "</empty>";
            }
            catch (Exception ex)
            {
                Scanweb.Core.AppLogger.LogFatalException(ex);
                json = "<error>" + ex.Message + "<BR />" + "</error>";
            }
            return json;
        }
        /// <summary>
        /// Gets Json serialized object with JavaScriptSerializer
        /// </summary>
        /// <returns></returns>
        private string GetCurrentBookingsJsonResponse()
        {
            string responseXml = string.Empty;
            int BookingIndex;
            bool FirstTimeCall;
            try
            {
                if (!UserLoggedInSessionWrapper.UserLoggedIn)
                    responseXml = "<session>" + string.Format("{0}", WebUtil.GetCompletePageUrl(GlobalUtil.GetUrlToPage(EpiServerPageConstants.SESSION_TIMEOUT_PAGE), false)) + "<session>";

                if (UserLoggedInSessionWrapper.UserLoggedIn)
                {
                    FirstTimeCall = Convert.ToBoolean(Request.Form["FirstTimeCall"]);
                    BookingIndex = Convert.ToInt16(Request.Form["BookingIndex"]);
                    string nameID = string.Empty;
                    if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null)
                    {
                        if (!string.IsNullOrEmpty(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID))
                            nameID = LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID;

                        if (nameID != string.Empty)
                        {
                            CurrentBookingSummary currentBookingSummary = null;
                            int bookingCount = Convert.ToInt16(AppConstants.CURRENT_BOOKINGS_COUNT.ToString());
                            if (FirstTimeCall)
                            {
                                ReservationController reservationController = new ReservationController();
                                currentBookingSummary = reservationController.GetCurrentBookingSummary(nameID);
                                Reservation2SessionWrapper.CurrentBookingsSummary = currentBookingSummary;
                            }

                            if (Reservation2SessionWrapper.CurrentBookingsSummary != null)
                            {
                                var serializer = new CustomJSSerializer();
                                int bookingsListCount = Reservation2SessionWrapper.CurrentBookingsSummary.CurrentBookingsList.Count;
                                string hotelClosed = WebUtil.GetTranslatedText(AppConstants.HOTEL_CLOSED);

                                Reservation2SessionWrapper.CurrentBookingsSummary.CurrentBookingsList
                                                          .ForEach(c => { if (string.IsNullOrEmpty(c.HotelName)) c.City = hotelClosed; });
                                hotelClosed = string.Empty;

                                System.Web.HttpCookie noOfBookings = new System.Web.HttpCookie("NoOfBookings", Convert.ToString(Reservation2SessionWrapper.CurrentBookingsSummary.CurrentBookingsList.Count));
                                noOfBookings.Expires = DateTime.MaxValue;
                                Response.Cookies.Add(noOfBookings);

                                int newBookingCount = bookingsListCount - BookingIndex;
                                if (newBookingCount < bookingCount)//Case when list contains less number of records than mentioned in configuration file
                                    bookingCount = newBookingCount;

                                var json = serializer.Serialize(Reservation2SessionWrapper.CurrentBookingsSummary.CurrentBookingsList.GetRange(BookingIndex, bookingCount));
                                responseXml = json;
                                if (string.IsNullOrEmpty(responseXml) || responseXml == "[]")
                                {
                                    responseXml = "<nomoreresults>" + "<p>" + WebUtil.GetTranslatedText("/bookingengine/booking/currentbooking/nomoreresults") +
                                    "</p>" + "</nomoreresults>";
                                }
                            }
                        }
                    }
                }
            }
            catch (OWSException owsException)
            {
                if (!string.Equals(owsException.ErrCode, OWSExceptionConstants.BOOKING_NOT_FOUND_IGNORE, StringComparison.InvariantCultureIgnoreCase))
                    Scanweb.Core.AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);

                switch (owsException.ErrCode)
                {
                    case OWSExceptionConstants.BOOKING_NOT_FOUND:
                    case OWSExceptionConstants.SEE_CONTACT_DETAILS_ON_CHAIN_INFO:
                        responseXml = "<error>" + "<p>" + WebUtil.GetTranslatedText("/bookingengine/booking/currentbooking/nobookings") +
                            "</p>" + "</error>";
                        break;

                    default:
                        if (owsException.Message == AppConstants.SYSTEM_ERROR)
                        {
                            WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                 WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                        }
                        else
                        {
                            responseXml = "<error>" + owsException.Message + "<BR />" + "</error>";
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Scanweb.Core.AppLogger.LogFatalException(ex);
                responseXml = "<error>" + ex.Message + "<BR />" + "</error>";
            }
            return responseXml;
        }

        private string GetCurrentBookingsRedirectUrl(string confId)
        {
            string responseXml = string.Empty;
            try
            {
                if (!UserLoggedInSessionWrapper.UserLoggedIn)
                    responseXml = "<session>" + string.Format("{0}", WebUtil.GetCompletePageUrl(GlobalUtil.GetUrlToPage(EpiServerPageConstants.SESSION_TIMEOUT_PAGE), false)) + "<session>";

                if (UserLoggedInSessionWrapper.UserLoggedIn)
                {
                    Utility.CleanSession();
                    string confirmationId = confId;
                    ReservationController reservationController = new ReservationController();
                    List<BookingDetailsEntity> bookingDetailsList = reservationController.FetchBookingSummary(confirmationId);
                    BookingEngineSessionWrapper.IsModifyBooking = true;

                    Reservation2SessionWrapper.IsModifyFlow = true;

                    if (bookingDetailsList != null && bookingDetailsList.Count > 0)
                    {
                        BookingDetailsEntity bookingDetails = bookingDetailsList[0];
                        if (bookingDetails != null)
                        {
                            GuestInformationEntity guestInfoEntity = bookingDetails.GuestInformation;
                            if (guestInfoEntity != null)
                            {
                                string lastName = guestInfoEntity.LastName;

                                BookingEngineSessionWrapper.BookingDetails = bookingDetails;
                                if (Utility.IsBlockCodeBooking)
                                    Utility.OverrideCancelableDateIfBlockCode(bookingDetailsList);
                                BookingEngineSessionWrapper.AllBookingDetails = bookingDetailsList;
                                ReservationNumberSessionWrapper.ReservationNumber = confirmationId;
                                Utility.SetBookingEntityIsModifiableAndIsComboReservation();
                                string currentAccountURL = GlobalUtil.GetUrlToPage(EpiServerPageConstants.MYACCOUNT_BOOKING_PAGE);
                                if ((currentAccountURL != string.Empty) && (currentAccountURL != null))
                                {
                                    BookingEngineSessionWrapper.PreviousPageURL = currentAccountURL;
                                }
                                else
                                {
                                    BookingEngineSessionWrapper.PreviousPageURL = Request.Url.AbsoluteUri;
                                }

                                responseXml = string.Format("{0}", WebUtil.GetCompletePageUrl(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_CHANGE_DATES), false));
                            }
                        }
                    }
                }
            }
            catch (OWSException owsException)
            {
                if (!string.Equals(owsException.ErrCode, OWSExceptionConstants.BOOKING_NOT_FOUND_IGNORE, StringComparison.InvariantCultureIgnoreCase))
                    Scanweb.Core.AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);
                switch (owsException.ErrCode)
                {
                    case OWSExceptionConstants.BOOKING_NOT_FOUND:
                    case OWSExceptionConstants.SEE_CONTACT_DETAILS_ON_CHAIN_INFO:
                        {
                            responseXml = "<error>" + WebUtil.GetTranslatedText(TranslatedTextConstansts.NOT_ONLINE_BOOKING) + "<BR />" + "</error>";
                            break;
                        }
                    default:
                        {
                            responseXml = "<error>" + owsException.Message + "<BR />" + "</error>";
                            break;
                        }
                }
                string exceptionMessage = WebUtil.GetTranslatedText(owsException.TranslatePath);
                if (owsException.Message == AppConstants.SYSTEM_ERROR)
                {
                    WebUtil.ShowApplicationError
                        (WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                         WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                }
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }
            return responseXml;
        }

        #region GetAvailableHotels

        /// <summary>
        /// To fetch hotels based on the search criteria provided by the user.
        /// When this call is made for the first time, it makes a call to OWS 
        /// and update the available hotels in the session sequentially by a thread pool.
        /// But for subsequent calls, it fetches the data i.e. hotel inform the session.  
        /// </summary>
        /// <param name="FirstTimeCall"></param>
        /// <returns></returns>
        private string GetAvailableHotels(bool FirstTimeCall)
        {
            string result = string.Empty;
            HotelSearchEntity hotelSearchEntity = SearchCriteriaSessionWrapper.SearchCriteria;
            List<IHotelDetails> hotelListResult = SelectHotelUtil.GetAllHotels();
            string ExceptionMsg = string.Empty;
            Boolean stillMoreHotelsToFetch = SelectHotelUtil.IsSearchToBeDone();
            bool showSearchHotelWidget = false;
            try
            {
                StringBuilder sb = new StringBuilder();
                if (false == stillMoreHotelsToFetch && hotelListResult == null)
                {
                    if (!AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                    {
                        SelectHotelUtil.InitiateAsynchronousSearchForAltCityHotels(hotelSearchEntity);
                        sb.Append("<hotels>");
                        sb.Append("<morehotelstofetch>true</morehotelstofetch>");
                        sb.Append("<totalhotels>0</totalhotels>");
                        //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
                        OperaStatus(sb);
                        sb.Append("</hotels>");
                    }
                    else
                    {
                        //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
                        if (hotelSearchEntity.CampaignCode != null && hotelSearchEntity.SearchingType == SearchType.REGULAR
                        && hotelSearchEntity.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City)
                            sb.Append(WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE3));
                        else
                            sb.Append("<error>" + WebUtil.GetTranslatedText(AppConstants.NO_HOTELS_FOUND) + "</error>");
                        showSearchHotelWidget = true;
                    }
                }
                //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
                else if (false == stillMoreHotelsToFetch && hotelSearchEntity.CampaignCode != null && hotelSearchEntity.SearchingType == SearchType.REGULAR
                    && false == SelectHotelUtil.HasHotelsWithPromotionRates() && AlternateCityHotelsSearchSessionWrapper.TotalRegionalAltCityHotelCount == -1
                    && TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels != TotalGeneralAvailableHotelsSessionWrapper.PromoGeneralAvailableHotels)
                {
                    AlternateCityHotelsSearchSessionWrapper.IsDestinationAlternateFlow = true;
                    SelectHotelUtil.InitiateAsynchronousSearchForAltCityHotels(hotelSearchEntity);
                    AlternateCityHotelsSearchSessionWrapper.IsDestinationAlternateFlow = false;
                    sb.Append("<hotels>");
                    sb.Append("<morehotelstofetch>true</morehotelstofetch>");
                    sb.Append("<totalhotels>0</totalhotels>");
                    OperaStatus(sb);
                    sb.Append("</hotels>");
                }
                else if (hotelListResult != null)
                {
                    AppLogger.LogInfoMessage("Fetching hotels from Session");
                    if (!stillMoreHotelsToFetch && hotelListResult.Count <= 0)
                    {
                        if (!AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                        {
                            AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone = true;
                            SelectHotelUtil.InitiateAsynchronousSearchForAltCityHotels(hotelSearchEntity);
                            sb.Append("<hotels>");
                            sb.Append("<morehotelstofetch>true</morehotelstofetch>");
                            sb.Append("<totalhotels>0</totalhotels>");
                            //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
                            OperaStatus(sb);
                            sb.Append("</hotels>");
                        }
                        else
                        {
                            sb.Append("<error>");
                            //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
                            if (hotelSearchEntity.CampaignCode != null && hotelSearchEntity.SearchingType == SearchType.REGULAR
                                && hotelSearchEntity.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City)
                                sb.Append(WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE3));
                            else
                                sb.Append(WebUtil.GetTranslatedText(AppConstants.NO_HOTELS_FOUND));
                            showSearchHotelWidget = true;
                            sb.Append("</error>");
                        }
                    }
                    //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)

                        /*else if(!stillMoreHotelsToFetch && hotelSearchEntity.CampaignCode != null && 
                        hotelSearchEntity.SearchingType == SearchType.REGULAR && 
                        TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels == TotalGeneralAvailableHotelsSessionWrapper.PromoGeneralAvailableHotels)*/

                    else
                    {
                        sb.Append("<hotels>");
                        sb.Append("<morehotelstofetch>");
                        sb.Append(stillMoreHotelsToFetch.ToString(CultureInfo.InvariantCulture).ToLower());
                        sb.Append("</morehotelstofetch>");

                        bool promoSearchShowHotelsWithPublicRates = false;
                        int totalHotels = hotelListResult.Count;
                        if (false == stillMoreHotelsToFetch)
                        {
                            switch (hotelSearchEntity.SearchingType)
                            {
                                case (SearchType.CORPORATE):
                                    if ((!Utility.IsBlockCodeBooking) &&
                                        (false == SelectHotelUtil.HasHotelsWithNegotiatedRates()))
                                    {
                                        if (!AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                                        {
                                            showSearchHotelWidget = true;
                                            if (!string.IsNullOrEmpty(hotelSearchEntity.CampaignCode))
                                                ErrorMessageForAlternateSearch(sb, AppConstants.NO_HOTELS_WITH_DNUMBER_MSG, hotelSearchEntity.CampaignCode);
                                            else
                                                ErrorMessageForAlternateSearch(sb, AppConstants.NO_HOTELS_WITH_DNUMBER_MSG, string.Empty);
                                            sb.Append("<removeDiscountTypeSort>");
                                            sb.Append("true");
                                            sb.Append("</removeDiscountTypeSort>");
                                        }
                                    }
                                    if (Utility.IsBlockCodeBooking &&
                                        (false == SelectHotelUtil.HasHotelsWithNegotiatedRates()))
                                    {
                                        if (!AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                                        {
                                            showSearchHotelWidget = true;
                                            if (!string.IsNullOrEmpty(hotelSearchEntity.CampaignCode))
                                                ErrorMessageForAlternateSearch(sb, AppConstants.NO_HOTELS_WITH_DNUMBER_MSG, hotelSearchEntity.CampaignCode);
                                            else
                                                ErrorMessageForAlternateSearch(sb, AppConstants.NO_HOTELS_WITH_DNUMBER_MSG, string.Empty);
                                            sb.Append("<removeDiscountTypeSort>");
                                            sb.Append("true");
                                            sb.Append("</removeDiscountTypeSort>");
                                        }
                                    }
                                    break;
                                case SearchType.REGULAR:
                                    if (null != hotelSearchEntity.CampaignCode && !SelectHotelUtil.HasHotelsWithPromotionRates())
                                    {
                                        promoSearchShowHotelsWithPublicRates = true;
                                        if (!AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                                        {
                                            showSearchHotelWidget = true;
                                            ErrorMessageForAlternateSearch(sb, AppConstants.NO_HOTELS_WITH_PROMOCODE_MSG, hotelSearchEntity.CampaignCode);
                                            sb.Append("<removeDiscountTypeSort>");
                                            sb.Append("true");
                                            sb.Append("</removeDiscountTypeSort>");
                                        }
                                        //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
                                        //SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = null;
                                    }
                                    //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
                                    //else if (null != hotelSearchEntity.CampaignCode)
                                    //{
                                    //    totalHotels = SelectHotelUtil.TotalPromotionRateAvailableHotels();
                                    //}
                                    break;
                                case SearchType.VOUCHER:
                                    if (false == SelectHotelUtil.HasHotelsWithPromotionRates())
                                    {
                                        promoSearchShowHotelsWithPublicRates = true;
                                        if (!AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                                        {
                                            showSearchHotelWidget = true;
                                            if (!string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode))
                                                ErrorMessageForAlternateSearch(sb, AppConstants.NO_HOTELS_WITH_VOUCHERCODE_MSG, SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                                            else
                                                ErrorMessageForAlternateSearch(sb, AppConstants.NO_HOTELS_WITH_VOUCHERCODE_MSG, string.Empty);
                                        }
                                        SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = null;
                                        SearchCriteriaSessionWrapper.SearchCriteria.SearchingType = SearchType.REGULAR;
                                    }
                                    else
                                    {
                                        totalHotels = SelectHotelUtil.TotalPromotionRateAvailableHotels();
                                    }
                                    break;
                            }
                            for (int i = 0; i < hotelListResult.Count; i++)
                            {
                                IHotelDetails hotel = hotelListResult[i];
                                //Rakesh - Change for SCANMOB-727 CR
                                TripAdvisorPlaceEntity TAEntity = WebUtil.GetTAReviewEntityForHotel(hotel.HotelDestination.TALocationID);
                                hotel.HideHotelTARating = hotel.HotelDestination != null ? hotel.HotelDestination.HideHotelTARating : false;
                                if (!hotel.HideHotelTARating)
                                    hotel.TripAdvisorHotelRating = TAEntity != null ? TAEntity.AverageRating : 0.0;
                                else
                                    hotel.TripAdvisorHotelRating = 0.0;

                            }
                        }

                        sb.Append("<totalhotels>");
                        sb.Append(totalHotels);
                        sb.Append("</totalhotels>");

                        AppLogger.LogInfoMessage("sb.Append( Hotel data from session...");

                        if (hotelSearchEntity != null && !string.IsNullOrEmpty(hotelSearchEntity.CampaignCode) &&
                                                hotelSearchEntity.SearchingType == SearchType.REGULAR &&
                                                hotelSearchEntity.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City)
                        {
                            //hotelListResult = SelectHotelUtil.SortHotelByDiscountStatus(hotelListResult);
                            SelectHotelUtil.SortByDiscountTypeAndRate(hotelListResult);
                            //hotelListResult.Sort(new PromoComparer());

                        }
                        else
                        {
                            SelectHotelUtil.SortCityCenterDistance(hotelListResult);
                        }
                        int hotelsAdded = 0;
                        PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage);
                        PageReference pageLink = rootPage[EpiServerPageConstants.SELECT_HOTEL_PAGE] as PageReference;
                        PageData referencedPage = DataFactory.Instance.GetPage(pageLink);

                        bool paginationEnabled = ConfigurationManager.AppSettings["SelectHotel.PaginationEnabled"] != null ? Convert.ToBoolean(ConfigurationManager.AppSettings["SelectHotel.PaginationEnabled"]) : true;

                        sb.Append("<paginationEnabled>");
                        sb.Append(paginationEnabled.ToString(CultureInfo.InvariantCulture).ToLower());
                        sb.Append("</paginationEnabled>");

                        int perPageHotelCount = 0;
                        if (!paginationEnabled)
                        {
                            perPageHotelCount = totalHotels;
                        }
                        else
                        {
                            perPageHotelCount = (int)referencedPage["PerPageHotelCount"];
                            if (perPageHotelCount <= 0)
                            {
                                perPageHotelCount = AppConstants.HOTELS_PER_PAGE;
                            }
                        }
                        for (int ctr = 0; ctr < hotelListResult.Count && hotelsAdded <= perPageHotelCount; ctr++)
                        {
                            try
                            {
                                IHotelDetails hotel = hotelListResult[ctr];
                                SearchType searchType = hotelSearchEntity.SearchingType;
                                IHotelDisplayInformation hotelDisplay =
                                    SelectHotelUtil.GetHotelDisplayObject(searchType, hotel, hotelSearchEntity.CampaignCode,
                                                                          promoSearchShowHotelsWithPublicRates, false);

                                string CityCenterDistanceString =
                                            WebUtil.GetTranslatedText(
                                                "/bookingengine/booking/selecthotel/citycenterdistance");
                                hotelDisplay.CityCenterDistance = string.Format(CityCenterDistanceString,
                                                                                hotel.HotelDestination.
                                                                                    CityCenterDistance,
                                                                                hotel.HotelDestination.HotelAddress.
                                                                                    City);

                                if (null != hotelDisplay && hotelDisplay.XML != string.Empty)
                                {
                                    ++hotelsAdded;
                                    sb.Append(hotelDisplay.XML);
                                }
                                if (hotelDisplay != null && string.IsNullOrEmpty(hotelDisplay.Price))
                                    UserNavTracker.LogTrackedData(string.Format(TrackerConstants.NO_RATE_RETURNED, hotelDisplay.ID, hotelDisplay.HotelName));
                            }
                            catch (ContentDataAccessException ex)
                            {
                                if (string.Equals(ex.ErrorCode, AppConstants.BONUS_CHEQUE_NOT_FOUND))
                                {
                                    sb.Append("<isbonuschequenotfounderror>");
                                    sb.Append(AppConstants.TRUE);
                                    sb.Append("</isbonuschequenotfounderror>");
                                    sb.Append("<error>");
                                    sb.Append(
                                       WebUtil.GetTranslatedText(
                                           "/scanweb/bookingengine/errormessages/businesserror/bonuschequenotfound"));
                                    sb.Append("</error>");
                                }
                            }

                        }
                        if (AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone && hotelListResult.Count > 0)
                        {
                            sb.Append("<error>");
                            if (hotelSearchEntity != null && hotelSearchEntity.CampaignCode != null
                                && hotelSearchEntity.SearchingType == SearchType.REGULAR && hotelSearchEntity.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City)
                            {
                                if (SelectHotelUtil.TotalPromotionRateAvailableHotels() > 0)
                                    sb.Append(string.Format(WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE1), hotelSearchEntity.CampaignCode));
                                else
                                    sb.Append(string.Format(WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE2), hotelSearchEntity.CampaignCode));
                                showSearchHotelWidget = true;
                            }
                            else
                                sb.Append(string.Format(WebUtil.GetTranslatedText(AppConstants.ALT_CITY_HOTELS_MSG),
                                                    hotelSearchEntity.SearchedFor.SearchString));
                            sb.Append("</error>");
                            sb.Append("<altCityHotelsfound>true</altCityHotelsfound>");
                        }
                        if (AvailabilityController.operaStatus == AppConstants.OPERANOTCONNECTED)
                        {
                            isOperaConnected = false;
                            sb.Append("<isOperaconnected>");
                            sb.Append(isOperaConnected);
                            sb.Append("</isOperaconnected>");
                        }
                        else
                        {
                            isOperaConnected = true;
                            sb.Append("<isOperaconnected>");
                            sb.Append(isOperaConnected);
                            sb.Append("</isOperaconnected>");
                        }
                        sb.Append("<showSearchHotelWidget>");
                        sb.Append(showSearchHotelWidget.ToString(CultureInfo.InvariantCulture).ToLower());
                        sb.Append("</showSearchHotelWidget>");
                        //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
                        OperaStatus(sb);
                        sb.Append("</hotels>");
                    }
                }

                result = sb.ToString();
                AppLogger.LogInfoMessage("Hotels XML: " + result);
            }
            catch (OWSException owsEx)
            {
                AppLogger.LogCustomException(owsEx, AppConstants.OWS_EXCEPTION);
                ExceptionMsg = WebUtil.GetTranslatedText(owsEx.TranslatePath);
                return GetErrMessage(ExceptionMsg, false);
            }
            catch (BusinessException busEx)
            {
                AppLogger.LogCustomException(busEx, AppConstants.BUSINESS_EXCEPTION);
                ExceptionMsg = WebUtil.GetTranslatedText(busEx.TranslatePath);
                return GetErrMessage(ExceptionMsg, false);
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx);
                return GetErrMessage(string.Empty, true);
            }
            return result;
        }

        private void ErrorMessageForAlternateSearch(StringBuilder sb, string errorMsg, string replacementText)
        {
            if (!AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
            {
                sb.Append("<error>");
                sb.Append(string.Format(WebUtil.GetTranslatedText(errorMsg), replacementText));
                sb.Append("</error>");
            }
        }

        private void OperaStatus(StringBuilder sb)
        {
            if (AvailabilityController.operaStatus == AppConstants.OPERANOTCONNECTED)
            {
                isOperaConnected = false;
                sb.Append("<isOperaconnected>" + isOperaConnected + "</isOperaconnected>");
            }
            else
            {
                isOperaConnected = true;
                sb.Append("<isOperaconnected>" + isOperaConnected + "</isOperaconnected>");
            }
        }
        #endregion getAvailableHotels

        #region GetAvailabilityCalendars

        /// <summary>
        /// fetches the availibility control[Arrival date, Departuredate and Minimum base rate according to these occupancy dates]
        /// </summary>
        /// <returns></returns>
        private string GetAvailabilityCalendars(bool FirstTimeCall)
        {
            string result = string.Empty;
            AppLogger.LogInfoMessage("In GetAvailibilityRateCalendars()");
            string ExceptionMsg = string.Empty;
            StringBuilder sbAvailibilityRateCalenders = new StringBuilder();
            AvailabilityCalendarEntity availabilityCalendar = Reservation2SessionWrapper.AvailabilityCalendar;
            int indexIncremental = 5;
            int leftIndex = 0;
            int rightIndex;
            int.TryParse(HygieneSessionWrapper.RightCarouselIdx, out rightIndex);
            SearchState srchState = SearchState.PROGRESS;
            leftIndex = rightIndex;
            rightIndex += indexIncremental;
            if (rightIndex >= availabilityCalendar.RightCarouselIndex)
            {
                rightIndex = availabilityCalendar.RightCarouselIndex;
                srchState = availabilityCalendar.SearchState;
            }
            else
            {
                HygieneSessionWrapper.RightCarouselIdx = rightIndex.ToString();
            }
            try
            {
                if (availabilityCalendar != null && availabilityCalendar.ListAvailCalendarItems != null
                    && availabilityCalendar.ListAvailCalendarItems.Count > 0)
                {
                    sbAvailibilityRateCalenders.Append("<AvailabilityCalenderItems>");
                    sbAvailibilityRateCalenders.Append("<LeftCarouselIndex>");
                    sbAvailibilityRateCalenders.Append(leftIndex);
                    sbAvailibilityRateCalenders.Append("</LeftCarouselIndex>");
                    sbAvailibilityRateCalenders.Append("<RightCarouselIndex>");
                    sbAvailibilityRateCalenders.Append(rightIndex);
                    sbAvailibilityRateCalenders.Append("</RightCarouselIndex>");
                    sbAvailibilityRateCalenders.Append("<LeftVisibleCarouselIndex>");
                    sbAvailibilityRateCalenders.Append(availabilityCalendar.LeftVisibleCarouselIndex);
                    sbAvailibilityRateCalenders.Append("</LeftVisibleCarouselIndex>");
                    sbAvailibilityRateCalenders.Append("<RightVisibleCarouselIndex>");
                    sbAvailibilityRateCalenders.Append(availabilityCalendar.RightVisibleCarouselIndex);
                    sbAvailibilityRateCalenders.Append("</RightVisibleCarouselIndex>");
                    sbAvailibilityRateCalenders.Append("<selectedItemIndex>");
                    sbAvailibilityRateCalenders.Append(availabilityCalendar.SelectedItemIndex);
                    sbAvailibilityRateCalenders.Append("</selectedItemIndex>");
                    sbAvailibilityRateCalenders.Append("<SearchState>");
                    sbAvailibilityRateCalenders.Append(srchState);
                    sbAvailibilityRateCalenders.Append("</SearchState>");

                    SetRateStringForCalendarItems(SearchCriteriaSessionWrapper.SearchCriteria, leftIndex, rightIndex,
                                                  availabilityCalendar);

                    IList<AvailCalendarItemEntity> availabilityCalendarList =
                        availabilityCalendar.ListAvailCalendarItems;
                    for (int iterator = leftIndex; iterator <= rightIndex; iterator++)
                    {
                        AvailCalendarItemEntity availCalendarItemEntity = availabilityCalendarList[iterator];
                        if (!availCalendarItemEntity.IsSearchDone)
                        {
                            HygieneSessionWrapper.RightCarouselIdx = iterator.ToString();
                            break;
                        }
                        availCalendarItemEntity.IsPerStaySelectedInSelectRatePage =
                            Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage;
                        string availabilityCalenderItemXML = availCalendarItemEntity.XML;
                        if (availabilityCalenderItemXML != null && !string.IsNullOrEmpty(availabilityCalenderItemXML))
                        {
                            sbAvailibilityRateCalenders.Append("<AvailabilityCalenderItem>");
                            sbAvailibilityRateCalenders.Append(availabilityCalenderItemXML);
                            sbAvailibilityRateCalenders.Append("</AvailabilityCalenderItem>");
                        }
                    }
                    if (rightIndex >= availabilityCalendar.RightCarouselIndex)
                    {
                        HygieneSessionWrapper.RightCarouselIdx = "0";
                    }
                    sbAvailibilityRateCalenders.Append("</AvailabilityCalenderItems>");
                    result = sbAvailibilityRateCalenders.ToString();
                }
                else
                {
                    sbAvailibilityRateCalenders.Append("<error>" +
                                                       WebUtil.GetTranslatedText(
                                                           "No rates available for selected search criteria") +
                                                       "</error>");
                }
            }
            catch (OWSException owsEx)
            {
                AppLogger.LogCustomException(owsEx, AppConstants.OWS_EXCEPTION);
                ExceptionMsg = WebUtil.GetTranslatedText(owsEx.TranslatePath);
                return GetErrMessage(ExceptionMsg, false);
            }
            catch (BusinessException busEx)
            {
                AppLogger.LogCustomException(busEx, AppConstants.BUSINESS_EXCEPTION);
                ExceptionMsg = WebUtil.GetTranslatedText(busEx.TranslatePath);
                return GetErrMessage(ExceptionMsg, false);
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx);
                return GetErrMessage(string.Empty, true);
            }
            return result;
        }

        /// <summary>
        /// Gets hotel image details. 
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        private string GetImageDetailsForHotel(string hotelId, string hotlName)
        {
            string result = string.Empty;
            AppLogger.LogInfoMessage("In GetImageDetailsForHotel()");
            string ExceptionMsg = string.Empty;
            try
            {
                HotelDestination hotelDestination = ContentDataAccess.GetHotelDestinationImageDetails(hotelId, hotlName);
                result = CreateHtmlImageData(hotelDestination);
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx);
                return GetErrMessage(string.Empty, true);
            }
            return result;
        }

        private int ProcessImageDetails(StringBuilder htmlString1, HotelDestination hotelDestination, string imageType)
        {
            var imageCount = 0;
            int maxGenealImages = 10;
            string imageHtml;
            string imageIndex;
            if (hotelDestination != null)
            {
                for (var index = 1; index <= maxGenealImages; index++)
                {
                    imageIndex = string.Concat(imageType, index);

                    if (!string.IsNullOrEmpty(hotelDestination.ImageDetailsList[imageIndex].ImageUrl))
                    {
                        var langAltText = new ImageAltText();
                        var altText = langAltText.GetAltText(hotelDestination.ImageDetailsList[imageIndex].CMSLanguageId, 
                                                                    hotelDestination.ImageDetailsList[imageIndex].CMSImagePath);

                        imageHtml = string.Format("<img alt='{1}' src={0} title='{1}'>",
                                                  hotelDestination.ImageDetailsList[imageIndex].ImageUrl, altText);

                        if (!string.IsNullOrEmpty(hotelDestination.ImageDetailsList[imageIndex].ImageTitle)
                            && hotelDestination.ImageDetailsList[imageIndex].ImageTitle.Length > 60)
                        {
                            hotelDestination.ImageDetailsList[imageIndex].ImageTitle =
                                hotelDestination.ImageDetailsList[imageIndex].ImageTitle.Remove(60);
                        }
                        htmlString1.Append("<li><div class='galleryImageContainer'>");
                        htmlString1.Append(imageHtml);
                        htmlString1.Append("</div><div class='galleryHeading'><h2 class='imageTitle'>");
                        htmlString1.Append(hotelDestination.ImageDetailsList[imageIndex].ImageTitle);
                        htmlString1.Append("</h2><div class='clearAll'>&#160;</div></div>");
                        htmlString1.Append("<div class='galleryContentContainer'>");
                        htmlString1.Append(hotelDestination.ImageDetailsList[imageIndex].ImageDescription);
                        htmlString1.Append("</div></li>");

                        imageCount++;
                    }
                }
            }

            return imageCount;
        }

        private int Process360Image(StringBuilder htmlString2, HotelDestination hotelDestination, string imageType)
        {
            string[] imageUrlInfo = null;
            string imageHtml2 = null;
            const int max360Images = 4;
            int image360Count = 0;
            string imageIndex = imageType;

            if (hotelDestination != null)
            {
                for (var index = 1; index <= max360Images; index++)
                {
                    imageIndex = index != 1 ? string.Concat(imageType, index) : imageIndex;
                    imageUrlInfo = null;
                    if (!string.IsNullOrEmpty(hotelDestination.ImageDetailsList[imageIndex].ImageUrl))
                        imageUrlInfo = hotelDestination.ImageDetailsList[imageIndex].ImageUrl.Split(',');

                    imageHtml2 = CreateImageUrlScript(imageUrlInfo);

                    if ((!string.IsNullOrEmpty(imageHtml2) && !string.IsNullOrEmpty(imageHtml2.Trim())))
                    {
                        if (!string.IsNullOrEmpty(hotelDestination.ImageDetailsList[imageIndex].ImageTitle) &&
                            hotelDestination.ImageDetailsList[imageIndex].ImageTitle.Length > 60)
                        {
                            hotelDestination.ImageDetailsList[imageIndex].ImageTitle =
                                hotelDestination.ImageDetailsList[imageIndex].ImageTitle.Remove(60);
                        }
                        if (!string.IsNullOrEmpty(hotelDestination.ImageDetailsList[imageIndex].ImageDescription) &&
                            hotelDestination.ImageDetailsList[imageIndex].ImageDescription.Length > 500)
                        {
                            hotelDestination.ImageDetailsList[imageIndex].ImageDescription =
                                hotelDestination.ImageDetailsList[imageIndex].ImageDescription.Remove(500);
                        }

                        Append360ImageUrlTags(htmlString2, imageHtml2, hotelDestination, imageIndex);

                        image360Count++;
                    }
                }
            }

            return image360Count;
        }
        private void Append360ImageUrlTags(StringBuilder htmlString2, string imageHtml2, HotelDestination hotelDestination, string imageIndex)
        {
            htmlString2.Append("<li>");
            htmlString2.Append(imageHtml2);
            htmlString2.Append("<div class='galleryHeading'><h2>");
            htmlString2.Append(hotelDestination.ImageDetailsList[imageIndex].ImageTitle);
            htmlString2.Append("</h2><div class='galleryCopyright'>");
            htmlString2.Append(hotelDestination.ImageDetailsList[imageIndex].ByLineText);
            htmlString2.Append("</div><div class='clearAll'>&#160;</div></div>");
            htmlString2.Append("<div class='galleryContentContainer'>");
            htmlString2.Append(hotelDestination.ImageDetailsList[imageIndex].ImageDescription);
            htmlString2.Append("</div></li>");
        }
        private string CreateImageUrlScript(string[] imageUrlInfo)
        {

            var imageHtml2 = new StringBuilder();
            if (imageUrlInfo != null && imageUrlInfo[0] != "0")
            {
                imageHtml2.Append(
                    "<div class='objectContainer'><object height=\"304\" width=\"474\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\" classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\"><param value=");

                imageHtml2.Append("/ImageVault/Images/id_");
                imageHtml2.Append(imageUrlInfo[0]);
                imageHtml2.Append("/ImageVaultHandler.aspx");
                imageHtml2.Append(
                    " name=\"src\"/><param value=\"true\" name=\"controller\"/><object height=\"304\" width=\"474\" class=\"mov\" data=");
                imageHtml2.Append("/ImageVault/Images/id_");
                imageHtml2.Append(imageUrlInfo[0]);
                imageHtml2.Append("/ImageVaultHandler.aspx");
                imageHtml2.Append(
                    " type=\"video/quicktime\"><param value=\"true\" name=\"controller\"/><param name=\"wmode\" value=\"opaque\" /></object></object></div>");
            }
            return imageHtml2.ToString();
        }
        /// <summary>
        /// This method creates string containing the image details in the html format
        /// </summary>
        /// <param name="hotelDestination"></param>
        /// <returns></returns>
        private string CreateHtmlImageData(HotelDestination hotelDestination)
        {
            StringBuilder htmlString1 = new StringBuilder();
            StringBuilder InnerDiv = new StringBuilder();
            int imageCount = 0;
            int image360Count = 0;
            if (hotelDestination != null)
            {
                imageCount += ProcessImageDetails(htmlString1, hotelDestination, "GeneralImage");
                imageCount += ProcessImageDetails(htmlString1, hotelDestination, "RoomImage");
                imageCount += ProcessImageDetails(htmlString1, hotelDestination, "RestBarImage");
                imageCount += ProcessImageDetails(htmlString1, hotelDestination, "LeisureImage");

                InnerDiv.Append("<div class=\"tabCntntt images\" style=\"clear: both;\">");
                InnerDiv.Append("<a class=\"jqmClose scansprite blkClose\" href=\"#\"></a>");
                InnerDiv.Append("<div class=\"spriteIcon m_PrevBtn\"></div>");
                InnerDiv.Append("<div class=\"spriteIcon m_NextBtn\"></div>");
                InnerDiv.Append("<div class=\"LightBoxCntnt\">");
                InnerDiv.Append("<ul id=\"bedTypeList\" runat=\"server\">");
                InnerDiv.Append(htmlString1);
                InnerDiv.Append("</ul>");
                InnerDiv.Append("</div>");
                InnerDiv.Append("</div>");

                InnerDiv.Append("<script type='text/javascript'>");
                InnerDiv.Append("$(function(){");
                InnerDiv.Append("$('.galleryImageContainer img').resizeImg({");
                InnerDiv.Append("scale: 0.95,");
                InnerDiv.Append("maxWidth: 472,");
                InnerDiv.Append("maxHeight: 308");
                InnerDiv.Append("});");
                InnerDiv.Append("});");
                InnerDiv.Append("</script>");

                StringBuilder htmlString2 = new StringBuilder();

                image360Count = Process360Image(htmlString2, hotelDestination, "360Image");

                InnerDiv.Append("<div class=\"tabCntntt galleryView\" style = \"display: none; clear: both;\">");
                InnerDiv.Append("<a class=\"jqmClose scansprite blkClose\" href = \"#\"></a>");
                InnerDiv.Append("<div class=\"spriteIcon m_PrevBtnGallery\"></div>");
                InnerDiv.Append("<div class=\"spriteIcon m_NextBtnGallery\"></div>");
                InnerDiv.Append("<div class=\"LightBoxCntnt\">");
                InnerDiv.Append("<ul id=\"bedTypeList1\" runat=\"server\">");
                InnerDiv.Append(htmlString2);
                InnerDiv.Append("</ul>");
                InnerDiv.Append("</div>");
                InnerDiv.Append("</div>");
                InnerDiv.Append("<input type=\"hidden\" id=\"image360Count\" value=\"" + image360Count +
                                "\" runat=\"server\" />");
                InnerDiv.Append("<input type=\"hidden\" id=\"imageCount\" value=\"" + imageCount +
                                "\" runat=\"server\" />");
            }
            return InnerDiv.ToString();
        }

        private void SetRateStringForCalendarItems(HotelSearchEntity search, int leftIndex,
                                                   int rightIndex, AvailabilityCalendarEntity availabilityCalendar)
        {
            IHotelDisplayInformation hotelDisplayPerNight, hotelDisplayPerStay;
            for (int iterator = leftIndex; iterator <= rightIndex; iterator++)
            {
                AvailCalendarItemEntity calendarItem = availabilityCalendar.ListAvailCalendarItems[iterator];
                if (calendarItem != null && calendarItem.IsSearchDone)
                {
                    StringBuilder sbMinPerNightRateString = new StringBuilder();
                    StringBuilder sbMinPerStayRateString = new StringBuilder();

                    if (calendarItem.IsRatesAvailable && calendarItem.HotelDetails != null)
                    {
                        hotelDisplayPerNight =
                            SelectHotelUtil.GetHotelDisplayObject(search.SearchingType,
                                                                  calendarItem.HotelDetails as IHotelDetails,
                                                                  search.CampaignCode, false, false);
                        hotelDisplayPerStay =
                            SelectHotelUtil.GetHotelDisplayObject(search.SearchingType,
                                                                  calendarItem.HotelDetails as IHotelDetails,
                                                                  search.CampaignCode, false, true);

                        if (search != null && search.ListRooms.Count > 0 && search.SearchingType == SearchType.VOUCHER)
                        {
                            if (hotelDisplayPerNight != null)
                            {
                                if (!string.IsNullOrEmpty(hotelDisplayPerNight.Price) &&
                                    (hotelDisplayPerNight.Price != AppConstants.SPACE))
                                {
                                    sbMinPerNightRateString.Append(
                                        WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher"));
                                }
                            }
                            if (hotelDisplayPerStay != null)
                            {

                                if (!string.IsNullOrEmpty(hotelDisplayPerStay.Price) &&
                                    (hotelDisplayPerStay.Price != AppConstants.SPACE))
                                {
                                    sbMinPerStayRateString.Append(
                                        WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher"));
                                }
                            }
                        }
                        else
                        {
                            if (hotelDisplayPerNight != null)
                            {
                                if (!string.IsNullOrEmpty(hotelDisplayPerNight.Price) &&
                                    (hotelDisplayPerNight.Price != AppConstants.SPACE))
                                {
                                    if (!BookingEngineSessionWrapper.HideARBPrice)
                                    {
                                        sbMinPerNightRateString.Append(
                                            WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from"));
                                        sbMinPerNightRateString.Append(AppConstants.SPACE);
                                    }
                                    sbMinPerNightRateString.Append(hotelDisplayPerNight.Price);
                                }
                            }
                            if (hotelDisplayPerStay != null)
                            {
                                if (!string.IsNullOrEmpty(hotelDisplayPerStay.Price) &&
                                    (hotelDisplayPerStay.Price != AppConstants.SPACE))
                                {
                                    if (!BookingEngineSessionWrapper.HideARBPrice)
                                    {
                                        sbMinPerStayRateString.Append(
                                            WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from"));
                                        sbMinPerStayRateString.Append(AppConstants.SPACE);
                                    }
                                    sbMinPerStayRateString.Append(hotelDisplayPerStay.Price);
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(sbMinPerNightRateString.ToString()))
                        {
                            calendarItem.MinPerNightRateString = sbMinPerNightRateString.ToString();
                        }
                        else
                        {
                            calendarItem.MinPerNightRateString =
                                WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/NotAvailable");
                        }

                        if (!string.IsNullOrEmpty(sbMinPerStayRateString.ToString()))
                        {
                            calendarItem.MinPerStayRateString = sbMinPerStayRateString.ToString();
                        }
                        else
                        {
                            calendarItem.MinPerStayRateString =
                                WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/NotAvailable");
                        }
                    }
                    else
                    {
                        calendarItem.MinPerNightRateString =
                            WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/NotAvailable");
                        calendarItem.MinPerStayRateString =
                            WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/NotAvailable");
                    }
                }
            }
        }

        /// <summary>
        /// This method fetches the minimumrate from opera and updates the List in the session.
        /// </summary>
        /// <param name="search"></param>
        private void SetRateStringForCalendarItems(HotelSearchEntity search)
        {
            IHotelDisplayInformation hotelDisplayPerNight, hotelDisplayPerStay;
            AvailabilityCalendarEntity availabilityCalendar = Reservation2SessionWrapper.AvailabilityCalendar;
            if (search != null && availabilityCalendar != null && availabilityCalendar.ListAvailCalendarItems != null)
            {
                for (int iterator = availabilityCalendar.LeftCarouselIndex; iterator <= availabilityCalendar.RightCarouselIndex; iterator++)
                {
                    StringBuilder sbMinPerNightRateString = new StringBuilder();
                    StringBuilder sbMinPerStayRateString = new StringBuilder();

                    AvailCalendarItemEntity calendarItem = availabilityCalendar.ListAvailCalendarItems[iterator];
                    if (calendarItem != null && calendarItem.IsSearchDone)
                    {
                        if (calendarItem.IsRatesAvailable && calendarItem.HotelDetails != null)
                        {
                            hotelDisplayPerNight = SelectHotelUtil.GetHotelDisplayObject(search.SearchingType, calendarItem.HotelDetails as IHotelDetails, search.CampaignCode, /*promoSearchShowHotelsWithPublicRates*/false, false/*Base|Total Rate*/);
                            hotelDisplayPerStay = SelectHotelUtil.GetHotelDisplayObject(search.SearchingType, calendarItem.HotelDetails as IHotelDetails, search.CampaignCode, /*promoSearchShowHotelsWithPublicRates*/false, true/*Base|Total Rate*/);

                            if (search != null && search.ListRooms.Count > 0 && search.SearchingType == SearchType.VOUCHER)
                            {
                                //display text "Gift Voucher" in availability Calender when booking type is voucher
                                if (!string.IsNullOrEmpty(hotelDisplayPerNight.Price) && (hotelDisplayPerNight.Price != AppConstants.SPACE))
                                {
                                    sbMinPerNightRateString.Append(WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher"));
                                }

                                if (!string.IsNullOrEmpty(hotelDisplayPerStay.Price) && (hotelDisplayPerStay.Price != AppConstants.SPACE))
                                {
                                    sbMinPerStayRateString.Append(WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher"));
                                }
                            }
                            else
                            {
                                //hotelDisplayPerNight.Price holds space when Minimum Rate and currency code is null. hence filter out the character space.
                                if (!string.IsNullOrEmpty(hotelDisplayPerNight.Price) && (hotelDisplayPerNight.Price != AppConstants.SPACE))
                                {
                                    if (!BookingEngineSessionWrapper.HideARBPrice)
                                    {
                                        sbMinPerNightRateString.Append(WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from"));
                                        sbMinPerNightRateString.Append(AppConstants.SPACE);
                                    }
                                    sbMinPerNightRateString.Append(hotelDisplayPerNight.Price);
                                }
                                //hotelDisplayPerStay.Price holds space when Minimum Rate and currency code is null. hence filter out the character space.
                                if (!string.IsNullOrEmpty(hotelDisplayPerStay.Price) && (hotelDisplayPerStay.Price != AppConstants.SPACE))
                                {
                                    if (!BookingEngineSessionWrapper.HideARBPrice)
                                    {
                                        sbMinPerStayRateString.Append(WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from"));
                                        sbMinPerStayRateString.Append(AppConstants.SPACE);
                                    }

                                    sbMinPerStayRateString.Append(hotelDisplayPerStay.Price);
                                }
                            }

                            //defect artf1152944 - mayur - replaced empty rates with Not available string for the selected locale
                            if (!string.IsNullOrEmpty(sbMinPerNightRateString.ToString()))
                            {
                                calendarItem.MinPerNightRateString = sbMinPerNightRateString.ToString();
                            }
                            else
                            {
                                //When no rate is available, set 'Notavailable' to MinimumRate
                                calendarItem.MinPerNightRateString = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/NotAvailable");

                            }

                            //defect artf1152944 - mayur - replaced empty rates with Not available string for the selected locale
                            if (!string.IsNullOrEmpty(sbMinPerStayRateString.ToString()))
                            {
                                calendarItem.MinPerStayRateString = sbMinPerStayRateString.ToString();
                            }
                            else
                            {
                                //When no rate is available, set 'Notavailable' to MinimumRate
                                calendarItem.MinPerStayRateString = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/NotAvailable");
                            }

                        }
                        else
                        {
                            //When no rate is available, set 'Notavailable' to MinimumRate
                            calendarItem.MinPerNightRateString = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/NotAvailable");
                            calendarItem.MinPerStayRateString = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/NotAvailable");
                        }
                    }
                }
            }
        }

        #endregion

        #region PRIVATE METHODS

        /// <summary>
        /// Saves credit card details from current user profile session
        /// </summary>
        /// <returns></returns>
        private string SaveCreditCard()
        {
            return mNameController.SaveCreditCard();
        }

        /// <summary>
        /// Deletes credit card information using the credit card opera id
        /// </summary>
        /// <param name="creditCardOperaId"></param>
        /// <returns></returns>
        private string DeleteCreditCard(long creditCardOperaId)
        {
            string responseHTML = string.Empty;

            try
            {
                var loyaltyDomain = new LoyaltyDomain();

                if (loyaltyDomain.DeleteCreditCard(creditCardOperaId))
                {
                    var loyaltyDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
                    var creditCards = loyaltyDomain.FetchCreditCardDetailsUserProfile(loyaltyDetails.NameID, true);

                    responseHTML = WebUtil.ConstructCreditCardListHTML(creditCards);
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
                responseHTML = "ERROR";
            }

            return responseHTML;
        }

        /// <summary>
        /// Processes Opera booking calls after nets payment
        /// </summary>
        /// <returns></returns>
        private string GetDynamicContent()
        {
            string transactionID = Request.QueryString["transactionId"];
            string responseCode = Request.QueryString["responseCode"];

            try
            {
                OperaBookingCallsAfterNetsPayment operaBookingCallsAfterNetsPayment = new OperaBookingCallsAfterNetsPayment();
                return operaBookingCallsAfterNetsPayment.ProcessOperaBookingCallsAfterNetsPayment(transactionID, responseCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Show Reservation Number if Error Occurs
        /// </summary>
        /// <returns></returns>
        private string CheckReservationStatus()
        {
            string reservationNumber = Request.QueryString["reservationNumber"];
            if (ReservationNumberSessionWrapper.IsReservationDone != null &&
                ReservationNumberSessionWrapper.IsReservationDone &&
                HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null &&
                SearchCriteriaSessionWrapper.SearchCriteria != null &&
                GuestBookingInformationSessionWrapper.AllGuestsBookingInformations != null)
            {
                try
                {
                    OperaBookingCallsAfterNetsPayment obj = new OperaBookingCallsAfterNetsPayment();
                    obj.SendConfirmationToGuest();
                    obj.SendMailConfirmationToGuestsUsingHTMLString();
                }
                catch (Exception ex)
                {
                    AppLogger.LogOnlinePaymentFatalException(ex, String.Format("{0}{1}{2}{3}{4}",
                        ex.Message,
                        "FAILED TO SEND MAIL DURING THE AJAX CALL | PAYMENT PROCESSING PAGE STUCK FOR RESERVATION NUMBER: ",
                        ReservationNumberSessionWrapper.ReservationNumber,
                        "AND PAYMENT TRANSACTION ID: ", Reservation2SessionWrapper.PaymentTransactionId));
                }

                return WebUtil.GetSecureUrl(GlobalUtil.GetUrlToPage(EpiServerPageConstants.BOOKING_CONFIRMATION_PAGE));
            }
            else
            {
                return String.Empty;
            }
        }

        private string ClearBlockedRooms()
        {
            WebUtil.ClearSelectedRateSessionData();
            return "true";
        }

        #region Utility Methods

        #region IsStillMoreHotelsToFetch
        private bool IsStillMoreHotelsToFetch()
        {
            Scanweb.Core.AppLogger.LogInfoMessage("In side IsStillMoreHotelsToFetch");
            int regionalAvailCount = TotalRegionalAvailableHotelsSessionWrapper.TotalRegionalAvailableHotels;
            int generalAvailCount = TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels;

            Scanweb.Core.AppLogger.LogInfoMessage("regionalAvailCount" + regionalAvailCount.ToString());
            Scanweb.Core.AppLogger.LogInfoMessage("generalAvailCount" + generalAvailCount.ToString());
            if (regionalAvailCount == generalAvailCount ||
                regionalAvailCount == int.MinValue ||
                generalAvailCount == int.MinValue)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion IsStillMoreHotelsToFetch

        #region GetErrMessage

        private string GetErrMessage(string ErrMsg, bool IsRedirectToCommonPage)
        {
            StringBuilder ExceptionBuilder = new StringBuilder();
            string errorPageUrl = string.Empty;

            if (IsRedirectToCommonPage)
            {
                errorPageUrl = WebUtil.GetErrorPageUrl(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                                       WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
            }

            ExceptionBuilder.Append("<error redirect='" + IsRedirectToCommonPage.ToString().ToLower() + "' pageurl='" +
                                    errorPageUrl + "'>");
            ExceptionBuilder.Append(ErrMsg);
            ExceptionBuilder.Append("</error>");
            return ExceptionBuilder.ToString();
        }

        #endregion GetErrMessage

        #region WriteToResponse

        /// <summary>
        /// Writes to response.
        /// </summary>
        /// <param name="responsString">The respons string.</param>
        /// <param name="contentType">Type of the content.</param>
        private void WriteToResponse(string responsString, string contentType)
        {
            try
            {
                Response.Clear();
                Response.ContentType = contentType;
                Response.Write(responsString);
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
            }
            finally
            {
                Response.End();
            }
        }

        #endregion

        #endregion Utility Methods

        #endregion PRIVATE METHODS
    }
}