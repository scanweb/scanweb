using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// ShoppingCartRoomBox
    /// </summary>
    public partial class ShoppingCartRoomBox : EPiServer.UserControlBase
    {
        public string RoomNumber { get; set; }

        public string RoomCategory { get; set; }

        public string RoomCategoryID { get; set; }

        public string RateType { get; set; }

        private string bedPreference;

        public string BedPreference
        {
            set { bedPref.Text = value; }
        }

        public string Rate { get; set; }

        public string BaseOrTotalRateText { get; set; }

        public string NoOfAdults { get; set; }

        public string NoOfChildren { get; set; }

        public string ChildrenAccomodationText { get; set; }

        public bool IsCloseButtonEnabled { get; set; }

        public bool IsChildrenAvailable { get; set; }

        public bool IsRateAvailable { get; set; }

        public bool IsRateDisplayed { get; set; }

        private bool isRoomModifiable = true;

        public bool IsRoomModifiable
        {
            get { return isRoomModifiable; }
            set { isRoomModifiable = value; }
        }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string searchData = null;
            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
            {
                searchData = SearchCriteriaSessionWrapper.SearchCriteria.SearchingType.ToString();
            }
            if (searchData == SearchType.BONUSCHEQUE.ToString())
            {
                string cartCostClass = cartCost.Attributes["class"];
                cartCostClass += " cartRoomCostSpecial";
                cartCost.Attributes.Add("class", cartCostClass);
            }
            else if(searchData == SearchType.VOUCHER.ToString())
            {
                string cartCostClass = cartCost.Attributes["class"];
                cartCostClass += !cartCostClass.Contains("voucher") ? " voucher" : string.Empty;
                cartCost.Attributes.Add("class", cartCostClass);
            }

            if (!isRoomModifiable)
                shoppingCartRoomBox.Attributes.Add("class", "thinBrdContainer disableMe");
        }
    }
}