#region Revision history

///  Description					  : This control is used to allow the user to login before doing a booking.
///----------------------------------------------------------------------------------------------------------
/// Author						      : Nandkishore M
/// Creation Date				      : 05-May-2008
///	Version	#					      : 1.0													  
///----------------------------------------------------------------------------------------------------------
/// Revision History				  : R1.3 | CR5 | StoryId: | Login on Booking Detail Screen.
///	Last Modified Date			      :	05-May-2008
///	Modified By                       : Nandkishore M
///	Latest Version                    : 1.1

#endregion

#region References

using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// This control will allow the user to login, in the booking detail screen.
    /// </summary>
    public partial class BookingDetailLogin : EPiServer.UserControlBase
    {
        #region Page Load

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            BDLoginErrorDiv.Visible = false;
            btnBDLogIn.Attributes.Add("onclick", "javascript:GetSiteCatalystForBookingLogin('" + CurrentPage.PageName +
                                      "');return performValidation(PAGE_LOGIN_BOOKING_DETAIL)");

            if (LoginErrorCodeSessionWrapper.LoginErrorCode > 0)
            {
                loginInfo.Attributes.Add("style", "display:block");
                BDLoginErrorDiv.Visible = true;
                LoginErrorCodeSessionWrapper.LoginErrorCode = 0;
            }
            btnBDLogIn.PostBackUrl = WebUtil.LoginValidatorUrl();
            if (!IsPostBack)
            {
                RememberUserIdPwd.Checked = true;

                txtBDLoginPassword.Attributes["rel"] = WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/Password");
                txtBDLoginUserName.Attributes["rel"] = WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/MembershipNo");
                if (!UserLoggedInSessionWrapper.UserLoggedIn)
                {
                    if (WebUtil.IsCookieExists(AppConstants.DONTREMEMBERME_COOKIE))
                        RememberUserIdPwd.Checked = false;
                    else
                        WebUtil.SetCookieToTextBox(txtBDLoginUserNamePrefix, txtBDLoginUserName, txtBDLoginPassword, RememberUserIdPwd);
                }
                btnBDLogIn.Text = WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/loginmessage");
                Utility.SetLoginDemographics(txtBDLoginUserNamePrefix.Name, txtBDLoginUserName.Name, txtBDLoginPassword.UniqueID,
                                             RememberUserIdPwd.Name, LoginSourceModule.BOOKING_DETAIL_MODULE,
                                             loginBookingDetailPage.Name);
                loginBookingDetail.Value = LoginSourceModule.BOOKING_DETAIL_MODULE.ToString();
            }
        }

        #endregion

        #region Login_Click

        /// <summary>
        /// This method will perform the login operation for the user.
        /// </summary>
        /// <param name="sender">Sender name</param>
        /// <param name="e">Event Arguments</param>
        protected void Login_Click(object sender, EventArgs e)
        {
            NameController nameController = new NameController();
            try
            {
                Reservation2SessionWrapper.IsLoginFromBookingDetailsPage = true;
                nameController.Login(txtBDLoginUserName.Value.Trim(), txtBDLoginPassword.Text.Trim());
                Response.Redirect(WebUtil.LoginValidatorUrl());
            }
            catch (OWSException owsEx)
            {
                BDLoginErrorDiv.Visible = true;
                UserLoggedInSessionWrapper.UserLoggedIn = false;
                AppLogger.LogCustomException(owsEx, AppConstants.OWS_EXCEPTION);
            }
            catch (Exception genEx)
            {
                UserLoggedInSessionWrapper.UserLoggedIn = false;
                //R2.2.6 -  App Error Screen - log all errors
                WebUtil.ApplicationErrorLog(genEx);
            }
        }

        #endregion Login_Click

        #region ForgottenPassword_Click

        /// <summary>
        /// This will redirect to forgotten password page where user is able to retrieve forgotten password.
        /// </summary>
        /// <param name="sender">Sender name</param>
        /// <param name="e">Event Arguments</param>
        protected void ForgottenPassword_Click(object sender, EventArgs e)
        {
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.FORGOTTEN_PASSWORD), false);
        }

        #endregion ForgottenPassword_Click
    }
}