////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: Code Behind class for Current booking User Control.     //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Priya Singh                                             //
/// Author email id				:                           							  //
/// Creation Date				: 11th December 2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revision History			:        												  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using System.Linq;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// CurrentBookings
    /// </summary>
    public partial class CurrentBookings : EPiServer.UserControlBase, INavigationTraking
    {

        protected string numberOfBookingRequestOnAjaxCall = "";
        #region Protected Methods

        /// <summary>
        /// Page load for this control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            Reservation2SessionWrapper.IsCurrentBookingsPage = true;
            DivError.Visible = false;
            DivErrorPopup.Visible = false;

            string greetText = string.Empty;
            string firstName = string.Empty;
            string title = string.Empty;
            string surName = string.Empty;
            string teaserTextPropName = string.Empty;

            if (this.Visible)
            {
                if (UserLoggedInSessionWrapper.UserLoggedIn == true)
                {
                    if (!Page.IsPostBack)
                    {
                        string exceptionMsg = string.Empty;
                        try
                        {
                            LoyaltyDetailsEntity loyaltyDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;

                            if (loyaltyDetails != null)
                            {
                                //Added as part of SCANAM-537
                                if (!string.IsNullOrEmpty(loyaltyDetails.NativeFirstName) && !string.IsNullOrEmpty(loyaltyDetails.NativeLastName))
                                {
                                    firstName = loyaltyDetails.NativeFirstName;
                                    surName = loyaltyDetails.NativeLastName;
                                }
                                else
                                {
                                    firstName = loyaltyDetails.FirstName;
                                    surName = loyaltyDetails.SurName;
                                }
                                title = loyaltyDetails.Title;
                                greetText = WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/greet");
                                greetMessage.InnerText = String.Concat(greetText, AppConstants.SPACE,
                                                                       Utility.GetNameWithTitle(title, firstName, surName));

                                string membershipNameID = loyaltyDetails.NameID;
                                string membershipOperaID = string.Empty;
                                hdnMembershipNameID.Value = membershipNameID;
                                membershipOperaID = loyaltyDetails.MembershipID; //Utility.GetMembershipOperaId(membershipNameID);
                                if (membershipOperaID != string.Empty)
                                {
                                    hdnMembershipOperaID.Value = membershipOperaID;
                                    ShowTransactionForDateRange(membershipOperaID, true);
                                }
                                else
                                {
                                    divTransaction.InnerHtml = "<p>" + WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/notransactions") +
                                                               "</p>";
                                }
                                ShowFastTrackEnrolmentMessage();
                            }
                        }
                        catch (OWSException owsEx)
                        {
                            AppLogger.LogCustomException(owsEx, AppConstants.OWS_EXCEPTION);
                            exceptionMsg = WebUtil.GetTranslatedText(owsEx.TranslatePath);
                            errMsg.InnerText = exceptionMsg;
                        }
                        catch (BusinessException busEx)
                        {
                            AppLogger.LogCustomException(busEx, AppConstants.BUSINESS_EXCEPTION);
                            exceptionMsg = WebUtil.GetTranslatedText(busEx.TranslatePath);
                            errMsg.InnerText = exceptionMsg;
                        }
                        catch (Exception genEx)
                        {
                            WebUtil.ApplicationErrorLog(genEx);
                        }
                    }
                }
                else
                {
                    AppLogger.LogInfoMessage(
                        "User is already logged out - Still accessing Account Information Functionality.");
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.LOGIN_FOR_DEEPLINK), false);
                }
            }
        }

        #endregion Protected Methods

        #region Private Methods

        #region ButtonView_Click

        /// <summary>
        /// On clicking View button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonView_Click(object sender, EventArgs e)
        {
            if (!UserLoggedInSessionWrapper.UserLoggedIn)
                Response.Redirect(WebUtil.GetCompletePageUrl(GlobalUtil.GetUrlToPage(EpiServerPageConstants.SESSION_TIMEOUT_PAGE), false));

            try
            {
                if (UserLoggedInSessionWrapper.UserLoggedIn && ValidateForm())
                {
                    int NumberOfRows = tableTransactions.Rows.Count;
                    for (int rowCount = 1; tableTransactions.Rows.Count > rowCount; rowCount++)
                    //for (int rowCount = 0; rowCount < NumberOfRows; rowCount++)
                    {
                        //if (tableTransactions.Rows.Count > rowCount)
                        //{
                        //    if (rowCount != 0)
                        //    {
                        HtmlTableRow htmlRow = tableTransactions.Rows[rowCount];
                        if (htmlRow != null)
                            tableTransactions.Rows.Remove(htmlRow);
                        //    }
                        //}
                    }
                    if (!string.IsNullOrEmpty(hdnMembershipOperaID.Value))
                        ShowTransactionForDateRange(hdnMembershipOperaID.Value.Trim(), false);
                }
            }
            catch (OWSException owsEx)
            {
                AppLogger.LogCustomException(owsEx, AppConstants.OWS_EXCEPTION);
                DivError.Visible = true;
                DivErrorPopup.Visible = true;
                DivError.InnerHtml = WebUtil.GetTranslatedText(owsEx.TranslatePath); ;
            }
            catch (BusinessException busEx)
            {
                AppLogger.LogCustomException(busEx, AppConstants.BUSINESS_EXCEPTION);
                DivError.InnerHtml = WebUtil.GetTranslatedText(busEx.TranslatePath);
                DivError.Visible = true;
                DivErrorPopup.Visible = true;
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
        }

        #endregion ButtonView_Click

        #region FastTrackEnrollmentMessage

        private void ShowFastTrackEnrolmentMessage()
        {
            if (!string.IsNullOrEmpty(FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage))
            {
                DivError.Visible = true;
                DivErrorPopup.Visible = true;
                DivError.InnerHtml = FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage;
                FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage = string.Empty;
            }
        }

        #endregion FastTrackEnrollmentMessage

        #region ValidateForm

        /// <summary>
        /// Validating the each for element entered/modified 
        /// by the user before next search can happen.
        /// </summary>
        /// <returns></returns>
        private bool ValidateForm()
        {
            DateTime fromDate = Convert.ToDateTime(ddlMonthFrom.Items[ddlMonthFrom.SelectedIndex].Value);
            DateTime toDate = Convert.ToDateTime(ddlMonthTo.Items[ddlMonthTo.SelectedIndex].Value);

            if (fromDate > toDate)
            {
                errMsg.InnerText = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/accountinfo_daterange");
                return false;
            }
            else
            {
                errMsg.InnerText = string.Empty;
                return true;
            }
        }

        #endregion ValidateForm

        #region ShowTransactionForDateRange

        /// <summary>
        /// Fetching all transactions of a membershipopera id.
        /// </summary>
        /// <param name="membershipOperaID"></param>
        /// <param name="firstTimeLoad">Checks if called from Pageload or by clicking View button</param>
        private void ShowTransactionForDateRange(string membershipOperaID, bool firstTimeLoad)
        {
            DateTime fromDate;
            DateTime toDate;

            NameController nameController = new NameController();

            LoyaltyAccountInformation accountInfo = nameController.GetAccountInfo(membershipOperaID);

            if (accountInfo != null)
            {
                accountInfo = Sort(accountInfo);

                strngNights.InnerText = AppConstants.SPACE + accountInfo.CardInfo.NoOfStays + AppConstants.SPACE +
                                        WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/Nights1");
                PopulateCardInfo(accountInfo);

                if (firstTimeLoad) SetDateFields(accountInfo);

                if ((ddlMonthFrom.Items.Count > 0) && (ddlMonthTo.Items.Count > 0))
                {
                    fromDate = DateUtil.GetFirstDayOfMonth(Convert.ToDateTime(ddlMonthFrom.Items[ddlMonthFrom.SelectedIndex].Value));
                    toDate = DateUtil.GetLastDayOfMonth(Convert.ToDateTime(ddlMonthTo.Items[ddlMonthTo.SelectedIndex].Value));
                    PopulateTransactions(accountInfo, fromDate, toDate);
                }
                else
                {
                    string noTransactionText = WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/notransactionsAvail");
                    divTransaction.Visible = false;
                    idSubHeading.Visible = false;
                    NoTransaction.Visible = true;
                    idAlertBox.Visible = true;
                    NoTransaction.InnerHtml = noTransactionText;
                }
                UserNavTracker.TrackAction(this, "View Transactions");
                //WebUtil.ApplicationErrorLog(new Exception("View Transactions"));
                //Response.End();
            }
        }

        #endregion ShowTransactionForDateRange

        #region Sort

        /// <summary>
        /// Sorts the entire list of Loyalty transactions based on the fromDate.
        /// </summary>
        /// <param name="accountInfo"></param>
        /// <returns>LoyaltyAccountInformation</returns>
        private LoyaltyAccountInformation Sort(LoyaltyAccountInformation accountInfo)
        {
            List<LoyaltyTransaction> loyaltyTransactions = accountInfo.LoyaltyTransactions;
            if (loyaltyTransactions != null)
            {
                loyaltyTransactions.Sort(new LoyaltyTransactionComparer());
                accountInfo.LoyaltyTransactions = loyaltyTransactions;
            }

            return accountInfo;
        }

        #endregion Sort

        #region SetDateFields

        /// <summary>
        /// Setting the date drop downs based on the transaction dates populated.
        /// </summary>
        /// <param name="accountInfo"></param>
        private void SetDateFields(LoyaltyAccountInformation accountInfo)
        {
            DateTime fromDate;
            DateTime toDate;

            if (accountInfo != null)
            {
                if (accountInfo.LoyaltyTransactions != null)
                {
                    if (accountInfo.LoyaltyTransactions.Count > 0)
                    {
                        List<LoyaltyTransaction> loyaltyTransactions = accountInfo.LoyaltyTransactions;

                        fromDate = loyaltyTransactions[0].FromDate;
                        toDate = loyaltyTransactions[0].ToDate;
                        foreach (LoyaltyTransaction eachTran in loyaltyTransactions)
                        {
                            if (eachTran.FromDate < fromDate)
                                fromDate = eachTran.FromDate;

                            if (eachTran.ToDate > toDate)
                                toDate = eachTran.ToDate;
                        }

                        Dictionary<DateTime, string> allDates = GetMonths(fromDate, toDate);

                        PopulateDateDropDowns(allDates);

                        if (ddlMonthTo.Items.Count > 0)
                        {
                            ddlMonthTo.SelectedIndex = ddlMonthTo.Items.Count - 1;
                        }

                        if (ddlMonthFrom.Items.Count > 0)
                        {
                            if (ddlMonthFrom.Items.Count >= 3)
                            {
                                ddlMonthFrom.SelectedIndex = ddlMonthFrom.Items.Count - 3;
                            }
                            else
                            {
                                ddlMonthFrom.SelectedIndex = 0;
                            }
                        }
                    }
                }
            }
        }

        #endregion SetDateFields

        #region PopulateCardInfo

        /// <summary>
        /// Populates the header info of the account information screen
        /// </summary>
        /// <param name="accountInfo"></param>
        private void PopulateCardInfo(LoyaltyAccountInformation accountInfo)
        {
            if (accountInfo != null)
            {
                if (accountInfo.CardInfo != null)
                {
                    membershipNo.InnerText = accountInfo.CardInfo.MembershipNo;
                    memberLevel.InnerText = GetFriendlyMembershipLevel(accountInfo.CardInfo.Level);
                    totalPoints.InnerText = accountInfo.CardInfo.NoOfPoints;
                }
            }
        }

        #endregion PopulateCardInfo

        #region GetFriendlyMembershipLevel

        /// <summary>
        /// It returns the membership level to be displayed on the screen.
        /// As opera returns memebrshiplevel as one single work without 
        /// any spaces in between, we have to add a space and then show to the user.
        /// </summary>
        /// <param name="operaMembershipLevel"></param>
        /// <returns></returns>
        private string GetFriendlyMembershipLevel(string operaMembershipLevel)
        {
            string friendlyLevel = string.Empty;

            switch (operaMembershipLevel.ToUpper())
            {
                case AppConstants.MEMBERSHIPLEVEL_1STFLOOR:
                    friendlyLevel = "1st floor";
                    break;
                case AppConstants.MEMBERSHIPLEVEL_2NDFLOOR:
                    friendlyLevel = "2nd floor";
                    break;
                case AppConstants.MEMBERSHIPLEVEL_3RDFLOOR:
                    friendlyLevel = "3rd floor";
                    break;
                case AppConstants.MEMBERSHIPLEVEL_TOPFLOOR:
                    friendlyLevel = "Top floor";
                    break;
                default:
                    friendlyLevel = operaMembershipLevel;
                    break;
            }

            return friendlyLevel;
        }

        #endregion GetFriendlyMembershipLevel

        #region PopulateTransactions

        /// <summary>
        /// Fetches each transaction from the accountInfo and updated the transaction table on screen
        /// </summary>
        /// <param name="accountInfo"></param>
        private void PopulateTransactions(LoyaltyAccountInformation accountInfo,
                                          DateTime fromDate, DateTime toDate)
        {

            Hashtable PointsExpiryDateList = new Hashtable();
            if (accountInfo != null)
            {
                if (accountInfo.LoyaltyTransactions != null)
                {
                    if (accountInfo.LoyaltyTransactions.Count <= 0)
                    {
                        string noTransactionText =
                            WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/notransactionsAvail");
                        divTransaction.Visible = false;
                        idSubHeading.Visible = false;
                        NoTransaction.Visible = true;
                        idAlertBox.Visible = true;
                        NoTransaction.InnerHtml = noTransactionText;
                    }
                    else
                    {
                        HtmlTableRow tableRow;
                        LoyaltyTransaction loyaltyTransaction = new LoyaltyTransaction();
                        int slNo = 0;

                        for (int tranCtr = 0; tranCtr < accountInfo.LoyaltyTransactions.Count; tranCtr++)
                        {
                            loyaltyTransaction = accountInfo.LoyaltyTransactions[tranCtr];
                            if (!PointsExpiryDateList.ContainsKey(loyaltyTransaction.ToDate.Year + 3))
                            {
                                PointsExpiryDateList.Add((loyaltyTransaction.ToDate.Year + 3),
                                                         Convert.ToDouble(loyaltyTransaction.Points));
                            }
                            else
                            {
                                PointsExpiryDateList[loyaltyTransaction.ToDate.Year + 3] =
                                    Convert.ToDouble(PointsExpiryDateList[loyaltyTransaction.ToDate.Year + 3].ToString()) +
                                    Convert.ToDouble(loyaltyTransaction.Points);
                            }
                            if ((loyaltyTransaction.ToDate.Date >= fromDate.Date) &&
                                (loyaltyTransaction.ToDate.Date <= toDate.Date))
                            {
                                slNo += 1;
                                tableRow = GetTransactionRow(loyaltyTransaction, slNo);

                                if ((slNo % 2) == 0)
                                {
                                    tableRow.Attributes.Add("class", "odd");
                                }

                                tableTransactions.Rows.Add(tableRow);
                            }
                        }

                        timeFrameFrom.InnerText =
                            WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/timeframefrom");
                        timeFrameTo.InnerText =
                            WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/timeframeto");
                        viewButtonCaption.InnerText =
                            WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/timeframeview");
                    }
                }
                else
                {
                    string noTransactionText =
                        WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/notransactionsAvail");
                    divTransaction.Visible = false;
                    idSubHeading.Visible = false;
                    NoTransaction.Visible = true;
                    idAlertBox.Visible = true;
                    NoTransaction.InnerHtml = noTransactionText;
                }
            }
        }

        #endregion PopulateTransactions

        #region PointsExpiryDates

        /// <summary>
        /// R1.5 | Artifact artf809442 : FGP - Display expiry date for FGP points
        /// Method used to display total points with its expiry dates.
        /// </summary>
        /// <param name="pointsExpiryDatesLst">
        /// List of expiry date and total points expiring on that date
        /// </param>
        private void DisplayPointsExpiryDate(Hashtable pointsExpiryDatesLst)
        {
            HtmlTableRow tableRow;
            if (pointsExpiryDatesLst.Count > 0)
            {
                foreach (int key in pointsExpiryDatesLst.Keys)
                {
                    tableRow = new HtmlTableRow();

                    HtmlTableCell cell1 = new HtmlTableCell();
                    HtmlTableCell cell2 = new HtmlTableCell();
                    DateTime ExpDate = new DateTime(key, 12, 31);
                    string expiredDate = ExpDate.ToString("dd.MM.yyyy");
                    cell1.InnerText = expiredDate;
                    cell2.InnerText = pointsExpiryDatesLst[key].ToString();

                    tableRow.Cells.Add(cell1);
                    tableRow.Cells.Add(cell2);


                }
            }
        }

        #endregion PointsExpiryDates

        #region PopulateDateDropDowns

        /// <summary>
        /// Adding rows to the drop downs with the dates as MMMM YYYY format
        /// </summary>
        /// <param name="allDates"></param>
        private void PopulateDateDropDowns(Dictionary<DateTime, string> allDates)
        {
            ddlMonthFrom.Items.Clear();
            ddlMonthTo.Items.Clear();

            ListItem listItem;

            foreach (DateTime key in allDates.Keys)
            {
                listItem = new ListItem(allDates[key], allDates[key]);
                ddlMonthFrom.Items.Add(listItem);

                listItem = new ListItem(allDates[key], allDates[key]);
                ddlMonthTo.Items.Add(listItem);
            }
        }

        #endregion PopulateDateDropDowns

        #region GetTransactionRow

        /// <summary>
        /// It returns a HTMLTableRow to be added to the 
        /// transactions table for each transaction being passed as parameter.
        /// </summary>
        /// <param name="loyaltyTransaction"></param>
        /// <param name="slNo"></param>
        /// <returns></returns>
        private HtmlTableRow GetTransactionRow(LoyaltyTransaction loyaltyTransaction, int slNo)
        {
            HtmlTableRow tableRow = new HtmlTableRow();

            HtmlTableCell cell1 = new HtmlTableCell();
            HtmlTableCell cell2 = new HtmlTableCell();
            HtmlTableCell cell3 = new HtmlTableCell();
            HtmlTableCell cell4 = new HtmlTableCell();
            HtmlTableCell cell5 = new HtmlTableCell();

            cell1.Width = "30";
            cell1.Align = "right";
            cell1.InnerText = "";
            cell2.Width = "195";
            cell2.Align = "left";

            if (loyaltyTransaction.TransactionInfo == "")
            {
                cell2.InnerHtml = WebUtil.GetTranslatedText(AppConstants.HOTEL_CLOSED);
            }
            else
            {
                HtmlGenericControl htmlSpan1 = new HtmlGenericControl();
                htmlSpan1.InnerHtml = loyaltyTransaction.TransactionInfo;
                cell2.Controls.Add(htmlSpan1);
            }
            cell3.Width = "160";
            HtmlGenericControl htmlSpan2 = new HtmlGenericControl();
            //string fromDate = Core.DateUtil.DateToDDMMYYYYString(loyaltyTransaction.FromDate);
            //string toDate = Core.DateUtil.DateToDDMMYYYYString(loyaltyTransaction.ToDate);
            string fromDate = Core.DateUtil.DateToDDMMMYYYYWords(loyaltyTransaction.FromDate);
            string toDate = Core.DateUtil.DateToDDMMMYYYYWords(loyaltyTransaction.ToDate);
            htmlSpan2.InnerHtml = fromDate + " - " + toDate;
            cell3.Controls.Add(htmlSpan2);
            cell4.Width = "70";
            cell4.Align = "right";
            cell4.Style.Add("padding-right", "5px");
            cell2.Attributes.Add("class", "first");
            cell4.Attributes.Add("class", "last");
            HtmlGenericControl htmlSpan3 = new HtmlGenericControl();
            htmlSpan3.InnerHtml = loyaltyTransaction.Points;
            cell4.Controls.Add(htmlSpan3);
            tableRow.Cells.Add(cell2);
            tableRow.Cells.Add(cell3);
            tableRow.Cells.Add(cell4);
            return tableRow;
        }

        #endregion getTransactionRow

        #region GetMonths

        /// <summary>
        /// It returns the sequence of months in the form of MMMM YYYY between the date range.
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        private Dictionary<DateTime, string> GetMonths(DateTime fromDate, DateTime toDate)
        {
            Dictionary<DateTime, string> allDates = new Dictionary<DateTime, string>();

            try
            {
                for (DateTime eachDate = fromDate; eachDate <= toDate; eachDate = eachDate.AddMonths(1))
                {
                    allDates.Add(eachDate, Core.DateUtil.DateToMMMMYYYYWords(eachDate));
                }

                if (!allDates.ContainsValue(Core.DateUtil.DateToMMMMYYYYWords(toDate)))
                    allDates.Add(toDate, Core.DateUtil.DateToMMMMYYYYWords(toDate));
            }
            catch (ArgumentException argEx)
            {
                AppLogger.LogFatalException(argEx);
            }

            return allDates;
        }

        #endregion GetMonths

        #endregion Private Methods

        #region INavigationTraking Members

        public List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> parameters = new List<KeyValueParam>();
            parameters.Add(new KeyValueParam("Action", actionName));
            parameters.Add(new KeyValueParam("From Date", ddlMonthFrom.Value));
            parameters.Add(new KeyValueParam("TO Date", ddlMonthTo.Value));
            return parameters;
        }

        #endregion
    }
}