//  Description					: Code Behind for Name OWS Calls.			          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Priya Singh 	                                          //
//  Author email id				:                           							  //
//  Creation Date				: 20th November  2007									  //
//	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using EPiServer.Globalization;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for UpdateProfile
    /// </summary>
    public partial class UpdateProfile : EPiServer.UserControlBase, INavigationTraking
    {
        #region Private properties

        private NameController nameController = null;
        /// <summary>
        /// Gets m_NameController
        /// </summary>
        private NameController m_NameController
        {
            get
            {
                if (nameController == null)
                {
                    ILoyaltyDomain loyaltyDomain = new LoyaltyDomain();
                    ISessionManager sessionManager = new SessionManager();
                    nameController = new NameController(loyaltyDomain, sessionManager);
                }
                return nameController;
            }
        }

        private InformationController informationController = null;
        /// <summary>
        /// Gets m_InformationController
        /// </summary>
        private InformationController m_InformationController
        {
            get
            {
                if (informationController == null)
                {
                    IInformationDomain informationDomain = new InformationDomain();
                    informationController = new InformationController(informationDomain);
                }
                return informationController;
            }
        }

        #endregion

        /// <summary>
        /// Page Load method for Update Profile User control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible && WebUtil.IsSessionValid())
            {
                if (UserLoggedInSessionWrapper.UserLoggedIn == true)
                {
                    if (!IsPostBack)
                    {
                        UpdateProfileMessageHeader.Visible = false;
                        SetSortDropDowns();

                        PopulateUserDetails();

                        //BtnSend.Attributes.Add("onclick", "javascript:return performValidation(PAGE_UPDATE_PROFILE);");
                        //spnSend.Attributes.Add("onclick", "javascript:return performValidation(PAGE_UPDATE_PROFILE);");
                        BtnSend.Text = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/save");
                        DisplayManageCreditCardControl();
                    }
                }

                else
                {
                    AppLogger.LogInfoMessage(
                        "User is already logged out - Still accessing Update Profile Functionality.");
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.LOGIN_FOR_DEEPLINK), false);
                }
            }
        }

        /// <summary>
        /// Button Click method to call the Update profile Controller method.
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="e">
        /// Arguments for the event
        /// </param>
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                clientErrorDivUP.InnerHtml = "";
                UpdateProfileMessageHeader.Visible = false;

                UserProfileEntity userProfilePrePopulated = UserProfileInformationSessionWrapper.UserProfileInformation;
                if (userProfilePrePopulated != null)
                {
                    UserProfileEntity capturedUserProfileFromUI = CaptureGuestProfileFromUI();

                    List<PartnershipProgram> newPartnershipProgramList = GetPartnerProgramFromUI();

                    m_NameController.PrimaryLangaueID = capturedUserProfileFromUI.PreferredLanguage;

                    string nameID = LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID;
                    userProfilePrePopulated.MembershipId = LoyaltyDetailsSessionWrapper.LoyaltyDetails.UserName;
                    UserNavTracker.TrackAction(this, "Update Profile");
                    //WebUtil.ApplicationErrorLog(new Exception("Update Profile"));
                    //Response.End();
                    m_NameController.UpdateUserDetails(nameID, userProfilePrePopulated, capturedUserProfileFromUI, newPartnershipProgramList);

                    capturedUserProfileFromUI.PartnerProgramList = newPartnershipProgramList;

                    UpdateLoyaltyDetails(capturedUserProfileFromUI);

                    PopulatePartnerProgram(capturedUserProfileFromUI);

                    UpdateProfileMessageHeader.Visible = true;
                }
            }
            catch (OWSException owsException)
            {
                UpdateProfileMessageHeader.Visible = false;
                AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);
                switch (owsException.ErrCode)
                {
                    case OWSExceptionConstants.RESERVATION_EXISTS_FOR_CREDITCARD:
                        {
                            clientErrorDivUP.InnerHtml =
                                WebUtil.GetTranslatedText(TranslatedTextConstansts.RESERVATION_EXISTS_FOR_CREDITCARD);
                            break;
                        }
                }
                string exceptionMessage = WebUtil.GetTranslatedText(owsException.TranslatePath);
                if (owsException.Message == AppConstants.SYSTEM_ERROR)
                {
                    WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                                 WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                }
                if (owsException.TranslatePath == AppConstants.SITE_WIDE_ERROR)
                {
                    if (owsException.ErrMessage.Length > 0)
                    {
                        clientErrorDivUP.InnerHtml = string.Format("{0}{1}",WebUtil.GetTranslatedText(AppConstants.UPDATE_PROFILE_ERROR), owsException.ErrMessage);
                    }
                    else
                    {
                        WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                                     WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                    }
                }
                else
                {
                    clientErrorDivUP.InnerHtml = WebUtil.GetTranslatedText(owsException.TranslatePath);
                }
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }
        }

        /// <summary>
        /// Fetches credit card list and calls populate credit cards method 
        /// </summary>
        private void DisplayManageCreditCardControl()
        {
            var loyaltyDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
            var loyaltyDomain = new LoyaltyDomain();
            var creditCards = loyaltyDomain.FetchCreditCardDetailsUserProfile(loyaltyDetails.NameID, true);

            ManageCreditCards.PopulateCreditCards(creditCards);
        }

        #region UpdateLoyaltyDetails

        /// <summary>
        /// After succesful updation in opera, we need to update the 
        /// session with the updated details of the user.
        /// </summary>
        /// <param name="userProfileEntity"></param>
        private void UpdateLoyaltyDetails(UserProfileEntity userProfileEntity)
        {
            //LoyaltyDetailsSessionWrapper.LoyaltyDetails.Title = userProfileEntity.NameTitle;
            //LoyaltyDetailsSessionWrapper.LoyaltyDetails.FirstName = userProfileEntity.FirstName;
            //LoyaltyDetailsSessionWrapper.LoyaltyDetails.SurName = userProfileEntity.LastName;
            LoyaltyDetailsSessionWrapper.LoyaltyDetails.AddressLine1 = userProfileEntity.AddressLine1;
            LoyaltyDetailsSessionWrapper.LoyaltyDetails.AddressLine2 = userProfileEntity.AddressLine2;
            LoyaltyDetailsSessionWrapper.LoyaltyDetails.City = userProfileEntity.City;
            LoyaltyDetailsSessionWrapper.LoyaltyDetails.Country = userProfileEntity.Country;
            LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email = userProfileEntity.EmailID;
            LoyaltyDetailsSessionWrapper.LoyaltyDetails.Fax = userProfileEntity.Fax;
            LoyaltyDetailsSessionWrapper.LoyaltyDetails.PostCode = userProfileEntity.PostCode;
            LoyaltyDetailsSessionWrapper.LoyaltyDetails.PreferredLanguage = userProfileEntity.PreferredLanguage;
            LoyaltyDetailsSessionWrapper.LoyaltyDetails.Telephone1 = userProfileEntity.MobilePhone;
        }

        #endregion UpdateLoyaltyDetails

        /// <summary>
        /// Populate User Profile Details
        /// </summary>
        private void PopulateUserDetails()
        {
            UserProfileEntity userProfileEntity = null;
            string nameID = string.Empty;
            try
            {

                userProfileEntity = UserProfileInformationSessionWrapper.UserProfileInformation;
                if (userProfileEntity != null)
                {
                    UserProfileInformationSessionWrapper.UserProfileInformation = userProfileEntity;
                    UpdateLoyaltyDetails(userProfileEntity);

                    string nameTitlelbl =
                        WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nameTitle");
                    //lblnameTitleHdr.InnerHtml = nameTitlelbl;
                    string firstnamelbl =
                        WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/firstname");
                    lblFirstNameHdr.InnerHtml = firstnamelbl.Replace('*', ' ');
                    string lastnamelbl =
                        WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/lastname");
                    lblLastNameHdr.InnerHtml = lastnamelbl.Replace('*', ' ');

                    //lblnameTitle.InnerHtml = string.IsNullOrEmpty(userProfileEntity.NameTitle) ? AppConstants.HTML_SPACE : userProfileEntity.NameTitle;
                    //pTitleSecion.Visible = !string.IsNullOrEmpty(userProfileEntity.NameTitle);

                    //Added as part of SCANAM-537
                    if (!string.IsNullOrEmpty(userProfileEntity.NativeFirstName) && !string.IsNullOrEmpty(userProfileEntity.NativeLastName))
                    {
                        lblFirstName.InnerHtml = userProfileEntity.NativeFirstName;
                        lblLastName.InnerHtml = userProfileEntity.NativeLastName;
                    }
                    else
                    {
                        lblFirstName.InnerHtml = userProfileEntity.FirstName;
                        lblLastName.InnerHtml = userProfileEntity.LastName;
                    }
                    

                    string gender = userProfileEntity.Gender.ToString();

                    if (gender == AppConstants.MALE_FROMOPERA)
                        lblGender.InnerHtml = WebUtil.GetTranslatedText(TranslatedTextConstansts.MALE);
                    else if (gender == AppConstants.FEMALE_FROMOPERA)
                        lblGender.InnerHtml = WebUtil.GetTranslatedText(TranslatedTextConstansts.FEMALE);
                    else
                        lblGender.InnerHtml = AppConstants.HTML_SPACE;

                    if (userProfileEntity.DateOfBirth != null)
                    {
                        lblDateofBirthDay.InnerHtml = userProfileEntity.DateOfBirth.Day.ToString();

                        IFormatProvider cultureInfo =
                            new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
                        int monthCount = userProfileEntity.DateOfBirth.Month;
                        string monthName = (new DateTime(1, monthCount, 1)).ToString("MMMM", cultureInfo);

                        lblDateofBirthMonth.InnerHtml = monthName;
                        lblDateofBirthYear.InnerHtml = userProfileEntity.DateOfBirth.Year.ToString();
                    }
                    else
                    {
                        lblDateofBirthDay.InnerHtml = "&nbsp;";
                    }

                    if (!string.IsNullOrEmpty(userProfileEntity.AddressLine1))
                    {
                        txtAddressLine1.Value = userProfileEntity.AddressLine1;
                    }
                    if (!string.IsNullOrEmpty(userProfileEntity.AddressLine2))
                    {
                        txtAddressLine2.Value = userProfileEntity.AddressLine2;
                    }

                    txtCityOrTown.Value = userProfileEntity.City;
                    txtPostCode.Value = userProfileEntity.PostCode;

                    ListItem selectedCountry = ddlCountry.Items.FindByValue(userProfileEntity.Country);
                    if (selectedCountry != null && ddlCountry.Items.IndexOf(selectedCountry) > 0)
                    {
                        ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(selectedCountry);
                    }

                    ListItem selectedLanguage =
                        ddlPreferredLang.Items.FindByValue(userProfileEntity.PreferredLanguage);
                    if (selectedLanguage != null && ddlPreferredLang.Items.IndexOf(selectedLanguage) >= 0)
                    {
                        ddlPreferredLang.SelectedIndex = ddlPreferredLang.Items.IndexOf(selectedLanguage);
                    }

                    InformationController informationCtrl = new InformationController();
                    Dictionary<string, string> nameTitleMapOpera =
                        informationCtrl.GetConfiguredTitles(LanguageConstant.LANGUAGE_GERMAN_CODE);

                    ListItem selectItem = new ListItem(AppConstants.TITLE_DEFAULT_VALUE,
                                                       AppConstants.TITLE_DEFAULT_VALUE);
                    ddlTitle.Items.Add(selectItem);

                    foreach (string key in nameTitleMapOpera.Keys)
                    {
                        ddlTitle.Items.Add(new ListItem(nameTitleMapOpera[key].ToString(), key));
                    }
                    ListItem selectedDefaultItem = ddlTitle.Items.FindByValue(AppConstants.TITLE_DEFAULT_VALUE);
                    if (!string.IsNullOrEmpty(userProfileEntity.NameTitle))
                        selectedDefaultItem = ddlTitle.Items.FindByValue(userProfileEntity.NameTitle);
                    if (selectedDefaultItem != null)
                    {
                        ddlTitle.SelectedItem.Selected = false;
                        selectedDefaultItem.Selected = true;
                    }

                    if (string.Equals(LanguageSelection.GetLanguageFromHost(), LanguageConstant.LANGUAGE_GERMAN, StringComparison.InvariantCultureIgnoreCase) ||
                        string.Equals(userProfileEntity.PreferredLanguage, LanguageConstant.LANGUAGE_GERMAN_CODE, StringComparison.InvariantCultureIgnoreCase))
                    {
                        dvNameTitle.Attributes.Add("style", "display:block");
                        lblNameTitle.Attributes.Add("style", "display:block");
                    }
                    else
                    {
                        dvNameTitle.Attributes.Add("style", "display:none");
                        lblNameTitle.Attributes.Add("style", "display:none");
                    }
                    SetSelectedPhoneCodes(userProfileEntity.HomePhone, userProfileEntity.MobilePhone);

                    if (!string.IsNullOrEmpty(userProfileEntity.EmailID))
                    {
                        txtEmail1.Value = userProfileEntity.EmailID;
                    }


                    LoyaltyDomain objLoyalityDomain = new LoyaltyDomain();
                    Dictionary<string, CreditCardEntity> creditCards =
                        objLoyalityDomain.FetchCreditCardDetailsUserProfile(
                            LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID,true);
                        if (creditCards != null && creditCards.Count > 0)
                        {
                            foreach (string cardKeys in creditCards.Keys)
                            {
                                if (creditCards[cardKeys].DisplaySequence == 1)
                                {
                                    string name = creditCards[cardKeys].NameOnCard;
                                    string cardType = creditCards[cardKeys].CardType;
                                    string cardNo = creditCards[cardKeys].CardNumber;
                                    ExpiryDateEntity expDate = creditCards[cardKeys].ExpiryDate;
                                    CreditCardEntity creditCardDetails = new CreditCardEntity(name, cardType, cardNo, expDate);
                                    userProfileEntity.CreditCard = creditCardDetails;
                                    userProfileEntity.CreditCardOperaId = long.Parse(cardKeys);
                                    break;
                                }
                            }
                            if (userProfileEntity.CreditCard != null)
                            {
                                previousCardNumber.Value = userProfileEntity.CreditCard.CardNumber;
                            }
                        }

                    if (userProfileEntity.UserPreference != null)
                    {
                        SetUserPreferencesOnUI(userProfileEntity.UserPreference);
                    }

                    SetUserInterestsOnUi(userProfileEntity.Interests);
                    PopulatePartnerProgram(userProfileEntity);

                    if (userProfileEntity.ThridPartyEmail)
                        chkReceivePartnersInfo.Checked = true;
                    else
                        chkReceivePartnersInfo.Checked = false;
                }
            }

            catch (OWSException owsException)
            {
                AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);
                string exceptionMessage = WebUtil.GetTranslatedText(owsException.TranslatePath);
                if (owsException.Message == AppConstants.SYSTEM_ERROR)
                {
                    WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                                 WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                }
                else
                {
                    clientErrorDivUP.InnerHtml = owsException.Message;
                }
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }
        }


        /// <summary>
        /// Populate the Partner Programs
        /// </summary>
        /// <param name="userProfileEntity">
        /// User Profile Entity
        /// </param>
        private void PopulatePartnerProgram(UserProfileEntity userProfileEntity)
        {
            SetPartnerProgramDropDown(userProfileEntity);
            if (userProfileEntity.PartnerProgramList != null)
            {
                int partnerProgramLength = userProfileEntity.PartnerProgramList.Count;
                StringBuilder partnerPrgAndValue = new StringBuilder();
                for (int count = 0; count < partnerProgramLength; count++)
                {
                    partnerPrgAndValue.Append(userProfileEntity.PartnerProgramList[count].PartnerProgram);
                    partnerPrgAndValue.Append(",");
                    partnerPrgAndValue.Append(userProfileEntity.PartnerProgramList[count].PartnerAccountNo);
                    partnerPrgAndValue.Append("%");
                }
                selectedPartnerProgramValues.Value = partnerPrgAndValue.ToString();
            }
        }

        private Dictionary<string, string> GetUserInterestResults()
        {
            var userInterestsFromOpera = m_InformationController.GetUserInterestTypes();
            var userInterestsFromCms = ContentDataAccess.GetUserInterestList();
            var interestResults = new Dictionary<string, string>();

            foreach (var interest in userInterestsFromCms)
            {
                if (userInterestsFromOpera.ContainsKey(interest.Key))
                {
                    interestResults.Add(interest.Key, interest.Value);
                }
            }

            return interestResults;
        }

        /// <summary>
        /// Set User interests on UI
        /// </summary>
        /// <param name="userInterestsEntity"></param>
        private void SetUserInterestsOnUi(UserInterestsEntity[] userInterestsEntity)
        {
            var interestResults = GetUserInterestResults();
            foreach (var interest in interestResults)
            {
                var item = new ListItem { Text = interest.Value, Value = interest.Key };
                if (userInterestsEntity != null && userInterestsEntity.Any(t => t.RequestValue == interest.Key))
                    item.Selected = true;
                chkMyInterestList.Items.Add(item);
            }
        }

        /// <summary>
        /// Set User Preferences on Update Profile screen
        /// </summary>
        private void SetUserPreferencesOnUI(UserPreferenceEntity[] userPreferences)
        {
            int maxPrefCount = userPreferences.Length;
            for (int prefCount = 0; prefCount < maxPrefCount; prefCount++)
            {
                switch (userPreferences[prefCount].RequestValue)
                {
                    case UserPreferenceConstants.LOWERFLOOR:
                        {
                            rdoLowerFloor.Checked = true;
                            break;
                        }
                    case UserPreferenceConstants.HIGHFLOOR:
                        {
                            rdoHighFloor.Checked = true;
                            break;
                        }
                    case UserPreferenceConstants.AWAYFROMELEVATOR:
                        {
                            rdoAwayFromElevator.Checked = true;
                            break;
                        }
                    case UserPreferenceConstants.NEARELEVATOR:
                        {
                            rdoNearElevator.Checked = true;
                            break;
                        }
                    case UserPreferenceConstants.SMOKING:
                        {
                            rdoSmoking.Checked = true;
                            break;
                        }
                    case UserPreferenceConstants.NOSMOKING:
                        {
                            rdoNoSmoking.Checked = true;
                            break;
                        }
                    case UserPreferenceConstants.ACCESSIBLEROOM:
                        {
                            chkAccessableRoom.Checked = true;
                            break;
                        }

                    case UserPreferenceConstants.ALLERGYROOM:
                        {
                            chkAllergyRooms.Checked = true;
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Caputer Inputs from EnrollGuestProgram Screen, For Updating
        /// </summary>
        /// <returns>UserProfileEntity</returns>
        private UserProfileEntity CaptureGuestProfileFromUI()
        {
            UserProfileEntity userProfileEntity = new UserProfileEntity();
            if (!string.IsNullOrEmpty(txtEmail1.Value))
            {
                userProfileEntity.EmailID = txtEmail1.Value;
                userProfileEntity.NonBusinessEmailID = txtEmail1.Value;
            }
            //added by Vaibhav to capture and the User Personal Interests.
            userProfileEntity.Interests = CaptureUserInterests();

            userProfileEntity.UserPreference = CaptureUserPreference();

            userProfileEntity.PreferredLanguage = ddlPreferredLang.Text;
            System.Globalization.TextInfo tInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo;

            userProfileEntity.NameTitle = (ddlTitle.Text != AppConstants.TITLE_DEFAULT_VALUE && string.Equals(ddlPreferredLang.Text, LanguageConstant.LANGUAGE_GERMAN_CODE)) ? tInfo.ToTitleCase(ddlTitle.Text) : string.Empty;
            if (ddlTel2.SelectedIndex > 0)
            {
                string mobilePhoneCode = Utility.FormatePhoneCode(ddlTel2.Text);
                userProfileEntity.MobilePhone = mobilePhoneCode + Regex.Replace(txtTelephone2.Value.Trim().TrimStart('0'), @"\D", "");
            }

            userProfileEntity.AddressType = AppConstants.ADDRESS_TYPE_HOME;
            userProfileEntity.AddressLine1 = txtAddressLine1.Value;
            userProfileEntity.AddressLine2 = txtAddressLine2.Value;

            userProfileEntity.PostCode = txtPostCode.Value;
            userProfileEntity.City = txtCityOrTown.Value;
            userProfileEntity.Country = ddlCountry.Text;


            userProfileEntity.ProfileType = UserProfileType.UPDATE_PROFILE;

            if (txtUpdatePassword.Value != "" && txtUpdateReTypePassword.Value != "")
            {
                userProfileEntity.Password = txtUpdatePassword.Value;
                userProfileEntity.RetypePassword = txtUpdateReTypePassword.Value;
            }
            else
            {
                userProfileEntity.Password = null;
                userProfileEntity.RetypePassword = null;
            }
            //RK: These flags should be set to TRUE bydefault in Update Profile as the field to update them not present on update profile screen
            //and These flags are being sent on "I Agree" checkbox which is a mandatory field so they will always be TRUE.
            userProfileEntity.ScandicEmail = true;
            userProfileEntity.ScandicEMailPrev = true;
            
            if (chkReceivePartnersInfo.Checked)
            {
                userProfileEntity.ThridPartyEmail = true;
            }
            else
            {
                userProfileEntity.ThridPartyEmail = false;
            }

            return userProfileEntity;
        }

        /// <summary>
        /// Populate home & mobile Phones DropDown & Phone number
        /// </summary>
        private void SetSelectedPhoneCodes(string homeTelPhoneNumber, string mobileTelPhoneNumber)
        {
            char[] zeroChars = new char[1];
            zeroChars[0] = '0';
            if (homeTelPhoneNumber != null && homeTelPhoneNumber.Length > 0)
            {
                string homePhoneCode = string.Empty;
                if (homeTelPhoneNumber != string.Empty)
                {
                    if (homeTelPhoneNumber.Length > AppConstants.MAX_LENGTH_PHONECODE)
                    {
                        if (!homeTelPhoneNumber.StartsWith(AppConstants.PLUS))
                        {
                            homePhoneCode = homeTelPhoneNumber.Substring(0, AppConstants.MAX_LENGTH_PHONECODE);
                            homeTelPhoneNumber = homeTelPhoneNumber.Substring(AppConstants.MAX_LENGTH_PHONECODE).TrimStart(zeroChars);
                            homePhoneCode = (homePhoneCode != string.Empty)
                                                ? homePhoneCode.TrimStart(zeroChars)
                                                : string.Empty;
                        }
                        else
                        {
                            homePhoneCode = homeTelPhoneNumber.Substring(AppConstants.ONE_INT, AppConstants.TWO_INT);
                            homeTelPhoneNumber = homeTelPhoneNumber.Substring(AppConstants.THREE_INT).TrimStart('0');
                            homePhoneCode = (homePhoneCode != string.Empty) ? homePhoneCode.TrimStart(zeroChars) : string.Empty;
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(mobileTelPhoneNumber))
            {
                string mobilePhoneCode = string.Empty;
                if (mobileTelPhoneNumber != string.Empty)
                {
                    if (mobileTelPhoneNumber.Length > AppConstants.MAX_LENGTH_PHONECODE || mobileTelPhoneNumber.StartsWith(AppConstants.PLUS))
                    {
                        if (!mobileTelPhoneNumber.StartsWith(AppConstants.PLUS))
                        {
                            mobilePhoneCode = mobileTelPhoneNumber.Substring(0, AppConstants.MAX_LENGTH_PHONECODE);
                            mobileTelPhoneNumber = mobileTelPhoneNumber.Substring(AppConstants.MAX_LENGTH_PHONECODE).TrimStart(zeroChars);
                            mobilePhoneCode = (mobilePhoneCode != string.Empty)
                                                  ? mobilePhoneCode.TrimStart(zeroChars)
                                                  : string.Empty;
                        }
                        else
                        {
                            mobilePhoneCode = mobileTelPhoneNumber.Substring(AppConstants.ONE_INT, AppConstants.TWO_INT);
                            mobileTelPhoneNumber = mobileTelPhoneNumber.Substring(AppConstants.THREE_INT).TrimStart('0');
                            mobilePhoneCode = (mobilePhoneCode != string.Empty) ? mobilePhoneCode.TrimStart(zeroChars) : string.Empty;
                        }
                    }
                }
                ListItem selectedMobileCode = ddlTel2.Items.FindByValue(mobilePhoneCode);
                if (selectedMobileCode != null)
                {
                    ddlTel2.SelectedItem.Selected = false;
                    selectedMobileCode.Selected = true;
                }
                txtTelephone2.Value = mobileTelPhoneNumber;
            }
        }

        /// <summary>
        /// Populate the Drop Down Lists
        /// </summary>
        private void SetSortDropDowns()
        {
            SetCountryDropDown();

            SetPhoneCodesDropDown();

            SetPreferredLanguageDropDown();
        }

        /// <summary>
        /// Set the drop down list of Countries
        /// </summary>
        private void SetCountryDropDown()
        {
            OrderedDictionary countryCodeMap = DropDownService.GetCountryCodes();
            if (countryCodeMap != null)
            {
                foreach (string key in countryCodeMap.Keys)
                {
                    ddlCountry.Items.Add(new ListItem(countryCodeMap[key].ToString(), key));
                }
            }
            ListItem selectedDefaultItem = ddlCountry.Items.FindByValue(AppConstants.DEFAULT_VALUE_CONST);
            if (selectedDefaultItem != null)
            {
                ddlCountry.SelectedItem.Selected = false;
                selectedDefaultItem.Selected = true;
            }
        }

        /// <summary>
        /// Set the drop down list of Prefered language
        /// </summary>
        private void SetPreferredLanguageDropDown()
        {
            OrderedDictionary preferredLangCodeMap = DropDownService.GetPreferredLanguageCodes();
            if (preferredLangCodeMap != null)
            {
                foreach (string key in preferredLangCodeMap.Keys)
                {
                    ddlPreferredLang.Items.Add(new ListItem(preferredLangCodeMap[key].ToString(), key));
                }
            }
            IFormatProvider cultureInfo =
                new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
            string PreferedlangSelected = cultureInfo.ToString();
            PreferedlangSelected = PreferedlangSelected.Substring(0, 2);
            PreferedlangSelected = PreferedlangSelected.ToUpper();

            if (PreferedlangSelected == LanguageConstant.LANGUAGE_ENGLISH)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_ENGLISH_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_DANISH)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_DANISH_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_NORWEGIAN)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_NORWEGIAN_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_SWEDISH)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_SWEDISH_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_FINNISH)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_FINNISH_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_GERMAN)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_GERMAN_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_RUSSIA)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_RUSSIA_CODE;
            }

            ListItem selectedDefaultItem = ddlPreferredLang.Items.FindByValue(PreferedlangSelected);
            if (selectedDefaultItem != null)
            {
                ddlPreferredLang.SelectedItem.Selected = false;
                selectedDefaultItem.Selected = true;
            }
        }

        /// <summary>
        /// Set the drop down list of Phone Codes
        /// </summary>
        private void SetPhoneCodesDropDown()
        {
            OrderedDictionary phoneCodesMap = DropDownService.GetTelephoneCodes();
            if (phoneCodesMap != null)
            {
                foreach (string key in phoneCodesMap.Keys)
                {
                    if (key == AppConstants.DEFAULT_VALUE_CONST || key == AppConstants.DEFAULT_SEPARATOR_CONST)
                    {
                        ddlTel2.Items.Add(new ListItem(phoneCodesMap[key].ToString(), key));
                    }
                    else
                    {
                        ddlTel2.Items.Add(new ListItem(key, key));
                    }
                }
            }
            ListItem tel2SelectedDefaultItem = ddlTel2.Items.FindByValue(AppConstants.DEFAULT_VALUE_CONST);
            if (tel2SelectedDefaultItem != null)
            {
                ddlTel2.SelectedItem.Selected = false;
                tel2SelectedDefaultItem.Selected = true;
            }
        }

        /// <summary>
        /// Set the drop down list of Partner Program
        /// </summary>
        /// <param name="partnerProgram">All partner program for this profile.</param>
        private void SetPartnerProgramDropDown(UserProfileEntity userProfileEntiy)
        {
            Dictionary<string, string> partnerProgramCodeMapOpera = m_InformationController.GetMembershipTypes();
            if (partnerProgramCodeMapOpera != null && partnerProgramCodeMapOpera.Count > 0)
            {
                prefPartnerCount.Value = (partnerProgramCodeMapOpera.Count).ToString();
            }
            string optionString = GetOptionString(partnerProgramCodeMapOpera);
            optionStringControl.Value = optionString;
            string divString = GetDynamicControlsForPartnerProgram(optionString, userProfileEntiy);
            preferredPartnerPgm.InnerHtml = divString;
        }

        /// <summary>
        /// This will generate the options for partnership program dropdown.
        /// </summary>
        /// <param name="partnerList">All available partner program in Opera</param>
        /// <returns>Options String</returns>
        private string GetOptionString(Dictionary<string, string> partnerList)
        {
            StringBuilder newString = new StringBuilder();
            string partnershipProgramDefaultKey = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/defaultpartnershipprogramkey");
            string partnershipProgramDefaultValue = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/defaultpartnershipprogramvalue");
            newString.Append("<option value=\"" + partnershipProgramDefaultKey + "\">");
            newString.Append(partnershipProgramDefaultValue + "</option>");

            if (partnerList != null && partnerList.Count > 0)
            {
                foreach (string keyValue in partnerList.Keys)
                {
                    newString.Append("<option value=\"");
                    newString.Append(keyValue);
                    newString.Append("\">");
                    newString.Append(partnerList[keyValue]);
                    newString.Append("</option>");
                }
            }
            return newString.ToString();
        }

        /// <summary>
        /// This will generate string which will be placed in the div.So that this div will display the 
        /// multiple controls.
        /// </summary>
        /// <param name="optionString">Option string</param>
        /// <param name="partnerProgram">All partnership program this profile have.</param>
        /// <returns>string that contains the html for generating controls for the partnership program div</returns>
        private string GetDynamicControlsForPartnerProgram(string optionString, UserProfileEntity userProfileEntity)
        {
            StringBuilder divString = new StringBuilder();
            string accountNumber = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/accountnumber");
            string deletePrefs = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/delete");
            string templateString =
                 "<div class=\"thisReplicates\"><div class=\"fltLft\"><div class=\"prefPartnerLablelft fltLft\"><span id=\"lblPartnerProgram{0}\"><label>{1} {4}</label></span></div><div class=\"prefPartnerLablert fltLft\"><span id=\"lblAccountNo{0}\"><label>{2} {6}</label></span></div></div><div class=\"columnOneA\"><p class=\"formRow\"><select name=\"selectPgm\" id=\"selectPgm{0}\" tabindex=\"340\" class=\"frmSelectBig hiddenClass\">{3}</select><br class=\"clear\" /></p></div><div class=\"columnTwoA\"><p class=\"formRow\"> <input name=\"memberNo\" type=\"text\" id=\"memberNo{0}\" class=\"frmInputText1 hiddenClass\" tabindex=\"340\" maxlength=\"20\" /></p></div>{5}<hr /></div>";
               // "<div class=\"thisReplicates\"><div class=\"columnOneA\"><p class=\"formRow\"><span id=\"lblPartnerProgram{0}\"><label>{1} {4}</label></span> <br /><select name=\"selectPgm\" id=\"selectPgm{0}\" tabindex=\"340\" class=\"frmSelectBig\">{3}</select><br class=\"clear\" /></p></div><div class=\"columnTwoA\"><p class=\"formRow\"> <span id=\"lblAccountNo{0}\"><label>{2} {6}</label></span><br /><input name=\"memberNo\" type=\"text\" id=\"memberNo{0}\" class=\"frmInputText1\" tabindex=\"340\" maxlength=\"20\" /></p></div>{5}<div class=\"columnThree\"></div><div class=\"clear\"></div><hr /></div>";
            string deleteString = string.Empty;
            if(userProfileEntity.PartnerProgramList != null && userProfileEntity.PartnerProgramList.Count >= 0)
                deleteString = "<div class=\"columnThree\"><a href=\"javascript:void(0);\" onclick=\"deletePref(0);\"><span>" + deletePrefs + "</span></a></div><div class=\"clear\"></div>";
            string partnerProgram =
                WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/preferredPartnerProgramme");
            divString.Append(string.Format(templateString, 0, partnerProgram, accountNumber, optionString, string.Empty,
                                           deleteString, string.Empty));
            if (userProfileEntity.PartnerProgramList != null)
            {
                int partnerProgramLength = userProfileEntity.PartnerProgramList.Count;
                if (partnerProgramLength > 0)
                {
                    bool hasPrimaryPartnerProgram = (2 == userProfileEntity.PartnerProgramList[0].DisplaySequence)
                                                        ? true
                                                        : false;
                    if (!((1 == partnerProgramLength) && (hasPrimaryPartnerProgram)))
                    {
                        partnerProgram =
                            WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/optionalPartnerProgram");
                        for (int partnerProgramCount = 0;
                             partnerProgramCount < (partnerProgramLength - 1);
                             partnerProgramCount++)
                        {
                            deleteString =
                                "<div class=\"columnThree\"><a href=\"javascript:void(0);\" onclick=\"deletePref(" +
                                (partnerProgramCount + 1) + ");\"><span>" + deletePrefs +
                                "</span></a></div><div class=\"clear\"></div>";
                            divString.Append(string.Format(templateString, partnerProgramCount + 1, partnerProgram,
                                                           accountNumber, optionString, partnerProgramCount + 1,
                                                           deleteString, partnerProgramCount + 1));
                        }
                    }
                }
            }
            return divString.ToString();
        }

        /// <summary>
        /// Capture the Special Requests of the guest
        /// </summary>
        /// <returns>
        /// Array of special request
        /// </returns>
        private UserPreferenceEntity[] CaptureUserPreference()
        {
            ArrayList userPrefList = new ArrayList();

            UserPreferenceEntity userPrefEntityForFloor = new UserPreferenceEntity();
            userPrefEntityForFloor.RequestType = UserPreferenceConstants.SPECIALREQUEST;
            if (rdoLowerFloor.Checked)
            {
                userPrefEntityForFloor.RequestValue = UserPreferenceConstants.LOWERFLOOR;
            }
            else if (rdoHighFloor.Checked)
            {
                userPrefEntityForFloor.RequestValue = UserPreferenceConstants.HIGHFLOOR;
            }
            if (userPrefEntityForFloor.RequestValue != null)
            {
                userPrefList.Add(userPrefEntityForFloor);
            }

            UserPreferenceEntity userPrefEntityForElevator = new UserPreferenceEntity();
            userPrefEntityForElevator.RequestType = UserPreferenceConstants.SPECIALREQUEST;
            if (rdoAwayFromElevator.Checked)
            {
                userPrefEntityForElevator.RequestValue = UserPreferenceConstants.AWAYFROMELEVATOR;
            }
            else if (rdoNearElevator.Checked)
            {
                userPrefEntityForElevator.RequestValue = UserPreferenceConstants.NEARELEVATOR;
            }
            if (userPrefEntityForElevator.RequestValue != null)
                userPrefList.Add(userPrefEntityForElevator);

            UserPreferenceEntity userPrefEntityForSmoking = new UserPreferenceEntity();
            userPrefEntityForSmoking.RequestType = UserPreferenceConstants.SPECIALREQUEST;
            if (rdoNoSmoking.Checked)
            {
                userPrefEntityForSmoking.RequestValue = UserPreferenceConstants.NOSMOKING;
            }
            else if (rdoSmoking.Checked)
            {
                userPrefEntityForSmoking.RequestValue = UserPreferenceConstants.SMOKING;
            }
            if (userPrefEntityForSmoking.RequestValue != null)
            {
                userPrefList.Add(userPrefEntityForSmoking);
            }

            if (chkAccessableRoom.Checked)
            {
                UserPreferenceEntity userPrefEntity = new UserPreferenceEntity();
                userPrefEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                userPrefEntity.RequestValue = UserPreferenceConstants.ACCESSIBLEROOM;
                userPrefList.Add(userPrefEntity);
            }
            if (chkAllergyRooms.Checked)
            {
                UserPreferenceEntity userPrefEntity = new UserPreferenceEntity();
                userPrefEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                userPrefEntity.RequestValue = UserPreferenceConstants.ALLERGYROOM;
                userPrefList.Add(userPrefEntity);
            }

            return userPrefList.ToArray(typeof(UserPreferenceEntity)) as UserPreferenceEntity[];
        }

        private UserInterestsEntity[] CaptureUserInterests()
        {
            var userinterests = new ArrayList();

            foreach (ListItem item in chkMyInterestList.Items)
            {
                if (item.Selected)
                {
                    var userInterestsEntity = new UserInterestsEntity
                    {
                        RequestType = UserInterestsConstants.INTERESTS,
                        RequestValue = item.Value
                    };
                    userinterests.Add(userInterestsEntity);
                }
            }
            return userinterests.ToArray(typeof(UserInterestsEntity)) as UserInterestsEntity[];


        }

        #region GetPartnerProgramFromUI
        /// <summary>
        /// Following method will find out the new partner program selected,modified or deleted from the UI.
        /// </summary>
        /// <returns>A dictionary of sequence number,along with partner program and account number.</returns>
        /// <remarks></remarks>
        private List<PartnershipProgram> GetPartnerProgramFromUI()
        {
            string partnerPrograms = selectedPartnerProgramValues.Value;

            char[] valueSeperator = new char[1];
            valueSeperator[0] = '%';

            char[] seperator = new char[1];
            seperator[0] = ',';

            string[] eachEntity = partnerPrograms.Split(valueSeperator, StringSplitOptions.RemoveEmptyEntries);
            eachEntity = eachEntity.Where(w => !(w.StartsWith("DFT,") || w.EndsWith(","))).ToArray();
            int entityLength = eachEntity.Length;

            Dictionary<string, string> partnerProgramAndAccountNo;
            Dictionary<int, Dictionary<string, string>> sequenceNumber =
                new Dictionary<int, Dictionary<string, string>>();

            int tempCount = 2;
            string defeultKey =
                WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/defaultpartnershipprogramkey");
            for (int count = 0; count < entityLength; count++)
            {
                string[] eachValue = eachEntity[count].Split(seperator);
                if ((0 == count) || (defeultKey != eachValue[0]))
                {
                    partnerProgramAndAccountNo = new Dictionary<string, string>();
                    partnerProgramAndAccountNo[eachValue[0]] = eachValue[1];
                    sequenceNumber[tempCount] = partnerProgramAndAccountNo;
                    tempCount++;
                }
            }

            List<PartnershipProgram> newPartnershipProgramList = new List<PartnershipProgram>();
            foreach (int key in sequenceNumber.Keys)
            {
                PartnershipProgram newPrg = new PartnershipProgram();
                newPrg.DisplaySequence = key;
                foreach (string accNo in sequenceNumber[key].Keys)
                {
                    newPrg.PartnerProgram = accNo;
                    newPrg.PartnerAccountNo = sequenceNumber[key][accNo];
                    newPartnershipProgramList.Add(newPrg);
                }
            }

            return newPartnershipProgramList;
        }

        #endregion

        #region INavigationTraking Members

        public List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> parameters = new List<KeyValueParam>();
            
            parameters.Add(new KeyValueParam(TrackerConstants.LOG_TYPE, actionName));
            parameters.Add(new KeyValueParam(TrackerConstants.E_MAIL, txtEmail1.Value));
            parameters.Add(new KeyValueParam(TrackerConstants.PHONETYPE2, ddlTel2.Text));
            parameters.Add(new KeyValueParam(TrackerConstants.TELEPHONE2, Regex.Replace(txtTelephone2.Value, @"\D", "")));
            parameters.Add(new KeyValueParam(TrackerConstants.ADDRESS_LINE1, txtAddressLine1.Value));
            parameters.Add(new KeyValueParam(TrackerConstants.ADDRESS_LINE2, txtAddressLine2.Value));
            parameters.Add(new KeyValueParam(TrackerConstants.POST_CODE, txtPostCode.Value));
            parameters.Add(new KeyValueParam(TrackerConstants.CITY, txtCityOrTown.Value));
            parameters.Add(new KeyValueParam(TrackerConstants.COUNTRY, ddlCountry.Text));
            parameters.Add(new KeyValueParam(TrackerConstants.PARTNER_PROGRAM_VALUE, selectedPartnerProgramValues.Value));
            parameters.Add(new KeyValueParam(TrackerConstants.PREFERENCELOWERFLOOR, rdoLowerFloor.Checked.ToString()));
            parameters.Add(new KeyValueParam(TrackerConstants.PREFERENCEHIGHFLOOR, rdoHighFloor.Checked.ToString()));
            parameters.Add(new KeyValueParam(TrackerConstants.PREFERENCEAWAYFROMELEVATOR, rdoAwayFromElevator.Checked.ToString()));
            parameters.Add(new KeyValueParam(TrackerConstants.PREFERENCEAWAYNEARELEVATOR, rdoNearElevator.Checked.ToString()));
            parameters.Add(new KeyValueParam(TrackerConstants.SPECIALPREFNOSMOKING, rdoNoSmoking.Checked.ToString()));
            parameters.Add(new KeyValueParam(TrackerConstants.SPECIALPREFACCESSABLEROOM, chkAccessableRoom.Checked.ToString()));
            parameters.Add(new KeyValueParam(TrackerConstants.SPECIALPREFALLERGYROOMS, chkAllergyRooms.Checked.ToString()));
            parameters.Add(new KeyValueParam(TrackerConstants.PREFERREDLANGUAGE, ddlPreferredLang.Text));
            parameters.Add(new KeyValueParam(TrackerConstants.RECEIVE_PARTNERSINFO, chkReceivePartnersInfo.Checked.ToString()));
            return parameters;
        }

        #endregion
    }
}