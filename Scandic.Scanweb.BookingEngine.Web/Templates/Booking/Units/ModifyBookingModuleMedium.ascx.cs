//  Description					: Code Behind class for Refine Booking Search Control	  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Girish Krishnanta                                   	  //
/// Author email id				:                           							  //
/// Creation Date				: 13th December  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces

using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion

    #region Scandic Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    public partial class ModifyBookingModuleMedium : EPiServer.UserControlBase
    {
        #region Page Events

        /// <summary>
        /// This method populates the control with hotelsearch information from sesssion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetDropDowns();
                HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
                if (hotelSearch != null)
                {
                    txtArrivalDate.Value = Core.DateUtil.DateToDDMMYYYYString(hotelSearch.ArrivalDate);
                    txtDepartureDate.Value = Core.DateUtil.DateToDDMMYYYYString(hotelSearch.DepartureDate);
                    txtNoOfNights.Value = hotelSearch.NoOfNights.ToString();
                    ddlAdultsPerRoom.Text = hotelSearch.AdultsPerRoom.ToString();
                    ddlChildPerRoom.Text = hotelSearch.ChildrenPerRoom.ToString();
                    ddlNoOfRooms.SelectedValue = hotelSearch.RoomsPerNight.ToString();
                }
            }
        }

        /// <summary>
        /// This methods update the session information with user inputs and 
        /// redirect accordingly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Search_Click(object sender, EventArgs e)
        {
            try
            {
                SearchCriteriaSessionWrapper.SearchCriteria = GetHotelSearchEntity();
                if ((SearchCriteriaSessionWrapper.SearchCriteria != null) && (SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor != null))
                {
                    if (SearchCriteriaSessionWrapper.SearchCriteria.ChildrenPerRoom > 0)
                    {
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CHILDREN_DETAILS_PAGE),
                                          false);
                    }
                    else
                    {
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE),
                                          false);
                    }
                }
                else
                {
                    ShowErrorMessage(
                        WebUtil.GetTranslatedText(
                            "/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name"));
                }
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
        }

        #endregion PageEvents

        #region Private Methods

        /// <summary>
        /// Get the hotel search information from the session and updates it 
        /// with the user inputs
        /// </summary>
        /// <returns>HotelSearchEntity</returns>
        private HotelSearchEntity GetHotelSearchEntity()
        {
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
            if (hotelSearch != null)
            {
                hotelSearch.ArrivalDate = Core.DateUtil.StringToDDMMYYYDate(txtArrivalDate.Value);
                hotelSearch.DepartureDate = Core.DateUtil.StringToDDMMYYYDate(txtDepartureDate.Value);
                hotelSearch.NoOfNights = int.Parse(txtNoOfNights.Value);
                hotelSearch.AdultsPerRoom = int.Parse(ddlAdultsPerRoom.Text);
                hotelSearch.ChildrenPerRoom = int.Parse(ddlChildPerRoom.Text);
                hotelSearch.RoomsPerNight = Int32.Parse(ddlNoOfRooms.Text);
            }
            return hotelSearch;
        }

        /// <summary>
        /// Display the Error message
        /// </summary>
        /// <param name="errorMessage"></param>
        private void ShowErrorMessage(string errorMessage)
        {
            clientErrorDiv.Attributes.Add("class", "errorText");

            string start = "<div id='errorReport' class='formGroupError'><div class='redAlertIcon'>"
                           +
                           WebUtil.GetTranslatedText(
                               "/scanweb/bookingengine/errormessages/requiredfielderror/errorheading")
                           + "</div><ul class='circleList'>";
            string end = "</ul></div>";
            clientErrorDiv.InnerHtml = start + errorMessage + end;
        }

        /// <summary>
        /// Populate the Drop Down
        /// </summary>
        private void SetDropDowns()
        {
            /*******************************************************************************************
            * Load Comboboxes on form
            *******************************************************************************************/
            ddlAdultsPerRoom.Items.Add(AppConstants.ONE);
            ddlAdultsPerRoom.Items.Add(AppConstants.TWO);
            ddlAdultsPerRoom.Items.Add(AppConstants.THREE);
            ddlAdultsPerRoom.Items.Add(AppConstants.FOUR);
            ddlAdultsPerRoom.Items.Add(AppConstants.FIVE);
            ddlAdultsPerRoom.Items.Add(AppConstants.SIX);

            ddlChildPerRoom.Items.Add(AppConstants.ZERO);
            ddlChildPerRoom.Items.Add(AppConstants.ONE);
            ddlChildPerRoom.Items.Add(AppConstants.TWO);
            ddlChildPerRoom.Items.Add(AppConstants.THREE);
            ddlChildPerRoom.Items.Add(AppConstants.FOUR);
            ddlChildPerRoom.Items.Add(AppConstants.FIVE);
            if (BookingEngineSessionWrapper.AllBookingDetails != null && BookingEngineSessionWrapper.AllBookingDetails.Count > 0)
            {
                AddNumberOfRooms(BookingEngineSessionWrapper.AllBookingDetails.Count);
            }
        }


        /// <summary>
        /// Add the number of rooms to the drop down
        /// </summary>
        /// <param name="numberOfRooms"></param>
        private void AddNumberOfRooms(int numberOfRooms)
        {
            switch (numberOfRooms.ToString())
            {
                case AppConstants.ONE:
                    {
                        ddlNoOfRooms.Items.Add(AppConstants.ONE);
                        ddlNoOfRooms.Items.Add(AppConstants.TWO);
                        ddlNoOfRooms.Items.Add(AppConstants.THREE);
                        ddlNoOfRooms.Items.Add(AppConstants.FOUR);
                        break;
                    }
                case AppConstants.TWO:
                    {
                        ddlNoOfRooms.Items.Add(AppConstants.TWO);
                        ddlNoOfRooms.Items.Add(AppConstants.THREE);
                        ddlNoOfRooms.Items.Add(AppConstants.FOUR);
                        break;
                    }
                case AppConstants.THREE:
                    {
                        ddlNoOfRooms.Items.Add(AppConstants.THREE);
                        ddlNoOfRooms.Items.Add(AppConstants.FOUR);
                        break;
                    }
                case AppConstants.FOUR:
                    {
                        ddlNoOfRooms.Items.Add(AppConstants.FOUR);
                        break;
                    }
            }
        }

        #endregion Private Methods             
    }
}