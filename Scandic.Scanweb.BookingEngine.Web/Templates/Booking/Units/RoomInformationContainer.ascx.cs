using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System.Linq;
using Scandic.Scanweb.BookingEngine.Web.HotelTimezone;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// RoomInformationContainer
    /// </summary>
    public partial class RoomInformationContainer : EPiServer.UserControlBase
    {
        private string topCss = "bookingDetail mrgTop19";

        protected string TopCss
        {
            get { return topCss; }
            set { topCss = value; }
        }

        public string MembershipNumber
        {
            get { return txtMembershipNumberRoom.Value; }
        }

        private string ratecode;

        public string Ratecode
        {
            set { ratecode = value; }
        }

        private GuestInformationEntity dataSource;

        public GuestInformationEntity DataSource
        {
            set { dataSource = value; }
        }

        private int _tabOrderIndexStartingIndex = 1;

        public int TabOrderStartingIndex
        {
            get
            {
                return _tabOrderIndexStartingIndex;
            }
            set
            {
                _tabOrderIndexStartingIndex = value;
            }
        }

        private int _taborderincrementalindex = 4;

        public int TabOrderIncrementalIndex
        {
            get
            {
                return _taborderincrementalindex;
            }
            set
            {
                _taborderincrementalindex = value;
            }
        }

        private int roomIdentifier;

        public int RoomIdentifier
        {
            set { roomIdentifier = value; }
        }

        public string RoomHeader { set; protected get; }

        private bool doDisplayGurantee;

        public bool DoDisplayGurantee
        {
            set { doDisplayGurantee = value; }
        }

        private bool isRoomConfirmed;

        public bool IsRoomConfirmed
        {
            set { isRoomConfirmed = value; }
        }

        private bool isStoredPanHashCreditCardsCanBeShown = false;

        /// <summary>
        /// Determines whether the stored pan hash credit cards can be shown for the rate category or not.
        /// </summary>
        public bool IsStoredPanHashCreditCardsCanBeShown
        {
            set { isStoredPanHashCreditCardsCanBeShown = value; }
            get { return isStoredPanHashCreditCardsCanBeShown; }
        }

        public bool DisplayCCGuranteeDivOption
        {
            set
            {
                if (value == false)
                {
                    HideUnhideCreditCardDiv.Attributes.Add("style", "display:none");
                    panHashCreditCardDiv.Attributes.Add("style", "display:none");
                }
            }
        }

        private bool isRoomModifiable = true;

        public bool IsRoomModifiable
        {
            get { return isRoomModifiable; }
            set { isRoomModifiable = value; }
        }

        private string nameTitle = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nametitle");
        private string firstNameText = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPFName");
        private string lastNameText = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPLName");
        private string email = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPEmail");
        private string confirmEmail = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/confirmemailadd");

        private string phone = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/PrimaryPhNo");
        private string membershipNumber = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/accountnumber");
        private string nameOnCreditCard = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/NameOnCard");
        private string creditCardNumber = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/NumberOnCard");
        string defaultText = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/nopreference");
        string selectBedtype = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/selectbedtype");
        private bool isBlockDisplay;
        private string currentLanguage = Scandic.Scanweb.BookingEngine.Web.Utility.GetCurrentLanguage();

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string language = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();
            string countryCodeID = "RoomInformation" + roomIdentifier + "_ddlCountry";
            string mobileNumberID = "RoomInformation" + roomIdentifier + "_ddlMobileNumber";
            ddlCountry.Attributes.Add("onchange",
                                      "javascript:selectPhoneCode(\'" + countryCodeID + "\',\'" + mobileNumberID +
                                      "\');");
            txtMembershipNumberRoom.Attributes.Add("rel",
                                                   WebUtil.GetTranslatedText(
                                                       "/bookingengine/booking/enrollguestprogram/accountnumber"));

            txtCardHolder.Attributes.Add("rel",
                                         WebUtil.GetTranslatedText(
                                             "/bookingengine/booking/bookingdetail/guaranteepolicytext/NameOnCard"));
            txtCardNumber.Attributes.Add("rel",
                                         WebUtil.GetTranslatedText(
                                             "/bookingengine/booking/bookingdetail/guaranteepolicytext/NumberOnCard"));
            if (roomIdentifier > 0)
            {
                txtMembershipNumberRoom.Attributes.Add("class", "text-input defaultColor distinctfgpnum fgpNumber" + roomIdentifier);
            }
            else
            {
                txtMembershipNumberRoom.Attributes.Add("class", "text-input defaultColor distinctfgpnum fgpNumber" + roomIdentifier);
            }
            if (HygieneSessionWrapper.IsComboReservation && this.IsRoomModifiable)
                guaranteeDepositAppendForComboReservation.Text = "(" +
                                                                 WebUtil.GetTranslatedText(
                                                                     "/bookingengine/booking/bookingdetail/guaranteedepositeforComboReservation") +
                                                                 ")";

            if (!IsPostBack)
            {
                //Default SMS checkbox will be checked for room 1 only
                if (roomIdentifier == 0)
                {
                    SMSconfirmation.Checked = true;
                    if (WebUtil.IsCookieExists(AppConstants.SMS_COOKIE))
                        SMSconfirmation.Checked = false;
                }
                if (currentLanguage.ToUpper() == LanguageConstant.LANGUAGE_GERMAN)
                {
                    divNameTitle.Visible = true;
                    if (ddlTitle.Items.Count <= 0)
                    {
                        InformationController informationCtrl = new InformationController();
                        Dictionary<string, string> nameTitleMapOpera =
                            informationCtrl.GetConfiguredTitles(LanguageConstant.LANGUAGE_GERMAN_CODE);
                        ListItem selectItem = new ListItem(AppConstants.TITLE_DEFAULT_VALUE,
                                                           AppConstants.TITLE_DEFAULT_VALUE);
                        ddlTitle.Items.Add(selectItem);

                        foreach (string key in nameTitleMapOpera.Keys)
                        {
                            ddlTitle.Items.Add(new ListItem(nameTitleMapOpera[key].ToString(), key));
                        }
                        ListItem selectedDefaultItem = ddlTitle.Items.FindByValue(AppConstants.TITLE_DEFAULT_VALUE);
                        if (selectedDefaultItem != null)
                        {
                            ddlTitle.SelectedItem.Selected = false;
                            selectedDefaultItem.Selected = true;
                        }
                    }
                }
                else
                {
                    divNameTitle.Visible = false;
                }

                SetRoomPreferences();
                //SetTabOrdersForControls();
            }


        }

        //private void SetTabOrdersForControls()
        //{

        //     System.Web.UI.Control  control = new System.Web.UI.Control();
        //     foreach (System.Web.UI.Control c in this.Controls[3].Controls)
        //        {

        //            if (c is System.Web.UI.HtmlControls.HtmlInputText)          // || c is DropDownList || c is System.Web.UI.HtmlControls.HtmlInputCheckBox)
        //            {

        //                System.Web.UI.HtmlControls.HtmlInputText textElement = (System.Web.UI.HtmlControls.HtmlInputText)Page.FindControl(c.ID);
        //                this.TabOrderStartingIndex = TabOrderStartingIndex++;
        //                textElement.Attributes.Add("tabIndex",TabOrderStartingIndex.ToString());
        //            }
        //            if(c is System.Web.UI.HtmlControls.HtmlInputCheckBox)
        //            {
        //                System.Web.UI.HtmlControls.HtmlInputCheckBox chckBoxElement = (System.Web.UI.HtmlControls.HtmlInputCheckBox)Page.FindControl(c.ID);
        //                this.TabOrderStartingIndex = TabOrderStartingIndex++;
        //                chckBoxElement.Attributes.Add("tabIndex",TabOrderStartingIndex.ToString());
        //            }
        //            if(c is DropDownList)
        //            {
        //                DropDownList dropDownListElement = (DropDownList)Page.FindControl(c.ID);
        //                this.TabOrderStartingIndex = TabOrderStartingIndex++;
        //                dropDownListElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());
        //            }
        //        }
        //}


        private void SetRoomPreferences()
        {
            XmlDocument bookingXmlDoc = new XmlDocument();
            string codeFileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            codeFileName = codeFileName.Substring(0, codeFileName.LastIndexOf("\\"));
            string currentLang = currentLanguage.ToUpper();
            string fileName = string.Format("{0}\\lang\\BookingEngine_{1}.xml", codeFileName, currentLang);
            bookingXmlDoc.Load(fileName);

            if (ddlRoomFloor.Items.Count <= 0)
            {
                ddlRoomFloor.Items.Add(new ListItem(defaultText, AppConstants.DEFAULT_VALUE_CONST));
                if (bookingXmlDoc != null)
                {
                    XmlNodeList roomFloorNodeList = bookingXmlDoc.SelectNodes("//languages/language/bookingengine/booking/bookingdetail/roomfloorpreference")[0].ChildNodes;
                    foreach (XmlNode node in roomFloorNodeList)
                    {
                        ddlRoomFloor.Items.Add(new ListItem(node.InnerText, node.Attributes["code"].Value));
                    }
                }
            }

            if (ddlElevator.Items.Count <= 0)
            {
                ddlElevator.Items.Add(new ListItem(defaultText, AppConstants.DEFAULT_VALUE_CONST));
                if (bookingXmlDoc != null)
                {
                    XmlNodeList elevatorNodeList = bookingXmlDoc.SelectNodes("//languages/language/bookingengine/booking/bookingdetail/elevatorpreference")[0].ChildNodes;
                    foreach (XmlNode node in elevatorNodeList)
                    {
                        ddlElevator.Items.Add(new ListItem(node.InnerText, node.Attributes["code"].Value));
                    }
                }
            }

            if (ddlSmoking.Items.Count <= 0)
            {
                ddlSmoking.Items.Add(new ListItem(defaultText, AppConstants.DEFAULT_VALUE_CONST));
                if (bookingXmlDoc != null)
                {
                    XmlNodeList smokingNodeList = bookingXmlDoc.SelectNodes("//languages/language/bookingengine/booking/bookingdetail/smokingpreference")[0].ChildNodes;
                    foreach (XmlNode node in smokingNodeList)
                    {
                        ddlSmoking.Items.Add(new ListItem(node.InnerText, node.Attributes["code"].Value));
                    }
                }
            }
        }


        public bool ShowUpdateCCDetails
        {
            set
            {
                divCreditCardUpdate.Visible = value;
                divCreditCardUpdatePanHash.Visible = value;
            }
        }

        public bool ShowDNumber
        {
            set { divDNumber.Visible = value; }
        }

        private string dNumber = string.Empty;

        public string DNumber
        {
            get
            {
                if (txtD_Number.Value.Trim() != string.Empty)
                {
                    dNumber = Utility.FormatCode(txtD_Number.Value);
                }
                return dNumber;
            }
            set { txtD_Number.Value = value; }
        }

        /// <summary>
        /// This will display only Credit card option after 6PM if user book a hotel after 6PM on DOA.
        /// </summary>
        public void DisplayCreditcardOptionAfterSixPM(HotelSearchEntity hotelSearch)
        {
            if (WebUtil.IsBookingAfterSixPM(hotelSearch))
            {
                SetAfterSixPMBookingText();
            }            
        }

        /// <summary>
        /// This will set correct description if user books after 6PM on the same day of arrival.
        /// </summary>
        private void SetAfterSixPMBookingText()
        {
            blkHoldRoom.Visible = false;
            lblGuranteewithRadio.Visible = true;
            rdoLateArrivalGurantee.Disabled = true;
            lblGuranteewithoutRadio.Visible = false;
            HygieneSessionWrapper.Is6PMBooking = true;
            System.Web.UI.HtmlControls.HtmlGenericControl displayTextLabel = new HtmlGenericControl();
            displayTextLabel = lblGuranteewithRadio;
            displayTextLabel.InnerHtml = (CurrentPage["GTDCC"] != null)
                                             ? CurrentPage["GTDCC"].ToString()
                                             : string.Empty;
        }

        /// <summary>
        /// OnDataBinding
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDataBinding(EventArgs e)
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            bool paymentFallback = ContentDataAccess.GetPaymentFallback(search.SelectedHotelCode);
            base.OnDataBinding(e);

            SetRoomPreferences();

            Utility.SetCountryDropDown(ddlCountry, CurrentPage);
            Utility.SetPhoneCodesDropDown(ddlMobileNumber, ddlCountry.SelectedItem.Value);
            txtEmail.Attributes.Add("class", "text-input defaultColor class" + roomIdentifier);

            txtMembershipNumberRoom.Attributes.Add("fieldName", "fgpNumber" + roomIdentifier);
            txtFNameRoom.Attributes.Add("fieldName", "name" + roomIdentifier);
            txtLNameRoom.Attributes.Add("fieldName", "name" + roomIdentifier);
            if (doDisplayGurantee)
            {
                this.Visible = true;
                HideUnhideGuranteeDiv.Visible = true;
                DisplayHoldOptionIfRequired(ratecode, paymentFallback);
                SetCreditCardDropDown();
                SetExpiryDateList();
            }
            SetBedPeferenceDropDown();
            bool flagGaurenteeType = false;
            if ((null != dataSource) && dataSource.GuranteeInformation != null)
            {
                if (dataSource.GuranteeInformation.GuranteeType == GuranteeType.HOLD)
                {
                    flagGaurenteeType = true;
                }
                else if (dataSource.GuranteeInformation.GuranteeType == GuranteeType.CREDITCARD)
                {
                    rdoLateArrivalGurantee.Checked = true;
                    //rdoHoldRoom.Checked = false;
                }
            }
            else
            {
                RateCategory rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(ratecode);
                if ((rateCategory != null) && rateCategory.HoldGuranteeAvailable)
                {
                    flagGaurenteeType = true;
                    rdoHoldRoom.Checked = true;
                    rdoLateArrivalGurantee.Checked = false;
                }
            }



            //if the Pan has credit card details are not populated then display 
            //the non pan has credit card details (as per before online payment implementation)

            if (HygieneSessionWrapper.IsPANHashCreditCardsAvailable && !paymentFallback && ((search != null) && (isStoredPanHashCreditCardsCanBeShown)))
            {
                HideUnhideCreditCardDiv.Visible = false;
                panHashCreditCardDiv.Visible = true;
            }
            else if ((search != null) && (search.SearchingType != SearchType.REDEMPTION))
            {
                PrePopulateCreditInfo(flagGaurenteeType);
                HideUnhideCreditCardDiv.Visible = true;
                panHashCreditCardDiv.Visible = false;
            }

            if (RoomRateUtil.IsSaveRateCategory(ratecode) && !paymentFallback)
            {
                HideUnhideCreditCardDiv.Visible = false;
                panHashCreditCardDiv.Visible = false;
            }

            if (null != dataSource)
            {
                if (Utility.GetCurrentLanguage().ToUpper() == LanguageConstant.LANGUAGE_GERMAN)
                {
                    ddlTitle.SelectedValue = (dataSource.Title != null && dataSource.Title != String.Empty)
                                                 ? dataSource.Title
                                                 : AppConstants.TITLE_DEFAULT_VALUE;
                }
				//Added as part of SCANAM-537
                if (!string.IsNullOrEmpty(dataSource.NativeFirstName) && !string.IsNullOrEmpty(dataSource.NativeLastName))
                {
                    txtFNameRoom.Value = dataSource.NativeFirstName;
                    txtLNameRoom.Value = dataSource.NativeLastName;
                }
                else
                {
                    txtFNameRoom.Value = dataSource.FirstName;
                    txtLNameRoom.Value = dataSource.LastName;
                }

                ddlCountry.SelectedValue = dataSource.Country;
                if (dataSource.EmailDetails != null)
                {
                    txtEmail.Value = dataSource.EmailDetails.EmailID;

                }
                if (null != dataSource.Mobile)
                {
                    string phNumber = dataSource.Mobile.Number;
                    if (dataSource.Mobile.Number.StartsWith(AppConstants.PLUS))
                    {
                        phNumber = string.Format("{0}{1}", "00", dataSource.Mobile.Number.TrimStart('+'));
                    }
                    phNumber = Utility.SetPhoneCountryCode(ddlMobileNumber, phNumber);
                    txtTelephone.Value = phNumber;
                }

                if (!string.IsNullOrEmpty(dataSource.GuestAccountNumber))
                {
                    txtMembershipNumberRoom.Value = dataSource.GuestAccountNumber;
                }

                string gauranteeType = string.Empty;
                if (dataSource.GuranteeInformation != null && dataSource.GuranteeInformation.ChannelGuranteeCode != null)
                {
                    gauranteeType = dataSource.GuranteeInformation.ChannelGuranteeCode;
                }
                else if (dataSource.GuranteeInformation.CreditCard != null)
                {
                    gauranteeType = dataSource.GuranteeInformation.CreditCard.CardType;
                }
                CreditCardEntity creditCard = dataSource.GuranteeInformation.CreditCard;
                bool isPrepaidCreditCard = (creditCard != null) && (!string.IsNullOrEmpty(creditCard.CardType))
                    && (creditCard.CardType.StartsWith(AppConstants.PREPAID_CCTYPE_PREFIX));
                if ((doDisplayGurantee) &&
                    (null != dataSource.GuranteeInformation) && (null != dataSource.GuranteeInformation.CreditCard)
                    && !RoomRateUtil.IsPrepaidSaveGuaranteeType(gauranteeType) && !isPrepaidCreditCard)
                {
                    if (null != creditCard.NameOnCard)
                    {
                        txtCardHolder.Value = creditCard.NameOnCard;
                    }
                    if (null != creditCard.CardNumber)
                    {
                        previousCardNumber.Value = creditCard.CardNumber;
                        if (creditCard.CardNumber.Length >= AppConstants.CREDIT_CARD_DISPLAY_CHARS)
                        {
                            txtCardNumber.Value = AppConstants.CARD_MASK +
                                                  creditCard.CardNumber.Substring((creditCard.CardNumber.Length -
                                                                                   AppConstants.
                                                                                       CREDIT_CARD_DISPLAY_CHARS));
                        }
                        else
                        {
                            txtCardNumber.Value = AppConstants.CARD_MASK + creditCard.CardNumber;
                        }
                    }
                    if ((null != creditCard.ExpiryDate) && (ddlExpiryYear.Items != null) &&
                        (ddlExpiryYear.Items.FindByValue(creditCard.ExpiryDate.Year.ToString()) != null))
                    {
                        //artf1295003 : Modify/Cancel - cc on booking details
                        ddlExpiryMonth.SelectedIndex = (creditCard.ExpiryDate.Month) > 0
                                                           ? (creditCard.ExpiryDate.Month)
                                                           : 0;
                        ddlExpiryYear.SelectedValue = creditCard.ExpiryDate.Year.ToString();
                    }
                    if ((null != creditCard.CardType) && (ddlCardType.Items != null) && (ddlCardType.Items.FindByValue(creditCard.CardType) != null))
                    {
                        ddlCardType.SelectedValue = creditCard.CardType;
                    }
                }
                PopulateSpecialRequests(dataSource.SpecialRequests);
            }
            ddlTitle.Attributes["rel"] = nameTitle;
            txtFNameRoom.Attributes["rel"] = firstNameText;
            txtLNameRoom.Attributes["rel"] = lastNameText;

            txtEmail.Attributes["rel"] = email;

            txtTelephone.Attributes["rel"] = phone;
            txtMembershipNumberRoom.Attributes["rel"] = membershipNumber;
            txtD_Number.Attributes["rel"] = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/DNumber");

            SetControlVisibility();

        }

        /// <summary>
        /// Pre populate credit card information 
        /// </summary>
        /// <param name="flagForGaurenteeType"></param>
        private void PrePopulateCreditInfo(bool flagForGaurenteeType)
        {
            NameController nameController = new NameController();
            CreditCardEntity creditCardDetails = null;

            if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null)
            {
                creditCardDetails = nameController.FetchCreditCard(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID, false);
            }
            if (creditCardDetails != null)
            {
                if (flagForGaurenteeType)
                    rdoLateArrivalGurantee.Checked = false;
                else
                    rdoLateArrivalGurantee.Checked = true;

                txtCardHolder.Value = creditCardDetails.NameOnCard;

                string creditCardNumber = creditCardDetails.CardNumber;
                if(string.IsNullOrEmpty(previousCardNumber.Value))
                    previousCardNumber.Value = creditCardNumber;
                if (creditCardNumber.Length >= AppConstants.CREDIT_CARD_DISPLAY_CHARS)
                {
                    txtCardNumber.Value = AppConstants.CARD_MASK +
                                          creditCardNumber.Substring((creditCardNumber.Length -
                                                                      AppConstants.CREDIT_CARD_DISPLAY_CHARS));
                }
                else
                {
                    txtCardNumber.Value = AppConstants.CARD_MASK + creditCardNumber;
                }


                ListItem selectedCard = ddlCardType.Items.FindByValue(creditCardDetails.CardType);
                if (selectedCard != null)
                {
                    ddlCardType.SelectedItem.Selected = false;
                    selectedCard.Selected = true;
                }
                ListItem selectedMonth = ddlExpiryMonth.Items.FindByValue(creditCardDetails.ExpiryDate.Month.ToString());
                if (selectedMonth != null)
                    selectedMonth.Selected = true;

                ListItem selectedYear = ddlExpiryYear.Items.FindByValue(creditCardDetails.ExpiryDate.Year.ToString());
                if (selectedYear != null)
                    selectedYear.Selected = true;
            }
        }

        /// <summary>
        /// SetControlVisibility
        /// </summary>
        private void SetControlVisibility()
        {
            if (this.isRoomConfirmed)
            {
                if (currentLanguage.ToUpper() == LanguageConstant.LANGUAGE_GERMAN)
                {
                    ddlTitle.Enabled = true;
                }
                txtFNameRoom.Disabled = true;
                txtLNameRoom.Disabled = true;
                txtMembershipNumberRoom.Disabled = true;
                if (txtMembershipNumberRoom.Value == txtMembershipNumberRoom.Attributes["rel"])
                {
                    txtMembershipNumberRoom.Value = string.Empty;
                }
            }
        }

        /// <summary>
        /// If the user is logged in, sets the user details to the text boxes
        /// </summary>
        public void SetLoggedUserDetails()
        {
            LoyaltyDetailsEntity loyaltyDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
            NameController nameController = new NameController();
            UserProfileEntity userProfileEntity = null;
            if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null)
            {
                if (UserProfileInformationSessionWrapper.UserProfileInformation != null)
                {
                    userProfileEntity = UserProfileInformationSessionWrapper.UserProfileInformation;
                }
                else
                {
                    userProfileEntity = nameController.FetchUserDetails(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID);
                }
            }

            if (loyaltyDetails != null)
            {
                ddlTitle.Text = loyaltyDetails.Title;
				//Added as part of SCANAM-537
                if (!string.IsNullOrEmpty(loyaltyDetails.NativeFirstName) && !string.IsNullOrEmpty(loyaltyDetails.NativeLastName))
                {
                    txtFNameRoom.Value = loyaltyDetails.NativeFirstName;
                    txtLNameRoom.Value = loyaltyDetails.NativeLastName;
                }
                else
                {
                    txtFNameRoom.Value = loyaltyDetails.FirstName;
                    txtLNameRoom.Value = loyaltyDetails.SurName;
                }

                ddlCountry.SelectedValue = loyaltyDetails.Country;
                txtEmail.Value = loyaltyDetails.Email;


                if (loyaltyDetails.Telephone1 != null && loyaltyDetails.Telephone1.Length > 0)
                {
                    txtTelephone.Value = Utility.SetPhoneCountryCode(ddlMobileNumber, loyaltyDetails.Telephone1);
                }

                txtMembershipNumberRoom.Value = loyaltyDetails.UserName;
                if (currentLanguage.ToUpper() == LanguageConstant.LANGUAGE_GERMAN)
                {
                    ddlTitle.Enabled = false;
                }
                txtFNameRoom.Disabled = true;
                txtLNameRoom.Disabled = true;
                txtMembershipNumberRoom.Disabled = true;
                ChangeUpdtProfileCtrlStatus(true);
            }
            if (userProfileEntity != null)
            {
                PopulateSpecialRequests(userProfileEntity.UserPreference);
            }
        }

        public string GetPhoneNumberWithoutCountryCode(string phoneNo)
        {
            return Utility.SetPhoneCountryCode(ddlMobileNumber, phoneNo);
        }


        /// <summary>
        /// This method updates the status of txtboxes in the control
        /// </summary>
        /// <param name="disable"></param>
        public void ChangeUpdtProfileCtrlStatus(bool disable)
        {
            ddlCountry.Enabled = !disable;
            txtEmail.Disabled = disable;
            txtTelephone.Disabled = disable;
            ddlMobileNumber.Enabled = !disable;
        }


        /// <summary>
        /// Populate the Expiry Date drop down list(non generic)
        /// </summary>
        private void SetExpiryDateList()
        {
            ddlExpiryMonth.Items.Add(new ListItem(AppConstants.HYPHEN, AppConstants.DEFAULT_VALUE_CONST));
            for (int monthCount = 1; monthCount <= AppConstants.TOTALMONTHS; monthCount++)
            {
                IFormatProvider cultureInfo =
                    new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
                string monthName = (new DateTime(1, monthCount, 1)).ToString("MMM", cultureInfo);
                ddlExpiryMonth.Items.Add(new ListItem(monthName, monthCount.ToString()));
            }
            int currentYear = DateTime.Today.Year;
            int currentMonth = DateTime.Today.Month;
            if (currentMonth == 12)
                currentYear = currentYear + 1;
            ddlExpiryYear.Items.Add(new ListItem(AppConstants.HYPHEN, AppConstants.DEFAULT_VALUE_CONST));
            for (int yearCount = 0; yearCount < AppConstants.TOTALYEARS; yearCount++)
            {
                ddlExpiryYear.Items.Add(new ListItem((currentYear + yearCount).ToString()));
            }
        }

        /// <summary>
        /// Set the drop down list of the credit card types
        /// </summary>
        private void SetCreditCardDropDown()
        {
            OrderedDictionary creditCardTypesMap = DropDownService.GetCreditCardsCodes();
            if (creditCardTypesMap != null)
            {
                foreach (string key in creditCardTypesMap.Keys)
                {
                    ddlCardType.Items.Add(new ListItem(creditCardTypesMap[key].ToString(), key));
                }
            }
            ListItem selectedDefaultItem = ddlCardType.Items.FindByValue(AppConstants.DEFAULT_VALUE_CONST);
            if (selectedDefaultItem != null)
            {
                ddlCardType.ClearSelection();
                selectedDefaultItem.Selected = true;
            }
        }



        /// <summary>
        /// This will display the GuranteeDiv.Which will contain the fields for the Gurantee infromations.
        /// </summary>
        /// <param name="rateCategoryID">RateCategoryId</param>
        /// <param name="noRateCodeButDisplayGuarantee">Wheather to display gurantee information to the user.</param>
        /// <param name="creditCardGuranteeType">Type of the credit card gurantee.</param>
        private void DisplayGuranteeDiv(string rateCategoryID, bool noRateCodeButDisplayGuarantee,
                                        string creditCardGuranteeType, bool paymentFallback)
        {
            RateCategory rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(rateCategoryID);
            Block block = null;
            if (null == rateCategory)
            {
                block = ContentDataAccess.GetBlockCodePages(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
            }
            if (((null != block) && (block.SixPMHoldAvailable)) ||
                ((null != rateCategory) && (rateCategory.HoldGuranteeAvailable)))
            {
                blkHoldRoom.Visible = true;

                lblGuranteewithRadio.Visible = true;
                lblGuranteewithoutRadio.Visible = false;
                isBlockDisplay = true;
            }
            else
            {
                lblGuranteewithRadio.Visible = false;
                lblGuranteewithoutRadio.Visible = true;
                lblGuranteewithRadioCCDivInfo.Visible = false;
                isBlockDisplay = false;
                txtCardHolder.Attributes.Add("class", "text-input defaultColor");
                txtCardNumber.Attributes.Add("class", "text-input defaultColor");
                ddlCardType.Attributes.Add("class", "text-input defaultColor");
            }
            if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType.Equals(SearchType.REDEMPTION))
            {
                blkHoldRoom.Visible = false;
                HideUnhideCreditCardDiv.Visible = false;
                panHashCreditCardDiv.Visible = false;
                lblGuranteewithRadio.Visible = false;
                lblGuranteewithoutRadio.Visible = true;
                lblGuranteewithRadioCCDivInfo.Visible = false;
                string redemptionGuranteeText = (null != CurrentPage["RewardNights"])
                                                    ? CurrentPage["RewardNights"].ToString()
                                                    : string.Empty;
                lblGuranteewithoutRadio.InnerHtml = redemptionGuranteeText;
            }
            else
            {
                DisplayGuranteeText(rateCategory, noRateCodeButDisplayGuarantee, creditCardGuranteeType, paymentFallback);
            }
        }

        /// <summary>
        /// Displays the gurantee text.
        /// </summary>
        /// <param name="rateCategory">The rate category.</param>
        /// <param name="noRateCodeButDisplayGuarantee">if set to <c>true</c> [no rate code but display guarantee].</param>
        /// <param name="creditCardGuranteeType">Type of the credit card gurantee.</param>
        private void DisplayGuranteeText(RateCategory rateCategory, bool noRateCodeButDisplayGuarantee,
                                         string creditCardGuranteeType, bool paymentFallback)
        {
            string[] prepaidGuranteeTypes = AppConstants.GUARANTEE_TYPE_PRE_PAID;

            string spanHoldText = (CurrentPage["GTDCC1800"] != null)
                                      ? CurrentPage["GTDCC1800"].ToString()
                                      : string.Empty;

            spanHoldText = spanHoldText.Replace("<DIV>", "");
            spanHoldText = spanHoldText.Replace("</DIV>", "");
            spanHoldRoom.InnerHtml = spanHoldText;
            System.Web.UI.HtmlControls.HtmlGenericControl displayTextLabel = new HtmlGenericControl();
            if (lblGuranteewithRadio.Visible == true || isBlockDisplay)
            {
                displayTextLabel = txtGuranteeWithRadio;
            }
            else if (lblGuranteewithoutRadio.Visible == true || !isBlockDisplay)
            {
                displayTextLabel = txtGuranteeWithoutRadio;
            }
            if (!noRateCodeButDisplayGuarantee && rateCategory != null)
            {
                creditCardGuranteeType = rateCategory.CreditCardGuranteeType;
                if (!paymentFallback && (prepaidGuranteeTypes != null) && prepaidGuranteeTypes.Contains(rateCategory.CreditCardGuranteeType))
                {
                    creditCardGuranteeType = string.Format("{0}-Prepaid", rateCategory.CreditCardGuranteeType);
                }
            }
            string displayTextLabelText = (CurrentPage[creditCardGuranteeType] != null)
                                              ? CurrentPage[creditCardGuranteeType].ToString()
                                              : string.Empty;
            displayTextLabelText = displayTextLabelText.Replace("<DIV>", "");
            displayTextLabelText = displayTextLabelText.Replace("</DIV>", "");
            displayTextLabel.InnerHtml = displayTextLabelText;
        }

        /// <summary>
        /// This will find out if the guarantee information need to be displayed or not.
        /// Along with it finds if no rate code associated and credit card guarantee type text.
        /// </summary>
        /// <param name="rateCategoryId">Rate category id if associated with this booking.</param>
        /// <param name="blockCode">Block code</param>
        /// <param name="noRateCodeButDisplayGuarantee">True if no rate code associated but display the guarantee information.</param>
        /// <param name="creditCardGuranteeType">Credit card guarantee type text</param>
        /// <returns>True if this is block code booking and if block code does not need the credit card information</returns>
        private bool IsTruelyPrepaid(string rateCategoryId, string blockCode,
                                     out bool noRateCodeButDisplayGuarantee, out string creditCardGuranteeType)
        {
            bool returnVaue = false;
            noRateCodeButDisplayGuarantee = false;
            creditCardGuranteeType = string.Empty;
            if (Utility.IsBlockCodeBooking)
            {
                if (rateCategoryId == AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
                {
                    Block blockPage = ContentDataAccess.GetBlockCodePages(blockCode);
                    if (null != blockPage)
                    {
                        if (blockPage.CaptureGuarantee)
                        {
                            noRateCodeButDisplayGuarantee = true;
                            if (!string.IsNullOrEmpty(blockPage.GuranteeType))
                            {
                                creditCardGuranteeType = blockPage.GuranteeType;
                            }
                        }
                        else
                        {
                            returnVaue = true;
                        }
                    }
                }
            }
            return returnVaue;
        }

        /// <summary>
        /// Generic Function to Displays the guarantee information based on the rate category.
        /// </summary>
        /// <param name="rateCategoryID">Rate category id</param>
        private void DisplayHoldOptionIfRequired(string rateCategoryID, bool paymentFallback)
        {
            HideUnhideCreditCardDiv.Attributes.Add("class", "ccInfoDetails fltLft" + " Room" + roomIdentifier);
            //rdoHoldRoom.Attributes.Add("rel", "Room" + roomIdentifier);
            rdoLateArrivalGurantee.Attributes.Add("rel", "Room" + roomIdentifier);

            string campaignCode = string.Empty;
            if ((null != SearchCriteriaSessionWrapper.SearchCriteria) &&
                (!string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode)))
            {
                campaignCode = SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode;
            }

            bool noRateCodeButDisplayGuarantee;
            string creditCardGuranteeType;
            if (IsTruelyPrepaid(rateCategoryID, campaignCode, out noRateCodeButDisplayGuarantee,
                                out creditCardGuranteeType))
            {
                HideUnhideGuranteeDiv.Visible = false;
            }
            else
            {
                DisplayGuranteeDiv(rateCategoryID, noRateCodeButDisplayGuarantee, creditCardGuranteeType, paymentFallback);
            }
            if (RoomRateUtil.IsSaveRateCategory(rateCategoryID) && !paymentFallback)
            {
                HideUnhideCreditCardDiv.Visible = false;
                panHashCreditCardDiv.Visible = false;
            }
        }

        /// <summary>
        /// Set the drop down list of Bed Perference
        /// </summary>
        private void SetBedPeferenceDropDown()
        {
            List<SelectedRoomAndRateEntity> selectedRoomAndRateEntity = new List<SelectedRoomAndRateEntity>();
            List<RoomRateEntity> roomRates = null;
            if ((BookingEngineSessionWrapper.IsModifyBooking) && (BookingEngineSessionWrapper.DirectlyModifyContactDetails)
                && HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable == null
                )
            {
                if (BookingEngineSessionWrapper.BookingDetails != null && BookingEngineSessionWrapper.BookingDetails.HotelRoomRate != null)
                {
                    string roomTypeCode = BookingEngineSessionWrapper.BookingDetails.HotelRoomRate.RoomtypeCode;
                    string ratePlanCode = BookingEngineSessionWrapper.BookingDetails.HotelRoomRate.RatePlanCode;
                    if ((!string.IsNullOrEmpty(roomTypeCode)) && (!string.IsNullOrEmpty(ratePlanCode)))
                        if (!string.IsNullOrEmpty(roomTypeCode))
                        {
                            roomRates = new List<RoomRateEntity>();
                            RoomRateEntity roomRateEntity = new RoomRateEntity(roomTypeCode, ratePlanCode);
                            roomRates.Add(roomRateEntity);
                            if (roomRates != null)
                            {
                                setBedPreference(roomRates);
                            }
                        }
                }
            }
            else if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null)
            {
                SelectedRoomAndRateEntity roomAndRate = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[roomIdentifier]
                                                        as SelectedRoomAndRateEntity;

                if (null != roomAndRate)
                {
                    roomRates = roomAndRate.RoomRates;
                    if (roomRates != null)
                    {
                        setBedPreference(roomRates);
                    }
                }
            }
        }

        /// <summary>
        /// Clear Method.
        /// </summary>
        /// <param name="roomRates">List of RoomRateEntity</param>
        private void setBedPreference(List<RoomRateEntity> roomRates)
        {
            List<RoomType> roomTypes = new List<RoomType>();
            List<RoomType> allRoomType = new List<RoomType>();
            if (roomRates != null && roomRates.Count > 0)
            {
                foreach (RoomRateEntity roomRate in roomRates)
                {
                    if (RoomRateUtil.GetRoomType(roomRate.RoomTypeCode) != null)
                    {
                        roomTypes.Add(RoomRateUtil.GetRoomType(roomRate.RoomTypeCode));
                    }
                }
                RoomCategory rCategory = RoomRateUtil.GetRoomCategory(roomRates[0].RoomTypeCode);
                if(rCategory!=null)
                    allRoomType = new List<RoomType>(rCategory.RoomTypes);
            }
            if (roomTypes != null && roomTypes.Count > 0)
            {
                Utility.SetRoomTypeSortOrderAsInOpera(roomTypes);

                // Sort the room types on the bed preference's priority given
                roomTypes.Sort(new RoomTypePriorityComparer());
            }
            if (roomTypes != null && roomTypes.Count > 0 && allRoomType != null && allRoomType.Count > 0)
            {
                foreach (var item in roomTypes)
                {
                    allRoomType.RemoveAll(obj => obj.BedTypeCode == item.BedTypeCode);
                }
            }

            ddlBedTypePreference.Items.Clear();
            ddlBedTypePreference.Items.Add(new ListItem(selectBedtype, AppConstants.DEFAULT_VALUE_CONST));
            string notAvailableText = string.Format("{0}{1}{2}{3}", AppConstants.SPACE, AppConstants.HYPHEN, AppConstants.SPACE, WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/NotAvailable"));
            foreach (RoomType roomType in roomTypes)
            {
                ddlBedTypePreference.Items.Add(new ListItem(roomType.BedTypeDescription, roomType.OperaRoomTypeId));
            }
            if (allRoomType != null && allRoomType.Count > 0)
            {
                foreach (RoomType roomType in allRoomType)
                {
                    if (roomType != null)
                    {
                        ListItem item = new ListItem((roomType.BedTypeDescription + notAvailableText), roomType.OperaRoomTypeId);
                        ddlBedTypePreference.Items.Add(item);
                        ddlBedTypePreference.Items[ddlBedTypePreference.Items.IndexOf(item)].Attributes.Add("disabled", "disabled");
                        ddlBedTypePreference.Items[ddlBedTypePreference.Items.IndexOf(item)].Attributes.Add("style", "font-style:italic");
                        
                    }
                    // ddlBedTypePreference.Items[ddlBedTypePreference.Items.IndexOf(item)].Attributes.Add("style", "color:red;");
                }
            }
            if (BookingEngineSessionWrapper.IsModifyBooking && ddlBedTypePreference.Items.Count > 0 && IsRoomModifiable)
            {
                    ddlBedTypePreference.SelectedIndex = 0;
            }
            if (!BookingEngineSessionWrapper.IsModifyBooking && ddlBedTypePreference.Items.Count >= 2 && IsRoomModifiable)
            {
                ddlBedTypePreference.SelectedIndex = 1;
            }
            else if (BookingEngineSessionWrapper.IsModifyBooking && ddlBedTypePreference.Items.Count > 0 && !IsRoomModifiable)
            {
                if (!string.IsNullOrEmpty(dataSource.BedTypePreferenceCode))
                {
                    if(ddlBedTypePreference.Items.FindByValue(dataSource.BedTypePreferenceCode) != null) 
                        ddlBedTypePreference.SelectedValue = dataSource.BedTypePreferenceCode;
                    else if(ddlBedTypePreference.Items.Count >= 1)
                        ddlBedTypePreference.SelectedIndex = 1;
                    else
                        ddlBedTypePreference.SelectedIndex = 0;
                }
            }
            int roomIdentifierForShoppingCart = roomIdentifier + 1;

        }

        /// <summary>
        /// Populate Special Requests from specialRequestList entity.
        /// </summary>
        /// <param name="userPreferenceEntity">
        /// User Preference List
        /// </param>
        private void PopulateSpecialRequests(UserPreferenceEntity[] userPreferenceEntity)
        {
            if (userPreferenceEntity != null)
            {
                int totalRequest = userPreferenceEntity.Length;
                for (int requestCount = 0; requestCount < totalRequest; requestCount++)
                {
                    switch (userPreferenceEntity[requestCount].RequestValue)
                    {
                        case UserPreferenceConstants.LOWERFLOOR:
                        case UserPreferenceConstants.HIGHFLOOR:
                            {
                                ExpandOtherRequests();
                                ddlRoomFloor.SelectedValue = userPreferenceEntity[requestCount].RequestValue;
                                break;
                            }
                        case UserPreferenceConstants.AWAYFROMELEVATOR:
                        case UserPreferenceConstants.NEARELEVATOR:
                            {
                                ExpandOtherRequests();
                                ddlElevator.SelectedValue = userPreferenceEntity[requestCount].RequestValue;
                                break;
                            }
                        case UserPreferenceConstants.SMOKING:
                        case UserPreferenceConstants.NOSMOKING:
                            {
                                ExpandOtherRequests();
                                ddlSmoking.SelectedValue = userPreferenceEntity[requestCount].RequestValue;
                                break;
                            }
                        case UserPreferenceConstants.ACCESSIBLEROOM:
                            {
                                ExpandOtherRequests();
                                chkAccessableRoom.Checked = true;
                                break;
                            }
                        case UserPreferenceConstants.ALLERGYROOM:
                            {
                                ExpandOtherRequests();
                                chkAllergyRooms.Checked = true;
                                break;
                            }
                        case UserPreferenceConstants.EARLYCHECKIN:
                            {
                                ExpandOtherRequests();
                                chkEarlyCheckIn.Checked = true;
                                break;
                            }
                        case UserPreferenceConstants.LATECHECKOUT:
                            {
                                ExpandOtherRequests();
                                chkLateCheckOut.Checked = true;
                                break;
                            }
                        case UserPreferenceConstants.PETFRIENDLYROOM:
                            {
                                ExpandOtherRequests();
                                ChkPetfriendlyroom.Checked = true;
                                break;
                            }
                    }
                }
            }
        }

        /// <summary>
        /// Populate Special Requests from specialRequestList entity.
        /// </summary>
        /// <param name="specialRequestList">
        /// Special Request List
        /// </param>
        private void PopulateSpecialRequests(SpecialRequestEntity[] specialRequestList)
        {
            if (specialRequestList != null)
            {
                int totalRequest = specialRequestList.Length;
                for (int requestCount = 0; requestCount < totalRequest; requestCount++)
                {
                    switch (specialRequestList[requestCount].RequestValue)
                    {
                        case UserPreferenceConstants.LOWERFLOOR:
                        case UserPreferenceConstants.HIGHFLOOR:
                            {
                                ExpandOtherRequests();
                                ddlRoomFloor.SelectedValue = specialRequestList[requestCount].RequestValue;
                                break;
                            }
                        case UserPreferenceConstants.AWAYFROMELEVATOR:
                        case UserPreferenceConstants.NEARELEVATOR:
                            {
                                ExpandOtherRequests();
                                ddlElevator.SelectedValue = specialRequestList[requestCount].RequestValue;

                                break;
                            }
                        case UserPreferenceConstants.NOSMOKING:
                        case UserPreferenceConstants.SMOKING:
                            {
                                ExpandOtherRequests();
                                ddlSmoking.SelectedValue = specialRequestList[requestCount].RequestValue;
                                break;
                            }
                        case UserPreferenceConstants.ACCESSIBLEROOM:
                            {
                                ExpandOtherRequests();
                                chkAccessableRoom.Checked = true;
                                break;
                            }
                        case UserPreferenceConstants.ALLERGYROOM:
                            {
                                ExpandOtherRequests();
                                chkAllergyRooms.Checked = true;
                                break;
                            }
                        case UserPreferenceConstants.EARLYCHECKIN:
                            {
                                ExpandOtherRequests();
                                chkEarlyCheckIn.Checked = true;
                                break;
                            }
                        case UserPreferenceConstants.LATECHECKOUT:
                            {
                                ExpandOtherRequests();
                                chkLateCheckOut.Checked = true;
                                break;
                            }
                        case UserPreferenceConstants.PETFRIENDLYROOM:
                            {
                                ExpandOtherRequests();
                                ChkPetfriendlyroom.Checked = true;
                                break;
                            }
                    }
                }
            }
        }

        /// <summary>
        /// Expand the Other request section in Booking details page if user books from add anather Hotel page.
        /// </summary>
        private void ExpandOtherRequests()
        {
            otherRequestContents.Attributes.Add("style", "display:block");
            otherRequestsHeading.Attributes.Add("class", "trigger active");
            roomPref.Attributes.Add("style", "display:none");
        }


        #region Methods in case of Modify Combo Reservation

        /// <summary>
        /// DisableRoomInformationContents
        /// </summary>
        public void DisableRoomInformationContents()
        {
            ddlMobileNumber.CssClass = "frmSelect fltLft";
            if (currentLanguage.ToUpper() == LanguageConstant.LANGUAGE_GERMAN)
            {
                ddlTitle.Enabled = false;
            }
            txtFNameRoom.Disabled = true;
            txtLNameRoom.Disabled = true;
            txtEmail.Disabled = true;
            ddlMobileNumber.Enabled = false;
            ddlCountry.Enabled = false;
            txtTelephone.Disabled = true;
            txtMembershipNumberRoom.Disabled = true;
            HideUnhideCreditCardDiv.Disabled = true;
            panHashCreditCardDiv.Disabled = true;
            HideUnhideGuranteeDiv.Disabled = true;
            DisableOtherRequest();
            DisableCreditCardInformation();
            ddlBedTypePreference.Enabled = false;
        }

        /// <summary>
        /// DisplayText
        /// </summary>
        public void DisplayText()
        {
            roomInfoText.Visible = true;
            roomInfoText.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/NotModifiableMsz");
        }

        /// <summary>
        /// DisableOtherRequest
        /// </summary>
        private void DisableOtherRequest()
        {

            ddlRoomFloor.Enabled = false;
            ddlElevator.Enabled = false;
            ddlSmoking.Enabled = false;
            chkAccessableRoom.Disabled = true;
            chkAllergyRooms.Disabled = true;
            ChkPetfriendlyroom.Disabled = true;
            chkEarlyCheckIn.Disabled = true;
            chkLateCheckOut.Disabled = true;
            additionalRequest.Disabled = true;
        }

        /// <summary>
        /// DisableCreditCardInformation
        /// </summary>
        private void DisableCreditCardInformation()
        {
            txtCardHolder.Disabled = true;
            txtCardNumber.Disabled = true;
            ddlExpiryMonth.Enabled = false;
            ddlExpiryYear.Enabled = false;
            divCreditCardUpdate.Disabled = true;
            ddlCardType.Enabled = false;
            updateCreditCard.Disabled = true;
            updateCreditCardPanHash.Disabled = true;
        }

        #endregion

    }
   
}