using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System.Linq;
using System.Web;
using Scandic.Scanweb.ExceptionManager;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// ShoppingCart
    /// </summary>
    public partial class ShoppingCart : EPiServer.UserControlBase, INavigationTraking
    {
        private string pageType = string.Empty;
        private AvailabilityController availabilityController = null;
        private int taLocationID = 0;
        private bool hideHotelTARatings = true;
        /// <summary>
        /// Gets m_AvailabilityController
        /// </summary>
        private AvailabilityController m_AvailabilityController
        {
            get
            {
                if (availabilityController == null)
                {
                    availabilityController = new AvailabilityController();
                }
                return availabilityController;
            }
        }

        public event EventHandler SetRoomUnavailableMessage;
        public event EventHandler ShowBonusRateUnavailableMessage;

        /// <summary>
        /// Sets the type of the page.
        /// </summary>
        /// <param name="pageType">Type of the page.</param>
        public void SetPageType(string pageType)
        {
            this.pageType = pageType;
            editStayCart.PageType = pageType;
            if (pageType == EpiServerPageConstants.BOOKING_DETAILS_PAGE.ToString() ||
                pageType == EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS.ToString())
                goBackLink.Visible = false;
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if ((!Page.IsPostBack) && (Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation))
                    Utility.PrepopulateNonModifiableRoomsInSelectedRoomRatesHashTable();

                if (this.Visible)
                {
                    SetShoppingCartInnerControlsVisibility(pageType);
                    SetShoppingCartDataFields();
                }

                if (!Page.IsPostBack)
                {
                    bool showTotalRates = false;
                    SetAddNewRoomDropDowns();
                    if (pageType == EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH ||
                        (HttpContext.Current.Request.UrlReferrer != null &&
                        HttpContext.Current.Request.UrlReferrer.ToString().Contains(AppConstants.RESERVATION_MODIFY_SELECT_RATE)))
                    {
                        SetTotalRateAndCurrencyCode();
                        showTotalRates = true;
                    }
                    else if (pageType == EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS)
                    {
                        SetTotalRateAndCurrencyCodeForModifyFlow();
                        showTotalRates = true;
                    }
                    else if (HygieneSessionWrapper.IsComboReservation)
                    {
                        showTotalRates = true;
                    }
                    else
                    {
                        HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                        if (search != null && search.ListRooms != null && search.ListRooms.Count > 0)
                        {
                            showTotalRates = true;
                            foreach (HotelSearchRoomEntity roomSearch in search.ListRooms)
                            {
                                if (roomSearch.IsBlocked == false)
                                {
                                    showTotalRates = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (showTotalRates)
                    {

                        if (pageType == EpiServerPageConstants.SELECT_HOTEL_PAGE)
                        {
                            totalRatesBlock.Style.Add("display", "none");
                        }
                        else
                        {
                            totalRatesBlock.Style.Add("display", "block");
                        }
                    }
                    else
                    {
                        totalRatesBlock.Style.Add("display", "none");
                    }
                }
                else
                {
                    if (pageType == EpiServerPageConstants.SELECT_HOTEL_PAGE)
                    {
                        totalRatesBlock.Style.Add("display", "none");
                    }
                    else
                    {
                        totalRatesBlock.Style.Add("display", "block");
                    }
                }
                btnRoomSearch.Attributes.Add("onclick", "javascript:return performValidationForChildAge();");
                SetTabIndexControl();
                //if (SearchCriteriaSessionWrapper.SearchCriteria != null && !string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode))

                if (pageType == EpiServerPageConstants.SELECT_RATE_PAGE ||
                  pageType == EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE)
                {
                    if (!WebUtil.HideTARating(false, this.hideHotelTARatings))
                    {
                        divTripAdvisorRating.InnerHtml = WebUtil.CreateTripAdvisorRatingWidget(taLocationID);
                        divTripAdvisorContainer.Style.Add("display", "block");

                    }
                    else
                    {
                        divTripAdvisorRating.InnerHtml = "";
                        divTripAdvisorContainer.Style.Add("display", "none");
                    }
                }
            }
            catch (ContentDataAccessException cdaException)
            {
                if (string.Equals(cdaException.ErrorCode, AppConstants.BONUS_CHEQUE_NOT_FOUND,
                                  StringComparison.InvariantCultureIgnoreCase))
                {
                    if (ShowBonusRateUnavailableMessage != null)
                        ShowBonusRateUnavailableMessage(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/bonuschequenotfound"), null);
                }
                else
                    WebUtil.ApplicationErrorLog(cdaException);
            }
        }

        /// <summary>
        /// Set the tabIndex to the controls
        /// </summary>
        private void SetTabIndexControl()
        {
            Int16 tabIndex = 15;
            for (int ctrlCount = 0; ctrlCount < this.Controls.Count; ctrlCount++)
            {
                if (this.Controls[ctrlCount].Controls.Count > 0)
                {
                    Utility.FetchInnerControlSetTabIndex(this.Controls[ctrlCount], ref tabIndex);
                }
                else
                {
                    Utility.SetTabIndex(this.Controls[ctrlCount], ref tabIndex);
                }
            }
        }

        /// <summary>
        /// Called when [remove room_ click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void OnRemoveRoom_Click(object sender, EventArgs e)
        {
            Reservation2SessionWrapper.AvailabilityCalendar = null;
            Reservation2SessionWrapper.AvailabilityCalendarAccessed = false;

            int removeRoomIndex;
            int.TryParse(removeRoomIndexHiddenField.Value, out removeRoomIndex);
            removeRoomIndex--;

            try
            {
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if (search != null && search.ListRooms != null && search.ListRooms.Count > 0 && removeRoomIndex >= 0 &&
                    removeRoomIndex < search.ListRooms.Count && search.ListRooms[removeRoomIndex] != null)
                {
                    removeRoomIndexHiddenField.Value = string.Empty;
                    if (search.ListRooms[removeRoomIndex].IsBlocked)
                    {
                        WebUtil.UnblockRoom(search.ListRooms[removeRoomIndex].RoomBlockReservationNumber);
                        search.ListRooms[removeRoomIndex].RoomBlockReservationNumber = string.Empty;
                        search.ListRooms[removeRoomIndex].IsBlocked = false;
                    }
                    search.ListRooms.RemoveAt(removeRoomIndex);
                    search.RoomsPerNight = search.ListRooms.Count;

                    IList<RoomStay> listRoomStay = HotelRoomRateSessionWrapper.ListRoomStay;
                    if (listRoomStay != null && listRoomStay.Count > 0 && removeRoomIndex < listRoomStay.Count)
                    {
                        listRoomStay.RemoveAt(removeRoomIndex);
                    }

                    IList<BaseRoomRateDetails> listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                    if (listRoomRateDetails != null && listRoomRateDetails.Count > 0 &&
                        removeRoomIndex < listRoomRateDetails.Count)
                    {
                        listRoomRateDetails.RemoveAt(removeRoomIndex);
                    }

                    Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                    if (selectedRoomAndRatesHashTable != null &&
                        selectedRoomAndRatesHashTable.ContainsKey(removeRoomIndex))
                    {
                        selectedRoomAndRatesHashTable.Remove(removeRoomIndex);
                        Hashtable newSelectedRoomAndRatesHashTable = new Hashtable();
                        for (int i = 0, j = 0; i < AppConstants.MAX_BOOKING_ROOMS_ALLOWED; i++)
                        {
                            if (selectedRoomAndRatesHashTable.ContainsKey(i))
                            {
                                newSelectedRoomAndRatesHashTable.Add(j++, selectedRoomAndRatesHashTable[i]);
                            }
                        }
                        HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable = newSelectedRoomAndRatesHashTable;
                    }
                    if (pageType == EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE)
                    {
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE), false);
                    }
                    else
                    {
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE), false);
                    }
                }
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
        }

        /// <summary>
        /// Sets the shopping cart inner controls visibility.
        /// </summary>
        /// <param name="pageType">Type of the page.</param>
        private void SetShoppingCartInnerControlsVisibility(string pageType)
        {
            if (pageType == EpiServerPageConstants.SELECT_HOTEL_PAGE)
            {
                totalRatesBlock.Style.Add("display", "none");
            }

            if (pageType == EpiServerPageConstants.SELECT_RATE_PAGE ||
                pageType == EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE)
            {
                divTripAdvisorRating.Style.Add("display", "block");
                divTripAdvisorContainer.Style.Add("display", "block");
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if (search != null && search.ListRooms != null && search.ListRooms.Count > 0)
                {
                    bool isAllRoomsBlocked = true;
                    foreach (HotelSearchRoomEntity roomSearch in search.ListRooms)
                    {
                        if (roomSearch.IsBlocked == false)
                        {
                            isAllRoomsBlocked = false;
                            break;
                        }
                    }

                    if (isAllRoomsBlocked == true)
                    {
                        bookNowBtnBlock.Style.Add("display", "block");

                        if (search.ListRooms.Count != AppConstants.MAX_BOOKING_ROOMS_ALLOWED &&
                            search.SearchingType != SearchType.REDEMPTION)
                        {
                            addRoomBlock.Style.Add("display", "block");
                        }
                    }
                    else
                    {
                        string errMsg = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/owserror/room_notavailable");
                        if (SetRoomUnavailableMessage != null)
                            SetRoomUnavailableMessage(errMsg, null);
                        bookNowBtnBlock.Style.Add("display", "none");
                        addRoomBlock.Style.Add("display", "none");
                    }
                }
            }
            else
            {
                divTripAdvisorContainer.Style.Add("display", "none");
                divTripAdvisorRating.Style.Add("display", "none");
            }
            if ((pageType == EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS) ||
                (pageType == EpiServerPageConstants.BOOKING_DETAILS_PAGE))
            {
                editYourStayLink.Style.Add("display", "none");
                goBackLink.Style.Add("display", "block");
                if (BookingEngineSessionWrapper.IsModifyBooking)
                {
                    if (BookingEngineSessionWrapper.DirectlyModifyContactDetails)
                    {
                        goBackLink.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_CHANGE_DATES);
                    }
                    else
                    {
                        goBackLink.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE);
                    }
                }
                else
                {
                    goBackLink.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE);
                }
            }
        }

        /// <summary>
        /// Sets the shopping cart data fields.
        /// </summary>
        private void SetShoppingCartDataFields()
        {
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
            if (null != searchCriteria)
            {
                if (searchCriteria.SearchedFor != null)
                {
                    if (searchCriteria.SelectedHotelCode != null)
                    {
                        HotelDestination hotel = m_AvailabilityController.GetHotelDestination(searchCriteria.SelectedHotelCode);
                        if (hotel != null)
                        {
                            lnkSearchedCity.InnerText = hotel.Name;
                            lnkSearchedCity.Attributes.Add("href", hotel.HotelPageURL);
                            lnkSearchedCity.Visible = true;
                            searchedCity.Visible = false;
                            imgHotel.ImageUrl = hotel.ImageURL;
                            imgHotel.Visible = true;
                            taLocationID = hotel.TALocationID;
                            hideHotelTARatings = hotel.HideHotelTARating;

                        }
                    }
                    else
                    {
                        searchedCity.InnerText = searchCriteria.SearchedFor.SearchString;
                        lnkSearchedCity.Visible = false;
                        searchedCity.Visible = true;
                        imgHotel.Visible = false;
                    }
                }

                labelArrivalDate.InnerText = WebUtil.GetDayFromDate(searchCriteria.ArrivalDate) + AppConstants.SPACE +
                                             Core.DateUtil.DateToDDMMYYYYString(searchCriteria.ArrivalDate);
                labelDepartureDate.InnerText = WebUtil.GetDayFromDate(searchCriteria.DepartureDate) + AppConstants.SPACE +
                                               Core.DateUtil.DateToDDMMYYYYString(searchCriteria.DepartureDate);

                AddShoppingCartRoomBoxes();

                if (!SelectHotelUtil.IsSearchToBeDone() && (HotelResultsSessionWrapper.HotelResults == null ||
                    (HotelResultsSessionWrapper.HotelResults != null && HotelResultsSessionWrapper.HotelResults.Count <= 0)) &&
                    !AlternateHotelsSessionWrapper.DisplayNoHotelsAvailable)
                {
                    chandgedDetails.Style.Add("display", "none");
                    editYourStayLink.Style.Add("display", "none");
                    roomWrap.Style.Add("display", "none");
                }

                SetNewRoomIndex(searchCriteria);
                if (!(pageType == EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS))
                    SetTotalRateAndCurrencyCode();
            }
            else
            {
                labelArrivalDate.InnerText = WebUtil.GetDayFromDate(DateTime.Now) + AppConstants.SPACE +
                                             Core.DateUtil.DateToDDMMYYYYString(DateTime.Now);
                labelDepartureDate.InnerText = WebUtil.GetDayFromDate(DateTime.Now.AddDays(1)) + AppConstants.SPACE +
                                               Core.DateUtil.DateToDDMMYYYYString(DateTime.Now.AddDays(1));
            }
        }

        /// <summary>
        /// Sets the new index of the room.
        /// </summary>
        /// <param name="searchCriteria">The search criteria.</param>
        private void SetNewRoomIndex(HotelSearchEntity searchCriteria)
        {
            if (searchCriteria != null && searchCriteria.ListRooms != null)
            {
                this.roomIndex.InnerText = (searchCriteria.ListRooms.Count + 1).ToString();
            }
        }

        /// <summary>
        /// Sets the total rate and currency code.
        /// </summary>
        private void SetTotalRateAndCurrencyCode()
        {
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
            Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
            double totalRate = 0;
            string currencyCode = string.Empty;
            BaseRateDisplay firstRateDisplayObject = null;
            SelectedRoomAndRateEntity firstSelectedRoomAndRateEntity = null;
            string firstRoomRatePlanCode = string.Empty;
            if (searchCriteria != null && searchCriteria.ListRooms != null && searchCriteria.ListRooms.Count > 0)
            {
                for (int i = 0; i < searchCriteria.ListRooms.Count; i++)
                {
                    if (pageType != EpiServerPageConstants.SELECT_HOTEL_PAGE)
                    {
                        if (selectedRoomAndRatesHashTable != null && selectedRoomAndRatesHashTable.ContainsKey(i))
                        {
                            int firstRoom = 0;
                            firstSelectedRoomAndRateEntity = selectedRoomAndRatesHashTable[0] as SelectedRoomAndRateEntity;
                            firstRateDisplayObject = Utility.GetRateCellDisplayObject(firstRoom, firstSelectedRoomAndRateEntity);
                            SelectedRoomAndRateEntity selectedRoomAndRateEntity = selectedRoomAndRatesHashTable[i] as SelectedRoomAndRateEntity;

                            if (selectedRoomAndRateEntity != null)
                            {
                                List<RoomRateEntity> selectedRoomRates = selectedRoomAndRateEntity.RoomRates;
                                if (selectedRoomRates != null && selectedRoomRates.Count > 0)
                                {
                                    RoomRateEntity roomRate = selectedRoomRates[0];

                                    if (roomRate != null)
                                    {
                                        firstRoomRatePlanCode = roomRate.RatePlanCode;

                                        if (searchCriteria.SearchingType == SearchType.REDEMPTION)
                                        {
                                            if (roomRate.PointsDetails != null)
                                                totalRate += roomRate.PointsDetails.PointsRequired;
                                            currencyCode = string.Empty;

                                            string appendClsRedemption = null;
                                            if (cartTotalDiv != null)
                                            {
                                                appendClsRedemption = cartTotalDiv.Attributes["class"].ToString();
                                            }
                                            if (!string.IsNullOrEmpty(appendClsRedemption))
                                            {
                                                appendClsRedemption += " cartRedemption";
                                                cartTotalDiv.Attributes.Add("class", appendClsRedemption);
                                            }
                                        }
                                        else if (searchCriteria.SearchingType == SearchType.BONUSCHEQUE)
                                        {
                                            if (roomRate.RatePlanCode == "BC")
                                            {
                                                totalRate += roomRate.TotalRate != null ? roomRate.TotalRate.Rate : 0;
                                            }
                                            currencyCode = roomRate.TotalRate != null ? roomRate.TotalRate.CurrencyCode : string.Empty;

                                            string appendClasss = null;
                                            if (cartTotalDiv != null)
                                            {
                                                appendClasss = cartTotalDiv.Attributes["class"].ToString();
                                            }
                                            if (!string.IsNullOrEmpty(appendClasss))
                                            {
                                                appendClasss += " cartTotalBonus";
                                                cartTotalDiv.Attributes.Add("class", appendClasss);
                                            }
                                        }
                                        else
                                        {
                                            if (roomRate.TotalRate != null)
                                            {
                                                totalRate += roomRate.TotalRate.Rate;
                                                currencyCode = roomRate.TotalRate.CurrencyCode;
                                            }
                                            if (searchCriteria.SearchingType == SearchType.VOUCHER)
                                            {
                                                this.totalRate.Attributes.Add("class", "giftvc");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                this.totalRate.InnerText = string.Empty;

                if (firstRateDisplayObject != null)
                {
                    string totalRateValue = string.Empty;
                    totalRateValue = firstRateDisplayObject.GetTotalRateDisplayStringForShoppingCart(true, totalRate, currencyCode);

                    if (BookingEngineSessionWrapper.HideARBPrice)
                    {
                        this.totalRate.InnerText = Utility.GetPrepaidString();
                        spnCartTerms.InnerText = string.Empty;
                    }
                    else
                    {
                        this.totalRate.InnerText = totalRateValue.Trim().TrimEnd(currencyCode.ToCharArray()).Trim();
                        spnCartTerms.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/InclTaxesFees");
                    }

                    if (searchCriteria.SearchingType == SearchType.VOUCHER || BookingEngineSessionWrapper.HideARBPrice)
                    {
                        currencyCode = string.Empty;
                    }

                    this.currencyCode.InnerText = currencyCode;
                }
            }
        }

        private void SetTotalRateAndCurrencyCodeForModifyFlow()
        {
            string totalRateandCurrency;
            totalRateandCurrency = Utility.SetTotalRateAndCurrencyCodeForModify();
            string[] ratestring = totalRateandCurrency.Trim().Split(AppConstants.SPACE_CHAR);

            int currencyIndex = (ratestring != null && ratestring.Length > 0) ? ratestring.Length - 1 : 0;
            string currencyCode = ratestring[currencyIndex];

            if (BookingEngineSessionWrapper.HideARBPrice)
            {
                this.totalRate.InnerText = Utility.GetPrepaidString();
                spnCartTerms.InnerText = string.Empty;
            }
            else
            {
                this.totalRate.InnerText = totalRateandCurrency.Trim().TrimEnd(currencyCode.ToCharArray()).Trim();
                spnCartTerms.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/InclTaxesFees");
            }

            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
            if (hotelSearch != null && hotelSearch.ListRooms.Count > 0 &&
                (hotelSearch.SearchingType == SearchType.VOUCHER || BookingEngineSessionWrapper.HideARBPrice))
                currencyCode = string.Empty;

            this.currencyCode.InnerText = currencyCode;
        }

        /// <summary>
        /// Adds the shopping cart room boxes.
        /// </summary>
        private void AddShoppingCartRoomBoxes()
        {
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
            Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;

            if (searchCriteria != null && searchCriteria.ListRooms != null && searchCriteria.ListRooms.Count > 0)
            {
                for (int i = 0; i < searchCriteria.ListRooms.Count; i++)
                {
                    HotelSearchRoomEntity room = searchCriteria.ListRooms[i];
                    ShoppingCartRoomBox shoppingCartRoomBox =
                        LoadControl("ShoppingCartRoomBox.ascx") as ShoppingCartRoomBox;
                    if (pageType == EpiServerPageConstants.SELECT_RATE_PAGE ||
                        pageType == EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE)
                    {
                        shoppingCartRoomBox.IsCloseButtonEnabled = ((searchCriteria.ListRooms[i].IsRoomConfirmed == true) ||
                                                                    (searchCriteria.ListRooms.Count == 1))
                                                                       ? false
                                                                       : true;
                    }
                    shoppingCartRoomBox.RoomNumber = (i + 1).ToString();
                    shoppingCartRoomBox.NoOfAdults = room.AdultsPerRoom.ToString();
                    shoppingCartRoomBox.NoOfChildren = room.ChildrenPerRoom.ToString();

                    if (room.ChildrenDetails != null && room.ChildrenDetails.ListChildren != null &&
                        room.ChildrenDetails.ListChildren.Count > 0)
                    {
                        shoppingCartRoomBox.IsChildrenAvailable = true;
                        string cot = string.Empty, extraBed = string.Empty, sharingBed = string.Empty;
                        Utility.GetLocaleSpecificChildrenAccomodationTypes(ref cot, ref extraBed, ref sharingBed);
                        shoppingCartRoomBox.ChildrenAccomodationText =
                            room.ChildrenDetails.GetChildrensAccommodationInString(sharingBed, cot, extraBed);
                    }

                    if (pageType != EpiServerPageConstants.SELECT_HOTEL_PAGE)
                    {
                        shoppingCartRoomBox.IsRateAvailable = true;
                        if (selectedRoomAndRatesHashTable != null && selectedRoomAndRatesHashTable.ContainsKey(i))
                        {
                            SelectedRoomAndRateEntity selectedRoomAndRateEntity =
                                selectedRoomAndRatesHashTable[i] as SelectedRoomAndRateEntity;

                            if (selectedRoomAndRateEntity != null)
                            {
                                shoppingCartRoomBox.IsRoomModifiable = room.IsRoomModifiable;
                                shoppingCartRoomBox.IsRateDisplayed = true;
                                shoppingCartRoomBox.RoomCategory = selectedRoomAndRateEntity.SelectedRoomCategoryName;
                                shoppingCartRoomBox.RoomCategoryID = selectedRoomAndRateEntity.RoomCategoryID;
                                bool showPricePerStay = Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage;
                                if ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation))
                                    SetRateStringInShoppingCartRoomBoxInModifyComboBooking(shoppingCartRoomBox,
                                                                                           selectedRoomAndRateEntity);
                                SetRateStringInShoppingCartRoomBox(i, shoppingCartRoomBox, selectedRoomAndRateEntity,
                                                                   showPricePerStay);
                            }

                            if (pageType == EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS &&
                                BookingEngineSessionWrapper.DirectlyModifyContactDetails)
                            {
                                List<RoomRateEntity> roomRates = selectedRoomAndRateEntity.RoomRates;
                                List<Scandic.Scanweb.Core.RoomType> roomTypes =
                                    new List<Scandic.Scanweb.Core.RoomType>();
                                foreach (RoomRateEntity roomRate in roomRates)
                                {
                                    Scandic.Scanweb.Core.RoomType roomType =
                                        RoomRateUtil.GetRoomType(roomRate.RoomTypeCode);
                                    roomTypes.Add(roomType);
                                }
                                shoppingCartRoomBox.BedPreference = ((roomTypes.Count > 0) && (roomTypes[0] != null))
                                                                        ? roomTypes[0].BedTypeDescription
                                                                        : string.Empty;
                                if (searchCriteria.SearchingType == SearchType.BONUSCHEQUE)
                                {
                                    shoppingCartRoomBox.Rate =
                                        WebUtil.GetBonusChequeRateString(
                                            "/bookingengine/booking/selectrate/bonuschequerate",
                                            searchCriteria.ArrivalDate.Year, searchCriteria.HotelCountryCode,
                                            roomRates[0].BaseRate.Rate,
                                            roomRates[0].BaseRate.CurrencyCode, Convert.ToInt32(AppConstants.ONE),
                                            Convert.ToInt32(AppConstants.ONE));
                                }
                                else if (searchCriteria.SearchingType == SearchType.VOUCHER && BookingEngineSessionWrapper.BookingDetails != null && BookingEngineSessionWrapper.BookingDetails.HotelSearch != null)
                                {
                                    string voucher = string.Empty;
                                    int noOfNights = BookingEngineSessionWrapper.BookingDetails.HotelSearch.NoOfNights;
                                    if (noOfNights > 1)
                                        voucher = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrates");
                                    else
                                        voucher = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrate");

                                    shoppingCartRoomBox.Rate = string.Format("{0} {1}", noOfNights, voucher);

                                }
                                else
                                {
                                    shoppingCartRoomBox.Rate = roomRates.Count > 0
                                                                   ? roomRates[0].BaseRate.Rate.ToString()
                                                                   : string.Empty;
                                    shoppingCartRoomBox.Rate += AppConstants.SPACE + roomRates[0].BaseRate.CurrencyCode;
                                }
                            }
                        }
                    }

                    shoppingCartRoomBoxes.Controls.Add(shoppingCartRoomBox);
                }
            }
        }


        /// <summary>
        /// Sets the rate string in shopping cart room box.
        /// </summary>
        /// <param name="roomNumber">The room number.</param>
        /// <param name="shoppingCartRoomBox">The shopping cart room box.</param>
        /// <param name="selectedRoomAndRateEntity">The selected room and rate entity.</param>
        /// <param name="showPricePerStay">if set to <c>true</c> [show price per stay].</param>
        private void SetRateStringInShoppingCartRoomBox(int roomNumber, ShoppingCartRoomBox shoppingCartRoomBox,
                                                        SelectedRoomAndRateEntity selectedRoomAndRateEntity,
                                                        bool showPricePerStay)
        {
            try
            {
                BaseRateDisplay rateDisplay = Utility.GetRateCellDisplayObject(roomNumber, selectedRoomAndRateEntity);
                if (rateDisplay != null && shoppingCartRoomBox != null)
                {
                    RoomCategoryEntity currentRoomCategoryEntity = selectedRoomAndRateEntity.SelectedRoomCategoryEntity;
                    if (currentRoomCategoryEntity != null)
                    {
                        if (BookingEngineSessionWrapper.HideARBPrice)
                        {
                            shoppingCartRoomBox.Rate = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/prepaid");
                        }
                        else
                        {
                            RateCellDisplay currentRateStringDisplay = rateDisplay.GetRateDisplayStringForShoppingCart(currentRoomCategoryEntity, selectedRoomAndRateEntity.RateCategoryID, true, roomNumber);
                            shoppingCartRoomBox.Rate = currentRateStringDisplay.RateString;
                        }
                    }
                    if (rateDisplay.RateCategoriesDisplay != null && rateDisplay.RateCategoriesDisplay.Count > 0)
                    {
                        for (int i = 0; i < rateDisplay.RateCategoriesDisplay.Count; i++)
                        {
                            if (rateDisplay.RateCategoriesDisplay[i].Id == selectedRoomAndRateEntity.RateCategoryID)
                            {
                                shoppingCartRoomBox.RateType = rateDisplay.RateCategoriesDisplay[i].Title;
                                return;
                            }
                        }
                    }
                }
            }
            catch (ContentDataAccessException cdaException)
            {
                if (string.Equals(cdaException.ErrorCode, AppConstants.BONUS_CHEQUE_NOT_FOUND,
                                  StringComparison.InvariantCultureIgnoreCase))
                {
                    throw cdaException;
                }
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }
        }

        /// <summary>
        /// Sets the add new room drop downs.
        /// </summary>
        private void SetAddNewRoomDropDowns()
        {
            CreateDropDown("ddlAdultsPerNewRoom", 1, 6, "");
            CreateDropDown("ddlChildPerNewRoom", 0, 5, "");
            int minAge = Convert.ToInt32(ConfigurationManager.AppSettings["Booking.Children.MinAge"]);
            int maxAge = Convert.ToInt32(ConfigurationManager.AppSettings["Booking.Children.MaxAge"]);
            for (int count = 1; count <= 5; count++)
            {
                CreateDropDown("childAgeforNewRoomChild" + count, minAge, maxAge,
                               WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age"));
            }
            string[] bedTypes = new string[3]
                                    {
                                        WebUtil.GetTranslatedText(
                                            "/bookingengine/booking/childrensdetails/accommodationtypes/sharingbed"),
                                        WebUtil.GetTranslatedText(
                                            "/bookingengine/booking/childrensdetails/accommodationtypes/crib"),
                                        WebUtil.GetTranslatedText(
                                            "/bookingengine/booking/childrensdetails/accommodationtypes/extrabed")
                                    };
            for (int count = 1; count <= 5; count++)
            {
                CreateBedType(bedTypes, "bedTypeforNewRoomChild" + count);
            }
        }

        /// <summary>
        /// Creates the drop down.
        /// </summary>
        /// <param name="controlName">Name of the control.</param>
        /// <param name="startValue">The start value.</param>
        /// <param name="endValue">The end value.</param>
        /// <param name="defaultValue">The default value.</param>
        private void CreateDropDown(string controlName, int startValue, int endValue, string defaultValue)
        {
            DropDownList dropDownList = this.FindControl(controlName) as DropDownList;
            if (null != dropDownList)
            {
                ListItem item;
                if (!string.IsNullOrEmpty(defaultValue))
                {
                    item = new ListItem(defaultValue, defaultValue);
                    dropDownList.Items.Add(item);
                }
                for (int constantCount = startValue; constantCount <= endValue; constantCount++)
                {
                    item = new ListItem(constantCount.ToString(), constantCount.ToString());
                    dropDownList.Items.Add(item);
                }
            }
        }

        /// <summary>
        /// Creates the type of the bed.
        /// </summary>
        /// <param name="bedTypes">The bed types.</param>
        /// <param name="controlName">Name of the control.</param>
        private void CreateBedType(string[] bedTypes, string controlName)
        {
            int bedCountLength = bedTypes.Length;
            DropDownList dropdown = this.FindControl(controlName) as DropDownList;
            if (null != dropdown)
            {
                for (int bedCount = 0; bedCount < bedCountLength; bedCount++)
                {
                    dropdown.Items.Add(new ListItem(bedTypes[bedCount], bedTypes[bedCount]));
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSearchRoom control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSearchRoom_Click(object sender, EventArgs e)
        {
            try
            {
                Reservation2SessionWrapper.AvailabilityCalendar = null;
                Reservation2SessionWrapper.AvailabilityCalendarAccessed = false;
                Reservation2SessionWrapper.IsEditYourStaySearchTriggered = true;
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if (search != null)
                {
                    HotelSearchRoomEntity roomSearch = new HotelSearchRoomEntity();

                    roomSearch.AdultsPerRoom = int.Parse(ddlAdultsPerNewRoom.Text);
                    roomSearch.ChildrenPerRoom = int.Parse(ddlChildPerNewRoom.Text);

                    roomSearch.ChildrenDetails = new ChildrensDetailsEntity();
                    roomSearch.ChildrenDetails.AdultsPerRoom = (uint)roomSearch.AdultsPerRoom;

                    string cot = string.Empty, extraBed = string.Empty, sharingBed = string.Empty;
                    Utility.GetLocaleSpecificChildrenAccomodationTypes(ref cot, ref extraBed, ref sharingBed);

                    for (int i = 0; i < roomSearch.ChildrenPerRoom; i++)
                    {
                        ChildEntity child = new ChildEntity();

                        DropDownList dropDownList = null;
                        dropDownList = this.FindControl("childAgeforNewRoomChild" + (i + 1)) as DropDownList;
                        if (dropDownList != null)
                        {
                            int childAge = 0;
                            int.TryParse(dropDownList.SelectedValue, out childAge);
                            child.Age = childAge;
                        }
                        dropDownList = this.FindControl("bedTypeforNewRoomChild" + (i + 1)) as DropDownList;
                        if (dropDownList != null)
                        {
                            string childBedType = dropDownList.SelectedValue;
                            if (0 == string.Compare(childBedType, cot, true))
                            {
                                child.ChildAccommodationType = ChildAccommodationType.CRIB;
                                child.AccommodationString = cot;
                            }
                            else if (0 == string.Compare(childBedType, sharingBed, true))
                            {
                                child.ChildAccommodationType = ChildAccommodationType.CIPB;
                                child.AccommodationString = sharingBed;
                            }
                            else if (0 == string.Compare(childBedType, extraBed, true))
                            {
                                child.ChildAccommodationType = ChildAccommodationType.XBED;
                                child.AccommodationString = extraBed;
                            }
                        }

                        roomSearch.ChildrenDetails.ListChildren.Add(child);
                    }

                    roomSearch.IsBlocked = false;
                    roomSearch.IsSearchDone = false;
                    roomSearch.ChildrenOccupancyPerRoom =
                        Utility.GetNoOfChildrenToBeAccommodated(roomSearch.ChildrenDetails);

                    if (search.ListRooms == null)
                    {
                        search.ListRooms = new List<HotelSearchRoomEntity>();
                    }
                    search.ListRooms.Add(roomSearch);
                    search.RoomsPerNight = search.ListRooms.Count;

                    if (pageType == EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE)
                    {
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE),
                                          false);
                    }
                    else
                    {
                        Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE), false);
                    }
                }
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
        }

        /// <summary>
        /// Called when [book_click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void OnBook_Click(object sender, EventArgs e)
        {
            if (WebUtil.IsSessionValid(null, false))
            {
                Reservation2SessionWrapper.IsEditYourStaySearchTriggered = true;
                Reservation2SessionWrapper.NetsPaymentErrorMessage = string.Empty;
                Reservation2SessionWrapper.NetsPaymentInfoMessage = string.Empty;
                GenericSessionVariableSessionWrapper.IsPrepaidBooking = false;
                SearchCriteriaSessionWrapper.IsPromocodeInvalidForHotel = false;
                string urlToRedirect = string.Empty;
                if (pageType == EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE)
                {
                    urlToRedirect =
                        WebUtil.GetSecureUrl(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS));
                }
                else
                {
                    urlToRedirect =
                        WebUtil.GetSecureUrl(GlobalUtil.GetUrlToPage(EpiServerPageConstants.BOOKING_DETAILS_PAGE));
                }
                UserNavTracker.TrackAction(this, "SC_Book");
                //WebUtil.ApplicationErrorLog(new Exception("Error in Shopping Cart Book Click"));
                //Response.End();
                Response.Redirect(urlToRedirect, false);
            }
        }

        #region Methods Used in Modify flow in case of Combo Reservation

        /// <summary>
        /// Creates the selected room rates hash table in modify combo booking.
        /// </summary>
        /// <summary>
        /// Sets the rate string in shopping cart room box in modify combo booking.
        /// </summary>
        /// <param name="shoppingCartRoomBox">The shopping cart room box.</param>
        /// <param name="selectedRoomAndRateEntity">The selected room and rate entity.</param>
        private void SetRateStringInShoppingCartRoomBoxInModifyComboBooking(ShoppingCartRoomBox shoppingCartRoomBox,
                                                                            SelectedRoomAndRateEntity
                                                                                selectedRoomAndRateEntity)
        {
            RateCategory currentRateCategory = null;
            if (selectedRoomAndRateEntity.RateCategoryID != null)
                currentRateCategory = RoomRateUtil.GetRateCategoryByCategoryId(selectedRoomAndRateEntity.RateCategoryID);
            shoppingCartRoomBox.RateType = (currentRateCategory != null)
                                               ? currentRateCategory.RateCategoryName
                                               : string.Empty;
            if (selectedRoomAndRateEntity.RoomRates != null && selectedRoomAndRateEntity.RoomRates.Count > 0)
                shoppingCartRoomBox.Rate = selectedRoomAndRateEntity.RoomRates[0].TotalRate.Rate + AppConstants.SPACE +
                                           selectedRoomAndRateEntity.RoomRates[0].TotalRate.CurrencyCode;
            shoppingCartRoomBox.RoomCategory = selectedRoomAndRateEntity.SelectedRoomCategoryName;
        }



        #endregion

        #region INavigationTraking Members

        public List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> returnParams = new List<KeyValueParam>();
            Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
            if (selectedRoomAndRatesHashTable != null)
            {
                returnParams.AddRange((from e in selectedRoomAndRatesHashTable.Cast<DictionaryEntry>()
                                       select new KeyValueParam(string.Format("CountryCode for {0}",
                                       e.Key.ToString()), ((SelectedRoomAndRateEntity)e.Value).CountryCode)).ToList());

                returnParams.AddRange((from e in selectedRoomAndRatesHashTable.Cast<DictionaryEntry>()
                                       select new KeyValueParam(string.Format("SelectedRoomCategoryName for {0}",
                                       e.Key.ToString()), ((SelectedRoomAndRateEntity)e.Value).SelectedRoomCategoryName)).ToList());
                returnParams.AddRange((from e in selectedRoomAndRatesHashTable.Cast<DictionaryEntry>()
                                       select new KeyValueParam(string.Format("RateCategoryID for {0}",
                                       e.Key.ToString()), ((SelectedRoomAndRateEntity)e.Value).RateCategoryID)).ToList());
                returnParams.AddRange((from e in selectedRoomAndRatesHashTable.Cast<DictionaryEntry>()
                                       select new KeyValueParam(string.Format("SelectedRoomCategory Name for {0}",
                                       e.Key.ToString()), ((SelectedRoomAndRateEntity)e.Value).SelectedRoomCategoryEntity.Name)).ToList());

            }
            return returnParams;
        }

        #endregion
    }
}