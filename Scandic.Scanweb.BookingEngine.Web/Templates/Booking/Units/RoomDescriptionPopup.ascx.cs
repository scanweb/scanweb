//  Description					: Code Behind class for RoomDescriptionPopup Control      //
//	This will hold controls for displaying room category details.  						  //
//----------------------------------------------------------------------------------------//
/// Author						: Ranajit Nayak                                   	      //
/// Author email id				:                           							  //
/// Creation Date				: 23rd June  2010									      //
///	Version	#					: 2.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region Namespace

using System;
using System.Text;
using Scandic.Scanweb.Entity;

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// This control holds method and proprty for displaying Room category details.
    /// </summary>
    public partial class RoomDescriptionPopup : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Set all values( like room description,Image Url and name)  of room description popup control one by one.
        /// </summary>
        /// <param name="roomCategoryDetails"></param>
        public void SetRoomCategoryDetails(RoomCategoryEntity roomCategoryDetails)
        {
            RoomDescription.Attributes.Add("class", roomCategoryDetails.Id + " hideRoomType");
            if (!string.IsNullOrEmpty(roomCategoryDetails.Url))
            {
                MainImage.ImageUrl = roomCategoryDetails.Url;
                MainImagePlaceHolder.Visible = true;
            }
            else
            {
                MainImagePlaceHolder.Visible = false;
                roomContent.Attributes.Add("class", "roomContent roomContentExpanded");
            }
            if (!string.IsNullOrEmpty(roomCategoryDetails.Description))
                TextProperty.Text = roomCategoryDetails.Description;
            else
                TextPropertyPlaceHolder.Visible = false;

            if (!string.IsNullOrEmpty(roomCategoryDetails.Id))
            {
                RoomCategoryName.Text = roomCategoryDetails.Name;
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<h3>&#160;</h3>");
                RoomCategoryName.Text = sb.ToString();
            }
        }
    }
}