﻿//  Description					: Displays credit cards list in update profile			  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Saravanan                                          	  //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    public partial class ManageCreditCards : System.Web.UI.UserControl
    {
        /// <summary>
        /// Pouplates credit cards from credit cards list
        /// </summary>
        /// <param name="creditCards"></param>
        public void PopulateCreditCards(Dictionary<string, CreditCardEntity> creditCards)
        {
            creditCardsSection.InnerHtml = WebUtil.ConstructCreditCardListHTML(creditCards);
        }
    }
}