//  Description					: Code Behind class for Booking receipt information Ctrl  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                    	  //
//  Author email id				:                           							  //
//  Creation Date				: 	                    								  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: 													      //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region Scandic Namespace
using System;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for BookingReceiptInformation
    /// </summary>
    public partial class BookingReceiptInformation : EPiServer.UserControlBase
    {
        private PaymentController _paymentController = null;
        private AvailabilityController availabilityController = null;

        /// <summary>
        /// Gets PaymentController
        /// </summary>
        private PaymentController PaymentController
        {
            get
            {
                if (_paymentController == null)
                {
                    IContentDataAccessManager contentDataAccessManager = new ContentDataAccessManager();
                    IAdvancedReservationDomain advancedReservationDomain = new AdvancedReservationDomain();
                    ILoyaltyDomain loyaltyDomain = new LoyaltyDomain();
                    IReservationDomain reservationDomain = new ReservationDomain();
                    _paymentController = new PaymentController(contentDataAccessManager, advancedReservationDomain, loyaltyDomain, reservationDomain);
                }
                return _paymentController;
            }
        }

        /// <summary>
        /// Gets m_AvailabilityController
        /// </summary>
        private AvailabilityController m_AvailabilityController
        {
            get
            {
                if (availabilityController == null)
                {
                    availabilityController = new AvailabilityController();
                }
                return availabilityController;
            }
        }


        #region Page Events
        /// <summary>
        /// Event Handler for the Page Load event of Booking Receipt Information User Control
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="e">
        /// Arguments for the event
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            AppLogger.LogInfoMessage("BookingReceiptInformation.Ascx:Page_Load() StartTime");

            pfLnkBtnPrint.Attributes.Add("onclick", "javascript:OpenPringDialog();return false;");
            pfSpnBtnPrint.Attributes.Add("onclick", "javascript:OpenPringDialog();return false;");

            if (Request.QueryString["command"] != null)
            {
                if (Request.QueryString["command"] == "print")
                {
                    printerFriendlySection.Visible = true;
                    pfPrintDiv.Visible = true;
                    watermarkImage.Visible = true;
                    divPleaseNoteLogo.Visible = true;
                    divPleaseNoteLogoBk.Visible = false;
                }
            }

            if (Request.QueryString["isPDF"] != null)
            {
                if (Convert.ToBoolean(Request.QueryString["isPDF"]))
                {
                    pfLnkBtnPrint.Visible = false;
                    pfSpnBtnPrint.Visible = false;
                    watermarkImage.Visible = false;
                    divPleaseNoteLogo.Visible = false;
                    divPleaseNoteLogoBk.Visible = true;
                }
            }

            AppLogger.LogInfoMessage("BookingReceiptInformation.Ascx:Page_Load() EndTime");
        }
       
       #endregion Page Events

        #region Public Methods
        /// <summary>
        /// Populate the UI with the confirmation details
        /// </summary>
        public void PopulateUi(List<BookingDetailsEntity> bookingDetailsList)
        {
            AppLogger.LogInfoMessage("BookingReceiptInformation.ASCX:PopulateUi StartTime:::" +
                DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));

            HotelSearchEntity hotelSearch = null;

            if (BookingEngineSessionWrapper.IsModifyBooking)
            {
                hotelSearch = BookingEngineSessionWrapper.BookingDetails.HotelSearch;
            }
            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
            {

                hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            }
            if (bookingDetailsList != null && hotelSearch != null)
                PopulateReservationInfoContainer(hotelSearch, bookingDetailsList);

            creditCardInfoSection.Visible = WebUtil.IsAnyRoomNotHavingSaveCategory(bookingDetailsList);

            AppLogger.LogInfoMessage("BookingReceiptInformation.ASCX:PopulateUi EndTime:::" + 
                DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
        }

        #endregion
       
        #region Private Methods
      
        /// <summary>
        /// This method populates the Reservation Information.
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="selectedRoomRateshashtable"></param>
        private void PopulateReservationInfoContainer(HotelSearchEntity hotelSearch, List<BookingDetailsEntity> bookingDetailsList)
        {
            var noOfAdults = 0;
            var noOfChildren = 0;
            
            if (hotelSearch != null && hotelSearch.ListRooms.Count > 0)
            {
                foreach (var room in hotelSearch.ListRooms)
                {
                    noOfAdults += room.AdultsPerRoom;
                    noOfChildren += room.ChildrenPerRoom;
                }
            }

            HotelDestination hotel = null;

            if (AppConstants.RECEIPT_IN_ENGLISH_ONLY)
                hotel = m_AvailabilityController.GetHotelDestination(hotelSearch.SelectedHotelCode, LanguageConstant.LANGUAGE_ENGLISH.ToLower());
            else
                hotel = m_AvailabilityController.GetHotelDestination(hotelSearch.SelectedHotelCode);

            if (bookingDetailsList != null && bookingDetailsList.Count > 0)
            {
                for (var iterator = 0; iterator < bookingDetailsList.Count; iterator++)
                {
                    pfReservationInfo.ArrivalDate = hotelSearch.ArrivalDate;
                    pfReservationInfo.DepartureDate = hotelSearch.DepartureDate;
                    pfReservationInfo.NoOfNights = hotelSearch.NoOfNights;
                   
                    if (hotelSearch != null)
                    {
                        if (Request.QueryString["command"] != null)
                        {
                            if ((Request.QueryString["command"].ToString() == QueryStringConstants.QUERY_STRING_PRINT)
                                || (Request.QueryString["command"].ToString() == QueryStringConstants.QUERY_STRING_CANCEL_PRINT))
                            {
                                pfReservationInfo.HotelNameLabel = hotel.Name;
                            }
                        }

                        pfReservationInfo.NoOfRooms = Convert.ToString(hotelSearch.RoomsPerNight);
                    }
                }
                
                pfReservationInfo.PopulateReceiptinfo(bookingDetailsList);

                pfReservationInfo.NoOfAdults = noOfAdults;
                pfReservationInfo.NoOfChildren = noOfChildren;

                if (bookingDetailsList.Count > 0)
                {
                    pfReservationInfo.SetRoomDetails(bookingDetailsList);
                }
            }

            MerchantProfileEntity hotelMerchantInfo = null;

            if(AppConstants.RECEIPT_IN_ENGLISH_ONLY)
                hotelMerchantInfo = PaymentController.GetMerchantByHotelOperaId(hotelSearch.SelectedHotelCode, LanguageConstant.LANGUAGE_ENGLISH.ToLower());
            else
                hotelMerchantInfo = PaymentController.GetMerchantByHotelOperaId(hotelSearch.SelectedHotelCode);

            if (hotelMerchantInfo != null)
            {
                lblPDFCompanyName.InnerText = hotelMerchantInfo.CompanyName;
                lblPDFOrganisationNumber.InnerText = hotelMerchantInfo.OrganizationNumber;           
                lblPDFMerchantID.InnerText = hotelMerchantInfo.MerchantID;                               
            }

            if (hotel != null)
            {
                lblPDFHomePage.InnerHtml = string.Concat(WebUtil.GetSiteUrl(), AppConstants.SLASH , hotel.ShortURL);
                lblPDFHotel.InnerText = hotel.Name;
                lblPDFVisitingAddress.InnerHtml = hotel.HotelAddress.ToString();
                lblPDFPhone.InnerText = hotel.Telephone;
                lblPDFEmail.InnerText = hotel.Email;
                lblPDFFax.InnerText = hotel.Fax;
            }
        }

        #endregion
    }
}