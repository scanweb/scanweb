<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CampaignLanding.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.CampaignLanding" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<div id="Loyalty" class="BE">
    <div id="clientErrorDivEL" class="errorText" runat="server" />
    <div id="clientErrorDivCCLP" class="errorText">
    </div>
    <div class="errorDivClient">
        <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
        <input type="hidden" id="CampaignCodeLandingPagePasswordInvalid" name="CampaignCodeLandingPagePasswordInvalid"
            value='<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/password") %>' />
        <input type="hidden" id="CampaignCodeLandingPageUserNameInvalid" name="CampaignCodeLandingPageUserNameInvalid"
            value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/user_name") %>' />
        <input type="hidden" id="CampaignCodeLandingPageCampaignCodeInvalid" name="CampaignCodeLandingPageCampaignCodeInvalid"
            value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalid_campaign_code") %>' />
        <input type="hidden" id="EnrollButtonLabel" name="EnrollButtonLabel" value='<%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/Join") %>' />
        <input type="hidden" id="UpgradeButtonLabel" name="UpgradeButtonLabel" value='<%= WebUtil.GetTranslatedText("/bookingengine/booking/campaignLanding/getOffer") %>' />
        <input type="hidden" id="hdnCampaignCode" runat="server" />
        <input type="hidden" id="hdnPrepopulateCampaignCode" runat="server" />
        <!-- Error -->
    </div>
    <div class="new2">
        <div class="box-top-grey">
            <span class="title"><%= WebUtil.GetTranslatedText("/bookingengine/booking/campaignLanding/loginBoxHeading") %></span>
        </div>
        <div class="box-middle">
            <div class="content">
                <p class="formRow">
                    <label class="lblRadio1">
                        <input class="frmInputRadio" type="radio" name="memberType" id="rdoNewMember" onclick="setStatus('DISABLE');"
                            tabindex="1" runat="server" /><%= WebUtil.GetTranslatedText("/bookingengine/booking/campaignLanding/newCustomer") %></label></p>
                <p class="formRow">
                    <label class="lblRadio1">
                        <input class="frmInputRadio" type="radio" name="memberType" id="rdoAlreadyMember"
                            onclick="setStatus('ENABLE');" tabindex="2" runat="server" /><%= WebUtil.GetTranslatedText("/bookingengine/booking/campaignLanding/existingMember") %></label></p>
                <div id="loginControls" runat="server">
                    <div class="columnOne">
                        <p class="formRow">
                            <span id="spanMemberNumber">
                                <label id="memNumber">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/campaignLanding/membershipNumber") %>
                                </label>
                                <br />
                            </span>
                            <input type="text" class="frmPrefixText" id="txtmembershipnoprefix" name="txtmembershipnoprefix"
                                maxlength="10" runat="server" readonly="readonly" />
                            <input id="txtmembershipno" class="frmInputText" type="text" maxlength="30" name="txtmembershipno"
                                tabindex="3" runat="server" />
                        </p>
                        <p class="formRow">
                            <span id="spanCampaignCode">
                                <label id="campaignCode">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/campaignLanding/campaignCode") %>
                                </label>
                                <br />
                            </span>
                            <input id="txtCampaignCode" class="frmInputText" type="text" maxlength="20" name="txtCampaignCode"
                                tabindex="5" runat="server" />
                        </p>
                    </div>
                    <div class="columnTwo">
                        <p class="formRow">
                            <span id="spanPin">
                                <label for="pin">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/campaignLanding/pin") %>
                                </label>
                                <br />
                            </span>
                            <asp:TextBox name="txtPin" TextMode="Password" id="txtPin" class="frmInputText" maxlength="30"
                                tabindex="4" runat="server" />
                            <!--<input name="txtPin1" type="password" id="txtPin1" class="frmInputText" maxlength="30"
                                tabindex="4" runat="server" />-->
                        </p>
                        <p class="formRow">
                            <input type="checkbox" id="RememberUserIdPwd" name="RememberUserIdPwd" class="chkBx"
                                tabindex="5" runat="server" />
                            <span>
                                <label><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/RememberMe") %></label>
                            </span>
                        </p>
                    </div>
                </div>
                <div class="clear">&nbsp;</div>
                    
                <div id="CampLandLoginProgressDiv" runat="server" style="display: none">
                    <span class="fltLft camplandlogProc">
                        <img src='<%=ResolveUrl("~/Templates/Booking/Styles/Default/Images/rotatingclockani.gif")%>'
                        alt="image" align="middle" width="25px" height="25px" />
                        <span>
                            <%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/Loading")%>
                        </span>
                        <div class="clear">&nbsp;</div>
                    </span>
                </div>
                    
                    
                <!-- Footer -->
                <div class="FooterContainer">
                    <div class="alignRight">
                        <div class="actionBtn fltRt">
                            <asp:LinkButton runat="server" OnClick="BtnEnroll_Click" class="buttonInner enroll"
                                ID="BtnEnroll"> <span id="SpanBtnEnroll"><%=WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/Join")%></span></asp:LinkButton>
                            <asp:LinkButton runat="server" OnClick="BtnEnroll_Click" class="buttonRt scansprite enroll"
                                ID="spnEnroll"></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <!-- Footer -->
            </div>
        </div>
        <div class="box-bottom">
            &nbsp;</div>
    </div>
    <!-- /LastCard -->
</div>

<script language="javascript" type="text/javascript">
    var defaultOption = "<%=defaultOption %>";
    $(document).ready(function() {
        if (defaultOption == "existingmember")
            setStatus('ENABLE');
        else
            setStatus('DISABLE');
        appendPrefix("txtmembershipnoprefix");
        $("input[id$='txtmembershipno']").bind('focusout', trimwhitespace);
    });

    function trimwhitespace() {
        trimWhiteSpaces("txtmembershipno");
        replaceDuplicatePrefix("txtmembershipno");
    }

    //Following will make the text boxes and radio button enabled and disabled based on the radio button selected.
    function setStatus(status) {
        // UAT R1.5 Bug Fix: artf854169. Applied a style for the fields which are disabled.
        // If Already a member radio option is selected
        if (status == 'ENABLE') {
            $("div[id*='loginControls']").show();
            // BUG Fix: artf850422
            $fn(_endsWith("SpanBtnEnroll")).innerHTML = $fn(_endsWith("UpgradeButtonLabel")).value;
            if ($fn(_endsWith("hdnPrepopulateCampaignCode")).value == "1") {
                $fn(_endsWith("txtCampaignCode")).disabled = true;
                $fn(_endsWith("txtCampaignCode")).className = "frmInputText frmInputDisabled";
                $fn(_endsWith("txtCampaignCode")).value = $fn(_endsWith("hdnCampaignCode")).value;
            }
            else {
                $fn(_endsWith("txtCampaignCode")).disabled = false;
                $fn(_endsWith("txtCampaignCode")).className = "frmInputText";
            }
        }
        else {
            // If New member radio option is selected
            // BUG Fix: artf850422
            $("div[id*='loginControls']").hide();
            $fn(_endsWith("SpanBtnEnroll")).innerHTML = $fn(_endsWith("EnrollButtonLabel")).value;
        }
    }
</script>

