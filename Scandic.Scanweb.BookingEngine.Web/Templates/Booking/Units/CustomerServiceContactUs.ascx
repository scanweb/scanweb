<%@ Control Language="C#" AutoEventWireup="true" Codebehind="CustomerServiceContactUs.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.CustomerServiceContactUs" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<div id="ContactFormArea">
    <div id="XForm" class="BE">
        <div class="xForm" id="FormPanel">
            <div id="clientErrorDivCSCU" class="errorText" runat="server">
            </div>
            <div>
                <input type="hidden" id="errMsgTitle" name="errMsgTitle" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading") %>' />
                <input type="hidden" id="CSCU_InvalidNatureofQuery" name="CSCU_InvalidNatureofQuery"
                    value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidNatureOfQuery") %>' />
                <input type="hidden" id="CSCU_InvalidEmail" name="CSCU_InvalidEmail" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/email_address") %>' />
            </div>
            <table id="id_matrix">
                <tbody>
                    <tr>
                        <td valign="top">
                            <span id="lblNatureOfQuery">
                                <label>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/CustomerServiceContactUs/NatureOfQuery") %>
                                </label>
                            </span>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="ddlNatureOfQuery" runat="server" CssClass="w50" AutoPostBack="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <span id="lblHotelList">
                                <label>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/CustomerServiceContactUs/HotelName") %>
                                </label>
                            </span>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="ddlHotelList" runat="server" CssClass="w50" AutoPostBack="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <span id="lblComment">
                                <label>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/CustomerServiceContactUs/Comment") %>
                                </label>
                            </span>
                        </td>
                        <td valign="top">
                            <textarea cols="20" rows="6" name="txtComments" id="txtComments" class="w50" runat="server">
                </textarea>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <span id="lblCustomerName">
                                <label>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/CustomerServiceContactUs/CustomerName") %>
                                </label>
                            </span>
                        </td>
                        <td valign="top">
                            <input type="text" name="txtCustomerName" id="txtCustomerName" class="w50" runat="server"
                                maxlength="100" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <span id="lblMembershipNo">
                                <label>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/CustomerServiceContactUs/MembershipNo") %>
                                </label>
                            </span>
                        </td>
                        <td valign="top">
                            <input type="text" name="txtMembershipNo" id="txtMembershipNo" class="w50" runat="server"
                                maxlength="50" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <span id="lblTelephoneNo">
                                <label>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/CustomerServiceContactUs/TelephoneNo") %>
                                </label>
                            </span>
                        </td>
                        <td valign="top">
                            <input type="text" name="txtTelephoneNo" id="txtTelephoneNo" class="w50" runat="server"
                                maxlength="20" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <span id="lblEmailId">
                                <label>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/CustomerServiceContactUs/EmailId") %>
                                </label>
                            </span>
                        </td>
                        <td valign="top">
                            <input type="text" name="txtEmailId" id="txtEmailId" class="w50" runat="server" maxlength="200" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                        </td>
                        <td valign="top">
                            <div class="actionBtn fltRt">
                                <asp:LinkButton ID="btnSubmit" runat="server" class="buttonInner sprite" OnClick="btnSubmit_Click">
			            <span><%= WebUtil.GetTranslatedText("/bookingengine/booking/CustomerServiceContactUs/Submit") %></span>
                                </asp:LinkButton>
                                <asp:LinkButton ID="spnSubmit" runat="server" class="buttonRt sprite" OnClick="btnSubmit_Click">			            
                                </asp:LinkButton>                                
                                
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br />
</div>
