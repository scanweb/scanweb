<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModifyBookingKids.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ModifyBookingKids" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%@ Import Namespace="Scandic.Scanweb.Entity" %>
<%@ Register Src="~/Templates/Booking/Units/UsefulLinksContainer.ascx" TagName="UsefulLinks" TagPrefix="ModifyBooking" %>
<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx" TagName="ModifyReservationInformation" TagPrefix="ModifyBooking" %>

<div class="BE">
    <div id="ChildrensDetails">
        <!-- Hidden inputs -->
        <input id="childrenInfoHidden" type="hidden" value="" class="hiddenTxt" runat="server"/>
        <input id="childrenCriteriaHidden" type="hidden" value="" class="hiddenTxt" runat="server"/>
        <input id="noOfChildrenHidden" type="hidden" value="" class="hiddenTxt" runat="server"/>
        <input id="noOfAdults" type="hidden" value="" class="hiddenTxt" runat="server"/>
        <input id="errMessage" name="errMessage" type="hidden" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/invalidbedtypechoosen") %>' />
        <input id="CIPBString" name="CIPBString" type="hidden" value="<%=
                WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/sharingbed") %>"/>
        <input id="InvalidKidsAge" name="InvalidKidsAge" type="hidden" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidAge") %>'/>
        <input id="InvalidBedType" name="InvalidBedType" type="hidden" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidBedType") %>'/>
        <!-- /Hidden inputs -->
 
        <!-- Progress Tab -->
        <div id="progTab">
          <div id="pt_1" >
            <ul>
              <li class="pt_num one">&nbsp;</li>
              <li class="text active"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/childrendetail") %></li>
              <li class="pt_arrow1 activeArrow">&nbsp;</li>
              
              <li class="pt_num two">&nbsp;</li>
              <li class="text"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selectrate") %></li>
              <li class="pt_arrow1">&nbsp;</li>
              
              <li class="pt_num three">&nbsp;</li>
              <li class="text"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/bookingdetails") %></li>
              <li class="pt_arrow1">&nbsp;</li>
              
              <li class="pt_num four">&nbsp; </li>
              <li class="text"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/confirmation") %></li>
              <li class="pt_last"> &nbsp;</li>
            </ul>
          </div>
          <div class="clear">&nbsp;</div>
        </div>
        <!-- /Progress Tab -->
    
        <!-- Booking Information -->
        <ModifyBooking:ModifyReservationInformation ID="prevBookingReservationInfo" runat="server"></ModifyBooking:ModifyReservationInformation>
        <ModifyBooking:ModifyReservationInformation ID="newBookingReservationInfo" ShowHelpLink ="false"  runat="server" ></ModifyBooking:ModifyReservationInformation>    
        <ModifyBooking:UsefulLinks ID="UsefulLinks" runat="server"></ModifyBooking:UsefulLinks>
        <!-- Booking Information -->    
        
        <!-- Children's details container div -->
        
        <!-- Childrens room requirements plain text -->
        <div><h2 class="lightHeading"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/childrendetail") %></h2></div>        
        <!-- /Childrens room requirements plain text -->
		
		<!-- Container on which all the children's accommodation types has been chosen dynamically -->
        <div id="kidsErrorDiv" class="errorText"></div>
        <div id="selectContainer">
            <table runat="server" class="kidsDDHolder" id="tableChildrenAcc" border="0" cellpadding="5" cellspacing="0"></table>        
        </div>
        <!-- /Container on which all the children's accommodation types has been chosen dynamically -->
                
        <!-- Children's accommodation rules displaying control -->
        <div >
            <%= CurrentPage["ChildrenOccupancyRule"] %>
        </div>
        <!-- /Children's accommodation rules displaying control -->
        
        <!-- Back and continue button -->
        <div class="btnHolder">
            <div class="buttonContainer">
		        <span class="btnSubmit">
		            <a href='<%= GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_CHANGE_DATES) %>' class="">
                        <span><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/back") %></span>
		        </span>
	        </div>
    	
            <div class="formRowButton">
                <span class="btnSubmit">            
                <asp:LinkButton ID="BtnContinueKidsPage" OnClick="OnContinue" runat="server"><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/search") %></span></asp:LinkButton></span>
            </div>
        </div>
        <!-- /Back and continue button -->
             
        <!-- Childish attractions -->
        <div class="childAttrHolder">
        <EPiServer:Property PropertyName="ChildAttraction" ID="ChildAttraction" Visible="true" runat="server" />
        </div>
        <!-- /Childish attractions -->
        
        <!-- /Children's details container div -->
    </div>
</div>