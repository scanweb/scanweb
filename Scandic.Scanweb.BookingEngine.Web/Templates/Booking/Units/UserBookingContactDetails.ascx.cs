//  Description					: Code Behind class for UserBookingContactDetails Control //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Shankar Dasgupta                                   	  //
//  Author email id				:                           							  //
//  Creation Date				: 14th November  2007									  //
//	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespace
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using System.Web;
using ImageStoreNET.Classes.Util;
using EPiServer.Globalization;
using Scandic.Scanweb.BookingEngine.Web.HotelTimezone;
using Scandic.CryptorEngine;

#endregion

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for UserBookingContactDetails
    /// </summary>
    public partial class UserBookingContactDetails : EPiServer.UserControlBase, INavigationTraking
    {
        private delegate void CreatePDF(List<EmailEntity> email, List<string> reservations);

        #region Private Members
        private AvailabilityController availabilityController = null;

        private int _taborderindex = 0;
        /// <summary>
        /// Gets m_AvailabilityController
        /// </summary>
        /// 
        public int TabOrderIndex
        {
            get { return _taborderindex; }
            set { _taborderindex = value; }
        }
        private AvailabilityController m_AvailabilityController
        {
            get
            {
                if (availabilityController == null)
                {
                    availabilityController = new AvailabilityController();
                }
                return availabilityController;
            }
        }

        private PaymentController paymentController = null;

        /// <summary>
        /// Gets PaymentController
        /// </summary>
        private PaymentController m_PaymentController
        {
            get
            {
                if (paymentController == null)
                {
                    IContentDataAccessManager contentDataAccessManager = new ContentDataAccessManager();
                    IAdvancedReservationDomain advancedReservationDomain = new AdvancedReservationDomain();
                    ILoyaltyDomain loyaltyDomain = new LoyaltyDomain();
                    IReservationDomain reservationDomain = new ReservationDomain();
                    paymentController = new PaymentController(contentDataAccessManager, advancedReservationDomain, loyaltyDomain, reservationDomain);
                }
                return paymentController;
            }
        }

        private NameController nameController = null;

        /// <summary>
        /// Gets PaymentController
        /// </summary>
        private NameController m_NameController
        {
            get
            {
                if (nameController == null)
                {
                    ISessionManager sessionManager = new SessionManager();
                    ILoyaltyDomain loyaltyDomain = new LoyaltyDomain();
                    nameController = new NameController(loyaltyDomain, sessionManager);
                }
                return nameController;
            }
        }

        private IContentDataAccessManager contentDataAccessManager = null;
        /// <summary>
        /// Gets m_ContentDataAccessManager
        /// </summary>
        private IContentDataAccessManager m_ContentDataAccessManager
        {
            get
            {
                if (contentDataAccessManager == null)
                {
                    return new ContentDataAccessManager();
                }

                return contentDataAccessManager;
            }
        }
        #endregion

        public string PageType { set; get; }

        public int TabOrderStartingIndex = 1;

        #region Enable/Disable Bed Type Preferences

        /// <summary>
        /// Enable/Disable Bed Type Preference
        /// </summary>
        // Shameem -- Remove for now
        public bool EnableBedTypePreference
        {
            get { return true; }
            set { }
        }



        #endregion

        #region Public Methods

        /// <summary>
        /// Method call on page load to populate the Drop Down Lists
        /// </summary>
        public void SetEnrollDropDowns()
        {
            SetDateOfBirthDropDown();
            string countryCodeID = "_ddlEnCountry";
            string mobileNumberID = "_ddlEnTelephoneCode";
            ddlEnCountry.Attributes.Add("onchange", "javascript:selectPhoneCode(\'" + countryCodeID + "\',\'" + mobileNumberID + "\');");
            Utility.SetCountryDropDown(ddlEnCountry, CurrentPage);
            Utility.SetPhoneCodesDropDown(ddlEnTelephoneCode, ddlEnCountry.SelectedItem.Value);
            SetPreferredLanguageDropDown();
            SetTitleDropDown();
        }


        /// <summary>
        /// Event Handler Method for Book Button Click
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="args">
        /// Arguments for the Event
        /// </param>
        protected void lnkBtnBook_Click(object sender, EventArgs args)
        {
            try
            {
                if (isSessionValid)
                {
                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:Book button StartTime:::" +
                                                                  DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));

                    Reservation2SessionWrapper.NetsPaymentErrorMessage = string.Empty;
                    Reservation2SessionWrapper.NetsPaymentInfoMessage = string.Empty;
                    if (transactionCount.Value == ReservationNumberSessionWrapper.TransactionCount.ToString())
                    {
                        if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                        {
                            bool bookingContiansPrepaidGuranteeTypes = false;
                            bool isSessionBooking = true;
                            bool isRedemptionBooking = false;
                            double totalBookingAmountOfPrepaidGuranteeTypes = 0.0;
                            double totalBookingAmountOfFlexGuranteeTypes = 0.0;
                            string prepaidGuaranteeTypeCurrencyCode = string.Empty;
                            if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REDEMPTION)
                            {
                                isSessionBooking = false;
                                isRedemptionBooking = true;
                            }
                            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:SetFetchedGuestInfo() StartTime:::" +
                                DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
                            SetFetchedGuestInfo();
                            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:SetFetchedGuestInfo() EndTime:::" +
                                DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));

                            List<GuestInformationEntity> guestList = new List<GuestInformationEntity>();
                            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:CaptureGuestInformation() StartTime:::" +
                                DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
                            CaptureGuestInformation(guestList);
                            UserNavTracker.TrackAction(this, "Booking Details");

                            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:CaptureGuestInformation() EndTime:::" +
                                DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));

                            GuestInformationEntity userInformation = guestList[0];

                            int totalGuests = guestList.Count;
                            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                            for (int guestCount = 0; guestCount < totalGuests; guestCount++)
                            {

                                if (((!BookingEngineSessionWrapper.IsModifyBooking) ||
                                     ((BookingEngineSessionWrapper.IsModifyBooking) && (!BookingEngineSessionWrapper.DirectlyModifyContactDetails)))
                                    && (search.ListRooms != null && search.ListRooms.Count > 0 &&
                                        guestCount < search.ListRooms.Count &&
                                        search.ListRooms[guestCount].ChildrenDetails != null))
                                {
                                    guestList[guestCount].ChildrensDetails = search.ListRooms[guestCount].ChildrenDetails;
                                }
                                else
                                {
                                    guestList[guestCount].ChildrensDetails = null;
                                }

                                SetPreferredLanguage(guestList[guestCount]);
                            }

                            if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null)
                            {
                                string membershipID = LoyaltyDetailsSessionWrapper.LoyaltyDetails.MembershipID;
                                if (null != membershipID && string.Empty != membershipID)
                                {
                                    for (int guestCount = 0; guestCount < guestList.Count; guestCount++)
                                    {
                                        guestList[guestCount].MembershipID = long.Parse(membershipID);
                                    }
                                }
                            }

                            GuestBookingInformationSessionWrapper.GuestBookingInformation = userInformation;

                            List<HotelRoomRateEntity> hotelRoomRate = new List<HotelRoomRateEntity>();
                            BedTypePreferenceForAllRoomsSessionWrapper.UserBedTypePreferenceForAllRooms = new List<string>();
                            int gCount = guestList.Count;
                            for (int count = 0; count < gCount; count++)
                            {
                                HotelRoomRateEntity temp = GetHotelRoomRate(guestList[count].BedTypePreferenceCode, count, guestList);
                                if (temp != null)
                                    hotelRoomRate.Add(temp);

                                if ((guestList[count].GuranteeInformation != null) &&
                                   (RoomRateUtil.IsPrepaidGuaranteeType(guestList[count].GuranteeInformation.ChannelGuranteeCode)))
                                {
                                    bookingContiansPrepaidGuranteeTypes = true;
                                    totalBookingAmountOfPrepaidGuranteeTypes = totalBookingAmountOfPrepaidGuranteeTypes + temp.TotalRate.Rate;
                                    prepaidGuaranteeTypeCurrencyCode = temp.TotalRate.CurrencyCode;
                                }
                                else
                                {
                                    if (temp.TotalRate != null)
                                    {
                                        totalBookingAmountOfFlexGuranteeTypes = totalBookingAmountOfFlexGuranteeTypes + temp.TotalRate.Rate;
                                    }
                                }
                            }
                            if (BookingEngineSessionWrapper.IsModifyBooking && chkModifyProfile.Checked)
                            {
                                if (UserLoggedInSessionWrapper.UserLoggedIn &&
                                    (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null &&
                                     (LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID == userInformation.NameID)))
                                {
                                    BookingEngineSessionWrapper.UpdateProfileBookingInfo = chkModifyProfile.Checked;
                                    UpdateUserDetails(userInformation);
                                }
                            }
                            else if (chkModifyProfile.Checked)
                            {
                                BookingEngineSessionWrapper.UpdateProfileBookingInfo = chkModifyProfile.Checked;
                                UpdateUserDetails(userInformation);
                            }
                            if (isSessionBooking && !isRedemptionBooking && !bookingContiansPrepaidGuranteeTypes)
                            {
                                Scandic.Scanweb.Core.AppLogger.LogInfoMessage(
                                    "UserContactBookingDetails:call unblock only if its a session booking UnBlockOnRdBtnChange() StartTime:::" +
                                    DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
                                UnBlockOnRdBtnChange();
                                Scandic.Scanweb.Core.AppLogger.LogInfoMessage(
                                    "UserContactBookingDetails:call unblock only if its a session booking UnBlockOnRdBtnChange() EndTime:::" +
                                    DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
                            }
                            Reservation2SessionWrapper.IsUserEnrollInBookingModule = false;

                            #region added by ashish

                            RoomInformationContainer RoomInformation = FindControl("RoomInformation0") as RoomInformationContainer;
                            double Num;
                            bool CheckNumOrStr = double.TryParse(RoomInformation.MembershipNumber, out Num);
                            if (lblChkEnroll.Checked == true && !CheckNumOrStr)
                            {
                                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:enrollment checkbox is clicked StartTime:::" +
                                    DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
                                Reservation2SessionWrapper.IsUserEnrollInBookingModule = true;
                                UserProfileEntity userProfileEntity = CaptureGuestProfileFromUI();
                                PreferredLanguageForEmailSessionWrapper.UserPreferredLanguage = ddlEnPreferredLang.Text;

                                NameController nameController = new NameController();
                                nameController.PrimaryLangaueID = ddlEnPreferredLang.Text;

                                string ConfirmationID = nameController.RegisterUser(userProfileEntity);

                                guestList[0].GuestAccountNumber = ConfirmationID;
                                guestList[0].IsNewlyEnrolled = true;

                                if (!string.IsNullOrEmpty(ConfirmationID))
                                {
                                    if (userProfileEntity.EmailID != null)
                                    {
                                        SendEnrollConfirmationMail(ConfirmationID);
                                    }
                                    IsEnrolledUserSessionWrapper.IsEnrolledUser = true;
                                }
                                else
                                {
                                    WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                        WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                                }
                                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:enrollment checkbox is clicked EndTime:::" +
                                    DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
                            }
                            IContentDataAccessManager contentDataAccessManager = new ContentDataAccessManager();

                            bool enablePaymentFallback = contentDataAccessManager.GetPaymentFallback(search.SelectedHotelCode);

                            #region for unlock rooms via ignore call and then creating reservation
                            if (!bookingContiansPrepaidGuranteeTypes || enablePaymentFallback || BookingEngineSessionWrapper.IsModifyComboBooking)
                            {
                                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:CreateReservation() StartTime:::" +
                                    DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
                                CreateReservation(guestList, hotelRoomRate, isSessionBooking, false, null);
                                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:CreateReservation() EndTime:::" +
                                    DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));

                                if (isSessionBooking)
                                {
                                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage(
                                        "UserContactBookingDetails:ConfirmBooking() if session Enabled StartTime:::" +
                                        DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
                                    ConfirmBooking(guestList);
                                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage(
                                        "UserContactBookingDetails:ConfirmBooking() if session Enabled EndTime:::" +
                                        DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
                                }
                                SortedList<string, GuestInformationEntity> sortdListGuestInfoEntity =
                                    new SortedList<string, GuestInformationEntity>();
                                foreach (GuestInformationEntity guest in guestList)
                                {
                                    sortdListGuestInfoEntity.Add(guest.LegNumber, guest);
                                }
                                GuestBookingInformationSessionWrapper.AllGuestsBookingInformations = sortdListGuestInfoEntity;

                            #endregion for unlock rooms via ignore call and then creating reservation

                            #endregion added by ashish

                                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:SendConfirmationToGuest()SEND sms StartTime:::" +
                                    DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
                                SendConfirmationToGuest();
                                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:SendConfirmationToGuest()SEND sms EndTime:::" +
                                    DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));

                                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:SendMailConfirmationToGuestsUsingHTMLString()SEND Email StartTime:::" +
                                    DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
                                SendMailConfirmationToGuestsUsingHTMLString();
                                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:SendMailConfirmationToGuestsUsingHTMLString()SEND Email EndTime:::" +
                                    DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));

                                string relativeUrl = string.Empty;
                                relativeUrl = GlobalUtil.GetUrlToPage(EpiServerPageConstants.BOOKING_CONFIRMATION_PAGE);
                                string urlToRedirect = WebUtil.GetSecureUrl(string.Format("{0}?BNo={1}&LNm={2}", relativeUrl, guestList[0].ReservationNumber, guestList[0].LastName));
                                AppLogger.LogInfoMessage(urlToRedirect);
                                Response.Redirect(urlToRedirect, false);
                                UserNavTracker.ClearTrackedData();
                            }
                            else if (!enablePaymentFallback)
                            {
                                string netsTerminalUrl = ProcessPaymentForPrepaidGuaranteeTypes(totalBookingAmountOfPrepaidGuranteeTypes,
                                                         guestList, hotelRoomRate, isSessionBooking, totalBookingAmountOfFlexGuranteeTypes,
                                                         prepaidGuaranteeTypeCurrencyCode);
                                if (!string.IsNullOrEmpty(netsTerminalUrl))
                                {
                                    Response.Redirect(netsTerminalUrl, false);
                                }
                            }

                            //SMS Cookie Logic applies for First room only
                            RoomInformationContainer roomContainer1 = FindControl("RoomInformation0") as RoomInformationContainer;
                            if (roomContainer1 != null)
                            {
                                HtmlInputControl chkSMS = roomContainer1.FindControl("SMSconfirmation") as HtmlInputControl;
                                if (chkSMS != null)
                                {
                                    if (!((HtmlInputCheckBox)chkSMS).Checked)
                                        WebUtil.StoreCookie(AppConstants.SMS_COOKIE, "false");
                                    else
                                        WebUtil.RemoveCookie(AppConstants.SMS_COOKIE);
                                }
                            }
                        }
                        else
                        {
                            if (BookingEngineSessionWrapper.IsModifyBooking)
                            {
                                Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH));
                            }
                            else
                            {
                                Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_HOTEL_PAGE));
                            }
                        }
                    }
                    else
                    {
                        operaErrorMsg.InnerHtml = WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_ALREADY_DONE);
                        clientErrorDivBD.Visible = true;
                    }
                }
            }
            catch (OWSException owsException)
            {
                if (!string.Equals(owsException.ErrCode, OWSExceptionConstants.INSUFFICIENT_AWARD_POINTS_FOR_RESERVATION, StringComparison.InvariantCultureIgnoreCase)
                    && !string.Equals(owsException.ErrCode, OWSExceptionConstants.BOOKING_NOT_FOUND_IGNORE, StringComparison.InvariantCultureIgnoreCase))
                    AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);

                switch (owsException.ErrCode)
                {
                    case OWSExceptionConstants.GENERAL_UPD_FAILURE:
                        {
                            operaErrorMsg.InnerHtml = WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_LOCKED_BY_STAFF_MEMBER);
                            clientErrorDivBD.Visible = true;
                            break;
                        }
                    case OWSExceptionConstants.RESERVATION_EXISTS_FOR_CREDITCARD:
                        {
                            operaErrorMsg.InnerHtml = WebUtil.GetTranslatedText(TranslatedTextConstansts.RESERVATION_EXISTS_FOR_CREDITCARD);
                            clientErrorDivBD.Visible = true;
                            break;
                        }
                    case OWSExceptionConstants.BOOKING_NOT_FOUND_IGNORE:
                        operaErrorMsg.InnerText = WebUtil.GetTranslatedText(owsException.TranslatePath);
                        clientErrorDivBD.Visible = true;
                        break;
                    case OWSExceptionConstants.INVALID_CREDIT_CARD:
                        operaErrorMsg.InnerText = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/owserror/invalid_credit_card");
                        clientErrorDivBD.Visible = true;
                        break;
                    default:
                        if ((owsException.Message == AppConstants.SYSTEM_ERROR))
                        {
                            WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                        }
                        if (owsException.TranslatePath == AppConstants.SITE_WIDE_ERROR)
                        {
                            if (owsException.ErrMessage.Length > 0)
                            {
                                operaErrorMsg.InnerHtml = owsException.ErrMessage;
                                clientErrorDivBD.Visible = true;
                            }
                            else
                            {
                                WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                    WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                            }
                        }
                        string notEnoughPointsMsg = "THIS CERTIFICATE OR AWARDS DONOT HAVE ENOUGH POINTS TO BOOK RESERVATION.";
                        string roomUnavailableForRedeemption = "RESORT IS RESTRICTED OR ROOM IS NOT AVAILABLE.";

                        if (owsException.Message == notEnoughPointsMsg)
                        {
                            operaErrorMsg.InnerHtml = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/owserror/not_enough_point_msg");
                            clientErrorDivBD.Visible = true;
                        }
                        else if (owsException.Message.Equals(roomUnavailableForRedeemption, StringComparison.CurrentCultureIgnoreCase))
                        {
                            operaErrorMsg.InnerHtml = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/owserror/room_unavailable");
                            clientErrorDivBD.Visible = true;
                        }
                        else if (String.Equals(OWSExceptionConstants.GUARANTEE_NOT_ACCEPTED, owsException.ErrMessage,
                                                             StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                            {
                                string roomTypes = string.Empty;
                                string ratePlanCodes = string.Empty;
                                if (BookingEngineSessionWrapper.SelectedRoomAndRatesHashTable != null && BookingEngineSessionWrapper.SelectedRoomAndRatesHashTable.Count > 0)
                                {
                                    for (int i = 0; i < BookingEngineSessionWrapper.SelectedRoomAndRatesHashTable.Count; i++)
                                    {
                                        SelectedRoomAndRateEntity selectedRoomRates = BookingEngineSessionWrapper.SelectedRoomAndRatesHashTable[i] as SelectedRoomAndRateEntity;
                                        roomTypes = roomTypes + selectedRoomRates.SelectedRoomCategoryName + ",";
                                        if (GuestBookingInformationSessionWrapper.GuestInformationList != null && GuestBookingInformationSessionWrapper.GuestInformationList.Count > 0)
                                        {
                                            GuestInformationEntity guestInfoEntity = GuestBookingInformationSessionWrapper.GuestInformationList[i];
                                            foreach (var roomRates in selectedRoomRates.RoomRates)
                                            {
                                                if (guestInfoEntity.BedTypePreferenceCode == roomRates.RoomTypeCode)
                                                    ratePlanCodes = ratePlanCodes + roomRates.RatePlanCode + ",";
                                            }
                                        }
                                    }
                                    roomTypes = roomTypes.TrimEnd(',');
                                    ratePlanCodes = ratePlanCodes.Trim(',');
                                }
                                if (!string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode))
                                    ratePlanCodes = SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode;

                                StringBuilder logMessage = new StringBuilder();
                                logMessage.Append(ConfigurationManager.AppSettings["GuaranteeMismatchEmail"]);
                                logMessage.Append("\r\n");
                                logMessage.Append("\r\n");
                                logMessage.Append(string.Format("Opera error code/exception: {0}{1}{2}", owsException.ErrCode, owsException.ErrMessage, "\r\n"));
                                logMessage.Append(string.Format("Error occured for bookingCode: {0}{1}", ratePlanCodes, "\r\n"));
                                logMessage.Append(string.Format("Type of Booking: {0}{1}", SearchCriteriaSessionWrapper.SearchCriteria.SearchingType, "\r\n"));
                                logMessage.Append(string.Format("Hotel opera Id: {0}{1}", SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode, "\r\n"));
                                logMessage.Append(string.Format("Arrival Date: {0}{1}", SearchCriteriaSessionWrapper.SearchCriteria.ArrivalDate, "\r\n"));
                                logMessage.Append(string.Format("Number of nights: {0}{1}", SearchCriteriaSessionWrapper.SearchCriteria.NoOfNights, "\r\n"));
                                logMessage.Append(string.Format("Room Type: {0}", roomTypes));
                                var logMsge = Convert.ToString(logMessage);
                                AppLogger.LogFatalException(owsException, logMsge);
                                AppLogger.LogSmtpAppenderLogger(logMsge);
                            }
                            operaErrorMsg.InnerText = WebUtil.GetTranslatedText(owsException.TranslatePath);
                            clientErrorDivBD.Visible = true;
                        }
                        else
                        {
                            operaErrorMsg.InnerText = WebUtil.GetTranslatedText(owsException.TranslatePath);
                            clientErrorDivBD.Visible = true;
                        }
                        break;
                }

                if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null)
                {
                    int roomCount = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count;
                    for (int count = 0; count < roomCount; count++)
                    {
                        RoomInformationContainer RoomInformation =
                            FindControl("RoomInformation" + count) as RoomInformationContainer;
                        if (null != RoomInformation)
                        {
                            string header = "/bookingengine/booking/bookingdetail/room" + count + count;
                            RoomInformation.RoomHeader = WebUtil.GetTranslatedText(header);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);

                if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null)
                {
                    int roomCount = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count;
                    for (int count = 0; count < roomCount; count++)
                    {
                        RoomInformationContainer RoomInformation =
                            FindControl("RoomInformation" + count) as RoomInformationContainer;
                        if (null != RoomInformation)
                        {
                            string header = "/bookingengine/booking/bookingdetail/room" + count + count;
                            RoomInformation.RoomHeader = WebUtil.GetTranslatedText(header);
                        }
                    }
                }
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:Book button EndTime:::" +
                                                          DateTime.Now.ToString(("yyyy-dd-MM`hh.mm.ss.fffftt")));
        }

        #region SendEnrollConfirmationMailToUser

        /// <summary>
        /// This method sends confirmation email to the user enrolled.
        /// </summary>
        /// <param name="registrationID"></param>
        /// <param name="email"></param>
        private void SendEnrollConfirmationMail(string registrationID)
        {
            try
            {
                Dictionary<string, string> emailEnrollMap = new Dictionary<string, string>();
                emailEnrollMap[CommunicationTemplateConstants.LOGOPATH] = AppConstants.LOGOPATH;
                emailEnrollMap[CommunicationTemplateConstants.CONFIRMATION_NUMBER] = registrationID;
                emailEnrollMap[CommunicationTemplateConstants.PASSWORD] = txtEnrollPassword.Text.Trim();
                emailEnrollMap[CommunicationTemplateConstants.FIRST_NAME] = txtEnFName.Value.Trim();
                emailEnrollMap[CommunicationTemplateConstants.LASTNAME] = txtEnLName.Value.Trim();
                emailEnrollMap[CommunicationTemplateConstants.EMAIL] = txtEnEmail.Value.Trim();
                emailEnrollMap[CommunicationTemplateConstants.ADDRESS_LINE1] = txtEnAddressLine1.Value.Trim();
                if (!string.IsNullOrEmpty(txtEnAddressLine2.Value))
                    emailEnrollMap[CommunicationTemplateConstants.ADDRESS_LINE2] = txtEnAddressLine2.Value.Trim() !=
                                                                                   txtEnAddressLine2.Attributes["rel"].
                                                                                       ToString()
                                                                                       ? txtEnAddressLine2.Value.Trim()
                                                                                       : string.Empty;
                else
                    emailEnrollMap[CommunicationTemplateConstants.ADDRESS_LINE2] = CommunicationTemplateConstants.BLANK_SPACES;
                emailEnrollMap[CommunicationTemplateConstants.CITY] = txtEnCityOrTown.Value;
                emailEnrollMap[CommunicationTemplateConstants.COUNTRY] = ddlEnCountry.SelectedItem.Text;
                emailEnrollMap[CommunicationTemplateConstants.POST_CODE] = txtEnPostCode.Value.Trim();
                emailEnrollMap[CommunicationTemplateConstants.MEMBERSHIPNO_BOOKMARK_LINK_DISPLAY_STYLE] = "none";
                if (Convert.ToBoolean(AppConstants.ShowMembershipDetailsLink))
                {
                    try
                    {
                        //var payload = new Dictionary<string, string>
                        //{
                        //    { "MembershipID", registrationID },
                        //    { "LastName", txtLName.Value.Trim().ToUpper() },
                        //};
                        string payload = string.Format("{0}{1}{2}", registrationID, AppConstants.COMMA, txtEnLName.Value.Trim().ToUpper());
                        string encKey = Convert.ToString(ConfigurationManager.AppSettings["EncryptionKey"]);
                        emailEnrollMap[CommunicationTemplateConstants.MEMBERSHIP_CARD_URL] = string.Format("{0}://{1}/{2}?memkey={3}",
                            Request.Url.Scheme, Request.Url.Host, AppConstants.VIEW_MEMBERSHIP_DETAILS,
                            System.Web.HttpUtility.UrlEncode(ScanwebJsonWebToken.Encrypt(payload, encKey)));
                        emailEnrollMap[CommunicationTemplateConstants.MEMBERSHIPNO_BOOKMARK_LINK_DISPLAY_STYLE] = "block";
                    }
                    catch (Exception ex)
                    {
                        AppLogger.LogFatalException(ex, string.Format("Error occured for the user with MembershipNumber: {0}", registrationID));
                    }
                }
                EmailEntity emailEntity = CommunicationUtility.GetEnrollConfirmationEmail(emailEnrollMap);

                emailEntity.Recipient = new string[] { txtEnEmail.Value.Trim() };

                CommunicationService.SendMail(emailEntity, null);
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
            }
        }

        #endregion SendEnrollConfirmationMailToUser

        /// <summary>
        /// Function Thatb calls webutil function to unblock the rooms
        /// </summary>
        private void UnBlockOnRdBtnChange()
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if (search != null && search.ListRooms != null && search.ListRooms.Count > 0)
            {
                for (int iListRoom = 0; iListRoom < search.ListRooms.Count; iListRoom++)
                {
                    if (search.ListRooms[iListRoom].IsBlocked &&
                        (!string.IsNullOrEmpty(search.ListRooms[iListRoom].RoomBlockReservationNumber)) &&
                        (search.ListRooms[iListRoom].IsRoomModifiable))
                    {
                        WebUtil.UnblockRoom(search.ListRooms[iListRoom].RoomBlockReservationNumber);
                        search.ListRooms[iListRoom].RoomBlockReservationNumber = string.Empty;
                        search.ListRooms[iListRoom].IsBlocked = false;
                    }
                }
            }
        }

        /// <summary>
        /// Confirms the created booking. This is required for the sessionEnabled.
        /// </summary>
        private void ConfirmBooking(List<GuestInformationEntity> guestList)
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if ((search != null) && (guestList.Count == search.ListRooms.Count))
            {
                for (int i = 0; i < guestList.Count; i++)
                {
                    if (search.ListRooms[i].IsRoomModifiable)
                    {
                        GuestInformationEntity guest = guestList[i];
                        WebUtil.ConfirmBooking(guest.ReservationNumber, guest.LegNumber);
                    }
                }
            }
        }
        #endregion

        protected string firstNameText = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPFName");
        protected string lastNameText = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPLName");
        protected string email = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPEmail");
        protected string townOrCity = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/townorcity");
        protected string phone = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/PrimaryPhNo");
        protected string password = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/password");
        private string address1 = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPAddress1");
        private string address2 = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/FGPAddress2");
        private string retypePassword = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/retypepassword");
        private string typeAnswer = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/answer");
        private string postCode = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/postcode");
        private bool isSessionValid = true;
        #region Protected Methods

        /// <summary>
        /// Page Load Method of UserBookingContact Details User Control
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="e">
        /// Arguments for the Event
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            txtEnrollReTypePassword.Attributes.Add("fieldName", "EnrollPassword");
            txtEnrollPassword.Attributes.Add("fieldName", "EnrollReTypePassword");
            isSessionValid = WebUtil.IsSessionValid();
            Page.Form.SubmitDisabledControls = true;
            
            this.chkTermsCondition.Attributes.Add("tip",
                                                  WebUtil.GetTranslatedText(
                                                      "/scanweb/bookingengine/errormessages/bookingDetails/CheckboxRd"));
            ////this.lblAgeconfirmation.Attributes.Add("tip",
            //                                       WebUtil.GetTranslatedText(
            //                                           "/scanweb/bookingengine/errormessages/bookingDetails/CheckboxRd"));
            //this.chkReceiveScandicInfo.Attributes.Add("tip",
            //                                          WebUtil.GetTranslatedText(
            //                                              "/scanweb/bookingengine/errormessages/bookingDetails/CheckboxRd"));
            rdoEnMale.Checked = true;
            clientErrorDivBD.Visible = false;
            FindControls();
            AppLogger.LogInfoMessage("START : In UserBookingContactDetails page :: Page_Load() : ");

            if (UserLoggedInSessionWrapper.UserLoggedIn)
            {
                lblFGPDiv.Attributes["style"] = "display:none;";
            }

            txtCardHolderCommon.Attributes["rel"] =
                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/NameOnCard");
            txtCardNumberCommon.Attributes["rel"] =
                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/NumberOnCard");
            try
            {
                if (this.Visible && isSessionValid)
                {
                    txtComments.Visible = false;
                    if (!string.IsNullOrEmpty(Reservation2SessionWrapper.NetsPaymentErrorMessage))
                    {
                        operaErrorMsg.InnerHtml = Reservation2SessionWrapper.NetsPaymentErrorMessage;
                        clientErrorDivBD.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(Reservation2SessionWrapper.NetsPaymentInfoMessage))
                    {
                        lblInfoMessage.InnerHtml = Reservation2SessionWrapper.NetsPaymentInfoMessage;
                        divInfoMessage.Visible = true;
                    }
                    if (BookingEngineSessionWrapper.IsModifyBooking)
                    {
                        if (BookingEngineSessionWrapper.DirectlyModifyContactDetails)
                        {
                            btnBack.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_CHANGE_DATES);
                        }
                        else
                        {
                            btnBack.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE);
                        }
                        lblFGPDiv.Attributes["style"] = "display:none;";
                    }
                    else
                    {
                        btnBack.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE);
                    }

                    if (!IsPostBack)
                    {
                        HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                        SetIfRoomIsModifiable();
                        SetEnrollDropDowns();
                        SetEnrollRelText();
                        SetTotalRateandCurrencyCode();
                        bool paymentFallback = m_ContentDataAccessManager.GetPaymentFallback(search.SelectedHotelCode);

                        UserLoggedInSessionWrapper.IsLoginFromBookingDetailsPage = false;

                        divBookForSomeOneElse.Visible = UserLoggedInSessionWrapper.UserLoggedIn &&
                                                        (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType ==
                                                         SearchType.REDEMPTION);
                        BookingDetailsLoginControl.Visible = !(UserLoggedInSessionWrapper.UserLoggedIn);
                        if (BookingDetailsLoginControl.Visible == true)
                        {
                            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                            {
                                if (SearchCriteriaSessionWrapper.SearchCriteria.QualifyingType ==
                                    AppConstants.QUALIFYING_TYPE_TRAVEL_AGENT)
                                {
                                    BookingDetailsLoginControl.Visible = false;
                                }
                            }
                        }

                        bool showMemberInfo = false;
                        if (UserLoggedInSessionWrapper.UserLoggedIn)
                        {
                            if (BookingEngineSessionWrapper.IsModifyBooking)
                            {
                                if (BookingEngineSessionWrapper.BookingDetails != null && LoyaltyDetailsSessionWrapper.LoyaltyDetails != null)
                                {
                                    GuestInformationEntity guestBookingInfo = BookingEngineSessionWrapper.BookingDetails.GuestInformation;
                                    LoyaltyDetailsEntity loyaltyEntity = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
                                    if (guestBookingInfo.NameID == loyaltyEntity.NameID)
                                    {
                                        showMemberInfo = true;
                                    }
                                }
                            }
                            else
                            {
                                showMemberInfo = true;
                            }
                        }
                        UpdateGuestInfo.Visible = showMemberInfo;

                        SelectedRoomAndRateEntity selectedRoomRates = null;
                        if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null &&
                            HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count > 0)
                        {
                            selectedRoomRates = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[0] as SelectedRoomAndRateEntity;
                        }

                        Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:PopulateRoomInformation() Begin");
                        PopulateRoomInformation();
                        if (!paymentFallback && !BookingEngineSessionWrapper.IsModifyComboBooking)
                        {
                            PopulateFGPSavedCreditCards();
                        }
                        SetBookButtonText(paymentFallback);

                        Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:PopulateRoomInformation() End");
                        if ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation))
                        {
                            DisableNonModifiableRooms();
                        }

                        HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                        if (hotelSearch != null)
                        {
                            string rateCategoryID = string.Empty;
                            if ((BookingEngineSessionWrapper.IsModifyBooking) && (BookingEngineSessionWrapper.DirectlyModifyContactDetails))
                            {
                                BookingDetailsEntity bookingDetails = BookingEngineSessionWrapper.BookingDetails;
                                if ((bookingDetails != null) && (bookingDetails.HotelRoomRate != null))
                                {
                                    string ratePlanCode = bookingDetails.HotelRoomRate.RatePlanCode;

                                    if ((string.IsNullOrEmpty(ratePlanCode)) && (Utility.IsBlockCodeBooking))
                                    {
                                        rateCategoryID = AppConstants.BLOCK_CODE_QUALIFYING_TYPE;
                                    }
                                    else
                                    {
                                        RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlanCode);
                                        if (rateCategory != null)
                                        {
                                            rateCategoryID = rateCategory.RateCategoryId;
                                        }
                                    }
                                }
                            }
                            else if (selectedRoomRates != null)
                            {
                                rateCategoryID = selectedRoomRates.RateCategoryID;
                            }

                            HotelRoomRateSessionWrapper.SelectedRateCategoryID = rateCategoryID;
                            isModifyBooking.Value = BookingEngineSessionWrapper.IsModifyBooking.ToString();
                            UserLoggedIn.Value = UserLoggedInSessionWrapper.UserLoggedIn.ToString();

                            transactionCount.Value = ReservationNumberSessionWrapper.TransactionCount.ToString();

                            Hashtable roomRates = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable as Hashtable;
                            if (roomRates != null && roomRates.Values.Count > 0)
                            {
                                foreach (SelectedRoomAndRateEntity roomRate in roomRates.Values)
                                {
                                    if (roomRate != null && roomRate.SelectedRoomCategoryEntity != null)
                                    {
                                        HotelDestination hotel =
                                            m_AvailabilityController.GetHotelDestination(hotelSearch.SelectedHotelCode);

                                        HotelRoomDescription roomDescription = hotel.HotelRoomDescriptions
                                            .FirstOrDefault(
                                                room =>
                                                room.HotelRoomCategory.RoomCategoryId ==
                                                roomRate.SelectedRoomCategoryEntity.Id);
                                        if (roomDescription != null)
                                            roomRate.SelectedRoomCategoryEntity.Url =
                                                roomDescription.HotelRoomCategory.ImageURL;
                                        else
                                            roomRate.SelectedRoomCategoryEntity.Url = string.Empty;
                                        RoomDescriptionPopup roomDescriptionPopup =
                                            LoadControl("RoomDescriptionPopup.ascx") as RoomDescriptionPopup;

                                        roomDescriptionPopup.SetRoomCategoryDetails(roomRate.SelectedRoomCategoryEntity);

                                        if (roomDescriptionPopup != null)
                                            RoomCategoryContainer.Controls.Add(roomDescriptionPopup);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (BookingEngineSessionWrapper.IsModifyBooking)
                            {
                                Response.Redirect(
                                    GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH));
                            }
                            else
                            {
                                Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_HOTEL_PAGE));
                            }
                        }
                    }
                }
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
            AppLogger.LogInfoMessage("END : In UserBookingContactDetails page :: Page_Load() : ");

        }



        private void SetIfRoomIsModifiable()
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if ((search != null) && (search.ListRooms != null) && (search.ListRooms.Count > 0))
            {
                for (int i = 0; i < search.ListRooms.Count; i++)
                {
                    HotelSearchRoomEntity currentRoom = search.ListRooms[i];
                    if (currentRoom != null)
                    {
                        RoomInformationContainer currentRoomInformationContainer =
                            FindControl("RoomInformation" + i) as RoomInformationContainer;
                        if (currentRoomInformationContainer != null)
                        {
                            currentRoomInformationContainer.IsRoomModifiable = currentRoom.IsRoomModifiable;
                            TabOrderStartingIndex = TabOrderIndex + (i + 1) * 30;
                            SetTabOrdersForControls(currentRoomInformationContainer);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Function to assign tab orders to Room Information Control
        /// </summary>
        /// <param name="roomControlElement">RooomInformation[n] control. Contains the collection of the controls of RoomInformation.ascx</param>
        private void SetTabOrdersForControls(Control roomControlElement)
        {
            //local variables
            string addnRequestID = "spnAddSpecialrequests";
            HtmlInputText textElement = new HtmlInputText();
            HtmlInputCheckBox chckBoxElement = new HtmlInputCheckBox();
            DropDownList dropDownListElement = new DropDownList();
            HtmlAnchor htmlAnchorElement = new HtmlAnchor();
            HtmlTextArea textAreaElement = new HtmlTextArea();
            HtmlInputRadioButton rdoElement = new HtmlInputRadioButton();
            ControlCollection additionalRequestControlCollection = new ControlCollection(this);
            additionalRequestControlCollection = roomControlElement.Controls[3].Controls;
            foreach (Control c in roomControlElement.Controls[3].Controls)  //control[3]: contains the set of controls to which tab order is to be assigned.
            {
                if (c is HtmlInputText) //select the input elements based on their type.
                {
                    textElement = (HtmlInputText)roomControlElement.FindControl(c.ID);
                    TabOrderStartingIndex = ++TabOrderStartingIndex;
                    textElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());
                }
                if (c is System.Web.UI.HtmlControls.HtmlInputCheckBox)
                {
                    chckBoxElement = (HtmlInputCheckBox)roomControlElement.FindControl(c.ID);
                    this.TabOrderStartingIndex = TabOrderStartingIndex++;
                    chckBoxElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());
                }
                if (c is DropDownList)
                {
                    dropDownListElement = (DropDownList)roomControlElement.FindControl(c.ID);
                    this.TabOrderStartingIndex = TabOrderStartingIndex++;
                    dropDownListElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());
                }
            }

            if (additionalRequestControlCollection[15] != null)     // select the Bonus Number text box
            {
                HtmlInputText txtInputElement = new HtmlInputText();
                txtInputElement = (HtmlInputText)additionalRequestControlCollection[15].FindControl("txtD_Number");
                this.TabOrderStartingIndex = ++TabOrderStartingIndex;
                txtInputElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());
            }

            if (additionalRequestControlCollection[16] != null) //selects the "Add additional requirements" 
            {
                htmlAnchorElement = (HtmlAnchor)roomControlElement.FindControl(addnRequestID);
                this.TabOrderStartingIndex = ++TabOrderStartingIndex;
                htmlAnchorElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());
                htmlAnchorElement.Attributes.Add("href", "#");
            }
            if (additionalRequestControlCollection[18].Controls != null) //controls inside the "additional requirements"
            {
                foreach (Control addRequestControl in additionalRequestControlCollection[18].Controls)
                {
                    if (addRequestControl is System.Web.UI.HtmlControls.HtmlInputText)
                    {
                        textElement = (HtmlInputText)roomControlElement.FindControl(addRequestControl.ID);
                        TabOrderStartingIndex = ++TabOrderStartingIndex;
                        textElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());
                    }
                    if (addRequestControl is HtmlInputCheckBox)
                    {
                        chckBoxElement = (HtmlInputCheckBox)roomControlElement.FindControl(addRequestControl.ID);
                        this.TabOrderStartingIndex = ++TabOrderStartingIndex;
                        chckBoxElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());
                    }
                    if (addRequestControl is DropDownList)
                    {
                        dropDownListElement = (DropDownList)roomControlElement.FindControl(addRequestControl.ID);
                        this.TabOrderStartingIndex = ++TabOrderStartingIndex;
                        dropDownListElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());
                    }
                    if (addRequestControl is HtmlTextArea)
                    {
                        textAreaElement = (HtmlTextArea)roomControlElement.FindControl(addRequestControl.ID);
                        this.TabOrderStartingIndex = ++TabOrderStartingIndex;
                        textAreaElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());
                    }
                }
            }
            //guarantee information control Tab order indexing.
            #region Guarantee Information Controls
            rdoElement = (HtmlInputRadioButton)roomControlElement.FindControl("rdoHoldRoom");
            this.TabOrderStartingIndex = ++TabOrderStartingIndex;
            rdoElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());

            chckBoxElement = (HtmlInputCheckBox)roomControlElement.FindControl("rdoLateArrivalGurantee");
            this.TabOrderStartingIndex = ++TabOrderStartingIndex;
            chckBoxElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());

            textElement = (HtmlInputText)roomControlElement.FindControl("txtCardHolder");
            this.TabOrderStartingIndex = ++TabOrderStartingIndex;
            textElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());

            dropDownListElement = (DropDownList)roomControlElement.FindControl("ddlCardType");
            this.TabOrderStartingIndex = ++TabOrderStartingIndex;
            dropDownListElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());

            textElement = (HtmlInputText)roomControlElement.FindControl("txtCardNumber");
            this.TabOrderStartingIndex = ++TabOrderStartingIndex;
            textElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());

            dropDownListElement = (DropDownList)roomControlElement.FindControl("ddlExpiryMonth");
            this.TabOrderStartingIndex = ++TabOrderStartingIndex;
            dropDownListElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());

            dropDownListElement = (DropDownList)roomControlElement.FindControl("ddlExpiryYear");
            this.TabOrderStartingIndex = ++TabOrderStartingIndex;
            dropDownListElement.Attributes.Add("tabIndex", TabOrderStartingIndex.ToString());
            #endregion

        }

        private void DisableNonModifiableRooms()
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if ((search != null) && (search.ListRooms != null) && (search.ListRooms.Count > 0))
            {
                for (int i = 0; i < search.ListRooms.Count; i++)
                {
                    HotelSearchRoomEntity currentRoom = search.ListRooms[i];
                    if (currentRoom != null)
                    {
                        RoomInformationContainer currentRoomInformationContainer =
                            FindControl("RoomInformation" + i) as RoomInformationContainer;
                        if (!(currentRoom.IsRoomModifiable))
                        {
                            //currentRoomInformationContainer.SetBedTypePreferenceForNonModifiableRooms();
                            currentRoomInformationContainer.DisableRoomInformationContents();
                            //Parvathi | Res2.1 | CR-Modify combo reservation.
                            currentRoomInformationContainer.DisplayText();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Reservation 2.0| Set the SessionWrapper.FetchedGuestInformation entity as this is being used 
        /// for comparision when loggedin user updates the details
        /// Author: Ruman Khan
        /// </summary>
        private void SetFetchedGuestInfo()
        {
            GuestInformationEntity guestInfoEntity = null;
            string nameID = string.Empty;
            if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null)
            {
                nameID = LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID;
                if (nameID != string.Empty)
                {
                    NameController nameController = new NameController();
                    UserProfileEntity userProfileEntity = null;

                    if (UserProfileInformationSessionWrapper.UserProfileInformation != null)
                    {
                        userProfileEntity = UserProfileInformationSessionWrapper.UserProfileInformation;
                    }
                    else
                    {
                        userProfileEntity = nameController.FetchUserDetails(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID);
                    }
                    guestInfoEntity = Utility.ConvertToGuestInformation(userProfileEntity);
                    if (guestInfoEntity != null)
                    {
                        guestInfoEntity.GuestAccountNumber = LoyaltyDetailsSessionWrapper.LoyaltyDetails.UserName;
                        GuestBookingInformationSessionWrapper.FetchedGuestInformation = guestInfoEntity;
                    }
                }
            }
        }

        /// <summary>
        /// Reservation 2.0 | Method to set the rel text for enrollment
        /// Author : RK
        /// </summary>
        private void SetEnrollRelText()
        {
            txtEnFName.Attributes["rel"] = firstNameText;
            txtEnLName.Attributes["rel"] = lastNameText;
            txtEnEmail.Attributes["rel"] = email;
            txtEnAddressLine1.Attributes["rel"] = address1;
            txtEnAddressLine2.Attributes["rel"] = address2;
            txtEnCityOrTown.Attributes["rel"] = townOrCity;
            txtEnTelephone.Value = phone;
            txtEnTelephone.Attributes["rel"] = phone;
            txtEnrollPassword.Attributes["rel"] = password;
            txtEnrollReTypePassword.Attributes["rel"] = retypePassword;
            txtEnPostCode.Attributes["rel"] = postCode;
        }

        private void FindControls()
        {
            for (int i = 0; i < this.Controls.Count; i++)
            {
                try
                {
                    if (this.Controls[i].GetType() == typeof(HtmlInputText))
                    {
                        HtmlInputText htmlInputContr = this.Controls[i] as HtmlInputText;
                        string strXml = htmlInputContr.Value;
                        htmlInputContr.Value = WebUtil.GetTranslatedText(strXml);
                    }
                    if (this.Controls[i].Controls.Count > 0)
                        FindControls(this.Controls[i]);
                }
                catch (Exception ex)
                {
                }
            }
        }

        private void FindControls(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                try
                {
                    if (control.Controls[i].Controls.Count > 0)
                        FindControls(control.Controls[i]);
                    if (control.Controls[i].GetType() == typeof(HtmlInputText))
                    {
                        HtmlInputText htmlInputContr = control.Controls[i] as HtmlInputText;
                        string strXml = htmlInputContr.Value;
                        htmlInputContr.Value = WebUtil.GetTranslatedText(strXml);
                    }
                }
                catch (Exception ex)
                { }
            }
        }

        private void SetTotalRateandCurrencyCode()
        {
            if (BookingEngineSessionWrapper.HideARBPrice)
            {
                spnTotalRate.InnerText = Utility.GetPrepaidString();
                spnCartTerms.InnerText = string.Empty;
            }
            else
            {
                string totalRateandCurrency;
                if (this.PageType == EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS &&
                    !(HttpContext.Current.Request.UrlReferrer != null &&
                    HttpContext.Current.Request.UrlReferrer.ToString().Contains(AppConstants.RESERVATION_MODIFY_SELECT_RATE)))
                    totalRateandCurrency = Utility.SetTotalRateAndCurrencyCodeForModify();
                else
                    totalRateandCurrency = Utility.GetTotalRateAndCurrencyCode();

                if (!string.IsNullOrEmpty(totalRateandCurrency))
                {
                    string[] ratestring = totalRateandCurrency.Trim().Split(AppConstants.SPACE.ToCharArray());
                    //MERCHANDISING | CR:Have number of gift vouchers printed |Parvathi
                    if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.VOUCHER)
                    {
                        spnTotalRate.InnerText = (ratestring != null && ratestring.Count() > 0) ? ratestring[0] : string.Empty;
                        string currency1 = (ratestring != null && ratestring.Count() > 1 && ratestring[1] != null) ? ratestring[1] : string.Empty;
                        string currency2 = (ratestring != null && ratestring.Count() > 2 && ratestring[2] != null) ? ratestring[2] : string.Empty;
                        spnCurrencyCode.InnerText = string.Format("{0} {1}", currency1, currency2);
                    }
                    else if (!string.IsNullOrEmpty(totalRateandCurrency))
                    {
                        int currencyIndex = (ratestring != null && ratestring.Count() > 0) ? ratestring.Count() - 1 : 0;
                        string currencyCode = ratestring[currencyIndex];
                        spnTotalRate.InnerText = totalRateandCurrency.Trim().TrimEnd(currencyCode.ToCharArray()).Trim();
                        spnCurrencyCode.InnerText = currencyCode;
                        spnCartTerms.InnerText = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/InclTaxesFees");
                    }
                }

            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// After Client side validation capture the user input from EnrollGuestProgram Screen
        /// </summary>
        /// <returns>UserProfileEntity</returns>
        private UserProfileEntity CaptureGuestProfileFromUI()
        {
            UserProfileEntity userProfileEntity = new UserProfileEntity();

            userProfileEntity.FirstName = txtEnFName.Value.Trim();
            userProfileEntity.LastName = txtEnLName.Value.Trim();
            userProfileEntity.SetGender(rdoEnMale.Checked ? AppConstants.MALE : AppConstants.FEMALE);

            int day = 0;
            int month = 0;
            int year = 0;
            if (int.TryParse(ddlEnDOBDay.Text, out day) && int.TryParse(ddlEnDOBMonth.Text, out month) &&
                int.TryParse(ddlEnDOBYear.Text, out year))
            {
                userProfileEntity.DateOfBirth = new DateOfBirthEntity(day, month, year);
            }

            if (txtEnEmail.Value != "")
                userProfileEntity.EmailID = txtEnEmail.Value;

            if (ddlEnTelephoneCode.Text != "DFT")
            {
                string mobilePhoneCode = Utility.FormatePhoneCode(ddlEnTelephoneCode.Text);
                userProfileEntity.MobilePhone = mobilePhoneCode + Regex.Replace(txtEnTelephone.Value.Trim().TrimStart('0'), @"\D", "");
            }

            userProfileEntity.AddressType = AppConstants.ADDRESS_TYPE_HOME;
            if (txtEnAddressLine1.Value != "")
                userProfileEntity.AddressLine1 = txtEnAddressLine1.Value;
            if (txtEnAddressLine2.Value != "")
                userProfileEntity.AddressLine2 = txtEnAddressLine2.Value;

            userProfileEntity.PostCode = txtEnPostCode.Value;
            userProfileEntity.City = txtEnCityOrTown.Value;
            userProfileEntity.Country = ddlEnCountry.Text;
            userProfileEntity.PreferredLanguage = ddlEnPreferredLang.Text;
            System.Globalization.TextInfo tInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
            userProfileEntity.NameTitle = (ddlTitle.Text != AppConstants.TITLE_DEFAULT_VALUE && string.Equals(ddlEnPreferredLang.Text, LanguageConstant.LANGUAGE_GERMAN_CODE)) ? tInfo.ToTitleCase(ddlTitle.Text) : null;

            userProfileEntity.Password = txtEnrollPassword.Text;

            if (chkMembershipTerms.Checked)
            {
                userProfileEntity.ScandicEmail = true;
                userProfileEntity.ScandicEMailPrev = true;
            }
            else
            {
                userProfileEntity.ScandicEmail = false;
                userProfileEntity.ScandicEMailPrev = false;
            }

            if (chkReceivePartnersInfo.Checked)
                userProfileEntity.ThridPartyEmail = true;
            else
                userProfileEntity.ThridPartyEmail = false;

            return userProfileEntity;
        }

        /// <summary>
        /// Set the drop down list of Prefered language
        /// </summary>
        private void SetPreferredLanguageDropDown()
        {
            OrderedDictionary preferredLangCodeMap = DropDownService.GetPreferredLanguageCodes();
            if (preferredLangCodeMap != null)
            {
                foreach (string key in preferredLangCodeMap.Keys)
                {
                    ddlEnPreferredLang.Items.Add(new ListItem(preferredLangCodeMap[key].ToString(), key));
                }
            }
            IFormatProvider cultureInfo =
                new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
            string PreferedlangSelected = cultureInfo.ToString().Substring(0, 2).ToUpper();

            if (PreferedlangSelected == LanguageConstant.LANGUAGE_ENGLISH)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_ENGLISH_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_DANISH)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_DANISH_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_NORWEGIAN)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_NORWEGIAN_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_SWEDISH)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_SWEDISH_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_FINNISH)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_FINNISH_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_GERMAN)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_GERMAN_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_RUSSIA)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_RUSSIA_CODE;
            }
            
            ListItem selectedDefaultItem = ddlEnPreferredLang.Items.FindByValue(PreferedlangSelected);
            if (selectedDefaultItem != null)
            {
                ddlEnPreferredLang.SelectedItem.Selected = false;
                selectedDefaultItem.Selected = true;
            }
        }

        /// <summary>
        /// Set the drop down list of German titles
        /// </summary>
        private void SetTitleDropDown()
        {
            InformationController informationCtrl = new InformationController();
            Dictionary<string, string> nameTitleMapOpera =
                informationCtrl.GetConfiguredTitles(LanguageConstant.LANGUAGE_GERMAN_CODE);

            ListItem selectItem = new ListItem(AppConstants.TITLE_DEFAULT_VALUE,
                                               AppConstants.TITLE_DEFAULT_VALUE);
            ddlTitle.Items.Add(selectItem);

            foreach (string key in nameTitleMapOpera.Keys)
            {
                ddlTitle.Items.Add(new ListItem(nameTitleMapOpera[key].ToString(), key));
            }
            ListItem selectedDefaultItem = ddlTitle.Items.FindByValue(AppConstants.TITLE_DEFAULT_VALUE);
            if (selectedDefaultItem != null)
            {
                ddlTitle.SelectedItem.Selected = false;
                selectedDefaultItem.Selected = true;
            }
            if (string.Equals(LanguageSelection.GetLanguageFromHost(), LanguageConstant.LANGUAGE_GERMAN, StringComparison.InvariantCultureIgnoreCase))
                dvNameTitle.Attributes.Add("style", "display:block");
            else
                dvNameTitle.Attributes.Add("style", "display:none");
        }

        /// <summary>
        /// Set the drop down list of the credit card types
        /// </summary>
        private void SetCreditCardDropDown()
        {
            OrderedDictionary creditCardTypesMap = DropDownService.GetCreditCardsCodes();
            if (creditCardTypesMap != null)
            {
                foreach (string key in creditCardTypesMap.Keys)
                {
                    ddlCardTypeCommon.Items.Add(new ListItem(creditCardTypesMap[key].ToString(), key));
                }
            }

            ListItem selectedDefaultItem = ddlCardTypeCommon.Items.FindByValue(AppConstants.DEFAULT_VALUE_CONST);
            if (selectedDefaultItem != null)
            {
                ddlCardTypeCommon.ClearSelection();
                selectedDefaultItem.Selected = true;
            }
        }

        private void SetExpiryDateList(DropDownList ddlMonth, DropDownList ddlYear)
        {
            ddlMonth.Items.Add(new ListItem(AppConstants.HYPHEN, AppConstants.DEFAULT_VALUE_CONST));
            //Populate the list of Months                        
            for (int monthCount = 1; monthCount <= AppConstants.TOTALMONTHS; monthCount++)
            {
                IFormatProvider cultureInfo =
                    new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
                string monthName = (new DateTime(1, monthCount, 1)).ToString("MMM", cultureInfo);
                ddlMonth.Items.Add(new ListItem(monthName, monthCount.ToString()));
            }
            int currentYear = DateTime.Today.Year;
            int currentMonth = DateTime.Today.Month;
            if (currentMonth == 12)
                currentYear = currentYear + 1;
            ddlYear.Items.Add(new ListItem(AppConstants.HYPHEN, AppConstants.DEFAULT_VALUE_CONST));
            for (int yearCount = 0; yearCount < AppConstants.TOTALYEARS; yearCount++)
            {
                ddlYear.Items.Add(new ListItem((currentYear + yearCount).ToString()));
            }
        }


        /// <summary>
        /// Populate the Date Of Birth drop downs 
        /// </summary>
        private void SetDateOfBirthDropDown()
        {
            string DD = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/day");

            ddlEnDOBDay.Items.Add(new ListItem(DD, "DFT"));
            for (int dayCount = 1; dayCount <= AppConstants.TOTALDAYS; dayCount++)
            {
                ddlEnDOBDay.Items.Add(new ListItem(dayCount.ToString()));
            }
            string Month = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/month");
            ddlEnDOBMonth.Items.Add(new ListItem(Month, "DFT"));
            for (int monthCount = 1; monthCount <= AppConstants.TOTALMONTHS; monthCount++)
            {
                IFormatProvider cultureInfo =
                    new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
                string monthName = (new DateTime(1, monthCount, 1)).ToString("MMMM", cultureInfo);
                ddlEnDOBMonth.Items.Add(new ListItem(monthName, monthCount.ToString()));
            }
            string YYYY = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/year");
            ddlEnDOBYear.Items.Add(new ListItem(YYYY, "DFT"));
            int startYear = AppConstants.TOTAL_DOB_YEAR_START;
            int currentyear = DateTime.Now.Year;
            int EndYear = currentyear - AppConstants.MINAGE;
            int noOfYear = EndYear - startYear;
            for (int yearCount = noOfYear; yearCount >= 0; yearCount--)
            {
                ddlEnDOBYear.Items.Add(new ListItem((startYear + yearCount).ToString()));
            }
        }

        /// <summary>
        /// Create reservations.
        /// </summary>
        /// <param name="guestList"></param>
        /// <param name="hotelRoomRate"></param>
        /// <param name="isSessionBooking"></param>
        /// <param name="isModifyBookingAfterNetsPayment"></param>
        /// <param name="paymentInfo"></param>
        private void CreateReservation(List<GuestInformationEntity> guestList, List<HotelRoomRateEntity> hotelRoomRate,
                                       bool isSessionBooking, bool isModifyBookingAfterNetsPayment, PaymentInfo paymentInfo)
        {
            string createdNameID = string.Empty;
            string sourceCode = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SourceCode");

            ReservationController reservationController = new ReservationController();

            SortedList<string, GuestInformationEntity> reservationMap = new SortedList<string, GuestInformationEntity>();

            string reservationNumber = string.Empty;
            int totalGuests = guestList.Count;
            SortedList<string, string> legReservationMap = null;
            for (int guestCount = 0; guestCount < totalGuests; guestCount++)
            {
                legReservationMap = null;
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if (search != null && search.ListRooms != null && search.ListRooms.Count > 0 &&
                    guestCount < search.ListRooms.Count && search.ListRooms[guestCount].IsRoomModifiable)
                {
                    HotelSearchRoomEntity roomSearch = search.ListRooms[guestCount];
                    if ((paymentInfo != null) && (guestList[guestCount].GuranteeInformation.GuranteeType == GuranteeType.PREPAIDINPROGRESS))
                    {
                        guestList[guestCount].GuranteeInformation.CreditCard =
                             new CreditCardEntity(paymentInfo.PanHash, paymentInfo.CardType, paymentInfo.CreditCardNumberMasked,
                                                  new ExpiryDateEntity(paymentInfo.ExpiryDate.Month, paymentInfo.ExpiryDate.Year));
                    }
                    if ((isModifyBookingAfterNetsPayment || BookingEngineSessionWrapper.IsModifyBooking) && (!string.IsNullOrEmpty(guestList[guestCount].LegNumber))
                        && ((roomSearch.IsRoomConfirmed == true) || isModifyBookingAfterNetsPayment))
                    {
                        guestList[guestCount].UserPreferences =
                            Utility.ConvertToUserPreferenceEntityList(guestList[guestCount].SpecialRequests);
                        if (isModifyBookingAfterNetsPayment && (guestList[guestCount].GuranteeInformation != null) &&
                           (guestList[guestCount].GuranteeInformation.GuranteeType == GuranteeType.PREPAIDINPROGRESS))
                        {
                            guestList[guestCount].GuranteeInformation.GuranteeType = GuranteeType.PREPAIDSUCCESS;
                            guestList[guestCount].GuranteeInformation.ChannelGuranteeCode = AppConstants.GUARANTEETYPE_PREAPIDSUCCESS;
                        }
                        legReservationMap = reservationController.ModifyBooking(search, roomSearch,
                                                                                hotelRoomRate[guestCount],
                                                                                guestList[guestCount],
                                                                                guestList[guestCount].ReservationNumber,
                                                                                sourceCode, out createdNameID,
                                                                                isSessionBooking);
                    }
                    else
                    {
                        legReservationMap = reservationController.CreateBooking(search, roomSearch,
                                                                                hotelRoomRate[guestCount],
                                                                                guestList[guestCount], reservationNumber,
                                                                                sourceCode, out createdNameID,
                                                                                isSessionBooking,
                                                                                HygieneSessionWrapper.PartnerID);
                    }
                }

                if (legReservationMap != null)
                {
                    foreach (string key in legReservationMap.Keys)
                    {
                        reservationNumber = legReservationMap[key];
                        guestList[guestCount].LegNumber = key;
                        guestList[guestCount].ReservationNumber = reservationNumber;
                        reservationMap[reservationNumber + AppConstants.HYPHEN + key] = guestList[guestCount];
                        break;
                    }
                }

                if (createdNameID != string.Empty)
                {
                    SearchCriteriaSessionWrapper.SearchCriteria.NameID = createdNameID;
                }

                if ((SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.BONUSCHEQUE) &&
                    (!guestList[guestCount].IsValidDNumber))
                {
                    SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = null;
                }
                if ((HygieneSessionWrapper.IsComboReservation) && (Reservation2SessionWrapper.IsModifyFlow) &&
                    (!search.ListRooms[guestCount].IsRoomModifiable))
                {
                    if ((GuestBookingInformationSessionWrapper.AllGuestsBookingInformations != null) && (guestList[guestCount] != null))
                    {
                        string legNumber = guestList[guestCount].LegNumber.ToString();
                        string resNumber = guestList[guestCount].ReservationNumber.ToString();
                        reservationMap[resNumber + AppConstants.HYPHEN + legNumber] =
                            GuestBookingInformationSessionWrapper.AllGuestsBookingInformations[resNumber + AppConstants.HYPHEN + legNumber];
                    }
                }
            }

            ReservationNumberSessionWrapper.ReservationNumber = reservationNumber;

            GuestBookingInformationSessionWrapper.AllGuestsBookingInformations = reservationMap;
        }

        /// <summary>
        /// Set the Preferred Language
        /// </summary>
        /// <param name="guestInfoEntity">
        /// GuestInformationEntity
        /// </param>
        private void SetPreferredLanguage(GuestInformationEntity guestInfoEntity)
        {
            if ((UserLoggedInSessionWrapper.UserLoggedIn) && (GuestBookingInformationSessionWrapper.FetchedGuestInformation != null))
            {
                if (!BookingEngineSessionWrapper.IsModifyBooking)
                {
                    guestInfoEntity.PreferredLanguage = GuestBookingInformationSessionWrapper.FetchedGuestInformation.PreferredLanguage;
                }
                else
                {
                    LoyaltyDetailsEntity loyaltyEntity = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
                    if (loyaltyEntity != null)
                    {
                        if (GuestBookingInformationSessionWrapper.FetchedGuestInformation.NameID == loyaltyEntity.NameID)
                        {
                            guestInfoEntity.PreferredLanguage = loyaltyEntity.PreferredLanguage;
                        }
                        else if ((BookingEngineSessionWrapper.BookingDetails != null) &&
                                 (BookingEngineSessionWrapper.BookingDetails.GuestInformation != null))
                        {
                            guestInfoEntity.PreferredLanguage =
                                BookingEngineSessionWrapper.BookingDetails.GuestInformation.PreferredLanguage;
                        }
                    }
                }
            }
            else if ((BookingEngineSessionWrapper.IsModifyBooking) && (BookingEngineSessionWrapper.BookingDetails != null)
                     && (BookingEngineSessionWrapper.BookingDetails.GuestInformation != null))
            {
                guestInfoEntity.PreferredLanguage = BookingEngineSessionWrapper.BookingDetails.GuestInformation.PreferredLanguage;
            }
        }



        /// <summary>
        /// Update User Details
        /// </summary>
        /// <param name="guestInfoEntity">
        /// GuestInformation Entity
        /// </param>
        private void UpdateUserDetails(GuestInformationEntity guestInfoEntity)
        {
            if ((LoyaltyDetailsSessionWrapper.LoyaltyDetails != null) && (guestInfoEntity != null))
            {
                string nameId = LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID;
                if (!string.IsNullOrEmpty(nameId))
                {
                    GuestInformationEntity cloneGuestInfoEntity = guestInfoEntity.Clone();

                    Utility.CompareGuestInformation(cloneGuestInfoEntity);

                    UserProfileEntity userProfileEntity =
                    Utility.ConvertToUserProfileEntity(cloneGuestInfoEntity);
                    if (userProfileEntity != null)
                    {
                        NameController nameController = new NameController();
                        nameController.PrimaryLangaueID = userProfileEntity.PreferredLanguage;
                        nameController.UpdateUserDetails(nameId, UserProfileInformationSessionWrapper.UserProfileInformation, userProfileEntity, null);
                    }
                }
            }
        }


        /// <summary>
        /// Get the HotelRoomRateEntity for the specified roomTypeCode
        /// </summary>
        /// <param name="roomTypeCode">RoomType Code</param>
        /// <param name="selectedRoomIndex">Index of the selected room.</param>
        /// <returns>HotelRoomRateEntity</returns>
        private HotelRoomRateEntity GetHotelRoomRate(string roomTypeCode, int selectedRoomIndex,
                                                     List<GuestInformationEntity> guestList)
        {
            HotelRoomRateEntity hotelRoomRate = null;
            if ((BookingEngineSessionWrapper.IsModifyBooking) && (BookingEngineSessionWrapper.DirectlyModifyContactDetails))
            {
                if (BookingEngineSessionWrapper.BookingDetails != null)
                {
                    BookingDetailsEntity bookDetEntity = null;
                    string LegNumberOfRes = guestList[selectedRoomIndex].LegNumber;
                    bookDetEntity = BookingEngineSessionWrapper.GetLegBookingDetails(LegNumberOfRes,
                                                                        BookingEngineSessionWrapper.BookingDetails.ReservationNumber);
                    hotelRoomRate = bookDetEntity != null ? bookDetEntity.HotelRoomRate : null;
                }
            }
            else if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null)
            {
                Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                SelectedRoomAndRateEntity selectedRoomAndRateEntity = null;
                if (selectedRoomAndRatesHashTable.ContainsKey(selectedRoomIndex))
                {
                    selectedRoomAndRateEntity =
                        selectedRoomAndRatesHashTable[selectedRoomIndex] as SelectedRoomAndRateEntity;
                }

                if (selectedRoomAndRateEntity != null)
                {
                    List<RoomRateEntity> selectedRoomRates = selectedRoomAndRateEntity.RoomRates;
                    hotelRoomRate = new HotelRoomRateEntity();
                    foreach (RoomRateEntity roomRate in selectedRoomRates)
                    {
                        if (roomRate.RoomTypeCode == roomTypeCode)
                        {
                            hotelRoomRate.RatePlanCode = roomRate.RatePlanCode;
                            hotelRoomRate.IsSpecialRate = roomRate.IsSpecialRate;
                            hotelRoomRate.RoomtypeCode = roomRate.RoomTypeCode;
                            hotelRoomRate.Rate = roomRate.BaseRate;
                            hotelRoomRate.TotalRate = roomRate.TotalRate;
                            if (null != roomRate.PointsDetails)
                            {
                                hotelRoomRate.AwardType = roomRate.PointsDetails.AwardType;
                                hotelRoomRate.Points = roomRate.PointsDetails.PointsRequired;
                            }
                            break;
                        }
                    }
                }
            }
            return hotelRoomRate;
        }


        /// <summary>
        /// Generic Function to Displays the guarantee information based on the rate category.
        /// </summary>
        /// <param name="rateCategoryID">Rate category id</param>
        /// <param name="campaignCode">Campaign code</param>
        /// <remarks>CR 1 | Release 1.5.3 | Accomodation of Rate block</remarks>
        private void DisplayHoldOptionIfRequired(string rateCategoryID, bool paymentFallback)
        {
            HideUnhideCreditCardDivCommon.Attributes.Add("class", "ccInfoDetails fltLft" + " Room" + 4);
            // rdoHoldRoomCommon.Attributes.Add("rel", "Room" + 4);
            rdoLateArrivalGuranteeCommon.Attributes.Add("rel", "Room" + 4);

            string campaignCode = string.Empty;
            if ((null != SearchCriteriaSessionWrapper.SearchCriteria) &&
                (!string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode)))
            {
                campaignCode = SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode;
            }

            bool noRateCodeButDisplayGuarantee;
            string creditCardGuranteeType;
            if (IsTruelyPrepaid(rateCategoryID, campaignCode, out noRateCodeButDisplayGuarantee,
                                out creditCardGuranteeType))
            {
                HideUnhideGuranteeDivCommon.Visible = false;
                panHashCreditCardDivCommon.Visible = false;
            }
            else
            {
                DisplayGuranteeDiv(rateCategoryID, noRateCodeButDisplayGuarantee, creditCardGuranteeType, paymentFallback);
            }
            if (RoomRateUtil.IsSaveRateCategory(rateCategoryID) && !paymentFallback)
            {
                HideUnhideCreditCardDivCommon.Visible = false;
                panHashCreditCardDivCommon.Visible = false;
            }
        }


        /// <summary>
        /// This will display the GuranteeDiv.Which will contain the fields for the Gurantee infromations.
        /// </summary>
        /// <param name="rateCategoryID">RateCategoryId</param>
        /// <param name="noRateCodeButDisplayGuarantee">Wheather to display gurantee information to the user.</param>
        /// <param name="creditCardGuranteeType">Type of the credit card gurantee.</param>
        /// <remarks>Last modified for CR 1 | Release 1.5.3 | Accomodation of Rate block</remarks>
        private void DisplayGuranteeDiv(string rateCategoryID, bool noRateCodeButDisplayGuarantee,
                                        string creditCardGuranteeType, bool paymentFallback)
        {
            RateCategory rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(rateCategoryID);
            Block block = null;
            if (null == rateCategory)
            {
                block = ContentDataAccess.GetBlockCodePages(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
            }
            if (((null != block) && (block.SixPMHoldAvailable)) ||
                ((null != rateCategory) && (rateCategory.HoldGuranteeAvailable)))
            {
                blkHoldRoomCommon.Visible = true;
            }
            else
            {
                lblGuranteewithRadioCommon.Visible = false;
                lblGuranteewithoutRadioCommon.Visible = true;
                lblGuranteewithRadioCCDivCommonInfo.Visible = false;
                txtCardHolderCommon.Attributes.Add("class", "text-input input defaultColor");
                txtCardNumberCommon.Attributes.Add("class", "text-input input defaultColor");
                ddlCardTypeCommon.Attributes.Add("class", "text-input input defaultColor");
            }
            if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType.Equals(SearchType.REDEMPTION))
            {
                blkHoldRoomCommon.Visible = false;
                HideUnhideCreditCardDivCommon.Visible = false;
                panHashCreditCardDivCommon.Visible = false;
                lblGuranteewithRadioCommon.Visible = false;
                lblGuranteewithoutRadioCommon.Visible = true;
                lblGuranteewithRadioCCDivCommonInfo.Visible = false;
                string redemptionGuranteeText = (null != CurrentPage["RewardNights"])
                                                    ? CurrentPage["RewardNights"].ToString()
                                                    : string.Empty;
                lblGuranteewithoutRadioCommon.InnerHtml = redemptionGuranteeText;
            }
            else
            {
                DisplayGuranteeText(rateCategory, noRateCodeButDisplayGuarantee, creditCardGuranteeType, paymentFallback);
            }
        }


        /// <summary>
        /// Generic Function to Display the Text depending upon the Gurantee Type
        /// </summary>
        /// <param name="rateCategory">RateCategory</param>
        /// <param name="noRateCodeButDisplayGuarantee">Is to display the guarantee information to the user or not</param>
        /// <param name="creditCardGuranteeType">CreditCard GuranteeType string</param>
        /// <remarks>Last modified for CR 1 | Release 1.5.3 | Accomodation of Rate block</remarks>
        private void DisplayGuranteeText(RateCategory rateCategory, bool noRateCodeButDisplayGuarantee,
                                         string creditCardGuranteeType, bool paymentFallback)
        {
            string[] prepaidGuranteeTypes = AppConstants.GUARANTEE_TYPE_PRE_PAID;
            string spanHoldRoomCommonText = (CurrentPage["GTDCC1800"] != null)
                                                ? CurrentPage["GTDCC1800"].ToString()
                                                : string.Empty;
            spanHoldRoomCommonText = spanHoldRoomCommonText.Replace("<DIV>", "");
            spanHoldRoomCommonText = spanHoldRoomCommonText.Replace("</DIV>", "");
            spanHoldRoomCommon.InnerHtml = spanHoldRoomCommonText;

            System.Web.UI.HtmlControls.HtmlGenericControl displayTextLabel = new HtmlGenericControl();
            if (lblGuranteewithRadioCommon.Visible == true)
            {
                displayTextLabel = txtGuranteewithRadioCommon;
            }
            else if (lblGuranteewithoutRadioCommon.Visible == true)
            {
                displayTextLabel = txtGuranteeWithoutRadioCommon;
            }

            if (!noRateCodeButDisplayGuarantee)
            {
                creditCardGuranteeType = rateCategory.CreditCardGuranteeType;

                if (!paymentFallback && (prepaidGuranteeTypes != null) && prepaidGuranteeTypes.Contains(rateCategory.CreditCardGuranteeType))
                {
                    creditCardGuranteeType = string.Format("{0}-Prepaid", rateCategory.CreditCardGuranteeType);
                }
            }
            string creditcardInfo = (CurrentPage[creditCardGuranteeType] != null)
                                        ? CurrentPage[creditCardGuranteeType].ToString()
                                        : string.Empty;

            creditcardInfo = creditcardInfo.Replace("<DIV>", "");
            creditcardInfo = creditcardInfo.Replace("</DIV>", "");
            displayTextLabel.InnerHtml = creditcardInfo;
        }


        /// <summary>
        /// Get the Gurantee Information of the guest
        /// </summary>
        /// <returns>
        /// Gurantee Information
        /// </returns>
        private GuranteeInformationEntity CaptureGuranteeInformation(string rateCategoryId, string code,
                                                                     ref GuranteeInformationEntity guranteeInfoEntity,
                                                                     RoomInformationContainer roomInformation,
                                                                     bool isCommonGurantee,
                                                                     string hotelOperaID)
        {
            RateCategory rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(rateCategoryId);
            guranteeInfoEntity = new GuranteeInformationEntity();

            HtmlInputCheckBox rdoLateArrivalGurantee;
            HtmlInputRadioButton rdoHoldRoom;
            HtmlInputText textCardNumber = null;
            DropDownList ddlExpiryMonth = null;
            DropDownList ddlExpiryYear = null;
            DropDownList ddlCardType = null;
            HtmlInputText txtCardHolder = null;
            HtmlInputHidden txtPreviousCardNumber;
            DropDownList ddlPanHashCreditCards;

            bool paymentFallback = m_ContentDataAccessManager.GetPaymentFallback(hotelOperaID);

            if (RoomRateUtil.IsSaveRateCategory(rateCategoryId) && !paymentFallback)
            {
                guranteeInfoEntity.GuranteeType = GuranteeType.PREPAIDINPROGRESS;
                guranteeInfoEntity.ChannelGuranteeCode = AppConstants.GUARANTEETYPE_PREAPIDINPROGRESS;
                roomInformation.DisplayCCGuranteeDivOption = false;
            }
            else
            {

                if (isCommonGurantee)
                {                    
                    rdoLateArrivalGurantee = rdoLateArrivalGuranteeCommon;
                    // rdoHoldRoom = rdoHoldRoomCommon;

                    if (!HygieneSessionWrapper.IsPANHashCreditCardsAvailable)
                    {
                        textCardNumber = txtCardNumberCommon;
                        if (txtCardNumberCommon.Value.Contains(AppConstants.CARD_MASK))
                        {
                        }
                        else
                            textCardNumber.Value = (txtCardNumberCommon.Value).Trim();

                        ddlExpiryMonth = ddlExpiryMonthCommon;
                        ddlExpiryYear = ddlExpiryYearCommon;
                        ddlCardType = ddlCardTypeCommon;

                        txtCardHolder = txtCardHolderCommon;
                        txtCardHolderCommon.Attributes["rel"] =
                            WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/NameOnCard");
                        txtCardNumberCommon.Attributes["rel"] =
                            WebUtil.GetTranslatedText(
                                "/bookingengine/booking/bookingdetail/guaranteepolicytext/NumberOnCard");
                    }

                    ddlPanHashCreditCards = ddlPanHashCreditCardsCommon;
                }
                else
                {
                    rdoLateArrivalGurantee = roomInformation.FindControl("rdoLateArrivalGurantee") as HtmlInputCheckBox;
                    rdoHoldRoom = roomInformation.FindControl("rdoHoldRoom") as HtmlInputRadioButton;

                    ddlPanHashCreditCards = roomInformation.FindControl("ddlPanHashCreditCards") as DropDownList;

                    if (!HygieneSessionWrapper.IsPANHashCreditCardsAvailable ||
                       ((ddlPanHashCreditCards != null) && (ddlPanHashCreditCards.Items.Count <= 0)))
                    {
                        textCardNumber = roomInformation.FindControl("txtCardNumber") as HtmlInputText;

                        if (textCardNumber.Value.Contains(AppConstants.CARD_MASK))
                        {
                        }
                        else
                            textCardNumber.Value = (textCardNumber.Value).Trim();


                        ddlExpiryMonth = roomInformation.FindControl("ddlExpiryMonth") as DropDownList;
                        ddlExpiryYear = roomInformation.FindControl("ddlExpiryYear") as DropDownList;
                        ddlCardType = roomInformation.FindControl("ddlCardType") as DropDownList;
                        txtCardHolder = roomInformation.FindControl("txtCardHolder") as HtmlInputText;

                        txtCardHolder.Attributes["rel"] =
                            WebUtil.GetTranslatedText(
                                "/bookingengine/booking/bookingdetail/guaranteepolicytext/NameOnCard");
                        textCardNumber.Attributes["rel"] =
                            WebUtil.GetTranslatedText(
                                "/bookingengine/booking/bookingdetail/guaranteepolicytext/NumberOnCard");
                    }
                }

                bool noRateCodeButDisplayGuarantee;
                string creditCardGuranteeType;
                if (IsTruelyPrepaid(rateCategoryId, code, out noRateCodeButDisplayGuarantee, out creditCardGuranteeType))
                {
                    Block block = ContentDataAccess.GetBlockCodePages(code);

                    if ((null != block) && (!string.IsNullOrEmpty(block.GuranteeType)))
                    {
                        guranteeInfoEntity.GuranteeType = GuranteeType.PREPAID;
                        guranteeInfoEntity.ChannelGuranteeCode = block.GuranteeType;
                    }
                }
                else if (((null != rdoLateArrivalGurantee) &&
                          (!(rdoLateArrivalGurantee.Visible) || (rdoLateArrivalGurantee.Checked)))
                         || (HygieneSessionWrapper.Is6PMBooking))
                {
                    if ((null != rateCategory) &&
                        (rateCategory.RateCategoryId.ToUpper().Equals(SearchType.REDEMPTION.ToString())))
                    {
                        guranteeInfoEntity.GuranteeType = GuranteeType.REDEMPTION;
                        guranteeInfoEntity.ChannelGuranteeCode = rateCategory.CreditCardGuranteeType;
                    }
                    else
                    {
                        string cardNumber = string.Empty;
                        if (!HygieneSessionWrapper.IsPANHashCreditCardsAvailable ||
                           ((ddlPanHashCreditCards != null) && (ddlPanHashCreditCards.Items.Count <= 0)))
                        {
                            if (textCardNumber.Value.Contains(AppConstants.CARD_MASK))
                            {
                                txtPreviousCardNumber = roomInformation.FindControl("previousCardNumber") as HtmlInputHidden;
                                if (textCardNumber.Value != null && textCardNumber.Value.Length >= 4 &&
                                    txtPreviousCardNumber.Value != null && txtPreviousCardNumber.Value.Length >= 4)
                                {
                                    if (textCardNumber.Value.Substring(textCardNumber.Value.Length - 4) ==
                                        txtPreviousCardNumber.Value.Substring(txtPreviousCardNumber.Value.Length - 4))
                                        cardNumber = (txtPreviousCardNumber.Value).Trim();
                                    else
                                        cardNumber = (textCardNumber.Value).Trim();
                                }
                                else
                                    cardNumber = (textCardNumber.Value).Trim();
                            }
                            else
                                cardNumber = (textCardNumber.Value).Trim();
                        }


                        guranteeInfoEntity.GuranteeType = GuranteeType.CREDITCARD;

                        if (noRateCodeButDisplayGuarantee)
                        {
                            guranteeInfoEntity.ChannelGuranteeCode = creditCardGuranteeType;
                        }
                        else
                        {
                            if (rateCategory != null)
                                guranteeInfoEntity.ChannelGuranteeCode = rateCategory.CreditCardGuranteeType;
                        }

                        if (!HygieneSessionWrapper.IsPANHashCreditCardsAvailable ||
                           ((ddlPanHashCreditCards != null) && (ddlPanHashCreditCards.Items.Count <= 0)))
                        {
                            if ((null != ddlExpiryMonth &&
                                 ddlExpiryMonth.SelectedItem.Value != AppConstants.DEFAULT_VALUE_CONST) &&
                                (null != ddlExpiryYear &&
                                 ddlExpiryMonth.SelectedItem.Value != AppConstants.DEFAULT_VALUE_CONST) &&
                                (null != ddlCardType &&
                                 ddlExpiryMonth.SelectedItem.Value != AppConstants.DEFAULT_VALUE_CONST) &&
                                (null != textCardNumber))
                            {
                                ExpiryDateEntity expiryDateEntity =
                                    new ExpiryDateEntity(Int32.Parse(ddlExpiryMonth.Text), Int32.Parse(ddlExpiryYear.Text));
                                guranteeInfoEntity.CreditCard =
                                    new CreditCardEntity(txtCardHolder.Value, ddlCardType.SelectedItem.Value, cardNumber,
                                                         expiryDateEntity);
                            }
                        }
                        else
                        {
                            if (ddlPanHashCreditCards != null && !string.IsNullOrEmpty(ddlPanHashCreditCards.SelectedValue))
                            {
                                LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
                                guranteeInfoEntity.CreditCard = loyaltyDomain.FetchCreditCardDetailsByPanHash(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID, ddlPanHashCreditCards.SelectedValue);
                            }
                        }

                        HygieneSessionWrapper.Is6PMBooking = false;
                    }
                }

                else if ((null != rdoLateArrivalGuranteeCommon) && (rdoLateArrivalGuranteeCommon.Checked))
                {
                    guranteeInfoEntity.GuranteeType = GuranteeType.HOLD;
                    guranteeInfoEntity.ChannelGuranteeCode = AppConstants.GURANTEE_TYPE_HOLD;
                    roomInformation.DisplayCCGuranteeDivOption = false;
                }
                else if (rdoLateArrivalGurantee != null && rdoLateArrivalGurantee.Visible && rdoLateArrivalGurantee.Checked != true)
                {
                    guranteeInfoEntity.GuranteeType = GuranteeType.HOLD;
                    guranteeInfoEntity.ChannelGuranteeCode = AppConstants.GURANTEE_TYPE_HOLD;
                }


                if (HygieneSessionWrapper.IsPANHashCreditCardsAvailable && guranteeInfoEntity.CreditCard != null && !paymentFallback)
                    SelectedCardTypeSessionWrapper.SelectedCardType = guranteeInfoEntity.CreditCard.CardType;
                else if (null != ddlCardType)
                    SelectedCardTypeSessionWrapper.SelectedCardType = ddlCardType.SelectedItem.Text;

            }            
            return guranteeInfoEntity;
        }

        /// <summary>
        /// Displays the pan hash credit cards stored in the user profile
        /// </summary>
        private void PopulatePanHashCreditCards(DropDownList ddlPanHashCreditCards)
        {
            Dictionary<string, CreditCardEntity> creditCards = null;

            ListItem ccItem = new ListItem(WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/ChooseCC"), string.Empty);
            ddlPanHashCreditCards.Items.Add(ccItem);

            if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null)
            {
                var loyaltyDomain = new LoyaltyDomain();
                creditCards = loyaltyDomain.FetchCreditCardDetailsUserProfile(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID, true);

                if (ddlPanHashCreditCards != null)
                {
                    foreach (string cardKey in creditCards.Keys)
                    {
                        string creditCardNo = string.Concat(WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/UserCreditCard"),
                                                                AppConstants.SPACE, WebUtil.GetScanwebCCNoForDisplay(creditCards[cardKey].CardType, creditCards[cardKey].CardNumber));

                        //set pan hash as value. NameOnCard used as pan hash
                        ccItem = new ListItem(creditCardNo, creditCards[cardKey].NameOnCard);
                        ddlPanHashCreditCards.Items.Add(ccItem);
                    }
                }
            }

            HygieneSessionWrapper.IsPANHashCreditCardsAvailable = creditCards != null && creditCards.Count > 0;
        }

        #endregion Gaurentee Information Divs


        #region gerenarting html preview of Confirmation page

        /// <summary>
        /// Generates the confirmation page HTML string.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="htmlString">The HTML string.</param>
        private void GenerateConfirmationPageHtmlString(string url, ref string htmlString)
        {
            #region Very IMP code To render the preview page

            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:GenerateConfirmationPageHtmlString() Begin");
            StringWriter sw = new StringWriter();
            try
            {
                Server.Execute(url, sw);
                htmlString = (sw.ToString()).Trim().Replace("\r\n", "").Replace("\"", "'");
            }
            catch (Exception ex)
            {
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:GenerateConfirmationPageHtmlString() End");

            #endregion
        }

        #endregion

        private void SendConfirmationToGuest()
        {
            try
            {
                Dictionary<string, string> emailBookingMap = new Dictionary<string, string>();

                emailBookingMap = Utility.CollectGenericHotelEmailDetails();
                SortedList<string, GuestInformationEntity> guestsList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;

                Hashtable htbl = new Hashtable();

                if (guestsList != null)
                {
                    emailBookingMap[CommunicationTemplateConstants.LOGOPATH] = AppConstants.LOGOPATH;
                    Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                    int totalGuests = guestsList.Count;
                    bool isSMSOptionSelected = false;
                    int guestIterator = 0;
                    foreach (string key in guestsList.Keys)
                    {
                        int roomNumber = Convert.ToInt32(key);
                        if (selectedRoomAndRatesHashTable != null && selectedRoomAndRatesHashTable.Count > 0)
                        {
                            SelectedRoomAndRateEntity selectedRoomAndRates = selectedRoomAndRatesHashTable[guestIterator] as SelectedRoomAndRateEntity;
                            CollectBookingConfirmationEmailDetails(ref emailBookingMap, selectedRoomAndRates, guestsList[key], guestIterator);
                            if (!htbl.ContainsKey(selectedRoomAndRates.RateCategoryID + AppConstants.Room + roomNumber))
                            {
                                htbl.Add(selectedRoomAndRates.RateCategoryID + AppConstants.Room + roomNumber,
                                         emailBookingMap[CommunicationTemplateConstants.POLICY_TEXT]);
                            }
                            else
                            {
                                if (!htbl.ContainsValue(emailBookingMap[CommunicationTemplateConstants.POLICY_TEXT]))
                                {
                                    htbl.Add(selectedRoomAndRates.RateCategoryID + AppConstants.Room + roomNumber,
                                             emailBookingMap[CommunicationTemplateConstants.POLICY_TEXT]);
                                }
                            }
                        }
                        string resNumber = string.Empty;
                        if (Convert.ToInt32(key) > 1)
                            resNumber = guestsList[key].ReservationNumber + "-" + key;
                        else
                            resNumber = guestsList[key].ReservationNumber;

                        CollectGuestsSpecificEmailDetails(ref emailBookingMap, guestsList[key], resNumber);
                        ConfirmationHeaderSessionWrapper.ConfirmationHeader = emailBookingMap[CommunicationTemplateConstants.HTMLPOLICYHEADER];
                        BookingEngineSessionWrapper.IsSmsRequired = false;
                        ErrorsSessionWrapper.FailedSMSRecepient = null;
                        if ((guestsList[key].IsSMSChecked) &&
                            (SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[guestIterator].IsRoomModifiable))
                        {
                            isSMSOptionSelected = true;
                            Utility.SendSMSConfirmationToGuest(ref emailBookingMap, key, resNumber);
                        }
                        guestIterator++;
                    }
                    BookingEngineSessionWrapper.IsSmsRequired = isSMSOptionSelected;
                }
                HygieneSessionWrapper.PolicyTextConfirmation = htbl;
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Failed to send booking confirmation in SendConfirmationToGuest() to the Guest");
            }
        }

        /// <summary>
        /// Sends Confirmation to guests.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="htmlString">The HTML string.</param>
        private void SendMailConfirmationToGuestsUsingHTMLString()
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("Start callBack method");
            try
            {
                SortedList<int, string> emailBookingMap = new SortedList<int, string>();
                HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
                ErrorsSessionWrapper.ClearFailedEmailRecipient();
                string templateFileName = string.Empty;
                string bookingType = PageIdentifier.BookingPageIdentifier;
                if (BookingEngineSessionWrapper.IsModifyBooking)
                    bookingType = PageIdentifier.ModifyBookingPageIdentifier;

                if (hotelSearch != null && hotelSearch.ListRooms.Count > 0)
                {
                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:GenerateHTMLString for Email and PDF:Start time");
                    for (int roomIterator = 0; roomIterator < hotelSearch.ListRooms.Count; roomIterator++)
                    {
                        string htmlStringForEmail = string.Empty;
                        string htmlStringForPDF = string.Empty;
                        string hostName = string.Empty;
                        string alterhtmlStringForPDF = string.Empty;
                        string creditCardReceiptHtmlString = string.Empty;

                        if (roomIterator == 0)
                        {
                            GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/ConfirmationEmailPage.aspx?AllROOMSTOBESHOWN=true&BOOKINGTYPE=" +
                                bookingType, ref htmlStringForEmail);

                            GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx?command=print&isPDF=true",
                                ref htmlStringForPDF);

                            if (string.IsNullOrEmpty(htmlStringForPDF))
                                GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx?command=print&isPDF=true",
                                    ref htmlStringForPDF);

                            //send receipt email only for the first person
                            if (WebUtil.IsAnyRoomHavingSaveCategory(HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable) && roomIterator == 0
                                && !String.IsNullOrEmpty(Reservation2SessionWrapper.PaymentTransactionId))
                                GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/Booking/Receipt.aspx?command=print&isPDF=true",
                                    ref creditCardReceiptHtmlString);

                            templateFileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
                            templateFileName = templateFileName.Remove(templateFileName.IndexOf("\\bin"));
                            alterhtmlStringForPDF = htmlStringForPDF.Replace("/Templates", templateFileName + "/Templates");
                            creditCardReceiptHtmlString = creditCardReceiptHtmlString.Replace("/Templates", templateFileName + "/Templates");
                            emailBookingMap.Add(roomIterator, htmlStringForEmail + "^" + alterhtmlStringForPDF + "^" + creditCardReceiptHtmlString);
                        }
                        else
                        {
                            if (
                                (!(Reservation2SessionWrapper.IsModifyFlow)) ||
                                ((Reservation2SessionWrapper.IsModifyFlow) && !(HygieneSessionWrapper.IsComboReservation)) ||
                                ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation) &&
                                 (hotelSearch.ListRooms[roomIterator].IsRoomModifiable))
                                )
                            {
                                GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/ConfirmationEmailPage.aspx?AllROOMSTOBESHOWN=false&ROOMNUMBERTOBEDISPLAYED=" +
                                    roomIterator + "&BOOKINGTYPE=" + bookingType, ref htmlStringForEmail);
                                GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx?AllROOMSTOBESHOWN=false&isPDF=true&command=print&roomNumber=" +
                                    roomIterator, ref htmlStringForPDF);
                                if (string.IsNullOrEmpty(htmlStringForPDF))
                                    GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx?isPDF=true&command=print&roomNumber=" +
                                        roomIterator, ref htmlStringForPDF);

                                //send receipt email only for the first person
                                if (WebUtil.IsAnyRoomHavingSaveCategory(HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable) && roomIterator == 0)
                                    GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/Booking/Receipt.aspx?command=print&isPDF=true",
                                        ref creditCardReceiptHtmlString);

                                templateFileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
                                templateFileName = templateFileName.Remove(templateFileName.IndexOf("\\bin"));
                                alterhtmlStringForPDF = htmlStringForPDF.Replace("/Templates", templateFileName + "/Templates");
                                creditCardReceiptHtmlString = creditCardReceiptHtmlString.Replace("/Templates", templateFileName + "/Templates");
                                emailBookingMap.Add(roomIterator, htmlStringForEmail + "^" + alterhtmlStringForPDF + "^" + creditCardReceiptHtmlString);
                            }
                        }
                    }
                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:GenerateHTMLString for Email and PDF:End time");
                }

                SortedList<string, GuestInformationEntity> guestsList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
                if (
                    (guestsList != null) &&
                    (guestsList.Count > 0) &&
                    (hotelSearch != null) &&
                    (hotelSearch.ListRooms != null) &&
                    (hotelSearch.ListRooms.Count > 0) &&
                    (guestsList.Count == hotelSearch.ListRooms.Count)
                    )
                {
                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:Send EMail and create PDF:Start time");
                    int counter = 0;
                    List<EmailEntity> emails = new List<EmailEntity>();
                    List<string> reservations = new List<string>();
                    foreach (string key in guestsList.Keys)
                    {
                        if (
                            (counter == 0) ||
                            !(Reservation2SessionWrapper.IsModifyFlow) ||
                            ((Reservation2SessionWrapper.IsModifyFlow) && !(HygieneSessionWrapper.IsComboReservation)) ||
                            ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation) &&
                             (hotelSearch.ListRooms[counter].IsRoomModifiable))
                            )
                        {
                            string emailText = string.Empty;
                            string[] emailTextAndPDF = null;
                            GuestInformationEntity currentGuestInformationEntity =
                                guestsList[key] as GuestInformationEntity;
                            if (currentGuestInformationEntity != null)
                            {
                                emailText = ((emailBookingMap[counter] != null)
                                                 ? emailBookingMap[counter]
                                                 : string.Empty);
                                if (!string.IsNullOrEmpty(emailText))
                                    emailTextAndPDF = emailText.Split('^');
                                EmailEntity emailEntity = new EmailEntity();
                                if (emailTextAndPDF != null)
                                {
                                    if (emailTextAndPDF[0] != null)
                                        emailEntity.Body = emailTextAndPDF[0];
                                    if (emailTextAndPDF[1] != null)
                                        emailEntity.TextEmailBody = emailTextAndPDF[1];

                                    if (emailTextAndPDF.Length > 2)
                                        if (!string.IsNullOrEmpty(emailTextAndPDF[2]))
                                            emailEntity.CreditCardReceiptHtmlText = emailTextAndPDF[2];
                                }
                                if (currentGuestInformationEntity.IsRewardNightGift &&
                                    LoyaltyDetailsSessionWrapper.LoyaltyDetails != null &&
                                    !string.IsNullOrEmpty(LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email))
                                {
                                    emailEntity.Recipient = new string[]
                                                                {
                                                                    currentGuestInformationEntity.EmailDetails.EmailID,
                                                                    LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email
                                                                };
                                }
                                else
                                {
                                    emailEntity.Recipient = new string[] { currentGuestInformationEntity.EmailDetails.EmailID };
                                }


                                emailEntity.Subject = WebUtil.GetTranslatedText("/bookingengine/booking/confirmation/subject");
                                emailEntity.Sender = WebUtil.GetTranslatedText("/bookingengine/booking/confirmation/fromEmail");

                                string keyValue = key;
                                keyValue = keyValue.Replace(currentGuestInformationEntity.ReservationNumber + "-", string.Empty);
                                string reservationNumber = currentGuestInformationEntity.ReservationNumber + "-" + keyValue;
                                emailEntity.ReceiptReservationNumber = string.Concat(currentGuestInformationEntity.ReservationNumber, "-",
                                                WebUtil.GetTranslatedText("/bookingengine/booking/BookingReceipt/BookingTypeSave"));
                                emails.Add(emailEntity);
                                reservations.Add(reservationNumber);
                            }
                        }
                        counter++;
                    }
                    SendMail(emails, reservations);
                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage("UserContactBookingDetails:Send EMail and create PDF:End time");
                }
            }
            catch (Exception ex)
            {
                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("exception in send email:" + ex.InnerException);
            }
        }

        /// <summary>
        /// This method will create PDF files asynchronously.
        /// </summary>
        /// <param name="emails">emails list</param>
        /// <param name="reservations">reservations list</param>
        public void SendMail(List<EmailEntity> emails, List<string> reservations)
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendMail() Start Time");
            CreatePDF createPDFDeligate = new CreatePDF(SendEmailAsync);
            IAsyncResult result = createPDFDeligate.BeginInvoke(emails, reservations, null, null);
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendMail() End Time");
        }

        /// <summary>
        /// Asynchronous create pdf method for Deligate.
        /// </summary>
        /// <param name="emails">emails list</param>
        /// <param name="reservations">reservations List</param>
        /// <returns></returns>
        public void SendEmailAsync(List<EmailEntity> emails, List<string> reservations)
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendEmailAsync(): Excuted sucessfully:Start Time");
            int counter = 0;
            foreach (EmailEntity email in emails)
            {
                Utility.SendEmailConfirmationToGuest(email, reservations[counter]);
                counter++;
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendEmailAsync(): Excuted sucessfully: End Time");
        }


        /// <summary>
        /// Get the Country PhoneCode Map
        /// </summary>
        /// <returns>
        /// OrdererdDictionary of Country-Phone Code map
        /// </returns>
        protected OrderedDictionary GetCountryPhoneCodeMap()
        {
            OrderedDictionary countryPhoneCodesMap = DropDownService.GetCountryPhoneCodeMap();
            return countryPhoneCodesMap;
        }

        /// <summary>
        /// If the user loggged in Subscribe news letter should not be visible
        /// </summary>
        private void DisplayNewsletterOption()
        {
            if (UserLoggedInSessionWrapper.UserLoggedIn)
            {
                //blkNewsLetter.Visible = false;
                //chkSubscribeNewsletter.Visible = false;
            }
            else
            {
                //blkNewsLetter.Visible = true;
                //chkSubscribeNewsletter.Visible = true;
            }
        }


        /// <summary>
        /// Collect the Booking Confirmation Informations to be send as Email
        /// </summary>
        /// <returns>
        /// OrderedDictionary
        /// </returns>
        private Dictionary<string, string> CollectBookingConfirmationEmailDetails(
            ref Dictionary<string, string> bookingMap, SelectedRoomAndRateEntity selectedRoomAndRateEntity,
            GuestInformationEntity guestInfoEntity, int guestIterator)
        {
            GuranteeType guaranteePolicyType = guestInfoEntity.GuranteeInformation != null ? guestInfoEntity.GuranteeInformation.GuranteeType : GuranteeType.HOLD;
            Utility.CollectHotelRoomEmailDetails(ref bookingMap, guestIterator);
            CollectGenericGuestEmailDetails(ref bookingMap, guestInfoEntity);
            CollectDynamicTextFromCMS(ref bookingMap, selectedRoomAndRateEntity, guestIterator, guaranteePolicyType);
            return bookingMap;
        }

        /// <summary>
        /// This method will fetch Dynamic content from CMS and replace it in Email template.
        /// </summary>
        /// <param name="bookingMap">Map for booking</param>
        /// <returns>Void</returns>
        /// <remarks>CR 4 | Text based email confirmation | Release 1.4</remarks>
        private void CollectDynamicTextFromCMS(ref Dictionary<string, string> bookingMap,
                                               SelectedRoomAndRateEntity selectedRoomAndRateEntity, int guestIterator, GuranteeType guaranteePolicyType)
        {
            string selectedRateCategoryID = selectedRoomAndRateEntity.RateCategoryID;
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            string guranteeType = string.Empty;
            RateCategory rateCategory = null;
            if (!string.IsNullOrEmpty(selectedRateCategoryID))
            {
                rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(selectedRateCategoryID);
            }
            bool noRateCodeButDisplayGuarantee;
            string creditCardGuranteeType;
            if (IsTruelyPrepaid(selectedRateCategoryID, search.CampaignCode,
                                out noRateCodeButDisplayGuarantee, out creditCardGuranteeType))
            {
                string[] prepaidGuranteeTypes = AppConstants.GUARANTEE_TYPE_PRE_PAID;
                if ((prepaidGuranteeTypes != null) && (prepaidGuranteeTypes.Length > 0))
                {
                    guranteeType = prepaidGuranteeTypes[0];
                }
            }
            else if (noRateCodeButDisplayGuarantee)
            {
                guranteeType = creditCardGuranteeType;
            }
            else if (null != rateCategory)
            {
                guranteeType = rateCategory.CreditCardGuranteeType;
            }

            string pageIdentifier = string.Empty;
            if (BookingEngineSessionWrapper.IsModifyBooking)
            {
                pageIdentifier = PageIdentifier.ModifyBookingPageIdentifier;
            }
            else
            {
                pageIdentifier = PageIdentifier.BookingPageIdentifier;
            }

            CollectCmsText(bookingMap, guranteeType, pageIdentifier, guestIterator, guaranteePolicyType);

            if ((null != rateCategory) &&
                (rateCategory.RateCategoryId.ToUpper().Equals(SearchType.REDEMPTION.ToString())))
            {
                bookingMap["POLICYTEXT"] = (null !=
                                            ContentDataAccess.GetTextForEmailTemplate(pageIdentifier)["POLICYTEXT"])
                                               ? ContentDataAccess.GetTextForEmailTemplate(pageIdentifier)["POLICYTEXT"]
                                                     .ToString()
                                               : AppConstants.SPACE;

            }
        }

        /// <summary>
        /// This will assign the key with values from CMS.
        /// </summary>
        /// <param name="bookingMap"></param>
        /// <param name="guranteeType"></param>
        /// <param name="pageIdentifier"></param>
        private void CollectCmsText(Dictionary<string, string> bookingMap, string guranteeType, string pageIdentifier,
                                    int guestIterator, GuranteeType guaranteePolicyType)
        {
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            Dictionary<string, string> cmsText = null;
            string[] prepaidGuranteeTypes = AppConstants.GUARANTEE_TYPE_PRE_PAID;
            if ((prepaidGuranteeTypes != null) && (prepaidGuranteeTypes.Contains(guranteeType) && !m_ContentDataAccessManager.GetPaymentFallback(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode)))
            {
                cmsText = ContentDataAccess.GetTextForEmailTemplate(guranteeType, pageIdentifier, string.Empty, GenericSessionVariableSessionWrapper.IsPrepaidBooking);

                if ((null != cmsText) && (null != cmsText["POLICYTEXT"]))
                {
                    cmsText["POLICYTEXT"] = cmsText["POLICYTEXT"].Replace(
                        CommunicationTemplateConstants.CANCELBYDATE, Utility.GetModifyCancelableDate());

                }
            }
            else if (WebUtil.IsBookingAfterSixPM(hotelSearch) || Enum.Equals(guaranteePolicyType, GuranteeType.CREDITCARD))
            {
                cmsText = ContentDataAccess.GetTextForEmailTemplate(guranteeType, pageIdentifier,
                                                                    CommunicationTemplateConstants.WITHCREDITCARD, GenericSessionVariableSessionWrapper.IsPrepaidBooking);
            }
            else
            {
                cmsText = ContentDataAccess.GetTextForEmailTemplate(guranteeType, pageIdentifier,
                                                                    CommunicationTemplateConstants.WITHOUTCREDITCARD, GenericSessionVariableSessionWrapper.IsPrepaidBooking);
            }

            if (null != cmsText)
            {

                bookingMap[CommunicationTemplateConstants.CONTACTUS_TEXT] =
                    cmsText.ContainsKey(CommunicationTemplateConstants.CONTACTUS_TEXT)
                        ? cmsText[CommunicationTemplateConstants.CONTACTUS_TEXT]
                        : AppConstants.SPACE;

                bookingMap[CommunicationTemplateConstants.CONFIRMATION_TEXT] =
                    cmsText.ContainsKey(CommunicationTemplateConstants.CONFIRMATION_TEXT)
                        ? cmsText[CommunicationTemplateConstants.CONFIRMATION_TEXT]
                        : AppConstants.SPACE;

                bookingMap[CommunicationTemplateConstants.STORYBOX_TEXT] =
                    cmsText.ContainsKey(CommunicationTemplateConstants.STORYBOX_TEXT)
                        ? cmsText[CommunicationTemplateConstants.STORYBOX_TEXT]
                        : AppConstants.SPACE;

                bookingMap[CommunicationTemplateConstants.HTMLPOLICYHEADER] =
                    cmsText.ContainsKey(CommunicationTemplateConstants.HTMLPOLICYHEADER)
                        ? cmsText[CommunicationTemplateConstants.HTMLPOLICYHEADER]
                        : AppConstants.SPACE;

                SortedList<string, GuestInformationEntity> guestInformationList =
                    GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
                if (guestInformationList != null)
                {
                        bookingMap[CommunicationTemplateConstants.POLICY_TEXT] = cmsText.ContainsKey("POLICYTEXT") ? cmsText["POLICYTEXT"] : 
                                                                                 AppConstants.SPACE;
                }
            }
            else
            {
                SetupCmsMapToEmptyString(bookingMap);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingMap"></param>
        /// <param name="guestInfoEntity"></param>
        private void CollectGuestsSpecificEmailDetails(ref Dictionary<string, string> bookingMap,
                                                       GuestInformationEntity guestInfoEntity, string reservationNumber)
        {
            bookingMap[CommunicationTemplateConstants.CONFIRMATION_NUMBER] = reservationNumber;
            if (guestInfoEntity.GuestAccountNumber != null && guestInfoEntity.GuestAccountNumber != string.Empty)
            {
                bookingMap[CommunicationTemplateConstants.MEMBERSHIPNUMBER] = guestInfoEntity.GuestAccountNumber;
            }
            else
            {
                bookingMap[CommunicationTemplateConstants.NO_MEMBERID] = AppConstants.SPACE;
            }
            bookingMap[CommunicationTemplateConstants.FIRST_NAME] = guestInfoEntity.FirstName;
            bookingMap[CommunicationTemplateConstants.LASTNAME] = guestInfoEntity.LastName;

            if (guestInfoEntity.Mobile != null && guestInfoEntity.Mobile.Number != null)
            {
                bookingMap[CommunicationTemplateConstants.TELEPHONE2] = guestInfoEntity.Mobile.Number;
            }

            if (guestInfoEntity.EmailDetails != null)
            {
                bookingMap[CommunicationTemplateConstants.EMAIL] = guestInfoEntity.EmailDetails.EmailID;
                if (guestInfoEntity.IsRewardNightGift)
                {
                    if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null &&
                        !string.IsNullOrEmpty(LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email))
                    {
                        bookingMap[CommunicationTemplateConstants.EMAIL2] = LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email;
                    }
                }
            }
        }

        /// <summary>
        /// Collect Guest Specific Email Details
        /// </summary>
        /// <param name="bookingMap">
        /// Dictionary of Email template tokens with its corresponding values
        /// </param>
        private void CollectGenericGuestEmailDetails(ref Dictionary<string, string> bookingMap,
                                                     GuestInformationEntity guestInfoEntity)
        {
            if (guestInfoEntity != null)
            {
                bookingMap[CommunicationTemplateConstants.CITY] = guestInfoEntity.City;

                OrderedDictionary countryCodes = DropDownService.GetCountryCodes();
                if (countryCodes != null)
                {
                    string country = countryCodes[guestInfoEntity.Country] as string;
                    country = country.Replace("\r\n ", "");
                    bookingMap[CommunicationTemplateConstants.COUNTRY] = country;
                }

                if (BedTypePreferenceSessionWrapper.UserBedTypePreference != null)
                {
                    bookingMap[CommunicationTemplateConstants.BED_TYPE_PREFERENCE] =
                        BedTypePreferenceSessionWrapper.UserBedTypePreference;
                }
                else
                {
                    bookingMap[CommunicationTemplateConstants.BED_TYPE_PREFERENCE] =
                        CommunicationTemplateConstants.BLANK_SPACES;
                }

                string selectedRateCategoryID = HotelRoomRateSessionWrapper.SelectedRateCategoryID;
                RateCategory rateCategory = null;
                if ((selectedRateCategoryID != null) && (selectedRateCategoryID != string.Empty))
                {
                    rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(selectedRateCategoryID);
                }
                RetrieveSpecialRequestDetails(bookingMap);

                RetrieveChildrenDetails(bookingMap);
            }
        }

        /// <summary>
        /// Retrieve children information from the booking and send it for the email confirmation.
        /// </summary>
        /// <param name="bookingMap"></param>
        private static void RetrieveChildrenDetails(Dictionary<string, string> bookingMap)
        {
            GuestInformationEntity guestInfoEntity =
                GuestBookingInformationSessionWrapper.GuestBookingInformation as GuestInformationEntity;

            if ((guestInfoEntity != null) && (guestInfoEntity.ChildrensDetails != null))
            {
                ChildrensDetailsEntity childrensDetailsEntity = guestInfoEntity.ChildrensDetails;
                bookingMap[CommunicationTemplateConstants.CHILDREN_AGES] =
                    childrensDetailsEntity.GetChildrensAgesInString();

                string cot = string.Empty, extraBed = string.Empty, sharingBed = string.Empty;
                Utility.GetLocaleSpecificChildrenAccomodationTypes(ref cot, ref extraBed, ref sharingBed);
                bookingMap[CommunicationTemplateConstants.CHILDREN_ACCOMMODATION] =
                    childrensDetailsEntity.GetChildrensAccommodationInString(sharingBed, cot, extraBed);
            }
            else
            {
                bookingMap[CommunicationTemplateConstants.NO_CHILDREN_AGES] =
                    CommunicationTemplateConstants.BLANK_SPACES;
                bookingMap[CommunicationTemplateConstants.NO_CHILDREN_ACCOMMODATION] =
                    CommunicationTemplateConstants.BLANK_SPACES;
            }
        }

        /// <summary>
        /// Retrieve the Guest's special Request Details 
        /// </summary>
        /// <param name="orderedDictionary">
        /// OrderedDictionary
        /// </param>
        private static void RetrieveSpecialRequestDetails(Dictionary<string, string> bookingMap)
        {
            GuestInformationEntity guestInfoEntity =
                GuestBookingInformationSessionWrapper.GuestBookingInformation as GuestInformationEntity;

            if (guestInfoEntity != null)
            {
                if (guestInfoEntity.SpecialRequests != null)
                {
                    /*string otherPeferencesList = string.Empty;
                    string specialRequestList = string.Empty;
                    int totalSpecialRequest = guestInfoEntity.SpecialRequests.Length;
                    for (int spRequestCount = 0; spRequestCount < totalSpecialRequest; spRequestCount++)
                    {
                        switch (guestInfoEntity.SpecialRequests[spRequestCount].RequestValue)
                        {
                            case UserPreferenceConstants.LOWERFLOOR:
                                {
                                    otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.LOWERFLOOR);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.HIGHFLOOR:
                                {
                                    otherPeferencesList +=
                                        WebUtil.GetTranslatedText(TranslatedTextConstansts.HIGHERFLOOR);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.AWAYFROMELEVATOR:
                                {
                                    otherPeferencesList +=
                                        WebUtil.GetTranslatedText(TranslatedTextConstansts.AWAYFROMELEVATOR);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.NEARELEVATOR:
                                {
                                    otherPeferencesList +=
                                        WebUtil.GetTranslatedText(TranslatedTextConstansts.NEARELEVATOR);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.NOSMOKING:
                                {
                                    otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.NOSMOKING);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.SMOKING:
                                {
                                    otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.SMOKING);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.ACCESSIBLEROOM:
                                {
                                    otherPeferencesList +=
                                        WebUtil.GetTranslatedText(TranslatedTextConstansts.ACCESSABLEROOM);
                                    otherPeferencesList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.ALLERGYROOM:
                                {
                                    otherPeferencesList += WebUtil.GetTranslatedText(TranslatedTextConstansts.ALLERYROOM);
                                    otherPeferencesList += " <br />";
                                    break;
                                }

                            case UserPreferenceConstants.EARLYCHECKIN:
                                {
                                    specialRequestList +=
                                        WebUtil.GetTranslatedText(TranslatedTextConstansts.EARLYCHECKIN);
                                    specialRequestList += " <br />";
                                    break;
                                }
                            case UserPreferenceConstants.LATECHECKOUT:
                                {
                                    specialRequestList +=
                                        WebUtil.GetTranslatedText(TranslatedTextConstansts.LATECHECKOUT);
                                    specialRequestList += " <br />";
                                    break;
                                }
                            default:
                                break;
                        }
                    }*/
                    string strotherPeferencesList = string.Empty;
                    string strspecialRequestList = string.Empty;
                    StringBuilder otherPeferencesList = new StringBuilder();
                    StringBuilder specialRequestList = new StringBuilder();

                    int totalSpecialRequest = guestInfoEntity.SpecialRequests.Length;
                    for (int spRequestCount = 0; spRequestCount < totalSpecialRequest; spRequestCount++)
                    {
                        switch (guestInfoEntity.SpecialRequests[spRequestCount].RequestValue)
                        {
                            case UserPreferenceConstants.LOWERFLOOR:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.LOWERFLOOR));
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.HIGHFLOOR:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.HIGHERFLOOR));
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.AWAYFROMELEVATOR:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.AWAYFROMELEVATOR));
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.NEARELEVATOR:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.NEARELEVATOR));
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.NOSMOKING:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.NOSMOKING));
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.SMOKING:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.SMOKING));
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.ACCESSIBLEROOM:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ACCESSABLEROOM));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ACCESSABLEROOMDESC));
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.ALLERGYROOM:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ALLERYROOM));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ALLERYROOMDESC));
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.PETFRIENDLYROOM:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.PETFRIENDLYROOM));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.PETFRIENDLYROOMDESC));
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.EARLYCHECKIN:
                                {
                                    specialRequestList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.EARLYCHECKIN));
                                    specialRequestList.Append(" <br />");
                                    specialRequestList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.EARLYCHECKINDESC));
                                    specialRequestList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.LATECHECKOUT:
                                {
                                    specialRequestList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.LATECHECKOUT));
                                    specialRequestList.Append(" <br />");
                                    specialRequestList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.LATECHECKOUTDESC));
                                    specialRequestList.Append(" <br />");
                                    break;
                                }
                            default:
                                break;
                        }
                    }
                    strotherPeferencesList = otherPeferencesList.ToString();
                    strspecialRequestList = specialRequestList.ToString();
                    if (bookingMap == null)
                    {
                        bookingMap = new Dictionary<string, string>();
                    }
                    if (!string.IsNullOrEmpty(strotherPeferencesList))
                    {
                        bookingMap[CommunicationTemplateConstants.OTHER_PREFERENCE] = strotherPeferencesList;

                        string otherPreference = strotherPeferencesList.Replace("<br />", "\r\n\t");
                        bookingMap[CommunicationTemplateConstants.TEXTFORMAT_OTHER_PREFERENCE] = otherPreference;
                    }
                    else
                    {
                        bookingMap[CommunicationTemplateConstants.OTHER_PREFERENCE] = CommunicationTemplateConstants.BLANK_SPACES;

                        bookingMap[CommunicationTemplateConstants.TEXTFORMAT_OTHER_PREFERENCE] = CommunicationTemplateConstants.BLANK_SPACES;
                    }

                    if (string.IsNullOrEmpty(strspecialRequestList))
                    {
                        bookingMap[CommunicationTemplateConstants.SPECIAL_SERVICE_REQUESTS] = strspecialRequestList;

                        string specilalRequest = strspecialRequestList.Replace("<br />", "\r\n\t");
                        bookingMap[CommunicationTemplateConstants.TEXTFORMAT_SPECIAL_SERVICE_REQUESTS] = specilalRequest;
                    }
                    else
                    {
                        bookingMap[CommunicationTemplateConstants.SPECIAL_SERVICE_REQUESTS] = CommunicationTemplateConstants.BLANK_SPACES;

                        bookingMap[CommunicationTemplateConstants.TEXTFORMAT_SPECIAL_SERVICE_REQUESTS] = CommunicationTemplateConstants.BLANK_SPACES;
                    }
                }
            }
        }

        /// <summary>
        /// This will set the different property to empty string 
        /// </summary>
        /// <param name="cmsMap">Map Used for holding the different cms text.</param>
        private static void SetupCmsMapToEmptyString(Dictionary<string, string> bookingMap)
        {
            if (bookingMap != null)
            {
                string emptyString = AppConstants.SPACE;

                bookingMap[CommunicationTemplateConstants.CONTACTUS_TEXT] = emptyString;
                bookingMap[CommunicationTemplateConstants.CONFIRMATION_TEXT] = emptyString;
                bookingMap[CommunicationTemplateConstants.STORYBOX_TEXT] = emptyString;
                bookingMap[CommunicationTemplateConstants.POLICY_TEXT] = emptyString;
            }
        }

        /// <summary>
        /// This will find out if the guarantee information need to be displayed or not.
        /// Along with it finds if no rate code associated and credit card guarantee type text.
        /// </summary>
        /// <param name="rateCategoryId">Rate category id if associated with this booking.</param>
        /// <param name="blockCode">Block code</param>
        /// <param name="noRateCodeButDisplayGuarantee">True if no rate code associated but display the guarantee information.</param>
        /// <param name="creditCardGuranteeType">Credit card guarantee type text</param>
        /// <returns>True if this is block code booking and if block code does not need the credit card information</returns>
        private bool IsTruelyPrepaid(string rateCategoryId, string blockCode,
                                     out bool noRateCodeButDisplayGuarantee, out string creditCardGuranteeType)
        {
            bool returnVaue = false;
            noRateCodeButDisplayGuarantee = false;
            creditCardGuranteeType = string.Empty;
            if (Utility.IsBlockCodeBooking)
            {
                if (rateCategoryId == AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
                {
                    Block blockPage = ContentDataAccess.GetBlockCodePages(blockCode);
                    if (null != blockPage)
                    {
                        if (blockPage.CaptureGuarantee)
                        {
                            noRateCodeButDisplayGuarantee = true;
                            if (!string.IsNullOrEmpty(blockPage.GuranteeType))
                            {
                                creditCardGuranteeType = blockPage.GuranteeType;
                            }
                        }
                        else
                        {
                            returnVaue = true;
                        }
                    }
                }
            }
            return returnVaue;
        }

        /// <summary>
        /// Pre populate credit card information 
        /// </summary>
        private void PrePopulateCreditInfo()
        {
            NameController nameController = new NameController();
            CreditCardEntity creditCardDetails = null;

            if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null)
            {
                creditCardDetails = nameController.FetchCreditCard(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID, false);
            }

            if (creditCardDetails != null && string.IsNullOrEmpty(txtCardHolderCommon.Value) && string.IsNullOrEmpty(txtCardNumberCommon.Value))
            {
                rdoLateArrivalGuranteeCommon.Checked = true;
                string creditCardNumber = creditCardDetails.CardNumber;

                txtCardHolderCommon.Value = creditCardDetails.NameOnCard;
                previousCardNumber.Value = creditCardNumber;
                if (creditCardNumber.Length >= AppConstants.CREDIT_CARD_DISPLAY_CHARS)
                {
                    txtCardNumberCommon.Value = AppConstants.CARD_MASK +
                                                creditCardNumber.Substring((creditCardNumber.Length -
                                                                            AppConstants.CREDIT_CARD_DISPLAY_CHARS));
                }
                else
                {
                    txtCardNumberCommon.Value = AppConstants.CARD_MASK + creditCardNumber;
                }

                ListItem selectedCard = ddlCardTypeCommon.Items.FindByValue(creditCardDetails.CardType);
                if (selectedCard != null)
                {
                    ddlCardTypeCommon.SelectedItem.Selected = false;
                    selectedCard.Selected = true;
                }
                ListItem selectedMonth =
                    ddlExpiryMonthCommon.Items.FindByValue(creditCardDetails.ExpiryDate.Month.ToString());
                if (selectedMonth != null)
                {
                    ddlExpiryMonthCommon.SelectedItem.Selected = false;
                    selectedMonth.Selected = true;
                }

                ListItem selectedYear =
                    ddlExpiryYearCommon.Items.FindByValue(creditCardDetails.ExpiryDate.Year.ToString());
                if (selectedYear != null)
                {
                    ddlExpiryYearCommon.SelectedItem.Selected = false;
                    selectedYear.Selected = true;
                }
            }
        }


        /// <summary>
        /// This method is responsible for Creating room containers.
        /// Read the room selected and create the room details.
        /// Raj Please have a relook.
        /// </summary>
        /// <remarks>Release - R2</remarks>
        private void PopulateRoomInformation()
        {
            div1LegNumber.Value = string.Empty;
            div2LegNumber.Value = string.Empty;
            div3LegNumber.Value = string.Empty;
            div4LegNumber.Value = string.Empty;


            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            int roomCount = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count;
            noOfRoomReserVationContainer.Value = roomCount.ToString();
            List<string> rateCategoryId = new List<string>();
            SortedList<string, GuestInformationEntity> guestInfoList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
            bool paymentFallback = m_ContentDataAccessManager.GetPaymentFallback(hotelSearch.SelectedHotelCode);
            if ((!string.IsNullOrEmpty(Reservation2SessionWrapper.NetsPaymentErrorMessage) || !string.IsNullOrEmpty(Reservation2SessionWrapper.NetsPaymentInfoMessage))
                && (Reservation2SessionWrapper.PaymentGuestInformation != null))
            {
                guestInfoList = new SortedList<string, GuestInformationEntity>();
                int keyCount = 0;
                List<GuestInformationEntity> guestInformationEntities = Reservation2SessionWrapper.PaymentGuestInformation as List<GuestInformationEntity>;
                foreach (GuestInformationEntity guestInformationEntity in guestInformationEntities)
                {
                    guestInfoList.Add(keyCount.ToString(), guestInformationEntity);
                    keyCount++;
                }
            }

            for (int count = 0; count < roomCount; count++)
            {
                SelectedRoomAndRateEntity roomAndRate =
                    HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[count] as SelectedRoomAndRateEntity;
                if ((null != roomAndRate) && (!rateCategoryId.Contains(roomAndRate.RateCategoryID)))
                {
                    rateCategoryId.Add(roomAndRate.RateCategoryID);
                    if (RoomRateUtil.IsSaveRateCategory(roomAndRate.RateCategoryID) && !paymentFallback)
                    {
                        GenericSessionVariableSessionWrapper.IsPrepaidBooking = true;
                    }
                }
            }
            bool guranteeDisplayFlag = true;
            bool isCommonGaurantee = false;
            if ((rateCategoryId.Count == 1) && (roomCount > 1))
            {
                bool isStoredPanHashCreditCardsCanBeShown = false;
                RateCategory rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(rateCategoryId[0]);
                if (rateCategory != null)
                {
                    isStoredPanHashCreditCardsCanBeShown =
                        rateCategory.RateCategoryColor.Equals(SelectRateConstants.FLEX_RATECATEGORY_COLOR,
                                                              StringComparison.InvariantCultureIgnoreCase) ||
                        RoomRateUtil.IsSaveRateCategory(rateCategoryId[0]);
                }
                isCommonGaurantee = true;
                guranteeDisplayFlag = false;
                HideUnhideGuranteeDivCommon.Visible = true;
                if (HygieneSessionWrapper.IsComboReservation)
                    guaranteeDepositAppendForComboReservation.Text = "(" +
                                                                     WebUtil.GetTranslatedText(
                                                                         "/bookingengine/booking/bookingdetail/guaranteedepositeforComboReservation") +
                                                                     ")";

                SelectedRoomAndRateEntity roomRate =
                    HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[0] as SelectedRoomAndRateEntity;
                DisplayHoldOptionIfRequired(roomRate.RateCategoryID, paymentFallback);
                SetCreditCardDropDown();
                SetExpiryDateList(ddlExpiryMonthCommon, ddlExpiryYearCommon);
                if (!HygieneSessionWrapper.IsComboReservation)
                {
                    if (guestInfoList != null)
                    {
                        CreditCardEntity creditCard =
                            guestInfoList[guestInfoList.Keys[0].ToString()].GuranteeInformation.CreditCard;
                        if (creditCard != null)
                        {
                            if (null != creditCard.NameOnCard)
                            {
                                txtCardHolderCommon.Value = creditCard.NameOnCard;
                            }
                            if (null != creditCard.CardNumber)
                            {
                                for (int i = 0; i < roomCount; i++)
                                {
                                    RoomInformationContainer tempRoominfoCont = FindControl("RoomInformation" + i) as RoomInformationContainer;
                                    HtmlInputHidden prevCardNumber = tempRoominfoCont.FindControl("previousCardNumber") as HtmlInputHidden;
                                    prevCardNumber.Value = creditCard.CardNumber;
                                }
                                if (creditCard.CardNumber.Length >= AppConstants.CREDIT_CARD_DISPLAY_CHARS)
                                {
                                    txtCardNumberCommon.Value = AppConstants.CARD_MASK + creditCard.CardNumber.Substring((creditCard.CardNumber.Length - AppConstants.CREDIT_CARD_DISPLAY_CHARS));
                                }
                                else
                                {
                                    txtCardNumberCommon.Value = AppConstants.CARD_MASK + creditCard.CardNumber;
                                }
                            }
                            if (null != creditCard.ExpiryDate)
                            {
                                //artf1295003 : Modify/Cancel - cc on booking details
                                ddlExpiryMonthCommon.SelectedIndex = (creditCard.ExpiryDate.Month) > 0
                                                                         ? (creditCard.ExpiryDate.Month)
                                                                         : 0;
                                ddlExpiryYearCommon.SelectedValue = creditCard.ExpiryDate.Year.ToString();
                            }
                            if (null != creditCard.CardType)
                            {
                                ddlCardTypeCommon.SelectedValue = creditCard.CardType;
                            }
                            rdoLateArrivalGuranteeCommon.Checked = true;
                            // rdoHoldRoomCommon.Checked = false;
                        }
                    }

                    if (!paymentFallback && (isStoredPanHashCreditCardsCanBeShown))
                    {
                        PopulatePanHashCreditCards(ddlPanHashCreditCardsCommon);
                    }

                    if (HygieneSessionWrapper.IsPANHashCreditCardsAvailable && !paymentFallback && (isStoredPanHashCreditCardsCanBeShown))
                    {
                        HideUnhideCreditCardDivCommon.Visible = false;
                        panHashCreditCardDivCommon.Visible = true;
                    }
                    else
                    {
                        PrePopulateCreditInfo();
                        HideUnhideCreditCardDivCommon.Visible = true;
                        panHashCreditCardDivCommon.Visible = false;                        
                    }

                    if (RoomRateUtil.IsSaveRateCategory(roomRate.RateCategoryID) && !paymentFallback)
                    {
                        HideUnhideCreditCardDivCommon.Visible = false;
                        panHashCreditCardDivCommon.Visible = false;
                    }
                    else
                    {
                        if (WebUtil.IsBookingAfterSixPM(hotelSearch))
                        {
                            rdoLateArrivalGuranteeCommon.Checked = true;
                            hid6PMBooking.Value = "1";
                            spanHoldRoomCommon.Visible = false;
                        }
                    }
                }
            }

            for (int count = 0; count < roomCount; count++)
            {
                GuestInformationEntity guestInformation = null;
                if (guestInfoList != null && guestInfoList.Keys.Count > count && guestInfoList.Keys[count] != null)
                {
                    string key = guestInfoList.Keys[count];
                    guestInformation = guestInfoList[key];
                }

                if (guestInformation != null && count == 0)
                    div1LegNumber.Value = guestInformation.LegNumber;

                if (guestInformation != null && count == 1)
                    div2LegNumber.Value = guestInformation.LegNumber;

                if (guestInformation != null && count == 2)
                    div3LegNumber.Value = guestInformation.LegNumber;

                if (guestInformation != null && count == 3)
                    div4LegNumber.Value = guestInformation.LegNumber;

                RoomInformationContainer RoomInformation =
                    FindControl("RoomInformation" + count) as RoomInformationContainer;
                if (null != RoomInformation)
                {
                    RoomInformation.DataSource = guestInformation;
                    HotelSearchRoomEntity hotelRoomEntity = hotelSearch.ListRooms.Count > count
                                                                ? hotelSearch.ListRooms[count]
                                                                : null;
                    RoomInformation.IsRoomConfirmed = hotelRoomEntity != null ? hotelRoomEntity.IsRoomConfirmed : false;

                    SelectedRoomAndRateEntity roomAndRate =
                        HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[count] as SelectedRoomAndRateEntity;
                    if (null != roomAndRate)
                    {
                        RoomInformation.Ratecode = roomAndRate.RateCategoryID;
                        RateCategory rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(roomAndRate.RateCategoryID);
                        if (rateCategory != null)
                        {
                            RoomInformation.IsStoredPanHashCreditCardsCanBeShown =
                                rateCategory.RateCategoryColor.Equals(SelectRateConstants.FLEX_RATECATEGORY_COLOR, StringComparison.InvariantCultureIgnoreCase) ||
                                RoomRateUtil.IsSaveRateCategory(roomAndRate.RateCategoryID);
                        }
                    }

                    if (!paymentFallback && RoomInformation.IsStoredPanHashCreditCardsCanBeShown)
                    {
                        DropDownList ddlPanHashCreditCards = null;
                        if (!isCommonGaurantee)
                        {
                            ddlPanHashCreditCards = RoomInformation.FindControl("ddlPanHashCreditCards") as DropDownList;
                            PopulatePanHashCreditCards(ddlPanHashCreditCards);
                        }
                        else
                        {
                            ddlPanHashCreditCards = ddlPanHashCreditCardsCommon;
                        }
                        if ((guestInformation != null) &&
                            (guestInformation.GuranteeInformation != null) &&
                            (guestInformation.GuranteeInformation.CreditCard != null) &&
                            (ddlPanHashCreditCards != null) && (ddlPanHashCreditCards.Items != null) &&
                            (ddlPanHashCreditCards.Items.Count > 0))
                        {
                            if (
                                guestInformation.GuranteeInformation.CreditCard.CardType.Contains(
                                    AppConstants.PREPAID_CCTYPE_PREFIX))
                            {
                                string creditCardNo =
                                    string.Concat(
                                        WebUtil.GetTranslatedText(
                                            "/bookingengine/booking/ManageCreditCard/UserCreditCard"),
                                        AppConstants.SPACE,
                                        WebUtil.GetScanwebCCNoForDisplay(
                                            guestInformation.GuranteeInformation.CreditCard.CardType,
                                            guestInformation.GuranteeInformation.CreditCard.CardNumber));
                                if (AppConstants.ModifyCreditCardNumberForMakePayment)
                                {
                                    creditCardNo = creditCardNo.Replace('x', '*');
                                }
                                ListItem selectedItem = ddlPanHashCreditCards.Items.FindByText(creditCardNo);
                                if (selectedItem != null)
                                {
                                    ddlPanHashCreditCards.SelectedValue = selectedItem.Value;
                                }
                            }
                        }
                    }

                    if (Utility.GetCurrentLanguage().ToUpper() == LanguageConstant.LANGUAGE_GERMAN)
                    {
                        DropDownList ddlTitle = RoomInformation.FindControl("ddlTitle") as DropDownList;
                        InformationController informationCtrl = new InformationController();
                        Dictionary<string, string> nameTitleMapOpera =
                            informationCtrl.GetConfiguredTitles(LanguageConstant.LANGUAGE_GERMAN_CODE);
                        ListItem selectItem = new ListItem(AppConstants.TITLE_DEFAULT_VALUE,
                                                           AppConstants.TITLE_DEFAULT_VALUE);
                        ddlTitle.Items.Add(selectItem);

                        foreach (string key in nameTitleMapOpera.Keys)
                        {
                            ddlTitle.Items.Add(new ListItem(nameTitleMapOpera[key].ToString(), key));
                        }
                        string selectedTitle = (guestInformation != null && guestInformation.Title != null &&
                                                guestInformation.Title != String.Empty)
                                                   ? guestInformation.Title
                                                   : AppConstants.TITLE_DEFAULT_VALUE;
                        ListItem selectedItem = ddlTitle.Items.FindByValue(selectedTitle);
                        selectedItem.Selected = true;
                    }
                    string header = "/bookingengine/booking/bookingdetail/room" + count + count;
                    RoomInformation.RoomIdentifier = count;
                    RoomInformation.DoDisplayGurantee = guranteeDisplayFlag;
                    RoomInformation.RoomHeader = WebUtil.GetTranslatedText(header);
                    RoomInformation.DataBind();
                    if (roomAndRate != null && roomAndRate.RateCategoryID != null &&
                        roomAndRate.RateCategoryID.Equals(AppConstants.BONUS_CHEQUE_RATES[0]))
                    {
                        RoomInformation.ShowDNumber = true;
                        if (guestInformation == null || string.IsNullOrEmpty(guestInformation.D_Number))
                        {
                        }
                        else
                        {
                            RoomInformation.DNumber = guestInformation.D_Number;
                        }
                    }
                    else
                    {
                        RoomInformation.ShowDNumber = false;
                    }

                    RoomInformation.Visible = true;
                    if (count == 0 && LoyaltyDetailsSessionWrapper.LoyaltyDetails != null && UpdateGuestInfo.Visible)
                    {
                        RoomInformation.SetLoggedUserDetails();
                        LoyaltyDetailsEntity loyalityDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
						//Added as part of SCANAM-537
                        if (!string.IsNullOrEmpty(loyalityDetails.NativeFirstName) && !string.IsNullOrEmpty(loyalityDetails.NativeLastName))
                        {
                            hdnFName.Value = loyalityDetails.NativeFirstName;
                            hdnLName.Value = loyalityDetails.NativeLastName;
                        }
                        else
                        {
                            hdnFName.Value = loyalityDetails.FirstName;
                            hdnLName.Value = loyalityDetails.SurName;
                        }
                        hdnEmail.Value = loyalityDetails.Email;
                        hdnConfEmail.Value = loyalityDetails.Email;
                        hdnCountry.Value = loyalityDetails.Country;
                        hdnPhoneNumber.Value = RoomInformation.GetPhoneNumberWithoutCountryCode(loyalityDetails.Telephone1);
                        hdnCity.Value = loyalityDetails.City;
                    }
                    else if (count == 0 && guestInformation != null &&
                             !string.IsNullOrEmpty(guestInformation.GuestAccountNumber))
                    {
                        lblFGPDiv.Attributes["style"] = "display:none;";
                    }
                    if (roomAndRate != null && !RoomRateUtil.IsSaveRateCategory(roomAndRate.RateCategoryID))
                        RoomInformation.DisplayCreditcardOptionAfterSixPM(hotelSearch);
                }

                HtmlGenericControl control = FindControl("divRoom" + count) as HtmlGenericControl;
                if (null != control)
                {
                    control.Visible = true;
                }
                //RoomInformation.DisplayCreditcardOptionAfterSixPM(hotelSearch);
            }
            if ((rateCategoryId.Count == 1) && (roomCount > 1))
            {

                Page.ClientScript.RegisterStartupScript(typeof(Page), "EnableCreditcardinfo",
                                                        "<script type=\"text/javascript\">EnableCreditcardinfoForCommonRoom();</script>");
            }
            else
            {

                Page.ClientScript.RegisterStartupScript(typeof(Page), "EnableCreditcardinfo",
                                                        "<script type=\"text/javascript\">EnableCreditcardinfo(" +
                                                        roomCount + ");</script>");
            }
        }

        

        /// <summary>
        /// Raj Kishore
        /// </summary>
        /// <param name="guestList"></param>
        private void CaptureGuestInformation(List<GuestInformationEntity> guestList)
        {
            int roomCount = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count;
            List<string> rateCategoryId = new List<string>();
            SelectedRoomAndRateEntity roomAndRate = null;
            for (int count = 0; count < roomCount; count++)
            {
                roomAndRate = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[count] as SelectedRoomAndRateEntity;
                if ((null != roomAndRate) && (!rateCategoryId.Contains(roomAndRate.RateCategoryID)))
                {
                    rateCategoryId.Add(roomAndRate.RateCategoryID);
                }
            }
            bool isCommonGurantee = false;
            if ((rateCategoryId.Count == 1) && (roomCount > 1))
            {
                isCommonGurantee = true;
            }

            for (int count = 0; count < roomCount; count++)
            {
                GuestInformationEntity guestInfo = new GuestInformationEntity();
                if (UserLoggedInSessionWrapper.UserLoggedIn &&
                    !BookingEngineSessionWrapper.IsModifyBooking &&
                    SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REDEMPTION &&
                    chkBookForSomeoneElse != null && chkBookForSomeoneElse.Visible == true)
                {
                    if (chkBookForSomeoneElse.Checked == true)
                    {
                        BookingEngineSessionWrapper.UpdateProfileBookingInfo = false;
                        guestInfo.IsRewardNightGift = true;
                    }
                    else
                    {
                        guestInfo.IsRewardNightGift = false;
                    }
                }
                HtmlGenericControl control = FindControl("divRoom" + count) as HtmlGenericControl;
                RoomInformationContainer RoomInformation =
                    FindControl("RoomInformation" + count) as RoomInformationContainer;
                if (null != control && null != RoomInformation)
                {
                    if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.ListRooms != null &&
                        SearchCriteriaSessionWrapper.SearchCriteria.ListRooms.Count > 0)
                    {
                        if (SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[count] != null)
                            RoomInformation.IsRoomModifiable =
                                SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[count].IsRoomModifiable;
                    }

                    DropDownList nameTitle = RoomInformation.FindControl("ddlTitle") as DropDownList;
                    guestInfo.Title = (nameTitle != null && !string.Equals(nameTitle.Text, AppConstants.TITLE_DEFAULT_VALUE, StringComparison.InvariantCultureIgnoreCase)) ? nameTitle.Text : string.Empty;

                    HtmlInputText firstName = RoomInformation.FindControl("txtFNameRoom") as HtmlInputText;
                    guestInfo.FirstName = (firstName != null) ? firstName.Value.Trim() : string.Empty;

                    HtmlInputText lastName = RoomInformation.FindControl("txtLNameRoom") as HtmlInputText;
                    guestInfo.LastName = (lastName != null) ? lastName.Value.Trim() : string.Empty;

                    string legNumber = null;
                    legNumber = GetLegNumber(count, guestInfo, legNumber);
                    guestInfo.LegNumber = legNumber;

                    HtmlInputCheckBox chkSMSConfirmation = RoomInformation.FindControl("SMSconfirmation") as HtmlInputCheckBox;

                    guestInfo.IsSMSChecked = (chkSMSConfirmation != null) ? chkSMSConfirmation.Checked : false;


                    BookingDetailsEntity fetchedBookingDetails = null;
                    if (BookingEngineSessionWrapper.IsModifyBooking && BookingEngineSessionWrapper.IsModifyingLegBooking)
                    {
                        fetchedBookingDetails = Utility.GetLegBookingDetail();
                        guestInfo.LegNumber = fetchedBookingDetails.LegNumber;
                    }
                    else
                    {
                        if (legNumber != null)
                            fetchedBookingDetails = Utility.GetBookingDetail(legNumber);
                    }

                    if ((fetchedBookingDetails != null) && (fetchedBookingDetails.GuestInformation != null))
                    {
                        guestInfo.NameID = fetchedBookingDetails.GuestInformation.NameID;
                        guestInfo.AddressID = fetchedBookingDetails.GuestInformation.AddressID;
                        guestInfo.AddressLine1 = fetchedBookingDetails.GuestInformation.AddressLine1;
                        guestInfo.AddressLine2 = fetchedBookingDetails.GuestInformation.AddressLine2;
                        guestInfo.PostCode = fetchedBookingDetails.GuestInformation.PostCode;
                        guestInfo.City = fetchedBookingDetails.GuestInformation.City;
                        guestInfo.ReservationNumber = fetchedBookingDetails.GuestInformation.ReservationNumber;
                    }

                    LoyaltyDetailsEntity loyaltyInfo = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
                    if (loyaltyInfo != null)
                    {
                        guestInfo.GuestAccountNumber = loyaltyInfo.UserName;
                        if (loyaltyInfo.MembershipID != null)
                        {
                            guestInfo.MembershipID = long.Parse(loyaltyInfo.MembershipID.ToString());
                        }
                    }
                    HtmlInputText membershipNo = RoomInformation.FindControl("txtMembershipNumberRoom") as HtmlInputText;
                    guestInfo.GuestAccountNumber = membershipNo.Value != membershipNo.Attributes["rel"]
                                                       ? membershipNo.Value
                                                       : string.Empty;
                    //  if (guestInfo.GuestAccountNumber == "308123") guestInfo.GuestAccountNumber = "";                  
                    HtmlInputText D_No = RoomInformation.FindControl("txtD_Number") as HtmlInputText;

                    string dNumberValue = D_No.Value != D_No.Attributes["rel"] ? D_No.Value : null;
                    if (!string.IsNullOrEmpty(dNumberValue))
                        guestInfo.D_Number = dNumberValue;

                    DropDownList countryDropDown = RoomInformation.FindControl("ddlCountry") as DropDownList;
                    guestInfo.Country = (countryDropDown != null) ? countryDropDown.Text : string.Empty;

                    DropDownList mobileDropDown = RoomInformation.FindControl("ddlMobileNumber") as DropDownList;
                    if (mobileDropDown != null && mobileDropDown.Text != "DFT")
                    {
                        string mobilePhoneCode = Utility.FormatePhoneCode(mobileDropDown.Text);
                        HtmlInputText telePhone = RoomInformation.FindControl("txtTelephone") as HtmlInputText;

                        if ((telePhone != null) && !string.IsNullOrEmpty(telePhone.Value))
                        {
                            telePhone.Value = Regex.Replace(telePhone.Value, @"\D", "");
                            guestInfo.Mobile = new PhoneDetailsEntity();
                            //artf1294954 : Booking confirmation - SMS confirmation
                            guestInfo.Mobile.Number = mobilePhoneCode.ToString() +
                                                      ((telePhone != null) ? telePhone.Value.TrimStart(new char[] { '0' }) : string.Empty);

                            if ((fetchedBookingDetails != null) && (fetchedBookingDetails.GuestInformation != null)
                                && (fetchedBookingDetails.GuestInformation.Mobile != null))
                            {
                                guestInfo.Mobile.OperaId =
                                    fetchedBookingDetails.GuestInformation.Mobile.OperaId;
                            }
                        }
                    }

                    if (Reservation2SessionWrapper.IsModifyFlow && HygieneSessionWrapper.IsComboReservation)
                    {
                        if (mobileDropDown != null && mobileDropDown.Text == "DFT")
                        {
                            HtmlInputText telePhone = RoomInformation.FindControl("txtTelephone") as HtmlInputText;
                            telePhone.Value = Regex.Replace(telePhone.Value, @"\D", "");
                            if ((telePhone != null) && !string.IsNullOrEmpty(telePhone.Value))
                            {
                                guestInfo.Mobile = new PhoneDetailsEntity();
                                guestInfo.Mobile.Number = ((telePhone != null) ? telePhone.Value.TrimStart(new char[] { '0' }) : string.Empty);

                                if ((fetchedBookingDetails != null) && (fetchedBookingDetails.GuestInformation != null)
                                    && (fetchedBookingDetails.GuestInformation.Mobile != null))
                                {
                                    guestInfo.Mobile.OperaId =
                                        fetchedBookingDetails.GuestInformation.Mobile.OperaId;
                                }
                            }
                        }
                    }
                    HtmlInputText email = RoomInformation.FindControl("txtEmail") as HtmlInputText;
                    if (null != email)
                    {
                        guestInfo.EmailDetails = new EmailDetailsEntity();
                        guestInfo.EmailDetails.EmailID = email.Value;

                        guestInfo.NonBusinessEmailDetails = new EmailDetailsEntity();
                        guestInfo.NonBusinessEmailDetails.EmailID = email.Value;
                    }
                    if (fetchedBookingDetails != null && fetchedBookingDetails.GuestInformation != null
                        && fetchedBookingDetails.GuestInformation.EmailDetails != null)
                    {
                        guestInfo.EmailDetails.EmailOperaID =
                            fetchedBookingDetails.GuestInformation.EmailDetails.EmailOperaID;
                        if (fetchedBookingDetails.GuestInformation.NonBusinessEmailDetails != null)
                            guestInfo.NonBusinessEmailDetails.EmailOperaID =
                                fetchedBookingDetails.GuestInformation.NonBusinessEmailDetails.EmailOperaID;
                    }


                    DropDownList ddlBedTypteList = RoomInformation.FindControl("ddlBedTypePreference") as DropDownList;
                    if (null != ddlBedTypteList)
                    {
                        guestInfo.BedTypePreference = ddlBedTypteList.SelectedItem.Text;
                        guestInfo.BedTypePreferenceCode = ddlBedTypteList.SelectedValue;
                    }

                    SpecialRequestEntity[] specialRequest = null;
                    CaptureSpecialRequest(RoomInformation, ref specialRequest);
                    guestInfo.SpecialRequests = specialRequest;

                    HtmlTextArea additionalReq = RoomInformation.FindControl("additionalRequest") as HtmlTextArea;
                    if (null != additionalReq)
                    {
                        guestInfo.AdditionalRequest = additionalReq.Value;
                    }

                    GuranteeInformationEntity guranteeInformation = null;
                    roomAndRate = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[count] as SelectedRoomAndRateEntity;

                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                    string campaignCode = string.Empty;
                    if (hotelSearch != null)
                    {
                        if (!string.IsNullOrEmpty(RoomInformation.DNumber) &&
                            RoomInformation.DNumber != Utility.FormatCode(RoomInformation.DNumber))
                        {
                            campaignCode = RoomInformation.DNumber;
                        }
                        else
                        {
                            campaignCode = hotelSearch.CampaignCode;
                        }
                    }
                    if (Reservation2SessionWrapper.IsModifyFlow && HygieneSessionWrapper.IsComboReservation)
                    {
                        if (RoomInformation.IsRoomModifiable)
                        {
                            CaptureGuranteeInformation(roomAndRate.RateCategoryID, campaignCode,
                                                       ref guranteeInformation, RoomInformation, isCommonGurantee, hotelSearch.SelectedHotelCode);

                            guestInfo.GuranteeInformation = guranteeInformation;
                        }
                        else
                        {
                            if (GuestBookingInformationSessionWrapper.AllGuestsBookingInformations != null)
                            {
                                GuestInformationEntity sessionGuest =
                                    GuestBookingInformationSessionWrapper.AllGuestsBookingInformations.Values[count];
                                if (sessionGuest != null)
                                    guestInfo.GuranteeInformation = sessionGuest.GuranteeInformation;
                                else
                                    guestInfo.GuranteeInformation = null;
                            }
                        }
                    }
                    else
                    {
                        CaptureGuranteeInformation(roomAndRate.RateCategoryID, campaignCode,
                                                   ref guranteeInformation, RoomInformation, isCommonGurantee, hotelSearch.SelectedHotelCode);
                        guestInfo.GuranteeInformation = guranteeInformation;
                    }
                }
                guestList.Add(guestInfo);
            }
            GuestBookingInformationSessionWrapper.GuestInformationList = guestList;

        }


        /// <summary>
        /// To Get the Leg number for already existing booking
        /// </summary>
        /// <param name="count"></param>
        /// <param name="guestInfo"></param>
        /// <param name="legNumber"></param>
        /// <returns></returns>
        private string GetLegNumber(int count, GuestInformationEntity guestInfo, string legNumber)
        {
            switch (count)
            {
                case 0:
                    if (div1LegNumber.Value != string.Empty)
                        legNumber = div1LegNumber.Value;
                    break;
                case 1:
                    if (div2LegNumber.Value != string.Empty)
                        legNumber = div2LegNumber.Value;
                    break;
                case 2:
                    if (div3LegNumber.Value != string.Empty)
                        legNumber = div3LegNumber.Value;
                    break;
                case 3:
                    if (div4LegNumber.Value != string.Empty)
                        legNumber = div4LegNumber.Value;
                    break;
            }
            return legNumber;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RoomInformation"></param>
        /// <param name="specialRequest"></param>
        private void CaptureSpecialRequest(RoomInformationContainer RoomInformation, ref SpecialRequestEntity[] specialRequest)
        {
            ArrayList spRequestList = new ArrayList();
            DropDownList ddlRoomFloor = RoomInformation.FindControl("ddlRoomFloor") as DropDownList;
            if (ddlRoomFloor != null && !string.Equals(ddlRoomFloor.SelectedValue, AppConstants.DEFAULT_VALUE_CONST, StringComparison.InvariantCultureIgnoreCase))
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = ddlRoomFloor.SelectedValue;
                spRequestList.Add(splRequestEntity);
            }

            DropDownList ddlElevator = RoomInformation.FindControl("ddlElevator") as DropDownList;
            if (ddlElevator != null && !string.Equals(ddlElevator.SelectedValue, AppConstants.DEFAULT_VALUE_CONST, StringComparison.InvariantCultureIgnoreCase))
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = ddlElevator.SelectedValue;
                spRequestList.Add(splRequestEntity);
            }

            DropDownList ddlSmoking = RoomInformation.FindControl("ddlSmoking") as DropDownList;
            if (ddlSmoking != null && !string.Equals(ddlSmoking.SelectedValue, AppConstants.DEFAULT_VALUE_CONST, StringComparison.InvariantCultureIgnoreCase))
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = ddlSmoking.SelectedValue;
                spRequestList.Add(splRequestEntity);
            }

            HtmlInputCheckBox chkAccessableRoom = RoomInformation.FindControl("chkAccessableRoom") as HtmlInputCheckBox;
            if (chkAccessableRoom != null && chkAccessableRoom.Checked)
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = UserPreferenceConstants.ACCESSIBLEROOM;
                spRequestList.Add(splRequestEntity);
            }

            HtmlInputCheckBox chkAllergyRooms = RoomInformation.FindControl("chkAllergyRooms") as HtmlInputCheckBox;
            if (chkAllergyRooms != null && chkAllergyRooms.Checked)
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = UserPreferenceConstants.ALLERGYROOM;
                spRequestList.Add(splRequestEntity);
            }

            HtmlInputCheckBox chkEarlyCheckIn = RoomInformation.FindControl("chkEarlyCheckIn") as HtmlInputCheckBox;
            if (chkEarlyCheckIn != null && chkEarlyCheckIn.Checked)
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = UserPreferenceConstants.EARLYCHECKIN;
                spRequestList.Add(splRequestEntity);
            }

            HtmlInputCheckBox chkLateCheckOut = RoomInformation.FindControl("chkLateCheckOut") as HtmlInputCheckBox;
            if (chkLateCheckOut != null && chkLateCheckOut.Checked)
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = UserPreferenceConstants.LATECHECKOUT;
                spRequestList.Add(splRequestEntity);
            }

            HtmlInputCheckBox ChkPetfriendlyroom = RoomInformation.FindControl("ChkPetfriendlyroom") as HtmlInputCheckBox;
            if (ChkPetfriendlyroom != null && ChkPetfriendlyroom.Checked)
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = UserPreferenceConstants.PETFRIENDLYROOM;
                spRequestList.Add(splRequestEntity);
            }

            specialRequest = spRequestList.ToArray(typeof(SpecialRequestEntity)) as SpecialRequestEntity[];
        }

        #region INavigationTraking Members

        public List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> parameters = new List<KeyValueParam>();
            List<GuestInformationEntity> guestList = GuestBookingInformationSessionWrapper.GuestInformationList;
            int counter = default(int);
            parameters.AddRange((from guest in guestList
                                 select new KeyValueParam(string.Format(" Room {0}: Title", ++counter),
                                                         guest.Title)).ToList());
            counter = default(int);
            parameters.AddRange((from guest in guestList
                                 select new KeyValueParam(string.Format(" Room {0}: FirstName", ++counter),
                                                         guest.FirstName)).ToList());
            counter = default(int);
            parameters.AddRange((from guest in guestList
                                 select new KeyValueParam(string.Format(" Room {0}: LastName", ++counter),
                                                         guest.LastName)).ToList());
            counter = default(int);
            parameters.AddRange((from guest in guestList
                                 select new KeyValueParam(string.Format(" Room {0}: EmailID", ++counter),
                                                         guest.EmailDetails.EmailID)).ToList());
            counter = default(int);
            parameters.AddRange((from guest in guestList
                                 select new KeyValueParam(string.Format(" Room {0}: Country", ++counter),
                                                         guest.Country)).ToList());
            counter = default(int);
            parameters.AddRange((from guest in guestList
                                 select new KeyValueParam(string.Format(" Room {0}: Mobile", ++counter),
                                                         guest.Mobile != null ? guest.Mobile.Number : string.Empty)).ToList());
            counter = default(int);
            parameters.AddRange((from guest in guestList
                                 select new KeyValueParam(string.Format(" Room {0}: MembershipNumber", ++counter),
                                                         guest.GuestAccountNumber)).ToList());
            counter = default(int);
            parameters.AddRange((from guest in guestList
                                 select new KeyValueParam(string.Format(" Room {0}: BedTypePreference", ++counter),
                                                         guest.BedTypePreference)).ToList());
            for (int i = 0; i < guestList.Count; i++)
            {
                parameters.AddRange((from spRequest in guestList[i].SpecialRequests
                                     select new KeyValueParam(string.Format(" Room {0}: SPRequest", i),
                                                              string.Format("Type{0},Value{1}", spRequest.RequestType, spRequest.RequestValue))).ToList());
            }
            counter = default(int);
            parameters.AddRange((from guest in guestList
                                 select new KeyValueParam(string.Format(" Room {0}: AdditionalRequest", ++counter),
                                                          guest.AdditionalRequest)).ToList());

            counter = default(int);
            parameters.AddRange((from guest in guestList
                                 select new KeyValueParam(string.Format(" Room {0}: GuaranteeInfo", ++counter),
                                                          string.Format("{0},{1},{2}",
                                                          guest.GuranteeInformation != null ? guest.GuranteeInformation.GuranteeType.ToString() : null,
                                                          guest.GuranteeInformation != null ? guest.GuranteeInformation.ChannelGuranteeCode : string.Empty,
                                                          (guest.GuranteeInformation != null && guest.GuranteeInformation.CreditCard != null) ?
                                                            "XXXXXXXXXX" : string.Empty
                                                          ))).ToList());

            counter = default(int);
            parameters.AddRange((from guest in guestList
                                 select new KeyValueParam(string.Format(" Room {0}: D-Number", ++counter),
                                                          guest.D_Number)).ToList());

            return parameters;
        }

        #endregion
        /// <summary>
        /// Process payment for prepaid gauranteetypes, registers and redirects Nets terminal window.
        /// </summary>
        /// <param name="totalAmount"></param>
        /// <param name="guestInformationEntities"></param>
        /// <param name="hotelRoomRateEntities"></param>
        /// <param name="isSessionBooking"></param>
        /// <returns></returns>
        private string ProcessPaymentForPrepaidGuaranteeTypes(double totalPrepaidAmount, List<GuestInformationEntity> guestInformationEntities,
                                                             List<HotelRoomRateEntity> hotelRoomRateEntities, bool isSessionBooking,
                                                             double totalFlexAmount, string prepaidGuaranteeTypeCurrencyCode)
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            string netsTerminalURL = string.Empty;
            string imageURL = string.Empty;
            UrlBuilder url = null;
            if ((search != null) && !string.IsNullOrEmpty(search.SelectedHotelCode))
            {
                Reservation2SessionWrapper.PaymentGuestInformation = guestInformationEntities;
                Reservation2SessionWrapper.PaymentHotelRoomRateInformation = hotelRoomRateEntities;
                Reservation2SessionWrapper.PaymentIsSessionBooking = isSessionBooking;
                Reservation2SessionWrapper.PaymentOrderAmount = totalPrepaidAmount.ToString();
                Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                string roomDescriptionForNets = string.Empty;
                bool isAllRoomBookingsArePrepaid =
                RoomRateUtil.IsAllRoomBookingsArePrepaid(selectedRoomAndRatesHashTable);
                if (selectedRoomAndRatesHashTable != null)
                {
                    for (int roomCount = 0; roomCount < selectedRoomAndRatesHashTable.Count; roomCount++)
                    {
                        ExtractAndFormatePaymentInfoForNets(selectedRoomAndRatesHashTable[roomCount] as SelectedRoomAndRateEntity, roomCount,
                                                            ref roomDescriptionForNets, isAllRoomBookingsArePrepaid);
                    }
                }

                string savedCreditCardPanHash = string.Empty;
                HygieneSessionWrapper.IsEarlyBookingUsingStoredPanHashCC = false;

                if (rdoListFGPSavedCreditCards.SelectedIndex > 0)
                {
                    savedCreditCardPanHash = rdoListFGPSavedCreditCards.SelectedValue;
                    HygieneSessionWrapper.IsEarlyBookingUsingStoredPanHashCC = true;
                }
                HotelDestination hotel = m_AvailabilityController.GetHotelDestination(search.SelectedHotelCode);

                if (!string.IsNullOrEmpty(hotel.ImageURL))
                {
                    url = UrlBuilder.ParseUrl(hotel.ImageURL);
                    {
                        url.ConversionFormatType = ImageStoreNET.Classes.Data.ConversionFormatTypes.WebSafe;
                    }
                }

                UnBlockOnRdBtnChange();

                if (!BookingEngineSessionWrapper.IsModifyBooking)
                    CreateReservation(guestInformationEntities, hotelRoomRateEntities, isSessionBooking, false, null);

                int roomIndex = 0;
                foreach (GuestInformationEntity guestInformationEntity in guestInformationEntities)
                {
                    search.ListRooms[roomIndex].IsBlocked = true;
                    search.ListRooms[roomIndex].RoomBlockReservationNumber = guestInformationEntity.ReservationNumber;
                    search.ListRooms[roomIndex].RoomBlockReservationLegNumber = guestInformationEntity.LegNumber;
                    roomIndex++;
                }

                string creditCardUrl = FormatCreditCardIconsUrlForMerchant(hotel.OperaDestinationId);
                string arrivalDate = WebUtil.GetDayFromDate(search.ArrivalDate) + AppConstants.SPACE +
                                           Core.DateUtil.DateToDDMMYYYYString(search.ArrivalDate);
                string departureDate = WebUtil.GetDayFromDate(search.DepartureDate) + AppConstants.SPACE +
                                             Core.DateUtil.DateToDDMMYYYYString(search.DepartureDate);
                Reservation2SessionWrapper.PaymentTransactionId = m_PaymentController.Register(
                                                                                                search.SelectedHotelCode,
                                                                                                url != null ? string.Format("{0}{1}", ConfigurationManager.AppSettings["Scanwebcom"], hotel.ImageURL) : string.Empty,
                                                                                                totalPrepaidAmount.ToString(),
                                                                                                roomDescriptionForNets,
                                                                                                GetPaymentHeader(),
                                                                                                creditCardUrl,
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/paymentdeclaration"),
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selecthotel"),
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/selectrate"),
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/bookingdetails"),
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/payment"),
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/confirmation"),
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/ModifyStay/YourStay"),
                                                                                                (totalPrepaidAmount + totalFlexAmount).ToString(),
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Adult"),
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults"),
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child"),
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children"),
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/chargednow"),
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/bookingtotal"),
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/InclTaxesFees"),
                                                                                                prepaidGuaranteeTypeCurrencyCode,
                                                                                                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/chargedathotel"),
                                                                                                savedCreditCardPanHash,
                                                                                                arrivalDate,
                                                                                                departureDate);


                if (!string.IsNullOrEmpty(Reservation2SessionWrapper.PaymentTransactionId))
                {
                    string merchantID = string.Empty;
                    var hotelMerchantInfo = paymentController.GetMerchantByHotelOperaId(search.SelectedHotelCode);
                    if (hotelMerchantInfo != null)
                    {
                        merchantID = hotelMerchantInfo.MerchantID;
                    }
                    AppLogger.LogOnlinePaymentTransactionsInfoMessage(
                        string.Format("Redirected to Nets window with the TransactionID: {0}, MerchantID: {5} and the BookingID: {1} for the hotel: {2} from the WebServer: {3} at {4}.",
                        Reservation2SessionWrapper.PaymentTransactionId, ReservationNumberSessionWrapper.ReservationNumber, search.SelectedHotelCode, System.Environment.MachineName, DateTime.Now, merchantID));
                    
                    netsTerminalURL = m_PaymentController.GetNetsTerminalURL(Reservation2SessionWrapper.PaymentTransactionId, search.SelectedHotelCode);
                }
                else
                {
                    if (!string.IsNullOrEmpty(Reservation2SessionWrapper.NetsPaymentErrorMessage))
                    {
                        operaErrorMsg.InnerHtml = Reservation2SessionWrapper.NetsPaymentErrorMessage;
                        clientErrorDivBD.Visible = true;
                    }
                    else if (!string.IsNullOrEmpty(Reservation2SessionWrapper.NetsPaymentInfoMessage))
                    {
                        operaErrorMsg.InnerHtml = Reservation2SessionWrapper.NetsPaymentInfoMessage;
                        clientErrorDivBD.Visible = true;
                    }
                    else
                    {
                        UnBlockOnRdBtnChange();
                        WebUtil.ShowApplicationError(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/MerchantAuthenticationFailureHeader"),
                            WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/MerchantAuthenticationFailureMessage"));
                    }
                }
            }
            return netsTerminalURL;
        }

        /// <summary>
        /// Sets book button text based on the gauranteetypes of the selected rooms.
        /// </summary>
        /// <returns></returns>
        private void SetBookButtonText(bool isPaymentFallbackEnabled)
        {
            int roomCount = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count;
            SelectedRoomAndRateEntity roomAndRate = null;
            lnkBtnBook.Text = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/book");
            if (!BookingEngineSessionWrapper.IsModifyComboBooking)
            {
                for (int count = 0; count < roomCount; count++)
                {
                    roomAndRate =
                        HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[count] as SelectedRoomAndRateEntity;
                    if (RoomRateUtil.IsSaveRateCategory(roomAndRate.RateCategoryID) && !isPaymentFallbackEnabled)
                    {
                        lnkBtnBook.Text =
                            WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/continuetopayment");
                        lnkBtnBook.Attributes.Add("class", "submit buttonInner scansprite LangWidthBtn");
                        lnkBtnBook.Attributes.Add("onclick", "UpdateCheckoutSelection();");

                        break;
                    }
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roomEntity"></param>
        /// <param name="guestCount"></param>
        /// <param name="roomDescriptionForNets"></param>
        private void ExtractAndFormatePaymentInfoForNets(SelectedRoomAndRateEntity roomEntity, int guestCount, ref string roomDescriptionForNets,
                                                         bool isAllRoomBookingsArePrepaid)
        {
            string roomText = string.Empty;
            switch (guestCount)
            {
                case 0:
                    roomText = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room1");
                    break;
                case 1:
                    roomText = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room2");
                    break;
                case 2:
                    roomText = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room3");
                    break;
                case 3:
                    roomText = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room4");
                    break;
            }
            if (roomEntity != null)
            {
                roomEntity.RoomDescription = string.Format("{0} - {1}", roomText, roomEntity.SelectedRoomCategoryName);
                if (!isAllRoomBookingsArePrepaid)
                {
                    if (RoomRateUtil.IsSaveRateCategory(roomEntity.RateCategoryID))
                    {
                        roomDescriptionForNets =
                            string.Format("{0} <span> {1} - <strong style='color:Green'> {2} </strong></br></span>",
                                          roomDescriptionForNets, roomEntity.RoomDescription,
                                          WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/chargednow"));
                    }
                    else
                    {
                        roomDescriptionForNets =
                            string.Format("{0} <span> {1} - <strong style='color:Red'> {2} </strong> </br></span>",
                                          roomDescriptionForNets, roomEntity.RoomDescription,
                                          WebUtil.GetTranslatedText(
                                              "/bookingengine/booking/bookingdetail/chargedathotel"));
                    }
                }
                else
                {
                    roomDescriptionForNets = string.Format("{0} <span> {1} </br></span>",
                                             roomDescriptionForNets, roomEntity.RoomDescription);
                }
            }
        }
        /// <summary>
        /// Populates saved credit cards of the logged in user.
        /// </summary>
        private void PopulateFGPSavedCreditCards()
        {
            divSaveRateCodePayment.Visible = false;
            if (GenericSessionVariableSessionWrapper.IsPrepaidBooking && UserLoggedInSessionWrapper.UserLoggedIn &&
                (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null) &&
               (!string.IsNullOrEmpty(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID)))
            {
                Collection<CreditCardEntity> savedCreditCards =
                    m_NameController.FetchFGPCreditCards(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID);
                if ((savedCreditCards != null) && (savedCreditCards.Count > 0))
                {
                    divSaveRateCodePayment.Visible = true;
                    string useCreditCardText =
                        WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/usecreditcard");
                    foreach (CreditCardEntity savedCreditCard in savedCreditCards)
                    {
                        savedCreditCard.CardNumber = string.Format("{0} {1}", useCreditCardText,
                                                                   WebUtil.GetScanwebCCNoForDisplay(savedCreditCard.CardType, savedCreditCard.CardNumber));
                    }
                    CreditCardEntity useNewCreditCard = new CreditCardEntity(string.Empty,
                        string.Empty, WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/usenewcreditcard"), new ExpiryDateEntity(DateTime.Now.Month, DateTime.Now.Year));
                    savedCreditCards.Insert(0, useNewCreditCard);
                    rdoListFGPSavedCreditCards.DataSource = savedCreditCards;
                    rdoListFGPSavedCreditCards.DataTextField = "CardNumber";
                    rdoListFGPSavedCreditCards.DataValueField = "NameOnCard";
                    rdoListFGPSavedCreditCards.DataBind();
                    rdoListFGPSavedCreditCards.SelectedIndex = 0;
                    rdoListFGPSavedCreditCards.CssClass = "noBorderInput";

                    HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                    if (search != null)
                    {
                        if ((search.ListRooms != null) && (search.ListRooms.Count > 1))
                        {
                            divSaveRateCodePayment.Attributes.Add("class",
                                                                  "bknewcoment fltLft reterm paymentMethod mrgTop19");
                        }
                    }

                }
            }
        }

        private string FormatCreditCardIconsUrlForMerchant(string hotelOperaID)
        {

            MerchantProfileEntity merchInfo = m_ContentDataAccessManager.GetMerchantByHotelOperaId(hotelOperaID);

            if (merchInfo != null)
            {

                StringBuilder sbcreditCardIconURL = new StringBuilder();

                sbcreditCardIconURL.Append("<div style='width:700px;'>");
                sbcreditCardIconURL.Append("<div style='float:left; line-height: 25px;'>");
                sbcreditCardIconURL.AppendFormat("{0}&nbsp;&nbsp;&nbsp;</div>",
                                                 WebUtil.GetTranslatedText(
                                                     "/bookingengine/booking/bookingdetail/weaccept"));
                sbcreditCardIconURL.Append("<div style='float:left; width:400px;'>");
                if (merchInfo.VisaCard)
                {
                    sbcreditCardIconURL.Append(
                        "<div style='background:url({0}/Templates/Booking/Styles/Default/Images/NetsTerminal/CreditCardLogo.png) no-repeat;float:left;height:25px;width:43px;background-position: -162px -7px;'>&nbsp;</div>");
                }
                if (merchInfo.MasterCard)
                {
                    sbcreditCardIconURL.Append(
                        "<div style='background:url({0}/Templates/Booking/Styles/Default/Images/NetsTerminal/CreditCardLogo.png) no-repeat;float:left;height:25px;width:43px;background-position: -85px -7px;'>&nbsp;</div>");
                }
                if (merchInfo.AmexCard)
                {
                    sbcreditCardIconURL.Append(
                        "<div style='background:url({0}/Templates/Booking/Styles/Default/Images/NetsTerminal/CreditCardLogo.png) no-repeat;float:left;height:25px;width:43px;background-position: -206px -7px;width:32px'>&nbsp;</div>");
                }
                if (merchInfo.DankortCard)
                {
                    sbcreditCardIconURL.Append(
                        "<div style='background:url({0}/Templates/Booking/Styles/Default/Images/NetsTerminal/CreditCardLogo.png) no-repeat;float:left;height:25px;width:43px;background-position: -3px -7px;'>&nbsp;</div>");
                }
                if (merchInfo.DinersCard)
                {
                    sbcreditCardIconURL.Append(
                        "<div style='background:url({0}/Templates/Booking/Styles/Default/Images/NetsTerminal/CreditCardLogo.png) no-repeat;float:left;height:25px;width:43px;background-position: -238px -7px;width:38px'>&nbsp;</div>");
                }
                if (merchInfo.JCBCard)
                {
                    sbcreditCardIconURL.Append(
                        "<div style='background:url({0}/Templates/Booking/Styles/Default/Images/NetsTerminal/CreditCardLogo.png) no-repeat;float:left;height:25px;width:43px;background-position: -130px -7px;width:31px'>&nbsp;</div>");
                }

                if (sbcreditCardIconURL != null)
                {
                    sbcreditCardIconURL.Append("</div></div>");
                }

                return string.Format(sbcreditCardIconURL.ToString(), ConfigurationManager.AppSettings["Scanwebcom"]);
            }
            return string.Empty;
        }


        private string GetPaymentHeader()
        {
            string paymentHeaderPosition = string.Empty;
            switch (Utility.CurrentLanguageBranch)
            {
                case "en":
                    paymentHeaderPosition = "<div style='background:url({0}/Templates/Booking/Styles/Default/Images/NetsTerminal/PayWithCreditCard.png) no-repeat scroll -3px 0px transparent; height: 35px;'></div>";
                    break;
                case "sv":
                    paymentHeaderPosition = "<div style='background:url({0}/Templates/Booking/Styles/Default/Images/NetsTerminal/PayWithCreditCard.png) no-repeat scroll -3px -35px transparent; height: 35px;'></div>";
                    break;
                case "no":
                    paymentHeaderPosition = "<div style='background:url({0}/Templates/Booking/Styles/Default/Images/NetsTerminal/PayWithCreditCard.png) no-repeat scroll -3px -70px transparent; height: 35px;'></div>";
                    break;
                case "da":
                    paymentHeaderPosition = "<div style='background:url({0}/Templates/Booking/Styles/Default/Images/NetsTerminal/PayWithCreditCard.png) no-repeat scroll -3px -105px transparent; height: 35px;'></div>";
                    break;
                case "de":
                    paymentHeaderPosition = "<div style='background:url({0}/Templates/Booking/Styles/Default/Images/NetsTerminal/PayWithCreditCard.png) no-repeat scroll -3px -140px transparent; height: 35px;'></div>";
                    break;
                case "fi":
                    paymentHeaderPosition = "<div style='background:url({0}/Templates/Booking/Styles/Default/Images/NetsTerminal/PayWithCreditCard.png) no-repeat scroll -3px -175px transparent; height: 35px;'></div>";
                    break;
                case "ru-RU":
                    paymentHeaderPosition = "<div style='background:url({0}/Templates/Booking/Styles/Default/Images/NetsTerminal/PayWithCreditCard.png) no-repeat scroll -3px -210px transparent; height: 35px;'></div>";
                    break;
                default:
                    paymentHeaderPosition = "";
                    break;
            }
            return string.Format(paymentHeaderPosition, ConfigurationManager.AppSettings["Scanwebcom"]);
        }
    }
}