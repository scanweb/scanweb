<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCart.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ShoppingCart" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register src="ShoppingCartEditStayModule.ascx" TagName="ShoppingCartEditStayModule" TagPrefix="ShoppingCart" %>

<%--<input type="hidden" id="carryingPageType" runat="server" />--%>
<input type="hidden" id="removeRoomIndexHiddenField" runat="server" />
<input id="InvalidKidsAge" name="InvalidKidsAge" type="hidden" value='<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/invalidAge") %>'/>
 <input type="hidden" id="childrenAgeBedTypeError" name="childrenAgeBedTypeError" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name") %>' />
 <input type="hidden" id="childInAdultsBedType" name="childInAdultsBedType" value="<%=
                WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/sharingbed") %>" />    
 <!--Boxi : Added input control for special charecter validation in Booking Code field -->
 <input type="hidden" id="invalidBlockCodeChars" name="invalidBlockCodeChars" value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/noSpecialCaracters") %>" />    
 <!--Priya Singh: Error msg displayed when invalid block code entered.
    Artifact artf1217694 : Scanweb - Input control missing for incorrect block code  -->
 <input type="hidden" id="invalidBlockCode" name="invalidBlockCode" value="<%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/InvalidBlockCode") %>" />
<div id="yourStayMod05">
	<div class="regular">
	
		<ShoppingCart:ShoppingCartEditStayModule id="editStayCart" runat="server" />
		
		<div id="chandgedDetails" class="cnt changedDetail" runat="server">
			<p>  <a id="lnkSearchedCity" a target="_blank" class="stockText" runat="server" tabindex="380" visible = "false"></a>
			     <strong id = "searchedCity" class ="stockText" runat="server" visible="false"></strong>
			<br /><span id="labelArrivalDate" runat="server"></span> - <span id="labelDepartureDate" runat="server"></span></p>
			<asp:Image id ="imgHotel" runat="server" Height="131px" Width="200px" style="display:block"/>
		</div>
		
		<div id="roomWrap" class="cnt broadBrd noBordr" runat="server">
			<hr />
			<div class="cnt" id="roomWrapper">
			    <div id="shoppingCartRoomBoxes" runat="server"></div>
			    
			    <div id="addRoomBlock" runat="server" class="addRoomBlock" style="display:none;">
				    <div class="addRoomLink" style="display:block;">
    				    <a class="addRoom" tabindex="382" rel="addRoom" href="#"><%= WebUtil.GetTranslatedText("/bookingengine/booking/ModifyStay/AddAnotherRoom") %></a>
				    </div>
                    <div id="RegShoppingCartClientErrorDiv" class="errorText" runat="server">
                   </div>
		
				    <div class="thinBrdContainer grayBox" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnRoomSearch.ClientID %>')">
				        <div class="cnt">
				            <strong><%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room") %><span id="roomIndex" runat="server"></span></strong>
				            <div class="fltRt">
				            <div class="colm1">
                                <label><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults") %></label>
                                <asp:DropDownList ID="ddlAdultsPerNewRoom" TabIndex="7" runat="server" rel="adult">
                                 </asp:DropDownList><br class="clearAll" />
                                 <label for="adult">Age 13+</label>
                            </div>
                            <div class="colm2">
                                <%--<label><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children")%></label>--%>
                                <label><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/children") %></label>
                                <asp:DropDownList ID="ddlChildPerNewRoom" TabIndex="8" runat="server" CssClass="child" rel="childWrapper">
                                </asp:DropDownList>
                                <div class="clearAll"></div>
                                <label for="child">0-12</label>
                            </div>
                            </div>
                            <br class="clear" \>
                            
                            <div class="childSelct childWrapper">
                                <div class="fltLft roundMe grayBoxStay">
                                    <div id="chdBedWrapper">
									<div>
                                        <strong style="float:left;">  <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/childrenBeds") %></strong>
                                        <a title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/ChildrensBedToolTip") %>" class="help toolTipMe spriteIcon" href="#"></a>
                                    </div>
                                    <br class="clearAll" \>
                                    <div class="fltLft chdSelTtl">
                                        <p class="fltLft"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Age") %></p>
                                    </div>
                                    <div class="selHdr1 addRoomBedType">
                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType") %>
                                    </div>
                                    
                                    
                                    
									</div>
                                    <div class="colm5 padTop">
                                        <label class="fltLft">1)</label>
                                        <asp:DropDownList ID="childAgeforNewRoomChild1" TabIndex="6" runat="server" CssClass="fltLft subchild" rel="sh_subchild1">
                                        </asp:DropDownList>
                                        <div class="fltLft bedoutput21">
                                            <asp:DropDownList ID="bedTypeforNewRoomChild1" TabIndex="6" runat="server" CssClass="selBedTyp sh_subchild1">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="colm5">
                                        <label class="fltLft">2)</label>
                                        <asp:DropDownList ID="childAgeforNewRoomChild2" TabIndex="6" runat="server" CssClass="fltLft subchild" rel="sh_subchild2">
                                        </asp:DropDownList>
                                        <div class="fltLft bedoutput22">
                                            <asp:DropDownList ID="bedTypeforNewRoomChild2" TabIndex="6" runat="server" CssClass="selBedTyp sh_subchild2">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    
                                    <div class="colm5">
                                        <label class="fltLft">3)</label>
                                            <asp:DropDownList ID="childAgeforNewRoomChild3" TabIndex="6" runat="server" CssClass="fltLft subchild" rel="sh_subchild3">
                                            </asp:DropDownList>
                                            <div class="fltLft bedoutput23">
                                                <asp:DropDownList ID="bedTypeforNewRoomChild3" TabIndex="6" runat="server" CssClass="selBedTyp sh_subchild3">
                                                </asp:DropDownList>
                                            </div>
                                    </div>
                                    
                                    <div class="colm5">
                                        <label class="fltLft">4)</label>
                                            <asp:DropDownList ID="childAgeforNewRoomChild4" TabIndex="6" runat="server" CssClass="fltLft subchild" rel="sh_subchild4">
                                            </asp:DropDownList>
                                            <div class="fltLft bedoutput24">
                                                <asp:DropDownList ID="bedTypeforNewRoomChild4" TabIndex="6" runat="server" CssClass="selBedTyp sh_subchild4">
                                                </asp:DropDownList>
                                            </div>
                                    </div>
                                    
                                    <div class="colm5">
                                        <label class="fltLft">5)</label>
                                            <asp:DropDownList ID="childAgeforNewRoomChild5" TabIndex="7" runat="server" CssClass="fltLft subchild" rel="sh_subchild5">
                                            </asp:DropDownList>
                                            <div class="fltLft bedoutput25">
                                                <asp:DropDownList ID="bedTypeforNewRoomChild5" TabIndex="7" runat="server" CssClass="selBedTyp sh_subchild5">
                                                </asp:DropDownList>
                                            </div>
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <%--<div style="display: inline-block; left: 81px; position: relative; top: -181px;" class="selHdr2"><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/SelectBedType")%></div>--%>
                            <div class="schAnthrNow">
                                <a href="#" class="goBack" rel="gobackAdd"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Cancel") %></a>
                                <div class="actionBtn fltRt">
                                    <asp:LinkButton ID="btnRoomSearch" runat="server" CssClass="buttonInner" OnClick="btnSearchRoom_Click" rel="schAnthrNow2" ><span><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Continue") %></span></asp:LinkButton>
                                    <asp:LinkButton ID="spnRoomSearch" runat="server" CssClass="buttonRt scansprite" OnClick="btnSearchRoom_Click" rel="schAnthrNow2" />
                                </div>
                            </div>
                            
				        </div>
                    </div>
				</div>
			</div>
			
			
			<!--<div class="ft sprite"></div>-->

			<p class="chngdate fltLft" runat="server" id="editYourStayLink"><a class="change scansprite fltLft" tabindex="388" href="#" rel="changeQury"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Edityourstay") %></a></p>
			
			 
			
			<p id="goBackLink"><a class="goBack" href="" rel="gobackDet" runat="server" id="goBackLink" style="display:none"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Cancel") %></a></p>

			<!--<div id="yourStayAlertBox">
					<div class="threeCol alertContainer">
						<div class="hd sprite">&nbsp;</div>
						<div class="cnt">
							<p><span class="spriteIcon"></span>Changes are subject to availability. You may have to select a new rate if your preferred rate is not available. </p>
						</div>
						<div class="ft sprite"></div>
					</div>
				</div>-->
			
			<div id="totalRatesBlock" runat="server" class="totalRatesBlock">
			    <hr/>
<%--			    <div class="cntTotal">
				    <div class="colm1 bookDetail">
					    <p><strong><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Total")%></strong></p>
				    </div>
				    <div class="colm2 bookDetail">
					    <span><strong id="totalRate" runat="server"></strong></span>&nbsp;<span id="currencyCode" runat="server"></span>
					    <p><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/InclTaxesFees")%></p> 
					    <div id="bookNowBtnBlock" class="actionBtn fltRt bkingBtn" runat="server" style="display:none;">
					        <asp:LinkButton ID="LinkButton1" CssClass="buttonInner sprite" OnClick="OnBook_Click" runat="server"><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Continue")%></asp:LinkButton>
					        <asp:LinkButton ID="LinkButton2" CssClass="buttonRt sprite" OnClick="OnBook_Click" runat="server"></asp:LinkButton>					        
					    </div>
					    <!--<a href="#" title="Currency converter" class="currencyLeft spriteIcon">Currency converter</a>-->
				    </div>
				    <br class="clear"/>
			    </div>--%>
			    
			    <!-- R2.3 | Author: Sheril | Cart Total -->
			    <!-- BEGIN Cart Total -->
			    <div id="cartTotalDiv" class="cartTotal" runat="server">
			        <h5><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Total") %></h5>
			        <div class="cartAmountHolder">
			            <div class="cartAmount">
			                <span class="rate" id="totalRate" runat="server"></span>
			                <span class="currency" id="currencyCode" runat="server"></span>		                
                            <div class="cartTerms"> <span id="spnCartTerms" runat="server"></span></div>
                        </div>
                        <div class="clearAll"><!-- Clearing Float --></div>
			        </div>
			        
				    <div id="bookNowBtnBlock"  class="actionBtn" runat="server" style="display:none;">
				        <asp:LinkButton ID="LinkButton1" TabIndex="390" CssClass="buttonInner" OnClick="OnBook_Click" runat="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Continue") %></asp:LinkButton>
				        <asp:LinkButton ID="LinkButton2" CssClass="buttonRt scansprite" OnClick="OnBook_Click" runat="server"></asp:LinkButton>					        
				    </div>
				    <div class="clearAll"><!-- Clearing Float --></div>
			    </div>
			    <!-- END Cart Total -->
			    
			</div>
			<br class="clear"/>
		</div>
		
		<div class="gradientft sprite"></div>
		
	</div>
	
	<div id="removeRoomBtnBlock" runat="server">
        <div class="topCntRemoveRoom" style="display:none;">
            <div class="ratesOverLayCnt">
            </div>
            <div class="backgrd ratesOverLay">
                <div class="roundMe removAlertBox">
                <!--
                <div class="cancelBtn alertMrgBtm"><span><strong><WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RemoveRoomWarningHeader")%></strong></span></div>-->
                    <div class="cancelBtn alertMrgBtm">
                        <%--<a href="#" class="buttonInner sprite closeroom">Remove room</a>--%>                        
                        <asp:LinkButton ID="LinkButton3" CssClass="buttonInner closeroom" OnClick="OnRemoveRoom_Click" runat="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RemoveRoom") %></asp:LinkButton>                         
                        <asp:LinkButton ID="LinkButton4" CssClass="buttonRt" OnClick="OnRemoveRoom_Click" runat="server"></asp:LinkButton>                       
                        
                    </div>
                    <div class="usrBtn">
                        <a href="#" class="buttonInner goBack"><%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Cancel") %></a>
                        <a href="#" class="buttonRt"></a>
                        
                    </div>
                    <br class="clear" />
                </div>
            </div>
        </div>
     
    </div>
</div>

<div runat="server" id="divTripAdvisorContainer" class="regular tripadvisorselectRate">
<div class="tripadvisorsprt sprite"></div> 
<div class="cnt">
<div id="divTripAdvisorRating" runat="server"></div>
</div>
<div class="ft sprite"></div>
	</div>
<%--<script type="text/javascript" language="javascript">

$(document).ready(function(){

var pageType = $('#carryingPageType').val();
if(pageType == SelectRate)
{

}

}
</script>--%>



<div class="jqmWindow imgGalPopUp" id="tripAdvisoModal">	
	<div class="cnt">
		<a href="#" class="jqmClose scansprite blkClose iframeClose"></a>
		<div id="tripAdvisorIframe">
		</div>
	</div>	
</div>
 <script type="text/javascript">
 $(document).ready(function(){
 $('#tripAdvisoModal').jqm({trigger: 'a#lnkReviewCount'});
});

 </script>