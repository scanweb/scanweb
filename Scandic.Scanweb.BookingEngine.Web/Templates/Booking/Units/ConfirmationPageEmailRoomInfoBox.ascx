<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ConfirmationPageEmailRoomInfoBox.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ConfirmationPageEmailRoomInfoBox" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<table width="522" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="left" valign="top">
            <asp:Image ID="singleBorderBoxTop" runat="server" Style="display: block;" Width="522"
                Height="5" alt="" /></td>
    </tr>
    <tr>
        <td align="left" valign="top" style="border-left: 1px solid #d4d4d4; border-right: 1px solid #d4d4d4;
            width: 505px; padding: 15px 0;">
            <h2 style="color: #333333; font-size: 16px; margin: 0; line-height: normal; padding-left: 15px;
                border-bottom: 1px solid #cccccc; padding-bottom: 10px;" id="reservationNumberText">
                <span id="reservationNumberHeader" runat="server" >:</span><span id="reservationNumber" runat="server" ></span><br /><br />
                <span id="cancellationNumberHeader" runat="server" ></span><span id="cancellationNumber" runat="server" ></span>
            </h2>
            
            <!--DLRedirect link-->
            <table width="495" border="0" cellspacing="0" cellpadding="0" style="margin-left: 15px;" runat="server" id="deeplinkPerRoom">
                <tr>
                    <td height="30">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/ViewModifyReservationMessage")%>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <a id="ancViewModifyDLRedirectLink" runat="server"></a>
                    </td>
                </tr>
            </table>
            
            <table width="497" border="0" cellspacing="0" cellpadding="0" style="margin-left: 15px;
                border-bottom: 1px solid #cccccc;">
                <tr>
                    <td height="30">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" width="166" style="padding-bottom: 12px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/namehotelheader") %>
                        </strong>
                    </td>
                    <td align="left" valign="top" width="256">
                        <span id="hotelName" runat="server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin") %>
                        </strong>
                    </td>
                    <td align="left" valign="top">
                        <span id="checkInDate" runat="server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut") %>
                        </strong>
                    </td>
                    <td align="left" valign="top">
                        <span id="checkOutDate" runat="server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="border-top: 1px solid #cccccc;">
                        &nbsp;</td>
                </tr>
                <tr id="dNumberTR" runat="server" visible="false">
                    <td align="left" valign="top" width="166" style="padding-bottom: 12px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/DNumber") %>
                        </strong>
                    </td>
                    <td align="left" valign="top" width="156">
                        <span runat="server" id="dNumberSpan"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr id="roomTypeRow" runat="server" style="display:block">
                    <td align="left" valign="top" width="166" style="padding-bottom: 12px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/RoomType") %>
                        </strong>
                    </td>
                    <td align="left" valign="top" width="156">
                        <span runat="server" id="roomTypeName"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/sortRate") %>
                        </strong>
                    </td>
                    <td align="left" valign="top">
                        <span id="rateTypeID" runat="server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                    <strong>
                        <label id="priceInfo" runat="server">                           
                         </label>
                     </strong>
                    </td>
                    <td align="left" valign="top">
                        <span id="rateString" runat="server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 5px;">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
            <table width="497" border="0" cellspacing="0" cellpadding="0" style="margin-left: 15px;
                border-bottom: 1px solid #cccccc;" id="adultAndChildInfoTable" runat="server">
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 5px;">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" width="166" style="padding-bottom: 12px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AdultsChilds") %>
                        </strong>
                    </td>
                    <td align="left" valign="top" width="156">
                        <span id="adultAndChildrenDetail" runat="server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AdultBedType") %>
                        </strong>
                    </td>
                    <td align="left" valign="top">
                        <span runat="server" id="adultBedType"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <strong id="childBedType" runat="server">
                            
                        </strong>
                    </td>
                    <td align="left" valign="top" colspan="2">
                        <span id="childernBedType" runat="server"></span>
                    </td>
                </tr>
                    <tr>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr runat="server" id="otherPreferencesRow">
                        <td align="left" valign="top" style="padding-bottom: 12px;">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/OtherPreferences") %>
                            </strong>
                        </td>
                        <td align="left" valign="top">
                            <span id="otherPreferences" runat="server"></span>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" >
                            &nbsp;
                        </td>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr runat="server" id="otherSpecialServiceRow">
                        <td align="left" valign="top" style="padding-bottom: 12px;">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/specialservicerequests") %>
                            </strong>
                        </td>
                        <td align="left" valign="top">
                            <span id="specialServiceRequest" runat="server"></span>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" >
                            &nbsp;</td>
                        <td align="left" valign="top">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <!--start Additional Request-->
                    <tr runat="server" id="AditionalServiceRow">
                        <td align="left" valign="top" style="padding-bottom: 12px;">
                            <strong id="addReq" runat="server">
                            </strong>
                        </td>
                        <td align="left" valign="top">
                            <span id="additionalRequest" runat="server"></span>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            &nbsp;</td>
                        <td align="left" valign="top">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <!--start Additional Request-->
            </table>
            <table width="497" border="0" cellspacing="0" cellpadding="0" style="margin-left: 15px;" runat="server" id="contactInfoTable">
                <tr>
                    <td align="left" valign="top" >
                        &nbsp;</td>
                    <td align="left" valign="top" >
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px; font-size: 14px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/Contactperson") %>
                        </strong>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" width="166" style="padding-bottom: 12px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/Name") %>
                        </strong>
                    </td>
                    <td align="left" valign="top">
                        <span id="nameOfPerson" runat="server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            <%--    <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/townorcity") %>
                        </strong>
                    </td>
                    <td align="left" valign="top">
                        <span id="nameOfCityTown" runat="server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>--%>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/countryname") %>
                        </strong>
                    </td>
                    <td align="left" valign="top">
                        <span id="nameOfCountry" runat="server">Sweden</span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/addressemail") %>
                        </strong>
                    </td>
                    <td align="left" valign="top">
                        <span id="emailAddress" runat="server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <strong>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/PhoneNumber") %>
                        </strong>
                    </td>
                    <td align="left" valign="top">
                        <span id="phoneNumber" runat="server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <div id="divFGPNumber" runat="server" style="display: none;">
                            <strong>
                               <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/accountnumber") %>
                            </strong>
                        </div>
                    </td>
                    <td align="left" valign="top">
                        <span id="lblFGPNumber" runat="Server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <div id="divCardHolderName" runat="server" visible="false">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/cardholder") %>
                            </strong>
                        </div>
                    </td>
                    <td align="left" valign="top">
                        <span id="lblCardHolderName" runat="Server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <div id="divCreditCardNum" runat="server" visible="false">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/cardnumber") %>
                            </strong>
                        </div>
                    </td>
                    <td align="left" valign="top">
                        <span id="lblCreditCardNumber" runat="Server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                                                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <div id="divCardType" runat="server" visible="false">
                            <strong>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/cardtype") %>
                            </strong>
                        </div>
                    </td>
                    <td align="left" valign="top">
                        <span id="lblCardType" runat="Server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding-bottom: 12px;">
                        <div id="divExpiryDate" runat="server" visible="false">
                            <strong>
                               <%= WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/expirydate") %>
                            </strong>
                        </div>
                    </td>
                    <td align="left" valign="top">
                        <span id="lblExpiryDate" runat="Server"></span>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                     <td colspan="3" style="border-top: 1px solid #cccccc;">&nbsp;</td>
                </tr>
                
                <tr>
                    <td align="left" valign="top" colspan="3">
                        <div id="DifferentGuaranteeInformationText" visible="false" runat="server">
                          <%--  <h3 style="color: #333333; font-size: 16px; margin: 0; line-height: normal; padding-left: 15px;
                                border: none 0px; padding-bottom: 10px;">--%>
                                <h3 style="color: #333333; font-size: 16px; margin: 0; line-height: normal;
                                border: none 0px; padding-bottom: 10px;">
                               <%--Defect fix - artf1148220 - Bhavya - to fetch the header from the CMS--%>
                               <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/GuaranteeInformation")%>--%>
                               <asp:Label ID="GuaranteeInfoHeader" runat="server"></asp:Label>
                                <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/GuaranteeInformation")%>--%>
                            </h3>
                           
                             <%--<asp:Label ID="liGuaranteeInformation" runat="server" style="padding-bottom: 5px;"></asp:Label>--%>
                               <div style="margin-top: 5px;">
                                    <div style="padding-bottom: 5px;" runat="server" id="liGuaranteeInformation"></div>
                                </div>
                            
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    </tr>
    <tr>
        <td align="left" valign="top">
            <asp:Image ID="singleBorderBoxBottom" runat="server" Style="display: block;" Width="522"
                Height="5" alt="" /></td>
    </tr>
    <tr>
        <td align="left" valign="top" height="30" style="line-height: normal;">
        </td>
    </tr>
</table>
