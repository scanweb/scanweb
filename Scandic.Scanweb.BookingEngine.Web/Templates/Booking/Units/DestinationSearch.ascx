<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DestinationSearch.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.DestinationSearch" %>

<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Scandic.Scanweb.Core" %>

<script type="text/javascript" language="JavaScript">
//Please donot refactore by adding white spaces as it will go to client browser.
       if(!allExclusionStrings)
        {
            allExclusionStrings = new ExclusionStrings();<%
            List<string> excludeStrings = ExcludeStrings();
            foreach (string excludeString in excludeStrings){%>
                allExclusionStrings.addString('<%=excludeString%>');<%}%>
           }
        if(!masterCityList)
        {
                masterCityList = new Array();
                var eachCity;
                var eachHotel;
                <%List<CityDestination> cityList = FetchAllDestination();    
                foreach (CityDestination eachCityDest in cityList)
                {%>eachCity = new City('<%=eachCityDest.OperaDestinationId%>', '<%=eachCityDest.Name%>', '<%=eachCityDest.Name%>');                
                var altCityNames = '<%=eachCityDest.AltCityNames%>';                
                var altCityNamesSplited = altCityNames.split(",");
                eachCity.AddalternateCity(altCityNamesSplited);                
                //altCityNames = Trim(altCityNames);
                    <%List<HotelDestination> hotelList = eachCityDest.SearchedHotels;
                        foreach (HotelDestination eachHotelDest in hotelList){%>
                            eachHotel = new Hotel('<%=eachHotelDest.OperaDestinationId%>', '<%=eachHotelDest.SearchableString%>', '<%=eachHotelDest.Name%>','<%=eachHotelDest.PostalCity%>', eachCity);
                            eachCity.addHotel(eachHotel);<%}%>
                        masterCityList.push(eachCity);<%}%>
                    }
                    
          if(!nonBookableHotelList)
          {
            nonBookableHotelList = new Array();
            var city;
            var hotel;
            <%List<CityDestination> NonBookablecityList = FetchAllDestinationsWithNonBookable();
                foreach (CityDestination eachCityDest in NonBookablecityList)
                {%>city = new City('<%=eachCityDest.OperaDestinationId%>', '<%=eachCityDest.Name%>', '<%=eachCityDest.Name%>');
                <%
                List<HotelDestination> hotelList = eachCityDest.SearchedHotels;
                        foreach (HotelDestination eachHotelDest in hotelList){
                        
                            if(eachHotelDest !=null && !string.IsNullOrEmpty(eachHotelDest.OperaDestinationId))
                            { %>
                            hotel = new Hotel('<%=eachHotelDest.OperaDestinationId%>', '<%=eachHotelDest.SearchableString%>', '<%=eachHotelDest.Name%>', eachCity);
                            city.addHotel(hotel);
                            <%}
                            
                            }%>
                    nonBookableHotelList.push(city);<%}%>
                    }
                    
           if(!masterCountryList)
           {
                masterCountryList = new Array();
                var eachCountry1;
                var eachCity1;
                var eachHotel1;
                <%List<CountryDestinatination> countryList1 = FetchAllCountryDestinationsForAutoSuggest();    
                foreach (CountryDestinatination eachCountryDest1 in countryList1)
                {%>eachCountry1 = new Country('<%=eachCountryDest1.CountryCode%>', '<%=eachCountryDest1.Name%>', '<%=eachCountryDest1.Name%>');                
                  

				  <%List<CityDestination> cityList1 = eachCountryDest1.SearchedCities;
                        foreach (CityDestination eachCityDest1 in cityList1){%>
                            eachCity1 = new City('<%=eachCityDest1.OperaDestinationId%>', '<%=eachCityDest1.Name%>', '<%=eachCityDest1.Name%>','<%=eachCityDest1.SearchedHotels%>', eachCity1);                            
                    <%List<HotelDestination> hotelList1 = eachCityDest1.SearchedHotels;
                        foreach (HotelDestination eachHotelDest1 in hotelList1){%>
                            eachHotel1 = new Hotel('<%=eachHotelDest1.OperaDestinationId%>', '<%=eachHotelDest1.SearchableString%>', '<%=eachHotelDest1.Name%>','<%=eachHotelDest1.PostalCity%>', eachCity1);
                            eachCity1.addHotel(eachHotel1);													
							<%}%>	
							eachCountry1.addCity(eachCity1);		
							<%}%>								
                        masterCountryList.push(eachCountry1);							
				<%}%>						
                    }
</script>