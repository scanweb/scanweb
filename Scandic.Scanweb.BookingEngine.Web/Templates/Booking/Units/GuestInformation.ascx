<%@ Control Language="C#" AutoEventWireup="true" Codebehind="GuestInformation.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.GuestInformation" %>
<%@ Register TagPrefix="Scanweb" TagName="ConfirmHotelMap" Src="~/Templates/Scanweb/Units/Static/ConfirmationHotelMap.ascx" %>
<%@ Register Src="~/Templates/Booking/Units/ReservationInformationContainer.ascx"
    TagName="ReservationInformation" TagPrefix="Booking" %>
<%@ Register TagName="ConfirmRoomInfo" TagPrefix="Booking" Src="~/Templates/Booking/Units/ConfirmationRoomInfoContainer.ascx" %>
<%@ Register TagPrefix="Booking" TagName="FlyOut" Src="~/Templates/Booking/Units/BookingModuleFlyout.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Controller" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<link rel="stylesheet" type="text/css" media="print" href="/Templates/Scanweb/Styles/Default/Print.css?v=<%=CmsUtil.GetCSSVersion()%>" />
<link rel="stylesheet" type="text/css" media="print" href="/Templates/Booking/Styles/Default/print.css?v=<%=CmsUtil.GetCSSVersion()%>" />
<link rel="stylesheet" type="text/css" href="/Templates/Booking/Styles/Default/reservation2.0.css" />
<!--START: HEADER message -->
<% if ((Request.QueryString["command"] == null) || ((Request.QueryString["command"] != null) && (Request.QueryString["command"].ToString() == "isPDF")))
   { %>
<div id="Header">
    <h2 class="borderH2">
        <%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/Confirmation")%>
    </h2>
    
    <div class="socialmedia"> 
	 <span class="sharestay"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/shareyourstay")%></span>
        <a href="#" class="social facebook" tabindex="11" onclick="javascript:shareonfb(); return false;"><span>&nbsp;</span><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/facebookshare")%></a>
       <div id="ttbutton" style="display:inline;">
	   <a href="http://twitter.com/share?lang=en&text=<%= TwitterText%>&size=large" tabindex="10" class="social twitter popituptwt">
	  <span>&nbsp;</span><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/twittershare")%></a>
		</div>
        
    </div>
    
    <div id="ThankYouheader" runat="server">
        <span id ="modifyBookingMessage" runat="server" visible="false"></span>
    </div>
    <a id="lnkBtnPrinterfriendly" runat="server" href="#" target="_top" tabindex="15" class="print spriteIcon pntfdlylink"
        onmousedown="javascript:handleRightMouseClick(event);" onmouseup="javascript:handleRightMouseClick(event);"
        onclick="javascript:getPrinter(printerFrindlyURL);return false;">
        <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/Print")%>
    </a><a id="lnkBtnPrintReceipt" runat="server" href="#" target="_top" class="print spriteIcon pntfdlylink"
        visible="false" onmousedown="javascript:handleRightMouseClick(event);" onmouseup="javascript:handleRightMouseClick(event);"
        onclick="javascript:getPrinter(receiptURL);return false;">
        <%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/PrintReceipt")%>
    </a>
    <div class="actionBtn fltRt confirmationPrint" id="PrintDiv" runat="server">
        <asp:LinkButton ID="LnkBtnPrint" runat="server" CssClass="buttonInner cPrint"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/print")%></asp:LinkButton>
        <asp:LinkButton ID="spnBtnPrint" runat="server" CssClass="buttonRt scansprite"></asp:LinkButton>
    </div>
    <br class="clear" />
</div>
<!--END: HEADER message -->
<asp:PlaceHolder ID="EmailPlaceHolder" runat="server">
    <div id="confirmAlertMod" class="confirmAlertMod" runat="server">
        <div class="threeCol alertContainer" id="confirmationMessageSection" runat="server">
            <div class="hd sprite">
                &nbsp;</div>
            <div class="cnt sprite">
            
                <!-- Confirmation Alert Messages -->
                <div class="confirmationAlertMsg">
                    <!-- eMail Confirmation -->
                    <div class="confirmationAlertEmail">
                        <div id="divConfirmationReceiptAlert" runat="server">
                            <strong class="fltLft"><span id="lblReceiptMessage" runat="server"></span></strong>
                            <br />
                            <span id="lblReceiptEmail" runat="server"></span>
                        </div>
                        <div id="divConfirmationAlert" runat="server">
                            <strong class="fltLft"><span id="lblConfirmationMessage" runat="server"></span></strong>
                            <br />
                            <span id="lblEmailConfirmation" runat="server"></span>
                        </div>
                    </div>
                    <div class="confirmationAlertSMS" id="smsConfirmationSection" runat="server" visible="false">
                        <strong class="fltLft">
                            <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent")%></strong>
                        <br />
                        <span id="lblSMSConfirmation" runat="server" visible="false"></span>
                    </div>
                </div>
                
            </div>
            <div class="ft sprite">
            </div>
        </div>
    </div>
    <div class="clear">
        &nbsp;
    </div>
    <!-- Save credit card section -->
    <div class="confirmAlertMod decremrgTop" id="saveCreditCardSection" runat="server"
        visible="false">
        <div class="threeCol alertContainer">
            <div class="hd sprite">
                &nbsp;</div>
            <div class="cnt sprite">
                <table>
                    <tr>
                        <td valign="top" class="saveCCImageSection">
                            <img src="/templates/Booking/Styles/Default/images/reservation2.0/saveCC.png" />
                        </td>
                        <td class="saveCCMessageSection">
                            <strong>
                                <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/SaveQuestion")%></strong>
                            <div>
                                <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/SaveDescription")%>
                            </div>
                        </td>
                        <td class="saveCCButtonSection">
                            <div class="loginActBtn fltRt">
                                <a id="saveCreditCard" class="buttonInner fltLft" href="javascript:SaveCreditCardInfo();">
                                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/SaveCCInformation") %></a>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="ccSaveSuccessOverlay" class="jqmWindow hide">
                <h2 class="overlayheader">
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/CCSaved")%>
                    <span>
                        <img src="/templates/Booking/Styles/Default/Images/cc_confirm_icon.png" width="32"
                            height="33" alt="confirm" /></span>
                </h2>
                <div class="overlaymessage">
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/CCSavedDescription")%>
                </div>
                <div class="saveCCMessageOK">
                    <span class="jqmClose jqmClose_saveCreditCard">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/CCSavedOK")%>
                    </span>
                </div>
            </div>
            <div id="ccSaveFailOverlay" class="jqmWindow hide">
                <h2 class="overlayheader">
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/CCNotSaved")%>
                    <span>
                        <img src="/templates/Booking/Styles/Default/Images/cc_fail_icon.png" width="33" height="30"
                            alt="no confirm" /></span>
                </h2>
                <div class="overlaymessage">
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/CCSaveError")%>
                </div>
                <div class="saveCCMessageOK">
                    <span class="jqmClose jqmClose_saveCreditCard">
                        <%=WebUtil.GetTranslatedText("/bookingengine/booking/ManageCreditCard/CCNotSavedOK")%>
                    </span>
                </div>
            </div>
            <div class="ft sprite">
            </div>
        </div>
    </div>
</asp:PlaceHolder>

<script type="text/javascript" language="javascript">

      $(document).ready(function() {
	     $('.alertContainer').css('position','static');
      });
      
    function SaveCreditCardInfo() {
        
        $("#saveCreditCard").removeAttr("href");
        $("#saveCreditCard").attr("disabled", true);
        $("#saveCreditCard").addClass("saveCCDisabled");
        
        $.ajax({
            url: "<%=GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") %>",
            type: "POST",
            data: { methodToCall: 'SaveCreditCard'},
            cache: false,
            success: function(result) {
                if (result == "OK") {
	                $("#ccSaveSuccessOverlay").jqm();
	                $("#ccSaveSuccessOverlay").jqmShow();
                } else {
	                $("#ccSaveFailOverlay").jqm();
	                $("#ccSaveFailOverlay").jqmShow();
                }
            },
            error: function(result) {
	             $("#ccSaveFailOverlay").jqm();
                 $("#ccSaveFailOverlay").jqmShow();
            }
        });
    }
</script>

<div id="masterReserMod21" class="masterReserMod21" runat="server">
    <div class="greyRd">
        <Booking:ReservationInformation ID="ReservationInfo" runat="server">
        </Booking:ReservationInformation>
        <div class="ft">
            &nbsp;</div>
    </div>
</div>
<br class="clear" />
<!-- new code -->
<div id="OccupantsInfo">
    <Booking:ConfirmRoomInfo ID="ConfirmRoomInfo0" runat="server" visible="false" page-break-inside:avoid
        page-break-after:always>
    </Booking:ConfirmRoomInfo>
    <Booking:ConfirmRoomInfo ID="ConfirmRoomInfo1" runat="server" visible="false" page-break-inside:avoid
        page-break-after:always>
    </Booking:ConfirmRoomInfo>
    <Booking:ConfirmRoomInfo ID="ConfirmRoomInfo2" runat="server" visible="false" page-break-inside:avoid
        page-break-after:always>
    </Booking:ConfirmRoomInfo>
    <Booking:ConfirmRoomInfo ID="ConfirmRoomInfo3" runat="server" visible="false" page-break-inside:avoid
        page-break-after:always>
    </Booking:ConfirmRoomInfo>
</div>
<!--roomdetails Bhavya - End-->
<br class="clear" />
<!-- BEGIN New Implemention -->
<div id="aboutOurRates" runat="server">
    <table>
        <tbody>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" border="0" width="716">
                        <tbody>
                            <tr>
                                <td align="left" valign="top" class="abtPdfTop">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" style="width: 699px; padding: 15px 0px; border-left: 1px solid #d4d4d4;
                                    border-right: 1px solid #d4d4d4;">
                                    <h2 id="reservationNumberText" style="margin: 0; padding-left: 16px; padding-right: 16px;
                                        padding-bottom: 10px; font-size: 16px; line-height: normal; border-bottom: 1px solid #ccc;
                                        color: #333;">
                                        <asp:Label ID="aboutOurRateHeading" runat="server"></asp:Label>
                                    </h2>
                                    <table cellspacing="0" cellpadding="0" border="0" style="margin-left: 15px; margin-right: 15px;">
                                        <tbody>
                                            <tr>
                                                <td style="padding-top: 15px;">
                                                    <asp:Label ID="aboutOurRateDescription" runat="server"></asp:Label></td>
                                            </tr>
                                            <asp:Label ID="AboutRate" runat="server" class="aboutOurRate"></asp:Label>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" class="abtPdfBtm">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<!-- END New Implemention-->
<div class="guaranteTypeInfo" id="guaranteeInformation" runat="server" style="display: none;">
    <h3>       
        <asp:Label ID ="lblGuaranteeCommonHeader" runat="server"></asp:Label>
    </h3>
    <label id="lblGuaranteeOption" runat="server" class="lblGuarantee">
    </label>
</div>
<!--START:Info box -->
<div class="infoAlertBox" id="BookingNoteConfirmationDiv" visible="false" runat="server">
    <div class="threeCol alertContainer">
        <div class="hd sprite">
            &nbsp;</div>
        <div class="cnt sprite">
            <p class="scansprite">
                <span id="BookingNoteConfirmation" runat="server"></span>
            </p>
        </div>
        <div class="ft sprite">
        </div>
    </div>
</div>
<!--END:Info box -->
<div class="clear">
</div>
<div class="aboutHotel mrgTop19" style="page-break-inside: avoid;">
    <h2 class="abtht">
        <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/Abouthotel")%>
    </h2>
    <hr />
    <h3 class="mrgTop19" id="mapHeaderHotelname" runat="server">
    </h3>
    <span><a href="#" tabindex="28" id="linkFindAHotel" runat="server" style="color: #006699;">&nbsp;(<%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/ShowOnMap")%>)</a></span>
<%--Merchandising - Special Alert - Display special alert in booking flow--%>
			<!-- BEGIN Special Alert -->
		<div id="divSpAlertWrap" class="confirmalertwrapper" runat="server">
			<div class="splalertcontent" id ="divSpAlert" runat ="server">				
			</div>
		</div>
		<!-- END Special Alert -->
    <div class="roundMe grayBox locBox">
        <div class="locContBox fltLft">
            <h3>
                <%=WebUtil.GetTranslatedText("/bookingengine/booking/contactus/locationandcontactinfo")%>
            </h3>
            <p class="fixWidth">
                <strong id="lblHotelAddress" runat="server"></strong>
                <%--<a id="linkHotelAddress" runat="server" href="#" title="hotel name" class="noBg"></a>--%>
            </p>
            <p class="fixWidth">
                <span id="phoneContainer" runat="server"><strong>
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/telephone")%>:&nbsp;</strong> 
                    <span id="lblHotelPhoneNum" runat="server"></span></span>
                <br />
                <span id="faxContainer" runat="server"><strong>
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/contactus/scandicfaxnumber")%>:&nbsp;</strong>
                    <span id="lblHotelFaxNum" runat="server"></span></span>
                <br />
                <span id="emailContainer" runat="server"><strong>
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/email")%>:&nbsp;</strong> 
                    <span><a id="linkHotelEmail" runat="server" tabindex="32" href="#" class="emailerText">
                    </a></span><span id="lblHotelEmail" runat="server" visible="false"></span></span>
                <br />
                <span id="customerCareNumberContainer" runat="server"><strong>
                    <%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Content/CentralReservationNumber")%>:&nbsp;</strong> 
                    <span id="lblCustomerCareNumber" runat="server"></span></span>
            </p>
            <p class="fixWidth">
                <strong>
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Coordinates")%>:&nbsp;</strong>
                    
                <br />
                <strong>
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Latitude")%>
                    &nbsp;-&nbsp; </strong><span id="lblLatitude" runat="server"></span>
                <br />
                <strong>
                    <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Longitude")%>
                    &nbsp;-&nbsp; </strong><span id="lblLongitude" runat="server"></span>
            </p>
        </div>
        <div class="locContBox fltLft hotelInformationBookingDetail">
            <h3><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/OverView")%></h3>
            <p id="noOfRoomsContainer" runat="server">
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/NoOfRooms")%></div>
                <div id="lblNoOfRooms" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>
            </p>
            <p id="noOfNonSmokingRoomsContainer" runat="server">
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/NonSmokingRooms")%></div>
                <div id="lblNoOfNonSmokingRooms" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>
            </p>
            <p>
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RoomsForDisabled")%></div>
                <div id="lblRoomsForDisabledAvailable" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>                
            </p>
            <p>
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RelaxCenter")%></div>
                <div id="lblRelaxCenterAvailable" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>                
            </p>
            <p>
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RestaurantBar")%></div>
                <div id="lblRestaurantBar" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>
            </p>
            <p>
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Garage")%></div>
                <div id="lblGarage" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>                
            </p>
            <p>
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/OutdoorParking")%></div>
                <div id="lblOutdoorParking" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>                
            </p>
            <p>
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Shop")%></div>
                <div id="lblShop" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>                
            </p>
            <p>
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/MeetingFacilities")%></div>
                <div id="lblMeetingFacilities" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>                
            </p>
            <p id="ecoLabeledContainer" runat="server">
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/EcoLabeled")%></div>
                <div id="lblEcoLabeled" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>                
            </p>
            <p>
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceCentre")%></div>
                <div id="lblDistanceCentre" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>                
            </p>
            <p id="airportDistanceContainer" runat="server">
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceAirport")%>&nbsp;<span id="lblAirportName" runat="server"></span></div>
                <div id="lblDistanceAirport" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>
            </p>
            <p id="trainStationDistContainer" runat="server">
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceTrain")%></div>
                <div id="lblDistanceTrain" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>                
            </p>
            <p>
                <div class="fltLft"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/WirelessInternet")%></div>
                <div id="lblWirelessInternet" runat="server" class="fltRt"></div>
                <div class="clearAll"><!-- Clearing All --></div>
            </p>
        </div>
        <div class="locLstLink">
            <a id="linkFullDescription" tabindex="35" runat="server" href="#" target="_top" class="fltLft">
                <%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/FullDescription")%>
            </a>
        </div>
    </div>
    <%-- Important Info and didYou know should be displayed only in confirmation email--%>
   </div>

<div class="actionBtn fltRt mrgTop19" style="clear:both;" id="divanatherHotel" runat="server">
 <asp:LinkButton TabIndex="40" ID="btnBookAnotherHotel" runat="server" CssClass="buttonInner"><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AddAnotherHotel")%></asp:LinkButton>
    <asp:LinkButton ID="spnBookAnotherHotel" runat="server" CssClass="buttonRt scansprite"></asp:LinkButton>

    <%--<asp:LinkButton TabIndex="40" ID="btnAddAnother" runat="server" CssClass="buttonInner jqModal"><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/AddAnotherHotel")%></asp:LinkButton>
    <asp:LinkButton ID="spnAddAnother" runat="server" CssClass="buttonRt scansprite"></asp:LinkButton>--%>
</div>


<%} %>
<script language="javascript" type="text/javascript">

var printerFrindlyURL = '<%= requestURL%>';  
    var receiptURL = '<%= receiptURL%>';
   
   $(document).ready(function() {
     
           $(document).bind("contextmenu",function(e){
              return false;
       });
   });  
   
</script>

    <div id='fb-root'></div>
    <p id='msg'></p>
    <script src='https://connect.facebook.net/en_US/all.js'></script>
  
    <script>
	function shareonfb(){
		var hoteldescription = '<%= HotelDestination != null ? HotelDestination.HotelDescription : string.Empty%>';
		hoteldescription = unescape(hoteldescription);
		var link= '<%= HostName%>' + '<%= HotelDestination != null ? HotelDestination.HotelPageURL : string.Empty%>';
		var picture= 'www.scandichotels.com' + '<%= HotelDestination != null ? HotelDestination.ImageURL : string.Empty%>';
		var name= '<%= HotelDestination != null ? HotelDestination.Name : string.Empty%>';
		var caption= ' ';
		var description= hoteldescription;
		var redirect_uri='https://scandicshare.herokuapp.com/';
		var fbsharelink="https://www.facebook.com/dialog/feed?app_id=502926949724064&link="+link+"&picture="+picture+"&name="+name+"&caption="+caption+"&description="+description+"&redirect_uri="+redirect_uri;
		var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = this.href,
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;
		window.open(fbsharelink, 'twitte', opts);
		
//	win.focus();
}
    
        !function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (!d.getElementById(id)) {
                js = d.createElement(s);
                js.id = id;
                js.src = "https://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);
            }
        }
       (document, "script", "twitter-wjs");
    
    </script>    
<script type="text/javascript">
    $('.popituptwt').click(function(event) {
    var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = this.href,
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;

    window.open(url, 'twitte', opts);

    return false;
  });</script>

   <% if (Request.QueryString["command"] != null && Request.QueryString["command"].ToString() == "print")
   { %>
<div id="printerFriendlySection" runat="server" visible="false">
    <div id="confirmationPrint">
         <div class="clearFix" id="pfPrintDiv" runat="server" visible="false">
            <!-- Print Button -->
            <div class="actionBtn fltRt">
                <asp:LinkButton ID="pfLnkBtnPrint" runat="server" CssClass="buttonInner cPrint"><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/print")%></asp:LinkButton>
                <asp:LinkButton ID="pfSpnBtnPrint" runat="server" CssClass="buttonRt scansprite"></asp:LinkButton>
            </div>
        </div>
        <!-- Title -->
        <h1><%=WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/Confirmation")%></h1>
        <div id="pfMasterReserMod21" runat="server">
            <Booking:ReservationInformation ID="pfReservationInfo" runat="server" />
        </div>
        
        <div id="pfOccupantsInfo">
            <Booking:ConfirmRoomInfo ID="pfConfirmRoomInfo0" runat="server" visible="false"/>
            <Booking:ConfirmRoomInfo ID="pfConfirmRoomInfo1" runat="server" visible="false" />
            <Booking:ConfirmRoomInfo ID="pfConfirmRoomInfo2" runat="server" visible="false" />
            <Booking:ConfirmRoomInfo ID="pfConfirmRoomInfo3" runat="server" visible="false" /> 
        </div>
        
        
        <!-- About our Rates -->
        <div class="section rateInfoWrap">
			<h3><span id="pfAboutOurRateHeading" runat="server"></span></h3>
			<div class="abtRateInfo">
				<span ID="pfAboutOurRateDescription" runat="server"></span><br />
			</div>
			<asp:Label ID="pfAboutRate" runat="server"></asp:Label>
		</div>	
			
 
        <!-- Gaurentee and Cancellation Policy -->
		<div class="section gaurentee">
		    <div id="pfGuaranteeInformation" runat="server" style="display:none">
			    <h3><span id ="pfLblGuaranteeCommonHeader" runat="server"></span></h3>
			    <div class="rateInfo abc" id="pfGuaranteeOptionDiv" runat="server">
				    <span id="pfLblGuaranteeOption" runat="server"></span>
			    </div>
		    </div>
		
		    <!--START:Info box -->
		    <div id="pfBookingNoteConfirmationDiv" visible="false" runat="server">
			    <div class="clearFix">
			        
			            <img id="imgPleaseNote" src="/Templates/Booking/Styles/Default/Images/reservation2.0/pleaseNote.gif" alt=""  runat="server"/>
			            <div id="pleaseNoteLogo" class="pleaseNoteLogo" runat="server"></div>
			     
			        <h6><%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/PleaseNote")%></h6>
			    </div>    
		        <div class="instruction">					
		            <span id="pfBookingNoteConfirmation" runat="server"></span>
	            </div>
		    </div>
		</div>
		    <!--END:Info box -->	
        
        <!-- Hotel Information -->
		<div class="section hotelInfo">
			<h3> <%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/Abouthotel")%>:&nbsp;<span id="pfHeaderHotelname" runat="server"></span></h3>
			<div class="row clearFix">
				<div class="col1">
					<h5><%=WebUtil.GetTranslatedText("/bookingengine/booking/contactus/locationandcontactinfo")%></h5>
					<span id="pfLblHotelAddress" runat="server"></span>
 
					<p>
					    <span id="pfPhoneContainer" runat="server">
					        <strong><%=WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/telephone")%>:&nbsp;</strong><span id="pfLblHotelPhoneNum" runat="server"></span>
					    </span><br />
					    <span id="pfFaxContainer" runat="server">
					        <strong><%=WebUtil.GetTranslatedText("/bookingengine/booking/contactus/scandicfaxnumber")%>:&nbsp;</strong> <span id="pfLblHotelFaxNum" runat="server"></span>
					    </span><br />
					    <span id="pfEmailContainer" runat="server">
					        <strong><%=WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/email")%>:&nbsp;</strong> <span id="pfLblHotelEmail" runat="server" visible="false"></span>
					    </span><br />
					    <span id="pfCustomerCareNumberContainer" runat="server">
	                        <strong><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Content/CentralReservationNumber")%>:&nbsp;</strong><span id="pfLblCustomerCareNumber" runat="server" ></span> 
                        </span>
					</p>
 
					<strong><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Coordinates")%>:&nbsp;</strong><br />
					<%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Latitude")%>
                    &nbsp;-&nbsp; <span id="pfLblLatitude" runat="server"></span><br />
					 <%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Longitude")%>
                    &nbsp;-&nbsp; <span id="pfLblLongitude" runat="server"></span>
				</div>
				<div class="col2">
					<h5><%=WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/OverView")%></h5>
					<table>
						<tbody>
							<tr id="pfNoOfRoomsContainer" runat="server">
								<td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/NoOfRooms")%></td>
								<td class="hotelInfoItem2"><span id="pfLblNoOfRooms" runat="server"></span></td>
							</tr>
							<tr id="pfNoOfNonSmokingRoomsContainer" runat="server">
								<td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/NonSmokingRooms")%></td>
								<td class="hotelInfoItem2"><span id="pfLblNoOfNonSmokingRooms" runat="server"></span></td>
							</tr>
							<tr>
								<td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RoomsForDisabled")%></td>
								<td class="hotelInfoItem2"><span id="pfLblRoomsForDisabledAvailable" runat="server"></span></td>
							</tr>
							<tr>
								<td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RelaxCenter")%></td>
								<td class="hotelInfoItem2"><span id="pfLblRelaxCenterAvailable" runat="server"></span></td>
							</tr>
							<tr>
								<td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RestaurantBar")%></td>
								<td class="hotelInfoItem2"><span id="pfLblRestaurantBar" runat="server"></span></td>
							</tr>
							<tr>
								<td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Garage")%></td>
								<td class="hotelInfoItem2"><span id="pfLblGarage" runat="server"></span></td>
							</tr>
							<tr>
								<td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/OutdoorParking")%></td>
								<td class="hotelInfoItem2"><span id="pfLblOutdoorParking" runat="server"></span></td>
							</tr>
							<tr>
								<td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Shop")%></td>
								<td class="hotelInfoItem2"><div id="pfLblShop" runat="server"></div></td>
							</tr>
							<tr>
								<td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/MeetingFacilities")%></td>
								<td class="hotelInfoItem2"><div id="pfLblMeetingFacilities" runat="server"></div></td>
							</tr>
							<tr id="pfEcoLabeledContainer" runat="server">
								<td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/EcoLabeled")%></td>
								<td class="hotelInfoItem2"><div id="pfLblEcoLabeled" runat="server"></div></td>
							</tr>
							<tr>
								<td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceCentre")%></td>
								<td class="hotelInfoItem2"><div id="pfLblDistanceCentre" runat="server"></div></td>
							</tr>
							<tr id="pfAirportDistanceContainer" runat="server">
							    <td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceAirport")%>:&nbsp;<span id="pfLblAirportName" runat="server"></span></td>
							    <td class="hotelInfoItem2"><div id="pfLblDistanceAirport" runat="server"></div></td>
							</tr>
							<tr id="pfTrainStationDistContainer" runat="server">
                                <td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceTrain")%></td>
                                <td class="hotelInfoItem2"><div id="pfLblDistanceTrain" runat="server"></div></td>
							</tr>
							<tr>
                                <td class="hotelInfoItem1"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/WirelessInternet")%></td>
                                <td class="hotelInfoItem2"><div id="pfLblWirelessInternet" runat="server"></div></td>
							</tr>
							
						</tbody>
					</table>
				</div>
            </div>
            </div>
        <div id="showImportantAndDidUKnwInfo" class="section fltLft mrgTop19" runat="server" style="display:none;">
        <table width="522" border="0" cellspacing="0" cellpadding="0">
            <tbody id="rowGroupForConfirmation" runat="server" style="display: block">
            <tbody runat="server" id="otherImportantInfoRow">
                                <tr>
                                    <td align="left" valign="top">
                                        <h3 style="color: #333333; font-size: 16px; margin: 0; line-height: normal; border: none 0px;
                                            padding-bottom: 10px;">
                                            <div runat="server" id="OtherImportantInformationHeader"></div>
                                        </h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div runat="server" id="OtherImportantInformation">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        &nbsp;</td>
                                </tr>
                            </tbody>
                            <style type="text/css">
                            div#DidYouKnowHeader li { line-height: 12px;
                                    list-style-position: inside;
                                    margin: 0;
                                    padding: 0 0 1px;                                                  
                                                    }
                             h3#DidYouKnowHeader   {font-size: 14px; line-height: 16px;  padding: 0 0 8px;}
                             img {  border: 0 none; }
                            </style>
            <tbody id="didYouKnowRow" runat="server">
                <tr>
                    <td align="left" valign="top">
                        <h3 runat="server" id="DidYouKnowHeader" style="border-bottom: 1px solid #777777;">
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div runat="server" id="DidYouKnow">
                        </div>
                    </td>
                </tr>
            </tbody>
        </tbody>
        </table>
    </div>
    
    
      
    </div>
</div>
    <%} %>
<asp:PlaceHolder ID="TrivagoPlaceHolder" Visible="false" runat="server">
</asp:PlaceHolder>