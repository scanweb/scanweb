//  Description					: Code Behind for Name OWS Calls.			          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Priya Singh 	                                          //
//  Author email id				:                           							  //
//  Creation Date				: 13th November  2007									  //
//	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using EPiServer.Globalization;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using System.Configuration;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using System.Linq;
using Scandic.CryptorEngine;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind for Name OWS Calls.
    /// </summary>
    public partial class EnrollGuestProgram : EPiServer.UserControlBase, INavigationTraking
    {
        #region Page_Load

        /// <summary>
        /// Page Load Method of EnrollGuestProgram Control 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (!IsPostBack)
                {
                    Logout();

                    if (FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.EnrolmentThroughCampaignLandingPage)
                    {
                        ShowOrHideFastTrackCodeDiv();
                    }

                    SetSortDropDowns();
                    SetCountryPhoneCodeByLocale();
                    BtnSend.Text = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/send");

                    InformationController informationCtrl = new InformationController();
                    Dictionary<string, string> nameTitleMapOpera = informationCtrl.GetConfiguredTitles(LanguageConstant.LANGUAGE_GERMAN_CODE);

                    ListItem selectItem = new ListItem(AppConstants.TITLE_DEFAULT_VALUE, AppConstants.TITLE_DEFAULT_VALUE);
                    ddlTitle.Items.Add(selectItem);

                    foreach (string key in nameTitleMapOpera.Keys)
                    {
                        ddlTitle.Items.Add(new ListItem(nameTitleMapOpera[key].ToString(), key));
                    }
                    ListItem selectedDefaultItem = ddlTitle.Items.FindByValue(AppConstants.TITLE_DEFAULT_VALUE);
                    if (selectedDefaultItem != null)
                    {
                        ddlTitle.SelectedItem.Selected = false;
                        selectedDefaultItem.Selected = true;
                    }
                    if (!string.Equals(LanguageSelection.GetLanguageFromHost(), LanguageConstant.LANGUAGE_GERMAN, StringComparison.InvariantCultureIgnoreCase))
                    {
                        dvNameTitle.Attributes.Add("style", "display:none");
                        lblNameTitle.Attributes.Add("style", "display:none");
                    }
                }
            }
        }

        #endregion Page_Load

        #region BtnSend_Click

        /// <summary>
        /// Button Click method to call the RegisterUser Controller method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnSend_Click(object sender, EventArgs e)
        {
            try
            {
                clientErrorDivEL.InnerHtml = "";

                UserProfileEntity userProfileEntity = CaptureGuestProfileFromUI();
                PreferredLanguageForEmailSessionWrapper.UserPreferredLanguage = ddlPreferredLang.Text;
                UserNavTracker.TrackAction(this, "Enroll User");
                //WebUtil.ApplicationErrorLog(new Exception("Error in Enroll"));
                //Response.End();
                NameController nameController = new NameController();
                nameController.PrimaryLangaueID = ddlPreferredLang.Text;
                string ConfirmationID = nameController.RegisterUser(userProfileEntity);

                if (FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.EnrolmentThroughCampaignLandingPage)
                {
                    UpgradeToFastTrackEnrolment(ConfirmationID, txtEnrollPassword.Value);
                }

                if (!string.IsNullOrEmpty(ConfirmationID))
                {
                    if (userProfileEntity.EmailID != null)
                    {
                        SendConfirmationMailToUser(ConfirmationID);
                    }
                    if (Reservation2SessionWrapper.IsUserClickTopRightButtonForRegistration)
                    {
                        IsEnrolledUserSessionWrapper.IsEnrolledUserFromTopRightButton = true;
                        Reservation2SessionWrapper.IsUserClickTopRightButtonForRegistration = false;
                    }
                    else
                    {
                        IsEnrolledUserSessionWrapper.IsEnrolledUserFromScandicFriends = true;
                    }
                    IsEnrolledUserSessionWrapper.IsEnrolledUser = true;
                    LoginToSite(ConfirmationID, txtEnrollPassword.Value);
                }
                else
                {
                    WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                                 WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                }
            }
            catch (OWSException owsException)
            {
                AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);
                string exceptionMessage = WebUtil.GetTranslatedText(owsException.TranslatePath);
                if (owsException.Message == AppConstants.SYSTEM_ERROR)
                {
                    WebUtil.ShowApplicationError(WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR_HEADER),
                                                 WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR));
                }
                else
                {
                    clientErrorDivEL.InnerHtml = owsException.Message;
                }
            }
            catch (Exception ex)
            {
                WebUtil.ApplicationErrorLog(ex);
            }
        }

        #endregion BtnSend_Click

        #region Private Methods

        protected string GetDomain()
        {
            string domain = Request.Url.Host;
            int iPos = domain.LastIndexOf('.') + 1;
            return domain.Substring(iPos, domain.Length - iPos).ToUpper();
        }

        private void SetCountryPhoneCodeByLocale()
        {
            Dictionary<string, string> dicCountryPhone = new Dictionary<string, string>();
            string currLocale = GetDomain(); // GetCurrentLocale();
            if (ConfigurationManager.AppSettings["CountryPhoneCodeMapping"] != null)
            {
                string[] countryPhoneMapping = ConfigurationManager.AppSettings["CountryPhoneCodeMapping"].Split(',');
                Dictionary<string, string> cntryPhnMapping = null;
                //SE|46,FI|358,NO|47,DK|45,RU|7,DE|49
                if ((countryPhoneMapping != null) && (countryPhoneMapping.Length > 0))
                {
                    cntryPhnMapping = new Dictionary<string, string>();
                    foreach (string mapping in countryPhoneMapping)
                    {
                        string[] ccType = mapping.Split('|');
                        if (!cntryPhnMapping.ContainsKey(ccType[0].ToUpper()))
                            cntryPhnMapping.Add(ccType[0].ToUpper(), ccType[1]);
                    }
                }

                if (cntryPhnMapping != null && cntryPhnMapping.ContainsKey(currLocale.ToUpper()))
                {
                    string strCountryPhoneCode = string.Empty;
                    if (cntryPhnMapping.ContainsKey(currLocale.ToUpper()))
                        strCountryPhoneCode = cntryPhnMapping[currLocale.ToUpper()].ToString();
                    ListItem selectedDefaultItem = ddlCountry.Items.FindByValue(currLocale.ToUpper());
                    if (selectedDefaultItem != null)
                    {
                        ddlCountry.SelectedItem.Selected = false;
                        selectedDefaultItem.Selected = true;
                    }

                    ListItem selectedTelItem = ddlTel2.Items.FindByValue(strCountryPhoneCode);
                    if (selectedTelItem != null)
                    {
                        ddlTel2.SelectedItem.Selected = false;
                        selectedTelItem.Selected = true;
                    }
                }
            }
        }

        private string GetCurrentLocale()
        {
            IFormatProvider cultureInfo = new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
            string currentLocale = cultureInfo.ToString().Substring(0, 2).ToUpper();

            return currentLocale;
        }

        #region UpgradeToFastTrackEnrolment

        /// <summary>
        /// This method upgrades a newly enrolled member with the promo code.
        /// </summary>
        /// <param name="membershipNo"></param>
        /// <param name="password"></param>
        private void UpgradeToFastTrackEnrolment(string membershipNo, string password)
        {
            string membershipNameId = string.Empty;
            string membershipOperaId = string.Empty;
            string fastTrackCode = string.Empty;
            NameController nameController = new NameController();

            FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage = string.Empty;

            try
            {
                fastTrackCode = txtCampaignCode.Value.ToUpper().Trim();

                membershipNameId = Utility.GetMembershipNameId(membershipNo, password);
                membershipOperaId = Utility.GetMembershipOperaId(membershipNameId);

                nameController.UpdateMembershipWithPromoCode(membershipOperaId, fastTrackCode);

                FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage =
                    WebUtil.GetTranslatedText(FastTrackEnrolmentMessageConstants.SuccessfulSubscription);
            }
            catch (OWSException owsException)
            {
                AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);

                if (owsException.TranslatePath == AppConstants.SITE_WIDE_ERROR)
                {
                    FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage =
                        WebUtil.GetTranslatedText(FastTrackEnrolmentMessageConstants.PromoCodeGenericMessageForNewUser);
                }
                else
                {
                    FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage =
                        WebUtil.GetTranslatedText(owsException.TranslatePath);
                }
            }
            catch (BusinessException busEx)
            {
                AppLogger.LogCustomException(busEx, AppConstants.BUSINESS_EXCEPTION);
                FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage = busEx.Message;
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
                FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.ErrorMessage = ex.Message;
            }

            FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.EnrolmentThroughCampaignLandingPage = false;
        }

        #endregion UpgradeToFastTrackEnrolment

        #region Logout

        /// <summary>
        /// This method will clear up the session and make the user log off. 
        /// </summary>
        private void Logout()
        {
            NameController nameController = new NameController();
            nameController.Logout();

            LoginStatus ctrl = (LoginStatus)this.Page.Master.FindControl("MenuLoginStatus");
            if (ctrl != null)
            {
                ctrl.UpdateLoginStatus(false);
            }
        }

        #endregion LogOut

        #region LoginToSite

        /// <summary>
        /// </summary>
        /// <param name="?"></param>
        /// <param name="?"></param>
        private void LoginToSite(string userName, string password)
        {
            string MembershipID = string.Empty;

            try
            {
                MembershipID = Utility.GetMembershipNameId(userName, password);

                NameController nameController = new NameController();
                nameController.PopulateSession(MembershipID, userName);

                Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MYACCOUNT_BOOKING_PAGE), false);
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx);
                Response.Redirect("/.", false);
            }
        }

        #endregion LoginToSite

        #region SendConfirmationMailToUser

        /// <summary>
        /// This method sends confirmation email to the user enrolled.
        /// </summary>
        /// <param name="registrationID"></param>
        private void SendConfirmationMailToUser(string registrationID)
        {
            try
            {
                Dictionary<string, string> emailEnrollMap = new Dictionary<string, string>();
                emailEnrollMap[CommunicationTemplateConstants.LOGOPATH] = AppConstants.LOGOPATH;
                emailEnrollMap[CommunicationTemplateConstants.CONFIRMATION_NUMBER] = registrationID;
                emailEnrollMap[CommunicationTemplateConstants.PASSWORD] = txtEnrollPassword.Value.Trim();
                emailEnrollMap[CommunicationTemplateConstants.TITLE] = Utility.GetNameTitle(ddlTitle.Text).Trim();

                emailEnrollMap[CommunicationTemplateConstants.FIRST_NAME] = txtFName.Value.Trim();
                emailEnrollMap[CommunicationTemplateConstants.LASTNAME] = txtLName.Value.Trim();
                emailEnrollMap[CommunicationTemplateConstants.EMAIL] = txtEmail1.Value.Trim();
                emailEnrollMap[CommunicationTemplateConstants.ADDRESS_LINE1] = txtAddressLine1.Value.Trim();
                if (!string.IsNullOrEmpty(txtAddressLine2.Value))
                    emailEnrollMap[CommunicationTemplateConstants.ADDRESS_LINE2] = txtAddressLine2.Value.Trim();
                else
                    emailEnrollMap[CommunicationTemplateConstants.ADDRESS_LINE2] = CommunicationTemplateConstants.BLANK_SPACES;
                emailEnrollMap[CommunicationTemplateConstants.CITY] = txtCityOrTown.Value;
                emailEnrollMap[CommunicationTemplateConstants.COUNTRY] = ddlCountry.SelectedItem.Text;
                emailEnrollMap[CommunicationTemplateConstants.POST_CODE] = txtPostCode.Value.Trim();

                emailEnrollMap[CommunicationTemplateConstants.MEMBERSHIPNO_BOOKMARK_LINK_DISPLAY_STYLE] = "none";
                if (Convert.ToBoolean(AppConstants.ShowMembershipDetailsLink))
                {
                    string nameId = Utility.GetMembershipNameId(registrationID, txtEnrollPassword.Value);
                    try
                    {
                        //var payload = new Dictionary<string, string>
                        //{
                        //    { "MembershipID", registrationID },
                        //    { "LastName", txtLName.Value.Trim().ToUpper() },
                        //};
                        string payload = string.Format("{0}{1}{2}", registrationID, AppConstants.COMMA, txtLName.Value.Trim().ToUpper());
                        string encKey = Convert.ToString(ConfigurationManager.AppSettings["EncryptionKey"]);
                        emailEnrollMap[CommunicationTemplateConstants.MEMBERSHIP_CARD_URL] = string.Format("{0}://{1}/{2}?memkey={3}",
                            Request.Url.Scheme, Request.Url.Host, AppConstants.VIEW_MEMBERSHIP_DETAILS, 
                            System.Web.HttpUtility.UrlEncode(ScanwebJsonWebToken.Encrypt(payload, encKey)));
                        emailEnrollMap[CommunicationTemplateConstants.MEMBERSHIPNO_BOOKMARK_LINK_DISPLAY_STYLE] = "block";
                    }
                    catch (Exception ex)
                    {
                        AppLogger.LogFatalException(ex, string.Format("Error occured for the user with MembershipNumber: {0}", registrationID));
                    }
                }

                EmailEntity emailEntity = CommunicationUtility.GetEnrollConfirmationEmail(emailEnrollMap);
                emailEntity.Recipient = new string[] { txtEmail1.Value.Trim() };
                CommunicationService.SendMail(emailEntity, null);
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
            }
        }

        #endregion SendConfirmationMailToUser

        #region CaptureGuestProfileFromUI

        /// <summary>
        /// After Client side validation capture the user input from EnrollGuestProgram Screen
        /// </summary>
        /// <returns>UserProfileEntity</returns>
        private UserProfileEntity CaptureGuestProfileFromUI()
        {
            UserProfileEntity userProfileEntity = new UserProfileEntity();
            userProfileEntity.NameTitle = Utility.GetNameTitle(ddlTitle.Text);
            System.Globalization.TextInfo tInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
            userProfileEntity.NameTitle = (ddlTitle.Text != AppConstants.TITLE_DEFAULT_VALUE && string.Equals(ddlPreferredLang.Text, LanguageConstant.LANGUAGE_GERMAN_CODE)) ? tInfo.ToTitleCase(ddlTitle.Text) : null;

            userProfileEntity.FirstName = txtFName.Value.Trim();
            userProfileEntity.LastName = txtLName.Value.Trim();
            userProfileEntity.SetGender(rdoMale.Checked ? AppConstants.MALE : AppConstants.FEMALE);
            userProfileEntity.DateOfBirth = new DateOfBirthEntity(int.Parse(ddlDOBDay.Text), int.Parse(ddlDOBMonth.Text),
                                                                  int.Parse(ddlDOBYear.Text));

            if (txtEmail1.Value != "")
                userProfileEntity.EmailID = txtEmail1.Value;

            if (ddlTel2.Text != "DFT")
            {
                string mobilePhoneCode = Utility.FormatePhoneCode(ddlTel2.Text);
                userProfileEntity.MobilePhone = mobilePhoneCode + Regex.Replace(txtTelephone2.Value.Trim().TrimStart('0'), @"\D", "");
            }

            userProfileEntity.AddressType = AppConstants.ADDRESS_TYPE_HOME;
            if (txtAddressLine1.Value != "")
                userProfileEntity.AddressLine1 = txtAddressLine1.Value;
            if (txtAddressLine2.Value != "")
                userProfileEntity.AddressLine2 = txtAddressLine2.Value;

            userProfileEntity.PostCode = txtPostCode.Value;
            userProfileEntity.City = txtCityOrTown.Value;
            userProfileEntity.Country = ddlCountry.Text;

            string partnerPrograms = selectedPartnerProgramValues.Value;
            char[] valueSeperator = new char[1];
            valueSeperator[0] = '%';

            char[] seperator = new char[1];
            seperator[0] = ',';

            string[] eachEntity = partnerPrograms.Split(valueSeperator, StringSplitOptions.RemoveEmptyEntries);
            int entityLength = eachEntity.Length;

            List<PartnershipProgram> partnerProgramList = new List<PartnershipProgram>();
            List<bool> isPrimaryPartnerProgram = new List<bool>();
            for (int count = 0; count < entityLength; count++)
            {
                string[] eachValue = eachEntity[count].Split(seperator);
                PartnershipProgram partnerProgram = new PartnershipProgram();
                partnerProgram.PartnerProgram = eachValue[0];
                partnerProgram.PartnerAccountNo = eachValue[1];
                partnerProgramList.Add(partnerProgram);
            }

            userProfileEntity.PartnerProgramList = partnerProgramList;
            userProfileEntity.PreferredLanguage = ddlPreferredLang.Text;
            userProfileEntity.Password = txtEnrollPassword.Value;
            userProfileEntity.RetypePassword = txtEnrollReTypePassword.Value;

            if (chkReceiveScandicInfo.Checked)
            {
                userProfileEntity.ScandicEmail = true;
                userProfileEntity.ScandicEMailPrev = true;
            }
            else
            {
                userProfileEntity.ScandicEmail = false;
                userProfileEntity.ScandicEMailPrev = false;
            }

            if (chkReceivePartnersInfo.Checked)
                userProfileEntity.ThridPartyEmail = true;
            else
                userProfileEntity.ThridPartyEmail = false;

            return userProfileEntity;
        }

        #endregion CaptureGuestProfileFromUI

        #region SetSortDropDowns

        /// <summary>
        /// Method call on page load to populate the Drop Down Lists
        /// </summary>
        private void SetSortDropDowns()
        {
            SetDateOfBirthDropDown();

            SetCountryDropDown();

            SetPhoneCodesDropDown();

            SetPartnerProgramDropDown();

            SetPreferredLanguageDropDown();
        }

        #endregion SetSortDropDowns

        #region SetCountryDropDown

        /// <summary>
        /// Set the drop down list of Countries
        /// </summary>
        private void SetCountryDropDown()
        {
            OrderedDictionary countryCodeMap = DropDownService.GetCountryCodes();
            if (countryCodeMap != null)
            {
                foreach (string key in countryCodeMap.Keys)
                {
                    ddlCountry.Items.Add(new ListItem(countryCodeMap[key].ToString(), key));
                }
            }
            ListItem selectedDefaultItem = ddlCountry.Items.FindByValue(AppConstants.DEFAULT_VALUE_CONST);
            if (selectedDefaultItem != null)
            {
                ddlCountry.SelectedItem.Selected = false;
                selectedDefaultItem.Selected = true;
            }
        }

        #endregion SetCountryDropDown

        #region SetPreferredLanguageDropDown

        /// <summary>
        /// Set the drop down list of Prefered language
        /// </summary>
        private void SetPreferredLanguageDropDown()
        {
            OrderedDictionary preferredLangCodeMap = DropDownService.GetPreferredLanguageCodes();

            if (preferredLangCodeMap != null)
            {
                foreach (string key in preferredLangCodeMap.Keys)
                {
                    ddlPreferredLang.Items.Add(new ListItem(preferredLangCodeMap[key].ToString(), key));
                }
            }

            IFormatProvider cultureInfo = new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
            string PreferedlangSelected = cultureInfo.ToString().Substring(0, 2).ToUpper();

            if (PreferedlangSelected == LanguageConstant.LANGUAGE_ENGLISH)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_ENGLISH_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_DANISH)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_DANISH_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_NORWEGIAN)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_NORWEGIAN_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_SWEDISH)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_SWEDISH_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_FINNISH)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_FINNISH_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_GERMAN)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_GERMAN_CODE;
            }
            else if (PreferedlangSelected == LanguageConstant.LANGUAGE_RUSSIA)
            {
                PreferedlangSelected = LanguageConstant.LANGUAGE_RUSSIA_CODE;
            }
            ListItem selectedDefaultItem = ddlPreferredLang.Items.FindByValue(PreferedlangSelected);
            if (selectedDefaultItem != null)
            {
                ddlPreferredLang.SelectedItem.Selected = false;
                selectedDefaultItem.Selected = true;
            }
        }

        #endregion SetPreferredLanguageDropDown

        #region SetPhoneCodesDropDown

        /// <summary>
        /// Set the drop down list of Phone Codes
        /// </summary>
        private void SetPhoneCodesDropDown()
        {
            OrderedDictionary phoneCodesMap = DropDownService.GetTelephoneCodes();
            if (phoneCodesMap != null)
            {
                foreach (string key in phoneCodesMap.Keys)
                {
                    if (key == AppConstants.DEFAULT_VALUE_CONST || key == AppConstants.DEFAULT_SEPARATOR_CONST)
                    {
                        ddlTel2.Items.Add(new ListItem(phoneCodesMap[key].ToString(), key));
                    }
                    else
                    {
                        ddlTel2.Items.Add(new ListItem(key, key));
                    }
                }
            }

            ListItem tel2SelectedDefaultItem = ddlTel2.Items.FindByValue(AppConstants.DEFAULT_VALUE_CONST);
            if (tel2SelectedDefaultItem != null)
            {
                ddlTel2.SelectedItem.Selected = false;
                tel2SelectedDefaultItem.Selected = true;
            }
        }

        #endregion SetPhoneCodesDropDown

        #region SetPartnerProgramDropDown

        /// <summary>
        /// Set the hidden field in .ascx control and this value is read from using the javascript to populate the
        /// dropdown dynamically.
        /// </summary>
        /// <remarks>
        /// </remarks>
        private void SetPartnerProgramDropDown()
        {
            string partnershipProgramDefaultKey = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/defaultpartnershipprogramkey");
            string partnershipProgramDefaultValue = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/defaultpartnershipprogramvalue");

            InformationController informationCtrl = new InformationController();
            Dictionary<string, string> partnerProgramCodeMapOpera = informationCtrl.GetMembershipTypes();

            if (partnerProgramCodeMapOpera != null && partnerProgramCodeMapOpera.Count > 0)
            {
                prefPartnerCount.Value = (partnerProgramCodeMapOpera.Count).ToString();
                StringBuilder partnershipPrograms = new StringBuilder();
                partnershipPrograms.Append(partnershipProgramDefaultKey);
                partnershipPrograms.Append(",");
                partnershipPrograms.Append(partnershipProgramDefaultValue);
                partnershipPrograms.Append("%");
                foreach (string key in partnerProgramCodeMapOpera.Keys)
                {
                    partnershipPrograms.Append(key);
                    partnershipPrograms.Append(",");
                    partnershipPrograms.Append(partnerProgramCodeMapOpera[key]);
                    partnershipPrograms.Append("%");
                }
                partnerProgramValues.Value = partnershipPrograms.ToString();
            }

            string optionString = CreateInitialPartnershipDropDown(partnerProgramValues.Value);
        }

        #endregion SetPartnerProgramDropDown

        #region SetDateOfBirthDropDown

        /// <summary>
        /// Populate the Bate Of Birth drop downs 
        /// </summary>
        private void SetDateOfBirthDropDown()
        {
            string DD = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/day");
            ddlDOBDay.Items.Add(new ListItem(DD, AppConstants.DEFAULT_VALUE_CONST));
            ddlDOBDay.SelectedIndex = 0;
            for (int dayCount = 1; dayCount <= AppConstants.TOTALDAYS; dayCount++)
            {
                ddlDOBDay.Items.Add(new ListItem(dayCount.ToString()));
            }
            string Month = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/month");
            ddlDOBMonth.Items.Add(new ListItem(Month, AppConstants.DEFAULT_VALUE_CONST));
            ddlDOBMonth.SelectedIndex = 0;
            for (int monthCount = 1; monthCount <= AppConstants.TOTALMONTHS; monthCount++)
            {
                IFormatProvider cultureInfo =
                    new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
                string monthName = (new DateTime(1, monthCount, 1)).ToString("MMMM", cultureInfo);
                ddlDOBMonth.Items.Add(new ListItem(monthName, monthCount.ToString()));
            }
            string YYYY = WebUtil.GetTranslatedText("/bookingengine/booking/enrollguestprogram/year");
            ddlDOBYear.Items.Add(new ListItem(YYYY, AppConstants.DEFAULT_VALUE_CONST));
            ddlDOBYear.SelectedIndex = 0;
            int startYear = AppConstants.TOTAL_DOB_YEAR_START;
            int currentyear = DateTime.Now.Year;
            int EndYear = currentyear - AppConstants.MINAGE;
            int noOfYear = EndYear - startYear;
            for (int yearCount = noOfYear; yearCount >= 0; yearCount--)
            {
                ddlDOBYear.Items.Add(new ListItem((startYear + yearCount).ToString()));
            }
        }

        #endregion SetDateOfBirthDropDown

        #region CreateInitialPartnershipDropDown

        /// <summary>
        /// Sreates the options inthe drop down
        /// </summary>
        /// <param name="value">String value.</param>
        /// <returns>Option string.</returns>
        /// <remarks></remarks>
        private string CreateInitialPartnershipDropDown(string value)
        {
            char[] valueSeperator = new char[1];
            valueSeperator[0] = '%';

            char[] seperator = new char[1];
            seperator[0] = ',';

            string[] program;
            program = value.Split(valueSeperator, StringSplitOptions.RemoveEmptyEntries);
            StringBuilder newString = new StringBuilder();
            int partenrLength = program.Length;
            for (int count = 0; count < partenrLength; count++)
            {
                string[] keyValue = program[count].Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                newString.Append("<option value=\"");
                newString.Append(keyValue[0]);
                newString.Append("\">");
                newString.Append(keyValue[1]);
                newString.Append("</option>");
            }
            return newString.ToString();
        }

        #endregion CreateInitialPartnershipDropDown

        #region ShowOrHideFastTrackCodeDiv

        /// <summary>
        /// This will hide or show the 'FastTrackCodeDiv' based on the episerver configuration.
        /// </summary>
        /// <remarks></remarks>
        private void ShowOrHideFastTrackCodeDiv()
        {
            hdnCampaignCode.Value = FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.PromotionCode;

            if (FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.PrePopulatePromotionCode)
            {
                txtCampaignCode.Value = FastTrackEnrolmentSessionWrapper.FastTrackEnrolment.PromotionCode;
                txtCampaignCode.Disabled = true;
                txtCampaignCode.Attributes.Add("class", "frmInputText frmInputDisabled");
            }
            else
            {
                txtCampaignCode.Value = string.Empty;
                txtCampaignCode.Disabled = false;
                txtCampaignCode.Attributes.Add("class", "frmInputText");
            }
        }

        #endregion ShowOrHideFastTrackCodeDiv

        #endregion Private Methods

        #region INavigationTraking Members

        public List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> parameters = new List<KeyValueParam>();
            parameters.Add(new KeyValueParam(TrackerConstants.LOG_TYPE, actionName));
            parameters.Add(new KeyValueParam(TrackerConstants.TITLE, ddlTitle.Text));
            parameters.Add(new KeyValueParam(TrackerConstants.FIRST_NAME, txtFName.Value.Trim()));
            parameters.Add(new KeyValueParam(TrackerConstants.LAST_NAME, txtLName.Value.Trim()));
            parameters.Add(new KeyValueParam(TrackerConstants.GENDER, rdoMale.Checked ? AppConstants.MALE : AppConstants.FEMALE));
            parameters.Add(new KeyValueParam(TrackerConstants.DOB_DAY, ddlDOBDay.Text));
            parameters.Add(new KeyValueParam(TrackerConstants.DOB_MONTH, ddlDOBMonth.Text));
            parameters.Add(new KeyValueParam(TrackerConstants.DOB_YEAR, ddlDOBYear.Text));
            parameters.Add(new KeyValueParam(TrackerConstants.E_MAIL, txtEmail1.Value));
            parameters.Add(new KeyValueParam(TrackerConstants.PHONETYPE2, ddlTel2.Text));
            parameters.Add(new KeyValueParam(TrackerConstants.TELEPHONE2, Regex.Replace(txtTelephone2.Value, @"\D", "")));
            parameters.Add(new KeyValueParam(TrackerConstants.ADDRESS_LINE1, txtAddressLine1.Value));
            parameters.Add(new KeyValueParam(TrackerConstants.ADDRESS_LINE2, txtAddressLine2.Value));
            parameters.Add(new KeyValueParam(TrackerConstants.POST_CODE, txtPostCode.Value));
            parameters.Add(new KeyValueParam(TrackerConstants.CITY, txtCityOrTown.Value));
            parameters.Add(new KeyValueParam(TrackerConstants.COUNTRY, ddlCountry.Text));
            parameters.Add(new KeyValueParam(TrackerConstants.PARTNER_PROGRAM_VALUE, selectedPartnerProgramValues.Value));
            parameters.Add(new KeyValueParam(TrackerConstants.RECEIVESCANDICINFO, chkReceiveScandicInfo.Checked.ToString()));
            parameters.Add(new KeyValueParam(TrackerConstants.RECEIVE_PARTNERSINFO, chkReceivePartnersInfo.Checked.ToString()));
            return parameters;
        }

        #endregion
    }
}