//  Description					: DestinationSearch                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code behind of DestinationSearch control
    /// </summary>
    public partial class DestinationSearch : EPiServer.UserControlBase
    {
        #region Properties
        private AvailabilityController availabilityController = null;

        /// <summary>
        /// Gets m_AvailabilityController
        /// </summary>
        private AvailabilityController m_AvailabilityController
        {
            get
            {
                if (availabilityController == null)
                {
                    availabilityController = new AvailabilityController();
                }
                return availabilityController;
            }
        }
        #endregion

        /// <summary>
        /// Excludes specific strings
        /// </summary>
        /// <returns></returns>
        protected List<string> ExcludeStrings()
        {
            List<string> excludeStrs = new List<string>();

            string destinationExclusionString =
                WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/destinationexclusion");
            string[] inputExclusions = destinationExclusionString.Split(',');

            for (int i = 0; i < inputExclusions.Length; i++)
            {
                excludeStrs.Add(inputExclusions[i].ToLower());
            }
            return excludeStrs;
        }

        /// <summary>
        /// Fetch all destinations.
        /// </summary>
        /// <returns></returns>
        protected List<CityDestination> FetchAllDestination()
        {
            List<CityDestination> CityList;
            CityList = m_AvailabilityController.FetchAllDestinationsForAutoSuggest();
            return CityList;
        }

        /// <summary>
        /// Fetch all destinations with non bookable.
        /// </summary>
        /// <returns></returns>
        protected List<CityDestination> FetchAllDestinationsWithNonBookable()
        {
            List<CityDestination> CityList;
            CityList = m_AvailabilityController.FetchAllDestinationsWithNonBookable();
            return CityList;
        }

        public List<Scandic.Scanweb.Core.CountryDestinatination> FetchAllCountryDestinationsForAutoSuggest()
        {
            List<Scandic.Scanweb.Core.CountryDestinatination> CountryList;
            CountryList = m_AvailabilityController.FetchAllCountryDestinationsForAutoSuggest(false,false);
            return CountryList;
        }

        public List<Scandic.Scanweb.Core.CountryDestinatination> FetchAllCountryDestinations()
        {
            List<Scandic.Scanweb.Core.CountryDestinatination> CountryList;
            CountryList = m_AvailabilityController.FetchAllCountryDestinationsForAutoSuggest(true, false);
            return CountryList;
        }
    }
}