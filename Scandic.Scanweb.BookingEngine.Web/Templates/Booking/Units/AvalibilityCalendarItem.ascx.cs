//  Description					: Code Behind class for AvalibilityCalendarItem control   //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Bhavya Shekar                              	      //
/// Author email id				:                           							  //
/// Creation Date				: 27th July  2010									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{

    /// <summary>
    /// This control represents the individual Item within availibilityControl
    /// </summary>
    public partial class AvalibilityCalendarItem : EPiServer.UserControlBase, IPostBackEventHandler
    {
        #region properties


        /// <summary>
        /// Gets or sets the arrival date.
        /// </summary>       
        public string ArrivalDate { get; set; }

        /// <summary>
        /// Gets or sets the departure date.
        /// </summary>    
        public string DepartureDate { get; set; }

        private int liPositionIndexInList;

        /// <summary>
        /// Gets or sets the LiPositionIndexInList, position of the avalibility calendar Item in the avalibility calendarList displayed in the UI.
        /// </summary>
        public int LiPositionIndexInList
        {
            get { return Convert.ToInt32(hdnLiPositionIndexInList.Value); }
            set
            {
                liPositionIndexInList = value;
                hdnLiPositionIndexInList.Value = liPositionIndexInList.ToString();
            }
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

            divAvailabilityCalenderItem.Attributes["onclick"] = Page.ClientScript.GetPostBackEventReference(this,
                                                                                                            LiPositionIndexInList
                                                                                                                .
                                                                                                                ToString
                                                                                                                ());
        }

        /// <summary>
        /// Performs an availability search for the new Arrival/Departure dates.
        /// Reloads the select rate page with the new rate information obtained.
        /// </summary>
        protected void AvailabilitySearch_Click()
        {
            AvailabilityCalendarEntity availabilityCalendar = Reservation2SessionWrapper.AvailabilityCalendar;
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            if (availabilityCalendar != null && availabilityCalendar.ListAvailCalendarItems != null &&
                availabilityCalendar.ListAvailCalendarItems.Count > 0
                && hotelSearch != null)
            {
                int selectedItemsIndexInSessionList = LiPositionIndexInList +
                                                      Reservation2SessionWrapper.AvailabilityCalendar.LeftCarouselIndex;
                hotelSearch.ArrivalDate =
                    availabilityCalendar.ListAvailCalendarItems[selectedItemsIndexInSessionList].ArrivalDate;
                hotelSearch.DepartureDate =
                    availabilityCalendar.ListAvailCalendarItems[selectedItemsIndexInSessionList].DepartureDate;

                double oldSelectedRate = 0;
                double newSelectedRate = 0;
                int oldSelectedCalendarItemIndex = Reservation2SessionWrapper.SelectedCalendarItemIndex +
                                                   Reservation2SessionWrapper.AvailabilityCalendar.LeftCarouselIndex;

                if (oldSelectedCalendarItemIndex > 0 &&
                    oldSelectedCalendarItemIndex < availabilityCalendar.ListAvailCalendarItems.Count)
                {
                    if (hotelSearch.SearchingType == SearchType.REDEMPTION &&
                        availabilityCalendar.ListAvailCalendarItems[oldSelectedCalendarItemIndex].HotelDetails is
                        RedemptionHotelDetails)
                    {
                        oldSelectedRate =
                            (availabilityCalendar.ListAvailCalendarItems[oldSelectedCalendarItemIndex].HotelDetails as
                             RedemptionHotelDetails).MinPoints.PointsRequired;
                    }
                    else
                    {
                        BaseHotelDetails hotelDetails =
                            availabilityCalendar.ListAvailCalendarItems[oldSelectedCalendarItemIndex].HotelDetails as
                            BaseHotelDetails;
                        if (hotelDetails != null)
                        {
                            if (Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage)
                            {
                                if (hotelDetails.MinRatePerStay != null)
                                    oldSelectedRate = hotelDetails.MinRatePerStay.Rate;
                            }
                            else if (hotelDetails.MinRate != null) oldSelectedRate = hotelDetails.MinRate.Rate;
                        }
                    }
                }

                if (selectedItemsIndexInSessionList > 0 &&
                    selectedItemsIndexInSessionList < availabilityCalendar.ListAvailCalendarItems.Count)
                {
                    if (hotelSearch.SearchingType == SearchType.REDEMPTION &&
                        availabilityCalendar.ListAvailCalendarItems[selectedItemsIndexInSessionList].HotelDetails is
                        RedemptionHotelDetails)
                    {
                        newSelectedRate =
                            (availabilityCalendar.ListAvailCalendarItems[selectedItemsIndexInSessionList].HotelDetails
                             as RedemptionHotelDetails).MinPoints.PointsRequired;
                    }
                    else
                    {
                        BaseHotelDetails hotelDetails =
                            availabilityCalendar.ListAvailCalendarItems[selectedItemsIndexInSessionList].HotelDetails as
                            BaseHotelDetails;
                        if (hotelDetails != null)
                        {
                            if (Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage)
                            {
                                if (hotelDetails.MinRatePerStay != null)
                                {
                                    newSelectedRate = hotelDetails.MinRatePerStay.Rate;
                                }
                            }
                            else
                            {
                                if (hotelDetails.MinRate != null)
                                {
                                    newSelectedRate = hotelDetails.MinRate.Rate;
                                }
                            }
                        }
                    }
                }
                if (oldSelectedRate < newSelectedRate)
                {
                    Reservation2SessionWrapper.SelectedCalenderItemRateValueStatus = AppConstants.HIGHERVALUE;
                }
                else if (oldSelectedRate > newSelectedRate)
                {
                    Reservation2SessionWrapper.SelectedCalenderItemRateValueStatus = AppConstants.LOWERVALUE;
                }
                else
                {
                    Reservation2SessionWrapper.SelectedCalenderItemRateValueStatus = AppConstants.SAMEVALUE;
                }
                WebUtil.ClearSelectedRateSessionData();
                AvailabilityController availController = new AvailabilityController();
                availController.MultiroomMultidateCalendarAvailSearch(hotelSearch, availabilityCalendar);
                Reservation2SessionWrapper.SelectedCalendarItemIndex = LiPositionIndexInList;
                int step = LiPositionIndexInList / AppConstants.VISIBLE_CAROUSEL_COUNT;

                HtmlInputHidden leftVisibleCarouselIndex =
                    this.Parent.Parent.Parent.FindControl("leftVisibleCarouselIndex") as HtmlInputHidden;
                if (leftVisibleCarouselIndex != null)
                {
                    leftVisibleCarouselIndex.Value = Convert.ToString(step * AppConstants.VISIBLE_CAROUSEL_COUNT);
                }
                Reservation2SessionWrapper.LeftVisibleCarouselIndex = Convert.ToInt32(step * AppConstants.VISIBLE_CAROUSEL_COUNT);
                Reservation2SessionWrapper.AvailabilityCalendarAccessed = true;

                if (Reservation2SessionWrapper.IsModifyFlow)
                {
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE), false);
                }
                else
                {
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.SELECT_RATE_PAGE), false);
                }
            }
        }

        /// <summary>
        /// This method raises a server postback event, when the string returned by client side click event matches the reference string set to identify the click event.
        /// </summary>
        /// <param name="eventArgument">reference string </param>
        public void RaisePostBackEvent(string eventArgument)
        {
            if (!string.IsNullOrEmpty(eventArgument))
            {
                if (eventArgument == LiPositionIndexInList.ToString())
                {
                    AvailabilitySearch_Click();
                }
            }
        }

        #endregion
    }
}