<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UsefulLinksContainer.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.UsefulLinksContainer" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%@ Import Namespace="Scandic.Scanweb.Entity" %>
<!-- Links -->
    <div id="Links">
      <div class="columnOne">       
        <p class="popupLink"> <a href='#' onclick="openPopupWin('<%= GlobalUtil.GetUrlToPage(EpiServerPageConstants.FAMILY_BOOKINGS_PAGE) %>','width=800,height=600,scrollbars=yes,resizable=yes');return false;" ><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/aboutfamily") %></a> </p>
      </div>
      <div class="columnTwo">
        <p class="popupLink"> <a href='#' onclick="openPopupWin('<%= GlobalUtil.GetUrlToPage(EpiServerPageConstants.RESERVATION_TERMS_AND_CONDITIONS_PAGE) %>','width=800,height=600,scrollbars=yes,resizable=yes');return false;"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/reservationterms") %></a> </p>
      </div>
       <div class="columnThree">
        <p class="popupLink"> <a href='#' onclick="openPopupWin('/Hotels/Rates/','width=800,height=600,scrollbars=yes,resizable=yes');return false;"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rateinformation") %></a> </p>
      </div>
      <!-- <div class="columnTwo">
        &nbsp;
      </div> -->
      <div class="clear">&nbsp;</div>
    </div>
    <!-- /Links -->