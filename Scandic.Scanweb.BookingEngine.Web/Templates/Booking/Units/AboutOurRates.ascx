<%@ Control Language="C#" AutoEventWireup="true" Codebehind="AboutOurRates.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.AboutOurRates" %>
<%@ Import Namespace="Scandic.Scanweb.Entity" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<div id="ourRateMod10">
	<div class="regular">
		<div class="stoolHeader">
				<%-- <div class="hd sprite"></div>  removed this code for branding project    --%>
			<div class="cnt">
				<% if (Utility.GetCurrentLanguage().Equals("ru-RU") || Utility.CheckifFallback())
                   {%>
				<h2 class="stoolHeadingAlternate">
				<% }
                   else
                   {%>
               <h2 class="stoolHeading">
               <% } %>
               <asp:Label ID="aboutOurRateHeading" runat="server"></asp:Label></h2>
			</div>
			<%-- <div class="ft sprite">&nbsp;</div>removed this code for branding project    --%>
		</div>
		<div class="cnt">
		<p class="disclaimer"><asp:Label ID="aboutOurRateDescription" runat="server"></asp:Label></p>
		<div id="AboutRate" runat="server" class="aboutOurRate">
		
		</div>
			<hr />
	   <div>
           <a href='#' onclick="openPopupWin('/Hotels/Rates/','width=800,height=600,scrollbars=yes,resizable=yes');return false;"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/rateinformationR2") %></a> 
        </div>
	    <div>
            <a href='#' onclick="openPopupWin('<%= GlobalUtil.GetUrlToPage(EpiServerPageConstants.RESERVATION_TERMS_AND_CONDITIONS_PAGE) %>','width=800,height=600,scrollbars=yes,resizable=yes');return false;"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/reservationtermsR2") %></a>
        </div>
     		<%--<a href="#" title="Read more about our rates">Read more about our rates</a>
			<a href="#" title="Booking conditions">Booking conditions</a>--%>
		</div>
		<div class="ft sprite"></div>
	</div>
</div>