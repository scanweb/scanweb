<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CurrentBookings.ascx.cs"
    Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.CurrentBookings" %>
<%@ Register TagPrefix="Scanweb" TagName="PointExpInformation" Src="~/Templates/Booking/Units/ExpiryPoints.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<div id="Loyalty" class="BE posRel">
    <h1 id="greetMessage" runat="server" class="welcomeText mp-page-title">
    </h1>
    <div class="dashboard">
        <div class="box box1">
            <div class="mp-key">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/YourPoint") %></div>
            <div id="totalPoints" runat="server" class="mp-value">
            </div>
        </div>
        <div class="box box2">
            <div class="mp-key">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/membershipNumber") %></div>
            <div id="membershipNo" runat="server" class="mp-value">
            </div>
        </div>
        <div class="box box3">
            <div class="mp-key">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/levelStatus") %></div>
            <div id="memberLevel" runat="server" class="mp-value">
            </div>
            <span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/hlpLevel") %>"
                class="help scansprite toolTipMe"></span>
        </div>
    </div>
    <div class="bbox">
        <div class="box-head">
            <h2 class="bbox-title">
                <strong class="futureReservationHeading">
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/currentbooking/futurereservations") %></strong></h2>
        </div>
        <div class="box-content">
            <input id="txtConfirmationId" value="XYZ" runat="server" type="hidden" />
            
            <div class="popuptopCntOverLay" id="DivErrorPopup" runat="server">
                <div class="popupOverLayCnt"></div>    
	            <div class="popupOverLay roundMe">        
		            <div class="cnt">
			            <a href="#" class="scansprite blkClose" id="">&nbsp;</a>
                        <div id="DivError" class="errorText" runat="server"></div>         
                    </div>       
                </div>
            </div>
            <span class="secure hidefReservation">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/currentbooking/hidefuturereservations") %></span>
            <span class="secure viewfReservation">
                <%= WebUtil.GetTranslatedText("/bookingengine/booking/currentbooking/viewfuturereservations") %></span>
            <!-- Progress Bar -->
            <div class="booking-progress" id="BookingListProgressDiv" style="display: none; text-align: center;">
                <img src='<%=ResolveUrl("~/Templates/Booking/Styles/Default/Images/rotatingclockani.gif")%>'
                    alt="ProgressBar" align="middle" width="25px" height="25px" />
            </div>
            <!-- /end progress -->
            <div id="DivCurrentBooking" class="futureReservationData" runat="server">
                <div id="CurrentBookingList" width="100%" border="0" align="center" cellpadding="0"
                    cellspacing="0" runat="server">
                    <ul id="currentBkngsUl">
                    </ul>
                </div>
                <div id="DivBooking" class="bookingDetailserror" style="padding: 5px;" runat="server">
                </div>
                <div class="actionBtn fltRt mrgTop5">
                    <a class="buttonInner accview" id="viewMoreResults" style="display: none" onclick="GetCurrentBookings(false,bookingIndex);">
                        <span>
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/currentbooking/viewmoreresults") %></span></a>
                </div>
            </div>
        </div>
    </div>
    <!-- /end future reservation -->
    <!-- Transactions -->
    <div class="bbox">
        <div id="TransactionGrid" runat="server">
            <div class="box-head">
                <h2 class="bbox-title">
                    <strong>
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/transactionMainheader") %></strong></h2>
            </div>
            <div class="box-content">
                <div class="free-nights-container">
                    <div id="idSubHeading" runat="server" class="fn-cont">
                        <div class="txt">
                            <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/YourQlNight") %></div>
                        <span id="strngNights" runat="server" class="nights"></span><span title="<%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/hlpBook") %>"
                            class="help scansprite toolTipMe" style="float: none;"></span>
                    </div>
                    <div class="fn-img">
                    </div>
                    <div id="NoTransaction" runat="server" visible="false" class="">
                    </div>
                </div>
                <div id="errMsg" class="errorText mgLefRit" runat="server" />
                <div id="divTransaction" runat="server" class="transactions-tbl">
                    <table runat="server" id="tableTransactions" class="transTbl mp-tbl" width="100%"
                        cellspacing="0" cellpadding="0" border="0" style="*width: 430px!important; *margin-left: 10px;
                        *padding-left: 0px;">
                        <tr>
                            <th>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/transactionheader") %>
                            </th>
                            <th>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/dateheader") %>
                            </th>
                            <th class="tRtg">
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/pointsheader") %>
                            </th>
                        </tr>
                    </table>
                    <div id="FooterContainer" onkeypress="return WebForm_FireDefaultButton(event, '<%= linkButtonView.ClientID %>')">
                        <div class="timeFrame">
                            <div class="fltRt">
                                <span id="timeFrameFrom" runat="server" class="text chrmLineHeight"></span><span
                                    class="selMonth">
                                    <select id="ddlMonthFrom" runat="server">
                                    </select>
                                </span><span id="timeFrameTo" runat="Server" class="selMonth chrmLineHeight"></span>
                                <span class="selMonth">
                                    <select id="ddlMonthTo" runat="server">
                                    </select>
                                </span>
                            </div>
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                        <div class="actionBtn fltRt">
                            <asp:LinkButton ID="linkButtonView" CssClass="buttonInner accview" OnClick="ButtonView_Click"
                                runat="server">
                                <span id="viewButtonCaption" runat="server"></span>
                            </asp:LinkButton>
                            <asp:LinkButton ID="linkBtnRt" CssClass="buttonRt scansprite accviewrt" OnClick="ButtonView_Click"
                                runat="server"></asp:LinkButton>
                        </div>
                        <div class="clear">
                            &nbsp;</div>
                    </div>
                </div>
                <div id="idAlertBox" class="mp-alrtBox" runat="server">
                    <div class="whitBoxLft">
                        <div class="cnt">
                            <p>
                                <span class="spriteIcon"></span>
                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/fiveDays") %></p>
                        </div>
                    </div>
                </div>
                <!-- /AlertBox -->
            </div>
        </div>
    </div>
    <!-- /Transactions -->
    <!-- Points Expiry -->
    <div id="expiringPointsdiv" runat="server">
        <Scanweb:PointExpInformation ID="PointOfExpControl" runat="server">
        </Scanweb:PointExpInformation>
    </div>
    <div id="PointsDetails" runat="Server" visible="false">
        <div class="box-top">
            &nbsp;</div>
        <div class="box-middle">
            <!-- Content -->
            <div class="content">
                <!-- Header -->
                <div class="subHeader">
                    <span class="column1">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/pointsexpiringheader") %>
                    </span><span class="column2">
                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/pointsexpiringdate") %>
                    </span>
                    <div class="clear">
                        &nbsp;</div>
                </div>
                <!-- /Header -->
                <div class="row-details">
                    <!-- DEV NOTE:  						
						//IF no date to extract show below code						
						
						<p>You have made no transactions</p>						
						
						//ELSE show show below code
						-->
                    <span class="column1"><span class="num">1500</span><%= WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/pointsexpiringpoints") %>
                    </span><span class="column2">31, December 2007</span>
                    <div class="clear">
                        &nbsp;</div>
                </div>
            </div>
            <!-- /Content -->
        </div>
        <div class="box-bottom">
            &nbsp;</div>
        <div class="clear">
            <input id="hdnMembershipNameID" style="width: 14px" type="hidden" runat="server" />
            <input id="hdnMembershipOperaID" style="width: 14px" type="hidden" runat="server" /></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var timesClicked = 0;
        $('span.viewfReservation').bind('click', function(event) {
            $('#BookingListProgressDiv').show();
            GetCurrentBookings(true, bookingIndex);
            timesClicked++;
            if (timesClicked >= 1) {
                $(this).unbind(event);
            }
        });
        $(".mp-tbl tr:odd td").addClass("tbl-white-row");
    });
</script>

