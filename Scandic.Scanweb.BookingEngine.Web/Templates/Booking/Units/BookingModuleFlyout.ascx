<%--<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingModuleFlyout.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.BookingModuleFlyout" %>--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingModuleFlyout.ascx.cs" Inherits="Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.BookingModuleFlyout" %>
<div class="BFlyOut" id="BFlyOut">
	<div class="hd sprite"></div>
	<div class="cnt">
		<a href="#" class="blkClose scansprite" id="blkClose"></a>
		<div class="topLinks" id="topLinks" runat="server">
			<%= GetDestinations(true) %>
		</div>		
		<div class="linkList" id="linkList" runat="server">
			<%= GetDestinations(false) %>			
		</div>		
	</div>
	<div class="ft sprite"></div>
</div>