//  Description					: Code Behind class for ModifyBookingKids Control		  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Kshipra Thombre                                   	  //
/// Author email id				:                           							  //
/// Creation Date				: 20th April  2009  									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: Bhavya - Cancel Story	changes done to accomodate changes of reservationInfo control											  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region Namespaces

using System;
using System.Configuration;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion Namespaces

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code Behind class for ModifyBookingKids Control
    /// Control shows and helps user modify information about the kids. 
    /// </summary>
    public partial class ModifyBookingKids : EPiServer.UserControlBase
    {
        #region Protected Events

        /// <summary>
        /// Page Load Methods for ModifyBookingKids Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool isSessionValid = WebUtil.IsSessionValid();
            if (this.Visible && isSessionValid)
            {
                try
                {
                    if (!IsPostBack)
                    {
                        PopulatePreviousBookingReservationController();
                        PopulateNewBookingReservationController();

                        HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                        if (hotelSearch != null)
                        {
                            SetNoOfChildren(hotelSearch);
                            SetNoOfAdults(hotelSearch);
                        }
                        SetChildrenBedTypeCriteria();

                        if (hotelSearch != null)
                        {
                            int childrenCount = hotelSearch.ChildrenPerRoom;
                            int maxAge = Convert.ToInt32(ConfigurationManager.AppSettings["Booking.Children.MaxAge"]);
                        }
                        BtnContinueKidsPage.Attributes.Add("onclick",
                                                           "javascript:return performValidation(PAGE_CHILDREN_DETAIL);");
                    }
                }
                catch (Exception busEx)
                {
                    WebUtil.ApplicationErrorLog(busEx);
                }
            }
        }

        /// <summary>
        /// OnClick event handler of the "BtnContinueKidsPage"
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event params</param>
        protected void OnContinue(object sender, EventArgs e)
        {
            try
            {
                HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                if (hotelSearch != null)
                {
                    ChildrensDetailsEntity modifiedChildrensDetailsEntity = new ChildrensDetailsEntity();
                    string cot = string.Empty, extraBed = string.Empty, sharingBed = string.Empty;
                    Utility.GetLocaleSpecificChildrenAccomodationTypes(ref cot, ref extraBed, ref sharingBed);
                    uint adultsPerRoom = (uint) hotelSearch.AdultsPerRoom;

                    modifiedChildrensDetailsEntity.SetChildrensDetailsEntity(adultsPerRoom, childrenInfoHidden.Value,
                                                                             cot, sharingBed, extraBed);

                    hotelSearch.ChildrenOccupancyPerRoom =
                        Utility.GetNoOfChildrenToBeAccommodated(modifiedChildrensDetailsEntity);
                }
                string urlToRedirect = GlobalUtil.GetUrlToPage(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE);
                if (!string.IsNullOrEmpty(urlToRedirect))
                {
                    Response.Redirect(urlToRedirect, false);
                }
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
        }

        #endregion Protected Events

        #region Private Methods

        /// <summary>
        /// Set the hidden input container childrenCriteriaHidden
        /// with the criteria based on which bed type allocation 
        /// and age ranges has to be populated. This value is read 
        /// from appsetting
        /// </summary>
        private void SetChildrenBedTypeCriteria()
        {
            childrenCriteriaHidden.Value =
                WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/childcriteria");
        }

        /// <summary>
        /// Set the hidden field with the number of adults.Which 
        /// is used for validating CIPB bedtype validation
        /// </summary>
        /// <param name="hotelSearch">HotelSearchEntity</param>
        private void SetNoOfAdults(HotelSearchEntity hotelSearch)
        {
            if (hotelSearch != null)
            {
                noOfAdults.Value = hotelSearch.AdultsPerRoom.ToString();
            }
        }

        /// <summary>
        /// Set the hidden input container noOfChildrenHidden
        /// with the value of the no of children fed by the user
        /// This field is used by the js to create the dynamic
        /// population effect to capture details of each children
        /// </summary>
        /// <param name="hotelSearch">HotelSearchEntity</param>
        private void SetNoOfChildren(HotelSearchEntity hotelSearch)
        {
            if (hotelSearch != null)
            {
                noOfChildrenHidden.Value = hotelSearch.ChildrenPerRoom.ToString();
            }
        }

        /// <summary>
        /// Populate the Previous Booking Reservation Controller
        /// </summary>
        private void PopulatePreviousBookingReservationController()
        {
            BookingDetailsEntity bookingDetails = BookingEngineSessionWrapper.BookingDetails;
            ChildrensDetailsEntity previousChildrensDetailsEntity = null;

            if (bookingDetails != null)
            {
                HotelSearchEntity hotelSearchEntity = bookingDetails.HotelSearch;

                if (!string.IsNullOrEmpty(hotelSearchEntity.CampaignCode))
                {
                    SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = hotelSearchEntity.CampaignCode;
                }

                HotelRoomRateEntity hotelRoomRateEntity = bookingDetails.HotelRoomRate;
                GuestInformationEntity guestInfoEntity = bookingDetails.GuestInformation;
                if ((hotelSearchEntity != null) && (hotelRoomRateEntity != null) && (guestInfoEntity != null))
                {
                    AvailabilityController availabilityController = new AvailabilityController();
                    HotelDestination hotelDestination =
                        availabilityController.GetHotelDestinationEntity(hotelSearchEntity.SelectedHotelCode);

                    HtmlAnchor hotelAnchor = new HtmlAnchor();
                    hotelAnchor.Attributes.Add("onclick", "showDesc(this,'blkHotelDetails'); return false");
                    hotelAnchor.Attributes.Add("href", "#");
                    hotelAnchor.ID = "Hotel";

                    if (hotelDestination != null)
                    {
                        hotelAnchor.InnerText = hotelDestination.Name;
                        prevBookingReservationInfo.CityOrHotelName = hotelDestination.Name;
                    }
                    RoomCategory roomCategory = RoomRateUtil.GetRoomCategory(hotelRoomRateEntity.RoomtypeCode);
                    if (roomCategory != null)
                    {
                        prevBookingReservationInfo.NoOfRooms = hotelSearchEntity.RoomsPerNight + " " +
                                                               roomCategory.RoomCategoryName;
                    }
                    prevBookingReservationInfo.ArrivalDate = hotelSearchEntity.ArrivalDate;
                    prevBookingReservationInfo.DepartureDate = hotelSearchEntity.DepartureDate;
                    prevBookingReservationInfo.NoOfAdults = hotelSearchEntity.AdultsPerRoom;
                    prevBookingReservationInfo.NoOfChildren = hotelSearchEntity.ChildrenPerRoom;
                    prevBookingReservationInfo.NoOfNights = hotelSearchEntity.NoOfNights;
                    string reservationMessage = string.Empty;
                    if (!BookingEngineSessionWrapper.IsModifyingLegBooking)
                    {
                        reservationMessage =
                            WebUtil.GetTranslatedText(
                                TranslatedTextConstansts.YOUR_BOOKING_RESERVATION_MESSAGE_WITHOUT_NUMBER);
                        reservationMessage = string.Format(reservationMessage, bookingDetails.ReservationNumber);
                    }
                    else
                    {
                        string username = guestInfoEntity.FirstName + AppConstants.SPACE + guestInfoEntity.LastName;
                        reservationMessage =
                            WebUtil.GetTranslatedText(TranslatedTextConstansts.YOUR_BOOKING_RESERVATION_MESSAGE);
                        string resNumber = bookingDetails.ReservationNumber + AppConstants.HYPHEN +
                                           bookingDetails.LegNumber;
                        reservationMessage = string.Format(reservationMessage, username, resNumber);
                    }
                    string ratePlanCode = hotelRoomRateEntity.RatePlanCode;
                    string selectedRateCategoryName = string.Empty;
                    if (!string.IsNullOrEmpty(ratePlanCode))
                    {
                        Rate rate = RoomRateUtil.GetRate(ratePlanCode);
                        if (rate != null)
                        {
                            selectedRateCategoryName = rate.RateCategoryName;
                        }
                    }
                    string hotelCountryCode = hotelSearchEntity.HotelCountryCode;
                    if (hotelCountryCode == null || hotelCountryCode == string.Empty)
                    {
                        if (hotelSearchEntity.SearchingType == SearchType.BONUSCHEQUE)
                        {
                            HotelSearchEntity cloneHotelSearch = hotelSearchEntity.Clone();
                            if (cloneHotelSearch != null)
                            {
                                cloneHotelSearch.ArrivalDate = DateTime.Now;
                                cloneHotelSearch.DepartureDate = DateTime.Now.AddDays(1);
                            }
                        }
                    }
                    double basePoints = 0;
                    double totalPoints = 0;
                    basePoints = hotelRoomRateEntity.Points;
                    if (hotelRoomRateEntity.TotalRate != null)
                    {
                        totalPoints = hotelRoomRateEntity.TotalRate.Rate;
                    }
                    string baseRateString = string.Empty;
                    if ((Utility.IsBlockCodeBooking)
                        && (string.IsNullOrEmpty(hotelRoomRateEntity.RatePlanCode))
                        && (!string.IsNullOrEmpty(hotelRoomRateEntity.RoomtypeCode)))
                    {
                        baseRateString = Utility.GetRoomRateString(hotelRoomRateEntity.Rate);
                    }
                    else
                    {
                        baseRateString = Utility.GetRoomRateString(selectedRateCategoryName, hotelRoomRateEntity.Rate,
                                                                   basePoints,
                                                                   hotelSearchEntity.SearchingType,
                                                                   hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                                                   AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                    }
                    string totalRateString = string.Empty;
                    switch (hotelSearchEntity.SearchingType)
                    {
                        case SearchType.REDEMPTION:
                            {
                                totalRateString = Utility.GetRoomRateString(null, null, hotelRoomRateEntity.Points,
                                                                            hotelSearchEntity.SearchingType,
                                                                            hotelSearchEntity.ArrivalDate.Year,
                                                                            hotelCountryCode,
                                                                            hotelSearchEntity.NoOfNights,
                                                                            hotelSearchEntity.RoomsPerNight);
                                break;
                            }
                        case SearchType.VOUCHER:
                            {
                                totalRateString = Utility.GetRoomRateString(null, null, totalPoints,
                                                                            hotelSearchEntity.SearchingType,
                                                                            hotelSearchEntity.ArrivalDate.Year,
                                                                            hotelCountryCode,
                                                                            hotelSearchEntity.NoOfNights,
                                                                            hotelSearchEntity.RoomsPerNight);
                                break;
                            }
                        default:
                            {
                                totalRateString = Utility.GetRoomRateString(null, hotelRoomRateEntity.TotalRate,
                                                                            totalPoints, hotelSearchEntity.SearchingType,
                                                                            hotelSearchEntity.ArrivalDate.Year,
                                                                            hotelCountryCode,
                                                                            hotelSearchEntity.NoOfNights,
                                                                            hotelSearchEntity.RoomsPerNight);
                                break;
                            }
                    }
                    prevBookingReservationInfo.SetTotalRoomRate(totalRateString);
                }
            }
        }

        /// <summary>
        /// Populate the New Booking Reservation Controller
        /// </summary>
        private void PopulateNewBookingReservationController()
        {
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            if (hotelSearch != null)
            {
                AvailabilityController availabilityController = new AvailabilityController();

                HotelDestination hotelDestination =
                    availabilityController.GetHotelDestinationEntity(hotelSearch.SelectedHotelCode);

                if (hotelDestination != null)
                {
                    newBookingReservationInfo.CityOrHotelName = hotelDestination.Name;
                }
                newBookingReservationInfo.NoOfRooms = hotelSearch.RoomsPerNight.ToString();
                newBookingReservationInfo.ArrivalDate = hotelSearch.ArrivalDate;
                newBookingReservationInfo.DepartureDate = hotelSearch.DepartureDate;
                newBookingReservationInfo.NoOfAdults = hotelSearch.AdultsPerRoom;
                newBookingReservationInfo.NoOfChildren = hotelSearch.ChildrenPerRoom;
                newBookingReservationInfo.NoOfNights = hotelSearch.NoOfNights;
            }
        }

        #endregion Private Methods
    }
}