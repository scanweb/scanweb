//  Description					: RoomDetailContainer                        	 		  //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						: Shankar Dasgupta                                  	  //
// Author email id				:                           							  //
// Creation Date				: 24th December  2007									  //
//	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
// Revison History				: -NA-													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code behind of RoomDetailContainer 
    /// </summary>
    public partial class RoomDetailContainer : System.Web.UI.UserControl
    {
    }
}