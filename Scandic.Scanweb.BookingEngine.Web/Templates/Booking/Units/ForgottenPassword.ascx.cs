#region Description

////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: Forgotten Password user control code behind.            //
//                                This user control is used when user wants to retrieve   //
//                                his forgotten password.This control is called from      //
//                                login screen and login error screen.                    //    
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Raj Kishore Marandi	                                  //
/// Author email id				:                           							  //
/// Creation Date				: 17th November  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revision History			: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#endregion Description

#region  System Namespaces

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion System Namespaces

#region Scandic Namespaces

#endregion Scandic Namespaces

#region EpiServer NameSpace

#endregion EpiServer NameSpace

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{
    /// <summary>
    /// Code behind of ForgottenPassword control.
    /// </summary>
    public partial class ForgottenPassword : EPiServer.UserControlBase
    {
        #region Protected Methods
        string memberShipNumber = string.Empty;
        /// <summary>
        /// This method is fired when send button is clicked.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event argument.</param>
        protected void ButtonSend_Click(object sender, EventArgs e)
        {
            try
            {
                string password = string.Empty;
                memberShipNumber = txtForgotPrefix.Value + txtMembershipNumber.Value;
                password = GenerateNewPassword(memberShipNumber);
                if (!String.IsNullOrEmpty(password))
                {
                    string nameID = FetchNameId(memberShipNumber, password);
                    if (!string.IsNullOrEmpty(nameID))
                    {
                        string emailID = FetchEmaiId(nameID);
                        string firstName = FetchName(nameID);
                        if (!string.IsNullOrEmpty(emailID))
                        {
                            if (SendEmail(emailID, password, firstName))
                            {
                                ForgottenPasswordEmailConfirmation.Visible = true;
                                ForgotPassword.Visible = false;
                            }
                        }
                        else
                        {
                            string errorMesage = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/emailid_not_exist");
                            ShowErrorMessage(errorMesage);
                        }
                    }
                }
                else
                {
                    string errorMessage = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/membership_number_not_correct");
                    ShowErrorMessage(errorMessage);
                }
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
        }

        /// <summary>
        /// This method is responsible for setting Question dropdown and setting Email confirmation screen.
        /// This method is fired when page is loaded.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event arguments</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (!IsPostBack)
                {
                    SetConfirmationUserControl();
                    LinkButtonSend.Attributes.Add("onclick", "javascript:return performValidation(PAGE_FORGOTTEN_PASSWORD);");
                    spnButtonSend.Attributes.Add("onclick", "javascript:return performValidation(PAGE_FORGOTTEN_PASSWORD);");
                    ForgottenPasswordEmailConfirmation.Visible = false;
                }
            }
        }

        #endregion Protected Methods

        #region Private Methods

        /// <summary>
        /// This method is responsible for getting email id from opera.
        /// If email id is not present in opera then empty string is returned else email id is returned.
        /// </summary>
        /// <param name="nameId"></param>
        /// <returns></returns>
        private string FetchEmaiId(string nameId)
        {
            NameController nameController = new NameController();
            string emailID = nameController.FetchEmailId(nameId);
            if (!string.IsNullOrEmpty(emailID))
            {
                return emailID;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// This will return First name of the user.
        /// </summary>
        /// <param name="nameId"></param>
        /// <returns></returns>
        private string FetchName(string nameId)
        {
            NameController nameController = new NameController();
            return nameController.FetchName(nameId).ToString();
        }

        /// <summary>
        /// This method is responsible for getting nameid from opera.
        /// </summary>
        /// <param name="loyaltyNumber"></param>
        /// <param name="pin"></param>
        /// <returns></returns>
        private string FetchNameId(string loyaltyNumber, string pin)
        {
            String nameID = string.Empty;
            AvailabilityController availabilityController = new AvailabilityController();
            nameID = availabilityController.AuthenticateUser(loyaltyNumber, pin);
            return nameID;
        }

        /// <summary>
        /// Collect the Forgotten Password Confirmation Information to be send as Email
        /// </summary>
        /// <returns>OrderedDictionary</returns>
        private Dictionary<string, string> ForgottenPasswordEmailConfirmationDetails(string password, string FirstName)
        {
            Dictionary<string, string> ForgottenPasswordEmailConfirmationMap = new Dictionary<string, string>();
            string valueNotPresent = AppConstants.SPACE;
            ForgottenPasswordEmailConfirmationMap[CommunicationTemplateConstants.PASSWORD] = password;
            ForgottenPasswordEmailConfirmationMap[CommunicationTemplateConstants.FREQUENT_GUEST_PROGRAMME] =
                (string.IsNullOrEmpty(memberShipNumber) == null ? valueNotPresent : memberShipNumber);
            ForgottenPasswordEmailConfirmationMap[CommunicationTemplateConstants.FIRST_NAME] = FirstName;
            ForgottenPasswordEmailConfirmationMap[CommunicationTemplateConstants.LOGOPATH] = AppConstants.LOGOPATH;

            return ForgottenPasswordEmailConfirmationMap;
        }

        /// <summary>
        /// This method will reset the password associated with the membership number 
        /// and give you back the brand new password.
        /// </summary>
        /// <param name="membershipNumber">Membership number for which the password will be reseted.</param>
        /// <returns>New reseted password.</returns>
        /// <remarks>CR 9 | Forgotten PIN | Release 1.4</remarks>
        private string GenerateNewPassword(string membershipNumber)
        {
            NameController nameController = new NameController();
            string newPassword = nameController.GetNewPassword(membershipNumber.Trim());
            return newPassword;
        }

        /// <summary>
        /// This is the functionality for sending email message.
        /// </summary>
        /// <param name="EmailId"></param>
        /// <param name="password"></param>
        private bool SendEmail(string emailId, string password, string firstName)
        {
            bool resultStatus = false;
            try
            {
                Dictionary<string, string> emailConfirmationMap = ForgottenPasswordEmailConfirmationDetails(password, firstName);
                EmailEntity emailEntity = CommunicationUtility.GetForgottenPasswordConfirmationEmail(emailConfirmationMap);
                string[] receiver = new string[1];
                receiver[0] = emailId;
                emailEntity.Recipient = receiver;
                resultStatus = CommunicationService.SendMail(emailEntity, null);
                if (!resultStatus)
                {
                    AppLogger.LogInfoMessage("Not able to send Email to the registered email ID of the user:" + firstName);
                }
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx, "Email could not send in forgotten Password screen, exception raised in the method - SendEmail.");
            }
            return resultStatus;
        }

        /// <summary>
        /// This will set the email confirmation user control
        /// </summary>
        private void SetConfirmationUserControl()
        {
            ForgottenPasswordEmailConfirmation.AssignHeader(WebUtil.GetTranslatedText("/bookingengine/booking/forgottenpinconfirmation/header"));
            ForgottenPasswordEmailConfirmation.AssignMessage(WebUtil.GetTranslatedText("/bookingengine/booking/forgottenpinconfirmation/message"));
            ForgottenPasswordEmailConfirmation.AssignLink(WebUtil.GetTranslatedText("/bookingengine/booking/forgottenpinconfirmation/link"));
        }


        /// <summary>
        /// This method is used to show error message.
        /// Used to display invalid user and unavailable email id.
        /// </summary>
        /// <param name="errorMessage"></param>
        private void ShowErrorMessage(string errorMessage)
        {
            ForgotPasswordErrorDiv.Attributes.Add("class", "errorText");
            string start = "<div id='errorReport' class='formGroupError'><div class='redAlertIcon'>"
                           +
                           WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/errorheading")
                           + "</div><ul class='circleList'>";
            string end = "</ul></div>";
            ForgotPasswordErrorDiv.InnerHtml = start + errorMessage + end;
        }
        #endregion Private Methods
    }
}