// <copyright file="BookingKids.ascx.cs" company="Sapient">
// Copyright (c) 2008 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>06-April-2009</date>
// <version>1.0(Scanweb 1.6)</version>
// <summary>Contains code behind logic for the control BookingKids.ascx</summary>

using System;
using System.Configuration;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units
{


    /// <summary>
    /// Booking Kids Control which shows and captures
    /// information about the kids while the booking
    /// happens
    /// </summary>
    public partial class BookingKids : EPiServer.UserControlBase
    {
        #region Protected Events

        /// <summary>
        /// Page_Load event of the BookingKids Control
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event params</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool isSessionValid = WebUtil.IsSessionValid();
            if (this.Visible && isSessionValid)
            {
                try
                {
                    if (!IsPostBack)
                    {
                        HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                        if (hotelSearch != null)
                        {
                            FillReservationInfo(hotelSearch);
                            SetNoOfChildren(hotelSearch);
                            SetNoOfAdults(hotelSearch);
                        }
                        SetChildrenBedTypeCriteria();
                        if (hotelSearch != null)
                        {
                            int childrenCount = hotelSearch.ChildrenPerRoom;
                            int maxAge = Convert.ToInt32(ConfigurationManager.AppSettings["Booking.Children.MaxAge"]);
                        }
                        BtnContinueKidsPage.Attributes.Add("onclick",
                                                           "javascript:return performValidation(PAGE_CHILDREN_DETAIL);");
                    }
                }
                catch (Exception busEx)
                {
                    WebUtil.ApplicationErrorLog(busEx);
                }
            }
        }


        /// <summary>
        /// OnClick event handler of the "BtnContinueKidsPage"
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event params</param>
        protected void OnContinue(object sender, EventArgs e)
        {
            try
            {
                HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                if (hotelSearch != null)
                {
                    ChildrensDetailsEntity childrensDetailsEntity = new ChildrensDetailsEntity();
                    string cot = string.Empty, extraBed = string.Empty, sharingBed = string.Empty;
                    Utility.GetLocaleSpecificChildrenAccomodationTypes(ref cot, ref extraBed, ref sharingBed);
                    uint adultsPerRoom = (uint) hotelSearch.AdultsPerRoom;
                    childrensDetailsEntity.SetChildrensDetailsEntity(adultsPerRoom, childrenInfoHidden.Value, cot,
                                                                     sharingBed, extraBed);
                    hotelSearch.ChildrenOccupancyPerRoom =
                        Utility.GetNoOfChildrenToBeAccommodated(childrensDetailsEntity);
                }
                string urlToRedirect = Utility.RedirectBookingFlow(BookingEnginePosition.CHILDRENS_DETAILS_PAGE);
                if (!string.IsNullOrEmpty(urlToRedirect))
                {
                    Response.Redirect(urlToRedirect, false);
                }
            }
            catch (Exception genEx)
            {
                WebUtil.ApplicationErrorLog(genEx);
            }
        }

        #endregion 
        #region Private Methods

        /// <summary>
        /// Set the hidden input container childrenCriteriaHidden
        /// with the criteria based on which bed type allocation 
        /// and age ranges has to be populated. This value is read 
        /// from appsetting
        /// </summary>
        private void SetChildrenBedTypeCriteria()
        {
            childrenCriteriaHidden.Value =
                WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/rules/childcriteria");
        }

        /// <summary>
        /// Set the hidden field with the number of adults.Which 
        /// is used for validating CIPB bedtype validation
        /// </summary>
        /// <param name="hotelSearch">HotelSearchEntity</param>
        private void SetNoOfAdults(HotelSearchEntity hotelSearch)
        {
            if (hotelSearch != null)
            {
                noOfAdults.Value = hotelSearch.AdultsPerRoom.ToString();
            }
        }

        /// <summary>
        /// Set the hidden input container noOfChildrenHidden
        /// with the value of the no of children fed by the user
        /// This field is used by the js to create the dynamic
        /// population effect to capture details of each children
        /// </summary>
        /// <param name="hotelSearch">HotelSearchEntity</param>
        private void SetNoOfChildren(HotelSearchEntity hotelSearch)
        {
            if (hotelSearch != null)
            {
                noOfChildrenHidden.Value = hotelSearch.ChildrenPerRoom.ToString();
            }
        }

        /// <summary>
        /// Fill up the reservation info container to show the details
        /// which captures till this point of the booking.
        /// </summary>
        /// /// <param name="hotelSearch">HotelSearchEntity</param>
        private void FillReservationInfo(HotelSearchEntity hotelSearch)
        {
            try
            {
                if (SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.UserSearchType ==
                    SearchedForEntity.LocationSearchType.Hotel)
                {
                    HotelDestination hotelDestination = null;
                    if (!string.IsNullOrEmpty(hotelSearch.SelectedHotelCode))
                    {
                        hotelDestination = ContentDataAccess.GetHotelByOperaID(hotelSearch.SelectedHotelCode);
                        if (hotelDestination != null)
                        {
                            PopulateHotelInfo(hotelDestination);
                        }
                        else
                        {
                            ReservationInfo.CityOrHotelName = hotelSearch.SearchedFor.SearchString;
                        }
                    }
                }
                else if (SearchCriteriaSessionWrapper.SearchCriteria.SearchedFor.UserSearchType ==
                         SearchedForEntity.LocationSearchType.City)
                {
                    ReservationInfo.CityOrHotelName = hotelSearch.SearchedFor.SearchString;
                }

                ReservationInfo.NoOfRooms = hotelSearch.RoomsPerNight.ToString();
                ReservationInfo.ArrivalDate = hotelSearch.ArrivalDate;
                ReservationInfo.DepartureDate = hotelSearch.DepartureDate;
                ReservationInfo.NoOfAdults = hotelSearch.AdultsPerRoom;
                ReservationInfo.NoOfChildren = hotelSearch.ChildrenPerRoom;
                ReservationInfo.NoOfNights = hotelSearch.NoOfNights; 
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Could not fill the reservation info container.");
            }
        }

        /// <summary>
        /// Populate the hotel information, in the Reservation Info control.
        /// </summary>
        /// <param name="hotelDestination">HotelDestination object containing the hotel information.</param>
        private void PopulateHotelInfo(HotelDestination hotelDestination)
        {
            try
            {
                if (hotelDestination != null)
                {
                    HtmlAnchor hotelAnchor = new HtmlAnchor();
                    hotelAnchor.Attributes.Add("onclick", "showDesc(this,'blkHotelDetails'); return false");
                    hotelAnchor.Attributes.Add("href", "#");
                    hotelAnchor.ID = "Hotel";
                    hotelAnchor.InnerText = hotelDestination.Name;
                    ReservationInfo.CityOrHotelName = hotelDestination.Name;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion
    }
}