using System;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.BookingEngine.Web
{
    public partial class Error : EPiServer.TemplatePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                errMsgContent.Text = WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR);
            }
        }
    }
}