<%@ Page language="c#" Inherits="Scandic.Scanweb.BookingEngine.Web.Default2" Codebehind="Default2.aspx.cs" %>
<%@ Register Src="Templates/Booking/Units/BookingModuleSmall.ascx" TagName="BookingModuleSmall"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head1" runat="server">
<link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/booking.css") %>" />
<link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Templates/Booking/Styles/Default/calendar.css") %>" />
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/init.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/dhtml_utils.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/autoSuggest.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/booking.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/initCalendar.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/dateManip.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/calendar.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/preCalendar.js") %>"></script>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/Validation.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/AjaxFramework.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/Common.js") %>"></script>
<script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/AjaxCall.js") %>"></script>
 <title>Default2</title>
</head>
<body>
    <form runat="server" id="form1">
    <div>
        &nbsp;<uc1:BookingModuleSmall ID="BookingModuleSmall1" runat="server" />
       
       </div>
    </form>
</body>
</html>
