<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="System.Collections.Generic" %>
<script runat="server">

    protected List<string> ExcludeStrings()
    {
        List<string> excludeStrs = new List<string>();
        // Read through the details
        for (int i = 0; i < 2; i++)
        {
            excludeStrs.Add("Scandic");
        }
        return excludeStrs;
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head1" runat="server">
    <title>TestAjaxTextBox</title>
    <script type="text/javascript" language="JavaScript" src="AjaxTextBox.js"></script>
    <script language="javascript">
        var exStr = new ExclusionStrings();
        <%
            List<string> excludeStrings = ExcludeStrings();
            foreach (string excludeString in excludeStrings)
            {
%>
        exStr.addString('<%= excludeString %>');
        <%
            }
%>
    </script>
</head>
<body>
    <form runat="server" id="form1">
    <div>
    
    </div>
    </form>
</body>
</html>