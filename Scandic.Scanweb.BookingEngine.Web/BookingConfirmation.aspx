<%@ Page language="c#" Inherits="Scandic.Scanweb.BookingEngine.Web.BookingConfirmation" Codebehind="BookingConfirmation.aspx.cs" %>
<%@ Register Src ="~/Templates/Booking/Units/BookingConfirmation.ascx" TagName="BookingConfirmation" TagPrefix="ucBC" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title>Booking Confirmation</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style>
body {
	color:#666666;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 0.75em;
	font-weight:normal;
	line-height:1.7em;visible = "false"
	text-align:center;
}
</style>

<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/common.css")%>" />
<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/booking.css")%>" />
<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/calendar.css")%>" />
<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/reservation.css")%>?v=<%=CmsUtil.GetCSSVersion()%>" />
<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/loyalty.css")%>" />
<script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/init.js")%>"></script>
<script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/dhtml_utils.js")%>"></script>
<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/autoSuggest.js")%>"></script>
<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/booking.js")%>"></script>
<script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/initCalendar.js")%>"></script>
<script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/dateManip.js")%>"></script>
<script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/calendar.js")%>"></script>
<script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/preCalendar.js")%>"></script>
<script type="text/javascript" language="JavaScript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/validateBookingDetails.js")%>"></script>
<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/AjaxFramework.js")%>"></script>
<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/Common.js")%>"></script>
<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Booking/JavaScript/AjaxCall.js")%>"></script>
<!-- TODO: Add the below line only for this page(confirmation page) 
<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/print.css")%>"  media="print" />
</head>
<body>
    <form id="BookingConfirmationForm" runat="server">
    <table width=100%>
    <tr><td><ucBC:BookingConfirmation ID="BC" runat="server"/></td></tr> 
    </table>    
    </form>
</body>
</html>
