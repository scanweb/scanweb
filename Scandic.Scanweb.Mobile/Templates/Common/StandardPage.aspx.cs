﻿//  Description					: StandardPage                                            //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Mobile.Templates.Common
{
    /// <summary>
    /// Code behind of StandardPage
    /// </summary>
    public partial class StandardPage : System.Web.UI.Page
    {
    }
}