﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="SelectHotel.aspx.cs" Inherits="Scandic.Scanweb.Mobile.Templates.Booking.SelectHotel"
    MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" ResponseEncoding="utf-8" %>

<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<asp:Content ID="selectHotelPageContent" ContentPlaceHolderID="cphMain" runat="server">
<div id="searchHotelMessage" class="searchHotelmsgTitle" runat="server"></div>
    <ul class="info-list itinerary-info">
        <li class="cf"><span id="hgcDates" runat="server"></span><span id="hgcArrivalDateValue"
            runat="server"></span><span>&nbsp;-&nbsp;</span> <span id="hgcDepartureDateValue"
                runat="server"></span></li>
        <li class="cf"><span id="hgcNumOfNights" runat="server"></span><span id="hgcNumOfNightsValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcAdults" runat="server"></span><span id="hgcAdultsValue"
            runat="server"></span></li>
        <li class="cf" id="liChildrenInfo" runat="server" style="display: none;"><span id="hgcBedType"
            runat="server"></span><span id="hgcChildrenDetailValue" runat="server"></span>
        </li>
        <li class="cf"><a class="edit-search right" onclick="trackFunction(this, 11);" id="editSearch" runat="server"></a></li>
    </ul>
    <div class="standard-form">
        <fieldset>
            <asp:DropDownList ID="ddlSort" runat="server">
            </asp:DropDownList>
        </fieldset>
    </div>
    <ul class="hotel-list cf">
    </ul>
    <div style="display: none" class="hotelListElementTemplate">
        <ul>
            <li class="cf">
                <div class="sel-hotel-name">
                    <a href="#" onclick="SubmitSelectedHotel(this,'hotelname'); return false;" data-hotelid="">
                    </a>
                </div>
                <div class="image">
                    <div class="polaroid">
                        <a href="#" onclick="SubmitSelectedHotel(this,'image'); return false;" data-hotelid=""><img src="" alt="" /></a>
                    </div>
                </div>
                <!--Hotel Contents Start-->
                <div class="select-hotel-content">
                    <div class="hotel-desc">
                        <p>
                        </p>
                    </div>
                    <div class="sel-hotel-addr">
                        <div class="addr-pin">
                        </div>
                        <div class="addr-details">
                            <span class="hotel-address"><a class="address new-window" href="#" target="_blank"
                                onclick="return trackFunction(this, 4);"><span class="street-address"></span></a>
                            </span><span class="desc"></span>&nbsp<span id="hgcViewOnMap" runat="server"></span>
                            <span class="hotel-distance"></span>
                        </div>
                    </div>
                    <div class="trip-advisor-wrapper">
                        <div id="divTripAdvisorRating">
                            <p class="tripAdvisorImage">
                                <img src="" /></p>
                            <p>
                                <a href="" class="tripReviewcount" onclick="trackFunction(this, 10);displayIframe(this.href);return false;">
                                </a>
                            </p>
                        </div>
                    </div>
                    
                    <div class="hotel-booking-wrapper">
                        <div class="booking-code-container">
                            <span class="booking-code"></span>                            
                        </div>
                        <div class="no-discount"></div>
                        <div class="price-container left">                            
                            <div class="price-details left">
                                <span class="price-info-text left"></span>                                
                                <br />
                                <span class="price-desc right"></span>
                            </div>
                        </div>
                        <div class="hotel-book-btn right">
                            <a href="#" onclick="SubmitSelectedHotel(this,'button'); return false;" class="book-btn"
                                data-hotelid=""></a>
                        </div>
                    </div>
                </div>
                <!--Hotel Contents End-->
                
            </li>
        </ul>
    </div>
    <hr style="border-top: 3px solid #999999;margin-bottom: 1em;" id="hrViewButton" />
    <a class="button purple submit hidden view-more-hotels" href="#" id="hgcViewMoreResults"
        runat="server">more results</a>
    <asp:HyperLink ID="lnkBack" runat="server" CssClass="button green submit">Back</asp:HyperLink>
    <asp:HiddenField ID="hidSelectedHotelId" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="cphScript" runat="server" ID="pageScripts">

    <script type="text/javascript">
        var noOfHotelsToDisplay = "<%= numberOfHotelToRequestOnAjaxCall %>";
        var maxHotelsPerPage = "<%= maxNumberOfResults %>";
        var fromText = "<%= fromText %>";
        var isRedemptionBooking = "<%= isRedemptionBooking %>";
        var IsSearchfromCurrentLocation = "<%= IsSearchfromCurrentLocation %>";
        var bookText = "<%= bookText %>";
        //var promoHotelRegularPriceFromText = "<%=promoHotelRegularPriceFromText %>";
        var promoCodeText = "<%=promoCodeText %>";
        var noDiscountAvailableText = "<%=noDiscountAvailableText %>";
        var isPromoSearch = "<%=isPromoSearch %>";
    </script>

    <script src="<%= ResolveUrl("~/ScanwebMobile/Public/Scripts/SelectHotel.min.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>

    <div id="overlay-tripiframe" class="overlay">
        <div class="top">
            <a onclick="return closeOverlay();" href="#" class="close">x</a>
            <div id="tripAdvisorIframe">
            </div>
        </div>
    </div>
</asp:Content>
