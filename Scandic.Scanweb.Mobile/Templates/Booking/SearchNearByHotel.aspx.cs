﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Booking.Business;
using EPiServer.Core;
namespace Scandic.Scanweb.Mobile.Templates.Booking
{
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionInValid(false,false)]
    public partial class SearchNearByHotel : VisualBasePage<SearchHotelPageSection>
    {
        private SearchHotelController pageController;
        string buttonText = string.Empty;
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            this.cityListRepeater.ItemCommand += new RepeaterCommandEventHandler(Repeater_ItemCommand);
            this.cityListRepeater.ItemDataBound += new RepeaterItemEventHandler(Repeater_ItemDataBound);
            pageController = new SearchHotelController();
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/Booking/SearchNearByHotel.aspx";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            List<SearchDestination> cityList = pageController.CurrentContext.SearchHotelPage.SearchNearbyDestinations; 
            if (!IsPostBack)
            {
                PageData pageData = CMSDataManager.GetPageData(Reference.SEARCH_NEARBY_HOTEL_PAGE_CMS_REFERENCE);
                this.Master.PageHeading = pageData[Reference.SEARCH_NEARBY_MEASSAGE].ToString();
                cityListRepeater.DataSource = cityList;
                cityListRepeater.DataBind();                
            }
        }

        protected void Repeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            pageController.CurrentContext.SearchHotelPage.SearchDestination = e.CommandArgument.ToString();
            pageController.PerformSearch(pageController.CurrentContext.SearchHotelPage);
        }

        protected void Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var data = (SearchDestination)e.Item.DataItem;
                var cityDistanceLabel = e.Item.FindControl("cityDistance") as Label;
                var citylinkButton = e.Item.FindControl("cityLinkButton") as LinkButton;

                citylinkButton.Text = data.name;
                citylinkButton.CommandArgument = data.name;
                cityDistanceLabel.Text = string.Format("{0}{1} {2}{3}","(", Math.Round(data.DistanceFromCurrentLocation,1), "km",")");
                
            }
        }
    }
}
