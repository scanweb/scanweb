﻿using System;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Mobile.Templates.Controls;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;

namespace Scandic.Scanweb.Mobile.Templates.Booking
{
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionExpired(true)]
    public partial class OffersList : VisualBasePage<OffersListPageSection>
    {
        private OffersController controller;
        private const string OFFER_LIST_HEADER = "offerListHeader";
        private const string SHOW_ALL_OFFERS_TEXT = "ShowAllOffersText";
        private const string OFFER_CONTAINER = "OfferContainer";
        private const string HIDE_OFFER_CATEGORY = "HideOfferCategory";
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            OfferCategory.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(OfferCategory_ItemDataBound);
            controller = new OffersController();
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/Booking/OffersList.aspx";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadControls();
            }
            this.Master.PageId = PageId();
        }

        /// <summary>
        /// Gets PageId
        /// </summary>
        /// <returns></returns>
        public override MobilePages PageId()
        {
            return MobilePages.AllOffers;
        }

        private void LoadControls()
        {
            var pageConfig = controller.GetPageConfig<OffersListPageSection>();
            this.Master.PageHeading = pageConfig.PageDetail.PageMessages.GetMessage(OFFER_LIST_HEADER);
            PageReference offerContainer = controller.GetOfferContainerPageData(OFFER_CONTAINER);
            PageData offerContainerPage = ContentDataAccess.GetPageData(offerContainer.ID, CMSSessionWrapper.CurrentLanguage);
            showAllOffer.InnerHtml = Convert.ToString(offerContainerPage[SHOW_ALL_OFFERS_TEXT]);
            if (offerContainerPage != null)
            {
                PageDataCollection offerCategoryPages = DataFactory.Instance.GetChildren(offerContainer);
                PageDataCollection pgDataCollection = new PageDataCollection();
                if (offerCategoryPages != null && offerCategoryPages.Count > 0)
                {
                    if (controller.IsOffersConfiguredForCurrentLocale(offerCategoryPages))
                    {
                        noOfferAvailable.Visible = false;
                        resultText.Visible = true;
                        OfferCategory.Visible = true;
                        categoryContainer.Visible = true;
                        foreach (PageData offerCategoryPage in offerCategoryPages)
                        {
                            if ((offerCategoryPage[HIDE_OFFER_CATEGORY] == null) || (offerCategoryPage[HIDE_OFFER_CATEGORY] != null &&
                                 offerCategoryPage[HIDE_OFFER_CATEGORY].ToString().ToLower() != "true"))
                            {
                                pgDataCollection.Add(offerCategoryPage);
                                OffersListing categoryListing = (OffersListing)LoadControl("/ScanwebMobile/Templates/Controls/OffersListing.ascx");
                                categoryListing.OfferCategoryPage = offerCategoryPage;
                                OfferCategoryPlaceHolder.Controls.Add(categoryListing);
                            }
                        }
                    }
                    else
                    {
                        noOfferAvailable.Visible = true;
                        OfferCategory.Visible = false;
                        categoryContainer.Visible = false;
                        resultText.Visible = false;
                        noOfferAvailable.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage("noOfferAvailable");
                    }
                }
                OfferCategory.DataSource = pgDataCollection;
                OfferCategory.DataBind();
            }
            
            this.Master.PageId = PageId();
        }

        private void OfferCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var pageData = e.Item.DataItem as PageData;
                if (pageData != null)
                {
                    var offerButton = e.Item.FindControl("offerButton") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (offerButton != null)
                    {
                        offerButton.InnerHtml = pageData.PageName;
                        offerButton.Attributes.Add("data-id", Convert.ToString(pageData.PageLink.ID));
                    }
                }
            }
        }
    }
}
