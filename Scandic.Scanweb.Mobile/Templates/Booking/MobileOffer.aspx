﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="MobileOffer.aspx.cs" Inherits="Scandic.Scanweb.Mobile.Templates.Booking.MobileOffer"
    MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" %>

<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<asp:Content ID="OfferDetailsContent" ContentPlaceHolderID="cphMain" runat="server">
    <div class="errorAlertBox" id="OfferExpiryDiv" runat="server" visible="false">
        <div class="errorAlertIcon">
        </div>
        <div class="errorDesc">
            <div class="errorHeading">
            </div>
            <div class="errorTxt" id="OfferExpiryText" runat="server">
            </div>
        </div>
    </div>
    <div class="offer-detail-wrapper">
        <div class="offer-detail-title">
            <h1>
                <%=offerName %></h1>
        </div>
        <div class="offer-detail-img" runat="server" visible="false" id="offerDetailImageContainer">
            <img id="offerDetailImage" runat="server" alt="" />
        </div>
        <asp:PlaceHolder ID="carouselPlaceHolder" runat="server"></asp:PlaceHolder>
        <div class="offer-detail-title">
            <h2>
                <%=offerHeading %></h2>
        </div>
        <div class="offer-detail-content" id="mobileOfferMainBody" runat="server">
        </div>
        <div class="offer-detail-btn">
            <a class="book-btn" id="bookButton" runat="server">
            <span></span>
            </a>
        </div>
    </div>
    
</asp:Content>
<asp:Content ContentPlaceHolderID="cphScript" runat="server" ID="pageScripts">
    <script type="text/javascript" >
        var trackingData = '<%=trackingInformation %>';
        var selectRateOfferTrackingData = '<%=selectRateOfferTrackingData %>';
    </script>
    <script type="text/javascript" src="<%= ResolveUrl("~/ScanwebMobile/Public/Scripts/OfferDetails.min.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>

</asp:Content>
