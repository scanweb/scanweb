﻿using System;
using System.Collections;
using System.Web.Services;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using System.Text;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.CMS.DataAccessLayer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using System.Collections.Generic;

namespace Scandic.Scanweb.Mobile.Templates.Booking
{
    /// <summary>
    /// SelectHotel
    /// </summary>
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionInValid(false,true)]
    public partial class SelectHotel : VisualBasePage<SelectHotelPageSection>
    {
        private const string DATE_FORMATE = "yyyy-MM-dd";
        private const string HOTEL_PHONE = "HotelTelephoneNumber";
        private const string NEAR_BY_HOTELS_PAGE_HEADING = "NearByHotelsPageHeading";
        private const string RING_US_TEXT = "RingUsText";
        private const string FROM_TEXT = "FromText";
        private const string BOOK_TEXT = "BookText";
        private SelectHotelController pageController;
        protected string maxNumberOfResults = "";
        protected string numberOfHotelToRequestOnAjaxCall = "";
        protected string telephoneNumber = "";
        protected string telephoneNumberLinkText = "";
        protected string fromText = "";
        protected string bookText = "";
        protected string promoHotelRegularPriceFromText = "";
        protected string promoCodeText = "";
        protected string isRedemptionBooking = "";
        protected string IsSearchfromCurrentLocation = "";
        private const string AGE_TEXT = "AgeText";
        private const string ADULT_TEXT = "AdultText";
        private const string CHILDREN_TEXT = "ChildrenText";
        private const string SPACE = " ";
        private const string COMMA = ",";
        private const string LEFT_PARENTHESES = "(";
        private const string RIGHT_PARENTHESES = ")";
        private const string PROMO_CODE_TEXT = "PromoCodeText";
        private const string PROMO_HOTEL_REGULARPRICE_FROM_TEXT = "PromoHotelRegPriceFromText";
        protected string noDiscountAvailableText = "";
        private const string NODISCOUNT_AVAILABLE_TEXT = "NoDiscountAvailableText";
        protected bool isPromoSearch = false;
        /// <summary>
        /// OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            pageController = new SelectHotelController();
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/Booking/SelectHotel.aspx";
        }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            IsSearchfromCurrentLocation = pageController.CurrentContext.SearchHotelPage.IsSearchfromCurrentLocation == true ? "1" : "0";
            if (!IsPostBack)
            {
                LoadControls();
            }
            else
            {
                ProcessRequest();
            }
            isRedemptionBooking = pageController.CurrentContext.CurrentBookingProcess == BookingProcess.ClaimRewardNights ? "1" : "0";

        }

        /// <summary>
        /// ProcessRequest
        /// </summary>
        private void ProcessRequest()
        {
            switch (this.Master.PostBackProcess.Value)
            {
                case "SelectHotel":
                    pageController.SelectHotel(ProcessInput());
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// ProcessInput
        /// </summary>
        /// <returns>SelectHotelModel</returns>
        private SelectHotelModel ProcessInput()
        {
            var selectHotelInput = new SelectHotelModel();
            var selectedData = hidSelectedHotelId.Value.Split(',');
            selectHotelInput.SelectedHotelId = selectedData[1];
            selectHotelInput.SelectedHotel = selectedData[0];
            selectHotelInput.IsHotelSelectedByUser = true;
            selectHotelInput.SelectedHotelHasOnlyPublicRates = string.Equals(selectedData[2], "1");
            return selectHotelInput;
        }

        /// <summary>
        /// LoadControls
        /// </summary>
        private void LoadControls()
        {
            var pageConfig = pageController.GetPageConfig<SelectHotelPageSection>();
            var appConfig = pageController.GetGeneralConfig<ApplicationConfigSection>();
            var checkInDate = pageController.CurrentContext.SearchHotelPage.CheckInDate;
            var checkOutDate = pageController.CurrentContext.SearchHotelPage.CheckOutDate;

            string searchDestination = pageController.CurrentContext.SearchHotelPage.SearchDestination;
            this.Master.PageHeading = "";
            searchHotelMessage.InnerText = pageController.CurrentContext.SearchHotelPage.IsSearchfromCurrentLocation == true ?
                                    pageConfig.PageDetail.PageMessages.GetMessage(NEAR_BY_HOTELS_PAGE_HEADING) :
                                    string.Format(pageConfig.PageDetail.PageHeading, searchDestination);
            if (checkInDate.HasValue && checkOutDate.HasValue)
            {
                this.hgcArrivalDateValue.InnerText = checkInDate.Value.ToString(Reference.SearchItineraryDateFormat);
                this.hgcDepartureDateValue.InnerText = checkOutDate.Value.ToString(Reference.SearchItineraryDateFormat);
                this.hgcChildrenDetailValue.InnerText = pageController.CurrentContext.SearchHotelPage.ChildrenDetailText;
                var timeSpan = checkOutDate.Value.Subtract(checkInDate.Value);
                hgcNumOfNightsValue.InnerText = timeSpan.Days.ToString();
            }
            string ageText = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(AGE_TEXT);
            string adultText = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(ADULT_TEXT);
            string childrenText = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(CHILDREN_TEXT);
            if (pageController.CurrentContext.SearchHotelPage.ChildrenInformation != null && pageController.CurrentContext.SearchHotelPage.ChildrenInformation.Count > 0)
            {
                StringBuilder sBuilder = new StringBuilder();
                sBuilder.Append(LEFT_PARENTHESES);
                sBuilder.Append(ageText);
                sBuilder.Append(SPACE);
                int count = 0;
                foreach (ChildrenInfo child in pageController.CurrentContext.SearchHotelPage.ChildrenInformation)
                {
                    if (count == 0)
                        sBuilder.Append(child.Age);
                    else if (count < pageController.CurrentContext.SearchHotelPage.ChildrenInformation.Count)
                    {
                        sBuilder.Append(COMMA);
                        sBuilder.Append(SPACE);
                        sBuilder.Append(child.Age);
                    }
                    count++;
                }
                sBuilder.Append(RIGHT_PARENTHESES);
                hgcAdultsValue.InnerText = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}", pageController.CurrentContext.SearchHotelPage.NumberOfAdults.Value.ToString(), SPACE, adultText, COMMA,
                 SPACE, pageController.CurrentContext.SearchHotelPage.ChildrenInformation.Count, SPACE, childrenText,
                 SPACE, sBuilder.ToString());
            }
            else
            {
                hgcAdultsValue.InnerText = string.Format("{0} {1} {2}", pageController.CurrentContext.SearchHotelPage.NumberOfAdults.Value.ToString(), SPACE, adultText);
            }
            if (pageController.CurrentContext.SearchHotelPage.IsSearchfromCurrentLocation == true)
            {
                this.ddlSort.SelectedIndex = 5;
            }

            if (pageController != null && pageController.CurrentContext != null && pageController.CurrentContext.SearchHotelPage != null
                && !string.IsNullOrEmpty(pageController.CurrentContext.SearchHotelPage.SearchDestinationId))
            {
                bool hideCityTARatings = false;
                AvailabilityController availabilityController = new AvailabilityController();
                if (!pageController.CurrentContext.SearchHotelPage.IsAlternateSearch)
                {
                    hideCityTARatings = availabilityController.GetCityTripAdvisorFlag(pageController.CurrentContext.SearchHotelPage.SearchDestinationId);
                }
                else
                {
                    PageData pageddata = ContentDataAccess.GetPageDataByOperaID(pageController.CurrentContext.SearchHotelPage.SearchDestinationId, pageController.CurrentContext.SearchHotelPage.SearchDestination);
                    if (pageddata != null)
                    {
                        string cityOperaID = string.Empty;
                        PageData cityPageData = ContentDataAccess.GetPageData((ContentDataAccess.GetPageData(pageddata.ParentLink.ID)).ParentLink.ID);
                        if (cityPageData != null)
                            cityOperaID = Convert.ToString(cityPageData["OperaID"]);
                        hideCityTARatings = !string.IsNullOrEmpty(cityOperaID) ? availabilityController.GetCityTripAdvisorFlag(cityOperaID) : false;
                    }
                }
                if (hideCityTARatings)
                {
                    ddlSort.Items.Remove(ddlSort.Items.FindByValue("6"));
                }
                availabilityController = null;
            }

            //if (pageController != null && pageController.CurrentContext != null && pageController.CurrentContext.SearchHotelPage != null
            //    && pageController.CurrentContext.SearchHotelPage.SearchType == SearchType.Regular &&
            //    !string.IsNullOrEmpty(pageController.CurrentContext.SearchHotelPage.BookingCode))
            //{
            //    ddlSort.SelectedIndex = 7;
            //}
            //else
            //    ddlSort.Items.Remove(ddlSort.Items.FindByValue("7"));

            if (pageController != null && pageController.CurrentContext != null && pageController.CurrentContext.SearchHotelPage != null
                && pageController.CurrentContext.SearchHotelPage.SearchType == SearchType.Regular &&
                !string.IsNullOrEmpty(pageController.CurrentContext.SearchHotelPage.BookingCode))
            {
                isPromoSearch = true;
            }

            this.ddlSort.Attributes.Add("onchange", "OnSort(this);");

            maxNumberOfResults = appConfig.GetMessage(Reference.MaxNumberOfSearchHotelResultsKey);
            numberOfHotelToRequestOnAjaxCall = appConfig.GetMessage(Reference.NumberOfHotelToRequestOnAjaxCallKey);
            telephoneNumber = string.Format("tel:{0}", pageConfig.PageDetail.PageMessages.GetMessage(HOTEL_PHONE));
            telephoneNumberLinkText = pageConfig.PageDetail.PageMessages.GetMessage(RING_US_TEXT);
            fromText = pageConfig.PageDetail.PageMessages.GetMessage(FROM_TEXT);
            bookText = pageConfig.PageDetail.PageMessages.GetMessage(BOOK_TEXT);
            //promoHotelRegularPriceFromText = pageConfig.PageDetail.PageMessages.GetMessage(PROMO_HOTEL_REGULARPRICE_FROM_TEXT);
            promoCodeText = pageConfig.PageDetail.PageMessages.GetMessage(PROMO_CODE_TEXT);
            noDiscountAvailableText = pageConfig.PageDetail.PageMessages.GetMessage(NODISCOUNT_AVAILABLE_TEXT);
            this.Master.PageId = PageId();
            string searchPageURL = pageController.GetPageUrl(MobilePages.Search);
            lnkBack.NavigateUrl = searchPageURL;
            this.editSearch.Attributes.Add("href", searchPageURL);
            if (pageController.CurrentContext.SelectHotelPage != null)
            {
                pageController.CurrentContext.SelectHotelPage.IsHotelSelectedByUser = false;
            }
        }

        /// <summary>
        /// PageId
        /// </summary>
        /// <returns>MobilePages</returns>
        public override MobilePages PageId()
        {
            return MobilePages.SelectHotel;
        }

        #region WebMethod

        /// <summary>
        /// GetHotelsToDisplay
        /// </summary>
        /// <param name="currentlyDisplayedHotels"></param>
        /// <param name="noOfHotelsToDisplay"></param>
        /// <param name="sortBy"></param>
        /// <returns>IEnumerable</returns>
        [WebMethod]
        //artf1294664 : Mobile - Price issues on Iphone
        //public static IEnumerable GetHotelsToDisplay(int currentlyDisplayedHotels, int noOfHotelsToDisplay, string sortBy)
        public static IEnumerable GetHotelsToDisplay(int currentlyDisplayedHotels, int noOfHotelsToDisplay, string sortBy, string timeout)
        {
            var controller = new SelectHotelController();
            try
            {
                return controller.GetAvailableHotels(currentlyDisplayedHotels, noOfHotelsToDisplay, sortBy);
            }
            catch (Exception ex)
            {
                ActionItem action = Utilities.GetActionItemToTrack(controller.CurrentContext);
                TrackActionParametersAndLogData("SelectHotel", "GetHotelsToDisplay", currentlyDisplayedHotels, noOfHotelsToDisplay,
                    sortBy, timeout, ex, action, true);
                throw ex;
            }
        }

        private static void TrackActionParametersAndLogData(string className, string methodName, int currentlyDisplayedHotels,
           int noOfHotelsToDisplay, string sortBy, string timeout, Exception ex, ActionItem action, bool clearSession)
        {
            if (action != null && action.Parameters != null)
            {
                action.Parameters.Add(new KeyValueParam(Reference.PAGE_NAME, className));
                action.Parameters.Add(new KeyValueParam(Reference.METHOD_NAME, string.Format("{0}:{1}-", className, methodName)));
                action.Parameters.Add(new KeyValueParam(Reference.CURRENTLY_RENDERED_ROOMTYPES, Convert.ToString(currentlyDisplayedHotels)));
                action.Parameters.Add(new KeyValueParam(Reference.NOOF_ROOMTYPES_TODISPLAY, Convert.ToString(noOfHotelsToDisplay)));
                action.Parameters.Add(new KeyValueParam(Reference.CURRENTLY_RENDERED_ROOMTYPES, sortBy));
                action.Parameters.Add(new KeyValueParam(Reference.CURRENTLY_RENDERED_ROOMTYPES, timeout));
                UserNavTracker.TrackAction(action, clearSession);
                UserNavTracker.LogAndClearTrackedData(ex != null ? ex :
                    new Exception(string.Format("Generic-Error in {0} Page {1} method", className, methodName)));
            }
        }

        #endregion
    }
}