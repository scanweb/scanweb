﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Container.aspx.cs" Inherits="Scandic.Scanweb.Mobile.Templates.Booking.Container" ResponseEncoding="utf-8" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head1" runat="server">
    <title><EPiServer:Property ID="Property2" PropertyName="PageName" runat="server" /></title>
    <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
</head>
<body>
    <form runat="server" id="form1">
    <div>
        <h1><EPiServer:Property PropertyName="PageName" runat="server" /></h1>
        <h2><EPiServer:Property PropertyName="PageTypeName" runat="server" /></h2>
        <asp:Table ID="PropertyTable" runat="server" HorizontalAlign="Left" GridLines="Both">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell Width="150" HorizontalAlign="Left">
                Property Name
                </asp:TableHeaderCell>
                <asp:TableHeaderCell width="500" HorizontalAlign="Left">
                Value
                </asp:TableHeaderCell>
            </asp:TableHeaderRow>
        </asp:Table>
    </div>
    </form>
</body>
</html>
 