<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="BookingDetail.aspx.cs"
    Inherits="Scandic.Scanweb.Mobile.Templates.Booking.BookingDetail" MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master"
    ResponseEncoding="utf-8" %>

<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<%@ Register Assembly="Scandic.Scanweb.Mobile" Namespace="Scandic.Scanweb.Mobile.UI.Controls"  TagPrefix="Mobile" %>
<asp:Content ContentPlaceHolderID="cphMain" ID="bookingDetailsMain" runat="server">    
    <asp:Repeater ID="infomationRepeater" runat="server">
        <ItemTemplate>
            <p class="icon information" runat="server" id="hgcInfo" ></p>
        </ItemTemplate>        
    </asp:Repeater>
    
   <div class="standard-form" id="validationSummary">
       <span class="error" id="hgcValidationSummaryheadingError" runat="server"></span>
       <span class="errorsummary" id="bulletErrorList"></span>
    </div>
   
    <ul class="info-list">
        <li class="cf"><span id="hgcHotel" runat="server"></span><span id="hgcHotelValue"
            runat="server"></span></li>
        <li class="cf" runat="server" id="hgcMembershipItem"><span id="hgcMembershipId" runat="server"></span><span id="hgcMembershipIdValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcArrivalDate" runat="server"></span><span id="hgcArrivalDateValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcDepartureDate" runat="server"></span><span id="hgcDepartureDateValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcNumOfNights" runat="server"></span><span id="hgcNumOfNightsValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcAdults" runat="server"></span><span id="hgcAdultsValue"
            runat="server"></span></li>
        
         <li class="cf" id="liChildrenInfo" style="display: none;" runat="server">
            <span id="hgcBedType" runat="server"></span>
            <span id="hgcChildrenDetailValue" runat="server"></span>
        </li>
        <li class="cf"><span id="hgcRoomType" runat="server"></span><span id="hgcRoomTypeValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcRateType" runat="server"></span><span id="hgcRateTypeValue"
            runat="server"></span><span id="hgcRateTypeText" class="rateHighlightTypeText" runat="server"></span></li>
        <li class="cf" id="hgcFirstNightPriceRow" runat="server"><span id="hgcFirstNightPrice" runat="server"></span><span 

id="hgcFirstNightPriceValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcTotalPrice" runat="server"></span><span id="hgcTotalPriceValue"
            runat="server"></span></li>
    </ul>
    <div class="standard-form">
        <p>
            <asp:LinkButton ID="lbtnSignIn" runat="server" CssClass="icon information"></asp:LinkButton>
            <asp:HyperLink ID="lnkUpdateProfile" runat="server" CssClass="icon information"  ></asp:HyperLink>            
        </p>
        <p><asp:Label ID="lblMandatoryFieldsInfoMessage" Text="* indicates a required field" runat="server"></asp:Label></p>
        <fieldset>
            <asp:Label ID="lblFirstName" runat="server" Text="First Name" AssociatedControlID="txtFirstName"></asp:Label>            
            <span class="error" runat="server" id="hgcFirstNameError"></span>
            <asp:TextBox ID="txtFirstName" runat="server" CssClass="history required"></asp:TextBox>
        </fieldset>
        <fieldset>
            <asp:Label ID="lblLastName" runat="server" Text="Last Name" AssociatedControlID="txtLastName"></asp:Label>
            <span class="error" runat="server" id="hgcLastNameError"></span>
            <asp:TextBox ID="txtLastName" runat="server" CssClass="history required"></asp:TextBox>
        </fieldset>
        <fieldset>
            <asp:Label ID="lblEmail" runat="server" Text="Email" AssociatedControlID="txtEmail"></asp:Label>
            <span class="error" runat="server" id="hgcEmailError"></span>
            <Mobile:EmailTextBox Id="txtEmail" runat="server" CssClass="history required"></Mobile:EmailTextBox>
        </fieldset>        
        <fieldset>
            <asp:Label ID="lblCountry" runat="server" Text="Country" AssociatedControlID="ddlCountry"></asp:Label>
            <span class="error" runat="server" id="hgcCountryError"></span>
            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="history required">
            </asp:DropDownList>
        </fieldset>
        <fieldset>
            <asp:Label ID="lblCountryCode" runat="server" Text="Country code" AssociatedControlID="ddlCountryCode"></asp:Label>            
            <span class="error" runat="server" id="hgcPhoneCountryCodeError"></span>
            <asp:DropDownList ID="ddlCountryCode" runat="server" CssClass="history required">
            </asp:DropDownList>
        </fieldset>
        <fieldset>
            <asp:Label ID="lblMobile" runat="server" Text="City" AssociatedControlID="txtMobile"></asp:Label>
            <span class="error" runat="server" id="hgcMobileError"></span>
            <Mobile:NumberTextBox ID="txtMobile" runat="server" CssClass="history required"></Mobile:NumberTextBox>
        </fieldset>
        <fieldset runat="server" id="hgcDnumberContainer">
            <asp:Label ID="lblDnumber" runat="server" Text="DNumber" AssociatedControlID="txtDnumber"></asp:Label>
            <asp:TextBox ID="txtDnumber" runat="server"></asp:TextBox>
        </fieldset>
        <fieldset>
            <asp:CheckBox ID="chkSMSConfirmation" runat="server" />
            <asp:Label ID="lblSMSConfirmation" runat="server" Text="SMS Confirmation" AssociatedControlID="chkSMSConfirmation"></asp:Label>
        </fieldset>
        <fieldset>
            <asp:Label ID="lblBedPreferences" runat="server" Text="BedPreferences" AssociatedControlID="ddlBedPreferences"></asp:Label>
            <a href="#" data-target="overlay-bedpreferences" class="icon tooltip" data-overlaytype="tooltip"
                data-associatedcontrol="lblBedPreferences" onclick="LoadOverlay(this); return false;">
                &nbsp;</a>
            <span class="error" runat="server" id="hgcBedPreferencesError"></span>                
            <asp:DropDownList ID="ddlBedPreferences" runat="server">
            </asp:DropDownList>
        </fieldset>
        <p>
            <asp:HyperLink ID="lnkOtherRequests" runat="server" CssClass="icon information toggler"
                data-target="otherRequests" NavigateUrl="#">Other Request</asp:HyperLink>
        </p>
        <div id="otherRequests" class="hidden">
            <fieldset>
                <h4 id="hgcOtherRequests" runat="server">
                </h4>
                <asp:Label ID="lblRoomFloor" runat="server" Text="Room floor" AssociatedControlID="ddlRoomFloor"></asp:Label>
                <asp:DropDownList ID="ddlRoomFloor" runat="server">
                </asp:DropDownList>
            </fieldset>
            <fieldset>
                <asp:Label ID="lblNearElevator" runat="server" Text="Near elevator" AssociatedControlID="ddlNearElevator"></asp:Label>
                <asp:DropDownList ID="ddlNearElevator" runat="server">
                </asp:DropDownList>
            </fieldset>
            <fieldset>
                <asp:Label ID="lblSmoking" runat="server" Text="Smoking" AssociatedControlID="ddlSmoking"></asp:Label>
                <asp:DropDownList ID="ddlSmoking" runat="server">
                </asp:DropDownList>
            </fieldset>
            <fieldset>
                <asp:Label ID="lblAdditionalRequest" runat="server" Text="Any other requests" 
                    AssociatedControlID="hgcAdditionalRequest"></asp:Label>
                <textarea id="hgcAdditionalRequest" rows="4" maxlength="255" runat="server" ></textarea>
            </fieldset>
            <fieldset>
                <div class="accessible-room-wrapper">
                    <asp:CheckBox ID="chkAccessibleRoom" runat="server" />
                    <asp:Label ID="lblAccessibleRoom" runat="server" Text="Accessible Room" AssociatedControlID="chkAccessibleRoom"></asp:Label>
                    <div class="additional-info">	
                        <span class="additional-info-icon icon"></span>
	                    <span class="additional-info-txt" id="accessibleRoomDescription" runat="server"></span>
                    </div>
                </div>
                <div class="allergy-room-wrapper">
                    <asp:CheckBox ID="chkAllergyFriendlyRoom" runat="server" />
                    <asp:Label ID="lblAllergyFriendlyRoom" runat="server" Text="Allergy Friendly Room"
                        AssociatedControlID="chkAllergyFriendlyRoom"></asp:Label>
                    <div class="additional-info">
                        <span class="additional-info-icon icon"></span>	
	                    <span class="additional-info-txt" id="allergyRoomDescription" runat="server"></span>
                    </div>
                </div>    
                <div class="petfriendly-room-wrapper">            
                    <asp:CheckBox ID="chkPetFriendlyRoom" runat="server" />
                    <asp:Label ID="lblPetFriendlyRoom" runat="server" Text="Pet Friendly Room" 
                        AssociatedControlID="chkPetFriendlyRoom"></asp:Label>
                     <div class="additional-info">	
	                    <span class="additional-info-icon icon"></span>
	                    <span class="additional-info-txt" id="petRoomDescription" runat="server"></span>
                    </div>
                 </div>
                
            </fieldset>
        </div>
        <fieldset>
            <h4>
                <span id="hgcDepositInfo" runat="server"></span>
                <asp:HyperLink ID="lnkRedemptionGuranteeToolTip" runat="server" NavigateUrl="#" CssClass="icon tooltip" 
                data-target="overlay-rdodepositeinfo"
                data-overlaytype="tooltip" data-associatedcontrol="hgcDepositInfo" onclick="LoadOverlay(this); return false;" >&nbsp;</asp:HyperLink>
            </h4>            
            <span runat="server" id="hgcRedemptionGuranteeInfo"></span>
            <span runat="server" id="spnCancellationText"></span><br/>
            <asp:CheckBox ID="rdoDepositeInfo" runat="server">
            </asp:CheckBox>
        </fieldset>
        <br/>
        <div ID="divFGPSavedCreditCards"  runat="server" visible="false">
        <fieldset class="creditCardPicker">
            <h4>
            <asp:Label runat="server" ID="lblCreditCardSelectionHeader" ></asp:Label>
            <img src="/ScanwebMobile/Public/img/genericCreditCardLarge.png">
            </h4>
          <asp:DropDownList runat="server" ID="ddlFGPSavedCreditCards"/>
        </fieldset>
        </div>
        <div id="cardInformation" class="hidden">
            <span runat="server" id="spnCCHiddenText"></span><br/>
            <img width="100" height="50" alt="SSL certificate by Verisign" src="/ScanwebMobile/Public/img/verisign.png">
            <fieldset>
                <asp:Label ID="lblCardHolderName" runat="server" Text="" AssociatedControlID="txtCardHolderName"></asp:Label>
                <span class="error" runat="server" id="hgcCardHolderNameError"></span>
                <asp:TextBox ID="txtCardHolderName" runat="server"></asp:TextBox>
            </fieldset>
            <fieldset>
                <asp:Label ID="lblCardNumber" runat="server" Text="" AssociatedControlID="txtCardNumber"></asp:Label>
                <span class="error" runat="server" id="hgcCardNumberError"></span>
                <asp:TextBox ID="txtCardNumber" runat="server"></asp:TextBox>
            </fieldset>
            <fieldset>
                <asp:Label ID="lblCardType" runat="server" Text="" AssociatedControlID="ddlCardType"></asp:Label>
                <span class="error" runat="server" id="hgcCardTypeError"></span>
                <asp:DropDownList ID="ddlCardType" runat="server">
                </asp:DropDownList>
            </fieldset>
            <fieldset>
                <asp:Label ID="lblCardValidMonth" runat="server" Text="" AssociatedControlID="ddlCardValidMonth"></asp:Label>
                <span class="error" runat="server" id="hgcCardValidMonthError"></span>
                <asp:DropDownList ID="ddlCardValidMonth" runat="server">
                </asp:DropDownList>
            </fieldset>
            <fieldset>
                <asp:Label ID="lblCardValidYear" runat="server" Text="" AssociatedControlID="ddlCardValidYear"></asp:Label>
                <span class="error" runat="server" id="hgcCardValidYearError"></span>
                <asp:DropDownList ID="ddlCardValidYear" runat="server">
                </asp:DropDownList>
            </fieldset>
        </div>
        <div id="divFlexFGPCreditCardsForPayment" class="hidden">
             <fieldset>
                 <asp:Label runat="server" ID="lblCreditCardForGuaranteeBooking"></asp:Label>
                 <span class="error" runat="server" id="hgcFlexFGPCreditCardSelectionError"></span>
                 <asp:DropDownList runat="server" ID="ddlFlexFGPCreditCards"/>
             </fieldset>
        </div>
        <fieldset>
            <h4 id="hgcBookingRule" runat="server">
            </h4>
            <span class="error" runat="server" id="hgcBookingRuleError"></span>
            <asp:CheckBox ID="chkAcceptedCondition" runat="server" CssClass="fltLft" />
            <asp:Label ID="lblAcceptedCondition" runat="server" Text="" AssociatedControlID="chkAcceptedCondition" CssClass="fltLft" 

Width="90%"></asp:Label>
        </fieldset>
        <asp:LinkButton ID="lbtnBook" runat="server" CssClass="button blue hand submit"></asp:LinkButton>
    </div>
    <asp:HiddenField runat="server" id="hidGuranteeType" ></asp:HiddenField>
</asp:content>
<asp:content contentplaceholderid="cphScript" id="bookingDetailsScripts" runat="server">
    <script type="text/javascript">
	    var phoneCodes;
	    var readFromHistory = ("1" === "<%=readFromHistory %>");
	    var saveInHistory = ("1" === "<%=saveInHistory %>");
	    var guranteeType = "<%=guranteeType %>";

	    //create a validation summary
	    function displayErrorSummary() {
	        var spanVisibleErrorCollection = $(".error:visible");
	        $('#bulletErrorList').html("");
	        if (spanVisibleErrorCollection.length > 0) {
	            for (var i = 1; i < spanVisibleErrorCollection.length; i++) {
	                var errorText = spanVisibleErrorCollection[i].innerHTML;
	                if (errorText != "") {
	                    $('#bulletErrorList').append("- " + errorText + "<br>");
	                }
	            }
	        }
	        else {
	            $('#validationSummary').hide();
	        }
	    }

	    $(document).ready(function() {
	        if ("<%= isPaymentFallbackEnabled %>" == "True")
	            $("#cardInformation").show();
	        else {
	            if ($("input[id$='hidGuranteeType']").val() == "GTDCC") {
	                $("#cardInformation").show();
	            }
	            else {
	                $("#cardInformation").hide();
	            }
	        }	        

	        $("input[id$='rdoDepositeInfo']").checked = false;
	        displayErrorSummary();
	        $("input[type=text],input[type=email],input[type=number],select").bind("change", displayErrorSummary);

	        if ("<%=IsRewardNightsBooking%>" == "true")
	            $("#cardInformation").hide();
	    });

    </script>
    <script src="<%= ResolveUrl("~/ScanwebMobile/Public/Scripts/BookingDetails.min.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>

</asp:content>
