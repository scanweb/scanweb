﻿//  Description					: SelectRate                                              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Web.Services;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.Mobile.UI.Entity.Model;
using Scandic.Scanweb.Core;
using System.Text;
using System.Linq;
using Scandic.Scanweb.Mobile.UI.Entity;
using System.Collections.Generic;
using Scandic.Scanweb.Mobile.UI.Content;
namespace Scandic.Scanweb.Mobile.Templates.Booking
{
    /// <summary>
    /// Code behind of SelectRate page.
    /// </summary>
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionInValid(false, true)]
    public partial class SelectRate : HotelBasePage<SelectRatePageSection>
    {
        private SelectRateController pageController;
        protected string maxNumberOfRoomTypesPerPage;
        private const string AGE_TEXT = "AgeText";
        private const string ADULT_TEXT = "AdultText";
        private const string CHILDREN_TEXT = "ChildrenText";
        private const string SPACE = " ";
        private const string COMMA = ",";
        private const string LEFT_PARENTHESES = "(";
        private const string RIGHT_PARENTHESES = ")";
        private const string ROOM_TYPE = "RO1";
        private const string RATE_NAME = "RA1";
        protected string bookNowText;
        protected string pointsText;
        private const string BOOK_NOW = "BookNow";
        //protected string hotelID;
        /// <summary>
        /// Oninit event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            pageController = new SelectRateController();
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/Booking/SelectRate.aspx";
        }

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (IsSelectRoomRateDeepLinkRequest())
                {
                    ProcessDeepLinkRoomRateSelection();
                }
                LoadControls();
            }
            else
            {
                Reservation2SessionWrapper.NetsPaymentErrorMessage = string.Empty;
                Reservation2SessionWrapper.NetsPaymentInfoMessage = string.Empty;
                ProcessRequest();
            }
        }

        private void ProcessDeepLinkRoomRateSelection()
        {
            var selectedRoomType = Request.QueryString[ROOM_TYPE];
            var selectedRateType = Request.QueryString[RATE_NAME];
            int maxNoOfRoomTypesToDisplay;
            var pageGenConfig = pageController.GetGeneralConfig<ApplicationConfigSection>();
            int.TryParse(pageGenConfig.GetMessage(Reference.MOBILE_DEEPLINK_NO_OF_ROOMTYPES_TO_DISPLAY), out maxNoOfRoomTypesToDisplay);
            var availableRoomType = pageController.GetRoomTypes(0, maxNoOfRoomTypesToDisplay);
            if (availableRoomType != null && availableRoomType.Count == 1)
            {
                if (!availableRoomType[0].IsAlternateSearch)
                {
                    if (!string.IsNullOrEmpty(selectedRateType))
                    {
                        selectedRateType = selectedRateType.Trim();
                    }
                    if (!string.IsNullOrEmpty(selectedRoomType))
                    {
                        selectedRoomType = selectedRoomType.Trim();
                    }
                    var deeplinkRoomType = (from roomType in availableRoomType[0].RoomTypes
                                            where string.Equals(roomType.Name, selectedRoomType, StringComparison.InvariantCultureIgnoreCase)
                                            from rateType in roomType.Rates
                                            where string.Equals(rateType.Name, selectedRateType, StringComparison.InvariantCultureIgnoreCase)
                                            select new RoomTypeDetails
                                            {
                                                Description = roomType.Description,
                                                Id = roomType.Id,
                                                ImagePath = roomType.ImagePath,
                                                Name = roomType.Name,
                                                PerNight = roomType.PerNight,
                                                Rates = new List<RateTypeDetails> { rateType }
                                            }).FirstOrDefault();
                    if (deeplinkRoomType != null && deeplinkRoomType.Rates != null && deeplinkRoomType.Rates.Count == 1)
                    {
                        string selectedValue = string.Format("{0},{1},{2},{3},{4},{5},{6}", deeplinkRoomType.Rates[0].Rate, deeplinkRoomType.Rates[0].Id,
                            deeplinkRoomType.Rates[0].HoldGuranteeAvailable, deeplinkRoomType.Rates[0].CreditCardGuranteeType, deeplinkRoomType.Name, deeplinkRoomType.Id,
                            deeplinkRoomType.Rates[0].Name);
                        hidValue.Value = selectedValue;
                        pageController.SelectRate(ProcessInput());
                    }

                }
                else
                {
                    if (!string.IsNullOrEmpty(availableRoomType[0].AlternativeSearchUrl))
                    {
                        pageController.Redirect(availableRoomType[0].AlternativeSearchUrl);
                    }
                }
            }
        }

        private bool IsSelectRoomRateDeepLinkRequest()
        {
            bool result = false;
            var pageGenConfig = pageController.GetGeneralConfig<ApplicationConfigSection>();
            var mobileDeeplinkProcessing = pageGenConfig.GetMessage(Reference.MOBILE_DEEPLINK_ROOMRATE_PROCESSING);

            if (string.Equals(mobileDeeplinkProcessing, "true", StringComparison.InvariantCultureIgnoreCase) && (!string.IsNullOrEmpty(Request.QueryString[ROOM_TYPE]) &&
                !string.IsNullOrEmpty(Request.QueryString[RATE_NAME])))
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        /// Loads required controls.
        /// </summary>
        private void LoadControls()
        {
            var pageConfig = pageController.GetPageConfig<SelectRatePageSection>();

            if (pageConfig != null && pageController.CurrentContext != null)
            {
                var checkInDate = pageController.CurrentContext.SearchHotelPage.CheckInDate;
                var checkOutDate = pageController.CurrentContext.SearchHotelPage.CheckOutDate;

                this.Master.PageHeading = "";
                //    string.Format(pageConfig.PageDetail.PageHeading, pageController.CurrentContext.SelectHotelPage != null ?
                //pageController.CurrentContext.SelectHotelPage.SelectedHotel : null);
                if (checkInDate.HasValue && checkOutDate.HasValue)
                {
                    this.hgcArrivalDateValue.InnerText = checkInDate.Value.ToString(Reference.SearchItineraryDateFormat);
                    this.hgcDepartureDateValue.InnerText = checkOutDate.Value.ToString(Reference.SearchItineraryDateFormat);
                    var timeSpan = checkOutDate.Value.Subtract(checkInDate.Value);
                    hgcNumOfNightsValue.InnerText = timeSpan.Days.ToString();
                }
                string searchPageUrl = pageController.GetPageUrl(MobilePages.Search);
                lnkBack.NavigateUrl = searchPageUrl;
                this.editSearch.Attributes.Add("href", searchPageUrl);
                string ageText = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(AGE_TEXT);
                string adultText = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(ADULT_TEXT);
                string childrenText = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(CHILDREN_TEXT);
                if (pageController.CurrentContext.SearchHotelPage.ChildrenInformation != null && pageController.CurrentContext.SearchHotelPage.ChildrenInformation.Count > 0)
                {
                    StringBuilder sBuilder = new StringBuilder();
                    sBuilder.Append(LEFT_PARENTHESES);
                    sBuilder.Append(ageText);
                    sBuilder.Append(SPACE);
                    int count = 0;
                    foreach (ChildrenInfo child in pageController.CurrentContext.SearchHotelPage.ChildrenInformation)
                    {
                        if (count == 0)
                            sBuilder.Append(child.Age);
                        else if (count < pageController.CurrentContext.SearchHotelPage.ChildrenInformation.Count)
                        {
                            sBuilder.Append(COMMA);
                            sBuilder.Append(SPACE);
                            sBuilder.Append(child.Age);
                        }
                        count++;
                    }
                    sBuilder.Append(RIGHT_PARENTHESES);
                    hgcAdultsValue.InnerText = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}", pageController.CurrentContext.SearchHotelPage.NumberOfAdults.Value.ToString(), SPACE, adultText, COMMA,
                 SPACE, pageController.CurrentContext.SearchHotelPage.ChildrenInformation.Count, SPACE, childrenText,
                 SPACE, sBuilder.ToString());
                }
                else
                {
                    hgcAdultsValue.InnerText = string.Format("{0} {1} {2}", pageController.CurrentContext.SearchHotelPage.NumberOfAdults.Value.ToString(), SPACE, adultText);
                }
                hgcChildrenDetailValue.InnerText = pageController.CurrentContext.SearchHotelPage.ChildrenDetailText;
            }
            //hotelID = pageController.CurrentContext.SearchHotelPage.SearchDestinationId;
            maxNumberOfRoomTypesPerPage = "3";
            bookNowText = pageController.GetPageConfig<SelectRatePageSection>().PageDetail.PageMessages.GetMessage(BOOK_NOW);
            pointsText = pageController.GetPageConfig<SelectRatePageSection>().PageDetail.PageMessages.GetMessage("Points");
            this.Master.PageId = PageId();
        }

        /// <summary>
        /// ProcessRequest
        /// </summary>
        private void ProcessRequest()
        {
            switch (this.Master.PostBackProcess.Value)
            {
                case "SelectRate":
                    pageController.SelectRate(ProcessInput());
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// ProcessInput
        /// </summary>
        /// <returns>SelectRateModel</returns>
        private SelectRateModel ProcessInput()
        {
            var pageModel = new SelectRateModel();
            var selectedData = hidValue.Value.Split(',');
            RateCategory rateCategoryItem = null;
            pageModel.SelectedRate = selectedData[0];
            pageModel.SelectedRateTypeId = selectedData[1];
            pageModel.HoldGuranteeAvailable = Convert.ToBoolean(selectedData[2]);
            pageModel.CreditCardGuranteeType = selectedData[3];
            pageModel.SelectedRoomType = selectedData[4];
            pageModel.SelectedRoomTypeId = selectedData[5];
            pageModel.SelectedRateType = selectedData[6];
            RoomRateResults roomRateCategoryresults = MiscellaneousSessionWrapper.HotelRoomRateResults as RoomRateResults;
            foreach (RateCategory rateCatecogies in roomRateCategoryresults.RateCategories)
            {
                if (rateCatecogies.RateCategoryId == pageModel.SelectedRateTypeId)
                {
                    rateCategoryItem = rateCatecogies;
                    break;
                }
            }
            pageModel.SelectedRateHighlightText = rateCategoryItem.RateHighlightTextMobile;
            pageModel.SelectedRateLanguage = rateCategoryItem.RateCategoryLanguage;
            return pageModel;
        }

        /// <summary>
        /// Gets PageId
        /// </summary>
        /// <returns></returns>
        public override MobilePages PageId()
        {
            return MobilePages.SelectRate;
        }

        #region WebMethod

        /// <summary>
        /// Gets room types to display.
        /// </summary>
        /// <param name="currentlyRenderedRoomTypes"></param>
        /// <param name="noOfRoomTypesToDisplay"></param>
        /// <returns>room types </returns>
        [WebMethod]
        //artf1294664 : Mobile - Price issues on Iphone
        //public static IEnumerable GetRoomsTypesToDisplay(int currentlyRenderedRoomTypes, int noOfRoomTypesToDisplay)
        public static IEnumerable GetRoomsTypesToDisplay(int currentlyRenderedRoomTypes, int noOfRoomTypesToDisplay, string timeout)
        {
            var controller = new SelectRateController();
            try
            {
                return controller.GetRoomTypes(currentlyRenderedRoomTypes, noOfRoomTypesToDisplay);
            }
            catch (Exception ex)
            {
                ActionItem action = Utilities.GetActionItemToTrack(controller.CurrentContext);
                TrackActionParametersAndLogData("SelectRate", "GetRoomsTypesToDisplay", currentlyRenderedRoomTypes, noOfRoomTypesToDisplay,
                    ex, action, true);
                throw ex;
            }
        }

        private static void TrackActionParametersAndLogData(string className, string methodName, int currentlyRenderedRoomTypes,
            int noOfRoomTypesToDisplay, Exception ex, ActionItem action, bool clearSession)
        {
            if (action != null && action.Parameters != null)
            {
                action.Parameters.Add(new KeyValueParam(Reference.PAGE_NAME, className));
                action.Parameters.Add(new KeyValueParam(Reference.METHOD_NAME, string.Format("{0}:{1}-", className, methodName)));
                action.Parameters.Add(new KeyValueParam(Reference.CURRENTLY_RENDERED_ROOMTYPES, Convert.ToString(currentlyRenderedRoomTypes)));
                action.Parameters.Add(new KeyValueParam(Reference.NOOF_ROOMTYPES_TODISPLAY, Convert.ToString(noOfRoomTypesToDisplay)));
                UserNavTracker.TrackAction(action, clearSession);
                UserNavTracker.LogAndClearTrackedData(ex != null ? ex : new Exception(string.Format("Generic-Error in {0} Page {1} method", className, methodName)));
            }
        }

        #endregion
    }
}