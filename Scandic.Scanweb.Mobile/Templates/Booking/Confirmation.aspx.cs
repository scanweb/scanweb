﻿using System;
using System.Text.RegularExpressions;
using System.Web.Services;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using System.Text;
using Scandic.Scanweb.Core;
using System.Web;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.BookingEngine.Web;
using System.Configuration;

namespace Scandic.Scanweb.Mobile.Templates.Booking
{
    /// <summary>
    /// Confirmation
    /// </summary>
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionInValid(false, false)]
    public partial class Confirmation : VisualBasePage<ConfirmationPageSection>
    {
        private ConfirmationController pageController;
        protected string BookMarkingScript;
        protected string BookMarkOverlayText;
        private const string AGE_TEXT = "AgeText";
        private const string ADULT_TEXT = "AdultText";
        private const string CHILDREN_TEXT = "ChildrenText";
        private const string SPACE = " ";
        private const string COMMA = ",";
        private const string LEFT_PARENTHESES = "(";
        private const string RIGHT_PARENTHESES = ")";
        /// <summary>
        /// OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            this.Error += new EventHandler(Confirmation_Error);
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/Booking/Confirmation.aspx";
            this.lbtnBookAnotherRoom.Click += new EventHandler(BookAnotherRoom_Click);
            pageController = new ConfirmationController();
        }

        void Confirmation_Error(object sender, EventArgs e)
        {
            var redirectUrl = string.Empty;
            Exception excpn = Server.GetLastError();
            var language = Request.Form["ctl00$hidCurrentLanguage"];
            if (string.IsNullOrEmpty(language))
            {
                language = LanguageRedirectionHelper.GetCurrentLanguage();
            }
            var errorPageUrl = pageController.GetPageUrl(MobilePages.Error, language);
            if (Request.QueryString["BNo"] != null && Request.QueryString["LNm"] != null)
            {
                AppLogger.LogCustomException(excpn, AppConstants.APPLICATION_EXCEPTION, string.Format("Booking No : {0} LastName : {1}", Convert.ToString(Request.QueryString["BNo"]), Convert.ToString(Request.QueryString["LNm"])));
                redirectUrl = string.Format("{0}?{1}={2}&lang={3}&BNo={4}&LNm={5}", errorPageUrl, Reference.QueryParameterErrorCode, Reference.MOBILE_CONFIRMATION, language, Convert.ToString(Request.QueryString["BNo"]), Convert.ToString(Request.QueryString["LNm"]));
            }
            else
            {
                redirectUrl = errorPageUrl;
            }
            Response.Redirect(redirectUrl);
        }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var pageGenConfig = pageController.GetGeneralConfig<ApplicationConfigSection>();
            var pageConfig = this.pageController.GetPageConfig<ConfirmationPageSection>();
            var bookMarkData = pageGenConfig.GetMessage(Reference.BookMarkOverlayKey);
            if (bookMarkData == Reference.SHOW)
            {
                BookMarkingScript = "/ScanwebMobile/Public/Scripts/add2home.js";
                BookMarkOverlayText = pageConfig.PageDetail.PageMessages.GetMessage(Reference.BookMarkOverlayText);
            }

            smssent.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage(Reference.SmsSentConfirmation);
            emailsent.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage(Reference.EmailSentConfirmation);

            if (!IsPostBack)
            {
                LoadControls();
            }
        }

        /// <summary>
        /// BookAnotherRoom_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BookAnotherRoom_Click(object sender, EventArgs e)
        {
            pageController.BookAnotherRoom();
        }

        /// <summary>
        /// LoadControls
        /// </summary>
        private void LoadControls()
        {
            SetBookingInfo();
            SetHotelDetails();
            SetPersonalInformation();
            hgcPolicyValue.InnerHtml =
                pageController.GetGuranteeCancellationText(pageController.CurrentContext.BookingDetailPage.ReservationGuaranteeOption);

            this.Master.PageId = PageId();

            SetTrivagoPixelTracking();
        }

        private void SetTrivagoPixelTracking()
        {
            HttpCookie TrivagoCampaigns = Request.Cookies[ConfigurationManager.AppSettings["SCampaigns"].ToString()];

            if (TrivagoCampaigns != null && string.Equals(TrivagoCampaigns.Value, ConfigurationManager.AppSettings["AffiliatedToTrivago"], StringComparison.InvariantCultureIgnoreCase))
            {
                string partnerID = Convert.ToString(ConfigurationManager.AppSettings["TrivagoPartnerID"]);

                string bucketID = string.Empty;
                string trivagoDomainID = string.Empty;
                if (Request.Cookies["TrivagoDomain"] != null)
                    trivagoDomainID = Convert.ToString(Request.Cookies["TrivagoDomain"].Value);

                if (Request.Cookies["TrivagoBucketID"] != null)
                    bucketID = Convert.ToString(Request.Cookies["TrivagoBucketID"].Value);
                string operaID = pageController.CurrentContext.SearchHotelPage.SearchDestinationId;
                DateTime arrDate = pageController.CurrentContext.SearchHotelPage.CheckInDate.Value;
                DateTime depDate = pageController.CurrentContext.SearchHotelPage.CheckOutDate.Value;
                string trivagoUrlscript = Utility.TrivagoTracking(partnerID, operaID, arrDate, depDate,
                    pageController.CurrentContext.BookingDetailPage.TotalRate, pageController.CurrentContext.BookingDetailPage.ReservationNumber,
                     trivagoDomainID, bucketID);

                if (!string.IsNullOrEmpty(trivagoUrlscript))
                {
                    HtmlImage img = new HtmlImage();
                    img.Height = 1;
                    img.Width = 1;
                    img.Style.Add("border-style", "none");
                    img.Src = trivagoUrlscript;
                    TrivagoPlaceHolder.Visible = true;
                    TrivagoPlaceHolder.Controls.Add(img);
                }
            }


        }

        /// <summary>
        /// SetBookingInfo
        /// </summary>
        private void SetBookingInfo()
        {
            var selectedhotel = pageController.SelectedHotel;
            var checkInDate = pageController.CurrentContext.SearchHotelPage.CheckInDate;
            var checkOutDate = pageController.CurrentContext.SearchHotelPage.CheckOutDate;
            var pageConfig = pageController.GetPageConfig<BookingDetailsPageSection>();
            string taxText = pageConfig.PageDetail.PageMessages.GetMessage(Reference.InclusiveAllTax);
            string ageText = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(AGE_TEXT);
            string adultText = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(ADULT_TEXT);
            string childrenText = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(CHILDREN_TEXT);
            hgcHotelValue.InnerText = selectedhotel.Name;
            if (checkInDate.HasValue && checkOutDate.HasValue)
            {
                hgcArrivalDateValue.InnerText = checkInDate.Value.ToString(Reference.IOS5DateFormat);
                hgcDepartureDateValue.InnerText = checkOutDate.Value.ToString(Reference.IOS5DateFormat);
            }
            if (pageController.CurrentContext.SearchHotelPage.ChildrenInformation != null && pageController.CurrentContext.SearchHotelPage.ChildrenInformation.Count > 0)
            {
                StringBuilder sBuilder = new StringBuilder();
                sBuilder.Append(LEFT_PARENTHESES);
                sBuilder.Append(ageText);
                sBuilder.Append(SPACE);
                int count = 0;
                foreach (ChildrenInfo child in pageController.CurrentContext.SearchHotelPage.ChildrenInformation)
                {
                    if (count == 0)
                        sBuilder.Append(child.Age);
                    else if (count < pageController.CurrentContext.SearchHotelPage.ChildrenInformation.Count)
                    {
                        sBuilder.Append(COMMA);
                        sBuilder.Append(SPACE);
                        sBuilder.Append(child.Age);
                    }
                    count++;
                }
                sBuilder.Append(RIGHT_PARENTHESES);
                hgcAdultsValue.InnerText = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}", pageController.CurrentContext.SearchHotelPage.NumberOfAdults.Value.ToString(), SPACE, adultText, COMMA,
                 SPACE, pageController.CurrentContext.SearchHotelPage.ChildrenInformation.Count, SPACE, childrenText,
                 SPACE, sBuilder.ToString());
            }
            else
            {
                hgcAdultsValue.InnerText = string.Format("{0} {1} {2}", pageController.CurrentContext.SearchHotelPage.NumberOfAdults.Value.ToString(), SPACE, adultText);
            }
            hgcChildrenDetailValue.InnerText = pageController.CurrentContext.SearchHotelPage.ChildrenDetailText;
            hgcRoomTypeValue.InnerText = pageController.CurrentContext.SelectRatePage.SelectedRoomType;
            hgcRateTypeValue.InnerText = pageController.CurrentContext.SelectRatePage.SelectedRateType;
            hgcRateTypeText.InnerText = LanguageRedirectionHelper.GetCurrentLanguage() == pageController.CurrentContext.SelectRatePage.SelectedRateLanguage ?
                                            pageController.CurrentContext.SelectRatePage.SelectedRateHighlightText : string.Empty;
            hgcFirstNightPriceValue.InnerText = string.Format("{0} {1}", pageController.CurrentContext.SelectRatePage.SelectedRate, taxText);
            hgcFirstNightPriceRow.Visible = (pageController.CurrentContext.CurrentBookingProcess != BookingProcess.ClaimRewardNights);
            hgcReservationNumberValue.InnerText = pageController.CurrentContext.BookingDetailPage.ReservationNumber;
            if (checkInDate.HasValue && checkOutDate.HasValue)
            {
                var timeSpan = checkOutDate.Value.Subtract(checkInDate.Value);
                hgcNumOfNightsValue.InnerText = timeSpan.Days.ToString();
            }
            hgcTotalPriceValue.InnerText = pageController.CurrentContext.BookingDetailPage.TotalRate;
        }

        /// <summary>
        /// SetHotelDetails
        /// </summary>
        private void SetHotelDetails()
        {
            var selectedhotel = pageController.SelectedHotel;

            hgcHotelName.InnerText = selectedhotel.Name;
            lnkHotelMobileNumber.Text = selectedhotel.Telephone;
            lnkHotelMobileNumber.NavigateUrl = string.Format("tel:{0}", Regex.Replace(selectedhotel.Telephone, "\\s", ""));
            lnkHotelEmail.Text = selectedhotel.Email;
            lnkHotelEmail.NavigateUrl = string.Format("mailto:{0}", selectedhotel.Email);
            string address = string.Format("{0} {1} {2} {3}",
                                           selectedhotel.HotelAddress.StreetAddress,
                                           selectedhotel.HotelAddress.PostCode,
                                           selectedhotel.HotelAddress.City,
                                           selectedhotel.HotelAddress.Country);
            lnkHotelAddress.Text = address;
            lnkHotelAddress.NavigateUrl = string.Format("http://maps.google.com/?q={0}", address);
        }

        /// <summary>
        /// SetPersonalInformation
        /// </summary>
        private void SetPersonalInformation()
        {
            var isAuthenticated = pageController.IsAuthenticated;

            hgcNameValue.InnerText = string.Format("{0} {1}", pageController.CurrentContext.BookingDetailPage.FirstName,
                                                   pageController.CurrentContext.BookingDetailPage.LastName);
            hgcEmailValue.InnerText = pageController.CurrentContext.BookingDetailPage.EmailAddress;
            hgcPhoneNumberValue.InnerText = string.Format(pageController.CurrentContext.BookingDetailPage.PhoneCountryCode.Length == 2 ? "+{0}{1}" : "00{0}{1}", pageController.CurrentContext.BookingDetailPage.
                                                                                                                                                                                PhoneCountryCode, pageController.CurrentContext.BookingDetailPage
                                                                                                                                                                                                                .MobileNumber);
            hgcMembershipNumberValue.InnerText = pageController.CurrentContext.BookingDetailPage.MembershipId;
            hgcMembershipContainer.Visible = isAuthenticated;
            lnkFeedback.NavigateUrl = string.Format("{0}?pageId={1}", pageController.GetPageUrl(MobilePages.Feedback),
                                                    Reference.FEEDBACK);

            hgcEmailConfirmation.InnerHtml = pageController.CurrentContext.BookingDetailPage.EmailAddress;
            if (pageController.CurrentContext.BookingDetailPage.ReceiveConfirmationSMS)
            {
                smslabel.Visible = true;
                hgcSMSConfirmation.Visible = true;
                hgcSMSConfirmation.InnerHtml = string.Format(pageController.CurrentContext.BookingDetailPage.PhoneCountryCode.Length == 2 ? "+{0}{1}" : "00{0}{1}", pageController.CurrentContext.BookingDetailPage.PhoneCountryCode, pageController.CurrentContext.BookingDetailPage.MobileNumber);
            }
        }

        /// <summary>
        /// PageId
        /// </summary>
        /// <returns>MobilePages</returns>
        public override MobilePages PageId()
        {
            return MobilePages.BookingConfirmation;
        }

        /// <summary>
        /// Gets save credit card overlay html.
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string GetSaveCreditCardOverlay()
        {
            var controller = new ConfirmationController();
            try
            {
                return controller.GetSaveCreditCardOverlay();
            }
            catch (Exception ex)
            {
                ActionItem actionItem = null;
                actionItem = Utilities.GetActionItemToTrack(controller.CurrentContext);
                UserNavTracker.TrackAction(actionItem, true);
                UserNavTracker.LogAndClearTrackedData(ex != null ? ex : new Exception("Generic-Error Mobile Confirmation GetSaveCreditCardOverlay method"));
                throw ex;
            }
        }

        /// <summary>
        /// Saves credit card information to usr profile.
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string SaveCreditCard()
        {
            var controller = new ConfirmationController();
            try
            {
                return controller.SaveCreditCard();
            }
            catch (Exception ex)
            {
                ActionItem actionItem = null;
                actionItem = Utilities.GetActionItemToTrack(controller.CurrentContext);
                UserNavTracker.TrackAction(actionItem, true);
                UserNavTracker.LogAndClearTrackedData(ex != null ? ex : new Exception("Generic-Error Mobile Confirmation SaveCreditCard method"));
                throw ex;
            }
        }
    }
}