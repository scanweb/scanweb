﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="OffersList.aspx.cs" Inherits="Scandic.Scanweb.Mobile.Templates.Booking.OffersList"
    MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" %>

<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<asp:Content ID="AllOffersPageContent" ContentPlaceHolderID="cphMain" runat="server">
    <div class="category-filter" onclick="SetFilterState();" id="categoryContainer" runat="server">
		<span class="category-filter-icon"></span>
        <episerver:translate text="/Templates/Scanweb/Pages/OfferList/FilterText" runat="server" />
    </div>
    <div class="category-container">
        <h2 class="offerFilterBtn offerFilterActiveBtn" data-id="DummyContainer" id="showAllOffer"
            runat="server">
        </h2>
        <asp:Repeater ID="OfferCategory" runat="server">
            <ItemTemplate>
                <h2 class="offerFilterBtn" id="offerButton" runat="server">
                </h2>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div class="category-results" id="resultText" runat="server">
        <episerver:translate text="/Templates/Scanweb/Pages/OfferList/ResultText" runat="server" />
        <span id="categoryTextcontainer"></span>
    </div>
    <asp:PlaceHolder ID="OfferCategoryPlaceHolder" runat="server"></asp:PlaceHolder>
    <div id="noOfferAvailable" runat="server" visible="false" class="nooffers-message"></div>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphScript" runat="server" ID="pageScripts">

    <script type="text/javascript" src="<%= ResolveUrl("~/ScanwebMobile/Public/Scripts/OffersList.min.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>

</asp:Content>
<asp:Content ContentPlaceHolderID="cphOutsideForm" runat="server" ID="cphSelectedOffer">

    <form id="selectedOffer" method="post">
        <input type="hidden" name="bannerInfo"  id="bannerInfo" />
        <input type="hidden" name="selectedFillters" id="selectedFillters" />
        <input type="hidden" name="fillterState" id="fillterState" />        
    </form>
</asp:Content>