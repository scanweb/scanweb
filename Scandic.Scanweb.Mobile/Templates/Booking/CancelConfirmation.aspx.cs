﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;

namespace Scandic.Scanweb.Mobile.Templates.Booking
{
    [AllowPublicAccess(true)]
    public partial class CancelConfirmation : VisualBasePage<CancelConfirmationPageSection>
    {
        private const string RESERVATION_HEADING_ON_ERROR = "onErrorReservationHeading";
        protected string reservationNumber;
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/Booking/CancelConfirmation.aspx";
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadControls();
            }
        }

        private void LoadControls()
        {
            var errorCount = Request.Form[Reference.ERROR_COUNT];
            var cancellationMessage = Request.Form[Reference.MESSAGE];
            reservationNumber = Request.Form[Reference.RESERVATION_NUMBER];
            var controller = new BaseController("");
            var pageConfig = controller.GetPageConfig<CancelConfirmationPageSection>();
            var userRepositoy = DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
            var backPageUrl = userRepositoy.IsUserAuthenticated ? controller.GetPageUrl(MobilePages.MyBookings) : controller.GetPageUrl(MobilePages.Start);
            var backPageId = userRepositoy.IsUserAuthenticated ? "backToMyBookingsPageText" : "backToStartPageText";
            var backPageText = pageConfig.PageDetail.PageMessages.GetMessage(backPageId);
            int errorValue = 0;
            int.TryParse(errorCount, out errorValue);
            if (errorValue > 0)
            {
                Master.PageHeading = pageConfig.PageDetail.PageMessages.GetMessage(RESERVATION_HEADING_ON_ERROR);
            }
            hgcCancellationMessage.InnerHtml = Server.HtmlDecode(cancellationMessage);
            lnkBackPage.Text = backPageText;
            lnkBackPage.NavigateUrl = backPageUrl;
            this.Master.PageId = PageId();
        }
    }
}
