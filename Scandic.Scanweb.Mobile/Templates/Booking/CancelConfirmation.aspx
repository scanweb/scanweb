﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="CancelConfirmation.aspx.cs"
    Inherits="Scandic.Scanweb.Mobile.Templates.Booking.CancelConfirmation" MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" %>

<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<asp:Content ContentPlaceHolderID="cphMain" ID="cancelConfirmationMain" runat="server">
    <div id="hgcCancellationMessage" runat="server">
    </div>
    <asp:HyperLink ID="lnkBackPage" runat="server" CssClass="button green"> </asp:HyperLink>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphScript" ID="cancelConfirmationScript" runat="server">

    <script type="text/javascript">
        var reservationNumber = "<%=reservationNumber %>";
        $().ready(function() {
            getPageTrackingDataWithParameter(15, reservationNumber);
        });
    </script>

</asp:Content>
