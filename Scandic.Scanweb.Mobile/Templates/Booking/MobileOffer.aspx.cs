﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Services;
using System.Web.UI;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;

namespace Scandic.Scanweb.Mobile.Templates.Booking
{
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionExpired(true)]
    public partial class MobileOffer : VisualBasePage<MobileOfferPageSection>
    {
        #region Variables
        private OfferDetailController controller;
        private const string OFFER_DETAIL_HEADER = "offerDetailHeader";
        private const string MOBILE_OFFER_MAIN_BODY = "MobileOfferMainBody";
        private const string MAIN_BODY = "MainBody";
        private string offerPageID = string.Empty;
        private int imageWidth;
        protected string offerName = string.Empty;
        protected string offerHeading = string.Empty;
        private string searchPageUrl = string.Empty;
        private PageData pagedata;
        protected string trackingInformation = string.Empty;
        protected string selectRateOfferTrackingData = string.Empty;
        #endregion

        #region Protected Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["pageid"]))
            {
                offerPageID = Convert.ToString(Request.QueryString["pageid"]);
                pagedata = controller.GetMobileOfferDetails(Convert.ToInt32(offerPageID));
                SetOfferTrackingInfo();
            }
            imageWidth = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.MOBILE_CAROUSEL_IMAGE_WIDTH]);

            searchPageUrl = GetSearchPageURL();
            bookButton.Attributes.Add("href", searchPageUrl);

            if (!IsPostBack)
            {
                LoadControls();
            }
            SetCanonicalURL();
            this.Master.PageId = PageId();
        }

        private void SetOfferTrackingInfo()
        {
            string offerTitle = string.Empty;
            if (pagedata != null)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(pagedata["MobileOfferHeading"])))
                    offerTitle = Convert.ToString(pagedata["MobileOfferHeading"]);
                else
                    offerTitle = Convert.ToString(pagedata["Heading"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["SN"]))
            {
                trackingInformation = string.Format("{0}*{1}", Convert.ToString(Request.QueryString["SN"]), offerTitle);
            }
            if (!string.IsNullOrEmpty(Request.Form["bannerInfo"]) && !string.IsNullOrEmpty(Request.Form["selectedFillters"]))
            {
                trackingInformation = string.Format("{0}*{1}*{2}*{3}", Convert.ToString(Request.Form["bannerInfo"]),
                    offerTitle, Convert.ToString(Request.Form["selectedFillters"]), Convert.ToString(Request.Form["fillterState"]));
            }
            if (!string.IsNullOrEmpty(Request.Form["pageName"]))
            {
                selectRateOfferTrackingData = offerTitle;
            }
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            controller = new OfferDetailController();
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/Booking/MobileOffer.aspx";
        }

        #endregion

        #region Private Methods

        private string GetSearchPageURL()
        {
            string url = controller.GetPageUrl(MobilePages.Start);
            if (pagedata != null)
            {
                string destinationID = string.Empty;
                string destination = Convert.ToString(pagedata["PromotionDestination"]);
                string promoCode = Convert.ToString(pagedata["PromotionCode"]);
                //IF destination text is defined in CMS then only chk for search type and send the values in query string on search hotel to prepopulate 
                //the search hotel screen otherwise just redirect to search hotel and show default values
                SearchHotelController searchHotelController = new SearchHotelController();
                SearchByType searchBy = SearchByType.Hotel;
                if (!string.IsNullOrEmpty(destination))
                    searchBy = searchHotelController.GetSearchByType(destination, out destinationID);

                if (searchBy == SearchByType.City)
                    url = string.Format("{0}{1}{2}{3}{4}{5}", url, "?CO=", destinationID, "&SC=", promoCode, "&FS=2").Trim();
                else               
                    url = string.Format("{0}{1}{2}{3}{4}{5}", url, "?HO=", destinationID, "&SC=", promoCode, "&FS=2").Trim();
                
            }
            return url;
        }

        private void SetCanonicalURL()
        {
            if (pagedata != null)
            {
                PlaceHolder linkContentPlaceHolder = (PlaceHolder)this.Master.FindControl("plhLinkCanonical");
                if (linkContentPlaceHolder != null)
                {
                    HtmlLink link = new HtmlLink();
                    link.Attributes.Add("rel", "canonical");
                    link.Href = controller.GetCanonicalURL(pagedata);
                    linkContentPlaceHolder.Controls.Add(link);
                }
            }
        }

        private void SetOfferExpiryText(PageData pageData)
        {
            if (pageData[Reference.OFFER_END_TIME] != null && Convert.ToDateTime(pageData[Reference.OFFER_END_TIME]) < DateTime.Now)
            {
                PageData offerpageData = controller.GetMobileOfferListPage("OfferContainer");
                OfferExpiryText.InnerHtml = Convert.ToString(offerpageData["OfferExpiryText"]);
                OfferExpiryDiv.Visible = true;
                bookButton.Visible = false;
            }
        }

        private void LoadControls()
        {
            if (pagedata != null)
            {
                SetOfferDetails(pagedata);
                SetOfferExpiryText(pagedata);
            }
        }

        private void SetOfferDetails(PageData pageData)
        {
            offerName = Convert.ToString(pageData["Name"]);
            if (!string.IsNullOrEmpty(Convert.ToString(pageData["MobileOfferHeading"])))
                offerHeading = Convert.ToString(pageData["MobileOfferHeading"]);
            else
                offerHeading = Convert.ToString(pageData["Heading"]);

            //Load Image Carousel if Image Carousel flag is selected
            if (pageData["ActivateImageCarousel"] != null && Convert.ToBoolean(pageData["ActivateImageCarousel"]))
            {
                offerDetailImageContainer.Visible = false;
                Control cntrl = new Control();
                carouselPlaceHolder.Controls.Add(cntrl = LoadControl("/ScanwebMobile/Templates/Controls/ImageCarousel.ascx"));
            }
            else //Else load Image 
            {
                offerDetailImageContainer.Visible = true;
                if (pageData["ContentTopImage"] != null)
                    offerDetailImage.Src = WebUtil.GetImageVaultImageUrl(Convert.ToString(pageData["ContentTopImage"]), imageWidth);
                offerDetailImage.Alt = Convert.ToString(pageData[Reference.BOX_HEADING]);
            }

            if (pageData[MOBILE_OFFER_MAIN_BODY] != null)
                mobileOfferMainBody.InnerHtml = Convert.ToString(pageData[MOBILE_OFFER_MAIN_BODY]);
            else
                mobileOfferMainBody.InnerHtml = Convert.ToString(pageData[MAIN_BODY]);
        }

        #endregion

        #region Public Mehtod
        /// <summary>
        /// Gets PageId
        /// </summary>
        /// <returns></returns>
        public override MobilePages PageId()
        {
            return MobilePages.OfferDetail;
        }
        #endregion

        #region WebMethod

        [WebMethod]
        public static List<ImageCarousel> GetOfferCarouselData(int offerPageID)
        {
            try
            {
                var offerController = new OfferDetailController();
                return offerController.LoadOfferCarouselData(offerPageID);
            }
            catch (Exception ex)
            {
                ActionItem actionItem = null;
                actionItem = Utilities.GetActionItemForOffersPage(offerPageID);
                UserNavTracker.TrackAction(actionItem, true);
                UserNavTracker.LogAndClearTrackedData(ex != null ? ex : new Exception("Generic-Error Mobile Offers GetOfferCarouselData method"));
                throw ex;
            }
        }
        #endregion
    }
}
