﻿//  Description					: SearchHotel                                             //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Services;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI;
using Scandic.Scanweb.Core;
using System.Globalization;
using System.Linq;
using Scandic.Scanweb.BookingEngine.Web;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.Web;

namespace Scandic.Scanweb.Mobile.Templates
{
    /// <summary>
    /// Code behind of SearchHotel page.
    /// </summary>
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionInValid(false,false)]
    public partial class SearchHotel : VisualBasePage<SearchHotelPageSection>
    {
        private const string REWARD_NIGHTS_PAGE_HEADING = "pageHeadingClaimRewardNights";
        private const string REWARD_NIGHTS_BOOKING_WITH_CHILDREN = "rewardNightsBookingWithChilderen";
        private const string NEAR_MY_POSITION_STRING = "nearMyPosition";
        private const string SHARE_LOCATION_STRING = "shareYourLocation";
        private const string LOCATION_UNAVAILABLE_STRING = "locationUnavailable";
        private const string LOCATION_TIMEOUT_STRING = "locationTimeOut";
        private const string LOCATION_UNKNOWN_ERROR_STRING = "locationUnknownError";
        private const string TODAY_STRING = "today";
        private const string TOMORROW_STRING = "tomorrow";
        private SearchHotelController pageController;
        protected string doNotSetDates = string.Empty;
        protected string monthsList = string.Empty;
        protected string weekDaysList = string.Empty;
        private const string BEDTYPE_CRIB = "bedtypeCrib";
        private const string BEDTYPE_EXTRA_BED = "bedtype_Extra_Bed";
        private const string BEDTYPE_IN_ADULT_BED = "bedtype_In_Adult_Bed";
        protected string nearMyPosition = string.Empty;
        protected string shareLocationMeassage = string.Empty;
        protected string todayString = string.Empty;
        protected string tomorrowString = string.Empty;
        protected string locationUnavailableString = string.Empty;
        protected string locationTimeOutString = string.Empty;
        protected string locationUnknownErrorString = string.Empty;
        private const string ERROR_INDEX = "Error";
        private const string AGE_ERROR_CONTROL = "hgcAgeError";
        private const string AGE_DROPDOWN_PREFIX = "childAgeforRoom1Child";
        private const string BED_TYPE_PREFIX = "bedTypeforRoom1Child";
        private const string INADULTBEDEXCEEDING_ERR_MSG_KEY = "hgcinadultbedexceedingError";
        protected string offerTrackingData;
        protected bool EnableCustomSearch;
        private const string INVALID_SOURCE_FOR_FAMILY_FRIENDS_DNUMBER = "InvalidSourceForFamilyAndFriendsDNumberErrorMsg";
        private const string VALIDATION_SUMMARY_ERR_CRTL = "hgcValidationSummaryheadingError";
        private const string VALIDATION_SUMMARY_ERR_MSG_KEY = "validationSummaryMsg";
        private const string BOOKING_CODE_ERR_CTRL = "hgcBookingCodeError";
        /// <summary>
        /// Oninit event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            string deeplinkPagePath = string.Empty;
            var deeplinkRepository =
                DependencyResolver.Instance.GetService(typeof(IDeeplinkRepository)) as IDeeplinkRepository;
            var bookingRepository =
                DependencyResolver.Instance.GetService(typeof(IBookingRepository)) as IBookingRepository;

            pageController = new SearchHotelController();
            if (bookingRepository.CurrentContext == null)
            {
                //Check if the Deeplink request is done directly to SearchHotel page, if yes then following call will
                //set all the context information for deeplink flow
                deeplinkRepository.IsDeeplinkRequest(Request, out deeplinkPagePath);
            }
            deeplinkPagePath = string.Empty;

            this.Load += new EventHandler(Page_Load);
            string parameter = Request["__EVENTARGUMENT"];
            nearMyPosition = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(NEAR_MY_POSITION_STRING);
            shareLocationMeassage = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(SHARE_LOCATION_STRING);
            todayString = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(TODAY_STRING);
            tomorrowString = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(TOMORROW_STRING);
            locationUnavailableString = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(LOCATION_UNAVAILABLE_STRING);
            locationTimeOutString = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(LOCATION_TIMEOUT_STRING);
            locationUnknownErrorString = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(LOCATION_UNKNOWN_ERROR_STRING);
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/Booking/SearchHotel.aspx";
            this.recentSearchItemRepeater.ItemDataBound += new RepeaterItemEventHandler(Repeater_ItemDataBound);
        }


        protected override void Render(HtmlTextWriter writer)
        {
            //register the items from the dropdownlist as possible postbackvalues for the listbox
            string crib = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(
                            BEDTYPE_CRIB);
            string extrabBed = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(
                            BEDTYPE_EXTRA_BED);
            string adultBed = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(
                            BEDTYPE_IN_ADULT_BED);
            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child1.UniqueID, crib);
            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child1.UniqueID, adultBed);
            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child1.UniqueID, extrabBed);

            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child2.UniqueID, crib);
            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child2.UniqueID, adultBed);
            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child2.UniqueID, extrabBed);

            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child3.UniqueID, crib);
            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child3.UniqueID, adultBed);
            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child3.UniqueID, extrabBed);

            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child4.UniqueID, crib);
            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child4.UniqueID, adultBed);
            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child4.UniqueID, extrabBed);

            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child5.UniqueID, crib);
            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child5.UniqueID, adultBed);
            Page.ClientScript.RegisterForEventValidation(bedTypeforRoom1Child5.UniqueID, extrabBed);
            base.Render(writer);
        }

        /// <summary>
        /// Onload event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadControls();
                if (Request != null && string.Equals(Request.QueryString["ErrCode"], "D1", StringComparison.InvariantCultureIgnoreCase))
                {
                    var bookingDetailspageController = new BookingDetailsController();
                    string errMsg = pageController.GetMessageFromXml(INVALID_SOURCE_FOR_FAMILY_FRIENDS_DNUMBER);
                    string errMsgValidationSummary = bookingDetailspageController.GetMessageFromXml(VALIDATION_SUMMARY_ERR_MSG_KEY);
                    List<KeyValueOption> listKeyValOpt = new List<KeyValueOption>();

                    listKeyValOpt.Add(new KeyValueOption { Key = BOOKING_CODE_ERR_CTRL, Value = errMsg });
                    listKeyValOpt.Add(new KeyValueOption { Key = VALIDATION_SUMMARY_ERR_CRTL, Value = errMsgValidationSummary });

                    RenderErrorMessages(this.Master, listKeyValOpt);
                }
            }
            if (Request["__EVENTARGUMENT"] == "OnClick")
            {
                lbtnSearch_Click(null, null);
            }
        }

        private void SetCookie(string cookieName, string cookieValue)
        {
            HttpCookie cookie = new HttpCookie(cookieName, cookieValue);
            cookie.Expires = new DateTime(2999, 12, 31);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        private string GetSearchTextByDestinationID(string destinationId)
        {
            string destination = string.Empty;
            var searchedCity = pageController.GetSearchDestinationList()
                .Where(city => string.Equals(city.id, HttpUtility.UrlDecode(destinationId), StringComparison.InvariantCultureIgnoreCase)).
                FirstOrDefault();

            if (searchedCity != null)
            {
                destination = searchedCity.name;
            }
            else
            {
                searchedCity = pageController.GetSearchDestinationList()
                    .Where(city => city.hotels.Where(hotel => string.Equals(hotel.id, HttpUtility.UrlDecode(destinationId),
                          StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault() != null).FirstOrDefault();
                if (searchedCity != null)
                    destination = searchedCity.hotels
                        .Where(hotel => string.Equals(hotel.id, HttpUtility.UrlDecode(destinationId), StringComparison.InvariantCultureIgnoreCase))
                        .Select(hotel => hotel.name).FirstOrDefault();
            }

            return destination;
        }

        private void GetRecentSearchHotelFromCookies()
        {
            if (Request.Cookies[AppConstants.RECENT_HOTEL_SEARCH_COOKIE_NAME] != null
                && !string.IsNullOrEmpty(Request.Cookies[AppConstants.RECENT_HOTEL_SEARCH_COOKIE_NAME].Value))
            {
                IList<string> recentHotels = Request.Cookies[AppConstants.RECENT_HOTEL_SEARCH_COOKIE_NAME].Value.Split(',').ToList();
                if (recentHotels != null && recentHotels.Count > 0)
                {
                    divRecentSearches.Visible = true;
                    this.recentSearchItemRepeater.DataSource = recentHotels;
                    this.recentSearchItemRepeater.DataBind();
                }
            }
        }

        /// <summary>
        /// Repeater_ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var recentSearchItem = e.Item.FindControl("recentSearch") as HtmlGenericControl;
                recentSearchItem.InnerHtml = GetSearchTextByDestinationID(e.Item.DataItem.ToString());
                recentSearchItem.Attributes.Add("onclick", "javascript:setCookieNametoSearchBox('" + recentSearchItem.InnerHtml + "'); return false;");
            }
        }

        private void SetTrackingInfo()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["FS"]))
                if (string.Equals(Request.QueryString["FS"], "2"))
                    offerTrackingData = "book now";

        }

        /// <summary>
        /// Load controls.
        /// </summary>
        private void LoadControls()
        {
            EnableCustomSearch = Convert.ToBoolean(ConfigurationManager.AppSettings[AppConstants.ENABLE_MOBILE_CUSTOM_SEARCH]);
            SetTrackingInfo();
            var pageConfig = pageController.GetPageConfig<SearchHotelPageSection>();
            GetRecentSearchHotelFromCookies();
            var pageData = pageController.GetPageData();
            var endDateOfArrival = DateTime.Today.AddDays(pageConfig.AllowedDaysForDateOfArrival);
            var dateFormat = Reference.DateFormat;
            endDateOfArrival = endDateOfArrival.AddDays(-1);
            doNotSetDates = pageController.IsClaimRewardNights() ? "1" : "0";

            txtSearch.Text = pageData.SearchDestination;// nearMyPosition;

            txtDateOfArrival.Attributes.Add("data-enddateofarrival", endDateOfArrival.ToString(dateFormat));
            if (pageData.CheckInDate.HasValue)
            {
                txtDateOfArrival.Text = pageData.CheckInDate.Value.ToString(dateFormat);
                txtDateOfArrival.Attributes.Add("value", txtDateOfArrival.Text);
                hidDateOfArrival.Value = txtDateOfArrival.Text;
                doNotSetDates = "0";
            }

            txtDateOfDeparture.Attributes.Add("data-allowedbookingdays", pageConfig.AllowedBookingDays.ToString());
            if (pageData.CheckOutDate.HasValue)
            {
                txtDateOfDeparture.Text = pageData.CheckOutDate.Value.ToString(dateFormat);
                txtDateOfDeparture.Attributes.Add("value", txtDateOfDeparture.Text);
                hidDateOfDeparture.Value = txtDateOfDeparture.Text;
                doNotSetDates = "0";
            }

            if (pageData.NumberOfAdults.HasValue)
            {
                ddlNumberOfAdult.SelectedValue = pageData.NumberOfAdults.ToString();
            }
            else if (pageController.CurrentContext.CurrentBookingProcess == BookingProcess.ClaimRewardNights)
            {
                ddlNumberOfAdult.SelectedValue = "0";
            }
            if (pageData.NumberOfChildren.HasValue)
            {
                ddlChildPerRoom1.SelectedValue = pageData.NumberOfChildren.ToString();
                if (pageData.ChildrenInformation != null && pageData.ChildrenInformation.Count > 0)
                {
                    int count = 1;
                    foreach (var child in pageData.ChildrenInformation)
                    {
                        var childAge = FindControlRecursive(this.Master, string.Format("{0}{1}", AGE_DROPDOWN_PREFIX, count)) as DropDownList;
                        if (childAge != null && child.Age.HasValue)
                        {
                            childAge.SelectedValue = child.Age.Value.ToString();
                            var bedType = FindControlRecursive(this.Master, string.Format("{0}{1}", BED_TYPE_PREFIX, count)) as DropDownList;
                            if (bedType != null && !string.IsNullOrEmpty(child.BedType))
                            {
                                bedType.Attributes.Add("data-defaultValue", child.BedType);
                            }
                        }
                        count++;
                    }
                }
            }
            if (pageController.CurrentContext.CurrentBookingProcess != BookingProcess.ClaimRewardNights)
            {
                ddlOffer.SelectedValue = ((int)pageData.Offer).ToString();
                txtBookingCode.Text = pageData.BookingCode;
                chkRememberBookingCode.Checked = pageData.RememberBookingCode;
            }
            else
            {
                fsbookingCode.Visible = false;
                fsOffer.Visible = false;
                this.Master.PageHeading = pageConfig.PageDetail.PageMessages.GetMessage(REWARD_NIGHTS_PAGE_HEADING);
                Title = string.Format("Scandic Hotels - {0}", this.Master.PageHeading);
            }

            this.Master.PageId = PageId();
        }

        /// <summary>
        /// Search button click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbtnSearch_Click(object sender, EventArgs e)
        {

            var userInput = ProcessInput();
            bool redirectToCityListpage = false;
            List<KeyValueOption> validationErrors = new List<KeyValueOption>();
            if (userInput.IsSearchfromCurrentLocation == true)
            {
                PopulateUserInputWithCityList(ref userInput, ref redirectToCityListpage);
            }
            if (pageController.IsPageModelValid(userInput, out validationErrors))
            {
                if (redirectToCityListpage)
                {
                    pageController.PerformNearbySearch(userInput);
                }
                else
                {
                    pageController.PerformSearch(userInput);
                }
            }
            else
            {
                RenderErrorMessages(this.Master, validationErrors);
                SetDatesOnErrors(userInput.CheckInDate, userInput.CheckOutDate);
            }

        }

        /// <summary>
        /// Gets nearest one or more cities based on config value
        /// </summary>
        /// <param name="userInput"></param>
        /// <param name="allCityAndHotels"></param>
        /// <returns></returns>
        private List<SearchDestination> GetNearestCityFromCurrentPosition(ref SearchHotelModel userInput, List<SearchDestination> allCityAndHotels)
        {
            List<SearchDestination> minDistanceDestinitions = new List<SearchDestination>();
            List<SearchDestination> orderedCityDestinitions = new List<SearchDestination>();
            Point userCurrentLocation = null;
            string[] coordinates = { string.Empty };
            try
            {
                string searchRadiusOption1 =
                    Convert.ToString(ConfigurationManager.AppSettings[AppConstants.SearchRadiusOption1]);
                string searchRadiusOption2 =
                    Convert.ToString(ConfigurationManager.AppSettings[AppConstants.SearchRadiusOption2]);
                if (Convert.ToString(ConfigurationManager.AppSettings[AppConstants.CurrentGeoLocationCordinate]) ==
                    string.Empty)
                {
                    AppLogger.LogInfoMessage("fetching user location");
                    userCurrentLocation =
                        new Point(Convert.ToDouble(hUserCurrentLatitude.Value, CultureInfo.InvariantCulture),
                                  Convert.ToDouble(hUserCurrentLongitude.Value, CultureInfo.InvariantCulture));
                    AppLogger.LogInfoMessage(string.Format("{0} - {1},{2}", "Users current location is",
                                                           Convert.ToDouble(hUserCurrentLatitude.Value,
                                                                            CultureInfo.InvariantCulture),
                                                           Convert.ToDouble(hUserCurrentLongitude.Value,
                                                                            CultureInfo.InvariantCulture)));
                }
                else
                {
                    coordinates =
                        Convert.ToString(ConfigurationManager.AppSettings[AppConstants.CurrentGeoLocationCordinate])
                               .Split(Convert.ToChar(AppConstants.COMMA));
                    userCurrentLocation = new Point(Convert.ToDouble(coordinates[0], CultureInfo.InvariantCulture),
                                                    Convert.ToDouble(coordinates[1], CultureInfo.InvariantCulture));
                }

                foreach (SearchDestination dest in allCityAndHotels)
                {
                    if (dest.Coordinate != null)
                    {
                        double distance = WebUtil.CalculateCurrentLocationDistance(userCurrentLocation.X,
                                          userCurrentLocation.Y, dest.Coordinate.X, dest.Coordinate.Y);
                        dest.DistanceFromCurrentLocation = distance;
                    }
                    else
                    {
                        AppLogger.LogFatalException(new Exception(string.Format("Coordinates missing for city = {0} with ID = {1}", dest.name, dest.id)));
                        userInput.CurrentCoordinate = userCurrentLocation;
                        return minDistanceDestinitions;
                    }
                }
                orderedCityDestinitions = allCityAndHotels.OrderBy(dist => dist.DistanceFromCurrentLocation).ToList();
                if (searchRadiusOption1 != string.Empty)
                {
                    minDistanceDestinitions = GetCityListWithinDefinedRadius(orderedCityDestinitions,
                                                                             searchRadiusOption1);
                    if (minDistanceDestinitions.Count > 0)
                    {
                        userInput.IsSearchWithinDefinedRadius = true;
                    }
                    else if (searchRadiusOption2 != string.Empty)
                    {
                        minDistanceDestinitions = GetCityListWithinDefinedRadius(orderedCityDestinitions,
                                                                                 searchRadiusOption2);
                        userInput.IsSearchWithinDefinedRadius = minDistanceDestinitions.Count > 0 ? true : false;
                    }
                }
                userInput.CurrentCoordinate = userCurrentLocation;
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, ex.Message);
                userInput.IsMinDistanceDestinationsNull = true;
                minDistanceDestinitions = null;
            }
            return minDistanceDestinitions;
        }
        /// <summary>
        /// Gets City list within defined radius
        /// </summary>
        /// <param name="orderedCityDestinitions"></param>
        /// <param name="searchRadius"></param>
        /// <returns></returns>
        private List<SearchDestination> GetCityListWithinDefinedRadius(List<SearchDestination> orderedCityDestinitions, string searchRadius)
        {
            return orderedCityDestinitions.Where(minDist => minDist.DistanceFromCurrentLocation
                                            <= Convert.ToDouble(searchRadius, CultureInfo.InvariantCulture)).ToList();
        }

        /// <summary>
        /// Fills userInput object with one or more cities
        /// </summary>
        /// <param name="userInput"></param>
        /// <param name="redirectToCityListpage"></param>
        /// <param name="isMinDistanceDestinationsNull"></param>
        /// <returns></returns>
        private void PopulateUserInputWithCityList(ref SearchHotelModel userInput, ref bool redirectToCityListpage)
        {
            List<SearchDestination> allCityAndHotels = new List<SearchDestination>();
            List<SearchDestination> minDistanceDestinitions = new List<SearchDestination>();

            allCityAndHotels = pageController.GetSearchDestinationList();

            if (allCityAndHotels == null)
            {
                throw new ApplicationException(string.Format("Referral URL:{0} , Current URL:{1}, AllCityHotelInfomation not avaliable in session.",
                    Convert.ToString(Context.Request.UrlReferrer), Convert.ToString(Context.Request.Url)));
            }
            minDistanceDestinitions = GetNearestCityFromCurrentPosition(ref userInput, allCityAndHotels);
            redirectToCityListpage = minDistanceDestinitions != null && minDistanceDestinitions.Count > 1 ? true : false;
            if (redirectToCityListpage)
            {
                userInput.SearchNearbyDestinations = minDistanceDestinitions;
            }
            if (minDistanceDestinitions != null && minDistanceDestinitions.Count == 1)
            {
                userInput.SearchDestination = minDistanceDestinitions[0].name;
            }
        }

        /// <summary>
        /// In IOS5 user's entered date is not getting set after postback from viewstate.
        /// This method is to fix that issue.
        /// </summary>
        private void SetDatesOnErrors(DateTime? checkInDate, DateTime? checkOutDate)
        {
            if (pageController.IsIOS5())
            {


                var checkInDateValue = checkInDate.HasValue ? checkInDate.Value.ToString(Reference.DateFormat) : string.Empty;
                var checkOutDateValue = checkOutDate.HasValue ? checkOutDate.Value.ToString(Reference.DateFormat) : string.Empty;

                txtDateOfArrival.Text = checkInDateValue;
                txtDateOfArrival.Attributes.Add("value", checkInDateValue);

                txtDateOfDeparture.Text = checkOutDateValue;
                txtDateOfDeparture.Attributes.Add("value", checkOutDateValue);
            }
        }

        private SearchHotelModel ProcessInput()
        {
            SearchHotelModel userInput = new SearchHotelModel();
            DateTime convertedDate;
            ChildrenInfo childInfo = null;
            userInput.IsSearchWithinDefinedRadius = false;
            userInput.SearchDestination = txtSearch.Text;

            if (txtSearch.Text == nearMyPosition || txtSearch.Text == string.Empty)
            {
                userInput.IsSearchfromCurrentLocation = true;
                userInput.SearchDestinationType = nearMyPosition;
            }
            else
            {
                userInput.IsSearchfromCurrentLocation = false;
            }

            if (!String.IsNullOrEmpty(hidDateOfArrival.Value) &&
                DateTime.TryParse(hidDateOfArrival.Value, out convertedDate))
            {
                userInput.CheckInDate = convertedDate;
            }

            if (!String.IsNullOrEmpty(hidDateOfDeparture.Value) &&
                DateTime.TryParse(hidDateOfDeparture.Value, out convertedDate))
            {
                userInput.CheckOutDate = convertedDate;
            }

            var numberOfAdult = Request.Form[ddlNumberOfAdult.ClientID.Replace("_", "$")];

            if (!string.IsNullOrEmpty(numberOfAdult))
            {
                userInput.NumberOfAdults = Convert.ToInt32(numberOfAdult);
            }
            int numberOfChildren = Convert.ToInt32(ddlChildPerRoom1.SelectedItem.Text);
            userInput.NumberOfChildren = numberOfChildren;
            if (numberOfChildren > 0)
            {
                userInput.ChildrenInformation = new List<ChildrenInfo>();
                for (int i = 0; i < numberOfChildren; i++)
                {
                    childInfo = new ChildrenInfo();
                    childInfo.Index = i + 1;
                    DropDownList child = this.FindControlRecursive(this.Master, string.Concat(AGE_DROPDOWN_PREFIX, childInfo.Index)) as DropDownList;
                    if (!userInput.IsAgeInputInvalid && child.SelectedIndex == 0)
                    {
                        userInput.IsAgeInputInvalid = true;
                        //SetDatesOnErrors(userInput.CheckInDate, userInput.CheckOutDate);

                    }
                    if (child.SelectedIndex != 0)
                    {
                        //If Bedtype implementation change from language specific text to language neutral value in dropdown then please change corresponding deepling functionlity
                        childInfo.Age = Convert.ToInt32(child.SelectedItem.Text);

                        DropDownList bed = this.FindControlRecursive(this.Master, string.Concat(BED_TYPE_PREFIX, childInfo.Index)) as DropDownList;
                        string BedType = Request.Form[bed.UniqueID];
                        childInfo.BedType = BedType.Trim();
                        userInput.ChildrenInformation.Add(childInfo);

                    }
                }
            }

            if (pageController.CurrentContext.CurrentBookingProcess != BookingProcess.ClaimRewardNights)
            {
                var bookingOption = Request.Form[ddlOffer.ClientID.Replace("_", "$")];
                if (!string.IsNullOrEmpty(bookingOption))
                {
                    userInput.Offer = (BookingOffer)Convert.ToInt32(bookingOption);
                }
                if (userInput.Offer == BookingOffer.BookingCode)
                {
                    userInput.BookingCode = txtBookingCode.Text.Trim().ToUpper();
                    userInput.RememberBookingCode = chkRememberBookingCode.Checked;
                }
                else
                {
                    userInput.BookingCode = string.Empty;
                    userInput.RememberBookingCode = false;
                }
            }

            return userInput;
        }
        /// <summary>
        /// Finds a Control recursively. Note finds the first match and exists
        /// </summary>
        /// <param name="ContainerCtl"></param>
        /// <param name="IdToFind"></param>
        /// <returns></returns>
        public Control FindControlRecursive(Control Root, string Id)
        {
            if (Root.ID == Id)
                return Root;

            foreach (Control Ctl in Root.Controls)
            {
                Control FoundCtl = FindControlRecursive(Ctl, Id);
                if (FoundCtl != null)
                    return FoundCtl;
            }

            return null;
        }
        /// <summary>
        /// Gets PageId
        /// </summary>
        /// <returns></returns>
        public override MobilePages PageId()
        {
            return MobilePages.Search;
        }

        #region WebMethod
        /// <summary>
        /// Gets search destination list
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static IEnumerable GetSearchDestinationList()
        {
            var controller = new SearchHotelController();
            try
            {
                return controller.GetSearchDestinationListFromSession();
            }
            catch (Exception ex)
            {
                List<KeyValueParam> keyValue = new List<KeyValueParam>();
                keyValue.Add(new KeyValueParam("Page Name", "SearchHotel"));
                keyValue.Add(new KeyValueParam("Method Name", "SearchHotel:GetSearchDestinationList"));
                if (controller != null && controller.CurrentContext != null && controller.CurrentContext.SearchHotelPage != null)
                {
                    if (!string.IsNullOrEmpty(controller.CurrentContext.SearchHotelPage.SearchDestination))
                        keyValue.Add(new KeyValueParam("Search Destination", controller.CurrentContext.SearchHotelPage.SearchDestination));
                    if (!string.IsNullOrEmpty(controller.CurrentContext.SearchHotelPage.SearchDestinationId))
                        keyValue.Add(new KeyValueParam("Search DestinationID", controller.CurrentContext.SearchHotelPage.SearchDestinationId));
                    if (!string.IsNullOrEmpty(controller.CurrentContext.SearchHotelPage.BookingCode))
                        keyValue.Add(new KeyValueParam("Booking Code", controller.CurrentContext.SearchHotelPage.BookingCode));
                    if (controller.CurrentContext.SearchHotelPage.SearchType != null)
                        keyValue.Add(new KeyValueParam("Search Type", controller.CurrentContext.SearchHotelPage.SearchType.ToString()));
                }
                AddActionParameters(ex, keyValue, true);
                throw;
            }
        }

        private static void AddActionParameters(Exception ex, List<KeyValueParam> parameters, bool clearSession)
        {
            ActionItem action = new ActionItem();
            action.ActionDate = DateTime.Now;
            action.Parameters = new List<KeyValueParam>();
            action.Parameters.AddRange(parameters);
            UserNavTracker.TrackAction(action, clearSession);
            UserNavTracker.LogAndClearTrackedData(ex);
        }

        /// <summary>
        /// Renders error messages
        /// </summary>
        /// <param name="validationErrors"></param>
        protected void RenderErrorMessages(Control control, List<KeyValueOption> validationErrors)
        {
            bool isChildClassRemoved = false;
            var errorControls = control.Controls.All()
               .Where(ctrl =>
                      Type.Equals(typeof(HtmlGenericControl), ctrl.GetType()) &&
                      ctrl.ID.IndexOf(ERROR_INDEX) != -1);
            bool isAgeError = false;
            if (errorControls != null && errorControls.Count() > 0)
            {
                KeyValueOption ageErrorCtrl = validationErrors.Where(err => err.Key.Equals(AGE_ERROR_CONTROL)).FirstOrDefault();
                if (ageErrorCtrl != null)
                    isAgeError = true;
                KeyValueOption childError = validationErrors.Where(err => err.Key.Equals(INADULTBEDEXCEEDING_ERR_MSG_KEY)).FirstOrDefault();

                if (isAgeError || childError != null)
                {

                    fldChildError.Attributes.Add("style", "display:block;");
                    chldErrorListContainer.Attributes.Add("style", "display: block;");
                }
                else if (!isAgeError && childError == null)
                {
                    fldChildError.Attributes.Add("style", "display:none;");
                    chldErrorListContainer.Attributes.Add("style", "display: none;");
                }
                if (isAgeError && childError == null)
                {
                    chldErrorListContainer.Attributes.Add("class", chldErrorListContainer.Attributes["class"].ToString().Replace("chlderrorList", ""));
                    isChildClassRemoved = true;
                    //string ageError = ageErrorCtrl.Value;
                    //ageError= ageError.Remove(0,1);
                    //ageErrorCtrl.Value = ageError;
                }

                if (!isAgeError && childError != null)
                {
                    chldErrorListContainer.Attributes.Add("class", chldErrorListContainer.Attributes["class"].ToString().Replace("chlderrorList", ""));
                    isChildClassRemoved = true;
                    //string childErrorMsg = childError.Value;
                    //childErrorMsg = childErrorMsg.Remove(0,1);
                    //childError.Value = childErrorMsg;
                }
                if (!isChildClassRemoved)
                {
                    chldErrorListContainer.Attributes.Add("class", "chlderrorList");
                }
                foreach (var errorCtrl in errorControls)
                {
                    var valError = validationErrors.Where(error => string.Equals(errorCtrl.ID, error.Key)).FirstOrDefault();
                    var ctrl = errorCtrl as HtmlGenericControl;
                    if (valError != null)
                    {
                        ctrl.InnerText = valError.Value;
                        ctrl.Attributes.Add("style", "display: block;");

                    }
                    else
                    {
                        if (!((ctrl.ID.Equals(fldChildError.ID) || ctrl.ID.Equals(chldErrorListContainer.ID)) && (childError != null || isAgeError)))
                            ctrl.Attributes.Add("style", "display: none;");

                    }
                }
            }
            List<DropDownList> ageControls = control.Controls.All()
               .Where(ctrl =>
                      Type.Equals(typeof(DropDownList), ctrl.GetType()) &&
                      ctrl.ID.IndexOf(AGE_DROPDOWN_PREFIX) != -1).Select(x => x as DropDownList).ToList();

            if (isAgeError)
            {

                List<DropDownList> ageControlWithError = ageControls.ToList<DropDownList>().Where(ddl => ddl.SelectedIndex == 0).ToList<DropDownList>();
                ageControlWithError.ForEach(c => c.Attributes.Add("style", "background-color:#F7F39C;border:1px solid #f2cd37;"));
                List<DropDownList> ageControlWithoutError = ageControls.Except(ageControlWithError).ToList();
                ageControlWithoutError.ForEach(c => c.Attributes.Add("style", "background-color:White;"));
            }
            else
            {
                ageControls.ForEach(c => c.Attributes.Add("style", "background-color:White;"));

            }
        }
        #endregion

    }
}