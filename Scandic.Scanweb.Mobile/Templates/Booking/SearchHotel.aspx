<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="SearchHotel.aspx.cs" EnableEventValidation="false"
    Inherits="Scandic.Scanweb.Mobile.Templates.SearchHotel" MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master"
    ResponseEncoding="utf-8" %>

<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<%@ Register Assembly="Scandic.Scanweb.Mobile" Namespace="Scandic.Scanweb.Mobile.UI.Controls"
    TagPrefix="Mobile" %>
<asp:content id="startPageContent" contentplaceholderid="cphMain" runat="server">

 <div class="standard-form" id="validationSummary">
       <span class="error" id="hgcValidationSummaryheadingError" runat="server"></span>
       <span class="errorsummary" id="bulletErrorList"></span>
    </div>
    <br/>   

    <div class="standard-form">
        <fieldset class="relative">
       
            <asp:Label ID="lblSearch" runat="server" Text="Search" AssociatedControlID="txtSearch"></asp:Label>
            <span class="error" runat="server" id="hgcSearchError"></span>
            <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            
            
            <div class="autocomplete">
				<div class="top">
					<a class="close" href="#">x</a>
				</div>
				<ul>							
				</ul>
			</div>
			
			<div class="mrgTop5" id="divRecentSearches" runat="server" visible="false">
			<asp:Label ID="lblRecentSearches" runat="server"></asp:Label>
            <asp:Repeater ID="recentSearchItemRepeater" runat="server">
                <ItemTemplate>
                    <span class="lastSearchContainer" id="recentSearch" runat="server"></span>
                </ItemTemplate>
             </asp:Repeater>
            </div>
			
        </fieldset>
        
       
        <fieldset>
            <asp:Label ID="lblDateOfArrival" runat="server" Text="Date Of Arrival" AssociatedControlID="txtDateOfArrival" ></asp:Label>            
            <span class="error" runat="server" id="hgcDateOfArrivalError"></span>
            <Mobile:DateTextbox ID="txtDateOfArrival" runat="server"  CssClass="datepicker dateOfArrival" ></Mobile:DateTextbox>            
            <input type="hidden" id="hidDateOfArrival" runat="server" />
        </fieldset>
        
        <fieldset>
            <asp:Label ID="lblDateOfDeparture" runat="server" Text="Date Of Departure" AssociatedControlID="txtDateOfDeparture"></asp:Label>            
            <span class="error" runat="server" id="hgcDateOfDepartureError"></span>
            <Mobile:DateTextbox ID="txtDateOfDeparture" runat="server"  CssClass="datepicker dateOfDeparture" ></Mobile:DateTextbox>            
             <input type="hidden" id="hidDateOfDeparture" runat="server" />
        </fieldset>
         <fieldset class="chldError" runat="server" style="display: none;" id="fldChildError">
         <ul class="chlderrorList" id="chldErrorListContainer" runat="server">
                <li class="error" runat="server" id="hgcinadultbedexceedingError"></li>
           <li class="error" runat="server" id="hgcAgeError"></li> 
      </ul>
        </fieldset>
       <fieldset class="childAdultdropdown">
            <asp:Label ID="lblNumberOfAdult" runat="server" Text="Number Of Adult" AssociatedControlID="ddlNumberOfAdult"></asp:Label>
            <%--<a href="#" data-target="overlay-adults" class="icon tooltip" data-overlaytype="tooltip" data-associatedcontrol="lblNumberOfAdult" onclick="LoadOverlay(this); return false;">&nbsp;</a>--%>
            <span class="error" runat="server" id="hgcNumberOfAdultError"></span>            
            <asp:DropDownList ID="ddlNumberOfAdult" runat="server">
            </asp:DropDownList>
           
        </fieldset>
       

       <fieldset class="childDropdown margleft">
         <asp:Label id="lblChildrenTitle" runat="server" AssociatedControlID="ddlChildPerRoom1" CssClass="chldlable"></asp:Label>
         <a href="#"  class="icon tooltip" data-overlaytype="tooltip" data-associatedcontrol="lblChildrenTitle" onclick="LoadOverlay(this); return false;">&nbsp;</a>
                               <asp:DropDownList ID="ddlChildPerRoom1" TabIndex="8" runat="server" CssClass="child chafltRt"
                                    rel="choutput1">
                                </asp:DropDownList>
                                
         </fieldset>

         
	 
		 
		 
		 
		 
<div class="childSelct choutput1" id="idRoom1ChildLabel" style="display: none;">
                          
                                <div class="colm5 padTop" id="idRoom1Child1Ddl" style="display: none;">
								<fieldset class="childAdultdropdown">
                                    
                                       <%-- Child 1--%>
                                       <asp:Label  id="lblChild1"  runat="server" AssociatedControlID="childAgeforRoom1Child1">
                                   
                                </asp:Label>
                                  
                              
 <asp:DropDownList ID="childAgeforRoom1Child1" name="childAgeforRoom1Child1"   TabIndex="22" runat="server" CssClass="fltLft subchild"
                                            rel="subchild114">
                              </asp:DropDownList>
							  </fieldset>
							    <fieldset class="childDropdown margleft" style="display: none;">
                                    <div class="widthMr fltLft bedoutput11">
                                    
                                       <%-- Child 1--%>
                                       <asp:Label  id="lblSelectBedType1"  runat="server" AssociatedControlID="bedTypeforRoom1Child1">
                                   
                                </asp:Label>
                                 
                                        <%--<select class="selBedTyp subchild114" tabindex="23" id="bedTypeforRoom1Child1" name="bedTypeforRoom1Child1"><option value="In adult's bed">In adult's bed</option><option value="Crib">Crib</option></select>--%>
                                         <asp:DropDownList ID="bedTypeforRoom1Child1" name="bedTypeforRoom1Child1" TabIndex="23" runat="server" CssClass="selBedTyp subchild114">
                                        </asp:DropDownList>
                                    </div>
								</fieldset>
                                </div>
                                <div class="colm5" id="idRoom1Child2Ddl" style="display: none;">
								 <fieldset class="childAdultdropdown">
                                   
                                        <%--Child 2--%>
                                        <asp:Label  id="lblChild2" runat="server" AssociatedControlID="childAgeforRoom1Child2">
                                   
                                </asp:Label>
                                 
                                 
  <asp:DropDownList ID="childAgeforRoom1Child2" name="childAgeforRoom1Child2"  TabIndex="24" runat="server" CssClass="fltLft subchild"
                                            rel="subchild124">
                              </asp:DropDownList>
								</fieldset>
							     <fieldset class="childDropdown margleft" style="display: none;">
                                    <div class="widthMr fltLft bedoutput12">
                                     
                                       <%-- Child 1--%>
                                       <asp:Label  id="lblSelectBedType2"  runat="server" AssociatedControlID="bedTypeforRoom1Child2">
                                   
                                </asp:Label>
 <asp:DropDownList ID="bedTypeforRoom1Child2" name="bedTypeforRoom1Child2" TabIndex="25" runat="server" CssClass="selBedTyp subchild124">
                                        </asp:DropDownList>
                                    </div>
									</fieldset>
                                </div>
                                <div class="colm5" id="idRoom1Child3Ddl" style="display: none;">
								 <fieldset class="childAdultdropdown">
                                   
                                        <%--Child 3--%>
                                         <asp:Label  id="lblChild3" runat="server" AssociatedControlID="childAgeforRoom1Child3">
                                   
                                </asp:Label>
                                  
                                   <asp:DropDownList ID="childAgeforRoom1Child3" name="childAgeforRoom1Child3"  TabIndex="26" runat="server" CssClass="fltLft subchild"
                                            rel="subchild134">
                              </asp:DropDownList>
							  	</fieldset>
							     <fieldset class="childDropdown margleft" style="display: none;">
                                    <div class="widthMr fltLft bedoutput13">
                                     
                                       <%-- Child 1--%>
                                       <asp:Label  id="lblSelectBedType3"  runat="server" AssociatedControlID="bedTypeforRoom1Child3">
                                   
                                </asp:Label>
 <asp:DropDownList ID="bedTypeforRoom1Child3" name="bedTypeforRoom1Child3" TabIndex="27" runat="server" CssClass="selBedTyp subchild134">
                                        </asp:DropDownList>
                                    </div>
									</fieldset>
                                </div>
                                <div class="colm5" id="idRoom1Child4Ddl" style="display: none;">
								 <fieldset class="childAdultdropdown">
                                   
                                       <%-- Child 4--%>
                                        <asp:Label  id="lblChild4" runat="server" AssociatedControlID="childAgeforRoom1Child4">
                                   
                                </asp:Label>
                                   
                              
  <asp:DropDownList ID="childAgeforRoom1Child4" name="childAgeforRoom1Child4"  TabIndex="28" runat="server" CssClass="fltLft subchild"
                                            rel="subchild144">
                              </asp:DropDownList>
							  </fieldset>
							    <fieldset class="childDropdown margleft" style="display: none;">
                                    <div class="widthMr fltLft bedoutput14">
                                    
                                       <%-- Child 1--%>
                                       <asp:Label  id="lblSelectBedType4"  runat="server" AssociatedControlID="bedTypeforRoom1Child4">
                                   
                                </asp:Label>
<asp:DropDownList ID="bedTypeforRoom1Child4" name="bedTypeforRoom1Child4" TabIndex="29" runat="server" CssClass="selBedTyp subchild144">
                                        </asp:DropDownList>
                                    </div>
									</fieldset>
                                </div>
                                <div class="colm5" id="ctl00_MainRegion_MainContentRegion_BE_idRoom1Child5Ddl" style="display: none;">
								 <fieldset class="childAdultdropdown">
                                   
                                       <%-- Child 5--%>
                                       <asp:Label  id="lblChild5" runat="server" AssociatedControlID="childAgeforRoom1Child5">
                                   
                                </asp:Label>
                                   
                                    <asp:DropDownList ID="childAgeforRoom1Child5" name="childAgeforRoom1Child5"  TabIndex="30" runat="server" CssClass="fltLft subchild"
                                            rel="subchild154">
                              </asp:DropDownList>
                               </fieldset>
							   <fieldset class="childDropdown margleft" style="display: none;">
                                    <div class="widthMr fltLft bedoutput15">
                                      
                                       <%-- Child 1--%>
                                       <asp:Label  id="lblSelectBedType5"  runat="server" AssociatedControlID="bedTypeforRoom1Child5">
                                   
                                </asp:Label>
<asp:DropDownList ID="bedTypeforRoom1Child5" name="bedTypeforRoom1Child5" TabIndex="31" runat="server" CssClass="selBedTyp subchild154">
                                        </asp:DropDownList>
                                    </div>
									</fieldset>
                                </div>
                 
                        </div>
 <%--<input type="hidden" runat="server"  class="hiddenTxt" id="childrenCriteriaHidden" name="childrenCriteriaHidden">--%>
        <%-- <asp:HiddenField  id="childrenCriteriaHidden"  runat="server"></asp:HiddenField>--%>
      
        <asp:Label ID="lblchildrenCriteriaHidden" runat="server" class="hiddenTxt" style="display:none"></asp:Label>
        
        <fieldset id="fsOffer" runat="server">
            <asp:Label ID="lblOffer" runat="server" Text="Have a booking code or bonus cheques?" AssociatedControlID="ddlOffer"></asp:Label>
            <a href="#" data-target="overlay-bookingcode" class="icon tooltip" data-overlaytype="tooltip" data-associatedcontrol="lblOffer" onclick="LoadOverlay(this); return false;">&nbsp;</a>
            <asp:DropDownList ID="ddlOffer" TabIndex="32"  runat="server">
            </asp:DropDownList>
        </fieldset>
        <fieldset  class="hidden" id="fsbookingCode" runat="server">
            <asp:Label ID="lblBookingCode" runat="server" Text="Booking code" AssociatedControlID="txtBookingCode"></asp:Label>
            <span class="error" runat="server" id="hgcBookingCodeError"></span>
            <asp:TextBox ID="txtBookingCode" runat="server"></asp:TextBox>            
            <asp:CheckBox ID="chkRememberBookingCode" runat="server"  />
            <asp:Label ID="lblRememberBookingCode" runat="server" Text="Remember my booking code" AssociatedControlID="chkRememberBookingCode"></asp:Label>
        </fieldset>
        <p><a id="lbtnSearch" class="button blue hand submit mrgB10" runat="server" TabIndex="33"></a></p>
        <input type="hidden" id ="hUserCurrentLatitude" value="" runat="server"/>
        <input type="hidden" id ="hUserCurrentLongitude" value="" runat="server"/>
    </div>
</asp:content>
<asp:content contentplaceholderid="cphScript" runat="server" id="pageScripts">
 
 <script type="text/javascript" >
     var doNotSetDates = "<%=doNotSetDates%>";
     var nearMyPosition = "<%=nearMyPosition%>";
     var shareLocationMeassage = "<%=shareLocationMeassage%>";
     var todayString = "<%=todayString%>";
     var tomorrowString = "<%=tomorrowString%>";
     var locationUnavailableString = "<%=locationUnavailableString%>";
     var locationTimeOutString = "<%=locationTimeOutString%>";
     var locationUnknownErrorString = "<%=locationUnknownErrorString%>";
     var enableCustomSearch = "<%=EnableCustomSearch%>";
     
     function displayErrorSummary() {
         var spanVisibleErrorCollection = $(".error:visible");
         $('#bulletErrorList').html("");
         if (spanVisibleErrorCollection.length > 0) {
             for (var i = 1; i < spanVisibleErrorCollection.length; i++) {
                 var errorText = spanVisibleErrorCollection[i].innerHTML;
                 if (errorText != "") {
                     $('#bulletErrorList').append("- " + errorText + "<br>");
                 }
             }
         }
         else {
             $('#validationSummary').hide();
         }
     }
     var offerTrackingData = '<%=offerTrackingData %>';
     $(document).ready(function() {
         displayErrorSummary();
         $("input[type=text],input[type=email],input[type=number],select").bind("change", displayErrorSummary);
         if (offerTrackingData != '') {
             getPageTrackingDataWithParameter(1, offerTrackingData);
             //Do not fire 2nd tracking request from common track js file if search hotel page is loading on clicking book now button on offer page
             pageId = "";
         }
     });
     
     
</script>
 <script src="<%= ResolveUrl("~/ScanwebMobile/Public/Scripts/SearchHotel.min.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>

</asp:content>
