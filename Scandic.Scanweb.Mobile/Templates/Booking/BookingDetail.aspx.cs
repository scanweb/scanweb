﻿//  Description					:   BookingDetail                                         //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using SearchType = Scandic.Scanweb.Mobile.UI.Entity.Booking.Model.SearchType;
using System.Text;

namespace Scandic.Scanweb.Mobile.Templates.Booking
{
    /// <summary>
    /// This class all members that supports booking.
    /// </summary>
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionInValid(false,true)]
    public partial class BookingDetail : VisualBasePage<BookingDetailsPageSection>
    {
        private LoginController loginController;
        private BookingDetailsController pageController;
        private ContentDataAccessManager contentDataAccessManager;
        protected string readFromHistory;
        protected string saveInHistory;
        protected string guranteeType;
        private bool isPageRedirectionFromNetsPaymentWindow = false;

        protected string IsRewardNightsBooking = "false";
        private const string AGE_TEXT = "AgeText";
        private const string ADULT_TEXT = "AdultText";
        private const string CHILDREN_TEXT = "ChildrenText";
        private const string SPACE = " ";
        private const string COMMA = ",";
        private const string LEFT_PARENTHESES = "(";
        private const string RIGHT_PARENTHESES = ")";
        protected bool isPaymentFallbackEnabled = false;
        /// <summary>
        /// On init event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            this.lbtnSignIn.Click += new EventHandler(SignIn_Click);
            this.lbtnBook.Attributes.Add("onclick", "DisableBookButton();");
            this.lbtnBook.Click += new EventHandler(Book_Click);
            infomationRepeater.ItemDataBound += new RepeaterItemEventHandler(infomationRepeater_ItemDataBound);
            loginController = new LoginController();
            pageController = new BookingDetailsController();
            contentDataAccessManager = new ContentDataAccessManager();
            IsRewardNightsBooking = pageController.CurrentContext.CurrentBookingProcess == BookingProcess.ClaimRewardNights ? "true" : "false";
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/Booking/BookingDetail.aspx";
        }

        /// <summary>
        /// Repeater infomation item data bound event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void infomationRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var data = e.Item.DataItem as MessageDetails;
                var infoControl = e.Item.FindControl("hgcInfo") as HtmlGenericControl;

                infoControl.InnerText = data.MessageInfo;
                var css = infoControl.Attributes["class"];
                css = string.Format("{0} {1}", css, "error");
                infoControl.Attributes["class"] = css;
            }
        }

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            infomationRepeater.Visible = false;
            string queryStringTransactionId = string.Empty;
            string paymentResponseCode = string.Empty;
            if (Request.QueryString[QueryStringConstants.PAYMENT_TRANSACTIONID] != null)
            {
                queryStringTransactionId = Request.QueryString[QueryStringConstants.PAYMENT_TRANSACTIONID].ToString();
            }
            if (Request.QueryString[QueryStringConstants.PAYMENT_RESPONSECODE] != null)
            {
                paymentResponseCode = Request.QueryString[QueryStringConstants.PAYMENT_RESPONSECODE].ToString();
                isPageRedirectionFromNetsPaymentWindow = true;
            }

            isPaymentFallbackEnabled = contentDataAccessManager.GetPaymentFallback(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode);

            if (!IsPostBack)
            {
                LoadControls();
            }
            if (!IsPostBack && !string.IsNullOrEmpty(queryStringTransactionId) && !string.IsNullOrEmpty(paymentResponseCode) &&
                !contentDataAccessManager.GetPaymentFallback(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode))
            {
                ProcessOperaBookingCallsAfterNetsPaymentSuccessful(queryStringTransactionId, paymentResponseCode);
            }
            saveInHistory = !pageController.IsAuthenticated ? "1" : "0";
        }

        /// <summary>
        /// Signin click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SignIn_Click(object sender, EventArgs e)
        {
            pageController.CurrentContext.BookingDetailPage = ProcessInput();
            loginController.RedirectToSignIn(Page.Request);
        }

        /// <summary>
        /// Button book click handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Book_Click(object sender, EventArgs e)
        {
            var pageModel = ProcessInput();
            List<KeyValueOption> validationErrors = new List<KeyValueOption>();
            Reservation2SessionWrapper.NetsPaymentErrorMessage = string.Empty;
            Reservation2SessionWrapper.NetsPaymentInfoMessage = string.Empty;
            if (pageController.IsPageModelValid(pageModel, out validationErrors))
            {
                bool isModifyBookingSuccess = false;
                bool isMakePaymentSuccess = true;
                pageModel.MobileNumber = Regex.Replace(pageModel.MobileNumber, @"\D", "");
                var errors = pageController.BookHotel(pageModel, null, true, false, out isModifyBookingSuccess, out isMakePaymentSuccess);
                if (errors != null)
                {
                    var messagesToShow = errors as List<MessageDetails>;
                    if (messagesToShow != null)
                    {
                        infomationRepeater.DataSource = errors;
                        infomationRepeater.DataBind();
                        infomationRepeater.Visible = true;
                    }
                }
            }
            RenderErrorMessages(validationErrors);
        }

        private void LoadControls()
        {
            var pageConfig = pageController.GetPageConfig<BookingDetailsPageSection>();
            var acceptedConditionLabel = (pageConfig.PageDetail.Inputs
                .Where(
                    input =>
                    string.Equals(input.Id, "lblAcceptedCondition", StringComparison.InvariantCultureIgnoreCase))
                .Select(input => input.Label)
                                         ).FirstOrDefault();
            var termsConditionPath = string.Format("{0}?{1}={2}", pageController.GetPageUrl(MobilePages.TermsCondition),
                                                   Reference.PAGE_ID, Reference.TERMS_AND_CONDITIONS);
            var priceInformationPath = string.Format("{0}?{1}={2}",
                                                     pageController.GetPageUrl(MobilePages.PriceInformation),
                                                     Reference.PAGE_ID, Reference.PRICE_INFORMATION);
            var pageModel = pageController.GetPageData();
            if (isPageRedirectionFromNetsPaymentWindow)
            {
                pageModel = pageController.BookingInformation;
            }
            petRoomDescription.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage("petFriendlyRoom");
            allergyRoomDescription.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage("allergyFriendlyRoom");
            accessibleRoomDescription.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage("accessibleRoom");
            SetBookingInfo(pageModel);
            MiscellaneousSessionWrapper.PhoneCodeSessionkey = pageController.GetPhoneCodes();

            lbtnSignIn.Visible = !pageController.IsAuthenticated;
            lnkUpdateProfile.Visible = !lbtnSignIn.Visible;
            lnkUpdateProfile.NavigateUrl = pageModel.UpdateProfileUrl;
            readFromHistory = !pageController.IsAuthenticated ? "1" : "0";
            LoadGauranteedOptions();
            SetPersonalInformation(pageModel);
            hgcDnumberContainer.Visible = (pageController.CurrentContext.SearchHotelPage.SearchType ==
                                           SearchType.BonusCheque);
            txtDnumber.Text = pageModel.DNumber;
            //IContentDataAccessManager contentDataAccessManager = new ContentDataAccessManager();
            //Dictionary<string, RoomCategory> dictRoomCategory = contentDataAccessManager.GetRoomTypeCategoryMap();
            //List<RoomCategory> roomCategoryCollection = dictRoomCategory.Select(obj => obj.Value).ToList();
            List<RoomType> allRoomType = new List<RoomType>();
            //if (roomCategoryCollection != null && roomCategoryCollection.Count > 0)
            //{
            //    foreach (RoomCategory rCategory in roomCategoryCollection)
            //    {
            //        allRoomType = allRoomType.Union(rCategory.RoomTypes).ToList();
            //    }
            //}
            if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null &&
               HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count > 0)
            {
                var selectedRoomRate = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[0] as SelectedRoomAndRateEntity;

                if (selectedRoomRate != null && selectedRoomRate.RoomRates != null && selectedRoomRate.RoomRates.Count > 0)
                {
                    RoomCategory rCategory = RoomRateUtil.GetRoomCategory(selectedRoomRate.RoomRates[0].RoomTypeCode);
                    allRoomType = new List<RoomType>(rCategory.RoomTypes);
                }
            }
            //var masterCollection = allRoomType;
            //var obj1 = new RoomTypeBedTypeComparer();
            //allRoomType = allRoomType.Select(o => o).Distinct(obj1).ToList();
            LoadBedPreference(pageModel);
            List<KeyValueOption> activeItems = pageController.GetBedPreference();
            if (activeItems != null && activeItems.Count > 0)
            {
                foreach (KeyValueOption key in activeItems)
                {
                    if (allRoomType != null && allRoomType.Count > 0)
                    {
                        RoomType rType = allRoomType.Where(obj => obj.OperaRoomTypeId.Equals(key.Key)).FirstOrDefault();
                        if (allRoomType != null && allRoomType.Count > 0 && rType != null)
                        {
                            allRoomType.RemoveAll(obj => obj.BedTypeCode == rType.BedTypeCode);
                        }
                    }

                }
            }
            LoadOtherBedPrefence(allRoomType);
            SetOtherRequest(pageModel);

            SetCreditCardInformation(pageModel);
            if (!contentDataAccessManager.GetPaymentFallback(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode))
            {
                PopulateFGPSavedCreditCards();
            }

            lblAcceptedCondition.Text = string.Format(acceptedConditionLabel, termsConditionPath, priceInformationPath);
            chkSMSConfirmation.Checked = true;
            //chkSMSConfirmation.Checked = pageModel.ReceiveConfirmationSMS;
            //chkAcceptedCondition.Checked = pageModel.AcceptTermsCondition;

            this.Master.PageId = PageId();
        }

        private void LoadOtherBedPrefence(List<RoomType> allRoomType)
        {
            string notAvailableText = string.Format("{0}{1}{2}{3}", AppConstants.SPACE, AppConstants.HYPHEN, AppConstants.SPACE, WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/NotAvailable"));
            if (allRoomType != null && allRoomType.Count > 0)
            {
                foreach (RoomType roomType in allRoomType)
                {
                    ListItem item = new ListItem(roomType.BedTypeDescription + notAvailableText, roomType.OperaRoomTypeId);
                    ddlBedPreferences.Items.Add(item);
                    ddlBedPreferences.Items[ddlBedPreferences.Items.IndexOf(item)].Attributes.Add("disabled", "disabled");
                    ddlBedPreferences.Items[ddlBedPreferences.Items.IndexOf(item)].Attributes.Add("style", "font-style:italic");
                }
            }
        }

        private void LoadBedPreference(BookingDetailModel pageModel)
        {
            var selectedValue = string.IsNullOrEmpty(pageModel.BedPreferenceCode) ? "0" : pageModel.BedPreferenceCode;
            var dataSource = pageController.GetBedPreference();
            LoadDropDown(ddlBedPreferences, dataSource, selectedValue, true);
        }

        private void SetOtherRequest(BookingDetailModel pageModel)
        {
            chkAccessibleRoom.Checked = pageModel.AccessibleRoom;
            chkAllergyFriendlyRoom.Checked = pageModel.AllergyFriendlyRoom;
            chkPetFriendlyRoom.Checked = pageModel.PetFriendlyRoom;

            ddlRoomFloor.SelectedValue = pageModel.FloorPreferenceCode;
            ddlNearElevator.SelectedValue = pageModel.ElevatorPreferenceCode;
            ddlSmoking.SelectedValue = pageModel.SmokingPreferenceCode;
        }

        private void SetCreditCardInformation(BookingDetailModel pageModel)
        {
            txtCardHolderName.Text = pageModel.CardHolderName;
            txtCardNumber.Text = pageModel.CardNumber;
            LoadDropDown(ddlCardType, pageController.GetCreditCardTypes(), pageModel.CardTypeCode, false);
            LoadDropDown(ddlCardValidMonth, pageController.GetMonths(), pageModel.CardExpiryMonthCode, false);
            LoadDropDown(ddlCardValidYear, pageController.GetYears(), pageModel.CardExpiryYearCode, false);
        }

        private void SetPersonalInformation(BookingDetailModel pageModel)
        {
            var isEnabled = !pageController.IsAuthenticated;

            hgcMembershipIdValue.InnerText = pageModel.MembershipId;
            hgcMembershipItem.Visible = pageController.IsAuthenticated;
            txtFirstName.Text = pageModel.FirstName;
            txtFirstName.Enabled = !string.IsNullOrEmpty(pageModel.FirstName) ? isEnabled : true;
            txtLastName.Text = pageModel.LastName;
            txtLastName.Enabled = !string.IsNullOrEmpty(pageModel.LastName) ? isEnabled : true;
            ;
            txtEmail.Text = pageModel.EmailAddress;
            txtEmail.Enabled = !string.IsNullOrEmpty(pageModel.EmailAddress) ? isEnabled : true;
            ddlCountry.Enabled = !string.IsNullOrEmpty(pageModel.CountryCode) ? isEnabled : true;
            var selectedCountry = string.IsNullOrEmpty(pageModel.CountryCode)
                                      ? pageController.GetCountryCodeToSelect(pageController.Language)
                                      : pageModel.CountryCode;
            //var selectedCountryCode = string.IsNullOrEmpty(pageModel.PhoneCountryCode)
            //                              ? pageController.GetCountryPhoneCode(selectedCountry)
            //                              : pageModel.PhoneCountryCode;

            var selectedCountryCode = string.IsNullOrEmpty(pageModel.PhoneCountryCode)
                                          ? (selectedCountry)
                                          : pageModel.PhoneCountryCode;

            LoadDropDown(ddlCountry, pageController.GetCountryList(), selectedCountry, false);
            Utility.SetPhoneCodesDropDown(ddlCountryCode, selectedCountryCode);

            if (!string.IsNullOrEmpty(pageModel.MobileNumber))
            {
                if (isPageRedirectionFromNetsPaymentWindow)
                {
                    txtMobile.Text = pageModel.MobileNumber;
                    ddlCountryCode.SelectedValue = selectedCountryCode;
                }
                else
                {
                    txtMobile.Text = Utility.SetPhoneCountryCode(ddlCountryCode, pageModel.MobileNumber);
                }
            }
            ddlCountryCode.Enabled = txtMobile.Enabled = !string.IsNullOrEmpty(txtMobile.Text) ? isEnabled : true;
        }

        private void SetBookingInfo(BookingDetailModel pageModel)
        {
            var checkInDate = pageController.CurrentContext.SearchHotelPage.CheckInDate;
            var checkOutDate = pageController.CurrentContext.SearchHotelPage.CheckOutDate;
            var pageConfig = pageController.GetPageConfig<BookingDetailsPageSection>();
            string taxText = pageConfig.PageDetail.PageMessages.GetMessage(Reference.InclusiveAllTax);
            string ageText = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(AGE_TEXT);
            string adultText = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(ADULT_TEXT);
            string childrenText = pageController.GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(CHILDREN_TEXT);

            hgcHotelValue.InnerText = pageController.CurrentContext.SelectHotelPage.SelectedHotel;
            if (checkInDate.HasValue && checkOutDate.HasValue)
            {
                hgcArrivalDateValue.InnerText = checkInDate.Value.ToString(Reference.IOS5DateFormat);
                hgcDepartureDateValue.InnerText = checkOutDate.Value.ToString(Reference.IOS5DateFormat);
            }
            if (pageController.CurrentContext.SearchHotelPage.ChildrenInformation != null && pageController.CurrentContext.SearchHotelPage.ChildrenInformation.Count > 0)
            {
                StringBuilder sBuilder = new StringBuilder();
                sBuilder.Append(LEFT_PARENTHESES);
                sBuilder.Append(ageText);
                sBuilder.Append(SPACE);
                int count = 0;
                foreach (ChildrenInfo child in pageController.CurrentContext.SearchHotelPage.ChildrenInformation)
                {
                    if (count == 0)
                        sBuilder.Append(child.Age);
                    else if (count < pageController.CurrentContext.SearchHotelPage.ChildrenInformation.Count)
                    {
                        sBuilder.Append(COMMA);
                        sBuilder.Append(SPACE);
                        sBuilder.Append(child.Age);
                    }
                    count++;
                }
                sBuilder.Append(RIGHT_PARENTHESES);
                hgcAdultsValue.InnerText = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}", pageController.CurrentContext.SearchHotelPage.NumberOfAdults.Value.ToString(), SPACE, adultText, COMMA,
                 SPACE, pageController.CurrentContext.SearchHotelPage.ChildrenInformation.Count, SPACE, childrenText,
                 SPACE, sBuilder.ToString());
            }
            else
            {
                hgcAdultsValue.InnerText = string.Format("{0} {1} {2}", pageController.CurrentContext.SearchHotelPage.NumberOfAdults.Value.ToString(), SPACE, adultText);
            }
            hgcChildrenDetailValue.InnerText = pageController.CurrentContext.SearchHotelPage.ChildrenDetailText;
            hgcRoomTypeValue.InnerText = pageController.CurrentContext.SelectRatePage.SelectedRoomType;
            hgcRateTypeValue.InnerText = pageController.CurrentContext.SelectRatePage.SelectedRateType;
            hgcRateTypeText.InnerText = LanguageRedirectionHelper.GetCurrentLanguage() == pageController.CurrentContext.SelectRatePage.SelectedRateLanguage ?
                                            pageController.CurrentContext.SelectRatePage.SelectedRateHighlightText : string.Empty;
            hgcFirstNightPriceValue.InnerText = string.Format("{0} {1}",
                                                              pageController.CurrentContext.SelectRatePage.SelectedRate,
                                                              taxText);
            hgcFirstNightPriceRow.Visible = (pageController.CurrentContext.CurrentBookingProcess !=
                                             BookingProcess.ClaimRewardNights);
            if (checkInDate.HasValue && checkOutDate.HasValue)
            {
                var timeSpan = checkOutDate.Value.Subtract(checkInDate.Value);
                hgcNumOfNightsValue.InnerText = timeSpan.Days.ToString();
            }
            hgcTotalPriceValue.InnerText = string.Format("{0} {1}", pageModel.TotalRate, taxText);
            //do not remove this code

            //if (pageController.CurrentContext.SearchHotelPage.NumberOfChildren.Value == 0)
            //{
            //    liChildrenInfo.Style.Add("display", "none");
            //}
            //else
            //{
            //    liChildrenInfo.Style.Add("display", "block");
            //}
        }

        private void LoadGauranteedOptions()
        {
            var options = pageController.GetGuaranteeOptions();
            bool isPrepaidBooking = false;
            if ((options != null) && (options.Count > 0))
            {
                if (options.Any(option => (RoomRateUtil.IsPrepaidGuaranteeType(option.Key) &&
                    !contentDataAccessManager.GetPaymentFallback(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode))))
                {
                    var pageConfig = pageController.GetPageConfig<BookingDetailsPageSection>();
                    lbtnBook.Text =
                        pageConfig.PageDetail.PageMessages.GetMessage("continueToPaymentButtonText");
                    this.lbtnBook.Attributes.Add("onclick", "return performFunctionTracking(13,'');");
                    isPrepaidBooking = true;
                    pageController.IsPrepaidBooking = true;
                }
            }
            if ((!isPrepaidBooking) && (pageController.CurrentContext.CurrentBookingProcess != BookingProcess.ClaimRewardNights))
            {
                if (options.Count == 1)
                {
                    spnCancellationText.InnerHtml = options[0].Value;
                    guranteeType = options[0].Key;
                    hidGuranteeType.Value = options[0].Key;
                    spnCCHiddenText.Visible = false;
                    rdoDepositeInfo.Visible = false;
                }
                else if (options.Count == 2)
                {
                    spnCancellationText.InnerHtml = options[0].Value;
                    rdoDepositeInfo.Visible = true;
                    rdoDepositeInfo.Text = options[1].Value;
                    rdoDepositeInfo.Attributes.Add("value", options[1].Key);
                }
            }
            else
            {
                lnkRedemptionGuranteeToolTip.Visible = false;
                spnCancellationText.Visible = false;
                rdoDepositeInfo.Visible = false;
                if (options != null && options.Count > 0)
                {
                    hgcRedemptionGuranteeInfo.InnerHtml = options[0].Value;
                }
            }
        }

        private void LoadDropDown(DropDownList control, List<KeyValueOption> dataSource, string selectedValue, bool selectSingleOption)
        {
            control.DataSource = dataSource;
            control.DataValueField = Reference.KeyText;
            control.DataTextField = Reference.ValueText;

            control.DataBind();

            if (!string.IsNullOrEmpty(selectedValue) && (control.Items.FindByValue(selectedValue) != null))
            {
                control.SelectedValue = selectedValue;
            }
            if (selectSingleOption && dataSource.Count >= 2)
            {
                control.SelectedValue = dataSource[1].Key;
            }
        }

        private BookingDetailModel ProcessInput()
        {
            var pageModel = new BookingDetailModel();

            pageModel.TotalRate = hgcTotalPriceValue.InnerText;
            pageModel.MembershipId = hgcMembershipIdValue.InnerText;
            pageModel.FirstName = txtFirstName.Text.Trim();
            pageModel.LastName = txtLastName.Text.Trim();
            pageModel.EmailAddress = txtEmail.Text.Trim();
            pageModel.Country = ddlCountry.SelectedItem.Text;
            pageModel.CountryCode = ddlCountry.SelectedValue;
            pageModel.PhoneCountryCode = ddlCountryCode.SelectedValue;
            pageModel.MobileNumber = txtMobile.Text.Trim().TrimStart('0');
            if (!string.IsNullOrEmpty(txtDnumber.Text))
            {
                pageModel.DNumber = txtDnumber.Text.ToUpper();
            }
            pageModel.ReceiveConfirmationSMS = chkSMSConfirmation.Checked;
            pageModel.BedPreference = ddlBedPreferences.SelectedItem.Text;
            pageModel.BedPreferenceCode = ddlBedPreferences.SelectedValue;
            pageModel.FloorPreference = ddlRoomFloor.SelectedItem.Text;
            if (!string.Equals(ddlRoomFloor.SelectedValue, "0"))
            {
                pageModel.FloorPreferenceCode = ddlRoomFloor.SelectedValue;
            }
            pageModel.ElevatorPreference = ddlNearElevator.SelectedItem.Text;
            if (!string.Equals(ddlNearElevator.SelectedValue, "0"))
            {
                pageModel.ElevatorPreferenceCode = ddlNearElevator.SelectedValue;
            }
            pageModel.SmokingPreference = ddlSmoking.SelectedItem.Text;
            if (!string.Equals(ddlSmoking.SelectedValue, "0"))
            {
                pageModel.SmokingPreferenceCode = ddlSmoking.SelectedValue;
            }
            pageModel.AdditionalRequest = hgcAdditionalRequest.Value;
            pageModel.AccessibleRoom = chkAccessibleRoom.Checked;
            pageModel.AllergyFriendlyRoom = chkAllergyFriendlyRoom.Checked;
            pageModel.PetFriendlyRoom = chkPetFriendlyRoom.Checked;

            if (!isPaymentFallbackEnabled)
            {
                var options = pageController.GetGuaranteeOptions();
                if ((options != null) && (options.Count > 0))
                {
                    if (options.Any(option => (RoomRateUtil.IsPrepaidGuaranteeType(option.Key))))
                    {
                        pageModel.ReservationGuaranteeOption = AppConstants.GUARANTEETYPE_PREAPIDINPROGRESS;
                    }
                }
            }

            if (pageController.CurrentContext.CurrentBookingProcess != BookingProcess.ClaimRewardNights)
            {
                if (string.IsNullOrEmpty(pageModel.ReservationGuaranteeOption))
                {
                    if ((rdoDepositeInfo.Visible == false) && isPaymentFallbackEnabled)
                        pageModel.ReservationGuaranteeOption = Reference.GuranteedTextCodeNoCancellation;
                    else
                    {
                        if (rdoDepositeInfo.Visible)
                        {
                            if (rdoDepositeInfo.Checked)
                                pageModel.ReservationGuaranteeOption = Reference.GuranteedTextCodeAfter6PM;
                            else
                                pageModel.ReservationGuaranteeOption = Reference.GuranteedTextCodeBefore6PM;
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(hidGuranteeType.Value))
                            {
                                pageModel.ReservationGuaranteeOption = hidGuranteeType.Value;
                            }
                        }
                    }
                }
            }
            else
            {
                pageModel.ReservationGuaranteeOption = Reference.GuranteedTextCodeRewardNights;
            }
            pageModel.CardHolderName = txtCardHolderName.Text;
            pageModel.CardNumber = pageController.ProcesCreditCardNumber(txtCardNumber.Text);
            pageModel.CardType = ddlCardType.SelectedItem.Text;
            pageModel.CardTypeCode = ddlCardType.SelectedValue;
            pageModel.CardExpiryMonth = ddlCardValidMonth.SelectedItem.Text;
            pageModel.CardExpiryMonthCode = ddlCardValidMonth.SelectedValue;
            pageModel.CardExpiryYear = ddlCardValidYear.SelectedItem.Text;
            pageModel.CardExpiryYearCode = ddlCardValidYear.SelectedValue;
            pageModel.AcceptTermsCondition = chkAcceptedCondition.Checked;

            ConfigHelper configHelper = new ConfigHelper();
            var sourceCode = configHelper.GetSourceCode();
            pageModel.SourceCode = sourceCode;
            pageController.IsEarlyBookingUsingStoredPanHashCC = false;
            if (ddlFGPSavedCreditCards.SelectedIndex > 0)
            {
                pageModel.PanHash = ddlFGPSavedCreditCards.SelectedValue;
                pageController.IsEarlyBookingUsingStoredPanHashCC = true;
            }
            pageModel.IsFlexFGPSavedCreditPayment = ddlFlexFGPCreditCards.Items.Count > 0;
            if (ddlFlexFGPCreditCards.SelectedIndex > 0)
            {
                pageModel.PanHash = ddlFlexFGPCreditCards.SelectedValue;
            }
            return pageModel;
        }

        private void ProcessOperaBookingCallsAfterNetsPaymentSuccessful(string queryStringTransactionId, string paymentResponseCode)
        {
            string sessionTransactionId = pageController.PaymentTransactionId;
            if (string.IsNullOrEmpty(queryStringTransactionId) && Request.QueryString[QueryStringConstants.PAYMENT_TRANSACTIONID] != null)
            {
                queryStringTransactionId = Request.QueryString[QueryStringConstants.PAYMENT_TRANSACTIONID].ToString();
            }
            if (string.IsNullOrEmpty(paymentResponseCode) && Request.QueryString[QueryStringConstants.PAYMENT_RESPONSECODE] != null)
            {
                paymentResponseCode = Request.QueryString[QueryStringConstants.PAYMENT_RESPONSECODE].ToString();
            }
            if (!string.IsNullOrEmpty(sessionTransactionId) && sessionTransactionId.Equals(queryStringTransactionId, StringComparison.InvariantCultureIgnoreCase)
               && !string.IsNullOrEmpty(paymentResponseCode) && (paymentResponseCode.Equals(SessionBookingConstants.NETS_PAYMENT_RESPONSECODE, StringComparison.InvariantCultureIgnoreCase)))
            {
                BookingDetailModel pageModel = pageController.BookingInformation;
                string authStatus = pageController.NetsAuthProcess(sessionTransactionId);
                if (authStatus.Equals(SessionBookingConstants.NETS_PAYMENT_RESPONSECODE, StringComparison.InvariantCultureIgnoreCase))
                {
                    bool isModifyBookingSuccess = false;
                    bool isMakePaymentSuccess = true;
                    try
                    {
                        PaymentInfo paymentInfo = pageController.GetPaymentInfo(sessionTransactionId);
                        pageController.ConfirmBooking();
                        paymentInfo.CaptureSuccess = pageController.NetsCaptureProcess(sessionTransactionId);
                        var errors = pageController.BookHotel(pageModel, paymentInfo, true, true,
                                                              out isModifyBookingSuccess, out isMakePaymentSuccess);
                        if (errors != null)
                        {
                            ProcessOWSFailuresInPaymentProcess(sessionTransactionId, isModifyBookingSuccess,
                                                            isMakePaymentSuccess);
                        }
                    }
                    catch (KeyNotFoundException keyNotFoundException)
                    {
                        AppLogger.LogOnlinePaymentFatalException(keyNotFoundException, keyNotFoundException.Message);
                        pageController.NetsAnnulProcess(sessionTransactionId);
                        PopulateErrorMessage(WebUtil.GetTranslatedText("/PSPErrors/InvalidCard"));
                    }
                    catch (Exception ex)
                    {
                        AppLogger.LogOnlinePaymentFatalException(ex, ex.Message);
                        pageController.NetsAnnulProcess(sessionTransactionId);
                        PopulateErrorMessage(WebUtil.GetTranslatedText("/PSPErrors/UnhandledError"));
                    }
                }
                else
                {
                    string errorMessage = !string.IsNullOrEmpty(Reservation2SessionWrapper.NetsPaymentErrorMessage)
                                              ? Reservation2SessionWrapper.NetsPaymentErrorMessage
                                              : Reservation2SessionWrapper.NetsPaymentInfoMessage;
                    PopulateErrorMessage(errorMessage);
                }
            }
            else
            {
                pageController.IgnoreBooking();
                pageController.ProcessErrorMessageForNetsErrorCode(paymentResponseCode, PaymentConstants.PaymentWindowErrorSource);
                string errorMessage = !string.IsNullOrEmpty(Reservation2SessionWrapper.NetsPaymentErrorMessage)
                                               ? Reservation2SessionWrapper.NetsPaymentErrorMessage
                                               : Reservation2SessionWrapper.NetsPaymentInfoMessage;
                PopulateErrorMessage(errorMessage);
            }
        }

        /// <summary>
        /// Populates saved credit cards of the logged in user.
        /// </summary>
        private void PopulateFGPSavedCreditCards()
        {
            divFGPSavedCreditCards.Visible = false;
            string firstItemText = string.Empty;
            if (pageController.IsAuthenticated)
            {
                var options = pageController.GetGuaranteeOptions();
                var pageConfig = pageController.GetPageConfig<BookingDetailsPageSection>();
                bool isPrepaidBooking = false;
                if ((options != null) && (options.Count > 0))
                {
                    if (options.Any(option => (RoomRateUtil.IsPrepaidGuaranteeType(option.Key) &&
                        !contentDataAccessManager.GetPaymentFallback(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode))))
                    {
                        isPrepaidBooking = true;
                    }
                }
                DropDownList ddlFGPCreditCards = null;
                if (isPrepaidBooking)
                {
                    ddlFGPCreditCards = ddlFGPSavedCreditCards;
                    firstItemText = pageConfig.PageDetail.PageMessages.GetMessage("useNewCreditCard");
                }
                else
                {
                    ddlFGPCreditCards = ddlFlexFGPCreditCards;
                    firstItemText = pageConfig.PageDetail.PageMessages.GetMessage("pleaseChooseACreditCard");
                }
                Collection<CreditCardEntity> savedCreditCards = pageController.FetchFGPCreditCards();
                if ((savedCreditCards != null) && (savedCreditCards.Count > 0))
                {
                    ddlFGPCreditCards.Visible = true;
                    string useCreditCardText = pageConfig.PageDetail.PageMessages.GetMessage("useCreditCard");
                    foreach (CreditCardEntity savedCreditCard in savedCreditCards)
                    {
                        savedCreditCard.CardNumber = string.Format("{0} {1}", useCreditCardText,
                                                                   WebUtil.GetMaskedCreditCardNo(savedCreditCard.CardNumber));
                    }
                    CreditCardEntity useNewCreditCard = new CreditCardEntity(string.Empty,
                        string.Empty, firstItemText, new ExpiryDateEntity(DateTime.Now.Month, DateTime.Now.Year));
                    savedCreditCards.Insert(0, useNewCreditCard);
                    ddlFGPCreditCards.DataSource = savedCreditCards;
                    ddlFGPCreditCards.DataTextField = "CardNumber";
                    ddlFGPCreditCards.DataValueField = "NameOnCard";
                    ddlFGPCreditCards.DataBind();
                    ddlFGPCreditCards.SelectedIndex = 0;
                    if (isPrepaidBooking)
                    {
                        divFGPSavedCreditCards.Visible = true;
                    }
                }
            }
        }

        /// <summary>
        /// Populates error message.
        /// </summary>
        /// <param name="errorMesage"></param>
        private void PopulateErrorMessage(string errorMesage)
        {
            List<MessageDetails> errorMessages = new List<MessageDetails>();
            MessageDetails errorMessage = new MessageDetails();
            errorMessage.MessageInfo = errorMesage;
            errorMessages.Add(errorMessage);
            infomationRepeater.DataSource = errorMessages;
            infomationRepeater.DataBind();
            infomationRepeater.Visible = true;
        }

        /// <summary>
        /// Handles OWS exceptions in payment process.
        /// </summary>
        private void ProcessOWSFailuresInPaymentProcess(string transactionId, bool isModifyBookingSuccess, bool isMakePaymentSuccess)
        {
            PopulateErrorMessage(WebUtil.GetTranslatedText("/PSPErrors/ErrorMessageForBookingFailureAfterAuthSuccess"));
            if (!isModifyBookingSuccess)
            {
                EmailEntity emailEntity = new EmailEntity();
                emailEntity.Recipient = AppConstants.EmailRecipientsForOnlinePaymentFailures;
                emailEntity.Sender = AppConstants.EmailSenderForOnlinePaymentFailures;
                emailEntity.Body = string.Format("Online payment(Mobile) | Modify booking failed for the transaction with the reservation number {0} and the transaction Id {1} with hotel operaId {2}.",
                                                                ReservationNumberSessionWrapper.ReservationNumber, Reservation2SessionWrapper.PaymentTransactionId, SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode);
                emailEntity.Subject = string.Format("Modify booking call failed.");
                CommunicationService.SendMail(emailEntity, null);
            }
            if (!isMakePaymentSuccess)
            {
                EmailEntity emailEntity = new EmailEntity();
                emailEntity.Recipient = AppConstants.EmailRecipientsForOnlinePaymentFailures;
                emailEntity.Sender = AppConstants.EmailSenderForOnlinePaymentFailures;
                emailEntity.Body = string.Format("Online payment(Mobile) | Make payment call failed for the transaction with the reservation number {0} and the transaction Id {1} with hotel operaId {2}.",
                                                               ReservationNumberSessionWrapper.ReservationNumber, Reservation2SessionWrapper.PaymentTransactionId, SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode);
                emailEntity.Subject =
                    string.Format(
                        "Make payment call failed in the process of online payment process.");
                CommunicationService.SendMail(emailEntity, null);
            }
        }

        /// <summary>
        /// PageId
        /// </summary>
        /// <returns></returns>
        public override MobilePages PageId()
        {
            return MobilePages.BookingDetails;
        }

        #region "Page Methods"

        /// <summary>
        /// Gets phone codes.
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static IEnumerable GetPhoneCodes()
        {
            try
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null &&
                    MiscellaneousSessionWrapper.PhoneCodeSessionkey != null)
                {
                    return MiscellaneousSessionWrapper.PhoneCodeSessionkey as List<KeyValueOption>;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ActionItem actionItem = null;
                Scandic.Scanweb.Mobile.UI.Entity.Booking.BookingContext bookingContext = MiscellaneousSessionWrapper.BookingContext as Scandic.Scanweb.Mobile.UI.Entity.Booking.BookingContext;
                actionItem = Utilities.GetActionItemToTrack(bookingContext);
                UserNavTracker.TrackAction(actionItem, true);
                UserNavTracker.LogAndClearTrackedData(ex != null ? ex : new Exception("Generic-Error Mobile Booking details"));
                throw ex;
            }
        }

        #endregion
    }
}