﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="SearchNearByHotel.aspx.cs" Inherits="Scandic.Scanweb.Mobile.Templates.Booking.SearchNearByHotel" MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" ResponseEncoding="utf-8"%>
<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<%@ Register Assembly="Scandic.Scanweb.Mobile" Namespace="Scandic.Scanweb.Mobile.UI.Controls"  TagPrefix="Mobile" %>

<asp:Content ID="startPageContent" ContentPlaceHolderID="cphMain" runat="server"> 
        
<ul class="info-list find-near-by">
        <asp:Repeater ID="cityListRepeater" runat="server">
            <ItemTemplate>
                <li class="cf">                                        
                    <asp:LinkButton ID="cityLinkButton" runat="server"></asp:LinkButton>
                    <asp:Label ID="cityDistance" runat="server" Text="Label"></asp:Label>
                </li>
            </ItemTemplate>            
        </asp:Repeater>
</ul>
</asp:Content>
