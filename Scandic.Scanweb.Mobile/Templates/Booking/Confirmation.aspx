﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Confirmation.aspx.cs" Inherits="Scandic.Scanweb.Mobile.Templates.Booking.Confirmation" MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" ResponseEncoding="utf-8" %>
<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<asp:Content ContentPlaceHolderID="cphMain" runat="server" ID="confirmationMain">
<head>
        <script type="text/javascript">
            var addToHomeConfig = {
            animationIn: 'bubble',
            animationOut: 'drop',
            lifespan:10000,
            expire:2,
            touchIcon:true,
            autostart: true,
            returningVisitor: true,
            message:'<%=BookMarkOverlayText%>'
        };
        </script>
        <script type="text/javascript" src=<%=BookMarkingScript %>></script>
    </head>
    
    
            
       
    <div class="confirmationAlertMsg">
        <div class="confirmationAlertEmail">
          <span class="fltLft cnfEmailHeading" id="emailsent" runat="server"></span>
          <br />
          <span id="hgcEmailConfirmation" class="cnfEmailValue" runat="server"></span>
        </div>
        <div class="confirmationAlertSMS" id="smslabel" runat="server" visible="false">
          <span class="fltLft cnfSmsHeading" id="smssent" runat="server" ></span>
          <br />
          <span id="hgcSMSConfirmation" class="cnfSmsValue" runat="server" visible="false"></span>
        </div>
     </div>
                  

    <ul class="info-list">
         <li class="cf"><span id="hgcReservationNumber" runat="server"></span><span id="hgcReservationNumberValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcHotel" runat="server"></span><span id="hgcHotelValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcArrivalDate" runat="server"></span><span id="hgcArrivalDateValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcDepartureDate" runat="server"></span><span id="hgcDepartureDateValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcNumOfNights" runat="server"></span><span id="hgcNumOfNightsValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcAdults" runat="server"></span><span id="hgcAdultsValue"
            runat="server"></span></li>
         <li class="cf" id="liChildrenInfo" runat="server" style="display: none;">
            <span id="hgcBedType" runat="server"></span>
            <span id="hgcChildrenDetailValue" runat="server"></span>
        </li>
        <li class="cf"><span id="hgcRoomType" runat="server"></span><span id="hgcRoomTypeValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcRateType" runat="server"></span><span id="hgcRateTypeValue"
            runat="server"></span><span id="hgcRateTypeText" class="rateHighlightTypeText" runat="server"></span></li>
        <li class="cf" id="hgcFirstNightPriceRow" runat="server"><span id="hgcFirstNightPrice" runat="server"></span><span id="hgcFirstNightPriceValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcTotalPrice" runat="server"></span><span id="hgcTotalPriceValue"
            runat="server"></span>
            <a href="#" data-target="overlay-TotalPrice" class="icon tooltip" data-overlaytype="tooltip"
                data-associatedcontrol="hgcTotalPrice" onclick="LoadOverlay(this); return false;">
                &nbsp;</a>    
        </li>

    </ul>
    <h3 id="hgcHotelName" runat="server"></h3>
        
    <ul class="info-list">
        <li class="cf"><span id="hgcHotelMobileNumber" runat="server"></span>
            <span>
                <asp:HyperLink ID="lnkHotelMobileNumber" runat="server" onclick="return performFunctionTracking(2,'');" CssClass="icon click2call phone"></asp:HyperLink>
            </span>
        </li>
        <li class="cf"><span id="hgcHotelEmail" runat="server"></span>
            <span>
                <asp:HyperLink ID="lnkHotelEmail" runat="server" onclick="return performFunctionTracking(5,'');" ></asp:HyperLink>
            </span>
        </li>
        <li class="cf"><span id="hgcHotelAddress" runat="server"></span>
            <span>
                <asp:HyperLink ID="lnkHotelAddress" runat="server" Target="_blank" onclick="return performFunctionTracking(4,'');" ></asp:HyperLink>
            </span>
        </li>
    </ul>
    <h3 id="hgcYourInfo" runat="server"></h3>
    <ul class="info-list">
        <li class="cf"><span id="hgcName" runat="server"></span><span id="hgcNameValue"
            runat="server"></span></li>        
        <li class="cf"><span id="hgcEmail" runat="server"></span><span id="hgcEmailValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcPhoneNumber" runat="server"></span><span id="hgcPhoneNumberValue"
            runat="server"></span></li>
        <li class="cf" id="hgcMembershipContainer" runat="server"><span id="hgcMembershipNumber" runat="server"></span><span id="hgcMembershipNumberValue"
            runat="server"></span></li>
            </ul>
             <ul class="info-list">
        <li class="cf">
            <h3 id="hgcPolicy" runat="server"></h3>
            <div id="hgcPolicyValue" runat="server" class="cmscontent"></div>
        </li>
        <li class="cf">
            <asp:HyperLink ID="lnkFeedback" runat="server" ></asp:HyperLink>
        </li>
    </ul>
     <asp:LinkButton ID="lbtnBookAnotherRoom" runat="server" CssClass="button green hand submit"></asp:LinkButton>
     <asp:PlaceHolder runat="server" ID="TrivagoPlaceHolder" Visible="false"></asp:PlaceHolder>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphScript" ID="confirmationScripts" runat="server">
     <script src="/ScanwebMobile/Public/Scripts/Confirmation.min.js"></script>
</asp:Content>
