﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="SelectRate.aspx.cs" Inherits="Scandic.Scanweb.Mobile.Templates.Booking.SelectRate"
    MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" ResponseEncoding="utf-8" %>

<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<%@ Register Src="/ScanwebMobile/Templates/Controls/HotelInfoSelectRate.ascx" TagName="HotelInfo"
    TagPrefix="uc1" %>
<asp:Content ID="selectRatePageContent" ContentPlaceHolderID="cphMain" runat="server">
    <ul class="info-list itinerary-info">
        <li class="cf"><span id="hgcDates" runat="server"></span><span id="hgcArrivalDateValue"
            runat="server"></span><span>&nbsp;-&nbsp;</span> <span id="hgcDepartureDateValue"
                runat="server"></span></li>
        <li class="cf"><span id="hgcNumOfNights" runat="server"></span><span id="hgcNumOfNightsValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcAdults" runat="server"></span><span id="hgcAdultsValue"
            runat="server"></span></li>
        <li class="cf" id="liChildrenInfo" runat="server" style="display: none;"><span id="hgcBedType"
            runat="server"></span><span id="hgcChildrenDetailValue" runat="server"></span>
        </li>
        <li class="cf"><a class="edit-search right" onclick="trackFunction(this, 11);" id="editSearch" runat="server"></a></li>
    </ul>
    <div class="select-rate-wrapper">
        <!--Select rate page Hotel Information Start-->
        <uc1:HotelInfo id="HotelInfoSelectRate" runat="server" />
        <!--Select rate page Hotel Information End-->
        <div class="rooms-info">
        </div>
        
		<!--End of Rooms List UL -->
		<div class="rooms-list" style="display: none">
			<ul class="rooms-list">
				<li class="room-type" >
					<div class="room-name">
					</div>
					<div class="room-image">
						<img alt="" src="" /></div>
					<div class="room-desc">
					</div>
					<ul class="rooms-price-container">
					</ul>
					<!-- End of Rooms Price Container -->
				</li>
			</ul>
		</div>
		<div class="templatePriceMarkup" style="display: none">
			<li class="templateBooking-section" >
				<div class="booking-type">
			</div>			
			<div class="hotel-book-btn right">
				<a class="book-btn" href="#" onclick="trackFunction(this, 12);SubmitSelectedRate(this); return false;"><span>
				</span></a>
			</div>
			<div class="price-details right">
				<span class="price"></span>
				<br />
				<span class="price-desc"></span>
			</div>
			<div class="horizontal-rule-short">
			</div>
			</li>
		</div>
    </div>
    
    <a class="button purple submit view-more-results" href="#" id="hgcViewMoreResults"
        runat="server" style="display: none"></a>
    <asp:HyperLink ID="lnkBack" runat="server" CssClass="button green submit">Back</asp:HyperLink>
    <asp:HiddenField ID="hidValue" runat="server" />
    <div id="overlay-tripiframe" class="overlay">
        <div class="top">
            <a onclick="return closeOverlay();" href="#" class="close">x</a>
            <div id="tripAdvisorIframe">
            </div>
        </div>
    </div>
	<div class="rooms-list" style="display: none">
		<ul class="rooms-list">
			<li class="room-type" >
				<div class="room-name">
				</div>
				<div class="room-image">
					<img alt="" src="" /></div>
				<div class="room-desc">
				</div>
				<ul class="rooms-price-container">
				</ul>
				<!-- End of Rooms Price Container -->
			</li>
		</ul>
	</div>
	<div class="priceMarkup" style="display: none">
		<li class="booking-section" >
			<div class="booking-type">
			</div>			
			<div class="hotel-book-btn right">
				<a class="book-btn" href="#" onclick="SubmitSelectedRate(this); return false;"><span>
				</span></a>
			</div>
			<div class="price-details right">
				<span class="price"></span>
				<br />
				<span class="price-desc"></span>
			</div>
			<div class="horizontal-rule-short">
			</div>
		</li>
	</div>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphOutsideForm" runat="server" ID="cphSelectedOffer">
    <form id="selectedHotelOffer" method="post">
        <input type="hidden" name="pageName"  id="pageName" />
    </form>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphScript" runat="server" ID="pageScripts">

    <script type="text/javascript">
        var noOfRoomTypesToDisplay = "<%= maxNumberOfRoomTypesPerPage %>";
        var maxRoomTypesPerPage = "<%= maxNumberOfRoomTypesPerPage %>";
        var bookNowText = "<%= bookNowText %>";
        var pointsText = "<%= pointsText %>";
    </script>

    <script src="<%= ResolveUrl("~/ScanwebMobile/Public/Scripts/SelectRate.min.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>

    </script>
</asp:Content>	