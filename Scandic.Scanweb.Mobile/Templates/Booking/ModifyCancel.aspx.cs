﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using System.Web.UI.HtmlControls;
using BookingEngineEntity = Scandic.Scanweb.Entity;
using System.Web.Services;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.Mobile.Templates.Booking
{
    [AllowPublicAccess(true)]
    public partial class ModifyCancel : VisualBasePage<ModifyCancelPageSection>
    {
        private ModifyCancelController pageController;
        private const string RESERVATION_NUMBER_TEXT = "reservationNumberText";
        private const string ROOM_TEXT = "roomText";
        private const string ROOM_INFORMATION_TEXT = "roomInfoText";
        private const string CONTACT_INFORMATION_TEXT = "contactInfoText";
        private const string FIRST_NAME_TEXT = "firstNameText";
        private const string LAST_NAME_TEXT = "lastNameText";
        private const string EMAIL_TEXT = "emailText";
        private const string PHONE_NUMBER_TEXT = "phoneNumberText";
        private const string MEMBERSHIP_NUMBER_TEXT = "membershipNumberText";
        private const string CANCEL_BOOKED_ROOM_TEXT = "cancelBookedRoomText";
        private const string ROOM_TYPE_TEXT = "roomTypeText";
        private const string RATE_TYPE_TEXT = "rateTypeText";
        private const string ADULT_TEXT = "adultText";
        private const string PRICE_FIRST_NIGHT_TEXT = "priceFirstNightText";
        private const string CONTACT_PERSON_TEXT = "contactPersonText";
        private const string CHILDREN_TEXT = "childrenText";
        private const string INCLUDE_ALL_TAXES_TEXT = "includeAllTaxesText";
        private int roomCount;
        private bool isPastBooking = false;
        private string includeAllTaxes = string.Empty;
        private string hotelCode;
        private string searchTypeCode;
        private IBookingRepository bookingRepository;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/Booking/ModifyCancel.aspx";
            this.rptBookedRooms.ItemDataBound += new RepeaterItemEventHandler(rptBookedRooms_ItemDataBound);
            pageController = new ModifyCancelController();
            //Purpose of using this repository in web page is to call and initialize the rate tootip data for this 
            //page in correct language context, so that when it is accessed from Page method (or web method) right 
            //langueage context data is available.
            bookingRepository = DependencyResolver.Instance.GetService(typeof(IBookingRepository)) as IBookingRepository;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            hgcBookingErrMsg.Visible = false;
            if (!IsPostBack)
            {
                LoadControls();
            }

        }

        private void LoadControls()
        {
            var userReservation = pageController.GetPageData();
            var messageCode = string.Empty;
            if (userReservation != null)
            {
                if (userReservation.Error != null)
                {
                    
                    if (userReservation.Error.ErrorMessaage.Equals(AppConstants.GUARANTEETYPE_FOR_SESSION_BOOKING))
                    {
                        hgcBookingErrMsg.Attributes.Add("class", "");
                        hgcBookingErrMsg.Style.Add("color", "red");
                        userReservation.Error.ErrorMessaage = WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/SessionBookingHelpDescription");
                    }
					hgcBookingErrMsg.InnerHtml = userReservation.Error.ErrorMessaage;
                }
                else if (userReservation.Message != null)
                {
                    hgcBookingErrMsg.InnerHtml = userReservation.Message.MessageInfo;
                    isPastBooking = string.Equals(userReservation.Message.MessageCode, Reference.OLD_BOOKING);
                    messageCode = userReservation.Message.MessageCode;
                }
                hgcBookingErrMsg.Visible = !string.IsNullOrEmpty(hgcBookingErrMsg.InnerText);
            }
            SetHotelInfo(userReservation, messageCode);
            this.Master.PageId = PageId();

        }

        void rptBookedRooms_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var data = e.Item.DataItem as ReservedRoomDetails;
                if (data != null)
                {
                    var pageConfig = pageController.GetPageConfig<ModifyCancelPageSection>();
                    var roomText = pageConfig.PageDetail.PageMessages.GetMessage(ROOM_TEXT);
                    var roomHeadingCtl = e.Item.FindControl("hgcRoomHeading") as HtmlGenericControl;                    
                    roomCount ++;
                    roomHeadingCtl.InnerText = string.Format("{0} {1} - {2} - {3}", roomText, roomCount, data.RoomType, data.ReservationNumber);
                    SetRoomInfo(e.Item, data, roomHeadingCtl.InnerText);
                    SetCustomerInfo(e.Item, data);
                }
            }
        }

        private void SetRoomInfo(RepeaterItem item, ReservedRoomDetails data, string roomHeading)
        {
            var pageConfig = pageController.GetPageConfig<ModifyCancelPageSection>();
            var roomInfoText = pageConfig.PageDetail.PageMessages.GetMessage(ROOM_INFORMATION_TEXT);
            var roomInformationCtl = item.FindControl("lnkRoomInfo") as HyperLink;
            var roomTypeLableCtl = item.FindControl("hgcRoomType") as HtmlGenericControl;
            var roomTypeCtl = item.FindControl("hgcRoomTypeValue") as HtmlGenericControl;
            var roomTypeText = pageConfig.PageDetail.PageMessages.GetMessage(ROOM_TYPE_TEXT);
            var rateTypeLableCtl = item.FindControl("hgcRateType") as HtmlGenericControl;
            var rateTypeCtl = item.FindControl("hgcRateTypeValue") as HtmlGenericControl;
            var rateTypeText = pageConfig.PageDetail.PageMessages.GetMessage(RATE_TYPE_TEXT);
            var rateToolTipCtl = item.FindControl("lnkRateToolTip") as HyperLink;
            var adultLableCtl = item.FindControl("hgcAdults") as HtmlGenericControl;
            var adultCtl = item.FindControl("hgcAdultsValue") as HtmlGenericControl;
            var adultText = pageConfig.PageDetail.PageMessages.GetMessage(ADULT_TEXT);
            var priceFirstNightLableCtl = item.FindControl("hgcPriceFirstNight") as HtmlGenericControl;
            var priceFirstNightCtl = item.FindControl("hgcPriceFirstNightValue") as HtmlGenericControl;
            var priceFirstNightText = pageConfig.PageDetail.PageMessages.GetMessage(PRICE_FIRST_NIGHT_TEXT);
            var roomInfoDiv = item.FindControl("roomInfo") as HtmlGenericControl;
            var cancelBookedRoomLink = item.FindControl("lnkCancelRoomBooking") as HyperLink;
            var cancelBookedRoomRow = item.FindControl("hgcCancelRoomBookingRow") as HtmlGenericControl;
            var cancelBookedRoomText = pageConfig.PageDetail.PageMessages.GetMessage(CANCEL_BOOKED_ROOM_TEXT);
            var childrenLableCtrl = item.FindControl("hgcChildren") as HtmlGenericControl;
            var childrenCtrl = item.FindControl("hgcChildrenValue") as HtmlGenericControl;
            var childrenRow = item.FindControl("hgcChildrenRow") as HtmlGenericControl;
            var childrenText = pageConfig.PageDetail.PageMessages.GetMessage(CHILDREN_TEXT);            

            roomInformationCtl.Text = roomInfoText;
            roomInformationCtl.Attributes.Add("data-target", roomInfoDiv.ClientID);
            roomTypeCtl.InnerText = data.RoomType;
            roomTypeLableCtl.InnerText = roomTypeText;
            rateTypeCtl.InnerText = data.RateType;
            rateTypeLableCtl.InnerText = rateTypeText;
            rateToolTipCtl.Attributes.Add("data-target", string.Format("rateType{0}", data.RateTypeId));
            rateToolTipCtl.Attributes.Add("data-overlaytype", "ratetooltip");
            rateToolTipCtl.Attributes.Add("data-value", string.Format("{0}#{1}", data.RateTypeId, data.IsBlockCode.ToString()));
             //RateTooltip is accessed here on page load so that it is available in 
            //right language context when user's request is fired from Page Methods(Web Method)
            bookingRepository.GetRateTypeToolTip(data.RateTypeId, data.IsBlockCode);
            adultCtl.InnerText = data.NoOfAdults;
            adultLableCtl.InnerText = adultText;
            priceFirstNightCtl.InnerText = string.Format("{0} {1}", data.PriceFirstNight, includeAllTaxes);
            priceFirstNightLableCtl.InnerText = priceFirstNightText;
            cancelBookedRoomLink.Text = cancelBookedRoomText;
            cancelBookedRoomLink.Attributes.Add("data-cancellable", data.IsCancellable.ToString());
            cancelBookedRoomLink.Attributes.Add("data-reservationnumber", data.ReservationNumber);
            cancelBookedRoomLink.Attributes.Add("data-policycode", data.GuaranteeCancellationPolicyCode);
            cancelBookedRoomLink.Attributes.Add("data-roomheading", roomHeading);
            cancelBookedRoomLink.Attributes.Add("data-hotelcode", hotelCode );
            cancelBookedRoomLink.Attributes.Add("data-searchtypecode", searchTypeCode);
            cancelBookedRoomRow.Visible = !isPastBooking;
            if (!data.IsCancellable)
            {
                cancelBookedRoomLink.CssClass = string.Format("{0} disabled", cancelBookedRoomLink.CssClass);
            }
            childrenLableCtrl.InnerText = childrenText;
            childrenCtrl.InnerText = data.NoOfChildren;
            childrenRow.Visible = !string.IsNullOrEmpty(data.NoOfChildren);
            if (data.HideRateInfo)
            {
                var rateInfo = item.FindControl("rateInfo") as HtmlGenericControl;
                var firstNightPriceInfo = item.FindControl("firstNightPriceInfo") as HtmlGenericControl;

                if(rateInfo != null)
                    rateInfo.Visible = false;

                if(firstNightPriceInfo != null)
                    firstNightPriceInfo.Visible = false;
            }
        }

        private void SetCustomerInfo(RepeaterItem item, ReservedRoomDetails data)
        {
            var pageConfig = pageController.GetPageConfig<ModifyCancelPageSection>();
            var contactInfoText = pageConfig.PageDetail.PageMessages.GetMessage(CONTACT_INFORMATION_TEXT);
            var contactInformationCtl = item.FindControl("lnkContactInfo") as HyperLink;
            var firstNameLableCtl = item.FindControl("hgcCustomerFirstName") as HtmlGenericControl;
            var firstNameCtl = item.FindControl("hgcCustomerFirstNameValue") as HtmlGenericControl;
            var firstNameText = pageConfig.PageDetail.PageMessages.GetMessage(FIRST_NAME_TEXT);
            var lastNameLableCtl = item.FindControl("hgcCustomerLastName") as HtmlGenericControl;
            var lastNameCtl = item.FindControl("hgcCustomerLastNameValue") as HtmlGenericControl;
            var lastNameText = pageConfig.PageDetail.PageMessages.GetMessage(LAST_NAME_TEXT);
            var emailLableCtl = item.FindControl("hgcCustomerEmail") as HtmlGenericControl;
            var emailCtl = item.FindControl("hgcCustomerEmailValue") as HtmlGenericControl;
            var emailText = pageConfig.PageDetail.PageMessages.GetMessage(EMAIL_TEXT);
            var phoneLableCtl = item.FindControl("hgcCustomerPhone") as HtmlGenericControl;
            var phoneCtl = item.FindControl("hgcCustomerPhoneValue") as HtmlGenericControl;
            var phoneText = pageConfig.PageDetail.PageMessages.GetMessage(PHONE_NUMBER_TEXT);
            var membershipIdRow = item.FindControl("hgcMembershipRow") as HtmlGenericControl;
            var membershipIdLableCtl = item.FindControl("hgcMembership") as HtmlGenericControl;
            var membershipIdCtl = item.FindControl("hgcMemebershipValue") as HtmlGenericControl;
            var membershipIdText = pageConfig.PageDetail.PageMessages.GetMessage(MEMBERSHIP_NUMBER_TEXT);
            var custInfoDiv = item.FindControl("contactInfo") as HtmlGenericControl;
            var contactPersonLableCtrl = item.FindControl("hgcContactPerson") as HtmlGenericControl;
            var contactPersonCtrl = item.FindControl("hgcContactPersonValue") as HtmlGenericControl;
            var contactPersonText = pageConfig.PageDetail.PageMessages.GetMessage(CONTACT_PERSON_TEXT);

            contactInformationCtl.Text = contactInfoText;
            contactInformationCtl.Attributes.Add("data-target", custInfoDiv.ClientID);
            firstNameCtl.InnerText = data.CustomerDetails.FirstName;
            firstNameLableCtl.InnerText = firstNameText;
            lastNameCtl.InnerText = data.CustomerDetails.LastName;
            lastNameLableCtl.InnerText = lastNameText;
            emailCtl.InnerText = data.CustomerDetails.Email;
            emailLableCtl.InnerText = emailText;
            phoneCtl.InnerText = data.CustomerDetails.PhoneNumber;
            phoneLableCtl.InnerText = phoneText;
            membershipIdCtl.InnerText = data.CustomerDetails.MembershipNumber;
            membershipIdRow.Visible = !string.IsNullOrEmpty(membershipIdCtl.InnerText);
            membershipIdLableCtl.InnerText = membershipIdText;
            contactPersonLableCtrl.InnerText = contactPersonText;
            contactPersonCtrl.InnerText = string.Format("{0} {1}", data.CustomerDetails.FirstName, data.CustomerDetails.LastName);
        }            

        private void SetHotelInfo(ReservationDetails reservationDetails, string messageCode)
        {
            if (reservationDetails != null && reservationDetails.HotelDetails != null &&
                reservationDetails.HotelDetails.BookedRooms != null && reservationDetails.HotelDetails.BookedRooms.Count > 0)
            {
                var masterPage = Page.Master as MobileDefault;
                var pageConfig = pageController.GetPageConfig<ModifyCancelPageSection>();
                var isCancellable = true;
                var isLegBooking = false;
                var queryStrReservationNumber = Request.QueryString[Reference.RESERVATION_ID_QUERY_STRING];
                var siteInfoRepository = DependencyResolver.Instance.GetService(typeof(ISiteInfoRepository)) as ISiteInfoRepository;
                if ((reservationDetails.HotelDetails.HotelSearchType == BookingEngineEntity.SearchType.REGULAR ||
                    reservationDetails.HotelDetails.HotelSearchType == BookingEngineEntity.SearchType.CORPORATE ||
                    reservationDetails.HotelDetails.HotelSearchType == BookingEngineEntity.SearchType.MODIFY) &&
                    !reservationDetails.HotelDetails.HideIncludingAllTaxText)
                {
                    includeAllTaxes = pageConfig.PageDetail.PageMessages.GetMessage(INCLUDE_ALL_TAXES_TEXT);
                }

                masterPage.PageHeading = string.Format("{0} - {1}", pageConfig.PageDetail.PageMessages.GetMessage(RESERVATION_NUMBER_TEXT),
                                                        reservationDetails.ReservationNumber);
                hgcHotelValue.InnerText = reservationDetails.HotelDetails.HotelName;
                lnkHotelAddress.NavigateUrl = string.Format("http://maps.google.com/?q={0}", reservationDetails.HotelDetails.Address);
                lnkHotelAddress.Text = reservationDetails.HotelDetails.Address;
                if (!string.IsNullOrEmpty(reservationDetails.HotelDetails.PhoneNumber))
                {
                    lnkHotelPhone.NavigateUrl = string.Format("tel:{0}", reservationDetails.HotelDetails.PhoneNumber.Replace(" ", ""));
                }
                lnkHotelPhone.Text = reservationDetails.HotelDetails.PhoneNumber;
                lnkHotelEmail.NavigateUrl = string.Format("mailto:{0}", reservationDetails.HotelDetails.Email);
                lnkHotelEmail.Text = reservationDetails.HotelDetails.Email;
                hgcArrivalDateValue.InnerText = reservationDetails.HotelDetails.ArrivalDate.ToString("dddd, MMMM dd, yyyy", siteInfoRepository.GetCurrentCultureInfo());
                hgcDepartureDateValue.InnerText = reservationDetails.HotelDetails.DepartureDate.ToString("dddd, MMMM dd, yyyy", siteInfoRepository.GetCurrentCultureInfo());
                hgcTotalPriceValue.InnerText = string.Format("{0} {1}", reservationDetails.HotelDetails.TotalPrice, includeAllTaxes);
                roomCount = 0;
                hotelCode = reservationDetails.HotelDetails.HotelCode;
                searchTypeCode = reservationDetails.HotelDetails.HotelSearchType.ToString();
                rptBookedRooms.DataSource = reservationDetails.HotelDetails.BookedRooms;
                rptBookedRooms.DataBind();
                hgcGuaranteePolicyValue.InnerHtml = pageController.GetGenricGuaranteePolicyText();
                hgcCancelBookingRow.Visible = !isPastBooking;
                isLegBooking = !string.IsNullOrEmpty(queryStrReservationNumber) && queryStrReservationNumber.Contains(AppConstants.HYPHEN);
                isCancellable = string.Equals(messageCode, Reference.FLEX_BOOKING) && !isLegBooking;
                hgcCancelBookingRow.Visible = isCancellable;
                lnkCancelBooking.Attributes.Add("onclick", "return ConfimCompleteBookingCancellation(this);");
                lnkCancelBooking.Attributes.Add("data-cancellable", isCancellable.ToString());

                if (reservationDetails.HotelDetails.HideRateInfo && totalPriceRow != null)
                    totalPriceRow.Visible = false;
            }
            else
            {
                //No valid booking available so hide the controls.
                hgcHotelInfo.Visible = false;
                hgcPolicyInfo.Visible = false;
            }
        }
        public override MobilePages PageId()
        {
            return MobilePages.ModifyCancel;
        }

        #region WebMethod

        [WebMethod]
        public static string GetConfirmMessageOverlay(bool isCancellable, string reservationNumber, string policyCode, string roomHeadingText, string cancellationControlId)
        {
            try
            {
                var controller = new ModifyCancelController();
                return controller.GetConfirmMessageOverlay(isCancellable, reservationNumber, policyCode, roomHeadingText, cancellationControlId);
                
            }
            catch (Exception ex)
            {
                string methodName = Reference.GETCONFIRM_MESSAGE_OVERLAY;
                ActionItem actionItem = null;
                actionItem = Utilities.GetActionItemInModifyCancelFlow(methodName, isCancellable, reservationNumber, policyCode, roomHeadingText, cancellationControlId, null, null);
                UserNavTracker.TrackAction(actionItem, true);
                UserNavTracker.LogAndClearTrackedData(ex != null ? ex : new Exception("Generic-Error Mobile ModifyCancel GetConfirmMessageOverlay method"));
                throw ex;
            }
        }

        [WebMethod]
        public static List<KeyValueOption> CancelRoom(CancelRoomDetails cancellationDetails, string currentLanguage)
        {
            try
            {
                var controller = new ModifyCancelController();
                return controller.GetRoomCancellationMessageOverly(cancellationDetails, currentLanguage);
                
            }
            catch (Exception ex)
            {
                string methodName = Reference.CANCEL_ROOM;
                List<CancelRoomDetails> listCancelRoomDetails = new List<CancelRoomDetails>();
                listCancelRoomDetails.Add(cancellationDetails);
                ActionItem actionItem = null;
                actionItem = Utilities.GetActionItemInModifyCancelFlow(methodName, false, null, null, null, null, listCancelRoomDetails, currentLanguage);
                UserNavTracker.TrackAction(actionItem, true);
                UserNavTracker.LogAndClearTrackedData(ex != null ? ex : new Exception("Generic-Error Mobile ModifyCancel CancelRoom method"));
                throw ex;
            }
        }

        [WebMethod]
        public static string GetCancelBookingOverlay(bool isCancellable)
        {
            try
            {
                var controller = new ModifyCancelController();
                return controller.GetCancelBookingOverlay(isCancellable);
                
            }
            catch (Exception ex)
            {
                string methodName = Reference.GETCANCEL_BOOKING_OVERLAY;
                ActionItem actionItem = null;
                actionItem = Utilities.GetActionItemInModifyCancelFlow(methodName, isCancellable, null, null, null, null, null, null);
                UserNavTracker.TrackAction(actionItem, true);
                UserNavTracker.LogAndClearTrackedData(ex != null ? ex : new Exception("Generic-Error Mobile ModifyCancel GetCancelBookingOverlay method"));
                throw ex;
            }
        }

        [WebMethod]
        public static List<KeyValueOption> CancelCompleteBooking(List<CancelRoomDetails> cancellationDetails, string currentLanguage)
        {
            try
            {
                var controller = new ModifyCancelController();
                return controller.CancelCompleteBooking(cancellationDetails, currentLanguage);
                
            }
            catch (Exception ex)
            {
                string methodName = Reference.CANCLE_COMPLETE_BOOKING;
                ActionItem actionItem = null;
                actionItem = Utilities.GetActionItemInModifyCancelFlow(methodName, false, null, null, null, null, cancellationDetails, currentLanguage);
                UserNavTracker.TrackAction(actionItem, true);
                UserNavTracker.LogAndClearTrackedData(ex != null ? ex : new Exception("Generic-Error Mobile ModifyCancel CancelCompleteBooking method"));
                throw ex;
            }
        }
        #endregion
    }
}
