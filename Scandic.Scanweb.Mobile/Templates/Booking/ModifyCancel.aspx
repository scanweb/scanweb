﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="ModifyCancel.aspx.cs"
    Inherits="Scandic.Scanweb.Mobile.Templates.Booking.ModifyCancel" MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>

<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<asp:Content ID="modifyCancelMain" ContentPlaceHolderID="cphMain" runat="server">
    <p class="icon crop information" runat="server" id="hgcBookingErrMsg" visible="false">
    </p>
    <ul class="info-list" id="hgcHotelInfo" runat="server">
        <li class="cf"><span id="hgcHotel" runat="server"></span><span id="hgcHotelValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcHotelAddress" runat="server"></span><span>
            <asp:HyperLink ID="lnkHotelAddress" runat="server" Target="_blank" onclick="return performFunctionTracking(4,'');"></asp:HyperLink>
        </span></li>
        <li class="cf"><span id="hgcHotelPhone" runat="server"></span><span>
            <asp:HyperLink ID="lnkHotelPhone" runat="server" CssClass="click2call" onclick="return performFunctionTracking(2,'');" ></asp:HyperLink></span>
        </li>
        <li class="cf"><span id="hgcHotelEmail" runat="server"></span><span>
            <asp:HyperLink ID="lnkHotelEmail" runat="server" onclick="return performFunctionTracking(5,'');"></asp:HyperLink></span> </li>
        <li class="cf"><span id="hgcArrivalDate" runat="server"></span><span id="hgcArrivalDateValue"
            runat="server"></span></li>
        <li class="cf"><span id="hgcDepartureDate" runat="server"></span><span id="hgcDepartureDateValue"
            runat="server"></span></li>
        <li class="cf" id="totalPriceRow" runat="server"><span id="hgcTotalPrice" runat="server"></span><span id="hgcTotalPriceValue"
            runat="server"></span></li>
        <li class="cf" id="hgcCancelBookingRow" runat="server">
            <p class="right">
                <asp:HyperLink ID="lnkCancelBooking" runat="server" CssClass="button red"> </asp:HyperLink>
            </p>
        </li>
    </ul>
    <asp:Repeater ID="rptBookedRooms" runat="server">
        <ItemTemplate>
            <ul id="roomHeader" class="info-list">
                <li class="cf">
                    <h3 id="hgcRoomHeading" runat="server">
                    </h3>
                </li>
                <li class="cf"><span id="hgcContactPerson" runat="server"></span><span id="hgcContactPersonValue"
                    runat="server"></span></li>
            </ul>
            <p>
                <strong>
                    <asp:HyperLink ID="lnkRoomInfo" runat="server" CssClass="information toggler-slide"
                    data-target="roomInfo" NavigateUrl="#">room info</asp:HyperLink>
                </strong>
            </p>
            <div id="roomInfo" class="hidden" runat="server">
                <ul id="roomInfoList" class="info-list">
                    <li class="cf"><span id="hgcRoomType" runat="server"></span><span id="hgcRoomTypeValue"
                        runat="server"></span></li>
                    <li class="cf" id="rateInfo" runat="server"><span id="hgcRateType" runat="server"></span><span id="hgcRateTypeValue"
                        runat="server"></span>
                        <asp:HyperLink ID="lnkRateToolTip" runat="server" onclick="LoadOverlay(this); return false;"
                            CssClass="icon tooltip" NavigateUrl="#">&nbsp;</asp:HyperLink>
                    </li>
                    <li class="cf"><span id="hgcAdults" runat="server"></span><span id="hgcAdultsValue"
                        runat="server"></span></li>
                    <li class="cf" id="hgcChildrenRow" runat="server"><span id="hgcChildren" runat="server">
                    </span><span id="hgcChildrenValue" runat="server"></span></li>
                    <li class="cf" id="firstNightPriceInfo" runat="server"><span id="hgcPriceFirstNight" runat="server"></span><span id="hgcPriceFirstNightValue"
                        runat="server"></span></li>
                </ul>
            </div>
            <p>
                <strong><asp:HyperLink ID="lnkContactInfo" runat="server" CssClass="information toggler-slide"
                    data-target="contactInfo" NavigateUrl="#">Contact info</asp:HyperLink>
                </strong>
            </p>
            <div id="contactInfo" class="hidden" runat="server">
                <ul id="contactInfoList" class="info-list">
                    <li class="cf"><span id="hgcCustomerFirstName" runat="server"></span><span id="hgcCustomerFirstNameValue"
                        runat="server"></span></li>
                    <li class="cf"><span id="hgcCustomerLastName" runat="server"></span><span id="hgcCustomerLastNameValue"
                        runat="server"></span></li>
                    <li class="cf"><span id="hgcCustomerEmail" runat="server"></span><span id="hgcCustomerEmailValue"
                        runat="server"></span></li>
                    <li class="cf"><span id="hgcCustomerPhone" runat="server"></span><span id="hgcCustomerPhoneValue"
                        runat="server"></span></li>
                    <li class="cf" id="hgcMembershipRow" runat="server"><span id="hgcMembership" runat="server">
                    </span><span id="hgcMemebershipValue" runat="server"></span></li>
                </ul>
            </div>
            <ul class="info-list">
                <li class="cf" id="hgcCancelRoomBookingRow" runat="server">
                    <p class="right">
                        <asp:HyperLink ID="lnkCancelRoomBooking" runat="server" CssClass="button red" onclick="return ConfirmCancellation(this);"> </asp:HyperLink>
                    </p>
                </li>
            </ul>
        </ItemTemplate>
    </asp:Repeater>
    <ul class="info-list" id="hgcPolicyInfo" runat="server">
        <li class="cf">
            <div id="hgcGuaranteePolicyValue" runat="server">
            </div>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="modifyCancelScript" ContentPlaceHolderID="cphScript" runat="server">
    <script src="<%= ResolveUrl("~/ScanwebMobile/Public/Scripts/ModifyCancelBooking.min.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphOutsideForm" ID="outSideForm" runat="server">
    <form id="cancelConfirmationForm" method="post" action="" name="cancelConfirmationForm">
    <input type="hidden" id="ErrorCount" name="ErrorCount" />
    <input type="hidden" id="Message" name="Message" />
    <input type="hidden" id="ReservationNumber" name="ReservationNumber" />
    </form>
</asp:Content>
