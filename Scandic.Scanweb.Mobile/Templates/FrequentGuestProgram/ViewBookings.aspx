﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="ViewBookings.aspx.cs" Inherits="Scandic.Scanweb.Mobile.Templates.FrequentGuestProgram.ViewBookings" MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" %>
<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<%@ Register src="/ScanwebMobile/Templates/Controls/MemberLogin.ascx" tagname="MemberLogin" tagprefix="uc2" %>

<asp:Content ContentPlaceHolderID="cphMain" ID="viewBookingsMain" runat="server">
    <p id="hgcIntroText" runat="server"></p>
    <br />
    <div class="standard-form">  
        <h3 id="hgcMyBookings" runat="server">
            <asp:HyperLink ID="lnkMyBookings" runat="server"></asp:HyperLink>
        </h3>
        <div id="hgcFGPLoginContainer" runat="server" >
            <h3>            
                <asp:HyperLink ID="lnkMemberLogin" runat="server"  CssClass="toggler-slide" data-target="memberLogin"></asp:HyperLink>
                
            </h3>
            <div id="memberLogin" class="cf signIn hidden" style="display: none;">
                <uc2:MemberLogin ID="uctlLogin" runat="server" />
            </div>
        </div>
        <h3>            
            <asp:HyperLink ID="lnkFindBooking" runat="server" CssClass="toggler-slide" data-target="findBooking"></asp:HyperLink>
        </h3>
        <div id="findBooking" class="signIn hidden" style="display: none;">
            <fieldset>
                <p runat="server" id="hgcFindBookingErrMsg" class="error" visible="false" style="display:block" ></p>  
                <asp:Label ID="lblReservationNumber" runat="server" Text="Reservation Number" AssociatedControlID="txtReservationNumber"></asp:Label>
                <span class="error" runat="server" id="hgcReservationNumberError"></span>
                <asp:TextBox ID="txtReservationNumber" runat="server" ></asp:TextBox>
            </fieldset>
            <fieldset>
                <asp:Label ID="lblLastName" runat="server" Text="Last Name" AssociatedControlID="txtLastName"></asp:Label>
                <span class="error" runat="server" id="hgcLastNameError"></span>
                <asp:TextBox ID="txtLastName" runat="server" ></asp:TextBox>
            </fieldset>
            <p><asp:LinkButton ID="lbtnFindBooking" runat="server" CssClass="button blue hand submit"></asp:LinkButton></p>
        </div>
    </div>  
</asp:Content>
<asp:Content ContentPlaceHolderID="cphScript" ID="viewBookingsScripts" runat="server">
    <script src="<%= ResolveUrl("~/ScanwebMobile/Public/Scripts/ViewBookings.min.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>
</asp:Content>