﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using System.Web.Services;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Controller;

namespace Scandic.Scanweb.Mobile.Templates.FrequentGuestProgram
{
    public partial class MyBookings : VisualBasePage<MyBookingsPageSection>
    {
        private MyBookingsController pageController;
        protected string modifyCancelUrl;
        protected string lastName;
        private const string FUTURE_BOOKING_PAGE_SIZE = "futureBookingPageSize";
        private const string USER_HISTORY_PAGE_SIZE = "userHistoryPageSize";
        private const string HIDE_HISTORY_TEXT = "hideHistoryText";
        protected string futureBookingPageSize;
        protected string userHistoryPageSize;
        protected string hideHistory;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/FrequentGuestProgram/MyBookings.aspx";
            pageController = new MyBookingsController();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadControls();
            }
        }

        private void LoadControls()
        {
            var appConfig = pageController.GetGeneralConfig<ApplicationConfigSection>();
            var pageConfig = pageController.GetPageConfig<MyBookingsPageSection>();
            futureBookingPageSize = appConfig.GetMessage(FUTURE_BOOKING_PAGE_SIZE);
            userHistoryPageSize = appConfig.GetMessage(USER_HISTORY_PAGE_SIZE);
            modifyCancelUrl = pageController.GetPageUrl(MobilePages.ModifyCancel);
            lastName = pageController.LastName;
            hideHistory = pageConfig.PageDetail.PageMessages.GetMessage(HIDE_HISTORY_TEXT);
            //Following call is made to intialize the information to render in the langauge
            //selected by user.
            pageController.LoadPageData();
            this.Master.PageId = PageId();
        }

        public override MobilePages PageId()
        {
            return MobilePages.MyBookings;
        }

        #region WebMethod

        [WebMethod]
        public static UserReservations GetFutureReservationToDisplay(int currentlyDisplayedBookings,
                                            int noOfBookingsToDisplay, BookingSortType sortType,
                                            SortOrder sortOrder, bool fetchLatestBookings)
        {
            try
            {
                var controller = new MyBookingsController();
                return controller.GetFutureReservation(currentlyDisplayedBookings, noOfBookingsToDisplay,
                                        sortType, sortOrder, fetchLatestBookings);
            }
            catch (Exception ex)
            {
                string nameID = string.Empty;
                string membershipID = string.Empty;
                Scandic.Scanweb.BookingEngine.Controller.SessionManager sessionManager = new Scandic.Scanweb.BookingEngine.Controller.SessionManager();
                if (sessionManager.LoyaltyDetails != null)
                {
                    nameID = sessionManager.LoyaltyDetails.NameID;
                    membershipID = sessionManager.LoyaltyDetails.MembershipID;
                }
                string methodName = Reference.GETFUTURE_RESERVATION_TODISPLAY;
                ActionItem actionItem = null;
                actionItem = Utilities.GetActionItemForUserBookings(methodName, currentlyDisplayedBookings, noOfBookingsToDisplay, Convert.ToString(sortType), sortOrder, fetchLatestBookings, nameID, membershipID);
                UserNavTracker.TrackAction(actionItem, true);
                UserNavTracker.LogAndClearTrackedData(ex != null ? ex : new Exception("Generic-Error Mobile MyBookings GetFutureReservationToDisplay method"));
                throw ex;
            }
        }

        
        [WebMethod]
        public static UserTransactions GetUserHistoryToDisplay(int currentlyDisplayedTransactions,
                                            int noOfTransactionsToDisplay, TranscationSortType sortType,
                                            SortOrder sortOrder, bool fetchLatestTransactions)
        {
            try
            {
                var controller = new MyBookingsController();
                return controller.GetUserHistory(currentlyDisplayedTransactions, noOfTransactionsToDisplay,
                                        sortType, sortOrder, fetchLatestTransactions);
            }
            catch (Exception ex)
            {
                string nameID = string.Empty;
                string membershipID = string.Empty;
                Scandic.Scanweb.BookingEngine.Controller.SessionManager sessionManager = new Scandic.Scanweb.BookingEngine.Controller.SessionManager();
                if (sessionManager.LoyaltyDetails != null)
                {
                    nameID = sessionManager.LoyaltyDetails.NameID;
                    membershipID = sessionManager.LoyaltyDetails.MembershipID;
                }
                string methodName = Reference.GETUSER_HISTORY_TODISPLAY;
                ActionItem actionItem = null;
                actionItem = Utilities.GetActionItemForUserBookings(methodName, currentlyDisplayedTransactions, noOfTransactionsToDisplay, Convert.ToString(sortType), sortOrder, fetchLatestTransactions, nameID, membershipID);
                UserNavTracker.TrackAction(actionItem, true);
                UserNavTracker.LogAndClearTrackedData(ex != null ? ex : new Exception("Generic-Error Mobile MyBookings GetUserHistoryToDisplay method"));
                throw ex;
            }
        }
        #endregion
    }
}
