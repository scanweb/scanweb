﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Common.Interface;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;

namespace Scandic.Scanweb.Mobile.Templates.FrequentGuestProgram
{
    [AllowPublicAccess(true)]
    public partial class ViewBookings : VisualBasePage<ViewBookingsPageSection>
    {
        private ViewBookingsController pageController;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/FrequentGuestProgram/ViewBookings.aspx";
            this.lbtnFindBooking.Click += new EventHandler(lbtnFindBooking_Click);
            pageController = new ViewBookingsController();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            hgcFindBookingErrMsg.Visible = false;
            if (!IsPostBack)
            {
                LoadControls();
            }
            uctlLogin.ReturnUrl = pageController.GetPageUrl(MobilePages.MyBookings);
            //Logic for View/Modify reservatiopn deeplink for Mobile
            if (!string.IsNullOrEmpty(Request.QueryString[Reference.DEEPLINK_VIEWMODIFY_RESERVATIONID_QUERYSTRING])
                && !string.IsNullOrEmpty(Request.QueryString[Reference.DEEPLINK_VIEWMODIFY_LASTNAME_QUERYSTRING]))
            {
                txtReservationNumber.Text = Request.QueryString[Reference.DEEPLINK_VIEWMODIFY_RESERVATIONID_QUERYSTRING];
                txtLastName.Text = Request.QueryString[Reference.DEEPLINK_VIEWMODIFY_LASTNAME_QUERYSTRING];
                lbtnFindBooking_Click(null, null);
            }

        }

        void lbtnFindBooking_Click(object sender, EventArgs e)
        {
            var validationErrors = pageController.FindBooking(txtReservationNumber, txtLastName.Text.Trim());
            if (validationErrors != null && validationErrors.Count > 0)
            {
                RenderErrorMessages(validationErrors);
            }
        }

        private void LoadControls()
        {
            SetErrorMessages();
            if (pageController.IsAuthenticated)
            {
                hgcFGPLoginContainer.Visible = false;
                lnkMyBookings.NavigateUrl = pageController.GetPageUrl(MobilePages.MyBookings);
            }
            else
            {
                hgcMyBookings.Visible = false;
            }
            txtReservationNumber.Text = Request.QueryString[Reference.RESERVATION_ID_QUERY_STRING];
            txtLastName.Text = Request.QueryString[Reference.LAST_NAME_QUERY_STRING];
            hgcFindBookingErrMsg.InnerText = pageController.GetErrorMessage(Request.QueryString[Reference.QueryParameterErrorCode]);
            hgcFindBookingErrMsg.Visible = !string.IsNullOrEmpty(hgcFindBookingErrMsg.InnerText);
            this.Master.PageId = PageId();
        }

        private void SetErrorMessages()
        {
            //Render any error message to user that could be due to 
            //Note: Sometime Type association between MemberLogin user control
            //is broken from .designer.cs file. Please set it manually in it.
            RenderErrorMessages(uctlLogin.GetErrorMessages());
        }

        public override MobilePages PageId()
        {
            return MobilePages.ViewBookings;
        }

        public override LogonContext GetLogonContext()
        {
            return uctlLogin.GetLogonContext();
        }


    }
}
