﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="MyBookings.aspx.cs" Inherits="Scandic.Scanweb.Mobile.Templates.FrequentGuestProgram.MyBookings" MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>

<asp:Content ID="myBookingMain" ContentPlaceHolderID="cphMain" runat="server">
    <h3 id="hgcFutureStays" runat="server"></h3>
    <table id="futureStaysTable" class="info-table" style="margin-bottom: 20px; display:none">
        <thead>
            <tr>
                <th data-sortby="0" data-sortorder="0" onclick="SortFutureBookings(this);">
                    <asp:HyperLink ID="lnkFutureStayHotelName" runat="server" ></asp:HyperLink>   
                </th>
                <th data-sortby="1" data-sortorder="0" class="asc" onclick="SortFutureBookings(this);">
                    <asp:HyperLink ID="lnkFutureStayCheckInDate" runat="server" ></asp:HyperLink>   
                </th>
                <th data-sortby="2" data-sortorder="0" onclick="SortFutureBookings(this);">
                    <asp:HyperLink ID="lnkFutureStayCheckOutDate" runat="server" ></asp:HyperLink>   
                </th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <p class="text-right cf">
        <asp:HyperLink ID="lnkFutureStayViewMore" runat="server" CssClass="button green" style="margin-bottom: 1em; display:none" ></asp:HyperLink>        
    </p>
    <div id="historyContainer"  style="display: none;">
        <h3 id="hgcHistory" runat="server"></h3>
        <table id="historyTable" class="info-table" style="margin-bottom: 20px; display:none;">
            <thead>
                <tr>
                    <th data-sortby="0" data-sortorder="0" onclick="SortHistory(this);">
                        <asp:HyperLink ID="lnkTransaction" runat="server" ></asp:HyperLink>   
                    </th>
                    <th data-sortby="1" data-sortorder="1" onclick="SortHistory(this);" class="desc">
                        <asp:HyperLink ID="lnkDate" runat="server" ></asp:HyperLink>   
                    </th>
                    <th data-sortby="2" data-sortorder="0" onclick="SortHistory(this);">
                        <asp:HyperLink ID="lnkPoints" runat="server" ></asp:HyperLink>   
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <p class="text-right cf">
            <asp:HyperLink ID="lnkHistoryViewMore" runat="server" CssClass="button green" style="margin-bottom: 1em; display:none" ></asp:HyperLink>        
        </p>
    </div>
    <p class="center">
        <asp:HyperLink ID="lnkViewHistory" runat="server"  CssClass="button green" data-target="historyContainer" style="display:none"></asp:HyperLink>
    </p>
    <div id="dvTemplateTable" style="display:none">
        <table>
            <tr class="templateRow">
                <td> <a class="hotelName" href="#" ></a> </td>
                <td> <a class="checkInDate" href="#" ></a> </td>
                <td> <a class="checkOutDate" href="#" ></a> </td>
            </tr>
            <tr class="historyRow">
                <td class="transaction"> </td>
                <td class="date"> </td>
                <td class="points"> </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="myBookingScript" ContentPlaceHolderID="cphScript" runat="server">
    <script type="text/javascript" >
        var noOfFutureStaysToDisplay = "<%=futureBookingPageSize %>";
        var noOfTransactionsToDisplay = "<%=userHistoryPageSize %>";
        var modifyCancelUrl = "<%=modifyCancelUrl %>";
        var hideHistory = "<%=hideHistory %>";
        var lastName = "<%=lastName %>";
    </script> 
    <script src="<%= ResolveUrl("~/ScanwebMobile/Public/Scripts/MyBookings.min.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>    
</asp:Content>