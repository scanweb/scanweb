﻿using System;
using System.Web.UI.WebControls;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using System.Web.UI;
using System.Collections.Generic;
using Scandic.Scanweb.Mobile.UI.Entity;
using System.Web.Services;

namespace Scandic.Scanweb.Mobile.Templates
{
    /// <summary>
    /// Start
    /// </summary>
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionExpired(true)]
    public partial class Start : VisualBasePage<StartPageSection>
    {
        private StartController pageController;
        private IBookingRepository bookingRepository;
        protected string BookMarkingScript;
        protected string BookMarkOverlayText;

        /// <summary>
        /// OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            string deeplinkPagePath = string.Empty;
            var deeplinkRepository = DependencyResolver.Instance.GetService(typeof(IDeeplinkRepository)) as IDeeplinkRepository;
            pageController = new StartController();
            if (pageController.CurrentContext != null)
                pageController.ClearContext();
            bookingRepository = DependencyResolver.Instance.GetService(typeof(IBookingRepository)) as IBookingRepository;
            if (deeplinkRepository.IsDeeplinkRequest(Request, out deeplinkPagePath))
            {
                pageController.Redirect(deeplinkPagePath);
            }

            this.Load += new EventHandler(Page_Load);
            this.menuItemsRepeater.ItemDataBound += new RepeaterItemEventHandler(Repeater_ItemDataBound);
            this.menuItemsRepeater.ItemCommand += new RepeaterCommandEventHandler(Repeater_ItemCommand);
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/Start.aspx";
        }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var pageGenConfig = pageController.GetGeneralConfig<ApplicationConfigSection>();
            var bookMarkData = pageGenConfig.GetMessage(Reference.BookMarkOverlayKey);
            if (bookMarkData == Reference.SHOW)
                BookMarkingScript = "/ScanwebMobile/Public/Scripts/add2home.js";
            if (!this.IsPostBack)
            {
                if (!IsMobileApplication())
                {
                    LoadPageData();
                }
            }
            var pageConfig = pageController.GetPageConfig<StartPageSection>();

            var siteInfoRepository = DependencyResolver.Instance.GetService(typeof(ISiteInfoRepository)) as ISiteInfoRepository;
            PageData CurrentPage = siteInfoRepository.GetCMSPageData(Reference.START_PAGE_CMS_REFERENCE);
            this.Master.MetaDescription = CurrentPage[Reference.MetaDescriptionId] as string ??
                                          pageConfig.PageDetail.PageMessages.GetMessage(Reference.MetaDescriptionId);
            if (!string.IsNullOrEmpty(CurrentPage[Reference.MetaKeywordsID] as string))
            {
                Master.CreateMetaTagMobile(Reference.MetaKeywordsID, CurrentPage[Reference.MetaKeywordsID] as string);
            }


            if (bookMarkData == Reference.SHOW)
                BookMarkOverlayText = pageConfig.PageDetail.PageMessages.GetMessage(Reference.BookMarkOverlayText);

            Control cntrl = new Control();
            carouselPlaceHolder.Controls.Add(cntrl = LoadControl("/ScanwebMobile/Templates/Controls/ImageCarousel.ascx"));
        }

        [WebMethod]
        public static List<ImageCarousel> GetCarouselData()
        {
            var startController = new StartController();
            try
            {
                return startController.LoadCarousalOffersData();
            }
            catch (Exception ex)
            {
                ActionItem action = Utilities.GetActionItemToTrack(startController.CurrentContext);
                Utilities.TrackActionParametersAndLogData("Mobile:Start", "GetCalenderData", ex, action, true);
                throw ex;
            }
        }

        /// <summary>
        /// IsMobileApplication
        /// </summary>
        /// <returns>True/False</returns>
        private bool IsMobileApplication()
        {
            var isIntializationReq = false;
            if (string.Equals(this.Request.Form["process"], "InitializeApp"))
            {
                isIntializationReq = true;
                var intializer = new MobileAppInitializer();
                intializer.IntializeApp();
            }
            return isIntializationReq;
        }

        /// <summary>
        /// LoadPageData
        /// </summary>
        private void LoadPageData()
        {
            var pageModel = pageController.GetPageData();
            this.menuItemsRepeater.DataSource = pageModel.ContextMenu.Items;
            this.menuItemsRepeater.DataBind();
            this.Master.PageId = PageId();

        }


        /// <summary>
        /// Repeater_ItemCommand
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Repeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "BookingProcess":
                    pageController.StartBookingProcess(e.CommandArgument.ToString());
                    break;
                case "ModifyCancelBooking":
                    pageController.ModifyCancelProcess();
                    break;
                case "AllOffers":
                    pageController.MobileOffer();
                    break;
                default:
                    break;
            }
        }


        /// <summary>
        /// Repeater_ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var data = e.Item.DataItem as UI.Entity.MenuItem;
                var menulink = e.Item.FindControl("menuLink") as HyperLink;
                var menulinkButton = e.Item.FindControl("menuLinkButton") as LinkButton;

                var commmandAttr = data.GetAttribute("commandname");
                if (commmandAttr != null)
                {
                    menulinkButton.Text = data.Name;
                    menulinkButton.CommandName = commmandAttr.Value;
                    var commmandArgumentAttr = data.GetAttribute("commmandArgument");
                    if (commmandArgumentAttr != null)
                    {
                        menulinkButton.CommandArgument = commmandArgumentAttr.Value;
                    }
                    menulinkButton.Visible = true;
                    Utilities.AddAttributes(menulinkButton, data.Attributes);
                }
                else
                {
                    menulink.Text = data.Name;
                    menulink.NavigateUrl = data.Path;
                    menulink.Visible = true;
                    Utilities.AddAttributes(menulink, data.Attributes);
                    //var attribute = data.GetAttribute("loadFeedbackPath");
                    //if
                    //    (attribute != null &&
                    //     string.Equals(attribute.Value, "true", StringComparison.InvariantCultureIgnoreCase))
                    //{
                    //    menulink.NavigateUrl =
                    //        string.Format
                    //            ("{0}?pageId={1}", pageController.GetPageUrl(MobilePages.Feedback), Reference.FEEDBACK);
                    //}
                    //Utilities.AddAttributes(menulink, data.Attributes);
                }
            }
        }

        /// <summary>
        /// PageId
        /// </summary>
        /// <returns>MobilePages</returns>
        public override MobilePages PageId()
        {
            return MobilePages.Start;
        }
    }
}