﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using EPiServer.Core;
using EPiServer.SpecializedProperties;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Common;

namespace Scandic.Scanweb.Mobile.Templates.Controls
{
    public partial class OffersListing : System.Web.UI.UserControl
    {
        #region Properties
        /// <summary>
        /// Gets/Sets OfferCategoryPage
        /// </summary>
        public PageData OfferCategoryPage { get; set; }
        public string OfferCategories { get; set; }
        private OffersController controller;
        private int imageWidth;
        private const string PROMO_BOX_PRICE_TEXT = "PromoBoxPriceText";
        private const string BOX_CONTENT = "BoxContent";
        private const string OFFER_CATEGORY_MAPPER = "OfferCategoryMapper";

        #endregion

        #region Protected Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            imageWidth = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.MOBILE_OFFER_IMAGE_WIDTH]);
            if (!IsPostBack)
            {
                LoadControls();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            this.OffersRepeater.ItemDataBound += new RepeaterItemEventHandler(Repeater_ItemDataBound);
            controller = new OffersController();
        }



        protected void Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var pageData = e.Item.DataItem as PageData;

                if (pageData != null)
                {
                    OfferCategories = Convert.ToString(OfferCategoryPage.PageLink.ID);
                    LinkItemCollection categoriesCollection = pageData[OFFER_CATEGORY_MAPPER] as LinkItemCollection;

                    if (categoriesCollection != null && categoriesCollection.Count > 0)
                    {
                        for (int i = 0; i < categoriesCollection.Count; i++)
                        {
                            NameValueCollection categoryCollection = HttpUtility.ParseQueryString(categoriesCollection[i].GetMappedHref());
                            if (categoryCollection != null && categoryCollection.Count > 0)
                                for (int j = 0; j < categoryCollection.Count; j++)
                                {
                                    if (categoryCollection.Keys[j].Contains("id"))
                                        OfferCategories += "," + categoryCollection[j];
                                }
                        }
                    }
                    var squaredImage = e.Item.FindControl("SquaredImage") as System.Web.UI.HtmlControls.HtmlImage;
                    if (squaredImage != null)
                    {
                        if (pageData[Reference.MOBILE_OFFER_IMAGE] != null)
                            squaredImage.Src = WebUtil.GetImageVaultImageUrl(Convert.ToString(pageData[Reference.MOBILE_OFFER_IMAGE]), imageWidth);
                        else
                            if (pageData[Reference.DESKTOP_OFFER_IMAGE] != null)
                                squaredImage.Src = WebUtil.GetImageVaultImageUrl(Convert.ToString(pageData[Reference.DESKTOP_OFFER_IMAGE]), imageWidth);
                        squaredImage.Alt = Convert.ToString(pageData[Reference.BOX_HEADING]);
                    }
                    var categoryListing = e.Item.FindControl("OffersListing") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (categoryListing != null)
                        categoryListing.Attributes.Add("data-id", OfferCategories);

                    var offerTitle = e.Item.FindControl("offerTitle") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (offerTitle != null)
                        offerTitle.InnerHtml = Regex.Replace(Convert.ToString(pageData[Reference.BOX_HEADING]), "<.*?>", string.Empty);

                    var offerDescription = e.Item.FindControl("offerDescription") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (offerDescription != null)
                        offerDescription.InnerHtml = Regex.Replace(GetOfferDescription(pageData), "<.*?>", string.Empty);

                    var priceText = e.Item.FindControl("priceText") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (priceText != null)
                        priceText.InnerHtml = Regex.Replace(GetPriceText(pageData), "<.*?>", string.Empty);

                    var offerCategoryListing = e.Item.FindControl("OffersListing") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (offerCategoryListing != null)
                    {
                        var selectedOfferMethod = string.Format("loadSelectedOffer(this,'{0}');", controller.GetOfferURL(Convert.ToString(pageData.PageLink.ID)));
                        offerCategoryListing.Attributes.Add("onclick", selectedOfferMethod);
                    }
                        
                }
            }
        }
        #endregion

        #region Private Methods
        private void LoadControls()
        {
            PageDataCollection newPageCollection = controller.GetOfferCategoryPageCollection(OfferCategoryPage);
            if (newPageCollection != null && newPageCollection.Count > 0)
            {
                OffersRepeater.DataSource = newPageCollection;
                OffersRepeater.DataBind();
            }
        }

        /// <summary>
        /// Gets price text
        /// </summary>
        /// <param name="offerPage"></param>
        /// <returns></returns>
        private string GetPriceText(PageData offerPage)
        {
            string returnString = string.Empty;
            if (offerPage[PROMO_BOX_PRICE_TEXT] != null)
            {
                returnString = offerPage[PROMO_BOX_PRICE_TEXT] as string;
            }
            return returnString;
        }

        /// <summary>
        /// Gets offer description text
        /// </summary>
        /// <param name="offerPage"></param>
        /// <returns></returns>
        private string GetOfferDescription(PageData offerPage)
        {
            string returnString = string.Empty;
            if (offerPage[BOX_CONTENT] != null)
                returnString = offerPage[BOX_CONTENT] as string;
            return returnString;
        }
        #endregion
    }
}