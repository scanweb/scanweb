﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageCarousel.ascx.cs"
    Inherits="Scandic.Scanweb.Mobile.Templates.Controls.ImageCarousel" %>
<%@ Register Assembly="Scandic.Scanweb.Mobile" Namespace="Scandic.Scanweb.Mobile.UI.Controls"
    TagPrefix="Mobile" %>
<div id="mySwipe" class="swipe">
  <div class="swipe-wrap">
  </div>
  
  <div class="slider-left" onclick="mySwipe.prev()"></div>
  <div class="slider-leftarrow" onclick="mySwipe.prev()"></div>
  <div class="slider-right" onclick="mySwipe.next()"></div>
  <div class="slider-rightarrow" onclick="mySwipe.next()"></div>
	<div class="carousel-indicators">
		<ul id="position">
		</ul>
	</div>
</div>

<div style="display: none" class="slideTemplate">
    <div class="slide"><a href=""><div class="title"><h3></h3></div><img src=""/></a></div>
</div>