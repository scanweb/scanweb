﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Scandic.Scanweb.Mobile.UI.Content.Controller.Hotel;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Mobile.UI.Entity.Content.Hotel;
using EPiServer.Core;
using System.Globalization;
using System.Collections.Generic;
using Scandic.Scanweb.CMS.code.Util.Map;

namespace Scandic.Scanweb.Mobile.Templates.Controls
{
    public partial class HotelInfoSelectRate : System.Web.UI.UserControl
    {
        protected string overviewText;
        protected string locationText;
        protected string offersText;
        protected string moreDetailsText;
        protected string rewardNightText;
        private const string OVERVIEW_TEXT = "OverviewText";
        private const string LOCATION_TEXT = "LocationText";
        private const string OFFERS_TEXT = "OffersText";
        private const string MORE_DETAILS_TEXT = "MoreDetailsText";
        private const string REWARD_NIGHT_TEXT = "RewardNightText";
        private const string HOTEL_PHONE = "HotelTelephoneNumber";
        private const string RING_US_TEXT = "RingUsText";
        private HotelSelectRateController hotelSelectRateController;
        protected string noOfRoomsCaption;
        protected string nonSmokingRoomsCaption;
        protected string roomsForDisabledCaption;
        protected string relaxCenterCaption;
        protected string restaurantBarCaption;
        protected string garageCaption;
        protected string outdoorParkingCaption;
        protected string shopCaption;
        protected string meetingFacilitiesCaption;
        protected string ecoLabeledCaption;
        protected string cityCenterDistanceCaption;
        protected string airport1NameCaption;
        //protected string airport1DistanceCaption;
        protected string distanceTrainCaption;
        protected string wirelessInternetCaption;
        protected string telephoneNumber = "";
        protected string telephoneNumberLinkText = "";
        protected string mapCaption;

        /// <summary>
        /// Oninit event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            hotelSelectRateController = new HotelSelectRateController();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                overviewText = hotelSelectRateController.GetPageConfig<SelectRatePageSection>().PageDetail.PageMessages.GetMessage(OVERVIEW_TEXT);
                locationText = hotelSelectRateController.GetPageConfig<SelectRatePageSection>().PageDetail.PageMessages.GetMessage(LOCATION_TEXT);
                offersText = hotelSelectRateController.GetPageConfig<SelectRatePageSection>().PageDetail.PageMessages.GetMessage(OFFERS_TEXT);
                moreDetailsText = hotelSelectRateController.GetPageConfig<SelectRatePageSection>().PageDetail.PageMessages.GetMessage(MORE_DETAILS_TEXT);
                rewardNightText = hotelSelectRateController.GetPageConfig<SelectRatePageSection>().PageDetail.PageMessages.GetMessage(REWARD_NIGHT_TEXT);
                telephoneNumber = string.Format("tel:{0}", hotelSelectRateController.GetPageConfig<SelectRatePageSection>().PageDetail.PageMessages.GetMessage(HOTEL_PHONE));
                telephoneNumberLinkText = hotelSelectRateController.GetPageConfig<SelectRatePageSection>().PageDetail.PageMessages.GetMessage(RING_US_TEXT);
                SetAdditionalInfoCaptions();
                LoadMap();
                DataBind();
            }
        }

        private void LoadMap()
        {
            MapUnit hotelmap = hotelSelectRateController.LoadHotelMap(Request.Url.Host);
            if (hotelmap != null)
            {
                hotelMap.Visible = true;
                CultureInfo ci = new System.Globalization.CultureInfo("en-US");
                GMapControl1.GoogleMapKey = (ConfigurationManager.AppSettings["googlemaps." + Request.Url.Host] as string ?? (ConfigurationManager.AppSettings["DevKey"] as string));
                IList<MapUnit> hotels = new List<MapUnit>();
                hotels.Add(hotelmap);
                GMapControl1.DataSource = hotels;
                GMapControl1.MarkerLatitudeField = "latitude";
                GMapControl1.MarkerLongitudeField = "longitude";
                GMapControl1.MapPageType = MapPageType.HOTELLOCATION;
                GMapControl1.EnablePanControl = false;
                //GMapControl1.AutoCenterAndZoom();
                GMapControl1.CenterAndZoomLevel = CenterAndZoomLevel.HOTEL;
                GMapControl1.Latitude = hotels[0].Latitude;
                GMapControl1.Longitude = hotels[0].Longitude;
                GMapControl1.CenterAndZoom(new MapPoint(hotels[0].Latitude, hotels[0].Longitude), 16);
            }
            else
                hotelMap.Visible = false;
        }

        private void SetAdditionalInfoCaptions()
        {
            noOfRoomsCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/NoOfRooms");
            nonSmokingRoomsCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/NonSmokingRooms");
            roomsForDisabledCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RoomsForDisabled");
            relaxCenterCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RelaxCenter");
            restaurantBarCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RestaurantBar");
            garageCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Garage");
            outdoorParkingCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/OutdoorParking");
            shopCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Shop");
            meetingFacilitiesCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/MeetingFacilities");
            cityCenterDistanceCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceCentre");
            ecoLabeledCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/EcoLabeled");
            airport1NameCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceAirport");
            //airport1DistanceCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceAirport");
            distanceTrainCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceTrain");
            wirelessInternetCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/WirelessInternet");
            mapCaption = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Map");
        }

    }
}