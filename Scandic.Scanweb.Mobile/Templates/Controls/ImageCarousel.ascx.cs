﻿using System;
using System.Collections.Generic;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using CarousalEntity = Scandic.Scanweb.Mobile.UI.Entity;
using System.Web.UI;
namespace Scandic.Scanweb.Mobile.Templates.Controls
{
    public partial class ImageCarousel : System.Web.UI.UserControl
    {
        /// <summary>
        /// OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
    }
}