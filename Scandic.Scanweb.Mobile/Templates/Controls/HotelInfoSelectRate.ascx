﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="HotelInfoSelectRate.ascx.cs"
    Inherits="Scandic.Scanweb.Mobile.Templates.Controls.HotelInfoSelectRate" %>
<%@ Register Src="/ScanwebMobile/Templates/Controls/ImageCarousel.ascx" TagName="SelectRateCarousel"
    TagPrefix="uc1" %>
<%@ Register Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3"
    TagPrefix="cc1" %>
<div class="hotel-info">
    <h2 class="sel-hotel-name">
    </h2>
    <uc1:SelectRateCarousel id="SelectRateImageCarousel" runat="server" />
    <!--End of Hotel Image -->
    <div class="hotel-info-tabs">
        <ul class='tabs'>
            <li><a href='#tab1'>
                <%=overviewText %></a></li>
            <li><a href='#tab2'>
                <%=locationText %></a></li>
            <li><a href='#tab3'>
                <%=offersText %></a></li>
        </ul>
        <div id='tab1'>
            <p class="hotel-desc-txt">
            </p>
            <span class="more-details"><a href="#">
                <%=moreDetailsText %>&nbsp;<span class="more-details-arrow">&#9654;</span></a></span>
            <div class="more-details-grid">
                <ul class="two-col-grid">
                    <li id="noofRooms">
                        <%=noOfRoomsCaption%></li>
                    <li id="noofRoomsValue"></li>
                    <li id="nonSmokingRooms">
                        <%=nonSmokingRoomsCaption %></li>
                    <li id="nonSmokingRoomsValue"></li>
                    <li id="roomsForDisabled">
                        <%=roomsForDisabledCaption %></li>
                    <li id="roomsForDisabledValue"></li>
                    <li id="relaxCenter">
                        <%=relaxCenterCaption %></li>
                    <li id="relaxCenterValue"></li>
                    <li id="restaurantBar">
                        <%=restaurantBarCaption %></li>
                    <li id="restaurantBarValue"></li>
                    <li id="garage">
                        <%=garageCaption %></li>
                    <li id="garageValue"></li>
                    <li id="outdoorParking">
                        <%=outdoorParkingCaption %></li>
                    <li id="outdoorParkingValue"></li>
                    <li id="shop">
                        <%=shopCaption %></li>
                    <li id="shopValue"></li>
                    <li id="meetingFacilities">
                        <%=meetingFacilitiesCaption %></li>
                    <li id="meetingFacilitiesValue"></li>
                    <li id="EcoLabeled">
                        <%=ecoLabeledCaption %></li>
                    <li id="EcoLabeledValue"></li>
                    <li id="CityCenterDistance">
                        <%=cityCenterDistanceCaption %></li>
                    <li id="CityCenterDistanceValue"></li>
                    <li id="Airport1Name">
                        <%=airport1NameCaption %></li>
                    <%--<li id="Airport1NameValue"></li>--%>
                    <li id="Airport1DistanceValue"></li>
                    <li id="DistanceTrain">
                        <%=distanceTrainCaption %></li>
                    <li id="DistanceTrainValue"></li>
                    <li id="WirelessInternet">
                        <%=wirelessInternetCaption%></li>
                    <li id="WirelessInternetValue"></li>
                </ul>
            </div>
            <!--Start of Rewards Night -->
            <div class="reward-nights">
            </div>
            <!--End of Rewards Night -->
            <div class="trip-advisor-wrapper">
                <div id="divTripAdvisorRating">
                    <p class="tripAdvisorImage">
                        <img src="" /></p>
                    <p>
                        <a href="" class="tripReviewcount" onclick="trackFunction(this, 10);displayIframe(this.href);return false;">
                        </a>
                    </p>
                </div>
            </div>
            <div class="horizontal-rule-short">
            </div>
        </div>
        <!-- End of Tab 1 -->
        <div id='tab2'>
            <div class="sel-hotel-addr">
                <div class="addr-pin">
                </div>
                <div class="addr-details">
                    <span class="hotel-address"><a class="address new-window" href="#" target="_blank"
                        onclick="return trackFunction(this, 4);"><span class="street-address"></span></a>
                    </span>&nbsp<span id="hgcViewOnMap" runat="server"></span> <span class="hotel-distance">
                    </span>
                </div>
            </div>
            <div class="call-reservation">
                <a onclick="return trackFunction(this, 2);" class="icon click2call phone" href="<%= telephoneNumber %>"
                    data-hotelid="">
                    <%= telephoneNumberLinkText %></a>
            </div>
            <div class="map-wrapper" id="hotelMap" runat="server">
                <div class="bottomGoogleMapContainer">
                    <div class="borderContainermap">
                        <div class="darkHeading">
                            <%= mapCaption %>
                        </div>
                        <div class="middle">
                            <div class="map">
                                <div id="GMapV3" style="height: 230px; width: 100%;">
                                    <cc1:Map ID="GMapControl1" Width="226" Height="230" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="bottom">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Tab 2 -->
        <div id='tab3'>
        </div>
        <!-- End of Tab 3 -->
    </div>
    <!--End of Tabs -->
    <!--<div class="reward-night-wrapper1" style="display: none">
        <li>
            <div class="hotel-info-reward-night left">
                <span class="reward-points-txt">
                    <%=rewardNightText %>
                </span><span class="reward-points-value"></span><span>
                    </span> <span class="reward-dates"></span>
            </div>
        </li>
    </div>-->
    <div class="reward-night-wrapper-template" style="display: none">
        <div class="hotel-info-reward-night multiple-reward-points left">
            <span class="reward-points-txt">
                <%=rewardNightText %></span> <span class="reward-points-value"></span>
            <ul class="reward-points-list">
            </ul>
        </div>
    </div>
    <div class="offers-wrapper" style="display: none">
        <div class="offer-box">
            <a>
                <div class="offer-content">
                    <div class="offer-title">
                    </div>
                    <div class="offer-img">
                        <img id="SquaredImage" src="" alt="" />
                    </div>
                    <div class="offer-desc">
                        <span class="ellipsis_text"></span>
                    </div>
                    <div class="offer-spl">
                    </div>
                </div>
                <div class="offer-arrow">
                </div>
                <div class="offer-divider">
                </div>
            </a>
        </div>
        <div class="noOfferMessage">
        </div>
    </div>
</div>
