﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EPiServer.Core;
using EPiServer;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Booking.Business;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.CMS.DataAccessLayer;
using EPiServer.Globalization;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using System.Text.RegularExpressions;

namespace Scandic.Scanweb.Mobile.Templates.Controls
{
    public partial class MobileCookieLaw : System.Web.UI.UserControl
    {
        /// <summary>
        /// Page load event hadndler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            agreeCookie.InnerHtml = EPiServer.Core.LanguageManager.Instance.Translate("/bookingengine/booking/ManageCreditCard/CCSavedOK");
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage);
            PageReference pageLink = rootPage[Reference.START_PAGE_CMS_REFERENCE] as PageReference;
            PageData page = ContentDataAccess.GetPageData(pageLink.ID, CMSSessionWrapper.CurrentLanguage);
            if (page.Property["CookieLawMessage"] != null)
            {
                cookieLawMessageDiv.InnerHtml = Convert.ToString(page.Property["CookieLawMessage"]).Trim();
                string cookieMessage = HttpUtility.HtmlDecode(cookieLawMessageDiv.InnerText);
                if (!IsNullOrWhiteSpace(cookieMessage))
                    cookieLawMessageDiv.InnerHtml = cookieMessage;
                else
                {
                    cookieLawMessageDiv.InnerHtml = string.Empty;
                    CookieWrapper.Visible = false;
                }
            }
            if (!Convert.ToBoolean(page.Property["IsCookieAllowed"].Value))
            {

                if (Request.Cookies.Get("cookieLaw") == null)
                {
                    HttpCookie displayCookie = new HttpCookie("cookieLaw", "false");
                    Response.Cookies.Add(displayCookie);
                }
            }
        }
     
        private bool IsNullOrWhiteSpace(string value)
        {
            return String.IsNullOrEmpty(value) || value.Trim().Length == 0;
        }
    }
}