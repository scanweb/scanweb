﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;

namespace Scandic.Scanweb.Mobile.Templates.Controls
{
    public partial class MemberLogin : System.Web.UI.UserControl
    {
        #region Declaration

        private LoginController pageController;
        private const string ID_ERROR_MSG = "membershipIdRequiredMsg";
        private const string ID_ERROR_CTRL = "hgcMembershipIdError";
        private const string PASS_ERROR_MSG = "passwordRequiredMsg";
        private const string PASS_ERROR_CTRL = "hgcPasswordError";
        private const string INVALID_ID_PASS_MSG = "idPasswordInvalidMsg";
        private const string INVALID_ID_PASS_CTRL = "hgcInValidIdPassError";

        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            pageController = new LoginController();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadControls();
            } 
        }

        private void LoadControls()
        {
            var appConfig = pageController.GetGeneralConfig<ApplicationConfigSection>();
			//REEP START | Artifact artf1284600 : Mobile | All static urls need to be fixed 
            var pageConfig = pageController.GetPageConfig<LoginPageSection>();
            //REEP END | Artifact artf1284600 : Mobile | All static urls need to be fixed 
            bool rememberMe = false;
            var serverHost = pageController.GetServerHostName(Request);
            var processingUrl = Utilities.ConvertToSecureProtocol(string.Format("{0}{1}", serverHost, appConfig.GetMessage(Reference.LoginProcessingPath)));

            lbtnSignIn.PostBackUrl = processingUrl;
            txtMembershipId.Text = pageController.GetMembershipId(out rememberMe);
            if (rememberMe)
            {
                txtPassword.Attributes["value"] = pageController.GetPassword(txtMembershipId.Text);
            }
            chkRememberMe.Checked = true;
            var returnUrl = Request.QueryString[Reference.ReturnUrlQueryString];
            if (!string.IsNullOrEmpty(returnUrl)) {
                ReturnUrl = returnUrl;
            }
			//REEP START | Artifact artf1284600 : Mobile | All static urls need to be fixed 
            var forgetPassURL = (pageConfig.PageDetail.Inputs
                                                      .Where(input => string.Equals(input.Id, "lnkForgotPassword", StringComparison.InvariantCultureIgnoreCase))
                                                      .Select(input => input.path)
                                                    ).FirstOrDefault();
            lnkForgotPassword.NavigateUrl = Utilities.ConvertToSecureProtocol(string.Format("{0}{1}", serverHost, forgetPassURL));

            //REEP START | Artifact artf1284600 : Mobile | All static urls need to be fixed 
            
        }

        public List<KeyValueOption> GetErrorMessages()
        {
            var pageConfig = pageController.GetPageConfig<LoginPageSection>();
            List<KeyValueOption> validationErrors = new List<KeyValueOption>();
            KeyValueOption error;

            if (string.Equals(Request.QueryString["Id"], "1"))
            {
                error = new KeyValueOption();
                error.Key = ID_ERROR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(ID_ERROR_MSG);
                validationErrors.Add(error);
            }

            if (string.Equals(Request.QueryString["Pass"], "1"))
            {
                error = new KeyValueOption();
                error.Key = PASS_ERROR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(PASS_ERROR_MSG);
                validationErrors.Add(error);
            }

            if (string.Equals(Request.QueryString["Invalid"], "1"))
            {
                error = new KeyValueOption();
                error.Key = INVALID_ID_PASS_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(INVALID_ID_PASS_MSG);
                validationErrors.Add(error);
            }

            return validationErrors;
        }

        public LogonContext GetLogonContext()
        {
            var logonContext = new LogonContext();

            logonContext.Id = this.txtMembershipId.Text;
            logonContext.Password = this.txtPassword.Text;
            logonContext.RememberMe = this.chkRememberMe.Checked;
            logonContext.ReturnUrl = this.hidReturnUrl.Value;

            return logonContext;
        }

        public string ReturnUrl
        {
            get { return hidReturnUrl.Value; }
            set { hidReturnUrl.Value = value; }
        }

    }
}