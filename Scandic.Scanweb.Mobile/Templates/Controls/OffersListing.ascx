﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OffersListing.ascx.cs"
    Inherits="Scandic.Scanweb.Mobile.Templates.Controls.OffersListing" %>
<div class="offers-wrapper">
    <asp:Repeater ID="OffersRepeater" runat="server">
        <ItemTemplate>
            <div class="offer-box" id="OffersListing" runat="server">
                
                <div class="offer-content">
                    <div class="offer-title" id="offerTitle" runat="server">
                    </div>
                    <div class="offer-img">
                    <img id="SquaredImage" runat="server" src="" alt="" />
                </div>
                    <div class="offer-desc">
                        <span class="ellipsis_text" id="offerDescription" runat="server"></span>
                    </div>
                    <div class="offer-spl" id="priceText" runat="server">
                    </div>
                </div>
                <div class="offer-arrow">
                </div>
                <div class="offer-divider"></div>
            </div>
            
        </ItemTemplate>
    </asp:Repeater>
</div>
