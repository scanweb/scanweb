﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MemberLogin.ascx.cs" Inherits="Scandic.Scanweb.Mobile.Templates.Controls.MemberLogin" %>
<%@ Register Assembly="Scandic.Scanweb.Mobile" Namespace="Scandic.Scanweb.Mobile.UI.Controls"  TagPrefix="Mobile" %>



    <fieldset>
        <asp:Label ID="lblMembershipId" runat="server" Text="Membership id" AssociatedControlID="txtMembershipId"></asp:Label>
        <a href="#" data-target="overlay-membershipid" class="icon tooltip" data-overlaytype="tooltip" data-associatedcontrol="lblMembershipId" onclick="LoadOverlay(this); return false;">&nbsp;</a>
        <span class="error" runat="server" id="hgcMembershipIdError"></span>            
        <Mobile:TelTextBox Id="txtMembershipId" runat="server"></Mobile:TelTextBox>
    </fieldset>
    <fieldset>
        <asp:Label ID="lblPassword" runat="server" Text="Password" AssociatedControlID="txtPassword"></asp:Label>
        <span class="error" runat="server" id="hgcPasswordError"></span>
        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
    </fieldset>
    <fieldset>
        <asp:CheckBox ID="chkRememberMe" runat="server"  />
        <asp:Label ID="lblRememberMe" runat="server" Text="Remember me" AssociatedControlID="chkRememberMe"></asp:Label>
    </fieldset>
    <span class="error" runat="server" id="hgcInValidIdPassError"></span>
    <p><asp:HyperLink ID="lnkForgotPassword" runat="server"  NavigateUrl="#">forgot password</asp:HyperLink></p>
    <p><asp:LinkButton ID="lbtnSignIn" runat="server" CssClass="button blue hand submit"></asp:LinkButton></p>
    <asp:HiddenField ID="hidReturnUrl" runat="server" />
    
<script src="/ScanwebMobile/Public/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
   
        $("input[id$='txtMembershipId']").focusout(function(){
            var memberid = $("input[id$='txtMembershipId']").val();
            memberid = memberid.replace(/ /g, '');
            $("input[id$='txtMembershipId']").val(memberid);

            //remove additional prefix if any.
            var text = $("input[id$='txtMembershipId']").val();
			 if(text.length > 14){
            text = text.replace(/308123((308123)+)/, "$1");}
            $("input[id$='txtMembershipId']").val(text);  
 
        });
   
    
</script>
    