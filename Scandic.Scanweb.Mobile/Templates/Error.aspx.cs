﻿//  Description					:   Error                                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.Mobile.Templates
{
    /// <summary>
    /// Code behind of error page.
    /// </summary>
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionExpired(true)]
    public partial class Error : VisualBasePage<ErrorPageSection>
    {
        private ErrorController pageController;

        /// <summary>
        /// Oninit event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!string.IsNullOrEmpty(Request.QueryString["lang"]))
            {
                LanguageRedirectionHelper.SetCurrentLanguage(Request.QueryString["lang"]);
            }
            this.Load += new EventHandler(Page_Load);
            pageController = new ErrorController();
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/Error.aspx";
        }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                SetErrorInfo();
                this.lnkHome.NavigateUrl = pageController.GetPageUrl(MobilePages.Start);
            }
        }

        /// <summary>
        /// Sets error info
        /// </summary>
        private void SetErrorInfo()
        {
            bool isErrorFromConfPage = false;
            var errorCode = Request.QueryString[Reference.QueryParameterErrorCode];
            string errorMessage = string.Empty;
            string errorMessageId = string.Empty;
            var pageConfig = pageController.GetPageConfig<ErrorPageSection>();
            switch (errorCode)
            {
                case "600":
                    errorMessageId = "sessionTimeOutMessage";
                    this.Master.PageHeading = pageConfig.PageDetail.PageMessages.GetMessage("sessionTimeHeading");
                    trackingData.Value = "SessionTimeOut";
                    break;
                case "404":
                    errorMessageId = "pageNotFoundMessage";
                    break;
                case "SessionNotValid":
                    errorMessageId = "SessionNotValidMessage";
                    trackingData.Value = "SessionNotValid";
                    this.Master.PageHeading = pageConfig.PageDetail.PageMessages.GetMessage("sessionTimeHeading");
                    break;
                case Reference.MOBILE_CONFIRMATION:
                    errorMessageId = "";
                    isErrorFromConfPage = true;
                    trackingData.Value = "MobileConfirmation";
                    string errMsg = string.Format(WebUtil.GetTranslatedText("/PSPErrors/ReservationNumberIfPaymentProcessingPageIsStuckFAILURE"), Convert.ToString(Request.QueryString["BNo"]));
                    string deepLinkUrl = string.Format("http://{0}/{1}?{2}={3}&{4}={5}", Request.Url.Host, AppConstants.DLREDIRECT, AppConstants.DEEPLINK_VIEW_MODIFY_RESERVATION_ID,
                                         Convert.ToString(Request.QueryString["BNo"]), AppConstants.DEEPLINK_VIEW_MODIFY_LAST_NAME, Convert.ToString(Request.QueryString["LNm"]));
                    string innerHtml = string.Format("<a href={0}>{1}</a>", deepLinkUrl, deepLinkUrl);
                    string viewModifyText = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/ViewModifyReservationMessage");
                    this.hgcErrorMsg.InnerHtml = string.Format("{0}<br/>{1}<br/>{2}", errMsg, viewModifyText, innerHtml);
                    this.Master.PageHeading = "Application Error";
                    break;
                default:
                    errorMessageId = "genericMessage";
                    break;
            }

            if(!isErrorFromConfPage)
                this.hgcErrorMsg.InnerText = pageConfig.PageDetail.PageMessages.GetMessage(errorMessageId);
            this.Master.PageId = PageId();
        }

        /// <summary>
        /// Gets PageId
        /// </summary>
        /// <returns></returns>
        public override MobilePages PageId()
        {
            return MobilePages.Error;
        }
    }
}