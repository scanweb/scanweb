﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Entity.Model;

namespace Scandic.Scanweb.Mobile.Templates.ContentPages
{
    /// <summary>
    /// Standard Page
    /// </summary>
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionExpired(true)]
    public partial class StandardPage : VisualBasePage<ContentPageSection>
    {
        private StaticPageDataController pageController;
        protected string feedbackSuccessful;
        private const string CONTACT_US_HEADING = "contactUsPageHeading";
        private const string CONTACT_US_TITLE = "contactUsTitle";

        /// <summary>
        /// On Init
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            this.lbtnFeedback.Click += new EventHandler(lbtnFeedback_Click);
            this.menuItemsRepeater.ItemDataBound += new RepeaterItemEventHandler(Repeater_ItemDataBound);
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/ContentPages/StandardPage.aspx";
            pageController = new StaticPageDataController();
        }

        /// <summary>
        /// lbtnFeedback_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbtnFeedback_Click(object sender, EventArgs e)
        {
            var userInput = ProcessInput();
            List<KeyValueOption> validationErrors = new List<KeyValueOption>();

            if (pageController.IsPageModelValid(userInput, out validationErrors))
            {
                ratingErrorMsg.Visible = false;
                bool success = pageController.SendFeedback(userInput);
                if (success)
                {
                    var pageConfig = pageController.GetPageConfig<ContentPageSection>();
                    dvThaksMsg.Visible = true;
                    dvThaksMsg.InnerText = pageConfig.PageDetail.PageMessages.GetMessage(Reference.THANK_YOU_MSG);
                    feedbackSuccessful = "1";
                    dvFeedbackPageContent.Visible = false;
                }
            }
            else
            {
                RenderErrorMessages(validationErrors);
            }
        }

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                LoadPageData();
            }
        }

        /// <summary>
        /// LoadPageData
        /// </summary>
        private void LoadPageData()
        {
            string queryString = Request.QueryString[Reference.PAGE_ID] as string;

            if (!string.IsNullOrEmpty(queryString))
            {
                this.dvThaksMsg.Visible = false;
                var pageReference = PageIdentifier();
                var pageModel = pageController.GetPageData(pageReference);
                var pageConfig = pageController.GetPageConfig<ContentPageSection>();

                if (queryString == Reference.FEEDBACK)
                {
                    this.Title = string.Format("Scandic Hotels - {0}", pageModel.PageHeading);
                    this.hdnAdminEmailID.Value = pageModel != null
                                                     ? pageModel.StaticPageData.Email
                                                     : "admin@scandichotels.com";
                    this.dvContactUsPageContent.Visible = false;
                }
                else if (queryString == Reference.CONTACT_US)
                {
                    string pageHeading = pageConfig.PageDetail.PageMessages.GetMessage(CONTACT_US_HEADING);
                    string pageTitle = pageConfig.PageDetail.PageMessages.GetMessage(CONTACT_US_TITLE);
                    this.menuItemsRepeater.DataSource = pageModel.ContentMenu.Items;
                    this.menuItemsRepeater.DataBind();
                    Title = pageTitle;
                    if (pageModel != null && pageModel.StaticPageData != null)
                    {
                        this.lblOfficeHeading.InnerText = pageModel.StaticPageData.OfficeHeading;
                        this.lblAddress.Text = pageModel.StaticPageData.PostAddress;
                        this.phoneLink.Text = pageModel.StaticPageData.PhoneNumber;
                        this.phoneLink.NavigateUrl = string.Format("tel:{0}", pageModel.StaticPageData.PhoneNumber);
                        this.emailLink.Text = pageModel.StaticPageData.Email;
                        this.emailLink.NavigateUrl = string.Format("mailto:{0}", pageModel.StaticPageData.Email);

                        this.lblVistingAdds.Text = string.Format("{0}: ", pageModel.StaticPageData.VisitingAddsLabel);
                        this.vistingAdds.Text = pageModel.StaticPageData.VisitingAddress;
                        this.vistingAdds.NavigateUrl =
                            string.Format("http://maps.google.com/?q={0}",
                                          Server.UrlEncode(pageModel.StaticPageData.VisitingAddress));
                        this.lblOrganizationNum.Text =
                            string.Format("{0}: ", pageModel.StaticPageData.OrganizationNumLabel);
                        this.OrganizationNum.Text = pageModel.StaticPageData.OrganizationNumber;
                        if (string.IsNullOrEmpty(pageModel.StaticPageData.VisitingAddsLabel) &&
                            string.IsNullOrEmpty(pageModel.StaticPageData.VisitingAddress))
                        {
                            this.lblVistingAdds.Visible = false;
                            this.vistingAdds.Visible = false;
                        }
                        if (string.IsNullOrEmpty(pageModel.StaticPageData.OrganizationNumLabel) &&
                            string.IsNullOrEmpty(pageModel.StaticPageData.OrganizationNumber))
                        {
                            this.lblOrganizationNum.Visible = false;
                            this.OrganizationNum.Visible = false;
                        }
                    }
                    this.Master.PageHeading = pageHeading;
                    this.dvFeedbackPageContent.Visible = false;
                }
                this.Master.PageId = PageId();
            }
            else
            {
                this.dvContactUsPageContent.Visible = false;
                this.dvFeedbackPageContent.Visible = false;
                this.Master.PageHeading = string.Empty;
            }
        }
       
        private void LoadControls()
        {
            var pageConfig = pageController.GetPageConfig<ContentPageSection>();
            var pageReference = PageIdentifier();
            var pageData = pageController.GetPageData(pageReference);

       }
        /// <summary>
        /// ProcessInput
        /// </summary>
        /// <returns>StaticPageModel</returns>
        private StaticPageModel ProcessInput()
        {
            StaticPageModel userInput = new StaticPageModel();

            userInput.AboutMobileApp = txtFeedback.Text;
            userInput.MobileAppRating =
                int.Parse(ddlRating.SelectedValue) != 0 ? ddlRating.SelectedItem.Text : string.Empty;
            userInput.StaticPageData = new StandardPageData();
            userInput.StaticPageData.Email = hdnAdminEmailID.Value;
            return userInput;
        }

        /// <summary>
        /// Repeater_ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var data = e.Item.DataItem as UI.Entity.MenuItem;
                var menulink = e.Item.FindControl("menuLink") as HyperLink;
                var menulinkButton = e.Item.FindControl("menuLinkButton") as LinkButton;

                if (data != null)
                {
                    menulink.Text = data.Name;
                    menulink.NavigateUrl = data.Path;
                    menulink.Visible = true;
                    Utilities.AddAttributes(menulink, data.Attributes);
                }
            }
        }

        /// <summary>
        /// PageId
        /// </summary>
        /// <returns>MobilePages</returns>
        public override MobilePages PageId()
        {
            return PageIdentifier();
        }

        /// <summary>
        /// PageIdentifier
        /// </summary>
        /// <returns>MobilePages</returns>
        private MobilePages PageIdentifier()
        {
            string requestedPage =
                Request.QueryString[Reference.PAGE_ID] != null
                    ? Request.QueryString[Reference.PAGE_ID].ToString()
                    : string.Empty;
            MobilePages mobilePage = MobilePages.ContactUs;
            switch (requestedPage.ToUpper())
            {
                case Reference.CONTACT_US:
                    mobilePage = MobilePages.ContactUs;
                    break;
                case Reference.FEEDBACK:
                    mobilePage = MobilePages.Feedback;
                    break;
            }
            return mobilePage;
        }
    }
}