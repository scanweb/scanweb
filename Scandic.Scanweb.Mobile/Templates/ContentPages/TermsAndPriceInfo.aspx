﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TermsAndPriceInfo.aspx.cs"
    Inherits="Scandic.Scanweb.Mobile.Templates.ContentPages.TermsAndPriceInfo" MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" ResponseEncoding="utf-8" %>

<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<asp:Content ID="staticPageContent" ContentPlaceHolderID="cphMain" runat="server">
    <ul class="standard-list expandable-list">
        <asp:Repeater ID="contentDataRepeater" runat="server">
            <ItemTemplate>
                <li>
                    <h3>
                        <asp:HyperLink ID="ItemLink" runat="server" Visible="false"  CssClass="toggler"></asp:HyperLink></h3>
                    <div id="hgcDescription" class="hidden" runat="server" ><asp:Label ID="ItemDescription" runat="server" Visible="false"></asp:Label></div>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <ul>
        <asp:Repeater ID="externalLinksRepeater" runat="server">
            <ItemTemplate>
                <li>
                    <p><asp:HyperLink  class="icon external" ID="ExtenalLink" runat="server" Visible="false"></asp:HyperLink></p>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <asp:HyperLink ID="lnkClose" runat="server" CssClass="button green submit" >Close</asp:HyperLink>
    
</asp:Content>
