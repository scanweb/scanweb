﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StandardPage.aspx.cs" Inherits="Scandic.Scanweb.Mobile.Templates.ContentPages.StandardPage" 
    MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" ResponseEncoding="utf-8"%>
<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>

<asp:Content ID="contactUsPageContent" ContentPlaceHolderID="cphMain" runat="server">
    <p runat="server" id="dvThaksMsg" ></p>
    <div ID="dvContactUsPageContent" runat="server"> 
        <ul class="button-list">
            <asp:Repeater ID="menuItemsRepeater" runat="server">
                <ItemTemplate>
                    <li>
                        <asp:HyperLink ID="menuLink" runat="server" Visible="false" ></asp:HyperLink>
                        <asp:LinkButton ID="menuLinkButton" runat="server" Visible="false"></asp:LinkButton>
                    </li>
                </ItemTemplate>            
            </asp:Repeater>
        </ul>
            <h3 ID="lblOfficeHeading" runat="server" />
	        <p>
	            <asp:Label ID="lblAddress" runat="server" /> <br>
	            <asp:HyperLink ID="phoneLink" runat="server" class="icon click2call phone" onclick="return performFunctionTracking(2,'');" ></asp:HyperLink> <br>
	            <asp:HyperLink ID="emailLink" runat="server"  onclick="return performFunctionTracking(5,'');" ></asp:HyperLink> <br>
	            <asp:Label ID="lblVistingAdds" runat="server" /><asp:HyperLink ID="vistingAdds" runat="server" class="address" onclick="return performFunctionTracking(4,'');" Target="_blank"  ></asp:HyperLink> <br>
	            <asp:Label ID="lblOrganizationNum" runat="server" /><asp:Label ID="OrganizationNum" runat="server" />
	        </p>
    </div>	
    <div class="standard-form" ID="dvFeedbackPageContent" runat="server">
       <fieldset>
            <asp:Label ID="lblRating" runat="server" Text="What's your rating? (optional)" AssociatedControlID="ddlRating"></asp:Label>
            <span class="error" runat="server" id="ratingErrorMsg"></span>
            <asp:DropDownList ID="ddlRating" runat="server">
            </asp:DropDownList>
        </fieldset>
        <fieldset  id="fsFeedback" runat="server">
            <asp:Label ID="lblMobileApp" runat="server" Text="What do you think of our mobile app?" AssociatedControlID="txtFeedback"></asp:Label>
            <span class="error" runat="server" id="hgcBookingCodeError"></span>
            <input type="hidden" id="hdnAdminEmailID" runat="server" />
            <asp:TextBox TextMode = "MultiLine" ID="txtFeedback" runat="server" Rows="4"></asp:TextBox>            
        </fieldset>
        <p><asp:LinkButton ID="lbtnFeedback" runat="server" CssClass="button green hand submit"></asp:LinkButton></p>
    </div>		
    	
</asp:Content>
<asp:Content ContentPlaceHolderID="cphScript" runat="server" ID="pageScripts">
<script type="text/javascript" >
    var feedbackSuccessful = "<%= feedbackSuccessful %>";
    $().ready(function() {
        if (feedbackSuccessful === '1') {
            getFunctionTrackingDataWithPageId(11, 6, '');
        }
    });
</script>
</asp:Content>

