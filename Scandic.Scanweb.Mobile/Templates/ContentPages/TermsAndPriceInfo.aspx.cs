﻿//  Description					:   TermsAndPriceInfo                                     //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;

namespace Scandic.Scanweb.Mobile.Templates.ContentPages
{
    /// <summary>
    /// Code behind of TermsAndPriceInfo page.
    /// </summary>
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionExpired(true)]
    public partial class TermsAndPriceInfo : VisualBasePage<String>
    {
        private ContentPageDataController pageController;
        private IBookingRepository bookingRepository;

        /// <summary>
        /// Oninit event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            this.contentDataRepeater.ItemDataBound += new RepeaterItemEventHandler(Repeater_ItemDataBound);
            this.externalLinksRepeater.ItemDataBound += new RepeaterItemEventHandler(externalLinksRepeater_ItemDataBound);
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/ContentPages/TermsAndPriceInfo.aspx";
            pageController = new ContentPageDataController();
        }

        /// <summary>
        /// External links repeater item data bound event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void externalLinksRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var data = e.Item.DataItem as ExternalLinks;
                var menulink = e.Item.FindControl("ExtenalLink") as HyperLink;

                if (menulink != null)
                {
                    menulink.Text = data.LinkLabel;
                    menulink.NavigateUrl = data.LinkURL;
                    menulink.Visible = true;
                }
            }
        }

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                LoadPageData();
            }
        }

        /// <summary>
        /// Loads page data
        /// </summary>
        private void LoadPageData()
        {
            var queryString = Request.QueryString[Reference.PAGE_ID] as string;
            if (!string.IsNullOrEmpty(queryString))
            {
                var pageReference = PageIdentifier();
                var pageModel = pageController.GetPageData(pageReference);

                if (pageModel != null)
                {
                    this.Master.PageHeading = pageModel.PageHeading;
                    this.Title = string.Format("Scandic Hotels - {0}", pageModel.PageHeading);
                    this.lnkClose.Text = pageModel.ContentPageInfo.CloseButtonText;
                    this.contentDataRepeater.DataSource = pageModel.ContentPageInfo.GenericItemsInfo;
                    this.externalLinksRepeater.DataSource = pageModel.ContentPageInfo.ExternalLinks;
                    this.contentDataRepeater.DataBind();
                    this.externalLinksRepeater.DataBind();
                }

                if (queryString != null && queryString != Reference.TERMS_AND_CONDITIONS)
                {
                    externalLinksRepeater.Visible = false;
                }
            }
            else
            {
                contentDataRepeater.Visible = false;
                externalLinksRepeater.Visible = false;
            }
            lnkClose.Attributes.Add("onclick", "window.close(); return false;");
            lnkClose.NavigateUrl = "#";
            Master.IsPageOpenInNewWindow = true;
            this.Master.PageId = PageId();
        }

        /// <summary>
        /// Repeater item data bound event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var data = e.Item.DataItem as ItemsInfo;
                var itemLink = e.Item.FindControl("ItemLink") as HyperLink;
                var ItemDescription = e.Item.FindControl("ItemDescription") as Label;
                var descriptionDiv = e.Item.FindControl("hgcDescription") as HtmlGenericControl;
                if (data != null)
                {
                    itemLink.Text = data.ItemLabel;
                    itemLink.Attributes.Add("data-target", descriptionDiv.ClientID);
                    ItemDescription.Text = data.ItemDescription;
                    itemLink.Visible = true;
                    ItemDescription.Visible = true;
                }
            }
        }

        /// <summary>
        ///Returns page id based on the query string.
        /// </summary>
        /// <returns></returns>
        public override MobilePages PageId()
        {
            return PageIdentifier();
        }

        private MobilePages PageIdentifier()
        {
            string requestedPage = Request.QueryString[Reference.PAGE_ID] != null
                                       ? Request.QueryString[Reference.PAGE_ID].ToString()
                                       : string.Empty;
            MobilePages mobilePage = MobilePages.TermsCondition;
            switch (requestedPage.ToUpper())
            {
                case Reference.TERMS_AND_CONDITIONS:
                    mobilePage = MobilePages.TermsCondition;
                    break;
                case Reference.PRICE_INFORMATION:
                    mobilePage = MobilePages.PriceInformation;
                    break;
            }
            return mobilePage;
        }
    }
}