﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Error.aspx.cs" 
Inherits="Scandic.Scanweb.Mobile.Templates.Error" MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" Title="Scandic - Error" ResponseEncoding="utf-8" %>

<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>

<asp:Content ContentPlaceHolderID="cphMain" ID="errorContent" runat="server">
    <h1 id="hgcErrorMsg" runat="server"></h1>
    <br />
    <asp:HyperLink ID="lnkHome" runat="server"></asp:HyperLink>
    <asp:HiddenField runat="server" ID="trackingData" Value=""/>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphScript" ID="errorScript">
    <script type="text/javascript">
        $().ready(function() {
            var data = $('input[id*="trackingData"]').val();
            getPageTrackingDataWithParameter(7, data);
           
        });
    </script>
</asp:Content>

