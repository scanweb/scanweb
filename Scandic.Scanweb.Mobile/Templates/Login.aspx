﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Login.aspx.cs" Inherits="Scandic.Scanweb.Mobile.Templates.Login"
    MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" ResponseEncoding="utf-8" %>
<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<%@ Register Assembly="Scandic.Scanweb.Mobile" Namespace="Scandic.Scanweb.Mobile.UI.Controls"
    TagPrefix="Mobile" %>
<%@ Register Src="Controls/MemberLogin.ascx" TagName="MemberLogin" TagPrefix="uc1" %>
<asp:content contentplaceholderid="cphMain" id="loginPageContent" runat="server">
  <div class="standard-form">  
    <uc1:MemberLogin ID="uctlLogin" runat="server" />
 </div>    
</asp:content>