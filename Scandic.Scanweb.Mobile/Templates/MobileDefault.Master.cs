﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Booking.Repository;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using EPiServer.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Business;
using System.Web.UI;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;

namespace Scandic.Scanweb.Mobile.Templates
{
    /// <summary>
    /// MobileDefault
    /// </summary>
    public partial class MobileDefault : System.Web.UI.MasterPage
    {
        private int headerItemCount = 0;
        protected string homePageUrl = "";
        protected string serverHost = "";
        private MasterPageController pageController;
        private LoginController loginController;
        private IUserInfoRespository userRepository;
        protected string ajaxCallPath = "";
        protected string pageId = "";
        protected string reportSuiteId = "";
        protected string callUsText = "";
        protected string cookieDisabledMsg = "";
        protected string javascriptDisabledMsg = "";
        protected string tripAdvisorImageURL = "";
        protected string bookMarkShortName = "";
        protected string carousalimageMaxWidth = string.Empty;
        /// <summary>
        /// OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            this.footerMenuRepeater.ItemDataBound += new RepeaterItemEventHandler(FooterRepeater_ItemDataBound);
            this.headerMenuRepeater.ItemDataBound += new RepeaterItemEventHandler(HeaderRepeater_ItemDataBound);
            this.headerMenuRepeater.ItemCommand += new RepeaterCommandEventHandler(HeaderRepeater_ItemCommand);
            pageController = new MasterPageController();
            loginController = new LoginController();
            userRepository = new UserInfoRepository();
            SetHostServerName();
        }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadPageData();
                SetPageIndexForSEO();
            }
            else
            {
                ProcessRequest();
            }

            if (!IsPageOpenInNewWindow)
            {
                homePageUrl = pageController.GetPageUrl(MobilePages.Start);
            }
            else
            {
                homePageUrl = "#";
            }
            var pageConfig = pageController.GetPageConfig<MasterPageSection>();

            string carousalImgWidth = Convert.ToString(ConfigurationManager.AppSettings[AppConstants.MOBILE_CAROUSEL_IMAGE_WIDTH]);
            carousalimageMaxWidth = string.Format("width_{0}", carousalImgWidth);

            form1.Action = Request.RawUrl;
            reportSuiteId = pageConfig.PageMessages.GetMessage(Reference.ReportSuiteId);
            callUsText = pageConfig.PageMessages.GetMessage(Reference.CallUsTextId);
            cookieDisabledMsg = pageConfig.PageMessages.GetMessage(Reference.CookieDisableMsgId);
            javascriptDisabledMsg = pageConfig.PageMessages.GetMessage(Reference.JavascriptDisableMsgId);
            lblReviewPoweredBy.InnerText = pageConfig.PageMessages.GetMessage(Reference.MOBILE_REVIEW_POWERED_BY);
            
            PageData startPageData = CMSDataManager.GetPageData(Reference.START_PAGE_CMS_REFERENCE);
            Control cookieLawInfo = new Control();
            if (startPageData.Property["IsCookieAllowed"] != null && startPageData.Property["CookieLawMessage"] != null)
            {
                if (!Convert.ToBoolean(startPageData.Property["IsCookieAllowed"].Value) &&
                    !string.IsNullOrEmpty(Convert.ToString(startPageData.Property["CookieLawMessage"])))
                    cookieLawPlaceHolder.Controls.Add
                        (cookieLawInfo = LoadControl("/ScanwebMobile/Templates/Controls/MobileCookieLaw.ascx"));
            }
            if (!string.IsNullOrEmpty(Convert.ToString(startPageData.Property["MobileBookmarkShortName"])))
            {
                PlaceHolder metaContentPlaceHolder = (PlaceHolder)this.FindControl("plhMetaDataBookMarkShortname");
                bookMarkShortName = Convert.ToString(startPageData.Property["MobileBookmarkShortName"]);
                HtmlMeta meta = new HtmlMeta();
                meta.Name = "apple-mobile-web-app-title";
                meta.Content = bookMarkShortName;
                if (metaContentPlaceHolder != null)
                {
                    metaContentPlaceHolder.Controls.Add(meta);
                }
            }
        }

        /// <summary>
        /// ProcessRequest
        /// </summary>
        private void ProcessRequest()
        {
            switch (this.PostBackProcess.Value)
            {
                case "SignOut":
                    loginController.SignOut();
                    break;
                case "LanguageSwitch":
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// LoadPageData
        /// </summary>
        private void LoadPageData()
        {
            if (!IsPageOpenInNewWindow)
            {
                var actualLang = Request.QueryString[Reference.ActualLanguage];
                var pageData = pageController.GetPageData(actualLang);
                this.headerMenuRepeater.DataSource = pageData.HeaderMenu.Items;
                headerItemCount = pageData.HeaderMenu.Items.Count;
                this.headerMenuRepeater.DataBind();
                this.footerMenuRepeater.DataSource = pageData.FooterMenu.Items;
                this.footerMenuRepeater.DataBind();
            }

            pageHeading.Visible = !string.IsNullOrEmpty(pageHeading.InnerText);
            pageId = Convert.ToInt32(PageId).ToString();

            if (PageId == MobilePages.SelectHotel)
            {
                //divPoweredby.Style.Add("display", "block");
                divPoweredby.Visible = true;
                var pageGenConfig = pageController.GetGeneralConfig<ApplicationConfigSection>();
                tripAdvisorImageURL = pageGenConfig.GetMessage(Reference.MOBILE_TRIP_ADVISOR_FOOTER_IMAGE);
                
            }
            else
            {
                //divPoweredby.Style.Add("display", "none");
                divPoweredby.Visible = false;
                tripAdvisorImageURL = "";
            }
            //This session is used to set the CMPID for language overlay helper as CMPID is not accessible due to AJAX call 
            MiscellaneousSessionWrapper.MOBILEURLCMPID = Request.QueryString["cmpid"];

        }

        /// <summary>
        /// SetHostServerName
        /// </summary>
        private void SetHostServerName()
        {
            serverHost = pageController.GetServerHostName(Request);
        }

        /// <summary>
        /// HeaderRepeater_ItemCommand
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HeaderRepeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "SignIn":
                    loginController.RedirectToSignIn(Page.Request);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// HeaderRepeater_ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HeaderRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var data = e.Item.DataItem as UI.Entity.MenuItem;
                var displayItem = true;
                if (userRepository.IsUserAuthenticated)
                {
                    var returnAttr =
                        data.Attributes.Where(
                            attr => string.Equals(attr.Id, "hidewhenAuthenticated") && string.Equals(attr.Value, "true"))
                            .FirstOrDefault();
                    displayItem = returnAttr == null;
                }
                else
                {
                    var returnAttr =
                        data.Attributes.Where(
                            attr =>
                            string.Equals(attr.Id, "hidewhenNotAuthenticated") && string.Equals(attr.Value, "true")).
                            FirstOrDefault();
                    displayItem = returnAttr == null;
                }

                if (displayItem)
                {
                    var menulink = e.Item.FindControl("headerMenuLink") as HyperLink;
                    var menuLinkButton = e.Item.FindControl("headerMenuLinkButton") as LinkButton;
                    var separator = e.Item.FindControl("hgcSeparator") as HtmlGenericControl;
                    var commmandAttr = data.GetAttribute("commandname");

                    if (commmandAttr != null)
                    {
                        menuLinkButton.Text = data.Name;
                        menuLinkButton.CommandName = commmandAttr.Value;
                        var commmandArgumentAttr = data.GetAttribute("commmandArgument");
                        if (commmandArgumentAttr != null)
                        {
                            menuLinkButton.CommandArgument = commmandArgumentAttr.Value;
                        }
                        menuLinkButton.Visible = true;
                        Utilities.AddAttributes(menuLinkButton, data.Attributes);
                    }
                    else
                    {
                        Utilities.AddAttributes(menulink, data.Attributes);
                        menulink.Text = data.Name;
                        menulink.NavigateUrl = data.Path;
                        menulink.Visible = true;
                    }
                    separator.Visible = e.Item.ItemIndex < (headerItemCount - 1);
                }
                else
                {
                    e.Item.Visible = false;
                }
            }
        }

        /// <summary>
        /// FooterRepeater_ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FooterRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var data = e.Item.DataItem as UI.Entity.MenuItem;
                var menulink = e.Item.FindControl("footerMenuLink") as HyperLink;

                menulink.Text = data.Name;
                menulink.NavigateUrl = GetCompleteURL(data.Path);
                var attribute = data.GetAttribute("loadContactUsPath");
                if (attribute != null &&
                    string.Equals(attribute.Value, "true", StringComparison.InvariantCultureIgnoreCase))
                {
                    menulink.NavigateUrl = string.Format("{0}?pageId={1}",
                                                         pageController.GetPageUrl(MobilePages.ContactUs),
                                                         Reference.CONTACT_US);
                }

                Utilities.AddAttributes(menulink, data.Attributes);

                var feedbackAttribute = data.GetAttribute("loadFeedbackPath");
                if(feedbackAttribute != null &&
                     string.Equals(feedbackAttribute.Value, "true", StringComparison.InvariantCultureIgnoreCase))
                {
                    menulink.NavigateUrl =
                        string.Format
                            ("{0}?pageId={1}", pageController.GetPageUrl(MobilePages.Feedback), Reference.FEEDBACK);
                }
                Utilities.AddAttributes(menulink, data.Attributes);
            }
        }

        private string GetCompleteURL(string path)
        {
            var actualLanguage = LanguageRedirectionHelper.GetActualLanguage();

            if (!string.IsNullOrEmpty(path) && !string.IsNullOrEmpty(actualLanguage))
            {
                var itemUrl = string.Empty;
                string hostName = "ScandicSiteHostName{0}";
                string countryCode = actualLanguage.ToUpper();
                string appSettingsKey = string.Format(hostName, countryCode);
                string hostURL = ConfigurationManager.AppSettings[appSettingsKey] as string;
                if (!string.IsNullOrEmpty(hostURL))
                {
                    path = string.Format("http://{0}{1}", hostURL, path);

                }

            }
            var cmpidValue = Request.QueryString["cmpid"];
            if (!string.IsNullOrEmpty(path) && path.Contains("msl=link_to_main_site") && !string.IsNullOrEmpty(cmpidValue))
            {                
                path = string.Format("{0}&cmpid={1}", path,cmpidValue);                
            }
            return path;

        }
        #region Properties

        public string PageHeading
        {
            get { return this.pageHeading.InnerHtml; }
            set { this.pageHeading.InnerHtml = value; }
        }

        public HiddenField PostBackProcess
        {
            get { return this.hidProcess; }
        }

        public string AjaxCallPath
        {
            get
            {
                return string.Format("/{0}", ajaxCallPath);
            }

            set { ajaxCallPath = value; }
        }

        public string MetaDescription
        {
            get { return metaDescription.Content; }
            set { metaDescription.Content = value; }
        }

        public MobilePages PageId { get; set; }

        public bool IsPageOpenInNewWindow { get; set; }

        /// <summary>
        /// This value should be read from form object of request that to before
        /// Pre-render event of page. This is useful in the case when session has 
        /// expired and application needs to know under which local it was running
        /// till last request. For all other scenarios, Language property on basecontroller
        /// should be used.
        /// </summary>
        public string CurrentLanguage
        {
            set { hidCurrentLanguage.Value = value; }
        }
        /// <summary>
        /// Adds a meta tag control to the page header
        /// </summary>
        /// <param name="name">The name of the meta tag</param>
        /// <param name="content">The content of the meta tag</param>
        public void CreateMetaTagMobile(string name, string content)
        {
            HtmlMeta tag = new HtmlMeta();
            tag.Name = name;
            tag.Content = content;
            plhMetaDataArea.Controls.Add(tag);
        }
        #endregion

        #region SEO

        protected string IndexPageForSEO
        {
            get
            {
                if (PageId == MobilePages.Start)
                {
                    return "index,nofollow";
                }
                else
                {
                    return "noindex";
                }
            }
        }

        /// <summary>
        /// SetPageIndexForSEO
        /// </summary>
        protected void SetPageIndexForSEO()
        {
            HtmlMeta meta = new HtmlMeta();
            meta.Name = "robots";
            meta.Content = IndexPageForSEO;

            this.Page.Header.Controls.Add(meta);
        }

        #endregion
    }
}