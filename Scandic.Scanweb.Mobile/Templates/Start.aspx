﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Start.aspx.cs" Inherits="Scandic.Scanweb.Mobile.Templates.Start"
    MasterPageFile="/ScanwebMobile/Templates/MobileDefault.Master" ResponseEncoding="utf-8" %>

<%@ MasterType VirtualPath="/ScanwebMobile/Templates/MobileDefault.Master" %>
<%@ Register Src="/ScanwebMobile/Templates/Controls/ImageCarousel.ascx" TagName="ImageCarousel"
    TagPrefix="uc2" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<asp:Content ID="startPageContent" ContentPlaceHolderID="cphMain" runat="server">
    <ul class="button-list">
        <asp:Repeater ID="menuItemsRepeater" runat="server">
            <ItemTemplate>
                <li>
                    <asp:HyperLink ID="menuLink" runat="server" Visible="false"></asp:HyperLink>
                    <asp:LinkButton ID="menuLinkButton" runat="server" Visible="false"></asp:LinkButton>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <asp:PlaceHolder ID="carouselPlaceHolder" runat="server"></asp:PlaceHolder>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphScript" runat="server" ID="pageScripts">

    <script type="text/javascript">
        var addToHomeConfig = {
            animationIn: 'bubble',
            animationOut: 'drop',
            lifespan: 10000,
            expire: 2,
            touchIcon: true,
            autostart: true,
            returningVisitor: true,
            message: '<%=BookMarkOverlayText%>'
        };
    </script>

    <script type="text/javascript" src="<%=BookMarkingScript %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/ScanwebMobile/Public/Scripts/Start.min.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>

</asp:Content>
