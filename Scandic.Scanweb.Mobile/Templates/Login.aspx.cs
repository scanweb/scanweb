﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common.Interface;
using Scandic.Scanweb.Core.Encryption;

namespace Scandic.Scanweb.Mobile.Templates
{
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionExpired(true)]
    public partial class Login : VisualBasePage<LoginPageSection>
    {        
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            this.Master.AjaxCallPath = "ScanwebMobile/Templates/Login.aspx";
            
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var cryptoData = new CryptoData("Ranjit");

            var encryptedData = ScanwebCryptography.Instance.Encrypt(cryptoData, "ScanWebEncryptionKey12345");
            var decryptedData = ScanwebCryptography.Instance.Decrypt(encryptedData, "ScanWebEncryptionKey12345");
            if (!IsPostBack)
            {
                LoadControls();
            }            
        }

        private void LoadControls()
        {
            SetErrorMessages();
            this.Master.PageId = PageId();
        }

        private void SetErrorMessages()
        {
            //Render any error message to user that could be due to 
            RenderErrorMessages(uctlLogin.GetErrorMessages());
        }
              
        public override MobilePages PageId()
        {
            return MobilePages.SignIn;
        }

        
        public override LogonContext GetLogonContext()
        {
            return uctlLogin.GetLogonContext();
        }

    }
}