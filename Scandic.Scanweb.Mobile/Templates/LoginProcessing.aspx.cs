﻿//  Description					: LoginProcessing                                         //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Common;

namespace Scandic.Scanweb.Mobile.Templates
{
    /// <summary>
    /// Code behind of Login processing page.
    /// </summary>
    [AllowPublicAccess(true)]
    [AccessibleWhenSessionExpired(true)]
    public partial class LoginProcessing : BasePage
    {
        #region Declaration

        private LoginController pageController;

        #endregion

        /// <summary>
        /// Oninit event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new EventHandler(Page_Load);
            pageController = new LoginController();
        }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (PreviousPage != null)
            {
                var logonContext = PreviousPage.GetLogonContext();

                if (pageController.ValidateContext(logonContext))
                {
                    pageController.SignIn(logonContext);
                }
            }
            else
            {
                throw new ApplicationException("This page must be called from a cross page post from a landing page.");
            }
        }
    }
}