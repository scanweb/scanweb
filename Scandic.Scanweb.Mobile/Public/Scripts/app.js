if (typeof (scandic) === 'undefined') {
    scandic = {};
}

scandic.responsive = (function($) {

    var isIOS5 = false;

    function detectIOS5() {
        if (navigator.userAgent.match(/OS 5_.*\ like Mac OS X/i)) {
            isIOS5 = true;
        }
    }

    function hideMobileSafariAddressBar() {
        window.addEventListener("load", function() {
            setTimeout(function() {
                window.scrollTo(0, 1);
            }, 0);
        });
    }

    function clearInputsOnBlur($elements) {
        $elements.each(function() {
            $(this).focus(function() {
                if (this.value == $(this).data('defaultvalue')) {
                    this.value = '';
                }
            });
            $(this).blur(function() {
                if (this.value == '') {
                    this.value = $(this).data('defaultvalue');
                }
            });
        });
    }

    function initExpandableList($elements) {
        $elements.click(function(event) {
            $(this).parents('li:eq(0)').find('p').toggle();
            event.preventDefault();
        });
    }

    function initTogglerLinks($elements) {
        var $overlays = $('.overlay');
        $elements.click(function(event) {
            $overlays.hide();
            var $elementToShow = $('#' + $(this).data('target'));
            $elementToShow.toggle();

            if ($(this).is('.tooltip, .information')) {
                $('body').animate({
                    scrollTop: $elementToShow.offset().top - 20
                }, 500);

                event.preventDefault();
            }
        });

        $overlays.find('a.close').click(function(event) {
            $overlays.hide();
            event.preventDefault();
        });
    }

    function initShowLinks($elements) {
        $elements.click(function() {
            var $elementToShow = $('#' + $(this).data('target')).show();
        });
    }

    function initAutoComplete($element) {
        $element.click(function() {
            $autoCompleteContainer = $(this).next('div.autocomplete');
            $autoCompleteContainer.show();

            $('body').animate({
                scrollTop: $element.parents('fieldset:eq(0)').offset().top
            }, 500);

            $autoCompleteContainer.find('div.top a.close').click(function(event) {
                $autoCompleteContainer.hide();
                event.preventDefault();
            });

            $autoCompleteContainer.find('li a').click(function() {
                $element.val($(this).text());
                $autoCompleteContainer.hide();
                event.preventDefault();
            });
        });
    }

    function initDatePickers() {
        if (!isIOS5) {
            $('input[type="date"]').glDatePicker();
        } else {
            $('input[type="date"]').removeAttr('readonly');
        }
    }

    function initFormValidation() {
        //TODO: Remove this line when jquery bug is fixed
        return true;
        var $validatedForm = $('form');
        var $elementsToValidate = $validatedForm.find('div.standard-form input:required');

        $('#submit').click(function() {
            var isValidForm = true;

            $elementsToValidate.filter('[type=text], [type=email], [type=number]').each(function() {
                if ($(this).val() === "" || $(this).val() === $(this).data('defaultvalue')) {
                    $(this).prev('span.error').css('display', 'block');
                    isValidForm = false;
                } else {
                    $(this).prev('span.error').hide();
                }
            });

            $elementsToValidate.filter('[type=checkbox]:not(:checked)').each(function() {
                $(this).prev('span.error').css('display', 'block');
                isValidForm = false;
            });

            if (isValidForm) {
                $validatedForm.submit();
            } else {
                $('body').animate({
                    scrollTop: $('span.error:visible:eq(0)').offset().top - 20
                }, 500);
                return false;
            }
        });
    }

    return {
        detectIOS5: detectIOS5,
        hideMobileSafariAddressBar: hideMobileSafariAddressBar,
        clearInputsOnBlur: clearInputsOnBlur,
        initExpandableList: initExpandableList,
        initTogglerLinks: initTogglerLinks,
        initShowLinks: initShowLinks,
        initAutoComplete: initAutoComplete,
        initDatePickers: initDatePickers,
        initFormValidation: initFormValidation
    }

})(jQuery);

jQuery().ready(function() {
	scandic.responsive.detectIOS5();
	scandic.responsive.initExpandableList($('ul.expandable-list > li > h3 > a'));
	scandic.responsive.initTogglerLinks($('.toggler, a.tooltip'));
	scandic.responsive.initShowLinks($('.show'));
	scandic.responsive.initAutoComplete($('#search'));
	scandic.responsive.initDatePickers();
	scandic.responsive.hideMobileSafariAddressBar();
	scandic.responsive.clearInputsOnBlur($('div.standard-form').find('[type=text], [type=email], [type=number]'));
	scandic.responsive.initFormValidation();
	
	/* Remove methods below this line after the demo */
	$('#sign-in-page .submit').click(function() {
  		localStorage.setItem('signedIn', true);
  	});
	
	if(localStorage.getItem('signedIn')) {
		$('header div > a.sign-in').hide();
		$('header div > a.my-profile').css('display', 'inline');
	}
	$('a.sign-out').click(function() {
		localStorage.removeItem('signedIn');
		window.location = "index.html";
	});
});  