﻿var firstRequest = true;
var firstUserHistoryRequest = true;
var currentlyRenderedFutureStays = 0;
var futureStaySortBy = 1; //Default Sort by Check-in date
var futureStaySortOrder = 0; //Default sort oder is acesending.
var currentlyRenderedTransaction = 0;
var historySortBy = 1; //Default Sort by Check-in date
var historySortOrder = 0; //Default sort oder is acesending.
var historySortOrder = 1; //Default sort oder is acesending.
var viewHistoryText="";

$(document).ready(function() {
    initializePage();
});

function initializePage() {

    $('a[id*="lnkFutureStayViewMore"]').click(function() {
        ViewMoreFutureStays(this);
        return false;
    });
    $('a[id*="lnkHistoryViewMore"]').click(function() {
        ViewMoreHistory(this);
        return false;
    });
    $('a[id*="lnkViewHistory"]').click(function() {
        ViewUserHistory();
    });
    viewHistoryText = $('a[id*="lnkViewHistory"]').text();
    initialFutureStayRequst();

}

function initialFutureStayRequst() {
    GetFutureStaysToDisplay(currentlyRenderedFutureStays, noOfFutureStaysToDisplay, futureStaySortBy, futureStaySortOrder, false);    
}

function GetFutureStaysToDisplay(currentlyDisplayedBookings, noOfBookingsToDisplay, sortBy, sortOrder, clearExistingResults) {

    if (clearExistingResults) {
        $("#futureStaysTable").find("tbody").empty();
    }
    var curDateTime = new Date();
    var timeOut = getCurrentDateTime(curDateTime);
    var inputData = "{'currentlyDisplayedBookings':" + currentlyDisplayedBookings + ",'noOfBookingsToDisplay':" + noOfBookingsToDisplay +
                        ",'sortType':" + sortBy + ",'sortOrder':" + sortOrder + ",'fetchLatestBookings':" + false + ",'timeStamp':'" + timeOut + "'}";
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetFutureReservationToDisplay",
        // Pass parameter, via JSON object.
        data: inputData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            ProcessFutureBookings(data.d);
        },
        error: function(data) {

        }
    });

}

function GetUserHistoryToDisplay(currentlyDisplayedTransactions, noOfTransactionsToDisplay, sortBy, sortOrder, clearExistingResults) {

    if (clearExistingResults) {
        $("#historyTable").find("tbody").empty();
    }
    var curDateTime = new Date();
    var timeOut = getCurrentDateTime(curDateTime);
    var inputData = "{'currentlyDisplayedTransactions':" + currentlyDisplayedTransactions + ",'noOfTransactionsToDisplay':" + noOfTransactionsToDisplay +
                        ",'sortType':" + sortBy + ",'sortOrder':" + sortOrder + ",'fetchLatestTransactions':" + false + ",'timeStamp':'" + timeOut + "'}";
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetUserHistoryToDisplay",
        // Pass parameter, via JSON object.
        data: inputData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            ProcessUserHistory(data.d);
        },
        error: function(data) {

        }
    });

}

function ViewUserHistory() {

    if (!$("#historyTable").find("tbody tr").length) {
        GetUserHistoryToDisplay(currentlyRenderedTransaction, noOfTransactionsToDisplay, historySortBy, historySortOrder, false);
        firstUserHistoryRequest = false;
    }
    var historyContainer = $("#historyContainer");
    if($(historyContainer).is(":visible")) {
        $('a[id*="lnkViewHistory"]').text(viewHistoryText);
    }
    else {
        $('a[id*="lnkViewHistory"]').text(hideHistory);
    }
    $(historyContainer).slideToggle();

}
function SortFutureBookings(control) {
    $(control).siblings("th").each(function(index) {
        $(this).attr("class", "");
    });
    futureStaySortBy = $(control).data("sortby");
    var sortOrder = $(control).attr("data-sortorder");
    sortOrder = (sortOrder === "0") ? "1" : "0";
    futureStaySortOrder = sortOrder;
    $(control).attr("data-sortorder", sortOrder);
    var columnCSS = (sortOrder === "0") ? "asc" : "desc";
    $(control).attr("class", columnCSS);
    $('a[id*="lnkFutureStayViewMore"]').hide();
    var bookingsToDisplay = currentlyRenderedFutureStays;
    currentlyRenderedFutureStays = 0;
    GetFutureStaysToDisplay(0, bookingsToDisplay, futureStaySortBy, futureStaySortOrder, true);

}

function SortHistory(control) {
    $(control).siblings("th").each(function(index) {
        $(this).attr("class", "");
    });
    historySortBy = $(control).data("sortby");
    var sortOrder = $(control).attr("data-sortorder");
    sortOrder = (sortOrder === "0") ? "1" : "0";
    historySortOrder = sortOrder;
    $(control).attr("data-sortorder", sortOrder);
    var columnCSS = (sortOrder === "0") ? "asc" : "desc";
    $(control).attr("class", columnCSS);
    $('a[id*="lnkHistoryViewMore"]').hide();
    var transactionToDisplay = currentlyRenderedTransaction;
    currentlyRenderedTransaction = 0;
    GetUserHistoryToDisplay(0, transactionToDisplay, historySortBy, historySortOrder, true);

}
function ViewMoreFutureStays(control) {
    $(control).hide();
    GetFutureStaysToDisplay(currentlyRenderedFutureStays, noOfFutureStaysToDisplay, futureStaySortBy, futureStaySortOrder, false);
}
function ViewMoreHistory(control) {
    $(control).hide();
    GetUserHistoryToDisplay(currentlyRenderedTransaction, noOfTransactionsToDisplay, historySortBy, historySortOrder, false);
}

function ProcessFutureBookings(data) {

    var table = $("#futureStaysTable");
    if (data != null && data.FutureBookings != null && data.FutureBookings.length) {
        var bookingContainer = $(table).find("tbody");
        $(table).show();
        CreateBookingRows(data.FutureBookings, bookingContainer);
        currentlyRenderedFutureStays = currentlyRenderedFutureStays + data.FutureBookings.length;
        if (data.FetchMoreFutureBookings) {
            $('a[id*="lnkFutureStayViewMore"]').show();
        }
        else {
            $('a[id*="lnkFutureStayViewMore"]').hide();
        }
    }
    else {
        $(table).hide();
    }   

    if (data !== null && data.Errors !== null) {
        ShowMessage(data.Errors);
    }

    if (firstRequest) {
        firstRequest = false;
        $('a[id*="lnkViewHistory"]').show();
    }
}

function ProcessUserHistory(data) {

    var table = $("#historyTable");
    if (data != null && data.Transactions != null && data.Transactions.length) {
        var historyContainer = $(table).find("tbody");
        $(table).show();
        CreateTransactionRows(data.Transactions, historyContainer);
        currentlyRenderedTransaction = currentlyRenderedTransaction + data.Transactions.length;
        if (data.FetchMoreTransactions) {
            $('a[id*="lnkHistoryViewMore"]').show();
        }
        else {
            $('a[id*="lnkHistoryViewMore"]').hide();
        }
    }
    else {
        $(table).hide();
        $('a[id*="lnkViewHistory"]').hide();        
    }

    if (data !== null && data.Errors !== null) {
        ShowMessage(data.Errors);
    }

}

function CreateTransactionRows(transactions, historyContainer) {
    $(transactions).each(function(index) {
        $(historyContainer).append(CreateTransactionRow(this));
    });
}

function CreateTransactionRow(transaction) {
    var transactionRow = $("#dvTemplateTable").find("table tr.historyRow");
    var transactionCell = $(transactionRow).find(".transaction");
    $(transactionCell).text(transaction.Transaction);
    var dateCell = $(transactionRow).find(".date");
    $(dateCell).text(transaction.CheckInDateInString + " - " + transaction.CheckOutDateInString);
    var pointsCell = $(transactionRow).find(".points");
    $(pointsCell).text(transaction.PointsInString);

    return $(transactionRow).outerHTML();

}

function CreateBookingRows(bookings, bookingContainer) {
    $(bookings).each(function(index) {
        $(bookingContainer).append(CreateBookingRow(this));
    });
}

function CreateBookingRow(booking) {
    var bookingRow = $("#dvTemplateTable").find("table tr.templateRow");
    var pageUrl = modifyCancelUrl + "?rId=" + booking.ReservationNumber + "&lName=" + lastName;
    var hotelName = $(bookingRow).find("a.hotelName");
    $(hotelName).text(booking.HotelName);
    $(hotelName).attr("href", pageUrl);
    var checkInDate = $(bookingRow).find("a.checkInDate");
    $(checkInDate).text(booking.CheckInDateInString);
    $(checkInDate).attr("href", pageUrl);
    var checkOutDate = $(bookingRow).find("a.checkOutDate");
    $(checkOutDate).text(booking.CheckOutDateInString);
    $(checkOutDate).attr("href", pageUrl);

    if (!booking.IsHotelExist) {
        $(hotelName).attr("onclick", "return false;");
        $(checkInDate).attr("onclick", "return false;");
        $(checkOutDate).attr("onclick", "return false;");
        $(hotelName).css('cursor', 'default');
        $(checkInDate).css('cursor', 'default');
        $(checkOutDate).css('cursor', 'default');
    }
    else {
        $(hotelName).attr("onclick", "return true;");
        $(checkInDate).attr("onclick", "return true;");
        $(checkOutDate).attr("onclick", "return true;");
    }
    return $(bookingRow).outerHTML();

}

function ShowMessage(data) {

    var errorBlock = $("p.information");    
    if (data !== null) {
        $(data).each(function(index) {
            $(errorBlock).append(this.ErrorMessaage + "<br />");
        });

        if (data.length) {
            $(errorBlock).show();
        }
    }
}