﻿var datePickerMonths;
var datePickerDays;
var doNotshowLoader = false;
var ignoreCustomSearch = false;
if (typeof (scandic) === 'undefined') {
    scandic = {};
}

scandic.responsive = (function($) {

    var isIOS5 = true;
    var isCallEnabled = true;
    var isOperaBrowser = false;
    var isAndroidBrowser = false;
    var isAndroid2x = false;
    var isAndroid3x = false;
    var isAndroid4x = false;
    var isIpad = false;
    var isIEMobile = false;
    var listings;

    function showDatePicker() {
        //return (!isIOS5 && !isOperaBrowser);
        return true;
    }

    function initFeatureDetection() {
        var userAgentString = navigator.userAgent.toLowerCase();
        isIOS5 = userAgentString.match(/OS 5_.*\ like Mac OS X/i) !== null;
        isCallEnabled = userAgentString.match(/android|iphone|windows phone|blackberry/i) !== null;
        isOperaBrowser = userAgentString.match(/opera/i) !== null;
        isAndroidBrowser = userAgentString.match(/android/i) !== null;
        isAndroid2x = userAgentString.match(/android [2]/i) !== null;
        isAndroid3x = userAgentString.match(/android [3]/i) !== null;
        isAndroid4x = userAgentString.match(/android [4]/i) !== null;
        isIpad = userAgentString.match(/ipad/i) !== null;
        isIEMobile = userAgentString.match(/IEMobile/i) !== null;
    }

    function initAjaxLoader() {
        $(document.body).ajaxStart(function() {
            if (!doNotshowLoader) {
                $('#ajax-loader').show();
            }
        }).ajaxStop(function() {
            if (!doNotshowLoader) {
                $('#ajax-loader').hide();
            }
        });
    }


    function initDatePickers() {
        if (isIOS5 || isOperaBrowser) {
            $('input[type="date"]').attr('readonly', false);
        } else {
            $('input[type="date"]').glDatePicker();
        }
    }

    function setCustomSearch() {
        if (!ignoreCustomSearch) {
            var searchInput = $('input[id*="txtSearch"]').val();
            if (searchInput != "") {
                var normSearchInput = normalizeChars(searchInput);
                var matchingCityDestinations;
                var filteredListing = filterHotelListing(listings, normSearchInput);
                var cityObject;
                for (var cityCtr = 0; cityCtr < filteredListing.length; cityCtr++) {
                    cityObject = filteredListing[cityCtr];
                    var cityName = cityObject.name;
                    cityName = normalizeChars(cityName);

                    if (normSearchInput.toLowerCase() == cityName.toLowerCase()) {
                        matchingCityDestinations = cityObject;
                        break;
                    }
                }
                if (cityObject != null)
                    $('input[id*="txtSearch"]').val(cityObject.name);
            }
        }
    }


    var stringMapping = ['aa:(aa|å)', 'ss:(ss|ß)', 'e:(e|æ)', 'o:(o|ö|ø)', 'ö:(o|ö|ø)', 'ø:(o|ö|ø)', 'a:(a|å|æ|ä)',
						'å:(a|å|æ|ä)', 'æ:(a|å|æ|ä)', 'ä:(a|å|æ|ä)', 'ü:(ü|u)', 'u:(ü|u)']

    /* Normalize diacritics and international (non-english) characters */
    function normalizeChars(strToSearch) {
        var stringToReturn = "";
        strCounter = 0;

        while (strCounter < strToSearch.length) {
            var stringLen = 1;

            var matchFound = false;

            for (i = 0; i < stringMapping.length; i++) {
                key = stringMapping[i].split(":")[0];
                value = stringMapping[i].split(":")[1];

                keyLen = key.length;
                var currentStr = strToSearch.substring(strCounter, strCounter + keyLen);
                if (currentStr.toLowerCase() == key.toLowerCase()) {
                    stringLen = keyLen;
                    stringToReturn += value;
                    matchFound = true;
                    break;
                }
            }

            if (!matchFound) {
                stringToReturn += strToSearch.charAt(strCounter);
            }

            strCounter += stringLen;
        }

        return stringToReturn;
    }

    function filterHotelListing(json, chars) {
        var result = [];
        var stopWord = 'scandic ';
        chars = chars.toLowerCase().replace(stopWord, '');
        chars = $.trim(normalizeChars(chars));
        var regExpr = new RegExp('^(' + chars + '.*?)|([\\s]' + chars + '.*?)', 'gi');
        var hotelName;
        $(json).filter(function(index, item) {
            if (item.name !== null && item.name.length) {
                var alternateCity = item.AlternateCity !== null ? item.AlternateCity : "";
                if (item.name.match(regExpr) || alternateCity.match(regExpr)) {
                    result.push(item);
                    jQuery.each(item.hotels, function() {
                        result.push(this);
                    });
                } else {
                    var cityAdded = false;
                    jQuery.each(item.hotels, function() {
                        hotelName = this.name.toLowerCase().replace(stopWord, '');
                        postalCity = this.PostalCity !== null ? this.PostalCity.toLowerCase() : "";
                        if (hotelName.match(regExpr) || postalCity.match(regExpr)) {
                            if (!cityAdded) {
                                result.push(item);
                                cityAdded = true;
                            }
                            result.push(this);
                        }
                    });
                }
            }
        });

        return result;
    }

    function initTogglerLinks($elements) {

        $elements.click(function(event) {
            var $elementToShow = $('#' + $(this).data('target'));
            $elementToShow.toggle();
            event.preventDefault();
        });
    }

    function initNewWindowLinks($elements) {
        $elements.click(function(event) {
            window.open($(this).attr('href'));
            event.preventDefault();
        });
    }

    function initForms(form, readFromHistory, saveInHistory) {
        var $validatedForm = $(form);
        var $elementsToValidate = $validatedForm.find('div.standard-form input.required');

        // read form history if booking details
        // only read it if local storage data exists (check length)
        var $historyElements = $validatedForm.find('.history');
        if (readFromHistory && localStorage.length > 0) {
            $historyElements.each(function() {
                var elementId = $(this).attr('id');
                if (localStorage.getItem(elementId)) {
                    if ($(this).is(':checkbox')) {
                        $(this).attr('checked', localStorage.getItem(elementId));
                    }
                    else {
                        $(this).val(localStorage.getItem(elementId));
                    }
                }
            });
        }

        if (saveInHistory) {
            $('.submit').click(function() {
                var isValidForm = true;

                $elementsToValidate.filter('[type=text], [type=email], [type=number], [type=tel]').each(function() {
                    if ($(this).val() === "" || $(this).val() === $(this).attr('placeholder')) {
                        isValidForm = false;
                    }
                });

                $elementsToValidate.filter('[type=checkbox]:not(:checked)').each(function() {
                    isValidForm = false;
                });

                /* save form history if booking details and valid form */
                if (isValidForm) {
                    $historyElements.each(function() {
                        if ($(this).is(':checkbox')) {
                            localStorage.setItem($(this).attr('id'), $(this).is(':checked'));
                        }
                        else {
                            localStorage.setItem($(this).attr('id'), $(this).val());
                        }
                    });
                }
                return true;
            });
        }
    }

    function initClick2CallLinks($elements) {
        if (!isCallEnabled) {
            $elements.each(function() {
                var phoneNumber = new String($(this).attr('href'));
                if (phoneNumber.indexOf("tel:") !== -1) {
                    $(this).attr('href', phoneNumber.substring(4));
                }

                $(this).click(function(event) {
                    if (callUsText === "") {
                        callUsText = "Call us at: ";
                    }
                    alert(callUsText + $(this).attr('href'));
                    event.preventDefault();
                });
            });
        }
    }

    function initAutoComplete($element, datasource) {
        listings = datasource;
        $autoCompleteContainer = $('div.autocomplete:eq(0)');
        var $listingsContainer = $autoCompleteContainer.find('ul');

        $element.click(function() {
            
            $element.keyup(function() { $autoCompleteContainer.show(); });
            $('body').animate({
                scrollTop: $element.parents('fieldset:eq(0)').offset().top
            }, 500);

            $element.keyup(function(event) {
                ignoreCustomSearch = false;
                $listingsContainer.empty();
                var data = $.trim($(this).val());
                if (data === "" || data === null || data.length < 2) {
                    $autoCompleteContainer.hide();
                    event.preventDefault();
                    return false;
                }

                var filteredListing = filterHotelListing(listings, data);
                var currentListing = null;
                $listingsContainer.append('<li><a href="#" class="heading">' + nearMyPosition + '</a></li>');
                for (var i in filteredListing) {
                    var currentClass = "";
                    currentListing = filteredListing[i];
                    if (currentListing.isCity) {
                        currentClass = "heading";
                        currentPrefix = "";
                    }

                    $listingsContainer.append('<li><a href="#" class="' + currentClass + '">' + filteredListing[i].name + '</a></li>');
                }

            });

            $autoCompleteContainer.find('div.top a.close').click(function(event) {
                $autoCompleteContainer.hide();
                event.preventDefault();
            });

            if (!isAndroid2x || !isIpad) {
                $autoCompleteContainer.find('ul').delegate('li a', 'click', function(event) {
                    ignoreCustomSearch = true;
                    $element.val($(this).text());
                    $autoCompleteContainer.hide();
                    event.preventDefault();
                });
            }

        });
    }

    function initTouchScroll() {
        var element = document.querySelector('div.autocomplete > ul');
        if ((isAndroid2x || isIpad) && element) {
            var scrollStartPos = 0;
            var startX;
            var startY;
            var moved = false;
            var $autoCompleteContainer = $('div.autocomplete:eq(0)');
            var $searchInput = $('input[id*="txtSearch"]');

            element.addEventListener("touchstart", function(event) {
                scrollStartPos = this.scrollTop + event.touches[0].pageY;

                startX = event.touches[0].clientX;
                startY = event.touches[0].clientY;
                moved = false;

                event.preventDefault();
            }, false);

            element.addEventListener("touchmove", function(event) {
                if (Math.abs(event.touches[0].clientX - startX) > 10 ||
			      	Math.abs(event.touches[0].clientY - startY) > 10) {
                    moved = true;
                }

                this.scrollTop = scrollStartPos - event.touches[0].pageY;
            }, false);

            element.addEventListener("touchend", function(event) {
                var target = event.changedTouches[0].target;
                var $element = $(target);
                if (!moved && (target.nodeName === 'A' || target.nodeName === '#text')) {
                    $searchInput.val($element.text());
                    $autoCompleteContainer.hide();
                    $searchInput.blur();
                }
            }, false);

        }
    }

    function checkCookieEnabled() {
        var info = $("p.information");
        if (getCookie("TestCookie") === null) {
            $(info).html(cookieDisabledMsg);
            $(info).show();
        }
    }

    function displayCookieMessage() {

        var cookieLawCookie = getCookie('cookieLaw');
        var divObject = $('.cookieLaw');
        if (cookieLawCookie != 'true') {
            $(divObject).show();
        }
        $('.agreeCookie').click(function() {

            divObject.hide();
            var date = new Date();
            date.setMonth(date.getMonth() + 1);
            var expires = "; expires=" + date.toGMTString();
            document.cookie = "cookieLaw=true" + expires + "; path=/";
        });

    }

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    function initTogglerSlideLinks($elements) {
        $elements.click(function(event) {
            var targetSelector = $(this).data('target');
            targetSelector = targetSelector ? '#' + targetSelector : '.' + $(this).data('targetclass');
            $(targetSelector).slideToggle();
            if ($(this).is('a')) {
                event.preventDefault();
            }
        });
    }

    return {
        showDatePicker: showDatePicker,
        initFeatureDetection: initFeatureDetection,
        initDatePickers: initDatePickers,
        initAutoComplete: initAutoComplete,
        initAjaxLoader: initAjaxLoader,
        initTogglerLinks: initTogglerLinks,
        initNewWindowLinks: initNewWindowLinks,
        initClick2CallLinks: initClick2CallLinks,
        initTouchScroll: initTouchScroll,
        initForms: initForms,
        getCookie: getCookie,
        checkCookieEnabled: checkCookieEnabled,
        initTogglerSlideLinks: initTogglerSlideLinks,
        displayCookieMessage: displayCookieMessage,
        setCustomSearch: setCustomSearch
    }

})(jQuery);

jQuery.fn.outerHTML = function(s) {
    return (s) ? this.before(s).remove() : jQuery("<p>").append(this.eq(0).clone()).html();
}

function LoadOverlay(control) {
    var targetDiv = $(control).data("target");
    var overlayType = $(control).data("overlaytype");
    var asCtrl = $(control).data("associatedcontrol");
    var value = $(control).data("value");
    var overlayDiv = $("#" + targetDiv);
    var fireAjax = true;

    if (overlayDiv.length) {
        var isCachable = $(overlayDiv).data("iscachable");
        if (isCachable == "True") {
            fireAjax = false;
        }
    }

    if (fireAjax) {
        var inputData = "{'overlayType':'" + overlayType + "', 'id':'" + targetDiv + "', 'associatedControl':'" + asCtrl + "', 'value':'" + value + "'}";
        $.ajax({
            type: "POST",
            url: GetPageURL() + "/GetOverlay",
            // Pass parameter, via JSON object.
            data: inputData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                AddOverlay(data.d, targetDiv);
            },
            error: function(data) {
                // alert("Overlay cannot be opened due to error.");
            }
        });
    }
    else {
        showOverlay($(overlayDiv));
    }
}

function AddOverlayOverride(html, overlayDiv, overlayHolderClickable) {
    $("#" + overlayDiv).remove();
    $("#container").append(html);
    //registerLanguages($("#container"));
    showOverlay($("#" + overlayDiv), overlayHolderClickable);
}

function AddOverlay(html, overlayDiv) {
    AddOverlayOverride(html, overlayDiv, true);
}

function showOverlay(control, overlayHolderClickable) {
    var overlayHolder = $('#overlay-holder');
    var overlays = $('.overlay');
    $(overlays).hide();
    $(control).show();
    if (overlayHolderClickable) {
        $(overlayHolder).show().click(function() {
            $(overlays).hide();
            $(overlayHolder).hide();
        });
    }
    else {
        $(overlayHolder).show();
    }
    $(control).css('top', window.pageYOffset + 50);
}

function closeOverlay() {
    var overlays = $('.overlay');
    var overlayHolder = $('#overlay-holder');

    $(overlays).hide();
    $(overlayHolder).hide();
    return false;
}

function registerErrorRemovingHandler() {
    $('input,select').change(function() {
        var value = $(this).val();
        if (value !== undefined && value !== "") {
            $(this).parent().find('.error').text("");
        }
    });
}

function registerLanguages(control) {
    $(control).delegate('a.lang', 'click', function() {
        submitForm("LanguageSwitch", $(this).data("lang"));
    });
}

function submitForm(process, value) {
    $('input[id*="hidProcess"]').val(process);
    $('input[id*="hidProcessValue"]').val(value);
    $("form").submit();

}
function signOut() {
    submitForm("SignOut", "");
}
function getCalenderData() {
    if ($(".datepicker").length) {
        $.ajax({
            type: "POST",
            url: GetPageURL() + "/GetCalenderData",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                datePickerMonths = data.d.Months;
                datePickerDays = data.d.WeekDays;
            },
            error: function(data) {
                //alert("error retrieving calender details..");
            }

        });
    }
}

$().ready(function() {
    scandic.responsive.initAjaxLoader();
    scandic.responsive.initFeatureDetection();
    //scandic.responsive.initDatePickers();
    scandic.responsive.initTogglerLinks($('a.toggler'));
    scandic.responsive.initNewWindowLinks($('a.new-window'));
    scandic.responsive.initClick2CallLinks($('a.click2call'));
    scandic.responsive.initTouchScroll();
    registerErrorRemovingHandler();
    getCalenderData();
    scandic.responsive.checkCookieEnabled();
    scandic.responsive.displayCookieMessage();
    scandic.responsive.initTogglerSlideLinks($('a.toggler-slide'));

});

function getCurrentDateTime(curDateTime) {
    return (curDateTime.getFullYear() + '-' + curDateTime.getMonth() + '-' +
			curDateTime.getDate() + '_' + curDateTime.getHours() + ':' +
			curDateTime.getMinutes() + ':' + curDateTime.getSeconds() + ':' +
			curDateTime.getMilliseconds()).toString();
}
/* $(function() {
    //iPhone Flag	
    var device = navigator.userAgent.toLowerCase();
    var ios = device.match(/(iphone|ipod|ipad)/);
    var is_nAndroid = ((device.indexOf("android") > -1) && !(device.indexOf("chrome") > -1));
    var is_safari = device.indexOf("Safari") > -1;
    var pixRatio = window.devicePixelRatio;
    if (ios) {
        if (pixRatio >= 1.5) {
            $('header > div a.settings.se').css('background-position', '0 8px');
            $('header > div a.settings.en').css('background-position', '0 -24px');
            $('header > div a.settings.no').css('background-position', '0 -58px');
            $('header > div a.settings.de').css('background-position', '0 -90px');
            $('header > div a.settings.fi').css('background-position', '0 -122px');
            $('header > div a.settings.dk').css('background-position', '0 -154px');
            $('header > div a.settings.ru').css('background-position', '0 -180px');
        }
    }
    if ((is_nAndroid) && (pixRatio >= 1.5)) {
        $('header > div a.settings.se').css('background-position', '0 4px');
        $('header > div a.settings.en').css('background-position', '0 -29px');
        $('header > div a.settings.no').css('background-position', '0 -62px');
        $('header > div a.settings.de').css('background-position', '0 -94px');
        $('header > div a.settings.fi').css('background-position', '0 -126px');
        $('header > div a.settings.dk').css('background-position', '0 -158px');
        $('header > div a.settings.ru').css('background-position', '0 -184px');
    } 
}); */

//Added For Offers CR 
$(document).ready(function() {
    $(".category-filter").click(function() {
        $(".category-container").toggle("slow", function() {
            if ($(".category-container").css("display") == "block") {
                $(".category-filter-icon").css("background-position", "0 2px");
            }
            else {
                $(".category-filter-icon").css("background-position", "-16px 2px");
            }
        });
    });

    //Truncate text more than 3 lines
    //$('.offer-desc').ThreeDots({max_rows:3});  
    $(".offer-arrow").each(function() {
        var olContentH = $(this).siblings(".offer-content").height();
        $(this).css("height", olContentH);
    });

    //Execute on resize
    $(window).resize(function() {
        $(".offer-arrow").each(function() {
            var olContentH = $(this).siblings(".offer-content").height();
            $(this).css("height", olContentH);
        });
    });   //End of resize code       
});

//Added for My Pref
$(document).ready(function() {
    var $bookingDetPreferences = $("#otherRequests input[type='checkbox']");
    if ($bookingDetPreferences.length > 0) {
        setPreferencePopupOnLoad($bookingDetPreferences);
    }
    $("#otherRequests input[type='checkbox']").click(function() {
        var $this = $(this);
        $($this).next().next(".additional-info").slideToggle();
    });

    function setPreferencePopupOnLoad(preferences) {
        var $checked = $(preferences).filter(':checked');
        $(".additional-info").hide();
        $($checked).each(function(index) {
            $(this).next().next(".additional-info").show()
        });
    }
});
