﻿$(document).ready(function() {
    initializePage();
});

function initializePage() {
    checkVisibilityOfFunctionality();
}

function checkVisibilityOfFunctionality() {
    var memberLogin = $("div#memberLogin");
    var findBooking = $("div#findBooking");

    $(memberLogin).show();
    if ($(memberLogin).find(".error:visible").length === 0) {    
        $(memberLogin).hide();
    }

    $(findBooking).show();
    if ($(findBooking).find(".error:visible").length === 0) {
        $(findBooking).hide();
    }
}