﻿
function GetOfferCarouselContents() {
    var curDateTime = new Date();
    var timeOut = getCurrentDateTime(curDateTime);
    var offerPageID = GetQueryStringParams('pageid');
    var inputData = "{'offerPageID':'" + offerPageID + "','timeout':'" + timeOut + "'}"
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetOfferCarouselData",
        data: inputData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            ProcessCarousalData(data.d);
        },
        error: function(data) {
            StopLoader();
        }
    });
}

$(document).ready(function() {
    GetOfferCarouselContents();
    if (trackingData != '')
        getPageTrackingDataWithParameter(18, trackingData);
    if (selectRateOfferTrackingData != '')
        getPageTrackingDataWithParameter(18, selectRateOfferTrackingData);
});

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}