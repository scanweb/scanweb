﻿$(document).ready(function() {
    $('.offerFilterBtn').bind('click', UpdateFilter);
    var selectedCategory = $('.offerFilterActiveBtn').text();
    $('#categoryTextcontainer').text(selectedCategory);
    function UpdateFilter(event) {
        var that = $(this);
        $('.offer-box').hide();
        if (that.attr("data-id") != "DummyContainer") {
            $("h2[id$='showAllOffer']").removeClass('offerFilterActiveBtn');
        }
        if (that.hasClass('offerFilterActiveBtn')) {
            that.removeClass('offerFilterActiveBtn');

        }
        else {
            that.addClass('offerFilterActiveBtn');
        }

        var tragetElement = $('.offerFilterActiveBtn');

        $.each(tragetElement, function(index, value) {
            var targetID = $(this).attr("data-id").split(',');

            $.each(targetID, function(index, value) {
                var tElement = $("div[data-id*='" + value + "']");
                tElement.show();		
				SetArrow();
            });

        });

        if ((!$('.offerFilterBtn').hasClass('offerFilterActiveBtn')) || that.attr("data-id") == "DummyContainer") {
            $('.offerFilterBtn').removeClass('offerFilterActiveBtn');
            $("h2[id$='showAllOffer']").addClass('offerFilterActiveBtn');
            $('.offer-box').show();
			SetArrow();
        }
        $('#categoryTextcontainer').text(setSelectedCategoryText(','));
        setOfferNo();
    }
    SetFilterState();
    setOfferNo();
});

function setSelectedCategoryText(seperator) {
    var selectedCategorylist = $(".offerFilterActiveBtn").map(function() {
        return $(this).text();
    }).get().join(seperator + " ");
    return selectedCategorylist;
}


function setOfferNo() {
    var allOfferBox = $('.offer-box');
    var offerBoxVisible = $('.offer-box:visible');
    var i = 0;
    $(allOfferBox).each(function(item) {
        this.removeAttribute('name');
    });
    $(offerBoxVisible).each(function(item) {
        i++;
        this.setAttribute('name', 'Banner' + i);
    });
}

function loadSelectedOffer(offerObj, offerUrl) {
    var selectedOfferForm = $('#selectedOffer');
    $(selectedOfferForm).attr('action', offerUrl);
    $('#bannerInfo').val($(offerObj).attr('name'));
    $('#selectedFillters').val(setSelectedCategoryText('|'));
    $('#fillterState').val(SetFilterState());
    $(selectedOfferForm).submit();
}

var filterState;
function SetFilterState() {
    var offerBoxVisible = $('.category-container');
    if ($(offerBoxVisible).is(":visible") && filterState != "Filter Activated") {
        filterState = "Filter Activated";
    }
    return filterState;
}
function SetArrow() {
	$(".offer-arrow").each(function() {
		var olContentH = $(this).siblings(".offer-content").height();
		$(this).css("height", olContentH);
	});
}