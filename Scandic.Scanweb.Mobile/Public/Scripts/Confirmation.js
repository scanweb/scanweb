﻿
function GetSaveCreditCardOverlay() {
	 var targetDiv = "divSaveCreditCardOverlay";
    var overlayDiv = $("#" + targetDiv);
    var fireAjax = true;

    if (overlayDiv.length) {
        var isCachable = $(overlayDiv).data("iscachable");
        if (isCachable == "True") {
            fireAjax = false;
        }
    }
    if (fireAjax) {
        var curDateTime = new Date();
        var timeOut = getCurrentDateTime(curDateTime);
        var inputData = "{'timeout':'" + timeOut + "'}";
        $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetSaveCreditCardOverlay",
        data: inputData,
        contentType: "application/json; charset=utf-8",
        success: function(data) {
            AddOverlay(data.d, targetDiv);
        },
        error: function(data) {
        }
    });
    }
    else {
        showOverlay($(overlayDiv));
    }
    return false;
}

$().ready(function() {
    GetSaveCreditCardOverlay();
});

function SaveCreditCardInformation() {
	var targetDiv = "divSaveCreditCardResponseOverlay";
    var overlayDiv = $("#" + targetDiv);
         $.ajax({
        type: "POST",
        url: GetPageURL() + "/SaveCreditCard",
        contentType: "application/json; charset=utf-8",
        success: function(data) {
            AddOverlay(data.d, targetDiv);
        },
        error: function(data) {
        }
    });
    return false;
}