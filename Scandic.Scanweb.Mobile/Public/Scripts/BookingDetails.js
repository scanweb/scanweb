﻿
$(document).ready(function() {
    initializePage();
});

function initializePage() {
    scandic.responsive.initForms($("form"), readFromHistory, saveInHistory);
    depositeInfoClick();
    RetrievePhoneCodes();
    RegisterCountryOnChange();
    CheckMessageToShow();
	AddinlineStyleforCheckbox();
}
function AddinlineStyleforCheckbox(){
	$('input[id$="chkAcceptedCondition"]').css({'float':'left'});
}

function CheckMessageToShow() {

    $('p[id*="hgcInfo"]').each(function() {
        if ($(this).html() != "") {
            $(this).show();
        }
    });
}
function RegisterCountryOnChange() {
    $('select[id*="ddlCountry"]').change(function() {
        SetPhoneCode(this);
    });
}

function SetPhoneCode(countryControl) {
    var countryValue = $(countryControl).find(":selected").val();
    $(phoneCodes).each(function() {
        var countryCode = this.Key;
        if (countryCode === countryValue) {
            var phoneCode = $.isNumeric(this.Value) ? this.Value : "";
            $('select[id*="ddlCountryCode"]').val(phoneCode);
            return true;
        }
    });
}

function depositeInfoClick() {
    var radioOptions = $('input[id*="rdoDepositeInfo"]');
    $(radioOptions).click(function() {
        showHideDepositeInfo(this);
    });

    if (radioOptions.length) {
        var checkOption = $('input[id*="rdoDepositeInfo"]:checked');
        var control = null;
        if (checkOption.length) {
            control = checkOption;
        }
        else {
            control = radioOptions[0];
        }
        showHideDepositeInfo(control);
    }
}

function showHideDepositeInfo(control) {
    var depositeInfoElement = $("#cardInformation");
	var divFlexFGPCreditCardsForPayment = $('#divFlexFGPCreditCardsForPayment');
	if ($(control).is(':checked')) {
	    var showFGPDropdownSection = false;
	    $(divFlexFGPCreditCardsForPayment).find('select').each(function() {
		    if($(this).find('option').length > 0) {
			    showFGPDropdownSection = true;
			    return;
		    }
	    });
	    if(showFGPDropdownSection) {
		    $(divFlexFGPCreditCardsForPayment).show();
	    } else {
		    $(depositeInfoElement).show();
	    }
    }
    else {
        $(depositeInfoElement).hide();
	    $(divFlexFGPCreditCardsForPayment).hide();
    }
}

function RetrievePhoneCodes() {
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetPhoneCodes",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            phoneCodes = data.d;
        },
        error: function(data) {
            
        }

    });
}

    function DisableBookButton() {
        var disableCover = $('#disable-cover');
        disableCover.show();
    }

