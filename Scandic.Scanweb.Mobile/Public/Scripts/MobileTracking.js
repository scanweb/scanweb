﻿
$().ready(function() {
    //Select Hotel, Select Rate, ErrorPage and Cancel confirmation page should not be tracked on page load
if (pageId !== "2" && pageId !== "3" && pageId !== "15" && pageId !== "7" && pageId !== "17" && pageId !== "18" && pageId !== "19") {
        getPageTrackingData();
    }
});

function getPageTrackingData() {
    if (pageId !== null && pageId !== "" && pageId !== undefined) {

        getPageTrackingDataWithParameter(pageId, "");
        
    }
}

function getPageTrackingDataWithParameter(trackingPageId, inforForTracking) {
    if (trackingPageId !== null && trackingPageId !== "" && trackingPageId !== undefined) {
        var curDateTime = new Date();
        var timeOut = getCurrentDateTime(curDateTime);
        var inputData = "{'pageId':" + trackingPageId + ",'data':'" + inforForTracking + "','timeout':'" + timeOut + "'}";
        $.ajax({
            type: "POST",
            url: GetPageURL() + "/GetPageTrackingData",
            // Pass parameter, via JSON object.
            data: inputData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                trackPageData(data.d);
            },
            error: function(data) {
                //alert("Error getting page tracking data.");
            }
        });
        
    }
}

function trackPageData(data) {
    if (data !== null) {
        s = SetTrackingInfo(s, data);
        var s_code = s.t(); 
        if (s_code) document.write(s_code);
        //if (navigator.appVersion.indexOf('MSIE') >= 0) document.write(unescape('%3C') + '\!-' + '-');
    }
    functionToTrackOnPageLoad();
}

function functionToTrackOnPageLoad() {
    if (document.URL.indexOf('loggedIn=1') > 0) {
        getFunctionTrackingDataWithPageId(6, 7, '');
    }
}

function performFunctionTracking(functionId, data) {
    getFunctionTrackingData(functionId, data);
    return true;
}

function getFunctionTrackingDataWithPageId(functionPageId, functionId, data) {
    if (functionPageId !== null && functionPageId !== "" && functionPageId !== undefined) {
        var curDateTime = new Date();
        var timeOut = getCurrentDateTime(curDateTime);
        var inputData = "{'pageId':" + functionPageId + ",'functionId':" + functionId + ",'data':'" + data + "','timeout':'" + timeOut + "'}";
        $.ajax({
            type: "POST",
            url: GetPageURL() + "/GetFunctionTrackingData",
            // Pass parameter, via JSON object.
            data: inputData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                trackFunctionData(data.d);
            },
            error: function(data) {
               // alert("Error getting fuction tracking data.");
            }
        });

    }
}
function getFunctionTrackingData(functionId, data) {
    getFunctionTrackingDataWithPageId(pageId, functionId, data);      
}

function trackFunctionData(data) {
    if (data !== null) {
        var sfunc = s_gi(s_account);
        sfunc = SetTrackingInfo(sfunc, data);
        sfunc.tl(this, 'o', data.functionName);
    }
}

function SetTrackingInfo(sCode, tData) {

    if (sCode !== null && tData !== null) {
        sCode.pageName = (tData.pageName !== null) ? tData.pageName : sCode.pageName;
        sCode.pageType = (tData.pageType !== null) ? tData.pageType : sCode.pageType;
        sCode.prop34 = (tData.prop34 !== null) ? tData.prop34 : sCode.prop34;
        sCode.eVar34 = (tData.eVar34 !== null) ? tData.eVar34 : sCode.eVar34;
        sCode.events = (tData.events !== null) ? tData.events : sCode.events;
        sCode.event17 = (tData.event17 !== null) ? tData.event17 : sCode.event17;
        sCode.prop11 = (tData.prop11 !== null) ? tData.prop11 : sCode.prop11;
        sCode.prop12 = (tData.prop12 !== null) ? tData.prop12 : sCode.prop12;
        sCode.prop13 = (tData.prop13 !== null) ? tData.prop13 : sCode.prop13;
        sCode.prop14 = (tData.prop14 !== null) ? tData.prop14 : sCode.prop14;
        sCode.prop15 = (tData.prop15 !== null) ? tData.prop15 : sCode.prop15;
        sCode.prop16 = (tData.prop16 !== null) ? tData.prop16 : sCode.prop16;
        sCode.prop17 = (tData.prop17 !== null) ? tData.prop17 : sCode.prop17;
        sCode.prop18 = (tData.prop18 !== null) ? tData.prop18 : sCode.prop18;
        sCode.prop25 = (tData.prop25 !== null) ? tData.prop25 : sCode.prop25;
        sCode.prop26 = (tData.prop26 !== null) ? tData.prop26 : sCode.prop26;
        sCode.eVar2 = (tData.eVar2 !== null) ? tData.eVar2 : sCode.eVar2;
        sCode.eVar4 = (tData.eVar4 !== null) ? tData.eVar4 : sCode.eVar4;
        sCode.eVar5 = (tData.eVar5 !== null) ? tData.eVar5 : sCode.eVar5;
        sCode.eVar6 = (tData.eVar6 !== null) ? tData.eVar6 : sCode.eVar6;
        sCode.eVar8 = (tData.eVar8 !== null) ? tData.eVar8 : sCode.eVar8;
        sCode.eVar11 = (tData.eVar11 !== null) ? tData.eVar11 : sCode.eVar11;
        sCode.eVar12 = (tData.eVar12 !== null) ? tData.eVar12 : sCode.eVar12;
        sCode.eVar13 = (tData.eVar13 !== null) ? tData.eVar13 : sCode.eVar13;
        sCode.eVar14 = (tData.eVar14 !== null) ? tData.eVar14 : sCode.eVar14;
        sCode.eVar15 = (tData.eVar15 !== null) ? tData.eVar15 : sCode.eVar15;
        sCode.eVar16 = (tData.eVar16 !== null) ? tData.eVar16 : sCode.eVar16;
        sCode.eVar17 = (tData.eVar17 !== null) ? tData.eVar17 : sCode.eVar17;
        sCode.eVar18 = (tData.eVar18 !== null) ? tData.eVar18 : sCode.eVar18;
        sCode.eVar21 = (tData.eVar21 !== null) ? tData.eVar21 : sCode.eVar21;        
        sCode.eVar25 = (tData.eVar25 !== null) ? tData.eVar25 : sCode.eVar25;
        sCode.eVar26 = (tData.eVar26 !== null) ? tData.eVar26 : sCode.eVar26;
        sCode.eVar27 = (tData.eVar27 !== null) ? tData.eVar27 : sCode.eVar27;
        sCode.eVar30 = (tData.eVar30 !== null) ? tData.eVar30 : sCode.eVar30;
        sCode.eVar39 = (tData.eVar39 !== null) ? tData.eVar39 : sCode.eVar39;
        sCode.eVar40 = (tData.eVar40 !== null) ? tData.eVar40 : sCode.eVar40;
        sCode.eVar41 = (tData.eVar41 !== null) ? tData.eVar41 : sCode.eVar41;
        sCode.eVar42 = (tData.eVar42 !== null) ? tData.eVar42 : sCode.eVar42;
        sCode.eVar43 = (tData.eVar43 !== null) ? tData.eVar43 : sCode.eVar43;
        sCode.eVar48 = (tData.eVar48 !== null) ? tData.eVar48 : sCode.eVar48;
        sCode.eVar49 = (tData.eVar49 !== null) ? tData.eVar49 : sCode.eVar49;
        sCode.eVar29 = (tData.eVar29 !== null) ? tData.eVar29 : sCode.eVar29;
        sCode.eVar31 = (tData.eVar31 !== null) ? tData.eVar31 : sCode.eVar31;
        sCode.eVar62 = (tData.eVar62 !== null) ? tData.eVar62 : sCode.eVar62;
        sCode.prop54 = (tData.prop54 !== null) ? tData.prop54 : sCode.prop54;
        sCode.prop44 = (tData.prop44 !== null) ? tData.prop44 : sCode.prop44;
        sCode.prop49 = (tData.prop49 !== null) ? tData.prop49 : sCode.prop49;
        sCode.prop50 = (tData.prop50 !== null) ? tData.prop50 : sCode.prop50;
        sCode.prop48 = (tData.prop48 !== null) ? tData.prop48 : sCode.prop48;
        sCode.prop20 = (tData.prop20 !== null) ? tData.prop20 : sCode.prop20;
        sCode.purchaseID = (tData.purchaseID !== null) ? tData.purchaseID : sCode.purchaseID;
        sCode.products = (tData.products !== null) ? tData.products : sCode.products;
        sCode.currencyCode = (tData.currencyCode !== null) ? tData.currencyCode : sCode.currencyCode;
        sCode.linkTrackEvents = (tData.linkTrackEvents !== null) ? tData.linkTrackEvents : sCode.linkTrackEvents;
        sCode.linkTrackVars = (tData.linkTrackVars !== null) ? tData.linkTrackVars : sCode.linkTrackVars;
        sCode.events = (tData.events !== null) ? tData.events : sCode.events;
        
    }
    
    return sCode;
}