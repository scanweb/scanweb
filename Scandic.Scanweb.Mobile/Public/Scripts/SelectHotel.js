﻿var currentlyRenderedHotels = 0;
var hotelPageCount = 0;
var stopGetHotelRequest = false;
var currentSortOption = "";
var firstTimeSearch = true;
var scrollPos = 0;
var timeoutHandle;
var firstRequest = false;
var maintainScrollPosition = false;
var timeoutId;
var requestFired = false;

$(document).ready(function() {
    initializePage();
});

function initializePage() {
    firstTimeSearch = true;
    $('a[id*="lnkBack"]').hide();
    $('select[id*="ddlSort"]').hide();
    $('a[id*="hgcViewMoreResults"]').hide();
    $('hr[id*="hrViewButton"]').hide();
    $('a.view-more-hotels').click(function() {
        ViewMoreResults(this);
        return false;
    });
    //timeoutHandle = setTimeout("initialHotelRequst();", 1000);
    initialHotelRequst();
	$('a[id*="hgcViewMoreResults"]').append("<span></span>");
}

function initialHotelRequst() {
    var sortCntl = $('select[id*="ddlSort"]');
    firstRequest = true;
    //Hide error message
    $("p.information").hide();
    if (isPromoSearch == 'True')
        $(sortCntl).val('7');
    GetHotelsToDisplay(currentlyRenderedHotels, noOfHotelsToDisplay, $(sortCntl).val(), false);
}

function setPinHeight() {
    var addrContH = $('.sel-hotel-addr').height();
    $(".addr-pin").css("height", addrContH);
}

function OnSort(control) {
    var sortOption = $(control).find(":selected").val();
    if (sortOption != undefined && sortOption != "") {
        currentSortOption = sortOption;
        hotelPageCount = 0;
        maintainScrollPosition = false;
        GetHotelsToDisplay(0, currentlyRenderedHotels, currentSortOption, true);
        currentlyRenderedHotels = 0;
    }
}

function SubmitSelectedHotel(control, clickSrc) {
    var hotelName = "";
    var corporateNameControl;
    //findNameInParent
    if (clickSrc == "button") {
        hotelName = $(control).parent().parent().parent().siblings(".sel-hotel-name").find("a").text(); // $(control).parent().find("h3.org a").text();
        corporateNameControl = $(control).parent().siblings("div.booking-code-container").find("span.booking-code"); //$(control).siblings(".corp-name");
    }
    else if (clickSrc == "hotelname") {
        hotelName = $(control).text();
        corporateNameControl = $(control).parent().siblings().find("div.hotel-booking-wrapper div.booking-code-container span.booking-code");
    }
    else if (clickSrc == "image") {
        hotelName = $(control).parent().parent().siblings(".sel-hotel-name").find("a").text();
        corporateNameControl = $(control).parent().parent().siblings().find("div.hotel-booking-wrapper div.booking-code-container span.booking-code");
    }
    var isPublicRate = 1;
    var hotelId = $(control).data("hotelid");
    if ($(corporateNameControl).is(":visible")) {
        isPublicRate = 0;
    }
    $('input[id*="hidSelectedHotelId"]').val(hotelName + "," + hotelId + "," + isPublicRate);
    $('input[id*="hidProcess"]').val("SelectHotel");

    //$("form").append('<input type="hidden" name="process" value="SelectHotel />');
    $("form").submit();
}

function GetHotelsToDisplay(currentlyDisplayedHotels, noOfHotelsToDisplay, sortBy, clearExistingResults) {

    if (!stopGetHotelRequest) {
        if (scrollPos === 0) {
            scrollPos = $(window).scrollTop();
        }
        if (clearExistingResults) {
            $("ul.hotel-list").empty();
        }
        $('#ajax-loader').show();
        doNotshowLoader = true;
        requestFired = true;
        //artf1294664 : Mobile - Price issues on Iphone
        //var inputData = "{'currentlyDisplayedHotels':'" + currentlyDisplayedHotels + "','noOfHotelsToDisplay':'" + noOfHotelsToDisplay + "','sortBy':'" + sortBy + "'}";
        var curDateTime = new Date();
        var timeOut = getCurrentDateTime(curDateTime);
        var inputData = "{'currentlyDisplayedHotels':'" + currentlyDisplayedHotels + "','noOfHotelsToDisplay':'" + noOfHotelsToDisplay + "','sortBy':'" + sortBy + "','timeout':'" + timeOut + "'}";
        $.ajax({
            type: "POST",
            url: GetPageURL() + "/GetHotelsToDisplay",
            // Pass parameter, via JSON object.
            data: inputData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                ProcessAvailableHotels(data.d);
                setPinHeight();
                //$("a.book-btn").append("<span></span>");
            },
            error: function(data) {
                //alert("error retriving available hotel list.");
                StopLoader();
            }
        });
    }
}

function StopLoader() {
    doNotshowLoader = false;
    requestFired = false;
    $('#ajax-loader').hide();
}
function ProcessAvailableHotels(data) {
    var availableHotels;
    requestFired = false;
    //Check if error is returned or not
    if (data != null && data.length && data[0].Errors != undefined) {
        //Show Error message.
        if (currentlyRenderedHotels == 0) {
            //ShowErrorMessgae(data[0].Errors);
        }
    }
    else {
        //if error not returned then data is about searched hotel.
        availableHotels = data[0].HotelList;
    }

    if (availableHotels == null && data[0].MoreHotelsToFetch) {
        timeoutId = setTimeout(function() {
            clearTimeout(timeoutId);
            currentSortOption = $('select[id*="ddlSort"]').val();
            LoadRemainingHotels(true);
        }, 1000);
    }
    else if ((availableHotels != null && availableHotels.length) || data[0].MoreHotelsToFetch) {
        ResetControls(data[0]);
        if (!requestFired) {
            ShowMessage(data[0].Messages);
            //RemoveSortBy(data[0].RemoveSoryByDiscountType);
            CreateHotelist(availableHotels);
            var newHotelCount = availableHotels.length;
            currentlyRenderedHotels = currentlyRenderedHotels + availableHotels.length;
            hotelPageCount = hotelPageCount + newHotelCount;
            LoadRemainingHotels(data[0].MoreHotelsToFetch);
        }
    }
    else {
        onNoResultFound(data[0].Messages);
        //No hotels are available so donot fire any further get hotel request
        // stopGetHotelRequest = true;
    }
}

function onNoResultFound(messages) {
    StopLoader();
    ShowMessage(messages);
    $('a[id*="lnkBack"]').show();
}

function ResetControls(data) {
    var sortCntl = $('select[id*="ddlSort"]');
    var initialSortOption = "0";
    if (!data.IsAlternateSearch) {
        $(sortCntl).find('option[value="0"]').remove();

        if (firstTimeSearch) {
            if (data.RemoveSoryByDiscountType) {
                $(sortCntl).find('option[value="7"]').remove();
                initialSortOption = "3";
            }
            if (!data.RemoveSoryByDiscountType && data.IsPromoSearch) {
                initialSortOption = "7";
            }
            else {
                initialSortOption = "3";
                $(sortCntl).find('option[value="7"]').remove();
            }
            $(sortCntl).val(initialSortOption);
        }
    }
    else {
        if (data.SearchBy === 1) {
            $(sortCntl).find('option[value="0"]').remove();
        }

        if (firstTimeSearch) {
            if (data.SearchBy === 1) {
                initialSortOption = "1";
            }

            if (data.RemoveSoryByDiscountType) {
                $(sortCntl).find('option[value="7"]').remove();
                initialSortOption = "3";
            }
            if (!data.RemoveSoryByDiscountType && data.IsPromoSearch) {
                initialSortOption = "7";
            }
            else
                $(sortCntl).find('option[value="7"]').remove();

            $(sortCntl).val(initialSortOption);
            currentSortOption = initialSortOption;
            GetHotelsToDisplay(0, currentlyRenderedHotels, currentSortOption, true);
        }
    }

    if (firstTimeSearch) {
        $('div[id*="searchHotelMessage"]').text(data.SearchHeading);
        //Perform tracking
        getPageTrackingData();
    }

    firstTimeSearch = false;
    if (isRedemptionBooking === "1") {
        $(sortCntl).find('option[value="2"]').remove();
    }
    else {
        $(sortCntl).find('option[value="4"]').remove();
    }

    if (IsSearchfromCurrentLocation === "1") {
        $(sortCntl).find('option[value="3"]').remove();
    }
    else {
        $(sortCntl).find('option[value="5"]').remove();
    }
    $(sortCntl).show();
}
function LoadRemainingHotels(moreHotelsToFetch) {
    var hotelToBeLoaded = noOfHotelsToDisplay;
    
    if ((maxHotelsPerPage - hotelPageCount) <= noOfHotelsToDisplay) {
        hotelToBeLoaded = (maxHotelsPerPage - hotelPageCount);
    }

    if (hotelToBeLoaded > 0 && moreHotelsToFetch) {
        GetHotelsToDisplay(currentlyRenderedHotels, hotelToBeLoaded, currentSortOption, false);
    }
    else {
        if (maintainScrollPosition) {
            //$(window).scrollTop(scrollPos);
        }
        if (moreHotelsToFetch) {
            $('a[id*="hgcViewMoreResults"]').show();
            $('hr[id*="hrViewButton"]').show();            
        }
        else {
            $('a[id*="hgcViewMoreResults"]').hide();
            $('hr[id*="hrViewButton"]').hide();
        }
        scandic.responsive.initClick2CallLinks($('a.click2call'));
        StopLoader();
    }
}


function ViewMoreResults(control) {
    $(control).hide();
    hotelPageCount = 0;
    scrollPos = 0;
    maintainScrollPosition = true;
    LoadRemainingHotels(true);
}

function ShowMessage(data) {

    var errorBlock = $("p.information");
    $(errorBlock).empty();
    if (data !== null) {
        $(data).each(function(index) {
            $(errorBlock).append(this.MessageInfo);
        });
        //$(errorBlock).html(data[0].ErrorMessaage);
        if (data.length) {
            $(errorBlock).show();
        }
    }
}


function CreateHotelist(availableHotels) {
    var hotelList = $("ul.hotel-list");
    $(availableHotels).each(function(index) {
        $(hotelList).append(CreateHotelElement(this));
    });
}

function CreateHotelElement(hotelDetail) {
    var hotelListElement = $("div.hotelListElementTemplate").find("ul li");
    var hotelH3 = $(hotelListElement).find("div.sel-hotel-name");
    var nameLink = $(hotelH3).find("a");
    $(nameLink).text(hotelDetail.Name);
    $(nameLink).attr("data-hotelid", hotelDetail.Id);

    var hotelImageLink = $(hotelListElement).find("div.image div.polaroid a");
    $(hotelImageLink).attr("data-hotelid", hotelDetail.Id);
    var hotelImage = $(hotelImageLink).find("img");
    $(hotelImage).attr("src", hotelDetail.ImageUrl);
    $(hotelImage).attr("alt", hotelDetail.Name);

    var hotelContentElement = $(hotelListElement).find("div.select-hotel-content");
    var descrption = $(hotelContentElement).find("div.hotel-desc p");
    $(descrption).text(hotelDetail.Description);

    var mapLink = $(hotelContentElement).find("div.sel-hotel-addr div.addr-details span.hotel-address a.address");
    $(mapLink).attr("href", "http://maps.google.com/?q=" + hotelDetail.Address);
    $(mapLink).attr("data-hotelid", hotelDetail.Id);
    var address = $(mapLink).find("span.street-address");
    $(address).text(hotelDetail.Address);

    var distanceToCity = $(hotelContentElement).find("div.sel-hotel-addr div.addr-details span.hotel-distance");
    $(distanceToCity).text(hotelDetail.CityCenterDistance);
    if (hotelDetail.CurrentLocationDistance != "") {
        $(distanceToCity).text(hotelDetail.CurrentLocationDistance);
    }

    var tripAdvisorWidget = $(hotelContentElement).find("div.trip-advisor-wrapper div#divTripAdvisorRating");
    if (!hotelDetail.HideTARating) {
        $(tripAdvisorWidget).css('display', 'block');
        var tripAdvisorImage = $(tripAdvisorWidget).find("p.tripAdvisorImage img");
        $(tripAdvisorImage).attr("src", hotelDetail.TARatingImageURL);
        var tripAdvisorReviewsCountLink = $(tripAdvisorWidget).find("p a.tripReviewcount");
        $(tripAdvisorReviewsCountLink).attr("href", hotelDetail.TAReviewPopUpURL);
        $(tripAdvisorReviewsCountLink).text(hotelDetail.TAReviewCountText);
    }
    else {
        $(tripAdvisorWidget).css('display', 'none');
    }
    var hotelBookingElement = $(hotelContentElement).find("div.hotel-booking-wrapper");
    var bookingCodeContainer = $(hotelBookingElement).find("div.booking-code-container");
    var fromtext = $(hotelBookingElement).find("div.price-container span.price-info-text");
    $(fromtext).text(fromText);

    var bookingCode = $(bookingCodeContainer).find("span.booking-code");
    var noDiscount = $(hotelBookingElement).find("div.no-discount");

    var regularSearch;
    var promoSearch;
    var corpSearch;

    if (hotelDetail.PromoCode != '' && hotelDetail.PromoCode != undefined) {
        promoSearch = true;
    }
    if (hotelDetail.CorporateCode != '' && hotelDetail.CorporateCode != undefined && !hotelDetail.HasPromotionRoomRates) {
        corpSearch = true;
    }
    if (hotelDetail.PromoCode != '' && hotelDetail.PromoCode != undefined &&
        hotelDetail.CorporateCode != '' && hotelDetail.CorporateCode != undefined) {
        regularSearch = true;
    }

    if (promoSearch) {
        if (hotelDetail.HasPromotionRoomRates) {
            $(bookingCodeContainer).css('display', 'block');
            if ($(bookingCodeContainer).css('display') == 'block') {
                $(bookingCodeContainer).parent('.hotel-booking-wrapper').find('.price-container').addClass('promo-color');
            }            
            $(noDiscount).css('display', 'none');
            $(bookingCode).text(hotelDetail.CorporateCode);
        }
        else {
            $(bookingCodeContainer).css('display', 'none');
            if($(bookingCodeContainer).css('display') == 'none') {
                $(bookingCodeContainer).parent('.hotel-booking-wrapper').find('.price-container').removeClass('promo-color');
            }
            $(noDiscount).css('display', 'inline-block');
            $(noDiscount).text(noDiscountAvailableText);
            $(fromtext).text(fromText);
        }
    }
    else if (corpSearch) {
        $(bookingCodeContainer).css('display', 'block');
        $(noDiscount).css('display', 'none');
        $(bookingCode).text(hotelDetail.CorporateCode);
    }
    else {
        $(bookingCodeContainer).css('display', 'none');
    }

    $(fromtext).append('<span class="price">' + hotelDetail.AvailableRoomPrice + '</span>');

    var perNight = $(hotelBookingElement).find("div.price-container div.price-details span.price-desc");
    $(perNight).text(hotelDetail.PerNight);

    var bookHotelButton = $(hotelBookingElement).find("div.hotel-book-btn a.book-btn");
    $(bookHotelButton).text(bookText);
    $(bookHotelButton).append("<span></span>");
    $(bookHotelButton).attr("data-hotelid", hotelDetail.Id);

    return $(hotelListElement).outerHTML();
}

function trackFunction(control, functionId) {
    var hotelId = $(control).data("hotelid");
    return performFunctionTracking(functionId, hotelId);
}

function displayIframe(url) {
    $('#tripAdvisorIframe').empty();
    var hrefVal = url;
    $("#tripAdvisorIframe").append($("<iframe />").attr({ src: hrefVal, width: '100%', height: '600px', frameborder: '0', id: 'testing', scrolling: 'yes' }));
    showOverlay($('#overlay-tripiframe'), true);
}