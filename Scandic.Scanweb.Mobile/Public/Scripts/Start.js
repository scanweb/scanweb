﻿function GetCarouselContents() {
    var curDateTime = new Date();
    var timeOut = getCurrentDateTime(curDateTime);
    var inputData = "{'timeout':'" + timeOut + "'}";
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetCarouselData",
        data: inputData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            ProcessCarousalData(data.d);
        },
        error: function(data) {
            StopLoader();
        }
    });
}

$(document).ready(function() {
    GetCarouselContents();
});