﻿var currentlyRenderedRoomTypes = 0;
var roomTypePageCount = 0;
var scrollPos = 0;
var firstRequest = true;
var loadMoreRoomTypes = true;
$(document).ready(function() {
    initializePage();

    //Add Toggle
    $(".more-details").click(function(e) {
        $(".more-details-grid").toggle("slow", function() {
            if ($(".more-details-arrow").hasClass("down-active")) {
                $(".more-details-arrow").html("&#9654;").removeClass("down-active");
            }
            else {
                $(".more-details-arrow").html("&#9660;").addClass("down-active");
                setGridElemHeight();
            }
        });

        //$(".more-details-grid").toggle("slow");
        e.preventDefault();
    });
});

function initializePage() {
    $('a[id*="lnkBack"]').hide();
    $('a.view-more-results').click(function() {
        ViewMoreResults(this);
        return false;
    });
    //initialy just render first avaialable hotel
    GetRoomTypesToDisplay(currentlyRenderedRoomTypes, noOfRoomTypesToDisplay, false);
    //firstRequest = true;
    GetHotelOverviewDetails();
    GetSelectRateCarouselContents();
}

function trackFunction(control, functionId) {
    var Id = $(control).data("hotelid");
    if (functionId == "12")
        Id = $(control).data("rateid");
    return performFunctionTracking(functionId, Id);
}

function GetHotelOverviewDetails() {
    var curDateTime = new Date();
    var timeOut = getCurrentDateTime(curDateTime);
    var inputData = "{'timeout':'" + timeOut + "'}";
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetSelectRateHotelOverview",
        data: inputData,
        // Pass parameter, via JSON object.
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            ProcessHotelOverview(data.d);
        },
        error: function(data) {
            StopLoader();
            //alert("error retriving available room types.");
        }
    });
}

function GetSelectRateCarouselContents() {
    var curDateTime = new Date();
    var timeOut = getCurrentDateTime(curDateTime);
    var inputData = "{'timeout':'" + timeOut + "'}";
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetSelectRateCarouselData",
        data: inputData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            ProcessCarousalData(data.d);
        },
        error: function(data) {
            StopLoader();
        }
    });
}

function ProcessHotelOverview(hotelDetail) {
    if (hotelDetail != null) {
        var hotelInfoElement = $("div.hotel-info");
        var hotelName = $(hotelInfoElement).find("h2.sel-hotel-name");
        $(hotelName).text(hotelDetail.HotelName);
        //Tabs
        var tabInfo = $(hotelInfoElement).find("div.hotel-info-tabs");
        //Tab1
        var overviewTab = $(tabInfo).find("div#tab1");
        var hotelDesc = $(overviewTab).find("p.hotel-desc-txt");
        $(hotelDesc).text(hotelDetail.HotelDescription);
        var moreDetElem = $(overviewTab).find("span.more-details");
        $(hotelDesc).append(moreDetElem);
        //additional Info
        CreateHotelAdditionalInfo(hotelDetail.HotelFacilities, overviewTab);
        var leftCol = $("ul.two-col-grid li:nth-child(odd)");
        var rightCol = $("ul.two-col-grid li:nth-child(even)");
        $("ul.two-col-grid li:nth-child(4n+3)").addClass("grid-white-row");
        $("ul.two-col-grid li:nth-child(4n)").addClass("grid-white-row")
        $(leftCol).css("clear", "left");
        DisplayRewardNights(hotelDetail.RewardNights, overviewTab);

        var tripAdvisorWidget = $(overviewTab).find("div.trip-advisor-wrapper div#divTripAdvisorRating");
        if (!hotelDetail.HideTARating) {
            $(tripAdvisorWidget).css('display', 'block');
            var tripAdvisorImage = $(tripAdvisorWidget).find("p.tripAdvisorImage img");
            $(tripAdvisorImage).attr("src", hotelDetail.TARatingImageURL);
            var tripAdvisorReviewsCountLink = $(tripAdvisorWidget).find("p a.tripReviewcount");
            $(tripAdvisorReviewsCountLink).attr("href", hotelDetail.TAReviewPopUpURL);
            $(tripAdvisorReviewsCountLink).text(hotelDetail.TAReviewCountText);
        }
        else {
            $(tripAdvisorWidget).css('display', 'none');
        }
    }

}

function DisplayRewardNights(rewardNights, overviewTab) {
    var rewardNightContainer = $(overviewTab).find("div.reward-nights");
    if (rewardNights != null && rewardNights.length > 0) {
        $(rewardNightContainer).css('display', 'block');
        var rewardInfo = $("div.reward-night-wrapper-template");
        var rewardNightElement = $(rewardInfo).find("div.hotel-info-reward-night");
        var rewardPoints = $(rewardNightElement).find("span.reward-points-value");
        var campaignPoints = $(rewardNightElement).find("ul.reward-points-list");
        $(rewardInfo).attr("class", "reward-night-wrapper");
        if (rewardNights[0].IsCampaignPointsAvailable == true && rewardNights.length > 1) {
            //if (rewardNights[0].RewardPoints != '') {
            $(rewardNightElement).attr("class", "hotel-info-reward-night multiple-reward-points left");
            $(rewardPoints).css('display', 'none');
            $(campaignPoints).css('display', 'block');

            $(rewardNights).each(function(index) {
                var rewardNight = CreateRewardNightElement(this, campaignPoints);
                $(campaignPoints).append(rewardNight);
            });
        }
        else {
            $(rewardNightElement).attr("class", "hotel-info-reward-night single-reward-points left");
            $(rewardPoints).css('display', 'block');
            $(campaignPoints).css('display', 'none');
            $(rewardPoints).text(rewardNights[0].RewardPoints + ' ' + pointsText);
        }
        $(rewardInfo).show();
        var htmlToReturn = $(rewardInfo).outerHTML();
        $(rewardInfo).attr("class", "reward-night-wrapper-template");
        $(rewardInfo).hide();
        $(campaignPoints).children().remove();
        $(rewardNightContainer).append(htmlToReturn);
    }
    else {
        $(rewardNightContainer).css('display', 'none');
    }
}

function CreateRewardNightElement(rewardNight, campaignPoints) {
    var rewardDates;
    if (rewardNight.StartDate != undefined && rewardNight.EndDate != undefined && rewardNight.StartDate != '' && rewardNight.EndDate != '') {
        rewardDates = ' (' + rewardNight.StartDate + ' - ' + rewardNight.EndDate + ')';
    }
    if (rewardDates != '' && rewardDates != undefined)
        $(campaignPoints).append('<li>' + rewardNight.RewardPoints + ' ' + pointsText + rewardDates + '</li>');
    else
        $(campaignPoints).append('<li>' + rewardNight.RewardPoints + ' ' + pointsText + '</li>');
}

function CreateHotelAdditionalInfo(info, overviewTab) {
    var moreDetailsElement = $(overviewTab).find("div.more-details-grid ul.two-col-grid");
    var noofRooms = $(moreDetailsElement).find("li#noofRoomsValue");
    $(noofRooms).text(info.NoOfRooms);
    var nonSmokingRooms = $(moreDetailsElement).find("li#nonSmokingRoomsValue");
    $(nonSmokingRooms).text(info.NonSmokingRooms);
    var roomsForDisabled = $(moreDetailsElement).find("li#roomsForDisabledValue");
    $(roomsForDisabled).text(info.RoomsForDisabled);
    var relaxCenter = $(moreDetailsElement).find("li#relaxCenterValue");
    $(relaxCenter).text(info.RelaxCenter);
    var restaurantBar = $(moreDetailsElement).find("li#restaurantBarValue");
    $(restaurantBar).text(info.RestaurantBar);
    var garage = $(moreDetailsElement).find("li#garageValue");
    $(garage).text(info.Garage);
    var outdoorParking = $(moreDetailsElement).find("li#outdoorParkingValue");
    $(outdoorParking).text(info.OutdoorParking);
    var shop = $(moreDetailsElement).find("li#shopValue");
    $(shop).text(info.Shop);
    var meeting = $(moreDetailsElement).find("li#meetingFacilitiesValue");
    $(meeting).text(info.MeetingFacilities);
    var ecoLabelled = $(moreDetailsElement).find("li#EcoLabeledValue");
    $(ecoLabelled).text(info.EcoLabeled);
    var citycenter = $(moreDetailsElement).find("li#CityCenterDistanceValue");
    $(citycenter).text(info.CityCenterDistance);
    var airport = $(moreDetailsElement).find("li#Airport1Name");
    $(airport).append(info.Airport1Name);
    var airportDistance = $(moreDetailsElement).find("li#Airport1DistanceValue");
    $(airportDistance).text(info.Airport1Distance);
    var trainDistance = $(moreDetailsElement).find("li#DistanceTrainValue");
    $(trainDistance).text(info.DistanceTrain);
    var wireless = $(moreDetailsElement).find("li#WirelessInternetValue");
    $(wireless).text(info.WirelessInternet);
}

function displayIframe(url) {
    $('#tripAdvisorIframe').empty();
    var hrefVal = url;
    $("#tripAdvisorIframe").append($("<iframe />").attr({ src: hrefVal, width: '100%', height: '600px', frameborder: '0', id: 'testing', scrolling: 'yes' }));
    showOverlay($('#overlay-tripiframe'), true);
}

function displayMoreDetails() {
    var tabInfo = $("div.hotel-info div.hotel-info-tabs");
    var overviewTab = $(tabInfo).find("div#tab1");
    var moreDetailsElement = $(overviewTab).find("div.more-details-grid");

}

$('ul.tabs').each(function() {
    var $active, $content, $links = $(this).find('a');

    $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
    $active.addClass('active');
    $content = $($active.attr('href'));

    $links.not($active).each(function() {
        $($(this).attr('href')).hide();
    });
    var roomratesElement = $("div.rooms-info");
    roomratesElement.show();
    $("a[id*=hgcViewMoreResults]").show();
    $('a[id*="hgcViewMoreResults"]').append("<span></span>");
    $(this).on('click', 'a', function(e) {
        $active.removeClass('active');
        $content.hide();
        $active = $(this);
        StopLoader();
        if ($active.attr('href') == '#tab1') {
            roomratesElement.show();
            if(loadMoreRoomTypes)
                $("a[id*=hgcViewMoreResults]").show();
        }
        if ($active.attr('href') == '#tab3') {
            roomratesElement.hide();
            $("a[id*=hgcViewMoreResults]").hide();
            var hotelInfoElement = $("div.hotel-info");
            var tabInfo = $(hotelInfoElement).find("div.hotel-info-tabs");
            //Tab3
            var offersTab = $(tabInfo).find("div#tab3");
            if ($.trim($(offersTab).html()) == '')
                GetOffersToDisplay();
        }
        if ($active.attr('href') == '#tab2') {
            roomratesElement.hide();
            $("a[id*=hgcViewMoreResults]").hide();
            GetHotelLocationToDisplay();
        }
        $content = $($(this).attr('href'));
        $active.addClass('active');
        $content.show();
        e.preventDefault();
    });
});

function GetOffersToDisplay() {
    var curDateTime = new Date();
    var timeOut = getCurrentDateTime(curDateTime);
    var inputData = "{'timeout':'" + timeOut + "'}";
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetSelectRateHotelOffersDetails",
        // Pass parameter, via JSON object.
        data: inputData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            ProcessHotelOffers(data.d);
        },
        error: function(data) {
            StopLoader();
            //alert("error retriving available room types.");
        }
    });
}

function GetHotelLocationToDisplay() {
    var curDateTime = new Date();
    var timeOut = getCurrentDateTime(curDateTime);
    var inputData = "{'timeout':'" + timeOut + "'}";
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetSelectRateHotelLocationDetails",
        // Pass parameter, via JSON object.
        data: inputData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            ProcessHotelLocationDetails(data.d);
        },
        error: function(data) {
            StopLoader(); //alert("error retriving available room types.");
        }
    });
}

function ProcessHotelLocationDetails(hotelLocation) {
    var hotelInfoElement = $("div.hotel-info");
    var tabInfo = $(hotelInfoElement).find("div.hotel-info-tabs");
    var locationTab = $(tabInfo).find("div#tab2");
    var mapLink = $(locationTab).find("div.sel-hotel-addr div.addr-details span.hotel-address a.address");
    $(mapLink).attr("href", "http://maps.google.com/?q=" + hotelLocation.HotelAddress);
    $(mapLink).attr("data-hotelid", hotelLocation.Id);
    var address = $(mapLink).find("span.street-address");
    $(address).text(hotelLocation.HotelAddress);
    var phoneLink = $(locationTab).find("div.call-reservation a.phone");
    $(phoneLink).attr("data-hotelid", hotelLocation.Id);
    var distanceToCity = $(locationTab).find("div.sel-hotel-addr div.addr-details span.hotel-distance");
    $(distanceToCity).text(hotelLocation.DistanceFromCityCenter);
    //    if (hotelDetail.CurrentLocationDistance != "") {
    //        $(distanceToCity).text(hotelDetail.CurrentLocationDistance);
    //    }
    setPinHeight();
    //Initializing Google Map. Yjis initialize function is available in Google Map Control
    initialize();
}
function setPinHeight() {
    var addrContH = $('.sel-hotel-addr').height();
    $(".addr-pin").css("height", addrContH);
}
function ProcessHotelOffers(hotelOffers) {
    var hotelInfoElement = $("div.hotel-info");
    var tabInfo = $(hotelInfoElement).find("div.hotel-info-tabs");
    var offerTab = $(tabInfo).find("div#tab3");
    $(hotelOffers).each(function(index) {
        var offerText = CreateOfferElement(this);
        $(offerTab).append(offerText);
    });
    //setOffersArrow();
    setTimeout(setOffersArrow, 400)
}

function CreateOfferElement(offer) {
    var offerElement;
    if (offer.NoOfferMessage != null && offer.NoOfferMessage != "") {
        offerElement = $("div.offers-wrapper div.noOfferMessage");
        $(offerElement).text(offer.NoOfferMessage);
    }
    else {
        offerElement = $("div.offers-wrapper div.offer-box");
        var offerURL = $(offerElement).find('a');
        //$(offerURL).attr("href", offer.OfferPageURL);
        $(offerURL).attr("onclick", "loadSelectedOffer('" + offer.OfferPageURL + "')");
        var offerContent = $(offerElement).find("div.offer-content");
        var offerTitle = $(offerContent).find("div.offer-title");
        $(offerTitle).text(offer.OfferTitle);
        var offerImage = $(offerContent).find("div.offer-img img");
        $(offerImage).attr("src", offer.OfferImageURL);
        $(offerImage).attr("alt", offer.OfferTitle);
        var offerDesc = $(offerContent).find("div.offer-desc span.ellipsis_text");
        $(offerDesc).text(offer.OfferDescription);
        var offerPriceText = $(offerContent).find("div.offer-spl");
        $(offerPriceText).text(offer.OfferPriceText);
    }
    var htmlToReturn = $(offerElement).outerHTML();
    $(offerContent).show();
    return htmlToReturn;
}

function loadSelectedOffer(offerUrl) {
    var selectedOfferForm = $('#selectedHotelOffer');
    $(selectedOfferForm).attr('action', offerUrl);
    $('#pageName').val('SelectRateOffer');
    $(selectedOfferForm).submit();
}

function setOffersArrow() {
    $(".offer-arrow").each(function() {
        var olContentH = $(this).siblings(".offer-content").height();
        $(this).css("height", olContentH);
    });
}
function GetRoomTypesToDisplay(currentlyRenderedRoomTypes, noOfRoomTypesToDisplay, clearExistingResults) {

    if (scrollPos === 0) {
        scrollPos = $(window).scrollTop();
    }
    if (clearExistingResults) {
        $("ul.room-list").empty();
    }
    //artf1294664 : Mobile - Price issues on Iphone
    //var inputData = "{'currentlyRenderedRoomTypes':'" + currentlyRenderedRoomTypes + "','noOfRoomTypesToDisplay':'" + noOfRoomTypesToDisplay + "'}";
    var curDateTime = new Date();
    var timeOut = getCurrentDateTime(curDateTime);
    var inputData = "{'currentlyRenderedRoomTypes':'" + currentlyRenderedRoomTypes + "','noOfRoomTypesToDisplay':'" + noOfRoomTypesToDisplay + "','timeout':'" + timeOut + "'}";
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetRoomsTypesToDisplay",
        // Pass parameter, via JSON object.
        data: inputData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            ProcessAvailableRoomTypes(data.d);
        },
        error: function(data) {
            StopLoader(); //alert("error retriving available room types.");
        }
    });
}

function ProcessAvailableRoomTypes(data) {

    var availableRoomTypes;
    //Check if error is returned or not
    if (data != null && data.length && data[0].Errors != null && data[0].Errors.length) {
        //Show Error message.
        if (currentlyRenderedRoomTypes == 0) {
            //ShowErrorMessgae(data[0].Errors);
        }
    }
    else {
        if (data[0].IsAlternateSearch) {
            location.href = data[0].AlternativeSearchUrl;
            return true;
        }
        else {
            //if error not returned then data is about searched hotel.
            availableRoomTypes = data[0].RoomTypes;
        }
    }

    if (firstRequest) {
        //Perform tracking
        getPageTrackingData();
    }

    firstRequest = false;

    if ((availableRoomTypes != null && availableRoomTypes.length) || data[0].MoreRoomTypesToFetch) {
        showHideBodyOnNoResult("show");
        ShowMessage(data[0].Messages);
        CreateRoomTypeListNew(availableRoomTypes);
        currentlyRenderedRoomTypes = currentlyRenderedRoomTypes + availableRoomTypes.length;
        roomTypePageCount = roomTypePageCount + availableRoomTypes.length;
        LoadRemainingRoomTypes(data[0].MoreRoomTypesToFetch);
    }
    else {
        showHideBodyOnNoResult("hide")
        onNoResultFound(data[0].Messages);
        StopLoader(); //No rooms are available so donot fire any further get hotel request
        // stopGetHotelRequest = true;
    }

}

function showHideBodyOnNoResult(value) {
    var tabs = $("div.select-rate-wrapper");
    if (value == "show") {
        $(tabs).show();
        $('a[id*="hgcViewMoreResults"]').show();
    }
    else {
        $(tabs).hide();
        $('a[id*="hgcViewMoreResults"]').hide();
    }
}


function onNoResultFound(messages) {
    ShowMessage(messages);
    $('a[id*="lnkBack"]').show();
}

function ShowMessage(data) {

    var errorBlock = $("p.information");
    $(errorBlock).empty();
    if (data !== null) {
        $(data).each(function(index) {
            $(errorBlock).append(this.MessageInfo);
        });
        //$(errorBlock).html(data[0].ErrorMessaage);
        if (data.length) {
            $(errorBlock).show();
        }
    }
}

function LoadRemainingRoomTypes(moreRoomTypesToFetch) {

    var roomTypesToBeLoaded = noOfRoomTypesToDisplay;
    if ((maxRoomTypesPerPage - roomTypePageCount) < noOfRoomTypesToDisplay) {
        roomTypesToBeLoaded = (maxRoomTypesPerPage - roomTypePageCount);
    }

    if (roomTypesToBeLoaded > 0 && moreRoomTypesToFetch) {
        GetRoomTypesToDisplay(currentlyRenderedRoomTypes, roomTypesToBeLoaded, false);
    }
    else {
        if (moreRoomTypesToFetch) {
            loadMoreRoomTypes = true;
            $('a[id*="hgcViewMoreResults"]').show();
        }
        else {
            loadMoreRoomTypes = false;
            $('a[id*="hgcViewMoreResults"]').hide();
        }
    }
}

function ViewMoreResults(control) {
    $(control).hide();
    roomTypePageCount = 0;
    scrollPos = 0;
    LoadRemainingRoomTypes(true);
}

function CreateRoomTypeListNew(availableRoomTypes) {
    var roomTypeListContainerElement = $("div.rooms-info");
    $(availableRoomTypes).each(function(index) {
        $(roomTypeListContainerElement).append(renderRoomTypes(this));
    });
}


function renderRoomTypes(roomType) {
    var roomList = $("div.rooms-list ul.rooms-list");
    var roomTypeList = $(roomList).find("li.room-type"); //
    var roomTypeName = $(roomTypeList).find("div.room-name");
    $(roomTypeName).text(roomType.Name);
    $(roomTypeName).attr("data-roomtypeid", roomType.Id);
    var roomTypeImage = $(roomTypeList).find("div.room-image img");
    $(roomTypeImage).attr("src", roomType.ImagePath);
    $(roomTypeImage).attr("alt", roomType.Name);
    var roomTypeDesc = $(roomTypeList).find("div.room-desc");
    $(roomTypeDesc).text(roomType.Description);

    //Prices
    var rateDiv = $(roomTypeList).find("ul.rooms-price-container");
    //Clear previous data
    $(rateDiv).children().remove();
    //Render Special Rate
    var rateIndex = renderRateTypesNew(roomType, rateDiv, 2, 0);
    //Render early rate
    rateIndex = renderRateTypesNew(roomType, rateDiv, 1, rateIndex);
    //Render flex rate
    renderRateTypesNew(roomType, rateDiv, 0, rateIndex);
    var htmlToReturn = $(roomList).outerHTML();
    $(roomList).show();
    return htmlToReturn;
}

function renderRateTypesNew(roomType, rateDiv, type, startIndex) {
    var rateIndex = startIndex;
    $(roomType.Rates).each(function(index) {
        if (this.Type === type) {
            renderRateNew(rateDiv, this, rateIndex, roomType.PerNight);
            rateIndex++;
        }
    });
    if (rateIndex == startIndex) {
        renderRateNew(rateDiv, null, rateIndex, null);
        rateIndex++;
    }
    return rateIndex;
}
function renderRateNew(rateContainer, rate, rateIndex, perNightText) {
    $(rateContainer).append(CreateRateListNew(rate, rateIndex, perNightText));
    var rateControl = $(rateContainer).find("li." + GetCSSNew(rateIndex));
    $(rateContainer).find('div[class=="templatePriceMarkup"]').remove();
    var rateValue = rate !== null ? rate.Rate : null;
    RemoveElementsIfNoRateNew(rateControl, rateValue);
    $(rateControl).show();
}

function RemoveElementsIfNoRateNew(control, rate) {
    if (rate === null || rate === undefined || rate === "") {
        $(control).children().remove();
    }
}

function CreateRateListNew(rateType, index, perNightText) {
    var templateElement = $("div.templatePriceMarkup li.templateBooking-section");
    $(templateElement).attr("class", "booking-section " + GetCSSNew(index));
    if (rateType !== null) {
        var rateName = $(templateElement).find("div.booking-type");
        $(rateName).text(rateType.Name);
        var priceDetail = $(templateElement).find("div.price-details span.price");
        $(priceDetail).text(rateType.Rate);
        var perRoomNight = $(templateElement).find("div.price-details span.price-desc");
        $(perRoomNight).text(perNightText);
        //Book Button
        var bookButton = $(templateElement).find("div.hotel-book-btn a.book-btn");
        $(bookButton).text(bookNowText);
        $(bookButton).attr("data-price", rateType.Rate);
        $(bookButton).attr("data-ratename", rateType.Name);
        $(bookButton).attr("data-rateid", rateType.Id);
        $(bookButton).attr("data-holdtill6", rateType.HoldGuranteeAvailable);
        $(bookButton).attr("data-guranteetypecode", rateType.CreditCardGuranteeType);
        $(bookButton).append("<span></span>");
    }
    var htmlToReturn = $(templateElement).outerHTML();
    $(templateElement).attr("class", "templateBooking-section");
    return htmlToReturn;
}
function setGridElemHeight() {
    $("ul.two-col-grid li:nth-child(4n+3)").each(function() {
        var whiteRowHeight = $(this).height();
        $(this).next().height(whiteRowHeight);
    });
    $("ul.two-col-grid li:nth-child(4n+1)").each(function() {
        var greyRowHeight = $(this).height();
        $(this).next().height(greyRowHeight);
    });
}
function SubmitSelectedRate(control) {
    var rate = $(control).data("price"); //text(); //only numeric val (price)
    var rateTypeId = $(control).data("rateid");
    var rateType = $(control).data("ratename"); //$(control).siblings("div").find("p span span.rateName").text(); //rate name
    var holdTill6PM = $(control).data("holdtill6");
    var guranteeTypeCode = $(control).data("guranteetypecode");
    var roomTypeElement = $(control).parent().parent().parent().siblings("div.room-name");
    var roomType = $(roomTypeElement).text(); //actual room name
    var roomTypeId = $(roomTypeElement).data("roomtypeid"); //room type id

    var selectValue = rate + "," + rateTypeId + "," + holdTill6PM + "," + guranteeTypeCode + "," + roomType + "," + roomTypeId + "," + rateType;
    $('input[id*="hidValue"]').val(selectValue);
    $('input[id*="hidProcess"]').val("SelectRate");
    $("#aspnetForm").submit();
}

function GetCSSNew(index) {
    switch (index) {
        case 0:
            return "promo-color";
            break;
        case 1:
            return "save-color";
            break;
        case 2:
            return "flex-color";
            break;
    }
}

function StopLoader() {
    doNotshowLoader = false;
    requestFired = false;
    $('#ajax-loader').hide();
}

/*function CreateRoomTypeList(availableRoomTypes) {
var roomTypeList = $("ul.room-list");
$(availableRoomTypes).each(function(index) {
$(roomTypeList).append(CreateRoomTypeElement(this));
});
}

function CreateRoomTypeElement(roomType) {
var roomTypeListElement = $("div.roomListElementTemplate").find("ul li");
var roomTypeImage = $(roomTypeListElement).find("div.image div.polaroid img");
$(roomTypeImage).attr("src", roomType.ImagePath);
$(roomTypeImage).attr("alt", roomType.Name);
var roomTypeH3 = $(roomTypeListElement).find("div.info div.first h3.roomType");
var nameLink = $(roomTypeH3).find("a");
$(nameLink).text(roomType.Name);
$(nameLink).attr("data-roomtypeid", roomType.Id);
var roomTypeDesc = $(roomTypeListElement).find("div.info div.first p");
$(roomTypeDesc).text(roomType.Description);
var rateDiv = $(roomTypeListElement).find("div.info");

//Render special rate
var rateIndex = renderRateTypes(roomType, rateDiv, 2, 0);
//Render early rate
rateIndex = renderRateTypes(roomType, rateDiv, 1, rateIndex);
//Render flex rate
renderRateTypes(roomType, rateDiv, 0, rateIndex);

var htmlToReturn = $(roomTypeListElement).outerHTML();
$(roomTypeListElement).find('div.info div[class!="first"]').remove();
return htmlToReturn;
}

function renderRateTypes(roomType, rateDiv, type, startIndex) {
var rateIndex = startIndex;
$(roomType.Rates).each(function(index) {
if (this.Type === type) {
renderRate(rateDiv, this, rateIndex, roomType.PerNight);
rateIndex++;
}
}); function CreateRoomTypeList(availableRoomTypes) {
var roomTypeList = $("ul.room-list");
$(availableRoomTypes).each(function(index) {
$(roomTypeList).append(CreateRoomTypeElement(this));
});
}
if (rateIndex == startIndex) {
renderRate(rateDiv, null, rateIndex, null);
rateIndex++;
}
return rateIndex;
}

function renderRate(rateContainer, rate, rateIndex, perNightText) {
$(rateContainer).append(CreateRateList(rate, rateIndex, perNightText));
var rateControl = $(rateContainer).find("div." + GetCSS(rateIndex));
var rateValue = rate !== null ? rate.Rate : null;
RemoveElementsIfNoRate(rateControl, rateValue);
$(rateControl).show();
}

function RemoveElementsIfNoRate(control, rate) {
if (rate === null || rate === undefined || rate === "") {
$(control).children().remove();
}
}

function CreateRateList(rateType, index, perNightText) {

var templateElement = $("div.rateType");
$(templateElement).attr("class", GetCSS(index));
if (rateType !== null) {
var rateName = $(templateElement).find("div p span span.rateName");
$(rateName).text(rateType.Name);
var rateButton = $(templateElement).find("a.submit");
$(rateButton).text(rateType.Rate);
$(rateButton).addClass(rateType.Color);
$(rateButton).attr("data-rateid", rateType.Id);
$(rateButton).attr("data-holdtill6", rateType.HoldGuranteeAvailable);
$(rateButton).attr("data-guranteetypecode", rateType.CreditCardGuranteeType);
var toolTip = $(templateElement).find("div p span.tooltip a.tooltip");
$(toolTip).attr("data-target", "rateType" + rateType.Id);
$(toolTip).attr("data-overlaytype", "ratetooltip");
$(toolTip).attr("data-value", rateType.Id);
$(templateElement).find("em").text(perNightText);
}
var htmlToReturn = $(templateElement).outerHTML();

$(rateButton).attr("class", "button submit");
$(templateElement).attr("class", "rateType");
return htmlToReturn;

}

function GetCSS(index) {
switch (index) {
case 0:
return "second";
break;
case 1:
return "third";
break;
case 2:
return "fourth";
break;
}
}*/