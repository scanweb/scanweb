if (typeof (scandic) === 'undefined') { scandic = {}; }

scandic.mobile = (function ($) {
	
	function initAjaxLoader() {
    	$(document.body).ajaxStart(function() 
    	{ 
	        $('#ajax-loader').show();
	    }).ajaxStop(function()  {
	        $('#ajax-loader').hide();
	    });
    }
    
    return {
    	initAjaxLoader : initAjaxLoader
	}  
})(jQuery);

// Initialize
jQuery().ready(function() {
	scandic.mobile.initAjaxLoader();
});