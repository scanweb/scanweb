﻿
function CancelRoomDetails(reservationNumber, legNumber, firstName, lastName, emailAddress, phoneNumber, hotelCode, searchTypeCode) {
    this.ReservationNumber = reservationNumber;
    this.LegNumber = legNumber;
    this.FirstName = firstName;
    this.LastName = lastName;
    this.EmailAddress = emailAddress;
    this.PhoneNumber = phoneNumber;
    this.HotelCode = hotelCode;
    this.SearchTypeCode = searchTypeCode;
}

function CancelRoom(cancellationControlId) {
    var control = $(cancellationControlId);
    var canceRoomDetails = GetCancelRoomDetails(control);    
    var jsonData = $.toJSON(canceRoomDetails);
    var currentLanguage = $('input[id*="hidCurrentLanguage"]').val();
    jsonData = "{cancellationDetails:" + jsonData + ",currentLanguage:'" + currentLanguage + "'}";    
    closeOverlay();
    var disableCover = $('#disable-cover');
    disableCover.show();
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/CancelRoom",
        // Pass parameter, via JSON object.
        data: jsonData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            processRoomCancellation(canceRoomDetails, data.d);
        },
        error: function(data) {
            disableCover.hide();
        }
    });
    return false;
}

function processRoomCancellation(canceRoomDetails, data) {

    if (data[0].Key === "InvalidSession") {
        location.href = data[0].Value;
    }
    else {
        var targetDiv = "cancelledRoom" + canceRoomDetails.ReservationNumber + "_" + canceRoomDetails.LegNumber;
        //Track cancellation
        getPageTrackingDataWithParameter(15, canceRoomDetails.ReservationNumber);
        //Show cancellation details
        AddOverlayOverride(data[0].Value, targetDiv, false);
    }
    
}
function onRoomCancellation(returnUrl) {
    closeOverlay();
    if ($('a[id*="lnkCancelRoomBooking"]').length > 1) {
        location.href = location.href;
    }
    else {
        location.href = returnUrl;
    }
    return false;
}
function CancelBooking() {
    var cancellationControls = $('a[id*="lnkCancelRoomBooking"]');
    var cancellationDetails = new Array();

    $(cancellationControls).each(function(index) {
        var isCancellable = ($(this).data("cancellable") === "False") ? false : true;
        if (isCancellable) {
            var canceRoomDetails = GetCancelRoomDetails(this);
            cancellationDetails[cancellationDetails.length] = canceRoomDetails;
        }
    });
    var jsonData = $.toJSON(cancellationDetails);
    var currentLanguage = $('input[id*="hidCurrentLanguage"]').val();
    jsonData = "{cancellationDetails:" + jsonData + ",currentLanguage:'" + currentLanguage + "'}";
    closeOverlay();
    var disableCover = $('#disable-cover');
    disableCover.show();
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/CancelCompleteBooking",
        // Pass parameter, via JSON object.
        data: jsonData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            onCancelBooking(data.d);
        },
        error: function(data) {
            disableCover.hide();
        }
    });
    return false;
}

function onCancelBooking(data) {

    if (data[0].Key === "InvalidSession") {
        location.href = data[0].Value;
    }
    else {
        var form = $("form#cancelConfirmationForm");

        $("#" + data[0].Key).val(data[0].Value);
        $("#" + data[1].Key).val(data[1].Value);
        $("#" + data[3].Key).val(data[3].Value);
        $(form).attr("action", data[2].Value);
        $(form).submit();
    }

}
function GetCancelRoomDetails(cancelControl) {
    var cancelControlId = new String($(cancelControl).attr("id"));
    var baseId = cancelControlId.substring(0, cancelControlId.indexOf("lnkCancelRoomBooking", 0));
    var firstName = $("#" + baseId + "hgcCustomerFirstNameValue").text();
    var lastName = $("#" + baseId + "hgcCustomerLastNameValue").text();
    var emailAddress = $("#" + baseId + "hgcCustomerEmailValue").text();
    var phoneNumber = $("#" + baseId + "hgcCustomerPhoneValue").text();
    var roomReservationNo = new String($(cancelControl).data("reservationnumber"));
    var hotelCode = new String($(cancelControl).data("hotelcode"));
    var searchTypeCode = new String($(cancelControl).data("searchtypecode"));
    var array = roomReservationNo.split("-");
    var reservationNumber = array[0];
    var legNumber = array[1];
    var cancelRoomDetails = new CancelRoomDetails(reservationNumber, legNumber, firstName, lastName, emailAddress, phoneNumber, hotelCode, searchTypeCode);
    return cancelRoomDetails;

}
function GetConfirmMessageOverlay(isCancellable, reservationNumber, policyCode, roomHeadingText, targetDiv, cancellationControlId) {
    var inputData = "{'isCancellable':" + isCancellable + ", 'reservationNumber':'" + reservationNumber + "', 'policyCode':'" + policyCode + "', 'roomHeadingText':'" + roomHeadingText + "', 'cancellationControlId':'" + cancellationControlId + "'}";
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetConfirmMessageOverlay",
        // Pass parameter, via JSON object.
        data: inputData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            AddOverlay(data.d, targetDiv);
        },
        error: function(data) {
        }
    });
}

function GetCancelBookingOverlay(isCancellable, targetDiv) {
    var inputData = "{'isCancellable':" + isCancellable + "}";
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetCancelBookingOverlay",
        // Pass parameter, via JSON object.
        data: inputData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            AddOverlay(data.d, targetDiv);
        },
        error: function(data) {
        }
    });
}

function ConfirmCancellation(control) {
    var isCancellable = ($(control).data("cancellable") === "False") ? false : true;
    var reservationNumber = $(control).data("reservationnumber");
    var policyCode = $(control).data("policycode");
    var roomHeading = $(control).data("roomheading");
    var targetDiv = "confirm" + reservationNumber;
    var overlayDiv = $("#" + targetDiv);
    var fireAjax = true;

    if (overlayDiv.length) {
        var isCachable = $(overlayDiv).data("iscachable");
        if (isCachable == "True") {
            fireAjax = false;
        }
    }

    if (fireAjax) {
        GetConfirmMessageOverlay(isCancellable, reservationNumber, policyCode, roomHeading, targetDiv, $(control).attr("id"));
    }
    else {
        showOverlay($(overlayDiv));
    }
    return false;
}

function ConfimCompleteBookingCancellation(control) {
    var isCancellable = ($(control).data("cancellable") === "False") ? false : true;
    var targetDiv = "confirmBookingCancellation";
    var overlayDiv = $("#" + targetDiv);
    var fireAjax = true;

    if (overlayDiv.length) {
        var isCachable = $(overlayDiv).data("iscachable");
        if (isCachable == "True") {
            fireAjax = false;
        }
    }

    if (fireAjax) {
        GetCancelBookingOverlay(isCancellable, targetDiv);
    }
    else {
        showOverlay($(overlayDiv));
    }
    return false;
}