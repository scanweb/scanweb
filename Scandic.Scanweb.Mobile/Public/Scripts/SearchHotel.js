﻿$().ready(function() {
    IntializeControls();
    scandicmobile.init();
    CreatePostbackParams();
});

function CreatePostbackParams() {
    if (document.forms[0].__EVENTTARGET == null || document.forms[0].__EVENTARGUMENT == null) {
        var lmTarget = document.createElement("INPUT");
        lmTarget.name = "__EVENTTARGET";
        lmTarget.id = "__EVENTTARGET";
        lmTarget.type = "hidden";

        var lmArgument = document.createElement("INPUT");
        lmArgument.name = "__EVENTARGUMENT";
        lmArgument.id = "__EVENTARGUMENT";
        lmArgument.type = "hidden";

        document.forms[0].appendChild(lmTarget);
        document.forms[0].appendChild(lmArgument);
    }

}

function IntializeControls() {
    InitializeDates();
    IntializeBookingOffer();
    SetBookingCodeState($('select[id*="ddlOffer"]')[0]);
    RetrieveAutocompleteDataSource();
    $('a[id*="lbtnSearch"]').click(function() {
        var isDatePickerAllowed = scandic.responsive.showDatePicker();
        if (!isDatePickerAllowed) {
            $('input[id*="hidDateOfArrival"]').val($(".dateOfArrival").val());
            $('input[id*="hidDateOfDeparture"]').val($(".dateOfDeparture").val());
        }
    });

}

function RetrieveAutocompleteDataSource() {
    var curDateTime = new Date();
    var timeOut = getCurrentDateTime(curDateTime);
    var inputData = "{'timeout':'" + timeOut + "'}";
    $.ajax({
        type: "POST",
        url: GetPageURL() + "/GetSearchDestinationList",
        data: inputData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            InitializeAutocompleteControl(data.d);
        },
        error: function(data) {
            // alert("error retriving search destination source.");
        }

    });
}

function InitializeAutocompleteControl(datasource) {
    scandic.responsive.initAutoComplete($('input[id*="txtSearch"]'), datasource);
}

function IntializeBookingOffer() {
    $('select[id*="ddlOffer"]').change(function() {
        SetBookingCodeState(this);
    });
}

function SetBookingCodeState(control) {
    if ($(control).val() == 2) {
        $('[id*="fsbookingCode"]').show();
    }
    else {
        $('[id*="fsbookingCode"]').hide();
    }
}

function formatDate(value) {
    var month = value.getMonth() + 1;
    var strMonth = month > 9 ? month.toString() : "0" + month.toString();
    var day = value.getDate();
    var strDay = day > 9 ? day.toString() : "0" + day.toString();

    return value.getFullYear() + "/" + strMonth + "/" + strDay;

}
//Date formatter for IOS5
function getISODate(date) {
    // zero-padding function
    var s = function(a, b) { return (1e15 + a + "").slice(-b) };

    // return ISO datetime
    return date.getFullYear() + '-' +
	        s(date.getMonth() + 1, 2) + '-' +
	        s(date.getDate(), 2);
}

var oneDayInMs = 86400000;
var today = new Date();
var tomorrow = new Date();
tomorrow.setDate(today.getDate()+1);
var isToday = today.toLocaleDateString();
var isTomorrow = tomorrow.toLocaleDateString();

function InitializeDates() {
    var isDatePickerAllowed = scandic.responsive.showDatePicker();
    var dateOfArrival = $(".dateOfArrival");
    var dateOfDeparture = $(".dateOfDeparture");
    if (isDatePickerAllowed) {
        var endDateOfArrival = new Date((dateOfArrival).data("enddateofarrival"));
        var startDateofArrival = new Date();
        var valueDate = new Date();

        $(dateOfArrival).glDatePicker({
            startDate: startDateofArrival,
            endDate: endDateOfArrival,
            allowOld: false,
            onChange: function(target, newDate) {
                $(target).attr('readonly', false);
                var nwDate = newDate.toLocaleDateString();
                if (nwDate == isToday) {
                    if (nwDate.toString().split(",").length == 3) {
                        target.val(todayString + ", " + nwDate.split(",")[1] + "," + nwDate.split(",")[2]);
                    }
                    else {
                        target.val(todayString + ", " + nwDate);
                    }
                }
                else if (nwDate == isTomorrow) {
                    if (nwDate.toString().split(",").length == 3) {
                        target.val(tomorrowString + ", " + nwDate.split(",")[1] + "," + nwDate.split(",")[2]);
                    }
                    else {
                        target.val(tomorrowString + ", " + nwDate);
                    }
                }
                else {
                    target.val(nwDate);
                }
                $(target).attr('readonly', true);
                $('input[id*="hidDateOfArrival"]').val(formatDate(newDate));
                SetDateOfDeparture(null, true);
            }
        });
        $(dateOfArrival).attr('readonly', true);
        if (doNotSetDates !== "1") {
            if ($('input[id*="hidDateOfArrival"]').val() != "") {
                var selectedDate = new Date($('input[id*="hidDateOfArrival"]').val());
                $(dateOfArrival).glDatePicker("setSelectedDate", selectedDate);
				var seletedDate = selectedDate.toLocaleDateString()
				if (seletedDate == isToday) {
				    if (seletedDate.toString().split(",").length == 3) {
				        $(dateOfArrival).val(todayString + ", " + seletedDate.split(",")[1] + "," + seletedDate.split(",")[2]);
				    }
				    else {
				        $(dateOfArrival).val(todayString + ", " + seletedDate);
				    }
				}
				else if (seletedDate == isTomorrow) {
				    if (seletedDate.toString().split(",").length == 3) {
				        $(dateOfArrival).val(tomorrowString + ", " + seletedDate.split(",")[1] + "," + seletedDate.split(",")[2]);
				    }
				    else {
				        $(dateOfArrival).val(tomorrowString + ", " + seletedDate);
				    }
				} 
				else {
				$(dateOfArrival).val(seletedDate);
				}
                valueDate = selectedDate;
                $(dateOfArrival).data('theDate', selectedDate);
            }
            else {
                $('input[id*="hidDateOfArrival"]').val(formatDate(startDateofArrival));
                $(dateOfArrival).val(startDateofArrival.toLocaleDateString());
                valueDate = startDateofArrival;
            }
        }

        InitializeDateOfDeparture();
    } else {
        $('input[type=date]').attr('readonly', false);
        $(dateOfArrival).blur(function() {
            var checkoutDate = $(dateOfArrival).val().substring(8, 10);
            var checkoutMonth = $(dateOfArrival).val().substring(5, 7);
            var checkoutYear = $(dateOfArrival).val().substring(0, 4);
            var checkout = new Date(Date.parse(checkoutYear + "-" + checkoutMonth + "-" + checkoutDate + "T00:00:00.000Z"));
            checkout.setTime(checkout.getTime() + oneDayInMs);
            $(dateOfDeparture).val(getISODate(checkout));
        });

    }

}

function SetDateOfDeparture(selectedDateOfDept, setDate) {
    var isDatePickerAllowed = scandic.responsive.showDatePicker();
    if (isDatePickerAllowed) {
        var enddateOfArrival = new Date($(".dateOfArrival").data("enddateofarrival"));
        var dateOfArrival = $('input[id*="hidDateOfArrival"]').val();
        var startDateOfDept = new Date();
        var endDateOfDept = new Date();
        var dateOfDeparture = $(".dateOfDeparture");
        var noOfBookingDaysAllowed = Math.abs($(dateOfDeparture).data("allowedbookingdays"));
        enddateOfArrival.setDate(enddateOfArrival.getDate() + 1);

        if (dateOfArrival != "") {
            endDateOfDept = new Date(dateOfArrival);
            startDateOfDept = new Date(dateOfArrival);
        }

        startDateOfDept.setDate(startDateOfDept.getDate() + 1);
        endDateOfDept.setDate(endDateOfDept.getDate() + noOfBookingDaysAllowed);

        if (startDateOfDept > enddateOfArrival) {
            startDateOfDept = enddateOfArrival;
        }

        if (endDateOfDept > enddateOfArrival) {
            endDateOfDept = enddateOfArrival;
        }
        if (selectedDateOfDept == null) {
            selectedDateOfDept = startDateOfDept;
        }

        if (setDate) {		
			var selectDeptDate = selectedDateOfDept.toLocaleDateString();			
				if(selectDeptDate == isToday){
				    if (selectDeptDate.toString().split(",").length == 3) {
				        $(dateOfDeparture).val(todayString + ", " + selectDeptDate.split(",")[1] + "," + selectDeptDate.split(",")[2]);
				    }
				    else {
				        $(dateOfDeparture).val(todayString + ", " + selectDeptDate);
				    }
				} 
				else if (selectDeptDate == isTomorrow){
				    if (selectDeptDate.toString().split(",").length == 3) {
				        $(dateOfDeparture).val(tomorrowString + ", " + selectDeptDate.split(",")[1] + "," + selectDeptDate.split(",")[2]);
				    }
				    else {
				        $(dateOfDeparture).val(tomorrowString + ", " + selectDeptDate);
				    }
				}
				else{
				 $(dateOfDeparture).val(selectDeptDate);
				}           
        }

        $(dateOfDeparture).glDatePicker("setStartDate", startDateOfDept);
        $(dateOfDeparture).glDatePicker("setEndDate", endDateOfDept);
        $(dateOfDeparture).glDatePicker("setSelectedDate", selectedDateOfDept);
        $(dateOfDeparture).data('theDate', selectedDateOfDept);
        $('input[id*="hidDateOfDeparture"]').val(formatDate(selectedDateOfDept));
    }

}

function InitializeDateOfDeparture() {
    var isDatePickerAllowed = scandic.responsive.showDatePicker();
    if (isDatePickerAllowed) {
        var startDateOfDept = null;
        var dateOfDeparture = $(".dateOfDeparture");
        var hiddenDateOfDeparture = $('input[id*="hidDateOfDeparture"]');

        $(dateOfDeparture).glDatePicker({
            allowOld: false,
            onChange: function(target, newDate) {
                $(target).attr('readonly', false);
				var nwDate = newDate.toLocaleDateString();
					if (nwDate == isToday) {
					    if (nwDate.toString().split(",").length == 3) {
					        target.val(todayString + ", " + nwDate.split(",")[1] + "," + nwDate.split(",")[2]);
					    }
					    else {
					        target.val(todayString + ", " + nwDate);
					    }
					}
					else if (nwDate == isTomorrow) {
					    if (nwDate.toString().split(",").length == 3) {
					        target.val(tomorrowString + ", " + nwDate.split(",")[1] + "," + nwDate.split(",")[2]);
					    }
					    else {
					        target.val(tomorrowString + ", " + nwDate);
					    }
					} 
					else {
					target.val(nwDate);
					}	
                $(target).attr('readonly', true);
                $('input[id*="hidDateOfDeparture"]').val(formatDate(newDate));
            }
        });
        $(dateOfDeparture).attr('readonly', true);
        if ($(hiddenDateOfDeparture).val() != "") {
            startDateOfDept = new Date($(hiddenDateOfDeparture).val());
        }

        var setDate = true;

        if (doNotSetDates === "1") {
            setDate = false;

        }

        SetDateOfDeparture(startDateOfDept, setDate);
    }


}
var scandicmobile = {
    init: function() {
        scandicmobile.chSelect();
        scandicmobile.agSelct();
        scandicmobile.bedTypeData();
    },
    chSelect: function() {
        $("select.child").change(function() {
            //scandic.genericChildBedTypeSelect();
            $(this).find(":selected").attr('selected', 'selected');
            var chid = $(this).find(":selected").val();
            var name = $(this).attr('rel');
            $("#chdBedWrapper").show();
            //hide colm5 of specific child module
            $('.' + name + " .colm5").hide();


            //var newStr='.'+name;
            if (chid > 0) {
                $('.' + name).show();
                $('.' + name + " .colm5:lt(" + chid + ")").show();
            }
            else {
                $('.' + name).hide();
            }
            scandicmobile.agSelct();
        }).trigger('change');
    },
    agSelct: function() {
        $("select.subchild").change(function() {
            $(this).find(":selected").attr('selected', 'selected')
            var ageid = $(this).find(":selected").val();
            var name = $(this).attr('rel');
            var hdrToShow = false;
            if (isNaN(ageid)) {
                $(this).parent().siblings('.margleft').hide();

                $(this).parent().siblings().children('.subchild:visible').each(
	            function() {
	                if (!isNaN($(this).find(":selected").val())) {
	                    hdrToShow = true;
	                }
	            }
	        )//each ends here

                if (hdrToShow) {
                    $(this).parent().siblings('.selHdr1').show();
                }
                else {
                    $(this).parent().siblings('.selHdr1').hide();
                }
            }
            else {
                if (isNaN($('.' + name).find(":selected").val())) {
                    //populate only if no value selected previously
                    var defaultValue = $('.' + name).attr('data-defaultValue');
                    $('.' + name).html(populateDropDown(ageid, defaultValue));
                }
                $(this).parent().siblings('.margleft').show();
            }
        }).trigger('change');
    },
    bedTypeData: function() {
        $('.selBedTyp').attr('data-defaultValue', '');
    }
}
function populateDropDown(age, defaultValue) {
    var bedTypes = GetBedTypes(age);
    var optionString = "";
    if (null != bedTypes) {
        // Loop through each bet type preference and generate a HTML radio option string.
        for (var bedCount = 0; bedCount < bedTypes.length; bedCount++) {
            if (bedTypes[bedCount] == defaultValue) {
                optionString += '<option value="' + bedTypes[bedCount] + '" selected >' + bedTypes[bedCount] + '</option>';                
            }
            else {
                optionString += '<option value="' + bedTypes[bedCount] + '">' + bedTypes[bedCount] + '</option>';
            }
        }
    }
    return optionString;
}

function GetBedTypes(selectedAge) {
    var criteria = $("span[id$='lblchildrenCriteriaHidden']").text();

    var eachCriteria = criteria.split(',');

    for (var index = 0; index < eachCriteria.length; index++) {
        var bedEntity = eachCriteria[index].split('|');
        var ages = bedEntity[0].split('-');
        var bedTypes = new Array();

        if ((parseInt(selectedAge) >= parseInt(ages[0])) && (parseInt(selectedAge) <= parseInt(ages[1]))) {
            var bed = bedEntity[1].split('_');

            for (var count = 0; count < bed.length; count++) {
                bedTypes.push(bed[count]);
            }

            // Return the collection of selected bed types for the chosen age in an array.
            return bedTypes;
        }
    }
}
$('.button').click(function() {
    getCurrentGPSLocation();
});

function getCurrentGPSLocation() {
    if ($("input[id$='txtSearch']").val() == nearMyPosition || $("input[id$='txtSearch']").val() == "") {
        SearchNearMyLocation();
        if ($("input[id$='hUserCurrentLatitude']").val() == "" || $("input[id$='hUserCurrentLongitude']").val() == "") {
            //successCallback(position);				
        }
        else {
            document.forms[0].__EVENTARGUMENT.value = 'OnClick';
            document.forms[0].submit();
        }
    }
    else {
        if (enableCustomSearch == 'True') {
            scandic.responsive.setCustomSearch();
        }
        scandic.responsive.ignoreCustomSearch = true;
        document.forms[0].__EVENTARGUMENT.value = 'OnClick';
        document.forms[0].submit();
    }
}

function SearchNearMyLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(successCallback, errorCallback, { timeout: 10000 });
    } else {
        alert('no geolocation support');

    }
}
function successCallback(position) {
    var lat = position.coords.latitude;
    var lon = position.coords.longitude;
    $("input[id$='hUserCurrentLatitude']").val(position.coords.latitude);
    $("input[id$='hUserCurrentLongitude']").val(position.coords.longitude);
    document.forms[0].__EVENTARGUMENT.value = 'OnClick';
    document.forms[0].submit();
}
function errorCallback(error) {
    if (error.code == 1) {
        alert(shareLocationMeassage);
    } else {
        if (error.code == 2) {
            alert(shareLocationMeassage);
        } else {
            if (error.code == 3) {
                alert(locationTimeOutString);
            }
            else {
                alert(shareLocationMeassage);
            }
        }
    }
}

function setCookieNametoSearchBox(searchText) {
    scandic.responsive.ignoreCustomSearch = true;
    $("input[id$='txtSearch']").val(searchText);
}