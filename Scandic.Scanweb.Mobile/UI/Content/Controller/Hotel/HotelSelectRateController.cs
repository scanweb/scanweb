﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Text.RegularExpressions;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.code.Util.Map;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Content.Interface;
using Scandic.Scanweb.Mobile.UI.Content.Repository;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Content.Hotel;

namespace Scandic.Scanweb.Mobile.UI.Content.Controller.Hotel
{
    public class HotelSelectRateController : BaseController
    {
        protected IHotelSelectRateRepository hotelRepository;
        public HotelSelectRateController()
            : this("")
        { }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in SelectRateController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public HotelSelectRateController(string requestLanguage)
            : base(requestLanguage)
        {
            //hotelRepository = DependencyResolver.Instance.GetService(typeof(IHotelSelectRateRepository)) as IHotelSelectRateRepository;
        }

        public SelectRateHotelOverview GetSelectRateHotelOverview()
        {
            hotelRepository = new HotelSelectRateRepository();
            SelectRateHotelOverview hotelOverview = null;
            var selectedHotel = bookingRepository.GetSelectedHotel();
            if (selectedHotel != null)
            {
                hotelOverview = hotelRepository.GetHotelDetails(selectedHotel);
                hotelOverview.RewardNights = GetHotelRedemptionPoints(Convert.ToInt32(selectedHotel.Id));
            }
            selectedHotel = null;
            return hotelOverview;

        }

        private List<RedemptionPoints> SetRedemptionPointsEntity(PageDataCollection redemptionPages)
        {
            List<RedemptionPoints> redemptionPoints = new List<RedemptionPoints>();
            bool campaignPointsAvailable = false;
            IFormatProvider cultureInfo = new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
            foreach (PageData redemptionPageData in redemptionPages)
            {
                if (redemptionPageData != null && redemptionPageData.CheckPublishedStatus(PagePublishedStatus.Published))
                {
                    RedemptionPoints points = new RedemptionPoints();
                    if (redemptionPageData["Points"] != null)
                    {
                        points.RewardPoints = Convert.ToString(redemptionPageData["Points"]);
                        if (redemptionPageData["Startdate"] != null && redemptionPageData["Enddate"] != null)
                        {
                            points.StartDate = Convert.ToDateTime(redemptionPageData["Startdate"]).ToString("dd MMM yy", cultureInfo);
                            points.EndDate = Convert.ToDateTime(redemptionPageData["Enddate"]).ToString("dd MMM yy", cultureInfo);
                            points.IsCampaignPointsAvailable = true;
                            campaignPointsAvailable = true;
                        }
                    }
                    redemptionPoints.Add(points);
                }
            }
            if (campaignPointsAvailable)
            {
                foreach (var item in redemptionPoints)
                {
                    item.IsCampaignPointsAvailable = true;
                }
            }

            return redemptionPoints;
        }


        private List<RedemptionPoints> GetHotelRedemptionPoints(int hotelId)
        {
            List<RedemptionPoints> points = new List<RedemptionPoints>();
            //string redPoints = string.Empty;
            PageData hotelData = siteRepository.GetPageDataByPageID(hotelId);
            PageDataCollection hotelRedemptionPages = ContentDataAccess.GetChildrenWithPageTypeID(
                Convert.ToInt32(ConfigurationManager.AppSettings["HotelRedemptionPointsPageTypeID"]), hotelData.PageLink as PageReference);
            int pointDisplayLimit = Convert.ToInt32(ConfigurationManager.AppSettings["PointsDisplayLimit"]);
            PageDataCollection redemptionPages = new PageDataCollection();
            int pointCount = 0;
            if (hotelRedemptionPages != null && hotelRedemptionPages.Count > 0)
            {
                foreach (PageData pageData in hotelRedemptionPages)
                {
                    if (pageData.StopPublish > DateTime.Today)
                    {
                        if (pageData["Points"] != null)
                        {
                            if (pointCount <= pointDisplayLimit)
                                redemptionPages.Add(pageData);
                            pointCount++;
                        }
                    }
                }
                if (redemptionPages.Count > 0)
                    points = SetRedemptionPointsEntity(redemptionPages);
            }

            hotelData = null;
            hotelRedemptionPages = null;
            redemptionPages = null;
            return points;
        }

        public SelectRateHotelLocation GetSelectRateHotelLocationDetails()
        {
            SelectRateHotelLocation hotelLocation = new SelectRateHotelLocation();
            hotelRepository = new HotelSelectRateRepository();
            var selectedHotel = bookingRepository.GetSelectedHotel();
            if (selectedHotel != null)
            {
                hotelLocation = hotelRepository.GetHotelLocationDetails(selectedHotel);
            }
            return hotelLocation;
        }

        public List<SelectRateHotelOffers> GetSelectRateHotelOffersDetails()
        {
            List<SelectRateHotelOffers> hotelOffers = new List<SelectRateHotelOffers>();
            hotelOffers = GetHotelOfferPages();
            return hotelOffers;
        }

        private List<SelectRateHotelOffers> SetHotelOfferEntity(PageDataCollection offerPages)
        {
            List<SelectRateHotelOffers> hotelOffers = new List<SelectRateHotelOffers>();
            int imageWidth = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.MOBILE_CAROUSEL_IMAGE_WIDTH]);
            if (offerPages != null)
            {
                foreach (var offer in offerPages)
                {
                    bool read = offer.QueryDistinctAccess(EPiServer.Security.AccessLevel.Read);
                    bool published = offer.CheckPublishedStatus(PagePublishedStatus.Published);
                    bool validOffer = true;
                    if (read == false || published == false || (offer["OfferEndTime"] != null && Convert.ToDateTime(offer["OfferEndTime"]) < DateTime.Now))
                    {
                        validOffer = false;
                    }
                    if (validOffer)
                    {
                        SelectRateHotelOffers hotelOffer = new SelectRateHotelOffers();
                        if (offer[Reference.BOX_HEADING] != null)
                            hotelOffer.OfferTitle = Regex.Replace(Convert.ToString(offer[Reference.BOX_HEADING]), "<.*?>", string.Empty);
                        if (offer[Reference.MOBILE_OFFER_IMAGE] != null)
                            hotelOffer.OfferImageURL = WebUtil.GetImageVaultImageUrl(Convert.ToString(offer[Reference.MOBILE_OFFER_IMAGE]), imageWidth);
                        else
                            if (offer[Reference.DESKTOP_OFFER_IMAGE] != null)
                                hotelOffer.OfferImageURL = WebUtil.GetImageVaultImageUrl(Convert.ToString(offer[Reference.DESKTOP_OFFER_IMAGE]), imageWidth);
                        if (offer["BoxContent"] != null)
                            hotelOffer.OfferDescription = Regex.Replace(Convert.ToString(offer["BoxContent"]), "<.*?>", string.Empty);
                        if (offer["PromoBoxPriceText"] != null)
                            hotelOffer.OfferPriceText = Regex.Replace(Convert.ToString(offer["PromoBoxPriceText"]), "<.*?>", string.Empty);

                        hotelOffer.OfferPageURL = GetOfferURL(Convert.ToString(offer.PageLink.ID));

                        hotelOffers.Add(hotelOffer);
                    }
                }
            }
            return hotelOffers;
        }

        private string GetOfferURL(string pageId)
        {
            string offerPageURL = siteRepository.GetPageUrl(MobilePages.OfferDetail);
            offerPageURL = string.Format("{0}?pageid={1}", offerPageURL, pageId);
            return offerPageURL;
        }

        private List<SelectRateHotelOffers> GetHotelOfferPages()
        {
            List<SelectRateHotelOffers> hotelOffers = new List<SelectRateHotelOffers>();
            PageDataCollection offerPages = null;

            hotelRepository = new HotelSelectRateRepository();
            var selectedHotel = bookingRepository.GetSelectedHotel();
            if (selectedHotel != null)
            {
                PageData hotelData = siteRepository.GetPageDataByPageID(Convert.ToInt32(selectedHotel.Id));
                offerPages = hotelRepository.GetHotelOfferPages(hotelData);
                if (offerPages != null)
                    hotelOffers = SetHotelOfferEntity(offerPages);
                else
                {
                    string noOfferMessage = WebUtil.GetTranslatedText(Reference.NO_OFFER_MESSAGE);
                    hotelOffers.Add(new SelectRateHotelOffers { NoOfferMessage = noOfferMessage });
                }
            }
            selectedHotel = null;
            return hotelOffers;
        }

        public List<ImageCarousel> LoadHotelSelectRateCarousalData()
        {
            List<ImageCarousel> lstImgCarousel = new List<ImageCarousel>();
            var selectedHotel = bookingRepository.GetSelectedHotel();
            int imageWidth = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.MOBILE_CAROUSEL_IMAGE_WIDTH]);
            if (selectedHotel != null)
            {
                //check for caching of CMSHotel object
                PageData hotelData = siteRepository.GetPageDataByPageID(Convert.ToInt32(selectedHotel.Id));
                if (hotelData != null)
                {
                    //Pick 2 images of every category to load in image carousel (General, Room, RestaurantBar and Leisure).
                    string imgcatNames = "GeneralImage,RoomImage,RestBarImage,LeisureImage";
                    string[] imgCategories = imgcatNames.Split(',');
                    int imgCount = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.MOBILE_SELECT_RATE_CAROUSEL_IMAGE_COUNT_PER_CATEGORY]);
                    foreach (string s in imgCategories)
                    {
                        for (int i = 1; i <= imgCount; i++)
                        {
                            if (hotelData[string.Format("{0}{1}", s, i.ToString())] != null)
                            {
                                ImageCarousel carousel = new ImageCarousel();
                                carousel.CarouselImageSrc = WebUtil.GetImageVaultImageUrl(Convert.ToString(hotelData[string.Format("{0}{1}", s, i.ToString())]), imageWidth);
                                lstImgCarousel.Add(carousel);
                            }
                        }
                    }
                }
            }
            selectedHotel = null;
            return lstImgCarousel;
        }

        #region GoogleMap
        public MapUnit LoadHotelMap(string urlHost)
        {
            var selectedHotel = bookingRepository.GetSelectedHotel();
            MapHotelUnit hotelMap = null;
            double GoeX = 0;
            double GoeY = 0;
            if (selectedHotel != null)
            {
                PageData hotelPage = siteRepository.GetPageDataByPageID(Convert.ToInt32(selectedHotel.Id));
                if (hotelPage != null)
                {
                    GoeX = hotelPage["GeoX"] != null ? (double)hotelPage["GeoX"] : 0;
                    GoeY = hotelPage["GeoY"] != null ? (double)hotelPage["GeoY"] : 0;
                    MapHotelUnit hotel = new MapHotelUnit(
                        GoeX,
                        GoeY,
                        -1,
                        string.Format("http://{0}/{1}", urlHost,
                                      "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png"),
                        string.Empty,
                        hotelPage["PageName"] as string,
                        "",
                        hotelPage["StreetAddress"] as string,
                        hotelPage["PostCode"] as string,
                        hotelPage["PostalCity"] as string,
                        hotelPage["City"] as string,
                        hotelPage["Country"] as string,
                        hotelPage["LinkURL"] as string, ""
                        //GetDeepLinkingURL(hotelPage["OperaID"] as string ?? string.Empty)
                        );
                    hotelMap = hotel;
                }
            }
            return hotelMap;
        }
        #endregion
    }
}
