﻿using System;
using System.Configuration;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Filters;
using EPiServer.Security;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.Templates.Units.Static;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Content.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Content.Hotel;
namespace Scandic.Scanweb.Mobile.UI.Content.Repository
{
    /// <summary>
    /// HotelSelectRateRepository
    /// </summary>
    public class HotelSelectRateRepository : IHotelSelectRateRepository
    {

        public BookingContext CurrentContext
        {
            get { return MiscellaneousSessionWrapper.BookingContext as BookingContext; }
        }

        /// <summary>
        /// Get Hotel Details for select Rate Page
        /// </summary>
        /// <param name="destination"></param>
        /// <returns>HotelDestination</returns>
        public SelectRateHotelOverview GetHotelDetails(HotelDestination destination)
        {
            SelectRateHotelOverview hotelOverview = null;
            hotelOverview = SetHotelOverviewEntity(destination);
            return hotelOverview;
        }

        /// <summary>
        /// Get Hotel Location Details for select Rate Page
        /// </summary>
        /// <param name="destination"></param>
        /// <returns>HotelDestination</returns>
        public SelectRateHotelLocation GetHotelLocationDetails(HotelDestination destination)
        {
            SelectRateHotelLocation hotelLocation = null;
            hotelLocation = SetHotelLocationEntity(destination);
            return hotelLocation;
        }

        private SelectRateHotelLocation SetHotelLocationEntity(HotelDestination destination)
        {
            SelectRateHotelLocation hotelLocation = new SelectRateHotelLocation();
            hotelLocation.HotelAddress = destination.HotelAddress.ToString();
            string cityCenterDistanceString = WebUtil.GetTranslatedText(Reference.CITY_CENTER_DISTANCE_XPATH);
            hotelLocation.DistanceFromCityCenter = string.Format(cityCenterDistanceString,
                                                            destination.CityCenterDistance,
                                                            destination.HotelAddress.City);
            hotelLocation.Id = destination.Id;
            hotelLocation.HotelPhoneNo = destination.CustomerCarePhone;
            if (destination.Coordinate != null)
            {
                hotelLocation.HotelLongitude = destination.Coordinate.X;
                hotelLocation.HotelLatitude = destination.Coordinate.Y;
            }
            //hotelLocation.DistanceFromCurrentLocation = Convert.ToDouble(Math.Round(WebUtil.CalculateCurrentLocationDistance
            //                                                    (CurrentContext.SearchHotelPage.CurrentCoordinate.X, CurrentContext.SearchHotelPage.CurrentCoordinate.Y,
            //                                                    destination.Coordinate.Y, destination.Coordinate.X), 1), CultureInfo.InvariantCulture);

            return hotelLocation;
        }

        private PageDataCollection GetOffersFromHotelPages(PageDataCollection allOffers, PageData hotelData)
        {
            PageDataCollection offerPages = new PageDataCollection();
            if (hotelData != null)
            {
                if (hotelData["PromotionBoxOffers"] != null)
                {
                    string selectedOffers = hotelData["PromotionBoxOffers"] as string;
                    string[] selectedOfferList = selectedOffers.Split(',');
                    string selectedOffer = string.Empty;

                    for (int ctr = 0; ctr < selectedOfferList.Length; ctr++)
                    {
                        selectedOffer = selectedOfferList[ctr].Replace("{", "").Replace("}", "");

                        foreach (PageData eachOffer in allOffers)
                        {
                            if (eachOffer.PageLink.ID.ToString() == selectedOffer.Trim())
                            {
                                offerPages.Add(eachOffer);
                            }
                        }
                    }
                }
            }
            return offerPages;
        }

        public PageDataCollection GetHotelOfferPages(PageData hotelData)
        {
            PageDataCollection hotelOffers = WebUtil.GetHotelOfferPages(hotelData);
            if (hotelOffers != null && hotelOffers.Count > 0)
            {
                PageReference primaryOffer = hotelData["PrimaryOffer"] as PageReference;
                if (primaryOffer != null)
                {
                    PageData primaryOfferPage = DataFactory.Instance.GetPage(primaryOffer, AccessLevel.NoAccess);
                    foreach (PageData offerPage in hotelOffers)
                    {
                        if (offerPage.PageLink.ID.Equals(primaryOfferPage.PageLink.ID))
                        {
                            hotelOffers.Remove(offerPage);
                            break;
                        }
                    }
                }
            }
            return hotelOffers;
        }

        private PageDataCollection GetOffersFromOfferPages(PageDataCollection allOffers, PageData hotelData)
        {
            PageDataCollection offerPages = new PageDataCollection();
            if (hotelData != null)
            {
                foreach (PageData offerPage in allOffers)
                {
                    offerPages.Add(offerPage);
                }
                // Keep only offers that should be visible on this hotel
                FilterCompareTo offerTypeFilter = new FilterCompareTo("Hotels", "{" + hotelData["OperaID"] as string + "}");
                offerTypeFilter.Condition = CompareCondition.Contained;
                offerTypeFilter.StringComparison = StringComparison.CurrentCultureIgnoreCase;
                offerTypeFilter.Filter(offerPages);
            }
            return offerPages;
        }

        private PageDataCollection GetConsolidatesOffersListForHotel(PageDataCollection offersFromOfferPages,
                                                                     PageDataCollection offersFromHotelPages)
        {
            PageDataCollection offerPages = new PageDataCollection();
            foreach (PageData offerPage in offersFromOfferPages)
            {
                if (!offerPages.Contains(offerPage))
                {
                    offerPages.Add(offerPage);
                }
            }
            foreach (PageData offerPage in offersFromHotelPages)
            {
                if (!offerPages.Contains(offerPage))
                {
                    offerPages.Add(offerPage);
                }
            }
            return offerPages;
        }

        private PageDataCollection GetAllOffers()
        {
            int offerPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["OfferPageTypeGUID"])).ID;
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            PageReference offerContainer = rootPage["OfferContainer"] as PageReference;
            PageDataCollection offerPages = new PageDataCollection();

            PageDataCollection offerCategories = DataFactory.Instance.GetChildren(offerContainer);
            foreach (PageData offerCategoryPage in offerCategories)
            {
                PageDataCollection offersInCategory = DataFactory.Instance.GetChildren(offerCategoryPage.PageLink);
                foreach (PageData offer in offersInCategory)
                {
                    if (offer.StopPublish > DateTime.Today)
                    {
                        if (offer.PageTypeID == offerPageTypeID)
                        {
                            offerPages.Add(offer);
                        }
                    }
                }
            }
            for (int i = offerPages.Count - 1; i >= 0; i--)
            {
                PageData p = offerPages[i];
                if (!p.CheckPublishedStatus(PagePublishedStatus.Published) || !p.QueryDistinctAccess(AccessLevel.Read))
                    offerPages.Remove(offerPages[i]);
            }
            return offerPages;
        }

        private SelectRateHotelOverview SetHotelOverviewEntity(HotelDestination hotel)
        {
            int imageWidth = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.MOBILE_CAROUSEL_IMAGE_WIDTH]);
            string yes = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Yes");
            string no = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/No");
            string km = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Km");
            ConfigHelper configHelper = new ConfigHelper();
            SelectRateHotelOverview hotelOverview = new SelectRateHotelOverview();
            hotelOverview.HotelDescription = hotel.HotelDescription;
            hotelOverview.HotelName = hotel.Name;
            hotelOverview.HotelImageURL = !string.IsNullOrEmpty(hotel.ImageURL) ? configHelper.GetImageURL(WebUtil.GetImageVaultImageUrl(hotel.ImageURL, imageWidth)) : Reference.ImagePlaceHolderPath;
            hotelOverview.TAReviewPopUpURL = WebUtil.CreateRatingPopupURL(hotel.TALocationID, LanguageRedirectionHelper.GetCurrentLanguage());
            TripAdvisorPlaceEntity TAEntity = WebUtil.GetTAReviewEntityForHotel(hotel.TALocationID);
            if (TAEntity != null)
            {
                hotelOverview.TAReviewCountText = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/TripAdvisorRatingCountLinkText").Replace("{0}",
                    Convert.ToString(WebUtil.GetTAReviewEntityForHotel(hotel.TALocationID).ReviewCount));
                hotelOverview.TARatingImageURL = !string.IsNullOrEmpty(TAEntity.TripAdvisorImageURL) ? TAEntity.TripAdvisorImageURL : TAEntity.TripAdvisorImageDefaultURL;
            }
            hotelOverview.HideTARating = hotel.HideHotelTARating;
            hotelOverview.HotelFacilities = new HotelFacility();
            hotelOverview.HotelFacilities.Airport1Distance = hotel.Airport1Distance != 0 ? string.Format("{0} {1}", hotel.Airport1Distance, km) : string.Format("{0} {1}", "0", km);
            hotelOverview.HotelFacilities.Airport1Name = hotel.Airport1Name;
            hotelOverview.HotelFacilities.CityCenterDistance = hotel.CityCenterDistance != 0 ? string.Format("{0} {1}", hotel.CityCenterDistance, km) : string.Format("{0} {1}", "0", km);
            hotelOverview.HotelFacilities.DistanceTrain = hotel.TrainStationDistance != null ? string.Format("{0} {1}", hotel.TrainStationDistance, km) : string.Format("{0} {1}", "0", km);
            hotelOverview.HotelFacilities.EcoLabeled = hotel.EcoLabeled;
            hotelOverview.HotelFacilities.Garage = hotel.IsGarageAvailable == true ? yes : no;
            hotelOverview.HotelFacilities.MeetingFacilities = hotel.IsMeetingRoomAvailable == true ? yes : no;
            hotelOverview.HotelFacilities.NonSmokingRooms = hotel.NoOfNonSmokingRooms;
            hotelOverview.HotelFacilities.NoOfRooms = hotel.NoOfRooms;
            hotelOverview.HotelFacilities.OutdoorParking = hotel.IsOutDoorParkingAvailable == true ? yes : no;
            hotelOverview.HotelFacilities.RelaxCenter = hotel.IsRelaxCenterAvailable == true ? yes : no;
            hotelOverview.HotelFacilities.RestaurantBar = hotel.IsRestaurantAndBarAvailable == true ? yes : no;
            hotelOverview.HotelFacilities.RoomsForDisabled = hotel.IsRoomsForDisabledAvailable == true ? yes : no;
            hotelOverview.HotelFacilities.Shop = hotel.IsShopAvailable == true ? yes : no;
            hotelOverview.HotelFacilities.WirelessInternet = hotel.IsWiFiAvailable == true ? yes : no;
            return hotelOverview;

        }


    }
}
