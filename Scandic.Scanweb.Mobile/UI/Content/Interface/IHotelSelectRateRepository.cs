﻿
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Content.Hotel;
using Scandic.Scanweb.Core;
using EPiServer.Core;
using System.Collections.Generic;
namespace Scandic.Scanweb.Mobile.UI.Content.Interface
{
    /// <summary>
    /// IHotelSelectRateRepository
    /// </summary>
    public interface IHotelSelectRateRepository
    {
        BookingContext CurrentContext { get; }
        SelectRateHotelOverview GetHotelDetails(HotelDestination destination);
        PageDataCollection GetHotelOfferPages(PageData hotelData);
        SelectRateHotelLocation GetHotelLocationDetails(HotelDestination destination);
    }
}
