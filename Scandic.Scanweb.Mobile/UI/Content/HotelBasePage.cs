﻿using System.Web.Services;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Content.Controller.Hotel;
using Scandic.Scanweb.Mobile.UI.Entity.Content.Hotel;
using Scandic.Scanweb.Mobile.UI.Entity;
using System.Collections.Generic;
using System;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.Mobile.UI.Content
{
    public class HotelBasePage<ConfigType> : VisualBasePage<ConfigType>
    {
        public HotelBasePage()
        {
        }

        private static SelectRateHotelOverview HotelOverview { get; set; }
        private static SelectRateHotelLocation HotelLocation { get; set; }
        private static List<SelectRateHotelOffers> HotelOffers { get; set; }
        private static List<ImageCarousel> SelectRateCarousalData { get; set; }

        [WebMethod]
        public static SelectRateHotelOverview GetSelectRateHotelOverview()
        {
            var startController = new HotelSelectRateController();
            try
            {
                HotelOverview = startController.GetSelectRateHotelOverview();
                return HotelOverview;
            }
            catch (Exception ex)
            {
                ActionItem action = Utilities.GetActionItemToTrack(startController.CurrentContext);
                Utilities.TrackActionParametersAndLogData("SelectRate:HotelBasePage", "GetSelectRateHotelOverview", ex, action, true);
                throw ex;
            }
        }

        [WebMethod]
        public static SelectRateHotelLocation GetSelectRateHotelLocationDetails()
        {
            var startController = new HotelSelectRateController();
            try
            {
                HotelLocation = startController.GetSelectRateHotelLocationDetails();
                return HotelLocation;
            }
            catch (Exception ex)
            {
                ActionItem action = Utilities.GetActionItemToTrack(startController.CurrentContext);
                Utilities.TrackActionParametersAndLogData("SelectRate:HotelBasePage", "GetSelectRateHotelLocationDetails", ex, action, true);
                throw ex;
            }
        }

        [WebMethod]
        public static List<SelectRateHotelOffers> GetSelectRateHotelOffersDetails()
        {
            var startController = new HotelSelectRateController();
            try
            {
                HotelOffers = startController.GetSelectRateHotelOffersDetails();
                return HotelOffers;
            }
            catch (Exception ex)
            {
                ActionItem action = Utilities.GetActionItemToTrack(startController.CurrentContext);
                Utilities.TrackActionParametersAndLogData("SelectRate:HotelBasePage", "GetSelectRateHotelOffersDetails", ex, action, true);
                throw ex;
            }
        }

        [WebMethod]
        public static List<ImageCarousel> GetSelectRateCarouselData()
        {
            var startController = new HotelSelectRateController();
            try
            {
                SelectRateCarousalData = startController.LoadHotelSelectRateCarousalData();
                return SelectRateCarousalData;
            }
            catch (Exception ex)
            {
                ActionItem action = Utilities.GetActionItemToTrack(startController.CurrentContext);
                Utilities.TrackActionParametersAndLogData("SelectRate:HotelBasePage", "GetSelectRateCarouselData", ex, action, true);
                throw ex;
            }
        }

    }
}
