﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using System;

namespace Scandic.Scanweb.Mobile.UI.Booking.Business
{
    /// <summary>
    /// RoomRatesUtility
    /// </summary>
    public class RoomRatesUtility
    {
        /// <summary>
        /// GetRoomTypeDetailsList
        /// </summary>
        /// <param name="roomRateDetails"></param>
        /// <returns>List of RoomTypeDetails</returns>
        public static List<RoomTypeDetails> GetRoomTypeDetailsList(BaseRoomRateDetails roomRateDetails)
        {
            List<RoomCategoryEntity> roomCategorySortedList = new List<RoomCategoryEntity>();

            BaseRateDisplay rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);

            #region Sort by rate order

            double minRate = double.MaxValue;
            string minRateCategoryName = string.Empty;
            List<RoomCategoryEntity> roomCategoryList = rateDisplay.RateRoomCategories;
            bool isPerstay = false;
            Dictionary<string, double> minRateForEachCategory = new Dictionary<string, double>();
            for (int iterator = 0; iterator < roomCategoryList.Count; iterator++)
            {
                foreach (string rateCategoryKey in roomCategoryList[iterator].RateCategories.Keys)
                {
                    double minRateforCategory = GetMinRateInRoomCategory(roomCategoryList, rateCategoryKey, isPerstay);
                    if (minRateforCategory < minRate)
                    {
                        minRate = minRateforCategory;
                        minRateCategoryName = rateCategoryKey;
                    }
                }
            }
            Reservation2SessionWrapper.MinRateCategoryName = minRateCategoryName;

            List<string> rateCategoryIds = new List<string>();
            for (int i = 0; i < rateDisplay.RateCategoriesDisplay.Count; i++)
            {
                rateCategoryIds.Add(rateDisplay.RateCategoriesDisplay[i].Id);
            }
            RoomCategoryEntityComparer.RateCategoryIds = rateCategoryIds;

            roomCategoryList.Sort(new RoomCategoryEntityComparer());

            #endregion

            if (roomCategoryList != null)
            {
                List<RoomCategoryEntity> defaultRoomCategories = new List<RoomCategoryEntity>();
                List<RoomCategoryEntity> regularRoomCategories = new List<RoomCategoryEntity>();
                RoomCategoryEntity roomCategory = null;
                for (int roomCategoryCount = 0; roomCategoryCount < roomCategoryList.Count; roomCategoryCount++)
                {
                    roomCategory = roomCategoryList[roomCategoryCount];
                    if (roomCategoryCount == 0)
                    {
                        defaultRoomCategories.Add(roomCategory);
                    }
                    else if (
                        (Core.StringUtil.IsStringInArray(
                            RoomRateDisplayUtil.GetDefaultRoomTypeIds(
                                SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode), roomCategory.Id)))
                    {
                        defaultRoomCategories.Add(roomCategory);
                    }
                    else
                    {
                        regularRoomCategories.Add(roomCategory);
                    }
                }

                if (defaultRoomCategories.Count > 0)
                {
                    roomCategorySortedList.AddRange(defaultRoomCategories.GetRange(0, defaultRoomCategories.Count));
                }
                if (regularRoomCategories.Count > 0)
                {
                    roomCategorySortedList.AddRange(regularRoomCategories.GetRange(0, regularRoomCategories.Count));
                }
            }

            return PrepareRoomTypeDetails(roomCategorySortedList, rateDisplay, roomRateDetails);
        }

        /// <summary>
        /// PrepareRoomTypeDetails
        /// </summary>
        /// <param name="roomCategorySortedList"></param>
        /// <param name="baseRateDisplay"></param>
        /// <param name="roomRateDetails"></param>
        /// <returns>List Of RoomTypeDetails</returns>
        private static List<RoomTypeDetails> PrepareRoomTypeDetails(List<RoomCategoryEntity> roomCategorySortedList,
                                                                    BaseRateDisplay baseRateDisplay,
                                                                    BaseRoomRateDetails roomRateDetails)
        {
            int imageWidth = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.MOBILE_CAROUSEL_IMAGE_WIDTH]);
            List<RoomTypeDetails> roomTypeDetailsList = new List<RoomTypeDetails>();
            RoomCategory hotelRoomDescription = null;
            ConfigHelper configHelper = new ConfigHelper();
            Dictionary<string, RoomCategory> hotelRoomDescriptions = GetUniqueHotelRoomsDescriptions(baseRateDisplay);
            HotelSearchEntity hotelSearchEntity = SearchCriteriaSessionWrapper.SearchCriteria;
            bool isPerStay = false;
            if (hotelSearchEntity != null)
            {
                isPerStay = hotelSearchEntity.SearchingType == SearchType.REDEMPTION ? true : false;
            }

            foreach (RoomCategoryEntity roomCategory in roomCategorySortedList)
            {
                RoomTypeDetails roomTypeDetails = new RoomTypeDetails();
                hotelRoomDescription = GetRoomDescription(roomCategory, hotelRoomDescriptions);
                roomTypeDetails.Description = (hotelRoomDescription != null &&
                                               !string.IsNullOrEmpty(hotelRoomDescription.RoomCategoryteserText))
                                                  ? hotelRoomDescription.RoomCategoryteserText
                                                  : string.Empty;
                roomTypeDetails.ImagePath = (hotelRoomDescription != null &&
                                             !string.IsNullOrEmpty(hotelRoomDescription.ImageURL))
                                                ? configHelper.GetImageURL(WebUtil.GetImageVaultImageUrl(hotelRoomDescription.ImageURL, imageWidth))
                                                : Reference.ImagePlaceHolderPath;
                roomTypeDetails.Id = string.IsNullOrEmpty(roomCategory.Id) ? string.Empty : roomCategory.Id;
                roomTypeDetails.Name = string.IsNullOrEmpty(roomCategory.Name) ? string.Empty : roomCategory.Name;
                roomTypeDetails.PerNight = configHelper.GetPerNightString(isPerStay);
                roomTypeDetails.Rates = GetRateTypeDetails(roomCategory, baseRateDisplay, roomRateDetails, isPerStay);

                roomTypeDetailsList.Add(roomTypeDetails);
            }

            return roomTypeDetailsList;
        }

        /// <summary>
        /// GetRateTypeDetails
        /// </summary>
        /// <param name="roomCategory"></param>
        /// <param name="baseRateDisplay"></param>
        /// <param name="roomRateDetails"></param>
        /// <param name="isPerStay"></param>
        /// <returns>List Of RateTypeDetails</returns>
        private static List<RateTypeDetails> GetRateTypeDetails(RoomCategoryEntity roomCategory,
                                                                BaseRateDisplay baseRateDisplay,
                                                                BaseRoomRateDetails roomRateDetails, bool isPerStay)
        {
            List<RateTypeDetails> rateTypeDetailsList = new List<RateTypeDetails>();
            List<RateCategoryHeaderDisplay> rateCategoriesList = baseRateDisplay.RateCategoriesDisplay;
            RearrangeCoulmnNumber(rateCategoriesList);
            RateTypeDetails rateTypeDetails = null;
            RateCellDisplay rateObject = null;
            RateCategory rateDetailObject = null;

            foreach (RateCategoryHeaderDisplay rateCategory in rateCategoriesList)
            {
                rateTypeDetails = new RateTypeDetails();
                rateObject = baseRateDisplay.GetRateCellDisplay(roomCategory, 0, rateCategory.Id, isPerStay, 0);
                rateTypeDetails.Id = rateCategory.Id;
                rateTypeDetails.Color = rateCategory.RateCategoryColor;
                rateTypeDetails.Rate = rateObject != null ? rateObject.RateString : string.Empty;
                rateTypeDetails.Type = GetRateType(rateCategory.RateCategoryColor);
                rateTypeDetails.Name = rateCategory.Title;

                rateDetailObject = GetRateCategoryByKey(roomRateDetails, rateCategory.Id);
                rateTypeDetails.HoldGuranteeAvailable = rateDetailObject != null
                                                            ? rateDetailObject.HoldGuranteeAvailable
                                                            : false;
                rateTypeDetails.CreditCardGuranteeType = rateDetailObject != null
                                                             ? rateDetailObject.CreditCardGuranteeType
                                                             : string.Empty;

                rateTypeDetailsList.Add(rateTypeDetails);
            }

            return rateTypeDetailsList;
        }

        /// <summary>
        /// This method will rearrange the coulmn number.
        /// </summary>
        /// <param name="headerList">Header list for different category</param>
        /// <remarks>Rate structure display</remarks>
        private static void RearrangeCoulmnNumber(List<RateCategoryHeaderDisplay> headerList)
        {
            string totalCoulmnAccepted = ConfigurationManager.AppSettings.Get("SelectRate.NoOfRateCoulmn");
            bool isCoulmnNumberValidFormat = false;
            int totalCoulmnConfigured = 0;
            if (!string.IsNullOrEmpty(totalCoulmnAccepted))
            {
                isCoulmnNumberValidFormat = int.TryParse(totalCoulmnAccepted, out totalCoulmnConfigured);
            }
            int headerCount = headerList.Count;
            headerList.Sort(new SortHeaderDisplay());
            for (int count = 0; count < headerCount; count++)
            {
                headerList[count].CoulmnNumber = totalCoulmnConfigured;
                totalCoulmnConfigured--;
            }
            headerList.Sort(new RateCategoryCoulmnComparer());
        }

        /// <summary>
        /// GetRateCategoryByKey
        /// </summary>
        /// <param name="roomRateDetails"></param>
        /// <param name="rateCategoryKey"></param>
        /// <returns>RateCategory</returns>
        private static RateCategory GetRateCategoryByKey(BaseRoomRateDetails roomRateDetails, string rateCategoryKey)
        {
            RateCategory rateCategory = null;

            if (roomRateDetails.RateCategories != null && roomRateDetails.RateCategories.Count > 0)
            {
                rateCategory =
                    roomRateDetails.RateCategories.Where(rate => rate.RateCategoryId == rateCategoryKey).SingleOrDefault
                        ();
            }

            return rateCategory;
        }

        /// <summary>
        /// GetRateType
        /// </summary>
        /// <param name="rateCategoryColor"></param>
        /// <returns>RateType</returns>
        private static RateType GetRateType(string rateCategoryColor)
        {
            RateType rateType = RateType.Early;

            switch (rateCategoryColor.ToLower())
            {
                case Reference.RATES_CATEGORY_ORANGE:
                    rateType = RateType.Early;
                    break;
                case Reference.RATES_CATEGORY_BLUE:
                    rateType = RateType.Flex;
                    break;
                default:
                    rateType = RateType.SpecialRate;
                    break;
            }

            return rateType;
        }

        /// <summary>
        /// GetRoomDescription
        /// </summary>
        /// <param name="roomCategory"></param>
        /// <param name="hotelRoomDescriptions"></param>
        /// <returns>RoomCategory</returns>
        private static RoomCategory GetRoomDescription(RoomCategoryEntity roomCategory,
                                                       Dictionary<string, RoomCategory> hotelRoomDescriptions)
        {
            RoomCategory hotelRoomDescription = null;

            if (hotelRoomDescriptions != null && hotelRoomDescriptions.Count > 0)
            {
                hotelRoomDescription =
                    hotelRoomDescriptions.Where(hotelRoom => hotelRoom.Key == roomCategory.Id).SingleOrDefault().Value;
            }

            return hotelRoomDescription;
        }

        /// <summary>
        /// GetUniqueHotelRoomsDescriptions
        /// </summary>
        /// <param name="rateDisplay"></param>
        /// <returns>UniqueHotelRoomsDescriptions</returns>
        private static Dictionary<string, RoomCategory> GetUniqueHotelRoomsDescriptions(BaseRateDisplay rateDisplay)
        {
            Dictionary<string, RoomCategory> hotelRoomDescriptions = new Dictionary<string, RoomCategory>();
            foreach (HotelRoomDescription hotelRoomDescription in rateDisplay.HotelDestination.HotelRoomDescriptions)
            {
                if (!hotelRoomDescriptions.ContainsKey(hotelRoomDescription.HotelRoomCategory.RoomCategoryId))
                {
                    hotelRoomDescriptions.Add(hotelRoomDescription.HotelRoomCategory.RoomCategoryId,
                                              hotelRoomDescription.HotelRoomCategory);
                }
            }
            return hotelRoomDescriptions;
        }

        /// <summary>
        /// Method to find out the min rate from the list against the given rate category
        /// </summary>
        /// <param name="roomCategoryList">Room category list to be compaired to get the min rate.</param>
        /// <param name="rateCategoryKey">Rate category to be compaired to get the min rate</param>
        /// <param name="showPricePerStay">Boolean indicates whether to compare base rate or total rate</param>
        /// <returns>Returns the min rate from the list.</returns>
        private static double GetMinRateInRoomCategory(List<RoomCategoryEntity> roomCategoryList, string rateCategoryKey,
                                                       bool showPricePerStay)
        {
            double minRateValue = double.MaxValue;
            double rateToCheck = 0;
            for (int ctr = 0; ctr < roomCategoryList.Count; ctr++)
            {
                try
                {
                    List<RoomRateEntity> roomRateEntityList = roomCategoryList[ctr].RateCategories[rateCategoryKey];
                    if (!showPricePerStay)
                    {
                        if (roomRateEntityList[0] != null && roomRateEntityList[0].BaseRate != null)
                            rateToCheck = roomRateEntityList[0].BaseRate.Rate;
                    }
                    else
                    {
                        if (roomRateEntityList[0] != null && roomRateEntityList[0].TotalRate != null)
                            rateToCheck = roomRateEntityList[0].TotalRate.Rate;
                    }

                    if (minRateValue > rateToCheck)
                    {
                        minRateValue = rateToCheck;
                    }
                }
                catch (KeyNotFoundException knfe)
                {

                }
            }

            return minRateValue;
        }
    }

    /// <summary>
    /// SortRateColumn
    /// </summary>
    public class SortRateColumn : IComparer<RateTypeDetails>
    {
        #region IComparable<RateTypeDetails> Members

        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RateTypeDetails first, RateTypeDetails second)
        {
            return (second.Type.CompareTo(first.Type));
        }

        #endregion
    }

    /// <summary>
    /// RateCategoryCoulmnComparer
    /// </summary>
    public class RateCategoryCoulmnComparer : IComparer<RateCategoryHeaderDisplay>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RateCategoryHeaderDisplay first, RateCategoryHeaderDisplay second)
        {
            return first.CoulmnNumber.CompareTo(second.CoulmnNumber);
        }
    }
}