﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Web;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Core.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;

namespace Scandic.Scanweb.Mobile.UI.Booking.Business
{
    /// <summary>
    /// CMSDataManager
    /// </summary>
    public class CMSDataManager
    {
        #region Public Methods

        /// <summary>
        /// This method retrieves the basic page data for the mobile navigational links.
        /// </summary>
        /// <returns>List of MobilePageData</returns>
        public static List<MobilePageData> GetMobileSitePageData()
        {
            return GetMobileSitePageData(false);
        }


        /// <summary>
        /// This methods creates a list of mobile cms page data list
        /// </summary>
        /// <param name="cmsPageReferenceKey"></param>
        /// <returns>list of mobile cms page data list</returns>
        private static MobilePageData GetMobilePageData(string cmsPageReferenceKey)
        {
            PageData pageData = DataFactory.Instance.GetPage(GetPageReferenceFromRoot(cmsPageReferenceKey));

            return PrepareMobilePageData(pageData, cmsPageReferenceKey);
        }

        /// <summary>
        /// This method maps the cms page data for the mobile page.
        /// </summary>
        /// <param name="pageData"></param>
        /// <param name="pageReferenceKey"></param>
        /// <returns>cms page data for the mobile page.</returns>
        private static MobilePageData PrepareMobilePageData(PageData pageData, string pageReferenceKey)
        {
            MobilePageData mPageData = new MobilePageData();
            if (pageData != null)
            {
                mPageData.PageReferenceKey = pageReferenceKey;
                mPageData.PageName = pageData.PageName;
                mPageData.PageURL = GlobalUtil.GetUrlToPage(pageData.PageLink);
                mPageData.LinkURL = pageData.LinkURL;
                mPageData.LanguageBranch = pageData.LanguageBranch;
            }
            return mPageData;
        }

        /// <summary>
        /// GetPageReferenceFromRoot
        /// </summary>
        /// <param name="cmsPageReferenceKey"></param>
        /// <returns>PageReference</returns>
        public static PageReference GetPageReferenceFromRoot(string cmsPageReferenceKey)
        {
            PageData rootPage = ContentDataAccess.GetPageData(PageReference.RootPage, false);
            return rootPage[cmsPageReferenceKey] as PageReference;
        }

        /// <summary>
        /// This method gets the page url from the CMS
        /// </summary>
        /// <param name="cmsPageName"></param>
        /// <returns>PageURL</returns>
        public static string GetPageURL(string cmsPageName)
        {
            return GlobalUtil.GetUrlToPage(cmsPageName);
        }

        /// <summary>
        /// It will fetch all destinations in which AvailableInBooking property as checked.
        /// </summary>
        /// <returns>The list of city destination, which will contain the matching cities and hotels</returns>
        public static List<CityDestination> FetchAllDestinationsForAutoSuggest()
        {
            List<CityDestination> cityList = new List<CityDestination>();
            cityList = ContentDataAccess.GetCityAndHotelForAutoSuggest(false);
            return cityList;
        }
        /// <summary>
        /// It will fetch all Country City Hotels in which AvailableInBooking property as checked.
        /// </summary>
        /// <returns>The list of city destination, which will contain the matching cities and hotels</returns>
        public static List<CountryDestinatination> CountryCityAndHotel()
        {
            List<CountryDestinatination> countryList = new List<CountryDestinatination>();
            countryList = ContentDataAccess.GetCountryCityAndHotelForAutoSuggest(false, false);
            return countryList;
        }
        /// <summary>
        /// GetPageLinkURL
        /// </summary>
        /// <param name="cmsPageReference"></param>
        /// <returns>PageLinkURL</returns>
        public static string GetPageLinkURL(string cmsPageReference)
        {
            PageData rootPage = ContentDataAccess.GetPageData(PageReference.RootPage, false);
            PageReference pageLink = rootPage[cmsPageReference] as PageReference;

            if (PageReference.IsNullOrEmpty(pageLink))
                return string.Empty;
            else
            {
                PageData referencedPage = DataFactory.Instance.GetPage(pageLink, EPiServer.Security.AccessLevel.NoAccess);
                return referencedPage.LinkURL;
            }
        }

        /// <summary>
        /// GetAboutOurRateDesciption
        /// </summary>
        /// <returns>AboutOurRateDesciption</returns>
        public static string[] GetAboutOurRateDesciption()
        {
            string[] aboutOurRates = new string[2];

            PageData pageData = GetPageData(Reference.SCANWEB_SELECT_RATE_REFERENCE);
            if (pageData != null)
            {
                aboutOurRates[0] = pageData[Reference.SELECT_RATE_ABOUT_OURRATES_HEADING].ToString();
                aboutOurRates[1] = pageData[Reference.SELECT_RATE_ABOUT_OURRATES].ToString();
            }
            return aboutOurRates;
        }

        /// <summary>
        /// GetPageData
        /// </summary>
        /// <param name="cmsPageReference"></param>
        /// <returns>PageData</returns>
        public static PageData GetPageData(string cmsPageReference)
        {
            PageData rootPage = ContentDataAccess.GetPageData(PageReference.RootPage, false);
            PageReference pageLink = rootPage[cmsPageReference] as PageReference;
            var request = HttpContext.Current.Request;
            var language = request.QueryString["epslanguage"];
            PageData referencedPage = null;

            if (string.Equals(language, CurrentPageLanguageConstant.LANGAUGE_RUSSIAN, StringComparison.InvariantCultureIgnoreCase) ||
                string.Equals(language, CurrentPageLanguageConstant.LANGUAGE_GERMANY, StringComparison.InvariantCultureIgnoreCase))
            {
                language = CurrentPageLanguageConstant.LANGUAGE_ENGLISH.ToLower();
            }

            if (!PageReference.IsNullOrEmpty(pageLink))
            {
                if (string.IsNullOrEmpty(language))
                {
                    referencedPage = DataFactory.Instance.GetPage(pageLink, EPiServer.Security.AccessLevel.NoAccess);
                }
                else
                {
                    ILanguageSelector languageSelector = new LanguageSelector(language);
                    referencedPage = DataFactory.Instance.GetPage(pageLink, languageSelector,  EPiServer.Security.AccessLevel.NoAccess);
                }
            }

            return referencedPage;
        }

        /// <summary>
        /// GetPageRedirectUrl
        /// </summary>
        /// <param name="cmsPageReference"></param>
        /// <param name="language"></param>
        /// <returns>PageRedirectUrl</returns>
        public static string GetPageRedirectUrl(string cmsPageReference, string language)
        {
            return GetPageRedirectUrl(cmsPageReference, language, false);
        }

        /// <summary>
        /// GetPageRedirectUrl
        /// </summary>
        /// <param name="cmsPageReference"></param>
        /// <param name="language"></param>
        /// <param name="isScanwebPage"></param>
        /// <returns>PageRedirectUrl</returns>
        public static string GetPageRedirectUrl(string cmsPageReference, string language, bool isScanwebPage)
        {
            PageData pageData = GetPageData(cmsPageReference);
            return GetPageRedirectUrl(pageData, language, isScanwebPage);
        }

        /// <summary>
        /// GetPageRedirectUrl
        /// </summary>
        /// <param name="pageData"></param>
        /// <param name="language"></param>
        /// <param name="isScanwebPage"></param>
        /// <returns>PageRedirectUrl</returns>
        public static string GetPageRedirectUrl(PageData pageData, string language, bool isScanwebPage)
        {
            bool isSecured = false;
            if (pageData["Secured"] != null)
            {
                isSecured = (bool)pageData["Secured"];
            }

            return ConvertUrlToLocale(pageData.LinkURL, language, pageData.PageLink, isSecured, isScanwebPage);
        }

        /// <summary>
        /// ConvertUrlToLocale
        /// </summary>
        /// <param name="linkURL"></param>
        /// <param name="language"></param>
        /// <param name="pageLink"></param>
        /// <param name="isSecured"></param>
        /// <returns>Converted UrlToLocale</returns>
        public static string ConvertUrlToLocale(string linkURL, string language, PageReference pageLink, bool isSecured)
        {
            return ConvertUrlToLocale(linkURL, language, pageLink, isSecured, false);
        }

        /// <summary>
        /// ConvertUrlToLocale
        /// </summary>
        /// <param name="linkURL"></param>
        /// <param name="language"></param>
        /// <param name="pageLink"></param>
        /// <param name="isSecured"></param>
        /// <param name="isScanwebPage"></param>
        /// <returns>Converted UrlToLocale</returns>
        public static string ConvertUrlToLocale(string linkURL, string language, PageReference pageLink, bool isSecured,
                                                bool isScanwebPage)
        {
            bool isMobileHostname = false;
            string finalUrl = string.Empty;
            string languageSelectionURL = EPiServer.UriSupport.AddLanguageSelection(linkURL, language);
            string fullLanguageSelectionURL = EPiServer.UriSupport.AbsoluteUrlBySettings(languageSelectionURL);
            UrlBuilder finalUrlBuilder = new UrlBuilder(fullLanguageSelectionURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(finalUrlBuilder, pageLink,
                                                                  System.Text.UTF8Encoding.UTF8);
            finalUrl = finalUrlBuilder.ToString();

            string hostName = isScanwebPage ? "ScandicSiteHostName{0}" : "ScandicMobileSiteHostName{0}";
            string appSettingsKey = string.Format(hostName, language.ToUpper());
            string hostURL = ConfigurationManager.AppSettings[appSettingsKey] as string;
			AppLogger.LogInfoMessage("Requested Hostname : " + HttpContext.Current.Request.Url.Host);
            isMobileHostname = IsHostnameExists(HttpContext.Current.Request.Url.Host);
            // This check is to see whether requested URL's hostname is exists in the servers appsetting file. If exists pick the hostname 
            // from appsettings, if it doesn't exist then take it from Request object.
            if (!isMobileHostname)
                hostURL = HttpContext.Current.Request.Url.Host;
            Url url = new Url(fullLanguageSelectionURL);
            if (hostURL != null)
            {
                finalUrl = finalUrl.Replace(url.Host, hostURL);
                finalUrl = finalUrl.Replace("/" + language + "/", "/");
            }
            finalUrl = isSecured ? Utilities.ConvertToSecureProtocol(finalUrl) : finalUrl;

            return finalUrl;
        }

        /// <summary>
        /// This method is added to test mobilesite on a particular server.
        /// </summary>
        /// <param name="reqHost"></param>
        /// <returns>True/False</returns>
        private static bool IsHostnameExists(string requestedHost)
        {
            string HostEN = ConfigurationManager.AppSettings["ScandicMobileSiteHostNameEN"] as string;
            string HostSV = ConfigurationManager.AppSettings["ScandicMobileSiteHostNameSV"] as string;
            string HostFI = ConfigurationManager.AppSettings["ScandicMobileSiteHostNameFI"] as string;
            string HostNO = ConfigurationManager.AppSettings["ScandicMobileSiteHostNameNO"] as string;
            string HostDE = ConfigurationManager.AppSettings["ScandicMobileSiteHostNameDE"] as string;
            string HostRU = ConfigurationManager.AppSettings["ScandicMobileSiteHostNameRU"] as string;
            string HostDK = ConfigurationManager.AppSettings["ScandicMobileSiteHostNameDA"] as string;
            if (string.Equals(requestedHost, HostEN, StringComparison.InvariantCultureIgnoreCase) || string.Equals(requestedHost, HostSV, StringComparison.InvariantCultureIgnoreCase) ||
                string.Equals(requestedHost, HostFI, StringComparison.InvariantCultureIgnoreCase) || string.Equals(requestedHost, HostNO, StringComparison.InvariantCultureIgnoreCase) ||
                string.Equals(requestedHost, HostDE, StringComparison.InvariantCultureIgnoreCase) || string.Equals(requestedHost, HostRU, StringComparison.InvariantCultureIgnoreCase) ||
                string.Equals(requestedHost, HostDK, StringComparison.InvariantCultureIgnoreCase)
               )
                return true;
            else
                return false;
        }
        /// <summary>
        /// GetLanguageBranch
        /// </summary>
        /// <param name="cmsPageReference"></param>
        /// <returns>LanguageBranch</returns>
        public static string GetLanguageBranch(string cmsPageReference)
        {
            PageData pageData = GetPageData(cmsPageReference);
            return pageData.LanguageBranch;
        }

        /// <summary>
        /// IsIPRedirectionEnabled
        /// </summary>
        /// <param name="cmsPageReference"></param>
        /// <returns>True/False</returns>
        public static bool IsIPRedirectionEnabled(string cmsPageReference)
        {
            PageData pageData = GetPageData(cmsPageReference);

            if (pageData.Property["EnableIPRedirection"] != null &&
                pageData.Property["EnableIPRedirection"].Value != null &&
                pageData.Property["EnableIPRedirection"].Value.Equals(true))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// This gets the bonus cheque value from CMS for a country-year for a hotel
        /// </summary>
        /// <returns>Voide</returns>
        public static void GetBonusChequeValue(HotelSearchEntity hotelSearchEntity)
        {
          
            double bonusChequeValue =
                ContentDataAccess.GetBonusChequeRate(hotelSearchEntity.DepartureDate.Year,
                                                     ContentDataAccess.GetCountryNamebyCountryID(
                                                         hotelSearchEntity.HotelCountryCode),
                                                     hotelSearchEntity.HotelCountryCode);
        }

        #region CacheFiller Methods
        /// <summary>
        /// This method retrieves the basic page data for the mobile navigational links.
        /// </summary>
        /// <returns>List of MobilePageData</returns>
        public static List<MobilePageData> GetMobileSitePageData(bool isCacheFillerRequest)
        {
            List<MobilePageData> cmsPageDataList = null;
            try
            {
                if ((ScanwebCacheManager.Instance.LookInCache<List<MobilePageData>>(Reference.MOBILE_CMS_PAGE_DATA) != null) && !isCacheFillerRequest)
                {
                    cmsPageDataList =
                        ScanwebCacheManager.Instance.LookInCache<List<MobilePageData>>(Reference.MOBILE_CMS_PAGE_DATA);
                }
                else
                {
                    cmsPageDataList = new List<MobilePageData>();
                    string[] cmsPageReferenceKeys = Reference.MOBILE_CMS_PAGE_DATA_LIST.Split(new char[] { ',' });
                    foreach (string cmsPageReferenceKey in cmsPageReferenceKeys)
                    {
                        cmsPageDataList.Add(GetMobilePageData(cmsPageReferenceKey));
                    }
                    ScanwebCacheManager.Instance.AddToCache(Reference.MOBILE_CMS_PAGE_DATA,cmsPageDataList, new Collection<object>{true},
                        (Func<bool,List<MobilePageData>>)GetMobileSitePageData);
                }
            }
            catch (Exception exp)
            {
                AppLogger.LogInfoMessage(string.Format("{0} Failed to GetMobileSitePageData. {1}",
                                                       Reference.APPLICATION_ID, exp.Message));
            }
            return cmsPageDataList;
        }

        #endregion

        #endregion
    }
}