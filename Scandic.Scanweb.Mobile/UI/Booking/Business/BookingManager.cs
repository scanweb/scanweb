﻿//  Description					:   BookingManager                                        //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Common.Interface;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Entity.Model;
using BookingModel = Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using BookingModelMobile = Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;

namespace Scandic.Scanweb.Mobile.UI.Booking.Business
{
    /// <summary>
    /// This class will expose the business logic involved
    /// in booking process.
    /// </summary>
    public class BookingManager
    {
        private BookingContext bookingContext;
        private IUserInfoRespository userInfoRepository;
        private ISiteInfoRepository siteRepository;

        #region Constructors

        /// <summary>
        /// Constructor of BookingManager
        /// </summary>
        /// <param name="currentContext"></param>
        public BookingManager(BookingContext currentContext)
        {
            bookingContext = currentContext;
            userInfoRepository =
                DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
            siteRepository = DependencyResolver.Instance.GetService(typeof(ISiteInfoRepository)) as ISiteInfoRepository;
        }

        #endregion

        #region members

        private bool FirstTimeCall = true;
        private bool removeSortByDiscount = false;
        private bool isPromoSearch = false;
        #endregion

        #region PublicMethods

        #region CreateHotelSearchEntity

        /// <summary>
        /// Create Search Enity object for Regular Booking module
        /// </summary>
        /// <param name="searchInput"></param>
        /// <returns></returns>
        public HotelSearchEntity PrepareHotelSearchEntity(BookingModel.SearchHotelModel searchInput)
        {
            HotelSearchEntity hotelSearch = null;
            string childDetailText = string.Empty;

            hotelSearch = new HotelSearchEntity();
            SearchedForEntity searchedFor = null;

            List<HotelSearchRoomEntity> hotelSearchRoomList = new List<HotelSearchRoomEntity>();
            HotelSearchRoomEntity hotelSearchRoom = new HotelSearchRoomEntity();
            try
            {
                if (!string.IsNullOrEmpty(searchInput.SearchDestination))
                {
                    searchedFor = new SearchedForEntity(searchInput.SearchDestinationId);
                    if (searchInput.SearchBy == BookingModel.SearchByType.Hotel)
                        searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.Hotel;
                    else
                        searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.City;

                    searchedFor.SearchCode = searchInput.SearchDestinationId;
                    searchedFor.SearchString = searchInput.SearchDestination;
                    hotelSearch.SearchedFor = searchedFor;
                }
                else
                {
                    hotelSearch.SearchedFor = searchedFor;
                }

                if (searchInput.CheckInDate.HasValue && searchInput.CheckOutDate.HasValue)
                {
                    hotelSearch.ArrivalDate = searchInput.CheckInDate.Value;
                    hotelSearch.DepartureDate = searchInput.CheckOutDate.Value;
                }
                hotelSearch.NoOfNights = DateUtil.DateDifference(hotelSearch.DepartureDate, hotelSearch.ArrivalDate);
                hotelSearch.AdultsPerRoom = searchInput.NumberOfAdults.Value;
                //Ashish
                // hotelSearch.ChildrenPerRoom = 0;
                hotelSearch.ChildrenPerRoom = searchInput.NumberOfChildren.Value;
                hotelSearch.ChildrenOccupancyPerRoom = GetNoOfChildrenToBeAccommodated(searchInput.ChildrenInformation);

                hotelSearch.RoomsPerNight = 1;

                hotelSearch.SearchingType = GetSearchType(searchInput.SearchType, searchInput.BookingCode);

                hotelSearch.CampaignCode = string.IsNullOrEmpty(searchInput.BookingCode)
                                               ? null
                                               : searchInput.BookingCode;

                if (hotelSearch.SearchingType == SearchType.CORPORATE)
                {
                    hotelSearch.QualifyingType = Utility.GetQualifyingType(hotelSearch.CampaignCode);
                }

                hotelSearchRoom.AdultsPerRoom = searchInput.NumberOfAdults.Value;
                List<HotelSearchRoomEntity> listHotelSearchRoomEntity = new List<HotelSearchRoomEntity>();
                string cot = string.Empty, extraBed = string.Empty, sharingBed = string.Empty;
                GetLocaleSpecificChildrenAccomodationTypes(ref cot, ref extraBed, ref sharingBed);
                string[] bedTypes = new string[3]
                                    {
                                        cot, extraBed, sharingBed
                                    };
                List<ChildrensDetailsEntity> childrenDetailsallRooms = new List<ChildrensDetailsEntity>();
                bool errorSettingChildBedType = false;
                CreateChildrenDetails(listHotelSearchRoomEntity, bedTypes, childrenDetailsallRooms,
                                      searchInput.NumberOfChildren, searchInput.NumberOfAdults, searchInput.ChildrenInformation, ref childDetailText);
                hotelSearch.ListRooms = listHotelSearchRoomEntity;
                searchInput.ChildrenDetailText = childDetailText;
                //hotelSearchRoomList.Add(hotelSearchRoom);
                //hotelSearch.ListRooms = hotelSearchRoomList;

                if ((userInfoRepository != null && userInfoRepository.IsUserAuthenticated) &&
                    ((searchInput.SearchType == BookingModel.SearchType.Regular)
                     || (searchInput.SearchType == BookingModel.SearchType.BonusCheque)
                     || (searchInput.SearchType == BookingModel.SearchType.Corporate)))
                {
                    LoyaltyDetailsEntity loyaltyDetails = userInfoRepository.GetLoyaltyDetails();
                    if (null != loyaltyDetails)
                    {
                        long membershipNumber;
                        bool parseStatus = long.TryParse(loyaltyDetails.MembershipID, out membershipNumber);
                        if (parseStatus)
                        {
                            hotelSearch.MembershipID = membershipNumber;
                        }

                        LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID = loyaltyDetails.NameID;
                    }
                }

                Reservation2SessionWrapper.CurrentLanguage = EPiServer.Globalization.ContentLanguage.PreferredCulture.Name;
                MiscellaneousSessionWrapper.MobileContentLanguage = Reservation2SessionWrapper.CurrentLanguage;
            }
            catch (Exception exp)
            {
                AppLogger.LogInfoMessage(string.Format("ScanwebMobile: Error in creating hotel search entity - {0}.",
                                                       exp.Message));
                MiscellaneousSessionWrapper.MobileContentLanguage = null;
            }


            return hotelSearch;
        }
        /// <summary>
        /// Get the locate specific string for cot, extra bed and Child in adult's bed
        /// </summary>
        /// <param name="cot">locale for cot</param>
        /// <param name="extraBed">locale for extra bed</param>
        /// <param name="sharingBed">locale for Child in adult's bed</param>
        public void GetLocaleSpecificChildrenAccomodationTypes
            (ref string cot, ref string extraBed, ref string sharingBed)
        {
            cot = WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/crib");
            extraBed = WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/extrabed");
            sharingBed =
                WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/sharingbed");
        }
        #endregion

        #region CleanUp SearchResults

        /// <summary>
        /// Clear previous search results.
        /// </summary>
        public void ClearPreviousSearchResults(BookingModel.SearchHotelModel searchInput)
        {
            WebUtil.ClearBlockedRooms();
            if (!string.IsNullOrEmpty(searchInput.BookingCode) && searchInput.SearchType == BookingModel.SearchType.Regular)
            {
                TotalGeneralAvailableHotelsSessionWrapper.PromoGeneralAvailableHotels = 0;
            }
            AlternateCityHotelsSearchSessionWrapper.TotalRegionalAltCityHotelCount = -1;
            TotalRegionalAvailableHotelsSessionWrapper.TotalRegionalAvailableHotels = 0;
            TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels = 0;
            CleanSessionSearchResultsSessionWrapper.CleanSessionSearchResults();
            AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone = false;
            HotelResultsSessionWrapper.HotelDetails = null;
            AlternateCityHotelsSearchSessionWrapper.IsDestinationAlternateFlow = false;
            ClearPreviousRateResults();
        }

        /// <summary>
        /// Clear previous rate results.
        /// </summary>
        public void ClearPreviousRateResults()
        {
            MiscellaneousSessionWrapper.HotelRoomRateResults = null;
            HotelRoomRateSessionWrapper.ListHotelRoomRate = null;
        }

        #endregion

        /// <summary>
        /// Confirms booking
        /// </summary>
        /// <param name="guestList"></param>
        public void ConfirmBooking(List<GuestInformationEntity> guestList)
        {
            BookingDetailsUtility bookingDetailsUtility = new BookingDetailsUtility();
            bookingDetailsUtility.ConfirmBooking(guestList);
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Gets available hotels
        /// </summary>
        /// <param name="sortKey"></param>
        /// <param name="currentDisplayedHotels"></param>
        /// <param name="nextSetOfHotels"></param>
        /// <param name="requestTime"></param>
        /// <param name="currentTime"></param>
        /// <returns></returns>
        public HotelResults GetAvailableHotels(BookingModel.DestinationSortType sortKey, int currentDisplayedHotels,
                                               int nextSetOfHotels, DateTime requestTime, DateTime currentTime)
        {
            List<MessageDetails> messages = new List<MessageDetails>();
            int nextDisplaySet = currentDisplayedHotels + nextSetOfHotels;
            int maxHotelCount = 0;
            var appConfig = siteRepository.GetGeneralConfig<ApplicationConfigSection>();

            AppLogger.LogInfoMessage("ScanwebMobile: GetAvailableHotels start");

            List<HotelDetail> availableHotelsList = null;
            List<IHotelDetails> hotelListResult = SelectHotelUtil.GetAllHotels();
            Boolean stillMoreHotelsToFetch = SelectHotelUtil.IsSearchToBeDone();
            var hotelResults = new HotelResults();
            bool noHotelsFound = false;

            try
            {
                HotelSearchEntity hotelSearchEntity = SearchCriteriaSessionWrapper.SearchCriteria;
                bool processFetchedHotels = true;
                if (hotelListResult != null)
                {
                    if ((!stillMoreHotelsToFetch && hotelListResult.Count == 0 &&
                         !AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                        || (stillMoreHotelsToFetch && RedemptionHotelsAvailableSessionWrapper.NoRedemptionHotelsAvailable))
                    {
                        InitiateSearch(hotelSearchEntity, true);
                    }
                    else
                    {
                        bool promoSearchShowHotelsWithPublicRates = false;

                        if (!stillMoreHotelsToFetch && hotelSearchEntity.SearchingType == SearchType.REGULAR
                            && hotelSearchEntity.CampaignCode != null)
                        {
                            //if (TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels == TotalGeneralAvailableHotelsSessionWrapper.PromoGeneralAvailableHotels)
                            //{
                            //    noHotelsFound = true;
                            //}
                            if (!SelectHotelUtil.HasHotelsWithPromotionRates())
                            {
                                //No Hotel with Promocodes found in Promo Search Case so remove Discount Type sort option
                                removeSortByDiscount = true;
                                if (AlternateCityHotelsSearchSessionWrapper.TotalRegionalAltCityHotelCount == -1
                                    && TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels != TotalGeneralAvailableHotelsSessionWrapper.PromoGeneralAvailableHotels)
                                {
                                    //Alternate hotels for promocode flow.
                                    processFetchedHotels = false;
                                    AlternateCityHotelsSearchSessionWrapper.IsDestinationAlternateFlow = true;
                                    SelectHotelUtil.InitiateAsynchronousSearchForAltCityHotels(hotelSearchEntity);
                                    AlternateCityHotelsSearchSessionWrapper.IsDestinationAlternateFlow = false;
                                }
                                else
                                {
                                    promoSearchShowHotelsWithPublicRates = true;
                                    //SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = null;
                                }
                            }
                        }

                        if (processFetchedHotels && !noHotelsFound)
                        {
                            foreach (var item in hotelListResult)
                            {
                                TripAdvisorPlaceEntity TAEntity = WebUtil.GetTAReviewEntityForHotel(item.HotelDestination.TALocationID);
                                item.HideHotelTARating = item.HotelDestination != null ? item.HotelDestination.HideHotelTARating : false;
                                if (!item.HideHotelTARating)
                                    item.TripAdvisorHotelRating = TAEntity != null ? TAEntity.AverageRating : 0.0;
                                else
                                    item.TripAdvisorHotelRating = 0.0;
                            }

                            if (hotelSearchEntity.SearchingType == SearchType.CORPORATE &&
                                (!Utility.IsBlockCodeBooking) && (!SelectHotelUtil.HasHotelsWithNegotiatedRates()))
                            {
                                if (!AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                                {
                                    removeSortByDiscount = true;
                                }
                            }
                            if (hotelSearchEntity.SearchingType == SearchType.REGULAR && !string.IsNullOrEmpty(hotelSearchEntity.CampaignCode))
                            {
                                isPromoSearch = true;
                            }

                            availableHotelsList = GetHotelDetailsList(sortKey, hotelSearchEntity, promoSearchShowHotelsWithPublicRates,
                                                                      currentDisplayedHotels, nextSetOfHotels, out maxHotelCount);
                        }
                    }
                }

                if ((!WaitingForSearchResults() && processFetchedHotels) || noHotelsFound)
                {
                    messages = SetErrorMessages(hotelSearchEntity);
                    hotelResults = ReturnSearchResults(availableHotelsList, messages, nextSetOfHotels, nextDisplaySet, maxHotelCount);
                }
                else
                {
                    hotelResults.MoreHotelsToFetch = true;
                }

                return hotelResults;
            }
            catch (OWSException owsException)
            {
                AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);
                return ReturnExceptions(owsException.ErrCode, owsException.Message);
            }
            catch (BusinessException busException)
            {
                AppLogger.LogCustomException(busException, AppConstants.BUSINESS_EXCEPTION);
                return ReturnExceptions(busException.ErrCode, busException.Message);
            }
            catch (ContentDataAccessException cdaException)
            {
                if (string.Equals(cdaException.ErrorCode, AppConstants.BONUS_CHEQUE_NOT_FOUND, StringComparison.InvariantCultureIgnoreCase))
                {
                    messages.Add(GetMessageDetails(Reference.BONUS_CHEQUE_NOT_FOUND,
                                                             WebUtil.GetTranslatedText(Reference.BONUS_CHEQUE_NOT_FOUND_XPATH)));
                    hotelResults = ReturnSearchResults(availableHotelsList, messages, nextSetOfHotels, nextDisplaySet, maxHotelCount);
                    return hotelResults;
                }
                else
                    throw;
            }
            catch (Exception genException)
            {
                AppLogger.LogFatalException(genException);
                AppLogger.LogInfoMessage(genException.ToString());
                return ReturnExceptions("AppError: ", genException.Message);
            }
        }

        private HotelResults ReturnExceptions(string errorCode, string errorMessage)
        {
            List<ErrorDetails> errors = new List<ErrorDetails>();
            HotelResults exceptions = new HotelResults();
            errors.Add(new ErrorDetails { ErrorCode = errorCode, ErrorMessaage = errorMessage });
            exceptions.Errors = errors;
            return exceptions;
        }

        private HotelResults ReturnSearchResults(List<HotelDetail> availableHotelsList, List<MessageDetails> messages,
                                                 int nextSetOfHotels, int totalHotelsToDisplay, int maxHotelCount)
        {
            HotelResults hotelResults = new HotelResults();
            AppLogger.LogInfoMessage("ScanwebMobile: ReturnSearchResults start");

            if ((availableHotelsList != null && availableHotelsList.Count >= nextSetOfHotels))
            {
                hotelResults.MoreHotelsToFetch = SelectHotelUtil.IsSearchToBeDone() || (maxHotelCount > totalHotelsToDisplay);
                AppLogger.LogInfoMessage("ScanwebMobile: MoreHotelsToFetch: " + hotelResults.MoreHotelsToFetch);
            }
            else if (availableHotelsList != null && !SelectHotelUtil.IsSearchToBeDone())
            {
                hotelResults.MoreHotelsToFetch = false;
            }
            else
            {
                hotelResults.MoreHotelsToFetch = false;
            }

            if (AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
            {
                hotelResults.IsAlternateSearch = true;
            }

            hotelResults.HotelList = availableHotelsList;
            hotelResults.Messages = messages;
            hotelResults.HotelsFound = maxHotelCount;
            hotelResults.RemoveSoryByDiscountType = removeSortByDiscount;
            hotelResults.IsPromoSearch = isPromoSearch;
            return hotelResults;
        }

        private static List<IHotelDetails> GetSortedHotelList(BookingModel.DestinationSortType sortKey)
        {
            List<IHotelDetails> hotels = SelectHotelUtil.GetAllHotels();

            if (hotels != null && hotels.Count > 0)
            {
                switch (sortKey)
                {
                    case BookingModel.DestinationSortType.Price:
                        {
                            hotels.Sort(new RateComparer());
                            SelectHotelUtil.SortByMaxRate(hotels);
                            hotels = HotelResultsSessionWrapper.HotelResults;
                        }
                        break;
                    case BookingModel.DestinationSortType.DistanceToCity:
                        hotels.Sort(new CityCentreDistanceComparer());
                        break;
                    case BookingModel.DestinationSortType.DistanceToSearchedDestination:
                        hotels.Sort(new AlternateHotelsDistanceComparer());
                        break;
                    case BookingModel.DestinationSortType.Points:
                        hotels.Sort(new PointComparer());
                        break;
                    case BookingModel.DestinationSortType.DistanceFormCurrentLocation:
                        hotels.Sort(new CurrentLocationDistanceComparer());
                        break;
                    case BookingModel.DestinationSortType.TripAdvisorRatings:
                        /*hotels.Sort(new HotelNameComparer());
                        hotels.Sort(new TripAdvisorRatingComparer());*/
                        SelectHotelUtil.SortByTARatingMaxRate(hotels);
                        hotels = HotelResultsSessionWrapper.HotelResults;
                        break;
                    case BookingModel.DestinationSortType.DiscountType:
                        //hotels.Sort(new RateComparer());
                        //SelectHotelUtil.SortByMaxRate(hotels);
                        //hotels.Sort(new PromoComparer());
                        SelectHotelUtil.SortByDiscountTypeAndRate(hotels);
                        break;
                    default:
                        hotels.Sort(new HotelNameComparer());
                        break;
                }
            }
            return hotels;
        }

        private bool WaitingForSearchResults()
        {
            bool moreHotelsToFetch = SelectHotelUtil.IsSearchToBeDone();
            List<IHotelDetails> hotelListResult = SelectHotelUtil.GetAllHotels();
            bool waitForResults = false;

            if (hotelListResult != null && hotelListResult.Count <= 0 && !moreHotelsToFetch)
            {
                waitForResults = false;
            }
            else if (hotelListResult != null && RedemptionHotelsAvailableSessionWrapper.NoRedemptionHotelsAvailable)
            {
                waitForResults = false;
                AppLogger.LogInfoMessage("ScanwebMobile: WaitingForSearchResults- NoRedemptionHotelsAvailable");
            }
            else if (moreHotelsToFetch && hotelListResult != null)
            {
                waitForResults = true;
                AppLogger.LogInfoMessage("ScanwebMobile: WaitingForSearchResults- waitForResults:true");
            }
            return waitForResults;
        }

        private List<MessageDetails> SetErrorMessgesForHotelFlow(HotelSearchEntity hotelSearchEntity,
                                                                 RoomRateResults roomRateCategoryResults)
        {
            List<MessageDetails> messages = new List<MessageDetails>();
            if (hotelSearchEntity != null)
            {
                switch (hotelSearchEntity.SearchingType)
                {
                    case SearchType.REGULAR:
                        //if (hotelSearchEntity.CampaignCode != null && roomRateCategoryResults.AlternateHotelSearchRequired &&
                        //    !SelectHotelUtil.HasHotelsWithPromotionRates())
                        if (hotelSearchEntity.CampaignCode != null && hotelSearchEntity.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City
                            && !SelectHotelUtil.SelectedHotelHasPromoRates(hotelSearchEntity.SelectedHotelCode))
                        {
                            if (roomRateCategoryResults.RoomRateDetails != null)
                            {
                                messages.Add(GetMessageDetails(Reference.NO_HOTELS_WITH_PROMOCODE, string.Format(WebUtil.GetTranslatedText(
                                                                   AppConstants.ALTERNATE_HOTELS_PROMO_MESSAGE2), hotelSearchEntity.CampaignCode)));
                            }
                            else
                            {
                                messages.Add(GetMessageDetails(Reference.NO_HOTELS_WITH_PROMOCODE,
                                                               WebUtil.GetTranslatedText(AppConstants.ALTERNATE_HOTELS_PROMO_MESSAGE3)));
                            }
                        }
                        break;

                    case SearchType.BONUSCHEQUE:
                        if (roomRateCategoryResults.AlternateHotelSearchRequired)
                        {
                            if (roomRateCategoryResults.RoomRateDetails != null)
                            {
                                messages.Add(GetMessageDetails(Reference.BONUS_CHEQUE_NOT_FOUND, WebUtil.GetTranslatedText(AppConstants.NO_HOTELS_FOUND)));
                            }
                            else
                            {
                                try
                                {
                                    if (string.IsNullOrEmpty(hotelSearchEntity.HotelCountryCode))
                                    {
                                        var countryPages = CMSDataManager.CountryCityAndHotel();
                                        var countryCode = from cnts in countryPages
                                                          from cities in cnts.SearchedCities
                                                          from hotels in cities.SearchedHotels
                                                          where (hotels != null) && string.Equals(hotels.OperaDestinationId, hotelSearchEntity.SelectedHotelCode)
                                                          select cnts.CountryCode;
                                        hotelSearchEntity.HotelCountryCode = (countryCode != null &&
                                                                              !string.IsNullOrEmpty(
                                                                                  countryCode.FirstOrDefault()))
                                                                                 ? countryCode.FirstOrDefault()
                                                                                 : null;
                                    }
                                    CMSDataManager.GetBonusChequeValue(hotelSearchEntity);
                                }
                                catch (ContentDataAccessException cdaException)
                                {

                                    if (string.Equals(cdaException.ErrorCode, AppConstants.BONUS_CHEQUE_NOT_FOUND,
                                                      StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        messages.Add(GetMessageDetails(Reference.BONUS_CHEQUE_NOT_FOUND,
                                                                       WebUtil.GetTranslatedText(Reference.BONUS_CHEQUE_NOT_FOUND_XPATH)));
                                    }
                                    else
                                        throw;
                                }
                            }
                        }
                        break;
                }
            }

            return messages;
        }

        private List<MessageDetails> SetErrorMessages(HotelSearchEntity hotelSearchEntity)
        {
            List<MessageDetails> messages = new List<MessageDetails>();
            List<IHotelDetails> hotelListResult = SelectHotelUtil.GetAllHotels();
            bool IsMoreHotelsToFecth = SelectHotelUtil.IsSearchToBeDone();

            if (hotelListResult != null && hotelListResult.Count <= 0 && !IsMoreHotelsToFecth)
            {
                messages.Add(GetMessageDetails(Reference.NO_HOTELS_FOUND,
                                               WebUtil.GetTranslatedText(AppConstants.NO_HOTELS_FOUND)));
            }
            else if (hotelListResult != null && !IsMoreHotelsToFecth)
            {
                if (AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone && hotelListResult.Count > 0)
                {
                    if (bookingContext != null && bookingContext.SearchHotelPage != null
                             && bookingContext.SearchHotelPage.SearchBy == BookingModel.SearchByType.Hotel)
                    {
                        string alternateHotelMessage = string.Empty;
                        if (hotelSearchEntity.CampaignCode != null && hotelSearchEntity.SearchingType == SearchType.REGULAR)
                        {
                            alternateHotelMessage = string.Format(WebUtil.GetTranslatedText(AppConstants.ALTERNATE_HOTELS_PROMO_MESSAGE1),
                                hotelSearchEntity.CampaignCode);
                        }
                        else
                        {
                            alternateHotelMessage = WebUtil.GetTranslatedText(Reference.ALTERNATE_HOTELS_MESSAGE);
                        }

                        alternateHotelMessage = string.Format(alternateHotelMessage, bookingContext.SearchHotelPage.SearchDestination);
                        messages.Add(GetMessageDetails(Reference.ALTERNATE_HOTELS_AVAILABLE, alternateHotelMessage));
                    }
                    else
                    {
                        if (hotelSearchEntity.CampaignCode != null)
                        {
                            if (SelectHotelUtil.TotalPromotionRateAvailableHotels() > 0)

                                messages.Add(GetMessageDetails(Reference.ALTERNATE_HOTELS_AVAILABLE,
                                                                string.Format(WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE1),
                                                               hotelSearchEntity.CampaignCode)));
                            else
                                messages.Add(GetMessageDetails(Reference.ALTERNATE_HOTELS_AVAILABLE,
                                                                string.Format(WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE2),
                                                               hotelSearchEntity.CampaignCode)));
                        }
                        else
                        {
                            messages.Add(GetMessageDetails(Reference.ALTERNATE_HOTELS_AVAILABLE, string.Format(
                            WebUtil.GetTranslatedText(AppConstants.ALT_CITY_HOTELS_MSG), hotelSearchEntity.SearchedFor.SearchString)));
                        }
                    }
                }

                switch (hotelSearchEntity.SearchingType)
                {
                    case (SearchType.CORPORATE):
                        if ((!Utility.IsBlockCodeBooking) && (!SelectHotelUtil.HasHotelsWithNegotiatedRates()))
                        {
                            if (!AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                            {
                                string msg = string.Empty;
                                if (!string.IsNullOrEmpty(hotelSearchEntity.CampaignCode))
                                    msg = string.Format(WebUtil.GetTranslatedText(Reference.NO_HOTELS_WITH_DNUMBER_XPATH), hotelSearchEntity.CampaignCode);
                                else
                                    msg = string.Format(WebUtil.GetTranslatedText(Reference.NO_HOTELS_WITH_DNUMBER_XPATH), string.Empty);
                                messages.Add(GetMessageDetails(Reference.NO_HOTELS_WITH_DNUMBER, msg));
                                removeSortByDiscount = true;
                            }
                        }
                        break;
                    case SearchType.REGULAR:
                        if (hotelSearchEntity.CampaignCode != null)
                        {
                            if (TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels == TotalGeneralAvailableHotelsSessionWrapper.PromoGeneralAvailableHotels)
                            {
                                messages.Add(GetMessageDetails(Reference.NO_HOTELS_WITH_PROMOCODE,
                                    string.Format(WebUtil.GetTranslatedText(AppConstants.NO_HOTELS_WITH_PROMOCODE_MSG), hotelSearchEntity.CampaignCode)));
                            }
                            else if (!SelectHotelUtil.HasHotelsWithPromotionRates() && !AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                            {
                                messages.Add(GetMessageDetails(Reference.NO_HOTELS_WITH_PROMOCODE,
                                    string.Format(WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE2), hotelSearchEntity.CampaignCode)));
                            }

                        }

                        break;
                }
            }
            else
            {
                messages.Add(GetMessageDetails(Reference.NO_HOTELS_FOUND, WebUtil.GetTranslatedText(AppConstants.NO_HOTELS_FOUND)));
            }

            if (messages != null && messages.Count > 0 && string.Equals(messages[0].MessageCode, Reference.NO_HOTELS_FOUND) &&
                hotelSearchEntity.CampaignCode != null && hotelSearchEntity.SearchingType == SearchType.REGULAR
                                && hotelSearchEntity.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City)
            {
                messages[0].MessageInfo = WebUtil.GetTranslatedText(AppConstants.ALTERNATE_DESTINATION_PROMO_MESSAGE3);
            }

            return messages;
        }

        /// <summary>
        /// This method gets the error details object.
        /// </summary>
        /// <param name="scanwebRepository"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        private MessageDetails GetMessageDetails(string msgCode, string msgInfo)
        {
            MessageDetails messageDetail = null;
            messageDetail = new MessageDetails();
            messageDetail.MessageCode = msgCode;
            messageDetail.MessageInfo = msgInfo;
            return messageDetail;
        }

        public void InitiateSearch(HotelSearchEntity hotelSearchEntity, bool fireAlternativeSearch)
        {
            SearchCriteriaSessionWrapper.SearchCriteria = hotelSearchEntity;
            if (fireAlternativeSearch)
            {
                AppLogger.LogInfoMessage("ScanwebMobile: Initiating alternate city search");
                AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone = true;
                TotalRegionalAvailableHotelsSessionWrapper.TotalRegionalAvailableHotels = 0;
                SelectHotelUtil.InitiateAsynchronousSearchForAltCityHotels(hotelSearchEntity);
            }
            else
            {
                AppLogger.LogInfoMessage("ScanwebMobile: Initiating city search");
                SelectHotelUtil.InitiateAsynchronousSearch(hotelSearchEntity);
            }
        }

        /// <summary>
        /// This method gets the all available hotels list
        /// </summary>
        /// <param name="hotelsList"></param>
        /// <param name="hotelSearchEntity"></param>
        /// <param name="showHotelsWithPublicRates"></param>
        /// <returns></returns>
        private List<HotelDetail> GetHotelDetailsList(BookingModel.DestinationSortType sortKey, HotelSearchEntity hotelSearchEntity,
                                                      bool showHotelsWithPublicRates, int currentlyDisplayedHotels,
                                                      int nextHotelsToDisplayed, out int maxHotelCount)
        {
            List<HotelDetail> availableHotels = null;
            IHotelDisplayInformation hotelDisplay = null;
            List<IHotelDetails> hotelsList = bookingContext.SearchHotelPage.IsSearchfromCurrentLocation == true ? SortHotelResultsByCurrentGeoPosition(sortKey) : GetSortedHotelList(sortKey);
            maxHotelCount = 0;
            if (hotelsList != null && hotelsList.Count > 0)
            {
                availableHotels = new List<HotelDetail>();

                int hotelCount = hotelsList.Count;
                maxHotelCount = hotelCount;
                int currentHotelIndex = currentlyDisplayedHotels > hotelCount ? hotelCount : currentlyDisplayedHotels;
                currentHotelIndex = currentHotelIndex < 0 ? 0 : currentHotelIndex;
                int hotelLimit = currentHotelIndex + nextHotelsToDisplayed > hotelCount ? hotelCount
                                     : currentHotelIndex + nextHotelsToDisplayed;

                for (var index = currentHotelIndex; index < hotelLimit; index++)
                {
                    IHotelDetails hotelDetail = hotelsList[index];
                    hotelDisplay = SelectHotelUtil.GetHotelDisplayObject(hotelSearchEntity.SearchingType, hotelDetail,
                                                                         hotelSearchEntity.CampaignCode, showHotelsWithPublicRates, false);

                    if (hotelDisplay != null)
                    {
                        availableHotels.Add(GetMappedHotelDetail(hotelDisplay, hotelDetail, sortKey,
                                                                 hotelSearchEntity.SearchedFor.SearchString));
                    }
                }
            }

            return availableHotels;
        }

        private List<IHotelDetails> SortHotelResultsByCurrentGeoPosition(BookingModel.DestinationSortType sortKey)
        {
            List<IHotelDetails> hotels = SelectHotelUtil.GetAllHotels();
            foreach (IHotelDetails hotel in hotels)
            {

                hotel.DistanceFromCurrentLocationToSearchedHotel = Convert.ToDouble(Math.Round(WebUtil.CalculateCurrentLocationDistance
                                                                (bookingContext.SearchHotelPage.CurrentCoordinate.X, bookingContext.SearchHotelPage.CurrentCoordinate.Y,
                                                                hotel.HotelDestination.Coordinate.Y, hotel.HotelDestination.Coordinate.X), 1), CultureInfo.InvariantCulture);
            }

            HotelResultsSessionWrapper.HotelResults = hotels;
            return GetSortedHotelList(sortKey);
        }


        /// <summary>
        /// This method maps the scanweb hotel detail entity to mobile site's hotel detail entity.
        /// </summary>
        /// <param name="hotelInfo"></param>
        /// <returns></returns>
        private HotelDetail GetMappedHotelDetail(IHotelDisplayInformation hotelInfo, IHotelDetails hotel,
                                                 BookingModel.DestinationSortType sortKey, string searchedFor)
        {
            HotelDetail hotelDetails = new HotelDetail();
            var appConfig = siteRepository.GetGeneralConfig<ApplicationConfigSection>();
            var imageValutHost = appConfig.GetMessage(Reference.ImageVaultHost);
            var imageWidthRegx = appConfig.GetMessage(Reference.ImageURLWidthRegx);
            var imageWidthReplacement = appConfig.GetMessage(Reference.ImageURLWidthReplacmentString);

            hotelDetails.Id = hotelInfo.ID;
            hotelDetails.Name = hotelInfo.HotelName;
            hotelDetails.Description = hotelInfo.Teaser;
            hotelDetails.Address = hotelInfo.Address;

            hotelDetails.CityCenterDistance = hotelInfo.CityCenterDistance;
            hotelDetails.ImageUrl = string.Format("{0}/{1}", imageValutHost, hotelInfo.ImageSrc);
            hotelDetails.ImageUrl = Regex.Replace(hotelDetails.ImageUrl, imageWidthRegx, imageWidthReplacement);
            hotelDetails.AvailableRoomPrice = hotelInfo.Price;
            hotelDetails.PerNight = hotelInfo.PerNight;
            hotelDetails.CorporateCode = hotelInfo.RateTitle;

            if (sortKey == BookingModel.DestinationSortType.DistanceToSearchedDestination)
            {
                string alternativeHotelCityDistance = WebUtil.GetTranslatedText(Reference.ALTERNATE_HOTEL_DISTANCE_XPATH);
                hotelDetails.CityCenterDistance = hotel.AlternateHotelsDistance != 0
                                                      ? string.Format(alternativeHotelCityDistance,
                                                                      hotel.AlternateHotelsDistance, searchedFor)
                                                      : string.Empty;
            }
            else if (sortKey == BookingModel.DestinationSortType.DistanceToCity)
            {
                string cityCenterDistanceString = WebUtil.GetTranslatedText(Reference.CITY_CENTER_DISTANCE_XPATH);

                hotelDetails.CityCenterDistance = string.Format(cityCenterDistanceString,
                                                                hotel.HotelDestination.CityCenterDistance,
                                                                hotel.HotelDestination.HotelAddress.City);
            }
            else
            {
                hotelDetails.CityCenterDistance = string.Empty;
            }
            if (sortKey == BookingModel.DestinationSortType.DistanceFormCurrentLocation)
            {
                hotelDetails.CityCenterDistance = string.Empty;
                string currentLocationDistanceString = WebUtil.GetTranslatedText(Reference.CURRENT_LOCATION_DISTANCE_XPATH);
                hotelDetails.CurrentLocationDistance = string.Format(currentLocationDistanceString, hotel.DistanceFromCurrentLocationToSearchedHotel);
            }
            else
            {
                hotelDetails.CurrentLocationDistance = string.Empty;
            }

            hotelDetails.TALocationID = hotel.HotelDestination.TALocationID;
            hotelDetails.HideCityTARating = hotel.HotelDestination.HideCityTARating;
            hotelDetails.HideTARating = WebUtil.HideTARating(false, hotel.HotelDestination.HideHotelTARating == true ? hotel.HotelDestination.HideHotelTARating : false);
            if (!hotelDetails.HideTARating)
            {
                hotelDetails.TAReviewPopUpURL = WebUtil.CreateRatingPopupURL(hotelDetails.TALocationID, LanguageRedirectionHelper.GetCurrentLanguage());
                TripAdvisorPlaceEntity TAEntity = WebUtil.GetTAReviewEntityForHotel(hotelDetails.TALocationID);
                if (TAEntity != null)
                {
                    hotelDetails.TAReviewCountText = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/TripAdvisorRatingCountLinkText").Replace("{0}", Convert.ToString(TAEntity.ReviewCount));
                    hotelDetails.TARatingImageURL = !string.IsNullOrEmpty(TAEntity.TripAdvisorImageURL) ? TAEntity.TripAdvisorImageURL : TAEntity.TripAdvisorImageDefaultURL;
                }
                else
                {
                    hotelDetails.TAReviewCountText = "";
                    hotelDetails.TARatingImageURL = "";
                    hotelDetails.HideTARating = true;
                }
            }
            else
            {
                hotelDetails.TAReviewPopUpURL = "";
                hotelDetails.TAReviewCountText = "";
                hotelDetails.TARatingImageURL = "";
            }
            if (SearchCriteriaSessionWrapper.SearchCriteria != null &&
                SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REGULAR &&
                !string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode))
            {
                hotelDetails.HasPromotionRoomRates = ((PromotionHotelDetails)hotel).HasPromotionRoomRates;
                hotelDetails.PromoCode = ((PromotionHotelDetails)hotel).promotionCode;
            }
            return hotelDetails;
        }

        private SearchType GetSearchType(BookingModel.SearchType searchType, string bookingCode)
        {
            SearchType returnValue = SearchType.REGULAR;
            switch (searchType)
            {
                case BookingModel.SearchType.Regular:
                    {
                        if (string.IsNullOrEmpty(bookingCode))
                        {
                            returnValue = SearchType.REGULAR;
                        }
                        else
                        {
                            returnValue = GetBookingType(bookingCode);
                        }
                        break;
                    }
                case BookingModel.SearchType.BonusCheque:
                    {
                        returnValue = SearchType.BONUSCHEQUE;
                        break;
                    }
                case BookingModel.SearchType.Redemption:
                    {
                        returnValue = SearchType.REDEMPTION;
                        break;
                    }
                case BookingModel.SearchType.Corporate:
                    {
                        returnValue = SearchType.CORPORATE;
                        break;
                    }
                default:
                    {
                        returnValue = SearchType.REGULAR;
                        break;
                    }
            }
            return returnValue;
        }

        /// <summary>
        /// After merging of booking tabs Regular tab is used to identify the booking type.
        /// D - If code starts with D then it is Corporate Booking.
        /// L - If code starts with L then it is Travel agent booking.
        /// B - If code starts with B then it is block code booking.
        /// VO- IF code starts with VO then it is Voucher booking.
        /// </summary>
        /// <returns>Search Type</returns>
        private SearchType GetBookingType(string bookingCode)
        {
            SearchType returnValue = SearchType.REGULAR;
            if (bookingCode.StartsWith(Reference.CORPORATE_KEYCODE) ||
                bookingCode.StartsWith(Reference.TRAVELAGENT_KEYCODE)
                || bookingCode.StartsWith(Reference.BLOCKCODE_KEYCODE))
            {
                returnValue = SearchType.CORPORATE;
            }
            else if (bookingCode.StartsWith(Reference.VOUCHER_KEYCODE))
            {
                returnValue = SearchType.VOUCHER;
            }
            return returnValue;
        }

        #endregion

        #region Methods for HotelSearch

        /// <summary>
        /// This method returns hotel desination
        /// </summary>
        /// <param name="hotelSearchEntity"> hotel search entity</param>
        /// <returns></returns>
        private HotelDestination GetHotelDestination(HotelSearchEntity hotelSearchEntity)
        {
            AvailabilityController availabilityController = new AvailabilityController();

            return availabilityController.GetHotelDestinationEntity(hotelSearchEntity.SearchedFor.SearchCode);
        }

        /// <summary>
        /// This method returns hotel destination for a perticular hotel
        /// </summary>
        /// <param name="operaId">opera id</param>
        /// <returns></returns>
        private HotelDestination GetHotelDestination(string operaId)
        {
            AvailabilityController availabilityController = new AvailabilityController();

            return availabilityController.GetHotelDestinationEntity(operaId);
        }

        /// <summary>
        /// This method initiates the alternate hotel search for search hotel flow.
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <returns></returns>
        public bool InitiateAlternateHotelSearch(HotelSearchEntity hotelSearchEntity)
        {
            HotelDestination hotelDestination = GetHotelDestination(hotelSearchEntity);
            return InitiateAlternateHotelSearch(hotelDestination);
        }

        /// <summary>
        /// Display Alternate Hotels
        /// </summary>
        /// <param name="hotelDestination">
        /// HotelDestination
        /// </param>
        public bool InitiateAlternateHotelSearch(HotelDestination hotelDestination)
        {
            bool isAlternativeSearchFired = false;
            SearchedHotelCityNameSessionWrapper.SearchedHotelCityName = hotelDestination.HotelAddress.City;

            List<AlternativeHotelReference> alternateHotelsList = GetAlternateHotelsList(hotelDestination);
            if ((alternateHotelsList != null) && (alternateHotelsList.Count > 0))
            {
                SearchAlternateHotels(alternateHotelsList, ref isAlternativeSearchFired);
            }
            else
            {
                SetNoHotelAvailableFlags();
            }
            return isAlternativeSearchFired;
        }

        /// <summary>
        /// Get the List of alternate hotels 
        /// </summary>
        /// <returns>
        /// List of AlternativeHotelReference
        /// </returns>
        private List<AlternativeHotelReference> GetAlternateHotelsList(HotelDestination hotelDestination)
        {
            List<AlternativeHotelReference> alternateHotelsList =
                hotelDestination.AlternativeHotels;
            return alternateHotelsList;
        }

        /// <summary>
        /// Search for Alternate Hotels
        /// </summary>
        /// <param name="alternateHotelsList">
        /// List of AlternateHotel References
        /// </param>
        private void SearchAlternateHotels(List<AlternativeHotelReference> alternateHotelsList, ref bool isAlternativeSearchFired)
        {
            CleanSessionSearchResultsSessionWrapper.CleanSessionSearchResults();
            AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone = false;
            AvailabilityController availabilityController = new AvailabilityController();
            availabilityController.AlternateSearchHotels(alternateHotelsList);

            List<IHotelDetails> hotelDetails =
                HotelResultsSessionWrapper.GetHotelResults(HttpContext.Current.Session) as List<IHotelDetails>;

            //Merchandising:R3:Display ordinary rates for unavailable promo rates 
            if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode != null
                && SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REGULAR && HotelResultsSessionWrapper.HotelDetails != null)
            {
                hotelDetails.Add(HotelResultsSessionWrapper.HotelDetails);
            }

            if ((hotelDetails != null) && (hotelDetails.Count > 0))
            {
                TotalRegionalAvailableHotelsSessionWrapper.TotalRegionalAvailableHotels = hotelDetails.Count;
                TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels = hotelDetails.Count;
                hotelDetails.Sort(new HotelNameComparer());
                isAlternativeSearchFired = true;
            }
            else
            {
                SetNoHotelAvailableFlags();
                isAlternativeSearchFired = false;
            }
        }

        private void SetNoHotelAvailableFlags()
        {
            TotalRegionalAvailableHotelsSessionWrapper.TotalRegionalAvailableHotels = 0;
            TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels = 0;
        }

        /// <summary>
        /// This method fetches the rate details for a selected hotel
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <returns></returns>
        public IList<BaseRoomRateDetails> GetHotelRateDetails(HotelSearchEntity hotelSearch)
        {
            IList<BaseRoomRateDetails> listRoomRateDetails = null;
            AvailabilityController availController = new AvailabilityController();

            if (hotelSearch != null)
            {
                listRoomRateDetails = availController.GetSelectRateDetails(hotelSearch);

                if (Convert.ToBoolean(AppConstants.ENABLE_PUBLIC_RATE_SEARCH_FOR_PROMOTIONS))
                {
                    if (hotelSearch.SearchingType == SearchType.REGULAR && !string.IsNullOrEmpty(hotelSearch.CampaignCode))
                    {
                        string strCampaignCode = hotelSearch.CampaignCode;
                        hotelSearch.CampaignCode = null;
                        AppLogger.LogInfoMessage("Promo Search: An extra call to fetch the Public Rates for Promotional Search");
                        IList<BaseRoomRateDetails> listHotelPublicRoomRateDetails = SelectRoomUtil.MultiRoomAvailSearchOrPrefetch(hotelSearch);
                        hotelSearch.CampaignCode = strCampaignCode;
                        listRoomRateDetails = SelectRoomUtil.MergePublicRatesWithPromotionalRates(listRoomRateDetails, listHotelPublicRoomRateDetails);
                    }
                }
            }
            return listRoomRateDetails;
        }

        private RoomRateResults GetRatesDetailList(bool selectedHotelHasOnlyPublicRates)
        {
            RoomRateResults roomRateCategoryResults = new RoomRateResults();

            //HotelSearchEntity tempHotelSearch = null;
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            string currentCampaignCode = hotelSearch.CampaignCode;
            if (selectedHotelHasOnlyPublicRates)
            {
                SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = null;
            }

            IList<BaseRoomRateDetails> listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;

            if (listRoomRateDetails == null)
            {
                try
                {
                    listRoomRateDetails = GetHotelRateDetails(hotelSearch);
                }
                catch (OWSException owsException)
                {
                    if (!string.IsNullOrEmpty(currentCampaignCode))
                    {
                        SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = currentCampaignCode;
                    }
                    throw owsException;
                }

                HotelRoomRateSessionWrapper.ListHotelRoomRate = listRoomRateDetails;
            }

            if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
            {
                BaseRoomRateDetails roomRateDetails = listRoomRateDetails[0];
                if ((!Utility.IsBlockCodeBooking) && ((roomRateDetails.RateCategories == null
                                                       || (roomRateDetails.RateCategories.Count == 0)
                                                       || (roomRateDetails.RateRoomCategories == null))))
                {
                    HotelDestination hotelDestination = roomRateDetails.HotelDestination;
                    InitiateAlternateHotelSearch(hotelSearch);
                    roomRateCategoryResults.AlternateHotelSearchRequired = true;
                }
                else
                {
                    roomRateCategoryResults.RateCategoryPopups = PrepareRateCategoryListForPopups(roomRateDetails);
                    roomRateCategoryResults.AboutOurRates = GetAboutOurRatesString();
                    roomRateCategoryResults.RoomRateDetails = RoomRatesUtility.GetRoomTypeDetailsList(roomRateDetails);
                    roomRateCategoryResults.RateCategories = roomRateDetails.RateCategories;
                }

                SearchAlternateHotelForSpecialRates(hotelSearch, roomRateDetails, ref roomRateCategoryResults);

                MiscellaneousSessionWrapper.HotelRoomRateResults = roomRateCategoryResults;
            }

            if (!string.IsNullOrEmpty(currentCampaignCode))
            {
                SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = currentCampaignCode;
            }
            return roomRateCategoryResults;
        }

        private string GetAboutOurRatesString()
        {
            string aboutOurRate = string.Empty;
            string[] aboutOurRateDesc = CMSDataManager.GetAboutOurRateDesciption();

            if (aboutOurRateDesc != null && aboutOurRateDesc.Length > 0)
            {
                aboutOurRate = aboutOurRateDesc[1];

                Reservation2SessionWrapper.AboutOurRateHeading = aboutOurRateDesc[0];
                Reservation2SessionWrapper.AboutOurRateDescription = aboutOurRateDesc[1];
            }
            return aboutOurRate;
        }

        /// <summary>
        /// This method prepare a dictionary for RoomCategory list for Popups
        /// </summary>
        /// <param name="roomRateDetails"></param>
        private List<RateCategoryPopup> PrepareRateCategoryListForPopups(BaseRoomRateDetails roomRateDetails)
        {
            List<RateCategoryPopup> rateCategoryPopups = new List<RateCategoryPopup>();

            if (roomRateDetails != null && roomRateDetails.RateCategories != null &&
                roomRateDetails.RateCategories.Count > 0)
            {
                BaseRateDisplay rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                List<RateCategoryHeaderDisplay> rateCategoriesList = rateDisplay.RateCategoriesDisplay;

                foreach (RateCategoryHeaderDisplay rateCategory in rateCategoriesList)
                {
                    RateCategoryPopup ratePopup = new RateCategoryPopup();
                    ratePopup.RateCategoryId = rateCategory.Id;
                    ratePopup.RateCategoryName = rateCategory.Title;
                    ratePopup.RateCategoryDesc = rateCategory.Description;

                    if (!rateCategoryPopups.Contains(ratePopup))
                    {
                        rateCategoryPopups.Add(ratePopup);
                    }
                }
            }

            return rateCategoryPopups;
        }

        private void SearchAlternateHotelForSpecialRates(HotelSearchEntity hotelSearch,
                                                         BaseRoomRateDetails roomRateDetails,
                                                         ref RoomRateResults roomRateResults)
        {
            switch (hotelSearch.SearchingType)
            {
                case SearchType.CORPORATE:
                    if (!Utility.IsBlockCodeBooking)
                    {
                        NegotiatedRoomRateDetails negotiatedRoomRate = roomRateDetails as NegotiatedRoomRateDetails;
                        if ((null != negotiatedRoomRate) && !(negotiatedRoomRate.HasNegotiatedRates))
                        {
                            string errMsg = string.Empty;
                            if (hotelSearch.CampaignCode != null)
                                errMsg = string.Format(WebUtil.GetTranslatedText(Reference.NO_ROOMRATE_WITH_DNUMBER_XPATH), hotelSearch.CampaignCode);
                            else
                                errMsg = string.Format(WebUtil.GetTranslatedText(Reference.NO_ROOMRATE_WITH_DNUMBER_XPATH), string.Empty);
                            MessageDetails messageDetails = GetMessageDetails(Reference.NO_ROOMRATE_WITH_DNUMBER, errMsg);
                            roomRateResults.ErrorMessages = new List<MessageDetails>();
                            roomRateResults.ErrorMessages.Add(messageDetails);
                        }
                    }
                    break;
                case SearchType.REGULAR:
                    if (hotelSearch.CampaignCode != null)
                    {
                        PromotionRoomRateDetails promoRoomRate = roomRateDetails as PromotionRoomRateDetails;
                        if (!promoRoomRate.HasPromtionRates)
                        {
                            roomRateResults.AlternateHotelSearchRequired = true;
                            HotelDestination hotelDestination = roomRateDetails.HotelDestination;
                            InitiateAlternateHotelSearch(hotelDestination);
                        }
                    }
                    break;
            }
        }


        /// <summary>
        /// This method searches alternate hotel and logs the errors for ows exceptions.
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="owsException"></param>
        /// <returns></returns>
        private void SearchAlternateHotelsIfOWSExceptions(HotelSearchEntity hotelSearch, OWSException owsException,
                                                          ref RoomRateResults roomRateResults)
        {
            List<MessageDetails> messageDetailList = null;
            switch (owsException.ErrCode)
            {
                case OWSExceptionConstants.PROMOTION_CODE_INVALID:
                    if ((hotelSearch != null) && (hotelSearch.SearchingType == SearchType.REGULAR))
                    {
                        string Promo_Code = string.Empty;
                        try
                        {
                            if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                            {
                                Promo_Code = SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode;
                                SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = null;
                            }
                            roomRateResults = GetRatesDetailList(true);
                        }
                        catch (OWSException owsExpn)
                        {
                            switch (owsExpn.ErrCode)
                            {
                                case OWSExceptionConstants.PROPERTY_NOT_AVAILABLE:
                                case OWSExceptionConstants.PRIOR_STAY:
                                    roomRateResults.AlternateHotelSearchRequired = true;
                                    if (!AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                                    {
                                        hotelSearch.CampaignCode = null;
                                        if (!InitiateAlternateHotelSearch(hotelSearch))
                                        {
                                            messageDetailList = new List<MessageDetails>();
                                            messageDetailList.Add(GetMessageDetails(Reference.NO_HOTELS_FOUND,
                                                           WebUtil.GetTranslatedText(AppConstants.NO_HOTELS_FOUND)));
                                        }
                                    }
                                    break;
                                default:
                                    if (owsException.ErrMessage != null &&
                                        owsException.ErrMessage.Contains(Reference.NO_RATECODES_AVAILABLE))
                                    {
                                        roomRateResults.AlternateHotelSearchRequired = true;
                                        if (!InitiateAlternateHotelSearch(hotelSearch))
                                        {
                                            messageDetailList = new List<MessageDetails>();
                                            messageDetailList.Add(GetMessageDetails(OWSExceptionConstants.PROMOTION_CODE_INVALID,
                                                                                    WebUtil.GetTranslatedText(owsException.TranslatePath)));
                                        }
                                    }
                                    break;
                            }
                        }
                        messageDetailList = new List<MessageDetails>();
                        messageDetailList.Add(GetMessageDetails(OWSExceptionConstants.PROMOTION_CODE_INVALID,
                                                                string.Format(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/noroomratewithdnumber"),
                                                                Promo_Code)));
                    }
                    break;
                case OWSExceptionConstants.PROPERTY_NOT_AVAILABLE:
                case OWSExceptionConstants.INVALID_PROPERTY:
                case OWSExceptionConstants.PROPERTY_RESTRICTED:
                case OWSExceptionConstants.PRIOR_STAY:
                    {
                        roomRateResults.AlternateHotelSearchRequired = true;
                        if (!AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone)
                        {
                            if (!InitiateAlternateHotelSearch(hotelSearch))
                            {
                                messageDetailList = new List<MessageDetails>();
                                messageDetailList.Add(GetMessageDetails(Reference.NO_HOTELS_FOUND,
                                               WebUtil.GetTranslatedText(AppConstants.NO_HOTELS_FOUND)));
                            }
                        }
                    }
                    break;
                default:
                    if (owsException.ErrMessage != null &&
                        owsException.ErrMessage.Contains(Reference.NO_RATECODES_AVAILABLE))
                    {
                        roomRateResults.AlternateHotelSearchRequired = true;
                        if (!InitiateAlternateHotelSearch(hotelSearch))
                        {
                            messageDetailList = new List<MessageDetails>();
                            messageDetailList.Add(GetMessageDetails(OWSExceptionConstants.PROMOTION_CODE_INVALID,
                                                                    WebUtil.GetTranslatedText(owsException.TranslatePath)));
                        }
                    }
                    break;
            }

            roomRateResults.ErrorMessages = messageDetailList;
        }

        /// <summary>
        /// This method creates the availability calender for the selected search date.
        /// </summary>
        /// <param name="hotelSearch"></param>
        private AvailabilityCalendarEntity CreateAvailabilityCalender(HotelSearchEntity hotelSearch)
        {
            AvailabilityCalendarEntity availabilityCalendar = null;

            if (hotelSearch != null)
            {
                availabilityCalendar = Reservation2SessionWrapper.AvailabilityCalendar;
                if (availabilityCalendar == null)
                {
                    availabilityCalendar = new AvailabilityCalendarEntity();
                    availabilityCalendar.LeftCarouselIndex = 0;
                    availabilityCalendar.RightCarouselIndex = 0;
                    Reservation2SessionWrapper.AvailabilityCalendar = availabilityCalendar;
                    Reservation2SessionWrapper.LeftVisibleCarouselIndex = 0;
                }

                availabilityCalendar.SearchState = SearchState.NONE;
                availabilityCalendar.ListAvailCalendarItems = new List<AvailCalendarItemEntity>();

                AvailCalendarItemEntity calendarItem = new AvailCalendarItemEntity();
                calendarItem.ArrivalDate = hotelSearch.ArrivalDate;
                calendarItem.DepartureDate = hotelSearch.DepartureDate;
                availabilityCalendar.ListAvailCalendarItems.Add(calendarItem);
            }

            return availabilityCalendar;
        }

        public void InitiateHotelSearch(HotelSearchEntity hotelSearchEntity)
        {
            SearchCriteriaSessionWrapper.SearchCriteria = hotelSearchEntity;
            //AvailabilityCalendarEntity availabilityCalendar = Reservation2SessionWrapper.AvailabilityCalendar;
            //if (availabilityCalendar == null)
            //{
            //    availabilityCalendar = CreateAvailabilityCalender(hotelSearchEntity);
            //}

            //AvailabilityController availController = new AvailabilityController();
            //availController.MultiroomMultidateCalendarAvailSearch(hotelSearchEntity, availabilityCalendar);
        }

        public RateOverlayToolTip GetRateTypeToolTip(string rateCateogryId, bool isBlockCode)
        {
            RateOverlayToolTip rateOverlayToolTip = null;
            RateCategoryPopup rateTypeToolTip = default(RateCategoryPopup);
            string aboutOurRateDesc = string.Empty;
            var roomRateCategoryResults = MiscellaneousSessionWrapper.HotelRoomRateResults as RoomRateResults;

            if (roomRateCategoryResults != null)
            {
                rateTypeToolTip = roomRateCategoryResults.RateCategoryPopups.Where(rate => rate.RateCategoryId == rateCateogryId).SingleOrDefault();

                if (rateTypeToolTip != null)
                {
                    rateOverlayToolTip = new RateOverlayToolTip();
                    rateOverlayToolTip.RateCategoryDesc = rateTypeToolTip.RateCategoryDesc;
                    rateOverlayToolTip.RateCategoryName = rateTypeToolTip.RateCategoryName;
                }

            }

            if (rateOverlayToolTip == null)
            {
                rateOverlayToolTip = GetRateTypeTooltipFromCMS(rateCateogryId, isBlockCode);
            }

            //return string.Format("{0}<br>{1}", aboutOurRateDesc, rateTypeToolTip);
            return rateOverlayToolTip;
        }

        public RateOverlayToolTip GetRateTypeTooltipFromCMS(string rateCategoryId, bool isBlockCode)
        {
            //Session is used here to store the block values and rate category as RateToolTip Overlay
            //is accessed through Web\Page Method, which doesnot have CMS language context. Due to this
            //these values should be called when page is loaded so that they are added in session in right
            //language context and reterived later on.
            RateOverlayToolTip rateOverlayToolTip = new RateOverlayToolTip();
            var dataKey = string.Format("{0}_{1}", LanguageRedirectionHelper.GetCurrentLanguage(), rateCategoryId);
            var sessionStore = StoreManager.Instance.GetStore(StoreType.Session);
            if (isBlockCode)
            {
                var block = sessionStore.GetData<Block>(dataKey);
                block = block ?? ContentDataAccess.GetBlockCodePages(rateCategoryId);
                if (block != null)
                {
                    rateOverlayToolTip.RateCategoryDesc = block.BlockDescription;
                    rateOverlayToolTip.RateCategoryName = block.BlockName;
                    sessionStore.SaveData<Block>(dataKey, block);
                }
            }
            else
            {
                var rateCategory = sessionStore.GetData<RateCategory>(dataKey);
                rateCategory = rateCategory ?? RoomRateUtil.GetRateCategoryByRatePlanCode(rateCategoryId);
                if (rateCategory != null)
                {
                    rateOverlayToolTip.RateCategoryDesc = rateCategory.RateCategoryDescription;
                    rateOverlayToolTip.RateCategoryName = rateCategory.RateCategoryName;
                    sessionStore.SaveData<RateCategory>(dataKey, rateCategory);
                }
            }

            return rateOverlayToolTip;
        }

        public RoomTypeResults GetAvailableRates(string hotelId, Boolean isHotelSelctedByUser, bool selectedHotelHasOnlyPublicRates)
        {
            HotelDestination hotelDestination = null;
            List<ErrorDetails> errorDetails = null;
            List<MessageDetails> messageDetails = null;
            RoomRateResults roomRateCategoryResults = new RoomRateResults();
            List<RoomTypeDetails> roomTypeDetailsList = null;
            RoomTypeResults roomRateResults = null;
            HotelSearchEntity hotelSearch = null;
            try
            {
                hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;

                if (hotelSearch != null && hotelSearch.SelectedHotelCode != hotelId)
                {
                    ClearPreviousRateResults();
                }

                if (hotelSearch != null)
                {
                    hotelSearch.SelectedHotelCode = hotelId;
                }
                roomRateCategoryResults = MiscellaneousSessionWrapper.HotelRoomRateResults as RoomRateResults;

                if (roomRateCategoryResults != null)
                {
                    roomTypeDetailsList = roomRateCategoryResults.RoomRateDetails;
                    messageDetails = roomRateCategoryResults.ErrorMessages;
                }
                else
                {
                    roomRateCategoryResults = new RoomRateResults();
                    errorDetails = new List<ErrorDetails>();
                    messageDetails = new List<MessageDetails>();

                    hotelDestination = GetHotelDestination(hotelSearch.SelectedHotelCode);
                    if (hotelDestination != null && hotelDestination.IsAvailableInBooking)
                    {
                        roomRateCategoryResults = GetRatesDetailList(selectedHotelHasOnlyPublicRates);
                        roomTypeDetailsList = roomRateCategoryResults != null
                                                  ? roomRateCategoryResults.RoomRateDetails
                                                  : null;
                        messageDetails = roomRateCategoryResults != null ? roomRateCategoryResults.ErrorMessages : null;
                    }
                    else
                    {
                        messageDetails.Add(GetMessageDetails(Reference.NO_ROOMRATES,
                                                             WebUtil.GetTranslatedText(Reference.NO_ROOMRATES_XPATH)));
                    }
                }
            }
            catch (OWSException owsException)
            {
                AppLogger.LogInfoMessage(string.Format("{0}: {1} \n OWS Error Message: {2} \n OWS Stack trace: {3}",
                                                       Reference.APPLICATION_ID, AppConstants.OWS_EXCEPTION,
                                                       owsException.Message, owsException.StackTrace));
                SearchAlternateHotelsIfOWSExceptions(hotelSearch, owsException, ref roomRateCategoryResults);
                messageDetails = roomRateCategoryResults.ErrorMessages;
            }
            catch (ContentDataAccessException cdaException)
            {
                if (string.Equals(cdaException.ErrorCode, AppConstants.BONUS_CHEQUE_NOT_FOUND,
                                  StringComparison.InvariantCultureIgnoreCase))
                {
                    messageDetails = new List<MessageDetails>
                        {
                            GetMessageDetails(Reference.BONUS_CHEQUE_NOT_FOUND,
                                              WebUtil.GetTranslatedText(Reference.BONUS_CHEQUE_NOT_FOUND_XPATH))
                        };
                }
                else
                    throw;
            }
            catch (Exception exception)
            {
                AppLogger.LogInfoMessage(string.Format("{0}: Exception occured while fetching rates {1} :",
                                                       Reference.APPLICATION_ID, exception.Message));
                errorDetails.Add(new ErrorDetails() { ErrorCode = Reference.NO_ROOMRATES, ErrorMessaage = exception.Message });
            }
            finally
            {
                if (messageDetails == null)
                {
                    messageDetails = new List<MessageDetails>();
                }
                messageDetails.AddRange(SetErrorMessgesForHotelFlow(hotelSearch, roomRateCategoryResults));
                roomRateResults = ReturnRoomRateDetails(roomRateCategoryResults, messageDetails, errorDetails,
                                                        roomTypeDetailsList, isHotelSelctedByUser);
            }

            return roomRateResults;
        }

        private RoomTypeResults ReturnRoomRateDetails(RoomRateResults roomRateCategoryResults,
                                                      List<MessageDetails> messageDetails,
                                                      List<ErrorDetails> errorDetails,
                                                      List<RoomTypeDetails> roomTypeDetailsList,
                                                      bool isHotelSelctedByUser)
        {
            RoomTypeResults roomRateResults = new RoomTypeResults();
            int hotelCount = 0;
            List<IHotelDetails> hotelDetails =
                HotelResultsSessionWrapper.GetHotelResults(HttpContext.Current.Session) as List<IHotelDetails>;

            if (hotelDetails != null)
            {
                hotelCount = hotelDetails.Count;
            }
            if (roomRateCategoryResults != null && roomRateCategoryResults.AlternateHotelSearchRequired && hotelCount >= 1 && !isHotelSelctedByUser)
            {
                roomRateResults.IsAlternateSearch = true;
                roomRateResults.RoomTypes = null;
                roomRateResults.Messages = messageDetails;
                roomRateResults.Errors = errorDetails;
                AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone = true;
            }
            else
            {
                roomRateResults.RoomTypes = roomTypeDetailsList;
                roomRateResults.Messages = messageDetails;
                roomRateResults.Errors = errorDetails;
                roomRateResults.IsAlternateSearch = false;
            }


            return roomRateResults;
        }

        private string GetRoomCategoryIndexById(string roomCategoryId)
        {
            string roomCategoryIndex = "0";

            IList<BaseRoomRateDetails> listHotelRoomRate = HotelRoomRateSessionWrapper.ListHotelRoomRate;
            if (listHotelRoomRate == null)
            {
                listHotelRoomRate = GetHotelRateDetails(SearchCriteriaSessionWrapper.SearchCriteria);
                HotelRoomRateSessionWrapper.ListHotelRoomRate = listHotelRoomRate;
            }
            BaseRoomRateDetails roomRateDetails = null;

            if (listHotelRoomRate != null && listHotelRoomRate.Count > 0)
            {
                roomRateDetails = listHotelRoomRate[0];
                BaseRateDisplay rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                roomCategoryIndex = rateDisplay.RateRoomCategories.FindIndex(rc => rc.Id == roomCategoryId).ToString();
            }

            return roomCategoryIndex;
        }

        public IEnumerable ProcessSelectRoomRate(string roomCategoryId, string rateCategoryId)
        {
            List<MessageDetails> messageDetails = new List<MessageDetails>();
            List<ErrorDetails> errorDetails = new List<ErrorDetails>();
            var currentCampaignCode = SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode;
            if (bookingContext != null && bookingContext.SelectHotelPage != null &&
                bookingContext.SelectHotelPage.SelectedHotelHasOnlyPublicRates)
            {
                SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = null;
            }
            try
            {
                string roomNumber = "0";

                roomCategoryId = GetRoomCategoryIndexById(roomCategoryId);
                SelectedRoomAndRateEntity selectedRoomRate = SelectRoomUtil.SetRoomAndRateSelection(roomCategoryId,
                                                                                                    rateCategoryId,
                                                                                                    roomNumber);
                string errorMsg = string.Empty;
                int selectedRoomIndex = -1;
                int.TryParse(roomNumber, out selectedRoomIndex);
                if (SearchCriteriaSessionWrapper.SearchCriteria != null &&
                    SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REDEMPTION)
                {
                    if (SearchCriteriaSessionWrapper.SearchCriteria != null && SearchCriteriaSessionWrapper.SearchCriteria.ListRooms != null
                        && SearchCriteriaSessionWrapper.SearchCriteria.ListRooms.Count >= selectedRoomIndex)
                    {
                        SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[selectedRoomIndex].IsBlocked = true;
                    }
                }
                else
                {
                    ConfigHelper configHelper = new ConfigHelper();
                    var sourceCode = configHelper.GetSourceCode();
                    errorMsg = SelectRoomUtil.BlockRoom(selectedRoomRate, roomNumber, true, sourceCode);
                    if (string.IsNullOrEmpty(errorMsg))
                    {
                        WebUtil.IsUblockRequired(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode);
                    }

                    messageDetails.Add(GetMessageDetails("BlockRoomError", errorMsg));
                }
            }
            catch (Exception exp)
            {
                if (!string.IsNullOrEmpty(currentCampaignCode))
                {
                    SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = currentCampaignCode;
                }
                errorDetails.Add(new ErrorDetails() { ErrorCode = "BlockRoomError", ErrorMessaage = exp.Message });
                return errorDetails;
            }

            if (!string.IsNullOrEmpty(currentCampaignCode))
            {
                SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = currentCampaignCode;
            }
            return messageDetails;
        }

        #region Booking Details

        public IEnumerable MakeHotelReservation(BookingModel.BookingDetailModel bookingDetailEntity, PaymentInfo paymentInfo,
                           bool isRedirectToConfirmationPage, bool isModifyBookingAfterNetsPaymentSuccessful)
        {
            BookingDetailsUtility bookingDetailsUtility = new BookingDetailsUtility();
            IEnumerable results = bookingDetailsUtility.MakeBookingConfirmation(bookingDetailEntity, paymentInfo, isRedirectToConfirmationPage,
                                  isModifyBookingAfterNetsPaymentSuccessful);
            RefreshRewardPoints(results);

            return results;
        }

        /// <summary>
        /// This method refreshes the reward points after successful reservation.
        /// </summary>
        /// <param name="results"></param>
        private void RefreshRewardPoints(IEnumerable results)
        {
            HotelSearchEntity searchEntity = SearchCriteriaSessionWrapper.SearchCriteria;
            if (searchEntity != null && searchEntity.SearchingType == SearchType.REDEMPTION)
            {
                var bookingConfirmation = results as Dictionary<string, string>;
                if (bookingConfirmation != null && bookingConfirmation.ContainsKey(Reference.RESERVATION_NUMBER))
                {
                    userInfoRepository.RefreshLoyaltyDetails();
                }
            }
        }

        /// <summary>
        /// Gets total price
        /// </summary>
        /// <param name="roomNumber"></param>
        /// <param name="isCurrencySymbolRequired"> </param>
        /// <returns></returns>
        public string GetTotalPrice(int roomNumber, bool isCurrencySymbolRequired)
        {
            BaseRateDisplay firstRateDisplayObject = null;
            string currencyCode = string.Empty;
            double totalRate = 0;
            string firstRoomRatePlanCode = string.Empty;

            var selectedRoomRateEntity =
                HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[roomNumber] as SelectedRoomAndRateEntity;

            if (selectedRoomRateEntity != null)
            {
                List<RoomRateEntity> selectedRoomRates = selectedRoomRateEntity.RoomRates;
                if (selectedRoomRates != null && selectedRoomRates.Count > 0)
                {
                    RoomRateEntity roomRate = selectedRoomRates[0];

                    if (roomRate != null)
                    {
                        HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;

                        switch (search.SearchingType)
                        {
                            case SearchType.REDEMPTION:
                                totalRate = roomRate.PointsDetails.PointsRequired;
                                currencyCode = string.Empty;
                                break;
                            case SearchType.BONUSCHEQUE:
                                totalRate = roomRate.TotalRate.Rate;
                                currencyCode = roomRate.TotalRate.CurrencyCode;
                                break;
                            default:
                                totalRate = roomRate.TotalRate.Rate;
                                currencyCode = roomRate.TotalRate.CurrencyCode;
                                break;
                        }
                    }
                }
            }

            if (HotelRoomRateSessionWrapper.ListHotelRoomRate == null)
            {
                HotelRoomRateSessionWrapper.ListHotelRoomRate = GetHotelRateDetails(SearchCriteriaSessionWrapper.SearchCriteria);
            }
            firstRateDisplayObject = Utility.GetRateCellDisplayObject(roomNumber, selectedRoomRateEntity);
            string totalRateValue = totalRate.ToString();
            if (isCurrencySymbolRequired)
            {
                totalRateValue = firstRateDisplayObject.GetTotalRateDisplayStringForShoppingCart(true, totalRate,
                                                                                                 currencyCode);
            }
            return totalRateValue;
        }



        public int GetNoOfChildrenToBeAccommodated(List<BookingModel.ChildrenInfo> childrensDetailsEntity)
        {
            int noOfChildrenToBeAccommodated = 0;
            if (childrensDetailsEntity != null && childrensDetailsEntity.Count > 0)
            {
                foreach (BookingModel.ChildrenInfo child in childrensDetailsEntity)
                {
                    if (child != null && (child.BedType.Equals(WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/crib"), StringComparison.InvariantCultureIgnoreCase) ||
                             child.BedType.Equals(WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/extrabed"), StringComparison.InvariantCultureIgnoreCase)))
                    {
                        noOfChildrenToBeAccommodated++;
                    }
                }
            }
            return noOfChildrenToBeAccommodated;
        }


        private void CreateChildrenDetails(List<HotelSearchRoomEntity> listHotelSearchRoomEntity, string[] bedTypes, List<ChildrensDetailsEntity> childrenDetailsallRooms,
                                    int? numberOfChildren, int? numberOfAdults, List<BookingModelMobile.ChildrenInfo> childrenInformation, ref string childDetailText)
        {
            HotelSearchRoomEntity hotelSearchRoomEntity = new HotelSearchRoomEntity();
            int childrenCount = numberOfChildren.Value;
            int adultsCount = numberOfAdults.Value;
            hotelSearchRoomEntity.AdultsPerRoom = adultsCount;
            hotelSearchRoomEntity.ChildrenPerRoom = childrenCount;
            List<ChildEntity> listChildren = CreateChildEntityForRooms(childrenInformation, bedTypes);
            if ((listChildren != null) && (listChildren.Count > 0))
            {
                ChildrensDetailsEntity childrensDetailsEntity = new ChildrensDetailsEntity();
                childrensDetailsEntity.SetChildrensEntity((uint)childrenCount, listChildren);
                childDetailText = childrensDetailsEntity.GetChildrensAccommodationInString(bedTypes[2], bedTypes[0], bedTypes[1]);
                hotelSearchRoomEntity.ChildrenDetails = childrensDetailsEntity;
                childrenDetailsallRooms.Add(childrensDetailsEntity);
                hotelSearchRoomEntity.ChildrenOccupancyPerRoom = GetNoOfChildrenToBeAccommodated(childrenInformation);

            }
            listHotelSearchRoomEntity.Add(hotelSearchRoomEntity);
        }
        private List<ChildEntity> CreateChildEntityForRooms(List<BookingModelMobile.ChildrenInfo> childrenInformation, string[] bedTypes)
        {
            List<ChildEntity> listChildren = new List<ChildEntity>();
            ChildEntity childEntity = null;
            if (childrenInformation != null)
            {
                for (int count = 0; count < childrenInformation.Count; count++)
                {

                    childEntity = CreateChildEntity(childrenInformation[count].Age.Value, childrenInformation[count].BedType,
                                                            bedTypes[0], bedTypes[1], bedTypes[2]);
                    listChildren.Add(childEntity);

                }
            }
            return listChildren;
        }

        /// <summary>
        /// Create child enity object for each room
        /// </summary>
        /// <param name="childAgeforRoom1Child1"></param>
        /// <param name="bedTypeforRoom1Child1"></param>
        /// <param name="cot"></param>
        /// <param name="extraBed"></param>
        /// <param name="sharingBed"></param>
        /// <returns></returns>
        private ChildEntity CreateChildEntity(int childAge, string bedType, string cot, string extraBed,
                                              string sharingBed)
        {
            ChildEntity childEntity = new ChildEntity();
            childEntity.Age = childAge;
            if (string.Compare(bedType, cot, false) == 0)
            {
                childEntity.ChildAccommodationType = ChildAccommodationType.CRIB;
                childEntity.AccommodationString = cot;
            }
            else if (string.Compare(bedType, sharingBed, false) == 0)
            {
                childEntity.ChildAccommodationType = ChildAccommodationType.CIPB;
                childEntity.AccommodationString = sharingBed;
            }
            else if (string.Compare(bedType, extraBed, false) == 0)
            {
                childEntity.ChildAccommodationType = ChildAccommodationType.XBED;
                childEntity.AccommodationString = extraBed;
            }

            return childEntity;
        }

        #endregion

        #endregion
    }
}