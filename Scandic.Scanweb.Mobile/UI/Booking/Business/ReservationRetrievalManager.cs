﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Entity;
using System.Collections.Specialized;
using Scandic.Scanweb.Mobile.UI.Common;
using System.Globalization;
using Scandic.Scanweb.ExceptionManager;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Mobile.UI.Common.Interface;
using Scandic.Scanweb.Mobile.UI.Entity;
using BookingEntity = Scandic.Scanweb.Entity;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Booking.Business
{
    public class ReservationRetrievalManager
    {
        ISiteInfoRepository siteInfoRepository;
        private const string DATE_FORMATE = "dd MMM yyyy";
        private int roomPerNight_BC;

        public ReservationRetrievalManager()
        {
            siteInfoRepository = DependencyResolver.Instance.GetService(typeof(ISiteInfoRepository)) as ISiteInfoRepository;
        }

        public ReservationDetails GetReservationDetails(string reservationNumber)
        {

            ReservationDetails reservationDetails = null;
            var reservationController = new ReservationController();
            var sessionStore = StoreManager.Instance.GetStore(StoreType.Session);
            if (!string.IsNullOrEmpty(reservationNumber))
            {
                try
                {
                    List<BookingDetailsEntity> bookingEntities = null;
                    if (reservationNumber.Contains(AppConstants.HYPHEN))
                    {
                        var splittedReservationNumber = reservationNumber.Split(char.Parse(AppConstants.HYPHEN));
                        string legNumber = splittedReservationNumber[1].Trim();
                        reservationNumber = splittedReservationNumber[0].Trim();
                        var bookingEntity = reservationController.FetchBooking(reservationNumber, legNumber);

                        if (bookingEntity != null)
                        {
                            bookingEntities = new List<BookingDetailsEntity>();
                            bookingEntities.Add(bookingEntity);
                            //This session variable is set here to use it in sending email and 
                            //SMS using Scanweb functionality.
                            BookingEngineSessionWrapper.IsModifyingLegBooking = true;
                        }
                    }
                    else
                    {
                        bookingEntities = reservationController.FetchBookingSummary(reservationNumber);
                        //This session variable is set here to use it in sending email and 
                        //SMS using Scanweb functionality.
                        BookingEngineSessionWrapper.IsModifyingLegBooking = false;
                    }
                    reservationDetails = ConvertToReservationDetails(bookingEntities);
                    //Fetched booking details are addded in the session to use it to populate the session objects
                    //required in Scanweb for sending email and sms for modification and cancellation.
                    sessionStore.SaveData<List<BookingDetailsEntity>>(Reference.MODIFY_BOOKING_DETAILS_SESSION_KEY, bookingEntities);
                }
                catch (OWSException owsException)
                {
                    AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION,
                                    string.Format("No booking found for reservation number - {0}. ", reservationNumber));
                    switch (owsException.ErrCode)
                    {
                        case OWSExceptionConstants.BOOKING_NOT_FOUND:
                        case OWSExceptionConstants.SEE_CONTACT_DETAILS_ON_CHAIN_INFO:
                            {
                                reservationDetails = new ReservationDetails();
                                reservationDetails.Error = new ErrorDetails();
                                reservationDetails.Error.ErrorCode = Reference.NON_WEB_BOOKING;
                                reservationDetails.Error.ErrorMessaage = WebUtil.GetTranslatedText(TranslatedTextConstansts.NOT_ONLINE_BOOKING);
                                break;
                            }
                        default:
                            {
                                //else throw exception
                                throw owsException;
                            }
                    }

                }

            }
            return reservationDetails;
        }

        public UserReservations GetUserBookings(string nameId)
        {
            UserReservations userReservations = null;
            CurrentBookingSummary currentBookingSummary = null;
            ReservationController reservationController = new ReservationController();
            try
            {
                currentBookingSummary = reservationController.GetCurrentBookingSummary(nameId);
            }
            catch (OWSException owsException)
            {
                switch (owsException.ErrCode)
                {
                    case OWSExceptionConstants.BOOKING_NOT_FOUND:
                    case OWSExceptionConstants.BOOKING_NOT_FOUND_IGNORE:
                        currentBookingSummary = null;
                        break;
                    default:
                        throw new ApplicationException(
                                    string.Format("ReservationRetrievalManager: OWS error in GetUserBooking with nameId={0}", nameId),
                                                    owsException);

                }
            }
            if (currentBookingSummary != null)
            {
                currentBookingSummary = RemoveDuplicateBooking(currentBookingSummary);
                userReservations = ConvertToUserReservations(currentBookingSummary);
            }
            return userReservations;
        }

        public UserTransactions GetUserTransactions(string nameId)
        {
            var userTransactions = new UserTransactions();
            if (!string.IsNullOrEmpty(nameId))
            {
                var membershipOperaID = Utility.GetMembershipOperaId(nameId);
                var nameController = new NameController();
                if (!string.IsNullOrEmpty(membershipOperaID))
                {
                    var accountInfo = nameController.GetAccountInfo(membershipOperaID);
                    userTransactions = CovertToUserTransactions(accountInfo);
                }
            }

            if (userTransactions.Transactions == null)
            {
                userTransactions.Errors = new List<ErrorDetails>();
                userTransactions.Errors.Add(new ErrorDetails
                {
                    ErrorMessaage = WebUtil.GetTranslatedText("/bookingengine/booking/accountinfo/notransactionsAvail")
                });
            }

            return userTransactions;
        }

        private UserTransactions CovertToUserTransactions(LoyaltyAccountInformation accountInfo)
        {
            var userTransactions = new UserTransactions();

            if (accountInfo != null && accountInfo.LoyaltyTransactions != null && accountInfo.LoyaltyTransactions.Count > 0)
            {
                userTransactions.Transactions = new List<UserTransaction>();
                var culture = siteInfoRepository.GetCurrentCultureInfo();
                foreach (var transaction in accountInfo.LoyaltyTransactions)
                {
                    userTransactions.Transactions.Add(
                                   new UserTransaction
                                   {
                                       CheckInDate = transaction.FromDate,
                                       CheckInDateInString = transaction.FromDate.ToString(DATE_FORMATE, culture),
                                       CheckOutDate = transaction.ToDate,
                                       CheckOutDateInString = transaction.ToDate.ToString(DATE_FORMATE, culture),
                                       Points = string.IsNullOrEmpty(transaction.Points) ? 0 : Convert.ToDouble(transaction.Points),
                                       PointsInString = transaction.Points,
                                       Transaction = !string.IsNullOrEmpty(transaction.TransactionInfo) ? 
                                                                        transaction.TransactionInfo
                                                                        : WebUtil.GetTranslatedText(AppConstants.HOTEL_CLOSED)
                                       
                                   });

                }
            }
            return userTransactions;
        }

        private CurrentBookingSummary RemoveDuplicateBooking(CurrentBookingSummary currencyBookingSummary)
        {
            List<BookingEntity.CurrentBooking> oldCurrentBookings = currencyBookingSummary.CurrentBookingsList;
            List<BookingEntity.CurrentBooking> newCurrentBooking = new List<BookingEntity.CurrentBooking>();

            //Following map will contain all unique booking irrespective of leg number.
            StringCollection confirmationIdMap = new StringCollection();
            int totalBookingsCount = oldCurrentBookings.Count;
            for (int count = 0; count < totalBookingsCount; count++)
            {
                if (!confirmationIdMap.Contains(oldCurrentBookings[count].ConfirmationId))
                {
                    //If confirmation number is not there then add it so that it will be avoided in next time.
                    confirmationIdMap.Add(oldCurrentBookings[count].ConfirmationId);
                    newCurrentBooking.Add(oldCurrentBookings[count]);
                }
            }
            //Assigning back the unique current booking.
            currencyBookingSummary.CurrentBookingsList = newCurrentBooking;
            return currencyBookingSummary;
        }

        private UserReservations ConvertToUserReservations(CurrentBookingSummary currencyBookingSummary)
        {
            UserReservations userReservations = null;

            if (currencyBookingSummary.CurrentBookingsList != null)
            {
                var currentDate = DateTime.ParseExact(DateTime.Now.ToString(Reference.DateFormat), Reference.DateFormat, CultureInfo.InvariantCulture);
                userReservations = new UserReservations();
                var culture = siteInfoRepository.GetCurrentCultureInfo();
                userReservations.FutureBookings = (from booking in currencyBookingSummary.CurrentBookingsList
                                                   where DateTime.Compare(booking.FromDate, currentDate) >= 0
                                                   select new UserReservation
                                                   {
                                                       HotelName = !string.IsNullOrEmpty(booking.HotelName)? 
                                                                        string.Format("{0}, {1}", booking.HotelName, booking.City)
                                                                        : WebUtil.GetTranslatedText(AppConstants.HOTEL_CLOSED),
                                                       CheckInDate = booking.FromDate,
                                                       CheckInDateInString = booking.FromDate.ToString(DATE_FORMATE, culture),
                                                       CheckOutDate = booking.ToDate,
                                                       CheckOutDateInString = booking.ToDate.ToString(DATE_FORMATE, culture),
                                                       ReservationNumber = booking.ConfirmationId,
                                                       IsHotelExist = !string.IsNullOrEmpty(booking.HotelName)
                                                   }
                                                                              ).ToList<UserReservation>();


            }

            return userReservations;
        }


        private ReservationDetails ConvertToReservationDetails(List<BookingDetailsEntity> bookingEntities)
        {
            ReservationDetails reservationDetails = null;

            if (bookingEntities != null && bookingEntities.Count > 0)
            {
                reservationDetails = new ReservationDetails();
                //set hotel details
                reservationDetails.HotelDetails = SetHotelDetails(bookingEntities);
                reservationDetails.ReservationNumber = bookingEntities[0].ReservationNumber;
                reservationDetails.HotelSearch = bookingEntities[0].HotelSearch;
            }
            foreach (var bkng in bookingEntities)
            {
                if (bkng.HotelRoomRate != null && bkng.HotelRoomRate.IsSessionBooking == true && reservationDetails != null)
                {
                    reservationDetails.IsSessionBooking = true;
                    break;
                }
            }
            return reservationDetails;
        }

        private ReservedHotelDetails SetHotelDetails(List<BookingDetailsEntity> bookingEntities)
        {
            int NoOfRoomsForVoucherBooking = 0;
            bool rateDoesNotExistsInCMS = false;
            var reservedHotelDetails = new ReservedHotelDetails();
            double previousSubTotal = 0;
            roomPerNight_BC = 0;
            var totalPrice = string.Empty;
            var availabilityController = new AvailabilityController();
            var hotelDetails = availabilityController.GetHotelDestination(bookingEntities[0].HotelSearch.SelectedHotelCode);

            reservedHotelDetails.Address = hotelDetails.HotelAddress.ToString();
            reservedHotelDetails.HotelName = hotelDetails.Name;
            reservedHotelDetails.HotelCode = bookingEntities[0].HotelSearch.SelectedHotelCode;
            reservedHotelDetails.PhoneNumber = hotelDetails.Telephone;
            reservedHotelDetails.Email = hotelDetails.Email;
            reservedHotelDetails.ArrivalDate = bookingEntities[0].HotelSearch.ArrivalDate;
            reservedHotelDetails.DepartureDate = bookingEntities[0].HotelSearch.DepartureDate;
            reservedHotelDetails.HotelSearchType = bookingEntities[0].HotelSearch.SearchingType;
            reservedHotelDetails.BookedRooms = new List<ReservedRoomDetails>();
            foreach (var booking in bookingEntities)
            {
                
                var rate = RoomRateUtil.GetRate(booking.HotelRoomRate.RatePlanCode);
                if (rate == null && !rateDoesNotExistsInCMS && !string.Equals(booking.HotelSearch.QualifyingType, AppConstants.BLOCK_CODE_QUALIFYING_TYPE, StringComparison.InvariantCultureIgnoreCase))
                    rateDoesNotExistsInCMS = true;

                if (!booking.IsCancelledByUser)
                {
                    NoOfRoomsForVoucherBooking = NoOfRoomsForVoucherBooking + 1;
                    var roomDetails = new ReservedRoomDetails();
                    var roomCategory = RoomRateUtil.GetRoomCategory(booking.HotelRoomRate.RoomtypeCode);
                    var rateCategoryName = string.Empty;
                    var isCancellable = false;

                    roomDetails.ReservationNumber = string.Format("{0}-{1}", booking.ReservationNumber, booking.LegNumber);
                    if (roomCategory != null)
                    {
                        roomDetails.RoomType = roomCategory.RoomCategoryName;
                    }
                    roomDetails.PriceFirstNight = GetPricePerNightAndRateCategory(booking, out rateCategoryName, out isCancellable);
                    previousSubTotal = GetTotalPrice(booking, previousSubTotal, out totalPrice, NoOfRoomsForVoucherBooking);
                    roomDetails.RateType = rateCategoryName;
                    roomDetails.IsBlockCode = IsBlockCodeBooking(booking.HotelSearch);
                    roomDetails.RateTypeId = roomDetails.IsBlockCode ? booking.HotelSearch.CampaignCode : booking.HotelRoomRate.RatePlanCode;
                    roomDetails.IsCancellable = isCancellable;
                    roomDetails.NoOfAdults = booking.HotelSearch.AdultsPerRoom.ToString();
                    roomDetails.GuaranteeCancellationPolicyCode = GetGuranteeCancellationPolicyCode(booking, isCancellable);
                    if (booking.HotelSearch.ChildrenPerRoom > 0)
                    {
                        roomDetails.NoOfChildren = booking.HotelSearch.ChildrenPerRoom.ToString();
                    }
                    roomDetails.HideRateInfo = rateDoesNotExistsInCMS;
                    roomDetails.CustomerDetails = GetCustomerDetails(booking);
                    reservedHotelDetails.BookedRooms.Add(roomDetails);

                    
                }
            }
            reservedHotelDetails.TotalPrice = totalPrice;
            reservedHotelDetails.HideIncludingAllTaxText = IsBlockCodeBooking(bookingEntities[0].HotelSearch) && 
                                                           Utility.GetHideARBPrice(bookingEntities[0].HotelSearch.CampaignCode);
            reservedHotelDetails.HideRateInfo = rateDoesNotExistsInCMS;
            return reservedHotelDetails;
        }

        private string GetGuranteeCancellationPolicyCode(BookingDetailsEntity booking, bool isCancellable)
        {
            string policyCode = string.Empty;
            switch (booking.GuestInformation.GuranteeInformation.GuranteeType)
            {
                case GuranteeType.CREDITCARD:

                    policyCode = isCancellable ? Reference.GuranteedTextCodeAfter6PM : Reference.GuranteedTextCodeNoCancellation;
                    break;
                case GuranteeType.HOLD:
                    policyCode = Reference.GuranteedTextCodeBefore6PM;
                    break;
                case GuranteeType.PREPAID:
                    policyCode = Reference.GuranteedTextCodeNoCancellation;
                    break;
                case GuranteeType.REDEMPTION:
                    policyCode = Reference.GuranteedTextCodeRewardNights;
                    break;

            }
            if (booking.HotelSearch.SearchingType == SearchType.REDEMPTION)
            {
                policyCode = Reference.GuranteedTextCodeRewardNights;
            }
            return policyCode;

        }

        private ReservedCustomerDetails GetCustomerDetails(BookingDetailsEntity booking)
        {
            var customerDetails = new ReservedCustomerDetails();

			//Added as part of SCANAM-537
            if (!string.IsNullOrEmpty(booking.GuestInformation.NativeFirstName) && !string.IsNullOrEmpty(booking.GuestInformation.NativeLastName))
            {
                customerDetails.FirstName = booking.GuestInformation.NativeFirstName;
                customerDetails.LastName = booking.GuestInformation.NativeLastName;
                customerDetails.AltLastName = booking.GuestInformation.LastName;
            }
            else
            {
                customerDetails.FirstName = booking.GuestInformation.FirstName;
                customerDetails.LastName = booking.GuestInformation.LastName;
                customerDetails.AltLastName = booking.GuestInformation.NativeLastName;
            }
            customerDetails.Email = booking.GuestInformation.EmailDetails != null ? booking.GuestInformation.EmailDetails.EmailID : string.Empty;
            customerDetails.PhoneNumber = booking.GuestInformation.Mobile != null ? booking.GuestInformation.Mobile.Number : string.Empty;

            return customerDetails;
        }

        private double GetTotalPrice(BookingDetailsEntity booking, double previousSubTotal, out string totalPrice, int NoOfRooms)
        {
            totalPrice = string.Empty;

            switch (booking.HotelSearch.SearchingType)
            {
                case SearchType.BONUSCHEQUE:
                    if (booking.HotelRoomRate != null && booking.HotelRoomRate.TotalRate != null)
                    {
                        var totalPoints = booking.HotelRoomRate.TotalRate.Rate;
                        previousSubTotal = previousSubTotal + booking.HotelRoomRate.TotalRate.Rate;
                        roomPerNight_BC++;
                        RateEntity tempRateEntity =
                                    new RateEntity(previousSubTotal, booking.HotelRoomRate.TotalRate.CurrencyCode);
                        totalPrice =
                            Utility.GetRoomRateString(null, tempRateEntity, totalPoints,
                                                      booking.HotelSearch.SearchingType,
                                                      booking.HotelSearch.ArrivalDate.Year,
                                                      booking.HotelSearch.HotelCountryCode, booking.HotelSearch.NoOfNights,
                                                      roomPerNight_BC);
                        
                    }
                    break;
                case SearchType.REDEMPTION:
                    previousSubTotal = previousSubTotal + booking.HotelRoomRate.Points;
                    totalPrice = WebUtil.GetRedeemString(previousSubTotal);
                    break;
                case SearchType.VOUCHER:
                    var noOfNight = booking.HotelSearch.NoOfNights;
                    var totalVouchers = noOfNight * NoOfRooms;
                    totalPrice = noOfNight > 1 ? WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrates")
                                            : WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrate");
                    totalPrice = string.Format("{0} {1}", Convert.ToString(totalVouchers), totalPrice);
                    break;
                default:
                    if (IsBlockCodeBooking(booking.HotelSearch) && Utility.GetHideARBPrice(booking.HotelSearch.CampaignCode))
                    {
                        totalPrice = Utility.GetPrepaidString();
                    }
                    else
                    {
                        if (booking.HotelRoomRate != null && booking.HotelRoomRate.TotalRate != null)
                            previousSubTotal = previousSubTotal + booking.HotelRoomRate.TotalRate.Rate;

                        totalPrice = string.Format("{0} {1}", previousSubTotal.ToString(), booking.HotelRoomRate.Rate.CurrencyCode);
                    }
                    break;
            }

            return previousSubTotal;
        }





        private string GetPricePerNightAndRateCategory(BookingDetailsEntity booking, out string rateCategory, out bool isCancellable)
        {
            string pricePerNight = string.Empty;
            var rate = RoomRateUtil.GetRate(booking.HotelRoomRate.RatePlanCode);
            
            rateCategory = string.Empty;
            isCancellable = false;
            if (rate != null)
            {
                rateCategory = rate.RateCategoryName;
                //TODO: Check how cancellable functionality works for blockcodes.
                isCancellable = rate.IsCancellable;
            }
            switch (booking.HotelSearch.SearchingType)
            {
                case SearchType.REGULAR:
                    string price = WebUtil.GetPricePerNight(booking.HotelRoomRate.TotalRate.Rate, booking.HotelSearch.NoOfNights);
                    pricePerNight = string.Format("{0} {1}", price, booking.HotelRoomRate.TotalRate.CurrencyCode);
                    break;
                case SearchType.BONUSCHEQUE:
                    pricePerNight = WebUtil.GetBonusChequeRateString("/bookingengine/booking/selectrate/bonuschequerate",
                            booking.HotelSearch.ArrivalDate.Year, booking.HotelSearch.HotelCountryCode,
                            booking.HotelRoomRate.Rate.Rate, booking.HotelRoomRate.Rate.CurrencyCode, AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                    break;
                case SearchType.REDEMPTION:
                    pricePerNight = WebUtil.GetRedeemString(booking.HotelRoomRate.Points);
                    break;
                case SearchType.VOUCHER:
                    rateCategory = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher");
                    pricePerNight = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher");
                    break;
                default:
                    //This is scenario when booking code is provided for corporate and block-code search
                    bool isBlockCodeBookingCancellable;
                    rateCategory = GetRateCategoryForBookingCodeReservation(booking, out isBlockCodeBookingCancellable);
                    if (IsBlockCodeBooking(booking.HotelSearch))
                    {
                        isCancellable = isBlockCodeBookingCancellable;
                        if (Utility.GetHideARBPrice(booking.HotelSearch.CampaignCode))
                        {
                            pricePerNight = Utility.GetPrepaidString();
                        }
                    }
                    if (string.IsNullOrEmpty(pricePerNight))
                    {
                        string perNightRate = WebUtil.GetPricePerNight(booking.HotelRoomRate.TotalRate.Rate, booking.HotelSearch.NoOfNights);
                        pricePerNight = string.Format("{0} {1}", perNightRate, booking.HotelRoomRate.TotalRate.CurrencyCode);
                    }
                    break;

            }
            return pricePerNight;
        }

        private string GetRateCategoryForBookingCodeReservation(BookingDetailsEntity booking, out bool isBlockBookingCancellable)
        {
            string rateCategoryName = string.Empty;
            var rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(booking.HotelRoomRate.RatePlanCode);
            isBlockBookingCancellable = false;

            if (rateCategory != null)
            {
                if (!string.Equals(rateCategory.RateCategoryColor.Trim(), AppConstants.FlexColor, StringComparison.InvariantCultureIgnoreCase) &&
                    !string.Equals(rateCategory.RateCategoryColor.Trim(), AppConstants.EarlyColor, StringComparison.InvariantCultureIgnoreCase))
                {
                    rateCategoryName = booking.GuestInformation.CompanyName;
                }
                else
                {
                    rateCategoryName = rateCategory.RateCategoryName;
                }
            }

            var isBlockCodeBooking = IsBlockCodeBooking(booking.HotelSearch);
            if (isBlockCodeBooking)
            {
                var block = ContentDataAccess.GetBlockCodePages(booking.HotelSearch.CampaignCode);
                if (block != null)
                {
                    rateCategoryName = block.BlockName;
                    isBlockBookingCancellable = (string.Equals(Reference.GuranteedTextCodeAfter6PM, block.GuranteeType) ||
                                                string.Equals(Reference.GuranteedTextCodeBefore6PM, block.GuranteeType) ||
                                                string.Equals(Reference.GuranteedTextCodeBlockCode, block.GuranteeType));

                }
            }
            
            return rateCategoryName;
        }

        private IList<BaseRoomRateDetails> GetBaseRoomRateDetails(HotelSearchEntity hotelSearch)
        {
            IList<BaseRoomRateDetails> listRoomRateDetails = null;
            AvailabilityController availController = new AvailabilityController();

            if (hotelSearch != null)
            {
                listRoomRateDetails = availController.GetSelectRateDetails(hotelSearch);
            }
            return listRoomRateDetails;
        }

        private bool IsBlockCodeBooking(HotelSearchEntity hotelSearch)
        {
            return (hotelSearch != null && string.Equals(hotelSearch.QualifyingType, AppConstants.BLOCK_CODE_QUALIFYING_TYPE));
        }
    }
}
