﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using MobileEntities = Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using MobileCommon = Scandic.Scanweb.Mobile.UI.Common;
using System.Linq;
using System.Text;

namespace Scandic.Scanweb.Mobile.UI.Booking.Business
{
    /// <summary>
    /// BookingDetailsUtility
    /// </summary>
    public class BookingDetailsUtility
    {
        private delegate void CreatePDF(List<EmailEntity> email, List<string> reservations);

        /// <summary>
        /// Event Handler Method for Book Button Click
        /// </summary>
        /// <param name="bookingDetailEntity"> </param>
        /// <param name="paymentInfo"> </param>
        /// <param name="isModifyBookingAfterNetsPaymentSuccessful"> </param>
        /// <param name="isRedirectToConfirmationPage"> </param>
        public IEnumerable MakeBookingConfirmation(MobileEntities.BookingDetailModel bookingDetailEntity, PaymentInfo paymentInfo,
                                                   bool isRedirectToConfirmationPage, bool isModifyBookingAfterNetsPaymentSuccessful)
        {
            try
            {
                if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                {
                    LanguageRedirectionHelper.SetCurrentCultureInfo();
                    bool isSessionBooking = true;
                    bool isRedemptionBooking = false;

                    if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REDEMPTION)
                    {
                        isSessionBooking = false;
                        isRedemptionBooking = true;
                    }
                    List<GuestInformationEntity> guestList = null;
                    if (!isModifyBookingAfterNetsPaymentSuccessful)
                    {
                        SetFetchedGuestInfo();
                        guestList = new List<GuestInformationEntity>();
                        CaptureGuestInformation(guestList, bookingDetailEntity);
                    }
                    else
                    {
                        guestList = MobileCommon.SessionManager.BookingGuestInformation;
                    }
                    GuestInformationEntity userInformation = guestList[0];
                    int totalGuests = guestList.Count;
                    for (int guestCount = 0; guestCount < totalGuests; guestCount++)
                    {
                        HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                        if (((!BookingEngineSessionWrapper.IsModifyBooking) ||
                             ((BookingEngineSessionWrapper.IsModifyBooking) && (!BookingEngineSessionWrapper.DirectlyModifyContactDetails)))
                            && (search.ListRooms != null && search.ListRooms.Count > 0 &&
                                guestCount < search.ListRooms.Count &&
                                search.ListRooms[guestCount].ChildrenDetails != null))
                        {
                            guestList[guestCount].ChildrensDetails = search.ListRooms[guestCount].ChildrenDetails;
                        }
                        else
                        {
                            guestList[guestCount].ChildrensDetails = null;
                        }
                        SetPreferredLanguage(guestList[guestCount]);
                    }
                    if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null)
                    {
                        string membershipID = LoyaltyDetailsSessionWrapper.LoyaltyDetails.MembershipID;
                        if (null != membershipID && string.Empty != membershipID)
                        {
                            for (int guestCount = 0; guestCount < guestList.Count; guestCount++)
                            {
                                guestList[guestCount].MembershipID = long.Parse(membershipID);
                            }
                        }
                    }

                    GuestBookingInformationSessionWrapper.GuestBookingInformation = userInformation;
                    List<HotelRoomRateEntity> hotelRoomRate = new List<HotelRoomRateEntity>();
                    BedTypePreferenceForAllRoomsSessionWrapper.UserBedTypePreferenceForAllRooms = new List<string>();
                    int gCount = guestList.Count;
                    for (int count = 0; count < gCount; count++)
                    {
                        HotelRoomRateEntity temp = GetHotelRoomRate(guestList[count].BedTypePreferenceCode, count,
                                                                    guestList);
                        hotelRoomRate.Add(temp);
                    }
                    if (isSessionBooking && !isRedemptionBooking && !isModifyBookingAfterNetsPaymentSuccessful)
                    {
                        UnBlockOnRateChange();
                    }
                    Reservation2SessionWrapper.IsUserEnrollInBookingModule = false;

                    #region for unlock rooms via ignore call and then creating reservation

                    CreateReservation(guestList, hotelRoomRate, isSessionBooking, bookingDetailEntity.SourceCode, paymentInfo, isModifyBookingAfterNetsPaymentSuccessful);
                    if (isSessionBooking && isRedirectToConfirmationPage)
                    {
                        ConfirmBooking(guestList);
                    }

                    SortedList<string, GuestInformationEntity> sortdListGuestInfoEntity =
                        new SortedList<string, GuestInformationEntity>();
                    int roomIndex = 0;
                    foreach (GuestInformationEntity guest in guestList)
                    {
                        sortdListGuestInfoEntity.Add(guest.LegNumber, guest);

                        SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[roomIndex].RoomBlockReservationNumber =
                            guest.ReservationNumber;
                        SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[roomIndex].RoomBlockReservationLegNumber =
                            guest.LegNumber;
                        SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[roomIndex].IsBlocked = true;
                    }
                    GuestBookingInformationSessionWrapper.AllGuestsBookingInformations = sortdListGuestInfoEntity;

                    #endregion for unlock rooms via ignore call and then creating reservation
                    if (isRedirectToConfirmationPage)
                    {
                        SendConfirmationToGuest(bookingDetailEntity);
                        SendMailConfirmationToGuestsUsingHTMLString();
                    }
                    if (!isModifyBookingAfterNetsPaymentSuccessful && (RoomRateUtil.IsPrepaidGuaranteeType(bookingDetailEntity.ReservationGuaranteeOption)))
                    {
                        MobileCommon.SessionManager.BookingGuestInformation = guestList;

                    }
                }
            }
            catch (OWSException owsException)
            {
                AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);
                return SetErrorMessagesForOWSExceptions(owsException);
            }
            catch (Exception exp)
            {
                AppLogger.LogCustomException(exp, AppConstants.OWS_EXCEPTION);
                List<ErrorDetails> errorDetails = new List<ErrorDetails>();
                errorDetails.Add(new ErrorDetails() { ErrorCode = "FailedToBookHotel", ErrorMessaage = exp.Message });
                return errorDetails;
            }
            return GetReservationNumber();
        }

        /// <summary>
        /// GetReservationNumber
        /// </summary>
        /// <returns>ReservationNumber</returns>
        private Dictionary<string, string> GetReservationNumber()
        {
            Dictionary<string, string> bookingConfirmations = null;

            if (!string.IsNullOrEmpty(ReservationNumberSessionWrapper.ReservationNumber))
            {
                bookingConfirmations = new Dictionary<string, string>();
                bookingConfirmations.Add(Reference.RESERVATION_NUMBER, ReservationNumberSessionWrapper.ReservationNumber);
            }

            return bookingConfirmations;
        }

        /// <summary>
        /// SetErrorMessagesForOWSExceptions
        /// </summary>
        /// <param name="owsException"></param>
        /// <returns>List of MessageDetails</returns>
        private List<MessageDetails> SetErrorMessagesForOWSExceptions(OWSException owsException)
        {
            List<MessageDetails> messageDetails = new List<MessageDetails>();
            string operaErrorMsg = string.Empty;
            switch (owsException.ErrCode)
            {
                case OWSExceptionConstants.RESERVATION_EXISTS_FOR_CREDITCARD:
                    {
                        operaErrorMsg =
                            WebUtil.GetTranslatedText(TranslatedTextConstansts.RESERVATION_EXISTS_FOR_CREDITCARD);
                        break;
                    }
                case OWSExceptionConstants.BOOKING_NOT_FOUND_IGNORE:
                    operaErrorMsg = WebUtil.GetTranslatedText(owsException.TranslatePath);
                    break;
                default:
                    if ((owsException.Message == AppConstants.SYSTEM_ERROR))
                    {
                        operaErrorMsg = WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR);
                    }
                    if (owsException.TranslatePath == AppConstants.SITE_WIDE_ERROR)
                    {
                        if (owsException.ErrMessage.Length > 0)
                        {
                            operaErrorMsg = owsException.ErrMessage;
                        }
                        else
                        {
                            operaErrorMsg = WebUtil.GetTranslatedText(AppConstants.SITE_WIDE_ERROR);
                        }
                    }
                    if (owsException.Message.Equals(Reference.NOT_ENOUGH_POINTS, StringComparison.CurrentCultureIgnoreCase))
                    {
                        operaErrorMsg = WebUtil.GetTranslatedText(Reference.NOT_ENOUGH_POINTS_XPATH);
                    }
                    else if (owsException.Message.Equals(Reference.ROOM_UNAVAILABLE_FOR_REDEMPTION, StringComparison.CurrentCultureIgnoreCase))
                    {
                        operaErrorMsg = WebUtil.GetTranslatedText(Reference.ROOM_UNAVAILABLE_FOR_REDEMPTION_XPATH);
                    }
                    else if (owsException.Message.Equals(OWSExceptionConstants.ROOM_UNAVAILABLE, StringComparison.CurrentCultureIgnoreCase))
                    {
                        operaErrorMsg = WebUtil.GetTranslatedText(Reference.ROOM_UNAVAILABLE_FOR_REDEMPTION_XPATH);
                    }
                    else if (owsException.ErrCode.Equals(OWSExceptionConstants.INVALID_CREDIT_CARD, StringComparison.CurrentCultureIgnoreCase))
                    {
                        operaErrorMsg = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/owserror/invalid_credit_card");
                    }
                    else
                    {
                        operaErrorMsg = WebUtil.GetTranslatedText(owsException.TranslatePath);
                    }
                    break;
            }

            messageDetails.Add(new MessageDetails() { MessageCode = owsException.ErrCode, MessageInfo = operaErrorMsg });
            return messageDetails;
        }

        /// <summary>
        /// Set the SessionWrapper.FetchedGuestInformation entity as this is being used 
        /// for comparision when loggedin user updates the details
        /// </summary>
        private void SetFetchedGuestInfo()
        {
            GuestInformationEntity guestInfoEntity = null;
            string nameID = string.Empty;
            if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null)
            {
                nameID = LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID;
                if (nameID != string.Empty)
                {
                    NameController nameController = new NameController();
                    UserProfileEntity userProfileEntity = nameController.FetchUserDetails(nameID);
                    guestInfoEntity = Utility.ConvertToGuestInformation(userProfileEntity);
                    if (guestInfoEntity != null)
                    {
                        guestInfoEntity.GuestAccountNumber = LoyaltyDetailsSessionWrapper.LoyaltyDetails.UserName;
                        GuestBookingInformationSessionWrapper.FetchedGuestInformation = guestInfoEntity;
                    }
                }
            }
        }

        /// <summary>
        /// CaptureGuestInformation
        /// </summary>
        /// <param name="guestList"></param>
        /// <param name="bookingDetailEntity"></param>
        private void CaptureGuestInformation(List<GuestInformationEntity> guestList,
                                             MobileEntities.BookingDetailModel bookingDetailEntity)
        {
            int roomCount = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count;
            var userProfile = UserProfileInformationSessionWrapper.UserProfileInformation;

            List<string> rateCategoryId = new List<string>();
            SelectedRoomAndRateEntity roomAndRate = null;
            for (int count = 0; count < roomCount; count++)
            {
                roomAndRate = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[count] as SelectedRoomAndRateEntity;
                if ((null != roomAndRate) && (!rateCategoryId.Contains(roomAndRate.RateCategoryID)))
                {
                    rateCategoryId.Add(roomAndRate.RateCategoryID);
                }
            }
            bool isCommonGurantee = false;
            if ((rateCategoryId.Count == 1) && (roomCount > 1))
            {
                isCommonGurantee = true;
            }

            for (int count = 0; count < roomCount; count++)
            {
                GuestInformationEntity guestInfo = new GuestInformationEntity();
                if (bookingDetailEntity != null)
                {
                    guestInfo.FirstName = bookingDetailEntity.FirstName;
                    guestInfo.LastName = bookingDetailEntity.LastName;

                    LoyaltyDetailsEntity loyaltyInfo = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
                    if (loyaltyInfo != null)
                    {
                        guestInfo.GuestAccountNumber = loyaltyInfo.UserName;
                        if (loyaltyInfo.MembershipID != null)
                        {
                            guestInfo.MembershipID = long.Parse(loyaltyInfo.MembershipID.ToString());
                        }
                    }

                    guestInfo.GuestAccountNumber = bookingDetailEntity.MembershipId;
                    guestInfo.D_Number = bookingDetailEntity.DNumber;
                    guestInfo.Country = bookingDetailEntity.CountryCode;
                    guestInfo.Mobile = new PhoneDetailsEntity();
                    guestInfo.Mobile.Number = string.Format(bookingDetailEntity.PhoneCountryCode.Length == 2 ? "+{0}{1}" : "00{0}{1}",
                                  bookingDetailEntity.PhoneCountryCode,
                                  bookingDetailEntity.MobileNumber);
                    guestInfo.EmailDetails = new EmailDetailsEntity();
                    guestInfo.EmailDetails.EmailID = bookingDetailEntity.EmailAddress;
                    guestInfo.NonBusinessEmailDetails = new EmailDetailsEntity();
                    guestInfo.NonBusinessEmailDetails.EmailID = bookingDetailEntity.EmailAddress;
                    guestInfo.BedTypePreference = bookingDetailEntity.BedPreference;
                    guestInfo.BedTypePreferenceCode = bookingDetailEntity.BedPreferenceCode;
                    SpecialRequestEntity[] specialRequest = null;
                    CaptureSpecialRequest(bookingDetailEntity, ref specialRequest);
                    guestInfo.SpecialRequests = specialRequest;
                    guestInfo.AdditionalRequest = bookingDetailEntity.AdditionalRequest;
                    GuranteeInformationEntity guranteeInformation = null;
                    roomAndRate = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[count] as SelectedRoomAndRateEntity;
                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                    string campaignCode = string.Empty;
                    if (hotelSearch != null)
                    {
                        if (!string.IsNullOrEmpty(bookingDetailEntity.DNumber))
                        {
                            campaignCode = bookingDetailEntity.DNumber;
                        }
                        else
                        {
                            campaignCode = hotelSearch.CampaignCode;
                        }
                    }

                    CaptureGuranteeInformation(roomAndRate.RateCategoryID, campaignCode, ref guranteeInformation,
                                               bookingDetailEntity, isCommonGurantee);
                    guestInfo.GuranteeInformation = guranteeInformation;
                }
                guestList.Add(guestInfo);
            }
        }

        /// <summary>
        /// Get the Gurantee Information of the guest
        /// </summary>
        /// <param name="rateCategoryId"></param>
        /// <param name="code"></param>
        /// <param name="guranteeInfoEntity"></param>
        /// <param name="bookingDetailEntity"></param>
        /// <param name="isCommonGurantee"></param>
        /// <returns>
        /// Gurantee Information
        /// </returns>
        private GuranteeInformationEntity CaptureGuranteeInformation(string rateCategoryId, string code,
                                                                     ref GuranteeInformationEntity guranteeInfoEntity,
                                                                     MobileEntities.BookingDetailModel
                                                                         bookingDetailEntity, bool isCommonGurantee)
        {
            RateCategory rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(rateCategoryId);
            guranteeInfoEntity = new GuranteeInformationEntity();

            bool paymentFallback = ContentDataAccess.GetPaymentFallback(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode);

            if (RoomRateUtil.IsSaveRateCategory(rateCategoryId) && !paymentFallback)
            {
                guranteeInfoEntity.GuranteeType = GuranteeType.PREPAIDINPROGRESS;
                guranteeInfoEntity.ChannelGuranteeCode = AppConstants.GUARANTEETYPE_PREAPIDINPROGRESS;

            }
            else
            {
                bool noRateCodeButDisplayGuarantee;
                string creditCardGuranteeType;
                if (IsTruelyPrepaid(rateCategoryId, code, out noRateCodeButDisplayGuarantee, out creditCardGuranteeType))
                {
                    Block block = ContentDataAccess.GetBlockCodePages(code);
                    if ((null != block) && (!string.IsNullOrEmpty(block.GuranteeType)))
                    {
                        guranteeInfoEntity.GuranteeType = GuranteeType.PREPAID;
                        guranteeInfoEntity.ChannelGuranteeCode = block.GuranteeType;
                    }
                }
                else if (IsGuanteeInfoRequested(bookingDetailEntity))
                {
                    if ((null != rateCategory) &&
                        (rateCategory.RateCategoryId.ToUpper().Equals(Reference.RewardNightBooking.ToUpper())))
                    {
                        guranteeInfoEntity.GuranteeType = GuranteeType.REDEMPTION;
                        guranteeInfoEntity.ChannelGuranteeCode = rateCategory.CreditCardGuranteeType;
                    }
                    else
                    {
                        guranteeInfoEntity.GuranteeType = GuranteeType.CREDITCARD;

                        if (noRateCodeButDisplayGuarantee)
                        {
                            guranteeInfoEntity.ChannelGuranteeCode = creditCardGuranteeType;
                        }
                        else
                        {
                            if (rateCategory != null)
                                guranteeInfoEntity.ChannelGuranteeCode = rateCategory.CreditCardGuranteeType;
                        }

                        if (!string.IsNullOrEmpty(bookingDetailEntity.PanHash))
                        {
                            var paymentController = new PaymentController();
                            guranteeInfoEntity.CreditCard = paymentController.GetCreditCardByPanHash(string.Empty,
                                                                                                     bookingDetailEntity
                                                                                                         .PanHash);

                        }
                        else if (!string.IsNullOrEmpty(bookingDetailEntity.CardNumber))
                        {
                            var expiryDateEntity =
                                new ExpiryDateEntity(Int32.Parse(bookingDetailEntity.CardExpiryMonthCode),
                                                     Int32.Parse(bookingDetailEntity.CardExpiryYearCode));
                            guranteeInfoEntity.CreditCard =
                                new CreditCardEntity(bookingDetailEntity.CardHolderName,
                                                     bookingDetailEntity.CardTypeCode,
                                                     bookingDetailEntity.CardNumber, expiryDateEntity);
                        }
                        HygieneSessionWrapper.Is6PMBooking = false;
                    }
                }
                else if (string.Equals(bookingDetailEntity.ReservationGuaranteeOption,
                                       Reference.GuranteedTextCodeBefore6PM))
                {
                    guranteeInfoEntity.GuranteeType = GuranteeType.HOLD;
                    guranteeInfoEntity.ChannelGuranteeCode = AppConstants.GURANTEE_TYPE_HOLD;
                }
            }
            return guranteeInfoEntity;
        }

        /// <summary>
        /// IsEarlyBooking
        /// </summary>
        /// <returns></returns>
        private bool IsEarlyBooking()
        {
            SelectedRoomAndRateEntity selectedRoomRates = BookingEngineSessionWrapper.SelectedRoomAndRatesHashTable[0] as SelectedRoomAndRateEntity;
            bool returnValue = true;

            if (selectedRoomRates != null)
            {
                Rate rate = RoomRateUtil.GetRate(selectedRoomRates.RoomRates[0].RatePlanCode);

                if (rate != null)
                {
                    if (!rate.IsCancellable)
                        returnValue = false;
                }
            }

            return returnValue;
        }

        /// <summary>
        /// IsGuanteeInfoRequested
        /// </summary>
        /// <param name="bookingDetailEntity"></param>
        /// <returns>True/False</returns>
        private bool IsGuanteeInfoRequested(MobileEntities.BookingDetailModel bookingDetailEntity)
        {
            if (string.Equals(bookingDetailEntity.ReservationGuaranteeOption, Reference.GuranteedTextCodeBefore6PM))
            {
                return false;
            }
            else if (!string.IsNullOrEmpty(bookingDetailEntity.ReservationGuaranteeOption))
            {
                return true;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// CaptureSpecialRequest
        /// </summary>
        /// <param name="bookingDetailEntity"></param>
        /// <param name="specialRequest"></param>
        private void CaptureSpecialRequest(MobileEntities.BookingDetailModel bookingDetailEntity,
                                           ref SpecialRequestEntity[] specialRequest)
        {
            ArrayList spRequestList = new ArrayList();

            if (!string.IsNullOrEmpty(bookingDetailEntity.FloorPreference))
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = bookingDetailEntity.FloorPreferenceCode;
                spRequestList.Add(splRequestEntity);
            }

            if (!string.IsNullOrEmpty(bookingDetailEntity.ElevatorPreference))
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = bookingDetailEntity.ElevatorPreferenceCode;
                spRequestList.Add(splRequestEntity);
            }

            if (!string.IsNullOrEmpty(bookingDetailEntity.SmokingPreference))
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = bookingDetailEntity.SmokingPreferenceCode;
                spRequestList.Add(splRequestEntity);
            }

            if (bookingDetailEntity.AccessibleRoom)
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = UserPreferenceConstants.ACCESSIBLEROOM;
                spRequestList.Add(splRequestEntity);
            }

            if (bookingDetailEntity.AllergyFriendlyRoom)
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = UserPreferenceConstants.ALLERGYROOM;
                spRequestList.Add(splRequestEntity);
            }

            if (bookingDetailEntity.PetFriendlyRoom)
            {
                SpecialRequestEntity splRequestEntity = new SpecialRequestEntity();
                splRequestEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                splRequestEntity.RequestValue = UserPreferenceConstants.PETFRIENDLYROOM;
                spRequestList.Add(splRequestEntity);
            }

            specialRequest = spRequestList.ToArray(typeof(SpecialRequestEntity)) as SpecialRequestEntity[];
        }

        /// <summary>
        /// Set the Preferred Language
        /// </summary>
        /// <param name="guestInfoEntity">
        /// GuestInformationEntity
        /// </param>
        private void SetPreferredLanguage(GuestInformationEntity guestInfoEntity)
        {
            if ((UserLoggedInSessionWrapper.UserLoggedIn) && (GuestBookingInformationSessionWrapper.FetchedGuestInformation != null))
            {
                if (!BookingEngineSessionWrapper.IsModifyBooking)
                {
                    guestInfoEntity.PreferredLanguage = GuestBookingInformationSessionWrapper.FetchedGuestInformation.PreferredLanguage;
                }
                else
                {
                    LoyaltyDetailsEntity loyaltyEntity = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
                    if (loyaltyEntity != null)
                    {
                        if (GuestBookingInformationSessionWrapper.FetchedGuestInformation.NameID == loyaltyEntity.NameID)
                        {
                            guestInfoEntity.PreferredLanguage = loyaltyEntity.PreferredLanguage;
                        }
                        else if ((BookingEngineSessionWrapper.BookingDetails != null) &&
                                 (BookingEngineSessionWrapper.BookingDetails.GuestInformation != null))
                        {
                            guestInfoEntity.PreferredLanguage =
                                BookingEngineSessionWrapper.BookingDetails.GuestInformation.PreferredLanguage;
                        }
                    }
                }
            }
            else if ((BookingEngineSessionWrapper.IsModifyBooking) && (BookingEngineSessionWrapper.BookingDetails != null)
                     && (BookingEngineSessionWrapper.BookingDetails.GuestInformation != null))
            {
                guestInfoEntity.PreferredLanguage = BookingEngineSessionWrapper.BookingDetails.GuestInformation.PreferredLanguage;
            }
        }

        /// <summary>
        /// Function Thatb calls webutil function to unblock the rooms
        /// </summary>
        private void UnBlockOnRateChange()
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if (search != null && search.ListRooms != null && search.ListRooms.Count > 0)
            {
                for (int iListRoom = 0; iListRoom < search.ListRooms.Count; iListRoom++)
                {
                    if (search.ListRooms[iListRoom].IsBlocked &&
                        (!string.IsNullOrEmpty(search.ListRooms[iListRoom].RoomBlockReservationNumber)) &&
                        (search.ListRooms[iListRoom].IsRoomModifiable))
                    {
                        WebUtil.UnblockRoom(search.ListRooms[iListRoom].RoomBlockReservationNumber);
                        search.ListRooms[iListRoom].RoomBlockReservationNumber = string.Empty;
                        search.ListRooms[iListRoom].IsBlocked = false;
                    }
                }
            }
        }

        /// <summary>
        /// Create the Reservations
        /// </summary>
        /// <param name="guestList"></param>
        /// <param name="hotelRoomRate"></param>
        /// <param name="isSessionBooking"></param>
        /// <param name="sourceCode"></param>
        /// <param name="paymentInfo"> </param>
        private void CreateReservation(List<GuestInformationEntity> guestList, List<HotelRoomRateEntity> hotelRoomRate,
                                       bool isSessionBooking, string sourceCode, PaymentInfo paymentInfo, bool isModifyBookingAfterNetsPaymentSuccessful)
        {
            string createdNameID = string.Empty;
            ReservationController reservationController = new ReservationController();
            SortedList<string, GuestInformationEntity> reservationMap = new SortedList<string, GuestInformationEntity>();

            string reservationNumber = string.Empty;
            int totalGuests = guestList.Count;
            SortedList<string, string> legReservationMap = null;
            for (int guestCount = 0; guestCount < totalGuests; guestCount++)
            {
                legReservationMap = null;
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if ((paymentInfo != null) && (guestList[guestCount].GuranteeInformation.GuranteeType == GuranteeType.PREPAIDINPROGRESS))
                {
                    guestList[guestCount].GuranteeInformation.CreditCard =
                         new CreditCardEntity(paymentInfo.PanHash, paymentInfo.CardType, paymentInfo.CreditCardNumberMasked,
                                              new ExpiryDateEntity(paymentInfo.ExpiryDate.Month, paymentInfo.ExpiryDate.Year));
                }
                if (search != null && search.ListRooms != null && search.ListRooms.Count > 0 &&
                    guestCount < search.ListRooms.Count && search.ListRooms[guestCount].IsRoomModifiable)
                {
                    HotelSearchRoomEntity roomSearch = search.ListRooms[guestCount];
                    if (((BookingEngineSessionWrapper.IsModifyBooking) &&
                        (!string.IsNullOrEmpty(guestList[guestCount].LegNumber))
                        && roomSearch.IsRoomConfirmed == true) || isModifyBookingAfterNetsPaymentSuccessful)
                    {
                        if (paymentInfo.MakePaymentFailure)
                        {
                            guestList[guestCount].GuranteeInformation.GuranteeType = GuranteeType.MAKEPAYMENTFAILURE;
                            guestList[guestCount].GuranteeInformation.ChannelGuranteeCode =
                                AppConstants.GUARANTEETYPE_MAKEPAYMENTFAILURE;
                        }
                        else if (paymentInfo.CaptureSuccess)
                        {
                            guestList[guestCount].GuranteeInformation.GuranteeType = GuranteeType.PREPAIDSUCCESS;
                            guestList[guestCount].GuranteeInformation.ChannelGuranteeCode =
                                AppConstants.GUARANTEETYPE_PREAPIDSUCCESS;
                        }
                        else
                        {
                            guestList[guestCount].GuranteeInformation.GuranteeType = GuranteeType.PREPAIDFAILURE;
                            guestList[guestCount].GuranteeInformation.ChannelGuranteeCode =
                                AppConstants.GUARANTEETYPE_PREAPIDFAILURE;
                        }
                        guestList[guestCount].UserPreferences =
                            Utility.ConvertToUserPreferenceEntityList(guestList[guestCount].SpecialRequests);

                        if (isModifyBookingAfterNetsPaymentSuccessful)
                        {
                            legReservationMap = reservationController.ModifyGuaranteeInformation(search, roomSearch,
                                                                               hotelRoomRate[guestCount],
                                                                               guestList[guestCount],
                                                                               guestList[guestCount].ReservationNumber,
                                                                               sourceCode, out createdNameID,
                                                                               isSessionBooking);
                        }
                        else
                        {
                            legReservationMap = reservationController.ModifyBooking(search, roomSearch,
                                                                                hotelRoomRate[guestCount],
                                                                                guestList[guestCount],
                                                                                guestList[guestCount].ReservationNumber,
                                                                                sourceCode, out createdNameID,
                                                                                isSessionBooking);
                        }
                    }
                    else
                    {
                        legReservationMap = reservationController.CreateBooking(search, roomSearch,
                                                                                hotelRoomRate[guestCount],
                                                                                guestList[guestCount], reservationNumber,
                                                                                sourceCode, out createdNameID,
                                                                                isSessionBooking,
                                                                                HygieneSessionWrapper.PartnerID);
                    }
                }
                if (legReservationMap != null)
                {
                    foreach (string key in legReservationMap.Keys)
                    {
                        reservationNumber = legReservationMap[key];
                        guestList[guestCount].LegNumber = key;
                        guestList[guestCount].ReservationNumber = reservationNumber;
                        reservationMap[reservationNumber + AppConstants.HYPHEN + key] = guestList[guestCount];
                        break;
                    }
                }
                if (createdNameID != string.Empty)
                {
                    SearchCriteriaSessionWrapper.SearchCriteria.NameID = createdNameID;
                }
                if ((SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.BONUSCHEQUE) &&
                    (!guestList[guestCount].IsValidDNumber))
                {
                    SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode = null;
                }
                if ((HygieneSessionWrapper.IsComboReservation) && (Reservation2SessionWrapper.IsModifyFlow) &&
                    (!search.ListRooms[guestCount].IsRoomModifiable))
                {
                    if ((GuestBookingInformationSessionWrapper.AllGuestsBookingInformations != null) && (guestList[guestCount] != null))
                    {
                        string legNumber = guestList[guestCount].LegNumber.ToString();
                        string resNumber = guestList[guestCount].ReservationNumber.ToString();
                        reservationMap[resNumber + AppConstants.HYPHEN + legNumber] =
                            GuestBookingInformationSessionWrapper.AllGuestsBookingInformations[resNumber + AppConstants.HYPHEN + legNumber];
                    }
                }
            }
            ReservationNumberSessionWrapper.ReservationNumber = reservationNumber;

            GuestBookingInformationSessionWrapper.AllGuestsBookingInformations = reservationMap;
        }

        /// <summary>
        /// Confirms the created booking. This is required for the sessionEnabled.
        /// </summary>
        public void ConfirmBooking(List<GuestInformationEntity> guestList)
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if ((search != null) && (guestList.Count == search.ListRooms.Count))
            {
                for (int i = 0; i < guestList.Count; i++)
                {
                    if (search.ListRooms[i].IsRoomModifiable)
                    {
                        GuestInformationEntity guest = guestList[i];
                        WebUtil.ConfirmBooking(guest.ReservationNumber, guest.LegNumber);
                    }
                }
            }
        }

        /// <summary>
        /// Sends Confirmation to guests.
        /// </summary>
        private void SendMailConfirmationToGuestsUsingHTMLString()
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("Start callBack method");
            try
            {
                SortedList<int, string> emailBookingMap = new SortedList<int, string>();
                HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
                ErrorsSessionWrapper.ClearFailedEmailRecipient();
                string templateFileName = string.Empty;
                string bookingType = PageIdentifier.BookingPageIdentifier;
                SelectedRoomAndRateEntity selectedRoomRate = null;

                if (BookingEngineSessionWrapper.IsModifyBooking)
                    bookingType = PageIdentifier.ModifyBookingPageIdentifier;

                if (hotelSearch != null && hotelSearch.ListRooms.Count > 0)
                {
                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage(
                        "UserContactBookingDetails:GenerateHTMLString for Email and PDF:Start time");

                    for (int roomIterator = 0; roomIterator < hotelSearch.ListRooms.Count; roomIterator++)
                    {
                        string htmlStringForEmail = string.Empty;
                        string htmlStringForPDF = string.Empty;
                        string hostName = string.Empty;
                        string alterhtmlStringForPDF = string.Empty;
                        string creditCardReceiptHtmlString = string.Empty;

                        if (roomIterator == 0)
                        {
                            GenerateConfirmationPageHtmlString(
                                @"/templates/Scanweb/Pages/ConfirmationEmailPage.aspx?AllROOMSTOBESHOWN=true&BOOKINGTYPE=" +
                                bookingType, ref htmlStringForEmail);

                            GenerateConfirmationPageHtmlString(
                                @"/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx?command=print&isPDF=true",
                                ref htmlStringForPDF);

                            if (string.IsNullOrEmpty(htmlStringForPDF))
                                GenerateConfirmationPageHtmlString(
                                    @"/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx?command=print&isPDF=true",
                                    ref htmlStringForPDF);

                            //send receipt email only for the first person
                            if (WebUtil.IsAnyRoomHavingSaveCategory(HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable) && roomIterator == 0)
                                GenerateConfirmationPageHtmlString(
                                    @"/templates/Scanweb/Pages/Booking/Receipt.aspx?command=print&isPDF=true",
                                    ref creditCardReceiptHtmlString);

                            templateFileName =
                                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();

                            templateFileName = templateFileName.Remove(templateFileName.IndexOf("\\bin"));

                            alterhtmlStringForPDF = htmlStringForPDF.Replace("/Templates",
                                                                             templateFileName + "/Templates");

                            creditCardReceiptHtmlString = creditCardReceiptHtmlString.Replace("/Templates",
                                                                             templateFileName + "/Templates");

                            emailBookingMap.Add(roomIterator, htmlStringForEmail + "^" + alterhtmlStringForPDF + "^" + creditCardReceiptHtmlString);
                        }
                        else
                        {
                            if (
                                (!(Reservation2SessionWrapper.IsModifyFlow)) ||
                                ((Reservation2SessionWrapper.IsModifyFlow) && !(HygieneSessionWrapper.IsComboReservation)) ||
                                ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation) &&
                                 (hotelSearch.ListRooms[roomIterator].IsRoomModifiable))
                                )
                            {
                                GenerateConfirmationPageHtmlString(
                                    @"/templates/Scanweb/Pages/ConfirmationEmailPage.aspx?AllROOMSTOBESHOWN=false&ROOMNUMBERTOBEDISPLAYED=" +
                                    roomIterator + "&BOOKINGTYPE=" + bookingType, ref htmlStringForEmail);

                                GenerateConfirmationPageHtmlString(
                                    @"/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx?AllROOMSTOBESHOWN=false&isPDF=true&command=print&roomNumber=" +
                                    roomIterator, ref htmlStringForPDF);

                                if (string.IsNullOrEmpty(htmlStringForPDF))
                                    GenerateConfirmationPageHtmlString(
                                        @"/templates/Scanweb/Pages/Booking/PrinterFriendlyConfirm.aspx?isPDF=true&command=print&roomNumber=" +
                                        roomIterator, ref htmlStringForPDF);

                                //send receipt email only for the first person
                                if (WebUtil.IsAnyRoomHavingSaveCategory(HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable) && roomIterator == 0)
                                    GenerateConfirmationPageHtmlString(
                                        @"/templates/Scanweb/Pages/Booking/Receipt.aspx?command=print&isPDF=true",
                                        ref creditCardReceiptHtmlString);

                                templateFileName =
                                    Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();

                                templateFileName = templateFileName.Remove(templateFileName.IndexOf("\\bin"));
                                alterhtmlStringForPDF = htmlStringForPDF.Replace("/Templates",
                                                                                 templateFileName + "/Templates");

                                creditCardReceiptHtmlString = creditCardReceiptHtmlString.Replace("/Templates",
                                                                             templateFileName + "/Templates");

                                emailBookingMap.Add(roomIterator, htmlStringForEmail + "^" + alterhtmlStringForPDF + "^" + creditCardReceiptHtmlString);
                            }
                        }
                    }
                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage(
                        "UserContactBookingDetails:GenerateHTMLString for Email and PDF:End time");
                }

                SortedList<string, GuestInformationEntity> guestsList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;

                if (
                    (guestsList != null) &&
                    (guestsList.Count > 0) &&
                    (hotelSearch != null) &&
                    (hotelSearch.ListRooms != null) &&
                    (hotelSearch.ListRooms.Count > 0) &&
                    (guestsList.Count == hotelSearch.ListRooms.Count)
                    )
                {
                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage(
                        "UserContactBookingDetails:Send EMail and create PDF:Start time");

                    int counter = 0;
                    List<EmailEntity> emails = new List<EmailEntity>();
                    List<string> reservations = new List<string>();

                    foreach (string key in guestsList.Keys)
                    {
                        if (
                            (counter == 0) ||
                            !(Reservation2SessionWrapper.IsModifyFlow) ||
                            ((Reservation2SessionWrapper.IsModifyFlow) && !(HygieneSessionWrapper.IsComboReservation)) ||
                            ((Reservation2SessionWrapper.IsModifyFlow) && (HygieneSessionWrapper.IsComboReservation) &&
                             (hotelSearch.ListRooms[counter].IsRoomModifiable))
                            )
                        {
                            string emailText = string.Empty;
                            string[] emailTextAndPDF = null;
                            GuestInformationEntity currentGuestInformationEntity =
                                guestsList[key] as GuestInformationEntity;

                            if (currentGuestInformationEntity != null)
                            {
                                emailText = ((emailBookingMap[counter] != null)
                                                 ? emailBookingMap[counter]
                                                 : string.Empty);

                                if (!string.IsNullOrEmpty(emailText))
                                    emailTextAndPDF = emailText.Split('^');

                                EmailEntity emailEntity = new EmailEntity();

                                if (emailTextAndPDF != null)
                                {
                                    if (emailTextAndPDF[0] != null)
                                        emailEntity.Body = emailTextAndPDF[0];

                                    if (emailTextAndPDF[1] != null)
                                        emailEntity.TextEmailBody = emailTextAndPDF[1];

                                    if (emailTextAndPDF.Length > 2)
                                        if (!string.IsNullOrEmpty(emailTextAndPDF[2]))
                                            emailEntity.CreditCardReceiptHtmlText = emailTextAndPDF[2];
                                }
                                if (currentGuestInformationEntity.IsRewardNightGift &&
                                    LoyaltyDetailsSessionWrapper.LoyaltyDetails != null &&
                                    !string.IsNullOrEmpty(LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email))
                                {
                                    emailEntity.Recipient = new string[]
                                                                {
                                                                    currentGuestInformationEntity.EmailDetails.EmailID,
                                                                    LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email
                                                                };
                                }
                                else
                                {
                                    emailEntity.Recipient = new string[] { currentGuestInformationEntity.EmailDetails.EmailID };
                                }

                                emailEntity.Subject =
                                    WebUtil.GetTranslatedText("/bookingengine/booking/confirmation/subject");
                                emailEntity.Sender =
                                    WebUtil.GetTranslatedText("/bookingengine/booking/confirmation/fromEmail");
                                string reservationNumber = currentGuestInformationEntity.ReservationNumber + "-" + key;
                                emailEntity.ReceiptReservationNumber = string.Concat(currentGuestInformationEntity.ReservationNumber, "-",
                                                WebUtil.GetTranslatedText("/bookingengine/booking/BookingReceipt/BookingTypeSave"));
                                //Send mail
                                emails.Add(emailEntity);
                                reservations.Add(reservationNumber);
                            }
                        }

                        counter++;
                    }
                    SendMail(emails, reservations);
                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage(
                        "UserContactBookingDetails:Send EMail and create PDF:End time");
                }
            }
            catch (Exception ex)
            {
                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("exception in send email:" + ex.InnerException);
            }
        }

        /// <summary>
        /// This method will create PDF files asynchronously.
        /// </summary>
        /// <param name="emails">emails list</param>
        /// <param name="reservations">reservations list</param>
        public void SendMail(List<EmailEntity> emails, List<string> reservations)
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendMail() Start Time");
            CreatePDF createPDFDeligate = new CreatePDF(SendEmailAsync);
            IAsyncResult result = createPDFDeligate.BeginInvoke(emails, reservations, null, null);
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendMail() End Time");
        }

        /// <summary>
        /// Asynchronous create pdf method for Deligate.
        /// </summary>
        /// <param name="emails">emails list</param>
        /// <param name="reservations">reservations List</param>
        public void SendEmailAsync(List<EmailEntity> emails, List<string> reservations)
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendEmailAsync(): Excuted sucessfully:Start Time");
            int counter = 0;
            foreach (EmailEntity email in emails)
            {
                Utility.SendEmailConfirmationToGuest(email, reservations[counter]);
                counter++;
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("SendEmailAsync(): Excuted sucessfully: End Time");
        }

        /// <summary>
        /// Generates the confirmation page HTML string.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="htmlString">The HTML string.</param>
        private void GenerateConfirmationPageHtmlString(string url, ref string htmlString)
        {
            #region Very IMP code To render the preview page

            Scandic.Scanweb.Core.AppLogger.LogInfoMessage(
                "BookingDetailsUtility:GenerateConfirmationPageHtmlString() Begin");
            StringWriter sw = new StringWriter();
            try
            {
                HttpContext.Current.Server.Execute(url, sw);
                htmlString = (sw.ToString()).Trim().Replace("\r\n", "").Replace("\"", "'");
            }
            catch (Exception ex)
            {
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage(
                "BookingDetailsUtility:GenerateConfirmationPageHtmlString() End");

            #endregion
        }

        /// <summary>
        /// GetReservationGuaranteeOption
        /// </summary>
        /// <param name="bookingDetailEntity"></param>
        /// <returns>True/False</returns>
        private bool GetReservationGuaranteeOption(MobileEntities.BookingDetailModel bookingDetailEntity)
        {
            if (bookingDetailEntity != null && !string.IsNullOrEmpty(bookingDetailEntity.ReservationGuaranteeOption) &&
                !string.Equals(bookingDetailEntity.ReservationGuaranteeOption, Reference.GuranteedTextCodeBefore6PM))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// SendConfirmationToGuest
        /// </summary>
        /// <param name="bookingDetailEntity"></param>
        private void SendConfirmationToGuest(MobileEntities.BookingDetailModel bookingDetailEntity)
        {
            try
            {
                bool IsBookingWithCreditCard = GetReservationGuaranteeOption(bookingDetailEntity);
                Dictionary<string, string> emailBookingMap = new Dictionary<string, string>();

                emailBookingMap = Utility.CollectGenericHotelEmailDetails();
                SortedList<string, GuestInformationEntity> guestsList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
                Hashtable htbl = new Hashtable();

                if (guestsList != null)
                {

                    emailBookingMap[CommunicationTemplateConstants.LOGOPATH] = AppConstants.LOGOPATH;
                    Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                    int totalGuests = guestsList.Count;
                    int guestIterator = 0;

                    foreach (string key in guestsList.Keys)
                    {
                        int roomNumber = Convert.ToInt32(key);
                        if (selectedRoomAndRatesHashTable != null && selectedRoomAndRatesHashTable.Count > 0)
                        {
                            SelectedRoomAndRateEntity selectedRoomAndRates =
                                selectedRoomAndRatesHashTable[guestIterator] as SelectedRoomAndRateEntity;
                            CollectBookingConfirmationEmailDetails(ref emailBookingMap, selectedRoomAndRates,
                                                                   guestsList[key], guestIterator,
                                                                   IsBookingWithCreditCard);
                            if (!htbl.ContainsKey(selectedRoomAndRates.RateCategoryID + AppConstants.Room + roomNumber))
                            {
                                htbl.Add(selectedRoomAndRates.RateCategoryID + AppConstants.Room + roomNumber,
                                         emailBookingMap[CommunicationTemplateConstants.POLICY_TEXT]);
                            }
                            else
                            {
                                if (!htbl.ContainsValue(emailBookingMap[CommunicationTemplateConstants.POLICY_TEXT]))
                                {
                                    htbl.Add(selectedRoomAndRates.RateCategoryID + AppConstants.Room + roomNumber,
                                             emailBookingMap[CommunicationTemplateConstants.POLICY_TEXT]);
                                }
                            }
                        }
                        string resNumber = string.Empty;
                        if (Convert.ToInt32(key) > 1)
                            resNumber = guestsList[key].ReservationNumber + "-" + key;
                        else
                            resNumber = guestsList[key].ReservationNumber;

                        CollectGuestsSpecificEmailDetails(ref emailBookingMap, guestsList[key], resNumber);
                        ConfirmationHeaderSessionWrapper.ConfirmationHeader =
                            emailBookingMap[CommunicationTemplateConstants.HTMLPOLICYHEADER];
                        BookingEngineSessionWrapper.IsSmsRequired = false;
                        ErrorsSessionWrapper.FailedSMSRecepient = null;
                        if ((bookingDetailEntity != null && bookingDetailEntity.ReceiveConfirmationSMS) &&
                            (SearchCriteriaSessionWrapper.SearchCriteria.ListRooms[guestIterator].IsRoomModifiable))
                        {
                            Utility.SendSMSConfirmationToGuest(ref emailBookingMap, key, resNumber);
                        }
                        guestIterator++;
                    }
                }
                HygieneSessionWrapper.PolicyTextConfirmation = htbl;
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Failed to send booking confirmation in SendConfirmationToGuest() to the Guest");
            }
        }

        /// <summary>
        /// CollectGuestsSpecificEmailDetails
        /// </summary>
        /// <param name="bookingMap"></param>
        /// <param name="guestInfoEntity"></param>
        /// <param name="reservationNumber"></param>
        private void CollectGuestsSpecificEmailDetails(ref Dictionary<string, string> bookingMap,
                                                       GuestInformationEntity guestInfoEntity, string reservationNumber)
        {
            bookingMap[CommunicationTemplateConstants.CONFIRMATION_NUMBER] = reservationNumber;
            if (guestInfoEntity.GuestAccountNumber != null && guestInfoEntity.GuestAccountNumber != string.Empty)
            {
                bookingMap[CommunicationTemplateConstants.MEMBERSHIPNUMBER] = guestInfoEntity.GuestAccountNumber;
            }
            else
            {
                bookingMap[CommunicationTemplateConstants.NO_MEMBERID] = AppConstants.SPACE;
            }
            bookingMap[CommunicationTemplateConstants.FIRST_NAME] = guestInfoEntity.FirstName;
            bookingMap[CommunicationTemplateConstants.LASTNAME] = guestInfoEntity.LastName;

            if (guestInfoEntity.Mobile != null && guestInfoEntity.Mobile.Number != null)
            {
                bookingMap[CommunicationTemplateConstants.TELEPHONE2] = guestInfoEntity.Mobile.Number;
            }

            if (guestInfoEntity.EmailDetails != null)
            {
                bookingMap[CommunicationTemplateConstants.EMAIL] = guestInfoEntity.EmailDetails.EmailID;
                if (guestInfoEntity.IsRewardNightGift)
                {
                    if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null &&
                        !string.IsNullOrEmpty(LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email))
                    {
                        bookingMap[CommunicationTemplateConstants.EMAIL2] = LoyaltyDetailsSessionWrapper.LoyaltyDetails.Email;
                    }
                }
            }
        }

        /// <summary>
        /// Collect the Booking Confirmation Informations to be send as Email
        /// </summary>
        /// <param name="bookingMap"></param>
        /// <param name="selectedRoomAndRateEntity"></param>
        /// <param name="guestInfoEntity"></param>
        /// <param name="guestIterator"></param>
        /// <param name="IsBookingWithCreditCard"></param>
        /// <returns>
        /// OrderedDictionary
        /// </returns>
        private Dictionary<string, string> CollectBookingConfirmationEmailDetails(
            ref Dictionary<string, string> bookingMap, SelectedRoomAndRateEntity selectedRoomAndRateEntity,
            GuestInformationEntity guestInfoEntity, int guestIterator, bool IsBookingWithCreditCard)
        {
            Utility.CollectHotelRoomEmailDetails(ref bookingMap, guestIterator);
            CollectGenericGuestEmailDetails(ref bookingMap, guestInfoEntity);
            CollectDynamicTextFromCMS(ref bookingMap, selectedRoomAndRateEntity, guestIterator, IsBookingWithCreditCard);

            return bookingMap;
        }

        /// <summary>
        /// This method will fetch Dynamic content from CMS and replace it in Email template.
        /// </summary>
        /// <param name="bookingMap"></param>
        /// <param name="selectedRoomAndRateEntity"></param>
        /// <param name="guestIterator"></param>
        /// <param name="IsBookingWithCreditCard"></param>
        private void CollectDynamicTextFromCMS(ref Dictionary<string, string> bookingMap,
                                               SelectedRoomAndRateEntity selectedRoomAndRateEntity, int guestIterator,
                                               bool IsBookingWithCreditCard)
        {
            string selectedRateCategoryID = selectedRoomAndRateEntity.RateCategoryID;
            string guranteeType = string.Empty;
            RateCategory rateCategory = null;
            if (!string.IsNullOrEmpty(selectedRateCategoryID))
            {
                rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(selectedRateCategoryID);
            }
            bool noRateCodeButDisplayGuarantee;
            string creditCardGuranteeType;
            if (IsTruelyPrepaid(selectedRateCategoryID, SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode,
                                out noRateCodeButDisplayGuarantee, out creditCardGuranteeType))
            {
                string[] prepaidGuranteeTypes = AppConstants.GUARANTEE_TYPE_PRE_PAID;
                if ((prepaidGuranteeTypes != null) && (prepaidGuranteeTypes.Length > 0) &&
                    !ContentDataAccess.GetPaymentFallback(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode))
                {
                    guranteeType = prepaidGuranteeTypes[0];
                }
            }
            else if (noRateCodeButDisplayGuarantee)
            {
                guranteeType = creditCardGuranteeType;
            }
            else if (null != rateCategory)
            {
                guranteeType = rateCategory.CreditCardGuranteeType;
            }

            string pageIdentifier = string.Empty;
            if (BookingEngineSessionWrapper.IsModifyBooking)
            {
                pageIdentifier = PageIdentifier.ModifyBookingPageIdentifier;
            }
            else
            {
                pageIdentifier = PageIdentifier.BookingPageIdentifier;
            }
            CollectCmsText(bookingMap, guranteeType, pageIdentifier, guestIterator, IsBookingWithCreditCard);

            if ((null != rateCategory) &&
                (rateCategory.RateCategoryId.ToUpper().Equals(SearchType.REDEMPTION.ToString())))
            {
                bookingMap["POLICYTEXT"] = (null !=
                                            ContentDataAccess.GetTextForEmailTemplate(pageIdentifier)["POLICYTEXT"])
                                               ? ContentDataAccess.GetTextForEmailTemplate(pageIdentifier)["POLICYTEXT"]
                                                     .ToString()
                                               : AppConstants.SPACE;
            }
        }

        /// <summary>
        /// This will assign the key with values from CMS.
        /// </summary>
        /// <param name="bookingMap"></param>
        /// <param name="guranteeType"></param>
        /// <param name="pageIdentifier"></param>
        /// <param name="guestIterator"></param>
        /// <param name="IsBookingWithCreditCard"></param>
        private void CollectCmsText(Dictionary<string, string> bookingMap, string guranteeType, string pageIdentifier,
                                    int guestIterator, bool IsBookingWithCreditCard)
        {
            Dictionary<string, string> cmsText = null;
            string[] prepaidGuranteeTypes = AppConstants.GUARANTEE_TYPE_PRE_PAID;

            if ((prepaidGuranteeTypes != null) && (prepaidGuranteeTypes.Contains(guranteeType)) && !ContentDataAccess.GetPaymentFallback(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode))
            {
                cmsText = ContentDataAccess.GetTextForEmailTemplate(guranteeType, pageIdentifier, string.Empty, GenericSessionVariableSessionWrapper.IsPrepaidBooking);
                if ((null != cmsText) && (null != cmsText["POLICYTEXT"]))
                {
                    cmsText["POLICYTEXT"] = cmsText["POLICYTEXT"].Replace(
                        CommunicationTemplateConstants.CANCELBYDATE, Utility.GetModifyCancelableDate());
                }
            }
            else
            {
                cmsText = ContentDataAccess.GetTextForEmailTemplate(guranteeType, pageIdentifier,
                                                                    CommunicationTemplateConstants.WITHOUTCREDITCARD, GenericSessionVariableSessionWrapper.IsPrepaidBooking);
            }
            if (null != cmsText)
            {
                bookingMap[CommunicationTemplateConstants.CONTACTUS_TEXT] =
                    cmsText.ContainsKey(CommunicationTemplateConstants.CONTACTUS_TEXT)
                        ? cmsText[CommunicationTemplateConstants.CONTACTUS_TEXT]
                        : AppConstants.SPACE;

                bookingMap[CommunicationTemplateConstants.CONFIRMATION_TEXT] =
                    cmsText.ContainsKey(CommunicationTemplateConstants.CONFIRMATION_TEXT)
                        ? cmsText[CommunicationTemplateConstants.CONFIRMATION_TEXT]
                        : AppConstants.SPACE;

                bookingMap[CommunicationTemplateConstants.STORYBOX_TEXT] =
                    cmsText.ContainsKey(CommunicationTemplateConstants.STORYBOX_TEXT)
                        ? cmsText[CommunicationTemplateConstants.STORYBOX_TEXT]
                        : AppConstants.SPACE;

                bookingMap[CommunicationTemplateConstants.HTMLPOLICYHEADER] =
                    cmsText.ContainsKey(CommunicationTemplateConstants.HTMLPOLICYHEADER)
                        ? cmsText[CommunicationTemplateConstants.HTMLPOLICYHEADER]
                        : AppConstants.SPACE;

                SortedList<string, GuestInformationEntity> guestInformationList =
                    GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
                if (guestInformationList != null)
                {
                    if (IsBookingWithCreditCard)
                    {
                        cmsText = ContentDataAccess.GetTextForEmailTemplate(guranteeType, pageIdentifier,
                                                                            CommunicationTemplateConstants.
                                                                                WITHCREDITCARD, GenericSessionVariableSessionWrapper.IsPrepaidBooking);
                        bookingMap[CommunicationTemplateConstants.POLICY_TEXT] =
                            cmsText.ContainsKey("POLICYTEXT")
                                ? cmsText["POLICYTEXT"]
                                : AppConstants.SPACE;
                    }
                    else
                    {
                        bookingMap[CommunicationTemplateConstants.POLICY_TEXT] =
                            cmsText.ContainsKey("POLICYTEXT")
                                ? cmsText["POLICYTEXT"]
                                : AppConstants.SPACE;
                    }
                }
            }
            else
            {
                SetupCmsMapToEmptyString(bookingMap);
            }
        }

        /// <summary>
        /// This will set the different property to empty string 
        /// </summary>
        /// <param name="bookingMap">Map Used for holding the different cms text.</param>
        private void SetupCmsMapToEmptyString(Dictionary<string, string> bookingMap)
        {
            if (bookingMap != null)
            {
                string emptyString = AppConstants.SPACE;

                bookingMap[CommunicationTemplateConstants.CONTACTUS_TEXT] = emptyString;
                bookingMap[CommunicationTemplateConstants.CONFIRMATION_TEXT] = emptyString;
                bookingMap[CommunicationTemplateConstants.STORYBOX_TEXT] = emptyString;
                bookingMap[CommunicationTemplateConstants.POLICY_TEXT] = emptyString;
            }
        }

        /// <summary>
        /// Collect Guest Specific Email Details
        /// </summary>
        /// <param name="bookingMap">
        /// Dictionary of Email template tokens with its corresponding values
        /// </param>
        /// <param name="guestInfoEntity"></param>
        private void CollectGenericGuestEmailDetails(ref Dictionary<string, string> bookingMap,
                                                     GuestInformationEntity guestInfoEntity)
        {
            if (guestInfoEntity != null)
            {
                bookingMap[CommunicationTemplateConstants.CITY] = guestInfoEntity.City;
                OrderedDictionary countryCodes = DropDownService.GetCountryCodes();
                if (countryCodes != null)
                {
                    string country = countryCodes[guestInfoEntity.Country] as string;
                    country = country.Replace("\r\n ", "");
                    bookingMap[CommunicationTemplateConstants.COUNTRY] = country;
                }
                if (BedTypePreferenceSessionWrapper.UserBedTypePreference != null)
                {
                    bookingMap[CommunicationTemplateConstants.BED_TYPE_PREFERENCE] =
                        BedTypePreferenceSessionWrapper.UserBedTypePreference;
                }
                else
                {
                    bookingMap[CommunicationTemplateConstants.BED_TYPE_PREFERENCE] =
                        CommunicationTemplateConstants.BLANK_SPACES;
                }
                string selectedRateCategoryID = HotelRoomRateSessionWrapper.SelectedRateCategoryID;
                RateCategory rateCategory = null;
                if ((selectedRateCategoryID != null) && (selectedRateCategoryID != string.Empty))
                {
                    rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(selectedRateCategoryID);
                }

                RetrieveSpecialRequestDetails(bookingMap);
            }
        }

        /// <summary>
        /// Retrieve the Guest's special Request Details 
        /// </summary>
        /// <param name="bookingMap">
        /// </param>
        private static void RetrieveSpecialRequestDetails(Dictionary<string, string> bookingMap)
        {
            GuestInformationEntity guestInfoEntity =
                GuestBookingInformationSessionWrapper.GuestBookingInformation as GuestInformationEntity;

            if (guestInfoEntity != null)
            {
                if (guestInfoEntity.SpecialRequests != null)
                {
                    string strotherPeferencesList = string.Empty;
                    string strspecialRequestList = string.Empty;
                    StringBuilder otherPeferencesList = new StringBuilder();
                    StringBuilder specialRequestList = new StringBuilder();

                    int totalSpecialRequest = guestInfoEntity.SpecialRequests.Length;
                    for (int spRequestCount = 0; spRequestCount < totalSpecialRequest; spRequestCount++)
                    {
                        switch (guestInfoEntity.SpecialRequests[spRequestCount].RequestValue)
                        {
                            case UserPreferenceConstants.LOWERFLOOR:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.LOWERFLOOR));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.HIGHFLOOR:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.HIGHERFLOOR));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.AWAYFROMELEVATOR:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.AWAYFROMELEVATOR));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.NEARELEVATOR:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.NEARELEVATOR));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.NOSMOKING:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.NOSMOKING));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.SMOKING:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.SMOKING));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.ACCESSIBLEROOM:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ACCESSABLEROOM));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ACCESSABLEROOMDESC));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.ALLERGYROOM:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ALLERYROOM));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ALLERYROOMDESC));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.PETFRIENDLYROOM:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.PETFRIENDLYROOM));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.PETFRIENDLYROOMDESC));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.EARLYCHECKIN:
                                {
                                    specialRequestList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.EARLYCHECKIN));
                                    specialRequestList.Append(" <br />");
                                    specialRequestList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.EARLYCHECKINDESC));
                                    specialRequestList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.LATECHECKOUT:
                                {
                                    specialRequestList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.LATECHECKOUT));
                                    specialRequestList.Append(" <br />");
                                    specialRequestList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.LATECHECKOUTDESC));
                                    specialRequestList.Append(" <br />");
                                    break;
                                }
                            default:
                                break;
                        }
                    }
                    strotherPeferencesList = otherPeferencesList.ToString();
                    strspecialRequestList = specialRequestList.ToString();
                    if (bookingMap == null)
                    {
                        bookingMap = new Dictionary<string, string>();
                    }
                    if (!string.IsNullOrEmpty(strotherPeferencesList)) // != null && otherPeferencesList != "")
                    {
                        bookingMap[CommunicationTemplateConstants.OTHER_PREFERENCE] = strotherPeferencesList;
                        string otherPreference = strotherPeferencesList.Replace("<br />", "\r\n\t");
                        bookingMap[CommunicationTemplateConstants.TEXTFORMAT_OTHER_PREFERENCE] = otherPreference;
                    }
                    else
                    {
                        bookingMap[CommunicationTemplateConstants.OTHER_PREFERENCE] =
                            CommunicationTemplateConstants.BLANK_SPACES;
                        bookingMap[CommunicationTemplateConstants.TEXTFORMAT_OTHER_PREFERENCE] =
                            CommunicationTemplateConstants.BLANK_SPACES;
                    }

                    if (string.IsNullOrEmpty(strspecialRequestList)) // != null && specialRequestList != "")
                    {
                        bookingMap[CommunicationTemplateConstants.SPECIAL_SERVICE_REQUESTS] = strspecialRequestList;
                        string specilalRequest = strspecialRequestList.Replace("<br />", "\r\n\t");
                        bookingMap[CommunicationTemplateConstants.TEXTFORMAT_SPECIAL_SERVICE_REQUESTS] = specilalRequest;
                    }
                    else
                    {
                        bookingMap[CommunicationTemplateConstants.SPECIAL_SERVICE_REQUESTS] =
                            CommunicationTemplateConstants.BLANK_SPACES;
                        bookingMap[CommunicationTemplateConstants.TEXTFORMAT_SPECIAL_SERVICE_REQUESTS] =
                            CommunicationTemplateConstants.BLANK_SPACES;
                    }
                }
            }
        }

        /// <summary>
        /// Get the HotelRoomRateEntity for the specified roomTypeCode
        /// </summary>
        /// <param name="roomTypeCode">RoomType Code</param>
        /// <param name="selectedRoomIndex">Index of the selected room.</param>
        /// <param name="guestList"></param>
        /// <returns>HotelRoomRateEntity</returns>
        private HotelRoomRateEntity GetHotelRoomRate(string roomTypeCode, int selectedRoomIndex,
                                                     List<GuestInformationEntity> guestList)
        {
            HotelRoomRateEntity hotelRoomRate = null;
            if ((BookingEngineSessionWrapper.IsModifyBooking) && (BookingEngineSessionWrapper.DirectlyModifyContactDetails))
            {
                if (BookingEngineSessionWrapper.BookingDetails != null)
                {
                    BookingDetailsEntity bookDetEntity = null;
                    string LegNumberOfRes = BookingEngineSessionWrapper.BookingDetails.LegNumber;
                    if (HygieneSessionWrapper.IsComboReservation && guestList != null)
                    {
                        LegNumberOfRes = guestList[selectedRoomIndex].LegNumber;
                    }
                    bookDetEntity = BookingEngineSessionWrapper.GetLegBookingDetails(LegNumberOfRes,
                                                                        BookingEngineSessionWrapper.BookingDetails.ReservationNumber);
                    hotelRoomRate = bookDetEntity.HotelRoomRate;
                }
            }
            else if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null)
            {
                Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                SelectedRoomAndRateEntity selectedRoomAndRateEntity = null;
                if (selectedRoomAndRatesHashTable.ContainsKey(selectedRoomIndex))
                {
                    selectedRoomAndRateEntity =
                        selectedRoomAndRatesHashTable[selectedRoomIndex] as SelectedRoomAndRateEntity;
                }

                if (selectedRoomAndRateEntity != null)
                {
                    List<RoomRateEntity> selectedRoomRates = selectedRoomAndRateEntity.RoomRates;
                    hotelRoomRate = new HotelRoomRateEntity();
                    foreach (RoomRateEntity roomRate in selectedRoomRates)
                    {
                        if (roomRate.RoomTypeCode == roomTypeCode)
                        {
                            hotelRoomRate.RatePlanCode = roomRate.RatePlanCode;
                            hotelRoomRate.IsSpecialRate = roomRate.IsSpecialRate;
                            hotelRoomRate.RoomtypeCode = roomRate.RoomTypeCode;
                            hotelRoomRate.Rate = roomRate.BaseRate;
                            hotelRoomRate.TotalRate = roomRate.TotalRate;
                            if (null != roomRate.PointsDetails)
                            {
                                hotelRoomRate.AwardType = roomRate.PointsDetails.AwardType;
                                hotelRoomRate.Points = roomRate.PointsDetails.PointsRequired;
                            }
                            break;
                        }
                    }
                }
            }
            return hotelRoomRate;
        }

        /// <summary>
        /// This will find out if the guarantee information need to be displayed or not.
        /// Along with it finds if no rate code associated and credit card guarantee type text.
        /// </summary>
        /// <param name="rateCategoryId">Rate category id if associated with this booking.</param>
        /// <param name="blockCode">Block code</param>
        /// <param name="noRateCodeButDisplayGuarantee">True if no rate code associated but display the guarantee information.</param>
        /// <param name="creditCardGuranteeType">Credit card guarantee type text</param>
        /// <returns>True if this is block code booking and if block code does not need the credit card information</returns>
        private bool IsTruelyPrepaid(string rateCategoryId, string blockCode,
                                     out bool noRateCodeButDisplayGuarantee, out string creditCardGuranteeType)
        {
            bool returnVaue = false;
            noRateCodeButDisplayGuarantee = false;
            creditCardGuranteeType = string.Empty;
            if (Utility.IsBlockCodeBooking)
            {
                if (rateCategoryId == AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
                {
                    Block blockPage = ContentDataAccess.GetBlockCodePages(blockCode);
                    if (null != blockPage)
                    {
                        if (blockPage.CaptureGuarantee)
                        {
                            noRateCodeButDisplayGuarantee = true;
                            if (!string.IsNullOrEmpty(blockPage.GuranteeType))
                            {
                                creditCardGuranteeType = blockPage.GuranteeType;
                            }
                        }
                        else
                        {
                            returnVaue = true;
                        }
                    }
                }
            }
            return returnVaue;
        }

    }
}