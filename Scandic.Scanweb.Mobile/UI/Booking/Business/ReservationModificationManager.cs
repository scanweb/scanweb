﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine;
using System.IO;
using System.Reflection;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using System.Globalization;

namespace Scandic.Scanweb.Mobile.UI.Booking.Business
{
    public class ReservationModificationManager
    {
        delegate void CreatePDF(List<EmailEntity> email, List<string> reservations);

        public void CancelBooking(ref CancelDetailsEntity cancelDetailsEntity, SearchType hotelSearchType)
        {
            if (cancelDetailsEntity != null)
            {
                ReservationController reservationCtrl = new ReservationController();
                reservationCtrl.CancelBooking(ref cancelDetailsEntity, (hotelSearchType == SearchType.REDEMPTION), true);
            }
        }

        public void SendConfimationEmailAndSMS(List<CancelDetailsEntity> cancelledEntities)
        {
            //Send Email Confirmation to the User
            SendMailConfirmationToGuestsUsingHTMLString(cancelledEntities);
            //Send the SMS
            SentSMSCancellationConfirmation(cancelledEntities);
        }

        /// <summary>
        /// Send SMS Cancellation Confirmation. This method is excact copy of method in 
        /// CancelBookingConfirmation.ascx.cs in Scanweb, it needs some session variables to be set
        /// which should be done before calling this method.
        /// </summary>
        private void SentSMSCancellationConfirmation(List<CancelDetailsEntity> cancelList)
        {
            if (cancelList != null && cancelList.Count > 0)
            {
                int totalCancel = cancelList.Count;
                for (int count = 0; count < totalCancel; count++)
                {
                    // AMS Bug Fix | artf727168 | HD031379 | ORS/Fidelio - Reservations made on scandichotels.com multiply on the way down to Fidelio.
                    // try..catch block added inside the for loop, as to continue sending SMS when any exception raised while sending SMS for a particular leg.
                    CancelDetailsEntity cancelEntity = null;

                    try
                    {
                        cancelEntity = cancelList[count];
                        if ((cancelEntity != null) && (!string.IsNullOrEmpty(cancelEntity.SMSNumber)) && (cancelEntity.CancelationStatus))
                        {
                            BookingDetailsEntity bookingEntity = Utility.GetBookingDetail(cancelEntity.LegNumber);
                            if (bookingEntity != null)
                            {
                                Dictionary<string, string> smsCancelBookingMap = CollectSMSBookingCancellationDetails(bookingEntity);
                                smsCancelBookingMap[CommunicationTemplateConstants.BOOKING_CANCALLATION_NUMBER] = cancelEntity.CancelNumber;
                                SMSEntity smsEntity = CommunicationUtility.GetBookingCancellationSMS(smsCancelBookingMap);
                                if (smsEntity != null)
                                {
                                    smsEntity.PhoneNumber = cancelEntity.SMSNumber;
                                    CommunicationService.SendSMS(smsEntity);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // AMS Bug Fix | artf727168 | HD031379 | ORS/Fidelio - Reservations made on scandichotels.com multiply on the way down to Fidelio.
                        AppLogger.LogFatalException(ex, "Failed to send cancel confirmation SMS to the Guest for booking: " + cancelEntity.ReservationNumber + AppConstants.HYPHEN + cancelEntity.LegNumber);
                    }
                }
            }
        }

        /// <summary>
        /// Collect the SMS Booking Cancellation Details to be displayed in the SMS Template.
        /// This method is exact copy of method written in
        /// CancelBookingConfirmation.ascx.cs in scanweb.
        /// </summary>
        /// <param name="bookingDetails">
        /// Dictionary of SMS Details
        /// </param>
        private Dictionary<string, string> CollectSMSBookingCancellationDetails(BookingDetailsEntity bookingDetails)
        {
            Dictionary<string, string> cancelBookingMap = null;
            if (bookingDetails != null)
            {
                //Get the Dictionary
                cancelBookingMap = new Dictionary<string, string>();
                //Set the Cancellation Number
                //cancelBookingMap[CommunicationTemplateConstants.BOOKING_CANCALLATION_NUMBER] =
                //    bookingDetails.ReservationNumber + AppConstants.HYPHEN + bookingDetails.LegNumber;

                GuestInformationEntity guestInfo = bookingDetails.GuestInformation;
                if (guestInfo != null)
                {
                    //Naresh : SiteExpansion - Added as part of title for name.
                    cancelBookingMap[CommunicationTemplateConstants.TITLE] = Utility.GetNameTitle(guestInfo.Title).Trim();
                    //
                    cancelBookingMap[CommunicationTemplateConstants.FIRST_NAME] =
                        (string.IsNullOrEmpty(guestInfo.FirstName)) ? string.Empty : guestInfo.FirstName;
                    cancelBookingMap[CommunicationTemplateConstants.LASTNAME] =
                        (string.IsNullOrEmpty(guestInfo.LastName)) ? string.Empty : guestInfo.LastName;
                }

                HotelSearchEntity hotelSearch = bookingDetails.HotelSearch;
                if (hotelSearch != null)
                {
                    AvailabilityController availabilityController = new AvailabilityController();
                    HotelDestination hotelDestination =
                       availabilityController.GetHotelDestinationEntity(hotelSearch.SelectedHotelCode);
                    cancelBookingMap[CommunicationTemplateConstants.CITY_HOTEL] =
                        (hotelDestination != null) ? (hotelDestination.Name) : string.Empty;
                    cancelBookingMap[CommunicationTemplateConstants.HOTELADDRESS] =
                        (hotelDestination != null) ? (hotelDestination.HotelAddress.StreetAddress) : string.Empty;
                    cancelBookingMap[CommunicationTemplateConstants.HOTELTELEPHONE] =
                        (hotelDestination != null) ? (hotelDestination.Telephone) : string.Empty;

                    ///Defect Fix -  Artifact artf634543 : UAT/Translation/SMS confirmation.
                    /// Date string will be in english and not culture specific.
                    IFormatProvider cultureInfo = new CultureInfo("en-US");
                    cancelBookingMap[CommunicationTemplateConstants.ARRIVAL_DATE] = hotelSearch.ArrivalDate.ToString("dd MMM yy", cultureInfo);
                    cancelBookingMap[CommunicationTemplateConstants.DEPARTURE_DATE] = hotelSearch.DepartureDate.ToString("dd MMM yy", cultureInfo);
                }
            }
            return cancelBookingMap;
        }


        /// <summary>
        /// Sends the mail confirmation to guests using HTML string. This method is excact copy of method in 
        /// CancelBookingConfirmation.ascx.cs in Scanweb, it needs some session variables to be set
        /// which should be done before calling this method.
        /// </summary>
        /// <param name="cancelList">The cancel list.</param>
        private void SendMailConfirmationToGuestsUsingHTMLString(List<CancelDetailsEntity> cancelList)
        {
            string bookingType = PageIdentifier.CancelBookingPageIdentifier;
            ErrorsSessionWrapper.ClearFailedEmailRecipient();
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
            
            string templateFileName = string.Empty;
            if (cancelList != null)
            {

                //Res 2.0 - Parvathi : Cancelled Booking CR
                bool isSentToMainGuest = false;
                List<EmailEntity> emails = new List<EmailEntity>();
                List<string> reservations = new List<string>();
                for (int count = 0; count < cancelList.Count; count++)
                {
                    CancelDetailsEntity cancelEntity = cancelList[count];
                    int currentLegNumber = Convert.ToInt32(cancelEntity.LegNumber) - 1;
                    string htmlStringForEmail = string.Empty;
                    string htmlStringforPDF = string.Empty;
                    EmailEntity emailObject = null;
                    string cancelNumber = string.Empty;
                    

                    //Res 2.0 - Parvathi : Cancelled Booking CR
                    // R2.0 Bhavya - artf1162324 : The informative text is missing in the mail sent to master when a leg reservation is cancelled. 
                    //R2.0 - Bhavya - To generate PDF and email for Master Booking
                    if (!isSentToMainGuest)
                    {
                        GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/ConfirmationEmailPage.aspx?AllROOMSTOBESHOWN=true&&command=CancelPageprint&BOOKINGTYPE=" + bookingType, ref htmlStringForEmail);
                        //GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/ModifyCancelBooking/CancellBookingPrinterFreindly.aspx?AllROOMSTOBESHOWN=true&isPDF=true&command=CancelPageprint", ref htmlStringforPDF);
                        //Res 2.2.9 - Patch6 : artf1208955 : Bucket - Pdf does not look good. 
                        GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/ModifyCancelBooking/CancellBookingPrinterFreindly.aspx?isCancel=true&AllROOMSTOBESHOWN=true&isPDF=false&command=CancelPageprint", ref htmlStringforPDF);

                        if (string.IsNullOrEmpty(htmlStringforPDF))
                        {
                            //GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/ModifyCancelBooking/CancellBookingPrinterFreindly.aspx?AllROOMSTOBESHOWN=true&isPDF=true&command=CancelPageprint", ref htmlStringforPDF);
                            //Res 2.2.9 - Patch6 : artf1208955 : Bucket - Pdf does not look good. 
                            GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/ModifyCancelBooking/CancellBookingPrinterFreindly.aspx?isCancel=true&AllROOMSTOBESHOWN=true&isPDF=false&command=CancelPageprint", ref htmlStringforPDF);
                        }
                        templateFileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
                        templateFileName = templateFileName.Remove(templateFileName.IndexOf("\\bin"));


                        string alterhtmlStringForPDF = htmlStringforPDF.Replace("/Templates", templateFileName + "/Templates");

                        SendMailForMainGuest("1", htmlStringForEmail + "^" + alterhtmlStringForPDF, ref emailObject, ref cancelNumber);
                        emails.Add(emailObject);
                        reservations.Add(cancelNumber);
                        isSentToMainGuest = true;
                    }
                    //R2.0 - Bhavya - to generate PDF and email for leg Booking
                    if (cancelList[count].LegNumber != 1.ToString())
                    {
                        htmlStringforPDF = string.Empty;
                        string alterhtmlStringForLegPDF = string.Empty;
                        GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/ConfirmationEmailPage.aspx?AllROOMSTOBESHOWN=false&&command=CancelPageprint&ROOMNUMBERTOBEDISPLAYED=" + currentLegNumber + "&BOOKINGTYPE=" + bookingType, ref htmlStringForEmail);
                        //GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/ModifyCancelBooking/CancellBookingPrinterFreindly.aspx?AllROOMSTOBESHOWN=false&isPDF=true&command=CancelPageprint&count=" + count + "&CurrentLegNumber=" + currentLegNumber, ref htmlStringforPDF);
                        GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/ModifyCancelBooking/CancellBookingPrinterFreindly.aspx?isCancel=true&AllROOMSTOBESHOWN=false&isPDF=false&command=CancelPageprint&count=" + count + "&CurrentLegNumber=" + currentLegNumber, ref htmlStringforPDF);
                        if (string.IsNullOrEmpty(htmlStringforPDF))
                        {
                            //GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/ModifyCancelBooking/CancellBookingPrinterFreindly.aspx?AllROOMSTOBESHOWN=false&isPDF=true&command=CancelPageprint&count=" + count + "&CurrentLegNumber=" + currentLegNumber, ref htmlStringforPDF);
                            //Res 2.2.9 - Patch6 : artf1208955 : Bucket - Pdf does not look good. 
                            GenerateConfirmationPageHtmlString(@"/templates/Scanweb/Pages/ModifyCancelBooking/CancellBookingPrinterFreindly.aspx?isCancel=true&AllROOMSTOBESHOWN=false&isPDF=false&command=CancelPageprint&count=" + count + "&CurrentLegNumber=" + currentLegNumber, ref htmlStringforPDF);
                        }
                        templateFileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
                        templateFileName = templateFileName.Remove(templateFileName.IndexOf("\\bin"));


                        alterhtmlStringForLegPDF = htmlStringforPDF.Replace("/Templates", templateFileName + "/Templates");

                        SendMailForEachCancelledRoom(ref emailObject, count, cancelList, htmlStringForEmail + "^" + alterhtmlStringForLegPDF, ref cancelNumber);
                        emails.Add(emailObject);
                        reservations.Add(cancelNumber);
                    }
                }
                SendMail(emails, reservations);
            }

        }

        /// <summary>
        /// Generates the confirmation page HTML string. This method is exact copy of method written in
        /// CancelBookingConfirmation.ascx.cs in scanweb.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="htmlString">The HTML string.</param>
        private void GenerateConfirmationPageHtmlString(string url, ref string htmlString)
        {
            #region Very IMP code To render the preview page

            try
            {
                StringWriter sw = new StringWriter();
                HttpContext.Current.Server.Execute(url, sw);
                htmlString = (sw.ToString()).Trim().Replace("\r\n", "").Replace("\"", "'");

            }
            catch (Exception ex)
            {

            }

            #endregion
        }
        /// <summary>
        /// Sends the mail for each cancelled room. This method is exact copy of method written in
        /// CancelBookingConfirmation.ascx.cs in scanweb.
        /// </summary>
        /// <param name="count">The count.</param>
        //Res 2.0 - Parvathi : Cancelled Booking CR
        private void SendMailForMainGuest(string legNumber, string htmlStringForEmail, ref EmailEntity emailObject, ref string cancelNumber)
        {
            //string htmlStringForEmail = string.Empty;
            string[] emailTextAndPDF = null;
            BookingDetailsEntity bookingEntity = Utility.GetBookingDetail(legNumber);

            //Fect booking details entity in case of leg cancellation
            if (bookingEntity == null)
            {
                if (BookingEngineSessionWrapper.IsModifyingLegBooking)
                {
                    ReservationController reservationController = new ReservationController();
                    bookingEntity = reservationController.FetchBooking(BookingEngineSessionWrapper.AllBookingDetails[0].ReservationNumber, legNumber);
                }
            }
            try
            {
                //Create the Email List
                if (bookingEntity != null)
                {
                    //CommunicationUtility.GetCancelBookingConfirmationEmail(emailBookingMap);
                    EmailEntity emailEntity = new EmailEntity();
                    if (emailEntity != null)
                    {
                        GuestInformationEntity guestInfoEntity = bookingEntity.GuestInformation;
                        if (guestInfoEntity != null && guestInfoEntity.EmailDetails != null)
                        {
                            string[] emailList = null;
                            if (guestInfoEntity.IsRewardNightGift && bookingEntity.RewardNightGiftGuestInfo != null)
                            {
                                emailList = new string[] { guestInfoEntity.EmailDetails.EmailID, bookingEntity.RewardNightGiftGuestInfo.EmailAddressFgpUser };
                            }
                            else
                            {
                                emailList = new string[] { guestInfoEntity.EmailDetails.EmailID };
                            }
                            emailEntity.Recipient = emailList;
                            emailEntity.Subject = WebUtil.GetTranslatedText("/bookingengine/booking/cancelconfirmation/subject");
                            emailEntity.Sender = WebUtil.GetTranslatedText("/bookingengine/booking/cancelconfirmation/fromEmail");
                            //htmlStringForEmail = string.Empty;
                            string reservationNumberForCancel = bookingEntity.ReservationNumber + AppConstants.HYPHEN + bookingEntity.LegNumber;
                            //Ranajit:added becuse we have diffrent text for PDF and Email
                            emailTextAndPDF = htmlStringForEmail.Split('^');
                            //emailEntity.Body = htmlStringForEmail;
                            if (emailTextAndPDF != null)
                            {
                                if (emailTextAndPDF[0] != null)
                                    emailEntity.Body = emailTextAndPDF[0];
                                //emailEntity.TextEmailBody = htmlStringForEmail;
                                if (emailTextAndPDF[1] != null)
                                    emailEntity.TextEmailBody = emailTextAndPDF[1];
                            }                            
                            emailObject = emailEntity;
                            cancelNumber = reservationNumberForCancel;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                // AMS Bug Fix | artf727168 | HD031379 | ORS/Fidelio - Reservations made on scandichotels.com multiply on the way down to Fidelio.
                AppLogger.LogFatalException(ex, "Failed to send cancel confirmation email to the Guest for booking: " + bookingEntity.ReservationNumber + AppConstants.HYPHEN + bookingEntity.LegNumber);
            }

        }
        /// <summary>
        /// Sends the mail for each cancelled room.This method is exact copy of method written in
        /// CancelBookingConfirmation.ascx.cs in scanweb.
        /// </summary>
        /// <param name="count">The count.</param>
        private void SendMailForEachCancelledRoom(ref EmailEntity emailObject, int count, List<CancelDetailsEntity> cancelList, string htmlStringForEmail, ref string cancelNumber)
        {
            // AMS Bug Fix | artf727168 | HD031379 | ORS/Fidelio - Reservations made on scandichotels.com multiply on the way down to Fidelio.
            // try..catch block added inside the for loop, as to continue sending email when any exception raised while sending email for a particular leg.
            CancelDetailsEntity cancelEntity = null;
            //string htmlStringForEmail = string.Empty;
            string[] emailTextAndPDF = null;
            try
            {
                cancelEntity = cancelList[count];

                if (cancelEntity != null)
                {
                    //Check if the Cancellation was sucessful
                    if (cancelEntity.CancelationStatus)
                    {
                        //Get the Booking Details corresponding to the Booking and Leg Number
                        BookingDetailsEntity bookingEntity = Utility.GetBookingDetail(cancelEntity.LegNumber);
                        //Create the Email List
                        if (bookingEntity != null)
                        {
                            //CommunicationUtility.GetCancelBookingConfirmationEmail(emailBookingMap);
                            EmailEntity emailEntity = new EmailEntity();
                            if (emailEntity != null)
                            {
                                GuestInformationEntity guestInfoEntity = bookingEntity.GuestInformation;
                                if (guestInfoEntity != null && guestInfoEntity.EmailDetails != null)
                                {
                                    string[] emailList = null;
                                    if (guestInfoEntity.IsRewardNightGift && bookingEntity.RewardNightGiftGuestInfo != null)
                                    {
                                        emailList = new string[] { guestInfoEntity.EmailDetails.EmailID, bookingEntity.RewardNightGiftGuestInfo.EmailAddressFgpUser };
                                    }
                                    else
                                    {
                                        emailList = new string[] { guestInfoEntity.EmailDetails.EmailID };
                                    }
                                    emailEntity.Recipient = emailList;
                                    emailEntity.Subject = WebUtil.GetTranslatedText("/bookingengine/booking/cancelconfirmation/subject");
                                    emailEntity.Sender = WebUtil.GetTranslatedText("/bookingengine/booking/cancelconfirmation/fromEmail");
                                    string reservationNumberForCancel = bookingEntity.ReservationNumber + AppConstants.HYPHEN + bookingEntity.LegNumber;
                                    //Ranajit:added becuse we have diffrent text for PDF and Email
                                    emailTextAndPDF = htmlStringForEmail.Split('^');
                                    if (emailTextAndPDF != null)
                                    {
                                        if (emailTextAndPDF[0] != null)
                                            emailEntity.Body = emailTextAndPDF[0];
                                        if (emailTextAndPDF[1] != null)
                                            emailEntity.TextEmailBody = emailTextAndPDF[1];
                                    }
                                    emailObject = emailEntity;
                                    cancelNumber = reservationNumberForCancel;
                                }
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                // AMS Bug Fix | artf727168 | HD031379 | ORS/Fidelio - Reservations made on scandichotels.com multiply on the way down to Fidelio.
                AppLogger.LogFatalException(ex, "Failed to send cancel confirmation email to the Guest for booking: " + cancelEntity.ReservationNumber + AppConstants.HYPHEN + cancelEntity.LegNumber);
            }

        }

        /// <summary>
        /// This method will create PDF files asynchronously.
        /// </summary>
        /// <param name="emails">emails list</param>
        /// <param name="reservations">reservations list</param>
        public void SendMail(List<EmailEntity> emails, List<string> reservations)
        {
            AppLogger.LogInfoMessage("SendMail() Start Time");
            CreatePDF createPDFDeligate = new CreatePDF(SendEmailAndCreatePDF);

            // invoke the method asynchronously
            IAsyncResult result = createPDFDeligate.BeginInvoke(emails, reservations, null, null);

            // get the result of that asynchronous operation
            //string retValue = createPDFDeligate.EndInvoke(result);
            AppLogger.LogInfoMessage("SendMail() End Time");
            
        }

        /// <summary>
        /// SendEmail And CreatePDF for all Guest.
        /// </summary>
        /// <param name="emails">emails list</param>
        /// <param name="reservations">reservations list</param>
        /// <returns></returns>
        private void SendEmailAndCreatePDF(List<EmailEntity> emails, List<string> reservations)
        {
            AppLogger.LogInfoMessage("SendEmailAndCreatePDF() Start Time");
            int counter = 0;
            foreach (EmailEntity email in emails)
            {
                //Res 2.2.8 - artf1227357 : Scanweb - PDF confirmations are too big 
                bool sendStatus = CommunicationService.SendMail(email,
                                    reservations[counter]);

                if (!sendStatus)
                {
                    if (email.Recipient != null)
                    {
                        if (email.Recipient.Length > 0)
                        {
                            for (int i = 0; i < email.Recipient.Length; i++)
                            {
                                ErrorsSessionWrapper.AddFailedEmailRecipient(email.Recipient[i].ToString());
                            }
                        }
                    }}
                counter++;
            }
            AppLogger.LogInfoMessage("SendEmailAndCreatePDF() End Time");
        }
    }
}
