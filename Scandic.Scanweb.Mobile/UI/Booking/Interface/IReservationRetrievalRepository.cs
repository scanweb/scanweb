﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;

namespace Scandic.Scanweb.Mobile.UI.Booking.Interface
{
    public interface IReservationRetrievalRepository
    {
        ReservationDetails FindBooking(string reservationNumber, string lastName, bool redirectOnError);

        List<UserReservation> GetFutureBookings(string nameId, int currentlyDisplayedBookings,
                                            int noOfBookingsToDisplay, BookingSortType sortType,
                                            SortOrder sortOrder, bool fetchLatestBookings,
                                            out bool moreResultsToFetch);
       
        UserTransactions GetUserHistory(string nameId, int currentlyDisplayedTransactions,
                                        int noOfTransactionsToDisplay, TranscationSortType sortType,
                                        SortOrder sortOrder, bool fetchLatestTransaction);
    }

    public enum BookingSortType
    {
        HotelName = 0,
        CheckInDate = 1,
        CheckOutDate = 2
    }

    public enum TranscationSortType
    {
        Transaction = 0,
        CheckInDate = 1,
        Points = 2
    }

    public enum SortOrder
    {
        Ascending = 0,
        Descending = 1
    }
}
