﻿//  Description					:   ISiteInfoRepository                                   //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using System.Globalization;
using EPiServer.Core;
using Scandic.Scanweb.Mobile.UI.Entity;

namespace Scandic.Scanweb.Mobile.UI.Booking.Interface
{
    /// <summary>
    /// This repository define the interfaces for common infromation that is required by
    /// mobile web site.
    /// </summary>
    public interface ISiteInfoRepository
    {
        /// <summary>
        /// This method returns the configuration requested page for specified language.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="pageId"></param>
        /// <returns></returns>
        T GetPageConfig<T>(string language);

        /// <summary>
        /// Gets general configuration.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetGeneralConfig<T>();

        /// <summary>
        /// Gets search destination list.
        /// </summary>
        /// <returns></returns>
        List<SearchDestination> GetSearchDestinationList();

        /// <summary>
        /// Gets mobile site page data.
        /// </summary>
        /// <returns></returns>
        List<MobilePageData> GetMobileSitePageData();

        /// <summary>
        /// Gets CMS page data
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        PageData GetCMSPageData(MobilePages pageId);

        /// <summary>
        /// Gets CMS page data
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        PageData GetCMSPageData(string pageId);

        /// <summary>
        /// Gets CMS page reference from root page
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        PageReference GetCMSPageReferenceFromRootPage(string pageId);

        /// <summary>
        /// Gets CMS page property
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="propertyId"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        string GetCMSPageProperty(string pageId, string propertyId, string language);

        /// <summary>
        /// Gets page url
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        string GetPageUrl(MobilePages pageId);

        /// <summary>
        /// Gets page url.
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        string GetPageUrl(MobilePages pageId, string language);

        /// <summary>
        /// Gets page url.
        /// </summary>
        /// <param name="pageData"></param>
        /// <param name="language"></param>
        /// <param name="isScanwebPage"></param>
        /// <returns></returns>
        string GetPageUrl(PageData pageData, string language, bool isScanwebPage);

        /// <summary>
        /// Checks whether the user agent is IsIOS5
        /// </summary>
        /// <param name="userAgent"></param>
        /// <returns></returns>
        bool IsIOS5(string userAgent);

        /// <summary>
        /// Checks whether the user agent is IsIEMobile
        /// </summary>
        /// <param name="userAgent"></param>
        /// <returns></returns>
        bool IsIEMobile(string userAgent);

        /// <summary>
        /// Gets country list
        /// </summary>
        /// <returns></returns>
        List<KeyValueOption> GetCountryList();

        /// <summary>
        /// Gets phones codes
        /// </summary>
        /// <returns></returns>
        List<KeyValueOption> GetPhoneCodes();

        /// <summary>
        /// Gets credit card types
        /// </summary>
        /// <returns></returns>
        List<KeyValueOption> GetCreditCardTypes();

        /// <summary>
        /// Gets month list
        /// </summary>
        /// <returns></returns>
        List<KeyValueOption> GetMonthList();

        /// <summary>
        /// Gets credit card valid years
        /// </summary>
        /// <returns></returns>
        List<KeyValueOption> GetCreditCardValidYears();

        CultureInfo GetCurrentCultureInfo();

        /// <summary>
        /// Get the page data by CMS page id
        /// </summary>
        /// <param name="offerPageID"></param>
        /// <returns></returns>
        PageData GetPageDataByPageID(int offerPageID);
    }

    /// <summary>
    /// Holds different mobile pages
    /// </summary>
    public enum MobilePages
    {
        None = -1,
        Start = 0,
        Search = 1,
        SelectHotel = 2,
        SelectRate = 3,
        BookingDetails = 4,
        BookingConfirmation = 5,
        SignIn = 6,
        Error = 7,
        TermsCondition = 8,
        PriceInformation = 9,
        ContactUs = 10,
        Feedback = 11,
        ViewBookings = 12,
        MyBookings = 13,
        ModifyCancel = 14,
        CancelConfirmation = 15,
        SearchNearby = 16,
        AllOffers = 17,
        OfferDetail = 18,
    }
}