﻿using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Entity;

namespace Scandic.Scanweb.Mobile.UI.Booking.Interface
{
    /// <summary>
    /// IUserInfoRespository
    /// </summary>
    public interface IUserInfoRespository
    {
        bool IsUserAuthenticated { get; }
        bool SignIn(LogonContext logonContext);
        LoyaltyDetailsEntity GetLoyaltyDetails();
        void RefreshLoyaltyDetails();
        UserProfileEntity GetProfile();
        void SignOut();
        bool IsCreditCardSaveAllowed();
        string SaveCreditCard();
    }
}