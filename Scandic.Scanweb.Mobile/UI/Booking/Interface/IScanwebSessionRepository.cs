﻿namespace Scandic.Scanweb.Mobile.UI.Booking.Interface
{
    /// <summary>
    /// IScanwebSessionRepository
    /// </summary>
    public interface IScanwebSessionRepository
    {
        bool IsAltCityHotelsSearchDone();

        /// <summary>
        /// Gets/Sets PaymentTransactionId
        /// </summary>
        string PaymentTransactionId { set; get; }
    }
}