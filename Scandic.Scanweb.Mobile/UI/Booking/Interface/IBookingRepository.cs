﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Model;

namespace Scandic.Scanweb.Mobile.UI.Booking.Interface
{
    /// <summary>
    /// IBookingRepository
    /// </summary>
    public interface IBookingRepository
    {
        HotelResults GetAvailableHotels(int currentlyDisplayedHotels, int nextHotelsToDisplayed,
                                        DestinationSortType sortBy);

        RoomTypeResults GetRoomTypes(string hotelId, bool isHotelSelctedByUser, bool selectedHotelHasOnlyPublicRates);
        RateOverlayToolTip GetRateTypeToolTip(string rateCategoryId, bool isBlockCode);
        String StartBookingProcess(BookingProcess bookingProcess);
        BookingContext CurrentContext { get; }
        bool IsPageInOrder(BookingPage requestBookingPageId);
        void AvailabilitySearch(SearchHotelModel searchInput);
        HotelDestination GetSelectedHotel();
        IEnumerable ProcessSelectedRoomRate(string roomCategoryId, string rateCategoryId);
        IEnumerable MakeHotelReservation(BookingDetailModel bookingDetailEntity, PaymentInfo paymentInfo, bool isRedirectToConfirmationPage,
            bool isModifyBookingAfterNetsPaymentSuccessful);
        string GetTotalPrice(int roomNumber);
        string Register(string panHash);
        string GetNetsTerminalURLForMobile(string transactionId);
        string NetsAuthProcess(string transactionId);
        bool NetsCaptureProcess(string transactionId);
        PaymentInfo GetPaymentInfo(string transactionId);
        bool MakePayment(PaymentInfo paymentInfo);
        Collection<CreditCardEntity> FetchFGPCreditCards(string nameId);
        void ProcessErrorMessageForNetsErrorCode(string errorCode, string errorSource);
        string NetsAnnulProcess(string transactionId);
        bool GetPaymentFallback(string hotelOperaID);
        void ConfirmBooking(List<GuestInformationEntity> guestList);
        void IgnoreBooking();
        void ClearCurrentContext();
    }
}