﻿using System.Web;

namespace Scandic.Scanweb.Mobile.UI.Booking.Interface
{
    public interface IDeeplinkRepository
    {
        bool IsDeeplinkRequest(HttpRequest request, out string deeplinkPath);
    }
}