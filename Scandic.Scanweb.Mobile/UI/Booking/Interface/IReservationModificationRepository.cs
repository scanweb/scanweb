﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;

namespace Scandic.Scanweb.Mobile.UI.Booking.Interface
{
    public interface IReservationModificationRepository
    {
        List<CancelRoomDetails> CancelBooking(List<CancelRoomDetails> cancellationRooms);
    }
}
