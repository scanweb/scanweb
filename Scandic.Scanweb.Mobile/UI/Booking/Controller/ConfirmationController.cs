﻿using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Booking.Repository;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Controls;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    /// <summary>
    /// ConfirmationController
    /// </summary>
    public class ConfirmationController : BaseController
    {
        private IUserInfoRespository userRepository;
        private const string BOOKING_CONFIRMATION_PAGE_NAME = "Booking confirmation";
        private const string CMS_POLICY_KEY_PART = "CancellationPolicyText";
        private const string CMS_REWARD_NIGHT_POLICY_KEY = "RewardNightCancelPolicyHTML";
        private const string WITH_CREDIT_CARD = "WithCreditcard";
        private const string WITHOUT_CREDIT_CARD = "WithoutCreditcard";
        private const string PREPAID = "Prepaid";

        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public ConfirmationController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in ConfirmationController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public ConfirmationController(string requestLanguage)
            : base(requestLanguage)
        {
            userRepository = DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
        }

        #endregion

        #region Public Properties

        public bool IsAuthenticated
        {
            get { return userRepository.IsUserAuthenticated; }
        }

        public HotelDestination SelectedHotel
        {
            get { return bookingRepository.GetSelectedHotel(); }
        }

        #endregion

        #region public Methods

        /// <summary>
        /// GetGuranteeCancellationText
        /// </summary>
        /// <param name="policyCode"></param>
        /// <returns>GuranteeCancellationText</returns>
        public string GetGuranteeCancellationText(string policyCode)
        {
            string policyText = string.Empty;

            var emailConfirmationContainer =
                siteRepository.GetCMSPageReferenceFromRootPage(Reference.EmailConfirmationContainerCMSReference);
            var emailConfirmationPages = DataFactory.Instance.GetChildren(emailConfirmationContainer);
            PageData selectPage = null;
            foreach (var page in emailConfirmationPages)
            {
                if (string.Equals(page.PageName, BOOKING_CONFIRMATION_PAGE_NAME,
                                  StringComparison.InvariantCultureIgnoreCase))
                {
                    selectPage = page;
                }
            }

            if (selectPage != null)
            {
                string policyKey = string.Empty;
                if (string.Equals(policyCode, Reference.GuranteedTextCodeBefore6PM,
                                  StringComparison.InvariantCultureIgnoreCase))
                {
                    policyKey = string.Format("{0}{1}{2}", CMS_POLICY_KEY_PART, WITHOUT_CREDIT_CARD,
                                              Reference.GuranteedTextCodeAfter6PM);
                }
                else if (string.Equals(policyCode, Reference.GuranteedTextCodeRewardNights,
                                       StringComparison.InvariantCultureIgnoreCase))
                {
                    policyKey = CMS_REWARD_NIGHT_POLICY_KEY;
                }
                else if (string.Equals(policyCode, Reference.GuranteedTextCodeBlockCode,
                                       StringComparison.InvariantCultureIgnoreCase))
                {
                    policyKey = string.Format("{0}{1}", CMS_POLICY_KEY_PART, policyCode);
                }
                else if (string.Equals(policyCode, Reference.GuranteedTextPrepaidInProgress,
                                       StringComparison.InvariantCultureIgnoreCase))
                {
                    policyKey = string.Format("{0}{1}", CMS_POLICY_KEY_PART, PREPAID);
                }
                else
                {
                    policyKey = string.Format("{0}{1}{2}", CMS_POLICY_KEY_PART, WITH_CREDIT_CARD, policyCode);
                }

                policyText = selectPage[policyKey] as string;
            }
            return policyText;
        }

        /// <summary>
        /// BookAnotherRoom
        /// </summary>
        public void BookAnotherRoom()
        {
            BookingProcess bookingProcess = BookingProcess.BookARoom;
            if (CurrentContext != null)
            {
                bookingProcess = CurrentContext.CurrentBookingProcess;
            }

            Redirect(bookingRepository.StartBookingProcess(bookingProcess));
        }

        /// <summary>
        /// Gets save credit card overlay HTML.
        /// </summary>
        /// <returns></returns>
        public string GetSaveCreditCardOverlay()
        {
            string htmlString = string.Empty; 
            if (userRepository.IsCreditCardSaveAllowed())
            {
                htmlString = string.Empty;
                var pageConfig = GetPageConfig<ConfirmationPageSection>();

                var divSaveCreditCard = new HtmlGenericControl("div");

                var pSaveCreditCardDescription = new HtmlGenericControl("p");
                pSaveCreditCardDescription.InnerHtml =
                    pageConfig.PageDetail.PageMessages.GetMessage("saveCreditCardOverlayDescription");
                divSaveCreditCard.Controls.Add(pSaveCreditCardDescription);

                var pCenter = new HtmlGenericControl("p");
                pCenter.Attributes.Add("class", "center");
                divSaveCreditCard.Controls.Add(pCenter);

                var lnkNoThanks = new HtmlAnchor();
                lnkNoThanks.HRef = "#";
                lnkNoThanks.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage("noThanksLinkText");
                lnkNoThanks.Attributes.Add("onclick", "return closeOverlay();");
                pCenter.Controls.Add(lnkNoThanks);

                var lnkSaveCreditCard = new HtmlAnchor();
                lnkSaveCreditCard.HRef = "#";
                lnkSaveCreditCard.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage("saveCreditCardButtonText");
                lnkSaveCreditCard.Attributes.Add("class", "button green");
                lnkSaveCreditCard.Attributes.Add("onclick", "return SaveCreditCardInformation();");
                pCenter.Controls.Add(lnkSaveCreditCard);

                var overlayData = new OverlayDetails();
                overlayData.Heading = pageConfig.PageDetail.PageMessages.GetMessage("saveCreditCardOverlayHeading");
                overlayData.Id = "divSaveCreditCardOverlay";
                overlayData.IsCachable = true;
                overlayData.Items = new List<OverlayItem>();
                var overlay = new Overlay(OverlayControlType.Value, overlayData, overlayData.Id);
                htmlString = overlay.GetControlHtml(divSaveCreditCard);
            }
            return htmlString;
        }

        /// <summary>
        /// Saves credit card information to user profile.
        /// </summary>
        /// <returns></returns>
        public string SaveCreditCard()
        {
            string saveCreditCardResult =  userRepository.SaveCreditCard();
            string responseHTML = string.Empty;
            var pageConfig = GetPageConfig<ConfirmationPageSection>();
            var divSaveCreditCardResponse = new HtmlGenericControl("div");
            var pSaveCreditCardResponseDescription = new HtmlGenericControl("p");
            divSaveCreditCardResponse.Controls.Add(pSaveCreditCardResponseDescription);
            var pCenter = new HtmlGenericControl("p");
            pCenter.Attributes.Add("class", "center");
            divSaveCreditCardResponse.Controls.Add(pCenter);
            var lnkOkay = new HtmlAnchor();
            lnkOkay.HRef = "#";
            lnkOkay.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage("closeButtonText");
            lnkOkay.Attributes.Add("class", "button green");
            lnkOkay.Attributes.Add("onclick", "return closeOverlay();");
            pCenter.Controls.Add(lnkOkay);
            var overlayData = new OverlayDetails();
            if (saveCreditCardResult.Equals(SessionBookingConstants.SAVE_CREDITCARD_RESPONSECODE, StringComparison.InvariantCultureIgnoreCase))
            {
                pSaveCreditCardResponseDescription.InnerHtml =
                pageConfig.PageDetail.PageMessages.GetMessage("saveCreditCardSuccessMsg");
                overlayData.Heading = pageConfig.PageDetail.PageMessages.GetMessage("saveCreditCardSuccessResponseOverlayHeading");
            }
            else
            {
                pSaveCreditCardResponseDescription.InnerHtml =
                pageConfig.PageDetail.PageMessages.GetMessage("saveCreditCardFailureMsg");
                overlayData.Heading = pageConfig.PageDetail.PageMessages.GetMessage("saveCreditCardFailureResponseOverlayHeading");
            }
            overlayData.Id = "divSaveCreditCardResponseOverlay";
            overlayData.IsCachable = true;
            overlayData.Items = new List<OverlayItem>();
            var overlay = new Overlay(OverlayControlType.Value, overlayData, overlayData.Id);
            return overlay.GetControlHtml(divSaveCreditCardResponse);
        }
        #endregion
    }
}