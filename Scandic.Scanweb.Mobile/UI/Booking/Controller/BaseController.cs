﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Entity.Model;
using System.Text;
using System.Collections.Specialized;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    /// <summary>
    /// BaseController
    /// </summary>
    public class BaseController
    {
        #region Declaration

        protected string language = "";
        protected ISiteInfoRepository siteRepository;
        protected IBookingRepository bookingRepository;

        #endregion

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in StartController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public BaseController(string requestLanguage)
        {
            if (string.IsNullOrEmpty(requestLanguage))
            {
                language = LanguageRedirectionHelper.GetCurrentLanguage();
            }
            else
            {
                language = requestLanguage;
            }

            if (string.IsNullOrEmpty(language))
            {
                throw new ApplicationException("Local language not initialized.");
            }

            siteRepository = DependencyResolver.Instance.GetService(typeof(ISiteInfoRepository)) as ISiteInfoRepository;
            bookingRepository =
                DependencyResolver.Instance.GetService(typeof(IBookingRepository)) as IBookingRepository;
        }

        /// <summary>
        /// This method must be overriden by the invidual page controller classes 
        /// to check the validity of the corresponding page model.
        /// </summary>
        /// <param name="pageModel">This object correspond to all user inputs
        /// encapsulated in the respective pagemodel. </param>
        /// <param name="validationErrors">This list object will be populated with the 
        /// keyvalue pair list of page input id and validator error message in case validation
        /// fails.</param>
        /// <returns>This method will return true if all vaildations are passed.</returns>
        public virtual bool IsPageModelValid(BaseBookingModel pageModel, out List<KeyValueOption> validationErrors)
        {
            validationErrors = null;
            return true;
        }

        /// <summary>
        /// This method must be overriden by the invidual page controller classes 
        /// to check the validity of the corresponding content page model.
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="validationErrors"></param>
        /// <returns>True/False</returns>
        public virtual bool IsPageModelValid(BasePageModel pageModel, out List<KeyValueOption> validationErrors)
        {
            validationErrors = null;
            return true;
        }

        public BookingContext CurrentContext
        {
            get { return bookingRepository.CurrentContext; }
        }

        /// <summary>
        /// This method will attach the language specific host header before redirecting.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="endResponse"></param>
        public void Redirect(string url, bool isCompleteUrl, bool endResponse)
        {
            HttpContext.Current.Response.Redirect(url, endResponse);
        }

        /// <summary>
        /// This method will attach the language specific host header before redirecting.
        /// </summary>
        /// <param name="url"></param>
        public void Redirect(string url)
        {
            Redirect(url, false, false);
        }

        /// <summary>
        /// This method will not attach the language specific host header before redirecting,
        /// if isCompleteUrl is set to true.
        /// </summary>
        /// <param name="url"></param>
        public void Redirect(string url, bool isCompleteUrl)
        {
            Redirect(url, false, false);
        }
        /// <summary>
        /// It appends querstring Name Collection to the Url
        /// </summary>
        /// <param name="url"></param>
        /// <param name="querystrings"></param>
        public void Redirect(string url, NameValueCollection querystrings)
        {
            Redirect(GetFormatedUrl(url, querystrings));
        }

        public string GetFormatedUrl(string url, NameValueCollection querystrings)
        {
            StringBuilder redirectUrl = new StringBuilder(url);

            if (querystrings != null)
            {
                for (int index = 0; index < querystrings.Count; index++)
                {
                    if (index == 0)
                    {
                        redirectUrl.Append("?");
                    }

                    redirectUrl.Append(querystrings.Keys[index]);
                    redirectUrl.Append("=");
                    redirectUrl.Append(HttpUtility.UrlEncode(querystrings[index]));

                    if (index < querystrings.Count - 1)
                    {
                        redirectUrl.Append("&");
                    }
                }
            }
            return redirectUrl.ToString();
        }
        /// <summary>
        /// GetPageConfig
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>PageConfig</returns>
        public T GetPageConfig<T>()
        {
            return siteRepository.GetPageConfig<T>(language);
        }

        /// <summary>
        /// GetGeneralConfig
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>GeneralConfig</returns>
        public T GetGeneralConfig<T>()
        {
            return siteRepository.GetGeneralConfig<T>();
        }


        /// <summary>
        /// GetPageUrl
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns>PageUrl</returns>
        public string GetPageUrl(MobilePages pageId)
        {
            return siteRepository.GetPageUrl(pageId);
        }

        /// <summary>
        /// GetPageUrl
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="language"></param>
        /// <returns>PageUrl</returns>
        public string GetPageUrl(MobilePages pageId, string language)
        {
            return siteRepository.GetPageUrl(pageId, language);
        }

        /// <summary>
        /// IsIOS5
        /// </summary>
        /// <returns>IOS5</returns>
        public bool IsIOS5()
        {
            return siteRepository.IsIOS5(HttpContext.Current.Request.Headers["User-Agent"]);
        }

        /// <summary>
        /// GetServerHostName
        /// </summary>
        /// <param name="request"></param>
        /// <returns>ServerHostName</returns>
        public string GetServerHostName(HttpRequest request)
        {
            var appConfig = GetGeneralConfig<ApplicationConfigSection>();
            var serverNameRegex = appConfig.GetMessage(Reference.ServerHostRegx);
            return Regex.Match(request.Url.AbsoluteUri, serverNameRegex).Value;
        }


        public string Language
        {
            get { return language; }
        }
    }
}