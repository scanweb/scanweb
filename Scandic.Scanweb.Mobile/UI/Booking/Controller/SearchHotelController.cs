﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Core;
using System.Configuration;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    /// <summary>
    /// SearchHotelController
    /// </summary>
    public class SearchHotelController : BaseController
    {
        #region Declaration
        private const string VALIDATION_SUMMARY_ERR_CRTL = "hgcValidationSummaryheadingError";
        private const string VALIDATION_SUMMARY_ERR_MSG_KEY = "validationSummaryMsg";
        private const string REMEMBER_MY_BOOKING_CODE_COOKIE = "Mobile.BookingCode";
        private const string DESTINATION_ERR_MSG_KEY = "searchDestinationErrorMsg";
        private const string DESTINATION_RADIUS_ERR_MSG_KEY = "searchDestinationRadiusErrorMsg";
        private const string DESTINATION_ERR_CTRL = "hgcSearchError";
        private const string CHECKIN_DATE_ERR_MSG_KEY = "checkInDateErrorMsg";
        private const string CHECKIN_DATE_ERR_CTRL = "hgcDateOfArrivalError";
        private const string CHECKOUT_DATE_ERR_MSG_KEY = "checkOutDateErrorMsg";
        private const string INADULTBEDEXCEEDING_ERR_MSG_KEY = "hgcinadultbedexceedingError";
        private const string CHECKOUT_DATE_ERR_CTRL = "hgcDateOfDepartureError";
        private const string NO_OF_ADULT_ERR_MSG_KEY = "numberOfAdultErrorMsg";
        private const string NO_OF_ADULT_ERR_CTRL = "hgcNumberOfAdultError";
        private const string BOOKING_CODE_ERR_MSG_KEY = "bookingCodeErrorMsg";
        private const string BOOKING_CODE_ERR_CTRL = "hgcBookingCodeError";
        private const string BOOKING_CODE_REGX_KEY = "notAllowedBookingCodeRegx";
        private const string BOOKING_CODE_NOT_ALLOWED_ERR_MSG_KEY = "notAllowedBookingCodeErrorMsg";
        private const string CORPORATE_CODE_REX_KEY = "corporateCodeRegx";
        private const string NEAR_MY_POSITION = "nearMyPosition";
        const string AUTOCOMPLETE_LIST = "Mobile.AutocompleteList.";
        private const string AGE_ERR_MSG_KEY = "hgcAgeError";
        private const string ADULT_EXCEEDING_ERR_MSG = "inAdultbedExceedingErrorMsg";
        private const string BEDTYPE_IN_ADULT_BED = "bedtype_In_Adult_Bed";
        private const string AGE_ERR_MSG = "selectAgeErrorMessage";
        private const string INVALID_SOURCE_FOR_FAMILY_FRIENDS_DNUMBER = "InvalidSourceForFamilyAndFriendsDNumberErrorMsg";

        #endregion

        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public SearchHotelController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in MasterPageController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public SearchHotelController(string requestLanguage)
            : base(requestLanguage)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// GetPageData
        /// </summary>
        /// <returns>SearchHotelModel</returns>
        public SearchHotelModel GetPageData()
        {
            var bookingCode = GetBookingCode();
            CurrentContext.SearchHotelPage.BookingCode = bookingCode;
            if (!string.IsNullOrEmpty(bookingCode))
            {
                CurrentContext.SearchHotelPage.RememberBookingCode = true;
            }
            if (!CurrentContext.SearchHotelPage.CheckInDate.HasValue &&
                CurrentContext.CurrentBookingProcess != BookingProcess.ClaimRewardNights)
            {
                CurrentContext.SearchHotelPage.CheckInDate =
                    DateTime.ParseExact(DateTime.Now.ToString(Reference.DateFormat), Reference.DateFormat,
                                        CultureInfo.InvariantCulture);
            }

            if (!CurrentContext.SearchHotelPage.CheckOutDate.HasValue &&
                CurrentContext.CurrentBookingProcess != BookingProcess.ClaimRewardNights)
            {
                CurrentContext.SearchHotelPage.CheckOutDate =
                    DateTime.ParseExact(DateTime.Today.AddDays(1).ToString(Reference.DateFormat), Reference.DateFormat,
                                        CultureInfo.InvariantCulture);
            }

            if (CurrentContext.SelectHotelPage != null)
            {
                CurrentContext.SearchHotelPage.SearchDestination = CurrentContext.SelectHotelPage.SelectedHotel;
                CurrentContext.SearchHotelPage.SearchDestinationId = CurrentContext.SelectHotelPage.SelectedHotelId;
            }
            var autoCompleteList = GetSearchDestinationList();
            string autoCompleteListKey = string.Format("{0}{1}", AUTOCOMPLETE_LIST, language);
            MiscellaneousSessionWrapper.setAutoCompleteList(autoCompleteListKey, autoCompleteList);

            return CurrentContext.SearchHotelPage;
        }

        /// <summary>
        /// IsClaimRewardNights
        /// </summary>
        /// <returns>True/False</returns>
        public bool IsClaimRewardNights()
        {
            return CurrentContext.CurrentBookingProcess == BookingProcess.ClaimRewardNights;
        }

        /// <summary>
        /// IsPageModelValid
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="validationErrors"></param>
        /// <returns>True/False</returns>
        public override bool IsPageModelValid(BaseBookingModel pageModel, out List<KeyValueOption> validationErrors)
        {
            bool pageIsValid = true;
            validationErrors = new List<KeyValueOption>();
            KeyValueOption error = null;
            var searchHotelPageModel = pageModel as SearchHotelModel;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();

            if (searchHotelPageModel != null)
            {
                if (searchHotelPageModel.IsMinDistanceDestinationsNull)
                {
                    error = new KeyValueOption();
                    error.Key = DESTINATION_ERR_CTRL;
                    error.Value =
                         GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(
                                DESTINATION_RADIUS_ERR_MSG_KEY);
                    pageIsValid = false;
                    validationErrors.Add(error);
                }
                if (searchHotelPageModel.IsAgeInputInvalid)
                {
                    error = new KeyValueOption();
                    error.Key = AGE_ERR_MSG_KEY;
                    error.Value =
                      GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(
                            AGE_ERR_MSG);
                    validationErrors.Add(error);
                    pageIsValid = false;
                    validationErrors.Add(error);
                }
                if (!searchHotelPageModel.IsSearchfromCurrentLocation)
                {
                    if (!IsValidSearchDestination(searchHotelPageModel.SearchDestination, out error))
                    {
                        pageIsValid = false;
                        validationErrors.Add(error);
                    }
                }
                if (searchHotelPageModel.IsSearchfromCurrentLocation && !searchHotelPageModel.IsSearchWithinDefinedRadius)
                {
                    error = new KeyValueOption();
                    error.Key = DESTINATION_ERR_CTRL;
                    error.Value =
                         GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(
                                DESTINATION_RADIUS_ERR_MSG_KEY);
                    pageIsValid = false;
                    validationErrors.Add(error);
                }
                if (!IsCheckInDateValid(searchHotelPageModel.CheckInDate, out error))
                {
                    pageIsValid = false;
                    validationErrors.Add(error);
                }
                if (!IsCheckOutDateValid(searchHotelPageModel.CheckInDate, searchHotelPageModel.CheckOutDate, out error))
                {
                    pageIsValid = false;
                    validationErrors.Add(error);
                }
                if ((searchHotelPageModel.NumberOfAdults.HasValue && searchHotelPageModel.NumberOfAdults.Value <= 0) ||
                    !searchHotelPageModel.NumberOfAdults.HasValue)
                {
                    pageIsValid = false;
                    error = new KeyValueOption();
                    error.Key = NO_OF_ADULT_ERR_CTRL;
                    error.Value =
                        GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(
                            NO_OF_ADULT_ERR_MSG_KEY);
                    validationErrors.Add(error);
                }
                if (!IsBookingCodeValid(searchHotelPageModel.Offer, searchHotelPageModel.BookingCode, out error))
                {
                    pageIsValid = false;
                    validationErrors.Add(error);
                }
                if (searchHotelPageModel.NumberOfAdults != null)
                {
                    if (searchHotelPageModel.NumberOfAdults.HasValue)
                    {
                        if (!IsBedSelectionValid(searchHotelPageModel.ChildrenInformation, searchHotelPageModel.NumberOfAdults.Value, out error))
                        {
                            pageIsValid = false;
                            validationErrors.Add(error);
                        }
                    }
                }
                if (validationErrors.Count > 0)
                {
                    var errorSummary = new KeyValueOption();
                    errorSummary.Key = VALIDATION_SUMMARY_ERR_CRTL;
                    errorSummary.Value = pageConfig.PageDetail.PageMessages.GetMessage(VALIDATION_SUMMARY_ERR_MSG_KEY);
                    validationErrors.Add(errorSummary);
                }

            }
            return pageIsValid;
        }

        /// <summary>
        /// PerformSearch
        /// </summary>
        /// <param name="pageModel"></param>
        public void PerformSearch(SearchHotelModel pageModel)
        {
            ClearSessionData();
            ProcessInput(pageModel);
            SetRecentHotelSearchCookie(pageModel.SearchDestinationId);
            bookingRepository.AvailabilitySearch(pageModel);
            var resultPage = MobilePages.SelectHotel;
            if (pageModel.SearchBy == SearchByType.Hotel)
            {
                var controller = new SelectRateController();
                CurrentContext.SelectHotelPage = new SelectHotelModel();
                CurrentContext.SelectHotelPage.SelectedHotel = pageModel.SearchDestination;
                CurrentContext.SelectHotelPage.SelectedHotelId = pageModel.SearchDestinationId;
                controller.GetRoomTypes(0, 1);
                resultPage = MobilePages.SelectRate;
            }
            else
            {
                CurrentContext.SelectHotelPage = null;
                CurrentContext.SelectRatePage = null;
                CurrentContext.BookingDetailPage = null;
            }
            var redirectPage = GetPageUrl(resultPage);
            if (!pageModel.IsDeeplinkRequest)
                Redirect(redirectPage);

        }
        public void PerformNearbySearch(SearchHotelModel pageModel)
        {
            CurrentContext.SearchHotelPage = pageModel;
            Redirect(GetPageUrl(MobilePages.SearchNearby));
        }

        /// <summary>
        /// GetSearchDestinationList
        /// </summary>
        /// <returns>List of SearchDestination</returns>
        public List<SearchDestination> GetSearchDestinationList()
        {
            return siteRepository.GetSearchDestinationList();
        }

        /// <summary>
        /// GetSearchDestinationListFromSession
        /// </summary>
        /// <returns>SearchDestinationListFromSession</returns>
        public List<SearchDestination> GetSearchDestinationListFromSession()
        {
            string autoCompleteListKey = string.Format("{0}{1}", AUTOCOMPLETE_LIST, language);
            return HttpContext.Current.Session[autoCompleteListKey] as List<SearchDestination>;
        }

        /// <summary>
        /// ProcessInput
        /// </summary>
        /// <param name="pageModel"></param>
        private void ProcessInput(SearchHotelModel pageModel)
        {
            if (pageModel != null)
            {
                if (pageModel.RememberBookingCode)
                {
                    SetCookie(REMEMBER_MY_BOOKING_CODE_COOKIE, pageModel.BookingCode);
                }
                else
                {
                    if (pageModel.Offer == BookingOffer.BookingCode)
                    {
                        RemoveCookie(REMEMBER_MY_BOOKING_CODE_COOKIE);
                    }
                }
                string destinationId = string.Empty;
                pageModel.SearchBy = GetSearchByType(pageModel.SearchDestination, out destinationId);
                pageModel.SearchDestinationId = destinationId;
                pageModel.SearchType = GetSearchType(pageModel);
                pageModel.IsVisited = true;
                CurrentContext.SearchHotelPage = pageModel;
            }
        }

        /// <summary>
        /// GetSearchType
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns>SearchType</returns>
        private SearchType GetSearchType(SearchHotelModel pageModel)
        {
            var searchType = SearchType.Regular;

            if (CurrentContext.CurrentBookingProcess == BookingProcess.ClaimRewardNights)
            {
                searchType = SearchType.Redemption;
            }

            if (pageModel.Offer == BookingOffer.BonusCheques)
            {
                searchType = SearchType.BonusCheque;
            }

            if (pageModel.Offer == BookingOffer.BookingCode)
            {
                var corporateCodeRegx = GetGeneralConfig<ApplicationConfigSection>().GetMessage(CORPORATE_CODE_REX_KEY);
                var regx = new Regex(corporateCodeRegx);

                if (regx.IsMatch(pageModel.BookingCode))
                {
                    searchType = SearchType.Corporate;
                }
            }
            return searchType;
        }

        /// <summary>
        /// GetSearchByType
        /// </summary>
        /// <param name="destination"></param>
        /// <param name="destinationId"></param>
        /// <returns>SearchByType</returns>
        public SearchByType GetSearchByType(string destination, out string destinationId)
        {
            var searchBy = SearchByType.None;
            var searchedCity = GetSearchDestinationList()
                .Where(city => string.Equals(city.name, destination, StringComparison.InvariantCultureIgnoreCase)).
                FirstOrDefault();

            if (searchedCity != null)
            {
                searchBy = SearchByType.City;
                destinationId = searchedCity.id;
            }
            else
            {
                searchedCity = GetSearchDestinationList()
                    .Where(city => city.hotels
                                       .Where(
                                           hotel =>
                                           string.Equals(hotel.name, destination,
                                                         StringComparison.InvariantCultureIgnoreCase))
                                       .FirstOrDefault() != null).FirstOrDefault();
                
                destinationId = "";

                if (searchedCity != null)
                {
                    destinationId = searchedCity.hotels
                    .Where(hotel => string.Equals(hotel.name, destination, StringComparison.InvariantCultureIgnoreCase))
                    .Select(hotel => hotel.id).FirstOrDefault();
                    searchBy = SearchByType.Hotel;
                }
            }

            return searchBy;
        }

        /// <summary>
        /// IsValidSearchDestination
        /// </summary>
        /// <param name="searchDestination"></param>
        /// <param name="error"></param>
        /// <returns>True/False</returns>
        private bool IsValidSearchDestination(string searchDestination, out KeyValueOption error)
        {
            string nearByPosition = GetPageConfig<SearchHotelPageSection>().PageDetail.PageMessages.GetMessage(
                                NEAR_MY_POSITION);
            bool isValid = true;
            error = null;
            var pageConfig = GetPageConfig<SearchHotelPageSection>();
            if (string.IsNullOrEmpty(searchDestination))
            {
                isValid = false;
            }
            else if (searchDestination.Contains(AppConstants.COMMA))
            {
                isValid = true;
            }
            else
            {
                var destination = GetSearchDestinationList().Where(city =>
                                                                   string.Equals(city.name, searchDestination,
                                                                                 StringComparison.
                                                                                     InvariantCultureIgnoreCase) ||
                                                                   (city.hotels
                                                                        .Where(hotel =>
                                                                               string.Equals(hotel.name,
                                                                                             searchDestination,
                                                                                             StringComparison.
                                                                                                 InvariantCultureIgnoreCase))
                                                                        .FirstOrDefault() != null)).FirstOrDefault();

                isValid = (destination != null);
            }

            if (!isValid)
            {
                error = new KeyValueOption();
                error.Key = DESTINATION_ERR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(DESTINATION_ERR_MSG_KEY);
            }

            return isValid;
        }

        /// <summary>
        /// IsCheckInDateValid
        /// </summary>
        /// <param name="checkInDate"></param>
        /// <param name="error"></param>
        /// <returns>True/False</returns>
        public bool IsCheckInDateValid(DateTime? checkInDate, out KeyValueOption error)
        {
            bool isValid = false;
            error = null;
            var currentDate = DateTime.ParseExact(DateTime.Now.ToString(Reference.DateFormat), Reference.DateFormat,
                                                  CultureInfo.InvariantCulture);
            var pageConfig = GetPageConfig<SearchHotelPageSection>();
            var endDateOfArrival = DateTime.Today.AddDays(pageConfig.AllowedDaysForDateOfArrival);
            endDateOfArrival = endDateOfArrival.AddDays(-1);
            if (checkInDate.HasValue &&
                (DateTime.Compare(checkInDate.Value, currentDate) >= 0) &&
                (DateTime.Compare(checkInDate.Value, endDateOfArrival) <= 0))
            {
                isValid = true;
            }

            if (!isValid)
            {
                error = new KeyValueOption();
                error.Key = CHECKIN_DATE_ERR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(CHECKIN_DATE_ERR_MSG_KEY);
            }
            return isValid;
        }

        /// <summary>
        /// IsCheckOutDateValid
        /// </summary>
        /// <param name="checkInDate"></param>
        /// <param name="checkOutDate"></param>
        /// <param name="error"></param>
        /// <returns>True/False</returns>
        public bool IsCheckOutDateValid(DateTime? checkInDate, DateTime? checkOutDate, out KeyValueOption error)
        {
            bool isValid = false;
            error = null;
            var pageConfig = GetPageConfig<SearchHotelPageSection>();

            if (checkInDate.HasValue)
            {
                var currentDate = DateTime.ParseExact(DateTime.Now.ToString(Reference.DateFormat), Reference.DateFormat,
                                                      CultureInfo.InvariantCulture);
                var endDateOfArrival = DateTime.Today.AddDays(pageConfig.AllowedDaysForDateOfArrival);
                var allowedDepartureDate = checkInDate.Value.AddDays(pageConfig.AllowedBookingDays);

                if (checkOutDate.HasValue &&
                    (DateTime.Compare(checkOutDate.Value, currentDate) > 0) &&
                    (DateTime.Compare(checkOutDate.Value, checkInDate.Value) > 0) &&
                    (DateTime.Compare(checkOutDate.Value, endDateOfArrival) <= 0) &&
                    (DateTime.Compare(checkOutDate.Value, allowedDepartureDate) <= 0))
                {
                    isValid = true;
                }
            }
            if (!isValid)
            {
                error = new KeyValueOption();
                error.Key = CHECKOUT_DATE_ERR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(CHECKOUT_DATE_ERR_MSG_KEY);
            }

            return isValid;
        }


        public bool IsBedSelectionValid(List<ChildrenInfo> childrenInformation, int numnberOfAdults, out KeyValueOption error)
        {
            bool isValid = false;
            error = null;
            var pageConfig = GetPageConfig<SearchHotelPageSection>();
            string adultBed = pageConfig.PageDetail.PageMessages.GetMessage(BEDTYPE_IN_ADULT_BED).Trim();
            if (childrenInformation != null && childrenInformation.Count > 0)
            {
                IEnumerable<ChildrenInfo> childrenWithAdultBed = (from child in childrenInformation where child.Age.HasValue && !string.IsNullOrEmpty(child.BedType) && string.Equals(child.BedType, adultBed) select child);
                if (childrenWithAdultBed != null)
                {
                    if (childrenWithAdultBed.Count() <= numnberOfAdults)
                        isValid = true;
                }
            }
            else
            {
                isValid = true;
            }
            if (!isValid)
            {
                error = new KeyValueOption();
                error.Key = INADULTBEDEXCEEDING_ERR_MSG_KEY;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(ADULT_EXCEEDING_ERR_MSG);
            }

            return isValid;
        }

        /// <summary>
        /// IsBookingCodeValid
        /// </summary>
        /// <param name="bookingOffer"></param>
        /// <param name="bookingCode"></param>
        /// <param name="error"></param>
        /// <returns>True/False</returns>
        private bool IsBookingCodeValid(BookingOffer bookingOffer, string bookingCode, out KeyValueOption error)
        {
            bool isValid = true;
            error = null;
            string errorMsg = string.Empty;
            var pageConfig = GetPageConfig<SearchHotelPageSection>();
            var appConfig = GetGeneralConfig<ApplicationConfigSection>();
            if (bookingOffer == BookingOffer.BookingCode)
            {
                if (String.IsNullOrEmpty(bookingCode))
                {
                    isValid = false;
                    errorMsg = pageConfig.PageDetail.PageMessages.GetMessage(BOOKING_CODE_ERR_MSG_KEY);
                }
                else
                {
                    List<string> strIpAddresses = new List<string>();

                    if (HttpContext.Current.Request.Headers["X-Forwarded-For"] != null)
                        strIpAddresses = HttpContext.Current.Request.Headers["X-Forwarded-For"].Split(',').ToList();

                    var regx = new Regex(appConfig.GetMessage(BOOKING_CODE_REGX_KEY));
                    if (regx.IsMatch(bookingCode))
                    {
                        isValid = false;
                        errorMsg = pageConfig.PageDetail.PageMessages.GetMessage(BOOKING_CODE_NOT_ALLOWED_ERR_MSG_KEY);
                    }
                    if (WebUtil.IsFamilyandFriendsDnumber(bookingCode) && !WebUtil.IsRequestedSourceValidForFamilyAndFriendsBooking(strIpAddresses))
                    {
                        isValid = false;
                        errorMsg = pageConfig.PageDetail.PageMessages.GetMessage(INVALID_SOURCE_FOR_FAMILY_FRIENDS_DNUMBER);
                    }
                }
            }

            if (!isValid)
            {
                error = new KeyValueOption { Key = BOOKING_CODE_ERR_CTRL, Value = errorMsg };
            }
            return isValid;
        }

        /// <summary>
        /// GetBookingCode
        /// </summary>
        /// <returns>BookingCode</returns>
        private string GetBookingCode()
        {
            HttpCookie bookingCodeCookie = Request.Cookies[REMEMBER_MY_BOOKING_CODE_COOKIE];
            string bookingCode = string.Empty;
            if (CurrentContext != null && CurrentContext.SearchHotelPage != null)
            {
                bookingCode = CurrentContext.SearchHotelPage.BookingCode;
            }
            if (string.IsNullOrEmpty(bookingCode) && bookingCodeCookie != null)
            {
                bookingCode = bookingCodeCookie.Value;
            }
            return bookingCode;
        }


        private void SetRecentHotelSearchCookie(string cookieValue)
        {
            cookieValue = HttpUtility.UrlEncode(HttpUtility.UrlDecode(cookieValue));
            if (Request.Cookies[AppConstants.RECENT_HOTEL_SEARCH_COOKIE_NAME] != null
                && !string.IsNullOrEmpty(Request.Cookies[AppConstants.RECENT_HOTEL_SEARCH_COOKIE_NAME].Value))
            {
                int recentSearchHotelCookieLength = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.RECENT_SEARCH_HOTEL_COOKIE_LENGTH]);
                IList<string> recentHotels = Request.Cookies[AppConstants.RECENT_HOTEL_SEARCH_COOKIE_NAME].Value.Split(',').ToList();
                if (recentHotels != null && recentHotels.Count > 0)
                {
                    if (!recentHotels.Contains(cookieValue))
                    {
                        if (recentHotels.Count == recentSearchHotelCookieLength)
                        {
                            recentHotels.RemoveAt(recentSearchHotelCookieLength - 1);
                        }
                        recentHotels.Insert(0, cookieValue);
                    }
                    else
                    {
                        string moveItem = string.Empty;
                        for (int i = 0; i < recentHotels.Count; i++)
                        {
                            if (i > 0)
                            {
                                if (recentHotels[i] == cookieValue)
                                {
                                    moveItem = recentHotels[i];
                                    recentHotels.RemoveAt(i);
                                    recentHotels.Insert(0, moveItem);
                                    break;
                                }
                            }
                        }
                    }
                    cookieValue = GenerateCookieValueForRecentSearch(recentHotels);
                    RemoveCookie(AppConstants.RECENT_HOTEL_SEARCH_COOKIE_NAME);
                    SetCookie(AppConstants.RECENT_HOTEL_SEARCH_COOKIE_NAME, cookieValue);
                }
            }
            else
                SetCookie(AppConstants.RECENT_HOTEL_SEARCH_COOKIE_NAME, cookieValue);

        }

        private string GenerateCookieValueForRecentSearch(IList<string> recentHotels)
        {
            string cookie = string.Empty;
            foreach (var item in recentHotels)
            {
                if (cookie == "")
                    cookie = item;
                else
                    cookie += "," + item;
            }
            return cookie;
        }

        /// <summary>
        /// Set Cookies
        /// </summary>
        /// <param name="bookingCode"></param>
        private void SetCookie(string cookieName, string cookieValue)
        {
            HttpCookie cookie = new HttpCookie(cookieName, cookieValue);
            cookie.Expires = new DateTime(2999, 12, 31);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// Remove Cookies
        /// </summary>
        private void RemoveCookie(string cookieName)
        {
            HttpCookie cookie = new HttpCookie(cookieName);
            cookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        private void ClearSessionData()
        {
            var controller = new BookingDetailsController();
            controller.IsPrepaidBooking = false;
        }

        private HttpRequest Request
        {
            get { return HttpContext.Current.Request; }
        }

        #endregion

        public string GetMessageFromXml(string str)
        {
            var pageConfig = GetPageConfig<SearchHotelPageSection>();
            return pageConfig.PageDetail.PageMessages.GetMessage(str);
        }
    }
}