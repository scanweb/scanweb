﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Web;
using System.Text.RegularExpressions;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    public class ViewBookingsController : BaseController
    {
        #region Declaration
            private const string BOOKING_NUMBER_ERR_CTRL = "hgcReservationNumberError";
            private const string BOOKING_NUMBER_ERR_MSG_KEY = "reservationNumberRequiredMsg";
            private const string LAST_NAME_ERR_CTRL = "hgcLastNameError";
            private const string LAST_NAME_ERR_MSG_KEY = "lastNameRequiredMsg";
            private const string BOOKING_NUMBER_INVALID_ERR_MSG_KEY = "incorrectReservationNumberMsg";
            private const string BOOKING_NUMBER_PATTREN = "bookingNumberRegx";
            private const string BOOKING_NUMBER_PATTREN_FIRST = "bookingNumberRegxFirst";
            private const string BOOKING_NUMBER_PATTREN_SECOND = "bookingNumberRegxSecond";

            private IUserInfoRespository userRepository;
        #endregion

        #region Constructors

        /// <summary>
            /// This is default constructor, which will help indentify local
            /// language automatically.
            /// </summary>
            public ViewBookingsController()
                : this("")
            {
            }

            /// <summary>
            /// This constructor intialize the language and other repositories 
            /// to be used in ViewBookingsController
            /// </summary>
            /// <param name="requestLanguage"></param>
            public ViewBookingsController(string requestLanguage)
                : base(requestLanguage)
            {
               userRepository = DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
            }

        #endregion

        #region Public Methods

            public bool ValidateBookingInput(System.Web.UI.WebControls.TextBox bookingNumberCtrl, string bookingHolderLastName, out List<KeyValueOption> validationErrors)
            {
                bool isValid = true;
                validationErrors = new List<KeyValueOption>();
                KeyValueOption error;
                var pageConfig = GetPageConfig<ViewBookingsPageSection>();

                if (string.IsNullOrEmpty(bookingNumberCtrl.Text.Trim()))
                {
                    isValid = false;
                    error = new KeyValueOption();
                    error.Key = BOOKING_NUMBER_ERR_CTRL;
                    error.Value = pageConfig.PageDetail.PageMessages.GetMessage(BOOKING_NUMBER_ERR_MSG_KEY);
                    validationErrors.Add(error);
                }
                else
                {
                    var appConfig = GetGeneralConfig<ApplicationConfigSection>();
                    var regx = new Regex(appConfig.GetMessage(BOOKING_NUMBER_PATTREN));
                    string value = bookingNumberCtrl.Text.Trim();
                    //var regExpFirst = new Regex(@"^[0-9]{8}$");
                    //var regExpSecond = new Regex(@"^[0-9]{8}\-[0-9]{1}$");
                    var regExpFirst = new Regex(appConfig.GetMessage(BOOKING_NUMBER_PATTREN_FIRST));
                    var regExpSecond = new Regex(appConfig.GetMessage(BOOKING_NUMBER_PATTREN_SECOND));
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (value.IndexOf('-') == -1)
                        {
                            bool resultFirst = regExpFirst.IsMatch(value);
                            if (resultFirst)
                                isValid = true;
                            else
                            {
                                isValid = false;
                                error = new KeyValueOption();
                                error.Key = BOOKING_NUMBER_ERR_CTRL;
                                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(BOOKING_NUMBER_INVALID_ERR_MSG_KEY);
                                validationErrors.Add(error);
                            }
                        }
                        else
                        {
                            bool resultSecond = regExpSecond.IsMatch(value);
                            if (resultSecond)
                                isValid = true;
                            else
                            {
                                bookingNumberCtrl.Text = bookingNumberCtrl.Text.Split('-')[0];
                                isValid = true;
                            }

                        }
                    }
                    //if (!string.Equals(regx.Match(bookingNumber).Value,bookingNumber))
                    //{
                    //    isValid = false;
                    //    error = new KeyValueOption();
                    //    error.Key = BOOKING_NUMBER_ERR_CTRL;
                    //    error.Value = pageConfig.PageDetail.PageMessages.GetMessage(BOOKING_NUMBER_INVALID_ERR_MSG_KEY);
                    //    validationErrors.Add(error);
                    //}
                }


                if (string.IsNullOrEmpty(bookingHolderLastName))
                {
                    isValid = false;
                    error = new KeyValueOption();
                    error.Key = LAST_NAME_ERR_CTRL;
                    error.Value = pageConfig.PageDetail.PageMessages.GetMessage(LAST_NAME_ERR_MSG_KEY);
                    validationErrors.Add(error);
                }

                return isValid;
            }

            public List<KeyValueOption> FindBooking(System.Web.UI.WebControls.TextBox bookingNumberCtrl, string bookingHolderLastName)
            {
                var validationErrors = new List<KeyValueOption>();

                if (ValidateBookingInput(bookingNumberCtrl, bookingHolderLastName, out validationErrors))
                {
                    var processUrl = siteRepository.GetPageUrl(MobilePages.ModifyCancel);
                    processUrl = string.Format("{0}?{1}={2}&{3}={4}&{5}=1", processUrl,Reference.RESERVATION_ID_QUERY_STRING,bookingNumberCtrl.Text.Trim(),
                                                Reference.LAST_NAME_QUERY_STRING,bookingHolderLastName,Reference.REDIRECT_BACK_ON_ERROR);

                    Redirect(processUrl);
                }
                return validationErrors;
            }

            public string GetErrorMessage(string errorCode)
            {
                string errorMessage = string.Empty;
                if (!string.IsNullOrEmpty(errorCode))
                {                    
                    switch (errorCode)
                    {
                        case Reference.NO_RESERVATION:
                            errorMessage = WebUtil.GetTranslatedText(TranslatedTextConstansts.INVALID_BOOKING_NUMBER);
                            break;
                        case Reference.EMPTY_LAST_NAME:
                            errorMessage = WebUtil.GetTranslatedText(Reference.EMPTY_LAST_NAME_ERR_MSG_ID);
                            break;
                        case Reference.WRONG_LAST_NAME:
                            errorMessage = WebUtil.GetTranslatedText(TranslatedTextConstansts.INVALID_SURNAME);
                            break;
                        case Reference.CANCELLED_RESERVATION:
                            errorMessage = WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_ALREADY_CANCELED);
                            break;
                        
                    }
                }
                return errorMessage;
            }

            public bool IsAuthenticated { get { return userRepository.IsUserAuthenticated; } }


        #endregion
    }
}
