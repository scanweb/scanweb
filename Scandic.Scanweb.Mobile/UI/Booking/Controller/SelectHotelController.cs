﻿using System;
using System.Collections;
using System.Collections.Generic;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Entity.Model;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    /// <summary>
    /// Select Hotel Controller
    /// </summary>
    public class SelectHotelController : BaseController
    {
        private const string ALTERNATIVE_PAGE_HEADING_KEY = "AlternativeHotelPageHeading";
        private const string PAGE_HEADING_AFTER_RESULTS_KEY = "PageHeadingAfterResults";

        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public SelectHotelController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in MasterPageController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public SelectHotelController(string requestLanguage)
            : base(requestLanguage)
        {
        }

        #endregion

        /// <summary>
        /// Select Hotel
        /// </summary>
        /// <param name="pageModel">Select Hotel Model</param>
        public void SelectHotel(SelectHotelModel pageModel)
        {
            pageModel.IsVisited = true;
            var controller = new SelectRateController();
            CurrentContext.SelectHotelPage = pageModel;
            controller.GetRoomTypes(0, 1);
            Redirect(GetPageUrl(MobilePages.SelectRate));
        }

        /// <summary>
        /// Get Available Hotels
        /// </summary>
        /// <param name="currentlyDisplayedHotels"></param>
        /// <param name="nextHotelsToDisplayed"></param>
        /// <param name="sortBy"></param>
        /// <returns>Available Hotel List</returns>
        public IEnumerable GetAvailableHotels(int currentlyDisplayedHotels, int nextHotelsToDisplayed, string sortBy)
        {
            var hotelResultList = new List<HotelResults>();
            var sortByEnum = sortBy != string.Empty ? (DestinationSortType)Convert.ToInt32(sortBy) : DestinationSortType.Name;
            var hotelResults = bookingRepository.GetAvailableHotels(currentlyDisplayedHotels, nextHotelsToDisplayed, sortByEnum);
            var pageConfig = GetPageConfig<SelectHotelPageSection>();
            string searchDestinationString = string.Empty;
            if (hotelResults.IsAlternateSearch)
            {
                hotelResults.SearchHeading = pageConfig.PageDetail.PageMessages.GetMessage(ALTERNATIVE_PAGE_HEADING_KEY);
            }
            else
            {
                if (CurrentContext.SearchHotelPage.IsSearchfromCurrentLocation == true)
                {
                    searchDestinationString = WebUtil.GetTranslatedText(Reference.NEAR_BY_HOTELS);
                }
                else
                {
                    searchDestinationString = WebUtil.GetTranslatedText(CurrentContext.SearchHotelPage.SearchDestination);
                }

                hotelResults.SearchHeading = string.Format(pageConfig.PageDetail.PageMessages.GetMessage(PAGE_HEADING_AFTER_RESULTS_KEY),
                                                           hotelResults.HotelsFound.ToString(), searchDestinationString);

            }

            hotelResults.SearchBy = CurrentContext.SearchHotelPage.SearchBy;
            CurrentContext.SearchHotelPage.TrackingPropertySearchedHotelCount = hotelResults.HotelsFound;
            hotelResultList.Add(hotelResults);
            return hotelResultList;
        }

        #region old

        /// <summary>
        /// Select Hotel Page Model
        /// </summary>
        /// <returns>Select Hotel Page Model</returns>
        public static SelectHotelPageModel GetPageData()
        {
            var pageData = new SelectHotelPageModel();
            pageData.SortOptions = new List<SelectOption>();
            pageData.SortOptions.Add(new SelectOption { Id = "NameID", Value = "Name" });
            pageData.SortOptions.Add(new SelectOption { Id = "RateID", Value = "Rate" });
            pageData.SortOptions.Add(new SelectOption { Id = "DistanceToCityCenterID", Value = "DistanceToCityCenter" });
            pageData.ContextInfo = new ContextInformation();
            pageData.ContextInfo.Header = "StockHolm";
            pageData.ContextInfo.InfoList = new List<string>();
            pageData.ContextInfo.InfoList.Add("Check-in Jan 07 2012");
            pageData.ContextInfo.InfoList.Add("Check-out Jan 12 2012");
            pageData.ContextInfo.InfoList.Add("Guest 1");

            return pageData;
        }

        #endregion
    }
}