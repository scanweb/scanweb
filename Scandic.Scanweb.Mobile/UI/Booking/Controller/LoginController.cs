﻿using System;
using System.Web;
using System.Web.UI;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Booking.Repository;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    public class LoginController : BaseController
    {
        private const string ID_QUERY_STRING = "Id=1";
        private const string PASS_QUERY_STRING = "Pass=1";
        private const string INVALID_QUERY_STRING = "Invalid=1";
        private const string TEMP_LOGIN_ID_COOKIE = "TLoginId";
        private IUserInfoRespository userRepository;

        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public LoginController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in MasterPageController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public LoginController(string requestLanguage)
            : base(requestLanguage)
        {
            userRepository = new UserInfoRepository();
        }

        #endregion

        #region Public Method

        /// <summary>
        /// GetMembershipId
        /// </summary>
        /// <param name="fromCookie"></param>
        /// <returns>MembershipId</returns>
        public string GetMembershipId(out bool fromCookie)
        {
            HttpCookie membershipIdCookie = HttpContext.Current.Request.Cookies[Reference.MembershipIdCookie];
            HttpCookie tempLoginIdCookie = HttpContext.Current.Request.Cookies[TEMP_LOGIN_ID_COOKIE];

            string membershipId = string.Empty;
            var appConfig = GetGeneralConfig<ApplicationConfigSection>();
            fromCookie = false;

            if (tempLoginIdCookie != null)
            {
                membershipId = tempLoginIdCookie.Value;
                RemoveCookie(TEMP_LOGIN_ID_COOKIE);
            }
            else if (membershipIdCookie != null)
            {
                membershipId = membershipIdCookie.Value;
                fromCookie = true;
            }
            else
            {
                membershipId = appConfig.GetMessage(Reference.PrefixMembershipId);
            }

            return membershipId;
        }

        public string GetPassword(string membershipId)
        {
            return WebUtil.GetPassword(membershipId);
        }

        /// <summary>
        /// SignIn
        /// </summary>
        /// <param name="logonContext"></param>
        public void SignIn(LogonContext logonContext)
        {
            if (userRepository.SignIn(logonContext))
            {
                OnSuccessfullSignIn(logonContext);
                logonContext.Password = "";
            }
            else
            {
                OnSignInFailure(logonContext.Id);
            }
        }

        /// <summary>
        /// SignOut
        /// </summary>
        public void SignOut()
        {
            userRepository.SignOut();
            Redirect(GetPageUrl(MobilePages.Start));
        }

        /// <summary>
        /// ValidateContext
        /// </summary>
        /// <param name="logonContext"></param>
        /// <returns>True/False</returns>
        public bool ValidateContext(LogonContext logonContext)
        {
            string errors = string.Empty;
            bool isValid = true;
            var queryString = HttpContext.Current.Request.UrlReferrer.Query;

            if (string.IsNullOrEmpty(logonContext.Id))
            {
                if (string.IsNullOrEmpty(queryString))
                {
                    errors = string.Format("?{0}", ID_QUERY_STRING);
                }
                else if (queryString.IndexOf(ID_QUERY_STRING) == -1)
                {
                    errors = string.Format("&{0}", ID_QUERY_STRING);
                }
                queryString = string.Format("{0}{1}", queryString, errors);
                isValid = false;
            }
            else
            {
                queryString = queryString.Replace(ID_QUERY_STRING, "");
            }

            if (string.IsNullOrEmpty(logonContext.Password))
            {
                errors = "";
                if (string.IsNullOrEmpty(queryString))
                {
                    errors = string.Format("?{0}", PASS_QUERY_STRING);
                }
                else if (queryString.IndexOf(PASS_QUERY_STRING) == -1)
                {
                    errors = string.Format("&{0}", PASS_QUERY_STRING);
                }
                queryString = string.Format("{0}{1}", queryString, errors);
                isValid = false;
            }
            else
            {
                queryString = queryString.Replace(PASS_QUERY_STRING, "");
            }

            if (!isValid)
            {
                var loginPagePath = HttpContext.Current.Request.UrlReferrer.AbsolutePath;

                loginPagePath = string.Format("{0}{1}", loginPagePath, queryString);
                Redirect(loginPagePath, true);
            }

            return isValid;
        }

        /// <summary>
        /// CheckPageAccess
        /// </summary>
        /// <param name="page"></param>
        public void CheckPageAccess(Page page)
        {
            var attSessionExpired =
                System.Attribute.GetCustomAttribute(page.GetType(), typeof(AllowPublicAccess)) as AllowPublicAccess;

            if (!userRepository.IsUserAuthenticated && !attSessionExpired.Accessible)
            {
                RedirectToSignIn(page.Request);
            }
        }

        /// <summary>
        /// RedirectToSignIn
        /// </summary>
        /// <param name="request"></param>
        public void RedirectToSignIn(HttpRequest request)
        {
            var signInPath = GetPageUrl(MobilePages.SignIn);
            var returnUrl = string.Empty;
            if(request.UrlReferrer != null)
            {
                if (!string.IsNullOrEmpty(request.UrlReferrer.Query) && request.UrlReferrer.Query.Contains(Reference.ReturnUrlQueryString))
                {
                    returnUrl = string.Format("{0}", request.UrlReferrer.Query);
                    signInPath = string.Format("{0}{1}", signInPath, returnUrl);
                }
                else
                {
                    returnUrl = string.Format("{0}={1}", Reference.ReturnUrlQueryString, request.UrlReferrer.AbsoluteUri);
                    signInPath = string.Format("{0}?{1}", signInPath, returnUrl);
                }
            }
            Redirect(signInPath);
        }

        #endregion

        #region Private Method

        /// <summary>
        /// OnSignInFailure
        /// </summary>
        /// <param name="loginId"></param>
        private void OnSignInFailure(string loginId)
        {
            var queryString = HttpContext.Current.Request.UrlReferrer.Query;
            var loginPagePath = HttpContext.Current.Request.UrlReferrer.AbsolutePath;

            string errors = string.Empty;

            if (string.IsNullOrEmpty(queryString))
            {
                errors = string.Format("?{0}", INVALID_QUERY_STRING);
            }
            else if (queryString.IndexOf(INVALID_QUERY_STRING) == -1)
            {
                errors = string.Format("&{0}", INVALID_QUERY_STRING);
            }
            queryString = string.Format("{0}{1}", queryString, errors);
            queryString = queryString.Replace(ID_QUERY_STRING, "");
            queryString = queryString.Replace(PASS_QUERY_STRING, "");

            loginPagePath = string.Format("{0}{1}", loginPagePath, queryString);
            SetCookie(TEMP_LOGIN_ID_COOKIE, loginId);
            Redirect(loginPagePath, true);
        }

        /// <summary>
        /// OnSuccessfullSignIn
        /// </summary>
        /// <param name="logonContext"></param>
        private void OnSuccessfullSignIn(LogonContext logonContext)
        {
            if (logonContext.RememberMe)
            {
                SetRememberMeCookie(logonContext.Id);
            }
            else
            {
                RemoveRememberMeCookie(logonContext.Id);
            }

            WebUtil.RememberMePassword(logonContext.Id, logonContext.Password, logonContext.RememberMe);

            if (string.IsNullOrEmpty(logonContext.ReturnUrl))
            {
                logonContext.ReturnUrl = GetPageUrl(MobilePages.Start);
            }
            string returnUrl = logonContext.ReturnUrl;

            if (logonContext.ReturnUrl.IndexOf("?") > 0)
            {
                returnUrl = string.Format("{0}&loggedIn=1", logonContext.ReturnUrl);
            }
            else
            {
                returnUrl = string.Format("{0}?loggedIn=1", logonContext.ReturnUrl);
            }

            Redirect(returnUrl);
        }

        /// <summary>
        /// SetRememberMeCookie
        /// </summary>
        /// <param name="membershipId"></param>
        private void SetRememberMeCookie(string membershipId)
        {
            SetCookie(Reference.MembershipIdCookie, membershipId);
        }

        /// <summary>
        /// RemoveRememberMeCookie
        /// </summary>
        private void RemoveRememberMeCookie(string membershipId)
        {
            RemoveCookie(Reference.MembershipIdCookie);
        }

        /// <summary>
        /// SetCookie
        /// </summary>
        /// <param name="cookieName"></param>
        /// <param name="cookieValue"></param>
        private void SetCookie(string cookieName, string cookieValue)
        {
            HttpCookie cookie = new HttpCookie(cookieName, cookieValue);

            cookie.Expires = new DateTime(2999, 12, 31);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// RemoveCookie
        /// </summary>
        /// <param name="cookieName"></param>
        private void RemoveCookie(string cookieName)
        {
            HttpCookie cookie = new HttpCookie(cookieName);

            cookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        #endregion
    }
}