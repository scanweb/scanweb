﻿using System;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Entity.Model;
using EPiServer.Core;
using EPiServer;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using System.Collections.Generic;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.BookingEngine.Web;
using System.Configuration;
using Scandic.Scanweb.Mobile.UI.Common;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    /// <summary>
    /// StartController
    /// </summary>
    public class StartController : BaseController
    {
        #region Declaration
        private IUserInfoRespository userRepository;
        private const string IMAGE_HEADING = "MobileCarouselImageTitle";

        #endregion

        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public StartController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in StartController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public StartController(string requestLanguage)
            : base(requestLanguage)
        {
            userRepository = DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method returns the inital data (Main Menu, page heading etc) to rendered on start page.
        /// </summary>
        /// <returns>StartPageModel</returns>
        public StartPageModel GetPageData()
        {
            StartPageModel pageModel = new StartPageModel();
            var pageConfig = GetPageConfig<StartPageSection>();

            pageModel.ContextMenu = pageConfig.ContextMenu; //siteRepository.GetMainMenu(language);
            pageModel.PageHeading = pageConfig.PageDetail.PageHeading;
            pageModel.PageTitle = pageConfig.PageDetail.PageTitle;

            return pageModel;
        }

        public void StartBookingProcess(string commandArgument)
        {
            BookingProcess bookingProcess = BookingProcess.BookARoom;
            if (string.Equals(commandArgument, "ClaimRewardNights", StringComparison.InvariantCultureIgnoreCase))
            {
                bookingProcess = BookingProcess.ClaimRewardNights;
            }

            Redirect(bookingRepository.StartBookingProcess(bookingProcess));

        }

        public void ModifyCancelProcess()
        {
            Redirect(siteRepository.GetPageUrl(MobilePages.ViewBookings));
        }

        public void MobileOffer()
        {
            Redirect(siteRepository.GetPageUrl(MobilePages.AllOffers));
        }

        public List<ImageCarousel> LoadCarousalOffersData()
        {
            PageReference offerOverview = siteRepository.GetCMSPageReferenceFromRootPage(Reference.OFFER_OVERVIEW_PAGE);
            PageData page = ContentDataAccess.GetPageData(offerOverview.ID, CMSSessionWrapper.CurrentLanguage);
            List<ImageCarousel> lstImgCarousel = new List<ImageCarousel>();
            int imageWidth = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.MOBILE_CAROUSEL_IMAGE_WIDTH]);
            int carouselOfferCount = Convert.ToInt32(Reference.HOMEPAGE_CAROUSEL_OFFERS_COUNT);
            int offerTrackingID = 0;
            for (int offerPosition = 1; offerPosition <= carouselOfferCount; offerPosition++)
            {
                if (page["MobileOffer" + offerPosition] != null)
                {
                    PageData offerPageData = DataFactory.Instance.GetPage(page["MobileOffer" + offerPosition] as PageReference);
                    TimeSpan span = DateTime.Now.Subtract(offerPageData.StopPublish);
                    TimeSpan offerEndTime = DateTime.Now.Subtract(Convert.ToDateTime(offerPageData[Reference.OFFER_END_TIME]));
                    if (offerPageData != null && offerPageData[Reference.SHOW_OFFER_FOR_MOBILE] != null && Convert.ToBoolean(offerPageData[Reference.SHOW_OFFER_FOR_MOBILE]) && span.Days < 0)
                    {
                        bool processCarousal = false;
                        if (offerPageData[Reference.OFFER_END_TIME] == null)
                            processCarousal = true;
                        else if (Convert.ToDateTime(offerPageData[Reference.OFFER_END_TIME]) > DateTime.Now)
                            processCarousal = true;

                        if (processCarousal)
                        {
                            offerTrackingID++;
                            ImageCarousel imgCarousel = new ImageCarousel();
                            if (offerPageData[Reference.MOBILE_OFFER_IMAGE] != null)
                                imgCarousel.CarouselImageSrc = WebUtil.GetImageVaultImageUrl(Convert.ToString(offerPageData[Reference.MOBILE_OFFER_IMAGE]), imageWidth);
                            else
                                if (offerPageData[Reference.DESKTOP_OFFER_IMAGE] != null)
                                    imgCarousel.CarouselImageSrc = WebUtil.GetImageVaultImageUrl(Convert.ToString(offerPageData[Reference.DESKTOP_OFFER_IMAGE]), imageWidth);

                            imgCarousel.CarouselImageAlt = Convert.ToString(offerPageData[Reference.BOX_HEADING]);
                            imgCarousel.CarouselImageTitle = Convert.ToString(offerPageData[IMAGE_HEADING]);
                            imgCarousel.CarouselImageHRef = GetImageURL(offerPageData.PageLink.ID.ToString(), Convert.ToString(offerTrackingID));
                            imgCarousel.CarouselImageWidth = Convert.ToString(imageWidth);
                            lstImgCarousel.Add(imgCarousel);
                        }
                    }
                }
            }
            return lstImgCarousel;
        }

        private string GetImageURL(string pageId, string offerTrackingID)
        {
            string offerPageURL = siteRepository.GetPageUrl(MobilePages.OfferDetail);
            offerPageURL = string.Format("{0}?pageid={1}&SN={2}", offerPageURL, pageId, offerTrackingID);
            return offerPageURL;
        }

        public void ClearContext()
        {
            bookingRepository.ClearCurrentContext();
        }
        #endregion
    }
}