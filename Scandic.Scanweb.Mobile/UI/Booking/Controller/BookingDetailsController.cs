﻿//  Description					: BookingDetailsController                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Booking.Repository;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using MobileCommon = Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Web.HotelTimezone;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    /// <summary>
    /// Contains members to facilitate booking.
    /// </summary>
    public class BookingDetailsController : BaseController
    {
        private const string VALIDATION_SUMMARY_ERR_CRTL = "hgcValidationSummaryheadingError";
        private const string VALIDATION_SUMMARY_ERR_MSG_KEY = "validationSummaryMsg";
        private const string FIRSTNAME_ERR_CTRL = "hgcFirstNameError";
        private const string FIRSTNAME_ERR_MSG_KEY = "firstNameErrorMsg";
        private const string LASTNAME_ERR_CTRL = "hgcLastNameError";
        private const string LASTNAME_ERR_MSG_KEY = "lastNameErrorMsg";
        private const string EMAIL_ERR_CTRL = "hgcEmailError";
        private const string EMAIL_ERR_MSG_KEY = "emailErrorMsg";
        private const string EMAILFORMAT_ERR_MSG_KEY = "emailFormatErrorMsg";
        private const string CITY_ERR_CTRL = "hgcCityError";
        private const string CITY_ERR_MSG_KEY = "cityErrorMsg";
        private const string PHONE_COUNTRY_CODE_ERR_CTRL = "hgcPhoneCountryCodeError";
        private const string PHONE_COUNTRY_CODE_ERR_MSG_KEY = "phoneCountryCodeErrorMsg";
        private const string COUNTRY_ERR_CTRL = "hgcCountryError";
        private const string COUNTRY_ERR_MSG_KEY = "countryErrorMsg";
        private const string PHONE_ERR_CTRL = "hgcMobileError";
        private const string PHONE_ERR_MSG_KEY = "phoneErrorMsg";
        private const string PHONEFORMAT_ERR_MSG_KEY = "phoneFormatErrorMsg";
        private const string BEDPREFERENCE_ERR_CTRL = "hgcBedPreferencesError";
        private const string BEDPREFERENCE_ERR_MSG_KEY = "bedPreferenceErrorMsg";
        private const string CARDHOLDER_NAME_ERR_CTRL = "hgcCardHolderNameError";
        private const string CARDHOLDER_NAME_ERR_MSG_KEY = "creditCardHolderNameErrorMsg";
        private const string CARD_NUMBER_ERR_CTRL = "hgcCardNumberError";
        private const string CARD_NUMBER_ERR_MSG_KEY = "creditCardNumberErrorMsg";
        private const string CARD_NUMBER_FORMAT_ERR_MSG_KEY = "creditCardNumberFormatErrorMsg";
        private const string CARD_TYPE_ERR_CTRL = "hgcCardTypeError";
        private const string CARD_TYPE_ERR_MSG_KEY = "creditCardTypeErrorMsg";
        private const string ACCEPT_CONDITION_ERR_CTRL = "hgcBookingRuleError";
        private const string ACCEPT_CONDITION_ERR_MSG_KEY = "termsConditionErrorMsg";
        private const string FLEX_FGP_CREDIT_CARD_ERROR_CTRL = "hgcFlexFGPCreditCardSelectionError";
        private const string FLEX_FGP_CREDIT_CARD_ERROR_MSG_KEY = "flexFGPCreditCardSelectionError";

        private IUserInfoRespository userRepository;
        private IScanwebSessionRepository scanwebSessionRepository;

        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public BookingDetailsController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in BookingDetailsController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public BookingDetailsController(string requestLanguage)
            : base(requestLanguage)
        {
            userRepository = new UserInfoRepository();
            scanwebSessionRepository = new ScanwebSessionRepository();
        }

        #endregion

        #region Public Method
        /// <summary>
        /// Gets page data
        /// </summary>
        /// <returns></returns>
        public BookingDetailModel GetPageData()
        {
            var pageModel = CurrentContext.BookingDetailPage;

            if (pageModel == null)
            {
                pageModel = new BookingDetailModel();
            }

            if (userRepository.IsUserAuthenticated)
            {
                var loyaltyDetails = userRepository.GetLoyaltyDetails();
                var profile = userRepository.GetProfile();

                pageModel.MembershipId = loyaltyDetails.UserName;
                if (profile != null)
                {
					//Added as part of SCANAM-537
                    if (!string.IsNullOrEmpty(profile.NativeFirstName) && !string.IsNullOrEmpty(profile.NativeLastName))
                    {
                        pageModel.FirstName = profile.NativeFirstName;
                        pageModel.LastName = profile.NativeLastName;
                    }
                    else
                    {
                        pageModel.FirstName = !string.IsNullOrEmpty(profile.FirstName) ? profile.FirstName.Trim() : string.Empty;
                        pageModel.LastName = !string.IsNullOrEmpty(profile.LastName) ? profile.LastName.Trim() : string.Empty;
                    }
                    pageModel.EmailAddress = !string.IsNullOrEmpty(profile.EmailID) ? profile.EmailID.Trim() : string.Empty; ;
                    pageModel.CountryCode = !string.IsNullOrEmpty(profile.Country) ? profile.Country.Trim() : string.Empty; ;
                    pageModel.Country = GetCountryList()
                        .Where(
                            country =>
                            string.Equals(country.Key, profile.Country, StringComparison.InvariantCultureIgnoreCase))
                        .Select(country => country.Value).FirstOrDefault();
                    pageModel.MobileNumber = !string.IsNullOrEmpty(profile.MobilePhone) ? profile.MobilePhone.Trim() : string.Empty;
                    if (profile.CreditCard != null)
                    {
                        pageModel.CardHolderName = !string.IsNullOrEmpty(profile.CreditCard.NameOnCard) ? profile.CreditCard.NameOnCard.Trim() : string.Empty;
                        pageModel.CardNumber = MaskCreditCardNumber(profile.CreditCard.CardNumber);
                        pageModel.CardTypeCode = !string.IsNullOrEmpty(profile.CreditCard.CardType) ? profile.CreditCard.CardType.Trim() : string.Empty;
                        if (profile.CreditCard.ExpiryDate != null)
                        {
                            pageModel.CardExpiryMonthCode = profile.CreditCard.ExpiryDate.Month.ToString();
                            pageModel.CardExpiryYearCode = profile.CreditCard.ExpiryDate.Year.ToString();
                        }
                    }

                    SetOtherRequests(pageModel, profile.UserPreference);
                }
            }
            var cmsPage = siteRepository.GetCMSPageData(MobilePages.BookingDetails);

            if (cmsPage != null)
            {
                var pageLink = cmsPage["ScanwebUpdateProfile"] as PageReference;
                if (!PageReference.IsNullOrEmpty(pageLink))
                {
                    var upadateProfilePage = DataFactory.Instance.GetPage(pageLink,
                                                                          EPiServer.Security.AccessLevel.NoAccess);
                    if (upadateProfilePage != null)
                    {
                        pageModel.UpdateProfileUrl = siteRepository.GetPageUrl(upadateProfilePage, language, true);
                    }
                }
            }

            pageModel.TotalRate = bookingRepository.GetTotalPrice(0);
            return pageModel;
        }

        /// <summary>
        /// Process credit card number
        /// </summary>
        /// <param name="enteredCardNumber"></param>
        /// <returns></returns>
        public string ProcesCreditCardNumber(string enteredCardNumber)
        {
            string cardNumber = enteredCardNumber;
            if (IsAuthenticated)
            {
                var profile = userRepository.GetProfile();
                if (profile != null && profile.CreditCard != null &&
                    !string.IsNullOrEmpty(profile.CreditCard.CardNumber))
                {
                    var maskedCardNumber = MaskCreditCardNumber(profile.CreditCard.CardNumber);
                    if (string.Equals(enteredCardNumber, maskedCardNumber))
                    {
                        cardNumber = profile.CreditCard.CardNumber;
                    }
                }
            }

            return cardNumber;
        }

        /// <summary>
        /// Gets country phone code
        /// </summary>
        /// <param name="CountryCode"></param>
        /// <returns></returns>
        public string GetCountryPhoneCode(string CountryCode)
        {
            string phoneCode = string.Empty;
            if (!string.IsNullOrEmpty(CountryCode))
            {
                phoneCode = (from map in GetPhoneCodes()
                             where string.Equals(map.Key, CountryCode)
                             select map.Value).FirstOrDefault();
            }

            return phoneCode;
        }

        /// <summary>
        /// Checks whether user is authenticated or not.
        /// </summary>
        public bool IsAuthenticated
        {
            get { return userRepository.IsUserAuthenticated; }
        }

        /// <summary>
        /// Gets contry list.
        /// </summary>
        /// <returns></returns>
        public List<KeyValueOption> GetCountryList()
        {
            return siteRepository.GetCountryList();
        }

        /// <summary>
        /// Gets credit card types.
        /// </summary>
        /// <returns></returns>
        public List<KeyValueOption> GetCreditCardTypes()
        {
            return siteRepository.GetCreditCardTypes();
        }

        /// <summary>
        /// Gets months.
        /// </summary>
        /// <returns></returns>
        public List<KeyValueOption> GetMonths()
        {
            return siteRepository.GetMonthList();
        }

        /// <summary>
        /// Gets years
        /// </summary>
        /// <returns></returns>
        public List<KeyValueOption> GetYears()
        {
            return siteRepository.GetCreditCardValidYears();
        }

        /// <summary>
        /// Gets phone codes
        /// </summary>
        /// <returns></returns>
        public List<KeyValueOption> GetPhoneCodes()
        {
            return siteRepository.GetPhoneCodes();
        }

        /// <summary>
        /// Gets guarantee options
        /// </summary>
        /// <returns></returns>
        public List<KeyValueOption> GetGuaranteeOptions()
        {
            var guaranteeOptions = new List<KeyValueOption>();
            var messageIds = new List<string>();
            if (CurrentContext.CurrentBookingProcess != BookingProcess.ClaimRewardNights)
            {
                if (CurrentContext.SelectRatePage.HoldGuranteeAvailable && IsSelectedHotelCurrentTimeBefore6PM())
                {
                    messageIds.Add(Reference.GuranteedTextCodeBefore6PM);
                }
                messageIds.Add(CurrentContext.SelectRatePage.CreditCardGuranteeType);
            }
            else
            {
                messageIds.Add(Reference.GuranteedTextCodeRewardNights);
            }

            if (messageIds.Count > 0)
            {
                var scanWebBookingDetailsCMSPage =
                    siteRepository.GetCMSPageData(EpiServerPageConstants.BOOKING_DETAILS_PAGE);
                foreach (var messageId in messageIds)
                {
                    string guaranteeType;
                    string guaranteeTextIdentifier = string.Empty ;
                    if(RoomRateUtil.IsPrepaidGuaranteeType(messageId) &&
                        !bookingRepository.GetPaymentFallback(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode))
                    {
                        guaranteeType = AppConstants.GUARANTEETYPE_PREAPIDINPROGRESS;
                        guaranteeTextIdentifier = string.Format("{0}-Prepaid", messageId);
                    }
                    else
                    {
                        guaranteeType = messageId;
                        guaranteeTextIdentifier = messageId;
                    }

                    guaranteeOptions.Add(new KeyValueOption
                                             {
                                                 Key = guaranteeType,
                                                 Value = scanWebBookingDetailsCMSPage[guaranteeTextIdentifier] != null? 
                                                         scanWebBookingDetailsCMSPage[guaranteeTextIdentifier].ToString() : string.Empty
                                             });
                }
            }
            return guaranteeOptions;
        }

        /// <summary>
        /// Gets country code to select.
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public string GetCountryCodeToSelect(string language)
        {
            string countryCode = string.Empty;

            language = language.ToUpper();

            switch (language)
            {
                case LanguageConstant.LANGUAGE_SWEDISH:
                    countryCode = LanguageConstant.LANGUAGE_SV_MAP_COUNTRY_CODE;
                    break;
                case LanguageConstant.LANGUAGE_DANISH:
                    countryCode = LanguageConstant.LANGUAGE_DA_MAP_COUNTRY_CODE;
                    break;
                case LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION:
                case LanguageConstant.LANGUAGE_FINNISH:
                    countryCode = language;
                    break;
            }
            return countryCode;
        }

        /// <summary>
        /// Gets bed preferences
        /// </summary>
        /// <returns></returns>
        public List<KeyValueOption> GetBedPreference()
        {
            var bedPreferences = new List<KeyValueOption>();
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();

            bedPreferences.Add(new KeyValueOption
                                   {
                                       Key = "0",
                                       Value = pageConfig.PageDetail.PageMessages.GetMessage("bedPreferenceFirstOption")
                                   });
            if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null &&
                HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count > 0)
            {
                var selectedRoomRate = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[0] as SelectedRoomAndRateEntity;

                if (selectedRoomRate != null && selectedRoomRate.RoomRates != null)
                {
                    List<RoomType> roomTypes = new List<RoomType>();
                    foreach (var roomRate in selectedRoomRate.RoomRates)
                    {
                        var roomType = RoomRateUtil.GetRoomType(roomRate.RoomTypeCode);
                        roomTypes.Add(roomType);
                    }
                    Utility.SetRoomTypeSortOrderAsInOpera(roomTypes);

                    roomTypes.Sort(new RoomTypePriorityComparer());

                    foreach (var roomType in roomTypes)
                    {
                        bedPreferences.Add(new KeyValueOption
                                               {Key = roomType.OperaRoomTypeId, Value = roomType.BedTypeDescription});
                    }
                }
            }
            return bedPreferences;
        }

        /// <summary>
        /// Checks whether the page mode is valid or not.
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="validationErrors"></param>
        /// <returns></returns>
        public override bool IsPageModelValid(BaseBookingModel pageModel, out List<KeyValueOption> validationErrors)
        {
            bool pageIsValid = true;
            validationErrors = new List<KeyValueOption>();
            KeyValueOption error;
            var bookingDetailPageModel = pageModel as BookingDetailModel;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();

            if (bookingDetailPageModel != null)
            {
                if (!IsFirstNameValid(bookingDetailPageModel.FirstName, out error))
                {
                    pageIsValid = false;
                    validationErrors.Add(error);
                }
                if (!IsLastNameValid(bookingDetailPageModel.LastName, out error))
                {
                    pageIsValid = false;
                    validationErrors.Add(error);
                }
                if (!IsEmailValid(bookingDetailPageModel.EmailAddress, out error))
                {
                    pageIsValid = false;
                    validationErrors.Add(error);
                }
                if (!IsCountryValid(bookingDetailPageModel.CountryCode, out error))
                {
                    pageIsValid = false;
                    validationErrors.Add(error);
                }
                if (!IsPhoneCountryCodeValid(bookingDetailPageModel.PhoneCountryCode, out error))
                {
                    pageIsValid = false;
                    validationErrors.Add(error);
                }
                if (!IsMobilePhoneValid(bookingDetailPageModel.MobileNumber, out error))
                {
                    pageIsValid = false;
                    validationErrors.Add(error);
                }
                if (!IsBedPreferenceValid(bookingDetailPageModel.BedPreferenceCode, out error))
                {
                    pageIsValid = false;
                    validationErrors.Add(error);
                }

                if (!string.Equals(bookingDetailPageModel.ReservationGuaranteeOption,
                                   Reference.GuranteedTextCodeBefore6PM) &&
                    CurrentContext.CurrentBookingProcess != BookingProcess.ClaimRewardNights
                    && !bookingDetailPageModel.ReservationGuaranteeOption.Equals(AppConstants.GUARANTEETYPE_PREAPIDINPROGRESS, StringComparison.InvariantCultureIgnoreCase)
                    && string.IsNullOrEmpty(bookingDetailPageModel.PanHash))
                {
                    if (!IsCardHolderNameValid(bookingDetailPageModel.CardHolderName, out error))
                    {
                        pageIsValid = false;
                        validationErrors.Add(error);
                    }
                    if (!IsCardNumberValid(bookingDetailPageModel.CardNumber, out error))
                    {
                        pageIsValid = false;
                        validationErrors.Add(error);
                    }
                    if (!IsCardTypeValid(bookingDetailPageModel.CardTypeCode, out error))
                    {
                        pageIsValid = false;
                        validationErrors.Add(error);
                    }
                }

                if (!IsTermsConditionAccepted(bookingDetailPageModel.AcceptTermsCondition, out error))
                {
                    pageIsValid = false;
                    validationErrors.Add(error);
                }

                if (string.Equals(bookingDetailPageModel.ReservationGuaranteeOption,Reference.GuranteedTextCodeAfter6PM, StringComparison.InvariantCultureIgnoreCase) &&
                    bookingDetailPageModel.IsFlexFGPSavedCreditPayment && !IsFlexFGPCreditCardSelected(bookingDetailPageModel.PanHash, out error))
                {
                     pageIsValid = false;
                    validationErrors.Add(error);
                }
            }
            if (validationErrors.Count > 0)
            {
                var errorSummary =  new KeyValueOption();
                errorSummary.Key = VALIDATION_SUMMARY_ERR_CRTL;
                errorSummary.Value  = pageConfig.PageDetail.PageMessages.GetMessage(VALIDATION_SUMMARY_ERR_MSG_KEY);
                validationErrors.Add(errorSummary);
            }

            return pageIsValid;
        }

        /// <summary>
        /// Books hotel
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="ignorePrepaidCheckForNetsPayment"> </param>
        /// <param name="paymentInfo"> </param>
        /// <param name="isRedirectToConfirmationPage"> </param>
        /// <param name="isModifyBookingAfterNetsPaymentSuccessful"> </param>
        /// <returns></returns>
        public IEnumerable BookHotel(BookingDetailModel pageModel, PaymentInfo paymentInfo,
                                     bool isRedirectToConfirmationPage, bool isModifyBookingAfterNetsPaymentSuccessful,
                                     out bool isModifyBookingSuccess, out bool isMakePaymentSuccess)
        {
            IEnumerable results = null;
            isModifyBookingSuccess = false;
            isMakePaymentSuccess = true;
            if (!RoomRateUtil.IsPrepaidGuaranteeType(pageModel.ReservationGuaranteeOption) || bookingRepository.GetPaymentFallback(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode) ||
                isModifyBookingAfterNetsPaymentSuccessful)
            {
                pageModel.IsVisited = true;
                if (isModifyBookingAfterNetsPaymentSuccessful && ((paymentInfo !=null) && (paymentInfo.CaptureSuccess)))
                {
                    if (!MakePayment(paymentInfo))
                    {
                        isMakePaymentSuccess = false;
                        paymentInfo.MakePaymentFailure = true;
                    }
                }
                results = bookingRepository.MakeHotelReservation(pageModel, paymentInfo, isRedirectToConfirmationPage,
                          isModifyBookingAfterNetsPaymentSuccessful);
                var bookingConfirmation = results as Dictionary<string, string>;
                if (isModifyBookingAfterNetsPaymentSuccessful)
                {
                    if (bookingConfirmation != null)
                        isModifyBookingSuccess = true;
                    if (isRedirectToConfirmationPage)
                    {
                        Redirect(string.Format("{0}?BNo={1}&LNm={2}",GetPageUrl(MobilePages.BookingConfirmation, language), pageModel.ReservationNumber, pageModel.LastName));
                        CurrentContext.BookingDetailPage = pageModel;
                    }
                }
                else if (bookingConfirmation != null)
                {
                    isModifyBookingSuccess = true;
                    pageModel.ReservationNumber = bookingConfirmation.ContainsKey(Reference.RESERVATION_NUMBER)
                                                      ? bookingConfirmation[Reference.RESERVATION_NUMBER]
                                                      : string.Empty;
                    CurrentContext.BookingDetailPage = pageModel;

                    if (isRedirectToConfirmationPage)
                    {
                        Redirect(string.Format("{0}?BNo={1}&LNm={2}",GetPageUrl(MobilePages.BookingConfirmation, language), pageModel.ReservationNumber, pageModel.LastName));
                    }
                    results = null;
                }
            }
            else
            {
                IgnoreBooking();
                results = bookingRepository.MakeHotelReservation(pageModel, null, false, false);
                var bookingConfirmation = results as Dictionary<string, string>;
                string transactionId = string.Empty;
                if (bookingConfirmation != null)
                {
                    pageModel.ReservationNumber = bookingConfirmation.ContainsKey(Reference.RESERVATION_NUMBER)
                                                      ? bookingConfirmation[Reference.RESERVATION_NUMBER]
                                                      : string.Empty;
                }
            
                if (!string.IsNullOrEmpty(pageModel.ReservationNumber))
                {
                     transactionId = bookingRepository.Register(pageModel.PanHash);
                }

               
                if(!string.IsNullOrEmpty(transactionId))
                {
                    MobileCommon.SessionManager.BookingInformation = pageModel;
                    scanwebSessionRepository.PaymentTransactionId = transactionId;
                    Redirect(bookingRepository.GetNetsTerminalURLForMobile(transactionId));
                }
                else
                {
                    if (!string.IsNullOrEmpty(Reservation2SessionWrapper.NetsPaymentErrorMessage) || 
                        !string.IsNullOrEmpty(Reservation2SessionWrapper.NetsPaymentInfoMessage))
                    {
                        List<ErrorDetails> errorDetails = new List<ErrorDetails>();
                        errorDetails.Add(new ErrorDetails() { ErrorCode = "NetsRegisterFailed", 
                            ErrorMessaage = !string.IsNullOrEmpty(Reservation2SessionWrapper.NetsPaymentErrorMessage)?
                                            Reservation2SessionWrapper.NetsPaymentErrorMessage : Reservation2SessionWrapper.NetsPaymentInfoMessage });
                        results = errorDetails;
                    }
                    else  if (!string.IsNullOrEmpty(pageModel.ReservationNumber))
                    {
                        IgnoreBooking();
                        WebUtil.ShowApplicationError(WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/MerchantAuthenticationFailureHeader"),
                            WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/MerchantAuthenticationFailureMessage"));
                    }
                }
            }
            return results;
        }

         /// <summary>
        /// Does a call to Nets authorization.  
        /// </summary>
        /// <param name="transactionId"></param>
        public string NetsAuthProcess(string transactionId)
        {
            return bookingRepository.NetsAuthProcess(transactionId);
        }

        /// <summary>
        /// Does a call to Nets to capture the order amount.
        /// </summary>
        /// <param name="transactionId"></param>
        public bool NetsCaptureProcess(string transactionId)
        {
            return bookingRepository.NetsCaptureProcess(transactionId);
        }

         ///<summary>
        /// Gets paymentinfo for the given transactionId.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public PaymentInfo GetPaymentInfo(string transactionId)
        {
            return bookingRepository.GetPaymentInfo(transactionId);
        }

        /// <summary>
        /// Does MakePayment call to update Opera after Nets capture.
        /// </summary>
        /// <param name="paymentInfo"></param>
        public bool MakePayment(PaymentInfo paymentInfo)
        {
           return bookingRepository.MakePayment(paymentInfo);
        }

        /// <summary>
        /// Fetches all avilable credit cards of a FGP user.
        /// </summary>
        /// <returns></returns>
        public Collection<CreditCardEntity> FetchFGPCreditCards()
        {
            Collection<CreditCardEntity> fgpCreditCards = null;
            if(UserLoggedInSessionWrapper.UserLoggedIn &&
                (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null) &&
               (!string.IsNullOrEmpty(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID)))
            {
                fgpCreditCards = bookingRepository.FetchFGPCreditCards(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID);
            }
            return fgpCreditCards;
        }

        /// <summary>
        /// Process error message for Nets error code and updates the corresponding session variable.
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="errorSource"></param>
        /// <returns></returns>
        public void ProcessErrorMessageForNetsErrorCode(string errorCode, string errorSource)
        {
            bookingRepository.ProcessErrorMessageForNetsErrorCode(errorCode, errorSource);
        }

        /// <summary>
        /// Does a annul call to Nets.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public string NetsAnnulProcess(string transactionId)
        {
            return bookingRepository.NetsAnnulProcess(transactionId);
        }

        /// <summary>
        /// Confirms session booking.
        /// </summary>
        public void ConfirmBooking()
        {
            if (MobileCommon.SessionManager.BookingGuestInformation != null)
            {
                bookingRepository.ConfirmBooking(MobileCommon.SessionManager.BookingGuestInformation);
            }
        }

        /// <summary>
        /// Ignores booking.
        /// </summary>
        public void IgnoreBooking()
        {
            bookingRepository.IgnoreBooking();
        }
        #endregion

        #region Private Methods

        private string MaskCreditCardNumber(string cardNumber)
        {
            string formatedNumber = string.Empty;
            if (!string.IsNullOrEmpty(cardNumber))
            {
                string lastFourDigit = Regex.Match(cardNumber, "((\\d){4})$").Value;
                formatedNumber = string.Format("{0}{1}", Reference.CREDIT_CARD_MASK, lastFourDigit);
            }
            return formatedNumber;
        }

        private bool IsFirstNameValid(string firstName, out KeyValueOption error)
        {
            bool isValid = true;
            error = null;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();

            if (string.IsNullOrEmpty(firstName))
            {
                isValid = false;
                error = new KeyValueOption();
                error.Key = FIRSTNAME_ERR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(FIRSTNAME_ERR_MSG_KEY);
            }
            return isValid;
        }

        private bool IsLastNameValid(string lastName, out KeyValueOption error)
        {
            bool isValid = true;
            error = null;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();

            if (string.IsNullOrEmpty(lastName))
            {
                isValid = false;
                error = new KeyValueOption();
                error.Key = LASTNAME_ERR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(LASTNAME_ERR_MSG_KEY);
            }
            return isValid;
        }

        private bool IsEmailValid(string email, out KeyValueOption error)
        {
            bool isValid = true;
            error = null;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();
            var appConfig = GetGeneralConfig<ApplicationConfigSection>();

            error = new KeyValueOption();
            error.Key = EMAIL_ERR_CTRL;

            if (string.IsNullOrEmpty(email))
            {
                isValid = false;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(EMAIL_ERR_MSG_KEY);
            }
            else if (!Regex.IsMatch(email, appConfig.GetMessage(Reference.EmailRegx)))
            {
                isValid = false;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(EMAILFORMAT_ERR_MSG_KEY);
            }
            else
            {
                error = null;
            }
            return isValid;
        }


        private bool IsCountryValid(string countryCode, out KeyValueOption error)
        {
            bool isValid = true;
            error = null;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();

            if (string.Equals(countryCode, "DFT") || string.Equals(countryCode, "SEPR"))
            {
                isValid = false;
                error = new KeyValueOption();
                error.Key = COUNTRY_ERR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(COUNTRY_ERR_MSG_KEY);
            }
            return isValid;
        }

        private bool IsPhoneCountryCodeValid(string phoneCountryCode, out KeyValueOption error)
        {
            bool isValid = true;
            int countryCode;
            error = null;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();

            isValid = int.TryParse(phoneCountryCode, out countryCode);
            if (!isValid)
            {
                error = new KeyValueOption();
                error.Key = PHONE_COUNTRY_CODE_ERR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(PHONE_COUNTRY_CODE_ERR_MSG_KEY);
            }
            return isValid;
        }

        private bool IsMobilePhoneValid(string phoneNumber, out KeyValueOption error)
        {
            bool isValid = true;
            error = null;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();
            var appConfig = GetGeneralConfig<ApplicationConfigSection>();

            error = new KeyValueOption();
            error.Key = PHONE_ERR_CTRL;

            if (string.IsNullOrEmpty(phoneNumber))
            {
                isValid = false;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(PHONE_ERR_MSG_KEY);
            }
            else if (!Regex.IsMatch(phoneNumber, appConfig.GetMessage(Reference.PhoneRegx)))
            {
                isValid = false;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(PHONEFORMAT_ERR_MSG_KEY);
            }
            else
            {
                error = null;
            }
            return isValid;
        }

        private bool IsBedPreferenceValid(string bedPreference, out KeyValueOption error)
        {
            bool isValid = true;
            error = null;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();

            if (string.Equals(bedPreference, "0"))
            {
                isValid = false;
                error = new KeyValueOption();
                error.Key = BEDPREFERENCE_ERR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(BEDPREFERENCE_ERR_MSG_KEY);
            }
            return isValid;
        }

        private bool IsCardHolderNameValid(string name, out KeyValueOption error)
        {
            bool isValid = true;
            error = null;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();

            if (string.IsNullOrEmpty(name))
            {
                isValid = false;
                error = new KeyValueOption();
                error.Key = CARDHOLDER_NAME_ERR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(CARDHOLDER_NAME_ERR_MSG_KEY);
            }
            return isValid;
        }

        private bool IsCardNumberValid(string cardNumber, out KeyValueOption error)
        {
            bool isValid = true;
            error = null;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();
            var appConfig = GetGeneralConfig<ApplicationConfigSection>();

            error = new KeyValueOption();
            error.Key = CARD_NUMBER_ERR_CTRL;

            if (string.IsNullOrEmpty(cardNumber))
            {
                isValid = false;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(CARD_NUMBER_ERR_MSG_KEY);
            }
            else if (!Regex.IsMatch(cardNumber, appConfig.GetMessage(Reference.CreditCardNumberRegx)))
            {
                isValid = false;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(CARD_NUMBER_FORMAT_ERR_MSG_KEY);
            }
            else
            {
                error = null;
            }
            return isValid;
        }

        private bool IsCardTypeValid(string cardTypeCode, out KeyValueOption error)
        {
            bool isValid = true;
            error = null;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();

            if (string.Equals(cardTypeCode, "DFT"))
            {
                isValid = false;
                error = new KeyValueOption();
                error.Key = CARD_TYPE_ERR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(CARD_TYPE_ERR_MSG_KEY);
            }
            return isValid;
        }

        private bool IsFlexFGPCreditCardSelected(string panHash, out KeyValueOption error)
        {
            bool isValid = true;
            error = null;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();

            if (string.IsNullOrEmpty(panHash))
            {
                isValid = false;
                error = new KeyValueOption();
                error.Key = FLEX_FGP_CREDIT_CARD_ERROR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(FLEX_FGP_CREDIT_CARD_ERROR_MSG_KEY);
            }
            return isValid;
        }

        private bool IsTermsConditionAccepted(bool conditionAccepted, out KeyValueOption error)
        {
            bool isValid = true;
            error = null;
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();

            if (!conditionAccepted)
            {
                isValid = false;
                error = new KeyValueOption();
                error.Key = ACCEPT_CONDITION_ERR_CTRL;
                error.Value = pageConfig.PageDetail.PageMessages.GetMessage(ACCEPT_CONDITION_ERR_MSG_KEY);
            }
            return isValid;
        }

        private bool IsSelectedHotelCurrentTimeBefore6PM()
        {
            bool isBefor6 = true;
            int SIXPMTIME = 18;
            if (CurrentContext != null && CurrentContext.SelectHotelPage != null)
            {                
                    var currentTime = TimeZoneManager.Instance.GetHotelCurrentTime(CurrentContext.SelectHotelPage.SelectedHotelId); 
                    if (!DateTime.Equals(currentTime, DateTime.MinValue))
                    {
                        var result = DateTime.Compare(CurrentContext.SearchHotelPage.CheckInDate.Value, currentTime.Date);
                        //Compare both date Date of arivial and Current time where hotel located.
                        if (result == 0)
                        {
                            var timeSpan = currentTime.TimeOfDay;
                            //Check if current time is already 6PM or not.
                            if (timeSpan.Hours >= SIXPMTIME)
                            {
                                isBefor6 = false;
                            }
                        }
                    }                
            }
            return isBefor6;
        }

        private void SetOtherRequests(BookingDetailModel pageModel, UserPreferenceEntity[] userPreferences)
        {
            if (userPreferences != null)
            {
                foreach (var preference in userPreferences)
                {
                    if (string.Equals(preference.RequestType, UserPreferenceConstants.SPECIALREQUEST,
                                      StringComparison.InvariantCultureIgnoreCase))
                    {
                        switch (preference.RequestValue)
                        {
                            case UserPreferenceConstants.LOWERFLOOR:
                            case UserPreferenceConstants.HIGHFLOOR:
                                pageModel.FloorPreferenceCode = preference.RequestValue;
                                break;
                            case UserPreferenceConstants.AWAYFROMELEVATOR:
                            case UserPreferenceConstants.NEARELEVATOR:
                                pageModel.ElevatorPreferenceCode = preference.RequestValue;
                                break;
                            case UserPreferenceConstants.NOSMOKING:
                            case UserPreferenceConstants.SMOKING:
                                pageModel.SmokingPreferenceCode = preference.RequestValue;
                                break;
                            case UserPreferenceConstants.ACCESSIBLEROOM:
                                pageModel.AccessibleRoom = true;
                                break;
                            case UserPreferenceConstants.ALLERGYROOM:
                                pageModel.AllergyFriendlyRoom = true;
                                break;
                            case UserPreferenceConstants.PETFRIENDLYROOM:
                                pageModel.PetFriendlyRoom = true;
                                break;
                        }
                    }
                }
            }
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Gets/Sets PaymentTransactionId
        /// </summary>
        public string PaymentTransactionId
        {
            set { scanwebSessionRepository.PaymentTransactionId = value; }
            get { return scanwebSessionRepository.PaymentTransactionId; }
        }

        /// <summary>
        /// Holds booking data of prepaid guarantee types on redirecting to Nets window.
        /// This information will be used to make a reservation on nets payment successful.
        /// </summary>
        public  BookingDetailModel BookingInformation
        {
            get { return MobileCommon.SessionManager.BookingInformation; }
            set { MobileCommon.SessionManager.BookingInformation = value; }
        }

        /// <summary>
        /// Gets/Sets IsEarlyBookingUsingStoredPanHashCC
        /// </summary>
        public bool IsEarlyBookingUsingStoredPanHashCC
        {
            set
            {
                HygieneSessionWrapper.IsEarlyBookingUsingStoredPanHashCC = value;
            }
            get { return HygieneSessionWrapper.IsEarlyBookingUsingStoredPanHashCC; }
        }

        /// <summary>
        /// IsPrepaidBooking
        /// </summary>
        public bool IsPrepaidBooking
        {
            set { GenericSessionVariableSessionWrapper.IsPrepaidBooking = value; }
            get { return GenericSessionVariableSessionWrapper.IsPrepaidBooking; }

        }
        #endregion

        public string GetMessageFromXml(string str)
        {
            var pageConfig = GetPageConfig<BookingDetailsPageSection>();
            return pageConfig.PageDetail.PageMessages.GetMessage(str);
        }
    }
}