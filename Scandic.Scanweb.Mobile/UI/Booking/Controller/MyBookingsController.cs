﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Common.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Entity;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    public class MyBookingsController: BaseController
    {
        private IUserInfoRespository userRepository;
        private IReservationRetrievalRepository reservationRetrievalRepo;
        private const string USER_BOOKINGS_KEY = "Mobile.UserBookingKey";
        private const string NO_FUTURE_BOOKING_MSG_ID = "noFutureBookingMsg";        

        #region Constructors

            /// <summary>
            /// This is default constructor, which will help indentify local
            /// language automatically.
            /// </summary>
            public MyBookingsController()
                : this("")
            {
            }

            /// <summary>
            /// This constructor intialize the language and other repositories 
            /// to be used in MyBookingsController
            /// </summary>
            /// <param name="requestLanguage"></param>
            public MyBookingsController(string requestLanguage)
                : base(requestLanguage)
            {
                userRepository = DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
                reservationRetrievalRepo = DependencyResolver.Instance.GetService(typeof(IReservationRetrievalRepository)) as IReservationRetrievalRepository;
            }

        #endregion

        #region Methods

            public string LastName
            {
                get
                {
                    string lastName = string.Empty;
                    if (userRepository.IsUserAuthenticated)
                    {
                        var userProfile = userRepository.GetProfile();
                        lastName = userProfile.LastName;
                    }
                    return lastName;
                }
            }

            public void LoadPageData()
            {
                //Future reservation an UserHistory calls are made here, so that
                //information is loaded in correct language context when function
                //calls are made from Page Methods (or Web Methods)                
                GetFutureReservation(0, 1, BookingSortType.HotelName, SortOrder.Ascending,true);
                GetUserHistory(0, 1, TranscationSortType.CheckInDate, SortOrder.Descending, true);
            }

            public UserReservations GetFutureReservation(int currentlyDisplayedBookings, 
                                            int noOfBookingsToDisplay, BookingSortType sortType, 
                                            SortOrder sortOrder, bool fetchLatestBookings)
            {
                UserReservations futureReservation = new UserReservations();
                

                if (userRepository.IsUserAuthenticated)
                {
                    var loyaltyDetails = userRepository.GetLoyaltyDetails();
                    var moreResultToFetch = false;
                    var sessionKey = string.Format("{0}_{1}", USER_BOOKINGS_KEY, loyaltyDetails.NameID);
                    var errorMsgId = string.Empty;

                    futureReservation.FutureBookings = reservationRetrievalRepo.GetFutureBookings(loyaltyDetails.NameID, 
                                                        currentlyDisplayedBookings, noOfBookingsToDisplay,
                                                        sortType, sortOrder, fetchLatestBookings, out moreResultToFetch);
                    futureReservation.FetchMoreFutureBookings = moreResultToFetch;
                    var sessionStore = StoreManager.Instance.GetStore(StoreType.Session);
                    var userBookings = sessionStore.GetData<UserReservations>(sessionKey);                    
                    if (futureReservation.FutureBookings == null || futureReservation.FutureBookings.Count == 0)
                    {
                        errorMsgId = NO_FUTURE_BOOKING_MSG_ID;
                    }                    
                    if (!string.IsNullOrEmpty(errorMsgId))
                    {
                        var pageConfig = GetPageConfig<MyBookingsPageSection>();

                        futureReservation.Errors = new List<ErrorDetails>();
                        futureReservation.Errors.Add( new ErrorDetails { ErrorMessaage = pageConfig.PageDetail.PageMessages.GetMessage(errorMsgId)});

                    }

                }                

                return futureReservation;
            }            

            public UserTransactions GetUserHistory(int currentlyDisplayedTransactions,
                                                int noOfTransactionsToDisplay, TranscationSortType sortType,
                                                SortOrder sortOrder,  bool fetchLatestTransactions)
            {
                UserTransactions userHistory = new UserTransactions();

                if (userRepository.IsUserAuthenticated)
                {
                    var loyaltyDetails = userRepository.GetLoyaltyDetails();
                    
                    userHistory = reservationRetrievalRepo.GetUserHistory(loyaltyDetails.NameID,
                                                        currentlyDisplayedTransactions, noOfTransactionsToDisplay,
                                                        sortType, sortOrder, fetchLatestTransactions);
                   

                }

                return userHistory;
            }

        #endregion
    }
}
