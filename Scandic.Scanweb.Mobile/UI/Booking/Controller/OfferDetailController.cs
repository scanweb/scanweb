﻿using System;
using System.Collections.Generic;
using System.Configuration;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.CMS.DataAccessLayer;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    public class OfferDetailController : BaseController
    {
        #region Declaration
        private IUserInfoRespository userRepository;
        #endregion

        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public OfferDetailController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in StartController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public OfferDetailController(string requestLanguage)
            : base(requestLanguage)
        {
            userRepository = DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
        }


        public PageData GetMobileOfferDetails(int offerPageID)
        {
            PageData pagedata = siteRepository.GetPageDataByPageID(offerPageID);
            return pagedata;
        }

        public PageData GetMobileOfferListPage(string offerPage)
        {
            PageReference offerPageContainer = siteRepository.GetCMSPageReferenceFromRootPage(offerPage);
            PageData offerContainerPage = ContentDataAccess.GetPageData(offerPageContainer.ID, CMSSessionWrapper.CurrentLanguage);
            return offerContainerPage;
        }

        public string GetCanonicalURL(PageData pData)
        {
            return siteRepository.GetPageUrl(pData, pData.LanguageID, true);
        
        }

        /// <summary>
        /// Gets images for Offer image carousel from CMS
        /// </summary>
        /// <param name="offerPageID"></param>
        /// <returns></returns>
        public List<ImageCarousel> LoadOfferCarouselData(int offerPageID)
        {
            PageData pagedata = GetMobileOfferDetails(Convert.ToInt32(offerPageID));
            List<ImageCarousel> lstImgCarousel = new List<ImageCarousel>();
            int imageWidth = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.MOBILE_CAROUSEL_IMAGE_WIDTH]);
            int imageCount = Convert.ToInt32(Reference.OFFER_CAROUSEL_IMAGE_COUNT);
            bool autoSlide = pagedata["AutoSlideRequired"] != null ? Convert.ToBoolean(pagedata["AutoSlideRequired"]) : false;

            for (int imageNo = 1; imageNo <= imageCount; imageNo++)
            {
                if (pagedata["Image" + imageNo] != null)
                {
                    ImageCarousel imgCarousel = new ImageCarousel();
                    imgCarousel.CarouselImageSrc = WebUtil.GetImageVaultImageUrl(Convert.ToString(pagedata["Image" + imageNo]), imageWidth);
                    imgCarousel.CarouselImageAlt = Convert.ToString(pagedata[Reference.BOX_HEADING]);
                    //imgCarousel.CarouselImageTitle = "";// Convert.ToString(pagedata[HEADING]);
                    imgCarousel.CarouselImageHRef = "#";
                    imgCarousel.CarouselAutoSlide = autoSlide.ToString();
                    imgCarousel.CarouselImageWidth = Convert.ToString(imageWidth);
                    lstImgCarousel.Add(imgCarousel);
                }
            }
            return lstImgCarousel;
        }
        #endregion
    }
}
