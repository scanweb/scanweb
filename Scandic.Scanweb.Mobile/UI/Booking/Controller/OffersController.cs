﻿using System;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    public class OffersController : BaseController
    {
        #region Declaration
        private IUserInfoRespository userRepository;
        #endregion

        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public OffersController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in StartController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public OffersController(string requestLanguage)
            : base(requestLanguage)
        {
            userRepository = DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
        }

        #endregion
        /// <summary>
        /// method to return offers category collection
        /// </summary>
        /// <param name="OfferCategoryPage"></param>
        /// <returns></returns>
        public PageDataCollection GetOfferCategoryPageCollection(PageData OfferCategoryPage)
        {
            PageDataCollection newPageCollection = new PageDataCollection();
            if (OfferCategoryPage != null)
            {
                PageDataCollection pageCollection = DataFactory.Instance.GetChildren(OfferCategoryPage.PageLink);
                if (pageCollection != null && pageCollection.Count > 0)
                {
                    foreach (PageData eachPage in pageCollection)
                    {
                        TimeSpan span = DateTime.Now.Subtract(eachPage.StopPublish);
                        TimeSpan offerEndTime = DateTime.Now.Subtract(Convert.ToDateTime(eachPage[Reference.OFFER_END_TIME]));
                        if (eachPage[Reference.SHOW_OFFER_FOR_MOBILE] != null && eachPage[Reference.SHOW_OFFER_FOR_MOBILE].ToString().ToLower() == "true" && span.Days < 0)
                        {
                            if (eachPage[Reference.OFFER_END_TIME] == null)
                                newPageCollection.Add(eachPage);
                            else if (Convert.ToDateTime(eachPage[Reference.OFFER_END_TIME]) > DateTime.Now)
                                newPageCollection.Add(eachPage);
                        }
                    }
                }
            }
            return newPageCollection;
        }

        public bool IsOffersConfiguredForCurrentLocale(PageDataCollection offerCategoryPages)
        {
            bool result = false;
            foreach (PageData offerCategoryPage in offerCategoryPages)
            {
                if ((offerCategoryPage["HideOfferCategory"] == null) || (offerCategoryPage["HideOfferCategory"] != null &&
                                 offerCategoryPage["HideOfferCategory"].ToString().ToLower() != "true"))
                {
                    PageDataCollection pageCollection = DataFactory.Instance.GetChildren(offerCategoryPage.PageLink);
                    if (pageCollection != null && pageCollection.Count > 0)
                    {
                        foreach (PageData eachPage in pageCollection)
                        {
                            TimeSpan span = DateTime.Now.Subtract(eachPage.StopPublish);
                            TimeSpan offerEndTime = DateTime.Now.Subtract(Convert.ToDateTime(eachPage[Reference.OFFER_END_TIME]));
                            if (eachPage[Reference.SHOW_OFFER_FOR_MOBILE] != null && eachPage[Reference.SHOW_OFFER_FOR_MOBILE].ToString().ToLower() == "true" && span.Days < 0)
                            {
                                if (eachPage[Reference.OFFER_END_TIME] == null)
                                    result = true;
                                else if (Convert.ToDateTime(eachPage[Reference.OFFER_END_TIME]) > DateTime.Now)
                                    result = true;
                                break;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public PageReference GetOfferContainerPageData(string PageID)
        {
            return siteRepository.GetCMSPageReferenceFromRootPage(PageID);
        }

        public string GetOfferURL(string pageId)
        {
            string offerPageURL = siteRepository.GetPageUrl(MobilePages.OfferDetail);
            offerPageURL = string.Format("{0}?pageid={1}", offerPageURL, pageId);
            return offerPageURL;
        }
    }
}
