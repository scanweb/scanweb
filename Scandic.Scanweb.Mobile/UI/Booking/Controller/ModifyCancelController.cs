﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.Mobile.UI.Controls;
using System.Text;
using Scandic.Scanweb.Mobile.UI.Common.Interface;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    public class ModifyCancelController : BaseController
    {
        private IReservationRetrievalRepository reservationRetrievalRepo;
        private IReservationModificationRepository reservationModifyRepo;
        private IUserInfoRespository userRepository;
        private const string CMS_PAGE_GUARANTEE_INFO_KEY = "GuaranteeInformation";
        private const string HEADING_CONFIRM_MESSAGE = "confirmMessageOverlayHeading";
        private const string CONFIRM_MESSAGE = "confirmMessage";
        private const string YES_BUTTON_TEXT = "yesButtonText";
        private const string NO_BUTTON_TEXT = "noButtonText";
        private const string OK_BUTTON_TEXT = "okButtonText";
        private const string CANCELLED_ROOM_HEADING = "cancelledRoomHeadingText";
        private const string CANCELLED_ROOM_ERROR_HEADING = "cancelledRoomErrorHeadingText";
        private const string CANCEL_BOOKING_MESSAGE = "cancelBookingConfirmationMessage";
        private const string CANCEL_BOOKING_HEADING = "cancelBookingHeadingText";
        private const string SCANWEB_CANCEL_CONFIRMATION_CMS_PAGE = "scanWebCancelConfirmationCMSPage";

        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public ModifyCancelController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in MyBookingsController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public ModifyCancelController(string requestLanguage)
            : base(requestLanguage)
        {
            reservationRetrievalRepo = DependencyResolver.Instance.GetService(typeof(IReservationRetrievalRepository)) as IReservationRetrievalRepository;
            reservationModifyRepo = DependencyResolver.Instance.GetService(typeof(IReservationModificationRepository)) as IReservationModificationRepository;
            userRepository = DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
        }

        #endregion

        #region Methods

        public ReservationDetails GetPageData()
        {
            var reservationNumber = HttpContext.Current.Request.QueryString[Reference.RESERVATION_ID_QUERY_STRING];
            var lastName = HttpContext.Current.Request.QueryString[Reference.LAST_NAME_QUERY_STRING];
            var redirectBackOnError = HttpContext.Current.Request.QueryString[Reference.REDIRECT_BACK_ON_ERROR];
            var redirectOnError = string.Equals(redirectBackOnError, "1");
            var reservationDetails = reservationRetrievalRepo.FindBooking(reservationNumber, lastName, redirectOnError);

            if (reservationDetails.Error != null && redirectOnError)
            {
                string redirectionUrl = string.Empty;
                if (HttpContext.Current.Request.UrlReferrer == null)
                {
                    redirectionUrl = string.Format("{0}?", GetPageUrl(MobilePages.ViewBookings));
                }
                else
                {
                    redirectionUrl = string.Format("{0}?", HttpContext.Current.Request.UrlReferrer.AbsolutePath);
                    
                }
                redirectionUrl = string.Format("{0}{1}={2}&{3}={4}&{5}={6}", redirectionUrl, Reference.RESERVATION_ID_QUERY_STRING, reservationNumber,
                                                                Reference.LAST_NAME_QUERY_STRING, lastName,
                                                                Reference.QueryParameterErrorCode, reservationDetails.Error.ErrorCode);
                Redirect(redirectionUrl);
            }

            if (reservationDetails != null)
            {
                reservationDetails.Message = CheckIsPastBookingMessage(reservationDetails);
                if (reservationDetails.Message == null)
                {
                    reservationDetails.Message = GetBookingTypeMessage(reservationDetails);
                }
                SetMembershipId(ref reservationDetails);
            }
            //This confirmation page data from CMS is accessed here and save in the session so that correct language context 
            //page can be accessed when controller is accessed through WebMethod where CMS language context is not available
            var scanwebConfirmCancellationPage = siteRepository.GetCMSPageData(EpiServerPageConstants.CONFIRM_CANCELLATION);
            StoreManager.Instance.GetStore(StoreType.Session).SaveData<PageData>(SCANWEB_CANCEL_CONFIRMATION_CMS_PAGE, scanwebConfirmCancellationPage);
            return reservationDetails;
        }

        public string GetGenricGuaranteePolicyText()
        {
            string guaranteePolicyText = string.Empty;
            var scanWebModifyCancelDateCMSPage = siteRepository.GetCMSPageData(EpiServerPageConstants.MODIFY_CANCEL_CHANGE_DATES);

            guaranteePolicyText = scanWebModifyCancelDateCMSPage[CMS_PAGE_GUARANTEE_INFO_KEY].ToString();

            return guaranteePolicyText;
        }

        private void SetMembershipId(ref ReservationDetails booking)
        {
            if (booking.HotelDetails != null && booking.HotelDetails.BookedRooms != null &&
                                                booking.HotelDetails.BookedRooms.Count > 0)
            {
                var loyaltyDetails = userRepository.GetLoyaltyDetails();
                if (loyaltyDetails != null)
                {
                    foreach (var bookedRoom in booking.HotelDetails.BookedRooms)
                    {
                        if (string.Equals(bookedRoom.CustomerDetails.FirstName, loyaltyDetails.FirstName, StringComparison.InvariantCultureIgnoreCase) &&
                            string.Equals(bookedRoom.CustomerDetails.LastName, loyaltyDetails.SurName))
                        {
                            bookedRoom.CustomerDetails.MembershipNumber = loyaltyDetails.UserName;
                        }
                    }
                }
            }

        }
        private MessageDetails CheckIsPastBookingMessage(ReservationDetails booking)
        {
            MessageDetails message = null;

            if (booking.HotelDetails != null && booking.HotelDetails.ArrivalDate < DateTime.Today)
            {
                message = new MessageDetails();
                message.MessageCode = Reference.OLD_BOOKING;
                message.MessageInfo = WebUtil.GetTranslatedText(Reference.OLD_BOOKING_MSG_ID);
            }

            return message;
        }

        private MessageDetails GetBookingTypeMessage(ReservationDetails booking)
        {
            MessageDetails message = null;
            if (booking.HotelDetails != null && booking.HotelDetails.BookedRooms != null &&
                                                booking.HotelDetails.BookedRooms.Count > 0)
            {
                string msg = string.Empty;
                string msgCode = string.Empty;

                int earlyBookingCount = 0;
                int flexBookingCount = 0;

                foreach (var bookedRoom in booking.HotelDetails.BookedRooms)
                {
                    flexBookingCount = bookedRoom.IsCancellable ? ++flexBookingCount : flexBookingCount;
                    earlyBookingCount = !bookedRoom.IsCancellable ? ++earlyBookingCount : earlyBookingCount;
                }

                if (earlyBookingCount == booking.HotelDetails.BookedRooms.Count)
                {
                    msgCode = Reference.EARLY_BOOKING;
                    msg = WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/EarlyHelpDescription");
                }
                else if (flexBookingCount == booking.HotelDetails.BookedRooms.Count)
                {
                    msgCode = Reference.FLEX_BOOKING;
                    msg = WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/FlexHelpDescription");
                }
                else
                {
                    msg = WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/ComboHelpDescription");
                }
                for(int i = 0; i < booking.HotelDetails.BookedRooms.Count; i++)
                {
                    if (RoomRateUtil.GetRateCategoryByRatePlanCode(booking.HotelDetails.BookedRooms[i].RateTypeId) == null)
                    {
                        if (string.Equals(booking.HotelSearch.QualifyingType, AppConstants.BLOCK_CODE_QUALIFYING_TYPE, StringComparison.InvariantCultureIgnoreCase))
                        {
                            msg = string.Empty;
                        }
                        else
                        {
                            msg = WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/RateCodeNotinCMS");
                        }
                        break;
                    }
                }
                message = new MessageDetails();
                message.MessageInfo = msg;
                message.MessageCode = msgCode;
            }

            return message;
        }

        private string GetGuaranteePolicyText(string policyCode)
        {
            string policyText = string.Empty;
            var scanwebConfirmCancellationPage = StoreManager.Instance.GetStore(StoreType.Session).GetData<PageData>(SCANWEB_CANCEL_CONFIRMATION_CMS_PAGE);

            if (scanwebConfirmCancellationPage != null)
            {
                policyText = scanwebConfirmCancellationPage[policyCode] as string;
            }
            return policyText;
        }

        public string GetConfirmMessageOverlay(bool isCancellable, string reservationNumber, string policyCode, string roomHeadingText, string cancellationControlId)
        {
            var htmlString = string.Empty;
            var policyText = string.Format("<section>{0}</section>", GetGuaranteePolicyText(policyCode));
            var pageConfig = GetPageConfig<ModifyCancelPageSection>();
            var overlayHeading = pageConfig.PageDetail.PageMessages.GetMessage(HEADING_CONFIRM_MESSAGE);
            var confirmMessage = pageConfig.PageDetail.PageMessages.GetMessage(CONFIRM_MESSAGE);
            var roomName = roomHeadingText.Substring(0, roomHeadingText.IndexOf(string.Format("- {0}", reservationNumber)));
            var overlayData = new OverlayDetails();

            confirmMessage = string.Format(confirmMessage, roomName);

            overlayData.Heading = overlayHeading;
            overlayData.Id = string.Format("confirm{0}", reservationNumber);
            overlayData.IsCachable = true;
            overlayData.Items = new List<OverlayItem>();
            overlayData.Items.Add(new OverlayItem { Value = policyText });

            var confirmationControls = new HtmlGenericControl("p");
            var closeAnchor = new HtmlAnchor();

            confirmationControls.Attributes.Add("class", "center");
            closeAnchor.HRef = "#";
            closeAnchor.ID = string.Format("close{0}", reservationNumber);
            closeAnchor.Attributes.Add("class", "button green");
            closeAnchor.Attributes.Add("onclick", "return closeOverlay();");
            closeAnchor.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage(OK_BUTTON_TEXT);
            if (isCancellable)
            {
                var confirmAnchor = new HtmlAnchor();

                overlayData.Items.Add(new OverlayItem { Value = confirmMessage });
                confirmAnchor.HRef = "#";
                confirmAnchor.ID = string.Format("confirmCancellation{0}", reservationNumber);
                confirmAnchor.Attributes.Add("class", "button green");
                confirmAnchor.Attributes.Add("onclick", string.Format("return CancelRoom({0});", cancellationControlId));
                confirmAnchor.Attributes.Add("data-reservationnumber", reservationNumber);
                confirmAnchor.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage(YES_BUTTON_TEXT);
                closeAnchor.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage(NO_BUTTON_TEXT);
                confirmationControls.Controls.Add(confirmAnchor);
            }
            confirmationControls.Controls.Add(closeAnchor);

            var overlay = new Overlay(OverlayControlType.Value, overlayData, overlayData.Id);
            htmlString = overlay.GetControlHtml(confirmationControls);

            return htmlString;
        }

        public List<KeyValueOption> GetRoomCancellationMessageOverly(CancelRoomDetails cancellationDetails, string currentLanguage)
        {
            List<KeyValueOption> returnData = new List<KeyValueOption>();
            if (IsValidCancellationSession())
            {
                string htmlString = string.Empty;
                var cancellationRoomsDetails = new List<CancelRoomDetails>();
                var pageConfig = GetPageConfig<ModifyCancelPageSection>();
                int errorCount;

                cancellationRoomsDetails.Add(cancellationDetails);
                cancellationRoomsDetails = reservationModifyRepo.CancelBooking(cancellationRoomsDetails);
                var cancellationMsg = GetCancellationMessage(cancellationRoomsDetails, out errorCount);
                var headingMsgKey = errorCount > 0 ? CANCELLED_ROOM_ERROR_HEADING : CANCELLED_ROOM_HEADING;
                var headingMsg = pageConfig.PageDetail.PageMessages.GetMessage(headingMsgKey);
                var overlayData = new OverlayDetails();

                overlayData.Heading = headingMsg;
                overlayData.Id = string.Format("cancelledRoom{0}_{1}", cancellationDetails.ReservationNumber, cancellationDetails.LegNumber);
                overlayData.IsCachable = false;
                overlayData.Items = new List<OverlayItem>();
                overlayData.Items.Add(new OverlayItem { Value = cancellationMsg });

                var confirmationControls = new HtmlGenericControl("p");
                var closeAnchor = new HtmlAnchor();
                var returnUrl = string.Empty;

                if (userRepository.IsUserAuthenticated)
                {
                    returnUrl = GetPageUrl(MobilePages.MyBookings);
                }
                else
                {
                    returnUrl = GetPageUrl(MobilePages.Start);
                }

                confirmationControls.Attributes.Add("class", "center");
                closeAnchor.HRef = "#";
                closeAnchor.ID = string.Format("{0}close", overlayData.Id);
                closeAnchor.Attributes.Add("class", "button green");
                closeAnchor.Attributes.Add("onclick", string.Format("return onRoomCancellation('{0}'); ", returnUrl));
                closeAnchor.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage(OK_BUTTON_TEXT);
                confirmationControls.Controls.Add(closeAnchor);

                var overlay = new Overlay(OverlayControlType.Value, overlayData, overlayData.Id);
                htmlString = overlay.GetControlHtml(confirmationControls, false);
                returnData.Add(new KeyValueOption { Key = Reference.MESSAGE, Value = htmlString });
            }
            else
            {
                returnData.Add(new KeyValueOption { Key = Reference.INVALID_SESSION, Value = GetSessionTimeUrl(currentLanguage) });
            }
            return returnData;
        }

        public string GetCancelBookingOverlay(bool isCancellable)
        {
            string htmlString = string.Empty;
            var pageConfig = GetPageConfig<ModifyCancelPageSection>();
            var cancellationMsg = string.Empty;
            var overlayHeading = pageConfig.PageDetail.PageMessages.GetMessage(CANCEL_BOOKING_HEADING);
            var confirmationControls = new HtmlGenericControl("p");
            var closeAnchor = new HtmlAnchor();
            var overlayData = new OverlayDetails();

            confirmationControls.Attributes.Add("class", "center");
            closeAnchor.HRef = "#";
            closeAnchor.ID = "cancelBookingClose";
            closeAnchor.Attributes.Add("class", "button green");
            closeAnchor.Attributes.Add("onclick", "return closeOverlay();");
            closeAnchor.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage(OK_BUTTON_TEXT);
            if (isCancellable)
            {
                var confirmAnchor = new HtmlAnchor();

                confirmAnchor.HRef = "#";
                confirmAnchor.ID = "cancelCompleteBooking";
                confirmAnchor.Attributes.Add("class", "button green");
                confirmAnchor.Attributes.Add("onclick", "return CancelBooking();");
                confirmAnchor.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage(YES_BUTTON_TEXT);
                closeAnchor.InnerHtml = pageConfig.PageDetail.PageMessages.GetMessage(NO_BUTTON_TEXT);
                confirmationControls.Controls.Add(confirmAnchor);
                cancellationMsg = pageConfig.PageDetail.PageMessages.GetMessage(CANCEL_BOOKING_MESSAGE);

            }
            else
            {
                cancellationMsg = string.Format("<p>{0}</p>", WebUtil.GetTranslatedText("/bookingengine/booking/ModifyBookingDates/EarlyHelpDescription"));

            }
            confirmationControls.Controls.Add(closeAnchor);
            overlayData.Heading = overlayHeading;
            overlayData.Id = "confirmBookingCancellation";
            overlayData.IsCachable = true;
            overlayData.Items = new List<OverlayItem>();
            overlayData.Items.Add(new OverlayItem { Value = cancellationMsg });

            var overlay = new Overlay(OverlayControlType.Value, overlayData, overlayData.Id);
            htmlString = overlay.GetControlHtml(confirmationControls);
            return htmlString;

        }

        public List<KeyValueOption> CancelCompleteBooking(List<CancelRoomDetails> cancellationRoomsDetails, string currentLanguage)
        {
            List<KeyValueOption> returnData = new List<KeyValueOption>();
            if (IsValidCancellationSession())
            {
                int errorCount;
                cancellationRoomsDetails = reservationModifyRepo.CancelBooking(cancellationRoomsDetails);
                var cancellationMsg = GetCancellationMessage(cancellationRoomsDetails, out errorCount);

                returnData.Add(new KeyValueOption { Key = Reference.MESSAGE, Value = HttpContext.Current.Server.HtmlEncode(cancellationMsg) });
                returnData.Add(new KeyValueOption { Key = Reference.ERROR_COUNT, Value = errorCount.ToString() });
                returnData.Add(new KeyValueOption { Key = Reference.POST_URL, Value = GetPageUrl(MobilePages.CancelConfirmation) });
                returnData.Add(new KeyValueOption { Key = Reference.RESERVATION_NUMBER, Value = cancellationRoomsDetails[0].ReservationNumber });
            }
            else
            {
                returnData.Add(new KeyValueOption { Key = Reference.INVALID_SESSION, Value = GetSessionTimeUrl(currentLanguage) });
            }

            return returnData;
        }

        private bool IsValidCancellationSession()
        {
            var sessionStore = StoreManager.Instance.GetStore(StoreType.Session);
            var bookingDetailEntities = sessionStore.GetData<List<BookingDetailsEntity>>(Reference.MODIFY_BOOKING_DETAILS_SESSION_KEY);

            return (bookingDetailEntities != null);
        }
        private string GetSessionTimeUrl(string currentLanguage)
        {
            var errorPath = siteRepository.GetPageUrl(MobilePages.Error, currentLanguage);
            return String.Format("{0}?{1}=600&lang={2}", errorPath,
                                                     Reference.QueryParameterErrorCode, currentLanguage);
        }

        private string GetCancellationMessage(List<CancelRoomDetails> cancellationRoomsDetails, out int cancellationErrorCount)
        {
            StringBuilder cancellationMessage = new StringBuilder();
            int roomCount = 0;
            cancellationErrorCount = 0;
            foreach (var cancelledRoom in cancellationRoomsDetails)
            {
                roomCount++;
                cancellationMessage.Append(string.Format("<div id='room{0}msg' >", roomCount));
                if (string.IsNullOrEmpty(cancelledRoom.ErrorMessage))
                {
                    var reservationNoMsg = string.Format("<strong>{0} {1}-{2}</strong>", WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/reservationNumber"),
                                                         cancelledRoom.ReservationNumber, cancelledRoom.LegNumber);
                    var nameMsg = string.Format("<strong>{0}:</strong> {1} {2}", WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/Name"),
                                                    cancelledRoom.FirstName, cancelledRoom.LastName);
                    var cancellationNoMsg = string.Format("<strong>{0}: {1}</strong>", WebUtil.GetTranslatedText("/bookingengine/booking/CancelledBooking/cancellationNumber"),
                                                                    cancelledRoom.CancellationNumber);
                    var emailMsg = string.Format("<strong>{0}:</strong> {1}", WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/addressemail"),
                                                     cancelledRoom.EmailAddress);
                    var smsMsg = string.Format("<strong>{0}:</strong> {1}", WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent"),
                                                       cancelledRoom.PhoneNumber);
                    var firstLine = string.Format("<p>{0} {1} {2}</p>", reservationNoMsg, nameMsg, cancellationNoMsg);
                    var secondLine = string.Format("<p>{0}</p>", emailMsg);
                    var thirdLine = string.Format("<p>{0}</p>", smsMsg);

                    cancellationMessage.Append(firstLine);
                    cancellationMessage.Append(secondLine);
                    cancellationMessage.Append(thirdLine);
                }
                else
                {
                    cancellationErrorCount++;
                    cancellationMessage.Append(string.Format("<p>{0}</p>", cancelledRoom.ErrorMessage));
                }
                cancellationMessage.Append("</div>");
            }

            return cancellationMessage.ToString();
        }

        #endregion
    }
}
