﻿namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    /// <summary>
    /// ErrorController
    /// </summary>
    public class ErrorController : BaseController
    {
        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public ErrorController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in ErrorController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public ErrorController(string requestLanguage)
            : base(requestLanguage)
        {
        }

        #endregion
    }
}