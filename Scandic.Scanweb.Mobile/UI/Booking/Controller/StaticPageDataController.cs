﻿using System;
using System.Collections.Generic;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Entity.Model;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    /// <summary>
    /// StaticPageDataController
    /// </summary>
    public class StaticPageDataController : BaseController
    {
        private const string RATING_ERR_MSG_KEY = "ratingErrorMsg";
        private const string FEEDBACK_EMAIL_FROM = "FeedbackEmailFrom";
        private const string FEEDBACK_EMAIL_BODY_FORMAT = "FeedbackEmailFormat";
        private const string RATING_PLACEHOLDER = "<<rating>>";
        private const string FEEDBACK_PLACEHOLDER = "<<feedback>>";
        private const string FEEDBACK_EMAIL_SUBJECT = "FeedbackEmailSubject";

        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public StaticPageDataController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in MasterPageController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public StaticPageDataController(string requestLanguage)
            : base(requestLanguage)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method returns static cms content data for Contact Us, Feedback pages.
        /// </summary>
        /// <returns>StaticPageModel</returns>
        public StaticPageModel GetPageData(MobilePages pageId)
        {
            StaticPageModel pageModel = new StaticPageModel();
            var pageConfig = GetPageConfig<ContentPageSection>();
            PageData pageData = siteRepository.GetCMSPageData(pageId);
            if (pageData != null)
            {
                StandardPageData staticPageData = new StandardPageData();

                staticPageData.PageIdentifier = pageData["PageIdentifier"] as string;
                staticPageData.Heading = pageData["Heading"] as string;
                staticPageData.OfficeHeading = pageData["OfficeHeading"] as string;
                staticPageData.PhoneNumber = pageData["PhoneNumber"] as string;
                staticPageData.PostAddress = pageData["PostAddress"] as string;
                staticPageData.Email = pageData["Email"] as string;
                staticPageData.VisitingAddsLabel = pageData["VisitingAddressLabel"] as string;
                staticPageData.VisitingAddress = pageData["VisitingAddress"] as string;
                staticPageData.OrganizationNumLabel = pageData["OrganizationNumberLabel"] as string;
                staticPageData.OrganizationNumber = pageData["OrganizationNumber"] as string;

                pageModel.StaticPageData = staticPageData;
                pageModel.PageHeading = pageData["Heading"] as string;
                pageModel.PageTitle = pageData["Heading"] as string;
            }
            if (pageConfig != null)
            {
                pageModel.ContentMenu = pageConfig.ContentPageMenu;
            }
            return pageModel;
        }

        /// <summary>
        /// IsPageModelValid
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="validationErrors"></param>
        /// <returns>True/False</returns>
        public override bool IsPageModelValid(BasePageModel pageModel, out List<KeyValueOption> validationErrors)
        {
            bool pageIsValid = true;
            validationErrors = new List<KeyValueOption>();
            KeyValueOption error;
            var contentPageModel = pageModel as StaticPageModel;

            if (contentPageModel != null)
            {
                if (!IsRatingValid(contentPageModel.MobileAppRating, out error))
                {
                    pageIsValid = false;
                    validationErrors.Add(error);
                }
            }
            return pageIsValid;
        }

        /// <summary>
        /// IsRatingValid
        /// </summary>
        /// <param name="rating"></param>
        /// <param name="error"></param>
        /// <returns>True/False</returns>
        private bool IsRatingValid(string rating, out KeyValueOption error)
        {
            bool isValid = true;
            error = null;
            string errorMsg = string.Empty;
            var pageConfig = GetPageConfig<ContentPageSection>();

            if (String.IsNullOrEmpty(rating))
            {
                isValid = false;
                errorMsg = pageConfig.PageDetail.PageMessages.GetMessage(RATING_ERR_MSG_KEY);
            }
            if (!isValid)
            {
                error = new KeyValueOption {Key = RATING_ERR_MSG_KEY, Value = errorMsg};
            }
            return isValid;
        }

        /// <summary>
        /// SendFeedback
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns>True/False</returns>
        public bool SendFeedback(StaticPageModel pageModel)
        {
            var feebackEmailFrom = GetGeneralConfig<ApplicationConfigSection>().GetMessage(FEEDBACK_EMAIL_FROM);
            var pageConfig = GetPageConfig<ContentPageSection>();
            string emailBody = pageConfig.PageDetail.PageMessages.GetMessage(FEEDBACK_EMAIL_BODY_FORMAT);
            string subject = pageConfig.PageDetail.PageMessages.GetMessage(FEEDBACK_EMAIL_SUBJECT);
            emailBody = emailBody.Replace(RATING_PLACEHOLDER, pageModel.MobileAppRating);
            emailBody = emailBody.Replace(FEEDBACK_PLACEHOLDER, pageModel.AboutMobileApp);
            string[] receipts = new string[1];
            receipts[0] = pageModel.StaticPageData != null ? pageModel.StaticPageData.Email : string.Empty;
            EmailEntity emailEntity = new EmailEntity();
            emailEntity.Subject = subject;
            emailEntity.Sender = feebackEmailFrom;
            emailEntity.Recipient = receipts;
            emailEntity.Body = emailBody;
            return CommunicationService.SendMail(emailEntity, null);
        }

        #endregion
    }
}