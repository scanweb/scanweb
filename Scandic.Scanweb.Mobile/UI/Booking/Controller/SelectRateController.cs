﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    /// <summary>
    /// SelectRateController
    /// </summary>
    public class SelectRateController : BaseController
    {
        #region Constants
        private const string CMP_ID = "cmpid";
        #endregion
        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public SelectRateController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in SelectRateController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public SelectRateController(string requestLanguage)
            : base(requestLanguage)
        {
        }

        #endregion

        /// <summary>
        /// GetRoomTypes
        /// </summary>
        /// <param name="currentlyRenderedRoomTypes"></param>
        /// <param name="noOfRoomTypesToDisplay"></param>
        /// <returns>List of RoomTypeResults</returns>
        public List<RoomTypeResults> GetRoomTypes(int currentlyRenderedRoomTypes, int noOfRoomTypesToDisplay)
        {
            var roomTypesToDisplay = new List<RoomTypeDetails>();
            var hotelId = CurrentContext.SelectHotelPage != null
                              ? CurrentContext.SelectHotelPage.SelectedHotelId
                              : CurrentContext.SearchHotelPage.SearchDestinationId;
            var isHotelSelctedByUser = CurrentContext.SelectHotelPage != null && CurrentContext.SelectHotelPage.IsHotelSelectedByUser;
            var selectedHotelHasOnlyPublicRates = CurrentContext.SelectHotelPage != null && CurrentContext.SelectHotelPage.SelectedHotelHasOnlyPublicRates;
            var roomTypeResults = bookingRepository.GetRoomTypes(hotelId, isHotelSelctedByUser, selectedHotelHasOnlyPublicRates);
            var roomTypeResultList = new List<RoomTypeResults>();

            if (roomTypeResults != null)
            {
                if (roomTypeResults.RoomTypes != null)
                {
                    int roomTypeCount = roomTypeResults.RoomTypes.Count;
                    int currentRoomTypeIndex = currentlyRenderedRoomTypes > roomTypeCount
                                                   ? roomTypeCount
                                                   : currentlyRenderedRoomTypes;
                    currentRoomTypeIndex = currentRoomTypeIndex < 0 ? 0 : currentRoomTypeIndex;
                    int roomTypeLimit = currentRoomTypeIndex + noOfRoomTypesToDisplay > roomTypeCount
                                            ? roomTypeCount
                                            : currentRoomTypeIndex + noOfRoomTypesToDisplay;

                    for (var index = currentRoomTypeIndex; index < roomTypeLimit; index++)
                    {
                        roomTypesToDisplay.Add(roomTypeResults.RoomTypes[index]);
                    }

                    roomTypeResults.RoomTypes = roomTypesToDisplay;
                    roomTypeResults.MoreRoomTypesToFetch = roomTypeLimit < roomTypeCount;

                    SetCssForColor(roomTypeResults.RoomTypes);
                }

                if (roomTypeResults.IsAlternateSearch)
                {
                    CurrentContext.SearchHotelPage.IsAlternateSearch = true;
                    roomTypeResults.AlternativeSearchUrl = GetPageUrl(MobilePages.SelectHotel);
                }
            }


            roomTypeResultList.Add(roomTypeResults);
            return roomTypeResultList;
        }

        /// <summary>
        /// SelectRate
        /// </summary>
        /// <param name="pageModel">SelectRateModel</param>
        public void SelectRate(SelectRateModel pageModel)
        {

            var selectedRoomRate = bookingRepository.ProcessSelectedRoomRate(pageModel.SelectedRoomTypeId,
                                                                             pageModel.SelectedRateTypeId);

            pageModel.IsVisited = true;
            CurrentContext.SelectRatePage = pageModel;

            NameValueCollection requiredIds = new NameValueCollection();
            if (CurrentContext.SearchHotelPage != null)
            {
                if (CurrentContext.SearchHotelPage.IsDeeplinkRequest)
                {
                    if (HttpContext.Current.Request.QueryString[CMP_ID] != null)
                    {
                        requiredIds.Add(CMP_ID, HttpContext.Current.Request.QueryString[CMP_ID]);
                    }
                    Redirect(GetPageUrl(MobilePages.BookingDetails), requiredIds);
                }
                else
                {
                    Redirect(GetPageUrl(MobilePages.BookingDetails));

                }
            }

        }
        
        /// <summary>
        /// SetCssForColor
        /// </summary>
        /// <param name="roomTypes">List of RoomTypeDetails</param>
        private void SetCssForColor(List<RoomTypeDetails> roomTypes)
        {
            var appConfig = GetGeneralConfig<ApplicationConfigSection>();
            bool fetchColorsFromCMS;
            bool.TryParse(appConfig.GetMessage(Reference.FetchRateTypeColorFromCMS), out fetchColorsFromCMS);

            if (!fetchColorsFromCMS)
            {
                var earlyColor = appConfig.GetMessage(Reference.EarlyColor);
                var flexColor = appConfig.GetMessage(Reference.FlexColor);
                var specialRateColor = appConfig.GetMessage(Reference.SpecialRateColor);

                foreach (var roomType in roomTypes)
                {
                    foreach (var rate in roomType.Rates)
                    {
                        string color = string.Empty;
                        switch (rate.Type)
                        {
                            case RateType.Early:
                                color = earlyColor;
                                break;
                            case RateType.Flex:
                                color = flexColor;
                                break;
                            case RateType.SpecialRate:
                                color = specialRateColor;
                                break;
                        }
                        rate.Color = color;
                    }
                }
            }
        }
    }
}