﻿using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    public class MasterPageController : BaseController
    {
        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public MasterPageController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in MasterPageController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public MasterPageController(string requestLanguage)
            : base(requestLanguage)
        {
        }

        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actualLanguage"></param>
        /// <returns></returns>
        public MasterPageSection GetPageData(string actualLanguage)
        {
            MasterPageSection masterConfig = null;
            var actualLanguageFromSession = LanguageRedirectionHelper.GetActualLanguage();
            var actualLang = string.Empty;
            if (string.IsNullOrEmpty(actualLanguageFromSession))
            {
                actualLang = actualLanguage;
                LanguageRedirectionHelper.SetActualLanguage(actualLang);
            }
            else
            {
                actualLang = actualLanguageFromSession;
            }
            if (string.IsNullOrEmpty(actualLang))
            {
                masterConfig = ConfigurationHelper.GetConfigurationSection<MasterPageSection>(LanguageRedirectionHelper.GetCurrentLanguage());
            }
            else
            {
                if (string.IsNullOrEmpty(actualLanguageFromSession))
                {
                    LanguageRedirectionHelper.SetActualLanguage(actualLang);
                }
                masterConfig = ConfigurationHelper.GetConfigurationSection<MasterPageSection>(actualLang);
                var footerMenu = masterConfig.FooterMenu;
            }
            return masterConfig;
        }

        #endregion
    }
}