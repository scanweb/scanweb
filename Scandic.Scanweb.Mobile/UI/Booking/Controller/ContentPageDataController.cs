﻿//  Description					:   HotelDetailBody                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using EPiServer.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Model;

namespace Scandic.Scanweb.Mobile.UI.Booking.Controller
{
    /// <summary>
    /// Contains members of ContentPageDataController
    /// </summary>
    public class ContentPageDataController : BaseController
    {
        #region Constructors

        /// <summary>
        /// This is default constructor, which will help indentify local
        /// language automatically.
        /// </summary>
        public ContentPageDataController()
            : this("")
        {
        }

        /// <summary>
        /// This constructor intialize the language and other repositories 
        /// to be used in MasterPageController
        /// </summary>
        /// <param name="requestLanguage"></param>
        public ContentPageDataController(string requestLanguage)
            : base(requestLanguage)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method returns static cms content data for Terms and Conditions, Price Information.
        /// </summary>
        /// <returns></returns>
        public ContentPageModel GetPageData(MobilePages pageId)
        {
            ContentPageModel pageModel = new ContentPageModel();
            PageData pageData = siteRepository.GetCMSPageData(pageId);
            if (pageData != null)
            {
                int maxItems = Reference.MAX_TERMS_AND_PRICEINFO_ITEMS;
                int maxLinks = Reference.MAX_TERMS_AND_PRICEINFO_EXTERNALLINKS;
                ContentPageData contentPageData = new ContentPageData();

                contentPageData.PageIdentifier = pageData["PageIdentifier"] as string;
                contentPageData.Heading = pageData["Heading"] as string;
                contentPageData.CloseButtonText = pageData["CloseButtonText"] as string;
                contentPageData.GenericItemsInfo = PrepareItemsInfo(pageData, maxItems);
                contentPageData.ExternalLinks = PrepareExternalLinks(pageData, maxLinks);
                pageModel.ContentPageInfo = contentPageData;
                pageModel.PageHeading = pageData["Heading"] as string;
                pageModel.PageTitle = pageData["Heading"] as string;
            }
            return pageModel;
        }

        private List<ItemsInfo> PrepareItemsInfo(PageData pageData, int maxItems)
        {
            List<ItemsInfo> genericItemsInfo = new List<ItemsInfo>();
            string itemLabel = string.Empty;
            string itemDesc = string.Empty;
            for (var count = 1; count <= maxItems; count++)
            {
                itemLabel = string.Format("Item{0}Label", count);
                itemDesc = string.Format("Item{0}Description", count);

                ItemsInfo itemInfo = new ItemsInfo();
                itemInfo.ItemLabel = pageData[itemLabel] as string;
                itemInfo.ItemDescription = pageData[itemDesc] as string;
                if (!string.IsNullOrEmpty(itemInfo.ItemDescription))
                {
                    genericItemsInfo.Add(itemInfo);
                }
            }

            return genericItemsInfo;
        }

        private List<ExternalLinks> PrepareExternalLinks(PageData pageData, int maxLinks)
        {
            List<ExternalLinks> externalLinks = new List<ExternalLinks>();

            List<ItemsInfo> genericItemsInfo = new List<ItemsInfo>();
            string linkLabel = string.Empty;
            string linkURL = string.Empty;
            for (var count = 1; count <= maxLinks; count++)
            {
                linkLabel = string.Format("ExternalLink{0}Label", count);
                linkURL = string.Format("ExternalLink{0}URL", count);

                ExternalLinks externalLink = new ExternalLinks();
                externalLink.LinkLabel = pageData[linkLabel] as string;
                externalLink.LinkURL = pageData[linkURL] as string;

                if (!string.IsNullOrEmpty(externalLink.LinkURL))
                {
                    externalLinks.Add(externalLink);
                }
            }
            return externalLinks;
        }

        #endregion
    }
}