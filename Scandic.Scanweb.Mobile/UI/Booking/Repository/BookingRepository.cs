﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Booking.Business;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Model;

namespace Scandic.Scanweb.Mobile.UI.Booking.Repository
{
    /// <summary>
    /// BookingRepository
    /// </summary>
    [Export(typeof(IBookingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class BookingRepository : IBookingRepository
    {
        #region Declaration

        private const string BOOKING_CONTEXT_KEY = "ScanWeb.Mobile.BookingContext";
        protected ISiteInfoRepository siteRepository;
        protected IUserInfoRespository userInfoRepository;
        private PaymentController paymentController;
        private NameController nameController;
        private IContentDataAccessManager contentDataAccessManager;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_siteRepository"></param>
        [ImportingConstructor]
        public BookingRepository(ISiteInfoRepository _siteRepository)
        {
            siteRepository = _siteRepository;
            userInfoRepository = new UserInfoRepository();
            paymentController = new PaymentController();
            nameController = new NameController();
            contentDataAccessManager = new ContentDataAccessManager();

        }

        #endregion

        #region IBookingRepository Members

        /// <summary>
        /// StartBookingProcess
        /// </summary>
        /// <param name="bookingProcess"></param>
        /// <returns>process url</returns>
        public String StartBookingProcess(BookingProcess bookingProcess)
        {
            string processUrl = siteRepository.GetPageUrl(MobilePages.Search);
            MiscellaneousSessionWrapper.BookingContext = new BookingContext();
            switch (bookingProcess)
            {
                case BookingProcess.ClaimRewardNights:
                    if (!userInfoRepository.IsUserAuthenticated)
                    {
                        processUrl = string.Format("{0}?{1}={2}", siteRepository.GetPageUrl(MobilePages.SignIn),
                                                   Reference.ReturnUrlQueryString, processUrl);
                    }
                    break;
                default:
                    break;
            }

            CurrentContext.CurrentBookingProcess = bookingProcess;

            return processUrl;
        }

        public BookingContext CurrentContext
        {
            get { return MiscellaneousSessionWrapper.BookingContext as BookingContext; }
        }

        /// <summary>
        /// IsPageInOrder
        /// </summary>
        /// <param name="requestBookingPageId"></param>
        /// <returns>True/False</returns>
        public bool IsPageInOrder(BookingPage requestBookingPageId)
        {
            var pageIsInOrder = false;
            if (CurrentContext != null)
            {
                var previousPage = requestBookingPageId - 1;
                var previousPageData = CurrentContext.GetPageData(previousPage);
                if (requestBookingPageId != BookingPage.SearchHotel && previousPageData != null &&
                    previousPageData.IsVisited)
                {
                    pageIsInOrder = true;
                }
            }
            else
            {
                throw new ApplicationException("Booking context not set.");
            }

            return pageIsInOrder;
        }

        /// <summary>
        /// AvailabilitySearch
        /// </summary>
        /// <param name="searchInput"></param>
        public void AvailabilitySearch(SearchHotelModel searchInput)
        {
            var bookingMgr = new BookingManager(CurrentContext);
            bookingMgr.ClearPreviousSearchResults(searchInput);
            var searchEntity = bookingMgr.PrepareHotelSearchEntity(searchInput);
            if (searchInput.SearchBy == SearchByType.City)
            {
                bookingMgr.InitiateSearch(searchEntity, false);
            }
            else
            {
                searchEntity.SelectedHotelCode = searchInput.SearchDestinationId;
                bookingMgr.InitiateHotelSearch(searchEntity);
            }
        }

        /// <summary>
        /// GetAvailableHotels
        /// </summary>
        /// <param name="currentDisplayedHotels"></param>
        /// <param name="nextSetOfHotels"></param>
        /// <param name="sortBy"></param>
        /// <returns>HotelResults</returns>
        public HotelResults GetAvailableHotels(int currentDisplayedHotels, int nextSetOfHotels, DestinationSortType sortBy)
        {
            var bookingMgr = new BookingManager(CurrentContext);
            return bookingMgr.GetAvailableHotels(sortBy, currentDisplayedHotels, nextSetOfHotels, DateTime.Now, DateTime.Now);
        }

        /// <summary>
        /// GetRoomTypes
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns>RoomTypeResults</returns>
        public RoomTypeResults GetRoomTypes(string hotelId, bool isHotelSelctedByUser, bool selectedHotelHasOnlyPublicRates)
        {
            var bookingMgr = new BookingManager(CurrentContext);
            return bookingMgr.GetAvailableRates(hotelId, isHotelSelctedByUser, selectedHotelHasOnlyPublicRates);
        }

        /// <summary>
        /// GetRateTypeToolTip
        /// </summary>
        /// <param name="rateCategoryId"></param>
        /// <returns>RateOverlayToolTip</returns>
        public RateOverlayToolTip GetRateTypeToolTip(string rateCategoryId, bool isBlockCode)
        {
            var bookingMgr = new BookingManager(CurrentContext);
            return bookingMgr.GetRateTypeToolTip(rateCategoryId, isBlockCode);
        }

        /// <summary>
        /// ProcessSelectedRoomRate
        /// </summary>
        /// <param name="roomCategoryId"></param>
        /// <param name="rateCategoryId"></param>
        /// <returns>IEnumerable</returns>
        public IEnumerable ProcessSelectedRoomRate(string roomCategoryId, string rateCategoryId)
        {
            var bookingMgr = new BookingManager(CurrentContext);
            return bookingMgr.ProcessSelectRoomRate(roomCategoryId, rateCategoryId);
        }

        /// <summary>
        /// GetSelectedHotel
        /// </summary>
        /// <returns>HotelDestination</returns>
        public HotelDestination GetSelectedHotel()
        {
            HotelDestination selectedHotel = null;

            if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null && HotelRoomRateSessionWrapper.ListHotelRoomRate.Count > 0)
            {
                selectedHotel = HotelRoomRateSessionWrapper.ListHotelRoomRate[0].HotelDestination;
            }

            return selectedHotel;
        }

        /// <summary>
        /// MakeHotelReservation
        /// </summary>
        /// <param name="bookingDetailEntity"></param>
        /// <param name="paymentInfo"> </param>
        /// <param name="isRedirectToConfirmationPage"> </param>
        /// <param name="isModifyBookingAfterNetsPaymentSuccessful"> </param>
        /// <returns>IEnumerable</returns>
        public IEnumerable MakeHotelReservation(BookingDetailModel bookingDetailEntity, PaymentInfo paymentInfo, bool isRedirectToConfirmationPage,
                                                bool isModifyBookingAfterNetsPaymentSuccessful)
        {
            var bookingMgr = new BookingManager(CurrentContext);
            return bookingMgr.MakeHotelReservation(bookingDetailEntity, paymentInfo, isRedirectToConfirmationPage, isModifyBookingAfterNetsPaymentSuccessful);
        }

        /// <summary>
        /// GetTotalPrice
        /// </summary>
        /// <param name="roomNumber"></param>
        /// <returns>TotalPrice</returns>
        public string GetTotalPrice(int roomNumber)
        {
            var bookingMgr = new BookingManager(CurrentContext);
            return bookingMgr.GetTotalPrice(roomNumber, true);
        }

        /// <summary>
        /// Registers the payment information to the Nets PSP.
        /// </summary>
        /// <returns></returns>
        public string Register(string panHash)
        {
            var bookingMgr = new BookingManager(CurrentContext);
            Reservation2SessionWrapper.PaymentOrderAmount = bookingMgr.GetTotalPrice(0, false);
            return paymentController.RegisterForMobileRequest(Reservation2SessionWrapper.PaymentOrderAmount, panHash);
        }

        /// <summary>
        /// Gets the terminal url based on the transactionId for mobile.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public string GetNetsTerminalURLForMobile(string transactionId)
        {
            return paymentController.GetNetsTerminalURLForMobile(transactionId);
        }

        /// <summary>
        /// Does a call to Nets authorization.  
        /// </summary>
        /// <param name="transactionId"></param>
        public string NetsAuthProcess(string transactionId)
        {
            return paymentController.NetsAuthProcess(transactionId, null);
        }

        /// <summary>
        /// Does a call to Nets to capture the order amount.
        /// </summary>
        /// <param name="transactionId"></param>
        public bool NetsCaptureProcess(string transactionId)
        {
            var bookingMgr = new BookingManager(CurrentContext);
            return paymentController.CaptureCall(transactionId, Reservation2SessionWrapper.PaymentOrderAmount, null);
        }

        ///<summary>
        /// Gets paymentinfo for the given transactionId.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public PaymentInfo GetPaymentInfo(string transactionId)
        {
            return paymentController.GetPaymentInfo(transactionId, null);
        }

        /// <summary>
        /// Does MakePayment call to update Opera after Nets capture.
        /// </summary>
        /// <param name="paymentInfo"></param>
        public bool MakePayment(PaymentInfo paymentInfo)
        {
            return paymentController.MakePaymentForMobile(paymentInfo);
        }

        /// <summary>
        /// Fetches all avilable credit cards of a FGP user.
        /// </summary>
        /// <param name="nameId"></param>
        /// <returns></returns>
        public Collection<CreditCardEntity> FetchFGPCreditCards(string nameId)
        {
            return nameController.FetchFGPCreditCards(nameId);
        }

        /// <summary>
        /// Process error message for Nets error code and updates the corresponding session variable.
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="errorSource"></param>
        /// <returns></returns>
        public void ProcessErrorMessageForNetsErrorCode(string errorCode, string errorSource)
        {
            paymentController.ProcessErrorMessageForNetsErrorCode(errorCode, errorSource);
        }

        /// <summary>
        /// Does a annul call to Nets.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public string NetsAnnulProcess(string transactionId)
        {
            return paymentController.NetsAnnulProcess(transactionId);
        }

        public bool GetPaymentFallback(string hotelOperaID)
        {
            return contentDataAccessManager.GetPaymentFallback(hotelOperaID);
        }

        public void ConfirmBooking(List<GuestInformationEntity> guestList)
        {
            var bookingMgr = new BookingManager(CurrentContext);
            bookingMgr.ConfirmBooking(guestList);
        }

        /// <summary>
        /// Ignore booking.
        /// </summary>
        public void IgnoreBooking()
        {
            paymentController.IgnoreBooking();
        }

        /// <summary>
        /// Clears the CurrentContext session object
        /// </summary>
        public void ClearCurrentContext()
        {
            MiscellaneousSessionWrapper.BookingContext = null;
        }
        #endregion
    }
}