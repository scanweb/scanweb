﻿using System.ComponentModel.Composition;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.Mobile.UI.Booking.Repository
{
    /// <summary>
    /// UserInfoRepository
    /// </summary>
    [Export(typeof (IUserInfoRespository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserInfoRepository : IUserInfoRespository
    {
        #region Decalration

        private NameController nameController;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public UserInfoRepository()
        {
            nameController = new NameController();
        }

        #endregion

        #region IUserInfoRespository Members

        public bool IsUserAuthenticated
        {
            get { return UserLoggedInSessionWrapper.UserLoggedIn; }
        }

        /// <summary>
        /// LoyaltyDetailsEntity
        /// </summary>
        /// <returns>LoyaltyDetailsEntity</returns>
        public LoyaltyDetailsEntity GetLoyaltyDetails()
        {
            return LoyaltyDetailsSessionWrapper.LoyaltyDetails;
        }

        /// <summary>
        /// GetProfile
        /// </summary>
        /// <returns>UserProfileEntity</returns>
        public UserProfileEntity GetProfile()
        {
            return IsUserAuthenticated ? nameController.FetchUserDetails(GetLoyaltyDetails().NameID) : null;
        }


        /// <summary>
        /// SignIn
        /// </summary>
        /// <param name="logonContext"></param>
        /// <returns>True/False</returns>
        public bool SignIn(Scandic.Scanweb.Mobile.UI.Entity.LogonContext logonContext)
        {
            bool isAuthenticated = true;
            try
            {
                var password = WebUtil.GetActualPassword(logonContext.Password, logonContext.Id);
                nameController.Login(logonContext.Id, password);
                logonContext.Password = password;
            }
            catch (OWSException)
            {
                isAuthenticated = false;
            }

            return isAuthenticated;
        }


        /// <summary>
        /// SignOut
        /// </summary>
        public void SignOut()
        {
            nameController.Logout();
        }

        /// <summary>
        /// RefreshLoyaltyDetails
        /// </summary>
        public void RefreshLoyaltyDetails()
        {
            if (IsUserAuthenticated)
            {
                var loyaltyDetails = GetLoyaltyDetails();
                if (loyaltyDetails != null)
                {
                    nameController.PopulateSession(loyaltyDetails.NameID, loyaltyDetails.UserName, true);
                }
            }
        }

        /// <summary>
        /// Determines whether or not save credit option is allowed.
        /// </summary>
        /// <returns></returns>
        public bool IsCreditCardSaveAllowed()
        {
            return nameController.CanCCSectionDisplayed();
        }

        /// <summary>
        /// Saves credit card information to user profile.
        /// </summary>
        /// <returns></returns>
        public string SaveCreditCard()
        {
            return nameController.SaveCreditCard();
        }
        #endregion
    }
}