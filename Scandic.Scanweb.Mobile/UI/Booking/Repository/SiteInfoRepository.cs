﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Business;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Common.Interface;
using Scandic.Scanweb.Mobile.UI.Configuration;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Core.Core;

namespace Scandic.Scanweb.Mobile.UI.Booking.Repository
{
    /// <summary>
    /// SiteInfoRepository
    /// </summary>
    [Export(typeof(ISiteInfoRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SiteInfoRepository : ISiteInfoRepository
    {
        #region Declaration

        private const string BASE_KEYVALUE_CONFIGURATION = "ScanWeb.Mobile.Configuration.";
        const string PAGE_DATA_CACHE_KEY = "ScanWeb.Mobile.PageData.Cache";
        #endregion

        #region ISiteInfoRepository Members

        /// <summary>
        /// GetPageConfig
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="language"></param>
        /// <returns>PageConfig</returns>
        public T GetPageConfig<T>(string language)
        {
            return GetConfig<T>(language, ConfigurationHelper.TypeOfConfig.Language);
        }

        /// <summary>
        /// GetGeneralConfig
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>GeneralConfig</returns>
        public T GetGeneralConfig<T>()
        {
            return GetConfig<T>(Reference.GeneralConfigFolder, ConfigurationHelper.TypeOfConfig.Generic);
        }

        /// <summary>
        /// GetCMSPageProperty
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="propertyId"></param>
        /// <param name="language"></param>
        /// <returns>CMSPageProperty</returns>
        public string GetCMSPageProperty(string pageId, string propertyId, string language)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the page data by CMS page id
        /// </summary>
        /// <param name="offerPageID"></param>
        /// <returns></returns>
        public PageData GetPageDataByPageID(int offerPageID)
        {
            PageData pagedata = EPiServer.DataFactory.Instance.GetPage(new PageReference(offerPageID));
            return pagedata;
        }

        /// <summary>
        /// GetPageUrl
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns>PageUrl</returns>
        public string GetPageUrl(MobilePages pageId)
        {
            return GetPageUrl(pageId, LanguageRedirectionHelper.GetCurrentLanguage());
        }

        /// <summary>
        /// GetCMSPageData
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns>PageData</returns>
        public PageData GetCMSPageData(MobilePages pageId)
        {
            if (SearchFromStartPage(pageId))
            {
                var startPage = CMSDataManager.GetPageData(GetCMSPageReference(MobilePages.Start));
                return FindPageData(startPage, GetCMSPageReference(pageId));
            }
            else
            {
                return CMSDataManager.GetPageData(GetCMSPageReference(pageId));
            }
        }

        public PageData FindPageData(PageData parentPageData, string pageId)
        {
            var pageData = GetPageDataFromCache(pageId, parentPageData.LanguageBranch);

            if (pageData == null)
            {
                var pageProperties = parentPageData.Property.Where(pp => string.Equals(pp.Type.ToString(), "PageReference", StringComparison.InvariantCultureIgnoreCase)
                                                                    && !pp.IsMetaData);

                foreach (var property in pageProperties)
                {
                    var pageReference = property.Value as PageReference;
                    var page = DataFactory.Instance.GetPage(pageReference, EPiServer.Security.AccessLevel.NoAccess);
                    if (string.Equals(property.Name, pageId, StringComparison.InvariantCultureIgnoreCase))
                    {
                        pageData = page;
                        SetPageDataInCache(pageId, pageData);
                    }
                    else
                    {
                        pageData = FindPageData(page, pageId);
                    }

                    if (pageData != null)
                    {
                        break;
                    }

                }
            }

            return pageData;
        }

        private PageData GetPageDataFromCache(string pageId, string language)
        {
            PageData pageData = null;
            var pageDataList = ScanwebCacheManager.Instance.LookInCache<Hashtable>(PAGE_DATA_CACHE_KEY);
            var pageKey = string.Format("{0}_{1}", pageId, language);

            if (pageDataList != null)
            {
                pageData = pageDataList[pageKey] as PageData;
            }

            return pageData;
        }

        private void SetPageDataInCache(string pageId, PageData page)
        {
            var pageDataList = ScanwebCacheManager.Instance.LookInCache<Hashtable>(PAGE_DATA_CACHE_KEY);

            if (pageDataList == null)
            {
                pageDataList = new Hashtable();
            }
            var pageKey = string.Format("{0}_{1}", pageId, page.LanguageBranch);
            pageDataList[pageKey] = page;

            ScanwebCacheManager.Instance.AddToCache(PAGE_DATA_CACHE_KEY, pageDataList);
        }

        private bool SearchFromStartPage(MobilePages pageId)
        {
            bool startSearch = false;
            switch (pageId)
            {

                case MobilePages.ViewBookings:
                case MobilePages.MyBookings:
                case MobilePages.ModifyCancel:
                case MobilePages.CancelConfirmation:
                case MobilePages.AllOffers:
                case MobilePages.OfferDetail:
                    startSearch = true;
                    break;
                default:
                    break;
            }
            return startSearch;
        }
        public PageData GetCMSPageData(string pageId)
        {
            return CMSDataManager.GetPageData(pageId);
        }

        /// <summary>
        /// GetCMSPageReferenceFromRootPage
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns>CMSPageReferenceFromRootPage</returns>
        public PageReference GetCMSPageReferenceFromRootPage(string pageId)
        {
            return CMSDataManager.GetPageReferenceFromRoot(pageId);
        }

        /// <summary>
        /// GetPageUrl
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="language"></param>
        /// <returns>PageUrl</returns>
        public string GetPageUrl(MobilePages pageId, string language)
        {
            string cmsPageReference = "";
            string pageUrl = "";
            cmsPageReference = GetCMSPageReference(pageId);
            if (SearchFromStartPage(pageId))
            {
                var pageData = GetCMSPageData(pageId);
                pageUrl = GetPageUrl(pageData, language, false);
            }
            else
            {
                if (string.IsNullOrEmpty(language))
                {
                    pageUrl = GetPageURL(cmsPageReference);
                }
                else
                {
                    pageUrl = CMSDataManager.GetPageRedirectUrl(cmsPageReference, language);
                }
            }
            return pageUrl;
        }

        /// <summary>
        /// GetPageUrl
        /// </summary>
        /// <param name="pageData"></param>
        /// <param name="language"></param>
        /// <param name="isScanwebPage"></param>
        /// <returns>PageUrl</returns>
        public string GetPageUrl(PageData pageData, string language, bool isScanwebPage)
        {
            return CMSDataManager.GetPageRedirectUrl(pageData, language, true);
        }

        /// <summary>
        /// GetPageURL
        /// </summary>
        /// <param name="pageReferenceKey"></param>
        /// <returns>PageURL</returns>
        private string GetPageURL(string pageReferenceKey)
        {
            string pageURL = string.Empty;

            List<MobilePageData> mobilePageDataList = GetMobileSitePageData();

            if (mobilePageDataList != null)
            {
                var pages = from mPageData in mobilePageDataList
                            where mPageData.PageReferenceKey == pageReferenceKey
                            select mPageData;

                if (pages.Count() > 0)
                {
                    pageURL = pages.First().PageURL;
                }
            }
            else
            {
                pageURL = CMSDataManager.GetPageURL(pageReferenceKey);
            }

            return pageURL;
        }

        /// <summary>
        /// GetMobileSitePageData
        /// </summary>
        /// <returns>List of MobileSitePageData</returns>
        public List<MobilePageData> GetMobileSitePageData()
        {
            List<MobilePageData> mobilePageDate = CMSDataManager.GetMobileSitePageData();
            return mobilePageDate;
        }

        /// <summary>
        /// IsIOS5
        /// </summary>
        /// <param name="userAgent"></param>
        /// <returns>True/False</returns>
        public bool IsIOS5(string userAgent)
        {
            var appConfig = GetGeneralConfig<ApplicationConfigSection>();
            var IOS5Pattern = appConfig.GetMessage(Reference.IOS5UserAgent);
            return !string.IsNullOrEmpty(Regex.Match(userAgent, IOS5Pattern).Value);
        }

        /// <summary>
        /// IsIEMobile
        /// </summary>
        /// <param name="userAgent"></param>
        /// <returns>True/False</returns>
        public bool IsIEMobile(string userAgent)
        {
            bool result = false;
            var appConfig = GetGeneralConfig<ApplicationConfigSection>();
            var IEMobileattern = appConfig.GetMessage(Reference.IEMobileUserAgent);
            //return !string.IsNullOrEmpty(Regex.Match(userAgent, IEMobileattern).Value);
            if(!string.IsNullOrEmpty(userAgent))
                result = !string.IsNullOrEmpty(Regex.Match(userAgent, IEMobileattern).Value);
            return result;
        }

        /// <summary>
        /// GetSearchDestinationList
        /// </summary>
        /// <returns>List of SearchDestination</returns>
        public List<SearchDestination> GetSearchDestinationList()
        {
            List<CityDestination> destinationList = CMSDataManager.FetchAllDestinationsForAutoSuggest();
            return SearchDestinationListMapper(destinationList);
        }

        /// <summary>
        /// SearchDestinationListMapper
        /// </summary>
        /// <param name="destinationList"></param>
        /// <returns>List of SearchDestination</returns>
        private List<SearchDestination> SearchDestinationListMapper(List<CityDestination> destinationList)
        {
            List<SearchDestination> searchDestinationList = (from city in destinationList
                                                             select new SearchDestination
                                                                        {
                                                                            id = city.OperaDestinationId,
                                                                            name = city.Name,
                                                                            isCity = true,
                                                                            AlternateCity = city.AltCityNames,
                                                                            Coordinate = city.Coordinate,
                                                                            hotels = (from hotel in city.SearchedHotels
                                                                                      select new SearchDestinationHotel
                                                                                      {
                                                                                          id = hotel.OperaDestinationId,
                                                                                          name = hotel.Name,
                                                                                          PostalCity = hotel.PostalCity,
                                                                                          Coordinate = hotel.Coordinate
                                                                                      }).ToList()
                                                                        }).ToList();

            //Return only those cities which has 1 or more hotels
            searchDestinationList = searchDestinationList.Where(city => city.hotels != null && city.hotels.Count > 0).ToList();

            return searchDestinationList;
        }

        /// <summary>
        /// GetCountryList
        /// </summary>
        /// <returns>CountryList</returns>
        public List<KeyValueOption> GetCountryList()
        {
            var countryList = new List<KeyValueOption>();
            var countriesMap = DropDownService.GetCountryCodes();

            foreach (var countryKey in countriesMap.Keys)
            {
                var value = countriesMap[countryKey].ToString().Replace("\r\n", "").Trim();
                countryList.Add(new KeyValueOption { Key = countryKey.ToString(), Value = value });
            }

            return countryList;
        }

        /// <summary>
        /// GetPhoneCodes
        /// </summary>
        /// <returns>PhoneCodes</returns>
        public List<KeyValueOption> GetPhoneCodes()
        {
            var phoneCodes = new List<KeyValueOption>();
            var phoneCodeMap = DropDownService.GetCountryPhoneCodeMap();

            foreach (var key in phoneCodeMap.Keys)
            {
                phoneCodes.Add(new KeyValueOption { Key = key.ToString(), Value = phoneCodeMap[key].ToString() });
            }

            return phoneCodes;
        }

        /// <summary>
        /// GetCreditCardTypes
        /// </summary>
        /// <returns>CreditCardTypes</returns>
        public List<KeyValueOption> GetCreditCardTypes()
        {
            var creditCardTypes = new List<KeyValueOption>();
            var creaditCardMap = DropDownService.GetCreditCardsCodes();

            foreach (var creditCardKey in creaditCardMap.Keys)
            {
                creditCardTypes.Add(new KeyValueOption
                                        {
                                            Key = creditCardKey.ToString(),
                                            Value = creaditCardMap[creditCardKey].ToString()
                                        });
            }

            return creditCardTypes;
        }

        /// <summary>
        /// GetMonthList
        /// </summary>
        /// <returns>MonthList</returns>
        public List<KeyValueOption> GetMonthList()
        {
            var months = new List<KeyValueOption>();
            for (int monthCount = 1; monthCount <= AppConstants.TOTALMONTHS; monthCount++)
            {
                IFormatProvider cultureInfo =
                    new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
                string monthName = (new DateTime(1, monthCount, 1)).ToString("MMM", cultureInfo);
                months.Add(new KeyValueOption { Key = monthCount.ToString(), Value = monthName });
            }

            return months;
        }

        /// <summary>
        /// GetCreditCardValidYears
        /// </summary>
        /// <returns>CreditCardValidYears</returns>
        public List<KeyValueOption> GetCreditCardValidYears()
        {
            var years = new List<KeyValueOption>();
            int currentYear = DateTime.Today.Year;
            int currentMonth = DateTime.Today.Month;
            if (currentMonth == 12)
                currentYear = currentYear + 1;
            for (int yearCount = 0; yearCount < AppConstants.TOTALYEARS; yearCount++)
            {
                var year = (currentYear + yearCount).ToString();
                years.Add(new KeyValueOption { Key = year, Value = year });
            }

            return years;
        }

        public CultureInfo GetCurrentCultureInfo()
        {
            var language = LanguageRedirectionHelper.GetCurrentLanguage().ToUpper();
            string cultureCode;
            switch (language)
            {
                case LanguageConstant.LANGUAGE_SWEDISH:
                    cultureCode = "sv-SE";
                    break;
                case LanguageConstant.LANGUAGE_DANISH:
                    cultureCode = "da-DK";
                    break;
                case LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION:
                    cultureCode = "nn-NO";
                    break;
                case LanguageConstant.LANGUAGE_FINNISH:
                    cultureCode = "fi-FI";
                    break;
                default:
                    cultureCode = "en-US";
                    break;
            }

            return new CultureInfo(cultureCode);
        }
        #endregion

        #region private methods

        /// <summary>
        /// GetCMSPageReference
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns>CMSPageReference</returns>
        private string GetCMSPageReference(MobilePages pageId)
        {
            string cmsPageReference = "";
            switch (pageId)
            {
                case MobilePages.Start:
                    cmsPageReference = Reference.START_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.Search:
                    cmsPageReference = Reference.SEARCH_HOTEL_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.SearchNearby:
                    cmsPageReference = Reference.SEARCH_NEARBY_HOTEL_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.SelectHotel:
                    cmsPageReference = Reference.SELECT_HOTEL_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.SelectRate:
                    cmsPageReference = Reference.SELECT_RATE_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.BookingDetails:
                    cmsPageReference = Reference.BOOKING_DETAIL_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.BookingConfirmation:
                    cmsPageReference = Reference.BOOKING_CONFIRMATION_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.SignIn:
                    cmsPageReference = Reference.SIGNIN_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.PriceInformation:
                    cmsPageReference = Reference.PRICE_INFORMATION_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.TermsCondition:
                    cmsPageReference = Reference.TERMS_AND_CONDITIONS_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.Feedback:
                    cmsPageReference = Reference.FEEDBACK_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.ContactUs:
                    cmsPageReference = Reference.CONTACTUS_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.Error:
                    cmsPageReference = Reference.ERROR_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.ViewBookings:
                    cmsPageReference = Reference.VIEWBOOKING_PAGE_CMS_REFERENCE;
                    break;
                case MobilePages.MyBookings:
                    cmsPageReference = Reference.MYBOOKING_PAGE_CMS_REFRENCE;
                    break;
                case MobilePages.ModifyCancel:
                    cmsPageReference = Reference.MODIFY_CANCEL_PAGE_CMS_REFRENCE;
                    break;
                case MobilePages.CancelConfirmation:
                    cmsPageReference = Reference.CANCEL_CONFIRMATION_PAGE_CMS_REFRENCE;
                    break;
                case MobilePages.AllOffers:
                    cmsPageReference = Reference.ALL_MOBILE_OFFERS;
                    break;
                case MobilePages.OfferDetail:
                    cmsPageReference = Reference.MOBILE_OFFER_DETAIL;
                    break;
                default:
                    break;
            }
            return cmsPageReference;
        }

        /// <summary>
        /// GetConfig
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reletiveFolder"></param>
        /// <param name="configType"></param>
        /// <returns>Config</returns>
        private T GetConfig<T>(string reletiveFolder, ConfigurationHelper.TypeOfConfig configType)
        {
            T pageConfig = default(T);
            pageConfig = GetCatchedConfig<T>(reletiveFolder);
            if (pageConfig == null)
            {
                switch (configType)
                {
                    case ConfigurationHelper.TypeOfConfig.Generic:
                        pageConfig = ConfigurationHelper.GetConfigurationSection<T>();
                        break;
                    case ConfigurationHelper.TypeOfConfig.Language:
                        pageConfig = ConfigurationHelper.GetConfigurationSection<T>(reletiveFolder);
                        break;
                    default:
                        break;
                }
                SetConfigInCache<T>(pageConfig, reletiveFolder);
            }
            return pageConfig;
        }

        /// <summary>
        /// GetCatchedConfigKey
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reletiveFolder"></param>
        /// <returns>CatchedConfigKey</returns>
        private string GetCatchedConfigKey<T>(string reletiveFolder)
        {
            return string.Format("{0}.{1}.{2}", BASE_KEYVALUE_CONFIGURATION, reletiveFolder, typeof(T).Name);
        }

        /// <summary>
        /// GetCatchedConfig
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reletiveFolder"></param>
        /// <returns>CatchedConfig</returns>
        private T GetCatchedConfig<T>(string reletiveFolder)
        {
            return ScanwebCacheManager.Instance.LookInCache<T>(GetCatchedConfigKey<T>(reletiveFolder));
        }

        /// <summary>
        /// SetConfigInCache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="catchItem"></param>
        /// <param name="reletiveFolder"></param>
        private void SetConfigInCache<T>(T catchItem, string reletiveFolder)
        {
            ScanwebCacheManager.Instance.AddToCache(GetCatchedConfigKey<T>(reletiveFolder), catchItem);
        }

        #endregion
    }
}