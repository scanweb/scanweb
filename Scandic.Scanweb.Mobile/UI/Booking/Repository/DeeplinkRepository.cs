﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using System.Collections.Specialized;
using System.Text;

namespace Scandic.Scanweb.Mobile.UI.Booking.Repository
{
    /// <summary>
    /// DeeplinkRepository
    /// </summary>
    [Export(typeof(IDeeplinkRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DeeplinkRepository : IDeeplinkRepository
    {
        #region Declaration

        private const string HOTEL_CODE = "HO";
        private const string CITY_CODE = "CO";
        private const string ARIVAL_DAY = "AD";
        private const string ARIVAL_MONTH = "AM";
        private const string ARIVAL_YEAR = "AY";
        private const string DEPARTURE_DAY = "DD";
        private const string DEPARTURE_MONTH = "DM";
        private const string DEPARTURE_YEAR = "DY";
        private const string NUMBER_OF_NIGHTS = "NN";
        private const string NUMBER_OF_ADULTS = "AN1";
        private const string NUMBER_OF_CHILDREN = "CN1";
        private const string CHILDREN_AGE = "CA1";
        private const string CHILDREN_BEDTYPE = "CB1";
        private const string SPECIAL_CODE = "SC";
        private const string CORPORATE_CODE = "NC";
        private const string BONUS_CHECK = "BC";
        private IBookingRepository bookngRepository;
        private ISiteInfoRepository siteInfoRepository;
        private SearchHotelController controller;
        private const string CIPB = "CIPB";
        private const string CRIB = "CRIB";
        private const string XBED = "XBED";
        private const string VOUCHER_CODE = "VC";
        private const string BLOCK_CODE = "RB";
        private const string ROOM_TYPE = "RO1";
        private const string RATE_NAME = "RA1";
        private const string PARTNER_ID = "PID";
        private const string CMP_ID = "cmpid";
        private const string RESERVATION_ID = "RID";
        private const string LAST_NAME = "LNM";
        private const string FRESH_SEARCH = "FS";
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_bookngRepository"></param>
        [ImportingConstructor]
        public DeeplinkRepository(IBookingRepository _bookngRepository, ISiteInfoRepository _siteInfoRepository)
        {
            bookngRepository = _bookngRepository;
            siteInfoRepository = _siteInfoRepository;
            controller = new SearchHotelController();
        }

        #endregion

        #region IDeeplinkRepository Members

        private bool IsViewModifyDeeplink(HttpRequest request)
        {
            bool isVMDeeplink = false;
            if (!string.IsNullOrEmpty(request.QueryString[RESERVATION_ID]) && !string.IsNullOrEmpty(request.QueryString[LAST_NAME]))
            {
                isVMDeeplink = true;
            }
            return isVMDeeplink;
        }

        private NameValueCollection AddQueryStrings(HttpRequest request)
        {
            NameValueCollection requiredIds = new NameValueCollection();

            if (request.QueryString[RESERVATION_ID] != null)
            {
                requiredIds.Add(RESERVATION_ID, request.QueryString[RESERVATION_ID]);
            }
            if (request.QueryString[LAST_NAME] != null)
            {
                requiredIds.Add(LAST_NAME, request.QueryString[LAST_NAME]);
            }
            if (request.QueryString[CMP_ID] != null)
            {
                requiredIds.Add(CMP_ID, request.QueryString[CMP_ID]);
            }
            if (request.QueryString[ROOM_TYPE] != null)
            {
                requiredIds.Add(ROOM_TYPE, request.QueryString[ROOM_TYPE]);
            }
            if (request.QueryString[RATE_NAME] != null)
            {
                requiredIds.Add(RATE_NAME, request.QueryString[RATE_NAME]);
            }
            return requiredIds;

        }

        /// <summary>
        /// IsDeeplinkRequest
        /// </summary>
        /// <param name="request"></param>
        /// <param name="deeplinkPath"></param>
        /// <returns>True/False</returns>
        public bool IsDeeplinkRequest(HttpRequest request, out string deeplinkPath)
        {
            bool isDeeplinkRequest = false;
            deeplinkPath = "";
            if (HasDeeplinkValues(request))
            {
                isDeeplinkRequest = true;
                //Logic for View/Modify reservatiopn deeplink
                if (IsViewModifyDeeplink(request))
                {
                    deeplinkPath = siteInfoRepository.GetPageUrl(MobilePages.ViewBookings);
                    NameValueCollection requiredIds = AddQueryStrings(request);
                    deeplinkPath = controller.GetFormatedUrl(deeplinkPath, requiredIds);
                }
                else //other than view/modify reservation via deeplink
                {
                    bool IsRequestSourceValidForFAndFDNumber = true;
                    var model = GetSearchHotelModel(request);
                    if (model != null)
                    {
                        var partnerId = request.QueryString[PARTNER_ID];
                        if (!string.IsNullOrEmpty(partnerId))
                        {
                            HygieneSessionWrapper.PartnerID = partnerId;
                        }
                        deeplinkPath = bookngRepository.StartBookingProcess(BookingProcess.BookARoom);
                        if (ValidateDeepLinkParameter(request) && (!string.Equals(request.QueryString[FRESH_SEARCH], "1") &&
                            !string.Equals(request.QueryString[FRESH_SEARCH], "2")))
                        {
                            var result = PerformSearch(model, out IsRequestSourceValidForFAndFDNumber);
                            if (result)
                            {
                                var resultPage = MobilePages.SelectHotel;
                                if (model.SearchBy == SearchByType.Hotel)
                                {
                                    resultPage = MobilePages.SelectRate;
                                }
                                deeplinkPath = siteInfoRepository.GetPageUrl(resultPage);
                            }
                        }

                        NameValueCollection requiredIds = AddQueryStrings(request);
                        deeplinkPath = controller.GetFormatedUrl(deeplinkPath, requiredIds);
                        if (string.Equals(request.QueryString[FRESH_SEARCH], "2"))
                            deeplinkPath = string.Format("{0}{1}", deeplinkPath, "?FS=2");
                        bookngRepository.CurrentContext.SearchHotelPage = model;

                        if (!IsRequestSourceValidForFAndFDNumber)
                        {
                            if (deeplinkPath.Contains("?"))
                                deeplinkPath = string.Format("{0}{1}", deeplinkPath, "&ErrCode=D1");
                            else
                                deeplinkPath = string.Format("{0}{1}", deeplinkPath, "?ErrCode=D1");
                        }
                    }
                }
            }

            return isDeeplinkRequest;
        }

        private bool ValidateDeepLinkParameter(HttpRequest request)
        {
            bool result = true;
            if (!string.IsNullOrEmpty(request.QueryString[VOUCHER_CODE]) ||
                  !string.IsNullOrEmpty(request.QueryString[BLOCK_CODE]))
            {
                result = false;
            }
            return result;

        }


        #endregion

        /// <summary>
        /// GetSearchHotelModel
        /// </summary>
        /// <param name="request"></param>
        /// <returns>SearchHotelModel</returns>
        private SearchHotelModel GetSearchHotelModel(HttpRequest request)
        {
            var model = new SearchHotelModel();

            model.SearchDestination = ProcessSearchDestination(request);

            string bookingCode = string.Empty;
            DateTime? checkInDate;
            DateTime? checkOutDate;
            bool isAgeValid;
            model.NumberOfAdults = ProcessNumberOfAdult(request);
            model.NumberOfChildren = ProcessNumberOfChildren(request);
            model.ChildrenInformation = ProcessChildrenInformation(request, model.NumberOfChildren, out isAgeValid);
            model.Offer = GetBookingOfferNBookingCode(request, out bookingCode);
            model.BookingCode = bookingCode;
            ProcessDates(request, out checkInDate, out checkOutDate);
            model.CheckInDate = checkInDate;
            model.CheckOutDate = checkOutDate;
            model.IsDeeplinkRequest = true;
            model.IsAgeInputInvalid = !isAgeValid;
            return model;
        }




        /// <summary>
        /// HasDeeplinkValues
        /// </summary>
        /// <param name="request"></param>
        /// <returns>True/False</returns>
        private bool HasDeeplinkValues(HttpRequest request)
        {
            bool hasDeeplinkValues = false;

            if (!string.IsNullOrEmpty(request.QueryString[HOTEL_CODE]) ||
                !string.IsNullOrEmpty(request.QueryString[CITY_CODE]) ||
                !string.IsNullOrEmpty(request.QueryString[ARIVAL_DAY]) ||
                !string.IsNullOrEmpty(request.QueryString[ARIVAL_MONTH]) ||
                !string.IsNullOrEmpty(request.QueryString[ARIVAL_YEAR]) ||
                !string.IsNullOrEmpty(request.QueryString[DEPARTURE_DAY]) ||
                !string.IsNullOrEmpty(request.QueryString[DEPARTURE_MONTH]) ||
                !string.IsNullOrEmpty(request.QueryString[DEPARTURE_YEAR]) ||
                !string.IsNullOrEmpty(request.QueryString[NUMBER_OF_NIGHTS]) ||
                !string.IsNullOrEmpty(request.QueryString[NUMBER_OF_ADULTS]) ||
                !string.IsNullOrEmpty(request.QueryString[SPECIAL_CODE]) ||
                !string.IsNullOrEmpty(request.QueryString[CORPORATE_CODE]) ||
                !string.IsNullOrEmpty(request.QueryString[BONUS_CHECK]) ||
                !string.IsNullOrEmpty(request.QueryString[NUMBER_OF_CHILDREN]) ||
                !string.IsNullOrEmpty(request.QueryString[CHILDREN_AGE]) ||
                !string.IsNullOrEmpty(request.QueryString[CHILDREN_BEDTYPE]) ||
                !string.IsNullOrEmpty(request.QueryString[VOUCHER_CODE]) ||
                !string.IsNullOrEmpty(request.QueryString[BLOCK_CODE]) ||
                !string.IsNullOrEmpty(request.QueryString[ROOM_TYPE]) ||
                !string.IsNullOrEmpty(request.QueryString[RATE_NAME]) ||
                !string.IsNullOrEmpty(request.QueryString[RESERVATION_ID]) ||
                !string.IsNullOrEmpty(request.QueryString[LAST_NAME]) ||
                !string.IsNullOrEmpty(request.QueryString[FRESH_SEARCH])
                )
            {
                hasDeeplinkValues = true;
            }

            return hasDeeplinkValues;
        }

        /// <summary>
        /// ProcessDates
        /// </summary>
        /// <param name="request"></param>
        /// <param name="checkInDate"></param>
        /// <param name="checkOutDate"></param>
        private void ProcessDates(HttpRequest request, out DateTime? checkInDate, out DateTime? checkOutDate)
        {
            DateTime dateToConvert;
            checkInDate = null;
            checkOutDate = null;
            string numberOfNights = request.QueryString[NUMBER_OF_NIGHTS];
            string dateFormat = Reference.DateFormat;
            KeyValueOption error;

            checkInDate = GetArivalDate(request);
            checkOutDate = GetDepartureDate(request, checkInDate);
            if (!string.IsNullOrEmpty(numberOfNights))
            {
                int noOfNight;
                Int32.TryParse(numberOfNights, out noOfNight);
                if (noOfNight > 0 && noOfNight < 99)
                {
                    Utilities.IsDate(checkInDate.Value.AddDays(noOfNight).ToString(dateFormat), out dateToConvert);
                    checkOutDate = dateToConvert;
                }
            }

            if (!controller.IsCheckOutDateValid(checkInDate, checkOutDate, out error))
            {
                Utilities.IsDate(checkInDate.Value.AddDays(1).ToString(dateFormat), out dateToConvert);
                checkOutDate = dateToConvert;
            }
        }

        /// <summary>
        /// GetDepartureDate
        /// </summary>
        /// <param name="request"></param>
        /// <param name="checkInDate"></param>
        /// <returns>DateTime</returns>
        private DateTime? GetDepartureDate(HttpRequest request, DateTime? checkInDate)
        {
            DateTime dateToConvert;
            DateTime? checkOutDate = null;
            string checkOutDateString = string.Empty;
            string format = "{0}/{1}/{2}";
            string dateFormat = Reference.DateFormat;
            KeyValueOption error;

            if (!string.IsNullOrEmpty(request.QueryString[DEPARTURE_DAY]) &&
                !string.IsNullOrEmpty(request.QueryString[DEPARTURE_MONTH]) &&
                !string.IsNullOrEmpty(request.QueryString[DEPARTURE_YEAR]))
            {
                checkOutDateString = string.Format(format, request.QueryString[DEPARTURE_YEAR],
                                                   request.QueryString[DEPARTURE_MONTH],
                                                   request.QueryString[DEPARTURE_DAY]);
            }

            if (!string.IsNullOrEmpty(checkOutDateString))
            {
                if (Utilities.IsDate(checkOutDateString, out dateToConvert))
                {
                    checkOutDate = dateToConvert;
                }
                else
                {
                    checkOutDate = null;
                }
            }

            if (!checkOutDate.HasValue || !controller.IsCheckOutDateValid(checkInDate, checkOutDate, out error))
            {
                Utilities.IsDate(checkInDate.Value.AddDays(1).ToString(dateFormat), out dateToConvert);
                checkOutDate = dateToConvert;
            }

            return checkOutDate;
        }

        /// <summary>
        /// GetArivalDate
        /// </summary>
        /// <param name="request"></param>
        /// <returns>DateTime</returns>
        private DateTime? GetArivalDate(HttpRequest request)
        {
            DateTime dateToConvert;
            DateTime? checkInDate = null;
            string checkInDateString = string.Empty;
            string format = "{0}/{1}/{2}";
            string dateFormat = Reference.DateFormat;
            KeyValueOption error;

            if (!string.IsNullOrEmpty(request.QueryString[ARIVAL_DAY]) &&
                !string.IsNullOrEmpty(request.QueryString[ARIVAL_MONTH]) &&
                !string.IsNullOrEmpty(request.QueryString[ARIVAL_YEAR]))
            {
                checkInDateString = string.Format(format, request.QueryString[ARIVAL_YEAR],
                                                  request.QueryString[ARIVAL_MONTH], request.QueryString[ARIVAL_DAY]);
            }
            if (!string.IsNullOrEmpty(checkInDateString))
            {
                if (Utilities.IsDate(checkInDateString, out dateToConvert))
                {
                    checkInDate = dateToConvert;
                }
                else
                {
                    checkInDate = null;
                }
            }

            if (!checkInDate.HasValue || !controller.IsCheckInDateValid(checkInDate, out error))
            {
                Utilities.IsDate(DateTime.Now.ToString(dateFormat), out dateToConvert);
                checkInDate = dateToConvert;
            }

            return checkInDate;
        }

        /// <summary>
        /// GetBookingOfferNBookingCode
        /// </summary>
        /// <param name="request"></param>
        /// <param name="bookingCode"></param>
        /// <returns>BookingOffer</returns>
        private BookingOffer GetBookingOfferNBookingCode(HttpRequest request, out string bookingCode)
        {
            BookingOffer bookingOffer = BookingOffer.None;
            bookingCode = string.Empty;
            var specialCode = request.QueryString[SPECIAL_CODE];
            var corporateCode = request.QueryString[CORPORATE_CODE];
            var bonusCheck = request.QueryString[BONUS_CHECK];

            if (!string.IsNullOrEmpty(corporateCode))
            {
                bookingCode = corporateCode;
                bookingOffer = BookingOffer.BookingCode;
            }
            else if (string.Equals(bonusCheck, "Y", StringComparison.InvariantCultureIgnoreCase))
            {
                bookingOffer = BookingOffer.BonusCheques;
            }
            else if (!string.IsNullOrEmpty(specialCode))
            {
                bookingCode = specialCode;
                bookingOffer = BookingOffer.BookingCode;
            }
            return bookingOffer;
        }

        /// <summary>
        /// ProcessNumberOfAdult
        /// </summary>
        /// <param name="request"></param>
        /// <returns>ProcessNumberOfAdult</returns>
        private int? ProcessNumberOfAdult(HttpRequest request)
        {
            int? noOfAdults = 1;

            if (!string.IsNullOrEmpty(request.QueryString[NUMBER_OF_ADULTS]))
            {
                int numberOfAdults;
                Int32.TryParse(request.QueryString[NUMBER_OF_ADULTS], out numberOfAdults);
                if (numberOfAdults > 0 && numberOfAdults < 7)
                {
                    noOfAdults = numberOfAdults;
                }
            }

            return noOfAdults;
        }
        /// <summary>
        /// ProcessNumberOfChildren
        /// </summary>
        /// <param name="request"></param>
        /// <returns>NumberOfChildren</returns>
        private int? ProcessNumberOfChildren(HttpRequest request)
        {
            int? noOfChildren = 0;

            if (!string.IsNullOrEmpty(request.QueryString[NUMBER_OF_CHILDREN]))
            {
                int numberOfChildren;
                Int32.TryParse(request.QueryString[NUMBER_OF_CHILDREN], out numberOfChildren);
                if (numberOfChildren >= 0 && numberOfChildren <= 5)
                {
                    noOfChildren = numberOfChildren;
                }
                else
                    noOfChildren = 0;
            }

            return noOfChildren;
        }

        /// <summary>
        /// ProcessChildrenInformation
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Children Information</returns>
        private List<ChildrenInfo> ProcessChildrenInformation(HttpRequest request, int? numberOfChildren, out bool isAgeValuesValid)
        {
            List<ChildrenInfo> childrenInfo = null;
            isAgeValuesValid = true;
            if (numberOfChildren != null)
            {
                if (numberOfChildren.HasValue && numberOfChildren.Value > 0 && numberOfChildren.Value <= 5)
                {
                    childrenInfo = new List<ChildrenInfo>();
                    for (int i = 1; i <= numberOfChildren.Value; i++)
                    {
                        ChildrenInfo child = new ChildrenInfo();
                        if (!string.IsNullOrEmpty(request.QueryString[CHILDREN_AGE + i]))
                        {
                            int age;
                            Int32.TryParse(request.QueryString[CHILDREN_AGE + i], out age);
                            if (age >= 0 && age <= 12)
                            {
                                child.Age = age;
                                child.Index = i;
                            }
                            else
                            {
                                child.Age = 0;
                                child.Index = i;
                            }

                            var bedTypeInput = request.QueryString[CHILDREN_BEDTYPE + i];
                            switch (bedTypeInput)
                            {
                                case CIPB:
                                    child.BedType = WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/sharingbed");
                                    break;
                                case CRIB:
                                    child.BedType = WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/crib");
                                    break;
                                case XBED:
                                    child.BedType = WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/extrabed");
                                    break;
                                default:
                                    if (child.Age >= 0 && child.Age <= 2)
                                    {
                                        child.BedType = WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/crib");
                                    }
                                    else
                                    {
                                        child.BedType = WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/accommodationtypes/extrabed");
                                    }
                                    break;
                            }


                        }
                        else
                        {
                            isAgeValuesValid = false;
                        }
                        childrenInfo.Add(child);
                    }
                }
            }
            return childrenInfo;

        }
        /// <summary>
        /// ProcessSearchDestination
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Processed SearchDestination</returns>
        private string ProcessSearchDestination(HttpRequest request)
        {
            string searchDestination = string.Empty;
            var hotelCode = request.QueryString[HOTEL_CODE];
            var cityCode = request.QueryString[CITY_CODE];

            if (!string.IsNullOrEmpty(hotelCode))
            {
                searchDestination = GetHotelName(hotelCode);
            }

            if (string.IsNullOrEmpty(searchDestination))
            {
                searchDestination = GetCityName(cityCode);
            }
            return searchDestination;
        }

        /// <summary>
        /// GetHotelName
        /// </summary>
        /// <param name="hotelCode"></param>
        /// <returns>HotelName</returns>
        private string GetHotelName(string hotelCode)
        {
            var hotelName = (from city in controller.GetSearchDestinationList()
                             from hotel in city.hotels
                             where string.Equals(hotel.id, hotelCode, StringComparison.InvariantCultureIgnoreCase)
                             select hotel.name).FirstOrDefault();
            return hotelName;
        }

        /// <summary>
        /// GetCityName
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns>CityName</returns>
        private string GetCityName(string cityCode)
        {
            var cityName = (from city in controller.GetSearchDestinationList()
                            where string.Equals(city.id, cityCode, StringComparison.InvariantCultureIgnoreCase)
                            select city.name).FirstOrDefault();
            return cityName;
        }

        private bool PerformSearch(SearchHotelModel searchModel, out bool IsRequestSourceValidForFAndFDNumber)
        {
            IsRequestSourceValidForFAndFDNumber = true;
            bool retValue = false;
            List<KeyValueOption> validationErrors = new List<KeyValueOption>();

            if (controller.IsPageModelValid(searchModel, out validationErrors))
            {
                controller.PerformSearch(searchModel);
                retValue = true;
            }

            foreach (KeyValueOption kVal in validationErrors)
            {
                if (string.Equals(kVal.Value, controller.GetMessageFromXml("InvalidSourceForFamilyAndFriendsDNumberErrorMsg"), StringComparison.InvariantCultureIgnoreCase))
                    IsRequestSourceValidForFAndFDNumber = false;
            }

            return retValue;
        }
    }
}