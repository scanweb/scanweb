﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Common.Interface;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Common;
using System.ComponentModel.Composition;
using Scandic.Scanweb.Mobile.UI.Booking.Business;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.Mobile.UI.Booking.Repository
{
    [Export(typeof(IReservationRetrievalRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ReservationRetiervalRepository : IReservationRetrievalRepository
    {
        #region Declaration
        private IStore sessionStore;
        private const string USER_BOOKINGS_KEY = "Mobile.UserBookingKey";
        private const string USER_TRANSACTION_KEY = "Mobile.UserTransactionKey";
        #endregion

        public ReservationRetiervalRepository()
        {
            sessionStore = StoreManager.Instance.GetStore(StoreType.Session);
        }


        #region IReservationRetiervalRepository Members

        public ReservationDetails FindBooking(string reservationNumber, string lastName, bool redirectOnError)
        {
            var reservationRetrievalMgr = new ReservationRetrievalManager();
            var bookings = reservationRetrievalMgr.GetReservationDetails(reservationNumber);
            var error = CheckError(bookings, lastName, redirectOnError);
            if (bookings == null)
            {
                bookings = new ReservationDetails();
            }
            bookings.Error = error;
            return bookings;
        }

        public List<UserReservation> GetFutureBookings(string nameId, int currentlyDisplayedBookings,
                                                int noOfBookingsToDisplay, BookingSortType sortType,
                                                SortOrder sortOrder, bool fetchLatestBookings,
                                                out bool moreResultsToFetch)
        {
            List<UserReservation> futureBookings = null;
            var sessionKey = string.Format("{0}_{1}", USER_BOOKINGS_KEY, nameId);
            var userBookings = GetUserBookings(nameId, fetchLatestBookings);
            moreResultsToFetch = false;
            if (userBookings != null && userBookings.FutureBookings != null)
            {
                userBookings.FutureBookings = SortBookings(userBookings.FutureBookings, sortType, sortOrder);
                sessionStore.SaveData<UserReservations>(sessionKey, userBookings);
                futureBookings = GetBookingsSet(userBookings.FutureBookings, currentlyDisplayedBookings, noOfBookingsToDisplay);
                moreResultsToFetch = userBookings.FutureBookings.Count > (currentlyDisplayedBookings + futureBookings.Count);
            }
            return futureBookings;
        }
        
        public UserTransactions GetUserHistory(string nameId, int currentlyDisplayedTransactions,
                                                int noOfTransactionsToDisplay, TranscationSortType sortType,
                                                SortOrder sortOrder, bool fetchLatestTransaction)
        {
            var userHistory = new UserTransactions();
            var sessionKey = string.Format("{0}_{1}", USER_TRANSACTION_KEY, nameId);
            var userTransactions = GetUserTransactions(nameId, fetchLatestTransaction);

            if (userTransactions != null)
            {
                if (userTransactions.Transactions != null)
                {
                    userTransactions.Transactions = SortTransactions(userTransactions.Transactions, sortType, sortOrder);
                    sessionStore.SaveData<UserTransactions>(sessionKey, userTransactions);
                    userHistory.Transactions = GetTransactionsSet(userTransactions.Transactions, currentlyDisplayedTransactions, noOfTransactionsToDisplay);
                    userHistory.FetchMoreTransactions = userTransactions.Transactions.Count > (currentlyDisplayedTransactions + userHistory.Transactions.Count);
                }
                userHistory.Errors = userTransactions.Errors;
            }
            return userHistory;
        }

        #endregion

        #region  Private Methods

        private UserTransactions GetUserTransactions(string nameId, bool fetchLatestTransactions)
        {
            UserTransactions userTransactions = null;
            var sessionKey = string.Format("{0}_{1}", USER_TRANSACTION_KEY, nameId);

            if (fetchLatestTransactions)
            {
                var reservationRetrievalMgr = new ReservationRetrievalManager();
                userTransactions = reservationRetrievalMgr.GetUserTransactions(nameId);
                sessionStore.SaveData<UserTransactions>(sessionKey, userTransactions);
            }
            else
            {
                userTransactions = sessionStore.GetData<UserTransactions>(sessionKey);
            }

            return userTransactions;
        }
        private UserReservations GetUserBookings(string nameId, bool fetchLatestBookings)
        {
            UserReservations userBookings = null;
            var sessionKey = string.Format("{0}_{1}", USER_BOOKINGS_KEY, nameId);

            if (fetchLatestBookings)
            {
                var reservationRetrievalMgr = new ReservationRetrievalManager();
                userBookings = reservationRetrievalMgr.GetUserBookings(nameId);
                sessionStore.SaveData<UserReservations>(sessionKey, userBookings);
            }
            else
            {
                userBookings = sessionStore.GetData<UserReservations>(sessionKey);
            }

            return userBookings;
        }

        private List<UserReservation> GetBookingsSet(List<UserReservation> bookings, int currentlyDisplayedBookings,
                                                int noOfBookingsToDisplay)
        {
            var bookingsSet = new List<UserReservation>();
            if (bookings != null)
            {
                int bookingCount = bookings.Count;
                int currentBookingIndex = currentlyDisplayedBookings > bookingCount ? bookingCount : currentlyDisplayedBookings;
                int bookingCountLimit = currentlyDisplayedBookings + noOfBookingsToDisplay > bookingCount ? bookingCount : currentlyDisplayedBookings + noOfBookingsToDisplay;

                for (var index = currentBookingIndex; index < bookingCountLimit; index++)
                {
                    bookingsSet.Add(bookings[index]);
                }
            }

            return bookingsSet;
        }

        private List<UserTransaction> GetTransactionsSet(List<UserTransaction> transactions, int currentlyDisplayedTransaction,
                                                int noOfTransactionToDisplay)
        {
            var transactionSet = new List<UserTransaction>();
            if (transactions != null)
            {
                int transactionCount = transactions.Count;
                int currentTransactionIndex = currentlyDisplayedTransaction > transactionCount ? transactionCount : currentlyDisplayedTransaction;
                int transactionCountLimit = currentlyDisplayedTransaction + noOfTransactionToDisplay > transactionCount ? transactionCount : currentlyDisplayedTransaction + noOfTransactionToDisplay;

                for (var index = currentTransactionIndex; index < transactionCountLimit; index++)
                {
                    transactionSet.Add(transactions[index]);
                }
            }

            return transactionSet;
        }
        private List<UserReservation> SortBookings(List<UserReservation> bookings, BookingSortType sortType, SortOrder sortOrder)
        {
            switch (sortType)
            {
                case BookingSortType.CheckInDate:
                    if (sortOrder == SortOrder.Ascending)
                    {
                        bookings = bookings.OrderBy(booking => booking.CheckInDate).ToList<UserReservation>();
                    }
                    else
                    {
                        bookings = bookings.OrderByDescending(booking => booking.CheckInDate).ToList<UserReservation>();
                    }
                    break;
                case BookingSortType.CheckOutDate:
                    if (sortOrder == SortOrder.Ascending)
                    {
                        bookings = bookings.OrderBy(booking => booking.CheckInDate).ToList<UserReservation>();
                    }
                    else
                    {
                        bookings = bookings.OrderByDescending(booking => booking.CheckInDate).ToList<UserReservation>();
                    }

                    break;
                default:
                    if (sortOrder == SortOrder.Ascending)
                    {
                        bookings = bookings.OrderBy(booking => booking.HotelName).ToList<UserReservation>();
                    }
                    else
                    {
                        bookings = bookings.OrderByDescending(booking => booking.HotelName).ToList<UserReservation>();
                    }
                    break;
            }
            return bookings;
        }

        private List<UserTransaction> SortTransactions(List<UserTransaction> transactions, TranscationSortType sortType, SortOrder sortOrder)
        {
            switch (sortType)
            {
                case TranscationSortType.Transaction:
                    if (sortOrder == SortOrder.Ascending)
                    {
                        transactions = transactions.OrderBy(transaction => transaction.Transaction).ToList<UserTransaction>();
                    }
                    else
                    {
                        transactions = transactions.OrderByDescending(transaction => transaction.Transaction).ToList<UserTransaction>();
                    }
                    break;
                case TranscationSortType.Points:
                    if (sortOrder == SortOrder.Ascending)
                    {
                        transactions = transactions.OrderBy(transaction => transaction.Points).ToList<UserTransaction>();
                    }
                    else
                    {
                        transactions = transactions.OrderByDescending(transaction => transaction.Points).ToList<UserTransaction>();
                    }
                    break;
                default:
                    if (sortOrder == SortOrder.Ascending)
                    {
                        transactions = transactions.OrderBy(transaction => transaction.CheckInDate).ToList<UserTransaction>();
                    }
                    else
                    {
                        transactions = transactions.OrderByDescending(transaction => transaction.CheckInDate).ToList<UserTransaction>();
                    }
                    break;
            }
            return transactions;
        }

        private ErrorDetails CheckError(ReservationDetails booking, string lastName, bool redirectOnError )
        {
            ErrorDetails error = null;
            if (booking == null || booking.Error != null)
            {
                if (booking != null && booking.Error != null && !redirectOnError)
                {
                    error = booking.Error;
                }
                else
                {
                    error = new ErrorDetails();
                    error.ErrorCode = Reference.NO_RESERVATION;
                    error.ErrorMessaage = WebUtil.GetTranslatedText(TranslatedTextConstansts.INVALID_BOOKING_NUMBER);
                }
            }
            else
            {
                error = CheckOtherErrors(booking, lastName);

            }
            if (error == null && booking != null && booking.IsSessionBooking)
            {
                error = new ErrorDetails();
                error.ErrorCode = Reference.NO_RESERVATION;
                error.ErrorMessaage = AppConstants.GUARANTEETYPE_FOR_SESSION_BOOKING;
            }
            return error;
        }

        private ErrorDetails CheckOtherErrors(ReservationDetails booking, string lastName)
        {
            ErrorDetails error = null;
            if (booking.HotelDetails != null)
            {
                if (booking.HotelDetails.BookedRooms == null || booking.HotelDetails.BookedRooms.Count == 0)
                {
                    error = new ErrorDetails();
                    error.ErrorCode = Reference.CANCELLED_RESERVATION;
                    error.ErrorMessaage = WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_ALREADY_CANCELED);
                }
                else
                {
                    string defaultLastName = WebUtil.GetTranslatedText("/bookingengine/booking/BookingSearch/Surname");
                    if (string.IsNullOrEmpty(lastName) || string.Equals(lastName, defaultLastName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        error = new ErrorDetails();
                        error.ErrorCode = Reference.EMPTY_LAST_NAME;
                        error.ErrorMessaage = WebUtil.GetTranslatedText(Reference.EMPTY_LAST_NAME_ERR_MSG_ID);
                    }
                    else
                    {
                        var lastNameFound = false;
                        foreach (var bookedRoom in booking.HotelDetails.BookedRooms)
                        {
                            if (string.Equals(bookedRoom.CustomerDetails.LastName, lastName, StringComparison.InvariantCultureIgnoreCase) ||
                                string.Equals(bookedRoom.CustomerDetails.AltLastName, lastName, StringComparison.InvariantCultureIgnoreCase))
                            {
                                lastNameFound = true;
                                break;
                            }
                        }

                        if (!lastNameFound)
                        {
                            error = new ErrorDetails();
                            error.ErrorCode = Reference.WRONG_LAST_NAME;
                            error.ErrorMessaage = WebUtil.GetTranslatedText(TranslatedTextConstansts.INVALID_SURNAME);
                        }
                    }
                }
            }
            return error;
        }
        #endregion
    }
}
