﻿using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Booking.Repository
{
    /// <summary>
    /// ScanwebSessionRepository
    /// </summary>
    public class ScanwebSessionRepository : IScanwebSessionRepository
    {
        /// <summary>
        /// Gets the status of Alternate hotels search done flag.
        /// </summary>
        /// <returns>True/False</returns>
        public bool IsAltCityHotelsSearchDone()
        {
            return AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone;
        }

        /// <summary>
        /// Gets/Sets PaymentTransactionId
        /// </summary>
        public string PaymentTransactionId 
        { 
            set { Reservation2SessionWrapper.PaymentTransactionId = value; }
            get { return Reservation2SessionWrapper.PaymentTransactionId; }
        }
    }
}