﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using System.ComponentModel.Composition;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Booking.Business;
using Scandic.Scanweb.ExceptionManager;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Common.Interface;
using Scandic.Scanweb.BookingEngine.Controller;
using System.Collections;

namespace Scandic.Scanweb.Mobile.UI.Booking.Repository
{
    [Export(typeof(IReservationModificationRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ReservationModificationRepository : IReservationModificationRepository
    {
        #region IReservationModificationRepository Members

        public List<CancelRoomDetails> CancelBooking(List<CancelRoomDetails> cancellationRooms)
        {
            var reservationModifyMgr = new ReservationModificationManager();
            var cancelledEntities = new List<CancelDetailsEntity>();
            foreach (var cancellationRoom in cancellationRooms)
            {
                var cancelDetailsEntity = ConvertToCancelDetailsEntity(cancellationRoom);
                string errorMessage = string.Empty;
                try
                {
                    reservationModifyMgr.CancelBooking(ref cancelDetailsEntity, cancellationRoom.SearchTypeCode);
                    if (cancelDetailsEntity.CancelationStatus)
                    {
                        cancellationRoom.CancellationNumber = cancelDetailsEntity.CancelNumber;
                        if (cancellationRoom.SearchTypeCode == SearchType.REDEMPTION)
                        {
                            var userRepository = DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
                            userRepository.RefreshLoyaltyDetails();
                        }
                    }
                }
                catch (OWSException owsException)
                {
                    AppLogger.LogCustomException(owsException, AppConstants.OWS_EXCEPTION);
                    switch (owsException.ErrCode)
                    {
                        case OWSExceptionConstants.BOOKING_LOCKED_BY_STAFF_MEMBER:
                            errorMessage = WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_LOCKED_BY_STAFF_MEMBER);
                            break;
                        case OWSExceptionConstants.BOOKING_ALREADY_CANCELED:
                            errorMessage = WebUtil.GetTranslatedText(TranslatedTextConstansts.BOOKING_ALREADY_CANCELED);
                            break;
                        case OWSExceptionConstants.CANCELLATION_TOO_LATE:
                            errorMessage = WebUtil.GetTranslatedText(TranslatedTextConstansts.CANCALLATION_TOO_LATE);
                            break;
                        default:
                            errorMessage = owsException.ErrMessage;
                            break;
                    }
                }
                cancellationRoom.ErrorMessage = errorMessage;
                cancelledEntities.Add(cancelDetailsEntity);
            }
            SendConfimationEmailAndSMS(cancelledEntities);
            return cancellationRooms;
        }

        #endregion

        private CancelDetailsEntity ConvertToCancelDetailsEntity(CancelRoomDetails cancelRoomDetails)
        {
            CancelDetailsEntity cancelDetailsEntity = null;
            if (cancelRoomDetails != null)
            {
                cancelDetailsEntity = new CancelDetailsEntity();

                cancelDetailsEntity.ReservationNumber = cancelRoomDetails.ReservationNumber;
                cancelDetailsEntity.LegNumber = cancelRoomDetails.LegNumber;
                cancelDetailsEntity.HotelCode = cancelRoomDetails.HotelCode;
                cancelDetailsEntity.ChainCode = AppConstants.CHAIN_CODE;
                cancelDetailsEntity.CancelType = CancelTermType.Cancel;
                cancelDetailsEntity.SMSNumber = cancelRoomDetails.PhoneNumber;

            }
            return cancelDetailsEntity;
        }

        private void SendConfimationEmailAndSMS(List<CancelDetailsEntity> cancelledEntities)
        {
            var reservationModifyMgr = new ReservationModificationManager();
            //Set session variable required for email and sms functionality to work
            SetSessionDataForEmailAndSMS(cancelledEntities);
            //Send email and SMS confirmations.
            reservationModifyMgr.SendConfimationEmailAndSMS(cancelledEntities);
        }

        private void SetSessionDataForEmailAndSMS(List<CancelDetailsEntity> cancelledEntities)
        {
            var sessionStore = StoreManager.Instance.GetStore(StoreType.Session);
            var bookingDetailEntities = sessionStore.GetData<List<BookingDetailsEntity>>(Reference.MODIFY_BOOKING_DETAILS_SESSION_KEY);

            if (bookingDetailEntities != null)
            {

                //SessionWrapper.SearchCriteria = bookingDetailEntities[0].HotelSearch;
                //SessionWrapper.SearchCriteria = GetSearchCriteria(bookingDetailEntities);
                BookingEngineSessionWrapper.AllBookingDetails = bookingDetailEntities;
                BookingEngineSessionWrapper.BookingDetails = GetFirtsActiveBooking(bookingDetailEntities);
                BookingEngineSessionWrapper.AllCancelledDetails = cancelledEntities;
                ReservationNumberSessionWrapper.ReservationNumber = cancelledEntities[0].ReservationNumber;
                //SessionWrapper.AllGuestsBookingInformations = GetGuestInfo(bookingDetailEntities);
                SetAboutRateSessionData();
                //this method must be called last as subsiquent methods refer to some session variables set above.
                SetSelectedRoomRatesInSession(bookingDetailEntities);
                if (!string.IsNullOrEmpty(bookingDetailEntities[0].HotelSearch.CampaignCode))
                {
                    BookingEngineSessionWrapper.HideARBPrice = Utility.GetHideARBPrice(bookingDetailEntities[0].HotelSearch.CampaignCode);
                }
            }
        }

        private HotelSearchEntity GetSearchCriteria(List<BookingDetailsEntity> bookingEntities)
        {
            var searchCriteria = bookingEntities[0].HotelSearch;

            for (int index = 1; index < bookingEntities.Count; index++)
            {
                if (bookingEntities[index].HotelSearch.ListRooms != null && bookingEntities[index].HotelSearch.ListRooms.Count > 0)
                {
                    searchCriteria.ListRooms.Add(bookingEntities[index].HotelSearch.ListRooms[0]);
                }
            }
            searchCriteria.RoomsPerNight = searchCriteria.ListRooms.Count;
            return searchCriteria;
        }

        private void SetSelectedRoomRatesInSession(List<BookingDetailsEntity> bookingEntities)
        {
            Hashtable selectedRoomAndRates = new Hashtable();
            var searchCriteria = BookingEngineSessionWrapper.BookingDetails.HotelSearch;
            List<HotelSearchRoomEntity> listOfRooms = new List<HotelSearchRoomEntity>();
            SortedList<string, GuestInformationEntity> allGuestsInfo = new SortedList<string, GuestInformationEntity>();
            int roomIndex = 0;

            foreach (var bookingEntity in bookingEntities)
            {
                if (bookingEntity.HotelSearch.ListRooms != null && bookingEntity.HotelSearch.ListRooms.Count > 0)
                {
                    listOfRooms.Add(bookingEntity.HotelSearch.ListRooms[0]);

                    SelectedRoomAndRateEntity selectedRoomRate = new SelectedRoomAndRateEntity();
                    RoomRateEntity roomRate = new RoomRateEntity(bookingEntity.HotelRoomRate.RoomtypeCode, bookingEntity.HotelRoomRate.RatePlanCode);
                    roomRate.BaseRate = bookingEntity.HotelRoomRate.Rate;
                    roomRate.IsSpecialRate = bookingEntity.HotelRoomRate.IsSpecialRate;
                    roomRate.TotalRate = bookingEntity.HotelRoomRate.TotalRate;
                    selectedRoomRate.RoomRates = new List<RoomRateEntity>();
                    //RK: Index mentioned as 0 here as there cannot be a list of selected rates for a room
                    selectedRoomRate.RoomRates.Add(roomRate);

                    RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(bookingEntity.HotelRoomRate.RatePlanCode);
                    RoomCategory roomCategory = RoomRateUtil.GetRoomCategory(bookingEntity.HotelRoomRate.RoomtypeCode);
                    //RK: Reservation 2.0 | For Block booking, RatePlanCode would be null
                    if (Utility.IsBlockCodeBooking)
                    {
                        selectedRoomRate.RateCategoryID = AppConstants.BLOCK_CODE_QUALIFYING_TYPE;
                    }
                    else
                    {
                        selectedRoomRate.RateCategoryID = rateCategory != null ? rateCategory.RateCategoryId : null;
                    }

                    selectedRoomRate.RoomCategoryID = roomCategory != null ? roomCategory.RoomCategoryId : null;

                    //R2.0|BugID:506292|Modify Booking details Pg| Shopping cart now showing rates
                    selectedRoomRate.SelectedRoomCategoryName = roomCategory != null ? roomCategory.RoomCategoryName : null;

                    selectedRoomAndRates.Add(roomIndex, selectedRoomRate);
                    roomIndex++;
                    //RK: Collect the guestinformation for all the users
                    allGuestsInfo[bookingEntity.GuestInformation.ReservationNumber + AppConstants.HYPHEN + bookingEntity.GuestInformation.LegNumber] = bookingEntity.GuestInformation;
                }
            }
            if (bookingEntities[0].HotelSearch.SearchingType != SearchType.REDEMPTION)
            {
                HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable = selectedRoomAndRates;
            }
            else
            {
                HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable = null;
            }
            GuestBookingInformationSessionWrapper.AllGuestsBookingInformations = allGuestsInfo;
            searchCriteria.ListRooms = listOfRooms;

            var bookingMgr = new BookingManager(null);

            //Fix for Page can't be loaded error for following scenario:
            //User booked a room in a particular hotel.Only 1 room was available.When user tried to modify the booking
            //he got page can't be loaded error because a fresh search has return zero rooms and hence exception.
            IList<BaseRoomRateDetails> listRoomRateDetails = bookingMgr.GetHotelRateDetails(searchCriteria);
            HotelRoomRateSessionWrapper.ListHotelRoomRate = listRoomRateDetails;
            SearchCriteriaSessionWrapper.SearchCriteria = searchCriteria;

        }

        //private void SetSelectedRoomRatesInSession(List<BookingDetailsEntity> bookingEntities)
        //{
        //    foreach (var bookingEntity in bookingEntities)
        //    {                
        //        var roomCategory = RoomRateUtil.GetRoomCategory(bookingEntity.HotelRoomRate.RoomtypeCode);
        //        var roomCategoryId = roomCategory.RoomCategoryId;
        //        var rateCategoryId = Utility.IsBlockCodeBooking ? bookingEntity.HotelSearch.CampaignCode : bookingEntity.HotelRoomRate.RatePlanCode;
        //        var roomNumber = Convert.ToInt32(bookingEntity.LegNumber) - 1;
        //        var roomCategoryIndex = GetRoomCategoryIndexById(roomCategoryId, roomNumber);
        //        SelectRoomUtil.SetRoomAndRateSelection(roomCategoryIndex, rateCategoryId, roomNumber.ToString());
        //    }
        //}

        private string GetRoomCategoryIndexById(string roomCategoryId, int roomNumberIndex)
        {
            string roomCategoryIndex = "0";

            IList<BaseRoomRateDetails> listHotelRoomRate = HotelRoomRateSessionWrapper.ListHotelRoomRate;
            BaseRoomRateDetails roomRateDetails = null;

            if (listHotelRoomRate != null && listHotelRoomRate.Count > 0)
            {
                roomRateDetails = listHotelRoomRate[0];
                BaseRateDisplay rateDisplay = RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                roomCategoryIndex = rateDisplay.RateRoomCategories.FindIndex(rc => rc.Id == roomCategoryId).ToString();
            }

            return roomCategoryIndex;
        }
        private void SetAboutRateSessionData()
        {
            var siteInfoRepo = DependencyResolver.Instance.GetService(typeof(ISiteInfoRepository)) as ISiteInfoRepository;
            var selectRatePage = siteInfoRepo.GetCMSPageData(EpiServerPageConstants.SELECT_RATE_PAGE);
            Reservation2SessionWrapper.AboutOurRateHeading = selectRatePage["AboutOurRatesHeading"] as string ?? string.Empty;
            Reservation2SessionWrapper.AboutOurRateDescription = selectRatePage["AboutOurRateDescription"] as string ?? string.Empty;
        }

        private SortedList<string, GuestInformationEntity> GetGuestInfo(List<BookingDetailsEntity> bookingDetails)
        {
            SortedList<string, GuestInformationEntity> sortdGuestInfo = new SortedList<string, GuestInformationEntity>();
            foreach (var bookingDetail in bookingDetails)
            {
                if (!bookingDetail.IsCancelledByUser)
                {
                    sortdGuestInfo.Add(bookingDetail.LegNumber, bookingDetail.GuestInformation);
                }
            }
            return sortdGuestInfo;
        }

        private BookingDetailsEntity GetFirtsActiveBooking(List<BookingDetailsEntity> bookingDetails)
        {
            BookingDetailsEntity activeBooking = null;
            if (bookingDetails != null)
            {
                foreach (var bookingDetail in bookingDetails)
                {
                    if (!bookingDetail.IsCancelledByUser)
                    {
                        activeBooking = bookingDetail;
                        break;
                    }
                }
            }
            return activeBooking;
        }

    }
}
