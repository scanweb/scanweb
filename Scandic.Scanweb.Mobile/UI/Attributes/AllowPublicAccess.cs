﻿using System;

namespace Scandic.Scanweb.Mobile.UI.Attributes
{
    /// <summary>
    /// This attribute will be used to check the access to the webpage. 
    /// If this attribute is set to true then user will be allowed to 
    /// access page even when he is not logged in otherwise he/she
    /// will redirected to the sign-in page.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class AllowPublicAccess : Attribute
    {
        #region Declaration

        private bool isAccessible = false;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="accessible"></param>
        public AllowPublicAccess(bool accessible)
        {
            isAccessible = accessible;
        }

        #endregion

        #region Properties           

        public bool Accessible
        {
            get { return isAccessible; }
        }

        #endregion
    }
}