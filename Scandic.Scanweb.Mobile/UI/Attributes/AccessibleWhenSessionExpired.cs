﻿//  Description					: AccessibleWhenSessionExpired                            //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.Mobile.UI.Attributes
{
    /// <summary>
    /// This attribute will be used to check the access to the webpage when user's session
    /// has expired. In most of the cases all page on session expiry will be redirected 
    /// to the session expiry page, but there will be some pages link start page, error pages
    /// that need to be provided access even in case of session expiry.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class AccessibleWhenSessionExpired : Attribute
    {
        #region Declaration

        private bool isAccessible = false;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor of AccessibleWhenSessionExpired class
        /// </summary>
        /// <param name="accessible"></param>
        public AccessibleWhenSessionExpired(bool accessible)
        {
            isAccessible = accessible;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets Accessible
        /// </summary>
        public bool Accessible
        {
            get { return isAccessible; }
        }

        #endregion
    }
}