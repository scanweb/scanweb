﻿using System.ComponentModel.Composition;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    /// <summary>
    /// SearchHotelTracking
    /// </summary>
    [Export(typeof(IPageTracking))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SearchHotelTracking : IPageTracking
    {
        private IUserInfoRespository userRepository;

        /// <summary>
        /// SearchHotelTracking
        /// </summary>
        public SearchHotelTracking()
        {
            userRepository =
                DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
        }

        #region IPageTracking Members

        /// <summary>
        /// GetPageTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns>TrackingDetails</returns>
        public TrackingDetails GetPageTrackingData(BookingContext currentContext, string data)
        {
            var trackingData = new TrackingDetails();

            trackingData.pageName = Reference.SearchHotelPageTrackingHeading;
            trackingData.prop34 =
                userRepository.IsUserAuthenticated
                    ? Reference.LoggedInUserTextTracking
                    : Reference.LoggedOutUserTextTracking;
            trackingData.eVar34 =
                userRepository.IsUserAuthenticated
                    ? Reference.LoggedInUserTextTracking
                    : Reference.LoggedOutUserTextTracking;

            if (!string.IsNullOrEmpty(data))
                trackingData.prop48 = data;

            return trackingData;
        }

        /// <summary>
        /// GetFunctionTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <param name="function"></param>
        /// <param name="data"></param>
        /// <returns>TrackingDetails</returns>
        public TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function, string data)
        {
            var trackingData = new TrackingDetails(function);

            trackingData.linkTrackVars = "events,eVar48";
            trackingData.linkTrackEvents = "event21";
            trackingData.events = "event21";
            switch (function)
            {
                case TrackingFunctions.ClickToCallBookWithChildren:
                    trackingData.eVar48 =
                        string.Format("Click to call : {0} : Booking with children",
                                      Reference.SearchHotelPageTrackingHeading);
                    break;
            }
            return trackingData;
        }

        /// <summary>
        /// GetPageId
        /// </summary>
        /// <returns>Page </returns>
        public Scandic.Scanweb.Mobile.UI.Booking.Interface.MobilePages GetPageId()
        {
            return MobilePages.Search;
        }

        #endregion
    }
}