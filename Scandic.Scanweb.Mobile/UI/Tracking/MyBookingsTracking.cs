﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.Composition;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;

namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    [Export(typeof(IPageTracking))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MyBookingsTracking : IPageTracking
    {
        private IUserInfoRespository userRepository;

        public MyBookingsTracking()
        {
            userRepository = DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
        }

        #region IPageTracking Members

        public TrackingDetails GetPageTrackingData(BookingContext currentContext, string data)
        {
            var trackingData = new TrackingDetails();

            trackingData.pageName = Reference.MyBookingsPageTrackingHeading;
            trackingData.prop34 = userRepository.IsUserAuthenticated ? Reference.LoggedInUserTextTracking : Reference.LoggedOutUserTextTracking;
            trackingData.eVar34 = userRepository.IsUserAuthenticated ? Reference.LoggedInUserTextTracking : Reference.LoggedOutUserTextTracking;
            return trackingData;
        }

        public TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function, string data)
        {
            return null;
        }

        public MobilePages GetPageId()
        {
            return MobilePages.MyBookings;
        }

        #endregion
    }
}
