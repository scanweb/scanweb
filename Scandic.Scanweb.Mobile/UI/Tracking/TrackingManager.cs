﻿//  Description					: TrackingManager                                         //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Linq;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    /// <summary>
    /// Contains members to facilitate tracking.
    /// </summary>
    public class TrackingManager
    {
        private BookingContext context;
        private IPageTracking tracking;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="currentContext"></param>
        /// <param name="pageId"></param>
        public TrackingManager(BookingContext currentContext, MobilePages pageId)
        {
            context = currentContext;
            tracking = GetTrackingObject(pageId);
        }

        /// <summary>
        /// Gets page tracking info
        /// </summary>
        /// <returns></returns>
        public TrackingDetails GetPageTrackingInfo(string data)
        {
            TrackingDetails trackingData = null;
            if (tracking != null)
            {
                trackingData = EncodeVariables(tracking.GetPageTrackingData(context, data));
            }
            return trackingData;
        }

        /// <summary>
        /// Gets funciton tracking data
        /// </summary>
        /// <param name="function"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public TrackingDetails GetFunctionTrackingData(TrackingFunctions function, string data)
        {
            TrackingDetails trackingData = null;
            if (tracking != null)
            {
                trackingData = EncodeVariables(tracking.GetFunctionTrackingData(context, function, data));
            }
            return trackingData;
        }

        /// <summary>
        /// Gets tracking object
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        private IPageTracking GetTrackingObject(MobilePages pageId)
        {
            IPageTracking trackingObject = null;

            var trackingObjects = DependencyResolver.Instance.GetServices(typeof (IPageTracking)).ToList<Object>();

            if (trackingObjects != null)
            {
                trackingObject = (from trackObj in trackingObjects
                                  where ((IPageTracking) trackObj).GetPageId() == pageId
                                  select trackObj).FirstOrDefault() as IPageTracking;
            }
            return trackingObject;
        }

        private TrackingDetails EncodeVariables(TrackingDetails trackingData)
        {
            var data = trackingData;
            if (data != null)
            {
                data.eVar11 = string.IsNullOrEmpty(data.eVar11) ? data.eVar11 : data.eVar11;
                data.prop11 = string.IsNullOrEmpty(data.prop11) ? data.prop11 : data.prop11;
            }
            return data;
        }
    }
}