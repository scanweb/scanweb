﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.Composition;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;


namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    [Export(typeof(IPageTracking))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ErrorPageTracking : IPageTracking
    {
        public TrackingDetails GetPageTrackingData(BookingContext currentContext, string data)
        {
            var trackingData = new TrackingDetails();
            var pageName = "AppError-Mobile";
            var pageType = "errorPage";
            

            switch (data)
            {
                case "SessionTimeOut":
                    pageName = "Session Timeout";
                    pageType = "";
                    break;
                case "SessionNotValid":
                    pageName = "AppError-Mobile-SessionInvalid";
                    break;
                case "MobileConfirmation":
                    pageName = "AppError-Mobile-Confirmation";
                    break;

            }

            trackingData.pageName = pageName;
            trackingData.pageType = pageType;
            return trackingData;
        }

        public TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function, string data)
        {
            return null;
        }

        public MobilePages GetPageId()
        {
            return MobilePages.Error;
        }
    }
}
