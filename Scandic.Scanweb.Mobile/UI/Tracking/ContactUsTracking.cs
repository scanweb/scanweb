﻿using System.ComponentModel.Composition;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    /// <summary>
    /// ContactUsTracking
    /// </summary>
    [Export(typeof (IPageTracking))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ContactUsTracking : IPageTracking
    {
        private IUserInfoRespository userRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        public ContactUsTracking()
        {
            userRepository =
                DependencyResolver.Instance.GetService(typeof (IUserInfoRespository)) as IUserInfoRespository;
        }

        #region IPageTracking Members

        /// <summary>
        /// GetPageTrackingData
        /// </summary>
        /// <param name="currentContext">BookingContext</param>
        /// <returns>TrackingDetails</returns>
        public TrackingDetails GetPageTrackingData(BookingContext currentContext, string data)
        {
            var trackingData = new TrackingDetails();

            trackingData.pageName = Reference.ContactUsPageTrackingHeading;
            trackingData.prop34 = userRepository.IsUserAuthenticated
                                      ? Reference.LoggedInUserTextTracking
                                      : Reference.LoggedOutUserTextTracking;
            trackingData.eVar34 = userRepository.IsUserAuthenticated
                                      ? Reference.LoggedInUserTextTracking
                                      : Reference.LoggedOutUserTextTracking;
            return trackingData;
        }

        /// <summary>
        /// GetFunctionTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <param name="function"></param>
        /// <param name="data"></param>
        /// <returns>TrackingDetails</returns>
        public TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function,
                                                       string data)
        {
            var trackingData = new TrackingDetails(function);

            trackingData.linkTrackVars = "events,eVar48";
            trackingData.linkTrackEvents = "event21";
            trackingData.events = "event21";
            switch (function)
            {
                case TrackingFunctions.ClickToMap:
                    trackingData.linkTrackEvents = "event4";
                    trackingData.events = "event4";
                    trackingData.eVar48 = string.Format(Reference.LinkToMapFunctionName,
                                                        Reference.ContactUsPageTrackingHeading);
                    break;
                case TrackingFunctions.ClickToCallUs:
                    trackingData.eVar48 = string.Format(Reference.ClickToCallFunctionName,
                                                        Reference.ContactUsPageTrackingHeading);
                    break;
                case TrackingFunctions.ClickToEmail:
                    trackingData.eVar48 = string.Format(Reference.EmailFunctionName,
                                                        Reference.ContactUsPageTrackingHeading);
                    break;
            }
            return trackingData;
        }

        /// <summary>
        /// GetPageId
        /// </summary>
        /// <returns>MobilePages</returns>
        public MobilePages GetPageId()
        {
            return MobilePages.ContactUs;
        }

        #endregion
    }
}