﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;

namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    /// <summary>
    /// Offer List Tracking
    /// </summary>
    [Export(typeof(IPageTracking))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class OfferListTracking : IPageTracking
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OfferListTracking()
        {
        }

        #region IPageTracking Members

        /// <summary>
        /// GetPageTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns>PageTrackingData</returns>
        public TrackingDetails GetPageTrackingData(BookingContext currentContext, string data)
        {
            TrackingDetails trackingData = null;
            //if (!string.IsNullOrEmpty(data))
            //{
            //    var values = data.Split('*');
            //    if (values != null && values.Length > 1)
            //    {
            //        trackingData = new TrackingDetails();
            //        trackingData.prop54 = string.Format("Box{0}", values[0]);
            //        trackingData.prop44 = values[1];
            //    }
            //}


            return trackingData;
        }

        /// <summary>
        /// GetFunctionTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <param name="function"></param>
        /// <param name="data"></param>
        /// <returns>FunctionTrackingData</returns>
        public TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function,
                                                       string data)
        {
            var trackingData = new TrackingDetails(function);


            return trackingData;
        }

        /// <summary>
        /// GetPageId
        /// </summary>
        /// <returns>MobilePages</returns>
        public MobilePages GetPageId()
        {
            return MobilePages.AllOffers;
        }

        #endregion
    }
}
