﻿using System.ComponentModel.Composition;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    /// <summary>
    /// Offer Details Tracking
    /// </summary>
    [Export(typeof(IPageTracking))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class OfferDetailsTracking : IPageTracking
    {

        /// <summary>
        /// Constructor
        /// </summary>
        public OfferDetailsTracking()
        {

        }

        #region IPageTracking Members

        /// <summary>
        /// GetPageTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns>PageTrackingData</returns>
        public TrackingDetails GetPageTrackingData(BookingContext currentContext, string data)
        {
            TrackingDetails trackingData = null;
            if (!string.IsNullOrEmpty(data))
            {
                var values = data.Split('*');
                if (values != null && values.Length > 1 && values.Length < 3)
                {
                    trackingData = new TrackingDetails();
                    trackingData.prop54 = string.Format("Box{0}", values[0]);
                    trackingData.prop44 = values[1];
                }
                if (values != null && values.Length > 3)
                {
                    trackingData = new TrackingDetails();
                    trackingData.prop54 = values[0];
                    trackingData.prop44 = values[1];
                    trackingData.prop50 = values[2];
                    trackingData.prop49 = values[3];
                }
                if (values != null && values.Length == 1)
                {
                    trackingData = new TrackingDetails();
                    trackingData.eVar31 = values[0];
                }
            }


            return trackingData;
        }

        /// <summary>
        /// GetFunctionTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <param name="function"></param>
        /// <param name="data"></param>
        /// <returns>FunctionTrackingData</returns>
        public TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function, string data)
        {
            return null;
        }

        /// <summary>
        /// GetPageId
        /// </summary>
        /// <returns>MobilePages</returns>
        public MobilePages GetPageId()
        {
            return MobilePages.OfferDetail;
        }
        #endregion
    }
}
