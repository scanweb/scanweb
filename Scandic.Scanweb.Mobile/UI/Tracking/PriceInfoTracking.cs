﻿//  Description					:   PriceInfoTracking                                     //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.ComponentModel.Composition;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    /// <summary>
    /// This class represents priceinfo.
    /// </summary>
    [Export(typeof (IPageTracking))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class PriceInfoTracking : IPageTracking
    {
        private IUserInfoRespository userRepository;

        /// <summary>
        /// Constructor of PriceInfoTracking
        /// </summary>
        public PriceInfoTracking()
        {
            userRepository =
                DependencyResolver.Instance.GetService(typeof (IUserInfoRespository)) as IUserInfoRespository;
        }

        #region IPageTracking Members

        /// <summary>
        /// Gets page tracking data.
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns></returns>
        public TrackingDetails GetPageTrackingData(BookingContext currentContext, string data)
        {
            var trackingData = new TrackingDetails();

            trackingData.pageName = Reference.PriceInfoPageTrackingHeading;
            trackingData.prop34 = userRepository.IsUserAuthenticated
                                      ? Reference.LoggedInUserTextTracking
                                      : Reference.LoggedOutUserTextTracking;
            trackingData.eVar34 = userRepository.IsUserAuthenticated
                                      ? Reference.LoggedInUserTextTracking
                                      : Reference.LoggedOutUserTextTracking;
            return trackingData;
        }

        /// <summary>
        /// Gets function tracking data.
        /// </summary>
        /// <param name="currentContext"></param>
        /// <param name="function"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function,
                                                       string data)
        {
            return null;
        }

        /// <summary>
        /// Gets pageID
        /// </summary>
        /// <returns></returns>
        public MobilePages GetPageId()
        {
            return MobilePages.PriceInformation;
        }

        #endregion
    }
}