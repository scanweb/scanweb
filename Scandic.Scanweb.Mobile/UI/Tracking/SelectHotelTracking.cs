﻿//  Description					:   SelectHotelTracking                                   //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.ComponentModel.Composition;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    /// <summary>
    /// SelectHotelTracking
    /// </summary>
    [Export(typeof(IPageTracking))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SelectHotelTracking : IPageTracking
    {
        private IUserInfoRespository userRepository;

        /// <summary>
        /// SelectHotelTracking
        /// </summary>
        public SelectHotelTracking()
        {
            userRepository =
                DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
        }

        #region IPageTracking Members

        /// <summary>
        /// Gets page tracking data
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns></returns>
        public TrackingDetails GetPageTrackingData(BookingContext currentContext, string data)
        {
            TrackingDetails trackingData = null;

            if (currentContext != null && currentContext.SearchHotelPage != null
                && currentContext.SearchHotelPage.TrackingPropertySearchedHotelCount != -1)
            {
                trackingData = new TrackingDetails();
                var checkInDate = currentContext.SearchHotelPage.CheckInDate;
                var checkOutDate = currentContext.SearchHotelPage.CheckOutDate;
                if (checkInDate.HasValue && checkOutDate.HasValue)
                {
                    var timeSpan = checkOutDate.Value.Subtract(checkInDate.Value);
                    trackingData.prop12 = timeSpan.Days.ToString();
                    trackingData.eVar12 = trackingData.prop12;
                    trackingData.prop14 = checkInDate.Value.ToString(Reference.IOS5DateFormat);
                    trackingData.eVar14 = trackingData.prop14;
                    trackingData.prop17 = string.Format("{0}-{1}", trackingData.prop14, checkOutDate.Value.ToString(Reference.IOS5DateFormat));
                    trackingData.eVar17 = trackingData.prop17;
                }
                
                trackingData.pageName = Reference.SelectHotelPageTrackingHeading;
                trackingData.prop34 = userRepository.IsUserAuthenticated
                                          ? Reference.LoggedInUserTextTracking
                                          : Reference.LoggedOutUserTextTracking;
                trackingData.eVar34 = userRepository.IsUserAuthenticated
                                          ? Reference.LoggedInUserTextTracking
                                          : Reference.LoggedOutUserTextTracking;
                if (currentContext.SearchHotelPage.IsSearchfromCurrentLocation == true)
                    trackingData.prop11 = currentContext.SearchHotelPage.SearchDestinationType;
                else
                    trackingData.prop11 = currentContext.SearchHotelPage.SearchDestination;
                trackingData.eVar11 = trackingData.prop11;
                trackingData.prop13 = "1";
                trackingData.eVar13 = trackingData.prop13;
                trackingData.prop15 = currentContext.SearchHotelPage.NumberOfAdults.Value.ToString();
                trackingData.eVar15 = trackingData.prop15;
                //trackingData.prop16 = "0";
                trackingData.prop16 = currentContext.SearchHotelPage.NumberOfChildren.Value.ToString();
                trackingData.eVar16 = trackingData.prop16;


                trackingData.prop18 = currentContext.SearchHotelPage.TrackingPropertySearchedHotelCount.ToString();
                trackingData.eVar18 = trackingData.prop18;
                trackingData.events = "scCheckout";
            }
            return trackingData;
        }

        /// <summary>
        /// Gets function tracking data
        /// </summary>
        /// <param name="currentContext"></param>
        /// <param name="function"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function, string data)
        {
            var trackingData = new TrackingDetails(function);

            trackingData.linkTrackVars = "events,eVar48,products";
            trackingData.linkTrackEvents = "event21";
            trackingData.products = string.Format(";{0}", data);
            trackingData.events = "event21";
            switch (function)
            {
                case TrackingFunctions.ClickToCallUs:
                    trackingData.eVar48 = string.Format(Reference.ClickToCallFunctionName,
                                                        Reference.SelectHotelPageTrackingHeading);
                    break;
                case TrackingFunctions.ClickToMap:
                    trackingData.linkTrackEvents = "event4";
                    trackingData.events = "event4";
                    trackingData.eVar48 = "";
                    break;
                case TrackingFunctions.ClickToTripAdvisorReviews:
                    trackingData.prop25 = "Tripadvisor function";
                    trackingData.prop26 = "Tripadvisor review link";
                    break;
                case TrackingFunctions.EditSearch:
                    trackingData.prop20 = "Edit Search";
                    break;
            }
            return trackingData;
        }

        /// <summary>
        /// Gets page Id
        /// </summary>
        /// <returns></returns>
        public Scandic.Scanweb.Mobile.UI.Booking.Interface.MobilePages GetPageId()
        {
            return MobilePages.SelectHotel;
        }

        #endregion
    }
}