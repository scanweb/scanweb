﻿using System.ComponentModel.Composition;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    /// <summary>
    /// FeedbackTracking
    /// </summary>
    [Export(typeof (IPageTracking))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class FeedbackTracking : IPageTracking
    {
        #region IPageTracking Members

        public TrackingDetails GetPageTrackingData(BookingContext currentContext, string data)
        {
            return null;
        }

        /// <summary>
        /// GetFunctionTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <param name="function"></param>
        /// <param name="data"></param>
        /// <returns>TrackingDetails</returns>
        public TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function,
                                                       string data)
        {
            var trackingData = new TrackingDetails(function);

            trackingData.linkTrackVars = "events,eVar48";
            trackingData.linkTrackEvents = "event4";
            trackingData.events = "event4";
            trackingData.eVar48 = "feedback form";

            return trackingData;
        }

        /// <summary>
        /// GetPageId
        /// </summary>
        /// <returns>MobilePages</returns>
        public MobilePages GetPageId()
        {
            return MobilePages.Feedback;
        }

        #endregion
    }
}