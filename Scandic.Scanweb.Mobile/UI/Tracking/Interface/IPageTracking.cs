﻿using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;

namespace Scandic.Scanweb.Mobile.UI.Tracking.Interface
{
    /// <summary>
    /// IPageTracking
    /// </summary>
    public interface IPageTracking
    {
        /// <summary>
        /// GetPageTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns>TrackingDetails</returns>
        TrackingDetails GetPageTrackingData(BookingContext currentContext, string data);

        /// <summary>
        /// GetFunctionTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <param name="function"></param>
        /// <param name="data"></param>
        /// <returns>TrackingDetails</returns>
        TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function, string data);
        
        /// <summary>
        /// GetPageId
        /// </summary>
        /// <returns>MobilePages</returns>
        MobilePages GetPageId();
    }
}