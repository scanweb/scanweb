﻿using System;
using System.ComponentModel.Composition;
using System.Text;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    /// <summary>
    /// ConfirmationTracking
    /// </summary>
    [Export(typeof (IPageTracking))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ConfirmationTracking : IPageTracking
    {
        private IUserInfoRespository userRepository;
        /// <summary>
        /// Constructor
        /// </summary>
        public ConfirmationTracking()
        {
            userRepository =
                DependencyResolver.Instance.GetService(typeof (IUserInfoRespository)) as IUserInfoRespository;
        }

        #region IPageTracking Members

        /// <summary>
        /// GetPageTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns>TrackingDetails</returns>
        public TrackingDetails GetPageTrackingData(BookingContext currentContext, string data)
        {
            var trackingData = new TrackingDetails();
            string currencyCode = string.Empty;
            string price = string.Empty;

            trackingData.pageName = Reference.ConfirmationPageTrackingHeading;
            trackingData.prop34 = userRepository.IsUserAuthenticated
                                      ? Reference.LoggedInUserTextTracking
                                      : Reference.LoggedOutUserTextTracking;
            trackingData.eVar34 = userRepository.IsUserAuthenticated
                                      ? Reference.LoggedInUserTextTracking
                                      : Reference.LoggedOutUserTextTracking;
            if (currentContext.SearchHotelPage.NumberOfChildren.Value > 0)
            {
                trackingData.events = "purchase,event17,event18";
            }
            else
            {
            trackingData.events = "purchase,event17";
            }
            trackingData.event17 = "1";
            if (currentContext != null && currentContext.BookingDetailPage != null)
            {
                var checkInDate = currentContext.SearchHotelPage.CheckInDate;
                var checkOutDate = currentContext.SearchHotelPage.CheckOutDate;
                if (checkInDate.HasValue && checkOutDate.HasValue)
                {
                    var timeSpan = checkOutDate.Value.Subtract(checkInDate.Value);
                    trackingData.eVar6 = string.Format("{0} days", timeSpan.Days.ToString());
                    trackingData.eVar4 = string.Format("{0} days", LeadTime(currentContext.SearchHotelPage.CheckInDate.Value).ToString());
                    trackingData.eVar5 = currentContext.SearchHotelPage.CheckOutDate.Value.ToString(Reference.IOS5DateFormat);
                }
                var bookingCode = currentContext.SearchHotelPage.BookingCode;
                trackingData.purchaseID = currentContext.BookingDetailPage.ReservationNumber;
                trackingData.eVar2 = !string.IsNullOrEmpty(bookingCode) ? bookingCode : string.Empty;
                
                
                trackingData.eVar8 = "1";
                trackingData.eVar25 = PaymentMethod(currentContext);
                trackingData.eVar27 = BookingTypeCode(currentContext);
                trackingData.eVar30 = currentContext.BookingDetailPage.Country;
                trackingData.eVar39 = currentContext.SelectRatePage.SelectedRateType;
                trackingData.eVar40 = userRepository.IsUserAuthenticated
                                          ? userRepository.GetLoyaltyDetails().MembershipLevel
                                          : string.Empty;
                trackingData.eVar41 = currentContext.SelectRatePage.SelectedRoomType;
                trackingData.eVar42 = GetBookingType(currentContext);
                trackingData.eVar43 = currentContext.BookingDetailPage.ReservationNumber;
                trackingData.eVar49 = GetHotelCountryCode();
                price = GetPriceAndCurrencyCode(currentContext.BookingDetailPage.TotalRate, out currencyCode,
                                                currentContext.SearchHotelPage.SearchType);
                trackingData.currencyCode = currencyCode;

                if (currentContext.SearchHotelPage.SearchType == Entity.Booking.Model.SearchType.BonusCheque ||
                    currentContext.SearchHotelPage.SearchType == Entity.Booking.Model.SearchType.Redemption)
                {
                    trackingData.eVar26 = price;
                }
                trackingData.products = GetProductsCode(currentContext, trackingData);
            }

            return trackingData;
        }

        /// <summary>
        /// GetFunctionTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <param name="function"></param>
        /// <param name="data"></param>
        /// <returns>TrackingDetails</returns>
        public TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function,
                                                       string data)
        {
            var trackingData = new TrackingDetails(function);

            trackingData.linkTrackVars = "events,eVar48,products";
            trackingData.linkTrackEvents = "event21";
            trackingData.products = string.Format(";{0}", currentContext.SelectHotelPage.SelectedHotelId);
            trackingData.events = "event21";
            switch (function)
            {
                case TrackingFunctions.ClickToCallUs:
                    trackingData.eVar48 = string.Format(Reference.ClickToCallFunctionName,
                                                        Reference.ConfirmationPageTrackingHeading);
                    break;
                case TrackingFunctions.ClickToMap:
                    trackingData.linkTrackEvents = "event4";
                    trackingData.events = "event4";
                    trackingData.eVar48 = string.Format(Reference.LinkToMapFunctionName,
                                                        Reference.ConfirmationPageTrackingHeading);
                    break;
                case TrackingFunctions.ClickToEmail:
                    trackingData.eVar48 = string.Format(Reference.EmailFunctionName,
                                                        Reference.ConfirmationPageTrackingHeading);
                    break;
            }
            return trackingData;
        }

        /// <summary>
        /// GetPageId
        /// </summary>
        /// <returns>MobilePages</returns>
        public MobilePages GetPageId()
        {
            return MobilePages.BookingConfirmation;
        }

        /// <summary>
        /// LeadTime
        /// </summary>
        /// <param name="arrivalDate"></param>
        /// <returns>LeadTime</returns>
        private int LeadTime(DateTime arrivalDate)
        {
            var currentDate = Convert.ToDateTime(DateTime.Now.Date.ToShortDateString());
            var timeSpan = arrivalDate.Subtract(currentDate);

            return timeSpan.Days;
        }

        /// <summary>
        /// PaymentMethod
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns>PaymentMethod</returns>
        private string PaymentMethod(BookingContext currentContext)
        {
            string paymentMethod = string.Empty;
            var controller = new BookingDetailsController();
            if (controller.IsPrepaidBooking)
            {
                paymentMethod = "Credit Card";
            }
            else
            {
                switch (currentContext.SearchHotelPage.SearchType)
                {
                    case Entity.Booking.Model.SearchType.BonusCheque:
                        paymentMethod = "Bonus";
                        break;
                    case Entity.Booking.Model.SearchType.Redemption:
                        paymentMethod = "Points";
                        break;
                }
            }
            return paymentMethod;
        }

        /// <summary>
        /// BookingTypeCode
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns>BookingTypeCode</returns>
        private string BookingTypeCode(BookingContext currentContext)
        {
            string bookingTypeCode = string.Empty;
            switch (currentContext.SearchHotelPage.SearchType)
            {
                case Entity.Booking.Model.SearchType.BonusCheque:
                    bookingTypeCode = "Bonus Code";
                    break;
            }

            return bookingTypeCode;
        }

        /// <summary>
        /// GetHotelCountryCode
        /// </summary>
        /// <returns>HotelCountryCode</returns>
        private string GetHotelCountryCode()
        {
            var countryCode = string.Empty;
            if (HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable != null &&
                HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable.Count > 0)
            {
                var selectedRoomRate = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable[0] as SelectedRoomAndRateEntity;
                if (selectedRoomRate != null && selectedRoomRate.RoomRates != null)
                {
                    countryCode = selectedRoomRate.CountryCode;
                }
            }
            return countryCode;
        }

        /// <summary>
        /// GetPriceAndCurrencyCode
        /// </summary>
        /// <param name="totalPrice"></param>
        /// <param name="currencyCode"></param>
        /// <param name="bookingType"></param>
        /// <returns>PriceAndCurrencyCode</returns>
        private string GetPriceAndCurrencyCode(string totalPrice, out string currencyCode,
                                               Entity.Booking.Model.SearchType bookingType)
        {
            string price = string.Empty;
            currencyCode = string.Empty;

            if (!string.IsNullOrEmpty(totalPrice))
            {
                var pageConfig = new BaseController("").GetPageConfig<BookingDetailsPageSection>();
                string taxText = pageConfig.PageDetail.PageMessages.GetMessage(Reference.InclusiveAllTax);
                totalPrice = totalPrice.Replace(taxText, "");
                var priceArray = totalPrice.Trim().Split(' ');
                if (priceArray != null && priceArray.Length > 1)
                {
                    price = priceArray[0];
                    switch (bookingType)
                    {
                        case Scandic.Scanweb.Mobile.UI.Entity.Booking.Model.SearchType.Redemption:
                            currencyCode = "EUR";
                            break;
                        default:
                            currencyCode = priceArray[priceArray.Length - 1];
                            if (string.Equals(currencyCode, "BC", StringComparison.InvariantCultureIgnoreCase))
                            {
                                currencyCode = "EUR";
                            }
                            break;
                    }
                }
            }

            return price;
        }

        /// <summary>
        /// GetBookingType
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns>BookingType</returns>
        private string GetBookingType(BookingContext currentContext)
        {
            string bookingType = string.Empty;
            switch (currentContext.SearchHotelPage.SearchType)
            {
                case Entity.Booking.Model.SearchType.Regular:
                    bookingType = "Regular Booking";
                    if (currentContext.SearchHotelPage.Offer == Entity.Booking.Model.BookingOffer.BookingCode)
                    {
                        bookingType = "Promotion Booking";
                    }
                    break;
                case Entity.Booking.Model.SearchType.Corporate:
                    bookingType = "Negotiated rate Booking";
                    break;
                case Entity.Booking.Model.SearchType.BonusCheque:
                    bookingType = "Bonus Cheque";
                    break;
                case Entity.Booking.Model.SearchType.Redemption:
                    bookingType = "Reward Night";
                    break;
                case Entity.Booking.Model.SearchType.Voucher:
                    bookingType = "Gift Voucher";
                    break;
                default:
                    break;
            }
            return bookingType;
        }

        /// <summary>
        /// GetProductsCode
        /// </summary>
        /// <param name="currentContext"></param>
        /// <param name="trackingData"></param>
        /// <returns>ProductsCode</returns>
        private string GetProductsCode(BookingContext currentContext, TrackingDetails trackingData)
        {
            StringBuilder productsValue = new StringBuilder(string.Format(";{0}", currentContext.SelectHotelPage.SelectedHotelId));
            string currencyCode = string.Empty;
            var checkInDate = currentContext.SearchHotelPage.CheckInDate;
            var checkOutDate = currentContext.SearchHotelPage.CheckOutDate;
            if (checkInDate.HasValue && checkOutDate.HasValue)
            {
                var timeSpan = checkOutDate.Value.Subtract(checkInDate.Value);
                productsValue.AppendFormat(";{0}", timeSpan.Days.ToString());
            }
            string price = string.Empty;
            if (currentContext.SearchHotelPage.SearchType == Entity.Booking.Model.SearchType.BonusCheque ||
                currentContext.SearchHotelPage.SearchType == Entity.Booking.Model.SearchType.Redemption)
            {
                price = "0";
            }
            else
            {
                price = GetPriceAndCurrencyCode(currentContext.BookingDetailPage.TotalRate, out currencyCode,
                                                currentContext.SearchHotelPage.SearchType);
            }
            productsValue.AppendFormat(";{0}", price);
            productsValue.Append(";event17=1");
            productsValue.AppendFormat(";eVar40={0}", trackingData.eVar40);
            productsValue.AppendFormat("|eVar41={0}", trackingData.eVar41);
            productsValue.AppendFormat("|eVar42={0}", trackingData.eVar42);
            productsValue.AppendFormat("|eVar43={0}", trackingData.eVar43);
            productsValue.AppendFormat("|eVar39={0}", trackingData.eVar39);

            return productsValue.ToString();
        }

        #endregion
    }
}