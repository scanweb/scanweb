﻿using System.ComponentModel.Composition;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    /// <summary>
    /// SelectRateTracking
    /// </summary>
    [Export(typeof(IPageTracking))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SelectRateTracking : IPageTracking
    {
        private IUserInfoRespository userRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        public SelectRateTracking()
        {
            userRepository =
                DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
        }

        #region IPageTracking Members

        /// <summary>
        /// GetPageTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns>PageTrackingData</returns>
        public TrackingDetails GetPageTrackingData(BookingContext currentContext, string data)
        {
            var trackingData = new TrackingDetails();

            trackingData.pageName = Reference.SelectRatePageTrackingHeading;
            trackingData.prop34 = userRepository.IsUserAuthenticated
                                      ? Reference.LoggedInUserTextTracking
                                      : Reference.LoggedOutUserTextTracking;
            trackingData.eVar34 = userRepository.IsUserAuthenticated
                                      ? Reference.LoggedInUserTextTracking
                                      : Reference.LoggedOutUserTextTracking;

            if (currentContext != null && currentContext.SearchHotelPage != null)
            {
                var bookingCode = currentContext.SearchHotelPage.BookingCode;
                trackingData.eVar2 = !string.IsNullOrEmpty(bookingCode) ? bookingCode : string.Empty;

                if (currentContext.SearchHotelPage.SearchBy == SearchByType.City)
                {
                    trackingData.events = "event1";
                }
                else
                {
                    var checkInDate = currentContext.SearchHotelPage.CheckInDate;
                    var checkOutDate = currentContext.SearchHotelPage.CheckOutDate;
                    if (checkInDate.HasValue && checkOutDate.HasValue)
                    {
                        var timeSpan = checkOutDate.Value.Subtract(checkInDate.Value);
                        trackingData.prop12 = timeSpan.Days.ToString();
                        trackingData.eVar12 = trackingData.prop12;
                        trackingData.prop14 = checkInDate.Value.ToString(Reference.IOS5DateFormat);
                        trackingData.eVar14 = trackingData.prop14;
                        trackingData.prop17 = string.Format("{0}-{1}", trackingData.prop14, checkOutDate.Value.ToString(Reference.IOS5DateFormat));
                        trackingData.eVar17 = trackingData.prop17;
                    }

                    trackingData.prop11 = currentContext.SearchHotelPage.SearchDestination;
                    trackingData.eVar11 = trackingData.prop11;

                    trackingData.prop13 = "1";
                    trackingData.eVar13 = trackingData.prop13;

                    trackingData.prop15 = currentContext.SearchHotelPage.NumberOfAdults.Value.ToString();
                    trackingData.eVar15 = trackingData.prop15;
                    trackingData.prop16 = currentContext.SearchHotelPage.NumberOfChildren.Value.ToString();
                    trackingData.eVar16 = trackingData.prop16;

                    trackingData.prop18 = "1";
                    trackingData.eVar18 = trackingData.prop18;
                    trackingData.events = "scCheckout";
                }
            }
            return trackingData;
        }

        /// <summary>
        /// GetFunctionTrackingData
        /// </summary>
        /// <param name="currentContext"></param>
        /// <param name="function"></param>
        /// <param name="data"></param>
        /// <returns>FunctionTrackingData</returns>
        public TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function, string data)
        {
            var trackingData = new TrackingDetails(function);

            trackingData.linkTrackVars = "events,eVar48,products";
            trackingData.linkTrackEvents = "event21";
            trackingData.products = string.Format(";{0}", data);
            trackingData.events = "event21";
            switch (function)
            {
                //case TrackingFunctions.ClickToCallUs:
                //    trackingData.eVar31 = string.Format(Reference.ClickToCallFunctionName, Reference.SelectRatePageTrackingHeading);
                //    break;
                case TrackingFunctions.ClickToTripAdvisorReviews:
                    trackingData.prop25 = "Tripadvisor function";
                    trackingData.prop26 = "Tripadvisor review link";
                    break;
                case TrackingFunctions.EditSearch:
                    trackingData.prop20 = "Edit Search";
                    break;
                case TrackingFunctions.SelectRateSubmit:
                    trackingData.eVar29 = data;
                    break;
            }
            return trackingData;
        }

        /// <summary>
        /// GetPageId
        /// </summary>
        /// <returns>MobilePages</returns>
        public MobilePages GetPageId()
        {
            return MobilePages.SelectRate;
        }

        #endregion
    }
}