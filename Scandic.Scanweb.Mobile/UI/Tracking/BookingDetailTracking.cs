﻿//  Description					:   BookingDetailTracking                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.ComponentModel.Composition;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    /// <summary>
    /// Contains members to support tracking.
    /// </summary>
    [Export(typeof (IPageTracking))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class BookingDetailTracking : IPageTracking
    {
        private IUserInfoRespository userRepository;
        /// <summary>
        /// Constructor
        /// </summary>
        public BookingDetailTracking()
        {
            userRepository =
                DependencyResolver.Instance.GetService(typeof (IUserInfoRespository)) as IUserInfoRespository;
        }

        #region IPageTracking Members
        /// <summary>
        /// Gets page tracking data.
        /// </summary>
        /// <param name="currentContext"></param>
        /// <returns></returns>
        public TrackingDetails GetPageTrackingData(BookingContext currentContext, string data)
        {
            var trackingData = new TrackingDetails();

            trackingData.pageName = Reference.BookingDetailsPageTrackingHeading;
            trackingData.prop34 = userRepository.IsUserAuthenticated
                                      ? Reference.LoggedInUserTextTracking
                                      : Reference.LoggedOutUserTextTracking;
            trackingData.eVar34 = userRepository.IsUserAuthenticated
                                      ? Reference.LoggedInUserTextTracking
                                      : Reference.LoggedOutUserTextTracking;
            trackingData.events = "event2";
            if (currentContext != null && currentContext.SelectHotelPage != null)
            {
                if (currentContext.SearchHotelPage != null)
                {
                    var bookingCode = currentContext.SearchHotelPage.BookingCode;
                    trackingData.eVar2 = !string.IsNullOrEmpty(bookingCode) ? bookingCode : string.Empty;
                }
                trackingData.products = string.Format(";{0}", currentContext.SelectHotelPage.SelectedHotelId);
            }

            return trackingData;
        }

        /// <summary>
        /// Gets function tracking data
        /// </summary>
        /// <param name="currentContext"></param>
        /// <param name="function"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function,
                                                       string data)
        {
            var trackingData = new TrackingDetails(function);
            
            switch (function)
            {
                case TrackingFunctions.CheckoutSelection:
                    trackingData.linkTrackVars = "events,eVar62";
                    trackingData.linkTrackEvents = "event31";
                    trackingData.events = "event31";
                    trackingData.eVar62 = "CC Checkout";                    
                    break;
                default:
                    trackingData = null;
                    break;
                
            }
            return trackingData;
        }

        /// <summary>
        /// Gets PageId
        /// </summary>
        /// <returns></returns>
        public MobilePages GetPageId()
        {
            return MobilePages.BookingDetails;
        }
        #endregion
    }
}