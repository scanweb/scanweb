﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Mobile.UI.Tracking.Interface;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Common;
using System.ComponentModel.Composition;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;


namespace Scandic.Scanweb.Mobile.UI.Tracking
{
    [Export(typeof(IPageTracking))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ModifyCancelTracking : IPageTracking
    {
        private IUserInfoRespository userRepository;

        public ModifyCancelTracking()
        {
            userRepository = DependencyResolver.Instance.GetService(typeof(IUserInfoRespository)) as IUserInfoRespository;
        }

        #region IPageTracking Members

        public TrackingDetails GetPageTrackingData(BookingContext currentContext, string data)
        {
            var trackingData = new TrackingDetails();

            trackingData.pageName = Reference.ModifyCancelPageTrackingHeading;
            trackingData.prop34 = userRepository.IsUserAuthenticated ? Reference.LoggedInUserTextTracking : Reference.LoggedOutUserTextTracking;
            trackingData.eVar34 = userRepository.IsUserAuthenticated ? Reference.LoggedInUserTextTracking : Reference.LoggedOutUserTextTracking;
            return trackingData;
        }

        public TrackingDetails GetFunctionTrackingData(BookingContext currentContext, TrackingFunctions function, string data)
        {
            var trackingData = new TrackingDetails(function);

            trackingData.linkTrackVars = "events,eVar48";
            trackingData.linkTrackEvents = "event21";
            trackingData.events = "event21";
            switch (function)
            {
                case TrackingFunctions.ClickToCallUs:
                    trackingData.eVar48 = string.Format(Reference.ClickToCallFunctionName, Reference.ModifyCancelPageTrackingHeading);
                    break;
                case TrackingFunctions.ClickToMap:
                    trackingData.linkTrackEvents = "event4";
                    trackingData.events = "event4";
                    trackingData.eVar48 = string.Format(Reference.LinkToMapFunctionName, Reference.ModifyCancelPageTrackingHeading);
                    break;
                case TrackingFunctions.ClickToEmail:
                    trackingData.eVar48 = string.Format(Reference.EmailFunctionName, Reference.ModifyCancelPageTrackingHeading);
                    break;
            }
            return trackingData;
        }

        public MobilePages GetPageId()
        {
            return MobilePages.ModifyCancel;
        }

        #endregion
    }
}
