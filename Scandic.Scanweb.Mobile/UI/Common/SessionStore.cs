﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Mobile.UI.Common.Interface;
using System.ComponentModel.Composition;


namespace Scandic.Scanweb.Mobile.UI.Common
{
    [Export(typeof(IStore))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SessionStore : IStore
    {

        #region IStore Members

        public T GetData<T>(string dataKey)
        {
            return (T)HttpContext.Current.Session[dataKey];
        }

        public void SaveData<T>(string dataKey, T data)
        {
            HttpContext.Current.Session[dataKey] = data;
        }

        public StoreType Type
        {
            get { return StoreType.Session; }
        }

        #endregion
    }
}
