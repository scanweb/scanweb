﻿////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  ScanWebMobileApp                                       //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                                               		  //
// Creation Date				:           	    								      //
// Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
// Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Net;
using System.Text;
using System.Timers;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Core.Interface;
using Scandic.Scanweb.Mobile.UI.Booking.Repository;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Common
{
    /// <summary>
    /// This class represents the ScanWebMobileApp
    /// </summary>
    [Export(typeof (IApplication))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ScanWebMobileApp : IApplication
    {
        private static Timer appTimer;

        #region IApplication Members

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="data"></param>
        public void Initialize(object data)
        {
            AppLogger.LogInfoMessage(string.Format("{0}:{1}", Reference.APPLICATION_ID, "Intializing Timer"));
            appTimer = new Timer(60000);
            appTimer.Elapsed += new ElapsedEventHandler(appTimer_Elapsed);
            appTimer.Enabled = true;
        }

        private static void appTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                AppLogger.LogInfoMessage(string.Format("{0}:{1}", Reference.APPLICATION_ID,
                                                       "Firing web request to start page for initialization"));
                FireWebRequestToInitalizeMobileApp();
                appTimer.Interval = 600000;
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, string.Format("{0}:{1}",
                                                              Reference.APPLICATION_ID,
                                                              "Web request intialization failed in the method - appTimer_Elapsed"));
            }
        }

        private static void FireWebRequestToInitalizeMobileApp()
        {
            byte[] postInfo = Encoding.ASCII.GetBytes("process=InitializeApp");
            var siteRepository = new SiteInfoRepository();
            var appConfig = siteRepository.GetGeneralConfig<ApplicationConfigSection>();
            var requestPath = appConfig.GetMessage(Reference.MobileAppInitializerPath);
            var webReq = WebRequest.Create(requestPath) as HttpWebRequest;

            webReq.Method = "POST";
            webReq.ContentType = "application/x-www-form-urlencoded";
            webReq.ContentLength = postInfo.Length;
            Stream postData = webReq.GetRequestStream();
            postData.Write(postInfo, 0, postInfo.Length);
            postData.Close();
            var webResponse = webReq.GetResponse() as HttpWebResponse;

            if (webResponse.StatusCode == HttpStatusCode.OK)
            {
                AppLogger.LogInfoMessage(string.Format("{0}:{1}", Reference.APPLICATION_ID,
                                                       "Initialization successful in the method - FireWebRequestToInitalizeMobileApp."));
            }
            else
            {
                AppLogger.LogInfoMessage(string.Format("{0}:{1}", Reference.APPLICATION_ID,
                                                       "Initialization failure in the method - FireWebRequestToInitalizeMobileApp."));
            }
        }

        #endregion
    }
}