﻿using System;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Common
{
    /// <summary>
    /// MobileAppInitializer
    /// </summary>
    public class MobileAppInitializer
    {
        /// <summary>
        /// IntializeApp
        /// </summary>
        public void IntializeApp()
        {
            try
            {
                var siteInfoRepository =
                    DependencyResolver.Instance.GetService(typeof (ISiteInfoRepository)) as ISiteInfoRepository;
                siteInfoRepository.GetSearchDestinationList();
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException
                    (ex, string.Format("{0}:{1}", Reference.APPLICATION_ID, "MobileAppInitializer failed."));
            }
        }
    }
}