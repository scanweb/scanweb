﻿//  Description					:   Utilities                                             //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Entity.Booking;
using System.Web;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;

namespace Scandic.Scanweb.Mobile.UI.Common
{
    public static class Utilities
    {
        /// <summary>
        /// This method will add the key value pairs in passed menu item attribute collection
        /// to the specified webcontrol.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="attributes"></param>
        public static void AddAttributes(WebControl control, List<Entity.Attribute> attributes)
        {
            foreach (var attribute in attributes)
            {
                if (attribute.AddInMarkup)
                {
                    control.Attributes.Add(attribute.Id, attribute.Value);
                }
            }
        }

        /// <summary>
        /// This method will return language string to redirect
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        public static string GetRedirectLanguage(string countryCode)
        {
            string redirectlanguage = string.Empty;

            if (!string.IsNullOrEmpty(countryCode))
            {
                countryCode = countryCode.ToUpper();
                switch (countryCode)
                {
                    case CountryCodeConstant.COUNTRY_SWEDEN:
                        redirectlanguage = CurrentPageLanguageConstant.LANGUAGE_SWEDISH;
                        break;
                    case CountryCodeConstant.COUNTRY_DANMARK:
                        redirectlanguage = CurrentPageLanguageConstant.LANGUAGE_DANISH;
                        break;
                    case CountryCodeConstant.COUNTRY_NORWAY:
                        redirectlanguage = CurrentPageLanguageConstant.LANGUAGE_NORWEGIAN;
                        break;
                    case CountryCodeConstant.COUNTRY_FINLAND:
                        redirectlanguage = CurrentPageLanguageConstant.LANGUAGE_FINNISH;
                        break;
                }
            }
            return redirectlanguage;
        }

        /// <summary>
        /// Converts to secure protocol.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string ConvertToSecureProtocol(string url)
        {
            var path = url.ToLower();
            if (path.IndexOf("https") == -1)
            {
                path = path.Replace("http", "https");
            }
            return path;
        }

        #region GetFriendlyMembershipLevel

        /// <summary>
        /// It returns the membership level to be displayed on the screen.
        /// As opera returns memebrshiplevel as one single work without 
        /// any spaces in between, we have to add a space and then show to the user.
        /// </summary>
        /// <param name="operaMembershipLevel"></param>
        /// <returns></returns>
        public static string GetFriendlyMembershipLevel(string operaMembershipLevel)
        {
            string friendlyLevel = string.Empty;

            switch (operaMembershipLevel.ToUpper())
            {
                case Reference.MEMBERSHIPLEVEL_1STFLOOR:
                    friendlyLevel = "1st floor";
                    break;
                case Reference.MEMBERSHIPLEVEL_2NDFLOOR:
                    friendlyLevel = "2nd floor";
                    break;
                case Reference.MEMBERSHIPLEVEL_3RDFLOOR:
                    friendlyLevel = "3rd floor";
                    break;
                case Reference.MEMBERSHIPLEVEL_TOPFLOOR:
                    friendlyLevel = "Top floor";
                    break;
                default:
                    friendlyLevel = operaMembershipLevel;
                    break;
            }

            return friendlyLevel;
        }

        #endregion GetFriendlyMembershipLevel    

        /// <summary>
        /// Checks whether the given string is date or not.
        /// </summary>
        /// <param name="dateString"></param>
        /// <param name="convertedDate"></param>
        /// <returns></returns>
        public static bool IsDate(string dateString, out DateTime convertedDate)
        {
            bool isDate = true;

            if (!DateTime.TryParse(dateString, out convertedDate))
            {
                isDate = false;
            }

            return isDate;
        }

        public static ActionItem GetActionItemToTrack(BookingContext bkngContext)
        {
            ActionItem actnItem = new ActionItem();
            actnItem.Parameters = new List<KeyValueParam>();
            actnItem.ActionDate = DateTime.Now;
            if (bkngContext != null && (bkngContext.SearchHotelPage != null || bkngContext.SelectRatePage != null || bkngContext.SelectHotelPage != null || bkngContext.BookingDetailPage != null))
            {
                if (bkngContext.SearchHotelPage != null)
                {
                    actnItem.Parameters.Add(new KeyValueParam(Reference.CHECKIN_DATE, Convert.ToString(bkngContext.SearchHotelPage.CheckInDate)));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.CHECKOUT_DATE, Convert.ToString(bkngContext.SearchHotelPage.CheckOutDate)));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.NO_OF_ADULTS, Convert.ToString(bkngContext.SearchHotelPage.NumberOfAdults)));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.NO_OF_CHILDREN, Convert.ToString(bkngContext.SearchHotelPage.NumberOfChildren)));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.SEARCH_DESTINATION, bkngContext.SearchHotelPage.SearchDestination));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.SEARCH_DESTINATION_ID, bkngContext.SearchHotelPage.SearchDestinationId));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.SEARCH_TYPE, Convert.ToString(bkngContext.SearchHotelPage.SearchType)));
                }
                if (bkngContext.SelectRatePage != null)
                {
                    actnItem.Parameters.Add(new KeyValueParam(Reference.GUARANTEE_TYPE, bkngContext.SelectRatePage.CreditCardGuranteeType));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.SELECTED_RATE, bkngContext.SelectRatePage.SelectedRate));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.SELECTED_RATE_TYPE, bkngContext.SelectRatePage.SelectedRateType));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.SELECTED_RATE_TYPE_ID, bkngContext.SelectRatePage.SelectedRateTypeId));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.SELECTED_ROOMTYPE, bkngContext.SelectRatePage.SelectedRoomType));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.SELECTED_ROOMTYPE_ID, bkngContext.SelectRatePage.SelectedRoomTypeId));
                }
                if (bkngContext.SelectHotelPage != null)
                {
                    actnItem.Parameters.Add(new KeyValueParam(Reference.SELECTED_HOTEL, bkngContext.SelectHotelPage.SelectedHotel));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.SELECTED_HOTEL_ID, bkngContext.SelectHotelPage.SelectedHotelId));
                }
                if (bkngContext.BookingDetailPage != null)
                {
                    actnItem.Parameters.Add(new KeyValueParam(Reference.MEMBERSHIP_ID, !string.IsNullOrEmpty(bkngContext.BookingDetailPage.MembershipId) ? bkngContext.BookingDetailPage.MembershipId :
                                                                "UserNotLoggedIn"));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.RESERVATIONNUMBER,bkngContext.BookingDetailPage.ReservationNumber));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.FIRST_NAME, bkngContext.BookingDetailPage.FirstName));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.LAST_NAME, bkngContext.BookingDetailPage.LastName));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.EMAIL_ID, bkngContext.BookingDetailPage.EmailAddress));
                    actnItem.Parameters.Add(new KeyValueParam(Reference.MOBILE, bkngContext.BookingDetailPage.MobileNumber));
                }
            }
            else
            {
                actnItem.Parameters.Add(new KeyValueParam("BookingContextNull", "BookingContext is null"));
            }
            return actnItem;
        }

        public static ActionItem GetActionItemInModifyCancelFlow(string methodName, bool isCancellable, string resNumber, string policyCode, 
                                    string roomHdngText, string cancellationCtrlId, List<CancelRoomDetails> lstCnclRoomDetails, string currentLanguage)
        {
            ActionItem actnItem = new ActionItem();
            actnItem.Parameters = new List<KeyValueParam>();
            actnItem.ActionDate = DateTime.Now;
            if (string.Equals(methodName, Reference.GETCONFIRM_MESSAGE_OVERLAY, StringComparison.CurrentCultureIgnoreCase))
            {
                actnItem.Parameters.Add(new KeyValueParam(Reference.ISROOM_CANCELLABLE, Convert.ToString(isCancellable)));
                actnItem.Parameters.Add(new KeyValueParam(Reference.RESERVATIONNUMBER, resNumber));
                actnItem.Parameters.Add(new KeyValueParam(Reference.POLICYCODE, policyCode));
                actnItem.Parameters.Add(new KeyValueParam(Reference.ROOMHEADING_TEXT, roomHdngText));
                actnItem.Parameters.Add(new KeyValueParam(Reference.CANCELLATION_CONTROL_ID, cancellationCtrlId));
            }
            else if (string.Equals(methodName, Reference.CANCEL_ROOM, StringComparison.CurrentCultureIgnoreCase) || 
                     string.Equals(methodName, Reference.CANCLE_COMPLETE_BOOKING, StringComparison.CurrentCultureIgnoreCase))
            {
                actnItem.Parameters.Add(new KeyValueParam(Reference.CURRENT_LANGUAGE, currentLanguage));
                if (lstCnclRoomDetails != null && lstCnclRoomDetails.Count > 0)
                {
                    foreach (CancelRoomDetails value in lstCnclRoomDetails)
                    {
                        actnItem.Parameters.Add(new KeyValueParam(Reference.RESERVATIONNUMBER, value.ReservationNumber));
                        actnItem.Parameters.Add(new KeyValueParam(Reference.LEGNUMBER, value.LegNumber));
                        actnItem.Parameters.Add(new KeyValueParam(Reference.FIRST_NAME, value.FirstName));
                        actnItem.Parameters.Add(new KeyValueParam(Reference.LAST_NAME, value.LastName));
                        actnItem.Parameters.Add(new KeyValueParam(Reference.PHONE_NUMBER, value.PhoneNumber));
                        actnItem.Parameters.Add(new KeyValueParam(Reference.CANCELLATION_NUMBER, value.CancellationNumber));
                        actnItem.Parameters.Add(new KeyValueParam(Reference.ERROR_MESSAGE, value.ErrorMessage));
                        actnItem.Parameters.Add(new KeyValueParam(Reference.HOTELCODE, value.HotelCode));
                        actnItem.Parameters.Add(new KeyValueParam(Reference.SEARCHTYPE_CODE, Convert.ToString(value.SearchTypeCode)));
                    }
                }

            }
            else if(string.Equals(methodName, Reference.GETCANCEL_BOOKING_OVERLAY, StringComparison.CurrentCultureIgnoreCase))
            {
                actnItem.Parameters.Add(new KeyValueParam(Reference.ISROOM_CANCELLABLE, Convert.ToString(isCancellable)));
            }
            if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.UrlReferrer != null)
                actnItem.Parameters.Add(new KeyValueParam(Reference.URL_REFERRER, Convert.ToString(HttpContext.Current.Request.UrlReferrer)));
            return actnItem;
        }

        public static ActionItem GetActionItemForOffersPage(int offerPageId)
        {
            ActionItem actnItem = new ActionItem();
            actnItem.Parameters = new List<KeyValueParam>();
            actnItem.ActionDate = DateTime.Now;
            actnItem.Parameters.Add(new KeyValueParam(Reference.OFFER_PAGE_ID, Convert.ToString(offerPageId)));
            if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.UrlReferrer != null)
                actnItem.Parameters.Add(new KeyValueParam(Reference.URL_REFERRER, Convert.ToString(HttpContext.Current.Request.UrlReferrer)));
            return actnItem;
        }

        public static ActionItem GetActionItemForUserBookings(string methodName, int currentlyDisplayedItems, int noOfItemsToDisplay, string sortType,
                                    SortOrder sortOrder, bool fetchLatestItems, string nameId, string membershipId)
        {
            ActionItem actnItem = new ActionItem();
            actnItem.Parameters = new List<KeyValueParam>();
            actnItem.ActionDate = DateTime.Now;
            if (string.Equals(methodName, Reference.GETFUTURE_RESERVATION_TODISPLAY, StringComparison.CurrentCultureIgnoreCase))
            {
                actnItem.Parameters.Add(new KeyValueParam(Reference.CURRENT_DISPLAYED_BOOKINGS, Convert.ToString(currentlyDisplayedItems)));
                actnItem.Parameters.Add(new KeyValueParam(Reference.NO_OF_BOOKINGS_TODISPLAY, Convert.ToString(noOfItemsToDisplay)));
               
            }
            else if (string.Equals(methodName, Reference.GETUSER_HISTORY_TODISPLAY, StringComparison.CurrentCultureIgnoreCase))
            {
                actnItem.Parameters.Add(new KeyValueParam(Reference.CURRENT_DISPLAYED_TRANSACTIONS, Convert.ToString(currentlyDisplayedItems)));
                actnItem.Parameters.Add(new KeyValueParam(Reference.NO_OF_TRANSACTIONS_TODISPLAY, Convert.ToString(noOfItemsToDisplay)));
            }
            actnItem.Parameters.Add(new KeyValueParam(Reference.SORT_TYPE, sortType));
            actnItem.Parameters.Add(new KeyValueParam(Reference.SORT_ORDER, Convert.ToString(sortOrder)));
            actnItem.Parameters.Add(new KeyValueParam(Reference.FETCH_LATEST_ITEMS, Convert.ToString(fetchLatestItems)));
            actnItem.Parameters.Add(new KeyValueParam(Reference.NAME_ID, nameId));
            actnItem.Parameters.Add(new KeyValueParam(Reference.MEMBERSHIP_ID, membershipId));

            if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.UrlReferrer != null)
                actnItem.Parameters.Add(new KeyValueParam(Reference.URL_REFERRER, Convert.ToString(HttpContext.Current.Request.UrlReferrer)));

            return actnItem;
        }

        public static void TrackActionParametersAndLogData(string className, string methodName, Exception ex, ActionItem action, bool clearSession)
        {
            if (action != null && action.Parameters != null)
            {
                action.Parameters.Add(new KeyValueParam(Reference.PAGE_NAME, className));
                action.Parameters.Add(new KeyValueParam(Reference.METHOD_NAME, string.Format("{0}:{1}-", className, methodName)));
                UserNavTracker.TrackAction(action, clearSession);
                UserNavTracker.LogAndClearTrackedData(ex != null ? ex : new Exception(string.Format("Generic-Error in {0} Page {1} method", className, methodName)));
            }
        }
    }
}