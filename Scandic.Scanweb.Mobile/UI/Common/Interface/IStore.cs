﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Scanweb.Mobile.UI.Common.Interface
{
    public interface IStore
    {
        T GetData<T> (string dataKey);
        void SaveData<T> (string dataKey, T data);
        StoreType Type { get; }
    }

    public enum StoreType
    {
        Cache = 0,
        Session = 1
    }
}
