﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Booking.Repository;
using Scandic.Scanweb.Mobile.UI.Controls;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Common
{
    /// <summary>
    /// Overlay Helper
    /// </summary>
    public static class OverlayHelper
    {
        /// <summary>
        /// Get Language Overlay
        /// </summary>
        /// <returns>Language Overlay</returns>
        public static string GetLanguageOverlay()
        {
            var masterController = new MasterPageController();
            var masterConfigData = masterController.GetPageData("");

            masterConfigData.LanguageOverlay.Items = SetLanguageUrls(masterConfigData.LanguageOverlay.Items);
            var languageOverlay =
                new Overlay
                    (OverlayControlType.Value, masterConfigData.LanguageOverlay, masterConfigData.LanguageOverlay.Id);

            return languageOverlay.GetControlHtml();
        }

        /// <summary>
        /// Set Language Urls
        /// </summary>
        /// <param name="items"></param>
        /// <returns>Overlay Items</returns>
        private static List<OverlayItem> SetLanguageUrls(List<OverlayItem> items)
        {
            var siteRepository = DependencyResolver.Instance.GetService(typeof(ISiteInfoRepository)) as ISiteInfoRepository;
            foreach (var item in items)
            {
                var languageUrl = string.Format("{0}?{1}={2}", siteRepository.GetPageUrl
                                            (MobilePages.Start, item.Data), Reference.LanguageSwitch, item.Data);

                if (!string.IsNullOrEmpty(MiscellaneousSessionWrapper.MOBILEURLCMPID))
                {
                    languageUrl = string.Format("{0}&cmpid={1}", languageUrl, MiscellaneousSessionWrapper.MOBILEURLCMPID);
                }
                item.Value = string.Format(item.Value, languageUrl);
            }
            return items;
        }

        /// <summary>
        /// GetTooltipOverlay
        /// </summary>
        /// <param name="pageSection"></param>
        /// <param name="toolTipId"></param>
        /// <param name="associatedControlId"></param>
        /// <returns>html string</returns>
        public static string GetTooltipOverlay(IPageSection pageSection, string toolTipId, string associatedControlId)
        {
            string htmlString = "";

            if (pageSection != null)
            {
                var pageDetails = pageSection.GetPageSection();
                var toolTipConfig = pageDetails.Inputs.Where(
                    input => string.Equals(input.Id, associatedControlId, StringComparison.InvariantCultureIgnoreCase)
                    ).Select(input => input.ToolTip).FirstOrDefault();

                if (toolTipConfig != null)
                {
                    var overlayData = new OverlayDetails();

                    overlayData.Heading = toolTipConfig.Heading;
                    overlayData.Id = toolTipId;
                    overlayData.Items = new List<OverlayItem>();
                    overlayData.Items.Add(new OverlayItem { Value = toolTipConfig.Text });

                    var overlay = new Overlay(OverlayControlType.Value, overlayData, overlayData.Id);
                    htmlString = overlay.GetControlHtml();
                }
            }

            return htmlString;
        }

        /// <summary>
        /// GetProfileOverlay
        /// </summary>
        /// <param name="overlayId"></param>
        /// <returns>Profile overLay Html</returns>
        public static string GetProfileOverlay(string overlayId)
        {
            string htmlString = "";
            var masterController = new MasterPageController();
            var masterConfigData = masterController.GetPageData("");
            IUserInfoRespository userRepository = new UserInfoRepository();
            var profile = userRepository.GetLoyaltyDetails();
            var overlayData = new OverlayDetails();

            if (profile != null)
            {
                overlayData.Items = new List<OverlayItem>();
                var item = new OverlayKeyItem();

                item.Key = masterConfigData.PageMessages.GetMessage("profileYourPointText");
                item.Value = profile.CurrentPoints.ToString();
                overlayData.Items.Add(item);

                item = new OverlayKeyItem();
                item.Key = masterConfigData.PageMessages.GetMessage("profileMemberLevelText");
                ;
                item.Value = Utilities.GetFriendlyMembershipLevel(profile.MembershipLevel);
                overlayData.Items.Add(item);

                var controller = new BaseController("");
                item = new OverlayKeyItem();
                item.Key = string.Format(masterConfigData.PageMessages.GetMessage("profileViewMyBookingsLink"), controller.GetPageUrl(MobilePages.MyBookings));
                item.Value = "&nbsp;";
                overlayData.Items.Add(item);

                overlayData.Id = overlayId;
                overlayData.Heading = masterConfigData.PageMessages.GetMessage("profileOverlayHeading");
                overlayData.IsCachable = true;

                var overlay = new Overlay(OverlayControlType.KeyValue, overlayData, overlayData.Id);
                var signOutControl = new HtmlGenericControl("p");
                var signOutAnchor = new HtmlAnchor();
                var closeAnchor = new HtmlAnchor();

                signOutAnchor.HRef = "#";
                signOutAnchor.ID = "sign-out";
                signOutAnchor.Attributes.Add("class", "button green");
                signOutAnchor.Attributes.Add
                    ("onclick", masterConfigData.PageMessages.GetMessage("profileSignOutOnClick"));
                signOutAnchor.InnerHtml = masterConfigData.PageMessages.GetMessage("profileSignOutText");

                closeAnchor.HRef = "#";
                closeAnchor.ID = "close";
                closeAnchor.Attributes.Add("class", "button green");
                closeAnchor.Attributes.Add("onclick", string.Format("$('#{0}').hide(); return false;", overlayId));
                closeAnchor.InnerHtml = masterConfigData.PageMessages.GetMessage("profileCloseButtonText");

                signOutControl.Attributes.Add("class", "profile-actions");
                signOutControl.Controls.Add(closeAnchor);
                signOutControl.Controls.Add(signOutAnchor);

                htmlString = overlay.GetControlHtml(signOutControl);
            }

            return htmlString;
        }

        /// <summary>
        /// GetRateTooltip
        /// </summary>
        /// <param name="toolTipId"></param>
        /// <param name="rateCategoryId"></param>
        /// <returns>Rate ToolTip</returns>
        public static string GetRateTooltip(string toolTipId, string rateCategoryId, bool isBlockCode)
        {
            string htmlString = string.Empty;
            IBookingRepository bookingRepository =
                DependencyResolver.Instance.GetService(typeof(IBookingRepository)) as IBookingRepository;
            var ratePop = bookingRepository.GetRateTypeToolTip(rateCategoryId, isBlockCode);
            var toolTip = string.Empty;
            if (ratePop != null)
            {
                toolTip = string.Format("<p>{0}</p>", ratePop.RateCategoryDesc);
            }
            if (!string.IsNullOrEmpty(toolTip))
            {
                var overlayData = new OverlayDetails();

                overlayData.Heading = ratePop != null ? ratePop.RateCategoryName : string.Empty;
                overlayData.Id = toolTipId;
                overlayData.Items = new List<OverlayItem>();
                overlayData.Items.Add(new OverlayItem { Value = toolTip });

                var overlay = new Overlay(OverlayControlType.Value, overlayData, overlayData.Id);
                htmlString = overlay.GetControlHtml();
            }

            return htmlString;
        }
    }
}