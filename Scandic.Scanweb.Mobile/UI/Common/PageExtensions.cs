﻿using System.Collections.Generic;
using System.Web.UI;

namespace Scandic.Scanweb.Mobile.UI.Common
{
    /// <summary>
    /// This class provide the extenstion methods that will be used on a web page.
    /// </summary>
    public static class PageExtensions
    {
        /// <summary>
        /// This extension method will allow ControlCollection to flatten the 
        /// control tree so that LINQ could be used to query the control tree.
        /// </summary>
        /// <param name="controls">Control collection which will be required
        /// to converted in simple list.</param>
        /// <returns>ControlCollection</returns>
        public static IEnumerable<Control> All(this ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                foreach (Control grandChild in control.Controls.All())
                    yield return grandChild;

                yield return control;
            }
        }
    }
}