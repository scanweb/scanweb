﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Mobile.UI.Entity;

namespace Scandic.Scanweb.Mobile.UI.Common
{
    /// <summary>
    /// This abstract class must be implemented by all interactive Login pages that
    /// need to use the Logon Framework built in to the UI Framework
    /// </summary>
    /// <typeparam name="ConfigType">Config type of the inheriting page</typeparam>
    public abstract class LoginBasePage<ConfigType> : VisualBasePage<ConfigType>
    {
        protected LoginBasePage()
        {
        }

        /// <summary>
        /// This method will be implemented by login page to return the logon arguments like
        /// user name and password etc to the processing page.
        /// </summary>
        /// <returns></returns>
        public abstract LogonContext GetLogonContext();
    }
}
