﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;
using Scandic.Scanweb.Mobile.UI.Entity.Model;

namespace Scandic.Scanweb.Mobile.UI.Common
{
    public class SessionManager
    {
        private SessionManager() { }

        //RoomRate details
        private const string HOTEL_ROOMRATE_RESULT = "HOTEL_ROOMRATE_RESULT";
        /// <summary>
        /// The Room Rate Results object containing the details of the rate of
        /// the room selected by the user.
        /// </summary>
        public static RoomRateResults HotelRoomRateResults
        {
            get
            {
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                return session.Contents[HOTEL_ROOMRATE_RESULT] as RoomRateResults;
            }
            set
            {
                System.Web.HttpContext.Current.Session.Add(HOTEL_ROOMRATE_RESULT, value);
            }
        }

        private const string BOOKING_INFORMATION = "BOOKING_INFORMATION";
        /// <summary>
        /// Holds booking data of prepaid guarantee types on redirecting to Nets window.
        /// This information will be used to make a reservation on nets payment successful.
        /// </summary>
        public static BookingDetailModel BookingInformation
        {
            get
            {
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                return session.Contents[BOOKING_INFORMATION] as BookingDetailModel;
            }
            set
            {
                System.Web.HttpContext.Current.Session.Add(BOOKING_INFORMATION, value);
            }
        }

        private const string BOOKING_GUESTINFORMATION = "BOOKING_GUESTINFORMATION";
        /// <summary>
        /// Holds guest information used in create booking call, and later the same will be used in modify booking (guarantee type to PRESU). 
        /// </summary>
        public static List<GuestInformationEntity> BookingGuestInformation
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[BOOKING_GUESTINFORMATION] as List<GuestInformationEntity>;
            }
            set
            {
                HttpContext.Current.Session.Add(BOOKING_GUESTINFORMATION, value);
            }
        }
    }
}
