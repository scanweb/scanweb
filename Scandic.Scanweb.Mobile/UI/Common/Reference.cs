﻿//  Description					:   Reference                                             //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Common
{
    /// <summary>
    /// This class with contain common properties to used.
    /// </summary>
    public static class Reference
    {
        /// <summary>
        /// Property returning the query string parameter name for error codes
        /// </summary>
        public static string QueryParameterErrorCode
        {
            get { return "errorCode"; }
        }

        /// <summary>
        /// Gets GeneralConfigFolder
        /// </summary>
        public static string GeneralConfigFolder
        {
            get { return "general"; }
        }

        /// <summary>
        /// Gets MaxNumberOfSearchHotelResultsKey
        /// </summary>
        public static string MaxNumberOfSearchHotelResultsKey
        {
            get { return "MaxNumberOfSearchHotelResults"; }
        }

        /// <summary>
        /// Gets NumberOfHotelToRequestOnAjaxCallKey
        /// </summary>
        public static string NumberOfHotelToRequestOnAjaxCallKey
        {
            get { return "NumberOfHotelToRequestOnAjaxCall"; }
        }

        /// <summary>
        /// Gets LoginProcessingPath
        /// </summary>
        public static string LoginProcessingPath
        {
            get { return "LoginProcessingPath"; }
        }

        /// <summary>
        /// Gets ServerHostRegx
        /// </summary>
        public static string ServerHostRegx
        {
            get { return "ServerHostRegx"; }
        }

        /// <summary>
        /// Gets MembershipIdCookie
        /// </summary>
        public static string MembershipIdCookie
        {
            get { return "Mobile.MembershipId"; }
        }

        /// <summary>
        /// Gets PrefixMembershipId
        /// </summary>
        public static string PrefixMembershipId
        {
            get { return "PrefixMembershipId"; }
        }

        /// <summary>
        /// Gets ReturnUrlQueryString
        /// </summary>
        public static string ReturnUrlQueryString
        {
            get { return "ReturnUrl"; }
        }

        /// <summary>
        /// Gets DateFormat
        /// </summary>
        public static string DateFormat
        {
            get { return "yyyy/MM/dd"; }
        }

        /// <summary>
        /// Gets IOS5DateFormat
        /// </summary>
        public static string IOS5DateFormat
        {
            get { return "yyyy-MM-dd"; }
        }

        /// <summary>
        /// Gets IOS5DateFormat
        /// </summary>
        public static string SearchItineraryDateFormat
        {
            get { return "ddd, dd MMM"; }
        }

        /// <summary>
        /// Gets LanguageCookie
        /// </summary>
        public static string LanguageCookie
        {
            get { return "UserLocaleCookie"; }
        }

        /// <summary>
        /// Gets IOS5UserAgent
        /// </summary>
        public static string IOS5UserAgent
        {
            get { return "IOS5UserAgent"; }
        }

        /// <summary>
        /// Gets IEMobileUserAgent
        /// </summary>
        public static string IEMobileUserAgent
        {
            get { return "IEMobileUserAgent"; }
        }

        /// <summary>
        /// Gets EarlyColor
        /// </summary>
        public static string EarlyColor
        {
            get { return "EarlyColor"; }
        }

        /// <summary>
        /// Gets FlexColor
        /// </summary>
        public static string FlexColor
        {
            get { return "FlexColor"; }
        }

        /// <summary>
        /// Gets SpecialRateColor
        /// </summary>
        public static string SpecialRateColor
        {
            get { return "SpecialRateColor"; }
        }

        /// <summary>
        /// Gets FetchRateTypeColorFromCMS
        /// </summary>
        public static string FetchRateTypeColorFromCMS
        {
            get { return "FetchRateTypeColorFromCMS"; }
        }

        /// <summary>
        /// Gets KeyText
        /// </summary>
        public static string KeyText
        {
            get { return "Key"; }
        }

        /// <summary>
        /// Gets ValueText
        /// </summary>
        public static string ValueText
        {
            get { return "Value"; }
        }

        /// <summary>
        /// Gets ServerSideWaitTimeForResults
        /// </summary>
        public static string ServerSideWaitTimeForResults
        {
            get { return "ServerSideWaitTimeForResults"; }
        }

        /// <summary>
        /// Gets MobileAppInitializerPath
        /// </summary>
        public static string MobileAppInitializerPath
        {
            get { return "MobileAppInitializerPath"; }
        }

        /// <summary>
        /// Gets ImageVaultHost
        /// </summary>
        public static string ImageVaultHost
        {
            get { return "ImageVaultHost"; }
        }

        /// <summary>
        /// Gets ImageURLWidthRegx
        /// </summary>
        public static string ImageURLWidthRegx
        {
            get { return "ImageURLWidthRegx"; }
        }

        /// <summary>
        /// Gets MissingImageURLWidthRegx
        /// </summary>
        public static string MissingImageURLWidthRegx
        {
            get { return "MissingImageURLWidthRegx"; }
        }

        /// <summary>
        /// Gets CurrencyRegx
        /// </summary>
        public static string CurrencyRegx
        {
            get { return "currencyRegx"; }
        }

        /// <summary>
        /// Gets ImageURLWidthReplacmentString
        /// </summary>
        public static string ImageURLWidthReplacmentString
        {
            get { return "ImageURLWidthReplacmentString"; }
        }

        /// <summary>
        /// Gets RequestWaitTime
        /// </summary>
        public static string RequestWaitTime
        {
            get { return "RequestWaitTime"; }
        }

        /// <summary>
        /// Gets LanguageSessionKey
        /// </summary>
        public static string LanguageSessionKey
        {
            get { return "Mobile.LanguageKey"; }
        }

        /// <summary>
        /// Gets GuranteedTextCodeBefore6PM
        /// </summary>
        public static string GuranteedTextCodeBefore6PM
        {
            get { return "GTDCC1800"; }
        }

        /// <summary>
        /// Gets GuranteedTextCodeAfter6PM
        /// </summary>
        public static string GuranteedTextCodeAfter6PM
        {
            get { return "GTDCC"; }
        }

        /// <summary>
        /// Gets GuranteedTextCodeRewardNights
        /// </summary>
        public static string GuranteedTextCodeRewardNights
        {
            get { return "RewardNights"; }
        }

        /// <summary>
        /// Gets RewardNightBooking
        /// </summary>
        public static string RewardNightBooking
        {
            get { return "RewardNight"; }
        }

        /// <summary>
        /// Gets GuranteedTextCodeBlockCode
        /// </summary>
        public static string GuranteedTextCodeBlockCode
        {
            get { return "GTDRB"; }
        }

        public static string GuranteedTextCodeNoCancellation
        {
            get { return "GTDDEP"; }
        }

        public static string GuranteedTextPrepaidInProgress
        {
            get { return "PREIP"; }
        }

        /// <summary>
        /// Gets MonthsList
        /// </summary>
        public static string MonthsList
        {
            get { return "monthsList"; }
        }

        /// <summary>
        /// Gets WeekDaysList
        /// </summary>
        public static string WeekDaysList
        {
            get { return "weekDaysList"; }
        }

        /// <summary>
        /// Gets PhoneRegx
        /// </summary>
        public static string PhoneRegx
        {
            get { return "mobilePhoneRegx"; }
        }

        /// <summary>
        /// Gets EmailRegx
        /// </summary>
        public static string EmailRegx
        {
            get { return "emailAddressRegx"; }
        }

        /// <summary>
        /// Gets 
        /// </summary>
        public static string CreditCardNumberRegx
        {
            get { return "creditCardRegx"; }
        }

        /// <summary>
        /// Gets LanguageSwitch
        /// </summary>
        public static string LanguageSwitch
        {
            get { return "lSwitch"; }
        }

        /// <summary>
        /// Gets NonSupportedLanguagesPattren 
        /// </summary>
        public static string NonSupportedLanguagesPattren
        {
            get { return "nonSupportedLanguages"; }
        }

        /// <summary>
        /// Gets NonSupportedLanguageSwitch 
        /// </summary>
        public static string NonSupportedLanguageSwitch
        {
            get { return "nSupported"; }
        }
        public static string ActualLanguage { get { return "aLang"; } }

        /// <summary>
        /// Gets ErrorPagePath
        /// </summary>
        public static string ErrorPagePath
        {
            get { return ConfigurationManager.AppSettings["erroPage"]; }
        }

        /// <summary>
        /// Gets PerNightString
        /// </summary>
        public static string PerNightString
        {
            get { return "PerNight"; }
        }

        /// <summary>
        /// Gets PerStayString
        /// </summary> 
        public static string PerStayString
        {
            get { return "PerStay"; }
        }

        /// <summary>
        /// Gets SourceCode
        /// </summary>
        public static string SourceCode
        {
            get { return "SourceCode"; }
        }

        /// <summary>
        /// Gets InclusiveAllTax
        /// </summary>
        public static string InclusiveAllTax
        {
            get { return "includeAllTaxesText"; }
        }

        /// <summary>
        /// Gets EmailConfirmationContainerCMSReference
        /// </summary>
        public static string EmailConfirmationContainerCMSReference
        {
            get { return "EmailConfirmationContainer"; }
        }

        /// <summary>
        /// Gets MobileTheme
        /// </summary>
        public static string MobileTheme
        {
            get { return ConfigurationManager.AppSettings["mobileTheme"]; }
        }

        /// <summary>
        /// Gets CallUsTextId
        /// </summary>
        public static string CallUsTextId
        {
            get { return "callUsText"; }
        }

        /// <summary>
        /// Gets CookieDisableMsgId
        /// </summary>
        public static string CookieDisableMsgId
        {
            get { return "CookieDisableMsg"; }
        }

        /// <summary>
        /// Gets JavascriptDisableMsgId
        /// </summary>
        public static string JavascriptDisableMsgId
        {
            get { return "JavascriptDisableMsg"; }
        }

        /// <summary>
        /// Gets ImagePlaceHolderPath
        /// </summary>
        public static string ImagePlaceHolderPath
        {
            get { return "/ScanwebMobile/Public/img/imgplaceholder.png"; }
        }

        /// <summary>
        /// Gets MetaDescriptionId
        /// </summary>
        public static string MetaDescriptionId
        {
            get { return "MetaDescription"; }
        }

        public static string MetaKeywordsID
        {
            get { return "Keywords"; }
        }

        public static string BookMarkOverlayKey
        {
            get { return "BookMarkOverlay"; }
        }

        public static string BookMarkOverlayText
        {
            get { return "BookMarkOverlayText"; }
        }

        public static string SmsSentConfirmation
        {
            get { return "SMSConfirmationTobeSent"; }
        }

        public static string EmailSentConfirmation
        {
            get { return "EmailConfirmationTobeSent"; }
        }
        //Application Global constants
        public const string APPLICATION_ID = "ScanWebMobile";

        //Booking code starting key constants
        public static string CORPORATE_KEYCODE = "D";
        public static string TRAVELAGENT_KEYCODE = "L";
        public static string BLOCKCODE_KEYCODE = "B";
        public static string VOUCHER_KEYCODE = "VO";

        // CMS Page Type constants
        public const string SCANWEB_SELECT_RATE_REFERENCE = "ReservationSelectRatePage";
        public const string START_PAGE_CMS_REFERENCE = "MobileStartPage";
        public const string ERROR_PAGE_CMS_REFERENCE = "MobileErrorPage";
        public const string RESERVATION_CONTAINER_CMS_REFERENCE = "MobileReservationContainer";
        public const string STANDARD_PAGE_CMS_REFERENCE = "MobileStandardpage";
        public const string SEARCH_HOTEL_PAGE_CMS_REFERENCE = "MobileSearchHotelPage";
        public const string SEARCH_NEARBY_HOTEL_PAGE_CMS_REFERENCE = "MobileSearchNearbyHotelPage";
        public const string SELECT_HOTEL_PAGE_CMS_REFERENCE = "MobileSelectHotelPage";
        public const string SELECT_RATE_PAGE_CMS_REFERENCE = "MobileSelectRatePage";
        public const string BOOKING_DETAIL_PAGE_CMS_REFERENCE = "MobileBookingDetailPage";
        public const string BOOKING_CONFIRMATION_PAGE_CMS_REFERENCE = "MobileBookingConfirmationPage";
        public const string SIGNIN_PAGE_CMS_REFERENCE = "MobileSignInPage";
        public const string MOBILE_CMS_PAGE_DATA = "MobileSitData";
        public const string TERMS_AND_CONDITIONS_PAGE_CMS_REFERENCE = "MobileTermsAndConditions";
        public const string PRICE_INFORMATION_PAGE_CMS_REFERENCE = "MobilePriceInformation";
        public const string FEEDBACK_PAGE_CMS_REFERENCE = "MobileFeedback";
        public const string CONTACTUS_PAGE_CMS_REFERENCE = "MobileContactUs";
        public const string VIEWBOOKING_PAGE_CMS_REFERENCE = "ViewBookingsPage";
        public const string MYBOOKING_PAGE_CMS_REFRENCE = "MyBookingsPage";
        public const string MODIFY_CANCEL_PAGE_CMS_REFRENCE = "ModifyCancelPage";
        public const string CANCEL_CONFIRMATION_PAGE_CMS_REFRENCE = "CancelConfirmation";
        public const string ALL_MOBILE_OFFERS = "OffersContainer";
        public const string MOBILE_OFFER_DETAIL = "OfferDetailsPage";
        public const int MAX_TERMS_AND_PRICEINFO_ITEMS = 10;
        public const int MAX_TERMS_AND_PRICEINFO_EXTERNALLINKS = 2;

        public static string MOBILE_CMS_PAGE_DATA_LIST =
            "MobileStartPage,MobileSearchHotelPage,MobileSelectHotelPage,MobileSelectRatePage,MobileBookingDetailPage,MobileBookingConfirmationPage,MobileStandardpage,MobileSignInPage";

        // CMS Page properties
        public static string SELECT_RATE_ABOUT_OURRATES_HEADING = "AboutOurRatesHeading";
        public static string SELECT_RATE_ABOUT_OURRATES = "AboutOurRateDescription";
        // Booking flow error messages - these error code mapped with the messages in the xml config files.
        public const string NO_HOTELS_FOUND = "nohotelsfound";
        public const string NO_HOTELS_WITH_DNUMBER = "nohotelswithdnumber";
        public const string NO_HOTELS_WITH_PROMOCODE = "nohotelswithpromocode";
        public const string NO_ROOMRATE_WITH_DNUMBER = "noroomratewithdnumber";
        public const string ALTERNATE_HOTELS_AVAILABLE = "altcityhotelsmsg";
        public static string SEARCH_NEARBY_MEASSAGE = "SearchNearbyMessageText";
        public static string NO_HOTELS_WITH_DNUMBER_XPATH = "/scanweb/bookingengine/errormessages/businesserror/nohotelswithdnumber";
        public static string NO_HOTELS_WITH_PROMOCODE_XPATH = "/scanweb/bookingengine/errormessages/businesserror/nohotelswithpromocode";
        public static string CITY_CENTER_DISTANCE_XPATH = "/bookingengine/booking/selecthotel/citycenterdistance";
        public static string CURRENT_LOCATION_DISTANCE_XPATH = "/bookingengine/booking/selecthotel/currentlocationdistance";
        public static string NEAR_BY_HOTELS = "/bookingengine/booking/selecthotel/nearbyhotels";
        public static string NEAR_BY_POSITION = "/bookingengine/booking/searchhotel/nearmyposition";
        public static string ALTERNATE_HOTEL_DISTANCE_XPATH = "/bookingengine/booking/selecthotel/alternatehoteldistance";
        public static string ALTERNATE_HOTELS_MESSAGE = "/bookingengine/booking/selecthotel/alternateHotelMessage";
        //Error Messages for Rates
        public static string NO_ROOMRATE_WITH_DNUMBER_XPATH = "/scanweb/bookingengine/errormessages/businesserror/noroomratewithdnumber";
        public static string BONUS_CHEQUE_NOT_FOUND = "BCNF";
        public static string BONUS_CHEQUE_NOT_FOUND_XPATH = "/scanweb/bookingengine/errormessages/businesserror/bonuschequenotfound";
        public const string NO_RATECODES_AVAILABLE = "No RateCodes/Rooms Available:";
        public const string NO_ROOMRATES = "noratecode";
        public static string NO_ROOMRATES_XPATH = "/scanweb/bookingengine/errormessages/businesserror/customerMessage";
        public const string RATES_EARLY = "EARLY";
        public const string RATES_FLEX = "FLEX";
        public const string RATES_CATEGORY_ORANGE = "orange";
        public const string RATES_CATEGORY_BLUE = "blue";

        //Booking objects cache keys
        public const string DESTINATION_SEARCH_ENTITY = "SearchEntity";
        public static string MOBILE_CONTENT_LANGUAGE = "MobileContentLanguage";
        public static string MOBILE_ACTUAL_LANGUAGE = "MobileActualLanguage";

        //Booking Details constants
        public static string NOT_ENOUGH_POINTS = "THIS CERTIFICATE OR AWARDS DONOT HAVE ENOUGH POINTS TO BOOK RESERVATION.";
        public static string ROOM_UNAVAILABLE_FOR_REDEMPTION = "RESORT IS RESTRICTED OR ROOM IS NOT AVAILABLE.";
        public static string NOT_ENOUGH_POINTS_XPATH = "/scanweb/bookingengine/errormessages/owserror/not_enough_point_msg";
        public static string ROOM_UNAVAILABLE_FOR_REDEMPTION_XPATH = "/scanweb/bookingengine/errormessages/owserror/room_unavailable";

        //Booking Confirmation constants
        public static string RESERVATION_NUMBER = "ReservationNumber";

        /// <summary>
        /// Gets StartPageTrackingHeading
        /// </summary>
        public static string StartPageTrackingHeading
        {
            get { return "Start"; }
        }

        /// <summary>
        /// Gets SearchHotelPageTrackingHeading
        /// </summary>
        public static string SearchHotelPageTrackingHeading
        {
            get { return "Search Hotel"; }
        }

        /// <summary>
        /// Gets SelectHotelPageTrackingHeading
        /// </summary>
        public static string SelectHotelPageTrackingHeading
        {
            get { return "Select Hotel"; }
        }

        /// <summary>
        /// Gets SelectRatePageTrackingHeading
        /// </summary>
        public static string SelectRatePageTrackingHeading
        {
            get { return "Select Rate"; }
        }

        /// <summary>
        /// Gets BookingDetailsPageTrackingHeading
        /// </summary>
        public static string BookingDetailsPageTrackingHeading
        {
            get { return "Booking Details"; }
        }

        /// <summary>
        /// Gets ConfirmationPageTrackingHeading
        /// </summary>
        public static string ConfirmationPageTrackingHeading
        {
            get { return "Confirmation"; }
        }

        /// <summary>
        /// Gets FeedbackPageTrackingHeading
        /// </summary>
        public static string FeedbackPageTrackingHeading
        {
            get { return "Feedback"; }
        }

        /// <summary>
        /// Gets ContactUsPageTrackingHeading
        /// </summary>
        public static string ContactUsPageTrackingHeading
        {
            get { return "Contact Us"; }
        }

        /// <summary>
        /// Gets TermsConditionPageTrackingHeading 
        /// </summary>
        public static string TermsConditionPageTrackingHeading
        {
            get { return "Terms & Condition"; }
        }

        /// <summary>
        /// Gets PriceInfoPageTrackingHeading
        /// </summary>
        public static string PriceInfoPageTrackingHeading
        {
            get { return "Price Informtion"; }
        }

        public static string ViewBookingsPageTrackingHeading { get { return "View Bookings"; } }
        public static string ModifyCancelPageTrackingHeading { get { return "Modify Cancel"; } }
        public static string CancelConfirmationPageTrackingHeading { get { return "Cancelled Booking"; } }
        public static string MyBookingsPageTrackingHeading { get { return "My Bookings"; } }
        /// <summary>
        /// Gets LoggedInUserTextTracking
        /// </summary>
        public static string LoggedInUserTextTracking
        {
            get { return "FGP Logged In"; }
        }

        /// <summary>
        /// Gets LoggedOutUserTextTracking 
        /// </summary>
        public static string LoggedOutUserTextTracking
        {
            get { return "Logged Out"; }
        }

        /// <summary>
        /// Gets ReportSuiteId
        /// </summary>
        public static string ReportSuiteId
        {
            get { return "reportSuiteId"; }
        }

        /// <summary>
        /// Gets ClickToCallFunctionName 
        /// </summary>
        public static string ClickToCallFunctionName
        {
            get { return "Click to call : {0} : Call us"; }
        }

        /// <summary>
        /// Gets LinkToMapFunctionName 
        /// </summary>
        public static string LinkToMapFunctionName
        {
            get { return "Link to map : {0}"; }
        }

        /// <summary>
        /// Gets EmailFunctionName
        /// </summary>
        public static string EmailFunctionName
        {
            get { return "Email : {0}"; }
        }

        /// <summary>
        /// Gets EmailFunctionName
        /// </summary>
        public static string TripAdvisorFunctionName
        {
            get { return "Trip Advisor"; }
        }


        //Content Pages
        public const string TERMS_AND_CONDITIONS = "TC";
        public const string PRICE_INFORMATION = "PI";
        public const string CONTACT_US = "CU";
        public const string FEEDBACK = "FDB";
        public const string PAGE_ID = "pageId";
        public const string THANK_YOU_MSG = "ThankYouMsg";
        public const string SHOW = "Show";

        //opera membership level
        public const string MEMBERSHIPLEVEL_1STFLOOR = "1STFLOOR";
        public const string MEMBERSHIPLEVEL_2NDFLOOR = "2NDFLOOR";
        public const string MEMBERSHIPLEVEL_3RDFLOOR = "3RDFLOOR";
        public const string MEMBERSHIPLEVEL_TOPFLOOR = "TOPFLOOR";

        public const string CREDIT_CARD_MASK = "XXXXXXXXXXXX";
        public const string NO_SUPPORTED_URL = "NoSupportedUrl";
        //Mobile 1.5
        public const string RESERVATION_ID_QUERY_STRING = "rId";
        public const string LAST_NAME_QUERY_STRING = "lName";
        public const string REDIRECT_BACK_ON_ERROR = "rOE";
        public const string NO_RESERVATION = "NR";
        public const string CANCELLED_RESERVATION = "CR";
        public const string NON_WEB_BOOKING = "NWB";
        public const string EMPTY_LAST_NAME = "LN";
        public const string EMPTY_LAST_NAME_ERR_MSG_ID = "/scanweb/bookingengine/errormessages/requiredfielderror/invalidSurname";
        public const string WRONG_LAST_NAME = "WLN";
        public const string OLD_BOOKING = "OBK";
        public const string OLD_BOOKING_MSG_ID = "/bookingengine/booking/ModifyBookingDates/PastBookingMsg";
        public const string EARLY_BOOKING = "EBK";
        public const string FLEX_BOOKING = "FBK";
        public const string MESSAGE = "Message";
        public const string INVALID_SESSION = "InvalidSession";
        public const string ERROR_COUNT = "ErrorCount";
        public const string POST_URL = "PostUrl";
        public const string MODIFY_BOOKING_DETAILS_SESSION_KEY = "Mobile.ModifyBookingDetailsKey";
        public const string MOBILE_REVIEW_POWERED_BY = "TripAdvisorReviews";
        public const string MOBILE_TRIP_ADVISOR_FOOTER_IMAGE = "MobileTripAdvisorLogoURL";
        public const string MOBILE_DEEPLINK_ROOMRATE_PROCESSING = "MobileDeeplinkRoomRateProcessing";
        public const string MOBILE_DEEPLINK_NO_OF_ROOMTYPES_TO_DISPLAY = "MobileDeeplinkNoOfRoomTypesToDisplay";
        public const string DEEPLINK_VIEWMODIFY_RESERVATIONID_QUERYSTRING = "RID";
        public const string DEEPLINK_VIEWMODIFY_LASTNAME_QUERYSTRING = "LNM";
        public const string MOBILE_CONFIRMATION = "MCfrn";
        public const string SHOW_OFFER_FOR_MOBILE = "ShowOfferForMobile";
        public const string OFFER_END_TIME = "OfferEndTime";
        public const string MOBILE_OFFER_IMAGE = "MobileOfferImage";
        public const string DESKTOP_OFFER_IMAGE = "BoxImageMedium";
        public const string OFFER_OVERVIEW_PAGE = "OfferOverviewPage";
        public const string BOX_HEADING = "BoxHeading";
        public const string OFFER_CAROUSEL_IMAGE_COUNT = "10";
        public const string HOMEPAGE_CAROUSEL_OFFERS_COUNT = "5";

        //Mobile UserAction Tracking Constants
        public const string CHECKIN_DATE = "CheckInDate";
        public const string CHECKOUT_DATE = "CheckOutDate";
        public const string NO_OF_ADULTS = "NoOfAdults";
        public const string NO_OF_CHILDREN = "NoOfChildren";
        public const string SEARCH_DESTINATION = "SearchDestination";
        public const string SEARCH_DESTINATION_ID = "SearchDestinationId";
        public const string SEARCH_TYPE = "SearchType";
        public const string GUARANTEE_TYPE = "GuaranteeType";
        public const string SELECTED_RATE = "SelectedRate";
        public const string SELECTED_RATE_TYPE = "SelectedRateType";
        public const string SELECTED_RATE_TYPE_ID = "SelectedRateTypeId";
        public const string SELECTED_ROOMTYPE = "SelectedRoomType";
        public const string SELECTED_ROOMTYPE_ID = "SelectedRoomTypeId";
        public const string SELECTED_HOTEL = "SelectedHotel";
        public const string SELECTED_HOTEL_ID = "SelectedHotelId";
        public const string GETCONFIRM_MESSAGE_OVERLAY = "GetConfirmMessageOverlay";
        public const string CANCEL_ROOM = "CancelRoom";
        public const string GETCANCEL_BOOKING_OVERLAY = "GetCancelBookingOverlay";
        public const string CANCLE_COMPLETE_BOOKING = "CancelCompleteBooking";
        public const string ISROOM_CANCELLABLE = "IsRoomCancellable";
        public const string RESERVATIONNUMBER = "ReservationNumber";
        public const string POLICYCODE = "PolicyCode";
        public const string ROOMHEADING_TEXT = "RoomHeadingText";
        public const string CANCELLATION_CONTROL_ID = "CancellationControlId";
        public const string CURRENT_LANGUAGE = "CurrentLanguage";
        public const string LEGNUMBER = "LegNumber";
        public const string FIRST_NAME = "FirstName";
        public const string LAST_NAME = "LastName";
        public const string PHONE_NUMBER = "PhoneNumber";
        public const string CANCELLATION_NUMBER = "CancellationNumber";
        public const string ERROR_MESSAGE = "ErrorMessage";
        public const string HOTELCODE = "HotelCode";
        public const string SEARCHTYPE_CODE = "SearchTypeCode";
        public const string URL_REFERRER = "UrlReferrer";
        public const string OFFER_PAGE_ID = "OfferPageId";
        public const string GETFUTURE_RESERVATION_TODISPLAY = "GetFutureReservationToDisplay";
        public const string GETUSER_HISTORY_TODISPLAY = "GetUserHistoryToDisplay";
        public const string CURRENT_DISPLAYED_BOOKINGS = "CurrentDisplayedBookings";
        public const string NO_OF_BOOKINGS_TODISPLAY = "NoOfBookingsToDisplay";
        public const string SORT_TYPE = "SortType";
        public const string SORT_ORDER = "SortOrder";
        public const string FETCH_LATEST_ITEMS = "FetchLatestItems";
        public const string CURRENT_DISPLAYED_TRANSACTIONS = "CurrentDisplayedTransactions";
        public const string NO_OF_TRANSACTIONS_TODISPLAY = "NoOfTransactionsToDisplay";
        public const string PAGE_NAME = "PageName";
        public const string METHOD_NAME = "MethodName";
        public const string CURRENTLY_RENDERED_ROOMTYPES = "CurrentlyRenderedRoomTypes";
        public const string NOOF_ROOMTYPES_TODISPLAY = "NoOfRoomTypesToDisplay";
        public const string NO_OFFER_MESSAGE = "/scanweb/bookingengine/errormessages/businesserror/NoOfferMessage";
        public const string NAME_ID = "NameId";
        public const string MEMBERSHIP_ID = "MembershipId";
        public const string EMAIL_ID = "EmailId";
        public const string MOBILE = "MobileNumber";
    }
}