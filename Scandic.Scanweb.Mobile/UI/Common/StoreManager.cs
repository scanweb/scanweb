﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Mobile.UI.Common.Interface;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.Mobile.UI.Common
{
    public class StoreManager
    {
        #region Declaration
            private static StoreManager instance;
            private static object syncLock = new object();
            private List<Object> stores;
        #endregion

        #region Constructor
            public StoreManager()
            {
                ConfigureStore();
            }
        #endregion

        #region Property
            public static StoreManager Instance
            {
                get
                {
                    if (instance == null)
                    {
                        lock (syncLock)
                        {
                            if (instance == null)
                            {
                                instance = new StoreManager();
                            }
                        }
                    }

                    return instance;
                }
            }
        #endregion

        #region Methods
            private void ConfigureStore()
            {
                stores =  DependencyResolver.Instance.GetServices(typeof(IStore)).ToList<Object>();
            }

            public IStore GetStore(StoreType type)
            {
                if (stores != null)
                {
                    return stores.Where(store => ((IStore) store).Type == type).FirstOrDefault() as IStore;
                }
                else
                {
                    throw new ApplicationException("Mobile data stores not intialized.");
                }

            }
        #endregion
    }
}
