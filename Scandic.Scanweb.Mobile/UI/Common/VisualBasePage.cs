﻿//  Description					:   VisualBasePage                                        //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.Templates;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Controls;
using Scandic.Scanweb.Mobile.UI.Controls.Interface;
using Scandic.Scanweb.Mobile.UI.Entity;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;
using Scandic.Scanweb.Mobile.UI.Entity.Tracking;
using Scandic.Scanweb.Mobile.UI.Tracking;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.Mobile.UI.Common
{
    /// <summary>
    /// This is the base class for UI webpages that set's the Page's theme,
    /// and language specific labels. This class also contains the common
    /// pagemethods applicable to all UI pages.
    /// </summary>
    /// <typeparam name="ConfigType">This generic type should be set to the 
    /// type of page's configuration class. In case there is no configuration
    /// for the page then this type should be set to any on configuartion datatype
    /// such as String etc.</typeparam>
    public class VisualBasePage<ConfigType> : BasePage
    {
        #region Variables


        #endregion
        #region Protected Methods

        /// <summary>
        /// Page on preinit event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            Page.Theme = Reference.MobileTheme;
            LanguageRedirectionHelper.LanguageSpecificRedirect(PageId());
        }

        /// <summary>
        /// Prerender event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            FixAppThemeLinks();
            SetLanguage();
        }

        /// <summary>
        /// Pageload event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreLoad(EventArgs e)
        {
            base.OnPreLoad(e);
            if (!this.IsPostBack)
            {
                SetPageInfoFromConfig();
            }
        }
        public Control FindControlRecursive(Control Root, string Id)
        {
            if (Root.ID == Id)
                return Root;

            foreach (Control Ctl in Root.Controls)
            {
                Control FoundCtl = FindControlRecursive(Ctl, Id);
                if (FoundCtl != null)
                    return FoundCtl;
            }

            return null;
        }



        /// <summary>
        /// Renders error messages
        /// </summary>
        /// <param name="validationErrors"></param>
        protected void RenderErrorMessages(List<KeyValueOption> validationErrors)
        {
            var errorControls = this.Page.Form.Controls.All()
                .Where(ctrl =>
                       Type.Equals(typeof(HtmlGenericControl), ctrl.GetType()) &&
                       ctrl.ID.IndexOf("Error") != -1);

            foreach (var errorCtrl in errorControls)
            {
                var valError = validationErrors.Where(error => string.Equals(errorCtrl.ID, error.Key)).FirstOrDefault();
                var ctrl = errorCtrl as HtmlGenericControl;
                if (valError != null)
                {
                    ctrl.InnerText = valError.Value;
                    ctrl.Attributes.Add("style", "display: block;");
                }
                else
                {
                    ctrl.Attributes.Add("style", "display: none;");
                }
            }
        }

        #endregion

        #region Virtual Method

        /// <summary>
        /// PageId
        /// </summary>
        /// <returns></returns>
        public virtual MobilePages PageId()
        {
            return MobilePages.None;
        }

        #endregion

        #region Page Methods

        /// <summary>
        /// Gets overlay for the given data.
        /// </summary>
        /// <param name="overlayType"></param>
        /// <param name="id"></param>
        /// <param name="associatedControl"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetOverlay(string overlayType, string id, string associatedControl, string value)
        {
            string htmlMarkUp = string.Empty;
            try
            {
                switch (overlayType)
                {
                    case "language":
                        htmlMarkUp = OverlayHelper.GetLanguageOverlay();
                        break;
                    case "tooltip":
                        htmlMarkUp = GetToolTip(id, associatedControl);
                        break;
                    case "profile":
                        htmlMarkUp = OverlayHelper.GetProfileOverlay(id);
                        break;
                    case "ratetooltip":
                        htmlMarkUp = GetRateTooltip(id, value);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                List<KeyValueParam> keyValue = new List<KeyValueParam>();
                keyValue.Add(new KeyValueParam(Reference.PAGE_NAME, "VisualBasePage"));
                keyValue.Add(new KeyValueParam(Reference.METHOD_NAME, "GetOverlay"));
                keyValue.Add(new KeyValueParam("OverlayType", overlayType));
                keyValue.Add(new KeyValueParam("OverlayID", Convert.ToString(id)));
                keyValue.Add(new KeyValueParam("OverlayData", value));
                
                AddActionParameters(ex, keyValue, true);
                throw ex;
            }
            return htmlMarkUp;
        }

        /// <summary>
        /// Gets calendar data
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static CalenderData GetCalenderData()
        {
            var calenderData = new CalenderData();
            var controller = new BaseController("");
            try
            {
                var searchHotelConfig = controller.GetPageConfig<SearchHotelPageSection>();

                var months = searchHotelConfig.PageDetail.PageMessages.GetMessage(Reference.MonthsList);
                var weekDays = searchHotelConfig.PageDetail.PageMessages.GetMessage(Reference.WeekDaysList);

                if (!string.IsNullOrEmpty(months))
                {
                    calenderData.Months = months.Split(',').ToList();
                }

                if (!string.IsNullOrEmpty(weekDays))
                {
                    calenderData.WeekDays = weekDays.Split(',').ToList();
                }
            }
            catch (Exception ex)
            {
                List<KeyValueParam> keyValue = new List<KeyValueParam>();
                keyValue.Add(new KeyValueParam(Reference.PAGE_NAME, "VisualBasePage"));
                keyValue.Add(new KeyValueParam(Reference.METHOD_NAME, "GetCalenderData"));
                AddActionParameters(ex, keyValue, true);
                throw ex;
            }
            return calenderData;
        }

        /// <summary>
        /// Gets page tracking data
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        [WebMethod]
        public static TrackingDetails GetPageTrackingData(int pageId, string data)
        {
            MobilePages mobilePageId = (MobilePages)pageId;
            var controller = new BaseController("");
            try
            {
                var trackingManager = new TrackingManager(controller.CurrentContext, mobilePageId);
                return trackingManager.GetPageTrackingInfo(data);
            }
            catch (Exception ex)
            {
                List<KeyValueParam> keyValue = new List<KeyValueParam>();
                keyValue.Add(new KeyValueParam(Reference.PAGE_NAME, "VisualBasePage"));
                keyValue.Add(new KeyValueParam(Reference.METHOD_NAME, "GetPageTrackingData"));
                keyValue.Add(new KeyValueParam(Reference.PAGE_ID, Convert.ToString(pageId)));
                keyValue.Add(new KeyValueParam("Data", data));
                AddActionParameters(ex, keyValue, true);
                throw ex;
            }
        }

        /// <summary>
        /// Gets function tracking data
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="functionId"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [WebMethod]
        public static TrackingDetails GetFunctionTrackingData(int pageId, int functionId, string data)
        {
            MobilePages mobilePageId = (MobilePages)pageId;
            TrackingFunctions function = (TrackingFunctions)functionId;
            var controller = new BaseController("");
            try
            {
                var trackingManager = new TrackingManager(controller.CurrentContext, mobilePageId);
                return trackingManager.GetFunctionTrackingData(function, data);
            }
            catch (Exception ex)
            {
                List<KeyValueParam> keyValue = new List<KeyValueParam>();
                keyValue.Add(new KeyValueParam(Reference.PAGE_NAME, "VisualBasePage"));
                keyValue.Add(new KeyValueParam(Reference.METHOD_NAME, "GetFunctionTrackingData"));
                keyValue.Add(new KeyValueParam(Reference.PAGE_ID, Convert.ToString(pageId)));
                keyValue.Add(new KeyValueParam("FunctionId", Convert.ToString(functionId)));
                keyValue.Add(new KeyValueParam("Data", data));
                AddActionParameters(ex, keyValue, true);
                throw ex;
            }
        }

        #endregion

        #region Private Methods

        private static void AddActionParameters(Exception ex, List<KeyValueParam> parameters, bool clearSession)
        {
            ActionItem action = new ActionItem();
            action.ActionDate = DateTime.Now;
            action.Parameters = new List<KeyValueParam>();
            action.Parameters.AddRange(parameters);
            UserNavTracker.TrackAction(action, clearSession);
            UserNavTracker.LogAndClearTrackedData(ex);
        }

        private void SetLanguage()
        {
            var masterPage = Page.Master as MobileDefault;

            if (masterPage != null)
            {
                masterPage.CurrentLanguage = LanguageRedirectionHelper.GetCurrentLanguage();
            }
        }

        private static string GetToolTip(string tooltipId, string associatedControlId)
        {
            var pageConfig = GetPageSection();
            return OverlayHelper.GetTooltipOverlay(pageConfig, tooltipId, associatedControlId);
        }

        private static string GetRateTooltip(string id, string value)
        {
            var values = value.Split('#');
            var rateCategoryId = values[0];
            bool isBlockCode = false;
            if (values.Length > 1)
            {
                bool.TryParse(values[1], out isBlockCode);
            }

            string htmlMarkup = OverlayHelper.GetRateTooltip(id, rateCategoryId, isBlockCode);

            return htmlMarkup;
        }
        private void FixAppThemeLinks()
        {
            ISiteInfoRepository siteInfoRepository =
                DependencyResolver.Instance.GetService(typeof(ISiteInfoRepository)) as ISiteInfoRepository;
            var styleLinks = this.Header.Controls.All()
                .Where(ctrl => Type.Equals(typeof(HtmlLink), ctrl.GetType())
                               && ((HtmlLink)ctrl).Href.IndexOf("App_Themes") != -1);
            foreach (var link in styleLinks)
            {
                var styleLink = link as HtmlLink;
                var cssPath = styleLink.Href.Substring(styleLink.Href.IndexOf("App_Themes"));
                var styleLinkPath = string.Format("/{0}", cssPath);
                styleLink.Href = styleLinkPath;
                if (!siteInfoRepository.IsIEMobile(Request.UserAgent) &&
                    styleLinkPath.IndexOf("iemobile", StringComparison.InvariantCultureIgnoreCase) != -1)
                {
                    this.Header.Controls.Remove(link);
                }
            }
        }

        private static IPageSection GetPageSection()
        {
            IPageSection pageConfig = null;
            var pageController = new BaseController("");

            if (typeof(ConfigType).GetInterface("Scandic.Scanweb.Mobile.UI.Entity.Configuration.IPageSection", true) !=
                null)
            {
                pageConfig = pageController.GetPageConfig<ConfigType>() as IPageSection;
            }

            return pageConfig;
        }

        /// <summary>
        /// This method will read through the page's config set the language specfic labels on it.
        /// </summary>
        private void SetPageInfoFromConfig()
        {
            var pageConfig = GetPageSection();
            if (pageConfig != null)
            {
                var pageDetails = pageConfig.GetPageSection();

                if (pageDetails != null)
                {
                    SetPageHeadingAndTitle(pageDetails.PageTitle, pageDetails.PageHeading);
                    IntializePageInputs(pageDetails.Inputs);
                }
            }
        }

        /// <summary>
        /// This method will set the language specific page title and heading of 
        /// </summary>
        /// <param name="pageTitle"></param>
        /// <param name="pageHeading"></param>
        private void SetPageHeadingAndTitle(string pageTitle, string pageHeading)
        {
            this.Title = pageTitle;
            var masterPage = this.Master as MobileDefault;

            if (masterPage != null)
            {
                masterPage.PageHeading = pageHeading;
            }
        }

        private void IntializePageInputs(List<Input> inputs)
        {
            if (inputs == null || inputs.Count == 0)
                return;


            foreach (var configInput in inputs)
            {
                var webControl = this.Form.Controls.All().Where(
                    ctrl => string.Equals(ctrl.ID, configInput.Id, StringComparison.InvariantCultureIgnoreCase)
                                     ).FirstOrDefault() as Control;
                IConfigUIControl configControl = null;

                switch (configInput.Type)
                {
                    case ConfigInputType.Label:
                        configControl = new LabelConfigUIControl();
                        break;
                    case ConfigInputType.Link:
                        configControl = new LinkConfigUIControl();
                        break;
                    case ConfigInputType.DropDown:
                        configControl = new DropDownConfigUIControl();
                        break;
                    case ConfigInputType.Button:
                        configControl = new ButtonConfigUIControl();
                        break;
                    case ConfigInputType.HTMLGenericControl:
                        configControl = new GenericHtmlConfigUIControl();
                        break;
                    case ConfigInputType.RadioOptions:
                        configControl = new RadioListConfigUIControl();
                        break;
                    case ConfigInputType.HTMLAnchorControl:
                        configControl = new HTMLAnchorConfigUIControl();
                        break;
                    default:
                        break;
                }

                if (configControl != null && webControl != null)
                {
                    configControl.SetConfigInfo(configInput, webControl, this.Page);
                }
            }
        }

        #endregion
    }
}