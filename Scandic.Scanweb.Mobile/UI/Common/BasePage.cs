﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Attributes;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.Mobile.UI.Common
{
    /// <summary>
    /// BasePage
    /// </summary>
    [AllowPublicAccess(false)]
    [AccessibleWhenSessionExpired(false)]
    [AccessibleWhenSessionInValid(true, false)]
    public class BasePage : System.Web.UI.Page
    {
        /// <summary>
        /// OnPreInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            CheckIfApplicationRestoredByBrowser();
            HasSessionExpired();
            Response.AppendHeader("Cache-Control", "no-cache");
            Response.AppendHeader("Cache-Control", "private");
            Response.AppendHeader("Cache-Control", "no-store");
            Response.AppendHeader("Cache-Control", "must-revalidate");
            Response.AppendHeader("Cache-Control", "max-stale=0");
            Response.AppendHeader("Cache-Control", "post-check=0");
            Response.AppendHeader("Cache-Control", "pre-check=0");
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Expires", DateTime.UtcNow.AddDays(-1).ToString());

            AddTestCookie();
        }

        /// <summary>
        /// OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            CheckPageAccess();
            PerformSessionValidityOperation();
            HttpCookie pageStartTimeCookie = new HttpCookie(AppConstants.PageStartTimeCookieId, DateTime.Now.ToString());
            pageStartTimeCookie.HttpOnly = true;
            Response.Cookies.Add(pageStartTimeCookie);
        }

        private void CheckIfApplicationRestoredByBrowser()
        {
            var attSessionExpired = Attribute.GetCustomAttribute(this.GetType(), typeof(AccessibleWhenSessionExpired)) as AccessibleWhenSessionExpired;

            if (!attSessionExpired.Accessible)
            {
                var pageStartTimeCookie = Request.Cookies[AppConstants.PageStartTimeCookieId];

                if (pageStartTimeCookie != null)
                {
                    DateTime pageStartTime;
                    if (DateTime.TryParse(pageStartTimeCookie.Value, out pageStartTime))
                    {
                        var pageElapsedTime = DateTime.Now.Subtract(pageStartTime);

                        if (pageElapsedTime.Minutes > AppConstants.PageRestoreTimeWindow && Session.IsNewSession)
                        {
                            Response.Redirect("/Mobile");
                        }
                    }

                }
            }

        }

        /// <summary>
        /// 1. checks whether request is made for the first time. And creates a cookie Host which contains
        /// the Machine name of the server where the request goes to. 
        /// 2. checks session validity
        /// </summary>
        private void PerformSessionValidityOperation()
        {
            var attSessionInvalid =
                Attribute.GetCustomAttribute(this.GetType(), typeof(AccessibleWhenSessionInValid)) as
                AccessibleWhenSessionInValid;
            var hostCookieName = Request.Cookies.Get("host") == null ? "not set" : Request.Cookies.Get("host").Value;
            bool isFirstRequest = string.Equals(hostCookieName, "not set") ? true : false;

            if (isFirstRequest)
            {
                HttpCookie transitCookie = new HttpCookie("host", System.Environment.MachineName);
                transitCookie.HttpOnly = true;
                HttpContext.Current.Response.Cookies.Add(transitCookie);
                hostCookieName = transitCookie.Value;
            }

            if (attSessionInvalid != null && !attSessionInvalid.Accessible)
            {
                CheckSessionValidity(hostCookieName, attSessionInvalid);
            }

        }
        /// <summary>
        /// Checks whether session is valid or not and also logs the Cookie "host" value and machine name
        /// if session lost due to sticky session both the cookie host value and current host name would be different
        /// else it is the case of accessing pages directly where session is required
        /// </summary>
        /// <param name="hostNameInCookie"></param>
        private void CheckSessionValidity(string hostNameInCookie, AccessibleWhenSessionInValid attSessionInvalid)
        {

            string cookieHostValue = hostNameInCookie;
            var bookingRepository =
                DependencyResolver.Instance.GetService(typeof(IBookingRepository)) as IBookingRepository;
            if ((bookingRepository == null || bookingRepository.CurrentContext == null) ||
                (attSessionInvalid.SearchCriteriaRequired && SearchCriteriaSessionWrapper.SearchCriteria == null))
            {

                var language = Request.Form["ctl00$hidCurrentLanguage"];
                if (string.IsNullOrEmpty(language))
                {
                    language = LanguageRedirectionHelper.GetCurrentLanguage();
                }
                var siteRepository =
                    DependencyResolver.Instance.GetService(typeof(ISiteInfoRepository)) as ISiteInfoRepository;
                if (siteRepository != null)
                {
                    var errorPath = siteRepository.GetPageUrl(MobilePages.Error, language);
                    string strSessionNotValidPage = String.Format("{0}?{1}=SessionNotValid&lang={2}", errorPath,
                                                                  Reference.QueryParameterErrorCode, language);
                    //Id to track user's flow in booking process is not available in Mobile, so sessionId here 
                    //will be empty.
                    string sessionId = string.Empty;

                    string logMessage = String.Format(
                        "MOBILE | Session Data Not Valid | Current cookie Host Value: {0} | Current Host Name: {1} | Current request URL: {2} | Url Referrer: {3} | UserAgent: {4} | IP Address: {5} | SessionId: {6}",
                        cookieHostValue, System.Environment.MachineName, Request.Url,
                        Request.UrlReferrer, Request.UserAgent, Utility.GetUserIPAddress(), sessionId);

                    AppLogger.LogSessionNotValidScenarios(logMessage);
                    if (!String.Equals(cookieHostValue, System.Environment.MachineName,
                                      StringComparison.InvariantCultureIgnoreCase))
                    {
                        HttpCookie transitCookie = new HttpCookie("host", System.Environment.MachineName);
                        transitCookie.HttpOnly = true;
                        HttpContext.Current.Response.Cookies.Add(transitCookie);
                        Response.Redirect(strSessionNotValidPage);
                    }
                    else
                    {
                        Response.Redirect("/Mobile");
                    }
                }
            }


        }              

        /// <summary>
        /// Override of the original implementation to persist the ViewState in 
        /// Session rather than in a hidden field to accomodate low bandwidth clients
        /// </summary>
        //Commented by Chandra for Memory Leak Issue --16th Oct 2012
        //protected override System.Web.UI.PageStatePersister PageStatePersister
        //{
        //    get { return new SessionPageStatePersister(this); }
        //}

        /// <summary>
        /// This method is implemented to check if user has the access to the requested page.
        /// This is done as Scanweb is not implementing the FormAuthentication, had it been the
        /// case then there was no need for this method and default authentication/autherization
        /// functionality of ASP.NET would have been used. Do not remove this method until ScanWeb
        /// changes it authentication stratergy, to avoid Mobile and ScanWeb to be out of sync.
        /// </summary>
        private void CheckPageAccess()
        {
            var loginController = new LoginController();

            loginController.CheckPageAccess(Page);
        }

        /// <summary>
        /// HasSessionExpired
        /// </summary>
        /// <returns>True/False</returns>
        private bool HasSessionExpired()
        {
            bool blnSessionExpired = false;
            var attSessionExpired =
                Attribute.GetCustomAttribute(this.GetType(), typeof(AccessibleWhenSessionExpired)) as
                AccessibleWhenSessionExpired;

            if (attSessionExpired != null && (!attSessionExpired.Accessible && Context.Session != null))
            {
                if ((Session.IsNewSession))
                {
                    string strCookie = Request.Headers["Cookie"];
                    var sessionStateConfig =
                        ConfigurationManager.GetSection("system.web/sessionState") as SessionStateSection;
                    if (((!string.IsNullOrEmpty(strCookie)) && (strCookie.IndexOf(sessionStateConfig.CookieName) >= 0)))
                    {
                        blnSessionExpired = true;
                        Response.Cookies[sessionStateConfig.CookieName].Expires = DateTime.Now.AddDays(-1);
                        var language = Request.Form["ctl00$hidCurrentLanguage"];
                        if (string.IsNullOrEmpty(language))
                        {
                            language = LanguageRedirectionHelper.GetCurrentLanguage();
                        }
                        var siteRepository =
                            DependencyResolver.Instance.GetService(typeof(ISiteInfoRepository)) as ISiteInfoRepository;
                        var errorPath = siteRepository.GetPageUrl(MobilePages.Error, language);
                        string strSessionEndPage = String.Format("{0}?{1}=600&lang={2}", errorPath,
                                                                 Reference.QueryParameterErrorCode, language);
                        Response.Redirect(strSessionEndPage);
                    }
                }
            }
            return blnSessionExpired;
        }

        /// <summary>
        /// AddTestCookie
        /// </summary>
        private void AddTestCookie()
        {
            HttpCookie bookingCodeCookie = new HttpCookie("TestCookie", "1");

            HttpContext.Current.Response.Cookies.Add(bookingCodeCookie);
        }
        /// <summary>
        /// This method will be overriden by login page to return the logon arguments like
        /// user name and password etc to the processing page as property on PreviousPage object.
        /// Declartion of this method not ideal implementation, so should not be used as example 
        /// for any future functionality. Idealy this should be decalred as abstract function
        /// of some class that inherits the visual base page. But as visualbasepage class take dynamic 
        /// type as one of its input it became hard to use the new class object on login processing page.
        /// Hence had to be declared as virtual function in basepage class.
        /// </summary>
        /// <returns></returns>
        public virtual Entity.LogonContext GetLogonContext()
        {
            return null;
        }
    }
}