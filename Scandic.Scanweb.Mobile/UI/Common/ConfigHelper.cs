﻿using System.Text.RegularExpressions;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Common
{
    /// <summary>
    /// ConfigHelper
    /// </summary>
    public class ConfigHelper
    {
        private ISiteInfoRepository siteRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        public ConfigHelper()
        {
            siteRepository = DependencyResolver.Instance.GetService(typeof (ISiteInfoRepository)) as ISiteInfoRepository;
        }

        /// <summary>
        /// GetImageURL
        /// </summary>
        /// <param name="srcImageURL"></param>
        /// <returns>ImageURL</returns>
        public string GetImageURL(string srcImageURL)
        {
            string imageURL = string.Empty;

            var appConfig = siteRepository.GetGeneralConfig<ApplicationConfigSection>();
            var imageValutHost = appConfig.GetMessage(Reference.ImageVaultHost);
            var imageWidthRegx = appConfig.GetMessage(Reference.ImageURLWidthRegx);
            var missingImageWidthRegx = appConfig.GetMessage(Reference.MissingImageURLWidthRegx);
            var imageWidthReplacement = appConfig.GetMessage(Reference.ImageURLWidthReplacmentString);
            srcImageURL = string.Format("{0}/{1}", imageValutHost, srcImageURL);
            imageURL = Regex.Replace(srcImageURL, imageWidthRegx, imageWidthReplacement);

            if (!Regex.Match(srcImageURL, imageWidthRegx).Success)
            {
                var miPattern = Regex.Split(srcImageURL, missingImageWidthRegx)[1];
                var firstPart = srcImageURL.Substring(0, srcImageURL.IndexOf(miPattern) + miPattern.Length);
                var secondPart = srcImageURL.Substring(firstPart.Length, srcImageURL.Length - firstPart.Length);
                imageURL = string.Format("{0}{1}/{2}", firstPart, imageWidthReplacement, secondPart);
            }

            return imageURL;
        }

        /// <summary>
        /// return per night string from config
        /// </summary>
        /// <param name="isPerStay"></param>
        /// <returns>per night string</returns>
        public string GetPerNightString(bool isPerStay)
        {
            string language = LanguageRedirectionHelper.GetCurrentLanguage();
            var appConfig = siteRepository.GetPageConfig<SelectRatePageSection>(language);
            var messages = appConfig.GetPageSection().PageMessages;
            var perNightString = string.Empty;
            if (isPerStay)
            {
                perNightString = messages.GetMessage(Reference.PerStayString);
            }
            else
            {
                perNightString = messages.GetMessage(Reference.PerNightString);
            }

            return string.IsNullOrEmpty(perNightString) ? "per room per night" : perNightString;
        }

        /// <summary>
        /// GetSourceCode
        /// </summary>
        /// <returns>Source code</returns>
        public string GetSourceCode()
        {
            string sourceCode = string.Empty;
            string language = LanguageRedirectionHelper.GetCurrentLanguage();
            var appConfig = siteRepository.GetPageConfig<BookingDetailsPageSection>(language);
            var messages = appConfig.GetPageSection().PageMessages;
            sourceCode = messages.GetMessage(Reference.SourceCode);

            return sourceCode;
        }
    }
}