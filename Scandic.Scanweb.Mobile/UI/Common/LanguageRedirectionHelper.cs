﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.IP2Country;
using Scandic.Scanweb.Mobile.UI.Booking.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Common
{
    /// <summary>
    /// LanguageRedirectionHelper
    /// </summary>
    public class LanguageRedirectionHelper
    {
        private const string RUSSIAN_LANG_REGX = "russianLanguages";
        private const string GERMAN_LANG_REGX = "germanLanguages";

        /// <summary>
        /// GetCurrentLanguage
        /// </summary>
        /// <returns>CurrentLanguage</returns>
        public static string GetCurrentLanguage()
        {
            string currLanguage = string.Empty;
            if (MiscellaneousSessionWrapper.LanguageSessionKey != null)
            {
                currLanguage = MiscellaneousSessionWrapper.LanguageSessionKey;
            }
            else
            {
                currLanguage = GetPageLanguage(MobilePages.Start);
                SetCurrentLanguage(currLanguage);
            }

            return currLanguage;
        }

        /// <summary>
        /// SetCurrentLanguage
        /// </summary>
        /// <param name="language"></param>
        public static void SetCurrentLanguage(string language)
        {
            MiscellaneousSessionWrapper.LanguageSessionKey = language;
            MiscellaneousSessionWrapper.MobileContentLanguage = language;
            HttpContext.Current.Session[Reference.MOBILE_ACTUAL_LANGUAGE] = null;
            Reservation2SessionWrapper.CurrentLanguage = language;
        }

        /// <summary>
        /// LanguageSpecificRedirect
        /// </summary>
        /// <param name="pageId"></param>
        public static void SetActualLanguage(string language)
        {
            HttpContext.Current.Session[Reference.MOBILE_ACTUAL_LANGUAGE] = language;            
        }
        /// <summary>
        /// This will return the actual language code for un published languages
        /// such as Russian and German, for other languages it will return null
        /// string. Once all languages are implemented then this method must be 
        /// removed.
        /// </summary>
        /// <returns></returns>
        public static string GetActualLanguage()
        {
            return HttpContext.Current.Session[Reference.MOBILE_ACTUAL_LANGUAGE] as string;
        }
        public static void LanguageSpecificRedirect(MobilePages pageId)
        {
            if (!IsNonSupportedLanguage(pageId))
            {
                var nonSupportedLangaueRedirect =
                    HttpContext.Current.Request.QueryString[Reference.NonSupportedLanguageSwitch];

                if (string.IsNullOrEmpty(nonSupportedLangaueRedirect) ||
                    !string.Equals(nonSupportedLangaueRedirect, "1"))
                {
                    if ((Request.UrlReferrer == null || !string.Equals(Request.Url.Host, Request.UrlReferrer.Host)))
                    {
                        var siteRepository =
                            DependencyResolver.Instance.GetService(typeof (ISiteInfoRepository)) as ISiteInfoRepository;
                        var pageData = siteRepository.GetCMSPageData(pageId);
                            if (pageData != null)
                            {
                                SetCurrentLanguage(pageData.LanguageBranch);
                        }
                    }
                }


                var currLang = Request.QueryString[Reference.LanguageSwitch];
                if (!String.IsNullOrEmpty(currLang))
                {
                    SetCurrentLanguage(currLang);
                }
                var aLang = Request.QueryString[Reference.ActualLanguage];
                if (!String.IsNullOrEmpty(aLang))
                {
                    //This flag should be set for un-publised languages for mobile
                    SetActualLanguage(aLang);
            }
        }
        }

        /// <summary>
        /// IsNonSupportedLanguage
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns>True/False</returns>
        private static bool IsNonSupportedLanguage(MobilePages pageId)
        {
            var siteRepository =
                DependencyResolver.Instance.GetService(typeof (ISiteInfoRepository)) as ISiteInfoRepository;
            var appConfig = siteRepository.GetGeneralConfig<ApplicationConfigSection>();
            var nonSupportedPattren = appConfig.GetMessage(Reference.NonSupportedLanguagesPattren);
            var regx = new Regex(nonSupportedPattren);
            var pathToCheck = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Request.RawUrl);
            var isNonSupported = regx.IsMatch(pathToCheck);
            if (isNonSupported)
            {
                regx = new Regex(appConfig.GetMessage(RUSSIAN_LANG_REGX));
                string actualLanguage = string.Empty;
                if (regx.IsMatch(pathToCheck))
                {
                    actualLanguage = CurrentPageLanguageConstant.LANGAUGE_RUSSIAN;
                }
                else
                {
                    regx = new Regex(appConfig.GetMessage(GERMAN_LANG_REGX));
                    if (regx.IsMatch(pathToCheck))
                    {
                        actualLanguage = CurrentPageLanguageConstant.LANGUAGE_GERMANY.ToLower();
                    }
                }
                string language = CurrentPageLanguageConstant.LANGUAGE_ENGLISH.ToLower();
                string pageUrl = string.Format("{0}?{1}=1&{2}={3}", siteRepository.GetPageUrl(pageId, language.ToLower()), Reference.NonSupportedLanguageSwitch,
                                                Reference.ActualLanguage, actualLanguage);

                SetCurrentLanguage(language);
                HttpContext.Current.Response.Redirect(pageUrl, false);
                HttpContext.Current.Response.StatusCode = (int) System.Net.HttpStatusCode.MovedPermanently;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            return isNonSupported;
        }

        /// <summary>
        /// GetPageLanguage
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns>PageLanguage</returns>
        private static string GetPageLanguage(MobilePages pageId)
        {
            string lang = "en";
            var siteRepository =
                DependencyResolver.Instance.GetService(typeof (ISiteInfoRepository)) as ISiteInfoRepository;
            var pageData = siteRepository.GetCMSPageData(pageId);

            if (pageData != null)
            {
                lang = pageData.LanguageBranch;
            }
            return lang;
        }

        /// <summary>
        /// GetCountryLanguage
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns>CountryLanguage</returns>
        private static string GetCountryLanguage(string countryCode)
        {
            string language = string.Empty;
            if (!string.IsNullOrEmpty(countryCode))
            {
                countryCode = countryCode.ToUpper();
                switch (countryCode)
                {
                    case CountryCodeConstant.COUNTRY_SWEDEN:
                        language = CurrentPageLanguageConstant.LANGUAGE_SWEDISH;
                        break;
                    case CountryCodeConstant.COUNTRY_DANMARK:
                        language = CurrentPageLanguageConstant.LANGUAGE_DANISH;
                        break;
                    case CountryCodeConstant.COUNTRY_NORWAY:
                        language = CurrentPageLanguageConstant.LANGUAGE_NORWEGIAN;
                        break;
                    case CountryCodeConstant.COUNTRY_FINLAND:
                        language = CurrentPageLanguageConstant.LANGUAGE_FINNISH;
                        break;
                    case CountryCodeConstant.COUNTRY_RUSSIA:
                        language = CurrentPageLanguageConstant.LANGAUGE_RUSSIAN;
                        break;
                    case CountryCodeConstant.COUNTRY_GERMANY:
                        language = CurrentPageLanguageConstant.LANGUAGE_GERMANY;
                        break;
                }
            }

            return language;
        }

        private static HttpRequest Request
        {
            get { return HttpContext.Current.Request; }
        }

        /// <summary>
        /// SetCurrentCultureInfo
        /// </summary>
        public static void SetCurrentCultureInfo()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(GetCurrentLanguage());
        }
    }
}