﻿using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.Mobile.UI.Controls
{
    /// <summary>
    /// DateTextbox
    /// </summary>
    [ToolboxData("<{0}:DateTextBox runat=server></{0}:DateTextBox>")]
    public class DateTextbox : TextBox
    {
    }
}