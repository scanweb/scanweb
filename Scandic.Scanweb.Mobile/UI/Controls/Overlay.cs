﻿//  Description					:   Overlay                                               //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.Mobile.UI.Controls.Interface;
using Scandic.Scanweb.Mobile.UI.Entity;

namespace Scandic.Scanweb.Mobile.UI.Controls
{
    /// <summary>
    /// Contains members to implement overlay.
    /// </summary>
    public class Overlay
    {
        private IOverlay strategy;
        private OverlayDetails data;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="type"></param>
        /// <param name="overlayData"></param>
        /// <param name="controlId"></param>
        public Overlay(OverlayControlType type, OverlayDetails overlayData, string controlId)
        {
            data = overlayData;
            ID = controlId;
            switch (type)
            {
                case OverlayControlType.KeyValue:
                    strategy = new OverlayKeyValueControl();
                    break;
                default:
                    strategy = new OverlayValueControl();
                    break;
            }
        }

        /// <summary>
        /// Gets control.
        /// </summary>
        /// <param name="anyOtherControl"></param>
        /// <returns></returns>
        public HtmlGenericControl GetControl(Control anyOtherControl)
        {
            return GetControl(anyOtherControl, true);  
        }
        public HtmlGenericControl GetControl(Control anyOtherControl, bool showCloseButton)
        {
            return CreateControl(anyOtherControl, showCloseButton);
        }

        /// <summary>
        /// Gets control html.
        /// </summary>
        /// <returns></returns>
        public string GetControlHtml()
        {
            return GetControlHtml(null);
        }

        /// <summary>
        /// Gets control html.
        /// </summary>
        /// <param name="anyOtherControl"></param>
        /// <returns></returns>
        public string GetControlHtml(Control anyOtherControl)
        {
            return GetControlHtml(anyOtherControl, true);
        }

        public string GetControlHtml(Control anyOtherControl, bool showCloseButton)
        {
            //wrap generated control in someother html control and retrive its innerhtml value
            var overlayControl = GetControl(anyOtherControl, showCloseButton);
            var stringWriter = new StringWriter();
            var htmlStream = new System.Web.UI.HtmlTextWriter(stringWriter);
            overlayControl.RenderControl(htmlStream);
            return stringWriter.ToString();
        }

        private HtmlGenericControl CreateControl(Control anyOtherControl)
        {
            return CreateControl(anyOtherControl,true);
        }
        private HtmlGenericControl CreateControl(Control anyOtherControl, bool showCloseButton)
        {
            var controlDiv = new HtmlGenericControl("div");
            controlDiv.Attributes.Add("class", "overlay");
            controlDiv.Attributes.Add("style", "display: none;");
            controlDiv.Attributes.Add("data-iscachable", data.IsCachable.ToString());
            controlDiv.ID = ID;
            controlDiv.Controls.Add(GetHeading(showCloseButton));
            strategy.CreateOverlayItems(data, controlDiv);
            if (anyOtherControl != null)
            {
                controlDiv.Controls.Add(anyOtherControl);
            }

            return controlDiv;
        }

        /// <summary>
        /// Gets/Sets ID
        /// </summary>
        public string ID { get; set; }

        private HtmlGenericControl GetHeading()
        {
            return GetHeading(true);
        }

        private HtmlGenericControl GetHeading(bool showCloseButton)
        {
            var headingDiv = new HtmlGenericControl("div");
            var closeLink = new HtmlGenericControl("a");
            var headingElement = new HtmlGenericControl("h3");

            closeLink.Attributes.Add("class", "close");
            closeLink.Attributes.Add("href", "#");
            closeLink.Attributes.Add("onclick", "return closeOverlay();");
            closeLink.InnerHtml = "x";
            headingElement.InnerHtml = data.Heading;

            headingDiv.Attributes.Add("class", "top");
            if (showCloseButton)
            {
                headingDiv.Controls.Add(closeLink);
            }
            headingDiv.Controls.Add(headingElement);

            return headingDiv;
        }
    }

    /// <summary>
    /// Holds different types of overlay controls.
    /// </summary>
    public enum OverlayControlType
    {
        Value = 0,
        KeyValue = 1
    }
}