﻿////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  OverlayValueControl                                    //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Web.UI.HtmlControls;
using Scandic.Scanweb.Mobile.UI.Controls.Interface;
using Scandic.Scanweb.Mobile.UI.Entity;

namespace Scandic.Scanweb.Mobile.UI.Controls
{
    public class OverlayValueControl : IOverlay
    {
        #region IOverlay Members

        /// <summary>
        /// Creates overlay items.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="parentControl"></param>
        public void CreateOverlayItems(OverlayDetails data, HtmlGenericControl parentControl)
        {
            foreach (var item in data.Items)
            {
                var overlayItem = new HtmlGenericControl("div");
                overlayItem.InnerHtml = item.Value;
                parentControl.Controls.Add(overlayItem);
            }
        }

        #endregion
    }
}