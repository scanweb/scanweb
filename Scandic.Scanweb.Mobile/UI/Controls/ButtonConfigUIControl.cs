﻿using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Controls.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Controls
{
    /// <summary>
    /// ButtonConfigUIControl
    /// </summary>
    public class ButtonConfigUIControl : IConfigUIControl
    {
        #region IConfigUIControl Members

        /// <summary>
        /// SetConfigInfo
        /// </summary>
        /// <param name="configInput"></param>
        /// <param name="control"></param>
        /// <param name="parentPage"></param>
        public void SetConfigInfo(Input configInput, Control control, Page parentPage)
        {
            if (configInput != null)
            {
                var button = control as IButtonControl;
                button.Text = configInput.Label;
            }
        }

        #endregion
    }
}