﻿using System.Web.UI;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.Mobile.UI.Controls.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Controls
{
    /// <summary>
    /// This class implements the method of IConfigUIControl to set the config information
    /// on the HTMLGerenic control on the UI page.
    /// </summary>    
    public class HTMLAnchorConfigUIControl : IConfigUIControl
    {
        #region IConfigUIControl Members

        /// <summary>
        /// Set Config Info
        /// </summary>
        /// <param name="configInput"></param>
        /// <param name="control"></param>
        /// <param name="parentPage"></param>
        public void SetConfigInfo(Input configInput, Control control, Page parentPage)
        {
            if (configInput != null)
            {
                var htmlAnchorControl = control as HtmlAnchor;

                htmlAnchorControl.InnerHtml = configInput.Label;
            }
        }

        #endregion
    }
}