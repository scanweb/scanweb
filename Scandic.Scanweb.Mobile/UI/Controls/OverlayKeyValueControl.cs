﻿using System.Web.UI.HtmlControls;
using Scandic.Scanweb.Mobile.UI.Controls.Interface;
using Scandic.Scanweb.Mobile.UI.Entity;

namespace Scandic.Scanweb.Mobile.UI.Controls
{
    /// <summary>
    /// OverlayKeyValueControl
    /// </summary>
    public class OverlayKeyValueControl : IOverlay
    {
        #region IOverlay Members

        /// <summary>
        /// CreateOverlayItems
        /// </summary>
        /// <param name="data"></param>
        /// <param name="parentControl"></param>
        public void CreateOverlayItems(OverlayDetails data, HtmlGenericControl parentControl)
        {
            var unorderedList = new HtmlGenericControl("ul");
            foreach (var item in data.Items)
            {
                var dataItem = item as OverlayKeyItem;
                var listItem = new HtmlGenericControl("li");
                var overlayKeyItem = new HtmlGenericControl("span");
                var overlayValueItem = new HtmlGenericControl("span");

                overlayKeyItem.InnerHtml = dataItem.Key;
                overlayValueItem.InnerHtml = dataItem.Value;

                listItem.Controls.Add(overlayKeyItem);
                listItem.Controls.Add(overlayValueItem);
                listItem.Attributes.Add("class", "cf");

                unorderedList.Controls.Add(listItem);
            }

            unorderedList.Attributes.Add("class", "info-list");
            parentControl.Controls.Add(unorderedList);
        }

        #endregion
    }
}