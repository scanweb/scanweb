﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.Mobile.UI.Controls
{
    /// <summary>
    /// Email Text box
    /// </summary>
    [ToolboxData("<{0}:EmailTextBox runat=server></{0}:EmailTextBox>")]
    public class EmailTextBox : TextBox
    {
    

      protected override void AddAttributesToRender(System.Web.UI.HtmlTextWriter writer)
        {
            writer.AddAttribute("type", "email");
            base.AddAttributesToRender(writer);
        }
    }
}
