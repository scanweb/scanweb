﻿//  Description					: RadioListConfigUIControl                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Controls.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Controls
{
    /// <summary>
    /// Contains members to set the initial configuration for the controls.
    /// </summary>
    public class RadioListConfigUIControl : IConfigUIControl
    {
        #region IConfigUIControl Members
        /// <summary>
        /// Sets config info.
        /// </summary>
        /// <param name="configInput"></param>
        /// <param name="control"></param>
        /// <param name="parentPage"></param>
        public void SetConfigInfo(Input configInput, Control control, Page parentPage)
        {
            if (configInput != null)
            {
                var radioListControl = control as RadioButtonList;

                radioListControl.DataSource = configInput.DropDownOptions.Options;
                radioListControl.DataTextField = "Value";
                radioListControl.DataValueField = "Key";
                radioListControl.SelectedValue = configInput.DefaultValue;
                radioListControl.DataBind();
                Utilities.AddAttributes(radioListControl, configInput.Attributes);
            }
        }

        #endregion
    }
}