﻿using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Controls.Interface;

namespace Scandic.Scanweb.Mobile.UI.Controls
{
    /// <summary>
    /// DropDownConfigUIControl
    /// </summary>
    public class DropDownConfigUIControl : IConfigUIControl
    {
        #region IConfigUIControl Members

        /// <summary>
        /// SetConfigInfo
        /// </summary>
        /// <param name="configInput"></param>
        /// <param name="control"></param>
        /// <param name="parentPage"></param>
        public void SetConfigInfo
            (Scandic.Scanweb.Mobile.UI.Entity.Configuration.Input configInput, Control control, Page parentPage)
        {
            if (configInput != null)
            {
                var dropDownControl = control as DropDownList;

                dropDownControl.DataSource = configInput.DropDownOptions.Options;
                dropDownControl.DataTextField = "Value";
                dropDownControl.DataValueField = "Key";
                dropDownControl.SelectedValue = configInput.DefaultValue;
                dropDownControl.DataBind();
            }
        }

        #endregion
    }
}