﻿//  Description					:   LabelConfigUIControl                                  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Controls.Interface;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Controls
{
    /// <summary>
    /// This class implements the method of IConfigUIControl to set the config information
    /// on the label and corresponding input control on the UI page.
    /// </summary>
    public class LabelConfigUIControl : IConfigUIControl
    {
        #region IConfigUIControl Members

        /// <summary>
        /// Sets config info
        /// </summary>
        /// <param name="configInput"></param>
        /// <param name="control"></param>
        /// <param name="parentPage"></param>
        public void SetConfigInfo(Input configInput, Control control, Page parentPage)
        {
            if (configInput != null)
            {
                var labelControl = control as Label;
                var associatedControl = parentPage.Form.Controls.All().Where(
                    ctrl =>
                    string.Equals(ctrl.ID, labelControl.AssociatedControlID, StringComparison.InvariantCultureIgnoreCase)
                                            ).FirstOrDefault() as WebControl;

                labelControl.Text = configInput.Label;
                if (associatedControl != null)
                {
                    Utilities.AddAttributes(associatedControl, configInput.Attributes);
                }
            }
        }

        #endregion
    }
}