﻿using System.Web.UI;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Controls.Interface
{
    /// <summary>
    /// This interface defines the methods that will be used to set the config
    /// information on the UI controls
    /// </summary>
    internal interface IConfigUIControl
    {
        /// <summary>
        /// This method sets the passed config information to specified
        /// web controls. 
        /// </summary>
        /// <param name="configInput"></param>
        /// <param name="control"></param>
        void SetConfigInfo(Input configInput, Control control, Page parentPage);
    }
}