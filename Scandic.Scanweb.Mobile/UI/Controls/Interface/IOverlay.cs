﻿//  Description					:   IOverlay                                              //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Web.UI.HtmlControls;
using Scandic.Scanweb.Mobile.UI.Entity;

namespace Scandic.Scanweb.Mobile.UI.Controls.Interface
{
    /// <summary>
    /// Interface to hold members of overlay.
    /// </summary>
    internal interface IOverlay
    {
        /// <summary>
        /// Creates overlay items.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="parentControl"></param>
        void CreateOverlayItems(OverlayDetails data, HtmlGenericControl parentControl);
    }
}