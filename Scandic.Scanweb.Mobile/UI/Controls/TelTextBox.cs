﻿//  Description					: MainMenu                                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.Mobile.UI.Controls
{
    /// <summary>
    /// Custom textbox.
    /// </summary>
    [ToolboxData("<{0}:TelTextBox runat=server></{0}:TelTextBox>")]
    public class TelTextBox : TextBox
    {
        /// <summary>
        /// Adds attributes that are required to render.
        /// </summary>
        /// <param name="writer"></param>
        protected override void AddAttributesToRender(System.Web.UI.HtmlTextWriter writer)
        {
            writer.AddAttribute("type", "tel");
            base.AddAttributesToRender(writer);
        }
    }
}