﻿using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.Mobile.UI.Controls
{
    /// <summary>
    /// Number Text box
    /// </summary>
    [ToolboxData("<{0}:NumberTextBox runat=server></{0}:NumberTextBox>")]
    public class NumberTextbox : TextBox
    {
        /// <summary>
        /// AddAttributes To Render
        /// </summary>
        /// <param name="writer"></param>
        protected override void AddAttributesToRender(System.Web.UI.HtmlTextWriter writer)
        {
            writer.AddAttribute("type", "number");
            base.AddAttributesToRender(writer);
        }
    }
}