﻿//  Description					:   ConfigurationHelper                                   //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Scandic.Scanweb.Mobile.UI.Common;

namespace Scandic.Scanweb.Mobile.UI.Configuration
{
    /// <summary>
    /// Contains members support configuration tasks.
    /// </summary>
    public static class ConfigurationHelper
    {
        /// <summary>
        /// This method is used to deserialise a string containing an xml into it's corresponding type.
        /// </summary>
        /// <param name="configXml"><see cref="System.String">String</see> representing an xml that 
        /// needs to be desearilised</param>
        /// <returns></returns>
        public static object CreateConfigSectionFromString(this string configXml)
        {
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.LoadXml(configXml);

                XmlConfigurator configSectionHandler = new XmlConfigurator();
                object parent = null;
                XmlNode section = xmlDoc.DocumentElement;

                return configSectionHandler.Create(parent, section);
            }
            finally
            {
                xmlDoc = null;
            }
        }

        /// <summary>
        /// Gets the configuration section object of specified language for the generic type T.
        /// </summary>
        /// <typeparam name="T">Type corresponding to configuration which would be 
        /// returned by this function</typeparam>
        /// <returns></returns>
        public static T GetConfigurationSection<T>(string language)
        {
            return GetConfiguration<T>(GetConfigPath<T>(language, TypeOfConfig.Language));
        }

        /// <summary>
        /// Gets the general configuration that is not tied up with any language 
        /// object for the generic type T.
        /// </summary>
        /// <typeparam name="T">Type corresponding to configuration which would be 
        /// returned by this function.</typeparam>
        /// <returns></returns>
        public static T GetConfigurationSection<T>()
        {
            return GetConfiguration<T>(GetConfigPath<T>(Reference.GeneralConfigFolder, TypeOfConfig.Generic));
        }

        private static string GetConfigPath<T>(string reletiveFolder, TypeOfConfig configType)
        {
            Type genericType = typeof (T);
            string xmlRootElementName = null;

            var xRootAttribute = genericType.GetCustomAttributes(typeof (XmlRootAttribute), false);

            if ((xRootAttribute.Count() > 0))
            {
                xmlRootElementName = ((XmlRootAttribute) xRootAttribute[0]).ElementName;
            }
            else
            {
                xmlRootElementName = genericType.Name;
            }

            string contentConfigurationFolder = string.Format("{0}\\{1}",
                                                              HttpContext.Current.Request.PhysicalApplicationPath,
                                                              ConfigurationManager.AppSettings[
                                                                  "MobileConfigurationFolder"]);
            string xmlConfigFileName = string.Format("{0}\\{1}\\{2}.xml", contentConfigurationFolder, reletiveFolder,
                                                     xmlRootElementName);

            if (configType == TypeOfConfig.Language && !File.Exists(xmlConfigFileName))
            {
                xmlConfigFileName = string.Format("{0}\\{1}\\{2}.xml", contentConfigurationFolder, "en",
                                                  xmlRootElementName);
            }
            return xmlConfigFileName;
        }


        private static T GetConfiguration<T>(string configPath)
        {
            XDocument configFile = default(XDocument);
            T configurationSection = default(T);

            if (File.Exists(configPath))
            {
                configFile = XDocument.Load(configPath);
                configurationSection = (T) configFile.Root.ToString().CreateConfigSectionFromString();
            }
            else
            {
                throw new ApplicationException(string.Format("Configuration file {0} not found.", configPath));
            }

            return configurationSection;
        }

        /// <summary>
        /// Holds different types of config
        /// </summary>
        public enum TypeOfConfig
        {
            Generic = 0,
            Language = 1
        }
    }
}