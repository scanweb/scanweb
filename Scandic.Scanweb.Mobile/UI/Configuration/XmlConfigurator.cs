﻿//  Description					:   XmlConfigurator                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace Scandic.Scanweb.Mobile.UI.Configuration
{
    /// <summary>
    /// This provides methods to deserialize xml configuration file to the specified 
    /// </summary>
    public class XmlConfigurator
    {
        /// <summary>
        /// This function will deserialize config section into corresponding type's object.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, System.Xml.XmlNode section)
        {
            Object settings = null;
            if (section == null)
            {
                return settings;
            }
            XPathNavigator navigator = section.CreateNavigator();
            String typeName = (string) navigator.Evaluate("string(@type)");
            Type sectionType = Type.GetType(typeName);

            XmlSerializer xs = new XmlSerializer(sectionType);
            XmlNodeReader reader = new XmlNodeReader(section);

            try
            {
                settings = xs.Deserialize(reader);
            }
            catch (Exception ex)
            {
                StringBuilder strErrorMessage = new StringBuilder();
                strErrorMessage.Append("Unable to get configuration section: ");
                strErrorMessage.Append(ex.Message);
                throw new ApplicationException(strErrorMessage.ToString(), ex);
            }
            finally
            {
                xs = null;
            }
            return settings;
        }
    }
}