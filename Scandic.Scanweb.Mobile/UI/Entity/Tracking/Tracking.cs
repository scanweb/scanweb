﻿using System;
using Scandic.Scanweb.Mobile.UI.Booking.Controller;
using Scandic.Scanweb.Mobile.UI.Common;
using Scandic.Scanweb.Mobile.UI.Entity.Configuration;

namespace Scandic.Scanweb.Mobile.UI.Entity.Tracking
{
    /// <summary>
    /// TrackingDetails
    /// </summary>
    [Serializable()]
    public class TrackingDetails
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TrackingDetails()
            : this(TrackingFunctions.None)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="functionId"></param>
        public TrackingDetails(TrackingFunctions functionId)
        {
            var controller = new BaseController("");
            var pageConfig = controller.GetPageConfig<MasterPageSection>();
            reportSuiteId = pageConfig.PageMessages.GetMessage(Reference.ReportSuiteId);
            functionName = GetFunctionName(functionId);
        }

        public string pageName { get; set; }
        public string pageType { get; set; }
        public string prop34 { get; set; }
        public string eVar34 { get; set; }
        public string events { get; set; }
        public string event17 { get; set; }
        public string prop11 { get; set; }
        public string prop12 { get; set; }
        public string prop13 { get; set; }
        public string prop14 { get; set; }
        public string prop15 { get; set; }
        public string prop16 { get; set; }
        public string prop17 { get; set; }
        public string prop18 { get; set; }
        public string prop25 { get; set; }
        public string prop26 { get; set; }
        public string eVar2 { get; set; }
        public string eVar4 { get; set; }
        public string eVar5 { get; set; }
        public string eVar6 { get; set; }
        public string eVar8 { get; set; }
        public string eVar11 { get; set; }
        public string eVar12 { get; set; }
        public string eVar13 { get; set; }
        public string eVar14 { get; set; }
        public string eVar15 { get; set; }
        public string eVar16 { get; set; }
        public string eVar17 { get; set; }
        public string eVar18 { get; set; }
        public string eVar21 { get; set; }
        public string eVar25 { get; set; }
        public string eVar26 { get; set; }
        public string eVar27 { get; set; }
        public string eVar30 { get; set; }
        public string eVar39 { get; set; }
        public string eVar40 { get; set; }
        public string eVar41 { get; set; }
        public string eVar42 { get; set; }
        public string eVar43 { get; set; }
        public string eVar48 { get; set; }
        public string eVar49 { get; set; }
        public string eVar29 { get; set; }
        public string eVar31 { get; set; }
        public string eVar62 { get; set; }
        public string purchaseID { get; set; }
        public string products { get; set; }
        public string reportSuiteId { get; set; }
        public string linkTrackVars { get; set; }
        public string linkTrackEvents { get; set; }
        public string currencyCode { get; set; }
        public string functionName { get; set; }
        public string prop54 { get; set; }
        public string prop44 { get; set; }
        public string prop49 { get; set; }
        public string prop50 { get; set; }
        public string prop48 { get; set; }
        public string prop20 { get; set; }
        public string event31 { get; set; }
        /// <summary>
        /// GetFunctionName
        /// </summary>
        /// <param name="functionId"></param>
        /// <returns></returns>
        private string GetFunctionName(TrackingFunctions functionId)
        {
            string functionName = string.Empty;
            switch (functionId)
            {
                case TrackingFunctions.ClickToCallChangeOrCancel:
                    functionName = "click to call";
                    break;
                case TrackingFunctions.ClickToCallUs:
                    functionName = "click to call";
                    break;
                case TrackingFunctions.ClickToCallBookWithChildren:
                    functionName = "click to call";
                    break;
                case TrackingFunctions.ClickToMap:
                    functionName = "link to map";
                    break;
                case TrackingFunctions.ClickToEmail:
                    functionName = "email";
                    break;
                case TrackingFunctions.SuccessfulFeedback:
                    functionName = "feedback form";
                    break;
                case TrackingFunctions.SuccessfulLogin:
                    functionName = "member login";
                    break;
                case TrackingFunctions.ClickToTripAdvisorReviews:
                    functionName = "trip advisor review link";
                    break;
                case TrackingFunctions.EditSearch:
                    functionName = "edit search link";
                    break;
                case TrackingFunctions.SelectRateSubmit:
                    functionName = "rate selected on select rate page";
                    break;
                case TrackingFunctions.CheckoutSelection :
                    functionName = "Checkout";
                    break;
            }
            return functionName;
        }
    }

    public enum TrackingFunctions
    {
        None = 0,
        ClickToCallChangeOrCancel = 1,
        ClickToCallUs = 2,
        ClickToCallBookWithChildren = 3,
        ClickToMap = 4,
        ClickToEmail = 5,
        SuccessfulFeedback = 6,
        SuccessfulLogin = 7,
        ClickToCallReservationTeam = 8,
        ClickToCallMemberService = 9,
        ClickToTripAdvisorReviews = 10,
        EditSearch = 11,
        SelectRateSubmit = 12,
        CheckoutSelection = 13
    }
}