﻿using System.Collections.Generic;
using Scandic.Scanweb.Core;
namespace Scandic.Scanweb.Mobile.UI.Entity.Model
{
    /// <summary>
    /// Room Rate Results
    /// </summary>
    public class RoomRateResults
    {
        public List<RateCategoryPopup> RateCategoryPopups { get; set; }
        public List<RoomTypeDetails> RoomRateDetails { get; set; }
        public List<MessageDetails> ErrorMessages { get; set; }
        public bool AlternateHotelSearchRequired { get; set; }
        public string AboutOurRates { get; set; }
        public List<RateCategory> RateCategories { get; set; }
    }

    /// <summary>
    /// Rate Category Pop Up
    /// </summary>
    public class RateCategoryPopup
    {
        public string RateCategoryId { get; set; }
        public string RateCategoryName { get; set; }
        public string RateCategoryDesc { get; set; }
    }

    /// <summary>
    /// Rate Overlay Tool Tip
    /// </summary>
    public class RateOverlayToolTip
    {
        public string RateCategoryName { get; set; }
        public string RateCategoryDesc { get; set; }
    }
}