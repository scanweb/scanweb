﻿namespace Scandic.Scanweb.Mobile.UI.Entity.Model
{
    /// <summary>
    /// StaticPageModel
    /// </summary>
    public class StaticPageModel : BasePageModel
    {
        public StandardPageData StaticPageData { get; set; }
        public Menu ContentMenu { get; set; }
        public string MobileAppRating { get; set; }
        public string AboutMobileApp { get; set; }
    }
}