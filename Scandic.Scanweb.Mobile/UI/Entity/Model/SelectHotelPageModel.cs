﻿//  Description					: SelectHotelPageModel                                    //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

namespace Scandic.Scanweb.Mobile.UI.Entity.Model
{
    /// <summary>
    /// This class represents SelectHotelPageModel
    /// </summary>
    public class SelectHotelPageModel : BasePageModel
    {
        /// <summary>
        /// Gets/Sets SortOptions
        /// </summary>
        public List<SelectOption> SortOptions { get; set; }
        /// <summary>
        /// Gets/Sets ContextInfo
        /// </summary>
        public ContextInformation ContextInfo { get; set; }
    }

    /// <summary>
    /// This class represents SelectOption
    /// </summary>
    public class SelectOption
    {
        /// <summary>
        /// Gets/Sets Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Gets/Sets Value
        /// </summary>
        public string Value { get; set; }
    }

    /// <summary>
    /// This class represents ContextInformation
    /// </summary>
    public class ContextInformation
    {
        /// <summary>
        /// Gets/Sets Header 
        /// </summary>
        public string Header { get; set; }
        /// <summary>
        /// Gets/Sets InfoList 
        /// </summary>
        public List<string> InfoList { get; set; }
    }
}