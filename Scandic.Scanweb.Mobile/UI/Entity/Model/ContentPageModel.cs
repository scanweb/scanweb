﻿namespace Scandic.Scanweb.Mobile.UI.Entity.Model
{
    /// <summary>
    /// ContentPageModel
    /// </summary>
    public class ContentPageModel : BasePageModel
    {
        public ContentPageData ContentPageInfo { get; set; }
    }
}