﻿//  Description					:   StartPageModel                                        //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Mobile.UI.Entity.Model
{
    /// <summary>
    /// This class describe UI elements associated with start page
    /// </summary>
    public class StartPageModel : BasePageModel
    {
        /// <summary>
        /// Gets/Sets ContextMenu 
        /// </summary>
        public Menu ContextMenu { get; set; }
    }
}