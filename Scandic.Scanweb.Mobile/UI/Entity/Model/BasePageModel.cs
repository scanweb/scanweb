﻿namespace Scandic.Scanweb.Mobile.UI.Entity.Model
{
    /// <summary>
    /// BasePageModel
    /// </summary>
    public class BasePageModel
    {
        public string PageHeading { get; set; }
        public string PageTitle { get; set; }
    }
}