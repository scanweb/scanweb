﻿using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    /// <summary>
    /// SelectHotelPageSection
    /// </summary>
    [Serializable(), XmlRoot("SelectHotelPageSection")]
    public class SelectHotelPageSection : IPageSection
    {
        [XmlElement("PageSection")]
        public PageSection PageDetail { get; set; }

        #region IPageSection Members

        /// <summary>
        /// GetPageSection
        /// </summary>
        /// <returns>PageSection</returns>
        public PageSection GetPageSection()
        {
            return PageDetail;
        }

        #endregion
    }
}