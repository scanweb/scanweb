﻿//  Description					:   Input                                                 //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    /// <summary>
    /// This class will hold configuration data for any correspinding
    /// UI input field.
    /// </summary>
    [Serializable()]
    public class Input
    {
        /// <summary>
        /// This property corresponds to the id of the ASP.net label control on the page.
        /// </summary>
        [XmlAttribute("Id")]
        public string Id { get; set; }

        /// <summary>
        /// This property corresponds to the text to be set on the label control.
        /// </summary>
        [XmlElement("Label")]
        public string Label { get; set; }


        /// <summary>
        /// This property identify to which type of UI control config will be applied.
        /// </summary>
        [XmlAttribute("type")]
        public ConfigInputType Type { get; set; }

        /// <summary>
        /// This property hold the path or URL config informtation for the Hyperlink type UI control.
        /// </summary>
        [XmlAttribute("path")]
        public string path { get; set; }

        [XmlElement("Options")]
        public KeyValueOptions DropDownOptions { get; set; }

        [XmlAttribute("defaultValue")]
        public string DefaultValue { get; set; }

        [XmlElement("Tooltip")]
        public Tooltip ToolTip { get; set; }

        [XmlElement("Attribute")]
        public List<Attribute> Attributes { get; set; }
    }

    /// <summary>
    /// This holds different ConfigInputTypes
    /// </summary>
    public enum ConfigInputType
    {
        [XmlEnum("Label")] Label = 0,
        [XmlEnum("Link")] Link = 1,
        [XmlEnum("DropDown")] DropDown = 2,
        [XmlEnum("Button")] Button = 3,
        [XmlEnum("HTMLGenericControl")] HTMLGenericControl = 4,
        [XmlEnum("RadioOptions")] RadioOptions = 5,
        [XmlEnum("HTMLAnchorControl")] HTMLAnchorControl = 6
    }
}