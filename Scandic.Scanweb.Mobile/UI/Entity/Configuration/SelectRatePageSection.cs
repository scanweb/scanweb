﻿//  Description					:   SelectRatePageSection                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    /// <summary>
    /// SelectRatePageSection entity
    /// </summary>
    [Serializable(), XmlRoot("SelectRatePageSection")]
    public class SelectRatePageSection : IPageSection
    {
        /// <summary>
        /// Gets/Sets PageDetail
        /// </summary>
        [XmlElement("PageSection")]
        public PageSection PageDetail { get; set; }

        #region IPageSection Members
        /// <summary>
        /// Gets page section.
        /// </summary>
        /// <returns></returns>
        public PageSection GetPageSection()
        {
            return PageDetail;
        }
        #endregion
    }
}