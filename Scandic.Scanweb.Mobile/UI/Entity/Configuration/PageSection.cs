﻿////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  Page                                                   //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    /// <summary>
    /// This class represents PageSection entity.
    /// </summary>
    [Serializable(), XmlRoot("PageSection")]
    public class PageSection
    {
        /// <summary>
        /// Gets/Sets PageTitle
        /// </summary>
        [XmlElement("Title")]
        public string PageTitle { get; set; }

        /// <summary>
        /// Gets/Sets PageHeading
        /// </summary>
        [XmlElement("Heading")]
        public string PageHeading { get; set; }

        /// <summary>
        /// Gets/Sets PageMessages
        /// </summary>
        [XmlElement("MessageSection")]
        public MessageSection PageMessages { get; set; }

        /// <summary>
        /// Gets/Sets Inputs
        /// </summary>
        [XmlElement("Input")]
        public List<Input> Inputs { get; set; }
    }
}