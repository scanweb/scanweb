﻿namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    /// <summary>
    /// IPageSection
    /// </summary>
    public interface IPageSection
    {
        /// <summary>
        /// This method will return the PageSection type property of the 
        /// implemented Configuration so that it can be used by the
        /// VisualBasePage class.
        /// </summary>
        /// <returns>Page Section</returns>
        PageSection GetPageSection();
    }
}