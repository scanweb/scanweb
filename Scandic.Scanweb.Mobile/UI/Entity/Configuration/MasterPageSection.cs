﻿using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    /// <summary>
    /// MasterPageSection
    /// </summary>
    [Serializable(), XmlRoot("MasterPageSection")]
    public class MasterPageSection
    {
        [XmlElement("HeaderMenu")]
        public Menu HeaderMenu { get; set; }

        [XmlElement("FooterMenu")]
        public Menu FooterMenu { get; set; }

        [XmlElement("LanguageOverlay")]
        public LanguageOverlayDetails LanguageOverlay { get; set; }

        [XmlElement("MessageSection")]
        public MessageSection PageMessages { get; set; }
    }
}