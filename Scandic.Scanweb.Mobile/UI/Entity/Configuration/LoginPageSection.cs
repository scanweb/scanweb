﻿using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    /// <summary>
    /// Login Page Section
    /// </summary>
    [Serializable(), XmlRoot("LoginPageSection")]
    public class LoginPageSection : IPageSection
    {
        [XmlElement("PageSection")]
        public PageSection PageDetail { get; set; }

        #region IPageSection Members

        /// <summary>
        /// Get Page Section
        /// </summary>
        /// <returns></returns>
        public PageSection GetPageSection()
        {
            return PageDetail;
        }

        #endregion
    }
}