﻿using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    [Serializable(), XmlRoot("MobileOfferPageSection")]
    public class MobileOfferPageSection : IPageSection
    {
        /// <summary>
        /// Gets/Sets PageDetail
        /// </summary>
        [XmlElement("PageSection")]
        public PageSection PageDetail { get; set; }

        #region IPageSection Members

        /// <summary>
        /// Gets page section.
        /// </summary>
        public PageSection GetPageSection()
        {
            return PageDetail;
        }

        #endregion
    }
}
