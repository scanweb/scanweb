﻿//  Description					:   MessageSection                                        //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    /// <summary>
    /// This class represents message section.
    /// </summary>
    [Serializable()]
    public class MessageSection
    {
        /// <summary>
        /// Gets/Sets Messages
        /// </summary>
        [XmlElement("Message")]
        public List<Message> Messages { get; set; }

        /// <summary>
        /// Gets message by Id
        /// </summary>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public string GetMessage(string messageId)
        {
            string msg = "";

            if (Messages != null && Messages.Count > 0)
            {
                msg =
                    Messages.Where(
                        mssg => string.Equals(messageId, mssg.Id, StringComparison.InvariantCultureIgnoreCase))
                        .Select(mssg => mssg.Text).FirstOrDefault();
            }
            return msg;
        }
    }

    /// <summary>
    /// Message class
    /// </summary>
    [Serializable]
    public class Message
    {
        /// <summary>
        /// Gets/Sets Id
        /// </summary>
        [XmlAttribute("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets/Sets Text 
        /// </summary>
        [XmlText()]
        public string Text { get; set; }
    }
}