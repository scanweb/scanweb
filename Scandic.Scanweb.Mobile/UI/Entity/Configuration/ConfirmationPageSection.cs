﻿using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    [Serializable(), XmlRoot("ConfirmationPageSection")]
    public class ConfirmationPageSection : IPageSection
    {
        [XmlElement("PageSection")]
        public PageSection PageDetail { get; set; }

        #region IPageSection Members

        public PageSection GetPageSection()
        {
            return PageDetail;
        }

        #endregion
    }
}