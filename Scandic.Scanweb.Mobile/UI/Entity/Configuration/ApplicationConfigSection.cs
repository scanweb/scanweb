﻿using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    /// <summary>
    /// ApplicationConfigSection
    /// </summary>
    [Serializable(), XmlRoot("ApplicationConfigSection")]
    public class ApplicationConfigSection : MessageSection
    {
    }
}