﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    [Serializable(), XmlRoot("CancelConfirmationPageSection")]
    public class CancelConfirmationPageSection : IPageSection
    {
        [XmlElement("PageSection")]
        public PageSection PageDetail { get; set; }

        #region IPageSection Members

        public PageSection GetPageSection()
        {
            return PageDetail;
        }

        #endregion
    }
}
