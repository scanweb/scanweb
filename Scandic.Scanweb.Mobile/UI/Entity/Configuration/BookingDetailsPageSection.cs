﻿using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    /// <summary>
    /// BookingDetailsPageSection
    /// </summary>
    [Serializable(), XmlRoot("BookingDetailsPageSection")]
    public class BookingDetailsPageSection : IPageSection
    {
        [XmlElement("PageSection")]
        public PageSection PageDetail { get; set; }

        #region IPageSection Members

        /// <summary>
        /// GetPageSection
        /// </summary>
        /// <returns>PageSection</returns>
        public PageSection GetPageSection()
        {
            return PageDetail;
        }

        #endregion
    }
}