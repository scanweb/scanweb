﻿//  Description					: ContentPageSection                                      //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    /// <summary>
    /// ContentPageSection
    /// </summary>
    public class ContentPageSection : IPageSection
    {
        /// <summary>
        /// Gets/Sets PageDetail
        /// </summary>
        [XmlElement("PageSection")]
        public PageSection PageDetail { get; set; }

        /// <summary>
        /// Gets/Sets ContentPageMenu
        /// </summary>
        [XmlElement("Menu")]
        public Menu ContentPageMenu { get; set; }

        #region IPageSection Members

        /// <summary>
        /// GetPageSection
        /// </summary>
        /// <returns>PageSection</returns>
        public PageSection GetPageSection()
        {
            return PageDetail;
        }

        #endregion
    }
}