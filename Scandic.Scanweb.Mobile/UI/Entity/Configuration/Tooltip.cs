﻿using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    /// <summary>
    /// Tooltip
    /// </summary>
    [Serializable()]
    public class Tooltip
    {
        [XmlAttribute("heading")]
        public string Heading { get; set; }

        [XmlText()]
        public string Text { get; set; }
    }
}