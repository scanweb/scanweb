﻿//  Description					: SearchHotelPageSection                                  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    /// <summary>
    /// This class represents SearchHotelPageSection
    /// </summary>
    [Serializable(), XmlRoot("SearchHotelPageSection")]
    public class SearchHotelPageSection : IPageSection
    {
        /// <summary>
        /// Gets/Sets PageDetail
        /// </summary>
        [XmlElement("PageSection")]
        public PageSection PageDetail { get; set; }

        /// <summary>
        /// Gets/Sets AllowedYearsForDateOfArrival
        /// </summary>
        [XmlElement("AllowedDaysForDateOfArrival")]
        public int AllowedDaysForDateOfArrival { get; set; }

        /// <summary>
        /// Gets/Sets AllowedBookingDays
        /// </summary>
        [XmlElement("AllowedBookingDays")]
        public int AllowedBookingDays { get; set; }

        #region IPageSection Members

        /// <summary>
        /// Gets page section.
        /// </summary>
        public PageSection GetPageSection()
        {
            return PageDetail;
        }

        #endregion
    }
}