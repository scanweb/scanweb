﻿using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity.Configuration
{
    [Serializable(), XmlRoot("StartPageSection")]
    public class StartPageSection : IPageSection
    {
        [XmlElement("PageSection")]
        public PageSection PageDetail { get; set; }

        [XmlElement("Menu")]
        public Menu ContextMenu { get; set; }

        #region IPageSection Members

        public PageSection GetPageSection()
        {
            return PageDetail;
        }

        #endregion
    }
}