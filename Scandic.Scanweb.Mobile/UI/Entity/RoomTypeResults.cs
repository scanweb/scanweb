﻿//  Description					: RoomTypeResults                           	 		  //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                           							  //
// Creation Date				:                                                         //
//	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
// Revison History				: -NA-													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


using System.Collections.Generic;

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// RoomTypeResults entity
    /// </summary>
    public class RoomTypeResults
    {
        /// <summary>
        /// Gets/Sets RoomTypes
        /// </summary>
        public List<RoomTypeDetails> RoomTypes { get; set; }
        /// <summary>
        /// Gets/Sets MoreRoomTypesToFetch
        /// </summary>
        public bool MoreRoomTypesToFetch { get; set; }
        /// <summary>
        /// Gets/Sets Errors
        /// </summary>
        public List<ErrorDetails> Errors { get; set; }
        /// <summary>
        /// Gets/Sets Messages
        /// </summary>
        public List<MessageDetails> Messages { get; set; }
        /// <summary>
        /// Gets/Sets IsAlternateSearch
        /// </summary>
        public bool IsAlternateSearch { get; set; }
        /// <summary>
        /// Gets/Sets AlternativeSearchUrl
        /// </summary>
        public string AlternativeSearchUrl { get; set; }
    }
}