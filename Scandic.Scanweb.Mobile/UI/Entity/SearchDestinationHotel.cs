﻿using Scandic.Scanweb.Core;
namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// This class will hold data to be returned to client side for destination autocomplete control.
    /// For this reason all the properties will be in the camel case as required in the javascript
    /// method.
    /// </summary>
    public class SearchDestinationHotel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string PostalCity { get; set; }
        public Point Coordinate { get; set; }
    }
}