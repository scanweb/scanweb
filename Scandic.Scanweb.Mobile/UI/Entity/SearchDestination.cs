﻿//  Description					: SearchDestination                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// This class will hold data to be returned to client side for destination autocomplete control.
    /// For this reason all the properties will be in the camel case as required in the javascript
    /// method.
    /// </summary>
    public class SearchDestination
    {
        /// <summary>
        /// Gets/Sets id 
        /// </summary>
        public string id { get; set; }


        /// <summary>
        /// Gets/Sets name 
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Gets/Sets isCity
        /// </summary>
        public bool isCity { get; set; }
        public string AlternateCity { get; set; }

        /// <summary>
        /// The latitude and longitude positions where the City is located.
        /// </summary>
        public Point Coordinate {get; set;}
        /// <summary>
        /// The Distance from current location.
        /// </summary>
        public double DistanceFromCurrentLocation { get; set; }

        /// <summary>
        /// Gets/Sets hotels.
        /// </summary>
        public List<SearchDestinationHotel> hotels { get; set; }
    }
}