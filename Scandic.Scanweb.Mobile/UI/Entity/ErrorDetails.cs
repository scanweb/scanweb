﻿//  Description					:   ErrorDetails                                          //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// ErrorDetails
    /// </summary>
    public class ErrorDetails
    {
        /// <summary>
        /// Gets/Sets ErrorCode 
        /// </summary>
        public string ErrorCode { get; set; }

        /// <summary>
        /// Gets/Sets ErrorMessaage 
        /// </summary>
        public string ErrorMessaage { get; set; }
    }
}