﻿using System.Collections.Generic;

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// CalenderData
    /// </summary>
    public class CalenderData
    {
        public List<string> Months { get; set; }
        public List<string> WeekDays { get; set; }
    }
}