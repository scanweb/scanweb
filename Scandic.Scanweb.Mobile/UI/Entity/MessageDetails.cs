﻿namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// MessageDetails
    /// </summary>
    public class MessageDetails
    {
        public string MessageCode { get; set; }
        public string MessageInfo { get; set; }
    }
}