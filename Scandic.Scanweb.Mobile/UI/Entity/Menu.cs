﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// This class define the navigational menu items that need to be displayed. It will be
    /// used on start page.
    /// </summary>   
    [Serializable()]
    public class Menu
    {
        [XmlElement("Item")]
        public List<MenuItem> Items { get; set; }
    }

    /// <summary>
    /// Class defines the attributes for any menu item. It will be used on menu items
    /// listed for start page.
    /// </summary>
    [Serializable(), XmlRoot("Item")]
    public class MenuItem
    {
        [XmlElement("Text")]
        public string Name { get; set; }

        [XmlAttribute("path")]
        public string Path { get; set; }

        [XmlElement("Attribute")]
        public List<Attribute> Attributes { get; set; }

        public Attribute GetAttribute(string attributeId)
        {
            var returnedAttr =
                Attributes.Where(
                    attr => string.Equals(attr.Id, attributeId, StringComparison.InvariantCultureIgnoreCase)).
                    FirstOrDefault();

            return returnedAttr;
        }
    }

    /// <summary>
    /// Attribute 
    /// </summary>
    [Serializable(), XmlRoot("Attribute")]
    public class Attribute
    {
        [XmlText()]
        public string Value { get; set; }

        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("addInMarkup")]
        public bool AddInMarkup { get; set; }
    }
}