﻿
namespace Scandic.Scanweb.Mobile.UI.Entity
{
    public class ImageCarousel
    {
        public string CarouselImageHRef { get; set; }

        public string CarouselImageSrc { get; set; }

        public string CarouselImageTitle { get; set; }

        public string CarouselImageWidth { get; set; }

        public string CarouselImageAlt { get; set; }

        public string CarouselAutoSlide { get; set; }
    }
}
