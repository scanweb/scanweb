﻿//  Description					:   KeyValueOptions                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// This Class will hold list of key value pair data.
    /// </summary>
    public class KeyValueOptions
    {
        /// <summary>
        /// Gets/Sets Options
        /// </summary>
        [XmlElement("Option")]
        public List<KeyValueOption> Options { get; set; }
    }
}