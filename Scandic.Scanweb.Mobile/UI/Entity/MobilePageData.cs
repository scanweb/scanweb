﻿namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// MobilePageData
    /// </summary>
    public class MobilePageData
    {
        public string PageReferenceKey { get; set; }
        public string PageName { get; set; }
        public string LinkURL { get; set; }
        public string PageURL { get; set; }
        public string LanguageBranch { get; set; }
        public bool IsSecure { get; set; }
    }
}