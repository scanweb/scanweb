﻿using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// OverlayItem
    /// </summary>
    [Serializable(), XmlRoot("Item")]
    public class OverlayItem
    {
        [XmlText()]
        public string Value { get; set; }

        [XmlAttribute("data")]
        public string Data { get; set; }
    }
}