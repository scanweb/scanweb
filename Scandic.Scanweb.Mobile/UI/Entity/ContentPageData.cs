﻿//  Description					: ContentPageData                                         //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// This class represents ContentPageData
    /// </summary>
    public class ContentPageData
    {
        /// <summary>
        /// Gets/Sets Heading 
        /// </summary>
        public string Heading { get; set; }
        /// <summary>
        /// Gets/Sets PageIdentifier
        /// </summary>
        public string PageIdentifier { get; set; }
        /// <summary>
        /// Gets/Sets CloseButtonText
        /// </summary>
        public string CloseButtonText { get; set; }
        /// <summary>
        /// Gets/Sets GenericItemsInfo 
        /// </summary>
        public List<ItemsInfo> GenericItemsInfo { get; set; }
        /// <summary>
        /// Gets/Sets ExternalLinks
        /// </summary>
        public List<ExternalLinks> ExternalLinks { get; set; }
    }

    /// <summary>
    /// This class represents ItemsInfo
    /// </summary>
    public class ItemsInfo
    {
        /// <summary>
        /// Gets/Sets ItemLabel 
        /// </summary>
        public string ItemLabel { get; set; }
        /// <summary>
        /// Gets/Sets ItemDescription 
        /// </summary>
        public string ItemDescription { get; set; }
    }

    /// <summary>
    /// This class represents ExternalLinks
    /// </summary>
    public class ExternalLinks
    {
        /// <summary>
        /// Gets/Sets LinkLabel
        /// </summary>
        public string LinkLabel { get; set; }
        /// <summary>
        /// Gets/Sets LinkURL
        /// </summary>
        public string LinkURL { get; set; }
    }

    /// <summary>
    /// This class represents StandardPageData
    /// </summary>
    public class StandardPageData
    {
        /// <summary>
        /// Gets/Sets PageIdentifier
        /// </summary>
        public string PageIdentifier { get; set; }
        /// <summary>
        /// Gets/Sets Heading
        /// </summary>
        public string Heading { get; set; }
        /// <summary>
        /// Gets/Sets OfficeHeading
        /// </summary>
        public string OfficeHeading { get; set; }
        /// <summary>
        /// Gets/Sets PostAddress
        /// </summary>
        public string PostAddress { get; set; }
        /// <summary>
        /// Gets/Sets PhoneNumber 
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Gets/Sets Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Gets/Sets VisitingAddsLabel
        /// </summary>
        public string VisitingAddsLabel { get; set; }
        /// <summary>
        /// Gets/Sets VisitingAddress
        /// </summary>
        public string VisitingAddress { get; set; }
        /// <summary>
        /// Gets/Sets OrganizationNumLabel
        /// </summary>
        public string OrganizationNumLabel { get; set; }
        /// <summary>
        /// Gets/Sets OrganizationNumber
        /// </summary>
        public string OrganizationNumber { get; set; }
    }
}