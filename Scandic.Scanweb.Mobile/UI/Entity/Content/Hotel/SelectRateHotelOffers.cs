﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using EPiServer.Core;

namespace Scandic.Scanweb.Mobile.UI.Entity.Content.Hotel
{
    public class SelectRateHotelOffers
    {
        public string OfferTitle { get; set; }
        public string OfferImageURL { get; set; }
        public string OfferDescription { get; set; }
        public string OfferPriceText { get; set; }
        public string OfferPageURL { get; set; }
        public string NoOfferMessage { get; set; }
        
    }
}
