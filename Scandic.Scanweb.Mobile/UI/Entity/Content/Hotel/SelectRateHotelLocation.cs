﻿
using Scandic.Scanweb.Core;
namespace Scandic.Scanweb.Mobile.UI.Entity.Content.Hotel
{
    public class SelectRateHotelLocation
    {
        /// <summary>
        /// hotel address on select rate page
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// hotel address on select rate page
        /// </summary>
        public string HotelAddress { get; set; }

        /// <summary>
        /// hotel distance from city center
        /// </summary>
        public string DistanceFromCityCenter { get; set; }

        /// <summary>
        /// hotel distance from city center
        /// </summary>
        public double DistanceFromCurrentLocation { get; set; }

        /// <summary>
        /// hotel contact no
        /// </summary>
        public string HotelPhoneNo { get; set; }

        /// <summary>
        /// Hotel Longitude
        /// </summary>
        public double HotelLongitude { get; set; }

        /// <summary>
        /// Hotel Latitude
        /// </summary>
        public double HotelLatitude { get; set; }
    }
}
