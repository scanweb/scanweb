﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Scandic.Scanweb.Mobile.UI.Entity.Content.Hotel
{
    public class HotelFacility
    {
        public int? NoOfRooms { get; set; }
        public int? NonSmokingRooms { get; set; }
        public string RoomsForDisabled { get; set; }
        public string RelaxCenter { get; set; }
        public string RestaurantBar { get; set; }
        public string Garage { get; set; }
        public string OutdoorParking { get; set; }
        public string Shop { get; set; }
        public string MeetingFacilities { get; set; }
        public string EcoLabeled { get; set; }
        public string CityCenterDistance { get; set; }
        public string Airport1Name { get; set; }
        public string Airport1Distance { get; set; }
        public string DistanceTrain { get; set; }
        public string WirelessInternet { get; set; }
    }
}
