﻿
namespace Scandic.Scanweb.Mobile.UI.Entity.Content.Hotel
{
    public class RedemptionPoints
    {
        public string StartDate { get; set; }

        public string EndDate { get; set; }
        
        public string RewardPoints { get; set; }

        public bool IsCampaignPointsAvailable { get; set; }
        
    }
}
