﻿
using Scandic.Scanweb.Entity;
using System.Collections.Generic;
namespace Scandic.Scanweb.Mobile.UI.Entity.Content.Hotel
{
    public class SelectRateHotelOverview
    {
        public string HotelID { get; set; }

        public string HotelName { get; set; }

        public string HotelImageURL { get; set; }
        /// <summary>
        /// hotel description on select rate page
        /// </summary>
        public string HotelDescription { get; set; }

        /// <summary>
        /// reward nights for hotel on select rate page
        /// </summary>
        public string RewardNight { get; set; }

        public List<RedemptionPoints> RewardNights { get; set; } 
        /// <summary>
        /// hotel trip advisor ratings entity
        /// </summary>
        //public TripAdvisorPlaceEntity TripAdvisor { get; set; }

        /// <summary>
        /// trip Advisor Review review popup Url
        /// </summary>
        public string TAReviewPopUpURL { get; set; }

        /// <summary>
        /// Displays the trip Advisor Review count
        /// </summary>
        public string TAReviewCountText { get; set; }

        /// <summary>
        /// Trip Advisor Image Url
        /// </summary>
        public string TARatingImageURL { get; set; }

        /// <summary>
        /// Additional Facilities available in Hotel 
        /// </summary>
        public HotelFacility HotelFacilities { get; set; }

        public bool HideTARating { get; set; }
    }
}
