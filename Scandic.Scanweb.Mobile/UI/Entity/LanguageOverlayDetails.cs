﻿using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// LanguageOverlayDetails
    /// </summary>
    [Serializable(), XmlRoot("LanguageOverlay")]
    public class LanguageOverlayDetails : OverlayDetails
    {
    }
}