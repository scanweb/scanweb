﻿using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// This class will hold general key value pair of data.
    /// </summary>    
    public class KeyValueOption
    {
        [XmlAttribute("key")]
        public string Key { get; set; }

        [XmlText()]
        public string Value { get; set; }
    }
}