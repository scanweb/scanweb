﻿////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  HotelResults                                           //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                                               		  //
// Creation Date				:           	    								      //
// Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
// Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// This class will hold the data to be rendered for SelectHotel page.
    /// </summary>
    public class HotelResults
    {
        /// <summary>
        /// Gets/Sets HotelList
        /// </summary>
        public List<HotelDetail> HotelList { get; set; }

        /// <summary>
        /// Gets/Sets MoreHotelsToFetch
        /// </summary>
        public bool MoreHotelsToFetch { get; set; }

        /// <summary>
        /// Gets/Sets Errors
        /// </summary>
        public List<ErrorDetails> Errors { get; set; }

        /// <summary>
        /// Gets/Sets Messages
        /// </summary>
        public List<MessageDetails> Messages { get; set; }

        /// <summary>
        /// Gets/Sets IsAlternateSearch
        /// </summary>
        public bool IsAlternateSearch { get; set; }

        /// <summary>
        /// Gets/Sets SearchHeading
        /// </summary>
        public string SearchHeading { get; set; }

        /// <summary>
        /// Gets/Sets HotelsFound
        /// </summary>
        public int HotelsFound { get; set; }

        /// <summary>
        /// Gets/Sets SearchBy
        /// </summary>
        public SearchByType SearchBy { get; set; }

        public bool RemoveSoryByDiscountType { get; set; }

        public bool IsPromoSearch { get; set; }
    }
}