﻿using System.Collections.Generic;

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    public class RoomTypeDetails
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PerNight { get; set; }
        public string ImagePath { get; set; }
        public List<RateTypeDetails> Rates { get; set; }
    }
}