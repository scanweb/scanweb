﻿//  Description					:   HotelDetails                                          //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// Holds hotel information.
    /// </summary>
    public class HotelDetail
    {
        /// <summary>
        /// Gets/Sets Id  
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets/Sets Name  
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets/Sets Description  
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets/Sets ImageUrl  
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Gets/Sets AvailableRoomPrice  
        /// </summary>
        public string AvailableRoomPrice { get; set; }

        /// <summary>
        /// Gets/Sets Address  
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets/Sets CityCenterDistance  
        /// </summary>
        public string CityCenterDistance { get; set; }
        /// <summary>
        /// Gets/Sets CurrentLocationDistance  
        /// </summary>
        public string CurrentLocationDistance { get; set; }

        /// <summary>
        /// Gets/Sets PerNight  
        /// </summary>
        public string PerNight { get; set; }

        /// <summary>
        /// Gets/Sets CorporateCode  
        /// </summary>
        public string CorporateCode { get; set; }

        /// <summary>
        /// Gets/Sets the Hotel Location ID
        /// </summary>
        public int TALocationID { get; set; }

        /// <summary>
        /// Gets/Sets the popup url for Trip Advisor Reviews
        /// </summary>
        public string TAReviewPopUpURL { get; set; }

        /// <summary>
        /// Displays the trip Advisor Review count
        /// </summary>
        public string TAReviewCountText { get; set; }

        /// <summary>
        /// Displays the trip Advisor Review count
        /// </summary>
        public string TARatingImageURL { get; set; }

        /// <summary>
        /// IF Trip Advisor rating flag is enabled in CMS
        /// </summary>
        public bool HideTARating { get; set; }

        /// <summary>
        /// IF Trip Advisor rating flag for City is enabled in CMS
        /// </summary>
        public bool HideCityTARating { get; set; }

        /// <summary>
        /// If hotel has promotion rates
        /// </summary>
        public bool HasPromotionRoomRates { get; set; }

        /// <summary>
        /// Promotion code
        /// </summary>
        public string PromoCode { get; set; }

    }
}