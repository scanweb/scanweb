﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// OverlayDetails
    /// </summary>
    [Serializable()]
    public class OverlayDetails
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OverlayDetails()
        {
            IsCachable = true;
        }

        [XmlElement("Heading")]
        public string Heading { get; set; }

        [XmlElement("Item")]
        public List<OverlayItem> Items { get; set; }

        [XmlAttribute("Id")]
        public string Id { get; set; }

        [XmlAttribute("IsCachable")]
        public bool IsCachable { get; set; }
    }
}