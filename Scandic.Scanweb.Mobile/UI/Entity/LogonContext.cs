﻿namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// LogonContext
    /// </summary>
    public class LogonContext
    {
        public string Id { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
}