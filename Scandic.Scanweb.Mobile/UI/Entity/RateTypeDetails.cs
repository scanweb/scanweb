﻿//  Description					:   RateTypeDetails                                       //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// Contains members of rate type details.
    /// </summary>
    public class RateTypeDetails
    {
        /// <summary>
        /// Gets/Sets Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets/Sets Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets/Sets Rate
        /// </summary>
        public string Rate { get; set; }

        /// <summary>
        /// Gets/Sets Color
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Gets/Sets Type
        /// </summary>
        public RateType Type { get; set; }

        /// <summary>
        /// Gets/Sets HoldGuranteeAvailable
        /// </summary>
        public bool HoldGuranteeAvailable { get; set; }

        /// <summary>
        /// Gets/Sets CreditCardGuranteeType
        /// </summary>
        public string CreditCardGuranteeType { get; set; }
    }

    /// <summary>
    /// Holds all ratetypes.
    /// </summary>
    public enum RateType
    {
        Flex = 0,
        Early = 1,
        SpecialRate = 2
    }
}