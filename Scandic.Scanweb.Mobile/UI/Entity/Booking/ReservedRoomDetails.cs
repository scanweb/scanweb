﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking
{
    public class ReservedRoomDetails
    {
        public string ReservationNumber { get; set; }
        public string RoomType { get; set; }
        public string RateType { get; set; }
        public string RateTypeId { get; set; }
        public bool IsBlockCode { get; set; }
        public string NoOfAdults { get; set; }
        public string NoOfChildren { get; set; }        
        public string PriceFirstNight { get; set; }
        public string GuaranteeCancellationPolicyCode { get; set; }
        public bool IsCancellable { get; set; }
        public ReservedCustomerDetails CustomerDetails { get; set; }
        public bool HideRateInfo { get; set; }

    }
}
