﻿using Scandic.Scanweb.Mobile.UI.Entity.Booking.Model;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking
{
    /// <summary>
    /// BookingContext
    /// </summary>
    public class BookingContext
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public BookingContext()
        {
            SearchHotelPage = new SearchHotelModel();
        }

        public SearchHotelModel SearchHotelPage { get; set; }
        public SelectHotelModel SelectHotelPage { get; set; }
        public SelectRateModel SelectRatePage { get; set; }
        public BookingDetailModel BookingDetailPage { get; set; }
        public BookingPage LastVisitedPage { get; set; }
        public BookingProcess CurrentBookingProcess { get; set; }

        /// <summary>
        /// This method return the page model from the booking context for the 
        /// specified boooking page.
        /// </summary>
        /// <param name="bookingPageId">BookingPage</param>
        /// <returns>Page Model</returns>
        public BaseBookingModel GetPageData(BookingPage bookingPageId)
        {
            BaseBookingModel pageData = null;

            switch (bookingPageId)
            {
                case BookingPage.SearchHotel:
                    pageData = SearchHotelPage;
                    break;
                case BookingPage.SelectHotel:
                    pageData = SelectHotelPage;
                    break;
                case BookingPage.SelectRate:
                    break;
                case BookingPage.BookingDetails:
                    break;
                case BookingPage.BookingConfirmation:
                    break;
                default:
                    break;
            }
            return pageData;
        }
    }
}