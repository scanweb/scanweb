﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking
{
    public class ReservedHotelDetails
    {
        public string HotelName { get; set; }
        public string HotelCode { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public SearchType HotelSearchType { get; set; }
        public string TotalPrice { get; set; }
        public bool HideIncludingAllTaxText { get; set; }
        public List<ReservedRoomDetails> BookedRooms { get; set; }
        public bool HideRateInfo { get; set; }
    }
}
