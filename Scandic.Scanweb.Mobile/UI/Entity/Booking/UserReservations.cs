﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking
{
    /// <summary>
    /// This class will hold list of future and past bookings belonging to user.
    /// </summary>
    public class UserReservations
    {
        public List<UserReservation> FutureBookings { get; set; }
        public bool FetchMoreFutureBookings { get; set; }
        public List<ErrorDetails> Errors { get; set; }
    }
}
