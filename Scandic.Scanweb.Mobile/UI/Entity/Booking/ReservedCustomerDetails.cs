﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking
{
    public class ReservedCustomerDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string MembershipNumber { get; set; }
        public string AltLastName{ get; set; }
    }
}
