﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking
{
    public class UserTransaction
    {
        public string Transaction { get; set; }
        public DateTime CheckInDate { get; set; }
        public string CheckInDateInString { get; set; }
        public DateTime CheckOutDate { get; set; }
        public string CheckOutDateInString { get; set; }
        public double Points { get; set; }
        public string PointsInString { get; set; }
    }
}
