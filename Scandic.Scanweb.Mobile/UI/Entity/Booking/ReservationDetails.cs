﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking
{
    public class ReservationDetails
    {
        public string ReservationNumber { get; set; }

        public HotelSearchEntity HotelSearch { get; set; }
        public ReservedHotelDetails HotelDetails { get; set; }        
        public ErrorDetails Error { get; set; }
        public MessageDetails Message { get; set; }
        public bool IsSessionBooking { get; set; }
    }
}
