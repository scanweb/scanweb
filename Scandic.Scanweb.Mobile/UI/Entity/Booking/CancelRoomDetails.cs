﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking
{
    [Serializable]
    public class CancelRoomDetails
    {
        public string ReservationNumber { get; set; }
        public string LegNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string CancellationNumber { get; set; }
        public string ErrorMessage { get; set; }
        public string HotelCode { get; set; }
        public SearchType SearchTypeCode { get; set; }
    }
}
