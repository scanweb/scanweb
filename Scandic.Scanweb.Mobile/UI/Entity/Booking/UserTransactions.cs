﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking
{
    public class UserTransactions
    {
        public List<UserTransaction> Transactions { get; set; }
        public bool FetchMoreTransactions { get; set; }
        public List<ErrorDetails> Errors { get; set; }
    }
}
