﻿using System;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking.Model
{
    /// <summary>
    /// SelectRateModel
    /// </summary>
    [Serializable()]
    public class SelectRateModel : BaseBookingModel
    {
        public SelectRateModel()
        {
            PageId = BookingPage.SelectRate;
        }

        /// <summary>
        /// This property will hold the rate selected by the user on select rate page
        /// </summary>
        public string SelectedRate { get; set; }
        /// <summary>
        /// This property will hold selected RateHighlightText for mobile.
        /// </summary>
        /// 
        public string SelectedRateHighlightText { get; set; }
        public string SelectedRateLanguage { get; set; }
        
        public string SelectedRateTypeId { get; set; }

        /// <summary>
        /// This property will hold the name of the selected Rate catergory.
        /// </summary>
        public string SelectedRateType { get; set; }

        /// <summary>
        /// This property will indicate whether selected rate is applicable to hold the booking
        /// till 6 PM in evening.
        /// </summary>
        public bool HoldGuranteeAvailable { get; set; }

        /// <summary>
        /// This property will hold the CMS ID of radio option text to be shown to user for providing 
        /// the credit card details.
        /// </summary>
        public string CreditCardGuranteeType { get; set; }

        /// <summary>
        /// This property will hold the room type of the selected rate.
        /// </summary>
        public string SelectedRoomType { get; set; }

        public string SelectedRoomTypeId { get; set; }
    }
}