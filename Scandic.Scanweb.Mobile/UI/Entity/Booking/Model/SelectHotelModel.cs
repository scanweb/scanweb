﻿using System;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking.Model
{
    /// <summary>
    /// SelectHotelModel
    /// </summary>
    [Serializable()]
    public class SelectHotelModel : BaseBookingModel
    {
        public SelectHotelModel()
        {
            PageId = BookingPage.SelectHotel;
        }

        /// <summary>
        /// This property will hold the opera id of the hotel
        /// selected by user on the select hotel page.
        /// </summary>
        public string SelectedHotelId { get; set; }

        /// <summary>
        /// This property will hold the name of the hotel
        /// selected by user on the select hotel page.
        /// </summary>
        public string SelectedHotel { get; set; }

        /// <summary>
        /// This property will be set to true when user selects a particular hotel on select hotel page.
        /// </summary>
        public bool IsHotelSelectedByUser { get; set; }

        /// <summary>
        /// This property indicate that selected hotel has only public rates and not any promotional rates.
        /// </summary>
        public bool SelectedHotelHasOnlyPublicRates { get; set; }
    }
}