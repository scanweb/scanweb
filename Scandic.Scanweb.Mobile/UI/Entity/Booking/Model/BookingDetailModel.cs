﻿//  Description					: BookingDetailModel                                      //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking.Model
{
    /// <summary>
    /// BookingDetail entity
    /// </summary>
    [Serializable()]
    public class BookingDetailModel : BaseBookingModel
    {
        public BookingDetailModel()
        {
            PageId = BookingPage.BookingDetails;
        }

        /// <summary>
        /// Gets/Sets MembershipId
        /// </summary>
        public string MembershipId { get; set; }

        /// <summary>
        /// Gets/Sets TotalRate
        /// </summary>
        public string TotalRate { get; set; }

        /// <summary>
        /// Gets/Sets FirstName
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets/Sets LastName
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets/Sets EmailAddress
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets/Sets CountryCode
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets/Sets Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets/Sets PhoneCountryCode
        /// </summary>
        public string PhoneCountryCode { get; set; }

        /// <summary>
        /// Gets/Sets MobileNumber
        /// </summary>
        public string MobileNumber { get; set; }

        /// <summary>
        /// Gets/Sets DNumber
        /// </summary>
        public string DNumber { get; set; }

        /// <summary>
        /// Gets/Sets ReceiveConfirmationSMS
        /// </summary>
        public bool ReceiveConfirmationSMS { get; set; }

        /// <summary>
        /// Gets/Sets BedPreferenceCode
        /// </summary>
        public string BedPreferenceCode { get; set; }

        /// <summary>
        /// Gets/Sets BedPreference
        /// </summary>
        public string BedPreference { get; set; }

        /// <summary>
        /// Gets/Sets FloorPreference
        /// </summary>
        public string FloorPreference { get; set; }

        /// <summary>
        /// Gets/Sets FloorPreferenceCode
        /// </summary>
        public string FloorPreferenceCode { get; set; }

        /// <summary>
        /// Gets/Sets ElevatorPreferenceCode
        /// </summary>
        public string ElevatorPreferenceCode { get; set; }

        /// <summary>
        /// Gets/Sets ElevatorPreference
        /// </summary>
        public string ElevatorPreference { get; set; }

        /// <summary>
        /// Gets/Sets SmokingPreferenceCode
        /// </summary>
        public string SmokingPreferenceCode { get; set; }

        /// <summary>
        /// Gets/Sets SmokingPreference
        /// </summary>
        public string SmokingPreference { get; set; }

        /// <summary>
        /// Gets/Sets AdditionalRequest
        /// </summary>
        public string AdditionalRequest { get; set; }

        /// <summary>
        /// Gets/Sets AccessibleRoom
        /// </summary>
        public bool AccessibleRoom { get; set; }

        /// <summary>
        /// Gets/Sets AllergyFriendlyRoom
        /// </summary>
        public bool AllergyFriendlyRoom { get; set; }

        /// <summary>
        /// Gets/Sets PetFriendlyRoom
        /// </summary>
        public bool PetFriendlyRoom { get; set; }

        /// <summary>
        /// Gets/Sets ReservationGuaranteeOption
        /// </summary>
        public string ReservationGuaranteeOption { get; set; }

        /// <summary>
        /// Gets/Sets CardNumber
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Gets/Sets CardHolderName
        /// </summary>
        public string CardHolderName { get; set; }

        /// <summary>
        /// Gets/Sets CardType
        /// </summary>
        public string CardType { get; set; }

        /// <summary>
        /// Gets/Sets CardTypeCode
        /// </summary>
        public string CardTypeCode { get; set; }

        /// <summary>
        /// Gets/Sets CardExpiryMonthCode
        /// </summary>
        public string CardExpiryMonthCode { get; set; }

        /// <summary>
        /// Gets/Sets CardExpiryMonth
        /// </summary>
        public string CardExpiryMonth { get; set; }

        /// <summary>
        /// Gets/Sets CardExpiryYearCode
        /// </summary>
        public string CardExpiryYearCode { get; set; }

        /// <summary>
        /// Gets/Sets CardExpiryYear
        /// </summary>
        public string CardExpiryYear { get; set; }

        /// <summary>
        /// Gets/Sets AcceptTermsCondition
        /// </summary>
        public bool AcceptTermsCondition { get; set; }

        /// <summary>
        /// Gets/Sets SourceCode
        /// </summary>
        public string SourceCode { get; set; }

        /// <summary>
        /// Gets/Sets ReservationNumber
        /// </summary>
        public string ReservationNumber { get; set; }

        /// <summary>
        /// Gets/Sets UpdateProfileUrl
        /// </summary>
        public string UpdateProfileUrl { get; set; }

        /// <summary>
        /// Gets/Sets PanHash
        /// </summary>
        public string PanHash { get; set; }

        /// <summary>
        /// Gets/Sets IsFlexFGPSavedCreditPayment
        /// </summary>
        public bool IsFlexFGPSavedCreditPayment { set; get; }
    }
}