﻿//  Description					:   BaseBookingModel                                      //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using Scandic.Scanweb.Mobile.UI.Entity.Model;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking.Model
{
    /// <summary>
    /// This class represent base booking model.
    /// </summary>
    public class BaseBookingModel : BasePageModel
    {
        /// <summary>
        /// Gets/Sets IsVisited
        /// </summary>
        public bool IsVisited { get; set; }

        /// <summary>
        /// Gets/Sets PageId 
        /// </summary>
        public BookingPage PageId { get; set; }
    }

    /// <summary>
    /// Holds all booking page types.
    /// </summary>
    public enum BookingPage
    {
        None = 0,
        SearchHotel = 1,
        SelectHotel = 2,
        SelectRate = 3,
        BookingDetails = 4,
        BookingConfirmation = 5
    }

    /// <summary>
    /// Holds all booking process types.
    /// </summary>
    public enum BookingProcess
    {
        BookARoom = 0,
        ClaimRewardNights = 1
    }
}