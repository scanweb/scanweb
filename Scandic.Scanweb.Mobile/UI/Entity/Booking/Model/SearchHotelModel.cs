﻿using System;
using System.Collections.Generic;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking.Model
{
    /// <summary>
    /// SearchHotelModel
    /// </summary>
    [Serializable()]
    public class SearchHotelModel : BaseBookingModel
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SearchHotelModel()
        {
            PageId = BookingPage.SearchHotel;
            TrackingPropertySearchedHotelCount = -1;
            IsDeeplinkRequest = false;
        }

        public bool IsMinDistanceDestinationsNull = false;
        public bool IsAgeInputInvalid { get; set; }
        public string SearchDestination { get; set; }
        public string SearchDestinationId { get; set; }
        public SearchByType SearchBy { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public int? NumberOfAdults { get; set; }
        public int?[] AgeOfChildren { get; set; }
        public List<ChildrenInfo> ChildrenInformation { get; set; }
        public int? NumberOfChildren { get; set; }
        public BookingOffer Offer { get; set; }
        public string BookingCode { get; set; }
        public bool RememberBookingCode { get; set; }
        public SearchType SearchType { get; set; }
        public bool IsDeeplinkRequest { get; set; }
        public string ChildrenDetailText { get; set; }
        public Point CurrentCoordinate { get; set; }
        public bool IsSearchfromCurrentLocation { get; set; }
        public bool IsSearchWithinDefinedRadius { get; set; }
        public List<SearchDestination> SearchNearbyDestinations { get; set; }
        public string SearchDestinationType { get; set; }
        public bool IsAlternateSearch { get; set; }
        /// <summary>
        /// This property will be set in the Select Hotel controller's GetAvailableHotels to set the number of
        /// hotels returned for search. This property is set for site catalyst tracking purpose, so it should be
        /// used in other scenarios with utmost care or it shall not be used.
        /// </summary>
        public int TrackingPropertySearchedHotelCount { get; set; }
    }

    public enum BookingOffer
    {
        None = 0,
        BonusCheques = 1,
        BookingCode = 2
    }

    public enum SearchByType
    {
        None = 0,
        City = 1,
        Hotel = 2
    }

    public enum SearchType
    {
        Regular = 0,
        Corporate = 1,
        BonusCheque = 2,
        Redemption = 3,
        Voucher = 4
    }

    public enum DestinationSortType
    {
        DistanceToSearchedDestination = 0,
        Name = 1,
        Price = 2,
        DistanceToCity = 3,
        Points = 4,
        DistanceFormCurrentLocation = 5,
        TripAdvisorRatings = 6,
        DiscountType = 7
    }

    public class ChildrenInfo
    {
        public int Index { get; set; }
        public int? Age { get; set; }
        public string BedType { get; set; }
    }
}