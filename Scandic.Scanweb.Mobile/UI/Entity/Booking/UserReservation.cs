﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scandic.Scanweb.Mobile.UI.Entity.Booking
{
    /// <summary>
    /// This class is used to define the information that is shown on the 
    /// My Bookings page to show the list of bookings that belong to user
    /// </summary>
    public class UserReservation
    {
        public string HotelName { get; set; }
        public string ReservationNumber { get; set; }
        public DateTime CheckInDate { get; set; }
        public string CheckInDateInString { get; set; }
        public DateTime CheckOutDate { get; set; }
        public string CheckOutDateInString { get; set; }
        public bool IsHotelExist { get; set; }

    }
}
