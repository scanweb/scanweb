﻿using System;
using System.Xml.Serialization;

namespace Scandic.Scanweb.Mobile.UI.Entity
{
    /// <summary>
    /// OverlayKeyItem
    /// </summary>
    [Serializable()]
    public class OverlayKeyItem : OverlayItem
    {
        [XmlAttribute("key")]
        public string Key { get; set; }
    }
}