using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.CodeCoverage;

namespace RBC.Coverage.Analysis
{
    /// <summary>
    /// Program
    /// </summary>
	class Program
	{
		#region Constants
		private const string DEFAULT_OUTFILE_NAME = "CodeCoverageResults.xml";
		#endregion Constants

		#region Private Fields
		private static bool DisplayHelp = false;
		private static string CoverageDataFile = null;
		private static string SymbolsPath = null;
		private static string ExePath = null;
		private static string OutFile = null;
		#endregion Private Fields

		#region Main
        /// <summary>
        /// Main
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
		static int Main(string[] args)
		{
			try
			{
				//if no arguments supplied show help
				if (args.Length == 0)
				{
					ShowHelp();
					return 0;
				}

				//parse the supplied arguments
				ParseArguments(args);

				//if '/help' argument was supplied then show help
				if (DisplayHelp == true)
				{
					ShowHelp();
					return 0;
				}
				// '/help' argument not supplied so process normally
				else
				{
					//make sure a code coverage data file was specified
					if ((CoverageDataFile == null) || (CoverageDataFile.Trim().Length == 0))
					{
						System.Console.WriteLine("No code coverage data file was specified. Use the /infile parameter to specify it.");
						return 1;
					}

					//make sure output file is specified
					if ((OutFile == null) || (OutFile.Trim().Length == 0))
						OutFile = Path.GetFullPath(DEFAULT_OUTFILE_NAME);

					//set symbols/exe paths
					CoverageInfoManager.SymPath = SymbolsPath;
					CoverageInfoManager.ExePath = ExePath;

					//load the specified code coverage data file
					CoverageInfo cinfo = CoverageInfoManager.CreateInfoFromFile(CoverageDataFile);

					//Generate a dataset from the code coverage data file
					CoverageDS cdata = cinfo.BuildDataSet(null);

					//write the dataset as XML
					cdata.WriteXml(OutFile);

					return 0;
				}
			}
			catch (Exception ex)
			{
				System.Console.WriteLine(ex.Message);
				return 1;
			}
		}
		#endregion Main

		#region Private Methods
		private static void ParseArguments(string[] args)
		{
			foreach (string arg in args)
			{
				Arg argument = ParseArg(arg);
				switch (argument.Cmd.ToUpper())
				{
					case "/HELP":
						DisplayHelp = true;
						break;
					case "/INFILE":
						if ((argument.Value != null) && (argument.Value.Trim().Length > 0))
							CoverageDataFile = Path.GetFullPath(argument.Value);
						else
							CoverageDataFile = argument.Value;
						break;
					case "/OUTFILE":
						if ((argument.Value != null) && (argument.Value.Trim().Length > 0))
							OutFile = Path.GetFullPath(argument.Value);
						else
							OutFile = argument.Value;
						break;
					case "/SYMPATH":
						if ((argument.Value != null) && (argument.Value.Trim().Length > 0))
							SymbolsPath = Path.GetFullPath(argument.Value);
						else
							SymbolsPath = argument.Value;
						break;
					case "/EXEPATH":
						if ((argument.Value != null) && (argument.Value.Trim().Length > 0))
							ExePath = Path.GetFullPath(argument.Value);
						else
							ExePath = argument.Value;
						break;
					default:
						DisplayHelp = true;
						break;
				}
			}
		}

        /// <summary>
        /// ParseArg
        /// </summary>
        /// <param name="arg"></param>
        /// <returns>Arg</returns>
		private static Arg ParseArg(string arg)
		{
			Arg argument = new Arg();

			int index = arg.IndexOf(':');
			if (index > 0)
			{
				argument.Cmd = arg.Substring(0, index);
				argument.Value = arg.Substring(index+1, arg.Length - index - 1);
			}
			else
			{
				argument.Cmd = arg;
			}

			return argument;
		}

        /// <summary>
        /// ShowHelp
        /// </summary>
		private static void ShowHelp()
		{
			System.Console.WriteLine("");
			System.Console.WriteLine("Usage:");
			System.Console.WriteLine("");
			System.Console.WriteLine("Description: \t Converts a Visual Studio 2005 code coverage data file to XML.");
			System.Console.WriteLine("");
			System.Console.WriteLine("Options:");
			System.Console.WriteLine("");
			System.Console.WriteLine("/help \t\t Display this usage message.");
			System.Console.WriteLine("");
			System.Console.WriteLine("/infile \t Load a code coverage data file.");
			System.Console.WriteLine("\t\t Example:");
			System.Console.WriteLine("\t\t   /infile:data.coverage");
			System.Console.WriteLine("");
			System.Console.WriteLine("/outfile \t Save the conversion output to the specified file.");
			System.Console.WriteLine("\t\t Example:");
			System.Console.WriteLine("\t\t   /outfile:c:\\temp\\coverageresults.xml");
			System.Console.WriteLine("");
			System.Console.WriteLine("/sympath \t The path to the debug symbol files.");
			System.Console.WriteLine("\t\t Example:");
			System.Console.WriteLine("\t\t   /sympath:c:\\testresults\\out");
			System.Console.WriteLine("");
			System.Console.WriteLine("/exepath \t The path to the executable file.");
			System.Console.WriteLine("\t\t Example:");
			System.Console.WriteLine("\t\t   /exepath:c:\\testresults\\out");
		}
		#endregion Private Methods

		#region Types
		private struct Arg
		{
			public string Cmd;
			public string Value;
		}
		#endregion Structs
	}
}
