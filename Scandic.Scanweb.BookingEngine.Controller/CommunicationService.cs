//  Description					: Entity Class for Email                    			  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Shankar Dasgupta                                  	  //
/// Author email id				:                           							  //
/// Creation Date				: 04th November  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces

using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using ExpertPdf.HtmlToPdf;
using Scandic.Scanweb.CMS.Util.SMSManager;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// This class helps to send different types of communication
    /// </summary>
    public class CommunicationService
    {
        #region Public Methods

        /// <summary>
        /// Send Email using SMTP Server
        /// </summary>
        /// <param name="emailEntity">
        /// EmailEntity
        /// </param>
        /// <param name="confirmationNumber">
        /// This parameter is optional.Only be used for booking flow.
        /// This is only used for text file attachment in booking flow.
        /// </param>
        /// <returns>
        /// Return Status. True if successful else false.
        /// </returns>
        public static bool SendMail(EmailEntity emailEntity, string confirmationNumber)
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:SendMail():Start time");
            bool sendMailSuccess = true;
            string fileName = string.Empty;
            string receiptFileName = string.Empty;
            MailMessage mailMessage = null;
            try
            {
                Scanweb.Core.AppLogger.LogInfoMessage("In CommunicationService\\SendMail()");
                Scanweb.Core.AppLogger.LogInfoMessage(emailEntity.Body);
                Scanweb.Core.AppLogger.LogInfoMessage(emailEntity.TextEmailBody);
                mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(emailEntity.Sender);
                mailMessage.Subject = emailEntity.Subject;
                mailMessage.Body = emailEntity.Body;
                mailMessage.BodyEncoding = Encoding.UTF8;

                if (!string.IsNullOrEmpty(emailEntity.TextEmailBody) && (!string.IsNullOrEmpty(confirmationNumber)))
                {
                    AttachPDFFile(emailEntity, mailMessage, confirmationNumber, out fileName, out receiptFileName);
                }

                for (int recipientCount = 0; recipientCount < emailEntity.Recipient.Length; recipientCount++)
                {
                    mailMessage.To.Add(new MailAddress(emailEntity.Recipient[recipientCount]));
                }

                if (emailEntity.CarbonCopy != null)
                {
                    for
                        (int carbonCopyReciepientCount = 0;
                         carbonCopyReciepientCount < emailEntity.CarbonCopy.Length;
                         carbonCopyReciepientCount++)
                    {
                        mailMessage.CC.Add(new MailAddress(emailEntity.CarbonCopy[carbonCopyReciepientCount]));
                    }
                }
                mailMessage.SubjectEncoding = System.Text.Encoding.GetEncoding("utf-8");
                AlternateView view = AlternateView.CreateAlternateViewFromString(emailEntity.Body, null, "text/html");
                mailMessage.AlternateViews.Add(view);
                mailMessage.IsBodyHtml = true;

                SmtpClient smtpClient = new SmtpClient();
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                //smtpClient.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                //smtpClient.PickupDirectoryLocation = "C:\\SRI\\Scandic\\Mails";
                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:Actual smtpClient.Send() Start time");
                smtpClient.Send(mailMessage);
                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:Actual smtpClient.Send() End time");
            }
            catch (SmtpFailedRecipientsException FailedRecipientException)
            {
                sendMailSuccess = false;
                AppLogger.LogFatalException(FailedRecipientException,
                                            "The Failed Recipient is: " + FailedRecipientException.FailedRecipient);
            }
            catch (SmtpException smtpException)
            {
                sendMailSuccess = false;
                AppLogger.LogFatalException(smtpException);
            }
            catch (InvalidOperationException invalidException)
            {
                sendMailSuccess = false;
                AppLogger.LogFatalException(invalidException);
            }
            catch (Exception ex)
            {
                sendMailSuccess = false;
            }
            finally
            {
                mailMessage.Dispose();

                if (!string.IsNullOrEmpty(fileName))
                {
                    try
                    {
                        File.Delete(fileName);
                    }
                    catch (Exception fileDeletion)
                    {
                        AppLogger.LogFatalException
                            (fileDeletion, "IO exception occured during deletion of temporary text file");
                    }
                }

                if (!string.IsNullOrEmpty(receiptFileName))
                {
                    try
                    {
                        File.Delete(receiptFileName);
                    }
                    catch (Exception fileDeletion)
                    {
                        AppLogger.LogFatalException
                            (fileDeletion, "IO exception occured during deletion of temporary text file");
                    }
                }
            }
            return sendMailSuccess;
        }

        /// <summary>
        /// Send SMS 
        /// </summary>
        /// <param name="smsEntity">
        /// SMSEntityt
        /// </param>
        /// <returns>true or false</returns>
        public static bool SendSMS(SMSEntity smsEntity)
        {
            bool result = true;
            SMSManager smsManager = new SMSManager(smsEntity.PhoneNumber, smsEntity.Message);
            try
            {
                AppLogger.LogInfoMessage("In CommunicationService\\SendSMS()");
                AppLogger.LogInfoMessage
                    (string.Format("PhoneNumber: {0}  Message: {1}", smsEntity.PhoneNumber, smsEntity.Message));

                if (!smsManager.SendSMS())
                {
                    string customMessage = GetSMSExceptionMessage(smsManager.errMessage, smsEntity);
                    ApplicationException smsAppEx = new ApplicationException(customMessage);
                    AppLogger.LogFatalException(smsAppEx);
                    result = false;
                }
            }
            catch (Exception ex)
            {
                string customMessage = string.Empty;
                if (smsManager != null)
                {
                    customMessage = GetSMSExceptionMessage(smsManager.errMessage, smsEntity);
                }
                AppLogger.LogFatalException(ex, customMessage);
                result = false;
            }
            return result;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// This method returns a formatted custom message string containing the phone number and the message.
        /// </summary>
        /// <param name="errorMessage">The base error message string.</param>
        /// <param name="smsEntity">The SMS Entity object that holds the phone number and the sms.</param>
        /// <returns>Sms Exception message</returns>
        private static string GetSMSExceptionMessage(string errorMessage, SMSEntity smsEntity)
        {
            StringBuilder customMessage = new StringBuilder();
            customMessage.AppendLine("SMS Sending Failed.");
            customMessage.Append("  Provider Error Message: ");
            customMessage.AppendLine(errorMessage);
            customMessage.Append("  Phone Number: ");
            customMessage.AppendLine(smsEntity.PhoneNumber);
            customMessage.Append("  SMS Message Length: ");
            customMessage.AppendLine(smsEntity.Message.Length.ToString());
            customMessage.Append("  SMS Message: ");
            customMessage.AppendLine(smsEntity.Message);
            customMessage.AppendLine();
            return customMessage.ToString();
        }

        /// <summary>
        /// Attaches the PDF file.
        /// </summary>
        /// <param name="emailEntity">The email entity.</param>
        /// <param name="mailMessage">The mail message.</param>
        /// <param name="confirmationNumber">The confirmation number.</param>
        /// <param name="fileName">Name of the file.</param>
        private static void AttachPDFFile
            (EmailEntity emailEntity, MailMessage mailMessage, string confirmationNumber, out string fileName, out string receiptFileName)
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:CreatePDF:Start time");
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:Create File Name:Start time");

            string tempFolderPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            tempFolderPath = tempFolderPath.Substring(0, tempFolderPath.LastIndexOf("\\"));
            tempFolderPath += "\\lang\\dropdowns\\";
            tempFolderPath = tempFolderPath.Substring(tempFolderPath.IndexOf("\\") + 1);
            tempFolderPath += ConfigurationManager.AppSettings["BookingFolder"];
            fileName = tempFolderPath + "\\bookingDetails_" + confirmationNumber + ".pdf";
            receiptFileName = tempFolderPath + "\\creditCardReceipt_" + emailEntity.ReceiptReservationNumber + ".pdf";
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:Create File Name:End time");

            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:Create Directory:Start time");
            if (!Directory.Exists(tempFolderPath))
            {
                Directory.CreateDirectory(tempFolderPath);
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:Create Directory:End time");

            if (!string.IsNullOrEmpty(fileName))
            {
                ExpertPdf.HtmlToPdf.PdfConverter convertor = new PdfConverter();

                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:Get Licence key:Start time");
                convertor.LicenseKey = ConfigurationManager.AppSettings["ExpertPDF.LicenseKey"];
                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:Get Licence key:End time");

                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:SavePdfFromHtmlStringToFile:Start time");
                convertor.SavePdfFromHtmlStringToFile(emailEntity.TextEmailBody, fileName);
                Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:SavePdfFromHtmlStringToFile:End time");

                if (!string.IsNullOrEmpty(emailEntity.CreditCardReceiptHtmlText))
                {
                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:SaveReceiptPdfFromHtmlStringToFile:Start time");
                    convertor.SavePdfFromHtmlStringToFile(emailEntity.CreditCardReceiptHtmlText, receiptFileName);
                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:SaveReceiptPdfFromHtmlStringToFile:End time");
                }
            }

            System.Net.Mime.ContentType contentType = new System.Net.Mime.ContentType("application/pdf");
            Attachment attachment = new Attachment(fileName, contentType);
            mailMessage.Attachments.Add(attachment);

            if (!string.IsNullOrEmpty(emailEntity.CreditCardReceiptHtmlText))
            {
                contentType = new System.Net.Mime.ContentType("application/pdf");
                attachment = new Attachment(receiptFileName, contentType);
                mailMessage.Attachments.Add(attachment);
            }

            emailEntity.TextEmailBody = string.Empty;
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("CommunicationService:CreatePDF:End time");
        }

        #endregion
    }
}