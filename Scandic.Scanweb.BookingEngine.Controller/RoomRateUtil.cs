using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using System.Linq;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The utility class containing the various methods required for
    /// processing the room categories, room types, rate categories and rate plans
    /// </summary>
    public class RoomRateUtil
    {
        #region Private Members

        /// <summary>
        /// The ratePlanMap containing the key ==> RatePlanCode value==> The RateCategory it belongs to
        /// </summary>
        private static Dictionary<string, RateCategory> ratePlanMap;

        /// <summary>
        /// The roomTypeMap containing the key ==> RoomTypeCode value ==> The RoomCategory the roomtype belongs to
        /// </summary>
        private static Dictionary<string, RoomCategory> roomTypeMap;

        #endregion

        #region Public Properties

        public static Dictionary<string, RateCategory> RatePlanMap
        {
            set { RoomRateUtil.ratePlanMap = value; }
        }

        public static Dictionary<string, RoomCategory> RoomTypeMap
        {
            set { RoomRateUtil.roomTypeMap = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Checks if the ratePlanCode is mapped in the CMS
        /// </summary>
        /// <param name="ratePlanCode">The Rate Plan Code</param>
        /// <returns>true if the ratePlanCode exisiting and mapped to a rate category in CMS
        /// false if the ratePlanCode is not mapped to any rate category in CMS</returns>
        public static bool IsRatePlanCodeInCMS(string ratePlanCode)
        {
            return (null != GetRateCategoryByRatePlanCode(ratePlanCode));
        }

        /// <summary>
        /// Reads the corresponding RateCategory the <code>ratePlanCode</code> is mapped and returns it,
        /// If the ratePlan code is not configured and mapped to any ratecategory then returns <code>null</code>
        /// 
        /// If called from any thread the propery <code>RatePlanMap</code> should be previously set before
        /// called this method else throws the <code>InvalidOperationException</code>
        /// 
        /// If called from outside thread the RatePlanMap is read from the CDAL class which is already cached
        /// </summary>
        /// <param name="ratePlanCode">The ratePlanCode</param>
        /// <returns>RateCategory</returns>
        public static RateCategory GetRateCategoryByRatePlanCode(string ratePlanCode)
        {
            try
            {
                ratePlanMap = ContentDataAccess.GetRateTypeCategoryMap(false);
            }
            catch (Exception Ex)
            {
                AppLogger.LogInfoMessage("Unable to get RateTypeCategoryMap." + Ex.StackTrace);
            }

            if (null == ratePlanMap)
                throw new InvalidOperationException(
                    "The RatePlanMap is not set, it need to be set before calling this method");

            RateCategory rateCategory = null;
            try
            {
                if (ratePlanCode != null)
                {
                    rateCategory = ratePlanMap[ratePlanCode];
                }
            }
            catch (KeyNotFoundException knfe)
            {
                AppLogger.LogInfoMessage(" Rate Code " + ratePlanCode + "  Is not configured in CMS to a Rate Category." +
                                         knfe.StackTrace);
            }
            return rateCategory;
        }

        /// <summary>
        /// Reads the corresponding RateCategory which has <code>rateCategoryId</code> if not found
        /// then returns <code>null</code>
        /// 
        /// If called from any thread the propery <code>RatePlanMap</code> should be previously set before
        /// called this method else throws the <code>InvalidOperationException</code>
        /// 
        /// If called from outside thread the RatePlanMap is read from the CDAL class which is already cached
        /// </summary>
        /// <param name="rateCategoryId">The ratePlanCode</param>
        /// <returns>RateCategory</returns>
        public static RateCategory GetRateCategoryByCategoryId(string rateCategoryId)
        {
            Dictionary<string, RateCategory> rateCategoryMap = null;
            try
            {
                rateCategoryMap = ContentDataAccess.GetRateTypeCategoryMap(false);
            }
            catch (Exception ex)
            {
                AppLogger.LogInfoMessage("Unable to get RateTypeCategoryMap." + ex.StackTrace);
            }
            if (null == ratePlanMap)
                throw new InvalidOperationException(
                    "The RatePlanMap is not set, it need to be set before calling this method");
            else
            {
                foreach (string ratePlanCode in rateCategoryMap.Keys)
                {
                    RateCategory eachRateCategory = rateCategoryMap[ratePlanCode];
                    if (eachRateCategory.RateCategoryId == rateCategoryId)
                        return eachRateCategory;
                }
            }

            return null;
        }
        /// <summary>
        /// Checks if the <code>roomTypeCode is mapped in CMS</code>
        /// </summary>
        /// <param name="roomTypeCode">The roomTypeCode which needs to be searched in CMS</param>
        /// <returns>true if the roomTypeCode mapped to any category in CMS else returns false</returns>
        public static bool IsRoomTypeCodeInCMS(string roomTypeCode)
        {
            return (null != GetRoomCategory(roomTypeCode));
        }

        /// <summary>
        /// Returns the RoomCategory to which the <code>roomTypeCode</code> is mapped in CMS, if not mapped to
        /// any RoomCategory then returns null
        /// 
        /// If this method is called from within the thread then the <code>RoomTypeMap</code> should be previously
        /// set before calling this method, else <code>InvalidOperationException</code> is thrown
        /// 
        /// If called from normal method then the, <code>RoomTypeMap</code> is set by reading from the CDAL
        /// </summary>
        /// <param name="roomTypeCode"></param>
        /// <returns></returns>
        public static RoomCategory GetRoomCategory(string roomTypeCode)
        {
            try
            {
                roomTypeMap = ContentDataAccess.GetRoomTypeCategoryMap();
            }
            catch (Exception Ex)
            {
                AppLogger.LogInfoMessage("Unable to get RateTypeCategoryMap." + Ex.StackTrace);
            }
            if (null == roomTypeMap)
                throw new InvalidOperationException(
                    "The RoomTypeMap is not set, it need to be set before calling this method");

            RoomCategory roomCategory = null;
            try
            {
                if (roomTypeMap.ContainsKey(roomTypeCode))
                    roomCategory = roomTypeMap[roomTypeCode];
            }
            catch (KeyNotFoundException Knfe)
            {
                AppLogger.LogInfoMessage(" Room Code " + roomTypeCode + " Is not configured in CMS to a Room category." +
                                         Knfe.StackTrace);
            }
            return roomCategory;
        }

        /// <summary>
        /// Iterates through the list rates in the RateCategory 
        /// to which the corresponding ratePlanCode belongs to 
        /// find the Rate object and returns it, 
        /// if the ratePlanCode doesn't then null is returned
        /// </summary>
        /// <param name="ratePlanCode">The room type code</param>
        /// <returns></returns>
        public static Rate GetRate(string ratePlanCode)
        {
            RateCategory rateCategory = GetRateCategoryByRatePlanCode(ratePlanCode);

            if (null != rateCategory)
            {
                foreach (Rate rate in rateCategory.Rates)
                {
                    if (rate.OperaRateId == ratePlanCode)
                        return rate;
                }
            }
            return null;
        }
      

        /// <summary>
        /// Iterates through the list of RoomCategory to which the corresponding roomTypeCode belongs to 
        /// find the RoomType object and returns it, if the room type is not exisiting then null is returned
        /// </summary>
        /// <param name="roomTypeCode">The room type code</param>
        /// <returns>RoomType</returns>
        public static RoomType GetRoomType(string roomTypeCode)
        {
            RoomCategory roomCategory = GetRoomCategory(roomTypeCode);

            if (null != roomCategory)
            {
                foreach (RoomType roomType in roomCategory.RoomTypes)
                {
                    if (roomType.OperaRoomTypeId == roomTypeCode)
                        return roomType;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns the list of Bonus cheque rate plan codes configured
        /// </summary>
        /// <returns>list of Bonus cheque rate plan codes configured</returns>
        public static string[] GetBonusChequeRateCodes()
        {
            string[] BonusChequeRates = AppConstants.BONUS_CHEQUE_RATES;
            return BonusChequeRates;
        }

        private static string[] bonusChequeRateCategoryNames;

        /// <summary>
        /// GetBonusChequeRateCategoryNames
        /// </summary>
        /// <returns>BonusChequeRateCategoryNames</returns>
        public static string[] GetBonusChequeRateCategoryNames()
        {
            if (bonusChequeRateCategoryNames == null)
            {
                List<string> rateCategoryNames = new List<string>();
                string[] rateCodes = GetBonusChequeRateCodes();

                foreach (string rateCode in rateCodes)
                {
                    RateCategory rateCategory = GetRateCategoryByRatePlanCode(rateCode);
                    rateCategoryNames.Add(rateCategory.RateCategoryId);
                }
                bonusChequeRateCategoryNames = rateCategoryNames.ToArray();
            }

            return bonusChequeRateCategoryNames;
        }

        /// <summary>
        /// Checks whether or not given rate category is Saverate category.
        /// </summary>
        /// <param name="guranteeType"></param>
        /// <returns></returns>
        public static bool IsSaveRateCategory(string rateCategoryId)
        {
            bool isSaveRateCode = false;
            string hotelOperaID = SearchCriteriaSessionWrapper.SearchCriteria != null ? SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode : string.Empty;
            if (!ContentDataAccess.GetPaymentFallback(hotelOperaID))
            {
                RateCategory rateCategory = GetRateCategoryByCategoryId(rateCategoryId);
                if ((rateCategory != null) && (!string.IsNullOrEmpty(rateCategory.CreditCardGuranteeType)))
                {
                    string[] prepaidGuaranteeTypes = AppConstants.GUARANTEE_TYPE_PRE_PAID;
                    isSaveRateCode = (prepaidGuaranteeTypes != null) && (prepaidGuaranteeTypes.Contains(rateCategory.CreditCardGuranteeType));
                }
            }

            return isSaveRateCode;
        }

        /// <summary>
        /// Checks whether or not given guranteetype is prepaid or not.
        /// </summary>
        /// <param name="guranteeType"></param>
        /// <returns></returns>
        public static bool IsPrepaidGuaranteeType(string guranteeType)
        {
            string hotelOperaID = SearchCriteriaSessionWrapper.SearchCriteria != null ? SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode : string.Empty;
            if (!ContentDataAccess.GetPaymentFallback(hotelOperaID))
            {
                string[] prepaidGuaranteeTypes = AppConstants.GUARANTEE_TYPE_PRE_PAID;
                return (prepaidGuaranteeTypes != null) && (prepaidGuaranteeTypes.Contains(guranteeType));
            }

            return false;
        }

        /// <summary>
        /// Checks whether any one of the room is save category or not
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <returns></returns>
        public static bool IsAnyRoomHavingSaveCategory(Hashtable hashSelectedRoomRateEntity)
        {
            string hotelOperaID = SearchCriteriaSessionWrapper.SearchCriteria != null ? SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode : string.Empty;
            if (!ContentDataAccess.GetPaymentFallback(hotelOperaID))
            {
                for (int roomIterator = 0; roomIterator < hashSelectedRoomRateEntity.Count; roomIterator++)
                {
                    SelectedRoomAndRateEntity selectedRoomRate = hashSelectedRoomRateEntity[roomIterator] as SelectedRoomAndRateEntity;

                    if (RoomRateUtil.IsSaveRateCategory(selectedRoomRate.RateCategoryID))
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="netsPaymentInfo"></param>
        /// <returns></returns>
        public static PaymentInfo ConvertToPaymentInfo(Netaxept.NetsPaymentServices.PaymentInfo netsPaymentInfo)
        {
            PaymentInfo paymentInfo = new PaymentInfo();
            try
            {
                if ((netsPaymentInfo != null) && (netsPaymentInfo.CardInformation != null))
                {
                    paymentInfo.CreditCardNumberMasked = netsPaymentInfo.CardInformation.MaskedPAN;
                    paymentInfo.PanHash = netsPaymentInfo.CardInformation.PanHash;
                    if (!string.IsNullOrEmpty(netsPaymentInfo.CardInformation.ExpiryDate) &&
                        (netsPaymentInfo.CardInformation.ExpiryDate.Length >= 4))
                    {
                        int year =
                            System.Threading.Thread.CurrentThread.CurrentCulture.Calendar.ToFourDigitYear(
                               Convert.ToInt32(netsPaymentInfo.CardInformation.ExpiryDate.Substring(0, 2)));
                        int month = Convert.ToInt32(netsPaymentInfo.CardInformation.ExpiryDate.Substring(2, 2));
                        paymentInfo.ExpiryDate = new DateTime(year, month, 1);
                        paymentInfo.ExpiryDate = paymentInfo.ExpiryDate.AddMonths(1).AddDays(-1);
                    }

                    Dictionary<string, string> netsOperaCCTypeMappings = AppConstants.NETS_OPERA_CCTYPE_MAPPING;
                    if ((netsOperaCCTypeMappings != null) && (netsOperaCCTypeMappings[netsPaymentInfo.CardInformation.PaymentMethod] != null))
                    {
                        paymentInfo.CardType = netsOperaCCTypeMappings[netsPaymentInfo.CardInformation.PaymentMethod];
                    }
                }
            }
            catch (KeyNotFoundException keyNotFoundException)
            {
                EmailEntity emailEntity = new EmailEntity();
                emailEntity.Recipient = AppConstants.EmailRecipientsForOnlinePaymentFailures;
                emailEntity.Sender = AppConstants.EmailSenderForOnlinePaymentFailures;
                emailEntity.Subject = string.Format(
                    "Customer entered card is not avaialable in our list. As its valid card, need to add card as part of the config file. Card type {0}", netsPaymentInfo.CardInformation.PaymentMethod);
                emailEntity.Body = "Merchant authentication failed while making Nets register";
                CommunicationService.SendMail(emailEntity, null);

                AppLogger.LogOnlinePaymentFatalException(keyNotFoundException, string.Format("Opera credit card mapping doesn't found for Nets payment method: {0}",netsPaymentInfo.CardInformation.PaymentMethod));
                throw keyNotFoundException;
            }
            return paymentInfo;
        }

        /// <summary>
        /// Checks whether any one of the room is save category or not
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <returns></returns>
        public static bool IsAllRoomBookingsArePrepaid(Hashtable hashSelectedRoomRateEntity)
        {
            for (int roomIterator = 0; roomIterator < hashSelectedRoomRateEntity.Count; roomIterator++)
            {
                SelectedRoomAndRateEntity selectedRoomRate = hashSelectedRoomRateEntity[roomIterator] as SelectedRoomAndRateEntity;
                if (!RoomRateUtil.IsSaveRateCategory(selectedRoomRate.RateCategoryID))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Gets the translated text by reading the value from
        /// the xml files stored in the lang folder.
        /// 
        /// key is the XPATH string starting from the root element
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetTranslatedText(string key)
        {
            var sessionLang = MiscellaneousSessionWrapper.SessionLanguage;
            if (string.IsNullOrEmpty(sessionLang))
            {
                return EPiServer.Core.LanguageManager.Instance.Translate(key);
            }
            else
            {
                return GetTranslatedText(key, sessionLang);
            }
        }

        /// <summary>
        /// Gets the translated text by reading the value from
        /// the xml files stored in the lang folder.
        /// Overloaded to accept the language string to fetch culture specific translations.
        /// key is the XPATH string starting from the root element
        /// </summary>
        /// <param name="key">The key to get the translation string.</param>
        /// <param name="language">The language identifier string.</param>
        /// <returns></returns>
        public static string GetTranslatedText(string key, string language)
        {
            return EPiServer.Core.LanguageManager.Instance.Translate(key, language);
        }

        /// <summary>
        /// Checks whether specified guranteetype belongs to any of the prepaid gauranteetypes
        /// </summary>
        /// <param name="guranteeType"></param>
        /// <returns></returns>
        public static bool IsPrepaidSaveGuaranteeType(string guranteeType)
        {
            string hotelOperaID = SearchCriteriaSessionWrapper.SearchCriteria != null ? SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode : string.Empty;
            if (!ContentDataAccess.GetPaymentFallback(hotelOperaID))
            {
                if ((ConfigurationManager.AppSettings["PrepaidInProgress.Guarantee"] != null
                     && guranteeType.Equals(ConfigurationManager.AppSettings["PrepaidInProgress.Guarantee"].Trim().ToString(),
                                         StringComparison.InvariantCultureIgnoreCase))
                    || (ConfigurationManager.AppSettings["PrepaidSuccess.Guarantee"] != null
                        && guranteeType.Equals(ConfigurationManager.AppSettings["PrepaidSuccess.Guarantee"].Trim().ToString(),
                                            StringComparison.InvariantCultureIgnoreCase))
                    || (ConfigurationManager.AppSettings["PrepaidFailure.Guarantee"] != null
                        && guranteeType.Equals(ConfigurationManager.AppSettings["PrepaidFailure.Guarantee"].Trim().ToString(),
                                            StringComparison.InvariantCultureIgnoreCase))
                    || (ConfigurationManager.AppSettings["MakePaymentFailure.Guarantee"] != null
                        && guranteeType.Equals(
                            ConfigurationManager.AppSettings["MakePaymentFailure.Guarantee"].Trim().ToString(),
                            StringComparison.InvariantCultureIgnoreCase)))
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}