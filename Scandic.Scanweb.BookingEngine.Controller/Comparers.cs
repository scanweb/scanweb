﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// This class is used to for the comparision of the Nordic Characters as defined for Scanweb.
    /// </summary>
    public class NordicComparerUtil
    {
        private static Dictionary<string, string> stringMapping = new Dictionary<string, string>();

        static NordicComparerUtil()
        {
            SetupStringMapping();
        }

        /// <summary>
        /// Following function will setup all the mappings
        /// This mapping should be organised in such a way that mapping with more character strings first and others next
        /// </summary>
        private static void SetupStringMapping()
        {
            stringMapping["aa"] = "(aa|å)";
            stringMapping["ss"] = "(ss|ß)";
            stringMapping["e"] = "(e|æ)";
            stringMapping["o"] = "(o|ö|ø)";
            stringMapping["ö"] = "(o|ö|ø)";
            stringMapping["ø"] = "(o|ö|ø)";
            stringMapping["a"] = "(a|å|æ|ä)";
            stringMapping["å"] = "(a|å|æ|ä)";
            stringMapping["æ"] = "(a|å|æ|ä)";
            stringMapping["ä"] = "(a|å|æ|ä)";
            stringMapping["ü"] = "(ü|u)";
            stringMapping["u"] = "(ü|u)";
        }

        /// <summary>
        /// This function will take the search string and searches character by character for the
        /// availablility of the strings as per the stringMapping.
        /// If it found the character it will be replaced with corresponding regex mapping and traverse to the next character
        /// 
        /// If Malmo is passed as the strToSearch then M(a|å|æ|ä)lm(o|ö|ø) will be returned
        /// </summary>
        /// <param name="strToSearch"></param>
        /// <returns></returns>
        public static string GetStringAfterReplaceNordic(string strToSearch)
        {
            string stringToReturn = "";
            int strCounter = 0;
            int stringLength = strToSearch.Length;
            while (strCounter < stringLength)
            {
                int stringLen = 1;
                bool matchFound = false;
                foreach (string key in stringMapping.Keys)
                {
                    int keyLen = key.Length;

                    int substringLength = keyLen;
                    if ((substringLength + strCounter) > stringLength)
                    {
                        substringLength = stringLength - strCounter;
                    }
                    string currentStr = strToSearch.Substring(strCounter, substringLength);
                    if (currentStr == key)
                    {
                        stringLen = keyLen;
                        stringToReturn += stringMapping[key];
                        matchFound = true;
                        break;
                    }
                }
                if (!matchFound)
                {
                    stringToReturn += strToSearch.Substring(strCounter, 1);
                }
                strCounter += stringLen;
            }
            return stringToReturn;
        }

        /// <summary>
        /// This method will takes the "searchString" regulare expression and compares this regular expression
        /// with the mainString formed. If both matches then true will be returned else false will be returned
        /// </summary>
        /// <param name="mainString">The actual string which need to be compared with the search string</param>
        /// <param name="searchString">The Search string which has the Nordic character mapping replacement done 
        /// For example M(a|å|æ|ä)lm(o|ö|ø)</param>
        /// <returns>True/False</returns>
        public static bool IsNordicTextComparisionEqual(string mainString, string searchString)
        {
            if (mainString != null && searchString != null)
            {
                Regex regex = new Regex("^" + searchString + "$", RegexOptions.IgnoreCase);
                if (regex.IsMatch(mainString))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }

    #region HOTEL DETAILS COMPARERS

    /// <summary>
    /// The Comparer used to sort the hotel on the Name of the Hotel
    /// </summary>
    public class HotelNameComparer : IComparer<IHotelDetails>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(IHotelDetails first, IHotelDetails second)
        {
            return first.HotelDestination.Name.CompareTo(second.HotelDestination.Name);
        }
    }

    /// <summary>
    /// The comaprer used to sort the hotel on the Max rate of the hotel.
    /// </summary>
    public class HotelMaxRateComparer : IComparer<IHotelDetails>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(IHotelDetails first, IHotelDetails second)
        {
            return first.MaxRate.Rate.CompareTo(second.MaxRate.Rate);
        }
    }

    /// <summary>
    /// The Comparer used to sort the hotel on the City of the Hotel
    /// </summary>
    public class PlainCityNameComparer : IComparer<IHotelDetails>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(IHotelDetails first, IHotelDetails second)
        {
            return first.HotelDestination.HotelAddress.City.CompareTo(second.HotelDestination.HotelAddress.City);
        }
    }

    /// <summary>
    /// The Comparer used to sort the hotel on the City name and Name of the Hotel
    /// Implementation for CHR29
    /// </summary>
    public class CityAndHotelComparer : IComparer<IHotelDetails>
    {
        /// <summary>
        /// The city user has searched for
        /// </summary>
        private string citySearchedFor;

        /// <summary>
        /// The Regex relaced string of the <code>citySearchedFor</code>
        /// if citySearchedFor = Malmo
        /// then citySearchedForWithRegex = M(a|å|æ|ä)lm(o|ö|ø)
        /// </summary>
        private string citySearchedForWithRegex;

        /// <summary>
        /// The constructor takes the citySearchFor and sets the <code>citySearchedForWithRegex</code>
        /// </summary>
        /// <param name="citySearchedFor"></param>
        public CityAndHotelComparer(string citySearchedFor)
        {
            this.citySearchedFor = citySearchedFor;
            this.citySearchedForWithRegex = NordicComparerUtil.GetStringAfterReplaceNordic(citySearchedFor);
        }

        /// <summary>
        /// The comparision logic works as follows
        /// 
        /// 1. Following will the sorting logic when user selects “Alphabetic” and this CHR doesn’t affect any of the other sorting options i.e., (Rate, Points and City Centre Distance)
        ///     a. The city being searched for will be given highest precedence and placed first in the list. All the hotels in the searched city will be in turn sorted by hotel name
        ///     b. The remaining hotels (Hotels in city other than city being searched for) will be first grouped by city name then sorted by hotel name
        /// 2.  As there is some mismatch in the City Name (In the City tree) is different from the city name provided in Hotel’s Location tab. Refer to attached document so following rule apply for checking the city name being searched for
        ///     a. The Nordic characters rule as applied to the “Search Text Box” will be applied in this case. The following mapping will be done using regular expressions.
        ///         "aa" = "(aa|å)"
        ///         "ss" = "(ss|ß)";
        ///         "e" = "(e|æ)";
        ///         "o" = "(o|ö|ø)";
        ///         "ö" = "(o|ö|ø)";
        ///         "ø" = "(o|ö|ø)";
        ///         "a" = "(a|å|æ|ä)";
        ///         "å" = "(a|å|æ|ä)";
        ///         "æ" = "(a|å|æ|ä)";
        ///         "ä" = "(a|å|æ|ä)";
        ///         "ü" = "(ü|u)";
        ///         "u" = "(ü|u)";
        /// 
        ///     b. After applying the rules mentioned in a. if the city names doesn’t match, it will be considered as city which is not matching the city being searched for, for example if in City tree the city name is given as “Malmo” and in the hotel city name if entered as “Maslmo” both will not match even after applying the rules in a. So the hotel will be considered as hotel in the city being searched for.
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(IHotelDetails first, IHotelDetails second)
        {
            if (
                NordicComparerUtil.IsNordicTextComparisionEqual(first.HotelDestination.HotelAddress.City,
                                                                citySearchedForWithRegex) &&
                NordicComparerUtil.IsNordicTextComparisionEqual(second.HotelDestination.HotelAddress.City,
                                                                citySearchedForWithRegex))
            {
                return first.HotelDestination.Name.CompareTo(second.HotelDestination.Name);
            }
            else if (
                NordicComparerUtil.IsNordicTextComparisionEqual(first.HotelDestination.HotelAddress.City,
                                                                citySearchedForWithRegex) &&
                first.HotelDestination.HotelAddress.City != second.HotelDestination.HotelAddress.City)
            {
                return -1;
            }
            else if (
                NordicComparerUtil.IsNordicTextComparisionEqual(second.HotelDestination.HotelAddress.City,
                                                                citySearchedForWithRegex) &&
                first.HotelDestination.HotelAddress.City != second.HotelDestination.HotelAddress.City)
            {
                return 1;
            }
            else
            {
                PlainCityNameComparer cityComparer = new PlainCityNameComparer();
                if (cityComparer.Compare(first, second) != 0)
                {
                    return cityComparer.Compare(first, second);
                }
                else
                {
                    // If both cities matcheds, sort by hotel name
                    HotelNameComparer hotelComparer = new HotelNameComparer();
                    return hotelComparer.Compare(first, second);
                }
            }
        }
    }

    /// <summary>
    /// The Comparer used to sort the hotel on the minimum rate
    /// </summary>
    public class RateComparer : IComparer<IHotelDetails>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(IHotelDetails first, IHotelDetails second)
        {
            return first.MinRate.Rate.CompareTo(second.MinRate.Rate);
        }
    }

    /// <summary>
    /// The comparer used to sort the hotels in the following way
    /// I. All hotels having the corporate rates first, within corporate rate available hotels
    /// they are sorted by name
    /// II. All non corporate rate available hotels second, within them hotels are sorted by name
    /// </summary>
    public class CorporateComparer : IComparer<IHotelDetails>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(IHotelDetails first, IHotelDetails second)
        {
            NegotiatedHotelDetails firstHotel = first as NegotiatedHotelDetails;
            NegotiatedHotelDetails secondHotel = second as NegotiatedHotelDetails;
            if (firstHotel.HasNegotiateRoomRates && secondHotel.HasNegotiateRoomRates)
                return firstHotel.HotelDestination.Name.CompareTo(secondHotel.HotelDestination.Name);
            else if (firstHotel.HasNegotiateRoomRates && !secondHotel.HasNegotiateRoomRates)
                return -1;
            else if (!firstHotel.HasNegotiateRoomRates && secondHotel.HasNegotiateRoomRates)
                return 1;
            else
                return firstHotel.HotelDestination.Name.CompareTo(secondHotel.HotelDestination.Name);
        }
    }

    /// <summary>
    /// The comparer used to sort the hotels on the Minium number of points
    /// available to book the hotel
    /// </summary>
    public class PointComparer : IComparer<IHotelDetails>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(IHotelDetails first, IHotelDetails second)
        {
            if (null != first.MinPoints && null != second.MinPoints)
                return first.MinPoints.PointsRequired.CompareTo(second.MinPoints.PointsRequired);
            return 0;
        }
    }

    /// <summary>
    /// The comparer used to sort the hotels based the distance the hotel from the city centre
    /// </summary>
    public class CityCentreDistanceComparer : IComparer<IHotelDetails>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(IHotelDetails first, IHotelDetails second)
        {
            return first.HotelDestination.CityCenterDistance.CompareTo(second.HotelDestination.CityCenterDistance);
        }
    }
    /// <summary>
    /// The comparer used to sort the hotels based the distance from current location
    /// </summary>
    public class CurrentLocationDistanceComparer : IComparer<IHotelDetails>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(IHotelDetails first, IHotelDetails second)
        {
            return first.DistanceFromCurrentLocationToSearchedHotel.CompareTo(second.DistanceFromCurrentLocationToSearchedHotel);
        }
    }

    /// <summary>
    /// The comparer used to sort the alternate hotels based on the distance from the hotel being searched for
    /// </summary>
    public class AlternateHotelsDistanceComparer : IComparer<IHotelDetails>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(IHotelDetails first, IHotelDetails second)
        {
            return first.AlternateHotelsDistance.CompareTo(second.AlternateHotelsDistance);
        }
    }

    /// <summary>
    /// The comparer used to sort the alternate hotels based on the trip advisor ratings for the hotel being searched for
    /// </summary>
    public class TripAdvisorRatingComparer : IComparer<IHotelDetails>
    {
        /// <summary>
        /// Compare 
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(IHotelDetails first, IHotelDetails second)
        {
            return second.TripAdvisorHotelRating.CompareTo(first.TripAdvisorHotelRating);
        }
    }

    #endregion

    #region DIFFERENT SORTTYPES

    /// <summary>
    /// Enumeration holding the different sort types available for the hotels
    /// </summary>
    public enum SortType
    {
        NONE,
        ALPHABETIC,
        RATE,
        CORPORATE,
        POINTS,
        CITYCENTREDISTANCE,
        ALTERNATEHOTELSDISTANCE,
        DISTANCETOCITYCENTREOFSEARCHEDHOTEL,
        TRIPADVISOR,
        DISCOUNTTYPE
    }

    #endregion

    #region ROOM RATE COMPARERS

    /// <summary>
    /// Comparer used to sort the RoomRateEntities based on the roomTypeCodes
    /// </summary>
    public class RoomRateRoomTypeComparer : IComparer<RoomRateEntity>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomRateEntity first, RoomRateEntity second)
        {
            return first.RoomTypeCode.CompareTo(second.RoomTypeCode);
        }
    }

    /// <summary>
    /// Comparer used to sort the RoomRateEntities based on the RoomCategory type the room rate belongs
    /// </summary>
    public class RoomRateRoomCategoryComparer : IComparer<RoomRateEntity>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomRateEntity first, RoomRateEntity second)
        {
            RoomCategory firstRoomCategory = RoomRateUtil.GetRoomCategory(first.RoomTypeCode);
            RoomCategory secondRoomCategory = RoomRateUtil.GetRoomCategory(second.RoomTypeCode);

            return firstRoomCategory.RoomCategoryId.CompareTo(secondRoomCategory.RoomCategoryId);
        }
    }

    /// <summary>
    /// Comparer used to sort the RoomRateEntities based on the RateCategory the room rate belongs
    /// </summary>
    public class RoomRateRateCategoryComparer : IComparer<RoomRateEntity>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomRateEntity first, RoomRateEntity second)
        {
            RateCategory firstRateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(first.RatePlanCode);
            RateCategory secondRateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(second.RatePlanCode);

            return firstRateCategory.RateCategoryId.CompareTo(secondRateCategory.RateCategoryId);
        }
    }

    /// <summary>
    /// Comparer used to sort the RoomRateEntities on the Base Rate (Rate per night)
    /// </summary>
    public class RoomRateBaseRateComparer : IComparer<RoomRateEntity>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomRateEntity first, RoomRateEntity second)
        {
            return first.BaseRate.Rate.CompareTo(second.BaseRate.Rate);
        }
    }

    /// <summary>
    /// Comparer used to sort the RoomRateEntities on the Points required
    /// </summary>
    public class RoomRatePointsComparer : IComparer<RoomRateEntity>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomRateEntity first, RoomRateEntity second)
        {
            return first.PointsDetails.PointsRequired.CompareTo(second.PointsDetails.PointsRequired);
        }
    }

    /// <summary>
    /// Comparer used to sort the RoomRateEntities based on the Total Rate (Rate per stay)
    /// For future use if required
    /// </summary>
    public class RoomRateTotalRateComparer : IComparer<RoomRateEntity>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomRateEntity first, RoomRateEntity second)
        {
            return first.TotalRate.Rate.CompareTo(second.TotalRate.Rate);
        }
    }

    /// <summary>
    /// Comparer used to sort the RoomCategoryEntities based on the roomTypeCodes
    /// </summary>
    public class RoomCategoryRoomTypeComparer : IComparer<RoomCategoryEntity>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomCategoryEntity first, RoomCategoryEntity second)
        {
            return second.Id.CompareTo(first.Id);
        }
    }

    /// <summary>
    /// This sorts the hotels in following manner
    /// 
    /// 1. RoomType code (RoomCategoryRoomTypeComparer)
    /// 2. Rate categories count
    /// </summary>
    public class RateCategoryCountComparer : IComparer<RoomCategoryEntity>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomCategoryEntity first, RoomCategoryEntity second)
        {
            int roomTypeCompare = new RoomCategoryRoomTypeComparer().Compare(first, second);

            if (0 == roomTypeCompare)
            {
                return second.RateCategories.Count.CompareTo(first.RateCategories.Count);
            }
            else
            {
                return roomTypeCompare;
            }
        }
    }

    /// <summary>
    /// The composite comparer sorts the hotels in following manner
    /// 
    /// 1. RoomType code (RoomRateRoomTypeComparer)
    /// 2. Rate Category (RoomRateRateCategoryComparer)
    /// 3. Base rate (RoomRateBaseRateComparer)
    /// </summary>
    public class CompositeMinBaseRateComparer : IComparer<RoomRateEntity>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomRateEntity first, RoomRateEntity second)
        {
            int roomTypeCompare = new RoomRateRoomTypeComparer().Compare(first, second);
            if (0 == roomTypeCompare)
            {
                return new RoomRateBaseRateComparer().Compare(first, second);
            }
            else
            {
                return roomTypeCompare;
            }
        }
    }

    /// <summary>
    /// The composite comparer sorts the hotels in following manner
    /// 
    /// 1. RoomType code (RoomRateRoomTypeComparer)
    /// 2. Rate Category (RoomRateRateCategoryComparer)
    /// 3. Points required for stay (RoomRatePointsComparer)
    /// </summary>
    public class CompositeMinPointsComparer : IComparer<RoomRateEntity>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomRateEntity first, RoomRateEntity second)
        {
            int roomTypeCompare = new RoomRateRoomTypeComparer().Compare(first, second);
            if (0 == roomTypeCompare)
            {
                int rateCategoryCompare = new RoomRateRateCategoryComparer().Compare(first, second);
                if (0 == rateCategoryCompare)
                {
                    return new RoomRatePointsComparer().Compare(first, second);
                }
                else
                {
                    return rateCategoryCompare;
                }
            }
            else
            {
                return roomTypeCompare;
            }
        }
    }


    /// <summary>
    /// The composite comparer sorts the hotels in following manner
    /// 
    /// 1. RoomType code (RoomRateRoomTypeComparer)
    /// 2. Rate Category (RoomRateRateCategoryComparer)
    /// 3. Base rate (RoomRateTotalRateComparer)
    /// 
    /// For future use
    /// </summary>
    public class CompositeMinTotalRateComparer : IComparer<RoomRateEntity>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomRateEntity first, RoomRateEntity second)
        {
            int roomTypeCompare = new RoomRateRoomTypeComparer().Compare(first, second);
            if (0 == roomTypeCompare)
            {
                return new RoomRateTotalRateComparer().Compare(first, second);
            }
            else
            {
                return roomTypeCompare;
            }
        }
    }

    /// <summary>
    /// This comparer will sort the room categories to be displayed on the Select Rate page
    /// With Room being displayed first and Superior displayed next.
    /// Fix for artf615877.
    /// 
    /// **** Future extension *****
    /// This sort orders can be added to CMS. Based on this the Room Categories can be sorted
    /// </summary>
    public class RoomCategoryComparer : IComparer<RoomCategoryEntity>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomCategoryEntity first, RoomCategoryEntity second)
        {
            if (first.Id == second.Id)
            {
                return 0;
            }
            else if (second.Id == AppConstants.ROOM_CATEGORY_ROOM)
            {
                return 1;
            }
            else if (first.Id == AppConstants.ROOM_CATEGORY_ROOM)
            {
                return -1;
            }
            else if (second.Id == AppConstants.ROOM_CATEGORY_SUPERIOR)
            {
                return 1;
            }
            else if (first.Id == AppConstants.ROOM_CATEGORY_SUPERIOR)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }

    /// <summary>
    /// This comparer will sort the room categories in alphabetic order    
    /// </summary>
    public class RoomCategoryIdComparer : IComparer<RoomCategoryEntity>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomCategoryEntity first, RoomCategoryEntity second)
        {
            return first.Id.CompareTo(second.Id);
        }
    }

    /// <summary>
    /// This comparer will sort the room categories to be displayed on the Select Rate page
    /// With Room being displayed first and Superior displayed next.
    /// </summary>
    public class RateCategoryComparer : IComparer<RateCategory>
    {
        private static Dictionary<string, int> RATE_CATEGORY_PRIORITY = new Dictionary<string, int>();

        /// <summary>
        /// RateCategoryComparer
        /// </summary>
        static RateCategoryComparer()
        {
            RATE_CATEGORY_PRIORITY.Add(AppConstants.RATE_CATEGORY_EARLY, 1);
            RATE_CATEGORY_PRIORITY.Add(AppConstants.RATE_CATEGORY_FLEX, 2);
        }

        /// <summary>
        /// GetRatePriority
        /// </summary>
        /// <param name="rateCategoryId"></param>
        /// <returns>RatePriority</returns>
        private int GetRatePriority(string rateCategoryId)
        {
            int priority = 0;
            try
            {
                priority = RATE_CATEGORY_PRIORITY[rateCategoryId];
            }
            catch (KeyNotFoundException knfe)
            {
                priority = 0;
            }
            return priority;
        }

        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RateCategory first, RateCategory second)
        {
            if (first != null && second != null)
                return (GetRatePriority(first.RateCategoryId) - GetRatePriority(second.RateCategoryId));
            else
                return 0;
        }
    }

    #endregion ROOM RATE COMPARERS

    /// <summary>
    /// The Comparer used to sort the hotel on the Name of the Hotel
    /// </summary>
    public class HotelDestinationNameComparer : IComparer<HotelDestination>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(HotelDestination first, HotelDestination second)
        {
            return first.Name.CompareTo(second.Name);
        }
    }

    /// <summary>
    /// The comparer used to sort the alternate hotels based on the trip advisor ratings for the hotel being searched for
    /// </summary>
    public class HotelDestinationTripAdvisorRatingComparer : IComparer<HotelDestination>
    {
        /// <summary>
        /// Compare 
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(HotelDestination first, HotelDestination second)
        {
            return second.TripAdvisorHotelRating.CompareTo(first.TripAdvisorHotelRating);
        }
    }


    /// <summary>
    /// The comparer used to sort the hotels based the distance the hotel from the city centre
    /// </summary>
    public class HotelDestinationCityCentreDistanceComparer : IComparer<HotelDestination>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(HotelDestination first, HotelDestination second)
        {
            return first.CityCenterDistance.CompareTo(second.CityCenterDistance);
        }
    }

    #region CR -Display Room Rate Categories

    /// <summary>
    /// Compares Base Rate entity particular to room category entity
    /// </summary>
    public class RoomCategoryEntityComparer : IComparer<RoomCategoryEntity>
    {
        private HotelSearchEntity htlSearch = SearchCriteriaSessionWrapper.SearchCriteria;

        public static List<string> RateCategoryIds { get; set; }

        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(RoomCategoryEntity first, RoomCategoryEntity second)
        {
            RoomRateEntity roomRateEntity1 = null;
            RoomRateEntity roomRateEntity2 = null;


            int count = 0;
            string rateCategoryName = string.Empty;
            if (Reservation2SessionWrapper.MinRateCategoryName != null)
                rateCategoryName = Reservation2SessionWrapper.MinRateCategoryName;
            if ((htlSearch.SearchingType == SearchType.CORPORATE) &&
                (htlSearch.QualifyingType == AppConstants.BLOCK_CODE_QUALIFYING_TYPE))
            {
                foreach (String blkCodeRoomCategory in first.RateCategories.Keys)
                {
                    roomRateEntity1 = first.RateCategories[blkCodeRoomCategory][0];
                }

                foreach (String blkCodeRoomCategory in second.RateCategories.Keys)
                {
                    roomRateEntity2 = second.RateCategories[blkCodeRoomCategory][0];
                }

                if (roomRateEntity1.BaseRate.Rate == roomRateEntity2.BaseRate.Rate)
                {
                    count = first.Name.CompareTo(second.Name);
                }
                else
                {
                    count = roomRateEntity1.BaseRate.Rate.CompareTo(roomRateEntity2.BaseRate.Rate);
                }
            }
            else if (first.RateCategories.ContainsKey(rateCategoryName) &&
                     second.RateCategories.ContainsKey(rateCategoryName))
            {
                roomRateEntity1 = first.RateCategories[rateCategoryName][0];
                roomRateEntity2 = second.RateCategories[rateCategoryName][0];
                if (htlSearch != null)
                {
                    if (htlSearch.SearchingType == SearchType.REDEMPTION)
                        if (roomRateEntity1.PointsDetails.PointsRequired ==
                            roomRateEntity2.PointsDetails.PointsRequired)
                        {
                            count = first.Name.CompareTo(second.Name);
                        }
                        else
                        {
                            count =
                                roomRateEntity1.PointsDetails.PointsRequired.CompareTo(
                                    roomRateEntity2.PointsDetails.PointsRequired);
                        }
                    else
                    {
                        if (Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage)
                        {
                            if (roomRateEntity1.TotalRate.Rate == roomRateEntity2.TotalRate.Rate)
                            {
                                count = first.Name.CompareTo(second.Name);
                            }
                            else
                            {
                                count = roomRateEntity1.TotalRate.Rate.CompareTo(roomRateEntity2.TotalRate.Rate);
                            }
                        }
                        else if (roomRateEntity1.BaseRate.Rate == roomRateEntity2.BaseRate.Rate)
                        {
                            count = first.Name.CompareTo(second.Name);
                        }
                        else
                        {
                            count = roomRateEntity1.BaseRate.Rate.CompareTo(roomRateEntity2.BaseRate.Rate);
                        }
                    }
                }
            }
            else if (!first.RateCategories.ContainsKey(rateCategoryName))
            {
                count = 1;
            }
            else if (!second.RateCategories.ContainsKey(rateCategoryName))
            {
                count = -1;
            }
            return count;
        }
    }

    #endregion CR -Display Room Rate Categories

    /// <summary>
    /// HotelDestinationComparer
    /// </summary>
    public class HotelDestinationComparer : IComparer<DestinationInfo>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(DestinationInfo first, DestinationInfo second)
        {
            return first.DestinationName.CompareTo(second.DestinationName);
        }
    }

    //Merchandising:R3:Display ordinary rates for unavailable promo rates 
    public class PromoComparer : IComparer<IHotelDetails>
    {
        public int Compare(IHotelDetails second, IHotelDetails first)
        {
            PromotionHotelDetails firstHotel = first as PromotionHotelDetails;
            PromotionHotelDetails secondHotel = second as PromotionHotelDetails;
            if (firstHotel != null && secondHotel != null)
            {
                if (!firstHotel.HasPromotionRoomRates && !secondHotel.HasPromotionRoomRates)
                    return firstHotel.HotelDestination.Name.CompareTo(secondHotel.HotelDestination.Name);
                else if (!firstHotel.HasPromotionRoomRates && secondHotel.HasPromotionRoomRates)
                    return -1;
                else if (firstHotel.HasPromotionRoomRates && !secondHotel.HasPromotionRoomRates)
                    return 1;
                else
                    return firstHotel.HotelDestination.Name.CompareTo(secondHotel.HotelDestination.Name);
            }
            return 0;
        }
    }
}