﻿using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule
{
    public class MiscellaneousSessionWrapper
    {
        public static string SessionLanguage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[ScanwebMobile.MOBILE_CONTENT_LANGUAGE] as string;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[ScanwebMobile.MOBILE_CONTENT_LANGUAGE] = value;
            }
        }


        public static string InitialAPIKey
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.INITIAL_API_KEY] as string;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.INITIAL_API_KEY] = value;
            }
        }

        public static string PreviousTrackingCode
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.PREVIOUS_TRACKING_CODE] as string;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.PREVIOUS_TRACKING_CODE] = value;
            }
        }

        public static object IsJavascriptEnabled
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.IS_JAVASCRIPT_ENABLED];
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.IS_JAVASCRIPT_ENABLED] = value;
            }
        }

        public static string ExceptionMessage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.EXCEPTION_MESSAGE] as string;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.EXCEPTION_MESSAGE] = value;
            }
        }

        public static string ExceptionTime
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.EXCEPTION_TIME] as string;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.EXCEPTION_TIME] = value;
            }
        }

        public static object PhoneCodeSessionkey
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.PHONE_CODE_SESSION_KEY];
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.PHONE_CODE_SESSION_KEY] = value;
            }
        }
        public static string MobileContentLanguage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.MOBILE_CONTENT_LANGUAGE] as string;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.MOBILE_CONTENT_LANGUAGE] = value;
            }
        }
        public static string LanguageSessionKey
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.LANGUAGE_SESSION_KEY] as string;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.LANGUAGE_SESSION_KEY] = value;
            }
        }

        public static void setAutoCompleteList(string autoCompleteListKey, object autoCompleteList)
        {
            HttpSessionState session = HttpContext.Current.Session;
            session.Contents[autoCompleteListKey] = autoCompleteList;
        }

        public static object getAutoCompleteList(string language)
        {
            HttpSessionState session = HttpContext.Current.Session;
            string autoCompleteListKey = string.Format("{0}{1}", SessionConstants.AUTOCOMPLETE_LIST, language);
            return session.Contents[autoCompleteListKey];
        }

        public static object BookingContext
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.BOOKING_CONTEXT_KEY];
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.BOOKING_CONTEXT_KEY] = value;
            }
        }

        public static object ErrorPageError
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.ERROR_PAGE_ERROR];
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.ERROR_PAGE_ERROR] = value;
            }
        }

        public static bool IsMobileError
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objMobileError = session.Contents[SessionConstants.IS_MOBILE_ERROR];
                if (objMobileError != null)
                {
                    result = (bool) objMobileError;
                }
                return result;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.IS_MOBILE_ERROR] = value;
            }
        }

        public static object SearchedMeetingRoomsPDC
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.SEARCHED_MEETING_ROOMS_PDC];
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.SEARCHED_MEETING_ROOMS_PDC] = value;
            }
        }

        public static object SearchedCityCountryPage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.SEARCHED_CITY_COUNTRY_PAGE];
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.SEARCHED_CITY_COUNTRY_PAGE] = value;
            }
        }

        public static object SearchedCountryPage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.SEARCHED_COUNTRY_PAGE];
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.SEARCHED_COUNTRY_PAGE] = value;
            }
        }

        public static object CountryCurrency
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.COUNTRY_CURRENCY];
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.COUNTRY_CURRENCY] = value;
            }
        }

        public static object SearchedCapacity
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.SEARCHED_CAPACITY];
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.SEARCHED_CAPACITY] = value;
            }
        }

        public static object MeetingsSelectedHotels
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.MEETINGS_SELECTED_HOTELS];
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.MEETINGS_SELECTED_HOTELS] = value;
            }
        }

        public static object MeetingsSelectedHotelsForRPF
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.MEETINGS_SELECTED_HOTELS_FOR_RPF];
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.MEETINGS_SELECTED_HOTELS_FOR_RPF] = value;
            }
        }

        public static bool GetIsCrisisShown(string sessionString)
        {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objMobileError = session.Contents[sessionString];
                if (objMobileError != null)
                {
                    result = (bool) objMobileError;
                }
                return result;
        }

        public static void SetIsCrisisShown(string sessionString,bool value)
        {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[sessionString] = value;
        }

        public static string TradeDoubler
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.TRADE_DOUBLER] as string;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.TRADE_DOUBLER] = value;
            }
        }

        public static object HotelRoomRateResults
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.HOTEL_ROOMRATE_RESULT];
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.HOTEL_ROOMRATE_RESULT] = value;
            }
        }

        /// <summary>
        /// This stores information whether the page id getting copied or not
        /// </summary>
        public static bool IsPageCopied
        {
            get
            {
                bool result = true;

                if (HttpContext.Current.Session[SessionConstants.IS_PAGE_COPIED] != null)
                {
                    result = (bool)HttpContext.Current.Session[SessionConstants.IS_PAGE_COPIED];
                }
                return result;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_PAGE_COPIED, value); }
        }
        public static bool OverviewCarousel
        {
            get
            {
                bool result = true;

                if (HttpContext.Current.Session[SessionConstants.OVERVIEW_CAROUSEL] != null)
                {
                    result = (bool)HttpContext.Current.Session[SessionConstants.OVERVIEW_CAROUSEL];
                }
                return result;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.OVERVIEW_CAROUSEL, value); }
        }

        public static string MOBILEURLCMPID
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.CMP_ID] as string;
            }
            set
            {
                HttpContext.Current.Session.Add(SessionConstants.CMP_ID, value);
            }
        }

    }
}
