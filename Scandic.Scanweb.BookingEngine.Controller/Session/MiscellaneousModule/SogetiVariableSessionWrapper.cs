﻿using System;
using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule
{
    public class SogetiVariableSessionWrapper
    {
        #region Sogeti Variable

        

        /// <summary>
        /// Session Variable to store the Reservation Cancellation Number as requested 
        /// by Sogeti
        /// </summary>
        public static string ReservationNumberForThirdParty
        {
            get
            {
                string reservationNumberForThirdParty = String.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object objReservationNumberForThirdParty = session.Contents[SessionConstants.RESERVATION_NUMBER_FOR_THIRD_PARTY];
                if (objReservationNumberForThirdParty != null)
                {
                    reservationNumberForThirdParty = objReservationNumberForThirdParty as string;
                }
                return reservationNumberForThirdParty;
            }
            set { HttpContext.Current.Session[SessionConstants.RESERVATION_NUMBER_FOR_THIRD_PARTY] = value; }
        }

        

        /// <summary>
        /// Session Variable to store the Reservation Cancellation Number as requested 
        /// by Sogeti
        /// </summary>
        public static string MemberShipLevelForThirdParty
        {
            get
            {
                string membershipLevelForThirdParty = String.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object objMemberShipLevelForThirdParty = session.Contents[SessionConstants.MEMBERSHIP_LEVEL_FOR_THIRD_PARTY];
                if (objMemberShipLevelForThirdParty != null)
                {
                    membershipLevelForThirdParty = objMemberShipLevelForThirdParty as string;
                }
                return membershipLevelForThirdParty;
            }
            set { HttpContext.Current.Session[SessionConstants.MEMBERSHIP_LEVEL_FOR_THIRD_PARTY] = value; }
        }

        

        /// <summary>
        /// Session Variable to store the Reservation Cancellation Number as requested 
        /// by Sogeti
        /// </summary>
        public static string PostCodeForThirdParty
        {
            get
            {
                string postCodeForThirdParty = String.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object objPostCodeForThirdParty = session.Contents[SessionConstants.POST_CODE_FOR_THIRD_PARTY];
                if (objPostCodeForThirdParty != null)
                {
                    postCodeForThirdParty = objPostCodeForThirdParty as string;
                }
                return postCodeForThirdParty;
            }
            set { HttpContext.Current.Session[SessionConstants.POST_CODE_FOR_THIRD_PARTY] = value; }
        }

        

        /// <summary>
        /// Session Variable to store the Reservation Cancellation Number as requested 
        /// by Sogeti
        /// </summary>
        public static string TotalAmountForThirdParty
        {
            get
            {
                string totalAmountForThirdParty = String.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object objtotalAmountForThirdParty = session.Contents[SessionConstants.TOTAL_AMOUNT_FOR_THIRD_PARTY];
                if (objtotalAmountForThirdParty != null)
                {
                    totalAmountForThirdParty = objtotalAmountForThirdParty as string;
                }
                return totalAmountForThirdParty;
            }
            set { HttpContext.Current.Session[SessionConstants.TOTAL_AMOUNT_FOR_THIRD_PARTY] = value; }
        }

        #endregion
    }
}
