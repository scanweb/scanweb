﻿using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule
{
    public class GenericSessionVariableSessionWrapper
    {
        #region Generic Session Variable

        

        /// <summary>
        /// Store the Total Amount in the Session
        /// </summary>
        public static string TotalAmout
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.TOTAL_AMOUNT] as string;
            }
            set { HttpContext.Current.Session[SessionConstants.TOTAL_AMOUNT] = value; }
        }

        /// <summary>
        /// Store the Total Amount in the Session
        /// </summary>
        public static bool IsPrepaidBooking
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.IS_PREPAID_BOOKING] != null)
                {
                    return (bool)session.Contents[SessionConstants.IS_PREPAID_BOOKING];
                }
                return false;
            }
            set { HttpContext.Current.Session[SessionConstants.IS_PREPAID_BOOKING] = value; }
        }

     
        #endregion
    }
}
