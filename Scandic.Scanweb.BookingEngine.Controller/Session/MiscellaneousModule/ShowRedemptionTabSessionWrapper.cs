﻿using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule
{
    public class ShowRedemptionTabSessionWrapper
    {
        #region ShowRedemptionTab

        

        /// <summary>
        /// This is used to make the redemption tab in hotel search screen 
        /// active after user log in from redemption tab.
        /// </summary>
        public static bool ShowRedemptionTab
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                object showRedemption = session.Contents[SessionConstants.BE_SHOW_REDEMPTION_TAB];
                if (showRedemption != null)
                {
                    return (bool)showRedemption;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.BE_SHOW_REDEMPTION_TAB] = value;
            }
        }

        #endregion ShowRedemptionTab
    }
}
