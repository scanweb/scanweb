﻿using System;
using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule
{
    public class ConfirmationHeaderSessionWrapper
    {
        #region ConfirmationHeader

        

        /// <summary>
        /// Gets/Sets ConfirmationHeader
        /// </summary>
        public static string ConfirmationHeader
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                object head = session.Contents[SessionConstants.HEADER];
                string headerText = String.Empty;
                if (head != null)
                {
                    headerText = head.ToString();
                }
                return headerText;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.HEADER] = value;
            }
        }

        #endregion
    }
}
