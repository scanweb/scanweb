﻿using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule
{
    public class CMSSessionWrapper
    {
        #region CMS

        

        /// <summary>
        /// This would store true if the Right Column of the pages need to display anything
        /// </summary>
        public static bool RightColumnPresent
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object rightColumnPresent = session.Contents[SessionConstants.RIGHT_COLUMN_PRESENT];
                if (rightColumnPresent != null)
                {
                    result = (bool)rightColumnPresent;
                }
                return result;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.RIGHT_COLUMN_PRESENT, value); }
        }

        public static string CurrentLanguage
        {
            get {
                string result = string.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object currentLanguage = session.Contents[SessionConstants.CUR_LANG];
                if (currentLanguage != null)
                {
                    result = currentLanguage as string;
                }
                return result;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.CUR_LANG, value); }
        }

        public static string CurrentCulture
        {
            get
            {
                string result = string.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object currentCulture = session.Contents[SessionConstants.CULTURE];
                if (currentCulture != null)
                {
                    result = currentCulture as string;
                }
                return result;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.CULTURE, value); }
        }
        #endregion
    }
}
