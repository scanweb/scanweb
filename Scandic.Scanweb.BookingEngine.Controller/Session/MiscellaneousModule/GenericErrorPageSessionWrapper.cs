﻿using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule
{
    public class GenericErrorPageSessionWrapper
    {
        #region Generic Error Page

        

        /// <summary>
        /// Sets/Gets GenericErrorHeader
        /// </summary>
        public static string GenericErrorHeader
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.GENERIC_ERROR_HEADER] as string;
            }
            set { HttpContext.Current.Session.Contents[SessionConstants.GENERIC_ERROR_HEADER] = value; }
        }

        

        /// <summary>
        /// Sets/Gets GenericErrorMessage
        /// </summary>
        public static string GenericErrorMessage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.GENERIC_ERROR_MESSAGE] as string;
            }
            set { HttpContext.Current.Session.Contents[SessionConstants.GENERIC_ERROR_MESSAGE] = value; }
        }

        #endregion Generic Error Page
    }
}
