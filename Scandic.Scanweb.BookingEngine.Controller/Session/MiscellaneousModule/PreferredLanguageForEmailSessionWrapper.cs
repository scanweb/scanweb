﻿using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule
{
    public class PreferredLanguageForEmailSessionWrapper
    {
        #region PreferredLanguageForEmail

        

        /// <summary>
        /// Conatin information about the user preferred language selected for 
        /// sending enroll email confirmation.
        /// </summary>
        public static string UserPreferredLanguage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.BE_USER_PREFERRED_LANGUAGE] as string;
            }

            set { HttpContext.Current.Session.Add(SessionConstants.BE_USER_PREFERRED_LANGUAGE, value); }
        }

        #endregion PreferredLanguageForEmail
    }
}
