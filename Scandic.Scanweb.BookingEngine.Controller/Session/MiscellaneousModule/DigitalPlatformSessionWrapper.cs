﻿using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule
{
    public class DigitalPlatformSessionWrapper
    {
        #region Digital Platform

        

        /// <summary>
        /// Set a session for deeplinking page.
        /// </summary>
        public static List<HotelDestination> ExpansionPageHotelList
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return (List<HotelDestination>)session.Contents[SessionConstants.EXPANSIONPAGE_HOTELLIST];
            }
            set { HttpContext.Current.Session.Add(SessionConstants.EXPANSIONPAGE_HOTELLIST, value); }
        }

        #endregion
    }
}
