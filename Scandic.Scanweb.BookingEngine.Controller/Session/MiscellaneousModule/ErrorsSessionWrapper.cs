﻿using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule
{
    public class ErrorsSessionWrapper
    {
        #region Errors

        

        /// <summary>
        /// Save the email delivery error to be displayed do the user
        /// </summary>
        public static List<string> FailedEmailRecipient
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.EMAIL_RECIPIENT_FAILED] as List<string>;
            }
            set { HttpContext.Current.Session[SessionConstants.EMAIL_RECIPIENT_FAILED] = value; }
        }

        /// <summary>
        /// Add to the List of Delivery Failed Email Recepient
        /// </summary>
        /// <param name="value"></param>
        public static void AddFailedEmailRecipient(string value)
        {
            List<string> listofDeliveryFailedEmail = null;
            if (HttpContext.Current.Session[SessionConstants.EMAIL_RECIPIENT_FAILED] != null)
            {
                listofDeliveryFailedEmail =
                    HttpContext.Current.Session[SessionConstants.EMAIL_RECIPIENT_FAILED] as List<string>;
            }

            if (listofDeliveryFailedEmail == null)
            {
                listofDeliveryFailedEmail = new List<string>();
                HttpContext.Current.Session[SessionConstants.EMAIL_RECIPIENT_FAILED] = listofDeliveryFailedEmail;
            }
            listofDeliveryFailedEmail.Add(value);
        }

        /// <summary>
        /// Clear the FailedEmail Recepient
        /// </summary>
        public static void ClearFailedEmailRecipient()
        {
            List<string> listofDeliveryFailedEmail =
                HttpContext.Current.Session[SessionConstants.EMAIL_RECIPIENT_FAILED] as List<string>;

            if (listofDeliveryFailedEmail != null)
            {
                listofDeliveryFailedEmail.Clear();
            }
        }

        

        /// <summary>
        /// Save the sme delivery error to be displayed do the user
        /// </summary>
        public static List<string> FailedSMSRecepient
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.SMS_RECIPIENT_FAILED] as List<string>;
            }
            set { HttpContext.Current.Session[SessionConstants.SMS_RECIPIENT_FAILED] = value; }
        }

        /// <summary>
        /// Add to the List of Delivery Failed SMS Recepient
        /// </summary>
        /// <param name="value"></param>
        public static void AddFailedSMSRecepient(string value)
        {
            List<string> listofDeliveryFailedSMS =
                HttpContext.Current.Session[SessionConstants.SMS_RECIPIENT_FAILED] as List<string>;

            if (listofDeliveryFailedSMS == null)
            {
                listofDeliveryFailedSMS = new List<string>();
                HttpContext.Current.Session[SessionConstants.SMS_RECIPIENT_FAILED] = listofDeliveryFailedSMS;
            }
            listofDeliveryFailedSMS.Add(value);
        }

        /// <summary>
        /// Clear the FailedEmail Recepient
        /// </summary>
        public static void ClearFailedSMSRecepient()
        {
            List<string> listofDeliveryFailedSMS =
                HttpContext.Current.Session[SessionConstants.SMS_RECIPIENT_FAILED] as List<string>;

            if (listofDeliveryFailedSMS != null)
            {
                listofDeliveryFailedSMS.Clear();
            }
        }

        #endregion
    }
}
