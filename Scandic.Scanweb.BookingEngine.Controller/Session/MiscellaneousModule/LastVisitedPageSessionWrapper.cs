﻿using System.Web;
using System.Web.SessionState;
using Scandic.Scanweb.Entity;
using EPiServer.Core;
using Scandic.Scanweb.Core;
using EPiServer;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule
{
    public class LastVisitedPageSessionWrapper
    {
        #region Last Visited Page

        

        /// <summary>
        /// Get/Set of Last Visited Page
        /// </summary>
        public static PageDetails LastVisitedPage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[SessionConstants.LAST_VISITED_PAGE] != null)
                {
                    return session.Contents[SessionConstants.LAST_VISITED_PAGE] as PageDetails;
                }
                else
                {
                    return null;
                }
            }
            set { HttpContext.Current.Session[SessionConstants.LAST_VISITED_PAGE] = value; }
        }

        
        #endregion Last Visited Page

        /// <summary>
        /// This property hold the information about the current Main menu link selected by user on Scanweb.
        /// </summary>
        public static PageReference SelectedMainMenuLink
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[AppConstants.MAINMENU_SELECTED_PAGE_REFERENCE] != null)
                {
                    return session.Contents[AppConstants.MAINMENU_SELECTED_PAGE_REFERENCE] as PageReference;
                }
                else
                {
                    return null;
                }
            }
            set { HttpContext.Current.Session[AppConstants.MAINMENU_SELECTED_PAGE_REFERENCE] = value; }
        }

        /// <summary>
        /// This property hold the name of the current sub menu page selected by user on Scanweb.
        /// </summary>
        public static string SelectedSubMenuPageName
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[AppConstants.SUBMENU_SELECTED_PAGE_NAME] != null)
                {
                    return session.Contents[AppConstants.SUBMENU_SELECTED_PAGE_NAME] as string;
                }
                else
                {
                    return null;
                }
            }
            set { HttpContext.Current.Session[AppConstants.SUBMENU_SELECTED_PAGE_NAME] = value; }
        }

        public static PageBase DefaultSelectedSubMenuPageData
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[AppConstants.SUBMENU_SELECTED_PAGE_DATA] != null)
                {
                    return session.Contents[AppConstants.SUBMENU_SELECTED_PAGE_DATA] as PageBase;
                }
                else
                {
                    return null;
                }
            }
            set { HttpContext.Current.Session[AppConstants.SUBMENU_SELECTED_PAGE_DATA] = value; }
        }

        public static PageData UserSelectedSubMenuPageData
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[AppConstants.SUBMENU_SELECTED_PAGE_LINK] != null)
                {
                    return session.Contents[AppConstants.SUBMENU_SELECTED_PAGE_LINK] as PageData;
                }
                else
                {
                    return null;
                }
            }
            set { HttpContext.Current.Session[AppConstants.SUBMENU_SELECTED_PAGE_LINK] = value; }
        }

    }
}
