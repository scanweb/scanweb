﻿using System.Collections;
using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule
{
    public class HygieneSessionWrapper
    {
        #region Hygiene

        

        /// <summary>
        /// Set a session for 6 PM booking.
        /// </summary>
        public static bool Is6PMBooking
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                bool bookingFlag = false;
                if (session.Contents[SessionConstants.IS6PM_BOOKING] != null)
                {
                    bookingFlag = (bool)session.Contents[SessionConstants.IS6PM_BOOKING];
                }
                return bookingFlag;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS6PM_BOOKING, value); }
        }

        /// <summary>
        /// Set a session for whether pan hash credit cards available or not.
        /// </summary>
        public static bool IsPANHashCreditCardsAvailable
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                bool bookingFlag = false;

                if (session.Contents[SessionConstants.IS_PANHASH_CREDITCARDS_AVAILABLE] != null)
                {
                    bookingFlag = (bool)session.Contents[SessionConstants.IS_PANHASH_CREDITCARDS_AVAILABLE];
                }

                return bookingFlag;
            }

            set
            { 
                HttpContext.Current.Session.Add(SessionConstants.IS_PANHASH_CREDITCARDS_AVAILABLE, value); 
            }
        }

        /// <summary>
        /// Tracks whether the nets payment using the stored pan hash credit card or not
        /// </summary>
        public static bool IsEarlyBookingUsingStoredPanHashCC
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                bool bookingFlag = false;

                if (session.Contents[SessionConstants.IS_EARLYBOOKING_USING_STORED_PANHASHCC] != null)
                {
                    bookingFlag = (bool)session.Contents[SessionConstants.IS_EARLYBOOKING_USING_STORED_PANHASHCC];
                }

                return bookingFlag;
            }

            set
            {
                HttpContext.Current.Session.Add(SessionConstants.IS_EARLYBOOKING_USING_STORED_PANHASHCC, value);
            }

        }

        /// <summary>
        /// This is use for checcking for IE6 pop up closed or not.
        /// </summary>
        public static bool IsIE6PopupClosed
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                bool bookingFlag = false;
                if (session.Contents[SessionConstants.ISIE6POPUPCLOSED] != null)
                {
                    bookingFlag = (bool)session.Contents[SessionConstants.ISIE6POPUPCLOSED];
                }
                return bookingFlag;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.ISIE6POPUPCLOSED, value); }
        }

        

        /// <summary>
        /// Gets or sets a value indicating whether this instance is site redirected on ip.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is site redirected on ip; otherwise, <c>false</c>.
        /// </value>
        public static bool IsSiteRedirectedOnIp
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                bool isSiteRedirectedOnIp = false;
                if (session != null && session.Contents[SessionConstants.IS_SITE_REDIRECTED_ON_IP] != null)
                {
                    isSiteRedirectedOnIp = (bool)session.Contents[SessionConstants.IS_SITE_REDIRECTED_ON_IP];
                }
                return isSiteRedirectedOnIp;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_SITE_REDIRECTED_ON_IP, value); }
        }

        #endregion

        

        /// <summary>
        /// Store the Total Amount in the Session
        /// </summary>
        public static string RightCarouselIdx
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.RIGHTCAROUSEL_IDX] as string;
            }
            set { HttpContext.Current.Session[SessionConstants.RIGHTCAROUSEL_IDX] = value; }
        }

        

        /// <summary>
        /// Gets/Sets PolicyTextConfirmation
        /// </summary>
        public static Hashtable PolicyTextConfirmation
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.POLICYTEXTCONFIRMATION] as Hashtable;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.POLICYTEXTCONFIRMATION] = value;
            }
        }


        /// <summary>
        /// Gets or sets the session variable at key IS_XFORM_SUBMITTED
        /// </summary>
        public static bool IsXFormSubmitted
        {
            get
            {
                bool result = false;

                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[SessionConstants.IS_XFORM_SUBMITTED] != null)
                    {
                        object objXFormSubmitted = session.Contents[SessionConstants.IS_XFORM_SUBMITTED];
                        if (objXFormSubmitted != null)
                        {
                            result = (bool)objXFormSubmitted;
                        }
                    }
                }
                return result;
            }
            set
            {
                if (HttpContext.Current.Session.Contents[SessionConstants.IS_XFORM_SUBMITTED] != null)
                    HttpContext.Current.Session[SessionConstants.IS_XFORM_SUBMITTED] = value;
            }
        }

        

        /// <summary>
        /// Gets/Sets IsComboReservation
        /// </summary>
        public static bool IsComboReservation
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object isCombo = session.Contents[SessionConstants.IS_COMBO];
                if (isCombo != null)
                {
                    result = (bool)isCombo;
                }
                return result;
            }
            set { HttpContext.Current.Session[SessionConstants.IS_COMBO] = value; }
        }

        

        /// <summary>
        /// Gets/Sets IsModifySelectRateFromBreadCrumb
        /// </summary>
        public static bool IsModifySelectRateFromBreadCrumb
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object isMCFromBreadCrumb = session.Contents[SessionConstants.IS_MODIFY_SELECT_RATE_FROM_BREADCRUMB];
                if (isMCFromBreadCrumb != null)
                {
                    result = (bool)isMCFromBreadCrumb;
                }
                return result;
            }
            set { HttpContext.Current.Session[SessionConstants.IS_MODIFY_SELECT_RATE_FROM_BREADCRUMB] = value; }
        }

        /// <summary>
        /// Gets/Sets PartnerID
        /// </summary>
        public static string PartnerID
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents["PartnerID"] as string;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents["PartnerID"] = value;
            }
        }
    }
}
