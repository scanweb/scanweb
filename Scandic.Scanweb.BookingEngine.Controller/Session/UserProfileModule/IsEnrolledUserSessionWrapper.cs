﻿using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule
{
    public class IsEnrolledUserSessionWrapper
    {
        #region IsEnrolledUser

        

        /// <summary>
        /// This is to check if the user is enrolled or an exitsing user.
        /// If the user enrolled by hitting Enroll screen, then the value 
        /// for "IsEnrolledUser" set to true, but if an existing user login 
        /// to the system then the value for "IsEnrolledUser" set to false.
        /// </summary>
        public static bool IsEnrolledUser
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                object isEnrolledUSer = session.Contents[SessionConstants.ENROLLED_USER];
                if (isEnrolledUSer != null)
                {
                    return (bool)isEnrolledUSer;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.ENROLLED_USER] = value;
            }
        }

        public static bool IsEnrolledUserFromTopRightButton
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[SessionConstants.IS_USER_ENROLL_FROM_TOPRIGHT_BUTTON] != null)
                    {
                        return ((bool)session.Contents[SessionConstants.IS_USER_ENROLL_FROM_TOPRIGHT_BUTTON]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_USER_ENROLL_FROM_TOPRIGHT_BUTTON, value); }
        }


        public static bool IsEnrolledUserFromScandicFriends
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[SessionConstants.IS_USER_ENROLL_FROM_SCANDIC_FRIENDS] != null)
                    {
                        return ((bool)session.Contents[SessionConstants.IS_USER_ENROLL_FROM_SCANDIC_FRIENDS]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_USER_ENROLL_FROM_SCANDIC_FRIENDS, value); }
        }

        #endregion IsEnrolledUser
    }
}
