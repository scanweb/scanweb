﻿using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule
{
    public class UserLoggedInSessionWrapper
    {
        #region UserLoggedIn

        

        /// <summary>
        /// Returns true if the user is logged in to the system already
        /// else returns false
        /// 
        /// The logic is to check if the LoyaltyDetailsEntity is already in session return true
        /// if the value is not already saved into the session false is returned
        /// </summary>
        public static bool UserLoggedIn
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object loggedInUser = session.Contents[SessionConstants.BE_USER_LOGGED_IN];
                if (loggedInUser != null)
                {
                    result = (bool)loggedInUser;
                }
                return result;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.BE_USER_LOGGED_IN] = value;
            }
        }

        private const string IS_LOGIN_FROM_BOOKING_DETAIL = "IS_LOGIN_FROM_BOOKING_DETAIL";
        public static bool IsLoginFromBookingDetailsPage
        {
            get
            {
                //artf1174300 : Scanweb - Res 2.0 - Application Error persists - added null check :Rajneesh
                if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null)
                {
                    HttpSessionState session = System.Web.HttpContext.Current.Session;
                    if (session.Contents[IS_LOGIN_FROM_BOOKING_DETAIL] != null)
                    {
                        return ((bool)session.Contents[IS_LOGIN_FROM_BOOKING_DETAIL]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set
            {
                System.Web.HttpContext.Current.Session.Add(IS_LOGIN_FROM_BOOKING_DETAIL, value);
            }
        }
        #endregion UserLoggedIn
    }
}
