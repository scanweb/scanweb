﻿using System.Web;
using System.Web.SessionState;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule
{
    public class LoyaltyDetailsSessionWrapper
    {
        #region LoyaltyDetails

        

        /// <summary>
        /// The Loyalty details object containing the user name and the NameID
        /// of the Loyalty user after the user is logged into the system.
        /// This object existence in the session tells whether the user is 
        /// logged into the system or not
        /// </summary>
        public static LoyaltyDetailsEntity LoyaltyDetails
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.LOYALTY_DETAILS] as LoyaltyDetailsEntity;
            }

            set { HttpContext.Current.Session.Add(SessionConstants.LOYALTY_DETAILS, value); }
        }

        /// <summary>
        /// Gets loyalty details.
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static LoyaltyDetailsEntity GetLoyaltyDetails(HttpSessionState session)
        {
            return session.Contents[SessionConstants.LOYALTY_DETAILS] as LoyaltyDetailsEntity;
        }

        #endregion LoyaltyDetails
    }
}
