﻿using System.Web;
using System.Web.SessionState;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule
{
    public class FastTrackEnrolmentSessionWrapper
    {
        #region FastTrackEnrolment

        

        /// <summary>
        /// Gets/Sets fast track enrollment
        /// </summary>
        public static FastTrackEnrolmentEntity FastTrackEnrolment
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[SessionConstants.FASTTRACK_PARAMETERS] == null)
                {
                    FastTrackEnrolmentEntity fastTrackEnrolmentEntity = new FastTrackEnrolmentEntity();
                    HttpContext.Current.Session.Add(SessionConstants.FASTTRACK_PARAMETERS, fastTrackEnrolmentEntity);
                }

                return session.Contents[SessionConstants.FASTTRACK_PARAMETERS] as FastTrackEnrolmentEntity;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.FASTTRACK_PARAMETERS, value); }
        }

        #endregion FastTrackEnrolment
    }
}
