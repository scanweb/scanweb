﻿using System.Web;
using System.Web.SessionState;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule
{
    public class UserProfileInformationSessionWrapper
    {
        #region UserProfileInformation

        

        /// <summary>
        /// The User profile Info object contains the details of the User information
        /// The values are stored in a UserProfileEntity object.
        /// The session object is updated in the code behind of the Enroll user controls
        /// </summary>
        public static UserProfileEntity UserProfileInformation
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.USER_PROFILE_INFO] as UserProfileEntity;
            }
            set { HttpContext.Current.Session[SessionConstants.USER_PROFILE_INFO] = value; }
        }

        #endregion
    }
}
