﻿using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule
{
    public class GuestBookingInformationSessionWrapper
    {
        #region GuestBookingInformation

        

        /// <summary>
        /// The Guest Booking Info object contains the details of the booking information
        /// of the guest.
        /// The values are stored in a GuestInformationEntity object.
        /// The session object is updated in the code behind of the Booking Details user controls
        /// </summary>
        public static GuestInformationEntity GuestBookingInformation
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.GUEST_BOOKING_INFO] as GuestInformationEntity;
            }
            set { HttpContext.Current.Session[SessionConstants.GUEST_BOOKING_INFO] = value; }
        }

        

        /// <summary>
        /// The Guest Booking Info object contains the details of the booking information
        /// of the guest.
        /// The values are stored in a GuestInformationEntity object.
        /// The session object is updated in the code behind of the Booking Details user controls
        /// </summary>
        public static SortedList<string, GuestInformationEntity> AllGuestsBookingInformations
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.ALL_GUESTS_BOOKING_INFO] as SortedList<string, GuestInformationEntity>;
            }
            set { HttpContext.Current.Session[SessionConstants.ALL_GUESTS_BOOKING_INFO] = value; }
        }

        

        /// <summary>
        /// The Guest Info object contains the details of the profile of the of the guest.
        /// The values are stored in a GuestInformationEntity object.
        /// </summary>
        public static GuestInformationEntity FetchedGuestInformation
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.FETCHED_GUEST_INFO] as GuestInformationEntity;
            }
            set { HttpContext.Current.Session[SessionConstants.FETCHED_GUEST_INFO] = value; }
        }

        

        /// <summary>
        /// The Guest Info object contains the details of the profile of the of the guest.
        /// The values are stored in a GuestInformationEntity object.
        /// </summary>
        public static string GuranteeTypeDisplayText
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.GURANTEE_TYPE_DISPLAY_TEXT] as string;
            }
            set { HttpContext.Current.Session[SessionConstants.GURANTEE_TYPE_DISPLAY_TEXT] = value; }
        }

        public static List<GuestInformationEntity> GuestInformationList
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.GUESTINFOLIST] as List<GuestInformationEntity>;
            }
            set { HttpContext.Current.Session[SessionConstants.GUESTINFOLIST] = value; }
        }

        #endregion
    }
}
