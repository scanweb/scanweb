﻿using System;
using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule
{
    public class LoginErrorCodeSessionWrapper
    {
        #region Login Error code

        /// <summary>
        /// This variable is used to show a login error message to the user.
        /// This is designed as a code to give the flexibility of showing different messages based on the code.
        /// </summary>
        

        /// <summary>
        /// Get/Set of Last Visited Page
        /// </summary>
        public static int LoginErrorCode
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                int errorCode = 0;
                if (session.Contents[SessionConstants.LOGIN_ERROR_CODE] != null)
                {
                    Int32.TryParse(session.Contents[SessionConstants.LOGIN_ERROR_CODE].ToString(), out errorCode);
                }
                return errorCode;
            }
            set { HttpContext.Current.Session[SessionConstants.LOGIN_ERROR_CODE] = value; }
        }

        /// <summary>
        /// Get/Set of Last Visited Page
        /// </summary>
        public static int LoginPopupErrorCode
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                int errorCode = 0;
                if (session.Contents[SessionConstants.LOGIN_POPUP_ERROR_CODE] != null)
                {
                    Int32.TryParse(session.Contents[SessionConstants.LOGIN_POPUP_ERROR_CODE].ToString(), out errorCode);
                }
                return errorCode;
            }
            set { HttpContext.Current.Session[SessionConstants.LOGIN_POPUP_ERROR_CODE] = value; }
        }


        #endregion Login Error code
    }
}
