﻿using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule
{
    public class LoginDemographicsSessionWrapper
    {
        #region LoginDemographics

        

        /// <summary>
        /// It holds login control names of different modules from where user can login to the system.
        /// Login Popup / Booking Search / Booking Details / Login Error / Logout Confirmation
        /// </summary>
        public static Dictionary<LoginSourceModule, LoginDemographicEntity> LoginDemographics
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[SessionConstants.LOGIN_DEMOGRAPHICS] != null)
                {
                    return session.Contents[SessionConstants.LOGIN_DEMOGRAPHICS] as Dictionary<LoginSourceModule, LoginDemographicEntity>;
                }
                else
                {
                    return new Dictionary<LoginSourceModule, LoginDemographicEntity>();
                }
            }
            set { HttpContext.Current.Session.Add(SessionConstants.LOGIN_DEMOGRAPHICS, value); }
        }

        #endregion LoginDemographics
    }
}
