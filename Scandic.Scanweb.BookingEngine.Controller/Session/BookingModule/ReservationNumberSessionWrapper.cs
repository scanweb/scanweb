﻿using System;
using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule
{
    public class ReservationNumberSessionWrapper
    {
        #region Reservation Number

        

        /// <summary>
        /// Contains the Booking number for the last booking
        /// </summary>
        public static string ReservationNumber
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[SessionConstants.RESERVATION_NUMBER] != null)
                {
                    return session.Contents[SessionConstants.RESERVATION_NUMBER] as string;
                }
                else
                {
                    return String.Empty;
                }
            }
            set { HttpContext.Current.Session[SessionConstants.RESERVATION_NUMBER] = value; }
        }

        /// <summary>
        /// Checking if reservation is done or not
        ///</summary>
        public static bool IsReservationDone
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object isReservationDone = session.Contents[SessionConstants.IS_RESERVATION_DONE];
                if (isReservationDone != null)
                {
                    result = (bool)isReservationDone;
                }
                return result;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.IS_RESERVATION_DONE] = value;
            }
        }

        /// <summary>
        /// Checking if reservation is done or not
        ///</summary>
        public static bool IsAuthorizationDone
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object isAuthorizationDone = session.Contents[SessionConstants.IS_AUTHORIZATION_DONE];
                if (isAuthorizationDone != null)
                {
                    result = (bool)isAuthorizationDone;
                }
                return result;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.IS_AUTHORIZATION_DONE] = value;
            }
        }

        /// <summary>
        /// Is APPError page triggered from the PaymentProcessing page.
        ///</summary>
        public static bool IsAppErrorPageTriggeredFromThePaymentProcessingPage
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object isAppErrorPageTriggeredFromThePaymentProcessingPage = 
                    session.Contents[SessionConstants.IsAppErrorPageTriggeredFromThePaymentProcessingPage];
                if (isAppErrorPageTriggeredFromThePaymentProcessingPage != null)
                {
                    result = (bool)isAppErrorPageTriggeredFromThePaymentProcessingPage;
                }
                return result;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.IsAppErrorPageTriggeredFromThePaymentProcessingPage] = value;
            }
        }

        /// <summary>
        /// Contains the Booking number for the last booking
        /// </summary>
        public static long TransactionCount
        {
            get
            {
                long result = -1;
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.TRANSACTION_COUNT] != null)
                {
                    object objTranscationCount = session.Contents[SessionConstants.TRANSACTION_COUNT];
                    if (objTranscationCount != null)
                    {
                        result = (long)objTranscationCount;
                    }
                }
                return result;
            }
            set { HttpContext.Current.Session[SessionConstants.TRANSACTION_COUNT] = value; }
        }

        public static bool IsModifyOnlinePaymentMode
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object isModifyOnlinePaymentMode = session.Contents[SessionConstants.IS_MODIFY_ONLINE_PAYMENT_MODE];
                if (isModifyOnlinePaymentMode != null)
                {
                    result = (bool)isModifyOnlinePaymentMode;
                }
                return result;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.IS_MODIFY_ONLINE_PAYMENT_MODE] = value;
            }
        }

        #endregion
    }
}
