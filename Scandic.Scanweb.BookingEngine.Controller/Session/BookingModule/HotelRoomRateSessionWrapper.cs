﻿using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule
{
    public class HotelRoomRateSessionWrapper
    {
        #region HotelRoomRate

        

        /// <summary>
        /// The Hotel Room Rate object containing the details of the rate of
        /// the room selected by the user.
        /// The values are stored in a HotelRoomRateEntity object.
        /// The session object is updated in the code behind of the Select Rate user controls
        /// </summary>
        public static IList<BaseRoomRateDetails> ListHotelRoomRate
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.LIST_HOTEL_RATE] as List<BaseRoomRateDetails>;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.LIST_HOTEL_RATE, value); }
        }

        

        /// <summary>
        /// The Hotel Room Rate object containing the details of the rate of
        /// the room selected by the user.
        /// The values are stored in a HotelRoomRateEntity object.
        /// The session object is updated in the code behind of the Select Rate user controls
        /// </summary>
        public static IList<RoomStay> ListRoomStay
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.LIST_ROOM_STAY] as List<RoomStay>;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.LIST_ROOM_STAY, value); }
        }

        

        /// <summary>
        /// Flag to indicate whether the Total Rate is already calculated for the fetched list of bookings.
        /// </summary>
        public static bool TotalRateCalculated
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                object objTotalRateCalculated = session.Contents[SessionConstants.TOTAL_RATE_CALCULATED];
                if (objTotalRateCalculated != null)
                {
                    return (bool)objTotalRateCalculated;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.TOTAL_RATE_CALCULATED] = value;
            }
        }

        

        /// <summary>
        /// The Hotel Room Rate object containing the details of the rate of
        /// the room selected by the user.
        /// The values are stored in a HotelRoomRateEntity object.
        /// The session object is updated in the code behind of the Select Rate user controls
        /// </summary>
        public static Hashtable SelectedRoomAndRatesHashTable
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.HASH_SELECTED_ROOM_AND_RATES] as Hashtable;
            }
            set { HttpContext.Current.Session[SessionConstants.HASH_SELECTED_ROOM_AND_RATES] = value; }
        }

        

        /// <summary>
        /// Get/Set the Selected Room Category ID
        /// </summary>
        public static string SelectedRateCategoryID
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.SELECTED_RATE_CATEGORTY_ID] as string;
            }
            set { HttpContext.Current.Session[SessionConstants.SELECTED_RATE_CATEGORTY_ID] = value; }
        }

        #endregion

    }
}
