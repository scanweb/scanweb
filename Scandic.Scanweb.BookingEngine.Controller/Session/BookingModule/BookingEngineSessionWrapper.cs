﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule
{
    public class BookingEngineSessionWrapper
    {
        #region Booking Engine

        

        /// <summary>
        /// Get/Set Booking details Entity used during Modify/Cancel Booking
        /// </summary>
        public static List<BookingDetailsEntity> AllBookingDetails
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.ALL_FETCHED_BOOKING_DETAILS] as List<BookingDetailsEntity>;
            }
            set { HttpContext.Current.Session[SessionConstants.ALL_FETCHED_BOOKING_DETAILS] = value; }
        }

        /// <summary>
        /// Method returns the BookingDetails for respective booking from the active booking details
        /// Author: Ruman Khan
        /// </summary>
        /// <param name="legNumber"></param>
        /// <param name="reservationNumber"></param>
        /// <returns></returns>
        public static BookingDetailsEntity GetLegBookingDetails(string legNumber, string reservationNumber)
        {
            BookingDetailsEntity legBookingDetails = null;
            if (AllBookingDetails != null)
            {
                foreach (BookingDetailsEntity booking in AllBookingDetails)
                {
                    if (booking.LegNumber == legNumber && booking.ReservationNumber == reservationNumber)
                    {
                        legBookingDetails = booking;
                    }
                }
            }
            return legBookingDetails;
        }

        /// <summary>
        /// Added Get property to fetch the ActiveBookingDetails derived from the AllBookingDetails
        /// Author: Ruman Khan
        /// </summary>
        public static List<BookingDetailsEntity> ActiveBookingDetails
        {
            get
            {
                List<BookingDetailsEntity> activeList = new List<BookingDetailsEntity>();

                if (AllBookingDetails != null)
                {
                    foreach (BookingDetailsEntity booking in AllBookingDetails)
                    {
                        if (!booking.IsCancelledByUser)
                        {
                            activeList.Add(booking);
                        }
                    }
                }
                return activeList;
            }
        }


        

        /// <summary>
        /// Gets/Sets AvailabilityTable
        /// </summary>
        public static Hashtable AvailabilityTable
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.AVAILABILITY_TABLE] as Hashtable;
            }
            set { HttpContext.Current.Session[SessionConstants.AVAILABILITY_TABLE] = value; }
        }

        /// <summary>
        /// Gets availability table
        /// </summary>
        public static Hashtable GetAvailabilityTable(HttpSessionState session)
        {
            return session.Contents[SessionConstants.AVAILABILITY_TABLE] as Hashtable;
        }

        /// <summary>
        /// Sets availability table
        /// </summary>
        /// <param name="session"></param>
        /// <param name="availabilityTable"></param>
        public static void SetAvailabilityTable(HttpSessionState session, Hashtable availabilityTable)
        {
            session.Add(SessionConstants.AVAILABILITY_TABLE, availabilityTable);
        }

        

        /// <summary>
        /// Get/Set Booking details Entity used during Modify/Cancel Booking
        /// </summary>
        public static List<CancelDetailsEntity> AllCancelledDetails
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.ALL_CANCELLED_DETAILS] as List<CancelDetailsEntity>;
            }
            set { HttpContext.Current.Session[SessionConstants.ALL_CANCELLED_DETAILS] = value; }
        }

        

        /// <summary>
        /// Get/Set Booking details Entity used during Modify/Cancel Booking
        /// </summary>
        public static BookingDetailsEntity BookingDetails
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.FETCHED_BOOKING_DETAILS] as BookingDetailsEntity;
            }
            set { HttpContext.Current.Session[SessionConstants.FETCHED_BOOKING_DETAILS] = value; }
        }

        

        /// <summary>
        /// Session Variable to indicate as to Enable/Disable Bed type Preference
        /// </summary>
        public static bool DirectlyModifyContactDetails
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objenableBedPreference = session.Contents[SessionConstants.ENABLED_BED_PREFERENCE];
                if (objenableBedPreference != null)
                {
                    result = (bool)objenableBedPreference;
                }
                return result;
            }
            set { HttpContext.Current.Session[SessionConstants.ENABLED_BED_PREFERENCE] = value; }
        }

        

        /// <summary>
        /// Session Variable to indicate the Modify/Cancel flow        
        /// </summary>
        public static bool IsModifyBooking
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objBookingModifiable = session.Contents[SessionConstants.IS_BOOKING_MODIFIABLE];
                if (objBookingModifiable != null)
                {
                    result = (bool)objBookingModifiable;
                }
                return result;
            }
            set { HttpContext.Current.Session[SessionConstants.IS_BOOKING_MODIFIABLE] = value; }
        }

        public static bool IsPrepaidBlockBooking
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objBookingModifiable = session.Contents[SessionConstants.IS_PREPAID_BLOCK];
                if (objBookingModifiable != null)
                {
                    result = (bool)objBookingModifiable;
                }
                return result;
            }
            set { HttpContext.Current.Session[SessionConstants.IS_PREPAID_BLOCK] = value; }
        }

        

        /// <summary>
        /// Session Variable to indicate the Modify Check box in checked in booking details page;
        /// this is to implement the Update credit card only when the user have checked the Modify check box.
        /// </summary>
        /// <remarks>
        /// Do not update profile with the Credit Card information, if the user has not opted for 'Update Profile' option
        /// </remarks>
        public static bool UpdateProfileBookingInfo
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objUpdateProfile = session.Contents[SessionConstants.IS_UPDATEPROFILE_BOOKINGINFO];
                if (objUpdateProfile != null)
                {
                    result = (bool)objUpdateProfile;
                }
                return result;
            }
            set { HttpContext.Current.Session[SessionConstants.IS_UPDATEPROFILE_BOOKINGINFO] = value; }
        }

        

        /// <summary>
        /// Session Variable to store the Reservation Cancellation Number
        /// </summary>
        public static string ReservationCancellationNumber
        {
            get
            {
                string cancellatinNumber = String.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object objCancellationNumber = session.Contents[SessionConstants.RESERVATION_CANCELLATION_NUMBER];
                if (objCancellationNumber != null)
                {
                    cancellatinNumber = objCancellationNumber as string;
                }
                return cancellatinNumber;
            }
            set { HttpContext.Current.Session[SessionConstants.RESERVATION_CANCELLATION_NUMBER] = value; }
        }

        

        /// <summary>
        /// Session Variable to store the flag as whether SMS Confirmation is required
        /// </summary>
        public static bool IsSmsRequired
        {
            get
            {
                bool isSMSRequired = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objIsSmsRequired = session.Contents[SessionConstants.IS_SMS_REQUIRED];
                if (objIsSmsRequired != null)
                {
                    isSMSRequired = (bool)objIsSmsRequired;
                }
                return isSMSRequired;
            }
            set { HttpContext.Current.Session[SessionConstants.IS_SMS_REQUIRED] = value; }
        }

        

        /// <summary>
        /// Get/Set the Previous Page URL
        /// </summary>
        public static string PreviousPageURL
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.PREVIOUS_PAGE_URL] as string;
            }
            set { HttpContext.Current.Session[SessionConstants.PREVIOUS_PAGE_URL] = value; }
        }

        

        /// <summary>
        /// Session Variable to store the flag as whether the user is currently modifying a particular leg of a booking.
        /// </summary>
        public static bool IsModifyingLegBooking
        {
            get
            {
                bool isModifyingLegBooking = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objIsModifyingLegBooking = session.Contents[SessionConstants.MODIFYING_LEG_BOOKING];
                if (objIsModifyingLegBooking != null)
                {
                    isModifyingLegBooking = (bool)objIsModifyingLegBooking;
                }
                return isModifyingLegBooking;
            }
            set { HttpContext.Current.Session[SessionConstants.MODIFYING_LEG_BOOKING] = value; }
        }

        /// <summary>
        /// Gets/Sets IsModifyComboBooking
        /// </summary>
        public static bool IsModifyComboBooking
        {
            get
            {
                bool isModifyComboBooking = false;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null) &&
                    (HttpContext.Current.Session.Contents[SessionConstants.IS_MODIFY_COMBO_BOOKING] != null))
                {
                    isModifyComboBooking =
                        (bool) HttpContext.Current.Session.Contents[SessionConstants.IS_MODIFY_COMBO_BOOKING];
                }
                return isModifyComboBooking;
            }
            set
            {
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    HttpContext.Current.Session[SessionConstants.IS_MODIFY_COMBO_BOOKING] = value;
                }
            }
        }

        private const string HASH_SELECTED_ROOM_AND_RATES = "BE_HASH_SELECTED_ROOM_AND_RATES";
        /// <summary>
        /// The Hotel Room Rate object containing the details of the rate of
        /// the room selected by the user.
        /// The values are stored in a HotelRoomRateEntity object.
        /// The session object is updated in the code behind of the Select Rate user controls
        /// </summary>
        public static Hashtable SelectedRoomAndRatesHashTable
        {
            get
            {
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                return session.Contents[HASH_SELECTED_ROOM_AND_RATES] as Hashtable;
            }
            set
            {
                System.Web.HttpContext.Current.Session[HASH_SELECTED_ROOM_AND_RATES] = value;
            }
        }

        public static bool HideARBPrice
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                object hideARBPrice = session.Contents["HideARBPrice"];
                if (hideARBPrice != null)
                {
                    return (bool)hideARBPrice;
                }
                return false;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents["HideARBPrice"] = value;
            }
        }
        public static string IATAProfileCode
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.IATA_PROFILE_CODE] as string;
            }
            set { HttpContext.Current.Session[SessionConstants.IATA_PROFILE_CODE] = value; }
        }
        public static string BookingCodeIATA
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.BOOKING_CODE_IATA] as string;
            }
            set { HttpContext.Current.Session[SessionConstants.BOOKING_CODE_IATA] = value; }
        }
        #endregion
    }
}
