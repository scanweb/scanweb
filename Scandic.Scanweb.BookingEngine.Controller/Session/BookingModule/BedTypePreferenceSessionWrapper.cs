﻿using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule
{
    public class BedTypePreferenceSessionWrapper
    {
        #region BedTypePreference

        

        /// <summary>
        /// Conatin information about the user Bed Type Preference selected during Booking detail 
        /// </summary>
        public static string UserBedTypePreference
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.BCD_BED_TYPE_PREFERENCE] as string;
            }

            set { HttpContext.Current.Session[SessionConstants.BCD_BED_TYPE_PREFERENCE] = value; }
        }

        #endregion BedTypePreference
    }
}
