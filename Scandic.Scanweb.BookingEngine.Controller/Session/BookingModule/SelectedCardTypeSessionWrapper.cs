﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule
{
    public class SelectedCardTypeSessionWrapper
    {
        #region Selected Card Type


        /// <summary>
        /// Get/Set the selected card type
        /// </summary>
        public static string SelectedCardType
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[SessionConstants.SELECTED_CARD_TYPE] != null)
                {
                    return session.Contents[SessionConstants.SELECTED_CARD_TYPE] as string;
                }
                else
                {
                    return String.Empty;
                }
            }
            set { HttpContext.Current.Session[SessionConstants.SELECTED_CARD_TYPE] = value; }
        }

        #endregion
    }
}
