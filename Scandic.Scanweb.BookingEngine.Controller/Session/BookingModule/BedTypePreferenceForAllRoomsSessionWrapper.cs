﻿using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule
{
    public class BedTypePreferenceForAllRoomsSessionWrapper
    {
        #region BedTypePreferenceForAllRooms

        

        /// <summary>
        /// Conatin information about the user Bed Type Preference selected during Booking detail 
        /// </summary>
        public static List<string> UserBedTypePreferenceForAllRooms
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.BCD_BED_TYPE_PREFERENCE_FOR_ALL_ROOMS] as List<string>;
            }

            set { HttpContext.Current.Session[SessionConstants.BCD_BED_TYPE_PREFERENCE_FOR_ALL_ROOMS] = value; }
        }

        #endregion BedTypePreferenceForAllRooms
    }
}
