﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using Scandic.Scanweb.BookingEngine.Controller.Entity;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule
{
    public class Reservation2SessionWrapper
    {
        #region Reservation 2.0

        

        /// <summary>
        /// Holds which tab is selected in select hotel page. Bydefault it is list view.
        /// </summary>
        public static string SelectedTabInSelectHotel
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                //Default value of tab in select hotel.
                string bookingFlag = "listViewTab|.listView";
                if (session.Contents[SessionConstants.SELECTHOTELTAB] != null)
                {
                    bookingFlag = session.Contents[SessionConstants.SELECTHOTELTAB] as string;
                }
                return bookingFlag;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.SELECTHOTELTAB, value); }
        }


        

        /// <summary>
        /// Holds which tab is selected in select hotel page. Bydefault it is list view.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is per stay selected in select rate page; otherwise, <c>false</c>.
        /// </value>
        public static bool IsPerStaySelectedInSelectRatePage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                bool isPerStaySelectedInSelectRatePage = false;
                if (session.Contents[SessionConstants.SHOWPRICEPERSTAYFORSELECTRATEPAGE] != null)
                {
                    isPerStaySelectedInSelectRatePage =
                        Convert.ToBoolean(session.Contents[SessionConstants.SHOWPRICEPERSTAYFORSELECTRATEPAGE]);
                }
                return isPerStaySelectedInSelectRatePage;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.SHOWPRICEPERSTAYFORSELECTRATEPAGE, value); }
        }

        

        public static AvailabilityCalendarEntity AvailabilityCalendar
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.AVAILABILITY_CALENDAR] as AvailabilityCalendarEntity;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.AVAILABILITY_CALENDAR, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        

        /// <summary>
        /// Gets or sets the avail calendar hotel details list.
        /// </summary>
        /// <value>The avail calendar hotel details list.</value>
        public static IList<IHotelDetails> AvailCalendarHotelDetailsList
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.AVAILCALENDAR_HOTELDETAILS_LIST] as IList<IHotelDetails>;
            }

            set { HttpContext.Current.Session.Add(SessionConstants.AVAILCALENDAR_HOTELDETAILS_LIST, value); }
        }

        /// <summary>
        /// Gets the avail calendar hotel details list.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <returns></returns>
        public static IList<IHotelDetails> GetAvailCalendarHotelDetailsList(HttpSessionState session)
        {
            return session.Contents[SessionConstants.AVAILCALENDAR_HOTELDETAILS_LIST] as IList<IHotelDetails>;
        }

        /// <summary>
        /// Sets the avail calendar hotel details list.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <param name="hotelDetailsList">The hotel details list.</param>
        public static void SetAvailCalendarHotelDetailsList(HttpSessionState session,
                                                            IList<IHotelDetails> hotelDetailsList)
        {
            session.Add(SessionConstants.AVAILCALENDAR_HOTELDETAILS_LIST, hotelDetailsList);
        }

        

        public static int SelectedCalendarItemIndex
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.SELECTED_CALENDAR_ITEM_INDEX] != null)
                {
                    return ((int)session.Contents[SessionConstants.SELECTED_CALENDAR_ITEM_INDEX]);
                }
                else
                {
                    return 0;
                }
            }
            set { HttpContext.Current.Session.Add(SessionConstants.SELECTED_CALENDAR_ITEM_INDEX, value); }
        }

        

        public static bool IsUserEnrollInBookingModule
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[SessionConstants.IS_USER_ENROLL_IN_BOOKINGMODULE] != null)
                    {
                        return ((bool)session.Contents[SessionConstants.IS_USER_ENROLL_IN_BOOKINGMODULE]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_USER_ENROLL_IN_BOOKINGMODULE, value); }
        }

        public static bool IsUserClickTopRightButtonForRegistration
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[SessionConstants.IS_USER_CLICK_TOPRIGHT_BUTTON_FOR_REGISTRATION] != null)
                    {
                        return ((bool)session.Contents[SessionConstants.IS_USER_CLICK_TOPRIGHT_BUTTON_FOR_REGISTRATION]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_USER_CLICK_TOPRIGHT_BUTTON_FOR_REGISTRATION, value); }
        }


        public static bool IsUserEnrollFromScandicFriends1
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[SessionConstants.IS_USER_ENROLL_FROM_SCANDIC_FRIENDS] != null)
                    {
                        return ((bool)session.Contents[SessionConstants.IS_USER_ENROLL_FROM_SCANDIC_FRIENDS]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_USER_ENROLL_FROM_SCANDIC_FRIENDS, value); }
        }

        /// <summary>
        /// Gets or sets the selected calender item rate value status.
        /// </summary>
        /// <value>The selected calender item rate value status.</value>
        public static string SelectedCalenderItemRateValueStatus
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.SELECTED_CALENDAR_ITEM_RATE_VALUE_STATUS] != null)
                {
                    return ((string)session.Contents[SessionConstants.SELECTED_CALENDAR_ITEM_RATE_VALUE_STATUS]);
                }
                else
                {
                    return String.Empty;
                }
            }
            set { HttpContext.Current.Session.Add(SessionConstants.SELECTED_CALENDAR_ITEM_RATE_VALUE_STATUS, value); }
        }

        

        public static int LeftVisibleCarouselIndex
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.LEFT_VISIBLE_CAROUSEL_INDEX] != null)
                {
                    return ((Int32)session.Contents[SessionConstants.LEFT_VISIBLE_CAROUSEL_INDEX]);
                }
                else
                {
                    return 0;
                }
            }
            set { HttpContext.Current.Session.Add(SessionConstants.LEFT_VISIBLE_CAROUSEL_INDEX, value); }
        }

        /// <summary>
        /// Showing Reward Night Accordian After Login
        /// </summary>
        

        /// <summary>
        /// Use for getting the proper bool value to justify that the flow is comming from BookingModuleBig after login
        /// </summary>
        /// true or fale
        public static bool BookingModuleActiveTab
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object bookingModuleActiveTab = session.Contents[SessionConstants.BOOKING_MODULE_ACTIVE_TAB];
                if (bookingModuleActiveTab != null)
                {
                    result = (bool)bookingModuleActiveTab;
                }
                return result;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.BOOKING_MODULE_ACTIVE_TAB] = value;
            }
        }

        

        public static bool IsCalendarFirstBatch
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.IS_CALENDAR_FIRST_BATCH] != null)
                {
                    return ((bool)session.Contents[SessionConstants.IS_CALENDAR_FIRST_BATCH]);
                }
                else
                {
                    return false;
                }
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_CALENDAR_FIRST_BATCH, value); }
        }

        

        public static bool IsCalendarLastBatch
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.IS_CALENDAR_LAST_BATCH] != null)
                {
                    return ((bool)session.Contents[SessionConstants.IS_CALENDAR_LAST_BATCH]);
                }
                else
                {
                    return false;
                }
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_CALENDAR_LAST_BATCH, value); }
        }

        

        public static bool IsLoginFromBookingDetailsPage
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[SessionConstants.IS_LOGIN_FROM_BOOKING_DETAIL] != null)
                    {
                        return ((bool)session.Contents[SessionConstants.IS_LOGIN_FROM_BOOKING_DETAIL]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_LOGIN_FROM_BOOKING_DETAIL, value); }
        }

        

        /// <summary>
        /// Gets/Sets IsLoginFromUpperRight
        /// </summary>
        public static bool IsLoginFromUpperRight
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[SessionConstants.IS_LOGIN_FROM_UPPERRGHT] != null)
                    {
                        return ((bool)session.Contents[SessionConstants.IS_LOGIN_FROM_UPPERRGHT]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_LOGIN_FROM_UPPERRGHT, value); }
        }

        /// <summary>
        /// Gets/Sets IsLoginFromUpperRight
        /// </summary>
        public static bool IsLoginFromFGP
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[SessionConstants.IS_LOGIN_FROM_FGP] != null)
                    {
                        return ((bool)session.Contents[SessionConstants.IS_LOGIN_FROM_FGP]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_LOGIN_FROM_FGP, value); }
        }

        /// <summary>
        /// Gets/Sets IsLoginFromBookingPageLoginOrTopLogin
        /// </summary>
        public static bool IsLoginFromBookingPageLoginOrTopLogin
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.IS_LOGIN_FROM_BOOKINGDETAILSPAGE] != null)
                {
                    return ((bool)session.Contents[SessionConstants.IS_LOGIN_FROM_BOOKINGDETAILSPAGE]);
                }
                else
                {
                    return false;
                }
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_LOGIN_FROM_BOOKINGDETAILSPAGE, value); }
        }

        

        /// <summary>
        /// Gets/Sets CurrentLanguage
        /// </summary>
        public static string CurrentLanguage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.CUR_LANG] == null ? null : session.Contents[SessionConstants.CUR_LANG].ToString();
            }
            set { HttpContext.Current.Session.Add(SessionConstants.CUR_LANG, value); }
        }

        

        /// <summary>
        /// Gets/Sets GuaranteeInfoRate
        /// </summary>
        public static System.Collections.Generic.Dictionary<string, string> GuaranteeInfoRate
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.Guarantee_Info_Rate] as System.Collections.Generic.Dictionary<string, string>;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.Guarantee_Info_Rate, value); }
        }

        

        /// <summary>
        /// Gets or sets the About our rate heading from select rate page.
        /// </summary>
        /// <value>The About our rate heading from select rate page</value>
        public static string AboutOurRateHeading
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.ABOUTOURHEADING] != null)
                {
                    return ((string)session.Contents[SessionConstants.ABOUTOURHEADING]);
                }
                else
                {
                    return String.Empty;
                }
            }
            set { HttpContext.Current.Session.Add(SessionConstants.ABOUTOURHEADING, value); }
        }

        

        /// <summary>
        /// Gets or sets the About our rate Description from select rate page.
        /// </summary>
        /// <value>The About our rate Description from select rate page</value>
        public static string AboutOurRateDescription
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.ABOUTOURDESCRIPTION] != null)
                {
                    return ((string)session.Contents[SessionConstants.ABOUTOURDESCRIPTION]);
                }
                else
                {
                    return String.Empty;
                }
            }
            set { HttpContext.Current.Session.Add(SessionConstants.ABOUTOURDESCRIPTION, value); }
        }

        

        /// <summary>
        /// Session Variable to indicate the Modify/Cancel flow        
        /// </summary>
        public static bool IsModifyFlow
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objBookingModifiable = session.Contents[SessionConstants.IS_MODIFY_FLOW];
                if (objBookingModifiable != null)
                {
                    result = (bool)objBookingModifiable;
                }
                return result;
            }
            set { HttpContext.Current.Session[SessionConstants.IS_MODIFY_FLOW] = value; }
        }

        

        /// <summary>
        /// Gets/Sets MinRateCategoryName
        /// </summary>
        public static string MinRateCategoryName
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.MINRATECATEGORYNAME] != null)
                {
                    return ((string)session.Contents[SessionConstants.MINRATECATEGORYNAME]);
                }
                else
                {
                    return String.Empty;
                }
            }
            set { HttpContext.Current.Session[SessionConstants.MINRATECATEGORYNAME] = value; }
        }

        

        /// <summary>
        /// Gets/Sets IsEditYourStaySearchTriggered
        /// </summary>
        public static bool IsEditYourStaySearchTriggered
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[SessionConstants.ISEDITYOURSTAYSEARCHTRIGGERED] != null)
                    {
                        return ((bool)session.Contents[SessionConstants.ISEDITYOURSTAYSEARCHTRIGGERED]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set
            {
                if (HttpContext.Current.Session.Contents[SessionConstants.ISEDITYOURSTAYSEARCHTRIGGERED] != null)
                    HttpContext.Current.Session[SessionConstants.ISEDITYOURSTAYSEARCHTRIGGERED] = value;
            }
        }

        

        /// <summary>
        /// Gets/Sets IsCitySortDoneInSelectHotel
        /// </summary>
        public static bool IsCitySortDoneInSelectHotel
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.ISCITYSORTDONEINSELECTHOTEL] != null)
                {
                    return ((bool)session.Contents[SessionConstants.ISCITYSORTDONEINSELECTHOTEL]);
                }
                else
                {
                    return false;
                }
            }
            set { HttpContext.Current.Session[SessionConstants.ISCITYSORTDONEINSELECTHOTEL] = value; }
        }

        

        /// <summary>
        /// Gets/Sets SearchedCityName
        /// </summary>
        public static string SearchedCityName
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.SEARCHEDCITYNAME] != null)
                {
                    return ((string)session.Contents[SessionConstants.SEARCHEDCITYNAME]);
                }
                else
                {
                    return String.Empty;
                }
            }
            set { HttpContext.Current.Session[SessionConstants.SEARCHEDCITYNAME] = value; }
        }

        #region AvailabilityCalendarAccessed
        private const string AVAILABILITY_CALENDAR_ACCESSED = "AvailabilityCalendarAccessed";
        /// <summary>
        /// Conatin information about the user Accessed(btnOnNextCalendarRange_Click)the Rate Calendar 
        /// in the Select Rate & Modify Select Rate Page
        /// </summary>
        public static bool AvailabilityCalendarAccessed
        {
            get
            {
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                bool isAvailabilityCalendarAccessed = false;
                if (session.Contents[AVAILABILITY_CALENDAR_ACCESSED] != null)
                {
                    isAvailabilityCalendarAccessed = Convert.ToBoolean(session.Contents[AVAILABILITY_CALENDAR_ACCESSED]);
                }
                return isAvailabilityCalendarAccessed;
            }

            set
            {
                System.Web.HttpContext.Current.Session.Add(AVAILABILITY_CALENDAR_ACCESSED, value);
            }
        }
        #endregion AvailabilityCalendarAccessed

       public static bool IsCurrentBookingsPage
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object currentBookingPage = session.Contents[SessionConstants.CURRENT_BOOKING_PAGE];
                if (currentBookingPage != null)
                {
                    result = (bool)currentBookingPage;
                }
                return result;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.CURRENT_BOOKING_PAGE] = value;
            }
        }

        public static CurrentBookingSummary CurrentBookingsSummary
        {
            get
            {
                CurrentBookingSummary result = null;
                HttpSessionState session = HttpContext.Current.Session;
                object currentBookingSummary = session.Contents[SessionConstants.CURRENT_BOOKING_SUMMARY];
                if (currentBookingSummary != null)
                {
                    result = (CurrentBookingSummary)currentBookingSummary;
                }
                return result;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[SessionConstants.CURRENT_BOOKING_SUMMARY] = value;
            }
        }

 /// <summary>
        /// Gets/Sets PaymentGuestInformation
        /// </summary>
        public static List<GuestInformationEntity>  PaymentGuestInformation
        {
            set
            {
                HttpContext.Current.Session.Add(SessionConstants.PAYMENT_GUESTINFORMATION, value);
            }
            get
            {
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                List<GuestInformationEntity> guestInformationEntities = null;
                if (session.Contents[SessionConstants.PAYMENT_GUESTINFORMATION] != null)
                {
                    guestInformationEntities = (List<GuestInformationEntity>)session.Contents[SessionConstants.PAYMENT_GUESTINFORMATION];
                }
                return guestInformationEntities;
            }
        }

        /// <summary>
        /// Gets/Sets PaymentHotelRoomRateInformation
        /// </summary>
        public static List<HotelRoomRateEntity> PaymentHotelRoomRateInformation
        {
            set
            {
                HttpContext.Current.Session.Add(SessionConstants.PAYMENT_HOTELROOMRATEINFORMATION, value);
            }
            get
            {
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                List<HotelRoomRateEntity> hotelRoomRateEntities = null;
                if (session.Contents[SessionConstants.PAYMENT_HOTELROOMRATEINFORMATION] != null)
                {
                    hotelRoomRateEntities = (List<HotelRoomRateEntity>)session.Contents[SessionConstants.PAYMENT_HOTELROOMRATEINFORMATION];
                }
                return hotelRoomRateEntities;
            }
        }

        /// <summary>
        /// Gets/Sets PaymentIsSessionBooking
        /// </summary>
        public static bool PaymentIsSessionBooking
        {
            set
            {
                HttpContext.Current.Session.Add(SessionConstants.PAYMENT_ISSESSIONBOOKING, value);
            }
            get
            {
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                bool isSessionBooking = false;
                if (session.Contents[SessionConstants.PAYMENT_ISSESSIONBOOKING] != null)
                {
                    isSessionBooking = Convert.ToBoolean(session.Contents[SessionConstants.PAYMENT_ISSESSIONBOOKING]);
                }
                return isSessionBooking;
            }
        }

        /// <summary>
        /// Gets/Sets PaymentTransactionId
        /// </summary>
        public static string PaymentTransactionId
        {
            set
            {
                HttpContext.Current.Session.Add(SessionConstants.PAYMENT_TRANSACTIONID, value);
            }
            get
            {
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                string transactionId = string.Empty;
                if (session.Contents[SessionConstants.PAYMENT_TRANSACTIONID] != null)
                {
                    transactionId = session.Contents[SessionConstants.PAYMENT_TRANSACTIONID].ToString();
                }
                return transactionId;
            }
        }

        /// <summary>
        /// Gets/Sets PaymentOrderAmount
        /// </summary>
        public static string PaymentOrderAmount
        {
            set
            {
                HttpContext.Current.Session.Add(SessionConstants.PAYMENT_ORDERAMOUNT, value);
            }
            get
            {
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                string orderAmount = string.Empty;
                if (session.Contents[SessionConstants.PAYMENT_ORDERAMOUNT] != null)
                {
                    orderAmount = session.Contents[SessionConstants.PAYMENT_ORDERAMOUNT].ToString();
                }
                return orderAmount;
            }
        }

        /// <summary>
        /// Sets/Gets PanHashCreditCard
        /// </summary>
        public static CreditCardEntity PanHashCreditCard
        {
            set
            {
                HttpContext.Current.Session.Add(SessionConstants.PANHASH_CREDITCARD, value);
            }
            get
            {
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                CreditCardEntity panHashCreditCard = null;
                if (session.Contents[SessionConstants.PANHASH_CREDITCARD] != null)
                {
                    panHashCreditCard = session.Contents[SessionConstants.PANHASH_CREDITCARD] as CreditCardEntity;
                }
                return panHashCreditCard;
            }
        }

        /// <summary>
        /// Sets/Gets NetsPaymentErrorMessage
        /// </summary>
        public static string NetsPaymentErrorMessage
        {
            set
            {
                HttpContext.Current.Session.Add(SessionConstants.NETS_PAYMENT_ERROR_MESSAGE, value);
            }
            get
            {
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                string errorMessage = string.Empty;
                if (session.Contents[SessionConstants.NETS_PAYMENT_ERROR_MESSAGE] != null)
                {
                    errorMessage = session.Contents[SessionConstants.NETS_PAYMENT_ERROR_MESSAGE].ToString();
                }
                return errorMessage;
            }
        }

        /// <summary>
        /// Sets/Gets NetsPaymentInfoMessage
        /// </summary>
        public static string NetsPaymentInfoMessage
        {
            set
            {
                HttpContext.Current.Session.Add(SessionConstants.NETS_PAYMENT_INFO_MESSAGE, value);
            }
            get
            {
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                string infoMessage = string.Empty;
                if (session.Contents[SessionConstants.NETS_PAYMENT_INFO_MESSAGE] != null)
                {
                    infoMessage = session.Contents[SessionConstants.NETS_PAYMENT_INFO_MESSAGE].ToString();
                }
                return infoMessage;
            }
        }
        #endregion
    }
}
