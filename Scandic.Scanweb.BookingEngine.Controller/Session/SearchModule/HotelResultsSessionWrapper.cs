﻿using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule
{
    public class HotelResultsSessionWrapper
    {
        #region HotelResults

        


        /// <summary>
        /// The List of hotels avialble for the criteria user has searched for
        /// The Session object is updated in the Controller as the hotels
        /// are returned from the General Availabilty searches which are called
        /// through multithreading Thread pool
        /// </summary>
        public static List<IHotelDetails> HotelResults
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.SEARCH_RESULTS] as List<IHotelDetails>;
            }

            set { HttpContext.Current.Session.Add(SessionConstants.SEARCH_RESULTS, value); }
        }

        /// <summary>
        /// Gets hotel results
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static List<IHotelDetails> GetHotelResults(HttpSessionState session)
        {
            return session.Contents[SessionConstants.SEARCH_RESULTS] as List<IHotelDetails>;
        }

        /// <summary>
        /// Sets hotel results
        /// </summary>
        /// <param name="session"></param>
        /// <param name="hotels"></param>
        public static void SetHotelResults(HttpSessionState session, List<IHotelDetails> hotels)
        {
            session.Add(SessionConstants.SEARCH_RESULTS, hotels);
        }

        //Merchandising:R3:Display ordinary rates for unavailable promo rates 
        public static IHotelDetails HotelDetails
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.DISPLAY_HOTEL_DETAILS] as IHotelDetails;
            }

            set { HttpContext.Current.Session.Add(SessionConstants.DISPLAY_HOTEL_DETAILS, value); }
        }

        /// <summary>
        /// This property identifies if the booking code is valid for city or not
        /// </summary>
        public static bool IsPromoNotValidForCity
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.IS_PROMO_NOT_VALID_FOR_CITY] != null)
                {
                    result = (bool)session.Contents[SessionConstants.IS_PROMO_NOT_VALID_FOR_CITY];
                }
                return result;
            }
            set { HttpContext.Current.Session[SessionConstants.IS_PROMO_NOT_VALID_FOR_CITY] = value; }
        }

        #endregion
    }
}
