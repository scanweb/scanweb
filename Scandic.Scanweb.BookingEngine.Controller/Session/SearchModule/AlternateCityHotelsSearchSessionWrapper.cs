﻿using System.Web;
using System.Web.SessionState;
using Scandic.Scanweb.Core;
using System.Collections.Generic;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule
{
    public class AlternateCityHotelsSearchSessionWrapper
    {
        #region AlternativeCityHotelsSearch

        

        /// <summary>
        /// The Hotel Room Rate object containing the details of the rate of
        /// the room selected by the user.
        /// The values are stored in a HotelRoomRateEntity object.
        /// The session object is updated in the code behind of the Select Rate user controls
        /// </summary>
        public static bool AltCityHotelsSearchDone
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object altCityHotelsSearchDone = session.Contents[SessionConstants.ALT_CITY_HOTELS_SEARCH_DONE];
                if (altCityHotelsSearchDone != null)
                {
                    result = (bool)altCityHotelsSearchDone;
                }
                return result;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.ALT_CITY_HOTELS_SEARCH_DONE, value); }
        }
        //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
        public static bool IsDestinationAlternateFlow
        {
            get
            {
                bool result = false;
                result = HttpContext.Current.Session.Contents[SessionConstants.IS_DESTINATION_ALTERNATE_FLOW] != null &&
               (bool)HttpContext.Current.Session.Contents[SessionConstants.IS_DESTINATION_ALTERNATE_FLOW];
                return result;
            }
            set
            {
                System.Web.HttpContext.Current.Session.Add(SessionConstants.IS_DESTINATION_ALTERNATE_FLOW, value);
            }
        }
        public static int TotalRegionalAltCityHotelCount
        {
            get
            {
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                if (session.Contents[SessionConstants.TOTAL_REGIONAL_ALT_CITY_HOTEL_COUNT] != null)
                {
                    return ((int)session.Contents[SessionConstants.TOTAL_REGIONAL_ALT_CITY_HOTEL_COUNT]);
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                System.Web.HttpContext.Current.Session.Add(SessionConstants.TOTAL_REGIONAL_ALT_CITY_HOTEL_COUNT, value);
            }
        }
        public static List<HotelDestination> CityAltHotelDetails
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.CITY_ALT_DISPLAY_HOTEL_DETAILS] as List<HotelDestination>;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.CITY_ALT_DISPLAY_HOTEL_DETAILS, value); }
        }

        #endregion
    }
}
