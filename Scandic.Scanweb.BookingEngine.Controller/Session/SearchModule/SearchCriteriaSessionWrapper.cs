﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule
{
    public class SearchCriteriaSessionWrapper
    {
        #region SearchCriteria

        

        /// <summary>
        /// The Hotel Search Criteria object containing the details
        /// the user has searched for.
        /// The values are stored in a HotelSearchEntity object.
        /// The session object is updated in the code behind of the search user controls
        /// </summary>
        public static HotelSearchEntity SearchCriteria
        {
            get
            {
                HotelSearchEntity searchCriteria = null;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    searchCriteria = (session != null)
                                         ? session.Contents[SessionConstants.SEARCH_CRITERIA] as HotelSearchEntity
                                         : null;
                }
                return searchCriteria;
            }
            set
            {
                HttpContext.Current.Session.Add(SessionConstants.SEARCH_CRITERIA, value);
                if (value != null)
                {
                    HotelSearchEntity cloneHotelSearch = value.Clone();
                    if (cloneHotelSearch != null)
                    {
                        CloneSearchCriteria = cloneHotelSearch;
                    }
                }
            }
        }

        

        /// <summary>
        /// Gets/Sets UnblockRoomList
        /// </summary>
        public static System.Collections.Specialized.NameValueCollection UnblockRoomList
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.UNBLOCK_ROOMLIST] as System.Collections.Specialized.NameValueCollection;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.UNBLOCK_ROOMLIST, value); }
        }

        

        /// <summary>
        /// ********************************************************************
        /// IMPORTANT:         
        /// ==========
        /// This Session variable NOT to be used any where in Scanweb. 
        /// This is entirely for the use of SOGETI for Site Catalyst.
        /// ********************************************************************
        /// The Hotel Search Criteria object containing the details
        /// the user has searched for.
        /// The values are stored in a HotelSearchEntity object.
        /// The session object is updated in the code behind of the search user controls
        /// </summary>
        public static HotelSearchEntity CloneSearchCriteria
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.CLONE_SEARCH_CRITERIA] as HotelSearchEntity;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.CLONE_SEARCH_CRITERIA, value); }
        }

        

        /// <summary>        
        /// This Session variable is used for keeping a deep copy of Selected Room Rate Entity
        /// which is used in Booking Confirmation page.
        /// </summary>
        public static SelectedRoomAndRateEntity CloneSelectedRoomAndRateEntity
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.CLONE_SELECTED_ROOM_AND_RATE] as SelectedRoomAndRateEntity;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.CLONE_SELECTED_ROOM_AND_RATE, value); }
        }
	
		/// <summary>
        /// Gets/Sets IsPromocodeInvalidForHotel
        /// </summary>
        public static bool IsPromocodeInvalidForHotel
        {
                
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object isPromoValid = session.Contents[SessionConstants.PROMOCODE_INVALID_FOR_HOTEL];
                if (isPromoValid != null)
                {
                    result = (bool)isPromoValid;
                }
                return result;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.PROMOCODE_INVALID_FOR_HOTEL, value); }
        }

        #endregion SearchCriteria
    }
}
