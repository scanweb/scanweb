﻿using System;
using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule
{
    public class TotalRegionalAvailableHotelsSessionWrapper
    {
        #region TotalRegionalAvailableHotels

        

        /// <summary>
        /// This property will contain the total of the number of hotels returned
        /// by the Regional availability search done. 
        /// 
        /// This value will be compared against the total number of General Availability
        /// search results done dynamically. To check if the browser has to do
        /// request for further hotels
        /// </summary>
        public static int TotalRegionalAvailableHotels
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.TOTAL_REGIONAL_AVAIL_HOTEL_COUNT] != null)
                {
                    return ((int)session.Contents[SessionConstants.TOTAL_REGIONAL_AVAIL_HOTEL_COUNT]);
                }
                else
                {
                    return Int32.MinValue;
                }
            }
            set { HttpContext.Current.Session.Add(SessionConstants.TOTAL_REGIONAL_AVAIL_HOTEL_COUNT, value); }
        }

        //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
        public static int GetTotalRegionalAvailableHotels(HttpSessionState session)
        {
            if (session.Contents[SessionConstants.TOTAL_REGIONAL_AVAIL_HOTEL_COUNT] != null)
            {
                return ((int)session.Contents[SessionConstants.TOTAL_REGIONAL_AVAIL_HOTEL_COUNT]);
            }
            else
            {
                return Int32.MinValue;
            }
        }

        #endregion TotalRegionalAvailableHotels
    }
}
