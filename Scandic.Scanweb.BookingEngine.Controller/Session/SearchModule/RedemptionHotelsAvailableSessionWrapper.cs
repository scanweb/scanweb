﻿using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule
{
    public class RedemptionHotelsAvailableSessionWrapper
    {
        #region RedemptionHotelsAvailable

        

        /// <summary>
        /// This would store true if no hotels are available for Redemption search
        /// </summary>
        public static bool NoRedemptionHotelsAvailable
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object noRedemptionHotelsAvailable = session.Contents[SessionConstants.NO_REDEMPTION_HOTELS_AVAILABLE];
                if (noRedemptionHotelsAvailable != null)
                {
                    result = (bool)noRedemptionHotelsAvailable;
                }
                return result;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.NO_REDEMPTION_HOTELS_AVAILABLE, value); }
        }

        #endregion
    }
}
