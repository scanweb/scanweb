﻿using System;
using System.Web;
using System.Web.SessionState;
using System.Collections.Generic;
using System.Collections;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule
{
    public class TotalGeneralAvailableHotelsSessionWrapper
    {
        #region TotalGeneralAvailableHotels

        

        /// <summary>
        /// This property will contain the total of the number of General availabilty
        /// searches are done through thread and values are returned from OWS. 
        /// 
        /// This value will be compared against the total number of Regional availabilty
        /// results retured, To check if the browser has to do request for further hotels
        /// </summary>
        public static int TotalGeneralAvailableHotels
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.TOTAL_GENERAL_AVAIL_HOTEL_COUNT] != null)
                {
                    return ((int)session.Contents[SessionConstants.TOTAL_GENERAL_AVAIL_HOTEL_COUNT]);
                }
                else
                {
                    return Int32.MinValue;
                }
            }

            set { HttpContext.Current.Session.Add(SessionConstants.TOTAL_GENERAL_AVAIL_HOTEL_COUNT, value); }
        }

        public static int GetTotalGeneralAvailableHotels(HttpSessionState session)
        {
            if (session.Contents[SessionConstants.TOTAL_GENERAL_AVAIL_HOTEL_COUNT] != null)
            {
                return ((int)session.Contents[SessionConstants.TOTAL_GENERAL_AVAIL_HOTEL_COUNT]);
            }
            else
            {
                return Int32.MinValue;
            }
        }

        public static void SetTotalGeneralAvailableHotels(HttpSessionState session, int hotels)
        {
            session.Add(SessionConstants.TOTAL_GENERAL_AVAIL_HOTEL_COUNT, hotels);
        }

        //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
        public static int PromoGeneralAvailableHotels
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SessionConstants.PROMO_TOTAL_GENERAL_AVAIL_HOTEL_COUNT] != null)
                {
                    return ((int)session.Contents[SessionConstants.PROMO_TOTAL_GENERAL_AVAIL_HOTEL_COUNT]);
                }
                else
                {
                    return Int32.MinValue;
                }
            }

            set { HttpContext.Current.Session.Add(SessionConstants.PROMO_TOTAL_GENERAL_AVAIL_HOTEL_COUNT, value); }
        }

        public static int GetPromoGeneralAvailableHotels(HttpSessionState session)
        {
            if (session.Contents[SessionConstants.PROMO_TOTAL_GENERAL_AVAIL_HOTEL_COUNT] != null)
            {
                return ((int)session.Contents[SessionConstants.PROMO_TOTAL_GENERAL_AVAIL_HOTEL_COUNT]);
            }
            else
            {
                return Int32.MinValue;
            }
        }

        public static void SetPromoGeneralAvailableHotels(HttpSessionState session, int hotels)
        {
            session.Add(SessionConstants.PROMO_TOTAL_GENERAL_AVAIL_HOTEL_COUNT, hotels);
        }
        #endregion TotalGeneralAvailableHotels
    }
}
