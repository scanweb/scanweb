﻿using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule
{
    public class AlternateHotelsSessionWrapper
    {
        #region AlternateHotels

        

        /// <summary>
        /// Set the flag to display Alternate Hotels flow
        /// </summary>        
        public static bool DisplayAlternateHotelsAvailable
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object alternateHotelsDiplay = session.Contents[SessionConstants.DISPLAY_ALTERNATE_HOTEL];
                if (alternateHotelsDiplay != null)
                {
                    result = (bool)alternateHotelsDiplay;
                }
                return result;
            }
            set { HttpContext.Current.Session[SessionConstants.DISPLAY_ALTERNATE_HOTEL] = value; }
        }

        

        /// <summary>
        /// Set the flag to display Alternate Hotels flow
        /// </summary>        
        public static bool DisplayNoHotelsAvailable
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object alternateHotelsDiplay = session.Contents[SessionConstants.DISPLAY_NO_HOTEL_AVAILABLE];
                if (alternateHotelsDiplay != null)
                {
                    result = (bool)alternateHotelsDiplay;
                }
                return result;
            }
            set { HttpContext.Current.Session[SessionConstants.DISPLAY_NO_HOTEL_AVAILABLE] = value; }
        }

        //Merchandising:R3:Display ordinary rates for unavailable promo rates 
        public static bool IsHotelAlternateFlow
        {
            get
            {
                bool result = false;
                HttpSessionState session = System.Web.HttpContext.Current.Session;
                object isHotelAlternateFlow = session.Contents[SessionConstants.IS_HOTEL_ALTERNATE_FLOW];
                if (isHotelAlternateFlow != null)
                {
                    result = (bool)isHotelAlternateFlow;
                }
                return result;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.IS_HOTEL_ALTERNATE_FLOW, value);}
        }

        #endregion
    }
}
