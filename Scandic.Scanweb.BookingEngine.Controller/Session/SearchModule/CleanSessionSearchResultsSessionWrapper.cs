﻿using System;
using System.Collections.Generic;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule
{
    public class CleanSessionSearchResultsSessionWrapper
    {
        #region CleanSessionSearchResults

        /// <summary>
        /// This will clear all the hotel search results stored in the session
        /// Each search will be considered as new so all the details need to be
        /// populated again in the Select Hotel Page.
        /// </summary>
        public static void CleanSessionSearchResults()
        {
            TotalRegionalAvailableHotelsSessionWrapper.TotalRegionalAvailableHotels = Int32.MinValue;
            TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels = Int32.MinValue;
            HotelResultsSessionWrapper.HotelResults = null;
            AlternateHotelsSessionWrapper.DisplayAlternateHotelsAvailable = false;
            AlternateHotelsSessionWrapper.DisplayNoHotelsAvailable = false;
            RedemptionHotelsAvailableSessionWrapper.NoRedemptionHotelsAvailable = false;

            GoogleMapHotelResultsSessionWrapper.GoogleMapResults = new List<Dictionary<string, object>>();

            BookingEngineSessionWrapper.IsModifyBooking = false;
            Reservation2SessionWrapper.IsCitySortDoneInSelectHotel = false;

            HotelRoomRateSessionWrapper.ListHotelRoomRate = null;
            HotelRoomRateSessionWrapper.TotalRateCalculated = false;
            HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable = null;
            HotelRoomRateSessionWrapper.ListRoomStay = null;
            Reservation2SessionWrapper.AvailabilityCalendar = null;
            Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage = false;
            HygieneSessionWrapper.IsModifySelectRateFromBreadCrumb = false;
            HotelResultsSessionWrapper.IsPromoNotValidForCity = false;
        }

        /// <summary>
        /// Clears the combo reservation booleans.
        /// </summary>
        public static void ClearComboReservationBooleans()
        {
            HttpContext.Current.Session[SessionConstants.IS_COMBO] = false;
        }

        #endregion CleanSessionSearchResults
    }
}
