﻿using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule
{
    public class GoogleMapHotelResultsSessionWrapper
    {
        #region GoogleMapHotelResults

        

        /// <summary>
        /// This list contains the hotel details which are required by the
        /// Google map displayed on the Select a hotel page.
        /// These details are picked up by the GoogleMap user control from 
        /// the session and the hotel details are displayed accordingly
        /// </summary>
        public static List<Dictionary<string, object>> GoogleMapResults
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.GOOGLE_MAP_HOTELS] as List<Dictionary<string, object>>;
            }

            set { HttpContext.Current.Session.Add(SessionConstants.GOOGLE_MAP_HOTELS, value); }
        }

        /// <summary>
        /// Gets google map results
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static List<Dictionary<string, object>> GetGoogleMapResults(HttpSessionState session)
        {
            return session.Contents[SessionConstants.GOOGLE_MAP_HOTELS] as List<Dictionary<string, object>>;
        }

        /// <summary>
        /// Sets google map results
        /// </summary>
        /// <param name="session"></param>
        /// <param name="value"></param>
        public static void SetGoogleMapResults(HttpSessionState session, List<Dictionary<string, object>> value)
        {
            session.Add(SessionConstants.GOOGLE_MAP_HOTELS, value);
        }

        #endregion
    }
}
