﻿using System;
using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule
{
    public class SearchedHotelCityNameSessionWrapper
    {
        #region Searched Hotel's City Name

        /// <summary>
        /// This variable is used to store the searched hotel's city name,which is used in SelectRate.ascx.cs
        /// to populate.Used in SelectHotel.ascx.cs to display the city text.
        /// </summary>
        

        /// <summary>
        /// Gets or sets the searched hotel's city name.
        /// </summary>
        public static string SearchedHotelCityName
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                string cityName = String.Empty;
                if (session.Contents[SessionConstants.SEARCHED_HOTELS_CITY_NAME] != null)
                {
                    cityName = session.Contents[SessionConstants.SEARCHED_HOTELS_CITY_NAME] as string;
                }
                return cityName;
            }
            set { HttpContext.Current.Session[SessionConstants.SEARCHED_HOTELS_CITY_NAME] = value; }
        }

        #endregion Searched Hotel's City Name
    }
}
