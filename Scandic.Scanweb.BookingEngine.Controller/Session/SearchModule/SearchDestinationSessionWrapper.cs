﻿using System;
using System.Web;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule
{
    public class SearchDestinationSessionWrapper
    {
        #region Search Destination

        

        /// <summary>
        /// Contains the search destination: City name/Hotel name
        /// </summary>
        public static string SearchDestination
        {
            get
            {
                string result = String.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object isDest = session.Contents[SessionConstants.SEARCH_DESTINATION];
                if (isDest != null)
                {
                    result = (string)isDest;
                }
                return result;
            }
            set { HttpContext.Current.Session[SessionConstants.SEARCH_DESTINATION] = value; }
        }

        

        /// <summary>
        /// Contains the search destination: City name/Hotel name
        /// </summary>
        public static bool SearchDestinationUpdated
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object iDest = session.Contents[SessionConstants.SEARCH_DESTINATION_UPDATED];
                if (iDest != null)
                {
                    result = (bool)iDest;
                }
                return result;
            }
            set { HttpContext.Current.Session[SessionConstants.SEARCH_DESTINATION_UPDATED] = value; }
        }

        #endregion
    }
}
