﻿using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using System.Xml;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using EPiServer.Core;

namespace Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule
{
    public class FindAHotelSessionVariablesSessionWrapper
    {
        #region FindAHotel Session Variables

        

        /// <summary>
        /// Stores the Sorting type selected in hotel listing control
        /// Used for intimation to the google map
        /// </summary>
        public static string HotelSortOrder
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.SORT_TYPE] as string;
            }

            set { HttpContext.Current.Session.Add(SessionConstants.SORT_TYPE, value); }
        }

        /// <summary>
        /// Constant used when the dropdown for Alphabetic/Distance from city centre is selected
        /// For Listing control
        /// </summary>
        


        /// <summary>
        /// This Property is set when the dropdown for Alphabetic/Distance from city centre is selected
        /// </summary>
        public static bool IsHotelSorted
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objIsHotelSorted = session.Contents[SessionConstants.IS_HOTEL_SORTED];
                if (objIsHotelSorted != null)
                {
                    result = (bool)objIsHotelSorted;
                }
                return result;
            }

            set { HttpContext.Current.Session.Add(SessionConstants.IS_HOTEL_SORTED, value); }
        }

        

        /// <summary>
        /// Gets the HotelDestination List
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static List<HotelDestination> GetHotelDestinationResults()
        {
            HttpSessionState session = HttpContext.Current.Session;
            return session.Contents[SessionConstants.SEARCH_DESTINATION_RESULTS] as List<HotelDestination>;
        }

        /// <summary>
        /// Sets the HotelDestination List
        /// </summary>
        /// <param name="session"></param>
        /// <param name="hotels"></param>
        public static void SetHotelDestinationResults(List<HotelDestination> hotels)
        {
            HttpSessionState session = HttpContext.Current.Session;
            session.Add(SessionConstants.SEARCH_DESTINATION_RESULTS, hotels);
        }

        

        /// <summary>
        /// Set a session for deeplinking page.
        /// </summary>
        public static bool IsDeepLinking
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                object isDeeplinking = session.Contents[SessionConstants.IS_DEEPLINKING];
                if (isDeeplinking != null)
                {
                    return (bool)session.Contents[SessionConstants.IS_DEEPLINKING];
                }
                else
                {
                    return false;
                }
            }

            set { HttpContext.Current.Session.Add(SessionConstants.IS_DEEPLINKING, value); }
        }

        

        /// <summary>
        /// Gets or sets MarkerData in Session
        /// </summary>
        public static XmlDocument MarkerData
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return (XmlDocument)session.Contents[SessionConstants.MARKER_DATA];
            }
            set { HttpContext.Current.Session.Add(SessionConstants.MARKER_DATA, value); }
        }

        

        /// <summary>
        /// Stores the state of Show/Hide link of Hotel Listing in 'Find A Hotel' Page
        /// </summary>
        public static bool IsShowAll
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objIsShowAll = session.Contents[SessionConstants.IS_SHOW_ALL];
                if (objIsShowAll != null)
                {
                    result = (bool)objIsShowAll;
                }
                return result;
            }

            set { HttpContext.Current.Session.Add(SessionConstants.IS_SHOW_ALL, value); }
        }

        

        /// <summary>
        /// The Hotel Search Criteria object containing the details
        /// the user has searched for.
        /// The values are stored in a HotelSearchEntity object.
        /// The session object is updated in the code behind of the search user controls
        /// </summary>
        public static Node SelectedNodeInTreeView
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.SELECTED_NODE_IN_TREEVIEW] as Node;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.SELECTED_NODE_IN_TREEVIEW, value); }
        }

        public static Node SelectedNodeForMap
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.SELECTED_NODE_FOR_MAP] as Node;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.SELECTED_NODE_FOR_MAP, value); }
        }

        public static string SelectedMapId
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.SELECTED_MAP_ID] as string;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.SELECTED_MAP_ID, value); }
        }

        /// <summary>
        /// This is added for page tracking in find a hotel page.
        /// This will hold the value for the event source.
        /// </summary>
        public static string EventSource
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.EVENTSOURCE] as string;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.EVENTSOURCE, value); }
        }

        public static string SelectedCity
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.SELECTED_CITY] as string;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.SELECTED_CITY, value); }
        }
        public static string CountryCode
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SessionConstants.SELECTED_COUNTRY_CODE] as string;
            }
            set { HttpContext.Current.Session.Add(SessionConstants.SELECTED_COUNTRY_CODE, value); }
        }
        #endregion FindAHotel Session Variables
    }
}
