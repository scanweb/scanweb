﻿//  Description					: ContentDataAccess manager acts as a contract to access  //
//								  staticcontentdataccess members, and it enablesto decouple// 
//                                static methods that are getting called from controllers.//
//----------------------------------------------------------------------------------------//
//  Author						: Sapient                                            	  //
//  Author email id				:                           							  //
//  Creation Date				: August 22nd, 2012                                       //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
// 	Last Modified Date			:                                                         //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Controller.Entity;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// ContentDataAccess manager acts as a contract to access staticcontentdataccess members, 
    /// and it enables to decouple static methods that are getting called from controllers.
    /// </summary>
    public interface IContentDataAccessManager
    {
        /// <summary>
        /// Gets a dictionary with RateType keys and the corresponding RateCategories
        /// </summary>
        /// <returns>A dictionary with all Rate Type IDs and the each corresponding Rate Category.</returns>
        Dictionary<string, RateCategory> GetRateTypeCategoryMap();

        /// <summary>
        /// Gets a dictionary with RoomType keys and the corresponding RoomCategory
        /// </summary>
        /// <returns>A dictionary with all Room Type IDs and the each corresponding Room Category.</returns>
        Dictionary<string, RoomCategory> GetRoomTypeCategoryMap();

        /// <summary>
        /// Returs the List of all Bookable hotels
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        List<HotelDestination> GetBookableHotels(string cityCode);

        /// <summary>
        /// Get all alternative city hotels for a particular city from Cache.If it is not available 
        /// in Cache , call CreateHotelDestinationList() method which will get 
        /// all alternative city hotels for a particular city from CMS
        /// </summary>
        /// <returns>Return a list of  alternative city hotels for a  particular city</returns>
        List<HotelDestination> GetAlternateCityHotels(string cityCode);

        /// <summary>
        /// Gets the Hotel based on the specified Opera ID. 
        /// </summary>
        /// <param name="hotelOperaID">The Opera ID of the Hotel</param>
        /// <returns>Returns the Hotel object with the specified Opera ID. </returns>
        HotelDestination GetHotelByOperaID(string hotelOperaID);

        /// <summary>
        /// Gets the Hotel based on the specified Opera ID and language
        /// </summary>
        /// <param name="hotelOperaID">The Opera ID of the Hotel</param>
        /// <returns>Returns the Hotel object with the specified Opera ID. </returns>
        HotelDestination GetHotelByOperaID(string hotelOperaID, string language);

        /// <summary>
        /// This mehthod is consolida the GetAllDestinations() and GetAllDestinationsIgnoreVisibility().
        /// This is implementating the new tree framework.
        /// </summary>
        /// <param name="ignoreBookingVisibility">If True then Ignores the hotel i.e hotel is not available for booking</param>
        /// <returns>List of city</returns>
        /// <remarks>Find a hotel release.</remarks>
        List<CityDestination> GetCityAndHotel(bool ignoreBookingVisibility);

        List<CityDestination> GetDestinationsFromTheLastSixMonthscityList(bool ignoreBookingVisibility);
        
        /// <summary>
        /// This mehthod is consolida the GetAllDestinations() and GetAllDestinationsIgnoreVisibility().
        /// This is implementating the new tree framework.
        /// </summary>
        /// <param name="ignoreBookingVisibility">If True then Ignores the hotel i.e hotel is not available for booking</param>
        /// <returns>List of city</returns>
        /// <remarks>Find a hotel release.</remarks>
        List<CityDestination> GetCityAndHotelForAutoSuggest(bool ignoreBookingVisibility);

        List<Scandic.Scanweb.Core.CountryDestinatination> GetCountryCityAndHotelForAutoSuggest(bool ignoreBookingVisibility,bool removeChildrens);

        /// <summary>
        /// Fetches all destinations with non bookable status.
        /// </summary>
        /// <param name="ignoreBookingVisibility"></param>
        /// <returns></returns>
        List<CityDestination> FetchAllDestinationsWithNonBookable(bool ignoreBookingVisibility);


        /// <summary>
        /// Get all Transactions
        /// </summary>
        /// <returns>Return a list of all Transactions</returns>
        List<Transaction> GetAllTransactions();

        /// <summary>
        /// This will return the bonus cheque value configured for a country in a year
        /// </summary>
        /// <param name="year">The year for which the bonus cheque needs to be calcuated</param>
        /// <param name="country">The Opera Country code</param>
        /// <param name="currencyCode">The Opera Currency Code</param>
        /// <returns>The Bouns cheque value configured for the corresponding country</returns>
        double GetBonusChequeRate(int year, string country, string currencyCode);

        /// <summary>
        /// Gets the Country based on the specified countryCode. 
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns>Returns the Hotel object with the specified Opera ID. </returns>
        string GetCountryNamebyCountryID(string countryCode);

        /// <summary>
        /// Gets the hotel destination with the Hotel Name 
        /// </summary>
        /// <param name="hotelName"></param>
        /// <returns></returns>
        HotelDestination GetHotelDestinationWithHotelName(string hotelName);

        /// <summary>
        /// Returns the list of all Hotels (Bookable and non bookable)
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        List<HotelDestination> GetAllTypesofHotels(string cityCode);

        /// <summary>
        /// Returns the list of all Hotels and Redemption points associated to the hotels
        /// </summary>     
        /// <returns></returns>
        IList<HotelRedemptionPoints> GetAllHotelRedemptionPoints();

        /// <summary>
        /// returns marchant cms data by hotel opera id
        /// </summary>
        /// <param name="hotelOperaID"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        MerchantProfileEntity GetMerchantByHotelOperaId(string hotelOperaID);

        /// <summary>
        /// returns marchant cms data by hotel opera id and language
        /// </summary>
        /// <param name="hotelOperaID"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        MerchantProfileEntity GetMerchantByHotelOperaId(string hotelOperaID, string language);

        /// <summary>
        /// returns whether payment fallback is enabled for scanweb
        /// </summary>  
        /// <returns></returns>
        bool GetPaymentFallback(string hotelOperaID);
    }
}
