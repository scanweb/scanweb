using System.Collections;
using System.Threading;

namespace Scandic.Scanweb.BookingEngine.Domain
{
    /// <summary>
    /// ThreadPoolImpl
    /// </summary>
    public sealed class ThreadPoolImpl
    {
        /// <summary>
        /// Static Constructor
        /// </summary>
        static ThreadPoolImpl()
        {
        }

        #region SpawnThreadPool

        /// <summary>
        /// SpawnThreadPool
        /// </summary>
        /// <param name="callBackMethod"></param>
        /// <param name="parameters"></param>
        public static void SpawnThreadPool(WaitCallback callBackMethod, Hashtable parameters)
        {
            ThreadPool.QueueUserWorkItem(callBackMethod, parameters);
        }

        #endregion SpawnThreadPool
    }
}