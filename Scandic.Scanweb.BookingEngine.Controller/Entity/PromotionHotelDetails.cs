using System.Collections.Generic;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The HotelDetails subtype for the search type Promotion search
    /// </summary>
    public class PromotionHotelDetails : RegularHotelDetails
    {
        #region Variables

        /// <summary>
        /// The Negotiated rate code entered by the user
        /// </summary>
        public string promotionCode;

        /// <summary>
        /// The list of promotion rate plan codes configured and returned
        /// </summary>
        private string[] promotionRatePlanCodes;

        /// <summary>
        /// The Promotion room Rate list i.e., the RoomRateEntities having one of the <code>promotionRatePlanCodes</code>
        /// </summary>
        private List<RoomRateEntity> promotionRoomRates = new List<RoomRateEntity>();

        /// <summary>
        /// The Non-Promotion Room rate list i.e., the public rates
        /// </summary>
        private List<RoomRateEntity> publicRoomRates = new List<RoomRateEntity>();

        #endregion

        #region Properties

        public bool HasPromotionRoomRates
        {
            get { return (null != promotionRoomRates && promotionRoomRates.Count > 0); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="roomStay"></param>
        /// <param name="promotionCode"></param>
        public PromotionHotelDetails(HotelDestination hotel,
                                     string countryCode,
                                     RoomStay roomStay,
                                     string promotionCode)
        {
            SetUpPromotionHotelDetails(hotel, countryCode, roomStay, promotionCode);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="listRoomStay"></param>
        /// <param name="promotionCode"></param>
        public PromotionHotelDetails(HotelDestination hotel,
                                     string countryCode,
                                     IList<RoomStay> listRoomStay,
                                     string promotionCode)
        {
            SetUpPromotionHotelDetails(hotel, countryCode, listRoomStay, promotionCode);
        }

        //Merchandising:R3:Display ordinary rates for unavailable promo rates 
        public PromotionHotelDetails(HotelDestination hotel,
           string countryCode,
           IList<RoomStay> listRoomStay,
           string promotionCode, bool forAvailabilityCalendar)
        {
            SetUpPromotionHotelDetails(hotel, countryCode, listRoomStay, promotionCode, forAvailabilityCalendar);
        }

        #endregion

        /// <summary>
        /// Setup the Promotion Hotel Details
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="roomStay"></param>
        /// <param name="alternateHotelsDistance"></param>
        private void SetUpPromotionHotelDetails(HotelDestination hotel,
                                                string countryCode,
                                                RoomStay roomStay,
                                                string promotionCode)
        {
            this.promotionCode = promotionCode;
            this.HotelDestination = hotel;
            this.CountryCode = countryCode;
            this.promotionRatePlanCodes = GetPromotionRates(roomStay.RatePlans, promotionCode);
            SetUpRoomRates(roomStay);
            SetUpMinBaseRateInEachRateCategory();
            SetUpMinTotalRateInEachRateCategory();
            SetMinMaxRate();
            SetSelectedRatePlanes();
            SetAllRateCategories();
        }

        /// <summary>
        /// SetUp Promotion Hotel Details
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="listRoomStay"></param>
        /// <param name="promotionCode"></param>
        private void SetUpPromotionHotelDetails(HotelDestination hotel,
                                                string countryCode,
                                                IList<RoomStay> listRoomStay,
                                                string promotionCode)
        {
            this.promotionCode = promotionCode;
            this.HotelDestination = hotel;
            this.CountryCode = countryCode;
            this.promotionRatePlanCodes = GetPromotionRates(listRoomStay, promotionCode);
            SetUpRoomRates(listRoomStay);
            SetUpMinBaseRateInEachRateCategory(new object());
            SetUpMinTotalRateInEachRateCategory(new object());
            SetMinMaxRate(new object());
            SetMinMaxRateForEachRoom(new object());
            SetSelectedRatePlans(new object());
            SetAllRateCategories(new object());
        }

        //Merchandising:R3:Display ordinary rates for unavailable promo rates 
        private void SetUpPromotionHotelDetails(HotelDestination hotel,
           string countryCode,
           IList<RoomStay> listRoomStay,
           string promotionCode, bool forAvailabilityCalendar)
        {
            this.promotionCode = promotionCode;
            this.HotelDestination = hotel;
            this.CountryCode = countryCode;
            this.promotionRatePlanCodes = GetPromotionRates(listRoomStay, promotionCode, hotel, forAvailabilityCalendar);
            SetUpRoomRates(listRoomStay);
            SetUpMinBaseRateInEachRateCategory(new object());
            SetUpMinTotalRateInEachRateCategory(new object());
            SetMinMaxRate(new object());
            SetMinMaxRateForEachRoom(new object());
            SetSelectedRatePlans(new object());
            SetAllRateCategories(new object());
        }

        #region GetPromotionRates

        /// <summary>
        /// The list of promotion rates will be returned. 
        /// Each RatePlan will have the tags as promotionCode
        /// if this tag is exisiting in the RatePlan values then it is added to the list
        /// </summary>
        /// <param name="ratePlans">The list of RatePlan objects to be searched for</param>
        /// <param name="corporateCode">The Promotion code passed earlier the OWS Request</param>
        /// <returns>List of Promotion rates returned from the OWS</returns>
        private string[] GetPromotionRates(RatePlan[] ratePlans, string promotionCode)
        {
            List<string> rateCodes = new List<string>();

            if (null != ratePlans)
            {
                for (int i = 0; i < ratePlans.Length; i++)
                {
                    RatePlan ratePlan = ratePlans[i];
                    if (promotionCode == ratePlan.promotionCode)
                    {
                        rateCodes.Add(ratePlan.ratePlanCode);
                    }
                }
            }
            return rateCodes.ToArray();
        }


        /// <summary>
        /// Gets the promotion rates.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>
        /// <param name="promotionCode">The promotion code.</param>
        /// <returns>Promomtion Rates</returns>
        private string[] GetPromotionRates(IList<RoomStay> listRoomStay, string promotionCode)
        {
            List<string> rateCodes = new List<string>();

            foreach (RoomStay roomStay in listRoomStay)
            {
                RatePlan[] ratePlans = roomStay.RatePlans;

                if (null != ratePlans)
                {
                    for (int i = 0; i < ratePlans.Length; i++)
                    {
                        RatePlan ratePlan = ratePlans[i];
                        if (promotionCode == ratePlan.promotionCode)
                        {
                            rateCodes.Add(ratePlan.ratePlanCode);
                        }
                    }
                }
            }
            return rateCodes.ToArray();
        }
        //Merchandising:R3:Display ordinary rates for unavailable promo rates 
        /// <summary>
        /// Gets the promotion rates.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>
        /// <param name="promotionCode">The promotion code.</param>
        /// <returns></returns>
        private string[] GetPromotionRates(IList<RoomStay> listRoomStay, string promotionCode, HotelDestination hotel, bool forAvailabilityCalendar)
        {
            List<string> rateCodes = new List<string>();

            foreach (RoomStay roomStay in listRoomStay)
            {
                RatePlan[] ratePlans = roomStay.RatePlans;

                if (null != ratePlans)
                {
                    for (int i = 0; i < ratePlans.Length; i++)
                    {
                        RatePlan ratePlan = ratePlans[i];
                        rateCodes.Add(ratePlan.ratePlanCode);
                    }
                }
            }
            return rateCodes.ToArray();
        }

        #endregion GetNegotiatedRates

        #region SetUpRoomRates

        /// <summary>
        /// Iterates through the list of rateplans, roomtypes, roomRates from OWS and sets the corresponding
        /// application lists if the following conditions are met
        /// I. The RatePlan code should be configured in CMS
        /// II. The RoomType code should be configured in CMS
        /// III. For roomrates both the rateplan and roomtype should be configured in CMS
        /// </summary>
        /// <param name="roomStay">The <code>RoomStay</code> element from OWS</param>
        protected override void SetUpRoomRates(RoomStay roomStay)
        {
            string[] bonusChequeRates = RoomRateUtil.GetBonusChequeRateCodes();
            int roomTypeSortOrder = 0;

            if (null != roomStay.RatePlans)
            {
                foreach (RatePlan ratePlan in roomStay.RatePlans)
                {
                    if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlan.ratePlanCode) &&
                        false == StringUtil.IsStringInArray(bonusChequeRates, ratePlan.ratePlanCode))
                    {
                        this.RatePlans.Add(GetRatePlan(ratePlan));
                    }
                }
            }
            if (null != roomStay.RoomTypes)
            {
                foreach
                    (ServiceProxies.Availability.RoomType roomType in roomStay.RoomTypes)
                {
                    if (RoomRateUtil.IsRoomTypeCodeInCMS(roomType.roomTypeCode))
                    {
                        roomTypeSortOrder += 1;
                        this.RoomTypes.Add(GetRoomType(roomType, roomTypeSortOrder));
                    }
                }
            }
            if (null != roomStay.RoomRates)
            {
                foreach (RoomRate roomRate in roomStay.RoomRates)
                {
                    string ratePlanCode = roomRate.ratePlanCode;
                    if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlanCode) &&
                        RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode) &&
                        false == StringUtil.IsStringInArray(bonusChequeRates, ratePlanCode))
                    {
                        RoomRateEntity roomRateEntity = GetRoomRate(roomRate);

                        if (Core.StringUtil.IsStringInArray(promotionRatePlanCodes, ratePlanCode))
                        {
                            roomRateEntity.IsSpecialRate = true;
                            promotionRoomRates.Add(roomRateEntity);
                            roomRates.Add(roomRateEntity);
                        }
                        else
                        {
                            publicRoomRates.Add(roomRateEntity);
                            roomRates.Add(roomRateEntity);
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Sets up room rates.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>
        protected override void SetUpRoomRates(IList<RoomStay> listRoomStay)
        {
            string[] bonusChequeRates = RoomRateUtil.GetBonusChequeRateCodes();

            foreach (RoomStay roomStay in listRoomStay)
            {
                PromoRoomEntity room = new PromoRoomEntity();
                int roomTypeSortOrder = 0;
                if (null != roomStay.RatePlans)
                {
                    foreach (RatePlan ratePlan in roomStay.RatePlans)
                    {
                        if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlan.ratePlanCode) &&
                            false == StringUtil.IsStringInArray(bonusChequeRates, ratePlan.ratePlanCode))
                        {
                            room.RatePlans.Add(GetRatePlan(ratePlan));
                        }
                    }
                }
                if (null != roomStay.RoomTypes)
                {
                    foreach
                        (ServiceProxies.Availability.RoomType roomType in 
                            roomStay.RoomTypes)
                    {
                        if (RoomRateUtil.IsRoomTypeCodeInCMS(roomType.roomTypeCode))
                        {
                            roomTypeSortOrder += 1;
                            room.RoomTypes.Add(GetRoomType(roomType, roomTypeSortOrder));
                        }
                    }
                }
                if (null != roomStay.RoomRates)
                {
                    foreach (RoomRate roomRate in roomStay.RoomRates)
                    {
                        string ratePlanCode = roomRate.ratePlanCode;
                        if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlanCode) &&
                            RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode) &&
                            false == StringUtil.IsStringInArray(bonusChequeRates, ratePlanCode))
                        {
                            RoomRateEntity roomRateEntity = GetRoomRate(roomRate);

                            if (Core.StringUtil.IsStringInArray(promotionRatePlanCodes, ratePlanCode))
                            {
                                roomRateEntity.IsSpecialRate = true;
                                promotionRoomRates.Add(roomRateEntity);
                                room.RoomRates.Add(roomRateEntity);
                                room.RoomHasPromoRates = true;
                            }
                            else
                            {
                                publicRoomRates.Add(roomRateEntity);
                                room.RoomRates.Add(roomRateEntity);
                            }
                        }
                    }
                }

                rooms.Add(room);
            }
        }

        #endregion
    }
}