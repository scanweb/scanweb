using System.Collections.Generic;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The Abstract base room rate details class containing the logic for the Room Rate page
    /// </summary>
    public abstract class BaseRoomRateDetails
    {
        #region Variables

        /// <summary>
        /// The list of rate categories available for the hotels
        /// </summary>
        public List<RateCategory> rateCategories;

        /// <summary>
        /// The room categories having the all common rates for the different <code>rateCategories</code>
        /// </summary>
        protected List<RoomCategoryEntity> baseRateRoomCategories;

        /// <summary>
        /// The hotel destination object, containing the hotel related details
        /// </summary>
        protected HotelDestination hotel;

        /// <summary>
        /// The country code for which the hotel belongs to
        /// </summary>
        protected string countryCode;

        /// <summary>
        /// R1.2 | CR3 | Change sort order of bed type preferences.
        /// All room types across the room categories of the hotel
        /// </summary>
        protected List<RoomTypeEntity> roomTypes;

        protected bool isRoomModifiable;

        #endregion

        #region Properties

        public List<RateCategory> RateCategories
        {
            get { return rateCategories; }
            set { rateCategories = value; }
        }

        public List<RoomCategoryEntity> RateRoomCategories
        {
            get { return baseRateRoomCategories; }
            set { baseRateRoomCategories = value; }
        }

        public string CountryCode
        {
            get { return countryCode; }
        }

        public HotelDestination HotelDestination
        {
            get { return hotel; }
        }

        public List<RoomTypeEntity> RoomTypes
        {
            get { return roomTypes; }
            set { roomTypes = value; }
        }

        protected RateEntity minRateForEachRoom;

        /// <summary>
        /// The Maximum avialable rate per night
        /// </summary>
        protected RateEntity maxRateForEachRoom;

        protected RateEntity minRatePerStayForEachRoom;

        protected RateEntity maxRatePerStayForEachRoom;


        /// <summary>
        /// <see cref="minRate"/>
        /// </summary>
        public RateEntity MinRateForEachRoom
        {
            get { return minRateForEachRoom; }
        }

        /// <summary>
        /// <see cref="maxRate"/>
        /// </summary>
        public RateEntity MaxRateForEachRoom
        {
            get { return maxRateForEachRoom; }
        }

        public RateEntity MinRatePerStayForEachRoom
        {
            get { return minRatePerStayForEachRoom; }
        }

        /// <summary>
        /// <see cref="maxRate"/>
        /// </summary>        
        /// <remarks>
        public RateEntity MaxRatePerStayForEachRoom
        {
            get { return maxRatePerStayForEachRoom; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is room modifiable.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is room modifiable; otherwise, <c>false</c>.
        /// </value>
        public bool IsRoomModifiable
        {
            get { return isRoomModifiable; }
            set { isRoomModifiable = value; }
        }

        #endregion

        /// <summary>
        /// This method sorts the Room Categories as per the sorting logic defined
        /// </summary>
        public void SortRoomCategories()
        {
            if (baseRateRoomCategories != null && baseRateRoomCategories.Count > 0)
            {
                baseRateRoomCategories.Sort(new RoomCategoryIdComparer());
            }
        }

        /// <summary>
        /// This method sorts the Rate Categories as per the sorting logic defined
        /// </summary>
        public void SortRateCategories()
        {
            if (rateCategories != null && rateCategories.Count > 0)
            {
                rateCategories.Sort(new RateCategoryComparer());
            }
        }

        #region Creating Room and Rate Categories

        /// <summary>
        /// This method will sort the room rate detials on the <code>CompositeMinBaseRateComparer</code>
        /// comparer, i.e.,
        /// 1. Room type
        /// 2. Rate Category
        /// 3. Base Rate (Per night rate)
        /// 
        /// Will pick the first room type and rate category from the above sorted list
        /// 
        /// From above picked list, the room and rates which are having rate (in a Ratecateogry) and in a
        /// Room category will be merged into a common RoomCategoryEntity. The list of this RoomCategoryEntities will
        /// be create from this method
        /// </summary>
        /// <param name="hotelDetails">The BaseHotel details object</param>
        protected void CreateBaseRateRoomCategories(BaseHotelDetails hotelDetails)
        {
            rateCategories = hotelDetails.RateCategories;

            List<RoomRateEntity> roomRates = hotelDetails.MinBaseRateInEachRateCategoryList;
            roomRates.Sort(new CompositeMinBaseRateComparer());
            List<RoomCategoryEntity> rowWiseRoomRateDetails = new List<RoomCategoryEntity>();
            string currentRoomType = null;
            RoomCategoryEntity currentRoomCategoryEntity = null;
            foreach (RoomRateEntity roomRate in roomRates)
            {
                string roomType = roomRate.RoomTypeCode;
                string roomCategoryName = RoomRateUtil.GetRoomCategory(roomType).RoomCategoryId;
                if (currentRoomType == roomType)
                {
                    AddRoomRate(currentRoomCategoryEntity, roomRate);
                }
                else
                {
                    currentRoomCategoryEntity = new RoomCategoryEntity();
                    SetRoomDetails(currentRoomCategoryEntity, roomType);
                    AddRoomRate(currentRoomCategoryEntity, roomRate);
                    rowWiseRoomRateDetails.Add(currentRoomCategoryEntity);
                    currentRoomType = roomType;
                }
            }
            rowWiseRoomRateDetails.Sort(new RateCategoryCountComparer());
            for (int i = 0; i < rowWiseRoomRateDetails.Count; i++)
            {
                RoomCategoryEntity outerRoomCategory = rowWiseRoomRateDetails[i];
                for (int j = i + 1; j < rowWiseRoomRateDetails.Count; j++)
                {
                    RoomCategoryEntity innerRoomCategory = rowWiseRoomRateDetails[j];
                    if (outerRoomCategory.MergeRoomCateogryEntityWithBaseRate(innerRoomCategory))
                    {
                        rowWiseRoomRateDetails.RemoveAt(j);
                        j = j - 1;
                    }
                }
            }
            this.baseRateRoomCategories = rowWiseRoomRateDetails;
        }
        
       /// <summary>
        /// CreateBaseRateRoomCategories
       /// </summary>
       /// <param name="hotelDetails"></param>
       /// <param name="roomNumber"></param>
        protected void CreateBaseRateRoomCategories(BaseHotelDetails hotelDetails, int roomNumber)
        {
            rateCategories = hotelDetails.Rooms[roomNumber].RateCategories;
            List<RoomRateEntity> roomRates = hotelDetails.Rooms[roomNumber].MinBaseRateInEachRateCategoryList;
            roomRates.Sort(new CompositeMinBaseRateComparer());
            List<RoomCategoryEntity> rowWiseRoomRateDetails = new List<RoomCategoryEntity>();
            string currentRoomType = null;
            RoomCategoryEntity currentRoomCategoryEntity = null;
            foreach (RoomRateEntity roomRate in roomRates)
            {
                string roomType = roomRate.RoomTypeCode;
                string roomCategoryName = RoomRateUtil.GetRoomCategory(roomType).RoomCategoryId;
                if (currentRoomType == roomType)
                {
                    AddRoomRate(currentRoomCategoryEntity, roomRate);
                }
                else
                {
                    currentRoomCategoryEntity = new RoomCategoryEntity();
                    SetRoomDetails(currentRoomCategoryEntity, roomType);
                    AddRoomRate(currentRoomCategoryEntity, roomRate);
                    rowWiseRoomRateDetails.Add(currentRoomCategoryEntity);
                    currentRoomType = roomType;
                }
            }
            rowWiseRoomRateDetails.Sort(new RateCategoryCountComparer());
            for (int i = 0; i < rowWiseRoomRateDetails.Count; i++)
            {
                RoomCategoryEntity outerRoomCategory = rowWiseRoomRateDetails[i];
                for (int j = i + 1; j < rowWiseRoomRateDetails.Count; j++)
                {
                    RoomCategoryEntity innerRoomCategory = rowWiseRoomRateDetails[j];
                    if (outerRoomCategory.MergeRoomCateogryEntityWithBaseRate(innerRoomCategory))
                    {
                        rowWiseRoomRateDetails.RemoveAt(j);
                        j = j - 1;
                    }
                }
            }
            this.baseRateRoomCategories = rowWiseRoomRateDetails;
        }

        /// <summary>
        /// Sets the room details of the <code>roomTypeCode</code> by reading from CMS
        /// and the corresponding values are set to the RoomCategoryEntity
        /// </summary>
        /// <param name="roomCategory"></param>
        /// <param name="roomTypeCode"></param>
        protected void SetRoomDetails(RoomCategoryEntity roomCategory, string roomTypeCode)
        {
            RoomCategory cmsRoomCategory = RoomRateUtil.GetRoomCategory(roomTypeCode);
            roomCategory.Id = cmsRoomCategory.RoomCategoryId;
            roomCategory.Name = cmsRoomCategory.RoomCategoryName;
            roomCategory.Description = cmsRoomCategory.RoomCategoryDescription;
            roomCategory.Url = cmsRoomCategory.RoomCategoryURL;
        }

        /// <summary>
        /// Adds the RoomRate entity to the <code>roomCategory</code> to the corresponding roomRateEntity. If the
        /// corresponding rate category is already exist then add to the list else create new list
        /// add the <code>roomRate</code> to the list
        /// </summary>
        /// <param name="roomCateogry">The room Category for which the roomRate entity need to be added</param>
        /// <param name="roomRate">The room rate entity which need to be added</param>
        protected void AddRoomRate(RoomCategoryEntity roomCateogry, RoomRateEntity roomRate)
        {
            string rateCategoryName;
            if (string.IsNullOrEmpty(roomRate.RatePlanCode))
            {
                rateCategoryName = roomCateogry.Name;
            }
            else
            {
                rateCategoryName = RoomRateUtil.GetRateCategoryByRatePlanCode(roomRate.RatePlanCode).RateCategoryId;
            }
            try
            {
                List<RoomRateEntity> roomRates = roomCateogry.RateCategories[rateCategoryName];
                roomRates.Add(roomRate);
            }
            catch (KeyNotFoundException knfe)
            {
                List<RoomRateEntity> roomRates = new List<RoomRateEntity>();
                roomRates.Add(roomRate);
                roomCateogry.RateCategories.Add(rateCategoryName, roomRates);
            }
        }

        #endregion
    }
}