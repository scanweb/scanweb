//  Description					:   BonusChequeHotelDetails                               //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The Hotel Details sub type for the search type Bonus cheque search
    /// </summary>
    public class BonusChequeHotelDetails : BaseHotelDetails
    {
        #region Variables

        /// <summary>
        /// The List of Bonus cheque rate codes configured
        /// </summary>
        private string[] bonusChequeRates;

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets ArrivalDate
        /// </summary>
        public DateTime ArrivalDate { get; set; }

        /// <summary>
        /// Gets/Sets DepartureDate
        /// </summary>
        public DateTime DepartureDate { get; set; }

        /// <summary>
        /// Gets/Sets MaxRate
        /// </summary>
        public override RateEntity MaxRate
        {
            get { return maxRate; }
        }

        /// <summary>
        /// Gets/Sets MinRate
        /// </summary>
        public override RateEntity MinRate
        {
            get { return minRate; }
        }

        /// <summary>
        /// Gets MaxRatePerStay
        /// </summary>
        public override RateEntity MaxRatePerStay
        {
            get { return maxRatePerStay; }
        }

        /// <summary>
        /// Gets MinRatePerStay
        /// </summary>
        public override RateEntity MinRatePerStay
        {
            get { return minRatePerStay; }
        }

        /// <summary>
        /// Checks whether a room is available or not.
        /// <see cref="BaseHotelDetails#IsAvailable"/>
        /// </summary>
        public override bool IsAvailable
        {
            get
            {
                bool blnIsAvailable = true;
                foreach (RoomEntity room in rooms)
                {
                    if (null == room.MinBaseRateInEachRateCategoryList ||
                        room.MinBaseRateInEachRateCategoryList.Count <= 0)
                    {
                        blnIsAvailable = false;
                        break;
                    }
                }
                return blnIsAvailable;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor of BonusChequeHotelDetails
        /// </summary>
        /// <param name="hotel">
        /// HotelDestination
        /// </param>
        /// <param name="countryCode">
        /// CountryCode
        /// </param>
        /// <param name="roomStay">
        /// RoomStay
        /// </param>
        /// <param name="arrivalDate">
        /// ArrivalDate
        /// </param>
        /// <param name="departureDate">
        /// DepartureDate
        /// </param>       
        public BonusChequeHotelDetails(HotelDestination hotel, string countryCode, RoomStay roomStay,
                                       DateTime arrivalDate, DateTime departureDate)
        {
            this.HotelDestination = hotel;
            this.CountryCode = countryCode;
            bonusChequeRates = RoomRateUtil.GetBonusChequeRateCodes();

            this.ArrivalDate = arrivalDate;
            this.DepartureDate = departureDate;
            SetUpRoomRates(roomStay);

            SetUpMinBaseRateInEachRateCategory();
            SetUpMinTotalRateInEachRateCategory();
            SetMinMaxRate();
            SetSelectedRatePlanes();
            SetAllRateCategories();
        }

        /// <summary>
        /// Constructor of BonusChequeHotelDetails
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="listRoomStay"></param>
        /// <param name="arrivalDate"></param>
        /// <param name="departureDate"></param>
        public BonusChequeHotelDetails(HotelDestination hotel, string countryCode, IList<RoomStay> listRoomStay,
                                       DateTime arrivalDate, DateTime departureDate)
        {
            this.HotelDestination = hotel;
            this.CountryCode = countryCode;
            bonusChequeRates = RoomRateUtil.GetBonusChequeRateCodes();

            this.ArrivalDate = arrivalDate;
            this.DepartureDate = departureDate;
            SetUpRoomRates(listRoomStay);

            SetUpMinBaseRateInEachRateCategory(new object());
            SetUpMinTotalRateInEachRateCategory(new object());
            SetMinMaxRate(new object());
            SetMinMaxRateForEachRoom(new object());
            SetSelectedRatePlans(new object());
            SetAllRateCategories(new object());
        }

        #endregion

        /// <summary>
        /// Similar to the <code>SetUpMinBaseRateInEachRateCategory</code> but
        /// the Total rate (Rate for stay) will be considered here
        /// </summary>
        private new void SetUpMinTotalRateInEachRateCategory()
        {
            roomRates.Sort(new CompositeMinTotalRateComparer());
            minTotalRateInEachRateCategoryList = PickMinRateInEachRateCategory(roomRates);
        }


        /// <summary>
        /// Similar to the <code>SetUpMinBaseRateInEachRateCategory</code> but
        /// the Total rate (Rate for stay) will be considered here
        /// </summary>
        private new void SetUpMinTotalRateInEachRateCategory(object sample)
        {
            foreach (RoomEntity room in rooms)
            {
                room.RoomRates.Sort(new CompositeMinTotalRateComparer());
                room.MinTotalRateInEachRateCategoryList = PickMinRateInEachRateCategory(room.RoomRates);
            }
        }

        /// <summary>
        /// This method will set the <code>minBaseRateInEachRateCategoryList</code>
        /// First it will sort the <code>roomRates</code> by following composite comparator
        /// RoomType, RateCategory, BaseRate values
        /// 
        /// Then picks the first rate which would be minimum in each roomtype's ratecategory
        /// This method is specific to the BaseRate (Rate per night)
        /// </summary>
        private new void SetUpMinBaseRateInEachRateCategory()
        {
            roomRates.Sort(new CompositeMinBaseRateComparer());
            minBaseRateInEachRateCategoryList = PickMinRateInEachRateCategory(roomRates);
        }


        /// <summary>
        /// Sets up min base rate in each rate category.
        /// </summary>
        /// <param name="sample">The sample.</param>
        private new void SetUpMinBaseRateInEachRateCategory(object sample)
        {
            foreach (RoomEntity room in rooms)
            {
                room.RoomRates.Sort(new CompositeMinBaseRateComparer());
                room.MinBaseRateInEachRateCategoryList = PickMinRateInEachRateCategory(room.RoomRates);
            }
        }

        /// <summary>
        /// Iterates through the sorted <code>roomRates</code> list, picks the first element
        /// of the RoomType, RateCategory sorted value and addes to a new List and returns this
        /// list
        /// Assumption is the roomRates entity will be sorted by RoomType, RateCategory
        /// 
        /// For example if the room rates are sorted and send as follows
        /// FF => EA1 => 110 SEK
        /// FF => EA2 => 220 SEK
        /// TR => RA2 => 200 SEK
        /// TR => RA3 => 210 SEK
        /// TR => RA1 => 400 SEK
        /// ................
        /// ................
        /// 
        /// This method will pick the first in each RoomType and RateCategory i.e,
        /// FF => EA1 => 110 SEK
        /// TR => RA2 => 200 SEK
        /// and returns only these elements added to a new list and returns
        /// 
        /// One more important thing is that this method will only consider any element to addition
        /// to the final list if and only if the corresponding element has the ratePlanCode
        /// which is defined as the rates to be considered
        /// </summary>
        /// <returns></returns>
        private new List<RoomRateEntity> PickMinRateInEachRateCategory(List<RoomRateEntity> roomRates)
        {
            string[] rateCategoriesToConsider = RateCategoriesToConsider();

            List<RoomRateEntity> minRateInEachCategoryList = new List<RoomRateEntity>();
            string currentRoomType = null;
            string currentRateCategory = null;
            foreach (RoomRateEntity roomRate in roomRates)
            {
                string roomType = roomRate.RoomTypeCode;
                string rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(roomRate.RatePlanCode).RateCategoryId;
                if ((roomType != currentRoomType || rateCategory != currentRateCategory) &&
                    StringUtil.IsStringInArray(rateCategoriesToConsider, rateCategory))
                {
                    minRateInEachCategoryList.Add(roomRate);
                    currentRoomType = roomType;
                    currentRateCategory = rateCategory;
                }
            }
            return minRateInEachCategoryList;
        }

        /// <summary>
        /// Iterates through the list of rateplans, roomtypes, roomRates from OWS and sets the corresponding
        /// application lists if the following conditions are met
        /// 
        /// I. The RatePlan code should be configured in CMS
        /// II. The RoomType code should be configured in CMS
        /// III. For roomrates both the rateplan and roomtype should be configured in CMS
        /// IV. Importantly, the RatePlan code should be one of the Bonuscheque rate
        /// </summary>
        /// <param name="roomStay">The <code>RoomStay</code> element from OWS</param>
        protected override void SetUpRoomRates(RoomStay roomStay)
        {
            int roomTypeSortOrder = 0;

            if (null != roomStay.RatePlans)
            {
                foreach (RatePlan ratePlan in roomStay.RatePlans)
                {
                    string ratePlanCode = ratePlan.ratePlanCode;


                    if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlanCode) &&
                        (Core.StringUtil.IsStringInArray(bonusChequeRates, ratePlanCode)))

                    {
                        this.RatePlans.Add(GetRatePlan(ratePlan));
                    }
                }
            }
            if (null != roomStay.RoomTypes)
            {
                foreach (
                    ServiceProxies.Availability.RoomType roomType in roomStay.RoomTypes)
                {
                    if (RoomRateUtil.IsRoomTypeCodeInCMS(roomType.roomTypeCode))
                    {
                        roomTypeSortOrder += 1;
                        this.RoomTypes.Add(GetRoomType(roomType, roomTypeSortOrder));
                    }
                }
            }
            if (null != roomStay.RoomRates)
            {
                foreach (RoomRate roomRate in roomStay.RoomRates)
                {
                    string ratePlanCode = roomRate.ratePlanCode;

                    if ((RoomRateUtil.IsRatePlanCodeInCMS(roomRate.ratePlanCode) &&
                         RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode)) &&
                        (Core.StringUtil.IsStringInArray(bonusChequeRates, ratePlanCode)))

                    {
                        this.RoomRates.Add(GetRoomRate(roomRate));
                    }
                }
            }
        }

        protected override void SetUpRoomRates(IList<RoomStay> listRoomStay)
        {
            foreach (RoomStay roomStay in listRoomStay)
            {
                RoomEntity room = new RoomEntity();
                int roomTypeSortOrder = 0;

                if (null != roomStay.RatePlans)
                {
                    foreach (RatePlan ratePlan in roomStay.RatePlans)
                    {
                        string ratePlanCode = ratePlan.ratePlanCode;

                        if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlanCode) &&
                            (Core.StringUtil.IsStringInArray(bonusChequeRates, ratePlanCode)))

                        {
                            room.RatePlans.Add(GetRatePlan(ratePlan));
                        }
                    }
                }
                if (null != roomStay.RoomTypes)
                {
                    foreach (
                        ServiceProxies.Availability.RoomType roomType in
                            roomStay.RoomTypes)
                    {
                        if (RoomRateUtil.IsRoomTypeCodeInCMS(roomType.roomTypeCode))
                        {
                            roomTypeSortOrder += 1;
                            room.RoomTypes.Add(GetRoomType(roomType, roomTypeSortOrder));
                        }
                    }
                }
                if (null != roomStay.RoomRates)
                {
                    foreach (RoomRate roomRate in roomStay.RoomRates)
                    {
                        string ratePlanCode = roomRate.ratePlanCode;

                        if ((RoomRateUtil.IsRatePlanCodeInCMS(roomRate.ratePlanCode) &&
                             RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode)) &&
                            (Core.StringUtil.IsStringInArray(bonusChequeRates, ratePlanCode)))

                        {
                            room.RoomRates.Add(GetRoomRate(roomRate));
                        }
                    }
                }
                rooms.Add(room);
            }
        }

        /// <summary>
        /// Reads the configured bonus cheque rates and returns the rateCategories to be considered
        /// If the bonus cheque rates are configured into two different rate categories, then exception is thrown
        /// </summary>
        /// <returns></returns>
        //protected override string[] RateCategoriesToConsider()
        private string[] RateCategoriesToConsider()
        {
            List<string> rateCategories = new List<string>();
            string[] bonusChequeRates = AppConstants.BONUS_CHEQUE_RATES;

            foreach (string bonusChequeRate in bonusChequeRates)
            {
                rateCategories.Add(RoomRateUtil.GetRateCategoryByRatePlanCode(bonusChequeRate).RateCategoryId);
            }
            if (rateCategories.Count > 2)
                throw new BonusChequeInvalidRateConfigurationException();

            return rateCategories.ToArray();
        }
    }
}