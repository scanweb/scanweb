using System;
using System.Collections.Generic;
using System.Xml;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller.Entity
{

    #region Class CustomerServiceContactUsEntity

    /// <summary>
    /// This hold the values of whole list of items entered in 
    /// EPiServer CMS for the property "MultiRecipientEmailAddresses".
    /// </summary>
    public class CustomerServiceContactUsEntity
    {
        public List<ContactUsType> NatureOfQueries { get; set; }

        public List<ContactUsType> Hotels { get; set; }

        /// <summary>
        /// GetContactUsTypeList
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xPath"></param>
        /// <returns>List of Contact Type</returns>
        private List<ContactUsType> GetContactUsTypeList(XmlDocument xmlDoc, string xPath)
        {
            XmlNodeList itemList;
            List<ContactUsType> contactUsTypeList = new List<ContactUsType>();
            string itemText = string.Empty;

            itemList = xmlDoc.SelectNodes(xPath);

            foreach (XmlNode node in itemList)
            {
                foreach (XmlNode child in node)
                {
                    itemText = child.InnerText;

                    if (itemText != string.Empty)
                    {
                        ContactUsType contactUsType;
                        if (itemText.IndexOf(";") >= 0)
                            contactUsType = new ContactUsType(itemText.Split(';')[0], itemText.Split(';')[1]);
                        else
                            contactUsType = new ContactUsType(itemText, string.Empty);
                        contactUsTypeList.Add(contactUsType);
                    }
                }
            }
            return contactUsTypeList;
        }

        /// <summary>
        /// Constructor method
        /// </summary>
        /// <param name="contactUsQueriesXML"></param>
        public CustomerServiceContactUsEntity(string contactUsQueriesXML)
        {
            XmlDocument xmlDoc = new XmlDocument();

            try
            {
                xmlDoc.LoadXml(contactUsQueriesXML);
                this.NatureOfQueries = GetContactUsTypeList(xmlDoc, "/contactus/natureofquery");
                this.Hotels = GetContactUsTypeList(xmlDoc, "/contactus/hotellist");
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx);
            }
        }
    }

    #endregion Class CustomerServiceContactUsEntity
}