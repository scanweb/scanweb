using System.Collections.Generic;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The HotelDetails subtype for the search type Regular search
    /// </summary>
    public class RegularHotelDetails : BaseHotelDetails
    {
        #region Properties

        public override RateEntity MaxRate
        {
            get { return maxRate; }
        }

        public override RateEntity MinRate
        {
            get { return minRate; }
        }

        public override RateEntity MaxRatePerStay
        {
            get { return maxRatePerStay; }
        }

        public override RateEntity MinRatePerStay
        {
            get { return minRatePerStay; }
        }

        public override bool IsAvailable
        {
            get
            {
                bool blnIsAvailable = true;
                foreach (RoomEntity room in rooms)
                {
                    if (null == room.MinBaseRateInEachRateCategoryList ||
                        room.MinBaseRateInEachRateCategoryList.Count <= 0)
                    {
                        blnIsAvailable = false;
                        break;
                    }
                }
                return blnIsAvailable;
            }
        }

        #endregion

        #region Constructor

        protected RegularHotelDetails()
        {
        }

        /// <summary>
        /// Regular Hotel Details constructor
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="roomStay"></param>
        public RegularHotelDetails(HotelDestination hotel, string countryCode, RoomStay roomStay)
        {
            SetUpRegularHotelDetails(hotel, countryCode, roomStay);
        }

        /// <summary>
        /// Regular Hotel Details constructor
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="listRoomStay"></param>
        public RegularHotelDetails(HotelDestination hotel, string countryCode, IList<RoomStay> listRoomStay)
        {
            SetUpRegularHotelDetails(hotel, countryCode, listRoomStay);
        }

        /// <summary>
        /// Setup the Regular Details
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="roomStay"></param>
        /// <param name="alternateHotelsDistance"></param>
        private void SetUpRegularHotelDetails(HotelDestination hotel, string countryCode, RoomStay roomStay)
        {
            this.HotelDestination = hotel;
            this.CountryCode = countryCode;
            SetUpRoomRates(roomStay);
            SetUpMinBaseRateInEachRateCategory();
            SetUpMinTotalRateInEachRateCategory();
            SetMinMaxRate();
            SetSelectedRatePlanes();
            SetAllRateCategories();
        }

        /// <summary>
        /// Setup the Regular Details
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="listRoomStay"></param>
        private void SetUpRegularHotelDetails(HotelDestination hotel, string countryCode, IList<RoomStay> listRoomStay)
        {
            this.HotelDestination = hotel;
            this.CountryCode = countryCode;
            SetUpRoomRates(listRoomStay);
            SetUpMinBaseRateInEachRateCategory(new object());
            SetUpMinTotalRateInEachRateCategory(new object());
            SetMinMaxRate(new object());
            SetMinMaxRateForEachRoom(new object());
            SetSelectedRatePlans(new object());
            SetAllRateCategories(new object());
        }

        #endregion

        #region SetUpRoomRates

        /// <summary>
        /// Iterates through the list of rateplans, roomtypes, roomRates from OWS and sets the corresponding
        /// application lists if the following conditions are met
        /// 
        /// I. The RatePlan code should be configured in CMS
        /// II. The RoomType code should be configured in CMS
        /// III. For roomrates both the rateplan and roomtype should be configured in CMS
        /// IV. The rateplan belongs the rate cateogry which is in the ratecategories to consider list
        /// </summary>
        /// <param name="roomStay">The <code>RoomStay</code> element from OWS</param>
        protected override void SetUpRoomRates(RoomStay roomStay)
        {
            string[] bonusChequeRates = RoomRateUtil.GetBonusChequeRateCodes();

            int roomTypeSortOrder = 0;

            if (null != roomStay.RatePlans)
            {
                foreach (RatePlan ratePlan in roomStay.RatePlans)
                {
                    if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlan.ratePlanCode))
                    {
                        string rateCategoryName =
                            RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlan.ratePlanCode).RateCategoryId;
                        if (!StringUtil.IsStringInArray(bonusChequeRates, rateCategoryName))
                            this.RatePlans.Add(GetRatePlan(ratePlan));
                    }
                }
            }
            if (null != roomStay.RoomTypes)
            {
                foreach (ServiceProxies.Availability.RoomType roomType in 
                    roomStay.RoomTypes)
                {
                    if (RoomRateUtil.IsRoomTypeCodeInCMS(roomType.roomTypeCode))
                    {
                        roomTypeSortOrder += 1;
                        this.RoomTypes.Add(GetRoomType(roomType, roomTypeSortOrder));
                    }
                }
            }
            if (null != roomStay.RoomRates)
            {
                foreach (RoomRate roomRate in roomStay.RoomRates)
                {
                    if (RoomRateUtil.IsRatePlanCodeInCMS(roomRate.ratePlanCode) &&
                        RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode))
                    {
                        string rateCategoryName =
                            RoomRateUtil.GetRateCategoryByRatePlanCode(roomRate.ratePlanCode).RateCategoryId;
                        if (!StringUtil.IsStringInArray(bonusChequeRates, rateCategoryName))
                            this.RoomRates.Add(GetRoomRate(roomRate));
                    }
                }
            }
        }

        /// <summary>
        /// Set Up Room Rates
        /// </summary>
        /// <param name="listRoomStay"></param>
        protected override void SetUpRoomRates(IList<RoomStay> listRoomStay)
        {
            string[] bonusChequeRates = RoomRateUtil.GetBonusChequeRateCodes();

            if (listRoomStay != null && listRoomStay.Count > 0)
            {
                foreach (RoomStay roomStay in listRoomStay)
                {
                    RoomEntity room = new RoomEntity();
                    int roomTypeSortOrder = 0;

                    if (null != roomStay.RatePlans)
                    {
                        foreach (RatePlan ratePlan in roomStay.RatePlans)
                        {
                            if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlan.ratePlanCode))
                            {
                                string rateCategoryName =
                                    RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlan.ratePlanCode).RateCategoryId;
                                if (!StringUtil.IsStringInArray(bonusChequeRates, rateCategoryName))
                                {
                                    room.RatePlans.Add(GetRatePlan(ratePlan));
                                }
                            }
                        }
                    }
                    if (null != roomStay.RoomTypes)
                    {
                        foreach (ServiceProxies.Availability.RoomType roomType in 
                            roomStay.RoomTypes)
                        {
                            if (RoomRateUtil.IsRoomTypeCodeInCMS(roomType.roomTypeCode))
                            {
                                roomTypeSortOrder += 1;
                                room.RoomTypes.Add(GetRoomType(roomType, roomTypeSortOrder));
                            }
                        }
                    }
                    if (null != roomStay.RoomRates)
                    {
                        foreach (RoomRate roomRate in roomStay.RoomRates)
                        {
                            if (RoomRateUtil.IsRatePlanCodeInCMS(roomRate.ratePlanCode) &&
                                RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode))
                            {
                                string rateCategoryName =
                                    RoomRateUtil.GetRateCategoryByRatePlanCode(roomRate.ratePlanCode).RateCategoryId;
                                if (!StringUtil.IsStringInArray(bonusChequeRates, rateCategoryName))
                                {
                                    room.RoomRates.Add(GetRoomRate(roomRate));
                                }
                            }
                        }
                    }

                    rooms.Add(room);
                }
            }
        }

        #endregion
    }
}