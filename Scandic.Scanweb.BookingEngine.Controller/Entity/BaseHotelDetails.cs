    using System.Collections.Generic;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using OWSAvailability = Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The Base class implementing the IHotelDetails interface
    /// This class will contian all the methods which are useful
    /// for the SubType for each Search Type
    /// 
    /// It will also implement the required methods of the IHotelDetail
    /// </summary>
    public abstract class BaseHotelDetails : IHotelDetails
    {
        #region Declarations

        /// <summary>
        /// List of available room types in OWS
        /// </summary>
        private List<RoomTypeEntity> roomTypes = new List<RoomTypeEntity>();

        /// <summary>
        /// List of available rate plans for the selected dates and search criteria
        /// </summary>
        private List<RatePlanEntity> ratePlans = new List<RatePlanEntity>();

        /// <summary>
        /// List of available room rate combinations for the selected dates and search criteria
        /// </summary>
        protected List<RoomRateEntity> roomRates = new List<RoomRateEntity>();

        protected List<RoomEntity> rooms = new List<RoomEntity>();

        /// <summary>
        /// The Minimum available rate per night
        /// </summary>
        protected RateEntity minRate;

        /// <summary>
        /// The Maximum avialable rate per night
        /// </summary>
        protected RateEntity maxRate;

        /// <summary>
        /// The Minimum available rate per stay
        /// </summary>
        protected RateEntity minRatePerStay;

        /// <summary>
        /// The Maximum avialable rate per stay
        /// </summary>
        protected RateEntity maxRatePerStay;

        /// <summary>
        /// The List of RateCategories available for the hotel
        /// </summary>
        private List<RateCategory> rateCategories;

        ///<summary>
        /// The minium available rate in each Rate category list for a room type.
        /// For example if the room type TR has multiple rate types of a rate cateogry Flex
        /// are mapped and returned from OWS only the minimum value of the rate category should
        /// be considered
        /// 
        /// 1. TR ==> RA1 = 120SEK
        /// 2. TR ==> RA2 = 130SEK
        /// 3. TR ==> RA3 = 160SEK
        /// 4. TR ==> RA4 = 110SEK
        /// 
        /// Then the minimum rate i.e, TR ==> RA4 will be considered and added to the
        /// <code>minBaseRateInEachRateCategoryList</code>
        /// 
        /// The base rate or the rate per night will be considered here
        /// </summary>
        protected List<RoomRateEntity> minBaseRateInEachRateCategoryList = new List<RoomRateEntity>();

        /// <summary>
        /// Same as <code>minBaseRateInEachCategoryList</code> except the change is the TotalRate
        /// will be considered
        /// This is used for future use if there is any difference in the BaseRate and TotalRate
        /// </summary>
        protected List<RoomRateEntity> minTotalRateInEachRateCategoryList = new List<RoomRateEntity>();

        #endregion

        #region Abstract properties

        /// <summary>
        /// Refer to <see cref="isAvailable"/>
        /// </summary>
        public abstract bool IsAvailable { get; }

        /// <summary>
        /// <see cref="minRate"/>
        /// </summary>
        public abstract RateEntity MinRate { get; }

        /// <summary>
        /// <see cref="maxRate"/>
        /// </summary>
        public abstract RateEntity MaxRate { get; }

        public abstract RateEntity MinRatePerStay { get; }

        /// <summary>
        /// <see cref="maxRate"/>
        /// </summary>        
        public abstract RateEntity MaxRatePerStay { get; }

        #endregion

        #region Properties

        /// <summary>
        /// <see cref="hotelDesitnation"/>
        /// </summary>
        public HotelDestination HotelDestination { get; set; }

        /// <summary>
        /// <see cref="CountryCode"/>
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// <see cref="roomTypes"/>
        /// </summary>
        public List<RoomTypeEntity> RoomTypes
        {
            get { return roomTypes; }
            set { roomTypes = value; }
        }

        /// <summary>
        /// <see cref="ratePlans"/>
        /// </summary>
        public List<RatePlanEntity> RatePlans
        {
            get { return ratePlans; }
            set { ratePlans = value; }
        }

        /// <summary>
        /// <see cref="roomRates"/>
        /// </summary>
        public List<RoomRateEntity> RoomRates
        {
            get { return roomRates; }
            set { roomRates = value; }
        }

        public List<RoomEntity> Rooms
        {
            get { return rooms; }
            set { rooms = value; }
        }

        /// <summary>
        /// <see cref="minPoints"/>
        /// </summary>
        public virtual RedeemptionPointsEntity MinPoints
        {
            get { return null; }
        }

        /// <summary>
        /// <see cref="maxPoints"/>
        /// </summary>
        public virtual RedeemptionPointsEntity MaxPoints
        {
            get { return null; }
        }

        /// <summary>
        /// <see cref="rateCategories"/>
        /// </summary>
        public List<RateCategory> RateCategories
        {
            get { return rateCategories; }
        }

        /// <summary>
        /// <see cref="MinBaseRateInEachRateCategoryList"/>
        /// </summary>
        public List<RoomRateEntity> MinBaseRateInEachRateCategoryList
        {
            get { return minBaseRateInEachRateCategoryList; }
            set { minBaseRateInEachRateCategoryList = value; }
        }

        /// <summary>
        /// <see cref="minTotalRateInEachRateCategoryList"/>
        /// </summary>
        public List<RoomRateEntity> MinTotalRateInEachRateCategoryList
        {
            get { return minTotalRateInEachRateCategoryList; }
            set { minTotalRateInEachRateCategoryList = value; }
        }

        /// <summary>
        /// <see cref="AlternateHotelsDistance"/>
        /// </summary>
        public double AlternateHotelsDistance { get; set; }

        public double TripAdvisorHotelRating { get; set; }

        public bool HideHotelTARating { get; set; }
        /// <summary>
        /// This property contains the distance to the city center distance of searched hotel.
        /// </summary>
        public string DistanceToCityCentreOfSearchedHotel { get; set; }
        public double DistanceFromCurrentLocationToSearchedHotel { get; set; }
        #endregion

        #region Abstract methods

        /// <summary>
        /// This is the abstract method each subtype of this <code>BaseHotelDetails</code>
        /// has to implement, the individual implementation will knows which rooms and rates to be
        /// considered and the corresponding properties <code>RoomTypes</code>, <code>RatePlans</code>
        /// <code>RoomRates</code> need to be set
        /// </summary>
        /// <param name="roomStay"></param>
        protected abstract void SetUpRoomRates(RoomStay roomStay);

        /// <summary>
        /// Each implementation will return the different rate categories to be considered
        /// </summary>
        /// <returns></returns>
        //protected abstract string[] RateCategoriesToConsider();
        protected abstract void SetUpRoomRates(IList<RoomStay> listRoomStay);

        #endregion

        #region OWS RatePlan utility methods

        /// <summary>
        /// The utility method which will take the OWS RatePlan object and creates and returns
        /// the corresponding application RatePlanEntity
        /// </summary>
        /// <param name="ratePlan">The OWS RatePlan object</param>
        /// <returns>The application RatePlanEntity object </returns>
        protected RatePlanEntity GetRatePlan(RatePlan ratePlan)
        {
            RatePlanEntity ratePlanEntity = new RatePlanEntity(ratePlan.ratePlanCode);
            if (null != ratePlan.RatePlanDescription && null != ratePlan.RatePlanDescription.Items)
                ratePlanEntity.Description = (ratePlan.RatePlanDescription.Items[0] as Text).Value;
            AddRateAdditionalDescriptions(ratePlanEntity, ratePlan.AdditionalDetails);

            return ratePlanEntity;
        }

        /// <summary>
        /// Reads the additional descriptions returned from OWS and adds to the application object
        /// <code>ratePlanEntity</code>object
        /// </summary>
        /// <param name="ratePlanEntity">The application RatePlanEntity object</param>
        /// <param name="details">The additional details passed from OWS for the RatePlan</param>
        private void AddRateAdditionalDescriptions(RatePlanEntity ratePlanEntity, AdditionalDetail[] details)
        {
            foreach (AdditionalDetail detail in details)
            {
                ratePlanEntity.AdditionalInformation.Add(detail.detailType.ToString(),
                                                         detail.AdditionalDetailDescription.Items[0] as string);
            }
        }

        #endregion

        #region OWS RoomType utility methods

        /// <summary>
        /// The utility method which will take the OWS RoomType object and creates and returns
        /// the corresponding application RoomTypeEntity object
        /// </summary>
        /// <param name="roomType">The OWS RoomType object</param>
        /// <param name="roomTypeSortOrder"></param>
        /// <returns>The application RoomTypeEntity object </returns>
        protected RoomTypeEntity GetRoomType(
            OWSAvailability.RoomType roomType, int roomTypeSortOrder)
        {
            RoomTypeEntity roomTypeEntity = new RoomTypeEntity(roomType.roomTypeCode);
            roomTypeEntity.SortOrder = roomTypeSortOrder;

            if (null != roomType.RoomTypeDescription && null != roomType.RoomTypeDescription.Items)
                roomTypeEntity.Description = (roomType.RoomTypeDescription.Items[0] as Text).Value;

            return roomTypeEntity;
        }

        #endregion

        #region OWS RoomRate utility methods

        /// <summary>
        /// The utility method which will take the OWS RoomRate object, creates and retuens
        /// the corresponding application RoomRateEntity
        /// </summary>
        /// <param name="roomRate">The OWS RoomRate object</param>
        /// <returns>The application RoomRateEntity object</returns>
        protected RoomRateEntity GetRoomRate(RoomRate roomRate)
        {
            RoomRateEntity roomRateEntity = new RoomRateEntity(roomRate.roomTypeCode, roomRate.ratePlanCode);
            roomRateEntity.BaseRate = new RateEntity(roomRate.Rates[0].Base.Value, roomRate.Rates[0].Base.currencyCode);
            roomRateEntity.TotalRate = new RateEntity(roomRate.Total.Value, roomRate.Total.currencyCode);
            return roomRateEntity;
        }

        #endregion

        #region Methods to read minimum rate in each rate category

        /// <summary>
        /// This method will set the <code>minBaseRateInEachRateCategoryList</code>
        /// First it will sort the <code>roomRates</code> by following composite comparator
        /// RoomType, RateCategory, BaseRate values
        /// 
        /// Then picks the first rate which would be minimum in each roomtype's ratecategory
        /// This method is specific to the BaseRate (Rate per night)
        /// </summary>
        protected void SetUpMinBaseRateInEachRateCategory()
        {
            roomRates.Sort(new CompositeMinBaseRateComparer());
            minBaseRateInEachRateCategoryList = PickMinRateInEachRateCategory(roomRates);
        }

        /// <summary>
        /// SetUpMinBaseRateInEachRateCategory
        /// </summary>
        /// <param name="sample"></param>
        protected void SetUpMinBaseRateInEachRateCategory(object sample)
        {
            foreach (RoomEntity room in rooms)
            {
                room.RoomRates.Sort(new CompositeMinBaseRateComparer());
                room.MinBaseRateInEachRateCategoryList = PickMinRateInEachRateCategory(room.RoomRates);
            }
        }

        /// <summary>
        /// Similar to the <code>SetUpMinBaseRateInEachRateCategory</code> but
        /// the Total rate (Rate for stay) will be considered here
        /// </summary>
        protected void SetUpMinTotalRateInEachRateCategory()
        {
            roomRates.Sort(new CompositeMinTotalRateComparer());
            minTotalRateInEachRateCategoryList = PickMinRateInEachRateCategory(roomRates);
        }

        /// <summary>
        /// SetUpMinTotalRateInEachRateCategory
        /// </summary>
        /// <param name="sample"></param>
        protected void SetUpMinTotalRateInEachRateCategory(object sample)
        {
            foreach (RoomEntity room in rooms)
            {
                room.RoomRates.Sort(new CompositeMinTotalRateComparer());
                room.MinTotalRateInEachRateCategoryList = PickMinRateInEachRateCategory(room.RoomRates);
            }
        }

        /// <summary>
        /// This will remove the rateplans which are not listed during the rate code competition.
        /// Only contain the rateplans which are selected for display.
        /// </summary>
        protected void SetSelectedRatePlanes()
        {
            int rateplanCount = minBaseRateInEachRateCategoryList.Count;
            List<string> filteredRatePlan = new List<string>();
            for (int count = 0; count < rateplanCount; count++)
            {
                string rateplan = minBaseRateInEachRateCategoryList[count].RatePlanCode;
                if (!filteredRatePlan.Contains(rateplan))
                    filteredRatePlan.Add(rateplan);
            }
            List<RatePlanEntity> finalRoomRateEntities = new List<RatePlanEntity>();
            int filteredRatePlanCount = filteredRatePlan.Count;
            int totalRatePlanCount = ratePlans.Count;
            for (int count = 0; count < filteredRatePlanCount; count++)
            {
                for (int innerCount = 0; innerCount < totalRatePlanCount; innerCount++)
                {
                    if (ratePlans[innerCount].Code == filteredRatePlan[count])
                    {
                        finalRoomRateEntities.Add(ratePlans[innerCount]);
                        break;
                    }
                }
            }
            ratePlans.Clear();
            ratePlans.AddRange(finalRoomRateEntities);
        }

        /// <summary>
        /// SetSelectedRatePlans
        /// </summary>
        /// <param name="sample"></param>
        protected void SetSelectedRatePlans(object sample)
        {
            foreach (RoomEntity room in rooms)
            {
                int rateplanCount = room.MinBaseRateInEachRateCategoryList.Count;
                List<string> filteredRatePlan = new List<string>();
                for (int count = 0; count < rateplanCount; count++)
                {
                    string rateplan = room.MinBaseRateInEachRateCategoryList[count].RatePlanCode;
                    if (!filteredRatePlan.Contains(rateplan))
                        filteredRatePlan.Add(rateplan);
                }
                List<RatePlanEntity> finalRoomRateEntities = new List<RatePlanEntity>();
                int filteredRatePlanCount = filteredRatePlan.Count;
                int totalRatePlanCount = room.RatePlans.Count;
                for (int count = 0; count < filteredRatePlanCount; count++)
                {
                    for (int innerCount = 0; innerCount < totalRatePlanCount; innerCount++)
                    {
                        if (room.RatePlans[innerCount].Code == filteredRatePlan[count])
                        {
                            finalRoomRateEntities.Add(room.RatePlans[innerCount]);
                            break;
                        }
                    }
                }
                room.RatePlans.Clear();
                room.RatePlans.AddRange(finalRoomRateEntities);
            }
        }
        
        /// <summary>
        /// PickMinRateInEachRateCategory
        /// </summary>
        /// <param name="roomRates"></param>
        /// <returns>List of RoomRateEntity</returns>
        protected List<RoomRateEntity> PickMinRateInEachRateCategory(List<RoomRateEntity> roomRates)
        {
            List<RoomRateEntity> minRateInEachCategoryList = new List<RoomRateEntity>();
            List<RoomRateEntity> filteredList = new List<RoomRateEntity>();
            string currentRoomType = null;
            int currentCoulmnNumber = 0;
            List<int> uniqueCoulmnID = new List<int>();
            if ((roomRates != null) && (roomRates.Count > 0))
            {
                foreach (RoomRateEntity roomRate in roomRates)
                {
                    if (roomRate != null && roomRate.RatePlanCode != null)
                    {
                        string roomType = roomRate.RoomTypeCode;
                        int coulmnNumber =
                            RoomRateUtil.GetRateCategoryByRatePlanCode(roomRate.RatePlanCode).CoulmnNumber;
                        if ((roomType == currentRoomType) || (null == currentRoomType))
                        {
                            if (!uniqueCoulmnID.Contains(coulmnNumber))
                            {
                                minRateInEachCategoryList.Add(roomRate);
                                currentRoomType = roomType;
                                currentCoulmnNumber = coulmnNumber;
                                uniqueCoulmnID.Add(coulmnNumber);
                            }
                        }
                        else
                        {
                            uniqueCoulmnID.Clear();
                            minRateInEachCategoryList.Add(roomRate);
                            currentRoomType = roomType;
                            currentCoulmnNumber = coulmnNumber;
                            uniqueCoulmnID.Add(coulmnNumber);
                        }
                    }
                }
            }
            filteredList = GetUniqueRateCodeFromEachCategory(minRateInEachCategoryList);
            return filteredList;
        }

        #endregion

        /// <summary>
        /// GetUniqueRateCodeFromEachCategory
        /// </summary>
        /// <param name="roomRates"></param>
        /// <returns>List of RoomRateEntity</returns>
        private List<RoomRateEntity> GetUniqueRateCodeFromEachCategory(List<RoomRateEntity> roomRates)
        {
            List<RoomRateEntity> filteredRoomRateList = new List<RoomRateEntity>();

            if (roomRates != null && roomRates.Count > 0)
            {
                foreach (RoomRateEntity roomRate in roomRates)
                {
                    if (roomRate != null)
                    {
                        string currentRateCategory =
                            RoomRateUtil.GetRateCategoryByRatePlanCode(roomRate.RatePlanCode).RateCategoryId;
                        string currentRatePlanCode = roomRate.RatePlanCode;
                        if (filteredRoomRateList.Count > 0)
                        {
                            bool rateCategoryExists = (filteredRoomRateList.Exists(delegate(RoomRateEntity rateEntity)
                                                                                       {
                                                                                           return
                                                                                               ((currentRateCategory ==
                                                                                                 RoomRateUtil.
                                                                                                     GetRateCategoryByRatePlanCode
                                                                                                     (rateEntity.
                                                                                                          RatePlanCode).
                                                                                                     RateCategoryId)
                                                                                                &&
                                                                                                (currentRatePlanCode !=
                                                                                                 rateEntity.RatePlanCode));
                                                                                       }));
                            if (!rateCategoryExists)
                            {
                                filteredRoomRateList.Add(roomRate);
                            }
                        }
                        else
                            filteredRoomRateList.Add(roomRate);
                    }
                }
            }

            return filteredRoomRateList;
        }

        

        #region SetMinMaxRate

        /// <summary>
        /// Reads the <code>minBaseRateInEachRateCategoryList</code> list and finds
        /// miniume rate, maximum rate of the all available room rates of the Hotel
        /// </summary>
        protected void SetMinMaxRate()
        {
            if (null != minBaseRateInEachRateCategoryList && minBaseRateInEachRateCategoryList.Count > 0)
            {
                minRate = minBaseRateInEachRateCategoryList[0].BaseRate;
                maxRate = minBaseRateInEachRateCategoryList[0].BaseRate;
                minRatePerStay = minBaseRateInEachRateCategoryList[0].TotalRate;
                maxRatePerStay = minBaseRateInEachRateCategoryList[0].TotalRate;
                foreach (RoomRateEntity roomRate in minBaseRateInEachRateCategoryList)
                {
                    if (roomRate.BaseRate.Rate < minRate.Rate)
                        minRate = roomRate.BaseRate;
                    if (roomRate.BaseRate.Rate > maxRate.Rate)
                        maxRate = roomRate.BaseRate;
                    if (roomRate.TotalRate.Rate < minRatePerStay.Rate)
                        minRatePerStay = roomRate.TotalRate;
                    if (roomRate.TotalRate.Rate > maxRatePerStay.Rate)
                        maxRatePerStay = roomRate.TotalRate;
                }
            }
            else if (roomRates != null && roomRates.Count > 0)
            {          
                minRate = roomRates[0].BaseRate;
                maxRate = roomRates[0].BaseRate;
                minRatePerStay = roomRates[0].TotalRate;
                maxRatePerStay = roomRates[0].TotalRate;
                foreach (RoomRateEntity currentRoomRate in roomRates)
                {
                    if (currentRoomRate.BaseRate.Rate < minRate.Rate)
                    {
                        minRate = currentRoomRate.BaseRate;
                    }
                    if (currentRoomRate.BaseRate.Rate > maxRate.Rate)
                    {
                        maxRate = currentRoomRate.BaseRate;
                    }
                    if (currentRoomRate.TotalRate.Rate < minRatePerStay.Rate)
                    {
                        minRatePerStay = currentRoomRate.TotalRate;
                    }
                    if (currentRoomRate.TotalRate.Rate > maxRatePerStay.Rate)
                    {
                        maxRatePerStay = currentRoomRate.TotalRate;
                    }
                }
            }
        }

        /// <summary>
        /// Sets the min max rate.
        /// </summary>
        /// <param name="sample">The sample.</param>
        protected void SetMinMaxRate(object sample)
        {
            if (rooms != null && rooms.Count > 0)
            {
                for (int i = 0; i < rooms.Count; i++)
                {
                    RoomEntity room = rooms[i];
                    if (
                        room.MinBaseRateInEachRateCategoryList != null &&
                        room.MinBaseRateInEachRateCategoryList.Count > 0 &&
                        room.MinBaseRateInEachRateCategoryList[0] != null &&
                        room.MinBaseRateInEachRateCategoryList[0].BaseRate != null &&
                        room.MinBaseRateInEachRateCategoryList[0].TotalRate != null
                        )
                    {
                        minRate = room.MinBaseRateInEachRateCategoryList[0].BaseRate;
                        maxRate = room.MinBaseRateInEachRateCategoryList[0].BaseRate;
                        minRatePerStay = room.MinBaseRateInEachRateCategoryList[0].TotalRate;
                        maxRatePerStay = room.MinBaseRateInEachRateCategoryList[0].TotalRate;
                        break;
                    }
                    else if (
                        room.RoomRates != null &&
                        room.RoomRates.Count > 0 &&
                        room.RoomRates[0] != null &&
                        room.RoomRates[0].BaseRate != null &&
                        room.RoomRates[0].TotalRate != null
                        )
                    {          
                        minRate = room.RoomRates[0].BaseRate;
                        maxRate = room.RoomRates[0].BaseRate;
                        minRatePerStay = room.RoomRates[0].TotalRate;
                        maxRatePerStay = room.RoomRates[0].TotalRate;
                        break;
                    }
                }
                foreach (RoomEntity room in rooms)
                {
                    if (null != room.MinBaseRateInEachRateCategoryList &&
                        room.MinBaseRateInEachRateCategoryList.Count > 0)
                    {
                        foreach (RoomRateEntity roomRate in room.MinBaseRateInEachRateCategoryList)
                        {
                            if (roomRate.BaseRate.Rate < minRate.Rate)
                            {
                                minRate = roomRate.BaseRate;
                            }
                            if (roomRate.BaseRate.Rate > maxRate.Rate)
                            {
                                maxRate = roomRate.BaseRate;
                            }
                            if (roomRate.TotalRate.Rate < minRatePerStay.Rate)
                            {
                                minRatePerStay = roomRate.TotalRate;
                            }
                            if (roomRate.TotalRate.Rate > maxRatePerStay.Rate)
                            {
                                maxRatePerStay = roomRate.TotalRate;
                            }
                        }
                    }
                    else if (room.RoomRates != null && room.RoomRates.Count > 0)
                    {
                        foreach (RoomRateEntity currentRoomRate in room.RoomRates)
                        {
                            if (currentRoomRate.BaseRate.Rate < minRate.Rate)
                            {
                                minRate = currentRoomRate.BaseRate;
                            }
                            if (currentRoomRate.BaseRate.Rate > maxRate.Rate)
                            {
                                maxRate = currentRoomRate.BaseRate;
                            }
                            if (currentRoomRate.TotalRate.Rate < minRatePerStay.Rate)
                            {
                                minRatePerStay = currentRoomRate.TotalRate;
                            }
                            if (currentRoomRate.TotalRate.Rate > maxRatePerStay.Rate)
                            {
                                maxRatePerStay = currentRoomRate.TotalRate;
                            }
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Sets the min max rate for each room.
        /// </summary>
        /// <param name="sample">The sample.</param>
        protected void SetMinMaxRateForEachRoom(object sample)
        {
            if (rooms != null && rooms.Count > 0)
            {
                for (int i = 0; i < rooms.Count; i++)
                {
                    RoomEntity room = rooms[i];

                    if (
                        rooms[i].MinBaseRateInEachRateCategoryList != null &&
                        rooms[i].MinBaseRateInEachRateCategoryList.Count > 0 &&
                        rooms[i].MinBaseRateInEachRateCategoryList[0] != null
                        )
                    {
                        room.MinRateForEachRoom = rooms[i].MinBaseRateInEachRateCategoryList[0].BaseRate;
                        room.MaxRateForEachRoom = rooms[i].MinBaseRateInEachRateCategoryList[0].BaseRate;

                        room.MinRatePerStayForEachRoom = rooms[i].MinBaseRateInEachRateCategoryList[0].TotalRate;
                        room.MaxRatePerStayForEachRoom = rooms[i].MinBaseRateInEachRateCategoryList[0].TotalRate;
                    }
                    else if (
                        rooms[i].RoomRates != null &&
                        rooms[i].RoomRates.Count > 0 &&
                        rooms[i].RoomRates[0] != null
                        )
                    {
                        room.MinRateForEachRoom = rooms[i].RoomRates[0].BaseRate;
                        room.MaxRateForEachRoom = rooms[i].RoomRates[0].BaseRate;
                        room.MinRatePerStayForEachRoom = rooms[i].RoomRates[0].TotalRate;
                        room.MaxRatePerStayForEachRoom = rooms[i].RoomRates[0].TotalRate;
                    }


                    if (null != room.MinBaseRateInEachRateCategoryList &&
                        room.MinBaseRateInEachRateCategoryList.Count > 0)
                    {
                        foreach (RoomRateEntity roomRate in room.MinBaseRateInEachRateCategoryList)
                        {
                            if (roomRate.BaseRate.Rate < room.MinRateForEachRoom.Rate)
                            {
                                room.MinRateForEachRoom = roomRate.BaseRate;
                            }
                            if (roomRate.BaseRate.Rate > room.MaxRateForEachRoom.Rate)
                            {
                                room.MaxRateForEachRoom = roomRate.BaseRate;
                            }
                            if (roomRate.TotalRate.Rate < room.MinRatePerStayForEachRoom.Rate)
                            {
                                room.MinRatePerStayForEachRoom = roomRate.TotalRate;
                            }
                            if (roomRate.TotalRate.Rate > room.MaxRatePerStayForEachRoom.Rate)
                            {
                                room.MaxRatePerStayForEachRoom = roomRate.TotalRate;
                            }
                        }
                    }
                    else if (room.RoomRates != null && room.RoomRates.Count > 0)
                    {
                        foreach (RoomRateEntity currentRoomRate in room.RoomRates)
                        {
                            if (currentRoomRate.BaseRate.Rate < room.MinRateForEachRoom.Rate)
                            {
                                room.MinRateForEachRoom = currentRoomRate.BaseRate;
                            }
                            if (currentRoomRate.BaseRate.Rate > room.MaxRateForEachRoom.Rate)
                            {
                                room.MaxRatePerStayForEachRoom = currentRoomRate.BaseRate;
                            }
                            if (currentRoomRate.TotalRate.Rate < room.MinRatePerStayForEachRoom.Rate)
                            {
                                room.MinRatePerStayForEachRoom = currentRoomRate.TotalRate;
                            }
                            if (currentRoomRate.TotalRate.Rate > room.MaxRatePerStayForEachRoom.Rate)
                            {
                                room.MaxRatePerStayForEachRoom = currentRoomRate.TotalRate;
                            }
                        }
                    }
                }
            }
        }

        #endregion

        

        /// <summary>
        /// Reads the list of rates from the <code>roomRates</code>, gets the corresponding rateCategory
        /// from the RoomRateUtil i.e, from CMS and sets the list of all RateCategories available for
        /// the corresponding hotel
        /// </summary>
        protected void SetAllRateCategories()
        {
            rateCategories = new List<RateCategory>();

            foreach (RatePlanEntity ratePlanEntity in ratePlans)
            {
                RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlanEntity.Code);
                if ((null != rateCategory) && (0 != rateCategory.CoulmnNumber))
                    rateCategories.Add(rateCategory);
            }
            
        }

        /// <summary>
        /// SetAllRateCategories
        /// </summary>
        /// <param name="sample"></param>
        protected void SetAllRateCategories(object sample)
        {
            foreach (RoomEntity room in rooms)
            {
                room.RateCategories = new List<RateCategory>();

                foreach (RatePlanEntity ratePlanEntity in room.RatePlans)
                {
                    RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlanEntity.Code);
                    if ((null != rateCategory) && (0 != rateCategory.CoulmnNumber))
                        room.RateCategories.Add(rateCategory);
                }
            }
        }

        /// <summary>
        /// Remove the bonus cheque rates, if any exsiting from the list
        /// </summary>
        /// <param name="allRateCategories">List of Rate Categories</param>
        protected void RemoveBonusChequeRates(List<string> allRateCategories)
        {
            string[] bonusChequeRateCategoryNames = RoomRateUtil.GetBonusChequeRateCategoryNames();
            for (int i = 0; i < allRateCategories.Count; i++)
            {
                string rateCategory = allRateCategories[i];
                if (StringUtil.IsStringInArray(bonusChequeRateCategoryNames, rateCategory))
                {
                    allRateCategories.Remove(rateCategory);
                }
            }
        }
    }
}