using System;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The RoomRateDetails subtype for the Bonuscheque search
    /// </summary>
    public class BonusChequeRoomRateDetails : BaseRoomRateDetails
    {
        #region Variables

        /// <summary>
        /// The Arrival Date user has searched for
        /// </summary>
        private DateTime arrivalDate;

        /// <summary>
        /// The Departure date user has searched for
        /// </summary>
        private DateTime departureDate;

        #endregion

        #region Properties

        public DateTime ArrivalDate
        {
            get { return arrivalDate; }
        }

        public DateTime DepartureDate
        {
            get { return departureDate; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// BonusChequeRoomRateDetails
        /// </summary>
        /// <param name="hotelDetails">BonusChequeHotelDetails</param>
        public BonusChequeRoomRateDetails(BonusChequeHotelDetails hotelDetails)
        {
            RoomTypes = hotelDetails.RoomTypes;
            CreateBaseRateRoomCategories(hotelDetails);
            this.arrivalDate = hotelDetails.ArrivalDate;
            this.departureDate = hotelDetails.DepartureDate;
            hotel = hotelDetails.HotelDestination;
            countryCode = hotelDetails.CountryCode;
        }

        /// <summary>
        /// BonusChequeRoomRateDetails
        /// </summary>
        /// <param name="hotelDetails">BonusChequeHotelDetails</param>
        /// <param name="roomNumber"></param>
        public BonusChequeRoomRateDetails(BonusChequeHotelDetails hotelDetails, int roomNumber)
        {
            RoomTypes = hotelDetails.Rooms[roomNumber].RoomTypes;
            minRateForEachRoom = hotelDetails.Rooms[roomNumber].MinRateForEachRoom;
            maxRateForEachRoom = hotelDetails.Rooms[roomNumber].MaxRateForEachRoom;
            minRatePerStayForEachRoom = hotelDetails.Rooms[roomNumber].MinRatePerStayForEachRoom;
            maxRatePerStayForEachRoom = hotelDetails.Rooms[roomNumber].MaxRatePerStayForEachRoom;
            CreateBaseRateRoomCategories(hotelDetails, roomNumber);
            this.arrivalDate = hotelDetails.ArrivalDate;
            this.departureDate = hotelDetails.DepartureDate;
            hotel = hotelDetails.HotelDestination;
            countryCode = hotelDetails.CountryCode;
        }

        #endregion
    }
}