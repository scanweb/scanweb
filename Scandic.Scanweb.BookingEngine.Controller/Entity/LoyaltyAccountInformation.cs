////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: This is the class, as all transactions are in the       //
//                                form of this object stored and passed between the layers//                                    //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Himansu Bhusan Senapati                                 //
//  Author email id				:                           							  //
//  Creation Date				: 30th November  2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Membership;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller.Entity
{

    #region class LoyaltyAccountInformation

    /// <summary>
    /// LoyaltyAccountInformation
    /// </summary>
    public class LoyaltyAccountInformation
    {
        #region Properties

        public LoyaltyCardInfo CardInfo { get; set; }

        public List<LoyaltyTransaction> LoyaltyTransactions { get; set; }

        private AvailabilityController availabilityController = null;

        /// <summary>
        /// Gets m_AvailabilityController
        /// </summary>
        private AvailabilityController m_AvailabilityController
        {
            get
            {
                if (availabilityController == null)
                {
                    availabilityController = new AvailabilityController();
                }
                return availabilityController;
            }
        }
        #endregion Properties

        #region Constructor Method

        /// <summary>
        /// LoyaltyAccountInformation
        /// </summary>
        /// <param name="response"></param>
        public LoyaltyAccountInformation(FetchMembershipTransactionsResponse response)
        {
            NameMembership nameMembership = response.MembershipTransactionList.CardInfo;

            LoyaltyCardInfo cardInfo = new LoyaltyCardInfo();
            cardInfo.MembershipNo = nameMembership.membershipNumber.ToString();
            cardInfo.Level = nameMembership.membershipLevel.ToString();
            cardInfo.NoOfPoints = nameMembership.currentPoints.ToString();
            cardInfo.NoOfStays = "0";
            this.CardInfo = cardInfo;
            MembershipTransaction[] membershipTransactions = response.MembershipTransactionList.MembershipTransaction;
            if (membershipTransactions != null)
            {
                List<LoyaltyTransaction> loyaltyTransactions = new List<LoyaltyTransaction>();
                LoyaltyTransaction loyaltyTransaction;
                MembershipTransaction membershipTransaction;
                int totalNoOfNightsStayed = 0;

                for (int transCtr = 0; transCtr < membershipTransactions.Length; transCtr++)
                {
                    membershipTransaction = new MembershipTransaction();
                    membershipTransaction = membershipTransactions[transCtr];

                    if (membershipTransaction.transactionTypeCode == TransactionTypeCode.ST ||
                        membershipTransaction.transactionTypeCode == TransactionTypeCode.OT)
                    {
                        loyaltyTransaction = new LoyaltyTransaction();

                        loyaltyTransaction.TransactionInfo = GetTransactionInfo(membershipTransaction);
                        loyaltyTransaction.FromDate = membershipTransaction.startDate;

                        if (membershipTransaction.endDateSpecified)
                            loyaltyTransaction.ToDate = membershipTransaction.endDate;
                        else
                            loyaltyTransaction.ToDate = membershipTransaction.postingDate;
                        loyaltyTransaction.Points = GetPoints(membershipTransaction);
                        if (IsStayQualifiedFor12Months(loyaltyTransaction))
                        {
                            if (membershipTransaction.Tsc != null)
                            {
                                totalNoOfNightsStayed += membershipTransaction.Tsc.baseNights + membershipTransaction.Tsc.bonusNights;
                            }
                        }

                        loyaltyTransactions.Add(loyaltyTransaction);
                    }
                }

                this.CardInfo.NoOfStays = totalNoOfNightsStayed.ToString();
                this.LoyaltyTransactions = loyaltyTransactions;
            }
        }

        #endregion Constructor Method

        #region Private Methods

        #region GetPoints

        /// <summary>
        /// GetPoints
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns>Points</returns>
        private string GetPoints(MembershipTransaction transaction)
        {
            string points = string.Empty;
            string transactionTypeCode = string.Empty;

            transactionTypeCode = transaction.transactionTypeCode;

            points = transaction.Points.Value.ToString();

            if (transactionTypeCode.ToUpper() == TransactionTypeCode.AW)
            {
                points = "-" + points;
            }

            return points;
        }

        #endregion GetPoints

        #region GetTransactionInfo

        /// <summary>
        /// GetTransactionInfo
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns>TransactionInfo</returns>
        private string GetTransactionInfo(MembershipTransaction membershipTransaction)
        {
            string transactionType = string.Empty;

            if (membershipTransaction.source != null)
            {

                transactionType = m_AvailabilityController.GetTransactionInfoFromCMS(membershipTransaction.source);
            }
            else
            {
                transactionType = string.Empty;
            }

            return transactionType;
        }

        #endregion GetTransactionInfo
        #region GetHotelName
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hotelCode"></param>
        /// <returns></returns>
        private string GetHotelName(string hotelCode)
        {
            return m_AvailabilityController.GetHotelNameFromCMS(hotelCode);
        }
        #endregion GetHotelName
        #region IsStayQualifiedFor12Months

        /// <summary>
        /// IsStayQualifiedFor12Months
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns>True/False</returns>
        private bool IsStayQualifiedFor12Months(LoyaltyTransaction transaction)
        {
            bool status = false;
            DateTime date12MonthsFromNow;

            date12MonthsFromNow = DateTime.Now.AddYears(-1);
            if (transaction.ToDate.Date >= date12MonthsFromNow.Date)
            {
                status = true;
            }

            return status;
        }

        #endregion isStayQualifiedFor12Months

        #endregion Private Methods
    }

    #endregion class LoyaltyAccountInformation

    #region Loyalty Transaction Comparers

    /// <summary>
    /// LoyaltyTransactionComparer
    /// </summary>
    public class LoyaltyTransactionComparer : IComparer<LoyaltyTransaction>
    {
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(LoyaltyTransaction first, LoyaltyTransaction second)
        {
            return first.FromDate.CompareTo(second.FromDate);
        }
    }

    #endregion Loyalty Transaction Comparers
}