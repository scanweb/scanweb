//  Description					:   RedemptionHotelDetails                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Membership;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The HotelDetails subtype for the search type Redemption search
    /// </summary>
    public class RedemptionHotelDetails : BaseHotelDetails
    {
        #region Variables

        /// <summary>
        /// The list of RateAwards as returned from FetchRateAwards of OWS
        /// </summary>
        private List<RateAward> rateAwards = new List<RateAward>();

        /// <summary>
        /// Minium points required to do the redemption booking
        /// </summary>
        protected RedeemptionPointsEntity minPoints;

        /// <summary>
        /// Maximum points required to do the redemption booking
        /// </summary>
        protected RedeemptionPointsEntity maxPoints;

        /// <summary>
        /// The Number of Nights user has searched for
        /// </summary>
        protected int numberOfNights;

        #endregion

        #region Properties

        /// <summary>
        /// Gets MinPoints
        /// </summary>
        public override RedeemptionPointsEntity MinPoints
        {
            get { return minPoints; }
        }

        /// <summary>
        /// Gets MaxPoints
        /// </summary>
        public override RedeemptionPointsEntity MaxPoints
        {
            get { return maxPoints; }
        }

        /// <summary>
        /// Gets MaxRate
        /// </summary>
        public override RateEntity MaxRate
        {
            get { return null; }
        }

        /// <summary>
        /// Gets MinRate
        /// </summary>
        public override RateEntity MinRate
        {
            get { return null; }
        }

        /// <summary>
        /// Gets MaxRatePerStay
        /// </summary>
        public override RateEntity MaxRatePerStay
        {
            get { return null; }
        }

        /// <summary>
        /// Gets MinRatePerStay
        /// </summary>
        public override RateEntity MinRatePerStay
        {
            get { return null; }
        }

        /// <summary>
        /// Gets IsAvailable
        /// </summary>
        public override bool IsAvailable
        {
            get
            {
                bool blnIsAvailable = true;
                foreach (RoomEntity room in rooms)
                {
                    if (null == room.MinBaseRateInEachRateCategoryList ||
                        room.MinBaseRateInEachRateCategoryList.Count <= 0)
                    {
                        blnIsAvailable = false;
                        break;
                    }
                }
                return blnIsAvailable;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor of RedemptionHotelDetails
        /// </summary>
        /// <param name="hotel">
        /// HotelDestination
        /// </param>
        /// <param name="countryCode">
        /// CountryCode
        /// </param>
        /// <param name="roomStay">
        /// RoomStay
        /// </param>
        /// <param name="allRateAwards">
        /// Array of RateAward
        /// </param>
        /// <param name="numberOfNights">
        /// Number of Nights user has searched for
        /// </param>
        public RedemptionHotelDetails(HotelDestination hotel, string countryCode, RoomStay roomStay,
                                      RateAward[] allRateAwards, int numberOfNights)
        {
            this.numberOfNights = numberOfNights;
            SetUpRedemptionHotelDetails(hotel, countryCode, roomStay, allRateAwards);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="listRoomStay"></param>
        /// <param name="allRateAwards"></param>
        /// <param name="numberOfNights"></param>
        public RedemptionHotelDetails(HotelDestination hotel, string countryCode, IList<RoomStay> listRoomStay,
                                      RateAward[] allRateAwards, int numberOfNights)
        {
            this.numberOfNights = numberOfNights;
            SetUpRedemptionHotelDetails(hotel, countryCode, listRoomStay, allRateAwards);
        }

        /// <summary>
        /// Setup the RedemptionHotelDetails Details
        /// </summary>
        /// <param name="hotel">
        /// IHotels
        /// </param>
        /// <param name="countryCode">
        /// CountryCode
        /// </param>
        /// <param name="roomStay">
        /// RoomStay
        /// </param>
        /// <param name="allRateAwards">
        /// Array of RateAward
        /// </param>
        /// <param name="alternateHotelsDistance">
        /// AlternateHotel Distance
        /// </param>
        private void SetUpRedemptionHotelDetails(HotelDestination hotel, string countryCode, RoomStay roomStay,
                                                 RateAward[] allRateAwards)
        {
            this.HotelDestination = hotel;
            this.CountryCode = countryCode;
            SetRateAwards(allRateAwards, hotel.OperaDestinationId);
            SetUpRoomRates(roomStay);
            SetUpMinBaseRateInEachRateCategory();
            SetMinMaxPoints();
            SetSelectedRatePlanes();
            SetAllRateCategories();
        }

        /// <summary>
        /// Sets up redemption hotel details.
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="listRoomStay"></param>
        /// <param name="allRateAwards"></param>
        private void SetUpRedemptionHotelDetails(HotelDestination hotel, string countryCode,
                                                 IList<RoomStay> listRoomStay,
                                                 RateAward[] allRateAwards)
        {
            this.HotelDestination = hotel;
            this.CountryCode = countryCode;
            SetRateAwards(allRateAwards, hotel.OperaDestinationId);
            SetUpRoomRates(listRoomStay);
            SetUpMinBaseRateInEachRateCategory(new object());
            SetMinMaxPoints(listRoomStay);
            SetMinMaxPointsForEachRoom(new object());
            SetSelectedRatePlans(new object());
            SetAllRateCategories(new object());
        }

        #endregion

        #region SetUpRoomRates

        /// <summary>
        /// Iterates through the list of RateAwards from OWS which contains the ratepaln, roomtype code 
        /// and sets the corresponding application lists if the following conditions are met
        /// 
        /// I. The RatePlan code should be configured in CMS
        /// II. The RoomType code should be configured in CMS
        /// III. For roomrates both the rateplan and roomtype should be configured in CMS
        /// </summary>
        /// <param name="roomStay">The <code>RoomStay</code> element from OWS</param>
        protected override void SetUpRoomRates(RoomStay roomStay)
        {
            int roomTypeSortOrder = 0;

            if (null != rateAwards)
            {
                foreach (RateAward rateAward in rateAwards)
                {
                    if (RoomRateUtil.IsRatePlanCodeInCMS(rateAward.rateCode))
                        this.RatePlans.Add(GetRatePlan(rateAward));
                }
            }
            if (null != roomStay.RoomTypes && null != rateAwards)
            {
                foreach (
                    ServiceProxies.Availability.RoomType roomType in roomStay.RoomTypes)
                {
                    foreach (RateAward rateAward in rateAwards)
                    {
                        if (RoomRateUtil.IsRoomTypeCodeInCMS(rateAward.roomCategory) &&
                            RoomRateUtil.IsRatePlanCodeInCMS(rateAward.rateCode) &&
                            rateAward.roomCategory == roomType.roomTypeCode)
                        {
                            roomTypeSortOrder += 1;
                            this.RoomTypes.Add(GetRoomType(roomType, roomTypeSortOrder));
                            this.RoomRates.Add(GetRoomRate(rateAward));
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Each implementation will return the different rate categories to be considered
        /// </summary>
        /// <param name="listRoomStay"></param>
        protected override void SetUpRoomRates(IList<RoomStay> listRoomStay)
        {
            foreach (RoomStay roomStay in listRoomStay)
            {
                RoomEntity room = new RoomEntity();
                int roomTypeSortOrder = 0;

                if (null != rateAwards)
                {
                    foreach (RateAward rateAward in rateAwards)
                    {
                        if (RoomRateUtil.IsRatePlanCodeInCMS(rateAward.rateCode))
                            room.RatePlans.Add(GetRatePlan(rateAward));
                    }
                }
                if (null != roomStay.RoomTypes && null != rateAwards)
                {
                    foreach (
                        ServiceProxies.Availability.RoomType roomType in
                            roomStay.RoomTypes)
                    {
                        foreach (RateAward rateAward in rateAwards)
                        {
                            if (RoomRateUtil.IsRoomTypeCodeInCMS(rateAward.roomCategory) &&
                                RoomRateUtil.IsRatePlanCodeInCMS(rateAward.rateCode) &&
                                rateAward.roomCategory == roomType.roomTypeCode)
                            {
                                roomTypeSortOrder += 1;
                                room.RoomTypes.Add(GetRoomType(roomType, roomTypeSortOrder));
                                room.RoomRates.Add(GetRoomRate(rateAward));
                            }
                        }
                    }
                }
                rooms.Add(room);
            }
        }

        #endregion
     
        #region SetMinMaxPoints

        /// <summary>
        /// Reads the <code>RoomRates</code> list and finds
        /// miniume Points, maximum Points of the all available room rates of the Hotel
        /// </summary>
        private void SetMinMaxPoints()
        {
            if (null != roomRates && roomRates.Count > 0)
            {
                minPoints = roomRates[0].PointsDetails;
                maxPoints = roomRates[0].PointsDetails;
                foreach (RoomRateEntity roomRate in minBaseRateInEachRateCategoryList)
                {
                    if (roomRate.PointsDetails.PointsRequired < minPoints.PointsRequired)
                        minPoints = roomRate.PointsDetails;
                    if (roomRate.PointsDetails.PointsRequired > maxPoints.PointsRequired)
                        maxPoints = roomRate.PointsDetails;
                }
            }
        }


        /// <summary>
        /// Sets the min max points.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>
        private void SetMinMaxPoints(object sample)
        {
            if (rooms != null && rooms.Count > 0)
            {
                if (
                    rooms[0].RoomRates != null &&
                    rooms[0].RoomRates.Count > 0 &&
                    rooms[0].RoomRates[0] != null
                    )
                {
                    minPoints = rooms[0].RoomRates[0].PointsDetails;
                    maxPoints = rooms[0].RoomRates[0].PointsDetails;

                    foreach (RoomEntity room in rooms)
                    {
                        if (null != room.RoomRates && room.RoomRates.Count > 0)
                        {
                            foreach (RoomRateEntity roomRate in room.MinBaseRateInEachRateCategoryList)
                            {
                                if (roomRate.PointsDetails.PointsRequired < minPoints.PointsRequired)
                                    minPoints = roomRate.PointsDetails;
                                if (roomRate.PointsDetails.PointsRequired > maxPoints.PointsRequired)
                                    maxPoints = roomRate.PointsDetails;
                            }
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Sets the min max points for each room.
        /// </summary>
        /// <param name="sample">The sample.</param>
        private void SetMinMaxPointsForEachRoom(object sample)
        {
            if (rooms != null && rooms.Count > 0)
            {
                for (int i = 0; i < rooms.Count; i++)
                {
                    RoomEntity room = rooms[i];

                    if (
                        rooms[i].RoomRates != null &&
                        rooms[i].RoomRates.Count > 0 &&
                        rooms[i].RoomRates[0] != null
                        )
                    {
                        room.MinPointsForEachRoom = rooms[i].RoomRates[0].PointsDetails;
                        room.MaxPointsForEachRoom = rooms[i].RoomRates[0].PointsDetails;
                    }


                    if (null != room.RoomRates && room.RoomRates.Count > 0)
                    {
                        foreach (RoomRateEntity roomRate in room.MinBaseRateInEachRateCategoryList)
                        {
                            if (roomRate.PointsDetails.PointsRequired < room.MinPointsForEachRoom.PointsRequired)
                                room.MinPointsForEachRoom = roomRate.PointsDetails;
                            if (roomRate.PointsDetails.PointsRequired > room.MaxPointsForEachRoom.PointsRequired)
                                room.MaxPointsForEachRoom = roomRate.PointsDetails;
                        }
                    }
                }
            }
        }

        #endregion

        #region SetRateAwards

        /// <summary>
        /// As the RateAwards passed may contain rateAwards for different hotels, this method
        /// will filter and sets the RateAwards which are specific to the hotel we are considering
        /// </summary>
        /// <param name="allRateAwards">The RateAwards from OWS</param>
        /// <param name="hotelCode">The Hotel code being searched for</param>
        private void SetRateAwards(RateAward[] allRateAwards, string hotelCode)
        {
            if (null != rateAwards)
            {
                foreach (RateAward rateAward in allRateAwards)
                {
                    if (hotelCode == rateAward.resort)
                        rateAwards.Add(rateAward);
                }
            }
        }

        #endregion

        #region OWS Room and Rate Utility methods

        /// <summary>
        /// Creates the application specific RatePlanEntity from the RateAward returned from OWS
        /// </summary>
        /// <param name="rateAward">The RateAward from OWS</param>
        /// <returns>The application specific RatePlanEntity</returns>
        private RatePlanEntity GetRatePlan(RateAward rateAward)
        {
            RatePlanEntity ratePlan = new RatePlanEntity(rateAward.rateCode, rateAward.RateDescription);
            return ratePlan;
        }

        /// <summary>
        /// Creates the application specific RoomRateEntity from the RateAwards returned from OWS
        /// </summary>
        /// <param name="rateAward">The RateAward from OWS</param>
        /// <returns>The application specific RoomRateEntity</returns>
        private RoomRateEntity GetRoomRate(RateAward rateAward)
        {
            RoomRateEntity roomRate = new RoomRateEntity(rateAward.roomCategory, rateAward.rateCode);
            roomRate.PointsDetails = new RedeemptionPointsEntity((rateAward.points_required)/numberOfNights,
                                                                 rateAward.awardType);
            return roomRate;
        }

        #endregion

        #region Methods to read minimum rate in each rate category
        /// <summary>
        /// Sets up min base rate in each category.
        /// </summary>
        protected void SetUpMinBaseRateInEachRateCategory()
        {
            roomRates.Sort(new CompositeMinPointsComparer());
            minBaseRateInEachRateCategoryList = PickMinRateInEachRateCategory(roomRates);
        }

        /// <summary>
        /// Sets up min base rate in each rate category.
        /// </summary>
        /// <param name="sample"></param>
        protected void SetUpMinBaseRateInEachRateCategory(object sample)
        {
            foreach (RoomEntity room in rooms)
            {
                room.RoomRates.Sort(new CompositeMinPointsComparer());
                room.MinBaseRateInEachRateCategoryList = PickMinRateInEachRateCategory(room.RoomRates);
            }
        }

        #endregion

        /// <summary>
        /// Iterates through the sorted <code>roomRates</code> list, picks the first element
        /// of the RoomType, RateCategory sorted value and addes to a new List and returns this
        /// list
        /// Assumption is the roomRates entity will be sorted by RoomType, RateCategory
        /// 
        /// For example if the room rates are sorted and send as follows
        /// FF => EA1 => 110 SEK
        /// FF => EA2 => 220 SEK
        /// TR => RA2 => 200 SEK
        /// TR => RA3 => 210 SEK
        /// TR => RA1 => 400 SEK
        /// ................
        /// ................
        /// 
        /// This method will pick the first in each RoomType and RateCategory i.e,
        /// FF => EA1 => 110 SEK
        /// TR => RA2 => 200 SEK
        /// and returns only these elements added to a new list and returns
        /// 
        /// One more important thing is that this method will only consider any element to addition
        /// to the final list if and only if the corresponding element has the ratePlanCode
        /// which is defined as the rates to be considered
        /// </summary>
        /// <returns></returns>
        private new List<RoomRateEntity> PickMinRateInEachRateCategory(List<RoomRateEntity> roomRates)
        {
            List<RoomRateEntity> minRateInEachCategoryList = new List<RoomRateEntity>();
            string currentRoomType = null;
            string currentRateCategory = null;
            foreach (RoomRateEntity roomRate in roomRates)
            {
                string roomType = roomRate.RoomTypeCode;
                string rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(roomRate.RatePlanCode).RateCategoryId;
                if ((roomType != currentRoomType || rateCategory != currentRateCategory))
                {
                    minRateInEachCategoryList.Add(roomRate);
                    currentRoomType = roomType;
                    currentRateCategory = rateCategory;
                }
            }
            return minRateInEachCategoryList;
        }
    }
}