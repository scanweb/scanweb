#region Description

////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: This is the hotel room rate details                     //
//                                created when user do a block code booking.              //
//	                              This is resulted in Accomodation rate block.           //                        
//----------------------------------------------------------------------------------------//
/// Author						: Raj Kishore Marandi	                                  //
/// Author email id				:                           							  //
/// Creation Date				: 08th September 2008									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revision History			: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#endregion Description

using System.Collections.Generic;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    public class BlockCodeRoomRateDetails : RegularRoomRateDetails
    {
        /// <summary>
        /// Is this block is bookable
        /// </summary>
        private bool isTrueBlockRates;

        public bool IsRateNotConfigured { get; set; }

        public bool IsRateCodeNotAssociated { get; set; }

        public bool IsBlockRates
        {
            get { return isTrueBlockRates; }
        }

        /// <summary>
        /// BlockCodeRoomRateDetails
        /// </summary>
        /// <param name="blockCodeHotelDeails">BlockCodeHotelDetails</param>
        public BlockCodeRoomRateDetails(BlockCodeHotelDetails blockCodeHotelDeails) : base(blockCodeHotelDeails)
        {
            if (blockCodeHotelDeails.RatePlans != null && blockCodeHotelDeails.RatePlans.Count == 0)
            {
                IsRateCodeNotAssociated = true;
            }
            if (blockCodeHotelDeails.RoomRates != null && blockCodeHotelDeails.RoomRates.Count > 0)
            {
                if (blockCodeHotelDeails.RatePlans != null && blockCodeHotelDeails.RatePlans.Count > 1)
                {
                    CreateBaseRateRoomCategories(blockCodeHotelDeails);
                }
                else
                {
                    CreateRateRoomCategories(blockCodeHotelDeails);
                }
            }
            else
            {
                IsRateNotConfigured = true;
            }
            this.isTrueBlockRates = blockCodeHotelDeails.IsBlockRates;
        }

        /// <summary>
        /// Block Code Room Rate Details
        /// </summary>
        /// <param name="blockCodeHotelDeails">BlockCodeHotelDetails</param>
        /// <param name="roomNumber">roomNumber</param>
        public BlockCodeRoomRateDetails(BlockCodeHotelDetails blockCodeHotelDeails, int roomNumber)
            : base(blockCodeHotelDeails, roomNumber)
        {
            if (blockCodeHotelDeails.Rooms[roomNumber].RatePlans != null &&
                blockCodeHotelDeails.Rooms[roomNumber].RatePlans.Count == 0)
            {
                IsRateCodeNotAssociated = true;
            }
            if (blockCodeHotelDeails.Rooms[roomNumber].RoomRates != null &&
                blockCodeHotelDeails.Rooms[roomNumber].RoomRates.Count > 0)
            {
                if (blockCodeHotelDeails.Rooms[roomNumber].RatePlans != null &&
                    blockCodeHotelDeails.Rooms[roomNumber].RatePlans.Count > 1)
                {
                    CreateBaseRateRoomCategories(blockCodeHotelDeails, roomNumber);
                }
                else
                {
                    CreateRateRoomCategories(blockCodeHotelDeails, roomNumber);
                }
            }
            else
            {
                IsRateNotConfigured = true;
            }
            this.isTrueBlockRates = blockCodeHotelDeails.IsBlockRates;
        }

        /// <summary>
        /// CreateRateRoomCategories
        /// </summary>
        /// <param name="blockCodeHotelDeails">BlockCodeHotelDetails</param>
        private void CreateRateRoomCategories(BlockCodeHotelDetails blockCodeHotelDeails)
        {
            List<RoomCategoryEntity> rowWiseRoomRateDetails = new List<RoomCategoryEntity>();
            RoomCategoryEntity roomCategory;

            foreach (RoomRateEntity roomRate in blockCodeHotelDeails.RoomRates)
            {
                roomCategory = new RoomCategoryEntity();
                string roomCode = roomRate.RoomTypeCode;
                string roomCategoryName = RoomRateUtil.GetRoomCategory(roomCode).RoomCategoryId;
                SetRoomDetails(roomCategory, roomCode);
                AddRoomRate(roomCategory, roomRate);
                rowWiseRoomRateDetails.Add(roomCategory);
            }
            for (int counter = 0; counter < rowWiseRoomRateDetails.Count; counter++)
            {
                RoomCategoryEntity outerRoomCategory = rowWiseRoomRateDetails[counter];
                for (int innerCounter = counter + 1; innerCounter < rowWiseRoomRateDetails.Count; innerCounter++)
                {
                    RoomCategoryEntity innerRoomCategory = rowWiseRoomRateDetails[innerCounter];
                    if (outerRoomCategory.MergeRoomCateogryEntityWithBaseRate(innerRoomCategory))
                    {
                        rowWiseRoomRateDetails.RemoveAt(innerCounter);
                        innerCounter = innerCounter - 1;
                    }
                }
            }
            this.baseRateRoomCategories = rowWiseRoomRateDetails;
        }

        /// <summary>
        /// CreateRateRoomCategories
        /// </summary>
        /// <param name="blockCodeHotelDeails">BlockCodeHotelDetails</param>
        /// <param name="roomNumber">roomNumber</param>
        private void CreateRateRoomCategories(BlockCodeHotelDetails blockCodeHotelDeails, int roomNumber)
        {
            List<RoomCategoryEntity> rowWiseRoomRateDetails = new List<RoomCategoryEntity>();
            RoomCategoryEntity roomCategory;

            foreach (RoomRateEntity roomRate in blockCodeHotelDeails.Rooms[roomNumber].RoomRates)
            {
                roomCategory = new RoomCategoryEntity();
                string roomCode = roomRate.RoomTypeCode;
                string roomCategoryName = RoomRateUtil.GetRoomCategory(roomCode).RoomCategoryId;
                SetRoomDetails(roomCategory, roomCode);
                AddRoomRate(roomCategory, roomRate);
                rowWiseRoomRateDetails.Add(roomCategory);
            }
            for (int counter = 0; counter < rowWiseRoomRateDetails.Count; counter++)
            {
                RoomCategoryEntity outerRoomCategory = rowWiseRoomRateDetails[counter];
                for (int innerCounter = counter + 1; innerCounter < rowWiseRoomRateDetails.Count; innerCounter++)
                {
                    RoomCategoryEntity innerRoomCategory = rowWiseRoomRateDetails[innerCounter];
                    if (outerRoomCategory.MergeRoomCateogryEntityWithBaseRate(innerRoomCategory))
                    {
                        rowWiseRoomRateDetails.RemoveAt(innerCounter);
                        innerCounter = innerCounter - 1;
                    }
                }
            }
            this.baseRateRoomCategories = rowWiseRoomRateDetails;
        }
    }
}