﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Scanweb.BookingEngine.Controller.Entity
{
    public class CampaignPeriods
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Points { get; set; }
    }
}
