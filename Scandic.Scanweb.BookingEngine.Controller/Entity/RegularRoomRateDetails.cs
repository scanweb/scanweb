namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The RoomRateDetails object for the Regular Search
    /// </summary>
    public class RegularRoomRateDetails : BaseRoomRateDetails
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotelDetails"></param>
        public RegularRoomRateDetails(RegularHotelDetails hotelDetails)
        {
            RoomTypes = hotelDetails.RoomTypes;

            CreateBaseRateRoomCategories(hotelDetails);
            hotel = hotelDetails.HotelDestination;
            countryCode = hotelDetails.CountryCode;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="roomNumber"></param>
        public RegularRoomRateDetails(RegularHotelDetails hotelDetails, int roomNumber)
        {
            RoomTypes = hotelDetails.Rooms[roomNumber].RoomTypes;

            minRateForEachRoom = hotelDetails.Rooms[roomNumber].MinRateForEachRoom;
            maxRateForEachRoom = hotelDetails.Rooms[roomNumber].MaxRateForEachRoom;
            minRatePerStayForEachRoom = hotelDetails.Rooms[roomNumber].MinRatePerStayForEachRoom;
            maxRatePerStayForEachRoom = hotelDetails.Rooms[roomNumber].MaxRatePerStayForEachRoom;

            CreateBaseRateRoomCategories(hotelDetails, roomNumber);
            hotel = hotelDetails.HotelDestination;
            countryCode = hotelDetails.CountryCode;
        }

        #endregion
    }
}