//  Description					: NegotiatedHotelDetails Entity     			          //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                                                         //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				:                                                         //
///	Last Modified Date			:                                                         //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The HotelDetails subtype for the search type Negotiated search
    /// </summary>
    public class NegotiatedHotelDetails : BaseHotelDetails
    {
        #region Variables

        /// <summary>
        /// The Negotiated rate code entered by the user
        /// </summary>
        private string negotiatedCode;

        /// <summary>
        /// The comapany name corresponding the negotiated rate code returned from OWS
        /// </summary>
        private string companyName;

        /// <summary>
        /// The list of negotiated rate plan codes configured and returned for the <code>negotiatedCode</code>
        /// </summary>
        private string[] negotiatedRatePlanCodes;

        /// <summary>
        /// The Negotiated room Rate list i.e., the RoomRateEntities having one of the <code>negotiatedRatePlanCodes</code>
        /// </summary>
        private List<RoomRateEntity> negotiatedRoomRates;

        /// <summary>
        /// The Non-Negotiated Room rate list i.e., the public rates
        /// </summary>
        private List<RoomRateEntity> publicRoomRates;

        #endregion

        #region Properties

        public override RateEntity MaxRate
        {
            get { return maxRate; }
        }

        /// <summary>
        /// Gets NegotiatedRatePlanCodes 
        /// </summary>
        public string[] NegotiatedRatePlanCodes
        {
            get { return negotiatedRatePlanCodes; }
        }

        /// <summary>
        /// Gets MinRate
        /// </summary>
        public override RateEntity MinRate
        {
            get { return minRate; }
        }

        /// <summary>
        /// Gets MaxRatePerStay
        /// </summary>
        public override RateEntity MaxRatePerStay
        {
            get { return maxRatePerStay; }
        }

        /// <summary>
        /// Gets MinRatePerStay
        /// </summary>
        public override RateEntity MinRatePerStay
        {
            get { return minRatePerStay; }
        }

        /// <summary>
        /// Gets HasNegotiateRoomRates
        /// </summary>
        public bool HasNegotiateRoomRates
        {
            get { return (null != negotiatedRoomRates && negotiatedRoomRates.Count > 0); }
        }

        /// <summary>
        /// Gets <see cref="BaseHotelDetails#IsAvailable"/>
        /// </summary>
        public override bool IsAvailable
        {
            get
            {
                bool blnIsAvailable = true;
                foreach (RoomEntity room in rooms)
                {
                    if (null == room.MinBaseRateInEachRateCategoryList ||
                        room.MinBaseRateInEachRateCategoryList.Count <= 0)
                    {
                        blnIsAvailable = false;
                        break;
                    }
                }
                return blnIsAvailable;
            }
        }

        /// <summary>
        /// The company name as returned from OWS
        /// </summary>
        public string CompanyName
        {
            get { return companyName; }
            set { companyName = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor of NegotiatedHotelDetails
        /// </summary>
        /// <param name="hotel">
        /// HotelDestination
        /// </param>
        /// <param name="countryCode">
        /// CountryCode
        /// </param>
        /// <param name="roomStay">
        /// RoomStay
        /// </param>
        /// <param name="negotiatedCode">
        /// NegotiatedCode
        /// </param>        
        public NegotiatedHotelDetails(HotelDestination hotel, string countryCode,
                                      RoomStay roomStay, string negotiatedCode)
        {
            SetUpNegotiatedHotelDetails(hotel, countryCode, roomStay, negotiatedCode);
        }

        /// <summary>
        /// This method sets up NegotiatedHotelDetails.
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="listRoomStay"></param>
        /// <param name="negotiatedCode"></param>
        public NegotiatedHotelDetails(HotelDestination hotel, string countryCode, IList<RoomStay> listRoomStay,
                                      string negotiatedCode)
        {
            SetUpNegotiatedHotelDetails(hotel, countryCode, listRoomStay, negotiatedCode);
        }

        /// <summary>
        /// Setup Negotiated HotelDetails
        /// </summary>
        /// <param name="hotel">
        /// HotelDestination
        /// </param>
        /// <param name="countryCode">
        /// CountryCode
        /// </param>
        /// <param name="roomStay">
        /// RoomStay
        /// </param>
        /// <param name="negotiatedCode">
        /// NegotiatedCode
        /// </param>
        /// <param name="alternateHotelsDistance">
        /// AlternateHotelsDistance
        /// </param>
        private void SetUpNegotiatedHotelDetails(HotelDestination hotel, string countryCode, RoomStay roomStay,
                                                 string negotiatedCode)
        {
            this.HotelDestination = hotel;
            this.CountryCode = countryCode;
            this.negotiatedCode = negotiatedCode;
            this.negotiatedRatePlanCodes = GetNegotiatedRates(roomStay.RatePlans, negotiatedCode);
            this.companyName = GetCompanyName(roomStay.RatePlans, negotiatedCode);
            SetUpRoomRates(roomStay);
            SetUpMinBaseRateInEachRateCategory();
            SetUpMinTotalRateInEachRateCategory();
            SetMinMaxRate();
            SetSelectedRatePlanes();
            SetAllRateCategories();
        }

        /// <summary>
        /// This method sets up NegotiatedHotelDetails.
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="listRoomStay"></param>
        /// <param name="negotiatedCode"></param>
        private void SetUpNegotiatedHotelDetails(HotelDestination hotel, string countryCode,
                                                 IList<RoomStay> listRoomStay,
                                                 string negotiatedCode)
        {
            this.HotelDestination = hotel;
            this.CountryCode = countryCode;
            this.negotiatedCode = negotiatedCode;
            this.negotiatedRatePlanCodes = GetNegotiatedRates(listRoomStay, negotiatedCode);
            this.companyName = GetCompanyName(listRoomStay, negotiatedCode);
            SetUpRoomRates(listRoomStay);
            SetUpMinBaseRateInEachRateCategory(new object());
            SetUpMinTotalRateInEachRateCategory(new object());
            SetMinMaxRate(new object());
            SetMinMaxRateForEachRoom(new object());
            SetSelectedRatePlans(new object());
            SetAllRateCategories(new object());
        }

        #endregion

        #region SetUpRoomRates

        /// <summary>
        /// Iterates through the list of rateplans, roomtypes, roomRates from OWS and sets the corresponding
        /// application lists if the following conditions are met
        /// 
        /// I. The RatePlan code should be configured in CMS
        /// II. The RoomType code should be configured in CMS
        /// III. For roomrates both the rateplan and roomtype should be configured in CMS
        /// IV. The ratePlan code either belongs to an Early, Flex rate category or the rateplan code
        /// is the negotiated rate plan code returned as part of the response
        /// </summary>
        /// <param name="roomStay">The <code>RoomStay</code> element from OWS</param>
        protected override void SetUpRoomRates(RoomStay roomStay)
        {
            int roomTypeSortOrder = 0;
            string[] bonusChequeRates = RoomRateUtil.GetBonusChequeRateCodes();
            if (null != roomStay.RatePlans)
            {
                foreach (RatePlan ratePlan in roomStay.RatePlans)
                {
                    string ratePlanCode = ratePlan.ratePlanCode;
                    if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlanCode))
                    {
                        string rateCategoryName =
                            RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlan.ratePlanCode).RateCategoryId;
                        if (!StringUtil.IsStringInArray(bonusChequeRates, rateCategoryName))
                            this.RatePlans.Add(GetRatePlan(ratePlan));
                    }
                }
            }
            if (null != roomStay.RoomTypes)
            {
                foreach (
                    ServiceProxies.Availability.RoomType roomType in roomStay.RoomTypes)
                {
                    if (RoomRateUtil.IsRoomTypeCodeInCMS(roomType.roomTypeCode))
                    {
                        roomTypeSortOrder += 1;
                        this.RoomTypes.Add(GetRoomType(roomType, roomTypeSortOrder));
                    }
                }
            }
            if (null != roomStay.RoomRates)
            {
                negotiatedRoomRates = new List<RoomRateEntity>();
                publicRoomRates = new List<RoomRateEntity>();
                foreach (RoomRate roomRate in roomStay.RoomRates)
                {
                    string ratePlanCode = roomRate.ratePlanCode;

                    if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlanCode) &&
                        RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode))
                    {
                        string rateCategoryName =
                            RoomRateUtil.GetRateCategoryByRatePlanCode(roomRate.ratePlanCode).RateCategoryId;
                        RoomRateEntity roomRateEntity = GetRoomRate(roomRate);
                        if (Core.StringUtil.IsStringInArray(negotiatedRatePlanCodes, ratePlanCode))
                        {
                            roomRateEntity.IsSpecialRate = true;
                            negotiatedRoomRates.Add(roomRateEntity);
                            roomRates.Add(roomRateEntity);
                        }
                        else if (!StringUtil.IsStringInArray(bonusChequeRates, rateCategoryName))
                        {
                            publicRoomRates.Add(roomRateEntity);
                            roomRates.Add(roomRateEntity);
                        }
                    }
                }
            }
        }


        protected override void SetUpRoomRates(IList<RoomStay> listRoomStay)
        {
            foreach (RoomStay roomStay in listRoomStay)
            {
                RoomEntity room = new RoomEntity();

                int roomTypeSortOrder = 0;
                string[] bonusChequeRates = RoomRateUtil.GetBonusChequeRateCodes();
                if (null != roomStay.RatePlans)
                {
                    foreach (RatePlan ratePlan in roomStay.RatePlans)
                    {
                        string ratePlanCode = ratePlan.ratePlanCode;
                        if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlanCode))
                        {
                            string rateCategoryName =
                                RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlan.ratePlanCode).RateCategoryId;
                            if (!StringUtil.IsStringInArray(bonusChequeRates, rateCategoryName))
                            {
                                room.RatePlans.Add(GetRatePlan(ratePlan));
                            }
                        }
                    }
                }
                if (null != roomStay.RoomTypes)
                {
                    foreach (
                        ServiceProxies.Availability.RoomType roomType in
                            roomStay.RoomTypes)
                    {
                        if (RoomRateUtil.IsRoomTypeCodeInCMS(roomType.roomTypeCode))
                        {
                            roomTypeSortOrder += 1;
                            room.RoomTypes.Add(GetRoomType(roomType, roomTypeSortOrder));
                        }
                    }
                }
                if (null != roomStay.RoomRates)
                {
                    negotiatedRoomRates = new List<RoomRateEntity>();
                    publicRoomRates = new List<RoomRateEntity>();
                    foreach (RoomRate roomRate in roomStay.RoomRates)
                    {
                        string ratePlanCode = roomRate.ratePlanCode;

                        if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlanCode) &&
                            RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode))
                        {
                            string rateCategoryName =
                                RoomRateUtil.GetRateCategoryByRatePlanCode(roomRate.ratePlanCode).RateCategoryId;
                            RoomRateEntity roomRateEntity = GetRoomRate(roomRate);
                            if (Core.StringUtil.IsStringInArray(negotiatedRatePlanCodes, ratePlanCode))
                            {
                                roomRateEntity.IsSpecialRate = true;
                                negotiatedRoomRates.Add(roomRateEntity);
                                room.RoomRates.Add(roomRateEntity);
                            }
                            else if (!StringUtil.IsStringInArray(bonusChequeRates, rateCategoryName))
                            {
                                publicRoomRates.Add(roomRateEntity);
                                room.RoomRates.Add(roomRateEntity);
                            }
                        }
                    }
                }

                rooms.Add(room);
            }
        }

        #endregion

        #region GetNegotiatedRates

        /// <summary>
        /// The list of negotiated rates will be returned. 
        /// 
        /// Each RatePlan will have the tags as QualifyingIDType & QualifyingIdValue
        /// if this tag is exisiting in the RatePlan values then it is added to the list
        /// </summary>
        /// <param name="ratePlans">The list of RatePlan objects to be searched for</param>
        /// <param name="corporateCode">The Negotiated code passed earlier the OWS Request</param>
        /// <returns>List of Negotiated rates returned from the OWS</returns>
        private string[] GetNegotiatedRates(RatePlan[] ratePlans, string negotiatedCode)
        {
            List<string> rateCodes = new List<string>();

            if (null != ratePlans)
            {
                for (int i = 0; i < ratePlans.Length; i++)
                {
                    RatePlan ratePlan = ratePlans[i];
                    if ((ratePlan.qualifyingIdType == AppConstants.QUALIFYING_TYPE_CORPORATE ||
                         ratePlan.qualifyingIdType == AppConstants.QUALIFYING_TYPE_COMPANY ||
                         ratePlan.qualifyingIdType == AppConstants.QUALIFYING_TYPE_TRAVEL_AGENT) &&
                        ratePlan.qualifyingIdValue == negotiatedCode)
                    {
                        rateCodes.Add(ratePlan.ratePlanCode);
                    }
                }
            }
            return rateCodes.ToArray();
        }


        /// <summary>
        /// Gets the negotiated rates.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>
        /// <param name="negotiatedCode">The negotiated code.</param>
        /// <returns></returns>
        private string[] GetNegotiatedRates(IList<RoomStay> listRoomStay, string negotiatedCode)
        {
            List<string> rateCodes = new List<string>();
            foreach (RoomStay roomStay in listRoomStay)
            {
                RatePlan[] ratePlans = roomStay.RatePlans;
                if (null != ratePlans)
                {
                    for (int i = 0; i < ratePlans.Length; i++)
                    {
                        RatePlan ratePlan = ratePlans[i];
                        if ((ratePlan.qualifyingIdType == AppConstants.QUALIFYING_TYPE_CORPORATE ||
                             ratePlan.qualifyingIdType == AppConstants.QUALIFYING_TYPE_COMPANY ||
                             ratePlan.qualifyingIdType == AppConstants.QUALIFYING_TYPE_TRAVEL_AGENT) &&
                            ratePlan.qualifyingIdValue == negotiatedCode)
                        {
                            rateCodes.Add(ratePlan.ratePlanCode);
                        }
                    }
                }
            }
            return rateCodes.ToArray();
        }

        #endregion GetNegotiatedRates

        #region GetCompanyName

        /// <summary>
        /// Iterates throught the list of <code>RatePlan</code> returned from OWS
        /// If the rate plan belongs to the negotiatedCode entered by the user
        /// then the additional details "Miscellaneous" which contains the company name
        /// is returned
        /// </summary>
        /// <param name="ratePlans">The List of RatePlans as returned from OWS</param>
        /// <param name="negotiatedCode">The Negotiated code entered by user</param>
        /// <returns></returns>
        private string GetCompanyName(RatePlan[] ratePlans, string negotiatedCode)
        {
            string companyName = null;
            if (null != ratePlans)
            {
                for (int i = 0; i < ratePlans.Length; i++)
                {
                    RatePlan ratePlan = ratePlans[i];
                    if ((ratePlan.qualifyingIdType == AppConstants.QUALIFYING_TYPE_CORPORATE ||
                         ratePlan.qualifyingIdType == AppConstants.QUALIFYING_TYPE_COMPANY ||
                         ratePlan.qualifyingIdType == AppConstants.QUALIFYING_TYPE_TRAVEL_AGENT) &&
                        ratePlan.qualifyingIdValue == negotiatedCode)
                    {
                        AdditionalDetail[] details = ratePlan.AdditionalDetails;
                        foreach (AdditionalDetail detail in details)
                        {
                            if (AdditionalDetailType.Miscellaneous == detail.detailType)
                            {
                                companyName = (detail.AdditionalDetailDescription.Items[0] as Text).Value;
                                break;
                            }
                        }
                        if (null != companyName)
                            break;
                    }
                }
            }
            return companyName;
        }


        /// <summary>
        /// Gets the name of the company.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>
        /// <param name="negotiatedCode">The negotiated code.</param>
        /// <returns></returns>
        private string GetCompanyName(IList<RoomStay> listRoomStay, string negotiatedCode)
        {
            string companyName = null;
            foreach (RoomStay roomStay in listRoomStay)
            {
                RatePlan[] ratePlans = roomStay.RatePlans;
                if (null != ratePlans)
                {
                    for (int i = 0; i < ratePlans.Length; i++)
                    {
                        RatePlan ratePlan = ratePlans[i];
                        if ((ratePlan.qualifyingIdType == AppConstants.QUALIFYING_TYPE_CORPORATE ||
                             ratePlan.qualifyingIdType == AppConstants.QUALIFYING_TYPE_COMPANY ||
                             ratePlan.qualifyingIdType == AppConstants.QUALIFYING_TYPE_TRAVEL_AGENT) &&
                            ratePlan.qualifyingIdValue == negotiatedCode)
                        {
                            AdditionalDetail[] details = ratePlan.AdditionalDetails;
                            foreach (AdditionalDetail detail in details)
                            {
                                if (AdditionalDetailType.Miscellaneous == detail.detailType)
                                {
                                    companyName = (detail.AdditionalDetailDescription.Items[0] as Text).Value;
                                    break;
                                }
                            }
                            if (null != companyName)
                                break;
                        }
                    }
                }
            }
            return companyName;
        }

        #endregion
    }
}