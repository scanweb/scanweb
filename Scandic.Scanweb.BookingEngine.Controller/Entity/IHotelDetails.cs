//  Description					:   IHotelDetails                                        //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The interface containing the list of methods which will return
    /// the various detials related to the Hotel, this interface is implemented
    /// for each kind of search type
    /// </summary>
    public interface IHotelDetails
    {
        /// <summary>
        /// Is hotel available for stay
        ///// </summary>
        bool IsAvailable { get; }

        /// <summary>
        /// The Hotel Destination object containing the details of the hotel created from either CMS/OWS
        /// </summary>
        HotelDestination HotelDestination { get; }

        /// <summary>
        /// The OWS country code of the hotel
        /// </summary>
        string CountryCode { get; }

        /// <summary>
        /// The List of room Type available at the hotel
        /// </summary>
        List<RoomTypeEntity> RoomTypes { get; }

        /// <summary>
        /// The List of rate plan codes available at the hotel
        /// </summary>
        List<RatePlanEntity> RatePlans { get; }

        /// <summary>
        /// The List of room rate combinations avialable at the hotel
        /// </summary>
        List<RoomRateEntity> RoomRates { get; }

        List<RoomEntity> Rooms { get; }

        /// <summary>
        /// The Minimum rate to stay at the hotel
        /// </summary>
        RateEntity MinRate { get; }

        /// <summary>
        /// The Maximum rate to stay at the hotel
        /// </summary>
        RateEntity MaxRate { get; }

        /// <summary>
        /// The Minimum rate per stay at the hotel
        /// </summary>
        /// <remarks></remarks>
        RateEntity MinRatePerStay { get; }

        /// <summary>
        /// The Maximum rate per stay at the hotel
        /// </summary>
        /// <remarks></remarks>
        RateEntity MaxRatePerStay { get; }


        /// <summary>
        /// The Minium number of points required to stay at the hotel
        /// </summary>
        RedeemptionPointsEntity MinPoints { get; }

        /// <summary>
        /// The Maximum numer of points required to stay at the hotel
        /// </summary>
        RedeemptionPointsEntity MaxPoints { get; }

        /// <summary>
        /// If this hotel is read as alternate hotel for any hotel, then the distance
        /// of this hotel from the hotel being searched for
        /// </summary>
        double AlternateHotelsDistance { get; set; }

        /// <summary>
        /// This property is implemented in BaseHotelsDetails.cs
        /// If this hotel is read as alternate hotel for any hotel, then the distance
        /// of the city center of searched hotel is stored here.
        /// </summary>
        string DistanceToCityCentreOfSearchedHotel { get; set; }
        double DistanceFromCurrentLocationToSearchedHotel { get; set; }

        double TripAdvisorHotelRating { get; set; }

        bool HideHotelTARating { get; set; }
    }
}