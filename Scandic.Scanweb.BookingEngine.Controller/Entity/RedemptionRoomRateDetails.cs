//  Description					: RedemptionRoomRateDetails                               //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The RoomRateDetails class for the Redemption search
    /// </summary>
    public class RedemptionRoomRateDetails : BaseRoomRateDetails
    {
        #region Variables & Properties

        /// <summary>
        /// Gets/Sets NoOfNights
        /// </summary>
        public int NoOfNights { get; set; }

        #endregion

        /// <summary>
        /// minPointsForEachRoom
        /// </summary>
        private RedeemptionPointsEntity minPointsForEachRoom;

        /// <summary>
        /// maxPointsForEachRoom
        /// </summary>
        private RedeemptionPointsEntity maxPointsForEachRoom;

        /// <summary>
        /// Gets MaxPointsForEachRoom
        /// </summary>
        public RedeemptionPointsEntity MaxPointsForEachRoom
        {
            get { return maxPointsForEachRoom; }
        }

        /// <summary>
        /// Gets MinPointsForEachRoom
        /// </summary>
        public RedeemptionPointsEntity MinPointsForEachRoom
        {
            get { return minPointsForEachRoom; }
        }

        #region Constructor

        /// <summary>
        /// Constructor of RedemptionRoomRateDetails class.
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="noOfNights"></param>
        public RedemptionRoomRateDetails(RedemptionHotelDetails hotelDetails, int noOfNights)
        {
            this.NoOfNights = noOfNights;
            RoomTypes = hotelDetails.RoomTypes;
            CreateBasePointsRoomCategories(hotelDetails);
            hotel = hotelDetails.HotelDestination;
            countryCode = hotelDetails.CountryCode;
        }

        /// <summary>
        /// Constructor of RedemptionRoomRateDetails class.
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="noOfNights"></param>
        /// <param name="roomNumber"></param>
        public RedemptionRoomRateDetails(RedemptionHotelDetails hotelDetails, int noOfNights, int roomNumber)
        {
            this.NoOfNights = noOfNights;
            RoomTypes = hotelDetails.Rooms[roomNumber].RoomTypes;
            minPointsForEachRoom = hotelDetails.Rooms[roomNumber].MinPointsForEachRoom;
            maxPointsForEachRoom = hotelDetails.Rooms[roomNumber].MaxPointsForEachRoom;
            CreateBasePointsRoomCategories(hotelDetails, roomNumber);
            hotel = hotelDetails.HotelDestination;
            countryCode = hotelDetails.CountryCode;
        }

        #endregion

        #region CreateBasePointsRoomCategories

        /// <summary>
        /// Refer to <see cref="BaseRoomRateDetails#CreateBaseRateRoomCategories"/> except this will
        /// be creates the common Room and Rate categories by Points into consideration
        /// </summary>
        /// <param name="hotelDetails"></param>
        protected void CreateBasePointsRoomCategories(BaseHotelDetails hotelDetails)
        {
            rateCategories = hotelDetails.RateCategories;

            List<RoomRateEntity> roomRates = hotelDetails.MinBaseRateInEachRateCategoryList;
            roomRates.Sort(new CompositeMinPointsComparer());

            List<RoomCategoryEntity> rowWiseRoomRateDetails = new List<RoomCategoryEntity>();
            string currentRoomType = null;
            RoomCategoryEntity currentRoomCategoryEntity = null;
            foreach (RoomRateEntity roomRate in roomRates)
            {
                string roomType = roomRate.RoomTypeCode;
                string roomCategoryName = RoomRateUtil.GetRoomCategory(roomType).RoomCategoryId;
                if (currentRoomType == roomType)
                {
                    AddRoomRate(currentRoomCategoryEntity, roomRate);
                }
                else
                {
                    currentRoomCategoryEntity = new RoomCategoryEntity();
                    SetRoomDetails(currentRoomCategoryEntity, roomType);
                    AddRoomRate(currentRoomCategoryEntity, roomRate);
                    rowWiseRoomRateDetails.Add(currentRoomCategoryEntity);
                    currentRoomType = roomType;
                }
            }

            for (int i = 0; i < rowWiseRoomRateDetails.Count; i++)
            {
                RoomCategoryEntity outerRoomCategory = rowWiseRoomRateDetails[i];
                for (int j = i + 1; j < rowWiseRoomRateDetails.Count; j++)
                {
                    RoomCategoryEntity innerRoomCategory = rowWiseRoomRateDetails[j];
                    if (outerRoomCategory.MergeRoomCateogryEntityWithPoints(innerRoomCategory))
                    {
                        rowWiseRoomRateDetails.RemoveAt(j);
                        j = j - 1;
                    }
                }
            }
            this.baseRateRoomCategories = rowWiseRoomRateDetails;
        }

        /// <summary>
        /// Creates base points room categories.
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="roomNumber"></param>
        protected void CreateBasePointsRoomCategories(BaseHotelDetails hotelDetails, int roomNumber)
        {
            rateCategories = hotelDetails.Rooms[roomNumber].RateCategories;

            List<RoomRateEntity> roomRates = hotelDetails.Rooms[roomNumber].MinBaseRateInEachRateCategoryList;
            roomRates.Sort(new CompositeMinPointsComparer());

            List<RoomCategoryEntity> rowWiseRoomRateDetails = new List<RoomCategoryEntity>();
            string currentRoomType = null;
            RoomCategoryEntity currentRoomCategoryEntity = null;
            foreach (RoomRateEntity roomRate in roomRates)
            {
                string roomType = roomRate.RoomTypeCode;
                string roomCategoryName = RoomRateUtil.GetRoomCategory(roomType).RoomCategoryId;
                if (currentRoomType == roomType)
                {
                    AddRoomRate(currentRoomCategoryEntity, roomRate);
                }
                else
                {
                    currentRoomCategoryEntity = new RoomCategoryEntity();
                    SetRoomDetails(currentRoomCategoryEntity, roomType);
                    AddRoomRate(currentRoomCategoryEntity, roomRate);
                    rowWiseRoomRateDetails.Add(currentRoomCategoryEntity);
                    currentRoomType = roomType;
                }
            }

            for (int i = 0; i < rowWiseRoomRateDetails.Count; i++)
            {
                RoomCategoryEntity outerRoomCategory = rowWiseRoomRateDetails[i];
                for (int j = i + 1; j < rowWiseRoomRateDetails.Count; j++)
                {
                    RoomCategoryEntity innerRoomCategory = rowWiseRoomRateDetails[j];
                    if (outerRoomCategory.MergeRoomCateogryEntityWithPoints(innerRoomCategory))
                    {
                        rowWiseRoomRateDetails.RemoveAt(j);
                        j = j - 1;
                    }
                }
            }
            this.baseRateRoomCategories = rowWiseRoomRateDetails;
        }

        #endregion
    }
}