﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Scanweb.BookingEngine.Controller.Entity
{
    public class HotelRedemptionPoints
    {
        public enum SuggestionSearch { HOTEL, CITY, COUNTRY };
        public enum SortTypes {HOTEL,DESTINATION,POINTS };
        public enum SortOrder { Ascending = 0, Descending = 1 };
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string City { get; set; }
        public string CityCode { get; set; }
        public string Hotel { get; set; }
        public string HotelCode { get; set; }
        public string Points { get; set; }
        public string RedemptionStartDate { get; set; }
        public string RedemptionEndDate { get; set; }
        public string RedemptionDuration { get; set; }
        public string HotelLnk { get; set; }

        public List<CampaignPeriods> CampaignPeriod { get; set; }
        

       public IList<HotelRedemptionPoints> HotelRemptionPointsList{get;set;}

       public IList<HotelRedemptionPoints> GetHotelRemptionPoints()
        {
            
            ContentDataAccessManager manager = new ContentDataAccessManager();
            HotelRemptionPointsList = manager.GetAllHotelRedemptionPoints(); 
            return HotelRemptionPointsList;
        }

       public IList<HotelRedemptionPoints> GetHotelRemptionPoints(string searchType,string searchName,string minPoins,string maxPoints,string sorting,string sortingBasedOn)
       {
           ContentDataAccessManager manager = new ContentDataAccessManager();
           HotelRemptionPointsList = manager.GetAllHotelRedemptionPoints();
           var hotelRedemptionPointsList = new List<HotelRedemptionPoints>();

           if (!string.IsNullOrEmpty(searchType))
           {
               switch ((SuggestionSearch)Enum.Parse(typeof(SuggestionSearch), searchType))
               {
                   case SuggestionSearch.COUNTRY:
                       hotelRedemptionPointsList =(List<HotelRedemptionPoints>)HotelRemptionPointsList.Where(HotelRedemptionPoints => HotelRedemptionPoints.CountryCode == searchName).ToList();
                       break;
                   case (SuggestionSearch.CITY):
                       hotelRedemptionPointsList =(List<HotelRedemptionPoints>)HotelRemptionPointsList.Where(HotelRedemptionPoints => HotelRedemptionPoints.CityCode == searchName).ToList();
                       break;
                   case (SuggestionSearch.HOTEL):
                       hotelRedemptionPointsList =(List<HotelRedemptionPoints>)HotelRemptionPointsList.Where(HotelRedemptionPoints => HotelRedemptionPoints.HotelCode == searchName).ToList();
                       break;
               }
           }
           else
           {
               hotelRedemptionPointsList =(List<HotelRedemptionPoints>)HotelRemptionPointsList.ToList();
           }
           IList<HotelRedemptionPoints>  hotelList = GetSortedList(hotelRedemptionPointsList, sorting, sortingBasedOn, minPoins, maxPoints);
           return hotelList;
       }

       private IList<HotelRedemptionPoints> GetSortedList(List<HotelRedemptionPoints> hotelResults,string sorting,string sortingBasedOn,string minPoints,string maxPoints)
       {
           var hotelList = new List<HotelRedemptionPoints>();
           if (!string.IsNullOrEmpty(sortingBasedOn))
           {
               switch ((SortTypes)Enum.Parse(typeof(SortTypes), sortingBasedOn))
               {
                   case SortTypes.HOTEL:
                       if ((SortOrder)Enum.Parse(typeof(SortOrder), sorting) == SortOrder.Ascending)
                           hotelList = (from redemption in hotelResults orderby redemption.Hotel ascending where Convert.ToInt32(redemption.Points) >= Convert.ToInt32(minPoints) && Convert.ToInt32(redemption.Points) <= Convert.ToInt32(maxPoints) select redemption).ToList();
                       else
                           hotelList = (from redemption in hotelResults orderby redemption.Hotel descending where Convert.ToInt32(redemption.Points) >= Convert.ToInt32(minPoints) && Convert.ToInt32(redemption.Points) <= Convert.ToInt32(maxPoints) select redemption).ToList();
                       break;
                   case SortTypes.DESTINATION:
                       if ((SortOrder)Enum.Parse(typeof(SortOrder), sorting) == SortOrder.Ascending)
                           hotelList = (from redemption in hotelResults orderby redemption.City, redemption.Country ascending where Convert.ToInt32(redemption.Points) >= Convert.ToInt32(minPoints) && Convert.ToInt32(redemption.Points) <= Convert.ToInt32(maxPoints) select redemption).ToList();
                       else
                           hotelList = (from redemption in hotelResults orderby redemption.City, redemption.Country descending where Convert.ToInt32(redemption.Points) >= Convert.ToInt32(minPoints) && Convert.ToInt32(redemption.Points) <= Convert.ToInt32(maxPoints) select redemption).ToList();
                       break;
                   case SortTypes.POINTS:
                       if ((SortOrder)Enum.Parse(typeof(SortOrder), sorting) == SortOrder.Ascending)
                           hotelList = (from redemption in hotelResults orderby redemption.Points ascending where Convert.ToInt32(redemption.Points) >= Convert.ToInt32(minPoints) && Convert.ToInt32(redemption.Points) <= Convert.ToInt32(maxPoints) select redemption).ToList();
                       else
                           hotelList = (from redemption in hotelResults orderby redemption.Points descending where Convert.ToInt32(redemption.Points) >= Convert.ToInt32(minPoints) && Convert.ToInt32(redemption.Points) <= Convert.ToInt32(maxPoints) select redemption).ToList();
                       break;
               }
           }
           else
           {
               hotelList = (from redemption in hotelResults orderby redemption.City, redemption.Points ascending  where Convert.ToInt32(redemption.Points) >= Convert.ToInt32(minPoints)&& Convert.ToInt32(redemption.Points) <= Convert.ToInt32(maxPoints) select redemption).ToList();
           }

           return hotelList;
       }
       public IList<HotelRedemptionPoints> GetDistinctCountry()
        {           
            if (HotelRemptionPointsList == null)
            {            
                ContentDataAccessManager manager = new ContentDataAccessManager();
                HotelRemptionPointsList = manager.GetAllHotelRedemptionPoints(); 
            }


            IList<HotelRedemptionPoints> countryList = (from redemption in HotelRemptionPointsList
                                                        orderby redemption.Country ascending
                                                        select redemption).Distinct(new RedemptionPointsCountryComparer()).ToList();
            return countryList;
        }

       public IList<HotelRedemptionPoints> GetDistinctCity()
        {
            if (HotelRemptionPointsList == null)
            {
                ContentDataAccessManager manager = new ContentDataAccessManager();
                HotelRemptionPointsList = manager.GetAllHotelRedemptionPoints();
            }

            IList<HotelRedemptionPoints> cityList = (from redemption in HotelRemptionPointsList
                                                     orderby redemption.City ascending
                         select redemption).Distinct(new RedemptionPointsCityComparer()).ToList();
            return cityList;
        }

       public IList<HotelRedemptionPoints> GetDistinctHotel()
        {
            
            if (HotelRemptionPointsList == null)
            {
                ContentDataAccessManager manager = new ContentDataAccessManager();
                HotelRemptionPointsList = manager.GetAllHotelRedemptionPoints();
            }

            IList<HotelRedemptionPoints> hotelList = (from redemption in HotelRemptionPointsList
                                                      orderby redemption.Hotel ascending
                                         select redemption).Distinct(new RedemptionPointsHotelComparer()).ToList();
            return hotelList;
        }

       public IList<HotelRedemptionPoints> GetDistinctPoints()
       {

           if (HotelRemptionPointsList == null)
           {
               ContentDataAccessManager manager = new ContentDataAccessManager();
               HotelRemptionPointsList = manager.GetAllHotelRedemptionPoints();
           }

           IList<HotelRedemptionPoints> hotelList = (from redemption in HotelRemptionPointsList
                                                     orderby redemption.Points ascending
                                                     select redemption).Distinct(new RedemptionPointsPointsComparer()).ToList();
           return hotelList;
       }

    }

    public class RedemptionPointsCountryComparer : IEqualityComparer<HotelRedemptionPoints>
    {     
        public bool Equals(HotelRedemptionPoints redemptionPoints1, HotelRedemptionPoints redemptionPoints2)
        {
            return redemptionPoints1.Country.Trim().Equals(redemptionPoints2.Country.Trim(), StringComparison.InvariantCultureIgnoreCase);
        }
       
        public int GetHashCode(HotelRedemptionPoints redemptionPoints)
        {           
            return redemptionPoints.Country.Trim().GetHashCode();
        }
    }

    public class RedemptionPointsCityComparer : IEqualityComparer<HotelRedemptionPoints>
    {
        public bool Equals(HotelRedemptionPoints redemptionPoints1, HotelRedemptionPoints redemptionPoints2)
        {
            return redemptionPoints1.City.Trim().Equals(redemptionPoints2.City.Trim(), StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(HotelRedemptionPoints redemptionPoints)
        {
            return redemptionPoints.City.Trim().GetHashCode();
        }
    }

    public class RedemptionPointsHotelComparer : IEqualityComparer<HotelRedemptionPoints>
    {
        public bool Equals(HotelRedemptionPoints redemptionPoints1, HotelRedemptionPoints redemptionPoints2)
        {
            return redemptionPoints1.HotelCode.Trim().Equals(redemptionPoints2.HotelCode.Trim(), StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(HotelRedemptionPoints redemptionPoints)
        {
            return redemptionPoints.HotelCode.Trim().GetHashCode();
        }
    }

    public class RedemptionPointsPointsComparer : IEqualityComparer<HotelRedemptionPoints>
    {
        public bool Equals(HotelRedemptionPoints redemptionPoints1, HotelRedemptionPoints redemptionPoints2)
        {
            return redemptionPoints1.Points.Trim().Equals(redemptionPoints2.Points.Trim(), StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(HotelRedemptionPoints redemptionPoints)
        {
            return redemptionPoints.Points.Trim().GetHashCode();
        }
    }
    
}
