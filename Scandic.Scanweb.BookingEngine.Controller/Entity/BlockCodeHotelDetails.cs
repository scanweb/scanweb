#region Description

////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: This is the hotel details created when user do a block  //
//                                code booking.                                           //
//	                              This is resulted in Accomodation rate block.            //
//                                CR2 | Release 1.4 |Accomodation rate block              //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Raj Kishore Marandi	                                  //
/// Author email id				:                           							  //
/// Creation Date				: 08th September 2008									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revision History			: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#endregion Description

using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// This represents BlockCodeHotelDetails class.
    /// </summary>
    public class BlockCodeHotelDetails : RegularHotelDetails
    {
        #region Variables

        /// <summary>
        /// The block code configured and returned
        /// </summary>
        private string[] blockCodeRatePlanCode;

        /// <summary>
        /// If this hotel is available for booking then true else false.
        /// </summary>
        private bool isTrueBlockRates;

        #endregion

        #region properties

        /// <summary>
        /// Gets MaxRate
        /// </summary>
        public override RateEntity MaxRate
        {
            get { return maxRate; }
        }

        /// <summary>
        /// Gets MinRate
        /// </summary>
        public override RateEntity MinRate
        {
            get { return minRate; }
        }

        /// <summary>
        /// Gets IsAvailable
        /// </summary>
        public override bool IsAvailable
        {
            get
            {
                bool returnValue = true;
                foreach (RoomEntity room in rooms)
                {
                    if (!this.isTrueBlockRates)
                    {
                        if ((room.MinBaseRateInEachRateCategoryList == null
                             || room.MinBaseRateInEachRateCategoryList.Count <= 0) &&
                            (roomRates == null || roomRates.Count <= 0))
                        {
                            returnValue = false;
                            break;
                        }
                    }
                }
                return returnValue;
            }
        }

        /// <summary>
        /// Gets/Sets BlockCode
        /// </summary>
        public string BlockCode { get; set; }

        /// <summary>
        /// Gets/Sets IsBlockRates
        /// </summary>
        public bool IsBlockRates
        {
            get { return isTrueBlockRates; }
        }

        #endregion properties

        /// <summary>
        /// Constructor of block code pages
        /// </summary>
        /// <param name="hotel">Hotel</param>
        /// <param name="countryCode">Country Code</param>
        /// <param name="roomStay">Room Stay</param>
        /// <param name="blockCode">Block Code</param>
        /// <remarks>Last modified for artf885260 : ARB</remarks>
        public BlockCodeHotelDetails(HotelDestination hotel, string countryCode, RoomStay roomStay,
                                     HotelSearchEntity hotelSearchEntity)
        {
            this.BlockCode = hotelSearchEntity.CampaignCode;
            this.HotelDestination = hotel;
            this.CountryCode = countryCode;
            this.blockCodeRatePlanCode = GetBlockCodeRateCodes(roomStay.RatePlans, hotelSearchEntity.CampaignCode);
            this.isTrueBlockRates = GetBookableStatus(roomStay);
            SetUpRoomRatesForBlockHotels(roomStay, hotelSearchEntity.RoomsPerNight);
            if (roomStay.RatePlans != null && roomStay.RatePlans.Length > 0)
            {
                if (!string.IsNullOrEmpty(roomStay.RatePlans[0].ratePlanCode))
                {
                    SetUpMinBaseRateInEachRateCategory();
                    SetUpMinTotalRateInEachRateCategory();
                }
            }
            SetMinMaxRate();
            SetSelectedRatePlanes();
            SetAllRateCategories();
        }

        /// <summary>
        /// Constructor of block code pages
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="listRoomStay"></param>
        /// <param name="hotelSearchEntity"></param>
        public BlockCodeHotelDetails(HotelDestination hotel, string countryCode, IList<RoomStay> listRoomStay,
                                     HotelSearchEntity hotelSearchEntity)
        {
            this.BlockCode = hotelSearchEntity.CampaignCode;
            this.HotelDestination = hotel;
            this.CountryCode = countryCode;
            this.blockCodeRatePlanCode = GetBlockCodeRateCodes(listRoomStay, hotelSearchEntity.CampaignCode);
            this.isTrueBlockRates = GetBookableStatus(listRoomStay);
            SetUpRoomRatesForBlockHotels(listRoomStay, hotelSearchEntity.RoomsPerNight);
            foreach (RoomStay roomStay in listRoomStay)
            {
                if (roomStay.RatePlans != null && roomStay.RatePlans.Length > 0)
                {
                    if (!string.IsNullOrEmpty(roomStay.RatePlans[0].ratePlanCode))
                    {
                        SetUpMinBaseRateInEachRateCategory(new object());
                        SetUpMinTotalRateInEachRateCategory(new object());
                    }
                }
            }

            SetMinMaxRate(new object());
            SetMinMaxRateForEachRoom(new object());
            SetSelectedRatePlans(new object());
            SetAllRateCategories(new object());
        }


        /// <summary>
        /// This method will do following 3 works.
        /// 1.  Setup 'Rate plans' i.e 'rate code'.
        /// 2.  Setup 'Room types'
        /// 3.  Setup 'Room rates'
        /// </summary>
        /// <param name="roomStay">roomStay object</param>
        private void SetUpRoomRatesForBlockHotels(RoomStay roomStay, int roomsPerNight)
        {
            if (isTrueBlockRates)
            {
                SetUpRoomRatesForBlockHotelsForBlockRates(roomStay, roomsPerNight);
            }
            else
            {
                SetUpRoomRatesForBlockHotelForNonBlockRates(roomStay, roomsPerNight);
            }
        }


        /// <summary>
        /// Sets up room rates for block hotels.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>
        /// <param name="roomsPerNight">The rooms per night.</param>
        private void SetUpRoomRatesForBlockHotels(IList<RoomStay> listRoomStay, int roomsPerNight)
        {
            if (isTrueBlockRates)
            {
                SetUpRoomRatesForBlockHotelsForBlockRates(listRoomStay, roomsPerNight);
            }
            else
            {
                SetUpRoomRatesForBlockHotelForNonBlockRates(listRoomStay, roomsPerNight);
            }
        }


        /// <summary>
        /// Set up te RoomRates for Block Rates.
        /// If the Availability Search returns only block rates; Accept all rate plan categories returned from Opera.
        /// </summary>
        private void SetUpRoomRatesForBlockHotelsForBlockRates(RoomStay roomStay, int roomsPerNight)
        {
            int roomTypeSortOrder = 0;
            if (null != roomStay.RatePlans)
            {
                foreach (RatePlan ratePlan in roomStay.RatePlans)
                {
                    if ((null != ratePlan) && (null != ratePlan.ratePlanCode))
                    {
                        if ((RoomRateUtil.IsRatePlanCodeInCMS(ratePlan.ratePlanCode)))
                        {
                            this.RatePlans.Add(GetRatePlan(ratePlan));
                        }
                    }
                }
            }

            List<string> roomTypeToRemove = GetUnavailableRoomType(roomStay, roomsPerNight);
            if (null != roomStay.RoomRates)
            {
                foreach (RoomRate roomRate in roomStay.RoomRates)
                {
                    string ratePlanCode = roomRate.ratePlanCode;
                    if ((string.IsNullOrEmpty(ratePlanCode)))
                    {
                        if ((RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode))
                            && (!roomTypeToRemove.Contains(roomRate.roomTypeCode)))
                        {
                            RoomRateEntity roomRateEntity = GetBlockCodeRoomRate(roomRate);
                            roomRates.Add(roomRateEntity);
                        }
                    }
                    else if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlanCode) &&
                             RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode))
                    {
                        if (!roomTypeToRemove.Contains(roomRate.roomTypeCode))
                        {
                            RoomRateEntity roomRateEntity = GetBlockCodeRoomRate(roomRate);
                            roomRates.Add(roomRateEntity);
                        }
                    }
                }
            }

            if (null != roomStay.RoomTypes)
            {
                int roomTypeLength = roomStay.RoomTypes.Length;
                for (int count = 0; count < roomTypeLength; count++)
                {
                    if (RoomRateUtil.IsRoomTypeCodeInCMS(roomStay.RoomTypes[count].roomTypeCode))
                    {
                        if (!roomTypeToRemove.Contains(roomStay.RoomTypes[count].roomTypeCode))
                        {
                            roomTypeSortOrder += 1;
                            this.RoomTypes.Add(GetRoomType(roomStay.RoomTypes[count], roomTypeSortOrder));
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Set up te RoomRates for Block Rates.
        /// If the Availability Search returns only block rates; Accept all rate plan categories returned from Opera.
        /// </summary>
        private void SetUpRoomRatesForBlockHotelsForBlockRates(IList<RoomStay> listRoomStay, int roomsPerNight)
        {
            foreach (RoomStay roomStay in listRoomStay)
            {
                RoomEntity room = new RoomEntity();
                int roomTypeSortOrder = 0;
                if (null != roomStay.RatePlans)
                {
                    foreach (RatePlan ratePlan in roomStay.RatePlans)
                    {
                        if ((null != ratePlan) && (null != ratePlan.ratePlanCode))
                        {
                            if ((RoomRateUtil.IsRatePlanCodeInCMS(ratePlan.ratePlanCode)))
                            {
                                room.RatePlans.Add(GetRatePlan(ratePlan));
                            }
                        }
                    }
                }

                List<string> roomTypeToRemove = GetUnavailableRoomType(roomStay, roomsPerNight);
                if (null != roomStay.RoomRates)
                {
                    foreach (RoomRate roomRate in roomStay.RoomRates)
                    {
                        string ratePlanCode = roomRate.ratePlanCode;
                        if ((string.IsNullOrEmpty(ratePlanCode)))
                        {
                            if ((RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode))
                                && (!roomTypeToRemove.Contains(roomRate.roomTypeCode)))
                            {
                                RoomRateEntity roomRateEntity = GetBlockCodeRoomRate(roomRate);
                                room.RoomRates.Add(roomRateEntity);
                            }
                        }
                        else if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlanCode) &&
                                 RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode))
                        {
                            if (!roomTypeToRemove.Contains(roomRate.roomTypeCode))
                            {
                                RoomRateEntity roomRateEntity = GetBlockCodeRoomRate(roomRate);
                                room.RoomRates.Add(roomRateEntity);
                            }
                        }
                    }
                }

                if (null != roomStay.RoomTypes)
                {
                    int roomTypeLength = roomStay.RoomTypes.Length;
                    for (int count = 0; count < roomTypeLength; count++)
                    {
                        if (RoomRateUtil.IsRoomTypeCodeInCMS(roomStay.RoomTypes[count].roomTypeCode))
                        {
                            if (!roomTypeToRemove.Contains(roomStay.RoomTypes[count].roomTypeCode))
                            {
                                roomTypeSortOrder += 1;
                                room.RoomTypes.Add(GetRoomType(roomStay.RoomTypes[count], roomTypeSortOrder));
                            }
                        }
                    }
                }

                rooms.Add(room);
            }
        }


        /// <summary>
        /// Set up the RoomRates for Non-Block Rates.
        /// If the Availability Search returns non-block rates; accept only 'Early' & 'Flex' ratecategories.
        /// </summary>
        private void SetUpRoomRatesForBlockHotelForNonBlockRates(RoomStay roomStay, int roomsPerNight)
        {
            string[] bonusChequeRates = RoomRateUtil.GetBonusChequeRateCodes();
            int roomTypeSortOrder = 0;
            if (null != roomStay.RatePlans)
            {
                foreach (RatePlan ratePlan in roomStay.RatePlans)
                {
                    if ((null != ratePlan) && (null != ratePlan.ratePlanCode))
                    {
                        if ((RoomRateUtil.IsRatePlanCodeInCMS(ratePlan.ratePlanCode)))
                        {
                            string rateCategoryName =
                                RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlan.ratePlanCode).RateCategoryId;
                            if (!StringUtil.IsStringInArray(bonusChequeRates, rateCategoryName))
                                this.RatePlans.Add(GetRatePlan(ratePlan));
                        }
                    }
                }
            }

            List<string> roomTypeToRemove = GetUnavailableRoomType(roomStay, roomsPerNight);
            if (null != roomStay.RoomRates)
            {
                foreach (RoomRate roomRate in roomStay.RoomRates)
                {
                    string ratePlanCode = roomRate.ratePlanCode;
                    if ((string.IsNullOrEmpty(ratePlanCode)) && RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode))
                    {
                        if (!roomTypeToRemove.Contains(roomRate.roomTypeCode))
                        {
                            RoomRateEntity roomRateEntity = GetBlockCodeRoomRate(roomRate);
                            roomRates.Add(roomRateEntity);
                        }
                    }
                    else if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlanCode) &&
                             RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode))
                    {
                        if (!roomTypeToRemove.Contains(roomRate.roomTypeCode))
                        {
                            string rateCategoryName =
                                RoomRateUtil.GetRateCategoryByRatePlanCode(roomRate.ratePlanCode).RateCategoryId;
                            if (!StringUtil.IsStringInArray(bonusChequeRates, rateCategoryName))
                                this.RoomRates.Add(GetBlockCodeRoomRate(roomRate));
                        }
                    }
                }
            }

            if (null != roomStay.RoomTypes)
            {
                int roomTypeLength = roomStay.RoomTypes.Length;
                for (int count = 0; count < roomTypeLength; count++)
                {
                    if (RoomRateUtil.IsRoomTypeCodeInCMS(roomStay.RoomTypes[count].roomTypeCode))
                    {
                        if (!roomTypeToRemove.Contains(roomStay.RoomTypes[count].roomTypeCode))
                        {
                            roomTypeSortOrder += 1;
                            this.RoomTypes.Add(GetRoomType(roomStay.RoomTypes[count], roomTypeSortOrder));
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Set up the RoomRates for Non-Block Rates.
        /// If the Availability Search returns non-block rates; accept only 'Early' & 'Flex' ratecategories.
        /// </summary>
        private void SetUpRoomRatesForBlockHotelForNonBlockRates(IList<RoomStay> listRoomStay, int roomsPerNight)
        {
            string[] bonusChequeRates = RoomRateUtil.GetBonusChequeRateCodes();

            foreach (RoomStay roomStay in listRoomStay)
            {
                RoomEntity room = new RoomEntity();
                int roomTypeSortOrder = 0;
                if (null != roomStay.RatePlans)
                {
                    foreach (RatePlan ratePlan in roomStay.RatePlans)
                    {
                        if ((null != ratePlan) && (null != ratePlan.ratePlanCode))
                        {
                            if ((RoomRateUtil.IsRatePlanCodeInCMS(ratePlan.ratePlanCode)))
                            {
                                string rateCategoryName =
                                    RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlan.ratePlanCode).RateCategoryId;
                                if (!StringUtil.IsStringInArray(bonusChequeRates, rateCategoryName))
                                    room.RatePlans.Add(GetRatePlan(ratePlan));
                            }
                        }
                    }
                }

                List<string> roomTypeToRemove = GetUnavailableRoomType(roomStay, roomsPerNight);
                if (null != roomStay.RoomRates)
                {
                    foreach (RoomRate roomRate in roomStay.RoomRates)
                    {
                        string ratePlanCode = roomRate.ratePlanCode;
                        if ((string.IsNullOrEmpty(ratePlanCode)) &&
                            RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode))
                        {
                            if (!roomTypeToRemove.Contains(roomRate.roomTypeCode))
                            {
                                RoomRateEntity roomRateEntity = GetBlockCodeRoomRate(roomRate);
                                room.RoomRates.Add(roomRateEntity);
                            }
                        }
                        else if (RoomRateUtil.IsRatePlanCodeInCMS(ratePlanCode) &&
                                 RoomRateUtil.IsRoomTypeCodeInCMS(roomRate.roomTypeCode))
                        {
                            if (!roomTypeToRemove.Contains(roomRate.roomTypeCode))
                            {
                                string rateCategoryName =
                                    RoomRateUtil.GetRateCategoryByRatePlanCode(roomRate.ratePlanCode).RateCategoryId;
                                if (!StringUtil.IsStringInArray(bonusChequeRates, rateCategoryName))
                                    room.RoomRates.Add(GetBlockCodeRoomRate(roomRate));
                            }
                        }
                    }
                }

                if (null != roomStay.RoomTypes)
                {
                    int roomTypeLength = roomStay.RoomTypes.Length;
                    for (int count = 0; count < roomTypeLength; count++)
                    {
                        if (RoomRateUtil.IsRoomTypeCodeInCMS(roomStay.RoomTypes[count].roomTypeCode))
                        {
                            if (!roomTypeToRemove.Contains(roomStay.RoomTypes[count].roomTypeCode))
                            {
                                roomTypeSortOrder += 1;
                                room.RoomTypes.Add(GetRoomType(roomStay.RoomTypes[count], roomTypeSortOrder));
                            }
                        }
                    }
                }
                rooms.Add(room);
            }
        }


        /// <summary>
        /// The utility method which will take the OWS RoomRate object, creates and retuens
        /// the corresponding application RoomRateEntity
        /// </summary>
        /// <param name="roomRate">The OWS RoomRate object</param>
        /// <returns>The application RoomRateEntity object</returns>
        protected RoomRateEntity GetBlockCodeRoomRate(RoomRate roomRate)
        {
            RoomRateEntity roomRateEntity = new RoomRateEntity(roomRate.roomTypeCode, roomRate.ratePlanCode);
            if (roomRate.Rates != null && roomRate.Rates.Length > 0)
            {
                roomRateEntity.BaseRate = new RateEntity(roomRate.Rates[0].Base.Value,
                                                         roomRate.Rates[0].Base.currencyCode);
                roomRateEntity.TotalRate = new RateEntity(roomRate.Total.Value, roomRate.Total.currencyCode);
            }
            return roomRateEntity;
        }

        /// <summary>
        /// The list of promotion rates will be returned. 
        /// Each RatePlan will have the tags as promotionCode
        /// if this tag is exisiting in the RatePlan values then it is added to the list
        /// </summary>
        /// <param name="ratePlans">The list of RatePlan objects to be searched for</param>
        /// <param name="corporateCode">The Promotion code passed earlier the OWS Request</param>
        /// <returns>List of Promotion rates returned from the OWS</returns>
        private string[] GetBlockCodeRateCodes(RatePlan[] ratePlans, string blockCode)
        {
            List<string> rateCodes = new List<string>();
            if (null != ratePlans)
            {
                int ratePlanLength = ratePlans.Length;
                RatePlan ratePlan;
                if (1 == ratePlanLength)
                {
                    ratePlan = ratePlans[0];
                    rateCodes.Add(ratePlan.ratePlanCode);
                }
                else
                {
                    for (int ratePlanCount = 0; ratePlanCount < ratePlanLength; ratePlanCount++)
                    {
                        ratePlan = ratePlans[ratePlanCount];
                        rateCodes.Add(ratePlan.ratePlanCode);
                    }
                }
            }
            return rateCodes.ToArray();
        }

        /// <summary>
        /// Gets the block code rate codes.
        /// </summary>
        /// <param name="listRoomStay">The list room stay.</param>
        /// <param name="blockCode">The block code.</param>
        /// <returns></returns>
        private string[] GetBlockCodeRateCodes(IList<RoomStay> listRoomStay, string blockCode)
        {
            List<string> rateCodes = new List<string>();

            foreach (RoomStay roomStay in listRoomStay)
            {
                RatePlan[] ratePlans = roomStay.RatePlans;
                if (null != ratePlans)
                {
                    int ratePlanLength = ratePlans.Length;
                    RatePlan ratePlan;
                    if (1 == ratePlanLength)
                    {
                        ratePlan = ratePlans[0];
                        rateCodes.Add(ratePlan.ratePlanCode);
                    }
                    else
                    {
                        for (int ratePlanCount = 0; ratePlanCount < ratePlanLength; ratePlanCount++)
                        {
                            ratePlan = ratePlans[ratePlanCount];
                            rateCodes.Add(ratePlan.ratePlanCode);
                        }
                    }
                }
            }
            return rateCodes.ToArray();
        }

        /// <summary>
        /// This will identify if this block booking.
        /// </summary>
        /// <param name="roomStay">The object where the information regarding block code booking will contain</param>
        /// <returns>True or false</returns>
        private bool GetBookableStatus(RoomStay roomStay)
        {
            bool returnValue = false;
            RatePlan[] plans = roomStay.RatePlans;
            int planLength = plans.Length;
            for (int counter = 0; counter < planLength; counter++)
            {
                AdditionalDetail[] details = plans[counter].AdditionalDetails;
                int detailCount = details.Length;
                for (int count = 0; count < detailCount; count++)
                {
                    if (details[count].detailType.ToString() == AppConstants.MISC)
                    {
                        returnValue = true;
                        break;
                    }
                }
                if (returnValue)
                {
                    break;
                }
            }
            return returnValue;
        }


        /// <summary>
        /// This will identify if this block booking.
        /// </summary>
        /// <param name="roomStay">The object where the information regarding block code booking will contain</param>
        /// <returns>True or false</returns>
        private bool GetBookableStatus(IList<RoomStay> listRoomStay)
        {
            bool returnValue = false;
            foreach (RoomStay roomStay in listRoomStay)
            {
                RatePlan[] plans = roomStay.RatePlans;
                int planLength = plans.Length;
                for (int counter = 0; counter < planLength; counter++)
                {
                    AdditionalDetail[] details = plans[counter].AdditionalDetails;
                    int detailCount = details.Length;
                    for (int count = 0; count < detailCount; count++)
                    {
                        if (details[count].detailType.ToString() == AppConstants.MISC)
                        {
                            returnValue = true;
                            break;
                        }
                    }
                    if (returnValue)
                    {
                        break;
                    }
                }
            }
            return returnValue;
        }

        /// <summary>
        /// This will add the room type in two case
        /// 1. If room type is available with no rate configured happens for Grid booking.
        /// 2. Room type available is less then the user requested for.
        /// 
        /// In these two scenario room type will be discarded.
        /// </summary>
        /// <param name="roomStay">room stay object</param>
        /// <returns>List of room types need to be excluded.</returns>
        /// <remarks>Last modified for artf885260 : ARB</remarks>
        private List<string> GetUnavailableRoomType(RoomStay roomStay, int numberOfRoomPerNight)
        {
            List<string> roomToRemove = new List<string>();
            if (null != roomStay.RoomTypes)
            {
                int roomTypeLength = roomStay.RoomTypes.Length;
                for (int count = 0; count < roomTypeLength; count++)
                {
                    string roomTypeCode = roomStay.RoomTypes[count].roomTypeCode;
                    if (RoomRateUtil.IsRoomTypeCodeInCMS(roomTypeCode))
                    {
                        if (roomStay.RoomTypes[count].numberOfUnits < numberOfRoomPerNight)
                        {
                            roomToRemove.Add(roomTypeCode);
                        }
                    }
                }
            }
            if (null != roomStay.RoomRates)
            {
                int roomRateLength = roomStay.RoomRates.Length;
                for (int count = 0; count < roomRateLength; count++)
                {
                    string roomTypeCode = roomStay.RoomRates[count].roomTypeCode;
                    if (RoomRateUtil.IsRoomTypeCodeInCMS(roomTypeCode))
                    {
                        if ((roomStay.RoomRates[count].Rates == null) || (roomStay.RoomRates[count].Total == null))
                        {
                            roomToRemove.Add(roomTypeCode);
                        }
                    }
                }
            }
            return roomToRemove;
        }
    }
}