//  Description					: PromotionRoomRateDetails                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The RoomRateDetails object for the Promo search
    /// </summary>
    public class PromotionRoomRateDetails : RegularRoomRateDetails
    {
        #region Variables

        protected bool hasPromtionRates;

        protected bool roomHasPromtionRates;

        #endregion

        #region Properties
        /// <summary>
        /// Gets HasPromtionRates
        /// </summary>
        public bool HasPromtionRates
        {
            get { return hasPromtionRates; }
        }

        /// <summary>
        /// Gets RoomHasPromtionRates
        /// </summary>
        public bool RoomHasPromtionRates
        {
            get { return roomHasPromtionRates; }
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotelDetails"></param>
        public PromotionRoomRateDetails(PromotionHotelDetails hotelDetails) : base(hotelDetails)
        {
            hasPromtionRates = hotelDetails.HasPromotionRoomRates;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="roomNumber"></param>
        public PromotionRoomRateDetails(PromotionHotelDetails hotelDetails, int roomNumber)
            : base(hotelDetails, roomNumber)
        {
            hasPromtionRates = hotelDetails.HasPromotionRoomRates;
        }
    }
}