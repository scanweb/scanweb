using Scandic.Scanweb.Entity;
using System.Collections.Generic;
namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The RoomRateDetails object for the Voucher search
    /// </summary>
    public class VoucherRoomRateDetails : PromotionRoomRateDetails
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotelDetails"></param>
        public VoucherRoomRateDetails(VoucherHotelDetails hotelDetails)
            : base(hotelDetails)
        {
            hasPromtionRates = hotelDetails.HasPromotionRoomRates;
            roomHasPromtionRates = RoomsHasPromotionalRate(hotelDetails.Rooms);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotelDetails"></param>
        /// <param name="roomNumber"></param>
        public VoucherRoomRateDetails(VoucherHotelDetails hotelDetails, int roomNumber)
            : base(hotelDetails, roomNumber)
        {
            hasPromtionRates = hotelDetails.HasPromotionRoomRates;
            roomHasPromtionRates = RoomsHasPromotionalRate(hotelDetails.Rooms);
        }

        private bool RoomsHasPromotionalRate(List<RoomEntity> rooms)
        {
            var hasPromotionRates = true;
            foreach (var room in rooms)
            {
                var promoRoom = room as PromoRoomEntity;

                if (promoRoom != null)
                {
                    hasPromotionRates = promoRoom.RoomHasPromoRates;
                    if (!hasPromotionRates)
                        break;
                }
            }
            return hasPromotionRates;
        }
    }
}