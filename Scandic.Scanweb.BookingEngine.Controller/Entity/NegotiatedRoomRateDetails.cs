using System.Collections.Generic;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The RoomRateDetails subtype for the Negotiated search
    /// </summary>
    public class NegotiatedRoomRateDetails : BaseRoomRateDetails
    {
        #region Variables

        /// <summary>
        /// The Company name to which the negotiated code belongs to
        /// </summary>
        private string companyName;

        /// <summary>
        /// Does the hotel has the negotiated rates with the negotiated code user has searched for
        /// </summary>
        private bool hasNegotiatedRates;

        /// <summary>
        /// Negotiated RateCategory Id string.
        /// </summary>
        /// <remarks>ID:311742 | Rooms and Rate Description | Same rate codes are displaying in two different columns.|Hygiene release</remarks>
        private List<string> rateCategoryIdString;

        public List<string> RateCategoryIdString
        {
            get { return rateCategoryIdString; }
        }

        #endregion

        #region Properties

        public string CompanyName
        {
            get { return companyName; }
        }

        public bool HasNegotiatedRates
        {
            get { return hasNegotiatedRates; }
        }

        #endregion

        #region Constructor

        public NegotiatedRoomRateDetails(NegotiatedHotelDetails hotelDetails)
        {
            // R1.2 | CR3 | Change sort order of bed type preferences.
            // All room types of the hotel across different room categories 
            // will be stored. This is used in showing the bed type preferences 
            // in an order the general availability returns.
            RoomTypes = hotelDetails.RoomTypes;

            CreateBaseRateRoomCategories(hotelDetails);
            this.companyName = hotelDetails.CompanyName;
            this.hasNegotiatedRates = hotelDetails.HasNegotiateRoomRates;
            hotel = hotelDetails.HotelDestination;
            countryCode = hotelDetails.CountryCode;
            GetSpecialRateCategories(hotelDetails.NegotiatedRatePlanCodes);
        }

        public NegotiatedRoomRateDetails(NegotiatedHotelDetails hotelDetails, int roomNumber)
        {
            // R1.2 | CR3 | Change sort order of bed type preferences.
            // All room types of the hotel across different room categories 
            // will be stored. This is used in showing the bed type preferences 
            // in an order the general availability returns.
            RoomTypes = hotelDetails.Rooms[roomNumber].RoomTypes;
            minRateForEachRoom = hotelDetails.Rooms[roomNumber].MinRateForEachRoom;
            maxRateForEachRoom = hotelDetails.Rooms[roomNumber].MaxRateForEachRoom;
            minRatePerStayForEachRoom = hotelDetails.Rooms[roomNumber].MinRatePerStayForEachRoom;
            maxRatePerStayForEachRoom = hotelDetails.Rooms[roomNumber].MaxRatePerStayForEachRoom;
            CreateBaseRateRoomCategories(hotelDetails, roomNumber);
            this.companyName = hotelDetails.CompanyName;
            this.hasNegotiatedRates = hotelDetails.HasNegotiateRoomRates;
            hotel = hotelDetails.HotelDestination;
            countryCode = hotelDetails.CountryCode;
            GetSpecialRateCategories(hotelDetails.NegotiatedRatePlanCodes);
                // Negotiated code has to go inside RoomEntity
        }

        /// <summary>
        /// Adds Negotiated rate categories.
        /// </summary>
        /// <param name="ratePlanCodes">Rateplan codes</param>
        /// <remarks>ID:311742 | Rooms and Rate Description | Same rate codes are displaying in two different columns.|Hygiene release</remarks>
        private void GetSpecialRateCategories(string[] ratePlanCodes)
        {
            int rateplanCount = ratePlanCodes.Length;
            rateCategoryIdString = new List<string>();
            for (int count = 0; count < rateplanCount; count++)
            {
                RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlanCodes[count]);
                if (null != rateCategory)
                    rateCategoryIdString.Add(
                        RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlanCodes[count]).RateCategoryId);
            }
        }

        #endregion
    }
}