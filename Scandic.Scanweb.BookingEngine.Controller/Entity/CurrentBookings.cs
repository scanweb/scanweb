using System;
using System.Collections.Generic;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Reservation;
using Scandic.Scanweb.ExceptionManager;
using ServiceProxies = Scandic.Scanweb.BookingEngine.ServiceProxies;
using System.Linq;

namespace Scandic.Scanweb.BookingEngine.Controller.Entity
{
    /// <summary>
    /// CurrentBookingSummary
    /// </summary>
    [Serializable]
    public class CurrentBookingSummary
    {
        #region Properties
        /// <summary>
        /// Gets/Sets CurrentBookingsList
        /// </summary>
        public List<CurrentBooking> CurrentBookingsList { get; set; }
       #endregion Properties

        #region Constructor Method

        /// <summary>
        /// CurrentBookingSummary
        /// </summary>
        /// <param name="response">FutureBookingSummaryResponse</param>
        public CurrentBookingSummary(FutureBookingSummaryResponse response)
        {
            HotelReservation[] hotelReservationList = response.HotelReservations;
            if (hotelReservationList != null)
            {
                List<CurrentBooking> currentBookings = new List<CurrentBooking>();
                
                if (hotelReservationList.Length > 0)
                {
                    for (int count = 0; count < hotelReservationList.Length; count++)
                    {
                        var currentBooking = new CurrentBooking();
                        HotelReservation hotelReservation = new HotelReservation();
                        hotelReservation = hotelReservationList[count];

                        // Cancelled bookings should not be displayed.
                        // All cancelled bookings has an extra node called <CancelTerm> under 
                        // <HotelReservation> node. So we have to check if the node <CancelTerm> 
                        // is not available then we proceed to show the booking.
                        if (hotelReservation.RoomStays[0].CancelTerm == null || hotelReservation.reservationStatus != ReservationStatusType.CANCELED)
                        {
                            currentBooking.ConfirmationId = hotelReservation.UniqueIDList[0].Value;

                            string hotelCode = hotelReservation.RoomStays[0].HotelReference.hotelCode;
                            bool isHotelExist = !string.IsNullOrEmpty(hotelCode);
                            HotelDestination hotelDetail = null;
                            if (isHotelExist)
                            {
                                
                                try
                                {
                                    hotelDetail = ContentDataAccess.GetHotelByOperaID(hotelCode);
                                }
                                catch (ContentDataAccessException ex)
                                {
                                    AppLogger.LogFatalException(ex, string.Format(" -> Could not fetch information in future booking for hotelId - {0}", hotelCode));
                                }
                            }
                            if (hotelDetail != null)
                            {
                                currentBooking.HotelName = isHotelExist ? hotelDetail.Name : string.Empty;
                                currentBooking.City = isHotelExist ? hotelDetail.City : string.Empty;
                                hotelDetail = null;
                            }

                            var fromToDate = hotelReservation.RoomStays[0].TimeSpan;

                            currentBooking.FromDate = fromToDate.StartDate;
                            currentBooking.ToDate = (DateTime)fromToDate.Item;

                            currentBookings.Add(currentBooking);
                        }
                    }
                }

                if (currentBookings != null)
                {
                    currentBookings = currentBookings.Distinct(new CurrentBookingDistinct()).ToList<CurrentBooking>();
                    currentBookings.Sort(new CurrentBookingComparer());
                }
                this.CurrentBookingsList = currentBookings;
            }
        }

        #endregion Constructor Method

        #region Current booking Comparers
        public class CurrentBookingDistinct : IEqualityComparer<CurrentBooking>
        {

            #region IEqualityComparer<CurrentBooking> Members

            public bool Equals(CurrentBooking x, CurrentBooking y)
            {
                return x.ConfirmationId == y.ConfirmationId;
            }

            public int GetHashCode(CurrentBooking obj)
            {
                return obj.GetHashCode();
            }

            #endregion
        }

        #endregion
    }
}