//  Description					: VoucherHotelDetails                                     //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// The HotelDetails subtype for the search type Voucher search
    /// </summary>
    public class VoucherHotelDetails : PromotionHotelDetails
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="roomStay"></param>
        /// <param name="promotionCode"></param>
        public VoucherHotelDetails(HotelDestination hotel, string countryCode, RoomStay roomStay, string promotionCode)
            : base(hotel, countryCode, roomStay, promotionCode)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="countryCode"></param>
        /// <param name="listRoomStay"></param>
        /// <param name="promotionCode"></param>
        public VoucherHotelDetails(HotelDestination hotel, string countryCode, IList<RoomStay> listRoomStay,
                                   string promotionCode)
            : base(hotel, countryCode, listRoomStay, promotionCode)
        {
        }
        #endregion
    }
}