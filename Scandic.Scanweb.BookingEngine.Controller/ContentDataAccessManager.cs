﻿//  Description					: ContentDataAccess manager acts as a contract to access  //
//								  staticcontentdataccess members, and it enablesto decouple// 
//                                static methods that are getting called from controllers.//
//----------------------------------------------------------------------------------------//
//  Author						: Sapient                                            	  //
//  Author email id				:                           							  //
//  Creation Date				: August 22nd, 2012                                       //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
// 	Last Modified Date			:                                                         //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using EPiServer.BaseLibrary;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller.Entity;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using System.Globalization;
using EPiServer;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Core.Core;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// ContentDataAccess manager acts as a contract to access staticcontentdataccess members, 
    /// and it enables to decouple static methods that are getting called from controllers.
    /// </summary>
    public class ContentDataAccessManager : IContentDataAccessManager 
    {
        private static int CountryPageTypeID = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.CountryPageTypeID]);
        private static int CityPageTypeID = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.CityPageTypeID]);
        private static int HotelPageTypeID = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.HotelPageTypeID]);
        private static int HotelRedemptionPointsPageTypeID = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.HotelRedemptionPointsPageTypeID]);


        /// <summary>
        /// Gets a dictionary with RateType keys and the corresponding RateCategories
        /// </summary>
        /// <returns>A dictionary with all Rate Type IDs and the each corresponding Rate Category.</returns>
        public Dictionary<string, RateCategory> GetRateTypeCategoryMap()
        {
            return ContentDataAccess.GetRateTypeCategoryMap(false);
        }

        /// <summary>
        /// Gets a dictionary with RoomType keys and the corresponding RoomCategory
        /// </summary>
        /// <returns>A dictionary with all Room Type IDs and the each corresponding Room Category.</returns>
        public Dictionary<string, RoomCategory> GetRoomTypeCategoryMap()
        {
            return ContentDataAccess.GetRoomTypeCategoryMap();
        }

        /// <summary>
        /// Returs the List of all Bookable hotels
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        public List<HotelDestination> GetBookableHotels(string cityCode)
        {
            return ContentDataAccess.GetBookableHotels(cityCode);
        }


        /// <summary>
        /// Get all alternative city hotels for a particular city from Cache.If it is not available 
        /// in Cache , call CreateHotelDestinationList() method which will get 
        /// all alternative city hotels for a particular city from CMS
        /// </summary>
        /// <returns>Return a list of  alternative city hotels for a  particular city</returns>
        public List<HotelDestination> GetAlternateCityHotels(string cityCode)
        {
            return ContentDataAccess.GetAlternateCityHotels(cityCode);
        }

        /// <summary>
        /// Gets the Hotel based on the specified Opera ID and langauge
        /// </summary>
        /// <param name="hotelOperaID">The Opera ID of the Hotel</param>
        /// <returns>Returns the Hotel object with the specified Opera ID. </returns>
        public HotelDestination GetHotelByOperaID(string hotelOperaID, string language)
        {
            return ContentDataAccess.GetHotelByOperaID(hotelOperaID, language);
        }

        /// <summary>
        /// Gets the Hotel based on the specified Opera ID
        /// </summary>
        /// <param name="hotelOperaID">The Opera ID of the Hotel</param>
        /// <returns>Returns the Hotel object with the specified Opera ID. </returns>
        public HotelDestination GetHotelByOperaID(string hotelOperaID)
        {
            return GetHotelByOperaID(hotelOperaID, string.Empty);
        }


        /// <summary>
        /// This mehthod is consolida the GetAllDestinations() and GetAllDestinationsIgnoreVisibility().
        /// This is implementating the new tree framework.
        /// </summary>
        /// <param name="ignoreBookingVisibility">If True then Ignores the hotel i.e hotel is not available for booking</param>
        /// <returns>List of city</returns>
        /// <remarks>Find a hotel release.</remarks>
        public List<CityDestination> GetCityAndHotel(bool ignoreBookingVisibility)
        {
            return ContentDataAccess.GetCityAndHotel(ignoreBookingVisibility);
        }

        public List<CityDestination> GetDestinationsFromTheLastSixMonthscityList(bool ignoreBookingVisibility)
        {
            return ContentDataAccess.GetDestinationsFromTheLastSixMonthscityList(ignoreBookingVisibility);
        }

        /// <summary>
        /// This mehthod is consolida the GetAllDestinations() and GetAllDestinationsIgnoreVisibility().
        /// This is implementating the new tree framework.
        /// </summary>
        /// <param name="ignoreBookingVisibility">If True then Ignores the hotel i.e hotel is not available for booking</param>
        /// <returns>List of city</returns>
        /// <remarks>Find a hotel release.</remarks>
        public List<CityDestination> GetCityAndHotelForAutoSuggest(bool ignoreBookingVisibility)
        {
            return ContentDataAccess.GetCityAndHotelForAutoSuggest(ignoreBookingVisibility);
        }

        public List<Scandic.Scanweb.Core.CountryDestinatination> GetCountryCityAndHotelForAutoSuggest(bool ignoreBookingVisibility,bool removeChildrens)
        {
            return ContentDataAccess.GetCountryCityAndHotelForAutoSuggest(ignoreBookingVisibility,removeChildrens);
        }

        /// <summary>
        /// Fetches all destinations with non bookable status.
        /// </summary>
        /// <param name="ignoreBookingVisibility"></param>
        /// <returns></returns>
        public List<CityDestination> FetchAllDestinationsWithNonBookable(bool ignoreBookingVisibility)
        {
            return ContentDataAccess.FetchAllDestinationsWithNonBookable(ignoreBookingVisibility);
        }


        /// <summary>
        /// Get all Transactions
        /// </summary>
        /// <returns>Return a list of all Transactions</returns>
        public List<Transaction> GetAllTransactions()
        {
            return ContentDataAccess.GetAllTransactions();
        }

        /// <summary>
        /// This will return the bonus cheque value configured for a country in a year
        /// </summary>
        /// <param name="year">The year for which the bonus cheque needs to be calcuated</param>
        /// <param name="country">The Opera Country code</param>
        /// <param name="currencyCode">The Opera Currency Code</param>
        /// <returns>The Bouns cheque value configured for the corresponding country</returns>
        public double GetBonusChequeRate(int year, string country, string currencyCode)
        {
            return ContentDataAccess.GetBonusChequeRate(year, country, currencyCode);
        }

        /// <summary>
        /// Gets the Country based on the specified countryCode. 
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns>Returns the Hotel object with the specified Opera ID. </returns>
        public string GetCountryNamebyCountryID(string countryCode)
        {
            return ContentDataAccess.GetCountryNamebyCountryID(countryCode);
        }

        /// <summary>
        /// Gets the hotel destination with the Hotel Name 
        /// </summary>
        /// <param name="hotelName"></param>
        /// <returns></returns>
        public HotelDestination GetHotelDestinationWithHotelName(string hotelName)
        {
            return ContentDataAccess.GetHotelDestinationWithHotelName(hotelName);
        }

        /// <summary>
        /// Returns the list of all Hotels (Bookable and non bookable)
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        public List<HotelDestination> GetAllTypesofHotels(string cityCode)
        {
            return ContentDataAccess.GetAllTypesofHotels(cityCode);
        }

        public IList< HotelRedemptionPoints> GetAllHotelRedemptionPoints()
        {
            return GetAllHotelRedemptionPoints(false);
        }

        public static string GetExternalUrl(string linkURL)
        {
            UrlBuilder url = new UrlBuilder(linkURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, linkURL,
                                                                  System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }

        /// <summary>
        /// returns the list of child pages under the pagelink
        /// </summary>
        /// <param name="parentLink">parent pagelink which </param>
        /// <returns></returns>
        public static PageDataCollection GetChildren(PageReference parentLink)
        {
            return ContentDataAccess.GetChildren(parentLink);
        }

        public static PageData GetPageData(PageReference pageLink)
        {
            return ContentDataAccess.GetPageData(pageLink);
        }      

        public MerchantProfileEntity GetMerchantByHotelOperaId(string hotelOperaID, string language)
        {
            MerchantProfileEntity merchantProfile = null;
            PageData merchantPageData = ContentDataAccess.GetMerchantByHotelOperaId(hotelOperaID, language);
            if (merchantPageData != null)
            {
                merchantProfile = new MerchantProfileEntity(
                    merchantPageData["MerchantID"]!= null?merchantPageData["MerchantID"].ToString():string.Empty,
                    merchantPageData["MerchantName"] != null ? merchantPageData["MerchantName"].ToString() : string.Empty,
                    merchantPageData["MerchantToken"] != null ? merchantPageData["MerchantToken"].ToString() : string.Empty,
                    merchantPageData["CurrencyCode"] != null ? merchantPageData["CurrencyCode"].ToString() : string.Empty,
                    merchantPageData["LegalInformation"] != null ? merchantPageData["LegalInformation"].ToString() : string.Empty,
                    merchantPageData["OrganizationNumber"] != null ? merchantPageData["OrganizationNumber"].ToString() : string.Empty,
                    merchantPageData["Address"] != null ? merchantPageData["Address"].ToString() : string.Empty,
                    merchantPageData["Phone"] != null ? merchantPageData["Phone"].ToString() : string.Empty,
                    merchantPageData["Fax"] != null ? merchantPageData["Fax"].ToString() : string.Empty,
                    merchantPageData["Email"] != null ? merchantPageData["Email"].ToString() : string.Empty,
                    merchantPageData["CompanyName"] != null ? merchantPageData["CompanyName"].ToString() : string.Empty,
                    merchantPageData["VisaCard"] != null ? Convert.ToBoolean( merchantPageData["VisaCard"]) : false,
                    merchantPageData["MasterCard"] != null ? Convert.ToBoolean( merchantPageData["VisaCard"]) : false,
                    merchantPageData["DinersCard"] != null ? Convert.ToBoolean( merchantPageData["VisaCard"]) : false,
                    merchantPageData["AmexCard"] != null ? Convert.ToBoolean( merchantPageData["VisaCard"]) : false,
                    merchantPageData["DankortCard"] != null ? Convert.ToBoolean( merchantPageData["VisaCard"]) : false,
                    merchantPageData["JCBCard"] != null ? Convert.ToBoolean( merchantPageData["VisaCard"]) : false
                    );                
            }
            return merchantProfile;
        }

        
        
        public MerchantProfileEntity GetMerchantByHotelOperaId(string hotelOperaID)
        {
            return GetMerchantByHotelOperaId(hotelOperaID, string.Empty);
        }

        public bool GetPaymentFallback(string hotelOperaID)
        {
            return ContentDataAccess.GetPaymentFallback(hotelOperaID);
        }

        #region CacheFiller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isCacheFillerRequest"></param>
        /// <returns></returns>
        public IList<HotelRedemptionPoints> GetAllHotelRedemptionPoints(bool isCacheFillerRequest)
        {
            List<HotelRedemptionPoints> hotelRedemptionPointsList = new List<HotelRedemptionPoints>();


            string currentCulture = string.Empty;
            string currentLanguage = string.Empty;


            if (!string.IsNullOrEmpty(CMSSessionWrapper.CurrentLanguage))
            {
                currentLanguage = CMSSessionWrapper.CurrentLanguage;
            }
            else
            {
                currentLanguage = AppConstants.DEFAULT_LANGUAGE;
            }

            string cacheKey = string.Format(AppConstants.HotelRedemptionsCacheKeyFormat, currentLanguage.Trim().ToLower());

            if (!string.IsNullOrEmpty(CMSSessionWrapper.CurrentCulture))
            {
                currentCulture = CMSSessionWrapper.CurrentCulture;
            }
            else
            {
                currentCulture = AppConstants.DEFAULT_CULTURE;
            }
            IFormatProvider cultureInfo = new CultureInfo(currentCulture);

            if ((ScanwebCacheManager.Instance.LookInCache<List<HotelRedemptionPoints>>(cacheKey) == null) || isCacheFillerRequest)
            {
                PageDataCollection countryPages = ContentDataAccess.GetChildPagesWithPageTypeId(CountryPageTypeID, PageReference.RootPage, currentLanguage);
                HotelRedemptionPoints redemptionPoints = null;
                HotelDestination hotel = new HotelDestination();

                foreach (PageData countryPage in countryPages)
                {
                    if (countryPage.CheckPublishedStatus(PagePublishedStatus.Published))
                    {
                        PageDataCollection cityPages = ContentDataAccess.GetChildPagesWithPageTypeId(CityPageTypeID, countryPage.PageLink, currentLanguage);
                        {
                            foreach (PageData cityPage in cityPages)
                            {
                                if (cityPage.CheckPublishedStatus(PagePublishedStatus.Published))
                                {
                                    PageDataCollection hotelPages = ContentDataAccess.GetChildPagesWithPageTypeId(HotelPageTypeID, cityPage.PageLink, currentLanguage);
                                    foreach (PageData hotelPage in hotelPages)
                                    {
                                        if (hotelPage.CheckPublishedStatus(PagePublishedStatus.Published))
                                        {
                                            redemptionPoints = new HotelRedemptionPoints();
                                            PageDataCollection hotelRedemptionPages = ContentDataAccess.GetChildPagesWithPageTypeId(HotelRedemptionPointsPageTypeID, hotelPage.PageLink, null);
                                            List<CampaignPeriods> periods = new List<CampaignPeriods>();
                                            redemptionPoints.Country = hotelPage["Country"] as string ?? string.Empty;
                                            redemptionPoints.CountryCode = countryPage["CountryCode"] as string ?? string.Empty;
                                            redemptionPoints.City = hotelPage["City"] as string ?? string.Empty;
                                            redemptionPoints.CityCode = cityPage["OperaId"] as string ?? string.Empty;
                                            redemptionPoints.Hotel = hotelPage["Heading"] as string ?? string.Empty;
                                            redemptionPoints.HotelCode = hotelPage["OperaId"] as string ?? string.Empty;
                                            redemptionPoints.HotelLnk = GetExternalUrl(hotelPage.LinkURL);

                                            foreach (PageData hotelRedemptionPage in hotelRedemptionPages)
                                            {
                                                if (hotelRedemptionPage.CheckPublishedStatus(PagePublishedStatus.Published))
                                                {
                                                    if (hotelRedemptionPage["Points"] != null)
                                                    {
                                                        if (hotelRedemptionPage["Startdate"] == null && hotelRedemptionPage["Enddate"] == null)
                                                            redemptionPoints.Points = hotelRedemptionPage["Points"].ToString();
                                                        else
                                                        {
                                                            CampaignPeriods period = new CampaignPeriods();
                                                            period.Points = hotelRedemptionPage["Points"].ToString();
                                                            period.StartDate = Convert.ToDateTime(hotelRedemptionPage["Startdate"]);
                                                            period.EndDate = Convert.ToDateTime(hotelRedemptionPage["Enddate"]);
                                                            if (period.EndDate >= DateTime.Today)
                                                                periods.Add(period);
                                                        }
                                                    }
                                                }
                                            }
                                            redemptionPoints.CampaignPeriod = periods;
                                            if (redemptionPoints.Points != null)
                                                hotelRedemptionPointsList.Add(redemptionPoints);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (hotelRedemptionPointsList != null && hotelRedemptionPointsList.Count > 0)
                {
                    AppLogger.LogInfoMessage(string.Format("CDAL - Returning Cached HotelRedemptionPoints With CacheKey={0}", cacheKey));
                    ScanwebCacheManager.Instance.AddToCache(cacheKey, hotelRedemptionPointsList, new Collection<object>{true},
                        (Func<bool,IList<HotelRedemptionPoints>>)GetAllHotelRedemptionPoints);
                }
            }
            else
            {
                hotelRedemptionPointsList = ScanwebCacheManager.Instance.LookInCache<List<HotelRedemptionPoints>>(cacheKey);
            }

            return hotelRedemptionPointsList;
        }
        #endregion

    }
}
