using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.SessionState;
using System.Xml;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// This is the Session Wrapper to be used across the booking engine
    /// to set or retrieve any variables from the session
    /// </summary>
    public class SessionWrapper
    {
        #region FastTrackEnrolment

        private const string FASTTRACK_PARAMETERS = "BE_FASTTRACK_PARAMETERS";

        /// <summary>
        /// Gets/Sets fast track enrollment
        /// </summary>
        public static FastTrackEnrolmentEntity FastTrackEnrolment
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[FASTTRACK_PARAMETERS] == null)
                {
                    var fastTrackEnrolmentEntity = new FastTrackEnrolmentEntity();
                    HttpContext.Current.Session.Add(FASTTRACK_PARAMETERS, fastTrackEnrolmentEntity);
                }

                return session.Contents[FASTTRACK_PARAMETERS] as FastTrackEnrolmentEntity;
            }
            set { HttpContext.Current.Session.Add(FASTTRACK_PARAMETERS, value); }
        }

        #endregion FastTrackEnrolment

        #region SearchCriteria

        private const string SEARCH_CRITERIA = "BE_SEARCH_CRITERIA";

        private const string UNBLOCK_ROOMLIST = "UNBLOCK_ROOMLIST";

        private const string CLONE_SEARCH_CRITERIA = "BE_CLONE_SEARCH_CRITERIA";

        private const string CLONE_SELECTED_ROOM_AND_RATE = "BE_CLONE_SELECTED_ROOM_AND_RATE";

        /// <summary>
        /// The Hotel Search Criteria object containing the details
        /// the user has searched for.
        /// The values are stored in a HotelSearchEntity object.
        /// The session object is updated in the code behind of the search user controls
        /// </summary>
        public static HotelSearchEntity SearchCriteria
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SEARCH_CRITERIA] as HotelSearchEntity;
            }
            set
            {
                HttpContext.Current.Session.Add(SEARCH_CRITERIA, value);
                if (value != null)
                {
                    HotelSearchEntity cloneHotelSearch = value.Clone();
                    if (cloneHotelSearch != null)
                    {
                        CloneSearchCriteria = cloneHotelSearch;
                    }
                }
            }
        }

        /// <summary>
        /// Gets/Sets UnblockRoomList
        /// </summary>
        public static NameValueCollection UnblockRoomList
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[UNBLOCK_ROOMLIST] as NameValueCollection;
            }
            set { HttpContext.Current.Session.Add(UNBLOCK_ROOMLIST, value); }
        }

        /// <summary>
        /// ********************************************************************
        /// IMPORTANT:         
        /// ==========
        /// This Session variable NOT to be used any where in Scanweb. 
        /// This is entirely for the use of SOGETI for Site Catalyst.
        /// ********************************************************************
        /// The Hotel Search Criteria object containing the details
        /// the user has searched for.
        /// The values are stored in a HotelSearchEntity object.
        /// The session object is updated in the code behind of the search user controls
        /// </summary>
        public static HotelSearchEntity CloneSearchCriteria
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[CLONE_SEARCH_CRITERIA] as HotelSearchEntity;
            }
            set { HttpContext.Current.Session.Add(CLONE_SEARCH_CRITERIA, value); }
        }

        /// <summary>        
        /// This Session variable is used for keeping a deep copy of Selected Room Rate Entity
        /// which is used in Booking Confirmation page.
        /// </summary>
        public static SelectedRoomAndRateEntity CloneSelectedRoomAndRateEntity
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[CLONE_SELECTED_ROOM_AND_RATE] as SelectedRoomAndRateEntity;
            }
            set { HttpContext.Current.Session.Add(CLONE_SELECTED_ROOM_AND_RATE, value); }
        }

        #endregion SearchCriteria

        #region HotelRoomRate

        private const string LIST_HOTEL_RATE = "BE_LIST_HOTEL_ROOM_RATE";

        private const string LIST_ROOM_STAY = "BE_LIST_ROOM_STAY";

        private const string TOTAL_RATE_CALCULATED = "BE_TOTAL_RATE_CALCULATED";

        private const string HASH_SELECTED_ROOM_AND_RATES = "BE_HASH_SELECTED_ROOM_AND_RATES";

        private const string SELECTED_RATE_CATEGORTY_ID = "BE_SELECTED_ROOM_CATEGORTY_ID";

        /// <summary>
        /// The Hotel Room Rate object containing the details of the rate of
        /// the room selected by the user.
        /// The values are stored in a HotelRoomRateEntity object.
        /// The session object is updated in the code behind of the Select Rate user controls
        /// </summary>
        public static IList<BaseRoomRateDetails> ListHotelRoomRate
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[LIST_HOTEL_RATE] as List<BaseRoomRateDetails>;
            }
            set { HttpContext.Current.Session.Add(LIST_HOTEL_RATE, value); }
        }

        /// <summary>
        /// The Hotel Room Rate object containing the details of the rate of
        /// the room selected by the user.
        /// The values are stored in a HotelRoomRateEntity object.
        /// The session object is updated in the code behind of the Select Rate user controls
        /// </summary>
        public static IList<RoomStay> ListRoomStay
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[LIST_ROOM_STAY] as List<RoomStay>;
            }
            set { HttpContext.Current.Session.Add(LIST_ROOM_STAY, value); }
        }

        /// <summary>
        /// Flag to indicate whether the Total Rate is already calculated for the fetched list of bookings.
        /// </summary>
        public static bool TotalRateCalculated
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                object objTotalRateCalculated = session.Contents[TOTAL_RATE_CALCULATED];
                if (objTotalRateCalculated != null)
                {
                    return (bool) objTotalRateCalculated;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[TOTAL_RATE_CALCULATED] = value;
            }
        }

        /// <summary>
        /// The Hotel Room Rate object containing the details of the rate of
        /// the room selected by the user.
        /// The values are stored in a HotelRoomRateEntity object.
        /// The session object is updated in the code behind of the Select Rate user controls
        /// </summary>
        public static Hashtable SelectedRoomAndRatesHashTable
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[HASH_SELECTED_ROOM_AND_RATES] as Hashtable;
            }
            set { HttpContext.Current.Session[HASH_SELECTED_ROOM_AND_RATES] = value; }
        }

        /// <summary>
        /// Get/Set the Selected Room Category ID
        /// </summary>
        public static string SelectedRateCategoryID
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SELECTED_RATE_CATEGORTY_ID] as string;
            }
            set { HttpContext.Current.Session[SELECTED_RATE_CATEGORTY_ID] = value; }
        }

        #endregion

        #region GuestBookingInformation

        private const string GUEST_BOOKING_INFO = "BE_GUEST_BOOKING_INFO";

        private const string ALL_GUESTS_BOOKING_INFO = "BE_ALL_GUESTS_BOOKING_INFO";

        private const string FETCHED_GUEST_INFO = "BE_FETCHED_GUEST_INFO";

        private const string GURANTEE_TYPE_DISPLAY_TEXT = "BE_GURANTEE_TYPE_DISPLAY_TEXT";

        /// <summary>
        /// The Guest Booking Info object contains the details of the booking information
        /// of the guest.
        /// The values are stored in a GuestInformationEntity object.
        /// The session object is updated in the code behind of the Booking Details user controls
        /// </summary>
        public static GuestInformationEntity GuestBookingInformation
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[GUEST_BOOKING_INFO] as GuestInformationEntity;
            }
            set { HttpContext.Current.Session[GUEST_BOOKING_INFO] = value; }
        }

        /// <summary>
        /// The Guest Booking Info object contains the details of the booking information
        /// of the guest.
        /// The values are stored in a GuestInformationEntity object.
        /// The session object is updated in the code behind of the Booking Details user controls
        /// </summary>
        public static SortedList<string, GuestInformationEntity> AllGuestsBookingInformations
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[ALL_GUESTS_BOOKING_INFO] as SortedList<string, GuestInformationEntity>;
            }
            set { HttpContext.Current.Session[ALL_GUESTS_BOOKING_INFO] = value; }
        }

        /// <summary>
        /// The Guest Info object contains the details of the profile of the of the guest.
        /// The values are stored in a GuestInformationEntity object.
        /// </summary>
        public static GuestInformationEntity FetchedGuestInformation
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[FETCHED_GUEST_INFO] as GuestInformationEntity;
            }
            set { HttpContext.Current.Session[FETCHED_GUEST_INFO] = value; }
        }

        /// <summary>
        /// The Guest Info object contains the details of the profile of the of the guest.
        /// The values are stored in a GuestInformationEntity object.
        /// </summary>
        public static string GuranteeTypeDisplayText
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[GURANTEE_TYPE_DISPLAY_TEXT] as string;
            }
            set { HttpContext.Current.Session[GURANTEE_TYPE_DISPLAY_TEXT] = value; }
        }

        #endregion

        #region UserProfileInformation

        private const string USER_PROFILE_INFO = "BE_USER_PROFILE_INFO";

        /// <summary>
        /// The User profile Info object contains the details of the User information
        /// The values are stored in a UserProfileEntity object.
        /// The session object is updated in the code behind of the Enroll user controls
        /// </summary>
        public static UserProfileEntity UserProfileInformation
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[USER_PROFILE_INFO] as UserProfileEntity;
            }
            set { HttpContext.Current.Session[USER_PROFILE_INFO] = value; }
        }

        #endregion

        #region Reservation Number

        /// <summary>
        /// Reservation Number Constant
        /// </summary>
        private const string RESERVATION_NUMBER = "BE_RESERVATION_NUMBER";

        /// <summary>
        /// Reservation Number Constant
        /// </summary>
        private const string TRANSACTION_COUNT = "BE_TRANSACTION_COUNT";

        /// <summary>
        /// Contains the Booking number for the last booking
        /// </summary>
        public static string ReservationNumber
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[RESERVATION_NUMBER] != null)
                {
                    return session.Contents[RESERVATION_NUMBER] as string;
                }
                else
                {
                    return string.Empty;
                }
            }
            set { HttpContext.Current.Session[RESERVATION_NUMBER] = value; }
        }

        /// <summary>
        /// Contains the Booking number for the last booking
        /// </summary>
        public static long TransactionCount
        {
            get
            {
                long result = -1;
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[TRANSACTION_COUNT] != null)
                {
                    object objTranscationCount = session.Contents[TRANSACTION_COUNT];
                    if (objTranscationCount != null)
                    {
                        result = (long) objTranscationCount;
                    }
                }
                return result;
            }
            set { HttpContext.Current.Session[TRANSACTION_COUNT] = value; }
        }

        #endregion

        #region Selected Card Type

        /// <summary>
        /// Selected card type constant
        /// </summary>
        private const string SELECTED_CARD_TYPE = "BE_CARD_TYPE";

        /// <summary>
        /// Get/Set the selected card type
        /// </summary>
        public static string SelectedCardType
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[SELECTED_CARD_TYPE] != null)
                {
                    return session.Contents[SELECTED_CARD_TYPE] as string;
                }
                else
                {
                    return string.Empty;
                }
            }
            set { HttpContext.Current.Session[SELECTED_CARD_TYPE] = value; }
        }

        #endregion

        #region TotalRegionalAvailableHotels

        private const string TOTAL_REGIONAL_AVAIL_HOTEL_COUNT = "BE_TOTAL_REGIONAL_AVAIL_COUNT";

        /// <summary>
        /// This property will contain the total of the number of hotels returned
        /// by the Regional availability search done. 
        /// 
        /// This value will be compared against the total number of General Availability
        /// search results done dynamically. To check if the browser has to do
        /// request for further hotels
        /// </summary>
        public static int TotalRegionalAvailableHotels
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[TOTAL_REGIONAL_AVAIL_HOTEL_COUNT] != null)
                {
                    return ((int) session.Contents[TOTAL_REGIONAL_AVAIL_HOTEL_COUNT]);
                }
                else
                {
                    return int.MinValue;
                }
            }
            set { HttpContext.Current.Session.Add(TOTAL_REGIONAL_AVAIL_HOTEL_COUNT, value); }
        }

        #endregion TotalRegionalAvailableHotels

        #region TotalGeneralAvailableHotels

        private const string TOTAL_GENERAL_AVAIL_HOTEL_COUNT = "BE_TOTAL_GENERAL_AVAIL_COUNT";

        /// <summary>
        /// This property will contain the total of the number of General availabilty
        /// searches are done through thread and values are returned from OWS. 
        /// 
        /// This value will be compared against the total number of Regional availabilty
        /// results retured, To check if the browser has to do request for further hotels
        /// </summary>
        public static int TotalGeneralAvailableHotels
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[TOTAL_GENERAL_AVAIL_HOTEL_COUNT] != null)
                {
                    return ((int) session.Contents[TOTAL_GENERAL_AVAIL_HOTEL_COUNT]);
                }
                else
                {
                    return int.MinValue;
                }
            }

            set { HttpContext.Current.Session.Add(TOTAL_GENERAL_AVAIL_HOTEL_COUNT, value); }
        }

        public static int GetTotalGeneralAvailableHotels(HttpSessionState session)
        {
            if (session.Contents[TOTAL_GENERAL_AVAIL_HOTEL_COUNT] != null)
            {
                return ((int) session.Contents[TOTAL_GENERAL_AVAIL_HOTEL_COUNT]);
            }
            else
            {
                return int.MinValue;
            }
        }

        public static void SetTotalGeneralAvailableHotels(HttpSessionState session, int hotels)
        {
            session.Add(TOTAL_GENERAL_AVAIL_HOTEL_COUNT, hotels);
        }

        #endregion TotalGeneralAvailableHotels

        #region HotelResults

        private const string SEARCH_RESULTS = "BE_HOTEL_SEARCH_RESULTS";


        /// <summary>
        /// The List of hotels avialble for the criteria user has searched for
        /// The Session object is updated in the Controller as the hotels
        /// are returned from the General Availabilty searches which are called
        /// through multithreading Thread pool
        /// </summary>
        public static List<IHotelDetails> HotelResults
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SEARCH_RESULTS] as List<IHotelDetails>;
            }

            set { HttpContext.Current.Session.Add(SEARCH_RESULTS, value); }
        }

        /// <summary>
        /// Gets hotel results
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static List<IHotelDetails> GetHotelResults(HttpSessionState session)
        {
            return session.Contents[SEARCH_RESULTS] as List<IHotelDetails>;
        }

        /// <summary>
        /// Sets hotel results
        /// </summary>
        /// <param name="session"></param>
        /// <param name="hotels"></param>
        public static void SetHotelResults(HttpSessionState session, List<IHotelDetails> hotels)
        {
            session.Add(SEARCH_RESULTS, hotels);
        }

        #endregion

        #region AlternateHotels

        private const string DISPLAY_ALTERNATE_HOTEL = "BE_DISPLAY_ALTERNATE_HOTEL";

        private const string DISPLAY_NO_HOTEL_AVAILABLE = "BE_DISPLAY_NO_HOTEL_AVAILABLE";

        /// <summary>
        /// Set the flag to display Alternate Hotels flow
        /// </summary>        
        public static bool DisplayAlternateHotelsAvailable
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object alternateHotelsDiplay = session.Contents[DISPLAY_ALTERNATE_HOTEL];
                if (alternateHotelsDiplay != null)
                {
                    result = (bool) alternateHotelsDiplay;
                }
                return result;
            }
            set { HttpContext.Current.Session[DISPLAY_ALTERNATE_HOTEL] = value; }
        }

        /// <summary>
        /// Set the flag to display Alternate Hotels flow
        /// </summary>        
        public static bool DisplayNoHotelsAvailable
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object alternateHotelsDiplay = session.Contents[DISPLAY_NO_HOTEL_AVAILABLE];
                if (alternateHotelsDiplay != null)
                {
                    result = (bool) alternateHotelsDiplay;
                }
                return result;
            }
            set { HttpContext.Current.Session[DISPLAY_NO_HOTEL_AVAILABLE] = value; }
        }

        #endregion

        #region CleanSessionSearchResults

        /// <summary>
        /// This will clear all the hotel search results stored in the session
        /// Each search will be considered as new so all the details need to be
        /// populated again in the Select Hotel Page.
        /// </summary>
        public static void CleanSessionSearchResults()
        {
            TotalRegionalAvailableHotels = int.MinValue;
            TotalGeneralAvailableHotels = int.MinValue;
            HotelResults = null;
            DisplayAlternateHotelsAvailable = false;
            DisplayNoHotelsAvailable = false;
            NoRedemptionHotelsAvailable = false;

            GoogleMapResults = new List<Dictionary<string, object>>();

            IsModifyBooking = false;
            IsCitySortDoneInSelectHotel = false;

            ListHotelRoomRate = null;
            TotalRateCalculated = false;
            SelectedRoomAndRatesHashTable = null;
            ListRoomStay = null;
            AvailabilityCalendar = null;
            IsPerStaySelectedInSelectRatePage = false;
            IsModifySelectRateFromBreadCrumb = false;
        }

        /// <summary>
        /// Clears the combo reservation booleans.
        /// </summary>
        public static void ClearComboReservationBooleans()
        {
            HttpContext.Current.Session[IS_COMBO] = false;
        }

        #endregion CleanSessionSearchResults

        #region GoogleMapHotelResults

        private const string GOOGLE_MAP_HOTELS = "BE_GoogleMapHotels";

        /// <summary>
        /// This list contains the hotel details which are required by the
        /// Google map displayed on the Select a hotel page.
        /// These details are picked up by the GoogleMap user control from 
        /// the session and the hotel details are displayed accordingly
        /// </summary>
        public static List<Dictionary<string, object>> GoogleMapResults
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[GOOGLE_MAP_HOTELS] as List<Dictionary<string, object>>;
            }

            set { HttpContext.Current.Session.Add(GOOGLE_MAP_HOTELS, value); }
        }

        /// <summary>
        /// Gets google map results
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static List<Dictionary<string, object>> GetGoogleMapResults(HttpSessionState session)
        {
            return session.Contents[GOOGLE_MAP_HOTELS] as List<Dictionary<string, object>>;
        }

        /// <summary>
        /// Sets google map results
        /// </summary>
        /// <param name="session"></param>
        /// <param name="value"></param>
        public static void SetGoogleMapResults(HttpSessionState session, List<Dictionary<string, object>> value)
        {
            session.Add(GOOGLE_MAP_HOTELS, value);
        }

        #endregion

        #region LoyaltyDetails

        private const string LOYALTY_DETAILS = "BE_LOYALTY_DETAILS";

        /// <summary>
        /// The Loyalty details object containing the user name and the NameID
        /// of the Loyalty user after the user is logged into the system.
        /// This object existence in the session tells whether the user is 
        /// logged into the system or not
        /// </summary>
        public static LoyaltyDetailsEntity LoyaltyDetails
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[LOYALTY_DETAILS] as LoyaltyDetailsEntity;
            }

            set { HttpContext.Current.Session.Add(LOYALTY_DETAILS, value); }
        }

        /// <summary>
        /// Gets loyalty details.
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static LoyaltyDetailsEntity GetLoyaltyDetails(HttpSessionState session)
        {
            return session.Contents[LOYALTY_DETAILS] as LoyaltyDetailsEntity;
        }

        #endregion LoyaltyDetails

        #region UserLoggedIn

        private const string BE_USER_LOGGED_IN = "IsLoggedInUser";

        /// <summary>
        /// Returns true if the user is logged in to the system already
        /// else returns false
        /// 
        /// The logic is to check if the LoyaltyDetailsEntity is already in session return true
        /// if the value is not already saved into the session false is returned
        /// </summary>
        public static bool UserLoggedIn
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object loggedInUser = session.Contents[BE_USER_LOGGED_IN];
                if (loggedInUser != null)
                {
                    result = (bool) loggedInUser;
                }
                return result;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[BE_USER_LOGGED_IN] = value;
            }
        }

        #endregion UserLoggedIn

        #region ShowRedemptionTab

        private const string BE_SHOW_REDEMPTION_TAB = "BE_SHOW_REDEMPTION_TAB";

        /// <summary>
        /// This is used to make the redemption tab in hotel search screen 
        /// active after user log in from redemption tab.
        /// </summary>
        public static bool ShowRedemptionTab
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                object showRedemption = session.Contents[BE_SHOW_REDEMPTION_TAB];
                if (showRedemption != null)
                {
                    return (bool) showRedemption;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[BE_SHOW_REDEMPTION_TAB] = value;
            }
        }

        #endregion ShowRedemptionTab

        #region Generic Error Page

        private const string GENERIC_ERROR_HEADER = "ErrorHeader";

        private const string GENERIC_ERROR_MESSAGE = "ErrorMessage";

        /// <summary>
        /// Sets/Gets GenericErrorHeader
        /// </summary>
        public static string GenericErrorHeader
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[GENERIC_ERROR_HEADER] as string;
            }
            set { HttpContext.Current.Session.Contents[GENERIC_ERROR_HEADER] = value; }
        }

        /// <summary>
        /// Sets/Gets GenericErrorMessage
        /// </summary>
        public static string GenericErrorMessage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[GENERIC_ERROR_MESSAGE] as string;
            }
            set { HttpContext.Current.Session.Contents[GENERIC_ERROR_MESSAGE] = value; }
        }

        #endregion Generic Error Page

        #region PreferredLanguageForEmail

        private const string BE_USER_PREFERRED_LANGUAGE = "UserPreferredLanguage";

        /// <summary>
        /// Conatin information about the user preferred language selected for 
        /// sending enroll email confirmation.
        /// </summary>
        public static string UserPreferredLanguage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[BE_USER_PREFERRED_LANGUAGE] as string;
            }

            set { HttpContext.Current.Session.Add(BE_USER_PREFERRED_LANGUAGE, value); }
        }

        #endregion PreferredLanguageForEmail

        #region BedTypePreference

        private const string BCD_BED_TYPE_PREFERENCE = "BedTypePreference";

        /// <summary>
        /// Conatin information about the user Bed Type Preference selected during Booking detail 
        /// </summary>
        public static string UserBedTypePreference
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[BCD_BED_TYPE_PREFERENCE] as string;
            }

            set { HttpContext.Current.Session[BCD_BED_TYPE_PREFERENCE] = value; }
        }

        #endregion BedTypePreference

        #region BedTypePreferenceForAllRooms

        /// <summary>
        /// Added by Shameem |Res 2.0 | Booking details page
        /// Bed type Preference for all rooms on Booking details page
        /// Information about the user Bed Type Preference selected during Booking detail 
        /// </summary>
        private const string BCD_BED_TYPE_PREFERENCE_FOR_ALL_ROOMS = "BedTypePreferenceForAllRooms";

        /// <summary>
        /// Conatin information about the user Bed Type Preference selected during Booking detail 
        /// </summary>
        public static List<string> UserBedTypePreferenceForAllRooms
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[BCD_BED_TYPE_PREFERENCE_FOR_ALL_ROOMS] as List<string>;
            }

            set { HttpContext.Current.Session[BCD_BED_TYPE_PREFERENCE_FOR_ALL_ROOMS] = value; }
        }

        #endregion BedTypePreferenceForAllRooms

        #region Errors

        private const string EMAIL_RECIPIENT_FAILED = "BE_EMAIL_RECIPIENT_FAILED";
        private const string SMS_RECIPIENT_FAILED = "BE_SMS_RECIPIENT_FAILED";

        /// <summary>
        /// Save the email delivery error to be displayed do the user
        /// </summary>
        /// </summary>
        public static List<string> FailedEmailRecipient
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[EMAIL_RECIPIENT_FAILED] as List<string>;
            }
            set { HttpContext.Current.Session[EMAIL_RECIPIENT_FAILED] = value; }
        }

        /// <summary>
        /// Save the sme delivery error to be displayed do the user
        /// </summary>
        public static List<string> FailedSMSRecepient
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SMS_RECIPIENT_FAILED] as List<string>;
            }
            set { HttpContext.Current.Session[SMS_RECIPIENT_FAILED] = value; }
        }

        /// <summary>
        /// Add to the List of Delivery Failed Email Recepient
        /// </summary>
        /// <param name="value"></param>
        public static void AddFailedEmailRecipient(string value)
        {
            List<string> listofDeliveryFailedEmail = null;
            if (HttpContext.Current.Session[EMAIL_RECIPIENT_FAILED] != null)
            {
                listofDeliveryFailedEmail =
                    HttpContext.Current.Session[EMAIL_RECIPIENT_FAILED] as List<string>;
            }

            if (listofDeliveryFailedEmail == null)
            {
                listofDeliveryFailedEmail = new List<string>();
                HttpContext.Current.Session[EMAIL_RECIPIENT_FAILED] = listofDeliveryFailedEmail;
            }
            listofDeliveryFailedEmail.Add(value);
        }

        /// <summary>
        /// Clear the FailedEmail Recepient
        /// </summary>
        public static void ClearFailedEmailRecipient()
        {
            var listofDeliveryFailedEmail =
                HttpContext.Current.Session[EMAIL_RECIPIENT_FAILED] as List<string>;

            if (listofDeliveryFailedEmail != null)
            {
                listofDeliveryFailedEmail.Clear();
            }
        }

        /// <summary>
        /// Add to the List of Delivery Failed SMS Recepient
        /// </summary>
        /// <param name="value"></param>
        public static void AddFailedSMSRecepient(string value)
        {
            var listofDeliveryFailedSMS =
                HttpContext.Current.Session[SMS_RECIPIENT_FAILED] as List<string>;

            if (listofDeliveryFailedSMS == null)
            {
                listofDeliveryFailedSMS = new List<string>();
                HttpContext.Current.Session[SMS_RECIPIENT_FAILED] = listofDeliveryFailedSMS;
            }
            listofDeliveryFailedSMS.Add(value);
        }

        /// <summary>
        /// Clear the FailedEmail Recepient
        /// </summary>
        public static void ClearFailedSMSRecepient()
        {
            var listofDeliveryFailedSMS =
                HttpContext.Current.Session[SMS_RECIPIENT_FAILED] as List<string>;

            if (listofDeliveryFailedSMS != null)
            {
                listofDeliveryFailedSMS.Clear();
            }
        }

        #endregion

        #region IsEnrolledUser

        private const string ENROLLED_USER = "BE_ENROLLED_USER";

        /// <summary>
        /// This is to check if the user is enrolled or an exitsing user.
        /// If the user enrolled by hitting Enroll screen, then the value 
        /// for "IsEnrolledUser" set to true, but if an existing user login 
        /// to the system then the value for "IsEnrolledUser" set to false.
        /// </summary>
        public static bool IsEnrolledUser
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                object isEnrolledUSer = session.Contents[ENROLLED_USER];
                if (isEnrolledUSer != null)
                {
                    return (bool) isEnrolledUSer;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[ENROLLED_USER] = value;
            }
        }

        #endregion IsEnrolledUser

        #region Booking Engine

        private const string ALL_FETCHED_BOOKING_DETAILS = "BE_ALL_FETCHED_BOOKING_DETAILS";
        private const string AVAILABILITY_TABLE = "BE_AVAILABILITY_TABLE";
        private const string ALL_CANCELLED_DETAILS = "BE_ALL_CANCELLED_DETAILS";
        private const string FETCHED_BOOKING_DETAILS = "BE_FETCH_BOOKING_DETAILS";
        private const string ENABLED_BED_PREFERENCE = "BE_ENABLED_BED_PREFERENCE";
        private const string IS_BOOKING_MODIFIABLE = "BE_IS_BOOKING_MODIFIABLE";
        private const string IS_UPDATEPROFILE_BOOKINGINFO = "BE_IS_UPDATEPROFILE_BOOKINGINFO";
        private const string RESERVATION_CANCELLATION_NUMBER = "BE_RESERVATION_CANCELLATION_NUMBER";
        private const string IS_SMS_REQUIRED = "BE_IS_SMS_Required";
        private const string PREVIOUS_PAGE_URL = "BE_PREVIOUS_PAGE_URL";
        private const string MODIFYING_LEG_BOOKING = "BE_MODIFYING_LEG_BOOKING";

        /// <summary>
        /// Get/Set Booking details Entity used during Modify/Cancel Booking
        /// </summary>
        public static List<BookingDetailsEntity> AllBookingDetails
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[ALL_FETCHED_BOOKING_DETAILS] as List<BookingDetailsEntity>;
            }
            set { HttpContext.Current.Session[ALL_FETCHED_BOOKING_DETAILS] = value; }
        }

        /// <summary>
        /// Added Get property to fetch the ActiveBookingDetails derived from the AllBookingDetails
        /// Author: Ruman Khan
        /// </summary>
        public static List<BookingDetailsEntity> ActiveBookingDetails
        {
            get
            {
                var activeList = new List<BookingDetailsEntity>();

                if (AllBookingDetails != null)
                {
                    foreach (BookingDetailsEntity booking in AllBookingDetails)
                    {
                        if (!booking.IsCancelledByUser)
                        {
                            activeList.Add(booking);
                        }
                    }
                }
                return activeList;
            }
        }


        /// <summary>
        /// Gets/Sets AvailabilityTable
        /// </summary>
        public static Hashtable AvailabilityTable
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[AVAILABILITY_TABLE] as Hashtable;
            }
            set { HttpContext.Current.Session[AVAILABILITY_TABLE] = value; }
        }

        /// <summary>
        /// Get/Set Booking details Entity used during Modify/Cancel Booking
        /// </summary>
        public static List<CancelDetailsEntity> AllCancelledDetails
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[ALL_CANCELLED_DETAILS] as List<CancelDetailsEntity>;
            }
            set { HttpContext.Current.Session[ALL_CANCELLED_DETAILS] = value; }
        }

        /// <summary>
        /// Get/Set Booking details Entity used during Modify/Cancel Booking
        /// </summary>
        public static BookingDetailsEntity BookingDetails
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[FETCHED_BOOKING_DETAILS] as BookingDetailsEntity;
            }
            set { HttpContext.Current.Session[FETCHED_BOOKING_DETAILS] = value; }
        }

        /// <summary>
        /// Session Variable to indicate as to Enable/Disable Bed type Preference
        /// </summary>
        public static bool DirectlyModifyContactDetails
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objenableBedPreference = session.Contents[ENABLED_BED_PREFERENCE];
                if (objenableBedPreference != null)
                {
                    result = (bool) objenableBedPreference;
                }
                return result;
            }
            set { HttpContext.Current.Session[ENABLED_BED_PREFERENCE] = value; }
        }

        /// <summary>
        /// Session Variable to indicate the Modify/Cancel flow        
        /// </summary>
        public static bool IsModifyBooking
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objBookingModifiable = session.Contents[IS_BOOKING_MODIFIABLE];
                if (objBookingModifiable != null)
                {
                    result = (bool) objBookingModifiable;
                }
                return result;
            }
            set { HttpContext.Current.Session[IS_BOOKING_MODIFIABLE] = value; }
        }

        /// <summary>
        /// Session Variable to indicate the Modify Check box in checked in booking details page;
        /// this is to implement the Update credit card only when the user have checked the Modify check box.
        /// </summary>
        /// <remarks>
        /// https://resultspace2.sapient.com/sf/go/artf1030866?nav=1
        /// Do not update profile with the Credit Card information, if the user has not opted for 'Update Profile' option
        /// </remarks>
        public static bool UpdateProfileBookingInfo
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objUpdateProfile = session.Contents[IS_UPDATEPROFILE_BOOKINGINFO];
                if (objUpdateProfile != null)
                {
                    result = (bool) objUpdateProfile;
                }
                return result;
            }
            set { HttpContext.Current.Session[IS_UPDATEPROFILE_BOOKINGINFO] = value; }
        }

        /// <summary>
        /// Session Variable to store the Reservation Cancellation Number
        /// </summary>
        public static string ReservationCancellationNumber
        {
            get
            {
                string cancellatinNumber = string.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object objCancellationNumber = session.Contents[RESERVATION_CANCELLATION_NUMBER];
                if (objCancellationNumber != null)
                {
                    cancellatinNumber = objCancellationNumber as string;
                }
                return cancellatinNumber;
            }
            set { HttpContext.Current.Session[RESERVATION_CANCELLATION_NUMBER] = value; }
        }

        /// <summary>
        /// Session Variable to store the flag as whether SMS Confirmation is required
        /// </summary>
        public static bool IsSmsRequired
        {
            get
            {
                bool isSMSRequired = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objIsSmsRequired = session.Contents[IS_SMS_REQUIRED];
                if (objIsSmsRequired != null)
                {
                    isSMSRequired = (bool) objIsSmsRequired;
                }
                return isSMSRequired;
            }
            set { HttpContext.Current.Session[IS_SMS_REQUIRED] = value; }
        }

        /// <summary>
        /// Get/Set the Previous Page URL
        /// </summary>
        public static string PreviousPageURL
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[PREVIOUS_PAGE_URL] as string;
            }
            set { HttpContext.Current.Session[PREVIOUS_PAGE_URL] = value; }
        }

        /// <summary>
        /// Session Variable to store the flag as whether the user is currently modifying a particular leg of a booking.
        /// </summary>
        public static bool IsModifyingLegBooking
        {
            get
            {
                bool isModifyingLegBooking = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objIsModifyingLegBooking = session.Contents[MODIFYING_LEG_BOOKING];
                if (objIsModifyingLegBooking != null)
                {
                    isModifyingLegBooking = (bool) objIsModifyingLegBooking;
                }
                return isModifyingLegBooking;
            }
            set { HttpContext.Current.Session[MODIFYING_LEG_BOOKING] = value; }
        }

        /// <summary>
        /// Method returns the BookingDetails for respective booking from the active booking details
        /// Author: Ruman Khan
        /// </summary>
        /// <param name="legNumber"></param>
        /// <param name="reservationNumber"></param>
        /// <returns></returns>
        public static BookingDetailsEntity GetLegBookingDetails(string legNumber, string reservationNumber)
        {
            BookingDetailsEntity legBookingDetails = null;
            if (AllBookingDetails != null)
            {
                foreach (BookingDetailsEntity booking in AllBookingDetails)
                {
                    if (booking.LegNumber == legNumber && booking.ReservationNumber == reservationNumber)
                    {
                        legBookingDetails = booking;
                    }
                }
            }
            return legBookingDetails;
        }

        /// <summary>
        /// Gets availability table
        /// </summary>
        public static Hashtable GetAvailabilityTable(HttpSessionState session)
        {
            return session.Contents[AVAILABILITY_TABLE] as Hashtable;
        }

        /// <summary>
        /// Sets availability table
        /// </summary>
        /// <param name="session"></param>
        /// <param name="availabilityTable"></param>
        public static void SetAvailabilityTable(HttpSessionState session, Hashtable availabilityTable)
        {
            session.Add(AVAILABILITY_TABLE, availabilityTable);
        }

        #endregion

        #region Sogeti Variable

        private const string RESERVATION_NUMBER_FOR_THIRD_PARTY = "BE_RESVID";

        private const string MEMBERSHIP_LEVEL_FOR_THIRD_PARTY = "BE_TD_MEMBERSHIP_LEVEL";

        private const string POST_CODE_FOR_THIRD_PARTY = "BE_TD_POST_CODE";

        private const string TOTAL_AMOUNT_FOR_THIRD_PARTY = "BE_TD_TOTAL_AMOUNT";

        /// <summary>
        /// Session Variable to store the Reservation Cancellation Number as requested 
        /// by Sogeti
        /// </summary>
        public static string ReservationNumberForThirdParty
        {
            get
            {
                string reservationNumberForThirdParty = string.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object objReservationNumberForThirdParty = session.Contents[RESERVATION_NUMBER_FOR_THIRD_PARTY];
                if (objReservationNumberForThirdParty != null)
                {
                    reservationNumberForThirdParty = objReservationNumberForThirdParty as string;
                }
                return reservationNumberForThirdParty;
            }
            set { HttpContext.Current.Session[RESERVATION_NUMBER_FOR_THIRD_PARTY] = value; }
        }

        /// <summary>
        /// Session Variable to store the Reservation Cancellation Number as requested 
        /// by Sogeti
        /// </summary>
        public static string MemberShipLevelForThirdParty
        {
            get
            {
                string membershipLevelForThirdParty = string.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object objMemberShipLevelForThirdParty = session.Contents[MEMBERSHIP_LEVEL_FOR_THIRD_PARTY];
                if (objMemberShipLevelForThirdParty != null)
                {
                    membershipLevelForThirdParty = objMemberShipLevelForThirdParty as string;
                }
                return membershipLevelForThirdParty;
            }
            set { HttpContext.Current.Session[MEMBERSHIP_LEVEL_FOR_THIRD_PARTY] = value; }
        }

        /// <summary>
        /// Session Variable to store the Reservation Cancellation Number as requested 
        /// by Sogeti
        /// </summary>
        public static string PostCodeForThirdParty
        {
            get
            {
                string postCodeForThirdParty = string.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object objPostCodeForThirdParty = session.Contents[POST_CODE_FOR_THIRD_PARTY];
                if (objPostCodeForThirdParty != null)
                {
                    postCodeForThirdParty = objPostCodeForThirdParty as string;
                }
                return postCodeForThirdParty;
            }
            set { HttpContext.Current.Session[POST_CODE_FOR_THIRD_PARTY] = value; }
        }

        /// <summary>
        /// Session Variable to store the Reservation Cancellation Number as requested 
        /// by Sogeti
        /// </summary>
        public static string TotalAmountForThirdParty
        {
            get
            {
                string totalAmountForThirdParty = string.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object objtotalAmountForThirdParty = session.Contents[TOTAL_AMOUNT_FOR_THIRD_PARTY];
                if (objtotalAmountForThirdParty != null)
                {
                    totalAmountForThirdParty = objtotalAmountForThirdParty as string;
                }
                return totalAmountForThirdParty;
            }
            set { HttpContext.Current.Session[TOTAL_AMOUNT_FOR_THIRD_PARTY] = value; }
        }

        #endregion

        private const string RIGHTCAROUSEL_IDX = "RightCarouselIndex";

        private const string POLICYTEXTCONFIRMATION = "POLICYTEXTCONFIRMATION";

        /// <summary>
        /// Const key to retrieve IsXFormSubmitted
        /// </summary>
        private const string IS_XFORM_SUBMITTED = "IS_XFORM_SUBMITTED";

        private const string IS_COMBO = "IS_COMBO";

        private const string IS_MODIFY_SELECT_RATE_FROM_BREADCRUMB = "IS_MODIFY_SELECT_RATE_FROM_BREADCRUMB";

        /// <summary>
        /// Store the Total Amount in the Session
        /// </summary>
        public static string RightCarouselIdx
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[RIGHTCAROUSEL_IDX] as string;
            }
            set { HttpContext.Current.Session[RIGHTCAROUSEL_IDX] = value; }
        }

        /// <summary>
        /// Gets/Sets PolicyTextConfirmation
        /// </summary>
        public static Hashtable PolicyTextConfirmation
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[POLICYTEXTCONFIRMATION] as Hashtable;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[POLICYTEXTCONFIRMATION] = value;
            }
        }

        /// <summary>
        /// Gets or sets the session variable at key IS_XFORM_SUBMITTED
        /// </summary>
        public static bool IsXFormSubmitted
        {
            get
            {
                bool result = false;

                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[IS_XFORM_SUBMITTED] != null)
                    {
                        object objXFormSubmitted = session.Contents[IS_XFORM_SUBMITTED];
                        if (objXFormSubmitted != null)
                        {
                            result = (bool) objXFormSubmitted;
                        }
                    }
                }
                return result;
            }
            set
            {
                if (HttpContext.Current.Session.Contents[IS_XFORM_SUBMITTED] != null)
                    HttpContext.Current.Session[IS_XFORM_SUBMITTED] = value;
            }
        }

        /// <summary>
        /// Gets/Sets IsComboReservation
        /// </summary>
        public static bool IsComboReservation
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object isCombo = session.Contents[IS_COMBO];
                if (isCombo != null)
                {
                    result = (bool) isCombo;
                }
                return result;
            }
            set { HttpContext.Current.Session[IS_COMBO] = value; }
        }

        /// <summary>
        /// Gets/Sets IsModifySelectRateFromBreadCrumb
        /// </summary>
        public static bool IsModifySelectRateFromBreadCrumb
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object isMCFromBreadCrumb = session.Contents[IS_MODIFY_SELECT_RATE_FROM_BREADCRUMB];
                if (isMCFromBreadCrumb != null)
                {
                    result = (bool) isMCFromBreadCrumb;
                }
                return result;
            }
            set { HttpContext.Current.Session[IS_MODIFY_SELECT_RATE_FROM_BREADCRUMB] = value; }
        }

        /// <summary>
        /// Gets/Sets PartnerID
        /// </summary>
        public static string PartnerID
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents["PartnerID"] as string;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents["PartnerID"] = value;
            }
        }

        #region FindAHotel Session Variables

        private const string SORT_TYPE = "HOTEL_DESTINATION_SORT_TYPE";

        /// <summary>
        /// Constant used when the dropdown for Alphabetic/Distance from city centre is selected
        /// For Listing control
        /// </summary>
        private const string IS_HOTEL_SORTED = "IS_HOTEL_SORTED";


        private const string SEARCH_DESTINATION_RESULTS = "BE_HOTEL_DESTINATION_SEARCH_RESULTS";

        private const string IS_DEEPLINKING = "IS_DEEPLINKING";

        /// <summary>
        /// MARKER_DATA id
        /// </summary>
        private const string MARKER_DATA = "MARKER_DATA";

        private const string IS_SHOW_ALL = "IS_SHOW_ALL";

        private const string SELECTED_NODE_IN_TREEVIEW = "SELECTED_NODE_IN_TREEVIEW";

        private const string EVENTSOURCE = "EVENTSOURCE";

        /// <summary>
        /// Stores the Sorting type selected in hotel listing control
        /// Used for intimation to the google map
        /// </summary>
        public static string HotelSortOrder
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SORT_TYPE] as string;
            }

            set { HttpContext.Current.Session.Add(SORT_TYPE, value); }
        }

        /// <summary>
        /// This Property is set when the dropdown for Alphabetic/Distance from city centre is selected
        /// </summary>
        public static bool IsHotelSorted
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objIsHotelSorted = session.Contents[IS_HOTEL_SORTED];
                if (objIsHotelSorted != null)
                {
                    result = (bool) objIsHotelSorted;
                }
                return result;
            }

            set { HttpContext.Current.Session.Add(IS_HOTEL_SORTED, value); }
        }

        /// <summary>
        /// Set a session for deeplinking page.
        /// </summary>
        public static bool IsDeepLinking
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                object isDeeplinking = session.Contents[IS_DEEPLINKING];
                if (isDeeplinking != null)
                {
                    return (bool) session.Contents[IS_DEEPLINKING];
                }
                else
                {
                    return false;
                }
            }

            set { HttpContext.Current.Session.Add(IS_DEEPLINKING, value); }
        }

        /// <summary>
        /// Gets or sets MarkerData in Session
        /// </summary>
        public static XmlDocument MarkerData
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return (XmlDocument) session.Contents[MARKER_DATA];
            }
            set { HttpContext.Current.Session.Add(MARKER_DATA, value); }
        }

        /// <summary>
        /// Stores the state of Show/Hide link of Hotel Listing in 'Find A Hotel' Page
        /// </summary>
        public static bool IsShowAll
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objIsShowAll = session.Contents[IS_SHOW_ALL];
                if (objIsShowAll != null)
                {
                    result = (bool) objIsShowAll;
                }
                return result;
            }

            set { HttpContext.Current.Session.Add(IS_SHOW_ALL, value); }
        }

        /// <summary>
        /// The Hotel Search Criteria object containing the details
        /// the user has searched for.
        /// The values are stored in a HotelSearchEntity object.
        /// The session object is updated in the code behind of the search user controls
        /// </summary>
        public static Node SelectedNodeInTreeView
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[SELECTED_NODE_IN_TREEVIEW] as Node;
            }
            set { HttpContext.Current.Session.Add(SELECTED_NODE_IN_TREEVIEW, value); }
        }

        /// <summary>
        /// This is added for page tracking in find a hotel page.
        /// This will hold the value for the event source.
        /// </summary>
        public static string EventSource
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[EVENTSOURCE] as string;
            }
            set { HttpContext.Current.Session.Add(EVENTSOURCE, value); }
        }

        /// <summary>
        /// Gets the HotelDestination List
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static List<HotelDestination> GetHotelDestinationResults()
        {
            HttpSessionState session = HttpContext.Current.Session;
            return session.Contents[SEARCH_DESTINATION_RESULTS] as List<HotelDestination>;
        }

        /// <summary>
        /// Sets the HotelDestination List
        /// </summary>
        /// <param name="session"></param>
        /// <param name="hotels"></param>
        public static void SetHotelDestinationResults(List<HotelDestination> hotels)
        {
            HttpSessionState session = HttpContext.Current.Session;
            session.Add(SEARCH_DESTINATION_RESULTS, hotels);
        }

        #endregion FindAHotel Session Variables

        #region Digital Platform

        private const string EXPANSIONPAGE_HOTELLIST = "EXPANSIONPAGE_HOTELLIST";

        /// <summary>
        /// Set a session for deeplinking page.
        /// </summary>
        public static List<HotelDestination> ExpansionPageHotelList
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return (List<HotelDestination>) session.Contents[EXPANSIONPAGE_HOTELLIST];
            }
            set { HttpContext.Current.Session.Add(EXPANSIONPAGE_HOTELLIST, value); }
        }

        #endregion

        #region Hygiene

        private const string IS6PM_BOOKING = "IS6PM_BOOKING";

        private const string ISIE6POPUPCLOSED = "ISIE6POPUPCLOSED";

        private const string IS_SITE_REDIRECTED_ON_IP = "IS_SITE_REDIRECTED_ON_IP";

        /// <summary>
        /// Set a session for 6 PM booking.
        /// </summary>
        public static bool Is6PMBooking
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                bool bookingFlag = false;
                if (session.Contents[IS6PM_BOOKING] != null)
                {
                    bookingFlag = (bool) session.Contents[IS6PM_BOOKING];
                }
                return bookingFlag;
            }
            set { HttpContext.Current.Session.Add(IS6PM_BOOKING, value); }
        }

        /// <summary>
        /// This is use for checcking for IE6 pop up closed or not.
        /// </summary>
        public static bool IsIE6PopupClosed
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                bool bookingFlag = false;
                if (session.Contents[ISIE6POPUPCLOSED] != null)
                {
                    bookingFlag = (bool) session.Contents[ISIE6POPUPCLOSED];
                }
                return bookingFlag;
            }
            set { HttpContext.Current.Session.Add(ISIE6POPUPCLOSED, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is site redirected on ip.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is site redirected on ip; otherwise, <c>false</c>.
        /// </value>
        public static bool IsSiteRedirectedOnIp
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                bool isSiteRedirectedOnIp = false;
                if (session != null && session.Contents[IS_SITE_REDIRECTED_ON_IP] != null)
                {
                    isSiteRedirectedOnIp = (bool) session.Contents[IS_SITE_REDIRECTED_ON_IP];
                }
                return isSiteRedirectedOnIp;
            }
            set { HttpContext.Current.Session.Add(IS_SITE_REDIRECTED_ON_IP, value); }
        }

        #endregion

        #region Reservation 2.0

        private const string SELECTHOTELTAB = "SELECTHOTELTAB";


        private const string SHOWPRICEPERSTAYFORSELECTRATEPAGE = "SHOWPRICEPERSTAYFORSELECTRATEPAGE";

        private const string AVAILABILITY_CALENDAR = "BE_AVAILABILITY_CALENDAR";

        /// <summary>
        /// 
        /// </summary>
        private const string AVAILCALENDAR_HOTELDETAILS_LIST = "BE_AVAILCALENDAR_HOTELDETAILS_LIST";

        private const string SELECTED_CALENDAR_ITEM_INDEX = "BE_SELECTED_CALENDAR_ITEM_INDEX";

        /// <summary>
        /// This is used for if user enrolled from booking details page.
        /// </summary>
        private const string IS_USER_ENROLL_IN_BOOKINGMODULE = "IS_USER_ENROLL_IN_BOOKINGMODULE";

        /// <summary>
        /// Selected item value status
        /// </summary>
        private const string SELECTED_CALENDAR_ITEM_RATE_VALUE_STATUS = "BE_SELECTED_CALENDAR_ITEM_RATE_VALUE_STATUS";

        /// <summary>
        ///  This holds the starting index of the visibility availabilitycalendarItem in Select Rate page
        /// </summary>
        private const string LEFT_VISIBLE_CAROUSEL_INDEX = "BE_LEFT_VISIBLE_CAROUSEL_INDEX";

        /// <summary>
        /// Showing Reward Night Accordian After Login
        /// </summary>
        private const string BOOKING_MODULE_ACTIVE_TAB = "BookingModuleActiveTab";

        private const string IS_CALENDAR_FIRST_BATCH = "BE_IS_CALENDAR_FIRST_BATCH";

        private const string IS_CALENDAR_LAST_BATCH = "BE_IS_CALENDAR_LAST_BATCH";

        private const string IS_LOGIN_FROM_BOOKING_DETAIL = "IS_LOGIN_FROM_BOOKING_DETAIL";

        private const string IS_LOGIN_FROM_UPPERRGHT = "IS_LOGIN_FROM_UPPERRGHT";

        private const string IS_LOGIN_FROM_BOOKINGDETAILSPAGE = "IS_LOGIN_FROM_BOOKINGDETAILSPAGE";

        private const string CUR_LANG = "CUR_LANG_CODE";

        private const string Guarantee_Info_Rate = "Guarantee_Info_Rate";

        /// <summary>
        /// The About our rate heading from select rate page
        /// </summary>
        private const string ABOUTOURHEADING = "ABOUTOURHEADING";

        /// <summary>
        /// The About our rate heading from select rate page
        /// </summary>
        private const string ABOUTOURDESCRIPTION = "ABOUTOURDESCRIPTION";

        private const string IS_MODIFY_FLOW = "IS_MODIFY_FLOW";

        private const string MINRATECATEGORYNAME = "MINRATECATEGORYNAME";

        private const string ISEDITYOURSTAYSEARCHTRIGGERED = "ISEDITYOURSTAYSEARCHTRIGGERED";

        private const string ISCITYSORTDONEINSELECTHOTEL = "ISCITYSORTDONEINSELECTHOTEL";

        private const string SEARCHEDCITYNAME = "SEARCHEDCITYNAME";

        /// <summary>
        /// Holds which tab is selected in select hotel page. Bydefault it is list view.
        /// </summary>
        public static string SelectedTabInSelectHotel
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                //Default value of tab in select hotel.
                string bookingFlag = "listViewTab|.listView";
                if (session.Contents[SELECTHOTELTAB] != null)
                {
                    bookingFlag = session.Contents[SELECTHOTELTAB] as string;
                }
                return bookingFlag;
            }
            set { HttpContext.Current.Session.Add(SELECTHOTELTAB, value); }
        }

        /// <summary>
        /// Holds which tab is selected in select hotel page. Bydefault it is list view.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is per stay selected in select rate page; otherwise, <c>false</c>.
        /// </value>
        public static bool IsPerStaySelectedInSelectRatePage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                bool isPerStaySelectedInSelectRatePage = false;
                if (session.Contents[SHOWPRICEPERSTAYFORSELECTRATEPAGE] != null)
                {
                    isPerStaySelectedInSelectRatePage =
                        Convert.ToBoolean(session.Contents[SHOWPRICEPERSTAYFORSELECTRATEPAGE]);
                }
                return isPerStaySelectedInSelectRatePage;
            }
            set { HttpContext.Current.Session.Add(SHOWPRICEPERSTAYFORSELECTRATEPAGE, value); }
        }

        public static AvailabilityCalendarEntity AvailabilityCalendar
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[AVAILABILITY_CALENDAR] as AvailabilityCalendarEntity;
            }
            set { HttpContext.Current.Session.Add(AVAILABILITY_CALENDAR, value); }
        }

        /// <summary>
        /// Gets or sets the avail calendar hotel details list.
        /// </summary>
        /// <value>The avail calendar hotel details list.</value>
        public static IList<IHotelDetails> AvailCalendarHotelDetailsList
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[AVAILCALENDAR_HOTELDETAILS_LIST] as IList<IHotelDetails>;
            }

            set { HttpContext.Current.Session.Add(AVAILCALENDAR_HOTELDETAILS_LIST, value); }
        }

        public static int SelectedCalendarItemIndex
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SELECTED_CALENDAR_ITEM_INDEX] != null)
                {
                    return ((int) session.Contents[SELECTED_CALENDAR_ITEM_INDEX]);
                }
                else
                {
                    return 0;
                }
            }
            set { HttpContext.Current.Session.Add(SELECTED_CALENDAR_ITEM_INDEX, value); }
        }

        public static bool IsUserEnrollInBookingModule
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[IS_USER_ENROLL_IN_BOOKINGMODULE] != null)
                    {
                        return ((bool) session.Contents[IS_USER_ENROLL_IN_BOOKINGMODULE]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set { HttpContext.Current.Session.Add(IS_USER_ENROLL_IN_BOOKINGMODULE, value); }
        }

        /// <summary>
        /// Gets or sets the selected calender item rate value status.
        /// </summary>
        /// <value>The selected calender item rate value status.</value>
        public static string SelectedCalenderItemRateValueStatus
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SELECTED_CALENDAR_ITEM_RATE_VALUE_STATUS] != null)
                {
                    return ((string) session.Contents[SELECTED_CALENDAR_ITEM_RATE_VALUE_STATUS]);
                }
                else
                {
                    return string.Empty;
                }
            }
            set { HttpContext.Current.Session.Add(SELECTED_CALENDAR_ITEM_RATE_VALUE_STATUS, value); }
        }

        public static int LeftVisibleCarouselIndex
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[LEFT_VISIBLE_CAROUSEL_INDEX] != null)
                {
                    return ((Int32) session.Contents[LEFT_VISIBLE_CAROUSEL_INDEX]);
                }
                else
                {
                    return 0;
                }
            }
            set { HttpContext.Current.Session.Add(LEFT_VISIBLE_CAROUSEL_INDEX, value); }
        }

        /// <summary>
        /// Use for getting the proper bool value to justify that the flow is comming from BookingModuleBig after login
        /// </summary>
        /// true or fale
        public static bool BookingModuleActiveTab
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object bookingModuleActiveTab = session.Contents[BOOKING_MODULE_ACTIVE_TAB];
                if (bookingModuleActiveTab != null)
                {
                    result = (bool) bookingModuleActiveTab;
                }
                return result;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[BOOKING_MODULE_ACTIVE_TAB] = value;
            }
        }

        public static bool IsCalendarFirstBatch
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[IS_CALENDAR_FIRST_BATCH] != null)
                {
                    return ((bool) session.Contents[IS_CALENDAR_FIRST_BATCH]);
                }
                else
                {
                    return false;
                }
            }
            set { HttpContext.Current.Session.Add(IS_CALENDAR_FIRST_BATCH, value); }
        }

        public static bool IsCalendarLastBatch
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[IS_CALENDAR_LAST_BATCH] != null)
                {
                    return ((bool) session.Contents[IS_CALENDAR_LAST_BATCH]);
                }
                else
                {
                    return false;
                }
            }
            set { HttpContext.Current.Session.Add(IS_CALENDAR_LAST_BATCH, value); }
        }

        public static bool IsLoginFromBookingDetailsPage
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[IS_LOGIN_FROM_BOOKING_DETAIL] != null)
                    {
                        return ((bool) session.Contents[IS_LOGIN_FROM_BOOKING_DETAIL]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set { HttpContext.Current.Session.Add(IS_LOGIN_FROM_BOOKING_DETAIL, value); }
        }

        /// <summary>
        /// Gets/Sets IsLoginFromUpperRight
        /// </summary>
        public static bool IsLoginFromUpperRight
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[IS_LOGIN_FROM_UPPERRGHT] != null)
                    {
                        return ((bool) session.Contents[IS_LOGIN_FROM_UPPERRGHT]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set { HttpContext.Current.Session.Add(IS_LOGIN_FROM_UPPERRGHT, value); }
        }

        /// <summary>
        /// Gets/Sets IsLoginFromBookingPageLoginOrTopLogin
        /// </summary>
        public static bool IsLoginFromBookingPageLoginOrTopLogin
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[IS_LOGIN_FROM_BOOKINGDETAILSPAGE] != null)
                {
                    return ((bool) session.Contents[IS_LOGIN_FROM_BOOKINGDETAILSPAGE]);
                }
                else
                {
                    return false;
                }
            }
            set { HttpContext.Current.Session.Add(IS_LOGIN_FROM_BOOKINGDETAILSPAGE, value); }
        }

        /// <summary>
        /// Gets/Sets CurrentLanguage
        /// </summary>
        public static string CurrentLanguage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[CUR_LANG] == null ? null : session.Contents[CUR_LANG].ToString();
            }
            set { HttpContext.Current.Session.Add(CUR_LANG, value); }
        }

        /// <summary>
        /// Gets/Sets GuaranteeInfoRate
        /// </summary>
        public static Dictionary<string, string> GuaranteeInfoRate
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[Guarantee_Info_Rate] as Dictionary<string, string>;
            }
            set { HttpContext.Current.Session.Add(Guarantee_Info_Rate, value); }
        }

        /// <summary>
        /// Gets or sets the About our rate heading from select rate page.
        /// </summary>
        /// <value>The About our rate heading from select rate page</value>
        public static string AboutOurRateHeading
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[ABOUTOURHEADING] != null)
                {
                    return ((string) session.Contents[ABOUTOURHEADING]);
                }
                else
                {
                    return string.Empty;
                }
            }
            set { HttpContext.Current.Session.Add(ABOUTOURHEADING, value); }
        }

        /// <summary>
        /// Gets or sets the About our rate Description from select rate page.
        /// </summary>
        /// <value>The About our rate Description from select rate page</value>
        public static string AboutOurRateDescription
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[ABOUTOURDESCRIPTION] != null)
                {
                    return ((string) session.Contents[ABOUTOURDESCRIPTION]);
                }
                else
                {
                    return string.Empty;
                }
            }
            set { HttpContext.Current.Session.Add(ABOUTOURDESCRIPTION, value); }
        }

        /// <summary>
        /// Session Variable to indicate the Modify/Cancel flow        
        /// </summary>
        public static bool IsModifyFlow
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object objBookingModifiable = session.Contents[IS_MODIFY_FLOW];
                if (objBookingModifiable != null)
                {
                    result = (bool) objBookingModifiable;
                }
                return result;
            }
            set { HttpContext.Current.Session[IS_MODIFY_FLOW] = value; }
        }

        /// <summary>
        /// Gets/Sets MinRateCategoryName
        /// </summary>
        public static string MinRateCategoryName
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[MINRATECATEGORYNAME] != null)
                {
                    return ((string) session.Contents[MINRATECATEGORYNAME]);
                }
                else
                {
                    return string.Empty;
                }
            }
            set { HttpContext.Current.Session[MINRATECATEGORYNAME] = value; }
        }

        /// <summary>
        /// Gets/Sets IsEditYourStaySearchTriggered
        /// </summary>
        public static bool IsEditYourStaySearchTriggered
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpSessionState session = HttpContext.Current.Session;
                    if (session.Contents[ISEDITYOURSTAYSEARCHTRIGGERED] != null)
                    {
                        return ((bool) session.Contents[ISEDITYOURSTAYSEARCHTRIGGERED]);
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            set
            {
                if (HttpContext.Current.Session.Contents[ISEDITYOURSTAYSEARCHTRIGGERED] != null)
                    HttpContext.Current.Session[ISEDITYOURSTAYSEARCHTRIGGERED] = value;
            }
        }

        /// <summary>
        /// Gets/Sets IsCitySortDoneInSelectHotel
        /// </summary>
        public static bool IsCitySortDoneInSelectHotel
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[ISCITYSORTDONEINSELECTHOTEL] != null)
                {
                    return ((bool) session.Contents[ISCITYSORTDONEINSELECTHOTEL]);
                }
                else
                {
                    return false;
                }
            }
            set { HttpContext.Current.Session[ISCITYSORTDONEINSELECTHOTEL] = value; }
        }

        /// <summary>
        /// Gets/Sets SearchedCityName
        /// </summary>
        public static string SearchedCityName
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                if (session.Contents[SEARCHEDCITYNAME] != null)
                {
                    return ((string) session.Contents[SEARCHEDCITYNAME]);
                }
                else
                {
                    return string.Empty;
                }
            }
            set { HttpContext.Current.Session[SEARCHEDCITYNAME] = value; }
        }

        /// <summary>
        /// Gets the avail calendar hotel details list.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <returns></returns>
        public static IList<IHotelDetails> GetAvailCalendarHotelDetailsList(HttpSessionState session)
        {
            return session.Contents[AVAILCALENDAR_HOTELDETAILS_LIST] as IList<IHotelDetails>;
        }

        /// <summary>
        /// Sets the avail calendar hotel details list.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <param name="hotelDetailsList">The hotel details list.</param>
        public static void SetAvailCalendarHotelDetailsList(HttpSessionState session,
                                                            IList<IHotelDetails> hotelDetailsList)
        {
            session.Add(AVAILCALENDAR_HOTELDETAILS_LIST, hotelDetailsList);
        }

        #endregion

        #region Search Destination

        /// <summary>
        /// Search destination constant
        /// </summary>
        private const string SEARCH_DESTINATION = "BE_SEARCH_DESTINATION";

        /// <summary>
        /// This will check if searched destination is updated in the site catalyst variables: eVar11/s.prop11
        /// </summary>
        private const string SEARCH_DESTINATION_UPDATED = "BE_SEARCH_DESTINATION_UPDATED";

        /// <summary>
        /// Contains the search destination: City name/Hotel name
        /// </summary>
        public static string SearchDestination
        {
            get
            {
                string result = string.Empty;
                HttpSessionState session = HttpContext.Current.Session;
                object isDest = session.Contents[SEARCH_DESTINATION];
                if (isDest != null)
                {
                    result = (string) isDest;
                }
                return result;
            }
            set { HttpContext.Current.Session[SEARCH_DESTINATION] = value; }
        }

        /// <summary>
        /// Contains the search destination: City name/Hotel name
        /// </summary>
        public static bool SearchDestinationUpdated
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object iDest = session.Contents[SEARCH_DESTINATION_UPDATED];
                if (iDest != null)
                {
                    result = (bool) iDest;
                }
                return result;
            }
            set { HttpContext.Current.Session[SEARCH_DESTINATION_UPDATED] = value; }
        }

        #endregion

        #region Generic Session Variable

        private const string TOTAL_AMOUNT = "BE_TOTAL_AMOUNT";

        /// <summary>
        /// Store the Total Amount in the Session
        /// </summary>
        public static string TotalAmout
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                return session.Contents[TOTAL_AMOUNT] as string;
            }
            set { HttpContext.Current.Session[TOTAL_AMOUNT] = value; }
        }

        #endregion

        #region Last Visited Page

        /// <summary>
        /// It is used to store the current page which is visited by the user.
        /// This is used to redirect the user to the same page after successful login.
        /// </summary>
        private const string LAST_VISITED_PAGE = "BE_LAST_VISITED_PAGE";

        /// <summary>
        /// Get/Set of Last Visited Page
        /// </summary>
        public static PageDetails LastVisitedPage
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[LAST_VISITED_PAGE] != null)
                {
                    return session.Contents[LAST_VISITED_PAGE] as PageDetails;
                }
                else
                {
                    return null;
                }
            }
            set { HttpContext.Current.Session[LAST_VISITED_PAGE] = value; }
        }

        #endregion Last Visited Page

        #region Login Error code

        /// <summary>
        /// This variable is used to show a login error message to the user.
        /// This is designed as a code to give the flexibility of showing different messages based on the code.
        /// </summary>
        private const string LOGIN_ERROR_CODE = "BE_LOGIN_ERROR_CODE";

        /// <summary>
        /// Get/Set of Last Visited Page
        /// </summary>
        public static int LoginErrorCode
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                int errorCode = 0;
                if (session.Contents[LOGIN_ERROR_CODE] != null)
                {
                    Int32.TryParse(session.Contents[LOGIN_ERROR_CODE].ToString(), out errorCode);
                }
                return errorCode;
            }
            set { HttpContext.Current.Session[LOGIN_ERROR_CODE] = value; }
        }

        #endregion Login Error code

        #region Searched Hotel's City Name

        /// <summary>
        /// This variable is used to store the searched hotel's city name,which is used in SelectRate.ascx.cs
        /// to populate.Used in SelectHotel.ascx.cs to display the city text.
        /// </summary>
        private const string SEARCHED_HOTELS_CITY_NAME = "BE_SEARCHED_HOTELS_CITY_NAME";

        /// <summary>
        /// Gets or sets the searched hotel's city name.
        /// </summary>
        public static string SearchedHotelCityName
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                string cityName = string.Empty;
                if (session.Contents[SEARCHED_HOTELS_CITY_NAME] != null)
                {
                    cityName = session.Contents[SEARCHED_HOTELS_CITY_NAME] as string;
                }
                return cityName;
            }
            set { HttpContext.Current.Session[SEARCHED_HOTELS_CITY_NAME] = value; }
        }

        #endregion Searched Hotel's City Name

        #region LoginDemographics

        private const string LOGIN_DEMOGRAPHICS = "BE_LOGIN_DEMOGRAPHICS";

        /// <summary>
        /// It holds login control names of different modules from where user can login to the system.
        /// Login Popup / Booking Search / Booking Details / Login Error / Logout Confirmation
        /// </summary>
        public static Dictionary<LoginSourceModule, LoginDemographicEntity> LoginDemographics
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;

                if (session.Contents[LOGIN_DEMOGRAPHICS] != null)
                {
                    return session.Contents[LOGIN_DEMOGRAPHICS] as Dictionary<LoginSourceModule, LoginDemographicEntity>;
                }
                else
                {
                    return new Dictionary<LoginSourceModule, LoginDemographicEntity>();
                }
            }
            set { HttpContext.Current.Session.Add(LOGIN_DEMOGRAPHICS, value); }
        }

        #endregion LoginDemographics

        #region AlternativeCityHotelsSearch

        private const string ALT_CITY_HOTELS_SEARCH_DONE = "ALT_CITY_HOTELS_SEARCH_DONE";

        /// <summary>
        /// The Hotel Room Rate object containing the details of the rate of
        /// the room selected by the user.
        /// The values are stored in a HotelRoomRateEntity object.
        /// The session object is updated in the code behind of the Select Rate user controls
        /// </summary>
        public static bool AltCityHotelsSearchDone
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object altCityHotelsSearchDone = session.Contents[ALT_CITY_HOTELS_SEARCH_DONE];
                if (altCityHotelsSearchDone != null)
                {
                    result = (bool) altCityHotelsSearchDone;
                }
                return result;
            }
            set { HttpContext.Current.Session.Add(ALT_CITY_HOTELS_SEARCH_DONE, value); }
        }

        #endregion

        #region RedemptionHotelsAvailable

        private const string NO_REDEMPTION_HOTELS_AVAILABLE = "NO_REDEMPTION_HOTELS_AVAILABLE";

        /// <summary>
        /// This would store true if no hotels are available for Redemption search
        /// </summary>
        public static bool NoRedemptionHotelsAvailable
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object noRedemptionHotelsAvailable = session.Contents[NO_REDEMPTION_HOTELS_AVAILABLE];
                if (noRedemptionHotelsAvailable != null)
                {
                    result = (bool) noRedemptionHotelsAvailable;
                }
                return result;
            }
            set { HttpContext.Current.Session.Add(NO_REDEMPTION_HOTELS_AVAILABLE, value); }
        }

        #endregion

        #region CMS

        private const string RIGHT_COLUMN_PRESENT = "BE_RIGHT_COLUMN_PRESENT";

        /// <summary>
        /// This would store true if the Right Column of the pages need to display anything
        /// </summary>
        public static bool RightColumnPresent
        {
            get
            {
                bool result = false;
                HttpSessionState session = HttpContext.Current.Session;
                object rightColumnPresent = session.Contents[RIGHT_COLUMN_PRESENT];
                if (rightColumnPresent != null)
                {
                    result = (bool) rightColumnPresent;
                }
                return result;
            }
            set { HttpContext.Current.Session.Add(RIGHT_COLUMN_PRESENT, value); }
        }

        #endregion

        #region ConfirmationHeader

        private const string HEADER = "HEADER";

        /// <summary>
        /// Gets/Sets ConfirmationHeader
        /// </summary>
        public static string ConfirmationHeader
        {
            get
            {
                HttpSessionState session = HttpContext.Current.Session;
                object head = session.Contents[HEADER];
                string headerText = string.Empty;
                if (head != null)
                {
                    headerText = head.ToString();
                }
                return headerText;
            }
            set
            {
                HttpSessionState session = HttpContext.Current.Session;
                session.Contents[HEADER] = value;
            }
        }

        #endregion
    }
}