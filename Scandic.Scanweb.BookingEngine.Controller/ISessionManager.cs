﻿//  Description					: Session manager acts as a contract to access static     //
//								  session wrapper methods, and it enables to decouple     // 
//                                static methods that are getting called from controllers //
//----------------------------------------------------------------------------------------//
//  Author						: Sapient                                            	  //
//  Author email id				:                           							  //
//  Creation Date				: August 22nd, 2012                                       //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
// 	Last Modified Date			:                                                         //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using System.Web.SessionState;
using System.Collections;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// Session manager acts as a contract to access static session wrapper objects, 
    /// and it enables you to decouple static methods that are getting called from controllers.
    /// </summary>
    public interface ISessionManager
    {
        /// <summary>
        /// The Hotel Room Rate object containing the details of the rate of
        /// the room selected by the user.
        /// The values are stored in a HotelRoomRateEntity object.
        /// The session object is updated in the code behind of the Select Rate user controls
        /// </summary>
        bool AltCityHotelsSearchDone { get; set; }

        /// <summary>
        /// This property will contain the total of the number of hotels returned
        /// by the Regional availability search done. 
        /// 
        /// This value will be compared against the total number of General Availability
        /// search results done dynamically. To check if the browser has to do
        /// request for further hotels
        /// </summary>
        int TotalRegionalAvailableHotels { get; set; }

        /// <summary>
        /// Sets availability table
        /// </summary>
        /// <param name="session"></param>
        /// <param name="availabilityTable"></param>
        void SetAvailabilityTable(HttpSessionState session, Hashtable availabilityTable);

        /// <summary>
        /// This property will contain the total of the number of General availabilty
        /// searches are done through thread and values are returned from OWS. 
        /// 
        /// This value will be compared against the total number of Regional availabilty
        /// results retured, To check if the browser has to do request for further hotels
        /// </summary>
        int TotalGeneralAvailableHotels { get; set; }

        /// <summary>
        /// This would store true if no hotels are available for Redemption search
        /// </summary>
        bool NoRedemptionHotelsAvailable { get; set; }

        /// <summary>
        /// Gets availability table
        /// </summary>
        Hashtable GetAvailabilityTable(HttpSessionState session);

        /// <summary>
        /// The List of hotels avialble for the criteria user has searched for
        /// The Session object is updated in the Controller as the hotels
        /// are returned from the General Availabilty searches which are called
        /// through multithreading Thread pool
        /// </summary>
        List<IHotelDetails> HotelResults { get; set; }

        /// <summary>
        /// This list contains the hotel details which are required by the
        /// Google map displayed on the Select a hotel page.
        /// These details are picked up by the GoogleMap user control from 
        /// the session and the hotel details are displayed accordingly
        /// </summary>
        List<Dictionary<string, object>> GoogleMapResults { get; set; }

        /// <summary>
        /// Gets hotel results
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        List<IHotelDetails> GetHotelResults(HttpSessionState session);

        /// <summary>
        /// Sets hotel results
        /// </summary>
        /// <param name="session"></param>
        /// <param name="hotels"></param>
        void SetHotelResults(HttpSessionState session, List<IHotelDetails> hotels);

        /// <summary>
        /// Gets google map results
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        List<Dictionary<string, object>> GetGoogleMapResults(HttpSessionState session);

        /// <summary>
        /// Sets google map results
        /// </summary>
        /// <param name="session"></param>
        /// <param name="value"></param>
        void SetGoogleMapResults(HttpSessionState session, List<Dictionary<string, object>> value);

        /// <summary>
        /// GetTotalGeneralAvailableHotels
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        int GetTotalGeneralAvailableHotels(HttpSessionState session);

        /// <summary>
        /// SetTotalGeneralAvailableHotels
        /// </summary>
        /// <param name="session"></param>
        /// <param name="hotels"></param>
        void SetTotalGeneralAvailableHotels(HttpSessionState session, int hotels);

        /// <summary>
        /// The Hotel Room Rate object containing the details of the rate of
        /// the room selected by the user.
        /// The values are stored in a HotelRoomRateEntity object.
        /// The session object is updated in the code behind of the Select Rate user controls
        /// </summary>
        IList<RoomStay> ListRoomStay { get; set; }

        /// <summary>
        /// The Loyalty details object containing the user name and the NameID
        /// of the Loyalty user after the user is logged into the system.
        /// This object existence in the session tells whether the user is 
        /// logged into the system or not
        /// </summary>
        LoyaltyDetailsEntity LoyaltyDetails { get; set; }
            
        /// <summary>
        /// Gets/Sets IsComboReservation
        /// </summary>
        bool IsComboReservation { get; set; }

        /// Gets loyalty details.
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        LoyaltyDetailsEntity GetLoyaltyDetails(HttpSessionState session);

        /// <summary>
        /// The Hotel Search Criteria object containing the details
        /// the user has searched for.
        /// The values are stored in a HotelSearchEntity object.
        /// The session object is updated in the code behind of the search user controls
        /// </summary>
        HotelSearchEntity SearchCriteria { get; set; }

        /// <summary>
        /// Gets the HotelDestination List
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        List<HotelDestination> GetHotelDestinationResults();

        /// <summary>
        /// Sets the HotelDestination List
        /// </summary>
        /// <param name="session"></param>
        /// <param name="hotels"></param>
        void SetHotelDestinationResults(List<HotelDestination> hotels);

        /// <summary>
        /// Session Variable to indicate the Modify Check box in checked in booking details page;
        /// this is to implement the Update credit card only when the user have checked the Modify check box.
        /// </summary>
        /// <remarks>
        /// Do not update profile with the Credit Card information, if the user has not opted for 'Update Profile' option
        /// </remarks>
        bool UpdateProfileBookingInfo { get; set; }

        /// <summary>
        /// The Guest Booking Info object contains the details of the booking information
        /// of the guest.
        /// The values are stored in a GuestInformationEntity object.
        /// The session object is updated in the code behind of the Booking Details user controls
        /// </summary>
        GuestInformationEntity GuestBookingInformation { get; set; }

        /// <summary>
        /// Returns true if the user is logged in to the system already
        /// else returns false
        /// 
        /// The logic is to check if the LoyaltyDetailsEntity is already in session return true
        /// if the value is not already saved into the session false is returned
        /// </summary>
        bool UserLoggedIn { get; set; }

        /// <summary>
        /// The User profile Info object contains the details of the User information
        /// The values are stored in a UserProfileEntity object.
        /// The session object is updated in the code behind of the Enroll user controls
        /// </summary>
        UserProfileEntity UserProfileInformation { get; set; }

        /// <summary>
        /// Session Variable to indicate as to Enable/Disable Bed type Preference
        /// </summary>
        bool DirectlyModifyContactDetails { get; set; }

        /// <summary>
        /// Get/Set Booking details Entity used during Modify/Cancel Booking
        /// </summary>
        BookingDetailsEntity BookingDetails { get; set; }

        /// <summary>
        /// Method returns the BookingDetails for respective booking from the active booking details
        /// Author: Ruman Khan
        /// </summary>
        /// <param name="legNumber"></param>
        /// <param name="reservationNumber"></param>
        /// <returns></returns>
        BookingDetailsEntity GetLegBookingDetails(string legNumber, string reservationNumber);

        /// <summary>
        /// Added Get property to fetch the ActiveBookingDetails derived from the AllBookingDetails
        /// Author: Ruman Khan
        /// </summary>
        List<BookingDetailsEntity> ActiveBookingDetails { get;}

        //Merchandising:R3:Display ordinary rates for unavailable promo rates 
        IHotelDetails HotelDetails { get; set; }
        bool IsDestinationAlternateFlow { get; set; }
        int TotalRegionalAltCityHotelCount { get; set; }
        List<HotelDestination> CityAltHotelDetails { get; set; }
        int PromoGeneralAvailableHotels { get; set; }
        int GetPromoGeneralAvailableHotels(HttpSessionState session);
        void SetPromoGeneralAvailableHotels(HttpSessionState session, int hotels);
        int GetTotalRegionalAvailableHotels(HttpSessionState session);
    }
}
