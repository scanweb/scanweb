//  Description					: Controller class for Name OWS Calls			          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Himansu Senapati / Santhosh Yamsani / Priya Singh 	  //
//  Author email id				:                           							  //
//  Creation Date				: 06th November  2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: Raj K Marandi											  //
// 	Last Modified Date			: 15th Dec 2008											  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Membership;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Name;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using System.Linq;
using Netaxept;
using System.Collections.ObjectModel;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;

#endregion

    #region Scandic Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// Controller class for User Details Functionality
    /// </summary>
    public class NameController
    {
        #region Constructor
        /// <summary>
        /// Constructor, which accepts all requried domain instances. 
        /// </summary>
        ///<param name="loyaltyDomainObj"></param>
        ///<param name="sessionManagerObj"></param>
        public NameController(ILoyaltyDomain loyaltyDomainObj, ISessionManager sessionManagerObj)
        {
            loyaltyDomain = loyaltyDomainObj;
            sessionManager = sessionManagerObj;
        }

        /// <summary>
        /// Empty constructor 
        /// </summary>
        public NameController()
        {
            loyaltyDomain = new LoyaltyDomain();
            sessionManager = new SessionManager();
            contentDataAccessManager = new ContentDataAccessManager();
        }
        #endregion

        #region Domain Properties
        private ILoyaltyDomain loyaltyDomain = null;

        /// <summary>
        /// Gets m_LoyaltyDomain
        /// </summary>
        private ILoyaltyDomain m_LoyaltyDomain
        {
            get
            {
                return loyaltyDomain;
            }
        }
        #endregion

        #region Session Manager

        private ISessionManager sessionManager = null;
        /// <summary>
        /// Gets m_SessionManager
        /// </summary>
        private ISessionManager m_SessionManager
        {
            get
            {
                return sessionManager;
            }
        }

        private IContentDataAccessManager contentDataAccessManager = null;
        /// <summary>
        /// Gets m_ContentDataAccessManager
        /// </summary>
        private IContentDataAccessManager m_ContentDataAccessManager
        {
            get
            {
                return contentDataAccessManager;
            }
        }
        #endregion 

        #region Private Variable

        /// <summary>        
        /// Indicates the primaryLanguageID for be send to NameService header
        /// Defaulted to English "E"
        /// </summary>
        /// <remarks>
        /// </remarks>
        private string primaryLanguageID = LanguageConstant.LANGUAGE_ENGLISH_CODE;

        #endregion

        #region Property

        /// <summary>
        /// Get/Set the Primary Langauge ID
        /// </summary>
        /// <remarks></remarks>
        public string PrimaryLangaueID
        {
            get { return primaryLanguageID; }
            set { primaryLanguageID = value; }
        }

        #endregion

        #region RegisterUser

        /// <summary>
        /// Controller code for calling Name web service
        /// </summary>
        /// <param name="profile">UserProfileEntity Passed from Enroll loyalty Code Behind</param>        
        /// <returns>Registration ID as a confirmation of enrollment</returns>        
        [TraceSoapExtension(Priority = 1)]
        public string RegisterUser(UserProfileEntity profile)
        {
            try
            {
                AppLogger.LogInfoMessage("In Register User, creating LoyaltyDomain object...");

                m_LoyaltyDomain.PrimaryLanguageID = primaryLanguageID;

                string ConfirmationNumber = m_LoyaltyDomain.EnrollUser(profile);

                return ConfirmationNumber;
            }
            catch (BusinessException BusEx)
            {
                AppLogger.LogCustomException(BusEx, AppConstants.BUSINESS_EXCEPTION,
                                             " Business Exception occured in NameController : RegisterUser Method");
                throw BusEx;
            }
        }

        #endregion

        #region Fetch User Details

        /// <summary>
        /// Fetch User Details against the specified NameID
        /// </summary>
        /// <param name="nameID">
        /// Name ID of the User
        /// </param>
        /// <returns>
        /// Loyalty Member Information Entity
        /// </returns>
        [TraceSoapExtension(Priority = 1)]
        public UserProfileEntity FetchUserDetails(string nameID)
        {
            UserProfileEntity userProfileEntity = new UserProfileEntity();
            try
            {
                userProfileEntity = FetchUserProfileDetails(nameID);
            }
            catch (BusinessException BusEx)
            {
                AppLogger.LogCustomException(BusEx, AppConstants.BUSINESS_EXCEPTION,
                                             " Business Exception occured in NameController : FetchUserDetails Method");
                throw BusEx;
            }
            return userProfileEntity;
        }
        #endregion

        //Sateesh
        public UserProfileEntity FetchUserProfileDetails(string nameID)
        {
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            return loyaltyDomain.FetchUserProfile(nameID);
        }

        /// <summary>
        /// This method is used to fetch the credit card details for the loggedin user.
        /// So that the entire fetch user details will not be called only fetch Credit Ccard details Web Service will be called.
        /// Other user details will be stored in session variable.    
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns><see cref="creditCardDetails"/></returns>
        public CreditCardEntity FetchCreditCard(string nameID, bool fetchOnlyPrepaidCCTypes)
        {
            CreditCardEntity creditCardDetails = null;
            Dictionary<string, CreditCardEntity> creditCards = m_LoyaltyDomain.FetchCreditCardDetailsUserProfile(nameID, fetchOnlyPrepaidCCTypes);
            if (creditCards != null && creditCards.Count > 0)
            {
                foreach (string cardKeys in creditCards.Keys)
                {
                    if (creditCards[cardKeys].DisplaySequence == 1)
                    {
                        string name = creditCards[cardKeys].NameOnCard;
                        string cardType = creditCards[cardKeys].CardType;
                        string cardNo = creditCards[cardKeys].CardNumber;
                        ExpiryDateEntity expDate = creditCards[cardKeys].ExpiryDate;
                        creditCardDetails = new CreditCardEntity(name, cardType, cardNo, expDate);
                    }
                }
            }
            return creditCardDetails;
        }

        /// <summary>
        /// Fetches all avilable credit cards of a FGP user.
        /// </summary>
        /// <param name="nameId"></param>
        /// <returns></returns>
        public Collection<CreditCardEntity> FetchFGPCreditCards(string nameId)
        {
            Collection<CreditCardEntity> fgpCreditCards = new Collection<CreditCardEntity>();
            Dictionary<string, CreditCardEntity> creditCards = m_LoyaltyDomain.FetchCreditCardDetailsUserProfile(nameId,true);
            if (creditCards != null && creditCards.Count > 0)
            {
                foreach (string cardKeys in creditCards.Keys)
                {
                    if (creditCards[cardKeys].DisplaySequence == 1)
                    {
                        string name = creditCards[cardKeys].NameOnCard;
                        string cardType = creditCards[cardKeys].CardType;
                        string cardNo = creditCards[cardKeys].CardNumber;
                        ExpiryDateEntity expDate = creditCards[cardKeys].ExpiryDate;
                        fgpCreditCards.Add(new CreditCardEntity(name, cardType, cardNo, expDate));
                    }
                }
            }
            return fgpCreditCards;
        }

        /// <summary>
        /// Promote a membership to another level.
        /// </summary>
        /// <param name="membershipNumber">Membership number</param>
        /// <param name="promoCode">Promotion code used for updating membership number</param>
        /// <remarks></remarks>
        [TraceSoapExtension(Priority = 1)]
        public void UpdateMembershipWithPromoCode(string membershipNumber, string promoCode)
        {
            try
            {
                m_LoyaltyDomain.PrimaryLanguageID = PrimaryLangaueID;
                m_LoyaltyDomain.UpdateMembershipWithPromoCode(membershipNumber, promoCode);
            }
            catch (BusinessException BusEx)
            {
                AppLogger.LogCustomException(BusEx, AppConstants.BUSINESS_EXCEPTION,
                                             " Business Exception occured in NameController : UpdateMembershipWithPromoCode Method");
                throw BusEx;
            }
        }

        #region Update User Details

        /// <summary>
        /// Update User Details against the specified NameID
        /// </summary>
        /// <param name="nameID">Name ID of the User</param>
        /// <param name="userProfileEntity">Loyalty Member Information Entity</param>
        /// <param name="newPartnershipProgram">New partnership program selected by the user.</param>
        /// <returns>Return New List of Partner Programs</returns>
        [TraceSoapExtension(Priority = 1)]
        public void UpdateUserDetails(string nameID,
                                UserProfileEntity userProfilePrePopulated,
                                UserProfileEntity capturedUserProfileFromUI,
                                List<PartnershipProgram> newPartnershipProgram)
        {
            try
            {
                m_LoyaltyDomain.PrimaryLanguageID = primaryLanguageID;

                if (userProfilePrePopulated.AddressOperaID > 0)
                {
                    if ((!string.IsNullOrEmpty(capturedUserProfileFromUI.AddressLine1) && (userProfilePrePopulated.AddressLine1 != capturedUserProfileFromUI.AddressLine1))
                        || (!string.IsNullOrEmpty(capturedUserProfileFromUI.AddressLine2) && (userProfilePrePopulated.AddressLine2 != capturedUserProfileFromUI.AddressLine2))
                        || (!string.IsNullOrEmpty(capturedUserProfileFromUI.PostCode) && (userProfilePrePopulated.PostCode != capturedUserProfileFromUI.PostCode))
                        || (!string.IsNullOrEmpty(capturedUserProfileFromUI.City) && (userProfilePrePopulated.City != capturedUserProfileFromUI.City))
                        || (!string.IsNullOrEmpty(capturedUserProfileFromUI.Country) && (userProfilePrePopulated.Country != capturedUserProfileFromUI.Country))
                        )
                    {
                        capturedUserProfileFromUI.AddressOperaID = userProfilePrePopulated.AddressOperaID;
                        m_LoyaltyDomain.UpdateAddress(capturedUserProfileFromUI);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(capturedUserProfileFromUI.AddressLine1) &&
                        (!string.IsNullOrEmpty(capturedUserProfileFromUI.PostCode) &&
                        (!string.IsNullOrEmpty(capturedUserProfileFromUI.City))))
                    {
                        m_LoyaltyDomain.InsertAddress(nameID,
                                                    capturedUserProfileFromUI);
                    }
                }
                if (userProfilePrePopulated.PreferredLanguage != capturedUserProfileFromUI.PreferredLanguage
                    || userProfilePrePopulated.NameTitle != capturedUserProfileFromUI.NameTitle) 
                {
                    FetchNameResponse response = m_LoyaltyDomain.FetchNameService(nameID);
                    m_LoyaltyDomain.UpdatePreferredLanguage(nameID, capturedUserProfileFromUI, response);
                }
                if (userProfilePrePopulated.MobilePhoneOperaID > 0)
                {
                    if (!string.IsNullOrEmpty(capturedUserProfileFromUI.MobilePhone) && (userProfilePrePopulated.MobilePhone != capturedUserProfileFromUI.MobilePhone))
                        m_LoyaltyDomain.UpdatePhone(nameID,
                                                    userProfilePrePopulated.MobilePhoneOperaID,
                                                    capturedUserProfileFromUI.MobilePhone,
                                                    PhoneContants.PHONETYPE_MOBILE,
                                                    PhoneContants.PHONEROLE);
                }
                else
                {
                    if (!string.IsNullOrEmpty(capturedUserProfileFromUI.MobilePhone))

                        m_LoyaltyDomain.InsertPhone(nameID,
                                                    capturedUserProfileFromUI.MobilePhone,
                                                    PhoneContants.PHONETYPE_MOBILE, PhoneContants.PHONEROLE);
                }

                if (userProfilePrePopulated.HomePhoneOperaID > 0)
                {
                    if (!string.IsNullOrEmpty(capturedUserProfileFromUI.HomePhone) && (userProfilePrePopulated.HomePhone != capturedUserProfileFromUI.HomePhone))
                        m_LoyaltyDomain.UpdatePhone(nameID,
                                                    userProfilePrePopulated.HomePhoneOperaID,
                                                    capturedUserProfileFromUI.HomePhone,
                                                    PhoneContants.PHONETYPE_HOME,
                                                    PhoneContants.PHONEROLE);
                }
                else
                {
                    if (!string.IsNullOrEmpty(capturedUserProfileFromUI.HomePhone))
                        m_LoyaltyDomain.InsertPhone(nameID,
                                                    capturedUserProfileFromUI.HomePhone,
                                                    PhoneContants.PHONETYPE_HOME,
                                                    PhoneContants.PHONEROLE);
                }

                if ((userProfilePrePopulated.EmailOperaId > 0) && (userProfilePrePopulated.NonBusinessEmailOperaId > 0))
                {
                    if (!string.IsNullOrEmpty(capturedUserProfileFromUI.EmailID) && (userProfilePrePopulated.EmailID != capturedUserProfileFromUI.EmailID))
                        m_LoyaltyDomain.UpdateEmail(nameID,
                                                    userProfilePrePopulated.EmailOperaId,
                                                    capturedUserProfileFromUI.EmailID, true);

                    if (!string.IsNullOrEmpty(capturedUserProfileFromUI.NonBusinessEmailID) && (userProfilePrePopulated.NonBusinessEmailID != capturedUserProfileFromUI.NonBusinessEmailID))
                        m_LoyaltyDomain.UpdateEmail(nameID,
                                                    userProfilePrePopulated.NonBusinessEmailOperaId,
                                                    capturedUserProfileFromUI.NonBusinessEmailID, false);
                }
                else
                {
                    if (!string.IsNullOrEmpty(capturedUserProfileFromUI.EmailID))
                        m_LoyaltyDomain.InsertEmail(capturedUserProfileFromUI.EmailID,
                                                    nameID);
                }

                if (capturedUserProfileFromUI.ProfileType == UserProfileType.UPDATE_PROFILE)
                {
                    string memberName = string.Empty;
                    memberName = userProfilePrePopulated.FirstName + AppConstants.SPACE + userProfilePrePopulated.LastName;
                    if (!String.IsNullOrEmpty(memberName.Trim()))
                    {
                        UpdatePartnershipProgram(nameID,
                                                    memberName,
                                                    newPartnershipProgram, userProfilePrePopulated.PartnerProgramList);
                    }

                    m_LoyaltyDomain.UpdatePreferencesListUserProfile(nameID,
                                                                    capturedUserProfileFromUI.UserPreference, userProfilePrePopulated.UserPreference);
                    m_LoyaltyDomain.UpdateInterestsListUserProfile(nameID,
                                                capturedUserProfileFromUI.Interests, userProfilePrePopulated.Interests);
                }

                if (capturedUserProfileFromUI.Question != null)
                {
                    m_LoyaltyDomain.UpdateQuestionNAnswer(nameID,
                                                        capturedUserProfileFromUI.Question,
                                                        capturedUserProfileFromUI.Answer);
                }
                if (capturedUserProfileFromUI.Password != null && capturedUserProfileFromUI.RetypePassword != null)
                {
                    m_LoyaltyDomain.UpdatePassword(userProfilePrePopulated.MembershipId,
                                                    capturedUserProfileFromUI.Password,
                                                    capturedUserProfileFromUI.RetypePassword);
                }

                if ((userProfilePrePopulated.ScandicEmail != capturedUserProfileFromUI.ScandicEmail) ||
                    (userProfilePrePopulated.ThridPartyEmail != capturedUserProfileFromUI.ThridPartyEmail))

                    m_LoyaltyDomain.InsertUpdatePrivacyUserDetail(nameID,
                                                                    capturedUserProfileFromUI.ScandicEmail,
                                                                    capturedUserProfileFromUI.ThridPartyEmail,capturedUserProfileFromUI.ScandicEMailPrev);

                if (capturedUserProfileFromUI.ProfileType.Equals(UserProfileType.UPDATE_PROFILE) && capturedUserProfileFromUI.CreditCard != null)
                {
                    capturedUserProfileFromUI.CreditCardOperaId = userProfilePrePopulated.CreditCardOperaId;

                    UpdateCreditCardInfo(nameID,
                                            capturedUserProfileFromUI);
                }
                else if ((capturedUserProfileFromUI.ProfileType.Equals(UserProfileType.BOOKING_DETAILS)) && (m_SessionManager.UpdateProfileBookingInfo))
                {
                    if ((null != m_SessionManager.GuestBookingInformation) &&
                        (null != m_SessionManager.GuestBookingInformation.GuranteeInformation)
                        && (null != m_SessionManager.GuestBookingInformation.GuranteeInformation.CreditCard))
                    {
                        if (m_SessionManager.SearchCriteria != null && m_SessionManager.SearchCriteria.SearchingType != SearchType.REDEMPTION)
                        {
                            CreditCardEntity primaryCard = m_SessionManager.GuestBookingInformation.GuranteeInformation.CreditCard;
                            CheckExistingCreditCard(nameID, primaryCard);
                        }
                    }
                }
                LoyaltyDetailsEntity loyaltyDetails = m_SessionManager.LoyaltyDetails;
                if (loyaltyDetails != null)
                {
                    bool force = true;
                    this.PopulateSession(nameID, loyaltyDetails.UserName, force);
                }
            }
            catch (BusinessException BusEx)
            {
                AppLogger.LogCustomException(BusEx, AppConstants.BUSINESS_EXCEPTION,
                                             " Business Exception occured in NameController : UpdateUserDetails Method");
            }
        }

        #endregion

        #region GetAccountInfo

        /// <summary>
        /// All transactions of a loyalty member in the 
        /// form of "LoyaltyAccountInformation" will be returned.
        /// </summary>
        /// <param name="membershipOperaID"></param>
        /// <returns><see cref="accountInfo"/></returns>
        public LoyaltyAccountInformation GetAccountInfo(string membershipOperaID)
        {
            FetchMembershipTransactionsResponse response = m_LoyaltyDomain.FetchMembershipTransactions(membershipOperaID);

            LoyaltyAccountInformation accountInfo = new LoyaltyAccountInformation(response);
            return accountInfo;
        }

        #endregion GetAccountInfo

        #region ValidateUserCredentials

        /// <summary>
        /// This Method will validate user by taking membership id ,security qestion and  security answer.
        /// If question and answer are valid then it will send password else returns empty string.
        /// </summary>
        /// <param name="membershipId"></param>
        /// <param name="questionId"></param>
        /// <param name="answer"></param>
        /// <returns></returns>
        public string ValidateUserCredentials(string membershipId, string questionId, string questionString,
                                              string answer)
        {
            string password = string.Empty;

            m_LoyaltyDomain.PrimaryLanguageID = primaryLanguageID;

            password = m_LoyaltyDomain.FetchPassword(membershipId.Trim(), questionId, questionString, answer);
            if (!String.IsNullOrEmpty(password))
            {
                return password;
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion ValidateUserCredentials

        #region FetchEmaiId

        /// <summary>
        /// This will fetch the Email id of a user.
        /// </summary>
        /// <param name="nameId"></param>
        /// <returns></returns>
        public string FetchEmailId(string nameId)
        {
            string emailId = string.Empty;

            m_LoyaltyDomain.PrimaryLanguageID = primaryLanguageID;

            EmailDetailsEntity emailDetailsEntity = m_LoyaltyDomain.FetchEmail(nameId);
            if (emailDetailsEntity != null)
            {
                emailId = emailDetailsEntity.EmailID;
            }
            return emailId;
        }

        #endregion FetchEmaiId

        #region FetchName

        /// <summary>
        /// This will give the user name.Used in Forgotten Password functionality.
        /// </summary>
        /// <param name="nameId">
        /// name id used to fetch the name of the user
        /// </param>
        /// <returns>
        /// Returns the First Name
        /// </returns>
        public string FetchName(string nameId)
        {
            m_LoyaltyDomain.PrimaryLanguageID = primaryLanguageID;

            UserProfileEntity fetchUserEntity = m_LoyaltyDomain.FetchNameUserProfile(nameId);

            //Added as part of SCANAM-537
            if (!string.IsNullOrEmpty(fetchUserEntity.NativeFirstName))
            {
                return fetchUserEntity.NativeFirstName;
            }
            else if (!string.IsNullOrEmpty(fetchUserEntity.FirstName))
            {
                return fetchUserEntity.FirstName;
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion FetchName

        #region GetEmailAddress

        /// <summary>
        /// This method is responsible for assigning recipient and sender address. 
        /// </summary>
        /// <param name="emailEntity"></param>
        /// <param name="collatedScandicAddressTo"></param>
        /// <param name="collatedScandicAddressCC"></param>
        /// <param name="fromAddress"></param>
        /// <returns></returns>
        public EmailEntity GetEmailAddress(EmailEntity emailEntity, string collatedScandicAddressTo,
                                           string collatedScandicAddressCC, string fromAddress)
        {
            string[] emailListRecipient = collatedScandicAddressTo.Split(new Char[] {';'});
            if (emailListRecipient.Length > 0)
            {
                emailEntity.Recipient = emailListRecipient;
            }
            string[] emailListRecipientCC = collatedScandicAddressCC.Split(new Char[] {';'});
            if (emailListRecipientCC.Length > 0)
            {
                emailEntity.CarbonCopy = emailListRecipientCC;
            }
            if (fromAddress == null)
            {
                string defaultFromAddress = ConfigurationSettings.AppSettings["DefaultFromAddress"];
                emailEntity.Sender = defaultFromAddress;
            }
            else
            {
                emailEntity.Sender = fromAddress;
            }
            return emailEntity;
        }

        #endregion GetEmailAddress

        #region Login

        /// <summary>
        /// This method is responsible for Login in a user. 
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <param name="password">Password.</param>
        public void Login(string userName, string password)
        {
            string MembershipID = string.Empty;
            userName = userName.Trim();
            MembershipID = AuthenticateUser(userName, password);

            if (MembershipID != string.Empty)
            {
                PopulateSession(MembershipID, userName);
            }
        }

        #endregion Login

        #region AuthenticateUser

        /// <summary>
        /// This method is responsible for authenticating a user.
        /// </summary>
        /// <param name="loyaltyNumber">Membership Number/Loyalty Number</param>
        /// <param name="pin">Password.</param>
        /// <returns></returns>
        private string AuthenticateUser(string loyaltyNumber, string pin)
        {
            String nameId = string.Empty;
            AvailabilityController availController = new AvailabilityController();
            nameId = availController.AuthenticateUser(loyaltyNumber, pin);
            return nameId;
        }

        #endregion AuthenticateUser

        #region ReturnPoints

        /// <summary>
        /// This will return the no. of points the user have currently.
        /// </summary>
        /// <param name="nameId">Name id of the user</param>
        /// <returns>No. of points user have.</returns>
        public double ReturnPoints(string nameId)
        {
            double availablePoints = 0;
            try
            {
                AvailabilityController availabilityController = new AvailabilityController();
                ArrayList membershipList =
                    availabilityController.FetchGuestCardList(nameId,
                                                              AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE);
                if ((membershipList != null) && (membershipList.Count > 0))
                {
                    MembershipEntity membershipEntity = (MembershipEntity) membershipList[0];
                    availablePoints = membershipEntity.CurrentPoints;
                }
                return availablePoints;
            }
            catch (Exception Ex)
            {
                AppLogger.LogFatalException(Ex);
                return 0;
            }
        }

        #endregion ReturnPoints

        #region PopulateSession

        /// <summary>
        /// This method is used for populating session after successful login to the system.
        /// Method called from BookingModuleBig and LoginStatus functionality. 
        /// </summary>
        /// <param name="nameId">Name id(membership ID) returned after validating user name and password</param>
        /// <param name="loyaltyNumber">User name of the user</param>
        [TraceSoapExtension(Priority = 1)]
        public void PopulateSession(string nameId, string loyaltyNumber)
        {
            if (!m_SessionManager.UserLoggedIn)
            {
                NameController nameController = new NameController();
                UserProfileEntity fetchUserEntity = nameController.FetchUserDetails(nameId);

                LoyaltyDetailsEntity loyaltyEntity = new LoyaltyDetailsEntity();
                loyaltyEntity.AddressLine1 = fetchUserEntity.AddressLine1;
                loyaltyEntity.AddressLine2 = fetchUserEntity.AddressLine2;
                loyaltyEntity.City = fetchUserEntity.City;
                loyaltyEntity.Country = fetchUserEntity.Country;
                loyaltyEntity.PostCode = fetchUserEntity.PostCode;
                loyaltyEntity.Title = fetchUserEntity.NameTitle;
                loyaltyEntity.FirstName = fetchUserEntity.FirstName;
                loyaltyEntity.SurName = fetchUserEntity.LastName;

                //Added as part of SCANAM-537
                loyaltyEntity.NativeFirstName = fetchUserEntity.NativeFirstName;
                loyaltyEntity.NativeLastName = fetchUserEntity.NativeLastName;

                loyaltyEntity.Telephone1 = fetchUserEntity.MobilePhone;
                loyaltyEntity.Email = fetchUserEntity.EmailID;
                loyaltyEntity.Fax = fetchUserEntity.Fax;
                loyaltyEntity.PreferredLanguage = fetchUserEntity.PreferredLanguage;

                loyaltyEntity.NameID = nameId;
                loyaltyEntity.UserName = loyaltyNumber.Trim();
                //loyaltyEntity.CurrentPoints = ReturnPoints(nameId);
                loyaltyEntity.CurrentPoints = fetchUserEntity.GuestCardList.FirstOrDefault().CurrentPoints;

                //AvailabilityController availabilityController = new AvailabilityController();
                //string membershipLevel = null;
                //string membershipID = null;
                //ArrayList membershipList = availabilityController.FetchGuestCardList(nameId, AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE);
                MembershipEntity membershipList = fetchUserEntity.GuestCardList.FirstOrDefault();
                //if (null != membershipList && membershipList.Count > 0)
                if (membershipList != null)
                {
                    //MembershipEntity membership = membershipList[0] as MembershipEntity;
                    loyaltyEntity.MembershipLevel = membershipList.MembershipLevel;
                    loyaltyEntity.MembershipID = membershipList.OperaId;
                }
                //loyaltyEntity.MembershipLevel = membershipLevel;
                //loyaltyEntity.MembershipID = membershipID;
                
                m_SessionManager.LoyaltyDetails = loyaltyEntity;
                m_SessionManager.UserLoggedIn = true;
                fetchUserEntity.CreditCard = null;
                m_SessionManager.UserProfileInformation = fetchUserEntity;
            }
        }

        /// <summary>
        /// Populates the session.
        /// </summary>
        /// <param name="nameId">The name id.</param>
        /// <param name="loyaltyNumber">The loyalty number.</param>
        /// <param name="force">if set to <c>true</c> [force].</param>
        public void PopulateSession(string nameId, string loyaltyNumber, bool force)
        {
            if (!m_SessionManager.UserLoggedIn || force)
            {
                NameController nameController = new NameController();
                UserProfileEntity fetchUserEntity = nameController.FetchUserDetails(nameId);

                LoyaltyDetailsEntity loyaltyEntity = new LoyaltyDetailsEntity();
                loyaltyEntity.AddressLine1 = fetchUserEntity.AddressLine1;
                loyaltyEntity.AddressLine2 = fetchUserEntity.AddressLine2;
                loyaltyEntity.City = fetchUserEntity.City;
                loyaltyEntity.Country = fetchUserEntity.Country;
                loyaltyEntity.PostCode = fetchUserEntity.PostCode;
                loyaltyEntity.Title = fetchUserEntity.NameTitle;
                loyaltyEntity.FirstName = fetchUserEntity.FirstName;
                loyaltyEntity.SurName = fetchUserEntity.LastName;

                //Added as part of SCANAM-537
                loyaltyEntity.NativeFirstName = fetchUserEntity.NativeFirstName;
                loyaltyEntity.NativeLastName = fetchUserEntity.NativeLastName;

                loyaltyEntity.Telephone1 = fetchUserEntity.MobilePhone;
                loyaltyEntity.Email = fetchUserEntity.EmailID;
                loyaltyEntity.Fax = fetchUserEntity.Fax;
                loyaltyEntity.PreferredLanguage = fetchUserEntity.PreferredLanguage;

                loyaltyEntity.NameID = nameId;
                loyaltyEntity.UserName = loyaltyNumber.Trim();
                //loyaltyEntity.CurrentPoints = ReturnPoints(nameId);
                loyaltyEntity.CurrentPoints = fetchUserEntity.GuestCardList.FirstOrDefault().CurrentPoints;

                //AvailabilityController availabilityController = new AvailabilityController();
                //string membershipLevel = null;
                //string membershipID = null;
                //ArrayList membershipList = availabilityController.FetchGuestCardList(nameId, AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE);
                MembershipEntity membershipList = fetchUserEntity.GuestCardList.FirstOrDefault();
                //if (null != membershipList && membershipList.Count > 0)
                if(membershipList != null)
                {
                    //MembershipEntity membership = membershipList[0] as MembershipEntity;
                    loyaltyEntity.MembershipLevel = membershipList.MembershipLevel;
                    loyaltyEntity.MembershipID = membershipList.OperaId;
                }
                //loyaltyEntity.MembershipLevel = membershipLevel;
                //loyaltyEntity.MembershipID = membershipID;

                m_SessionManager.LoyaltyDetails = loyaltyEntity;
                m_SessionManager.UserLoggedIn = true;
                fetchUserEntity.CreditCard = null;
                m_SessionManager.UserProfileInformation = fetchUserEntity;
            }
        }

        #endregion PopulateSession

        #region FetchGuestCardList for Expiry Point

        /// <summary>
        /// Function use to get the Expiry Point with respect to the Membership Number
        /// </summary>
        /// <param name="nameId">OPera Mapping of the MembershipNumber Unique to a MemebershipNumber</param>
        /// <param name="membershipType">Type Of membership It is,ex. GUESTPR</param>
        /// <returns>an arraylist of the MembershipEntity entity that further contain the collection of the 
        /// ExpiryPointEntity entity which contain expiry point and respective expiry date information</returns>
        public ArrayList FetchGuestCardListExpPoint(string nameId, string membershipType)
        {
            return m_LoyaltyDomain.FetchGuestCardList(nameId, membershipType);
        }

        #endregion FetchGuestCardList for Expiry Point

        #region Logout

        /// <summary>
        /// This method will clear up the session and make the user log off. 
        /// </summary>
        public void Logout()
        {
            m_SessionManager.LoyaltyDetails = null;
            m_SessionManager.UserLoggedIn = false;

            if (HttpContext.Current.Response.Cookies["IsLoggedInUser"] != null)
            {
                HttpContext.Current.Response.Cookies["IsLoggedInUser"].Value = bool.FalseString;
            }
            else
            {
                HttpCookie httpCookie = new HttpCookie("IsLoggedInUser", bool.TrueString);
                httpCookie.Expires = DateTime.MaxValue;
                HttpContext.Current.Response.Cookies.Add(httpCookie);
            }

            if (HttpContext.Current.Response.Cookies["ActiveTab"] != null)
            {
                HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab1;
            }
        }

        #endregion Logout

        /// <summary>
        /// Reorder the partnership programs
        /// </summary>
        /// <param name="nameId">Name id of this profile</param>
        /// <param name="newPartnrshipProgram">New partnership program select by the user.</param>
        /// <param name="newlyAddedPartnerProgramList">List of newly added partnership program</param>
        /// <param name="modifiedPartnerProgramList">List of modified partnership program</param>
        /// <param name="deletedPartnerProgramList">Lis of deleted partnership program</param>
        /// <remarks></remarks>
        private void ReorderPartnershipProgram(string nameId, List<PartnershipProgram> newPartnershipProgramList,
                                               out List<PartnershipProgram> newlyAddedPartnerProgramList,
                                               out List<PartnershipProgram> modifiedPartnerProgramList,
                                               out List<PartnershipProgram> deletedPartnerProgramList, 
                                               List<PartnershipProgram> oldPartnershipProgramList)
        {
            //List<PartnershipProgram> oldPartnershipProgramList = GetPartnershipPrograms(nameId);
            newlyAddedPartnerProgramList = GetNewlyAddedPartnerProgram(oldPartnershipProgramList,
                                                                       newPartnershipProgramList);

            GetModifiedAndDeletedPartnerProgram(out modifiedPartnerProgramList, out deletedPartnerProgramList,
                                                oldPartnershipProgramList, newPartnershipProgramList);
        }

        /// <summary>
        /// Get the modified and deleted partnership program.
        /// </summary>
        /// <param name="modifiedPartnerProgramList">List of modified partnership program</param>
        /// <param name="deletedPartnerProgramList">List of deleted partnership program</param>
        /// <param name="oldPartnershipProgramList">List of existing partnership program</param>
        /// <param name="newPartnershipProgramList">List of newly added partnership program by user</param>
        /// <remarks></remarks>
        private void GetModifiedAndDeletedPartnerProgram(out List<PartnershipProgram> modifiedPartnerProgramList,
                                                         out List<PartnershipProgram> deletedPartnerProgramList,
                                                         List<PartnershipProgram> oldPartnershipProgramList,
                                                         List<PartnershipProgram> newPartnershipProgramList)
        {
            int oldPartnershipProgLength = oldPartnershipProgramList.Count;
            int newPartnershipProgLength = newPartnershipProgramList.Count;

            modifiedPartnerProgramList = new List<PartnershipProgram>();
            deletedPartnerProgramList = new List<PartnershipProgram>();

            for (int oldPartnerProgCount = 0; oldPartnerProgCount < oldPartnershipProgLength; oldPartnerProgCount++)
            {
                bool isModifiedPartnerProg = false;
                bool isDeletedPartnerProg = false;
                int newSequenceNo = 0;
                string newAccountNo = string.Empty;
                for (int newPartnerProgCount = 0; newPartnerProgCount < newPartnershipProgLength; newPartnerProgCount++)
                {
                    if (oldPartnershipProgramList[oldPartnerProgCount].PartnerProgram
                        == newPartnershipProgramList[newPartnerProgCount].PartnerProgram)
                    {
                        isDeletedPartnerProg = false;

                        if (oldPartnershipProgramList[oldPartnerProgCount].PartnerAccountNo
                            == newPartnershipProgramList[newPartnerProgCount].PartnerAccountNo)
                        {
                            if (oldPartnershipProgramList[oldPartnerProgCount].DisplaySequence
                                == newPartnershipProgramList[newPartnerProgCount].DisplaySequence)
                            {
                                break;
                            }
                            else
                            {
                                isModifiedPartnerProg = true;
                                newSequenceNo = newPartnershipProgramList[newPartnerProgCount].DisplaySequence;
                                newAccountNo = newPartnershipProgramList[newPartnerProgCount].PartnerAccountNo;
                                break;
                            }
                        }
                        else
                        {
                            isModifiedPartnerProg = true;
                            newAccountNo = newPartnershipProgramList[newPartnerProgCount].PartnerAccountNo;
                            newSequenceNo = newPartnershipProgramList[newPartnerProgCount].DisplaySequence;
                            break;
                        }
                    }
                    else
                    {
                        isDeletedPartnerProg = true;
                    }
                }
                if (isModifiedPartnerProg)
                {
                    oldPartnershipProgramList[oldPartnerProgCount].Operation = OperationType.UPDATE;
                    oldPartnershipProgramList[oldPartnerProgCount].DisplaySequence = newSequenceNo;
                    oldPartnershipProgramList[oldPartnerProgCount].PartnerAccountNo = newAccountNo;
                    modifiedPartnerProgramList.Add(oldPartnershipProgramList[oldPartnerProgCount]);
                }
                else if (isDeletedPartnerProg || int.Equals(newPartnershipProgLength, 0))
                {
                    oldPartnershipProgramList[oldPartnerProgCount].Operation = OperationType.UPDATE;
                    deletedPartnerProgramList.Add(oldPartnershipProgramList[oldPartnerProgCount]);
                }
            }
        }

        /// <summary>
        /// Get the newly added partnership programs.
        /// </summary>
        /// <param name="oldPartnershipProgramList">List of existing partnership program</param>
        /// <param name="newPartnershipProgramList">List of newly added partnership program by user</param>
        /// <returns>Newly added partnership program</returns>
        /// <remarks></remarks>
        private List<PartnershipProgram> GetNewlyAddedPartnerProgram(List<PartnershipProgram> oldPartnershipProgramList,
                                                                     List<PartnershipProgram> newPartnershipProgramList)
        {
            List<PartnershipProgram> newlyAddedPartnerProgrtamList = new List<PartnershipProgram>();
            int oldPartnershipProgLength = oldPartnershipProgramList.Count;
            int newPartnershipProgLength = newPartnershipProgramList.Count;

            for (int newCount = 0; newCount < newPartnershipProgLength; newCount++)
            {
                bool isNewPartnerProgram = true;
                for (int oldCount = 0; oldCount < oldPartnershipProgLength; oldCount++)
                {
                    if (string.Equals(newPartnershipProgramList[newCount].PartnerProgram,
                        oldPartnershipProgramList[oldCount].PartnerProgram) && 
                        string.Equals(newPartnershipProgramList[newCount].PartnerAccountNo, 
                        oldPartnershipProgramList[oldCount].PartnerAccountNo,StringComparison.InvariantCultureIgnoreCase))
                    {
                        isNewPartnerProgram = false;
                        break;
                    }
                }
                if (isNewPartnerProgram)
                {
                    newPartnershipProgramList[newCount].Operation = OperationType.INSERT;
                    newlyAddedPartnerProgrtamList.Add(newPartnershipProgramList[newCount]);
                }
            }
            return newlyAddedPartnerProgrtamList;
        }

        #region UpdatePartnershipProgram

        /// <summary>
        /// This will update the partnership program in the Opera.
        /// </summary>
        /// <param name="nameID">Name id against which partner program will be updated.</param>
        /// <param name="memberName">Member name of this profile</param>
        /// <param name="newPartnrshipProgram">New partnership program selected by the user.</param>
        /// <remarks></remarks>
        private void UpdatePartnershipProgram(string nameID, string memberName,
                                              List<PartnershipProgram> newPartnrshipProgram, List<PartnershipProgram> oldPartnrshipProgram)
        {
            List<PartnershipProgram> modifiedPartnershipProgram;
            List<PartnershipProgram> deletedPartnershipProgram;
            List<PartnershipProgram> newlyAddedPartnershipProgram;

            ReorderPartnershipProgram(nameID, newPartnrshipProgram, out newlyAddedPartnershipProgram,
                                      out modifiedPartnershipProgram, out deletedPartnershipProgram, oldPartnrshipProgram);

            m_LoyaltyDomain.PrimaryLanguageID = primaryLanguageID;

            int deletedPartnershipProgramLength = deletedPartnershipProgram.Count;
            for (int deleteCount = 0; deleteCount < deletedPartnershipProgramLength; deleteCount++)
            {
                m_LoyaltyDomain.DeleteGuestCard(deletedPartnershipProgram[deleteCount].OperaId.ToString());
            }

            int modifiedPartnershipProgramLength = modifiedPartnershipProgram.Count;
            for (int modifyCount = 0; modifyCount < modifiedPartnershipProgramLength; modifyCount++)
            {
                m_LoyaltyDomain.UpdateGuestCardDetails(nameID, modifiedPartnershipProgram[modifyCount].OperaId,
                                                     modifiedPartnershipProgram[modifyCount].PartnerProgram,
                                                     modifiedPartnershipProgram[modifyCount].PartnerAccountNo,
                                                     memberName,
                                                     modifiedPartnershipProgram[modifyCount].DisplaySequence);
            }

            int newlyAddedPartnershipProgramLength = newlyAddedPartnershipProgram.Count;
            string defaultKey = AppConstants.DEFAULT_VALUE_CONST;
            for (int newCount = 0; newCount < newlyAddedPartnershipProgramLength; newCount++)
            {
                if (defaultKey != newlyAddedPartnershipProgram[newCount].PartnerProgram)
                {
                    m_LoyaltyDomain.InsertUserGuestCard(nameID,
                                                      newlyAddedPartnershipProgram[newCount].PartnerProgram,
                                                      newlyAddedPartnershipProgram[newCount].PartnerAccountNo, null,
                                                      memberName,
                                                      newlyAddedPartnershipProgram[newCount].DisplaySequence);
                }
            }
        }

        #endregion UpdatePartnershipProgram

        /// <summary>
        /// Get the Partnership Programs
        /// </summary>
        /// <param name="nameId">
        /// Name Id of the Profile</param>
        /// <returns>
        /// List of Partners Program
        /// </returns>
        private List<PartnershipProgram> GetPartnershipPrograms(string nameId)
        {
            m_LoyaltyDomain.PrimaryLanguageID = primaryLanguageID;

            List<PartnershipProgram> partnershipProgramList = new List<PartnershipProgram>();
            PartnershipProgram partnershipProgram;

            FetchGuestCardListResponse response = m_LoyaltyDomain.FetchPartnerProgramNameService(nameId);
            ServiceProxies.Name.NameMembership[] guestcardlist = response.GuestCardList;

            if (guestcardlist != null && guestcardlist.Length > 0)
            {
                for (int count = 0; count < guestcardlist.Length; count++)
                {
                     
                    if (guestcardlist[count].membershipLevel != AppConstants.MEMBERSHIPLEVEL_1STFLOOR && guestcardlist[count].membershipLevel != AppConstants.MEMBERSHIPLEVEL_2NDFLOOR &&
                        guestcardlist[count].membershipLevel != AppConstants.MEMBERSHIPLEVEL_3RDFLOOR && guestcardlist[count].membershipLevel != AppConstants.MEMBERSHIPLEVEL_TOPFLOOR &&
                        guestcardlist[count].membershipLevel != AppConstants.MEMBERSHIPLEVEL_BYINVIT)
                    {
                        partnershipProgram = new PartnershipProgram();
                        partnershipProgram.PartnerProgram = guestcardlist[count].membershipType;
                        partnershipProgram.PartnerAccountNo = guestcardlist[count].membershipNumber;
                        partnershipProgram.DisplaySequence = guestcardlist[count].displaySequence;
                        partnershipProgram.OperaId = guestcardlist[count].operaId;
                        partnershipProgram.Operation = OperationType.SELECT;

                        partnershipProgramList.Add(partnershipProgram);
                    }
                }
            }

            partnershipProgramList.Sort(new PartnershipProgramComparer());
            return partnershipProgramList;
        }

        #region Credit Card

        ///<summary>
        ///If the opera ID exists and the credit card information is also send.
        ///It means that the user want to change the credit card information. Check if the 
        ///user's new credit card information matches with any of the existing credit card of the user. 
        ///If true, set the displaysequence and primary flag of the existing to 1 and true 
        ///respectively. Else, create a new credit card
        /// </summary>
        /// <param name="nameID">Name ID</param>
        /// <param name="newCreditCard">New Credit Card</param>
        private void CheckExistingCreditCard(string nameID, CreditCardEntity newCreditCard)
        {
            m_LoyaltyDomain.PrimaryLanguageID = primaryLanguageID;

            CreditCardEntity existingCreditCard = null;
            long existingCreditCardOperaId = -1;

            Dictionary<string, CreditCardEntity> creditCards = m_LoyaltyDomain.FetchCreditCardDetailsUserProfile(nameID);

            if (creditCards != null && creditCards.Count > 0)
            {
                foreach (string cardKey in creditCards.Keys)
                {
                    string name = creditCards[cardKey].NameOnCard;
                    string cardType = creditCards[cardKey].CardType;
                    string cardNo = creditCards[cardKey].CardNumber;
                    int displaySequence = creditCards[cardKey].DisplaySequence;
                    ExpiryDateEntity expDate = creditCards[cardKey].ExpiryDate;

                    if ((newCreditCard.CardType == cardType) &&
                        (newCreditCard.CardNumber == cardNo))
                    {
                        ChangeCreditCardDisplaySequence(nameID, newCreditCard);

                        int newDisplaySequence = 1;

                        existingCreditCard = new CreditCardEntity(newCreditCard.NameOnCard, cardType, cardNo,
                                                                  newCreditCard.ExpiryDate, newDisplaySequence);
                        existingCreditCardOperaId = long.Parse(cardKey);

                        bool isPrimary = true;
                        m_LoyaltyDomain.UpdateCreditCardDetails(existingCreditCard, existingCreditCardOperaId, isPrimary);
                        break;
                    }
                }
            }

            if (existingCreditCard == null)
            {
                ChangeCreditCardDisplaySequence(nameID, newCreditCard);

                string name = newCreditCard.NameOnCard;
                string cardType = newCreditCard.CardType;
                string cardNumber = newCreditCard.CardNumber;
                int displaySequence = 1;
                ExpiryDateEntity expDate = newCreditCard.ExpiryDate;

                CreditCardEntity creditCard = new CreditCardEntity(name, cardType, cardNumber, expDate, displaySequence);
                bool insertCreditCards = loyaltyDomain.InsertCreditCard(nameID, creditCard);
            }
        }

        /// <summary>
        /// Change the display sequence of the current primary credit card to next sequence no.
        /// </summary>
        /// <param name="nameID">Name ID</param>
        private void ChangeCreditCardDisplaySequence(string nameID,CreditCardEntity newCreditCard)
        {
            m_LoyaltyDomain.PrimaryLanguageID = primaryLanguageID;

            Dictionary<string, CreditCardEntity> creditCards = m_LoyaltyDomain.FetchCreditCardDetailsUserProfile(nameID);

            if (creditCards != null && creditCards.Count > 0)
            {
                foreach (string cardKeys in creditCards.Keys)
                {
                    string name = creditCards[cardKeys].NameOnCard;
                    string cardType = creditCards[cardKeys].CardType;
                    string cardNo = creditCards[cardKeys].CardNumber;
                    if ((newCreditCard!=null) && (newCreditCard.CardType == cardType) &&
                        (newCreditCard.CardNumber == cardNo))
                    {
                        continue;
                    }
                    ExpiryDateEntity expDate = creditCards[cardKeys].ExpiryDate;
                    int displaySequence = creditCards[cardKeys].DisplaySequence;
                    displaySequence += 1;

                    CreditCardEntity creditCardDetails = new CreditCardEntity(name, cardType, cardNo, expDate,
                                                                              displaySequence);
                    long creditCardOperaId = long.Parse(cardKeys);
                    bool isPrimary = false;
                    m_LoyaltyDomain.UpdateCreditCardDetails(creditCardDetails, creditCardOperaId, isPrimary);
                }
            }
        }

        /// <summary>
        /// This method updates the credit card info.
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="userProfileEntity"></param>
        private void UpdateCreditCardInfo(string nameID, UserProfileEntity userProfileEntity)
        {
            m_LoyaltyDomain.PrimaryLanguageID = primaryLanguageID;

            if ((userProfileEntity.CreditCardOperaId > 0) && (userProfileEntity.CreditCard == null))
            {
                ChangeCreditCardDisplaySequence(nameID,null);
            }

            if (userProfileEntity.CreditCard != null)
            {
                CheckExistingCreditCard(nameID, userProfileEntity.CreditCard);
            }
        }

        /// <summary>
        /// Checks whether the credit card information already available or not
        /// </summary>
        /// <returns></returns>
        private bool DoesCreditCardExistAlready()
        {
            var loyaltyDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
            var loyaltyDomain = new LoyaltyDomain();
            var creditCards = loyaltyDomain.FetchCreditCardDetailsUserProfile(loyaltyDetails.NameID);
            var search = SearchCriteriaSessionWrapper.SearchCriteria;
           
            if (search != null)
            {
                var hotelMerchantInfo = m_ContentDataAccessManager.GetMerchantByHotelOperaId(search.SelectedHotelCode);
                if (hotelMerchantInfo != null)
                {
                    PaymentHelper paymentHelper = new PaymentHelper();
                    var paymentInfo = RoomRateUtil.ConvertToPaymentInfo(
                        paymentHelper.Query(Reservation2SessionWrapper.PaymentTransactionId, hotelMerchantInfo.MerchantID, hotelMerchantInfo.MerchantToken));
                    if (paymentInfo != null)
                    {
                        if (creditCards != null)
                        {
                            foreach (string cardKey in creditCards.Keys)
                            {
                                if ((paymentInfo.CardType == creditCards[cardKey].CardType) &&
                                    (paymentInfo.CreditCardNumberMasked == creditCards[cardKey].CardNumber))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks whether to display credit card section or not based on the below conditions
        /// 1.Is it a logged in user or not
        /// 2.Is any one of the room is Save category or not
        /// 3.Credit card already not exists or not
        /// </summary>
        /// <returns></returns>
        public bool CanCCSectionDisplayed()
        {
            if (!UserLoggedInSessionWrapper.UserLoggedIn) return false;
            if (!RoomRateUtil.IsAnyRoomHavingSaveCategory(BookingEngineSessionWrapper.SelectedRoomAndRatesHashTable)) return false;
            if (HygieneSessionWrapper.IsEarlyBookingUsingStoredPanHashCC) return false;
            return !DoesCreditCardExistAlready();
        }

        /// <summary>
        /// Saves credit card into user profile.
        /// </summary>
        /// <returns></returns>
        public string SaveCreditCard()
        {
            string saveStatus = string.Empty;

            try
            {
                var loyaltyDetails = LoyaltyDetailsSessionWrapper.LoyaltyDetails;
                
                if (loyaltyDetails != null)
                {
                    var search = SearchCriteriaSessionWrapper.SearchCriteria;

                    if (!string.IsNullOrEmpty(Reservation2SessionWrapper.PaymentTransactionId) && (search != null) && 
                        (!string.IsNullOrEmpty(search.SelectedHotelCode)))
                    {
                        var hotelMerchantInfo = m_ContentDataAccessManager.GetMerchantByHotelOperaId(search.SelectedHotelCode);
                        if (hotelMerchantInfo != null)
                        {
                            PaymentHelper paymentHelper = new PaymentHelper();
                            var paymentInfo = RoomRateUtil.ConvertToPaymentInfo(
                                paymentHelper.Query(Reservation2SessionWrapper.PaymentTransactionId,
                                                    hotelMerchantInfo.MerchantID, hotelMerchantInfo.MerchantToken));

                            if (paymentInfo != null)
                            {
                                var expiryDate = new ExpiryDateEntity(paymentInfo.ExpiryDate.Month,
                                                                      paymentInfo.ExpiryDate.Year);
                                var creditCard =
                                    new CreditCardEntity(paymentInfo.PanHash,
                                        paymentInfo.CardType, paymentInfo.CreditCardNumberMasked, expiryDate);

                                var loyaltyDomain = new LoyaltyDomain();
                                loyaltyDomain.InsertCreditCard(loyaltyDetails.NameID, creditCard);

                                saveStatus = "OK";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
            }

            return saveStatus;
        }

        #endregion Credit Card

        #region GetMemberDOB

        /// <summary>
        /// Fetches the Date Of Birth opf a member.
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        public DateOfBirthEntity GetMemberDOB(string nameID)
        {
            UserProfileEntity userProfileEntityForDOB = null;
            try
            {
                if (m_SessionManager.UserProfileInformation != null &&
                    m_SessionManager.UserProfileInformation.DateOfBirth != null)
                {
                    return m_SessionManager.UserProfileInformation.DateOfBirth;
                }
                else
                {
                    userProfileEntityForDOB = FetchUserDetails(nameID);
                }

                if (userProfileEntityForDOB != null)
                {
                    if (userProfileEntityForDOB.DateOfBirth != null)
                    {
                        return userProfileEntityForDOB.DateOfBirth;
                    }
                }
            }
            catch (Exception Ex)
            {
                AppLogger.LogFatalException(Ex, "Unnable to fetch Date Of Birth in NameController\\GetMemberDOB().");
                return null;
            }

            return null;
        }

        #endregion GetMemberDOB

        #region GetNewPassword

        /// <summary>
        /// Gets brand new password for this membership number.
        /// </summary>
        /// <param name="membershipNumber">
        /// Membership number for which the password need to be changed
        /// </param>
        /// <returns>New password</returns>
        /// <remarks></remarks>
        public string GetNewPassword(string membershipNumber)
        {
            m_LoyaltyDomain.PrimaryLanguageID = primaryLanguageID;

            string newPassword = m_LoyaltyDomain.GetNewPassword(membershipNumber);
            return newPassword;
        }

        #endregion GetNewPassword
    }
}