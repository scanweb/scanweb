//  Description					: Domain class for Reservation OWS Calls.			     //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						: Himansu Senapati / Santhosh Yamsani / Shankar Dasgutpa  //
// Author email id				:                           							  //
// Creation Date				: 05th November  2007									  //
//	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.SessionState;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Membership;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using OWSAvailability = Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using System.Web;
using System.Collections.ObjectModel;
using Scandic.Scanweb.BookingEngine.Controller.Session;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// AvailabilityController 
    /// </summary>
    public class AvailabilityController
    {
        #region Constructors
        /// <summary>
        /// AvailabilityController, accepts domain dependencies as a part of instantiation. 
        /// </summary>
        /// <param name="availabilityDomainObj"></param>
        /// <param name="loyaltyDomainObj"></param>
        /// <param name="sessionManagerObj"></param>
        /// <param name="contentDataAccessManagerObj"></param>
        public AvailabilityController(IAvailabilityDomain availabilityDomainObj, ILoyaltyDomain loyaltyDomainObj,
                                      ISessionManager sessionManagerObj,
                                      IContentDataAccessManager contentDataAccessManagerObj)
        {
            availabilityDomain = availabilityDomainObj;
            loyaltyDomain = loyaltyDomainObj;
            sessionManager = sessionManagerObj;
            contentDataAccessManager = contentDataAccessManagerObj;
        }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public AvailabilityController()
        {
            availabilityDomain = new AvailabilityDomain();
            loyaltyDomain = new LoyaltyDomain();
            sessionManager = new SessionManager();
            contentDataAccessManager = new ContentDataAccessManager();
        }
        #endregion

        #region Delegates
        private delegate void ProcessServiceResponse(
            AvailabilityResponse response, Hashtable availabilitySearchData, int? roomAvailability);
        #endregion

        #region Domain Properties
        private IAvailabilityDomain availabilityDomain = null;
        private ILoyaltyDomain loyaltyDomain = null;

        /// <summary>
        /// Gets AvailabilityDomain instance
        /// </summary>
        private IAvailabilityDomain m_AvailabilityDomain
        {
            get
            {
                return availabilityDomain;
            }
        }

        /// <summary>
        /// Gets LoyaltyDomain instance
        /// </summary>
        private ILoyaltyDomain m_LoyalityDomain
        {
            get
            {
                return loyaltyDomain;
            }
        }
        #endregion

        #region Session Manager

        private ISessionManager sessionManager = null;
        /// <summary>
        /// Gets m_SessionManager
        /// </summary>
        private ISessionManager m_SessionManager
        {
            get
            {
               return sessionManager;
            }
        }

        #endregion 

        #region ContentDataAccess Manager
        private IContentDataAccessManager contentDataAccessManager = null;
        /// <summary>
        /// Gets m_ContentDataAccessManager
        /// </summary>
        private IContentDataAccessManager m_ContentDataAccessManager
        {
            get
            {
                return contentDataAccessManager;
            }
        }

        #endregion

        #region Select Hotel Methods
        /// <summary>
        /// Gets/Sets operaStatus 
        /// </summary>
        public static string operaStatus { get; set; }

        #region Private Variables

        /// <summary>
        ///  Object used for locking
        /// </summary>
        private object lockObject = new object();

        private object lockthreadCountObject = new object();

        private object lockAvailabilityTableRead = new object();

        private object lockAvailabilityTableWrite = new object();

        private object lockCalendarHotelsObject = new object();

        #endregion

        #region SearchHotels

        /// <summary>
        /// Makes RegionalAvailability and General Availability callsto display objects
        /// 
        /// General Availability searches are made using ThreadPool Technique. 
        /// Each thread will invoke a General Availability calls and search updates the session 
        /// with the returned availability details.   
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        public void SearchHotels(HotelSearchEntity hotelSearchEntity)
        {
            RoomRateUtil.RatePlanMap = m_ContentDataAccessManager.GetRateTypeCategoryMap();
            RoomRateUtil.RoomTypeMap = m_ContentDataAccessManager.GetRoomTypeCategoryMap();

            AppLogger.LogInfoMessage("In AvailabilityController :: SearchHotels()");

            HttpSessionState session = System.Web.HttpContext.Current.Session;
            SetupInitialHotelDetails();

            #region R1.4 Marketting City

            m_SessionManager.AltCityHotelsSearchDone = false;

            List<HotelDestination> hotels = m_ContentDataAccessManager.GetBookableHotels(hotelSearchEntity.SearchedFor.SearchCode);

            if (hotels != null && hotels.Count > 0)
            {
                if (hotels != null && hotels.Count > 0 && hotelSearchEntity.ListRooms != null &&
                    hotelSearchEntity.ListRooms.Count > 0)
                {
                    m_SessionManager.TotalRegionalAvailableHotels = hotels.Count*hotelSearchEntity.ListRooms.Count;

                    Hashtable availabilityTable = new Hashtable();
                    m_SessionManager.SetAvailabilityTable(session, availabilityTable);
                    for (int roomNo = 0; roomNo < hotelSearchEntity.ListRooms.Count; roomNo++)
                    {
                        for (int hotelNo = 0; hotelNo < hotels.Count; hotelNo++)
                        {
                            HotelDestination hotelDestination = hotels[hotelNo];
                            Hashtable paramList = CreateParameterForThread(hotelSearchEntity, session, hotelDestination,
                                                                           null, hotelNo, roomNo, hotels.Count,
                                                                           hotelSearchEntity.ListRooms.Count);
                            GeneralAvailabilitySearch(paramList);
                        }
                    }
                }
            }
            else
            {
                m_SessionManager.TotalRegionalAvailableHotels = int.MinValue;
                m_SessionManager.TotalGeneralAvailableHotels = int.MinValue;
                m_SessionManager.NoRedemptionHotelsAvailable = true;
            }

            #endregion
        }

        #region CallGeneralAvailability

        /// <summary>
        /// This method will do the General Avaialbility. This method is called by the thread, these
        /// threads are spawmed for each hotel after in the Regional availability response.
        /// 
        /// This is the Callback method for each Individual thread.
        /// 
        /// Will read the parameter from the threadcontext to do the General Availability OWS calls,
        /// The response will be merged with the CDAL/OWS call hotel details.
        /// 
        /// For each different Search type, the corresponding type of the IHotelDetails object is created
        /// and saved to the session
        /// </summary>
        /// <param name="threadContext"></param>
        public void CallGeneralAvailability(Object threadContext)
        {
            Hashtable paramList = threadContext as Hashtable;
            HotelSearchEntity hotelSearch = paramList[AppConstants.THREAD_HOTEL_SEARCH_ENTITY] as HotelSearchEntity;
            string chainCode = paramList[AppConstants.THREAD_HOTEL_CHAIN_CODE] as string;
            HotelDestination hotelDestination = paramList[AppConstants.THREAD_HOTEL_DESTINATION] as HotelDestination;
            HttpSessionState session = paramList[AppConstants.THREAD_SESSION] as HttpSessionState;
            string countryCode = paramList[AppConstants.THREAD_COUNTRY_CODE] as string;
            RateAward[] rateAwards = paramList[AppConstants.THREAD_RATE_AWARD] as RateAward[];

            int hotelNo = (int) paramList[AppConstants.THREAD_HOTEL_NO];
            int roomNo = (int) paramList[AppConstants.THREAD_ROOM_NO];
            int hotelCount = (int) paramList[AppConstants.THREAD_HOTEL_COUNT];
            int roomCount = (int) paramList[AppConstants.THREAD_ROOM_COUNT];
            RoomStay roomStay = null;

            Availability roomAvailability = Availability.UNKNOWN;
            Availability hotelAvailability = Availability.UNKNOWN;

            try
            {
                operaStatus = string.Empty;
                if (null == chainCode)
                    chainCode = AppConstants.CHAIN_CODE;

                string hotelCode = hotelDestination.OperaDestinationId;
                AvailabilityResponse response = null;

                try
                {
                    lock (lockAvailabilityTableRead)
                    {
                        Hashtable availabilityTable = m_SessionManager.GetAvailabilityTable(session);

                        for (int j = 0; j < roomCount; j++)
                        {
                            string key = string.Format("{0}{1}", hotelNo, j);
                            if (availabilityTable.ContainsKey(key))
                            {
                                HotelRoomTableEntry availabilityEntry = availabilityTable[key] as HotelRoomTableEntry;
                                if (availabilityEntry.RoomAvailability == Availability.UNAVAILABLE)
                                {
                                    hotelAvailability = Availability.UNAVAILABLE;
                                    break;
                                }
                            }
                        }
                    }

                    if (hotelAvailability == Availability.UNAVAILABLE)
                    {
                        throw new OWSException(string.Empty, string.Empty, new Hashtable(), true);
                    }

                    if (hotelSearch.ListRooms != null && hotelSearch.ListRooms.Count > 0 &&
                        roomNo < hotelSearch.ListRooms.Count)
                    {
                        HotelSearchRoomEntity roomSearch = hotelSearch.ListRooms[roomNo];
                        if (roomSearch != null)
                        {
                            if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                            {
                                response = m_AvailabilityDomain.RedemptionAvailability(hotelSearch, roomSearch, hotelCode,
                                                                                     chainCode,
                                                                                     ((LoyaltyDetailsEntity)
                                                                                      (session.Contents[
                                                                                          "BE_LOYALTY_DETAILS"])).
                                                                                         MembershipLevel);
                            }
                            else
                            {
                                response = m_AvailabilityDomain.GeneralAvailability(hotelSearch, roomSearch, hotelCode,
                                                                                  chainCode);
                            }
                        }
                    }

                    roomAvailability = Availability.AVAILABLE;
                }
                catch (OWSException oe)
                {
                    roomAvailability = Availability.UNAVAILABLE;
                    AppLogger.LogCustomException(oe, AppConstants.OWS_EXCEPTION,
                                                 "Exception occured in AvailabilityController: CallGeneralAvailability Method");
                }
                if (null != response)
                {
                    roomStay = response.AvailResponseSegments[0].RoomStayList[0];
                }
            }
            catch (Exception e)
            {
                AppLogger.LogFatalException(e,
                                            "Exception occured in AvailabilityController: CallGeneralAvailability Method");
                operaStatus = e.Message;
            }
            finally
            {
                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                {
                    List<RateAward> collectionRateAwards = new List<RateAward>();
                    if (roomStay != null)
                    {
                        foreach (
                            ServiceProxies.Availability.RoomRate roomRate in
                                roomStay.RoomRates)
                        {
                            string hotelCode = hotelDestination.OperaDestinationId;
                            RateAward lstrateAwards = new RateAward();
                            lstrateAwards.membershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;
                            lstrateAwards.awardType =
                                ((LoyaltyDetailsEntity)(session.Contents["BE_LOYALTY_DETAILS"])).MembershipLevel;
                            lstrateAwards.resort = hotelCode;
                            lstrateAwards.rateCode = roomRate.ratePlanCode;
                            lstrateAwards.points_required = roomRate.TotalPoints;
                            lstrateAwards.roomCategory = roomRate.roomTypeCode;
                            collectionRateAwards.Add(lstrateAwards);
                        }
                        rateAwards = collectionRateAwards.ToArray();
                    }
                }
                AddHotelDetails(hotelSearch, hotelDestination, roomStay, hotelNo, roomNo, countryCode, rateAwards,
                                session, roomAvailability, roomCount);
                IncrementGeneralAvailabilityCounter(session);
            }
        }

        /// <summary>
        /// AddHotelDetails
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="hotelDestination"></param>
        /// <param name="roomStay"></param>
        /// <param name="hotelNo"></param>
        /// <param name="roomNo"></param>
        /// <param name="countryCode"></param>
        /// <param name="rateAwards"></param>
        /// <param name="session"></param>
        /// <param name="roomAvailability"></param>
        /// <param name="roomCount"></param>
        private void AddHotelDetails(HotelSearchEntity hotelSearch, HotelDestination hotelDestination, RoomStay roomStay,
                                     int hotelNo, int roomNo, string countryCode, RateAward[] rateAwards,
                                     HttpSessionState session, Availability roomAvailability, int roomCount)
        {
            try
            {
                Availability hotelAvailability = Availability.AVAILABLE;
                HotelRoomTableEntry availabilityEntry = new HotelRoomTableEntry();
                availabilityEntry.RoomAvailability = roomAvailability;
                availabilityEntry.RoomStay = roomStay;

                IList<RoomStay> listRoomStay = null;
                lock (lockAvailabilityTableWrite)
                {
                    Hashtable availabilityTable = m_SessionManager.GetAvailabilityTable(session);
                    if (!availabilityTable.ContainsKey(string.Format("{0}{1}", hotelNo, roomNo)))
                        availabilityTable.Add(string.Format("{0}{1}", hotelNo, roomNo), availabilityEntry);

                    for (int j = 0; j < roomCount; j++)
                    {
                        string key = string.Format("{0}{1}", hotelNo, j);
                        if (availabilityTable.ContainsKey(key))
                        {
                            availabilityEntry = availabilityTable[key] as HotelRoomTableEntry;
                            if (availabilityEntry.RoomAvailability == Availability.UNAVAILABLE )
                            {
                                hotelAvailability = Availability.UNAVAILABLE;
                                break;
                            }
                            else if (availabilityEntry.RoomAvailability == Availability.UNKNOWN)
                            {
                                hotelAvailability = Availability.UNKNOWN;
                                break;
                            }
                            else if (availabilityEntry.RoomAvailability == Availability.AVAILABLE)
                            {
                                if (listRoomStay == null)
                                {
                                    listRoomStay = new List<RoomStay>();
                                }

                                listRoomStay.Add(availabilityEntry.RoomStay);
                            }
                        }
                        else
                        {
                            hotelAvailability = Availability.UNKNOWN;
                            break;
                        }
                    }
                }

                if (hotelAvailability == Availability.AVAILABLE)
                {
                    IHotelDetails hotelDetails = null;
                    if (hotelSearch != null)
                    {
                        switch (hotelSearch.SearchingType)
                        {
                            case SearchType.REDEMPTION:
                                {
                                    hotelDetails = new RedemptionHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                              rateAwards, hotelSearch.NoOfNights);
                                    break;
                                }
                            case SearchType.REGULAR:
                                {
                                    if (null != hotelSearch.CampaignCode)
                                    {
                                        hotelDetails = new PromotionHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                                 hotelSearch.CampaignCode);
                                    }
                                    else
                                    {
                                        hotelDetails = new RegularHotelDetails(hotelDestination, countryCode, listRoomStay);
                                    }
                                    break;
                                }
                            case SearchType.VOUCHER:
                                {
                                    hotelDetails = new VoucherHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                           hotelSearch.CampaignCode);
                                    break;
                                }
                            case SearchType.BONUSCHEQUE:
                                {
                                    hotelDetails = new BonusChequeHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                               hotelSearch.ArrivalDate,
                                                                               hotelSearch.DepartureDate);
                                    break;
                                }
                            case SearchType.CORPORATE:
                                {
                                    if ((!string.IsNullOrEmpty(hotelSearch.CampaignCode)) &&
                                        (hotelSearch.QualifyingType.Equals(AppConstants.BLOCK_CODE_QUALIFYING_TYPE)))
                                    {
                                        hotelDetails = new BlockCodeHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                                 hotelSearch);
                                        if (hotelDetails.MaxRate == null || hotelDetails.MinRate == null)
                                        {
                                            System.Exception arbException =
                                                new Exception("ARB Booking,Rates are not configured for hotel :" +
                                                              hotelDetails.HotelDestination != null ? hotelDetails.HotelDestination.Name : string.Empty);
                                            throw arbException;
                                        }
                                        else if ((hotelDetails.MaxRate.Rate == double.MinValue) &&
                                            (hotelDetails.MinRate.Rate == double.MinValue))
                                        {
                                            System.Exception arbException =
                                                new Exception("ARB Booking,Rates are not configured for hotel :" +
                                                              hotelDetails.HotelDestination != null ? hotelDetails.HotelDestination.Name : string.Empty);
                                            throw arbException;
                                        }
                                    }
                                    else
                                    {
                                        hotelDetails = new NegotiatedHotelDetails(hotelDestination, countryCode,
                                                                                  listRoomStay, hotelSearch.CampaignCode);
                                    }
                                    break;
                                }
                        }
                    }
                    UpdateSession(hotelDetails, session);
                }
            }
            catch (Exception e)
            {
                AppLogger.LogFatalException(e, "Exception occured in AvailabilityController: AddHotelDetails Method");
            }
        }

        /// <summary>
        /// CreateParameterForThread
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="session"></param>
        /// <param name="hotelDestEntity"></param>
        /// <param name="rateAwards"></param>
        /// <param name="hotelNo"></param>
        /// <param name="roomNo"></param>
        /// <param name="hotelCount"></param>
        /// <param name="roomCount"></param>
        /// <returns></returns>
        private Hashtable CreateParameterForThread(HotelSearchEntity hotelSearch, HttpSessionState session,
                                                   HotelDestination hotelDestEntity, RateAward[] rateAwards, int hotelNo,
                                                   int roomNo, int hotelCount, int roomCount)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add(AppConstants.THREAD_HOTEL_SEARCH_ENTITY, hotelSearch);
            paramList.Add(AppConstants.THREAD_HOTEL_CHAIN_CODE, AppConstants.CHAIN_CODE);
            paramList.Add(AppConstants.THREAD_SESSION, session);
            paramList.Add(AppConstants.THREAD_HOTEL_DESTINATION, hotelDestEntity);
            paramList.Add(AppConstants.THREAD_RATE_AWARD, rateAwards);
            paramList.Add(AppConstants.THREAD_COUNTRY_CODE, hotelDestEntity.CountryCode);
            paramList.Add(AppConstants.THREAD_HOTEL_COUNT, hotelCount);
            paramList.Add(AppConstants.THREAD_HOTEL_NO, hotelNo);
            paramList.Add(AppConstants.THREAD_ROOM_COUNT, roomCount);
            paramList.Add(AppConstants.THREAD_ROOM_NO, roomNo);
            return paramList;
        }
        #endregion

        #region SearchAlternativeCityHotels

        /// <summary>
        /// This method search for the availability for hotels 
        /// from the alternative cities for a particular city
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        public void SearchAlternativeCityHotels(HotelSearchEntity hotelSearchEntity)
        {
            RoomRateUtil.RatePlanMap = m_ContentDataAccessManager.GetRateTypeCategoryMap();
            RoomRateUtil.RoomTypeMap = m_ContentDataAccessManager.GetRoomTypeCategoryMap();

            AppLogger.LogInfoMessage("In AvailabilityController :: SearchHotels()");

            HttpSessionState session = System.Web.HttpContext.Current.Session;

            //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
            if (!m_SessionManager.IsDestinationAlternateFlow)
                SetupInitialHotelDetails();

            m_SessionManager.AltCityHotelsSearchDone = true;

            List<HotelDestination> hotels =
                m_ContentDataAccessManager.GetAlternateCityHotels(hotelSearchEntity.SearchedFor.SearchCode);

            //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
            if (m_SessionManager.IsDestinationAlternateFlow)
            {
                m_SessionManager.TotalRegionalAltCityHotelCount = hotels.Count;
                AlternateCityHotelsSearchSessionWrapper.CityAltHotelDetails = hotels;
            }

            if (hotels != null && hotelSearchEntity.ListRooms != null && hotelSearchEntity.ListRooms.Count > 0)
            {
                //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
                if (hotelSearchEntity != null && hotelSearchEntity.CampaignCode != null && hotelSearchEntity.SearchingType == SearchType.REGULAR)
                    m_SessionManager.TotalRegionalAvailableHotels = m_SessionManager.TotalRegionalAvailableHotels + (hotels.Count * hotelSearchEntity.ListRooms.Count);
                else
                    m_SessionManager.TotalRegionalAvailableHotels = hotels.Count * hotelSearchEntity.ListRooms.Count;

                Hashtable availabilityTable = new Hashtable();
                m_SessionManager.SetAvailabilityTable(session, availabilityTable);


                if (hotels.Count > 0)
                {
                    for (int roomNo = 0; roomNo < hotelSearchEntity.ListRooms.Count; roomNo++)
                    {
                        for (int hotelNo = 0; hotelNo < hotels.Count; hotelNo++)
                        {
                            HotelDestination hotelDestination = hotels[hotelNo];
                            Hashtable paramList = CreateParameterForThread(hotelSearchEntity, session, hotelDestination,
                                                                           null, hotelNo, roomNo, hotels.Count,
                                                                           hotelSearchEntity.ListRooms.Count);
                            GeneralAvailabilitySearch(paramList);
                        }
                    }
                }
            }
            else
            {
                m_SessionManager.TotalRegionalAvailableHotels = 0;
            }
        }

        #endregion

        #region RedemptionSearchHotels

        /// <summary>
        /// Method to initiate the Redemption Search, This method does the following steps in that sequence
        /// I. Do the Regional Availability search to get the list of hotels for the searched city
        /// II. For All the list of hotels returned as part of the regional availabiltiy response, 
        /// the Fetch rate awards is called
        /// III. For each hotel's responses of the Regional Availabillity property, 
        /// Fetch RateAwards the parameter required to call the General availability is created
        /// IV. With the paravmeters created above the General availability is called for each hotel
        /// </summary>
        /// <param name="hotelSearch">The hotel search entity containing the details user has searched for</param>
        /// <param name="membershipLevel">The membership level of the user initiated the Redemption search</param>
        public void RedemptionSearchHotels(HotelSearchEntity hotelSearch, string membershipLevel)
        {
            RoomRateUtil.RatePlanMap = m_ContentDataAccessManager.GetRateTypeCategoryMap();
            RoomRateUtil.RoomTypeMap = m_ContentDataAccessManager.GetRoomTypeCategoryMap();

            HttpSessionState session = System.Web.HttpContext.Current.Session;
            SetupInitialHotelDetails();

            #region R1.4 Markettting City

            m_SessionManager.AltCityHotelsSearchDone = false;
            
            List<HotelDestination> hotels = m_ContentDataAccessManager.GetBookableHotels(hotelSearch.SearchedFor.SearchCode);

            m_SessionManager.TotalRegionalAvailableHotels = hotels.Count;
            
            if (hotels != null && hotels.Count > 0)
            {
                RateAward[] rateAwards = null;
                
                Hashtable availabilityTable = new Hashtable();
                m_SessionManager.SetAvailabilityTable(session, availabilityTable);

                for (int roomNo = 0; roomNo < hotelSearch.ListRooms.Count; roomNo++)
                {
                    for (int hotelNo = 0; hotelNo < hotels.Count; hotelNo++)
                    {
                        HotelDestination hotelDestination = hotels[hotelNo];
                        Hashtable paramList = CreateParameterForThread(hotelSearch, session, hotelDestination,
                                                                       rateAwards, hotelNo, roomNo, hotels.Count,
                                                                       hotelSearch.ListRooms.Count);
                        GeneralAvailabilitySearch(paramList);
                    }
                }
            }
            else
            {
                m_SessionManager.TotalRegionalAvailableHotels = int.MinValue;
                m_SessionManager.TotalGeneralAvailableHotels = int.MinValue;
                m_SessionManager.NoRedemptionHotelsAvailable = true;
            }

            #endregion R1.4
        }

        #endregion

        #region RedemptionSearchAlternativeCityHotels

        /// <summary>
        /// Method to initiate the Redemption Search, This method does the following steps in that sequence
        /// I. Do the Regional Availability search to get the list of hotels for the searched city
        /// II. For All the list of hotels returned as part of the regional availabiltiy response, 
        /// the Fetch rate awards is called
        /// III. For each hotel's responses of the Regional Availabillity property, 
        /// Fetch RateAwards the parameter required to call the General availability is created
        /// IV. With the paravmeters created above the General availability is called for each hotel
        /// </summary>
        /// <param name="hotelSearch">The hotel search entity containing the details user has searched for</param>
        /// <param name="membershipLevel">The membership level of the user initiated the Redemption search</param>
        public void RedemptionSearchAlternativeCityHotels(HotelSearchEntity hotelSearch, string membershipLevel)
        {
            RoomRateUtil.RatePlanMap = m_ContentDataAccessManager.GetRateTypeCategoryMap();
            RoomRateUtil.RoomTypeMap = m_ContentDataAccessManager.GetRoomTypeCategoryMap();

            HttpSessionState session = System.Web.HttpContext.Current.Session;
            SetupInitialHotelDetails();

            m_SessionManager.AltCityHotelsSearchDone = true;
            
            List<HotelDestination> hotels
                = m_ContentDataAccessManager.GetAlternateCityHotels(hotelSearch.SearchedFor.SearchCode);

            if (hotels != null && hotels.Count > 0)
            {
                RateAward[] rateAwards = null;
                m_SessionManager.TotalRegionalAvailableHotels = hotels.Count;

                Hashtable availabilityTable = new Hashtable();
                m_SessionManager.SetAvailabilityTable(session, availabilityTable);

                for (int roomNo = 0; roomNo < hotelSearch.ListRooms.Count; roomNo++)
                {
                    for (int hotelNo = 0; hotelNo < hotels.Count; hotelNo++)
                    {
                        HotelDestination hotelDestination = hotels[hotelNo];
                        Hashtable paramList = CreateParameterForThread(hotelSearch, session, hotelDestination,
                                                                       rateAwards, hotelNo, roomNo, hotels.Count,
                                                                       hotelSearch.ListRooms.Count);
                        GeneralAvailabilitySearch(paramList);
                    }
                }
            }
            else
            {
                m_SessionManager.TotalRegionalAvailableHotels = 0;
            }
        }

        #endregion

        #region SetupInitialHotelDetails

        /// <summary>
        /// This method will setup the empty hotel list, empty google map results 
        /// and the total number of generalavailablity results to zero.
        /// 
        /// This need to be done before the regional avilability and generaly availability
        /// searches are done and details saved to session
        /// </summary>
        private void SetupInitialHotelDetails()
        {
            m_SessionManager.HotelResults = new List<IHotelDetails>();
            List<Dictionary<string, object>> googleMapResults = new List<Dictionary<string, object>>();
            m_SessionManager.GoogleMapResults = googleMapResults;
            m_SessionManager.TotalGeneralAvailableHotels = 0;
            m_SessionManager.TotalRegionalAvailableHotels = 0;
        }

        #endregion SetupInitialHotelDetails

        #region GetHotelDestinationEntity

        /// <summary>
        /// This method will search for the HotelDestination object from the CDAL
        /// if the hotel details are not existing in the CMS (i.e., CDAL) then
        /// the details returned from the OWS and existing in the RegionalAvailabileProperty 
        /// will be set to the new HotelDestination object.
        /// </summary>
        /// <param name="hotelReference">The hotel reference object from OWS</param>
        /// <param name="hotelContact">The hotel contact object returned from OWS</param>
        /// <returns></returns>
        private HotelDestination GetHotelDestinationEntity(
            OWSAvailability.HotelReference hotelReference,
            HotelContact hotelContact)
        {
            string hotelCode = hotelReference.hotelCode;
            HotelDestination hotelDestination = null;
            try
            {
                AppLogger.LogInfoMessage("Fetching HotelDestination from CDAL for HotelCode: " + hotelCode);

                hotelDestination = m_ContentDataAccessManager.GetHotelByOperaID(hotelCode);
            }
            catch (Exception e)
            {
                AppLogger.LogFatalException(e, "Could not fetch data from CDAL - GetHotelDestinationEntity.");
            }
            if (hotelDestination == null)
            {
                StringBuilder addressBuilder = new StringBuilder(string.Empty);
                if (hotelContact.Addresses[0] != null)
                {
                    int noOfAddress = hotelContact.Addresses[0].AddressLine.Length;
                    for (int a = 0; a < noOfAddress; a++)
                    {
                        if (addressBuilder.ToString() == string.Empty)
                        {
                            addressBuilder.Append(hotelContact.Addresses[0].AddressLine[a]);
                        }
                        else
                        {
                            addressBuilder.Append(", ");
                            addressBuilder.Append(hotelContact.Addresses[0].AddressLine[a]);
                        }
                    }
                }

                string name = hotelReference.Value;
                string description = string.Empty;
                string operaId = hotelReference.hotelCode;
                string postcode = hotelContact.Addresses[0].postalCode;
                string city = hotelContact.Addresses[0].cityName;
                string country = hotelContact.Addresses[0].countryCode;
                Core.Address address = new Core.Address(addressBuilder.ToString(),
                                       postcode, string.Empty, city, country);
                hotelDestination =
                    new HotelDestination(operaId, name, null, null, description, address, null, null, null, null, 0,
                                         null, 0, null, 0, string.Empty, string.Empty, null, null, null, null, true,
                                         false, 0, null, null, string.Empty, string.Empty, string.Empty, 0, 0, false,
                                         false, false, false, false, false, false, 0.0, false, string.Empty, false, string.Empty, 0, false, string.Empty, string.Empty);
            }

            return hotelDestination;
        }

        /// <summary>
        /// This method will search for the HotelDestination object from the CDAL
        /// </summary>
        /// <param name="operaId">
        /// The opera Id for the hotel</param>
        /// <returns>
        /// HotelDestination
        /// </returns>
        public HotelDestination GetHotelDestinationEntity(string operaId)
        {
            HotelDestination hotelDestination = null;
            try
            {
                if (operaId != null)
                {
                    AppLogger.LogInfoMessage("Fetching HotelDestination from CDAL for HotelCode: " + operaId);
                    hotelDestination = m_ContentDataAccessManager.GetHotelByOperaID(operaId);
                }
            }
            catch (Exception e)
            {
                AppLogger.LogFatalException(e, "Could not fetch data from CDAL.");
            }
            return hotelDestination;
        }

        #endregion

        #endregion CallGeneralAvailability

        #region UpdateSession

        /// <summary>
        /// Update the session object with the IHotelDetails object if the hotel is available
        /// so the hotel can be displayed in the Select Hotel page
        /// </summary>
        /// <param name="hotel">The Hotel object to be updated to session if available</param>
        /// <param name="session">The session object to which the hotel detials will be added</param>
        private void UpdateSession(IHotelDetails hotel, HttpSessionState session)
        {
            if (hotel != null && hotel.IsAvailable)
            {
                List<IHotelDetails> hotels = m_SessionManager.GetHotelResults(session);
                lock (lockObject)
                {
                    if (hotels == null)
                    {
                        hotels = new List<IHotelDetails>();
                    }
                    hotels.Add(hotel);
                    m_SessionManager.SetHotelResults(session, hotels);
                }

                HotelDestination hotelDestination = hotel.HotelDestination;
                List<Dictionary<string, object>> googleMapResults = m_SessionManager.GetGoogleMapResults(session);
                lock (googleMapResults)
                {
                    Dictionary<string, object> googleMapsEntry = new Dictionary<string, object>();
                    googleMapsEntry.Add(AppConstants.GM_HOTEL_NAME, hotelDestination.Name);
                    googleMapsEntry.Add(AppConstants.GM_HOTEL_ADDRESS, hotelDestination.HotelAddress.ToString());
                    googleMapsEntry.Add(AppConstants.GM_LANDING_PAGE_URL, hotelDestination.HotelPageURL);
                    googleMapsEntry.Add(AppConstants.GM_BOOKING_PAGE_URL, "http://www.scandic-hotels.com");
                    googleMapsEntry.Add(AppConstants.GM_LONGITUDE_X,
                                        (hotel.HotelDestination.Coordinate != null
                                             ? hotelDestination.Coordinate.X.ToString()
                                             : string.Empty));
                    googleMapsEntry.Add(AppConstants.GM_LATITUDE_Y,
                                        (hotel.HotelDestination.Coordinate != null
                                             ? hotelDestination.Coordinate.Y.ToString()
                                             : string.Empty));
                    googleMapsEntry.Add(AppConstants.GM_HOTEL_ID, hotel.HotelDestination.OperaDestinationId);

                    googleMapsEntry.Add(AppConstants.GM_IHOTELDETAILS, hotel);

                    googleMapResults.Add(googleMapsEntry);
                    m_SessionManager.SetGoogleMapResults(session, googleMapResults);
                }
            }
        }

        #endregion

        #region IncrementGeneralAvailabilityCounter

        /// <summary>
        /// Increment the GeneralAvailabilit counter, which indicates the number of
        /// General availablity calls done, based on this value the client (browser) will
        /// either continues/stops requesting hotels from the server(Scanweb)
        /// </summary>
        /// <param name="session">The http session object</param>
        private void IncrementGeneralAvailabilityCounter(HttpSessionState session)
        {
            lock (lockthreadCountObject)
            {
                int totalGeneralAvailabilityDoneHotels =
                    m_SessionManager.GetTotalGeneralAvailableHotels(session);
                if (int.MinValue == totalGeneralAvailabilityDoneHotels)
                    totalGeneralAvailabilityDoneHotels = 0;
                else
                    totalGeneralAvailabilityDoneHotels++;

                
                m_SessionManager.SetTotalGeneralAvailableHotels(session, totalGeneralAvailabilityDoneHotels);
            }
        }

        #endregion

        #endregion Select Hotel Methods

        #region Select Rate Methods

        #region GetSelectRateDetails
        /// <summary>
        /// Gets select rate details.
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <returns>List of BaseRoomRateDetails</returns>
        public IList<BaseRoomRateDetails> GetSelectRateDetails(HotelSearchEntity hotelSearchEntity)
        {
            IList<BaseRoomRateDetails> listRoomRateDetails = new List<BaseRoomRateDetails>();

            m_SessionManager.ListRoomStay = null;

            if (m_SessionManager.ListRoomStay == null)
            {
                if (hotelSearchEntity != null)
                {
                    for (int i = 0; i < hotelSearchEntity.ListRooms.Count; i++)
                    {
                        if (hotelSearchEntity.ListRooms[i].IsRoomModifiable)
                            hotelSearchEntity.ListRooms[i].IsSearchDone = false;
                    }
                }
            }

            IHotelDetails hotelDetails = GetHotelDetails(hotelSearchEntity);

            switch (hotelSearchEntity.SearchingType)
            {
                case SearchType.REDEMPTION:
                    {
                        int noOfDays = DateUtil.DateDifference(hotelSearchEntity.DepartureDate,
                                                               hotelSearchEntity.ArrivalDate);
                        RedemptionHotelDetails redemptionHotelDetails = hotelDetails as RedemptionHotelDetails;
                        if (null != redemptionHotelDetails && hotelDetails.Rooms != null && hotelDetails.Rooms.Count > 0)
                        {
                            for (int i = 0; i < hotelDetails.Rooms.Count; i++)
                            {
                                BaseRoomRateDetails roomRateDetails =
                                    new RedemptionRoomRateDetails(redemptionHotelDetails, noOfDays, i);
                                listRoomRateDetails.Add(roomRateDetails);
                            }
                        }
                        break;
                    }
                case SearchType.REGULAR:
                    {
                        if (null != hotelSearchEntity.CampaignCode)
                        {
                            //Merchandising:R3:Display ordinary rates for unavailable promo rates 
                            if (sessionManager != null && sessionManager.HotelDetails == null)
                                    sessionManager.HotelDetails = hotelDetails;

                            PromotionHotelDetails promoHotelDetails = hotelDetails as PromotionHotelDetails;
                            if (null != promoHotelDetails && hotelDetails.Rooms != null && hotelDetails.Rooms.Count > 0)
                            {
                                for (int i = 0; i < hotelDetails.Rooms.Count; i++)
                                {
                                    BaseRoomRateDetails roomRateDetails = new PromotionRoomRateDetails(
                                        promoHotelDetails, i);
                                    listRoomRateDetails.Add(roomRateDetails);
                                }
                            }
                        }
                        else
                        {
                            RegularHotelDetails regularHotelDetails = hotelDetails as RegularHotelDetails;
                            if (null != regularHotelDetails && hotelDetails.Rooms != null &&
                                hotelDetails.Rooms.Count > 0)
                            {
                                for (int i = 0; i < hotelDetails.Rooms.Count; i++)
                                {
                                    BaseRoomRateDetails roomRateDetails =
                                        new RegularRoomRateDetails(hotelDetails as RegularHotelDetails, i);
                                    listRoomRateDetails.Add(roomRateDetails);
                                }
                            }
                        }
                        break;
                    }
                case SearchType.VOUCHER:
                    {
                        VoucherHotelDetails voucherHotelDetails = hotelDetails as VoucherHotelDetails;
                        if (null != voucherHotelDetails && hotelDetails.Rooms != null && hotelDetails.Rooms.Count > 0)
                        {
                            for (int i = 0; i < hotelDetails.Rooms.Count; i++)
                            {
                                BaseRoomRateDetails roomRateDetails =
                                    new VoucherRoomRateDetails(hotelDetails as VoucherHotelDetails, i);
                                listRoomRateDetails.Add(roomRateDetails);
                            }
                        }
                        break;
                    }
                case SearchType.BONUSCHEQUE:
                    {
                        BonusChequeHotelDetails bonusChequeHotelDetails = hotelDetails as BonusChequeHotelDetails;
                        if (null != bonusChequeHotelDetails && hotelDetails.Rooms != null &&
                            hotelDetails.Rooms.Count > 0)
                        {
                            for (int i = 0; i < hotelDetails.Rooms.Count; i++)
                            {
                                BaseRoomRateDetails roomRateDetails =
                                    new BonusChequeRoomRateDetails(bonusChequeHotelDetails, i);
                                listRoomRateDetails.Add(roomRateDetails);
                            }
                        }
                        break;
                    }
                case SearchType.CORPORATE:
                    {
                        if (hotelSearchEntity.QualifyingType == AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
                        {
                            BlockCodeHotelDetails blockCodeHotelDeails = hotelDetails as BlockCodeHotelDetails;
                            if (null != blockCodeHotelDeails && hotelDetails.Rooms != null &&
                                hotelDetails.Rooms.Count > 0)
                            {
                                for (int i = 0; i < hotelDetails.Rooms.Count; i++)
                                {
                                    BaseRoomRateDetails roomRateDetails =
                                        new BlockCodeRoomRateDetails(blockCodeHotelDeails, i);
                                    listRoomRateDetails.Add(roomRateDetails);
                                }
                            }
                        }
                        else
                        {
                            NegotiatedHotelDetails negotiatedHotelDetails = hotelDetails as NegotiatedHotelDetails;
                            if (null != negotiatedHotelDetails && hotelDetails.Rooms != null &&
                                hotelDetails.Rooms.Count > 0)
                            {
                                for (int i = 0; i < hotelDetails.Rooms.Count; i++)
                                {
                                    BaseRoomRateDetails roomRateDetails =
                                        new NegotiatedRoomRateDetails(negotiatedHotelDetails, i);
                                    listRoomRateDetails.Add(roomRateDetails);
                                }
                            }
                        }
                        break;
                    }
            }

            return listRoomRateDetails;
        }

        /// <summary>
        /// Get the Hotel Country Code
        /// </summary>
        /// <param name="hotelSearchEntity">
        /// HotelSearchEntity
        /// </param>
        /// <param name="roomSearch"> </param>
        /// <returns>
        /// Country Code
        /// </returns>
        public string GetHotelCountryCode(HotelSearchEntity hotelSearchEntity, HotelSearchRoomEntity roomSearch)
        {
            string hotelCode = hotelSearchEntity.SelectedHotelCode;
            string chainCode = AppConstants.CHAIN_CODE;
            string countryCode = string.Empty;
            AvailabilityResponse response =
                m_AvailabilityDomain.GeneralAvailability(hotelSearchEntity, roomSearch, hotelCode, chainCode);

            if ((response != null) && (response.AvailResponseSegments != null) &&
                (response.AvailResponseSegments.Length > 0))
            {
                RoomStay[] roomStayList = response.AvailResponseSegments[0].RoomStayList;
                if ((roomStayList != null) && (roomStayList.Length > 0))
                {
                    if (roomStayList[0].HotelContact != null)
                    {
                        ServiceProxies.Availability.Address[] addressList = roomStayList[0].HotelContact.Addresses;
                        if ((addressList != null) && (addressList.Length > 0))
                        {
                            countryCode = addressList[0].countryCode;
                        }
                    }
                }
            }
            return countryCode;
        }

        #endregion

        #region GetHotelDetails
        /// <summary>
        /// Get hotel details.
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <returns>IHotelDetails</returns>
        private IHotelDetails GetHotelDetails(HotelSearchEntity hotelSearchEntity)
        {
            string hotelCode = hotelSearchEntity.SelectedHotelCode;
            string chainCode = AppConstants.CHAIN_CODE;
            IHotelDetails hotelDetails = null;

            IList<RoomStay> listRoomStay = m_SessionManager.ListRoomStay;

            bool isTrue = false;
            if (listRoomStay != null)
            {
                foreach (RoomStay roomStay in listRoomStay)
                {
                    if ((hotelSearchEntity.SelectedHotelCode != null) && (roomStay.HotelReference != null))
                    {
                        if (hotelSearchEntity.SelectedHotelCode == roomStay.HotelReference.hotelCode)
                        {
                            isTrue = true;
                            break;
                        }
                    }
                }
                if (!isTrue)
                {
                    for (int i = 0; i < hotelSearchEntity.ListRooms.Count; i++)
                    {
                        hotelSearchEntity.ListRooms[i].IsSearchDone = false;
                    }
                    m_SessionManager.ListRoomStay = null;
                    listRoomStay = null;
                }
            }

            if (listRoomStay == null)
            {
                listRoomStay = new List<RoomStay>();
            }

            if (hotelSearchEntity.ListRooms != null && hotelSearchEntity.ListRooms.Count > 0)
            {
                for (int i = 0; i < hotelSearchEntity.ListRooms.Count; i++)
                {
                    if (!hotelSearchEntity.ListRooms[i].IsSearchDone)
                    {
                        HotelSearchRoomEntity roomSearch = hotelSearchEntity.ListRooms[i];
                        if (roomSearch != null)
                        {
                            AvailabilityResponse response = null;
                            if (hotelSearchEntity.SearchingType == SearchType.REDEMPTION)
                            {
                                    string membershipLevel = m_SessionManager.LoyaltyDetails.MembershipLevel;
                                    response = m_AvailabilityDomain.RedemptionAvailability(hotelSearchEntity, roomSearch,
                                                                                         hotelCode, chainCode, membershipLevel);
                            }
                            else
                            {
                                response = m_AvailabilityDomain.GeneralAvailability(hotelSearchEntity, roomSearch, hotelCode, chainCode);
                            }
                            if (response != null && response.AvailResponseSegments != null &&
                                response.AvailResponseSegments.Length > 0 && response.AvailResponseSegments[0] != null &&
                                response.AvailResponseSegments[0].RoomStayList != null &&
                                response.AvailResponseSegments[0].RoomStayList.Length > 0 &&
                                response.AvailResponseSegments[0].RoomStayList[0] != null)
                            {
                                RoomStay roomStay = null;
                                roomStay = response.AvailResponseSegments[0].RoomStayList[0];
                                if (listRoomStay.Count <= i)
                                    listRoomStay.Add(roomStay);
                                else
                                    listRoomStay[i] = roomStay;
                                hotelSearchEntity.ListRooms[i].IsSearchDone = true;
                            }
                        }
                    } 
                    else if ((m_SessionManager.IsComboReservation) && (hotelSearchEntity.ListRooms[i].IsSearchDone) &&
                             !(hotelSearchEntity.ListRooms[i].IsRoomModifiable))
                    {
                        RoomStay roomStay = new RoomStay();
                        if (listRoomStay.Count <= i)
                            listRoomStay.Add(roomStay);
                        else
                            listRoomStay[i] = roomStay;
                    }
                }
                if (hotelSearchEntity.IsModifyComboBooking)
                {
                    foreach (RoomStay roomStay in listRoomStay)
                    {
                        if((roomStay != null) && (roomStay.RoomRates != null)&& (roomStay.RoomRates.Length > 0))
                        {
                           Collection<RoomRate> roomRates = new Collection<RoomRate>();
                            foreach(RoomRate roomRate in roomStay.RoomRates)
                            {
                                if ((roomRate != null) && (!string.IsNullOrEmpty(roomRate.ratePlanCode)))
                                {
                                    RateCategory rateCategory =
                                        RoomRateUtil.GetRateCategoryByRatePlanCode(roomRate.ratePlanCode);
                                    bool isSaveRate = (rateCategory != null) && (RoomRateUtil.IsSaveRateCategory(rateCategory.RateCategoryId));
                                    if (!isSaveRate)
                                    {
                                        roomRates.Add(roomRate);
                                    }
                                }
                            }
                            roomStay.RoomRates = roomRates.ToArray();
                        }
                    }
                }
                m_SessionManager.ListRoomStay = listRoomStay;

                string countryCode = null;
                HotelContact hotelContact = null;
                HotelDestination hotelDestination = null;
                ServiceProxies.Availability.HotelReference hotelReferece = null;
                if (listRoomStay != null && listRoomStay.Count > 0)
                {
                    foreach (RoomStay roomStayObj in listRoomStay)
                    {
                        if ((roomStayObj.HotelContact != null)
                            && (roomStayObj.HotelContact.Addresses != null) &&
                            (roomStayObj.HotelContact.Addresses.Length > 0)
                            && (roomStayObj.HotelContact.Addresses[0].countryCode != null)
                            && roomStayObj.HotelReference != null)
                        {
                            countryCode = roomStayObj.HotelContact.Addresses[0].countryCode;
                            hotelReferece = roomStayObj.HotelReference;
                            hotelContact = roomStayObj.HotelContact;
                            hotelDestination = GetHotelDestinationEntity(hotelReferece, hotelContact);
                            break;
                        }
                    }

                    switch (hotelSearchEntity.SearchingType)
                    {
                        case SearchType.REDEMPTION:
                            RateAward[] rateAwards = null;

                            List<RateAward> collectionRateAwards = new List<RateAward>();
                            foreach (RoomStay roomStay in listRoomStay)
                            {
                                foreach (
                                    ServiceProxies.Availability.RoomRate roomRate in
                                        roomStay.RoomRates)
                                {
                                    {
                                        RateAward lstrateAwards = new RateAward();
                                        lstrateAwards.membershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;
                                        lstrateAwards.roomCategory = roomRate.roomTypeCode;
                                        lstrateAwards.points_required = roomRate.TotalPoints;
                                        lstrateAwards.resort = hotelSearchEntity.SelectedHotelCode;
                                        lstrateAwards.rateCode = roomRate.ratePlanCode;
                                        lstrateAwards.awardType = m_SessionManager.LoyaltyDetails.MembershipLevel;
                                        collectionRateAwards.Add(lstrateAwards);
                                    }
                                }
                            }
                            rateAwards = collectionRateAwards.ToArray();
                            hotelDetails = new RedemptionHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                      rateAwards, hotelSearchEntity.NoOfNights);

                            break;
                        case SearchType.REGULAR:
                            if (null != hotelSearchEntity.CampaignCode)
                                hotelDetails = new PromotionHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                         hotelSearchEntity.CampaignCode);
                            else
                                hotelDetails = new RegularHotelDetails(hotelDestination, countryCode, listRoomStay);
                            break;
                        case SearchType.VOUCHER:
                            hotelDetails = new VoucherHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                   hotelSearchEntity.CampaignCode);
                            break;
                        case SearchType.BONUSCHEQUE:
                            hotelDetails = new BonusChequeHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                       hotelSearchEntity.ArrivalDate,
                                                                       hotelSearchEntity.DepartureDate);
                            break;
                        case SearchType.CORPORATE:
                            if (hotelSearchEntity.QualifyingType == AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
                            {
                                hotelDetails = new BlockCodeHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                         hotelSearchEntity);
                            }
                            else
                            {
                                hotelDetails = new NegotiatedHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                          hotelSearchEntity.CampaignCode);
                            }
                            break;
                    }
                }
            }
            return hotelDetails;
        }

        /// <summary>
        /// Get all alternate hotel by passing hotel search entity.
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <returns>IHotelDetails</returns>
        private IHotelDetails GetAlternateHotelDetails(HotelSearchEntity hotelSearchEntity)
        {
            string hotelCode = hotelSearchEntity.SelectedHotelCode;
            string chainCode = AppConstants.CHAIN_CODE;
            IHotelDetails hotelDetails = null;

            IList<RoomStay> listRoomStay = null;
            if (listRoomStay == null)
            {
                listRoomStay = new List<RoomStay>();
            }

            if (hotelSearchEntity.ListRooms != null && hotelSearchEntity.ListRooms.Count > 0)
            {
                for (int i = 0; i < hotelSearchEntity.ListRooms.Count; i++)
                {
                    if (!hotelSearchEntity.ListRooms[i].IsSearchDone)
                    {
                        HotelSearchRoomEntity roomSearch = hotelSearchEntity.ListRooms[i];
                        if (roomSearch != null)
                        {
                            RoomStay roomStay = null;
                            AvailabilityResponse response = null;
                            if (hotelSearchEntity.SearchingType == SearchType.REDEMPTION)
                            {
                                string membershipLevel = m_SessionManager.LoyaltyDetails.MembershipLevel;
                                response = m_AvailabilityDomain.RedemptionAvailability(hotelSearchEntity, roomSearch,
                                                                                     hotelCode, chainCode,
                                                                                     membershipLevel);
                            }
                            else
                            {
                                response = m_AvailabilityDomain.GeneralAvailability(hotelSearchEntity, roomSearch,
                                                                                  hotelCode, chainCode);
                            }
                            if (response != null && response.AvailResponseSegments != null &&
                                response.AvailResponseSegments.Length > 0 &&
                                response.AvailResponseSegments[0] != null &&
                                response.AvailResponseSegments[0].RoomStayList != null &&
                                response.AvailResponseSegments[0].RoomStayList.Length > 0 &&
                                response.AvailResponseSegments[0].RoomStayList[0] != null)
                            {
                                roomStay = response.AvailResponseSegments[0].RoomStayList[0];
                                listRoomStay.Add(roomStay);
                                hotelSearchEntity.ListRooms[i].IsSearchDone = true;
                            }
                        }
                    }
                }

                m_SessionManager.ListRoomStay = listRoomStay;

                string countryCode = listRoomStay[0].HotelContact.Addresses[0].countryCode;

                ServiceProxies.Availability.HotelReference hotelReferece = listRoomStay[0].HotelReference;
                HotelContact hotelContact = listRoomStay[0].HotelContact;
                HotelDestination hotelDestination = GetHotelDestinationEntity(hotelReferece, hotelContact);

                switch (hotelSearchEntity.SearchingType)
                {
                    case SearchType.REDEMPTION:
                        List<RateAward> collectionRateAwards = new List<RateAward>();
                        foreach (RoomStay roomStay in listRoomStay)
                        {
                            foreach (
                                ServiceProxies.Availability.RoomRate roomRate in
                                    roomStay.RoomRates)
                            {
                                {
                                    RateAward lstrateAwards = new RateAward();
                                    lstrateAwards.membershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;
                                    lstrateAwards.roomCategory = roomRate.roomTypeCode;
                                    lstrateAwards.points_required = roomRate.TotalPoints;
                                    lstrateAwards.resort = hotelSearchEntity.SelectedHotelCode;
                                    lstrateAwards.rateCode = roomRate.ratePlanCode;
                                    lstrateAwards.awardType = m_SessionManager.LoyaltyDetails.MembershipLevel;
                                    collectionRateAwards.Add(lstrateAwards);
                                }
                            }
                        }
                        RateAward[] rateAwards = collectionRateAwards.ToArray(); 
                        hotelDetails = new RedemptionHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                  rateAwards, hotelSearchEntity.NoOfNights);

                        break;
                    case SearchType.REGULAR:
                        if (null != hotelSearchEntity.CampaignCode)
                            hotelDetails = new PromotionHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                     hotelSearchEntity.CampaignCode);
                        else
                            hotelDetails = new RegularHotelDetails(hotelDestination, countryCode, listRoomStay);
                        break;
                    case SearchType.VOUCHER:
                        hotelDetails = new VoucherHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                               hotelSearchEntity.CampaignCode);
                        break;
                    case SearchType.BONUSCHEQUE:
                        hotelDetails = new BonusChequeHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                   hotelSearchEntity.ArrivalDate,
                                                                   hotelSearchEntity.DepartureDate);
                        break;
                    case SearchType.CORPORATE:
                        if (hotelSearchEntity.QualifyingType == AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
                        {
                            hotelDetails = new BlockCodeHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                     hotelSearchEntity);
                        }
                        else
                        {
                            hotelDetails = new NegotiatedHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                      hotelSearchEntity.CampaignCode);
                        }
                        break;
                }
            }
            return hotelDetails;
        }

        /// <summary>
        /// Creates the params calendar item thread.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <param name="multiDateSearch">The multi date search.</param>
        /// <param name="availabilityCalendar"> </param>
        /// <param name="availabilityCalendarItem">The availability calendar item.</param>
        /// <returns>Hashtable</returns>
        private Hashtable CreateParamsCalendarItemThread(HttpSessionState session, HotelSearchEntity multiDateSearch,
                                                         AvailabilityCalendarEntity availabilityCalendar,
                                                         AvailCalendarItemEntity availabilityCalendarItem)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add(AppConstants.THREAD_SESSION, session);
            paramList.Add(AppConstants.THREAD_HOTEL_SEARCH_ENTITY, multiDateSearch);
            paramList.Add(AppConstants.THREAD_AVAIL_CALENDAR_ITEM_ENTITY, availabilityCalendarItem);
            paramList.Add(AppConstants.THREAD_AVAIL_CALENDAR, availabilityCalendar);
            return paramList;
        }

        /// <summary>
        /// Multirooms the multidate calendar avail search.
        /// </summary>
        /// <param name="search">The search.</param>
        /// <param name="availabilityCalendar">The availability calendar.</param>
        public void MultiroomMultidateCalendarAvailSearch(HotelSearchEntity search,
                                                          AvailabilityCalendarEntity availabilityCalendar)
        {
            RoomRateUtil.RatePlanMap = m_ContentDataAccessManager.GetRateTypeCategoryMap();
            RoomRateUtil.RoomTypeMap = m_ContentDataAccessManager.GetRoomTypeCategoryMap();

            if (search != null && availabilityCalendar != null && availabilityCalendar.ListAvailCalendarItems != null &&
                availabilityCalendar.ListAvailCalendarItems.Count > 0)
            {
                availabilityCalendar.TotalMultiSearchRequests = 0;
                availabilityCalendar.TotalMultiSearchResponses = 0;
                for (int i = availabilityCalendar.LeftCarouselIndex; i <= availabilityCalendar.RightCarouselIndex; i++)
                {
                    if (availabilityCalendar.ListAvailCalendarItems[i] != null &&
                        !availabilityCalendar.ListAvailCalendarItems[i].IsSearchDone)
                    {
                        availabilityCalendar.SearchState = SearchState.PROGRESS;
                        
                        HotelSearchEntity multiDateSearch = search.Clone();
                        multiDateSearch.ArrivalDate = availabilityCalendar.ListAvailCalendarItems[i].ArrivalDate;
                        multiDateSearch.DepartureDate = availabilityCalendar.ListAvailCalendarItems[i].DepartureDate;
                       
                        availabilityCalendar.ListAvailCalendarItems[i].TotalSearchRequests  = 0;
                        availabilityCalendar.ListAvailCalendarItems[i].TotalSearchResponses = 0;
                        availabilityCalendar.ListAvailCalendarItems[i].RoomStays = null;
                        HttpSessionState session = System.Web.HttpContext.Current.Session;
                        MultiRoomCalendarAvailabilitySearch(session, multiDateSearch,
                                                                             availabilityCalendar,
                                                                             availabilityCalendar.ListAvailCalendarItems[i]);
                    }
                }
            }
        }

        /// <summary>
        /// Multis the room calendar availability search.
        /// </summary>
        /// <param name="threadContext">The thread context.</param>
        public void MultiRoomCalendarAvailabilitySearch(Object threadContext)
        {
            if (threadContext != null)
            {
                Hashtable paramList = threadContext as Hashtable;
                HttpSessionState session = paramList[AppConstants.THREAD_SESSION] as HttpSessionState;
                HotelSearchEntity multiDateSearch =
                    paramList[AppConstants.THREAD_HOTEL_SEARCH_ENTITY] as HotelSearchEntity;
                AvailabilityCalendarEntity availabilityCalendar =
                    paramList[AppConstants.THREAD_AVAIL_CALENDAR] as AvailabilityCalendarEntity;
                AvailCalendarItemEntity availabilityCalendarItem =
                    paramList[AppConstants.THREAD_AVAIL_CALENDAR_ITEM_ENTITY] as AvailCalendarItemEntity;

                string hotelCode = multiDateSearch.SelectedHotelCode;
                string chainCode = AppConstants.CHAIN_CODE;
                IHotelDetails hotelDetails = null;

                IList<RoomStay> listRoomStay = new List<RoomStay>();

                try
                {
                    if (multiDateSearch.ListRooms != null && multiDateSearch.ListRooms.Count > 0)
                    {
                        for (int i = 0; i < multiDateSearch.ListRooms.Count; i++)
                        {
                            HotelSearchRoomEntity roomSearch = multiDateSearch.ListRooms[i];
                            if (roomSearch != null)
                            {
                                RoomStay roomStay = null;
                                AvailabilityResponse response = null;
                                if (multiDateSearch.SearchingType == SearchType.REDEMPTION)
                                {
                                    LoyaltyDetailsEntity loyaltyDetails = m_SessionManager.GetLoyaltyDetails(session);
                                    string membershipLevel = loyaltyDetails.MembershipLevel;
                                    response = m_AvailabilityDomain.RedemptionAvailability(multiDateSearch, roomSearch,
                                                                                         hotelCode, chainCode,
                                                                                         membershipLevel);
                                }
                                else
                                {
                                    response = m_AvailabilityDomain.GeneralAvailability(multiDateSearch, roomSearch,
                                                                                      hotelCode, chainCode);
                                }
                                if (response != null && response.AvailResponseSegments != null &&
                                    response.AvailResponseSegments.Length > 0 &&
                                    response.AvailResponseSegments[0] != null &&
                                    response.AvailResponseSegments[0].RoomStayList != null &&
                                    response.AvailResponseSegments[0].RoomStayList.Length > 0 &&
                                    response.AvailResponseSegments[0].RoomStayList[0] != null)
                                {
                                    roomStay = response.AvailResponseSegments[0].RoomStayList[0];
                                    listRoomStay.Add(roomStay);
                                }
                            }
                        }

                        string countryCode = listRoomStay[0].HotelContact.Addresses[0].countryCode;

                        ServiceProxies.Availability.HotelReference hotelReferece = listRoomStay[0].HotelReference;
                        HotelContact hotelContact = listRoomStay[0].HotelContact;

                        HotelDestination hotelDestination = GetHotelDestinationEntity(hotelReferece, hotelContact);

                        switch (multiDateSearch.SearchingType)
                        {
                            case SearchType.REDEMPTION:
                                LoyaltyDetailsEntity loyaltyDetails = m_SessionManager.GetLoyaltyDetails(session);
                                string membershipLevel = loyaltyDetails.MembershipLevel;

                                List<RateAward> collectionRateAwards = new List<RateAward>();
                                foreach (RoomStay roomStay in listRoomStay)
                                {
                                    foreach (
                                        ServiceProxies.Availability.RoomRate roomRate in
                                            roomStay.RoomRates)
                                    {
                                        {
                                            RateAward lstrateAwards = new RateAward();
                                            lstrateAwards.membershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;
                                            lstrateAwards.awardType = membershipLevel;
                                            lstrateAwards.resort = multiDateSearch.SelectedHotelCode;
                                            lstrateAwards.rateCode = roomRate.ratePlanCode;
                                            lstrateAwards.points_required = roomRate.TotalPoints;
                                            lstrateAwards.roomCategory = roomRate.roomTypeCode;
                                            collectionRateAwards.Add(lstrateAwards);
                                        }
                                    }
                                }
                                RateAward[] rateAwards = collectionRateAwards.ToArray(); 
                                hotelDetails = new RedemptionHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                          rateAwards, multiDateSearch.NoOfNights);

                                break;
                            case SearchType.REGULAR:
                                if (null != multiDateSearch.CampaignCode)
                                    hotelDetails = new PromotionHotelDetails(hotelDestination, countryCode, listRoomStay, multiDateSearch.CampaignCode, true);//Merchandising:R3:Display ordinary rates for unavailable promo rates 
                                else
                                    hotelDetails = new RegularHotelDetails(hotelDestination, countryCode, listRoomStay);
                                break;
                            case SearchType.VOUCHER:
                                hotelDetails = new VoucherHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                       multiDateSearch.CampaignCode);
                                break;
                            case SearchType.BONUSCHEQUE:
                                hotelDetails = new BonusChequeHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                           multiDateSearch.ArrivalDate,
                                                                           multiDateSearch.DepartureDate);
                                break;
                            case SearchType.CORPORATE:
                                if (multiDateSearch.QualifyingType == AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
                                {
                                    hotelDetails = new BlockCodeHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                             multiDateSearch);
                                }
                                else
                                {
                                    hotelDetails = new NegotiatedHotelDetails(hotelDestination, countryCode,
                                                                              listRoomStay, multiDateSearch.CampaignCode);
                                }
                                break;
                        }

                        availabilityCalendarItem.IsSearchDone = true;
                        availabilityCalendarItem.IsRatesAvailable = true;
                    }
                }
                catch (OWSException owsException)
                {
                    availabilityCalendarItem.IsSearchDone = true;
                    availabilityCalendarItem.IsRatesAvailable = false;

                    switch (owsException.ErrCode)
                    {
                        case OWSExceptionConstants.PROMOTION_CODE_INVALID:
                        case OWSExceptionConstants.PROPERTY_NOT_AVAILABLE:
                        case OWSExceptionConstants.INVALID_PROPERTY:
                        case OWSExceptionConstants.PROPERTY_RESTRICTED:
                        case OWSExceptionConstants.PRIOR_STAY:
                            AppLogger.LogInfoMessage(AppConstants.OWS_EXCEPTION +
                                                     " \n Availability calendar - OWS Error Message: " +
                                                     owsException.Message +
                                                     "\n OWS Stack trace: " + owsException.StackTrace);
                            break;
                        default:
                            AppLogger.LogCustomException(owsException,
                                                         AppConstants.OWS_EXCEPTION +
                                                         "Availability calendar - OWS Exception occured");
                            break;
                    }
                }
                catch (Exception ex)
                {
                    availabilityCalendarItem.IsSearchDone = true;
                    availabilityCalendarItem.IsRatesAvailable = false;
                    AppLogger.LogCustomException(ex, AppConstants.BUSINESS_EXCEPTION,
                                                 "NON-OWS Exception occured in Availability calendar threading search");
                }
                finally
                {
                    availabilityCalendarItem.HotelDetails = hotelDetails;
                    availabilityCalendar.RunningThreadCount--;
                    if (availabilityCalendar.RunningThreadCount == 0)
                    {
                        availabilityCalendar.SearchState = SearchState.COMPLETED;
                    }
                }
            }
        }

        /// <summary>
        /// This method facilitates in preparing the search entity and calls corresponding method on domain object.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="multiDateSearch"></param>
        /// <param name="availabilityCalendar"></param>
        /// <param name="availabilityCalendarItem"></param>
        private void MultiRoomCalendarAvailabilitySearch(HttpSessionState session, HotelSearchEntity multiDateSearch, AvailabilityCalendarEntity availabilityCalendar,
                                                         AvailCalendarItemEntity availabilityCalendarItem)
        {
            string hotelCode = multiDateSearch.SelectedHotelCode;
            string chainCode = AppConstants.CHAIN_CODE;
            IHotelDetails hotelDetails = null;

            IList<RoomStay> listRoomStay = new List<RoomStay>();

            try
            {
                if (multiDateSearch.ListRooms != null && multiDateSearch.ListRooms.Count > 0)
                {
                    for (int i = 0; i < multiDateSearch.ListRooms.Count; i++)
                    {
                        HotelSearchRoomEntity roomSearch = multiDateSearch.ListRooms[i];
                        if (roomSearch != null)
                        {
                            Hashtable multiRoomCalendarAvailabilitySearchData = CreateParamsCalendarItemThread(session,
                                                                                                               multiDateSearch,
                                                                                                               availabilityCalendar,
                                                                                                               availabilityCalendarItem);
                            multiRoomCalendarAvailabilitySearchData.Add(AppConstants.SEARCHSERVICE_CALLBACK,
                                                        (ProcessServiceResponse)ProcessMultiRoomCalendarAvailabilitySearchResponse);

                            //Merchandising:R3:Display ordinary rates for unavailable promo rates 
                            multiRoomCalendarAvailabilitySearchData.Add(AppConstants.IS_PROMO_CITY_FAIL, false);

                            availabilityCalendar.TotalMultiSearchRequests++;
                            availabilityCalendarItem.TotalSearchRequests++;
                            RoomStay roomStay = null;
                            AvailabilityResponse response = null;
                            if (multiDateSearch.SearchingType == SearchType.REDEMPTION)
                            {
                                LoyaltyDetailsEntity loyaltyDetails = m_SessionManager.GetLoyaltyDetails(session);
                                    string membershipLevel = loyaltyDetails.MembershipLevel;
                                    m_AvailabilityDomain.RedemptionAvailability(multiDateSearch, roomSearch,
                                                                              hotelCode, chainCode,
                                                                              membershipLevel,
                                                                              multiRoomCalendarAvailabilitySearchData,
                                                                              true);
                            }
                            else
                            {
                                m_AvailabilityDomain.GeneralAvailability(multiDateSearch, roomSearch,
                                                                       hotelCode, chainCode,
                                                                       multiRoomCalendarAvailabilitySearchData, 
                                                                       true);
                            }
                        }
                    }
                }
            }
            catch (OWSException owsException)
            {
                availabilityCalendarItem.IsSearchDone = true;
                availabilityCalendarItem.IsRatesAvailable = false;

                switch (owsException.ErrCode)
                {
                    case OWSExceptionConstants.PROMOTION_CODE_INVALID:
                    case OWSExceptionConstants.PROPERTY_NOT_AVAILABLE:
                    case OWSExceptionConstants.INVALID_PROPERTY:
                    case OWSExceptionConstants.PROPERTY_RESTRICTED:
                    case OWSExceptionConstants.PRIOR_STAY:
                        AppLogger.LogInfoMessage(AppConstants.OWS_EXCEPTION +
                                                 " \n Availability calendar - OWS Error Message: " +
                                                 owsException.Message +
                                                 "\n OWS Stack trace: " + owsException.StackTrace);
                        break;
                    default:
                        AppLogger.LogCustomException(owsException,
                                                     AppConstants.OWS_EXCEPTION +
                                                     "Availability calendar - OWS Exception occured");
                        break;
                }
            }
            catch (Exception ex)
            {
                availabilityCalendarItem.IsSearchDone = true;
                availabilityCalendarItem.IsRatesAvailable = false;
                AppLogger.LogCustomException(ex, AppConstants.BUSINESS_EXCEPTION,
                                             "NON-OWS Exception occured in Availability calendar threading search");
            }
        }

        /// <summary>
        /// This method process the availability search response returned from service, and it's gets called from the domain object service call back.
        /// </summary>
        /// <param name="response"></param>
        /// <param name="multiRoomCalendarAvailabilitySearchData"></param>
        /// <param name="availability"></param>
        private void ProcessMultiRoomCalendarAvailabilitySearchResponse(AvailabilityResponse response, Hashtable multiRoomCalendarAvailabilitySearchData,
                                                                        int? availability)
        {
            Availability roomAvailability = Availability.UNKNOWN;
            if (availability.HasValue)
            {
                roomAvailability = (Availability)availability.Value;
            }
            HttpSessionState session = multiRoomCalendarAvailabilitySearchData[AppConstants.THREAD_SESSION] as HttpSessionState;
            HotelSearchEntity multiDateSearch =
                multiRoomCalendarAvailabilitySearchData[AppConstants.THREAD_HOTEL_SEARCH_ENTITY] as HotelSearchEntity;
            AvailabilityCalendarEntity availabilityCalendar =
                multiRoomCalendarAvailabilitySearchData[AppConstants.THREAD_AVAIL_CALENDAR] as AvailabilityCalendarEntity;
            AvailCalendarItemEntity availabilityCalendarItem =
                multiRoomCalendarAvailabilitySearchData[AppConstants.THREAD_AVAIL_CALENDAR_ITEM_ENTITY] as AvailCalendarItemEntity;
            IHotelDetails hotelDetails = null;
            IList<RoomStay> listRoomStay = new List<RoomStay>();
            try
            {
                availabilityCalendarItem.TotalSearchResponses++;
                availabilityCalendar.TotalMultiSearchResponses++;
                if (response != null && response.AvailResponseSegments != null &&
                                response.AvailResponseSegments.Length > 0 &&
                                response.AvailResponseSegments[0] != null &&
                                response.AvailResponseSegments[0].RoomStayList != null &&
                                response.AvailResponseSegments[0].RoomStayList.Length > 0 &&
                                response.AvailResponseSegments[0].RoomStayList[0] != null)
                {
                    if (availabilityCalendarItem.RoomStays == null)
                    {
                        availabilityCalendarItem.RoomStays = new List<object>();
                    }
                    availabilityCalendarItem.RoomStays.Add(response.AvailResponseSegments[0].RoomStayList[0]);
                }

                if ((availabilityCalendarItem.TotalSearchRequests == availabilityCalendarItem.TotalSearchResponses)
                    && (availabilityCalendarItem.RoomStays != null))
                {
                    foreach (object roomStay in availabilityCalendarItem.RoomStays)
                    {
                        listRoomStay.Add((RoomStay)roomStay);
                    }
                    string countryCode = listRoomStay[0].HotelContact.Addresses[0].countryCode;

                    ServiceProxies.Availability.HotelReference hotelReferece = listRoomStay[0].HotelReference;
                    HotelContact hotelContact = listRoomStay[0].HotelContact;

                    HotelDestination hotelDestination = GetHotelDestinationEntity(hotelReferece, hotelContact);

                    switch (multiDateSearch.SearchingType)
                    {
                        case SearchType.REDEMPTION:
                            LoyaltyDetailsEntity loyaltyDetails = m_SessionManager.GetLoyaltyDetails(session);
                            string membershipLevel = loyaltyDetails.MembershipLevel;

                            List<RateAward> collectionRateAwards = new List<RateAward>();
                            foreach (RoomStay roomStay in listRoomStay)
                            {
                                foreach (ServiceProxies.Availability.RoomRate roomRate in roomStay.RoomRates)
                                {
                                    {
                                        RateAward lstrateAwards = new RateAward();
                                        lstrateAwards.membershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;
                                        lstrateAwards.awardType = membershipLevel;
                                        lstrateAwards.resort = multiDateSearch.SelectedHotelCode;
                                        lstrateAwards.rateCode = roomRate.ratePlanCode;
                                        lstrateAwards.points_required = roomRate.TotalPoints;
                                        lstrateAwards.roomCategory = roomRate.roomTypeCode;
                                        collectionRateAwards.Add(lstrateAwards);
                                    }
                                }
                            }
                            RateAward[] rateAwards = collectionRateAwards.ToArray();
                            hotelDetails = new RedemptionHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                      rateAwards, multiDateSearch.NoOfNights);

                            break;
                        case SearchType.REGULAR:
                            if (null != multiDateSearch.CampaignCode)
                                hotelDetails = new PromotionHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                         multiDateSearch.CampaignCode,true);//Merchandising:R3:Display ordinary rates for unavailable promo rates 
                            else
                                hotelDetails = new RegularHotelDetails(hotelDestination, countryCode, listRoomStay);
                            break;
                        case SearchType.VOUCHER:
                            hotelDetails = new VoucherHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                   multiDateSearch.CampaignCode);
                            break;
                        case SearchType.BONUSCHEQUE:
                            hotelDetails = new BonusChequeHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                       multiDateSearch.ArrivalDate,
                                                                       multiDateSearch.DepartureDate);
                            break;
                        case SearchType.CORPORATE:
                            if (multiDateSearch.QualifyingType == AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
                            {
                                hotelDetails = new BlockCodeHotelDetails(hotelDestination, countryCode, listRoomStay,
                                                                         multiDateSearch);
                            }
                            else
                            {
                                hotelDetails = new NegotiatedHotelDetails(hotelDestination, countryCode,
                                                                          listRoomStay, multiDateSearch.CampaignCode);
                            }
                            break;
                    }
                    availabilityCalendarItem.HotelDetails = hotelDetails;
                }
            }
            catch (Exception e)
            {
                AppLogger.LogFatalException(e,"Exception occured in AvailabilityController - ProcessAvailabilityServiceResponse Method");
            }
            finally
            {
                if (roomAvailability == Availability.AVAILABLE)
                {
                    availabilityCalendarItem.IsSearchDone = true;
                    availabilityCalendarItem.IsRatesAvailable = true;
                }
                else
                {
                    availabilityCalendarItem.IsSearchDone = true;
                    availabilityCalendarItem.IsRatesAvailable = false;
                }
                if (availabilityCalendar.TotalMultiSearchRequests == availabilityCalendar.TotalMultiSearchResponses)
                {
                    availabilityCalendar.SearchState = SearchState.COMPLETED;
                }
            }
        }
        #endregion

        #endregion Select Rate Methods

        #region Alternate Hotel Methods

        #region AlternateSearchHotels

        /// <summary>
        /// Search for Alternate Hotels, takes the list of alternate hotels for the hotel being searched for
        /// for each alternate hotel reference the General Availablity OWS call is made and the corresponding
        /// IHotelDetails subtype of the SearchType is created and saved to the session.
        /// 
        /// The details saved to the session are read on the Select Hotel page and displayed as the alternate
        /// hotels to the user
        /// </summary>
        /// <param name="alternateHotelsList">
        /// List of Alternate Hotels References
        /// </param>
        public void AlternateSearchHotels(List<AlternativeHotelReference> alternateHotelsList)
        {
            AppLogger.LogInfoMessage("In AvailabilityController\\SearchAlternateHotels()");
          
            HttpSessionState session = System.Web.HttpContext.Current.Session;
            SetupInitialHotelDetails();

            foreach (AlternativeHotelReference alternateHotel in alternateHotelsList)
            {
                try
                {
                    string hotelCode = alternateHotel.OperaHotelId;
                    double alternateHotelDistance = alternateHotel.Distance;
                   
                    string distanceToCityCentreOfSearchedHotel = alternateHotel.DistanceToCityCentreOfSearchedHotel;

                    HotelSearchEntity alternateHotelSearchEntity = CreateAlternateHotelSearchEntity(hotelCode);
                    if (alternateHotelSearchEntity != null)
                    {
                        IHotelDetails hotelDetails = GetAlternateHotelDetails(alternateHotelSearchEntity);
                        if (hotelDetails != null)
                        {
                            hotelDetails.DistanceToCityCentreOfSearchedHotel = distanceToCityCentreOfSearchedHotel;

                            hotelDetails.AlternateHotelsDistance = alternateHotelDistance;
                            UpdateSession(hotelDetails, System.Web.HttpContext.Current.Session);
                        }
                    }
                }
                catch (OWSException owsException)
                {
                    switch (owsException.ErrCode)
                    {
                        case OWSExceptionConstants.PROMOTION_CODE_INVALID:
                        case OWSExceptionConstants.PROPERTY_NOT_AVAILABLE:
                        case OWSExceptionConstants.INVALID_PROPERTY:
                        case OWSExceptionConstants.PROPERTY_RESTRICTED:
                        case OWSExceptionConstants.PRIOR_STAY:
                            AppLogger.LogInfoMessage(AppConstants.OWS_EXCEPTION + " \n OWS Error Message: " +
                                                     owsException.Message +
                                                     "\n OWS Stack trace: " + owsException.StackTrace);
                            break;
                        default:
                            AppLogger.LogCustomException(owsException,
                                                         AppConstants.OWS_EXCEPTION +
                                                         "OWS Exception occured for Alternate hotel search.");
                            break;
                    }
                }
            }
        }

        #endregion

        #region CreateAlternateHotelSearchEntity

        /// <summary>
        /// Create the HotelsSearchEntity for the alternate hotels
        /// </summary>
        /// <param name="alternateHotelCode"> </param>
        /// <returns>HotelSearchEntity
        /// </returns>
        private HotelSearchEntity CreateAlternateHotelSearchEntity(string alternateHotelCode)
        {
            HotelSearchEntity alternateHotelSearchEntity = null;
            HotelSearchEntity orginalSearchHotel = m_SessionManager.SearchCriteria;
            if (orginalSearchHotel != null)
            {
                alternateHotelSearchEntity = new HotelSearchEntity();
                alternateHotelSearchEntity.SelectedHotelCode = alternateHotelCode;
                alternateHotelSearchEntity.ArrivalDate = orginalSearchHotel.ArrivalDate;
                alternateHotelSearchEntity.DepartureDate = orginalSearchHotel.DepartureDate;
                alternateHotelSearchEntity.NoOfNights = orginalSearchHotel.NoOfNights;
                alternateHotelSearchEntity.RoomsPerNight = orginalSearchHotel.RoomsPerNight;
                alternateHotelSearchEntity.AdultsPerRoom = orginalSearchHotel.AdultsPerRoom;
                alternateHotelSearchEntity.ChildrenPerRoom = orginalSearchHotel.ChildrenPerRoom;
                alternateHotelSearchEntity.ChildrenOccupancyPerRoom = orginalSearchHotel.ChildrenOccupancyPerRoom;
                alternateHotelSearchEntity.SearchingType = orginalSearchHotel.SearchingType;
                alternateHotelSearchEntity.CampaignCode = orginalSearchHotel.CampaignCode;
                alternateHotelSearchEntity.QualifyingType = orginalSearchHotel.QualifyingType;

                if (orginalSearchHotel.ListRooms != null && orginalSearchHotel.ListRooms.Count > 0)
                {
                    alternateHotelSearchEntity.ListRooms = new List<HotelSearchRoomEntity>();
                    HotelSearchRoomEntity alternateHotelSearchRoomEntity = null;
                    for (int i = 0; i < orginalSearchHotel.ListRooms.Count; i++)
                    {
                        alternateHotelSearchRoomEntity = new HotelSearchRoomEntity();
                        alternateHotelSearchRoomEntity.AdultsPerRoom = orginalSearchHotel.ListRooms[i].AdultsPerRoom;
                        alternateHotelSearchRoomEntity.ChildrenPerRoom = orginalSearchHotel.ListRooms[i].ChildrenPerRoom;
                        alternateHotelSearchRoomEntity.ChildrenOccupancyPerRoom =
                            orginalSearchHotel.ListRooms[i].ChildrenOccupancyPerRoom;

                        if (orginalSearchHotel.ListRooms[i].ChildrenDetails != null)
                        {
                            ChildrensDetailsEntity alternateChildrenDetails = new ChildrensDetailsEntity();
                            alternateChildrenDetails.AdultsPerRoom =
                                orginalSearchHotel.ListRooms[i].ChildrenDetails.AdultsPerRoom;
                            alternateChildrenDetails.ChildrensDetailsInText =
                                orginalSearchHotel.ListRooms[i].ChildrenDetails.ChildrensDetailsInText;

                            alternateChildrenDetails.ListChildren = new List<ChildEntity>();
                            for (int j = 0; j < orginalSearchHotel.ListRooms[i].ChildrenDetails.ListChildren.Count; j++)
                            {
                                ChildEntity alternateChild = new ChildEntity();
                                alternateChild.AccommodationString =
                                    orginalSearchHotel.ListRooms[i].ChildrenDetails.ListChildren[j].AccommodationString;
                                alternateChild.Age = orginalSearchHotel.ListRooms[i].ChildrenDetails.ListChildren[j].Age;
                                alternateChild.ChildAccommodationType =
                                    orginalSearchHotel.ListRooms[i].ChildrenDetails.ListChildren[j].
                                        ChildAccommodationType;

                                alternateChildrenDetails.ListChildren.Add(alternateChild);
                            }

                            alternateHotelSearchRoomEntity.ChildrenDetails = alternateChildrenDetails;
                        }

                        alternateHotelSearchEntity.ListRooms.Add(alternateHotelSearchRoomEntity);
                    }
                }
            }
            return alternateHotelSearchEntity;
        }

        #endregion

        #endregion Alternate Hotel Methods

        #region FetchAllDestinations

        /// <summary>
        /// It will fetch all destinations available in CDAL.
        /// </summary>
        /// <returns>The list of CityDestination, which will contain the matching cities and hotels</returns>
        public List<CityDestination> FetchAllDestinations()
        {
            List<CityDestination> cityList = new List<CityDestination>();
            cityList = m_ContentDataAccessManager.GetCityAndHotel(false);
            return cityList;
        }
        public List<CityDestination> FetchDestinationsFromTheLastSixMonthscityList()
        {
            List<CityDestination> cityList = new List<CityDestination>();
            cityList = m_ContentDataAccessManager.GetDestinationsFromTheLastSixMonthscityList(false);
            return cityList;
        }
        public List<Scandic.Scanweb.Core.CountryDestinatination> FetchAllCountryDestinationsForAutoSuggest(bool ignoreBookingVisibility, bool removeChildrens)
        {
            List<Scandic.Scanweb.Core.CountryDestinatination> countryList = new List<Scandic.Scanweb.Core.CountryDestinatination>();
            countryList = m_ContentDataAccessManager.GetCountryCityAndHotelForAutoSuggest(ignoreBookingVisibility, removeChildrens);
            return countryList;
        }

        /// <summary>
        /// It will fetch all destinations available in CDAL.
        /// </summary>
        /// <returns>The list of CityDestination, which will contain the matching cities and hotels</returns>
        public List<CityDestination> FetchAllDestinationsForAutoSuggest()
        {
            List<CityDestination> cityList = new List<CityDestination>();
            cityList = m_ContentDataAccessManager.GetCityAndHotelForAutoSuggest(false);
            return cityList;
        }

        /// <summary>
        /// Fetches all destinations with non bookings
        /// </summary>
        /// <returns>List of CityDestination</returns>
        public List<CityDestination> FetchAllDestinationsWithNonBookable()
        {
            List<CityDestination> cityList = new List<CityDestination>();
            cityList = m_ContentDataAccessManager.FetchAllDestinationsWithNonBookable(false);
            return cityList;
        }

        #endregion FetchAllDestinations

        #region FetchAllTransactions

        /// <summary>
        /// It will fetch all transactions available in CDAL.
        /// </summary>
        /// <returns>The list of transactions</returns>
        public List<Transaction> FetchAllTransactions()
        {
            List<Transaction> transactionList = new List<Transaction>();
            transactionList = m_ContentDataAccessManager.GetAllTransactions();
            return transactionList;
        }

        #endregion FetchAllTransactions

        #region Redemption/Loyalty Methods

        #region AuthenticateUser

        /// <summary>
        /// Method to fetch the Opera NameID by providing the Login and Pin, Used in the site to 
        /// login, Redemption booking login
        /// </summary>
        /// <param name="logIn"></param>
        /// <param name="password"></param>
        /// <returns>opera Id</returns>
        public string AuthenticateUser(string logIn, string password)
        {
            string operaID = m_AvailabilityDomain.AuthenticateUser(logIn, password);
            return operaID;
        }

        #endregion

        #region FetchGuestCardList

        /// <summary>
        /// The membership details of the Loyalty member, these details are passed to OWS.
        /// </summary>
        /// <param name="nameId">The Opera Name ID for the Loyalty membership number</param>
        /// <param name="membershipType">The Loyalty membership type</param>
        /// <returns>Loyalty member list</returns>
        public ArrayList FetchGuestCardList(string nameId, string membershipType)
        {
            return m_LoyalityDomain.FetchGuestCardList(nameId, membershipType);
        }

        #endregion

        #endregion Redemption/Loyalty Methods

        #region Other Methods

        #region GetBonusChequeValue

        /// <summary>
        /// This will return the bonus cheque value configured for a country in a year
        /// </summary>
        /// <param name="year">The year for which the bonus cheque needs to be calcuated</param>
        /// <param name="country">The Opera Country code</param>
        /// <param name="currencyCode">The Opera Currency Code</param>
        /// <returns>The Bouns cheque value configured for the corresponding country</returns>
        public double GetBonusChequeRate(int year, string country, string currencyCode)
        {
            return m_ContentDataAccessManager.GetBonusChequeRate(year, country, currencyCode);
        }

        #endregion

        #region GetHotelDestination

        /// <summary>
        /// Returns the HotelDestination object by reading the value from the CDAL
        /// If CDAL throws any exception then null is returned
        /// </summary>
        /// <param name="hotelOperaID">The Hotel Opera ID</param>
        /// <returns>The HotelDestination object as from the CDAL, null if CDAL throws any exception</returns>
        public HotelDestination GetHotelDestination(string hotelOperaID, string language)
        {
            HotelDestination hotel;
            try
            {
                hotel = m_ContentDataAccessManager.GetHotelByOperaID(hotelOperaID, language);
            }
            catch (ContentDataAccessException cdae)
            {
                hotel = null;
            }

            return hotel;
        }

        public HotelDestination GetHotelDestination(string hotelOperaID)
        {
            return GetHotelDestination(hotelOperaID, string.Empty);
        }


        #endregion

        #region GetCountryName

        /// <summary>
        /// Returns the string  by reading the value from the CDAL
        /// </summary>
        /// <param name="countryCode"> </param>
        /// <returns>The countryName  as from the CDAL, </returns>
        public string GetcountryNameByCountryCode(string countryCode)
        {
            string countryName = string.Empty;

            countryName = m_ContentDataAccessManager.GetCountryNamebyCountryID(countryCode);

            return countryName;
        }

        #endregion

        /// <summary>
        /// This will search all the city destinations by reading the GetAllDestinations from CDAL
        /// if there is a match found in the list then corresponding CityDesitnation is sent
        /// else if there is any exception in CDAL or no match is found then null is returned.
        /// </summary>
        /// <param name="cityOpearID">The City Opera ID</param>
        /// <returns>The matching CityDestination object, if there is no match or exception then null is returned</returns>
        public CityDestination GetCityDestination(string cityOpearID)
        {
            if (null == cityOpearID)
                return null;

            CityDestination cityDestination = null;
            try
            {
                List<CityDestination> cities = m_ContentDataAccessManager.GetCityAndHotel(false);

                if (null != cities)
                {
                    foreach (CityDestination city in cities)
                    {
                        if (0 == string.Compare(cityOpearID, city.OperaDestinationId, true))
                        {
                            cityDestination = city;
                        }
                    }
                }
            }
            catch (ContentDataAccessException cdae)
            {
                cityDestination = null;
            }

            return cityDestination;
        }

        #endregion

        #region Get Hotel name from CMS

        /// <summary>
        /// This method will search the Hotel Name from CMS against the given hotel code
        /// </summary>
        /// <param name="destinationCode">Hotel Code </param>
        /// <returns>Hotel Name</returns>
        public string GetHotelNameFromCMS(string destinationCode)
        {
            string hotelName = string.Empty;
            try
            {
                List<CityDestination> cityList = FetchAllDestinations();
                if ((cityList != null) && (cityList.Count > 0))
                {
                    hotelName = (from city in cityList
                                 from hotel in city.SearchedHotels
                                 where (hotel != null) && (hotel.OperaDestinationId == destinationCode)
                                 select hotel.Name).SingleOrDefault();
                }
                return hotelName;
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
                return hotelName;
            }
        }

        #endregion Get Hotel name from CMS

        #region Get City name from CMS

        /// <summary>
        /// This method will search the City Name from CMS against the given hotel code
        /// </summary>
        /// <param name="destinationCode">Hotel Code </param>
        /// <returns>Hotel Name</returns>
        public string GetCityNameFromCMS(string destinationCode, out string cityCode)
        {
            bool CityFound = false;
            string CityName = string.Empty;
            string CityCode = string.Empty;
            try
            {
                List<CityDestination> CityList = FetchAllDestinations();
                if (CityList != null && CityList.Count > 0)
                {
                    for (int cityCtr = 0; cityCtr < CityList.Count; cityCtr++)
                    {
                        List<HotelDestination> HotelList = CityList[cityCtr].SearchedHotels;
                        if (HotelList != null && HotelList.Count > 0)
                        {
                            for (int hotelCtr = 0; hotelCtr < HotelList.Count; hotelCtr++)
                            {
                                if (HotelList[hotelCtr].OperaDestinationId == destinationCode)
                                {
                                    CityName = CityList[cityCtr].Name;
                                    CityCode = CityList[cityCtr].OperaDestinationId;
                                    CityFound = true;
                                    break;
                                }
                            }
                        }
                        if (CityFound)
                            break;
                    }
                }
                cityCode = CityCode;
                return CityName;
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
                cityCode = CityCode;
                return CityName;
            }
        }

        #endregion Get City name from CMS

        #region GetTransactionNameFromCMS

        /// <summary>
        /// Fetches the Transaction name from CMS which is associated to the transactionCode.
        /// </summary>
        /// <param name="transactionCode">string</param>
        /// <returns>Transaction Name</returns>
        public string GetTransactionNameFromCMS(string transactionCode)
        {
            string transactionName = string.Empty;

            try
            {
                List<Transaction> transactionList = FetchAllTransactions();

                if (transactionList != null && transactionList.Count > 0)
                {
                    for (int transactionCtr = 0; transactionCtr < transactionList.Count; transactionCtr++)
                    {
                        if (transactionList[transactionCtr].OperaID == transactionCode)
                        {
                            transactionName = transactionList[transactionCtr].TransactionName;
                            break;
                        }
                    }
                }

                return transactionName;
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
                return transactionName;
            }
        }

        #endregion GetTransactionNameFromCMS

        #region GetTransactionDetailsFromCMS

        /// <summary>
        /// This fetches hotel name or transaction name from CMS which 
        /// is associated to the sourceCode parameter.
        /// </summary>
        /// <param name="sourceCode">string</param>
        /// <returns>string</returns>
        public string GetTransactionInfoFromCMS(string sourceCode)
        {
            string transactionDetails = string.Empty;
            HotelDestination hotelDetails = null;
            try
            {
                hotelDetails = ContentDataAccess.GetHotelByOperaID(sourceCode);
            }
            catch (ContentDataAccessException ex)
            {
                AppLogger.LogInfoMessage(string.Format(" -> Could not fetch information for hotelId - {0}", sourceCode));
            }  
            if (hotelDetails != null)
            {
                transactionDetails = string.Format("{0}, {1}", hotelDetails.Name, hotelDetails.City);
                    
                transactionDetails = !string.IsNullOrEmpty(hotelDetails.Name)
                                        ? string.Format("{0}, {1}", hotelDetails.Name, hotelDetails.City)
                                        : string.Empty;
            }

            if (transactionDetails == string.Empty)
            {
                transactionDetails = GetTransactionNameFromCMS(sourceCode);
            }

            return transactionDetails;
        }

        #endregion GetTransactionDetailsFromCMS

        #region FindAHotel Hotel ListingControl

        /// <summary>
        /// Makes the call to CMS to get all the List of hotelDestination
        /// </summary>
        /// <param name="searchCode"></param>
        /// <param name="hotelSearchType"> </param>
        public void SearchHotelsForFindAHotel(string searchCode, bool hotelSearchType)
        {
            AppLogger.LogInfoMessage("In AvailabilityController :: SearchHotelsForFindAHotel()");

            List<HotelDestination> hotelDestinations = new List<HotelDestination>();

            if (hotelSearchType)
            {
                HotelDestination hotelDestination = m_ContentDataAccessManager.GetHotelDestinationWithHotelName(searchCode);
                hotelDestinations.Add(hotelDestination);
            }
            else
            {
                hotelDestinations = m_ContentDataAccessManager.GetAllTypesofHotels(searchCode);
            }

            if (hotelDestinations != null && hotelDestinations.Count > 0)
            {
                foreach (HotelDestination hotelDestination in hotelDestinations)
                {
                    UpdateSessionForFindAHotelDestinations(hotelDestination);
                }
            }
        }

        /// <summary>
        /// Retrives the current session
        /// Updates the Destination by locking the session
        /// </summary>
        /// <param name="hotelDestination"></param>
        private void UpdateSessionForFindAHotelDestinations(HotelDestination hotelDestination)
        {
            if (hotelDestination != null)
            {
                List<HotelDestination> hotelDestinations = FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults();

                if (hotelDestinations == null)
                {
                    hotelDestinations = new List<HotelDestination>();
                }
                hotelDestinations.Add(hotelDestination);
                m_SessionManager.SetHotelDestinationResults(hotelDestinations);
            }
        }

        #endregion

        /// <summary>
        /// This method facilitates in creating search entities and calls corresponding methods on domain object.
        /// </summary>
        /// <param name="availabilitySearchData"></param>
        private void GeneralAvailabilitySearch(Hashtable availabilitySearchData)
        {
            HotelSearchEntity hotelSearch = availabilitySearchData[AppConstants.THREAD_HOTEL_SEARCH_ENTITY] as HotelSearchEntity;
            string chainCode = availabilitySearchData[AppConstants.THREAD_HOTEL_CHAIN_CODE] as string;
            HotelDestination hotelDestination = availabilitySearchData[AppConstants.THREAD_HOTEL_DESTINATION] as HotelDestination;
            HttpSessionState session = availabilitySearchData[AppConstants.THREAD_SESSION] as HttpSessionState;
            string countryCode = availabilitySearchData[AppConstants.THREAD_COUNTRY_CODE] as string;
            RateAward[] rateAwards = availabilitySearchData[AppConstants.THREAD_RATE_AWARD] as RateAward[];
            availabilitySearchData.Add(AppConstants.SEARCHSERVICE_CALLBACK, (ProcessServiceResponse)ProcessAvailabilitySearchResponse);

            //Merchandising:R3:Display ordinary rates for unavailable promo rates 
            availabilitySearchData.Add(AppConstants.IS_PROMO_CITY_FAIL,false);

            int hotelNo = (int)availabilitySearchData[AppConstants.THREAD_HOTEL_NO];
            int roomNo = (int)availabilitySearchData[AppConstants.THREAD_ROOM_NO];
            int roomCount = (int)availabilitySearchData[AppConstants.THREAD_ROOM_COUNT];

            string hotelCode = string.Empty;
            if (hotelDestination != null)
            {
                hotelCode = hotelDestination.OperaDestinationId;
            }
            Availability hotelAvailability = Availability.UNKNOWN;
            
            try
            {
                Hashtable availabilityTable = m_SessionManager.GetAvailabilityTable(session);

                for (int j = 0; j < roomCount; j++)
                {
                    string key = string.Format("{0}{1}", hotelNo, j);
                    if (availabilityTable.ContainsKey(key))
                    {
                        HotelRoomTableEntry availabilityEntry = availabilityTable[key] as HotelRoomTableEntry;
                        if (availabilityEntry.RoomAvailability == Availability.UNAVAILABLE)
                        {
                            hotelAvailability = Availability.UNAVAILABLE;
                            break;
                        }
                    }
                }

                if (hotelAvailability == Availability.UNAVAILABLE)
                {
                    throw new OWSException(string.Empty, string.Empty, new Hashtable(), true);
                }

                if (hotelSearch.ListRooms != null && hotelSearch.ListRooms.Count > 0 &&
                    roomNo < hotelSearch.ListRooms.Count)
                {
                    HotelSearchRoomEntity roomSearch = hotelSearch.ListRooms[roomNo];
                    if (roomSearch != null)
                    {
                        if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                        {
                            m_AvailabilityDomain.RedemptionAvailability(hotelSearch, roomSearch, hotelCode,
                                                                      chainCode,
                                                                      ((LoyaltyDetailsEntity)(session.Contents["BE_LOYALTY_DETAILS"])).MembershipLevel,
                                                                      availabilitySearchData,
                                                                      false);
                        }
                        else
                        {
                            m_AvailabilityDomain.GeneralAvailability(hotelSearch, roomSearch, hotelCode,
                                                                   chainCode, availabilitySearchData, false);
                        }
                    }
                }
            }
            catch (OWSException oe)
            {
                AppLogger.LogCustomException(oe, AppConstants.OWS_EXCEPTION,
                                             "Exception occured in AvailabilityController - GeneralAvailabilitySearch Method");
            }
            catch (Exception e)
            {
                AppLogger.LogFatalException(e,
                                            "Exception occured in AvailabilityController - GeneralAvailabilitySearch Method");
                operaStatus = e.Message;
            }
        }

       /// <summary>
       /// This method process the availability service response; it's called from domain service call back.
       /// </summary>
       /// <param name="response"></param>
       /// <param name="availabilitySearchData"></param>
       /// <param name="availability"></param>
       private void ProcessAvailabilitySearchResponse(AvailabilityResponse response, Hashtable availabilitySearchData, int? availability)
       {
           HttpSessionState session = availabilitySearchData[AppConstants.THREAD_SESSION] as HttpSessionState;
           bool isPromoFlowFail = false;
           try
           {
               if ((bool)availabilitySearchData[AppConstants.IS_PROMO_CITY_FAIL])
               {
                   isPromoFlowFail = true;
                   (availabilitySearchData[AppConstants.THREAD_SESSION] as HttpSessionState)[SessionConstants.IS_PROMO_NOT_VALID_FOR_CITY] = true;
                   ProcessPromoAvailabilityCount(availabilitySearchData);
                   return;
               }
               Availability roomAvailability = Availability.UNKNOWN;
               if (availability.HasValue)
               {
                   roomAvailability = (Availability) availability.Value;
               }
               RoomStay roomStay = null;
               HotelSearchEntity hotelSearch =
                   availabilitySearchData[AppConstants.THREAD_HOTEL_SEARCH_ENTITY] as HotelSearchEntity;
               string chainCode = availabilitySearchData[AppConstants.THREAD_HOTEL_CHAIN_CODE] as string;
               HotelDestination hotelDestination =
                   availabilitySearchData[AppConstants.THREAD_HOTEL_DESTINATION] as HotelDestination;
              
               string countryCode = availabilitySearchData[AppConstants.THREAD_COUNTRY_CODE] as string;
               RateAward[] rateAwards = availabilitySearchData[AppConstants.THREAD_RATE_AWARD] as RateAward[];
               int hotelNo = (int) availabilitySearchData[AppConstants.THREAD_HOTEL_NO];
               int roomNo = (int) availabilitySearchData[AppConstants.THREAD_ROOM_NO];
               int hotelCount = (int) availabilitySearchData[AppConstants.THREAD_HOTEL_COUNT];
               int roomCount = (int) availabilitySearchData[AppConstants.THREAD_ROOM_COUNT];
               if (response != null && roomAvailability == Availability.AVAILABLE && 
                   response.AvailResponseSegments != null && response.AvailResponseSegments.Length >0)
               {
                   roomStay = response.AvailResponseSegments[0].RoomStayList[0];
               }
               if (hotelSearch.SearchingType == SearchType.REDEMPTION)
               {
                   List<RateAward> collectionRateAwards = new List<RateAward>();
                   if (roomStay != null)
                   {
                       foreach (RoomRate roomRate in roomStay.RoomRates)
                       {
                           string hotelCode = hotelDestination.OperaDestinationId;
                           RateAward lstrateAwards = new RateAward();
                           lstrateAwards.membershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;
                           lstrateAwards.awardType =
                               ((LoyaltyDetailsEntity) (session.Contents["BE_LOYALTY_DETAILS"])).MembershipLevel;
                           lstrateAwards.resort = hotelCode;
                           lstrateAwards.rateCode = roomRate.ratePlanCode;
                           lstrateAwards.points_required = roomRate.TotalPoints;
                           lstrateAwards.roomCategory = roomRate.roomTypeCode;
                           collectionRateAwards.Add(lstrateAwards);
                       }
                       rateAwards = collectionRateAwards.ToArray();
                   }
               }
               AddHotelDetails(hotelSearch, hotelDestination, roomStay, hotelNo, roomNo, countryCode, rateAwards,
                               session, roomAvailability, roomCount);
           }
           catch (Exception e)
           {
               AppLogger.LogFatalException(e,
                                           "Exception occured in AvailabilityController - ProcessAvailabilityServiceResponse Method");
           }
           finally
           {
               if (!isPromoFlowFail)
                   IncrementGeneralAvailabilityCounter(session);
           }
       }

       //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
       private void ProcessPromoAvailabilityCount(Hashtable availabilitySearchData)
       {
           HttpSessionState session = availabilitySearchData[AppConstants.THREAD_SESSION] as HttpSessionState;
           int count = m_SessionManager.GetPromoGeneralAvailableHotels(session);
           m_SessionManager.SetPromoGeneralAvailableHotels(session, count + 1);
       }

       public bool GetCityTripAdvisorFlag(string OPId)
       {
           return ContentDataAccess.GetCityTripAdvisorFlag(OPId);
       }


    }

    /// <summary>
    /// Holds different availability types.
    /// </summary>
    public enum Availability
    {
        UNKNOWN = -1,
        UNAVAILABLE = 0,
        AVAILABLE = 1
    }

    /// <summary>
    /// Contains members of HotelRoomTableEntry
    /// </summary>
    public class HotelRoomTableEntry
    {
        private Availability roomAvailability = Availability.UNKNOWN;
        public Availability RoomAvailability
        {
            get { return roomAvailability; }
            set { roomAvailability = value; }
        }
        /// <summary>
        /// Gets/Sets RoomStay
        /// </summary>
        public RoomStay RoomStay { get; set; }
    }

}