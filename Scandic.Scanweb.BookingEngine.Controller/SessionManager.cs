﻿//  Description					: Session manager acts as a contract to access static     //
//								  session wrapper objects, and it enables you to decouple // 
//                                static methods that are getting called from controllers //
//----------------------------------------------------------------------------------------//
//  Author						: Sapient                                            	  //
//  Author email id				:                           							  //
//  Creation Date				: August 22nd, 2012                                       //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
// 	Last Modified Date			:                                                         //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using System.Web.SessionState;
using System.Collections;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// Session manager acts as a contract to access static session wrapper objects, 
    /// and it enables you to decouple static methods that are getting called from controllers.
    /// </summary>
    public class SessionManager : ISessionManager
    {
        /// <summary>
        /// The Hotel Room Rate object containing the details of the rate of
        /// the room selected by the user.
        /// The values are stored in a HotelRoomRateEntity object.
        /// The session object is updated in the code behind of the Select Rate user controls
        /// </summary>
        public bool AltCityHotelsSearchDone { 
            get { return AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone; }
            set { AlternateCityHotelsSearchSessionWrapper.AltCityHotelsSearchDone = value; }
        }

        /// <summary>
        /// This property will contain the total of the number of hotels returned
        /// by the Regional availability search done. 
        /// 
        /// This value will be compared against the total number of General Availability
        /// search results done dynamically. To check if the browser has to do
        /// request for further hotels
        /// </summary>
        public int TotalRegionalAvailableHotels
        {
            get { return TotalRegionalAvailableHotelsSessionWrapper.TotalRegionalAvailableHotels; }
            set { TotalRegionalAvailableHotelsSessionWrapper.TotalRegionalAvailableHotels = value; }
        }

        /// <summary>
        /// Sets availability table
        /// </summary>
        /// <param name="session"></param>
        /// <param name="availabilityTable"></param>
        public void SetAvailabilityTable(HttpSessionState session, Hashtable availabilityTable)
        {
            BookingEngineSessionWrapper.SetAvailabilityTable(session, availabilityTable);
        }

        /// <summary>
        /// This property will contain the total of the number of General availabilty
        /// searches are done through thread and values are returned from OWS. 
        /// 
        /// This value will be compared against the total number of Regional availabilty
        /// results retured, To check if the browser has to do request for further hotels
        /// </summary>
        public int TotalGeneralAvailableHotels
        {
            get { return TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels; }
            set { TotalGeneralAvailableHotelsSessionWrapper.TotalGeneralAvailableHotels = value; }
        }

        /// <summary>
        /// This would store true if no hotels are available for Redemption search
        /// </summary>
        public bool NoRedemptionHotelsAvailable
        {
            get { return RedemptionHotelsAvailableSessionWrapper.NoRedemptionHotelsAvailable; }
            set { RedemptionHotelsAvailableSessionWrapper.NoRedemptionHotelsAvailable = value; }
        }

        /// <summary>
        /// Gets availability table
        /// </summary>
        public Hashtable GetAvailabilityTable(HttpSessionState session)
        {
            return BookingEngineSessionWrapper.GetAvailabilityTable(session);
        }

        /// <summary>
        /// The List of hotels avialble for the criteria user has searched for
        /// The Session object is updated in the Controller as the hotels
        /// are returned from the General Availabilty searches which are called
        /// through multithreading Thread pool
        /// </summary>
        public List<IHotelDetails> HotelResults
        {
            get { return HotelResultsSessionWrapper.HotelResults; }
            set { HotelResultsSessionWrapper.HotelResults = value; }
        }

        /// <summary>
        /// This list contains the hotel details which are required by the
        /// Google map displayed on the Select a hotel page.
        /// These details are picked up by the GoogleMap user control from 
        /// the session and the hotel details are displayed accordingly
        /// </summary>
        public List<Dictionary<string, object>> GoogleMapResults
        {
            get { return GoogleMapHotelResultsSessionWrapper.GoogleMapResults; }
            set { GoogleMapHotelResultsSessionWrapper.GoogleMapResults = value; }
        }

        /// <summary>
        /// Gets hotel results
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public List<IHotelDetails> GetHotelResults(HttpSessionState session)
        {
            return HotelResultsSessionWrapper.GetHotelResults(session);
        }

        /// <summary>
        /// Sets hotel results
        /// </summary>
        /// <param name="session"></param>
        /// <param name="hotels"></param>
        public void SetHotelResults(HttpSessionState session, List<IHotelDetails> hotels)
        {
            HotelResultsSessionWrapper.SetHotelResults(session, hotels);
        }

        /// <summary>
        /// Gets google map results
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetGoogleMapResults(HttpSessionState session)
        {
            return GoogleMapHotelResultsSessionWrapper.GetGoogleMapResults(session);
        }

        /// <summary>
        /// Sets google map results
        /// </summary>
        /// <param name="session"></param>
        /// <param name="value"></param>
        public void SetGoogleMapResults(HttpSessionState session, List<Dictionary<string, object>> value)
        {
            GoogleMapHotelResultsSessionWrapper.SetGoogleMapResults(session, value);
        }

        /// <summary>
        /// GetTotalGeneralAvailableHotels
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public int GetTotalGeneralAvailableHotels(HttpSessionState session)
        {
            return TotalGeneralAvailableHotelsSessionWrapper.GetTotalGeneralAvailableHotels(session);
        }


        /// <summary>
        /// SetTotalGeneralAvailableHotels
        /// </summary>
        /// <param name="session"></param>
        /// <param name="hotels"></param>
        public void SetTotalGeneralAvailableHotels(HttpSessionState session, int hotels)
        {
          TotalGeneralAvailableHotelsSessionWrapper.SetTotalGeneralAvailableHotels(session, hotels);
        }


        /// <summary>
        /// The Hotel Room Rate object containing the details of the rate of
        /// the room selected by the user.
        /// The values are stored in a HotelRoomRateEntity object.
        /// The session object is updated in the code behind of the Select Rate user controls
        /// </summary>
        public IList<RoomStay> ListRoomStay
        {
            get { return HotelRoomRateSessionWrapper.ListRoomStay; }
            set { HotelRoomRateSessionWrapper.ListRoomStay = value; }
        }


        /// <summary>
        /// The Loyalty details object containing the user name and the NameID
        /// of the Loyalty user after the user is logged into the system.
        /// This object existence in the session tells whether the user is 
        /// logged into the system or not
        /// </summary>
        public LoyaltyDetailsEntity LoyaltyDetails
        {
            get { return LoyaltyDetailsSessionWrapper.LoyaltyDetails; }
            set { LoyaltyDetailsSessionWrapper.LoyaltyDetails = value; }
        }

        /// <summary>
        /// Gets/Sets IsComboReservation
        /// </summary>
        public bool IsComboReservation
        {
            get { return HygieneSessionWrapper.IsComboReservation; }
            set { HygieneSessionWrapper.IsComboReservation = value; }
        }

        /// Gets loyalty details.
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public LoyaltyDetailsEntity GetLoyaltyDetails(HttpSessionState session)
        {
            return LoyaltyDetailsSessionWrapper.GetLoyaltyDetails(session);
        }

        /// <summary>
        /// The Hotel Search Criteria object containing the details
        /// the user has searched for.
        /// The values are stored in a HotelSearchEntity object.
        /// The session object is updated in the code behind of the search user controls
        /// </summary>
        public HotelSearchEntity SearchCriteria
        {
            get { return SearchCriteriaSessionWrapper.SearchCriteria; }
            set { SearchCriteriaSessionWrapper.SearchCriteria = value; }
        }


        /// <summary>
        /// Gets the HotelDestination List
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public List<HotelDestination> GetHotelDestinationResults()
        {
            return FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults();
        }

        /// <summary>
        /// Sets the HotelDestination List
        /// </summary>
        /// <param name="session"></param>
        /// <param name="hotels"></param>
        public void SetHotelDestinationResults(List<HotelDestination> hotels)
        {
            FindAHotelSessionVariablesSessionWrapper.SetHotelDestinationResults(hotels);
        }

        /// <summary>
        /// Session Variable to indicate the Modify Check box in checked in booking details page;
        /// this is to implement the Update credit card only when the user have checked the Modify check box.
        /// </summary>
        /// <remarks>
        /// Do not update profile with the Credit Card information, if the user has not opted for 'Update Profile' option
        /// </remarks>
        public bool UpdateProfileBookingInfo
        {
            get { return BookingEngineSessionWrapper.UpdateProfileBookingInfo; }
            set { BookingEngineSessionWrapper.UpdateProfileBookingInfo = value; }
        }


        /// <summary>
        /// The Guest Booking Info object contains the details of the booking information
        /// of the guest.
        /// The values are stored in a GuestInformationEntity object.
        /// The session object is updated in the code behind of the Booking Details user controls
        /// </summary>
        public GuestInformationEntity GuestBookingInformation
        {
            get { return GuestBookingInformationSessionWrapper.GuestBookingInformation; }
            set { GuestBookingInformationSessionWrapper.GuestBookingInformation = value; }
        }

        /// <summary>
        /// Returns true if the user is logged in to the system already
        /// else returns false
        /// 
        /// The logic is to check if the LoyaltyDetailsEntity is already in session return true
        /// if the value is not already saved into the session false is returned
        /// </summary>
        public bool UserLoggedIn
        {
            get { return UserLoggedInSessionWrapper.UserLoggedIn; }
            set { UserLoggedInSessionWrapper.UserLoggedIn = value; }
        }

        /// <summary>
        /// The User profile Info object contains the details of the User information
        /// The values are stored in a UserProfileEntity object.
        /// The session object is updated in the code behind of the Enroll user controls
        /// </summary>
        public UserProfileEntity UserProfileInformation
        {
            get { return UserProfileInformationSessionWrapper.UserProfileInformation; }
            set { UserProfileInformationSessionWrapper.UserProfileInformation = value; }
        }


        /// <summary>
        /// Session Variable to indicate as to Enable/Disable Bed type Preference
        /// </summary>
        public bool DirectlyModifyContactDetails
        {
            get { return BookingEngineSessionWrapper.DirectlyModifyContactDetails; }
            set { BookingEngineSessionWrapper.DirectlyModifyContactDetails = value; }
        }

        /// <summary>
        /// Get/Set Booking details Entity used during Modify/Cancel Booking
        /// </summary>
        public BookingDetailsEntity BookingDetails
        {
            get { return BookingEngineSessionWrapper.BookingDetails; }
            set { BookingEngineSessionWrapper.BookingDetails = value; }
        }

        /// <summary>
        /// Method returns the BookingDetails for respective booking from the active booking details
        /// Author: Ruman Khan
        /// </summary>
        /// <param name="legNumber"></param>
        /// <param name="reservationNumber"></param>
        /// <returns></returns>
        public BookingDetailsEntity GetLegBookingDetails(string legNumber, string reservationNumber)
        {
            return BookingEngineSessionWrapper.GetLegBookingDetails(legNumber, reservationNumber);
        }


        /// <summary>
        /// Added Get property to fetch the ActiveBookingDetails derived from the AllBookingDetails
        /// Author: Ruman Khan
        /// </summary>
        public List<BookingDetailsEntity> ActiveBookingDetails
        {
            get { return BookingEngineSessionWrapper.ActiveBookingDetails; }
        }

        //Merchandising:R3:Display ordinary rates for unavailable promo rates 
        public IHotelDetails HotelDetails
        {
            get { return HotelResultsSessionWrapper.HotelDetails; }
            set { HotelResultsSessionWrapper.HotelDetails = value; }
        }
        public bool IsDestinationAlternateFlow
        {
            get { return AlternateCityHotelsSearchSessionWrapper.IsDestinationAlternateFlow; }
            set { AlternateCityHotelsSearchSessionWrapper.IsDestinationAlternateFlow = value; }
        }
        public int TotalRegionalAltCityHotelCount
        {
            get { return AlternateCityHotelsSearchSessionWrapper.TotalRegionalAltCityHotelCount; }
            set { AlternateCityHotelsSearchSessionWrapper.TotalRegionalAltCityHotelCount = value; }
        }
        public List<HotelDestination> CityAltHotelDetails
        {
            get { return AlternateCityHotelsSearchSessionWrapper.CityAltHotelDetails; }
            set { AlternateCityHotelsSearchSessionWrapper.CityAltHotelDetails = value; }
        }
        public int PromoGeneralAvailableHotels
        {
            get { return TotalGeneralAvailableHotelsSessionWrapper.PromoGeneralAvailableHotels; }
            set { TotalGeneralAvailableHotelsSessionWrapper.PromoGeneralAvailableHotels = value; }
        }
        public int GetPromoGeneralAvailableHotels(HttpSessionState session)
        {
            return TotalGeneralAvailableHotelsSessionWrapper.GetPromoGeneralAvailableHotels(session);
        }
        public void SetPromoGeneralAvailableHotels(HttpSessionState session, int hotels)
        {
            TotalGeneralAvailableHotelsSessionWrapper.SetPromoGeneralAvailableHotels(session, hotels);
        }
        public int GetTotalRegionalAvailableHotels(HttpSessionState session)
        {
            return TotalGeneralAvailableHotelsSessionWrapper.GetTotalGeneralAvailableHotels(session);
        }
    }
}
