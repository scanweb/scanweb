//  Description					:   InformationController                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.DomainContracts;
#endregion

namespace Scandic.Scanweb.BookingEngine.Controller
{
    public class InformationController
    {
        #region Constructor
        /// <summary>
        /// Constructor, which accepts instances of all required domain objects.
        /// </summary>
        /// <param name="informationDomainObj"></param>
        public InformationController(IInformationDomain informationDomainObj)
        {
            informationDomain = informationDomainObj;
        }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public InformationController()
        {
            informationDomain = new InformationDomain();
        }
        #endregion

        #region Domian properties
        private IInformationDomain informationDomain = null;

        /// <summary>
        /// Gets m_InformationDomain
        /// </summary>
        private IInformationDomain m_InformationDomain
        {
            get
            {
                return informationDomain;
            }
        }
        #endregion

        /// <summary>
        /// Get the Currency Value
        /// </summary>
        /// <param name="sourceCurrencyCode">
        /// Source Currency Code
        /// </param>
        /// <param name="destinationCurrencyCode">
        /// Destination Currency Code
        /// </param>
        /// <param name="value">
        /// Value to be converted
        /// </param>
        /// <returns>
        /// Converted value
        /// </returns>
        /// <remarks>
        /// This changes has been added for CR-4(Currency Converter) - Release 1.3
        /// </remarks>
        public double GetCurrency(string sourceCurrencyCode, string destinationCurrencyCode, double value)
        {
            return m_InformationDomain.GetCurrency(sourceCurrencyCode, destinationCurrencyCode, value);
        }

        /// <summary>
        /// Get the list of Membership Types 
        /// </summary>
        /// <remarks>
        /// </remarks>
        public Dictionary<string, string> GetMembershipTypes()
        {
            return m_InformationDomain.GetMembershipTypes();
        }

        public Dictionary<string, string> GetUserInterestTypes()
        {
            return m_InformationDomain.GetUserInterestTypes();
        }
        /// <summary>
        /// Get the list of Title Names 
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetConfiguredTitles(string languageCode)
        {
            return m_InformationDomain.GetConfiguredTitles(languageCode);
        }

        public string GetHotelCountryCode(string HotelCode)
        {
            return m_InformationDomain.GetHotelCountryCode(HotelCode);
        }
    }
}