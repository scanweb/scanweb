//  Description					: Controller class for Reservation OWS Calls.			  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Himansu Senapati / Santhosh Yamsani / Shankar Dasgutpa  //
//  Author email id				:                           							  //
//  Creation Date				: 05th November  2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces

using System;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Reservation;
using ServiceProxies = Scandic.Scanweb.BookingEngine.ServiceProxies;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using System.Linq;

#endregion

    #region Scandic Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Controller
{
    /// <summary>
    /// Controller class for Reservation functionality
    /// </summary>
    public class ReservationController
    {
        #region Constructor
        /// <summary>
        /// Constructor, which accepts instances of all required domain objects.
        /// </summary>
        /// <param name="reservationDomainObj"></param>
        /// <param name="loyaltyDomainObj"></param>
        /// <param name="informationDomainObj"></param>
        /// <param name="sessionManagerObj"></param>
        public ReservationController(IReservationDomain reservationDomainObj, ILoyaltyDomain loyaltyDomainObj,
                                     IInformationDomain informationDomainObj, ISessionManager sessionManagerObj)
        {
            reservationDomain = reservationDomainObj;
            loyaltyDomain = loyaltyDomainObj;
            informationDomain = informationDomainObj;
            sessionManager = sessionManagerObj;
        }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public ReservationController()
        {
            reservationDomain = new ReservationDomain();
            loyaltyDomain = new LoyaltyDomain();
            informationDomain = new InformationDomain();
            sessionManager = new SessionManager();
        }
        #endregion

        #region Domain Properties

        private IReservationDomain reservationDomain = null;

        /// <summary>
        /// Gets m_ReservationDomain
        /// </summary>
        private IReservationDomain m_ReservationDomain
        {
            get
            {
                return reservationDomain;
            }
        }

        private ILoyaltyDomain loyaltyDomain = null;

        /// <summary>
        /// Gets m_LoyaltyDomain
        /// </summary>
        private ILoyaltyDomain m_LoyaltyDomain
        {
            get
            {
                return loyaltyDomain;
            }
        }

        private IInformationDomain informationDomain = null;

        /// <summary>
        /// Gets m_LoyaltyDomain
        /// </summary>
        private IInformationDomain m_InformationDomain
        {
            get
            {
                return informationDomain;
            }
        }
        #endregion

        #region Session Manager

        private ISessionManager sessionManager = null;
        /// <summary>
        /// Gets m_SessionManager
        /// </summary>
        private ISessionManager m_SessionManager
        {
            get
            {
                return sessionManager;
            }
        }

        #endregion 

        #region Booking

        /// <summary>
        /// Provides CreateBooking calls to make the reservation.
        /// </summary>
        /// <param name="hotelSearchInfoEntity"></param>
        /// <param name="roomSearch"></param>
        /// <param name="roomRateInfoEntity"></param>
        /// <param name="guestInfoEntity"></param>
        /// <param name="reservationNumber"></param>
        /// <param name="sourceCode"></param>
        /// <param name="createdNameID"></param>
        /// <param name="isSessionBooking"></param>
        /// <param name="partnerID"></param>
        /// <returns>Booking Sorted List</returns>
        public SortedList<string, string> CreateBooking(HotelSearchEntity hotelSearchInfoEntity,
                                                        HotelSearchRoomEntity roomSearch,
                                                        HotelRoomRateEntity roomRateInfoEntity,
                                                        GuestInformationEntity guestInfoEntity, string reservationNumber,
                                                        string sourceCode, out string createdNameID,
                                                        bool isSessionBooking, string partnerID)
        {
            SortedList<string, string> legConfirmationMap =
                m_ReservationDomain.MakeReservation(hotelSearchInfoEntity, roomSearch, roomRateInfoEntity, guestInfoEntity,
                                                  reservationNumber, sourceCode, out createdNameID, isSessionBooking,
                                                  partnerID);

            if (legConfirmationMap != null && legConfirmationMap.Count > 0)
            {
                if (guestInfoEntity.GuestAccountNumber == string.Empty
                    ||
                    (
                        !guestInfoEntity.IsValidFGPNumber
                        && (!m_SessionManager.UpdateProfileBookingInfo)
                    )
                    )
                {
                    if (guestInfoEntity.EmailDetails != null && !guestInfoEntity.IsRewardNightGift)
                    {
                        m_LoyaltyDomain.InsertEmail(guestInfoEntity.EmailDetails.EmailID, createdNameID);
                    }
                    if (guestInfoEntity.Mobile != null && !guestInfoEntity.IsRewardNightGift)
                    {
                        m_LoyaltyDomain.InsertPhone(createdNameID, guestInfoEntity.Mobile.Number,
                                                  PhoneContants.PHONETYPE_MOBILE, PhoneContants.PHONEROLE);
                    }

                    if (!m_SessionManager.UserLoggedIn)
                    {
                        UserPreferenceEntity[] userPreferenceList =
                            ConvertToUserPreferenceEntityList(guestInfoEntity.SpecialRequests);
                        if ((userPreferenceList != null) && (userPreferenceList.Length > 0))
                        {
                            m_LoyaltyDomain.UpdatePreferencesListUserProfile(createdNameID, userPreferenceList, null);
                        }
                    }
                }

                string legNumber = legConfirmationMap.Keys[0];
                string confirmationNumber = legConfirmationMap[legNumber];

                m_ReservationDomain.AddChildrenAccommodations(confirmationNumber, legNumber,
                                                            hotelSearchInfoEntity.SelectedHotelCode, guestInfoEntity);
            }
            return legConfirmationMap;
        }

        /// <summary>
        /// Provides Modify Booking calls to change the reservation.
        /// </summary>
        /// <param name="hotelSearch">
        /// HotelSearch Entity
        /// </param>
        /// <param name="roomSearch">
        /// Room Search Entity
        /// </param>
        /// <param name="roomRate">
        /// Hotel Room Rate Entity
        /// </param>
        /// <param name="guestInformation">
        /// Guest Information Entity
        /// </param>
        /// <param name="prevReservationNumber"></param>
        /// <param name="sourceCode"></param>
        /// <param name="createdNameID"></param>
        /// <param name="isSessionBooking"></param>
        /// <returns>
        /// Booking Confirmation Number
        /// </returns>
        public SortedList<string, string> ModifyBooking(
            HotelSearchEntity hotelSearch, HotelSearchRoomEntity roomSearch, HotelRoomRateEntity roomRate,
            GuestInformationEntity guestInformation, string prevReservationNumber, string sourceCode,
            out string createdNameID, bool isSessionBooking)
        {
            string currentLegNumber = guestInformation.LegNumber;
            string confNumber = guestInformation.ReservationNumber;
            BookingDetailsEntity bookingDetails = m_SessionManager.GetLegBookingDetails(currentLegNumber,
                                                                                              confNumber);
            GuestInformationEntity previousGuestInfoEntity = bookingDetails != null
                                                                 ? bookingDetails.GuestInformation
                                                                 : null;
            if (!m_SessionManager.DirectlyModifyContactDetails)
            {
                
                if (m_SessionManager.BookingDetails != null && m_SessionManager.BookingDetails.GuestInformation != null)
                {

                    m_ReservationDomain.DeleteChildAccommodations(confNumber, currentLegNumber,
                                                                hotelSearch.SelectedHotelCode,
                                                                previousGuestInfoEntity);
                }
            }

            SortedList<string, string> legConfirmationMap =
                m_ReservationDomain.ModifyReservation(hotelSearch, roomSearch, roomRate, guestInformation,
                                                    prevReservationNumber, sourceCode, out createdNameID);

            if ((legConfirmationMap != null) && (legConfirmationMap.Count > 0))
            {
                EmailDetailsEntity emailDetails = guestInformation.EmailDetails;
                if (emailDetails != null)
                {
                    if ((emailDetails.EmailOperaID <= 0) || (guestInformation.NameID != createdNameID))
                    {
                        m_LoyaltyDomain.InsertEmail(emailDetails.EmailID, createdNameID);
                    }
                    else if(previousGuestInfoEntity != null && !string.Equals(previousGuestInfoEntity.EmailDetails.EmailID, guestInformation.EmailDetails.EmailID))
                    {
                        m_LoyaltyDomain.UpdateEmail(createdNameID, guestInformation.EmailDetails.EmailOperaID,
                                                  guestInformation.EmailDetails.EmailID, true);
                        m_LoyaltyDomain.UpdateEmail(createdNameID, guestInformation.NonBusinessEmailDetails.EmailOperaID,
                                                  guestInformation.NonBusinessEmailDetails.EmailID, false);
                    }
                    else if (previousGuestInfoEntity == null)
                    {
                        m_LoyaltyDomain.UpdateEmail(createdNameID, guestInformation.EmailDetails.EmailOperaID,
                                                  guestInformation.EmailDetails.EmailID, true);
                        m_LoyaltyDomain.UpdateEmail(createdNameID, guestInformation.NonBusinessEmailDetails.EmailOperaID,
                                                  guestInformation.NonBusinessEmailDetails.EmailID, false);
                    }
                }

                PhoneDetailsEntity mobilePhoneDetails = guestInformation.Mobile;
                if (mobilePhoneDetails != null)
                {
                    if ((mobilePhoneDetails.OperaId <= 0) || (guestInformation.NameID != createdNameID))
                    {
                        m_LoyaltyDomain.InsertPhone(createdNameID, mobilePhoneDetails.Number,
                                                  PhoneContants.PHONETYPE_MOBILE, PhoneContants.PHONEROLE);
                    }
                    else if(previousGuestInfoEntity != null && !string.Equals(previousGuestInfoEntity.Mobile.Number, guestInformation.Mobile.Number))
                    {
                        m_LoyaltyDomain.UpdatePhone(createdNameID, mobilePhoneDetails.OperaId, mobilePhoneDetails.Number,
                                                  PhoneContants.PHONETYPE_MOBILE, PhoneContants.PHONEROLE);
                    }
                    else if (previousGuestInfoEntity == null)
                    {
                        m_LoyaltyDomain.UpdatePhone(createdNameID, mobilePhoneDetails.OperaId, mobilePhoneDetails.Number,
                                                  PhoneContants.PHONETYPE_MOBILE, PhoneContants.PHONEROLE);
                    }
                }

                UserProfileEntity userProfileEntity = CreateUserProfileAddressEntity(guestInformation);
                m_LoyaltyDomain.PrimaryLanguageID = guestInformation.PreferredLanguage;
                if (userProfileEntity != null)
                {
                    if (userProfileEntity.AddressOperaID > 0)
                    {
                        if(previousGuestInfoEntity!= null && !string.Equals(previousGuestInfoEntity.Country, userProfileEntity.Country))
                            m_LoyaltyDomain.UpdateAddress(userProfileEntity);
                        else if(previousGuestInfoEntity == null )
                            m_LoyaltyDomain.UpdateAddress(userProfileEntity);
                    }
                    else
                    {
                        UserProfileEntity fetchUserEntity =
                            m_LoyaltyDomain.FetchAddressUserProfile(createdNameID);

                        if (fetchUserEntity != null)
                        {
                            userProfileEntity.AddressOperaID = fetchUserEntity.AddressOperaID;
                            userProfileEntity.AddressType = fetchUserEntity.AddressType;
                            m_LoyaltyDomain.UpdateAddress(userProfileEntity);
                        }
                    }
                }

                if (!m_SessionManager.DirectlyModifyContactDetails)
                {
                    string legNumber = legConfirmationMap.Keys[0];
                    string confirmationNumber = legConfirmationMap[legNumber];
                    m_ReservationDomain.AddChildrenAccommodations(confirmationNumber, legNumber,
                                                                hotelSearch.SelectedHotelCode,
                                                                guestInformation);
                }
            }
            return legConfirmationMap;
        }

        /// <summary>
        /// Fetch the Booking for the specified Reservation Number        
        /// </summary>
        /// <param name="reservationNumber">
        /// Reservation Number
        /// </param>
        /// <param name="legNumber">
        /// Leg Number
        /// </param>
        /// <returns>
        /// BookingDetailsEntity
        /// </returns>
        public BookingDetailsEntity FetchBooking(string reservationNumber, string legNumber)
        {
            BookingDetailsEntity bookingEntities = FetchBooking(reservationNumber, legNumber, null);
            return bookingEntities;
        }


        /// <summary>
        /// Fetch the Booking for the specified Reservation Number        
        /// </summary>
        /// <param name="reservationNumber">
        /// Reservation Number
        /// </param>
        /// <param name="legNumber">
        /// Leg Number
        /// </param>
        /// /// <param name="hotelCountryCode">
        /// Hotel CountryCode
        /// </param>
        /// <returns>
        /// BookingDetailsEntity
        /// </returns>
        public BookingDetailsEntity FetchBooking(string reservationNumber, string legNumber, string hotelCountryCode)
        {
            BookingDetailsEntity bookingEntities = m_ReservationDomain.FetchReservation(reservationNumber, legNumber);
            if (bookingEntities != null)
            {
                if (bookingEntities.HotelSearch != null && bookingEntities.HotelSearch.ListRooms != null &&
                    bookingEntities.HotelSearch.ListRooms.Count > 0 && bookingEntities.HotelSearch.ListRooms[0] != null)
                    bookingEntities.HotelSearch.ListRooms[0].IsRoomConfirmed = true;

                GuestInformationEntity guestInfoEntity = bookingEntities.GuestInformation;
                if (guestInfoEntity != null)
                {
                    guestInfoEntity.ReservationNumber = reservationNumber;
                    guestInfoEntity.LegNumber = legNumber;

                    string nameID = guestInfoEntity.NameID;
                    if (!string.IsNullOrEmpty(nameID))
                    {
                        UserProfileEntity fetchUserProfile = m_LoyaltyDomain.FetchUserProfile(nameID);

                        if (!string.IsNullOrEmpty(fetchUserProfile.HomePhone))
                        {
                            guestInfoEntity.Landline = new PhoneDetailsEntity()
                            {
                                Number = fetchUserProfile.HomePhone,
                                OperaId = fetchUserProfile.HomePhoneOperaID,
                                PhoneType = PhoneContants.PHONETYPE_HOME
                            };
                        }
                        if (!string.IsNullOrEmpty(fetchUserProfile.MobilePhone))
                        {
                            guestInfoEntity.Mobile = new PhoneDetailsEntity()
                            {
                                Number = fetchUserProfile.MobilePhone,
                                OperaId = fetchUserProfile.MobilePhoneOperaID,
                                PhoneType = PhoneContants.PHONETYPE_MOBILE
                            };
                        }
                        guestInfoEntity.EmailDetails = new EmailDetailsEntity()
                        {
                            EmailID = fetchUserProfile.EmailID,
                            EmailOperaID = fetchUserProfile.EmailOperaId
                        };
                        guestInfoEntity.NonBusinessEmailDetails = new EmailDetailsEntity()
                        {
                            EmailID = fetchUserProfile.NonBusinessEmailID,
                            EmailOperaID = fetchUserProfile.NonBusinessEmailOperaId
                        };
                        HotelSearchEntity hotelSearch = bookingEntities.HotelSearch;

                        
                        if (hotelSearch != null && string.IsNullOrEmpty(hotelCountryCode))
                        {
                            ContentDataAccessManager cdaManager = new ContentDataAccessManager();
                            var countryPages = cdaManager.GetCountryCityAndHotelForAutoSuggest(false, false);
                            var countryCode = from cnts in countryPages
                                              from cities in cnts.SearchedCities
                                              from hotels in cities.SearchedHotels
                                              where (hotels != null) && string.Equals(hotels.OperaDestinationId, hotelSearch.SelectedHotelCode)
                                              select cnts.CountryCode;
                            hotelSearch.HotelCountryCode = (countryCode != null && !string.IsNullOrEmpty(countryCode.FirstOrDefault())) ? countryCode.FirstOrDefault() : null;
                        }
                        
                        else if (hotelSearch != null && !string.IsNullOrEmpty(hotelCountryCode) && string.IsNullOrEmpty(hotelSearch.HotelCountryCode))
                        {
                            hotelSearch.HotelCountryCode = hotelCountryCode;
                        }
                        if(string.IsNullOrEmpty(hotelSearch.HotelCountryCode))
                        {
                            hotelSearch.HotelCountryCode =
                                m_InformationDomain.GetHotelCountryCode(hotelSearch.SelectedHotelCode);
                        }
                        guestInfoEntity.Gender = GetStringFromUserGender(fetchUserProfile.Gender);
                        guestInfoEntity.PreferredLanguage = fetchUserProfile.PreferredLanguage;
                        m_LoyaltyDomain.PrimaryLanguageID = fetchUserProfile.PreferredLanguage;

                        guestInfoEntity.AddressID = fetchUserProfile.AddressOperaID.ToString();
                        guestInfoEntity.AddressLine1 = fetchUserProfile.AddressLine1;
                        guestInfoEntity.AddressLine2 = fetchUserProfile.AddressLine2;
                        guestInfoEntity.City = fetchUserProfile.City;
                        guestInfoEntity.PostCode = fetchUserProfile.PostCode;
                        guestInfoEntity.Country = fetchUserProfile.Country;

                        List<BedAccommodation> listBedAccommodation = new List<BedAccommodation>();
                        if (hotelSearch != null && hotelSearch.ListRooms != null &&
                            hotelSearch.ListRooms.Count > 0 && hotelSearch.ListRooms[0].ChildrenPerRoom > 0)
                        {
                            listBedAccommodation = m_ReservationDomain.FetchReservationPackages(reservationNumber, legNumber,
                                                                           hotelSearch.SelectedHotelCode);
                        }
                       
                        if (listBedAccommodation != null && listBedAccommodation.Count > 0)
                        {
                            guestInfoEntity.ChildrensDetails = new ChildrensDetailsEntity() { AdultsPerRoom = (uint)hotelSearch.AdultsPerRoom };
                            guestInfoEntity.ChildrensDetails.SetAccommodationList(listBedAccommodation);

                            List<ChildEntity> listOfChildren = new List<ChildEntity>();
                            ChildrensDetailsEntity childDetailsEntity = new ChildrensDetailsEntity();
                            int totalChildren = 0;
                            int childrenOccupancy = 0;
                            for (int childCount = 0; childCount < listBedAccommodation.Count; childCount++)
                            {
                                for (int childQuantity = 0;
                                     childQuantity < listBedAccommodation[childCount].Quantity;
                                     childQuantity++)
                                {
                                    ChildEntity childEntityObj = new ChildEntity() { ChildAccommodationType = listBedAccommodation[childCount].BedType };
                                    listOfChildren.Add(childEntityObj);
                                    if (childEntityObj.ChildAccommodationType == ChildAccommodationType.CRIB
                                        || childEntityObj.ChildAccommodationType == ChildAccommodationType.XBED
                                        )
                                    {
                                        childrenOccupancy++;
                                    }
                                }
                                totalChildren += listBedAccommodation[childCount].Quantity;
                            }
                            childDetailsEntity.ListChildren = listOfChildren;
                            if (hotelSearch.ListRooms != null)
                            {
                                hotelSearch.ListRooms[0].ChildrenDetails = childDetailsEntity;
                                hotelSearch.ListRooms[0].ChildrenPerRoom = totalChildren;
                                hotelSearch.ListRooms[0].ChildrenOccupancyPerRoom = childrenOccupancy;
                            }
                            if (hotelSearch.ChildrenPerRoom < totalChildren)
                                hotelSearch.ChildrenPerRoom = totalChildren;
                        }
                    }
                }
            }
            return bookingEntities;
        }

        /// <summary>
        /// This will fetch the booking summary for leged bookings.
        /// </summary>
        /// <param name="bookingNumber">
        /// Booking number for which summary needs to be fetched.
        /// </param>
        /// <returns>List of Booking Details Entity</returns>
        public List<BookingDetailsEntity> FetchBookingSummary(string bookingNumber)
        {
            SortedList<string, string> bookingNumberList = m_ReservationDomain.FetchBookingSummary(bookingNumber);

            List<BookingDetailsEntity> bookingList = new List<BookingDetailsEntity>();
            if (bookingNumberList != null && bookingNumberList.Count > 0)
            {
                foreach (string legNumber in bookingNumberList.Keys)
                {
                    BookingDetailsEntity bookingEntity = null;
                    if (bookingList.Count > 0 && bookingList[0].HotelSearch != null && bookingList[0].HotelSearch.HotelCountryCode != null)
                        bookingEntity = FetchBooking(bookingNumberList[legNumber], legNumber, bookingList[0].HotelSearch.HotelCountryCode);
                    else
                        bookingEntity = FetchBooking(bookingNumberList[legNumber], legNumber);
                    bookingList.Add(bookingEntity);
                }
            }
            return bookingList;
        }

        /// <summary>
        /// Cancel the Booking
        /// </summary>
        /// <param name="cancelDetailsEntity">
        /// CancelDetailsEntity
        /// </param>
        /// <returns>
        /// Cancellation Number
        /// </returns>
        public string CancelBooking(ref CancelDetailsEntity cancelDetailsEntity)
        {
            //isRedemptionBooking is passed as false because this method is called from
            //scanweb where serach type is identified automatically,
            //isRequestFromMobile is passed as false bacuase this methos will be called from
            //scanweb and not from mobile application.
            return CancelBooking(ref cancelDetailsEntity,false, false);
        }

        public string CancelBooking(ref CancelDetailsEntity cancelDetailsEntity, bool isRedemptionBooking, bool isRequestFromMobile)
        {
            List<BookingDetailsEntity> activeBookings = m_SessionManager.ActiveBookingDetails;
            bool flag = isRedemptionBooking;
            string cancelResult = string.Empty;
            if (activeBookings != null && !isRequestFromMobile)
            {
                foreach (BookingDetailsEntity bookingDetailsEntity in activeBookings)
                {
                    if (bookingDetailsEntity.HotelSearch.SearchingType == SearchType.REDEMPTION)
                    {
                        flag = true;
                        break;
                    }
                }
            }
            if (flag)
            {
                cancelResult = m_ReservationDomain.CancelReservation(ref cancelDetailsEntity, true);
            }
            else
            {
                cancelResult = m_ReservationDomain.CancelReservation(ref cancelDetailsEntity, false);
            }
            return cancelResult;
        }

        /// <summary>
        /// Ignore Booking
        /// </summary>
        /// <param name="ignoreDetailsEntity"></param>
        /// <returns>Ignore Booking Number</returns>
        public string IgnoreBooking(IgnoreBookingEntity ignoreDetailsEntity)
        {
            return m_ReservationDomain.IgnoreBooking(ignoreDetailsEntity);
        }

        #endregion

        #region GetCurrentBooking

        /// <summary>
        /// Get the Current booking for the specified name id
        /// </summary>
        /// <param name="NameId">
        /// Name ID
        /// </param>
        /// <returns>
        /// CurrentBookingSummary
        /// </returns>
        public CurrentBookingSummary GetCurrentBookingSummary(string NameId)
        {
            FutureBookingSummaryResponse response = m_ReservationDomain.GetCurrentBookingList(NameId);
            CurrentBookingSummary currentBookingList = null;
            if (response.Result.resultStatusFlag != ServiceProxies.Reservation.ResultStatusFlag.FAIL)
            {
                currentBookingList = new CurrentBookingSummary(response);
            }
            return currentBookingList;
        }

        #endregion GetCurrentBooking   

        #region Private Methods

        /// <summary>
        /// Create the UserProfile Address Entity 
        /// </summary>
        /// <param name="guestInfoEntity">
        /// GuestInformation Entity
        /// </param>
        /// <returns>
        /// UserProfile Entity
        /// </returns>
        private UserProfileEntity CreateUserProfileAddressEntity(GuestInformationEntity guestInfoEntity)
        {
            UserProfileEntity userProfileEntity = null;
            if (guestInfoEntity != null)
            {
                userProfileEntity = new UserProfileEntity();
                string addressID = guestInfoEntity.AddressID;
                if (addressID != null && (addressID != string.Empty))
                {
                    try
                    {
                        userProfileEntity.AddressOperaID = Int32.Parse(addressID);
                    }
                    catch (Exception exception)
                    {
                        //Catch the exception for parsing failed.
                    }
                }
                userProfileEntity.AddressLine1 = guestInfoEntity.AddressLine1;
                userProfileEntity.AddressLine2 = guestInfoEntity.AddressLine2;
                userProfileEntity.City = guestInfoEntity.City;
                userProfileEntity.Country = guestInfoEntity.Country;
                userProfileEntity.PostCode = guestInfoEntity.PostCode;
                userProfileEntity.AddressType = guestInfoEntity.AddressType;
                userProfileEntity.PreferredLanguage = guestInfoEntity.PreferredLanguage;
            }
            return userProfileEntity;
        }

        /// <summary>
        /// Create the UserProfile Entity for Name
        /// </summary>
        /// <param name="guestInfoEntity">
        /// GuestInformation Entity
        /// </param>
        /// <returns>
        /// UserProfileEntity
        /// </returns>
        private UserProfileEntity CreateUserProfileNameEntity(GuestInformationEntity guestInfoEntity)
        {
            UserProfileEntity userProfileEntity = null;
            if (guestInfoEntity != null)
            {
                userProfileEntity = new UserProfileEntity();
                //Ramana: Added title as part of title for name.
                userProfileEntity.NameTitle = guestInfoEntity.Title;
                userProfileEntity.FirstName = guestInfoEntity.FirstName;
                userProfileEntity.LastName = guestInfoEntity.LastName;
                userProfileEntity.Gender = GetUserGenderFromString(guestInfoEntity.Gender);
                userProfileEntity.PreferredLanguage = guestInfoEntity.PreferredLanguage;
                
                if (m_SessionManager.LoyaltyDetails != null)
                {
                    if (m_SessionManager.LoyaltyDetails.NameID != null)
                    {
                        NameController nameController = new NameController();
                        DateOfBirthEntity DOB = nameController.GetMemberDOB(m_SessionManager.LoyaltyDetails.NameID);

                        if (DOB != null)
                        {
                            userProfileEntity.DateOfBirth = DOB;
                        }
                    }
                }
            }
            return userProfileEntity;
        }

        /// <summary>
        /// Convert to UserPreferenceEntity List
        /// </summary>
        /// <param name="specialRequestList">
        /// SpecialRequest List
        /// </param>
        /// <returns>
        /// UserPreference List
        /// </returns>
        private UserPreferenceEntity[] ConvertToUserPreferenceEntityList(SpecialRequestEntity[] specialRequestList)
        {
            UserPreferenceEntity[] userPreferenceList = null;
            //Preference List Conversion        
            if (specialRequestList != null)
            {
                int totalRequeset = specialRequestList.Length;
                userPreferenceList = new UserPreferenceEntity[totalRequeset];
                for (int requestCount = 0; requestCount < totalRequeset; requestCount++)
                {
                    userPreferenceList[requestCount] = new UserPreferenceEntity();
                    userPreferenceList[requestCount].RequestType =
                        specialRequestList[requestCount].RequestType;
                    userPreferenceList[requestCount].RequestValue =
                        specialRequestList[requestCount].RequestValue;
                }
            }
            return userPreferenceList;
        }

        /// <summary>
        /// Get UserGender from String
        /// </summary>
        /// <param name="gender">
        /// String representing Gender
        /// </param>
        /// <returns>
        /// UserGender
        /// </returns>
        private static UserGender GetUserGenderFromString(string gender)
        {
            UserGender userGender = UserGender.MALE;
            switch (gender)
            {
                case AppConstants.MALE:
                    {
                        userGender = UserGender.MALE;
                        break;
                    }
                case AppConstants.FEMALE:
                    {
                        userGender = UserGender.FEMALE;
                        break;
                    }
            }
            return userGender;
        }

        /// <summary>
        /// Get UserGender from String
        /// </summary>
        /// <param name="gender">
        /// String representing Gender
        /// </param>
        /// <returns>
        /// UserGender
        /// </returns>
        private static string GetStringFromUserGender(UserGender userGender)
        {
            string gender = AppConstants.MALE;
            switch (userGender)
            {
                case UserGender.MALE:
                    gender = AppConstants.MALE;
                    break;
                case UserGender.FEMALE:
                    gender = AppConstants.FEMALE;
                    break;
            }
            return gender;
        }

        #endregion

        /// <summary>
        /// Confirm Booking
        /// </summary>
        /// <param name="confirmBookingEntity">Confirm Booking Entity</param>
        /// <returns>Confirm Booking Number</returns>
        public string ConfirmBooking(ConfirmBookingEntity confirmBookingEntity)
        {
            return m_ReservationDomain.ConfirmBooking(confirmBookingEntity);
        }


        public SortedList<string, string> ModifyGuaranteeInformation(
        HotelSearchEntity hotelSearch, HotelSearchRoomEntity roomSearch, HotelRoomRateEntity roomRate,
        GuestInformationEntity guestInformation, string prevReservationNumber, string sourceCode,
        out string createdNameID, bool isSessionBooking)
        {            
            SortedList<string, string> legConfirmationMap =
                m_ReservationDomain.ModifyGuaranteeReservation(hotelSearch, roomSearch, roomRate, guestInformation,
                                                    prevReservationNumber, sourceCode, out createdNameID);
            
            if (!m_SessionManager.DirectlyModifyContactDetails)
            {
                string legNumber = legConfirmationMap.Keys[0];
                string confirmationNumber = legConfirmationMap[legNumber];
                m_ReservationDomain.AddChildrenAccommodations(confirmationNumber, legNumber,
                                                            hotelSearch.SelectedHotelCode,
                                                            guestInformation);
            }
            return legConfirmationMap;
        }

    }
}