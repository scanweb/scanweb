﻿//  Description					: Holds all members required for payment operations.	  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Sapient                                                 //
//  Author email id				:                           							  //
//  Creation Date				: 18th December  2012									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using Netaxept.Entities;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Netaxept;
using Nets.Entities;
using System.Configuration;
using Netaxept.NetsPaymentServices;
using ScanwebEntities = Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller
{
    public class PaymentController
    {
        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="paymentDomainObj"></param>
        public PaymentController(IContentDataAccessManager contentDataAccessManagerObj, IAdvancedReservationDomain advancedReservationDomainObj,
                                 ILoyaltyDomain loyaltyDomainObj, IReservationDomain reservationDomainObj)
        {
            contentDataAccessManager = contentDataAccessManagerObj;
            advancedReservationDomain = advancedReservationDomainObj;
            loyaltyDomain = loyaltyDomainObj;
            reservationDomain = reservationDomainObj;
        }

        public PaymentController()
        {
            contentDataAccessManager = new ContentDataAccessManager();
            advancedReservationDomain = new AdvancedReservationDomain();
            loyaltyDomain = new LoyaltyDomain();
            reservationDomain = new ReservationDomain();
        }
        #endregion

        #region Domain Properties

        private IContentDataAccessManager contentDataAccessManager = null;
        /// <summary>
        /// Gets m_ContentDataAccessManager
        /// </summary>
        private IContentDataAccessManager m_ContentDataAccessManager
        {
            get
            {
                return contentDataAccessManager;
            }
        }

        private IAdvancedReservationDomain advancedReservationDomain = null;
        /// <summary>
        /// Gets AdvancedReservationDomain
        /// </summary>
        private IAdvancedReservationDomain m_AdvancedReservationDomain
        {
            get { return advancedReservationDomain; }
        }

        private ILoyaltyDomain loyaltyDomain = null;

        /// <summary>
        /// Gets LoyaltyDomain
        /// </summary>
        private ILoyaltyDomain m_LoyaltyDomain
        {
            get { return loyaltyDomain; }
        }

        private IReservationDomain reservationDomain = null;
        /// <summary>
        /// Gets AdvancedReservationDomain
        /// </summary>
        private IReservationDomain m_ReservationDomain
        {
            get { return reservationDomain; }
        }
        #endregion

        #region Private Methods
        private List<NetsRoom> GetRoomInfoForNets(string adultText, string adultsText, string childText, string childrenText,
                                                  string chargedNowText, string chargedAtHotelText, ref bool isAllRoomBookingsArePrepaid)
        {
            List<NetsRoom> rooms = new List<NetsRoom>();
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
            List<HotelRoomRateEntity> hotelRoomRateEntities = Reservation2SessionWrapper.PaymentHotelRoomRateInformation;
            isAllRoomBookingsArePrepaid = RoomRateUtil.IsAllRoomBookingsArePrepaid(selectedRoomAndRatesHashTable);
            if ((search != null) && (selectedRoomAndRatesHashTable != null) && (hotelRoomRateEntities != null))
            {
                if (search.ListRooms != null)
                {
                    for (int roomCount = 0; roomCount < search.ListRooms.Count; roomCount++)
                    {
                        NetsRoom room = new NetsRoom();
                        if (search.ListRooms[roomCount] != null)
                        {
                            room.Children = string.Empty;
                            if (search.ListRooms[roomCount].ChildrenPerRoom > 1)
                            {
                                room.Children = string.Format("{0} {1}", search.ListRooms[roomCount].ChildrenPerRoom.ToString(), childrenText);
                            }
                            else if (search.ListRooms[roomCount].ChildrenPerRoom == 1)
                            {
                                room.Children = string.Format("{0} {1}", search.ListRooms[roomCount].ChildrenPerRoom.ToString(), childText);
                            }
                            if (search.ListRooms[roomCount].AdultsPerRoom > 1)
                            {
                                room.NoofAdults = string.Format("{0} {1}", search.ListRooms[roomCount].AdultsPerRoom.ToString(), adultsText);
                            }
                            else
                            {
                                room.NoofAdults = string.Format("{0} {1}", search.ListRooms[roomCount].AdultsPerRoom.ToString(), adultText);
                            }
                        }
                        SelectedRoomAndRateEntity selectedRoomAndRateEntity = selectedRoomAndRatesHashTable[roomCount] as SelectedRoomAndRateEntity;
                        if (selectedRoomAndRateEntity != null)
                        {
                            RateCategory rateCategory = RoomRateUtil.GetRateCategoryByCategoryId(selectedRoomAndRateEntity.RateCategoryID);
                            if (rateCategory != null)
                            {
                                room.RateCategory = rateCategory.RateCategoryName;
                            }
                            room.RoomName = selectedRoomAndRateEntity.RoomDescription;
                        }
                        if (hotelRoomRateEntities[roomCount] != null)
                        {
                            room.Rate = hotelRoomRateEntities[roomCount].TotalRate.Rate.ToString();
                        }
                        room.ChargedNowCaptionForRoom = string.Empty;
                        if (!isAllRoomBookingsArePrepaid)
                        {
                            if (RoomRateUtil.IsSaveRateCategory(selectedRoomAndRateEntity.RateCategoryID))
                            {
                                room.ChargedNowCaptionForRoom =
                                    string.Format("<strong style='color:Green;float:right; '> {0} </strong>", chargedNowText);
                            }
                            else
                            {
                                room.ChargedNowCaptionForRoom = string.Format("<strong style='color:Red;float:right; '> {0} </strong>", chargedAtHotelText);
                            }
                        }
                        rooms.Add(room);
                    }
                }
            }
            return rooms;
        }

        /// <summary>
        /// Gets user friendly error message for Nets error code.
        /// </summary>
        /// <param name="netsError"></param>
        /// <param name="errorSource"></param>
        /// <returns></returns>
        private void ExtractUserFriendlyErrorMessageForNetsErrorCode(NetsError netsError, string errorSource, string merchantId)
        {
            string logMessage = string.Format(
                    "Error occured in {0} for the transaction with the reservation number: {5} and the Nets transaction ID {1}. Error reason is {2}, and error code is {3}. Merchand ID is {4}.",
                    errorSource, Reservation2SessionWrapper.PaymentTransactionId, netsError.ErrorDescription,
                    netsError.ErrorCode, merchantId, ReservationNumberSessionWrapper.ReservationNumber);
            AppLogger.LogOnlinePaymentFatalException(new Exception(logMessage), logMessage);
            if (netsError.ErrorCode.Equals(NetsConstants.CancelErrorCode, StringComparison.InvariantCultureIgnoreCase))
            {
                Reservation2SessionWrapper.NetsPaymentInfoMessage = RoomRateUtil.GetTranslatedText(string.Format("/PSPErrors/{0}", netsError.ShortName));
            }
            else
            {
                Reservation2SessionWrapper.NetsPaymentErrorMessage = RoomRateUtil.GetTranslatedText(string.Format("/PSPErrors/{0}", netsError.ShortName));
            }
            if (netsError.IsEmailToBeSent)
            {
                EmailEntity emailEntity = new EmailEntity();
                emailEntity.Recipient = AppConstants.EmailRecipientsForOnlinePaymentFailures;
                emailEntity.Sender = AppConstants.EmailSenderForOnlinePaymentFailures;
                emailEntity.Subject = string.Format("Online payment | Error occured in {0} for the reservation with the Nets transaction ID {1} with hotel operaId {2}.", errorSource,
                    Reservation2SessionWrapper.PaymentTransactionId, SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode);
                emailEntity.Body = logMessage;
                emailEntity.TextEmailBody = logMessage;
                CommunicationService.SendMail(emailEntity, null);
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Fetches merchant profile for the given hotel operaId.
        /// </summary>
        /// <param name="operaId"></param>
        /// <returns></returns>
        public MerchantProfileEntity GetMerchantByHotelOperaId(string operaId, string language)
        {
            return m_ContentDataAccessManager.GetMerchantByHotelOperaId(operaId, language);
        }

        /// <summary>
        /// Fetches merchant profile for the given hotel operaId and language
        /// </summary>
        /// <param name="operaId"></param>
        /// <returns></returns>
        public MerchantProfileEntity GetMerchantByHotelOperaId(string operaId)
        {
            return m_ContentDataAccessManager.GetMerchantByHotelOperaId(operaId, string.Empty);
        }

        /// <summary>
        /// Registers the payment information to the Nets PSP.
        /// </summary>
        ///<param hotelOperaCode=""></param>    
        /// <returns>TransactionId</returns>
        public string Register(string hotelOperaCode, string hotelImageURL, string prepaidAmount, string roomDescriptionForNets, string paymentHeader,
                               string creditCardIconsUrl, string paymentDeclaration, string firstBreadcrumb, string secondBreadcrumb, string thirdBreadcrumb,
                               string fourthBreadcrumb, string fifthBreadcrumb, string yourStayText,
                               string totalAmount, string adultText, string adultsText, string childText,
                               string childrenText, string chargedNowText, string bookingTotalText,
                               string inclTaxesText, string totalRateCurrencyCode, string chargedAtHotelText, string savedCreditCardPanHash,
                               string arrivalDate, string departureDate)
        {
            string transactionId = string.Empty;
            string merchantId = string.Empty;
            string merchantToken = string.Empty;
            try
            {
                PaymentHelper paymentHelper = new PaymentHelper();
                if (!string.IsNullOrEmpty(hotelOperaCode))
                {
                    var hotelMerchantInfo = GetMerchantByHotelOperaId(hotelOperaCode);
                    if (hotelMerchantInfo != null)
                    {
                        NetsRegisterRequest netsRegisterRequest = new NetsRegisterRequest();
                        merchantId = hotelMerchantInfo.MerchantID;
                        netsRegisterRequest.MerchantId = hotelMerchantInfo.MerchantID;
                        merchantToken = hotelMerchantInfo.MerchantToken;
                        netsRegisterRequest.MerchantToken = hotelMerchantInfo.MerchantToken;
                        netsRegisterRequest.OrderAmount = prepaidAmount;
                        netsRegisterRequest.MerchantCurrencyCode = totalRateCurrencyCode;
                        netsRegisterRequest.Language = CMSSessionWrapper.CurrentLanguage;
                        netsRegisterRequest.OrderDescription = roomDescriptionForNets;
                        netsRegisterRequest.OrderNumber = string.Format("{0}-{1}", hotelOperaCode, ReservationNumberSessionWrapper.ReservationNumber);
                        netsRegisterRequest.PanHash = savedCreditCardPanHash;
                        bool isAllRoomBookingsArePrepaid = false;
                        netsRegisterRequest.RoomDetails =
                            GetRoomInfoForNets(adultText, adultsText, childText, childrenText, chargedNowText, chargedAtHotelText, ref isAllRoomBookingsArePrepaid);
                        netsRegisterRequest.IsAllRoomBookingsArePrepaid = isAllRoomBookingsArePrepaid;
                        NetsKeyValue netsKeyValue = new NetsKeyValue();
                        netsKeyValue.HotelImage = hotelImageURL;
                        netsKeyValue.MerchantNameHeader = RoomRateUtil.GetTranslatedText("/bookingengine/booking/PaymentProcessingPage/merchant");
                        netsKeyValue.OrderNumberHeader = RoomRateUtil.GetTranslatedText("/bookingengine/booking/PaymentProcessingPage/ordernumber");
                        netsKeyValue.OrderDescriptionHeader = RoomRateUtil.GetTranslatedText("/bookingengine/booking/PaymentProcessingPage/orderdescription");
                        netsKeyValue.AmountHeader = RoomRateUtil.GetTranslatedText("/bookingengine/booking/PaymentProcessingPage/amount");
                        netsKeyValue.PaymentDeclaration = paymentDeclaration;
                        netsKeyValue.PaymentHeader = paymentHeader;
                        netsKeyValue.CreditCardIconsUrl = creditCardIconsUrl;
                        netsKeyValue.FirstBreadcrumb = firstBreadcrumb;
                        netsKeyValue.SecondBreadcrumb = secondBreadcrumb;
                        netsKeyValue.ThirdBreadcrumb = thirdBreadcrumb;
                        netsKeyValue.FourBreadcrumb = fourthBreadcrumb;
                        netsKeyValue.FiveBreadcrumb = fifthBreadcrumb;
                        netsKeyValue.RightHeaderHeading = yourStayText;
                        netsKeyValue.ChargedTotal = prepaidAmount;
                        netsKeyValue.BookingTotal = totalAmount;
                        netsKeyValue.ChargedNowCaption = chargedNowText;
                        netsKeyValue.BookingTotalCaption = bookingTotalText;
                        netsKeyValue.TaxandFeesCaption = inclTaxesText;
                        netsKeyValue.CurrencyCode = totalRateCurrencyCode;
                        netsKeyValue.MerchantName = hotelMerchantInfo.Name;
                        netsKeyValue.Amount = prepaidAmount;
                        netsKeyValue.OrderNumber = netsRegisterRequest.OrderNumber;
                        netsKeyValue.OrderDescription = netsRegisterRequest.OrderDescription;
                        HotelDestination hoteldestination = contentDataAccessManager.GetHotelByOperaID(hotelOperaCode);
                        if (hoteldestination != null)
                        {
                            netsKeyValue.HotelName = hoteldestination.Name;
                        }
                        HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                        netsKeyValue.EndDayandDate = departureDate;
                        netsKeyValue.StartDayandDate = arrivalDate;
                        netsRegisterRequest.KeyValues = netsKeyValue;
                        transactionId = paymentHelper.Register(netsRegisterRequest);
                    }
                }
            }
            catch (FaultException faultException)
            {
                if (faultException.GetType() == typeof(FaultException<AuthenticationException>))
                {
                    EmailEntity emailEntity = new EmailEntity();
                    emailEntity.Recipient = AppConstants.EmailRecipientsForOnlinePaymentFailures;
                    emailEntity.Sender = AppConstants.EmailSenderForOnlinePaymentFailures;
                    emailEntity.Subject = string.Format("Online payment | Merchant authentication failed while making Nets register. Merchant ID: {0}.", merchantId);
                    emailEntity.Body = "Merchant authentication failed while making Nets register";
                    CommunicationService.SendMail(emailEntity, null);
                    AppLogger.LogOnlinePaymentFatalException(faultException, string.Format("{0}. Merchant ID: {1}.", faultException.Message, merchantId));
                }
                else
                {
                    ProcessErrorMessageForNetsErrorCode(string.Empty, NetsConstants.Register);
                    AppLogger.LogOnlinePaymentFatalException(faultException, string.Format("{0}. Merchant ID: {1}.", faultException.Message, merchantId));
                }
            }
            catch (Exception ex)
            {
                ProcessErrorMessageForNetsErrorCode(string.Empty, NetsConstants.Register);
                AppLogger.LogOnlinePaymentFatalException(ex, string.Format("{0}. Merchant ID: {1}.", ex.Message, merchantId));
            }
            return transactionId;
        }

        /// <summary>
        /// Gets the terminal url based on the transactionId.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns>Nets terminal url</returns>
        public string GetNetsTerminalURL(string transactionId, string hotelOperaCode)
        {
            PaymentHelper paymentHelper = new PaymentHelper();
            string netsTerminalURL = string.Empty;
            var hotelMerchantInfo = GetMerchantByHotelOperaId(hotelOperaCode);
            if (hotelMerchantInfo != null)
            {
                netsTerminalURL = paymentHelper.TerminalURL(hotelMerchantInfo.MerchantID, transactionId);
            }
            return netsTerminalURL;
        }

        /// <summary>
        /// Gets the terminal url based on the transactionId for mobile.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns>Nets terminal url</returns>
        public string GetNetsTerminalURLForMobile(string transactionId)
        {
            PaymentHelper paymentHelper = new PaymentHelper();
            string netsTerminalURL = string.Empty;
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if ((search != null) && !string.IsNullOrEmpty(search.SelectedHotelCode))
            {
                var hotelMerchantInfo = GetMerchantByHotelOperaId(search.SelectedHotelCode);
                if (hotelMerchantInfo != null)
                {
                    netsTerminalURL = paymentHelper.TerminalURLForMobile(hotelMerchantInfo.MerchantID, transactionId);
                }
            }
            return netsTerminalURL;
        }

        /// <summary>
        /// Does a call to Nets authorization.  
        /// </summary>
        /// <param name="transactionId"></param>
        public string NetsAuthProcess(string transactionId, string hotelOperaCode)
        {
            PaymentHelper paymentHelper = new PaymentHelper();
            if (string.IsNullOrEmpty(hotelOperaCode))
            {
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if ((search != null) && !string.IsNullOrEmpty(search.SelectedHotelCode))
                {
                    hotelOperaCode = search.SelectedHotelCode;
                }
            }
            string authProcessStatus = string.Empty;
            var hotelMerchantInfo = GetMerchantByHotelOperaId(hotelOperaCode);
            if (hotelMerchantInfo != null)
            {
                NetsError netsError = new NetsError();
                authProcessStatus = paymentHelper.AuthProcess(transactionId, hotelMerchantInfo.MerchantID, hotelMerchantInfo.MerchantToken, out netsError);

                string netsErrorMessage = string.Empty;

                if (netsError != null)
                {
                    netsErrorMessage = string.Format(" Error reason is {0} and error code is {1}.", netsError.ErrorDescription, netsError.ErrorCode);
                }

                AppLogger.LogOnlinePaymentTransactionsInfoMessage(
                       string.Format("In Authorization for Nets Payment Flow with TransactionID: {0}, Authorization Status: {6}, MerchantID: {5} and the BookingID: {1} for the hotel: {2}, from the WebServer: {3} at {4}.{7}",
                      transactionId, ReservationNumberSessionWrapper.ReservationNumber, hotelOperaCode, System.Environment.MachineName, DateTime.Now, hotelMerchantInfo.MerchantID, authProcessStatus, netsErrorMessage));

                if (!authProcessStatus.Equals(NetsConstants.OK, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (netsError != null)
                    {
                        ExtractUserFriendlyErrorMessageForNetsErrorCode(netsError, NetsConstants.Auth, hotelMerchantInfo.MerchantID);
                    }
                    else
                    {
                        ProcessErrorMessageForNetsErrorCode(authProcessStatus, NetsConstants.Auth);
                    }
                }
            }
            return authProcessStatus;
        }

        /// <summary>
        /// Does a call to Nets to capture the order amount.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="captureAmount"></param>
        public bool NetsCaptureProcess(string transactionId, string captureAmount, string hotelOperaCode)
        {
            bool returnValue = false;
            string netsCaptureStatus = string.Empty;
            try
            {
                PaymentHelper paymentHelper = new PaymentHelper();
                if (string.IsNullOrEmpty(hotelOperaCode))
                {
                    HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                    if ((search != null) && !string.IsNullOrEmpty(search.SelectedHotelCode))
                    {
                        hotelOperaCode = search.SelectedHotelCode;
                    }
                }
                var hotelMerchantInfo = GetMerchantByHotelOperaId(hotelOperaCode);
                if (hotelMerchantInfo != null)
                {
                    netsCaptureStatus = paymentHelper.CaptureProcess(transactionId, captureAmount, hotelMerchantInfo.MerchantID, hotelMerchantInfo.MerchantToken);
                }
                if (netsCaptureStatus.Equals(NetsConstants.OK, StringComparison.InvariantCultureIgnoreCase))
                {
                    returnValue = true;
                }
                else
                {
                    ProcessErrorMessageForNetsErrorCode(netsCaptureStatus, NetsConstants.Capture);
                }
            }
            catch (Exception ex)
            {
                ProcessErrorMessageForNetsErrorCode(string.Empty, NetsConstants.Capture);
                returnValue = false;
            }
            return returnValue;
        }

        /// <summary>
        /// Does a annul call to Nets.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public string NetsAnnulProcess(string transactionId)
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            string annulStatus = string.Empty;
            if ((search != null) && !string.IsNullOrEmpty(search.SelectedHotelCode))
            {
                var hotelMerchantInfo = GetMerchantByHotelOperaId(search.SelectedHotelCode);
                if (hotelMerchantInfo != null)
                {
                    PaymentHelper paymentHelper = new PaymentHelper();
                    NetsError netsError = new NetsError();
                    annulStatus = paymentHelper.AnnulProcess(transactionId, hotelMerchantInfo.MerchantID, hotelMerchantInfo.MerchantToken, out netsError);
                    if (!annulStatus.Equals(NetsConstants.OK, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (netsError != null)
                        {
                            ExtractUserFriendlyErrorMessageForNetsErrorCode(netsError, NetsConstants.Annul, hotelMerchantInfo.MerchantID);
                        }
                        else
                        {
                            ProcessErrorMessageForNetsErrorCode(annulStatus, NetsConstants.Auth);
                        }
                    }
                }
            }
            return annulStatus;
        }

        /// <summary>
        /// Gets paymentinfo for the given transactionId.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public ScanwebEntities.PaymentInfo GetPaymentInfo(string transactionId, string hotelOperaCode)
        {
            PaymentHelper paymentHelper = new PaymentHelper();
            ScanwebEntities.PaymentInfo paymentInfo = new ScanwebEntities.PaymentInfo();
            if (string.IsNullOrEmpty(hotelOperaCode))
            {
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if ((search != null) && !string.IsNullOrEmpty(search.SelectedHotelCode))
                {
                    hotelOperaCode = search.SelectedHotelCode;
                }
            }
            var hotelMerchantInfo = GetMerchantByHotelOperaId(hotelOperaCode);
            if (hotelMerchantInfo != null)
            {
                Netaxept.NetsPaymentServices.PaymentInfo netsPaymentInfo =
                   paymentHelper.Query(transactionId, hotelMerchantInfo.MerchantID, hotelMerchantInfo.MerchantToken);
                if (string.IsNullOrEmpty(netsPaymentInfo.CardInformation.MaskedPAN))
                {
                    if ((LoyaltyDetailsSessionWrapper.LoyaltyDetails != null) && !string.IsNullOrEmpty(LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID))
                    {
                        CreditCardEntity panHashCreditCard = m_LoyaltyDomain.FetchCreditCardDetailsByPanHash(
                            LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID, netsPaymentInfo.CardInformation.PanHash);
                        netsPaymentInfo.CardInformation.MaskedPAN = panHashCreditCard.CardNumber;
                        netsPaymentInfo.CardInformation.ExpiryDate = string.Format("{0}{1}", panHashCreditCard.ExpiryDate.ToDateTime().ToString("yy"),
                           panHashCreditCard.ExpiryDate.ToDateTime().ToString("MM"));
                    }
                }
                paymentInfo = RoomRateUtil.ConvertToPaymentInfo(netsPaymentInfo);

            }
            return paymentInfo;
        }

        /// <summary>
        /// Registers (for mobile request) the payment information to the Nets PSP.
        /// </summary>
        /// <param name="prepaidAmount"></param>
        /// <param name="panHash"> </param>
        /// <returns></returns>
        public string RegisterForMobileRequest(string prepaidAmount, string panHash)
        {
            PaymentHelper paymentHelper = new PaymentHelper();
            string transactionId = string.Empty;
            string merchantId = string.Empty;
            string merchantToken = string.Empty;
            try
            {
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if ((search != null) && !string.IsNullOrEmpty(search.SelectedHotelCode))
                {
                    var hotelMerchantInfo = GetMerchantByHotelOperaId(search.SelectedHotelCode);
                    if (hotelMerchantInfo != null)
                    {
                        NetsRegisterRequest netsRegisterRequest = new NetsRegisterRequest();
                        netsRegisterRequest.MerchantId = hotelMerchantInfo.MerchantID;
                        merchantId = hotelMerchantInfo.MerchantID;
                        merchantToken = hotelMerchantInfo.MerchantToken;
                        netsRegisterRequest.MerchantToken = hotelMerchantInfo.MerchantToken;
                        netsRegisterRequest.OrderAmount = prepaidAmount;
                        netsRegisterRequest.MerchantCurrencyCode = hotelMerchantInfo.CurrencyCode;
                        netsRegisterRequest.Language = CMSSessionWrapper.CurrentLanguage;
                        netsRegisterRequest.OrderNumber = string.Format("{0}-{1}", search.SelectedHotelCode, ReservationNumberSessionWrapper.ReservationNumber);
                        if (!string.IsNullOrEmpty(panHash))
                        {
                            netsRegisterRequest.PanHash = panHash;
                        }
                        transactionId = paymentHelper.RegisterMobileRequest(netsRegisterRequest);
                    }
                }
            }
            catch (FaultException faultException)
            {
                if (faultException.GetType() == typeof(FaultException<AuthenticationException>))
                {
                    EmailEntity emailEntity = new EmailEntity();
                    emailEntity.Recipient = AppConstants.EmailRecipientsForOnlinePaymentFailures;
                    emailEntity.Sender = AppConstants.EmailSenderForOnlinePaymentFailures;
                    emailEntity.Subject = string.Format(
                        "Online payment | Merchant authentication failed while making Nets register. Merchant ID: {0}.", merchantId);
                    emailEntity.Body = "Merchant authentication failed while making Nets register";
                    CommunicationService.SendMail(emailEntity, null);
                    AppLogger.LogOnlinePaymentFatalException(faultException, string.Format("{0}. Merchant ID: {1}.", faultException.Message, merchantId));
                }
                else
                {
                    ProcessErrorMessageForNetsErrorCode(string.Empty, NetsConstants.Register);
                    AppLogger.LogOnlinePaymentFatalException(faultException, string.Format("{0}. Merchant ID: {1}.", faultException.Message, merchantId));
                }
            }
            catch (Exception ex)
            {
                ProcessErrorMessageForNetsErrorCode(string.Empty, NetsConstants.Register);
                AppLogger.LogOnlinePaymentFatalException(ex, string.Format("{0}. Merchant ID: {1}.", ex.Message, merchantId));
            }
            return transactionId;
        }

        /// <summary>
        /// Does MakePayment call to update Opera after Nets capture.
        /// </summary>
        /// <param name="paymentInfo"></param>
        public bool MakePayment(ScanwebEntities.PaymentInfo paymentInfo)
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            List<GuestInformationEntity> guestInformationEntities = Reservation2SessionWrapper.PaymentGuestInformation;
            List<HotelRoomRateEntity> hotelRoomRateEntities = Reservation2SessionWrapper.PaymentHotelRoomRateInformation;
            bool makePaymentStatus = false;
            if ((search != null) && (guestInformationEntities != null) && (hotelRoomRateEntities != null)
                && (guestInformationEntities.Count == search.ListRooms.Count))
            {
                for (int roomCount = 0; roomCount < guestInformationEntities.Count; roomCount++)
                {
                    GuestInformationEntity guestInformationEntity = guestInformationEntities[roomCount];
                    if (guestInformationEntity.GuranteeInformation.GuranteeType == GuranteeType.PREPAIDINPROGRESS)
                    {

                        makePaymentStatus = m_AdvancedReservationDomain.MakePayment(guestInformationEntity.ReservationNumber,
                                              guestInformationEntity.LegNumber, search.SelectedHotelCode, AppConstants.CHAIN_CODE,
                                              hotelRoomRateEntities[roomCount].TotalRate.Rate.ToString(), paymentInfo,
                                              Reservation2SessionWrapper.PaymentTransactionId);
                    }
                }
            }
            return makePaymentStatus;
        }

        /// <summary>
        /// Does MakePayment call to update Opera after Nets capture.
        /// </summary>
        /// <param name="paymentInfo"></param>
        public bool MakePaymentForMobile(ScanwebEntities.PaymentInfo paymentInfo)
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            bool makePaymentStatus = false;
            if (search != null)
            {
                makePaymentStatus = m_AdvancedReservationDomain.MakePayment(ReservationNumberSessionWrapper.ReservationNumber, "1",
                                                        search.SelectedHotelCode, AppConstants.CHAIN_CODE,
                                                        Reservation2SessionWrapper.PaymentOrderAmount, paymentInfo,
                                                        Reservation2SessionWrapper.PaymentTransactionId);
            }
            return makePaymentStatus;
        }

        /// <summary>
        /// Gets credit card by Pan hash.
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="panHash"></param>
        /// <returns></returns>
        public CreditCardEntity GetCreditCardByPanHash(string nameId, string panHash)
        {
            if (string.IsNullOrEmpty(nameId))
            {
                nameId = LoyaltyDetailsSessionWrapper.LoyaltyDetails.NameID;
            }
            return m_LoyaltyDomain.FetchCreditCardDetailsByPanHash(nameId, panHash);
        }

        /// <summary>
        /// Process error message for Nets error code and updates the corresponding session variable.
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="errorSource"></param>
        /// <returns></returns>
        public void ProcessErrorMessageForNetsErrorCode(string errorCode, string errorSource)
        {
            string transactionId = Reservation2SessionWrapper.PaymentTransactionId;
            string merchantId = string.Empty;
            string merchantToken = string.Empty;
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            MerchantProfileEntity hotelMerchantInfo = null;
            if ((search != null) && !string.IsNullOrEmpty(search.SelectedHotelCode))
            {
                hotelMerchantInfo = GetMerchantByHotelOperaId(search.SelectedHotelCode);
                if (hotelMerchantInfo != null)
                {
                    merchantId = hotelMerchantInfo.MerchantID;
                    merchantToken = hotelMerchantInfo.MerchantToken;
                }
            }
            var paymentHelper = new PaymentHelper();
            if (!string.IsNullOrEmpty(merchantId) && !string.IsNullOrEmpty(merchantToken) && !string.IsNullOrEmpty(transactionId)
                && string.IsNullOrEmpty(errorCode))
            {
                errorCode = paymentHelper.GetResponseCode(transactionId, merchantId, merchantToken);
            }
            NetsError netsError = paymentHelper.GetNetsErrorDetails(errorCode, errorSource, transactionId, merchantId, merchantToken);
            if (netsError != null)
            {
                ExtractUserFriendlyErrorMessageForNetsErrorCode(netsError, errorSource, hotelMerchantInfo.MerchantID);
            }
        }

        public bool CaptureCall(string transactionID, string captureAmount, string hotelOperaCode)
        {
            AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper : Capture call Start");
            PaymentHelper paymentHelper = new PaymentHelper();
            bool returnValue = false;
            if (string.IsNullOrEmpty(hotelOperaCode))
            {
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if ((search != null) && !string.IsNullOrEmpty(search.SelectedHotelCode))
                {
                    hotelOperaCode = search.SelectedHotelCode;
                }
            }
            var hotelMerchantInfo = GetMerchantByHotelOperaId(hotelOperaCode);
            NetsError netsError = new NetsError();
            if (hotelMerchantInfo != null)
            {
                int noOfTimesToBeExecuted = 1;
                int timeOutValue = 1;
                int count = 0;
                bool bStatus = false;
                do
                {
                    bStatus = paymentHelper.CaptureProcess(transactionID, captureAmount, hotelMerchantInfo.MerchantID,
                                                           hotelMerchantInfo.MerchantToken, out netsError).Equals(
                                                               SessionBookingConstants.NETS_PAYMENT_RESPONSECODE,
                                                               StringComparison.InvariantCultureIgnoreCase);
                    count++;
                    if (!bStatus)
                    {
                        noOfTimesToBeExecuted = noOfTimesToBeExecuted > 1 ? noOfTimesToBeExecuted : int.Parse(ConfigurationManager.AppSettings["CaptureCallCount"]);
                        timeOutValue = timeOutValue > 1 ? timeOutValue : int.Parse(ConfigurationManager.AppSettings["TimerIntervalValue"]);
                        System.Threading.Thread.Sleep(timeOutValue);
                        AppLogger.LogOnlinePaymentInfoMessage(string.Format("Error processing capture call. " + netsError.ErrorDescription +
                                          ". Retrying processing {0} out of {1} times .", count, noOfTimesToBeExecuted));
                    }
                } while ((count <= noOfTimesToBeExecuted) && !bStatus);

                string netsErrorMessage = string.Empty;
                
                if (netsError != null)
                {
                    netsErrorMessage = string.Format(" Error reason is {0} and error code is {1}.", netsError.ErrorDescription, netsError.ErrorCode);
                }

                AppLogger.LogOnlinePaymentTransactionsInfoMessage(
                   string.Format("In Capture Call for Nets Payment Flow with TransactionID: {0}, Payment Capture Status: {6}, MerchantID: {5} and the BookingID: {1} for the hotel: {2}, from the WebServer: {3} at {4}.{7}",
                  transactionID, ReservationNumberSessionWrapper.ReservationNumber, hotelOperaCode, System.Environment.MachineName, DateTime.Now, hotelMerchantInfo.MerchantID, bStatus.ToString(), netsErrorMessage));

                returnValue = bStatus;
                if (!returnValue)
                {
                    ExtractUserFriendlyErrorMessageForNetsErrorCode(netsError, NetsConstants.Capture, hotelMerchantInfo.MerchantID);
                    Reservation2SessionWrapper.NetsPaymentErrorMessage = string.Empty;
                    Reservation2SessionWrapper.NetsPaymentInfoMessage = string.Empty;
                }
            }
            AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper : Capture call end");
            return returnValue;
        }

        /// <summary>
        /// Function Thatb calls webutil function to unblock the rooms
        /// </summary>
        public void IgnoreBooking()
        {
            try
            {
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                if (search != null && search.ListRooms != null && search.ListRooms.Count > 0)
                {
                    for (int iListRoom = 0; iListRoom < search.ListRooms.Count; iListRoom++)
                    {
                        if (search.ListRooms[iListRoom].IsBlocked &&
                            (!string.IsNullOrEmpty(search.ListRooms[iListRoom].RoomBlockReservationNumber)) &&
                            (search.ListRooms[iListRoom].IsRoomModifiable))
                        {
                            IgnoreBookingEntity ignoreBookingEntity = new IgnoreBookingEntity();
                            ignoreBookingEntity.ReservationNumber = search.ListRooms[iListRoom].RoomBlockReservationNumber;
                            ignoreBookingEntity.LegNumber = search.ListRooms[iListRoom].RoomBlockReservationLegNumber;
                            ignoreBookingEntity.HotelCode = search.SelectedHotelCode;
                            ignoreBookingEntity.ChainCode = AppConstants.CHAIN_CODE;
                            m_ReservationDomain.IgnoreBooking(ignoreBookingEntity);
                            search.ListRooms[iListRoom].RoomBlockReservationNumber = string.Empty;
                            search.ListRooms[iListRoom].IsBlocked = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, ex.Message);
            }

        }
        #endregion
    }
}