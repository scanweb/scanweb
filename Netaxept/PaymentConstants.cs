﻿namespace Netaxept
{
    #region Netaxept Constant
    public class NetsConstants
    {
        internal const string S = "S";
        public const string OK = "OK";
        internal const string True = "True";
        internal const string Hyphen = "-";

        internal const string RoomName = "RoomName";
        internal const string NoofAdults = "NoofAdults";
        internal const string Children = "Children";
        internal const string RateCategory = "RateCategory";
        internal const string Rate = "Rate";
        internal const string ChargedNowCaptionForRoom = "ChargedNowCaptionForRoom";
        internal const string Comma = ",";
        internal const string HTML = "HTML";
        internal const string Error = "ERROR";
        internal const string Dot = ".";
        internal const string ErrorMessagesXmlFileName = "ErrorMessages.xml";

        #region NetsKeyValue
        internal const string FirstBreadcrumb = "FirstBreadcrumb";
        internal const string SecondBreadcrumb = "SecondBreadcrumb";
        internal const string ThirdBreadcrumb = "ThirdBreadcrumb";
        internal const string FourBreadcrumb = "FourBreadcrumb";
        internal const string FiveBreadcrumb = "FiveBreadcrumb";
        internal const string HotelName = "HotelName";
        internal const string HotelImage = "HotelImage";
        internal const string StartDayandDate = "StartDayandDate";
        internal const string EndDayandDate = "EndDayandDate";
        internal const string ChargedTotal = "ChargedTotal";
        internal const string BookingTotal = "BookingTotal";
        internal const string CurrencyCode = "CurrencyCode";
        internal const string RightHeaderHeading = "RightHeaderHeading";
        internal const string ChargedNowCaption = "ChargedNowCaption";
        internal const string BookingTotalCaption = "BookingTotalCaption";
        internal const string NetsLanguageCode = "NetsLanguageCode";
        internal const string TaxandFeesCaption = "TaxandFeesCaption";
        internal const string MerchantCurrencyCode = "MerchantCurrencyCode";
        internal const string MerchantName = "MerchantName";
        internal const string Amount = "Amount";
        internal const string OrderNumber = "OrderNumber";
        internal const string OrderDescription = "OrderDescription";
        internal const string PaymentDeclaration = "PaymentDeclaration";
        internal const string PaymentHeaderImageUrl = "PaymentHeaderImageUrl";
        internal const string CreditCardIconsUrl = "CreditCardIconsUrl";
        internal const string DivCustomPaymentInfo = "DivCustomPaymentInfo";
        #endregion

        internal const string HtmlSpace = "&nbsp";
        internal const string CatchXML = "CatchXML";

        #region RoomTemplateNames
        internal const string SingleRoom1 = "SingleRoom1";
        internal const string MultiRoom2 = "MultiRoom2";
        internal const string MultiRoom3 = "MultiRoom3";
        internal const string MultiRoom4 = "MultiRoom4";
        #endregion

        #region ErrorXMLAttribute
        internal const string ErrorMessage = "ErrorMessage";
        internal const string ShortName = "shortName";
        internal const string ErrorCode = "errorCode";
        internal const string ErrorDesc = "errorDesc";
        internal const string IsEmailToBeSent = "isEmailToBeSent";
        internal const string GenericErrorCode = "GENERIC";
        public const string CancelErrorCode = "Cancel";
        #endregion

        public const string Auth = "Auth";
        public const string Annul = "Annul";
        public const string Capture = "Capture";
        public const string Register = "Register";

        #region TemplateConstant

        internal const string TemplateName = "NewMiniTemplate";
        internal const string MiniLayout = "mini";
        internal const string StartCurlyBracesForKey = "{";
        internal const string EndCurlyBracesForKey = " /}";

        //internal const string TemplateHeader = "<div><div><div style='background: none repeat scroll 0 0 #7C4277;'><h2 style='color: #FFFFFF; font-family: PopulaireforScandicRegular,Helvetica neue,Helvetica,Arial,sans-serif; font-size: 15px; font-weight: bold; text-transform: uppercase; padding: 10px; margin: 0px;'><span>{RightHeaderHeading /}</span></h2></div></div><div style='margin-bottom: 10px;'><p style='padding-left: 12px;'><a style='color: #218DA3; font-weight: 700;'>{HotelName /}</a><br><span>{StartDayandDate /}</span> - <span>{EndDayandDate /}</span></p><p align='center'><img src='{HotelImage /}' style='height: 131px; width: 200px; border-width: 0px; display: block'></p></div>";
        internal const string TemplateHeader = "<div><div><div style='background: none repeat scroll 0 0 #7C4277;'><h2 style='color: #FFFFFF; font-family: PopulaireforScandicRegular,Helvetica neue,Helvetica,Arial,sans-serif; font-size: 15px; font-weight: bold; text-transform: uppercase; padding: 10px; margin: 0px;'><span>{0}</span></h2></div></div><div style='margin-bottom: 10px;'><p style='padding-left: 12px;'><a style='color: #218DA3; font-weight: 700;'>{1}</a><br><span>{2}</span> - <span>{3}</span></p><p align='center'><img src='{4}' style='height: 131px; width: 200px; border-width: 0px; display: block'></p></div>";

        internal const string Room = "<div style='border-top: 1px solid #cccccc;'><div style='padding: 1px 0px 5px 2px; margin: 10px;'><div style='background: url(https://www.scandichotels.com/Templates/Booking/Styles/Default/Images/reservation2.0/roundedCornerSprite.png) no-repeat scroll left top transparent; background-position: 0 -1165px; height: 3px;'></div> <div style='background: url(https://www.scandichotels.com/Templates/Booking/Styles/Default/Images/reservation2.0/roundedCornerThinBrdSmallBox.png) repeat-y scroll left top transparent; padding: 4px 0px 10px 10px; margin-top: 0px; width: 196px;'><p style='margin: 0; padding: 2px 0 5px 0'><strong>{0}</strong></p><div style='background: url(https://www.scandichotels.com/Templates/Booking/Styles/Default/Images/reservation2.0/hr_module10.jpg) repeat-x scroll left top transparent; height: 1px; width: 181px;'></div><p><span>{1}</span><span>{2}</span></p><div style='display: block;'><div><div style='float: left;'>{3}&nbsp;</div><div style='float: right; padding-right: 10px; font-weight: bold'> {4} {6}&nbsp;</div><div style='float: left;width:181px; padding-right: 7px; font-weight: bold;'>{5}&nbsp;</div><div style='clear: both;'></div></div></div></div><div style='background: url(https://www.scandichotels.com/Templates/Booking/Styles/Default/Images/reservation2.0/roundedCornerSprite.png) no-repeat scroll left top transparent; background-position: 0 -1178px; height: 12px;'></div></div>";
        internal const string CloseDiv = "</div>";
        //internal const string ChargedNowFooter = "<div style='display: block;'><div style='clear: both; padding: 15px 0 4px 5px; width: 200px;'><h5 style='color: #000000; font-size: 12px; font-weight: bold; margin: 0 0 8px 8px; padding: 0;'>{ChargedNowCaption /}</h5><div><div style='text-align: right;'><span style='color: #1B1818; font-size: 25px; font-weight: normal; line-height: 1.1em;'>{ChargedTotal /}</span><span>{CurrencyCode /}</span><div><span>{TaxandFeesCaption /}</span></div></div><div></div></div><div></div></div></div>";
        //internal const string BookingFooter = "<div style='display: block;'><div style='clear: both; padding: 15px 0 4px 5px; width: 200px;'> <h5 style='color: #000000; font-size: 12px; font-weight: bold; margin: 0 0 8px 8px; padding: 0;'>{BookingTotalCaption /}</h5><div><div style='text-align: right;'><span style='color: #1B1818; font-size: 25px; font-weight: normal; line-height: 1.1em;'>{BookingTotal /}</span><span>{CurrencyCode /}</span><div><span>{TaxandFeesCaption /}</span></div></div><div></div></div><div></div></div></div></div>";

        internal const string ChargedNowFooter = "<div style='display: block;'><div style='clear: both; padding: 15px 0 4px 5px; width: 200px;'><h5 style='color: #000000; font-size: 12px; font-weight: bold; margin: 0 0 8px 8px; padding: 0;'>{0}</h5><div><div style='text-align: right;'><span style='color: #1B1818; font-size: 25px; font-weight: normal; line-height: 1.1em;'>{1}</span><span>{2}</span><div><span>{3}</span></div></div><div></div></div><div></div></div></div>";
        internal const string BookingFooter = "<div style='display: block;'><div style='clear: both; padding: 15px 0 4px 5px; width: 200px;'> <h5 style='color: #000000; font-size: 12px; font-weight: bold; margin: 0 0 8px 8px; padding: 0;'>{0}</h5><div><div style='text-align: right;'><span style='color: #1B1818; font-size: 25px; font-weight: normal; line-height: 1.1em;'>{1}</span><span>{2}</span><div><span>{3}</span></div></div><div></div></div><div></div></div></div></div>";
       
        //internal const string DivCustomPaymentDisplayBlocKHTML = "<div style='display: block;'>";
        internal const string DivCustomPaymentDisplayNoneHTML = "<div style='display: none;'>";

        internal const string DivCustomPaymentDisplayBlocKHTML =
            "<div style='display: block;padding-left:4px;'>" +
            "<div style='font-weight:bold; padding-bottom:5px'><span style='float:left;'>{0}</span><span style='font-weight:normal;display: block;float: right;font-weight: normal;margin-left: 0;text-align: left;width: 540px;'>{1} </span></div>" +
            "<div style='font-weight:bold; padding-bottom:5px; clear: both;'><span style='float:left;'>{2} </span> <span style='font-weight:normal;display: block;float: right;font-weight: normal;margin-left: 0;text-align: left; width: 540px;'>{3} ({4})</span></div>" +
            "<div style='font-weight:bold; padding-bottom:5px; clear: both;'> <span style='float:left;'>{5}</span> <span style='font-weight:normal;display: block;float: right;font-weight: normal;margin-left: 0;text-align: left; width: 540px;'>{6}</span></div>" +
            "  <div style='font-weight:bold; clear: both;'><span style='float:left;'>{7} </span><span style='font-weight:normal;display: block;float: right;font-weight: normal;margin-left: 0;text-align: left; width: 540px;padding-bottom: 15px;'>{8}</span>" +
            "</div><br style='clear:both;'>{9} <div style='padding-right:15px;clear:both;padding-top:4px;'><hr></div></div>";

        #endregion
    }
    #endregion
}
