﻿//  Description					: Contains members that are required for payment register.//
//								                                                          //
//----------------------------------------------------------------------------------------//
//  Author						: Sapient                                                 //
//  Author email id				:                           							  //
//  Creation Date				: 18th December 2012									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


using System.Collections.Generic;
using System.Configuration;

namespace Nets.Entities
{
    public class NetsRegisterRequest
    {
        /// <summary>
        /// Gets/Sets MerchantId
        /// </summary>
        [SettingsDescription("MerchantId")]
        public string MerchantId { set; get; }

        /// <summary>
        /// Gets/Sets MerchantToken
        /// </summary>
        [SettingsDescription("MerchantToken")]
        public string MerchantToken { set; get; }

        /// <summary>
        /// OrderDescription
        /// </summary>
        [SettingsDescription("OrderDescription")]
        public string OrderDescription { set; get; }

        /// <summary>
        /// TargetURLToRedirectAfterPaymentSuccessful
        /// </summary>
        [SettingsDescription("TargetUrlToRedirectAfterPaymentSuccessful")]
        public string TargetUrlToRedirectAfterPaymentSuccessful { set; get; }

        /// <summary>
        /// Language
        /// </summary>
        [SettingsDescription("Language")]
        public string Language { set; get; }

        /// <summary>
        /// Amount
        /// </summary>
        [SettingsDescription("OrderAmount")]
        public string OrderAmount { set; get; }

        /// <summary>
        /// CurrencyCode
        /// </summary>
        [SettingsDescription("MerchantCurrencyCode")]
        public string MerchantCurrencyCode { set; get; }

        /// <summary>
        /// OrderNumber
        /// </summary>
        [SettingsDescription("OrderNumber")]
        public string OrderNumber { set; get; }

        /// <summary>
        /// WebServicePlatform
        /// </summary>
        [SettingsDescription("WebServicePlatform")]
        public string WebServicePlatform { set; get; }

        /// <summary>
        /// 
        /// </summary>
        [SettingsDescription("TemplateName")]
        public string TemplateName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [SettingsDescription("PanHash")]
        public string PanHash { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [SettingsDescription("KeyValues")]
        public NetsKeyValue KeyValues { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [SettingsDescription("RoomDetails")]
        public List<NetsRoom> RoomDetails { get; set; }

        /// <summary>
        /// Gets/Sets IsAllRoomBookingsArePrepaid
        /// </summary>
        public bool IsAllRoomBookingsArePrepaid { get; set; }
    }
}
