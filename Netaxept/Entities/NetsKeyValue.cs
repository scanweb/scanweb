﻿using System.Configuration;
namespace Nets.Entities
{
    public class NetsKeyValue
    {
        [SettingsDescription("FirstBreadcrumb")]
        public string FirstBreadcrumb { get; set; }

        [SettingsDescription("SecondBreadcrumb")]
        public string SecondBreadcrumb { get; set; }

        [SettingsDescription("ThirdBreadcrumb")]
        public string ThirdBreadcrumb { get; set; }

        [SettingsDescription("FourBreadcrumb")]
        public string FourBreadcrumb { get; set; }

        [SettingsDescription("FiveBreadcrumb")]
        public string FiveBreadcrumb { get; set; }

        [SettingsDescription("HotelName")]
        public string HotelName { get; set; }

        [SettingsDescription("HotelImage")]
        public string HotelImage { get; set; }

        [SettingsDescription("StartDayandDate")]
        public string StartDayandDate { get; set; }

        [SettingsDescription("EndDayandDate")]
        public string EndDayandDate { get; set; }

        [SettingsDescription("ChargedTotal")]
        public string ChargedTotal { get; set; }

        [SettingsDescription("BookingTotal")]
        public string BookingTotal { get; set; }

        [SettingsDescription("CurrencyCode")]
        public string CurrencyCode { get; set; }

        [SettingsDescription("RightHeaderHeading")]
        public string RightHeaderHeading { get; set; }

        [SettingsDescription("ChargedNowCaption")]
        public string ChargedNowCaption { get; set; }

        [SettingsDescription("BookingTotalCaption")]
        public string BookingTotalCaption { get; set; }

        [SettingsDescription("TaxandFeesCaption")]
        public string TaxandFeesCaption { get; set; }

        [SettingsDescription("MerchantName")]
        public string MerchantName { get; set; }

        [SettingsDescription("Amount")]
        public string Amount { get; set; }

        [SettingsDescription("OrderNumber")]
        public string OrderNumber { get; set; }

        [SettingsDescription("OrderDescription")]
        public string OrderDescription { get; set; }

        [SettingsDescription("ScanwebPaymentDeclaration")]
        public string PaymentDeclaration { get; set; }

        [SettingsDescription("PaymentHeader")]
        public string PaymentHeader { get; set; }

        [SettingsDescription("CreditCardIconsUrl")]
        public string CreditCardIconsUrl { get; set; }

        [SettingsDescription("MerchantNameHeader")]
        public string MerchantNameHeader { get; set; }

        [SettingsDescription("AmountHeader")]
        public string AmountHeader { get; set; }

        [SettingsDescription("OrderNumberHeader")]
        public string OrderNumberHeader { get; set; }

        [SettingsDescription("OrderDescriptionHeader")]
        public string OrderDescriptionHeader { get; set; }
      
    }
}
