﻿namespace Netaxept.Entities
{
    public class NetsError
    {
        #region Properties
        /// <summary>
        /// ShortName
        /// </summary>
        public string ShortName { get; set; }
        
        /// <summary>
        /// ErrorCode
        /// </summary>
        public string ErrorCode { get; set; }

        /// <summary>
        /// ErrorDescription
        /// </summary>
        public string ErrorDescription { get; set; }

        /// <summary>
        /// IsEmailToBeSent
        /// </summary>
        public bool IsEmailToBeSent { get; set; }

        #endregion
    }
}
