﻿using System.Configuration;
namespace Nets.Entities
{
    public class NetsRoom
    {
        [SettingsDescription("RoomName")]
        public string RoomName { get; set; }

        [SettingsDescription("NoofAdults")]
        public string NoofAdults { get; set; }

        [SettingsDescription("Children")]
        public string Children { get; set; }

        [SettingsDescription("RateCategory")]
        public string RateCategory { get; set; }

        [SettingsDescription("Rate")]
        public string Rate { get; set; }

        [SettingsDescription("ChargedNowCaptionForRoom")]
        public string ChargedNowCaptionForRoom { get; set; }
    }
}
