﻿using System.Configuration;

namespace Netaxept
{
    #region Netaxept Config
    class ConfigHelper
    {
        #region Configuration Parameters

        internal static string MerchantId
        {
            get { return ConfigurationManager.AppSettings["merchantId"]; }
        }

        internal static string Token
        {
            get { return ConfigurationManager.AppSettings["token"]; }
        }

        internal static string RedirectUrl
        {
            get { return ConfigurationManager.AppSettings["redirectUrl"]; }
        }

        internal static string RedirectUrlForMobile
        {
            get { return ConfigurationManager.AppSettings["redirectUrlForMobile"]; }
        }

        internal static string TerminalUrl
        {
            get { return ConfigurationManager.AppSettings["terminalUrl"]; }
        }

        internal static string TerminalUrlForMobile
        {
            get { return ConfigurationManager.AppSettings["terminalUrlForMobile"]; }
        }

        internal static string WebServicePlatform
        {
            get { return ConfigurationManager.AppSettings["WebServicePlatform"]; }
        }

        internal static string PreferredLanguage
        {
            get { return ConfigurationManager.AppSettings["PreferredLanguage"]; }
        }

        internal static string GetLanguageCode(string languageKey)
        {
            return ConfigurationManager.AppSettings[string.Format("{0}-{1}",NetsConstants.NetsLanguageCode,languageKey)]; 
        }

        internal static string IsMiniLayout
        {
            get { return ConfigurationManager.AppSettings["IsMiniLayout"]; }
        }

        internal static string TemplateName
        {
            get { return ConfigurationManager.AppSettings["TemplateName"]; }
        }

        internal static string NetsErrorCodesXMLPath
        {
            get
            {
                return ConfigurationManager.AppSettings["NetsErrorCodesXMLPath"];       
            }
        }

        internal static bool HideCustomPaymentSectionForPanHashPayment
        {
            get
            {
                bool hideCustomPaymentSectionForPanHashPayment = true;
                if (ConfigurationManager.AppSettings["HideCustomPaymentSectionForPanHashPayment"] != null)
                {
                    bool.TryParse(
                        ConfigurationManager.AppSettings["HideCustomPaymentSectionForPanHashPayment"],
                        out hideCustomPaymentSectionForPanHashPayment);
                }
                return hideCustomPaymentSectionForPanHashPayment;
            }
        }

        #endregion
    }
    #endregion
}
