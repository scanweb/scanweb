﻿using System;
using System.Web;
using Scandic.Scanweb.Core;
using System.Reflection;
using System.Linq;
using System.Xml.Linq;
using Netaxept.Entities;
using Netaxept.NetsPaymentServices;
using Nets.Entities;
using System.Xml;
using System.IO;
using System.Diagnostics;

namespace Netaxept
{
    public class PaymentHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        string GetSecureUrl()
        {
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:GetSecureUrl() Begin");
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:GetSecureUrl() End");
                return string.Concat(AppConstants.SECURE_PROTOCOL, HttpContext.Current.Request.Url.Host);
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: GetSecureUrl Method");
                return string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paymentRegisterRequest"></param>
        /// <returns></returns>
        public string Register(NetsRegisterRequest paymentRegisterRequest)
        {
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:Register() Begin");
                var client = new NetaxeptClient();
                RegisterRequest registerRequest = new RegisterRequest();
                Terminal terminal = new Terminal();
                terminal.RedirectUrl = string.Concat(GetSecureUrl(), AppConstants.SLASH, paymentRegisterRequest.Language, ConfigHelper.RedirectUrl);
                terminal.Language = ConfigHelper.GetLanguageCode(paymentRegisterRequest.Language);
                terminal.Design = ConfigHelper.TemplateName;
                if (bool.Parse(ConfigHelper.IsMiniLayout)) terminal.Layout = NetsConstants.MiniLayout;
                terminal.TemplateData = GetkeyValuePair(paymentRegisterRequest);
                registerRequest.Terminal = terminal;

                Order order = new Order();
                double orderAmount;
                double.TryParse(paymentRegisterRequest.OrderAmount, out orderAmount);
                order.Amount = Convert.ToString(orderAmount * 100);
                order.CurrencyCode = paymentRegisterRequest.MerchantCurrencyCode;
                order.OrderNumber = paymentRegisterRequest.OrderNumber;
                registerRequest.Order = order;

                Netaxept.NetsPaymentServices.Environment environment = new Netaxept.NetsPaymentServices.Environment();
                environment.WebServicePlatform = ConfigHelper.WebServicePlatform;
                registerRequest.Environment = environment;

                Recurring recurring = new Recurring();
                recurring.Type = NetsConstants.S;
                if (!string.IsNullOrEmpty(paymentRegisterRequest.PanHash)) recurring.PanHash = paymentRegisterRequest.PanHash;
                registerRequest.Recurring = recurring;

                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();
                string merchantId = paymentRegisterRequest.MerchantId;

                var response = client.Register(paymentRegisterRequest.MerchantId, paymentRegisterRequest.MerchantToken, registerRequest);

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = string.Format("{0}, {7}, NETS, Register-MerchantId: {1}, {2}, {3}, {4}, {5}, {6}",
                   response.TransactionId, merchantId, reqStartTime, responseEndTime, timer.Elapsed.ToString(), System.Environment.MachineName, requestSource, paymentRegisterRequest.GetType().Name);

                AppLogger.CSVInfoLogger(strLog);

                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:Register() End");
                return response.TransactionId;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: Register Method");
                throw ex;
            }
            finally
            {
                timer = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paymentRegisterRequest"></param>
        /// <returns></returns>
        public string RegisterMobileRequest(NetsRegisterRequest paymentRegisterRequest)
        {
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:RegisterMobileRequest() Begin");
                var client = new NetaxeptClient();
                RegisterRequest registerRequest = new RegisterRequest();
                Terminal terminal = new Terminal();
                terminal.RedirectUrl = string.Concat(GetSecureUrl(), AppConstants.SLASH, paymentRegisterRequest.Language, ConfigHelper.RedirectUrlForMobile);
                terminal.Language = ConfigHelper.GetLanguageCode(paymentRegisterRequest.Language);

                registerRequest.Terminal = terminal;

                Order order = new Order();
                double orderAmount;
                double.TryParse(paymentRegisterRequest.OrderAmount, out orderAmount);
                order.Amount = Convert.ToString(orderAmount * 100);
                order.CurrencyCode = paymentRegisterRequest.MerchantCurrencyCode;
                order.OrderNumber = paymentRegisterRequest.OrderNumber;
                registerRequest.Order = order;

                Netaxept.NetsPaymentServices.Environment environment = new Netaxept.NetsPaymentServices.Environment();
                environment.WebServicePlatform = ConfigHelper.WebServicePlatform;
                registerRequest.Environment = environment;

                Recurring recurring = new Recurring();
                recurring.Type = NetsConstants.S;
                if (!string.IsNullOrEmpty(paymentRegisterRequest.PanHash)) recurring.PanHash = paymentRegisterRequest.PanHash;
                registerRequest.Recurring = recurring;

                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();
                string merchantId = paymentRegisterRequest.MerchantId;
                var response = client.Register(paymentRegisterRequest.MerchantId, paymentRegisterRequest.MerchantToken, registerRequest);

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = string.Format("{0}, {7}, NETS, RegisterMobileRequest-MerchantId: {1}, {2}, {3}, {4}, {5}, {6}",
                   response.TransactionId, merchantId, reqStartTime, responseEndTime, timer.Elapsed.ToString(), System.Environment.MachineName, requestSource, paymentRegisterRequest.GetType().Name);

                AppLogger.CSVInfoLogger(strLog);

                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:RegisterMobileRequest() End");
                return response.TransactionId;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: RegisterMobileRequest Method");
                return ex.Message;
            }
            finally
            {
                timer = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transcationID"></param>
        /// <returns></returns>
        public string TerminalURL(string merchantId, string transcationID)
        {
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:TerminalURL() Begin");
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:TerminalURL() End");
                return String.Format(ConfigHelper.TerminalUrl, merchantId, transcationID);
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: TerminalURL Method");
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transcationID"></param>
        /// <returns></returns>
        public string TerminalURLForMobile(string merchantId, string transcationID)
        {
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:TerminalURLForMobile() Begin");
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:TerminalURLForMobile() End");
                return String.Format(ConfigHelper.TerminalUrlForMobile, merchantId, transcationID);
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: TerminalURLForMobile Method");
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionID"></param>
        /// <returns></returns>
        public string AuthProcess(string transactionID, string merchantId, string merchantToken)
        {
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:AuthProcess() Begin");
                var client = new NetaxeptClient();
                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();

                var response = client.Process(merchantId, merchantToken, new ProcessRequest
                {
                    Operation = FinancialTransactions.AUTH.ToString(),
                    TransactionId = transactionID
                });

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = string.Format("{0}, ProcessRequest, NETS, AuthProcess-MerchantId: {1}, {2}, {3}, {4}, {5}, {6}",
                   transactionID, merchantId, reqStartTime, responseEndTime, timer.Elapsed.ToString(), System.Environment.MachineName, requestSource);

                AppLogger.CSVInfoLogger(strLog);

                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:AuthProcess() End");
                return response.ResponseCode;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: AuthProcess Method");
                return GetResponseCode(transactionID, merchantId, merchantToken);
            }
            finally
            {
                timer = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionID"></param>
        /// <param name="merchantId"></param>
        /// <param name="merchantToken"></param>
        /// <param name="errorFilePath"></param>
        /// <param name="netsError"></param>
        /// <returns></returns>
        public string AuthProcess(string transactionID, string merchantId, string merchantToken, out NetsError netsError)
        {
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:AuthProcess() Begin");
                netsError = null;
                var client = new NetaxeptClient();
                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();

                var response = client.Process(merchantId, merchantToken, new ProcessRequest
                {
                    Operation = FinancialTransactions.AUTH.ToString(),
                    TransactionId = transactionID
                });

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = string.Format("{0}, ProcessRequest, NETS, AuthProcess With Nets Error-MerchantId: {1}, {2}, {3}, {4}, {5}, {6}",
                    transactionID, merchantId, reqStartTime, responseEndTime, timer.Elapsed.ToString(), System.Environment.MachineName, requestSource);

                AppLogger.CSVInfoLogger(strLog);

                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:AuthProcess() End");
                return response.ResponseCode;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: AuthProcess Method");
                netsError = GetNetsErrorDetails(GetResponseCode(transactionID, merchantId, merchantToken), NetsConstants.Auth,
                            transactionID, merchantId, merchantToken);
                return NetsConstants.Error;
            }
            finally
            {
                timer = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionID"></param>
        /// <param name="merchantId"></param>
        /// <param name="merchantToken"></param>
        /// <returns></returns>
        public string AnnulProcess(string transactionID, string merchantId, string merchantToken)
        {
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:AnnulProcess() Begin");
                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();

                var response = new NetaxeptClient().Process(merchantId, merchantToken, new ProcessRequest
                {
                    Operation = FinancialTransactions.ANNUL.ToString(),
                    TransactionId = transactionID
                });

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = string.Format("{0}, ProcessRequest, NETS, AnnulProcess-MerchantId: {1}, {2}, {3}, {4}, {5}, {6}",
                   transactionID, merchantId, reqStartTime, responseEndTime, timer.Elapsed.ToString(), System.Environment.MachineName, requestSource);

                AppLogger.CSVInfoLogger(strLog);

                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:AnnulProcess() End");
                return response.ResponseCode;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: AnnulProcess Method");
                return GetResponseCodeWithResponseText(transactionID, merchantId, merchantToken);
            }
            finally
            {
                timer = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionID"></param>
        /// <param name="merchantId"></param>
        /// <param name="merchantToken"></param>
        /// <param name="errorFilePath"></param>
        /// <param name="netsError"></param>
        /// <returns></returns>
        public string AnnulProcess(string transactionID, string merchantId, string merchantToken, out NetsError netsError)
        {
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:AnnulProcess() Begin");
                netsError = null;
                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();

                var response = new NetaxeptClient().Process(merchantId, merchantToken, new ProcessRequest
                {
                    Operation = FinancialTransactions.ANNUL.ToString(),
                    TransactionId = transactionID
                });

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = string.Format("{0}, ProcessRequest, NETS, AnnulProcess With Nets Error-MerchantId: {1}, {2}, {3}, {4}, {5}, {6}",
                   transactionID, merchantId, reqStartTime, responseEndTime, timer.Elapsed.ToString(), System.Environment.MachineName, requestSource);

                AppLogger.CSVInfoLogger(strLog);

                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:AnnulProcess() End");
                return response.ResponseCode;
            }
            catch (Exception ex)
            {
                netsError = GetNetsErrorDetails(GetResponseCode(transactionID, merchantId, merchantToken), NetsConstants.Annul,
                            transactionID, merchantId, merchantToken);
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: AnnulProcess Method");
                return NetsConstants.Error;
            }
            finally
            {
                timer = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionID"></param>
        /// <param name="captureAmount"></param>
        /// <param name="merchantId"></param>
        /// <param name="merchantToken"></param>
        /// <returns></returns>
        public string CaptureProcess(string transactionID, string captureAmount, string merchantId, string merchantToken)
        {
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:CaptureProcess() Begin");
                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();

                var response = new NetaxeptClient().Process(merchantId, merchantToken, new ProcessRequest
                {
                    Operation = FinancialTransactions.CAPTURE.ToString(),
                    TransactionId = transactionID,
                    TransactionAmount = (int.Parse(captureAmount) * 100).ToString()
                });

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = string.Format("{0}, ProcessRequest, NETS, CaptureProcess-MerchantId: {1}, {2}, {3}, {4}, {5}, {6}",
                   transactionID, merchantId, reqStartTime, responseEndTime, timer.Elapsed.ToString(), System.Environment.MachineName, requestSource);

                AppLogger.CSVInfoLogger(strLog);

                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:CaptureProcess() End");
                return response.ResponseCode;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: CaptureProcess Method");
                return GetResponseCodeWithResponseText(transactionID, merchantId, merchantToken);
            }
            finally
            {
                timer = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionID"></param>
        /// <param name="captureAmount"></param>
        /// <param name="merchantId"></param>
        /// <param name="merchantToken"></param>
        /// <param name="errorFilePath"></param>
        /// <param name="netsError"></param>
        /// <returns></returns>
        public string CaptureProcess(string transactionID, string captureAmount, string merchantId, string merchantToken, out NetsError netsError)
        {
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:CaptureProcess() Begin");
                netsError = null;
                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();

                var response = new NetaxeptClient().Process(merchantId, merchantToken, new ProcessRequest
                {
                    Operation = FinancialTransactions.CAPTURE.ToString(),
                    TransactionId = transactionID,
                    TransactionAmount = (int.Parse(captureAmount) * 100).ToString()
                });

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = string.Format("{0}, ProcessRequest, NETS, CaptureProcess With Nets Error-MerchantId: {1}, {2}, {3}, {4}, {5}, {6}",
                   transactionID, merchantId, reqStartTime, responseEndTime, timer.Elapsed.ToString(), System.Environment.MachineName, requestSource);

                AppLogger.CSVInfoLogger(strLog);

                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:CaptureProcess() End");
                return response.ResponseCode;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: CaptureProcess Method");
                netsError = GetNetsErrorDetails(GetResponseCode(transactionID, merchantId, merchantToken), NetsConstants.Capture,
                            transactionID, merchantId, merchantToken);
                return NetsConstants.Error;
            }
            finally
            {
                timer = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionID"></param>
        /// <param name="merchantId"></param>
        /// <param name="merchantToken"></param>
        /// <returns></returns>
        public PaymentInfo Query(string transactionID, string merchantId, string merchantToken)
        {
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:Query() Begin");

                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();

                var response = new NetaxeptClient().Query(merchantId, merchantToken, new QueryRequest
                {
                    TransactionId = transactionID
                });

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = string.Format("{0}, ProcessRequest, NETS, Query-MerchantId: {1}, {2}, {3}, {4}, {5}, {6}",
                   transactionID, merchantId, reqStartTime, responseEndTime, timer.Elapsed.ToString(), System.Environment.MachineName, requestSource);

                AppLogger.CSVInfoLogger(strLog);

                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:Query() End");
                return response as PaymentInfo;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: Query Method");
                throw ex;
            }
            finally
            {
                timer = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="filePath"></param>
        /// <param name="financialTransactions"></param>
        /// <returns></returns>
        public NetsError GetNetsErrorDetails(string errorCode, string financialTransactions, string transactionId, string merchantId, string merchantToken)
        {
            AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:ParseXML() Begin");
            string netsErrorCodesXMLFilePath =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            netsErrorCodesXMLFilePath = netsErrorCodesXMLFilePath.Substring(0, netsErrorCodesXMLFilePath.LastIndexOf("\\"));
            XDocument xdoc = XDocument.Load(string.Format("{0}{1}", netsErrorCodesXMLFilePath, ConfigHelper.NetsErrorCodesXMLPath));
            try
            {
                NetsError netserror = (from errorMessage in xdoc.Root.Descendants(financialTransactions.ToString()).Descendants(NetsConstants.ErrorMessage)
                                       where errorMessage.Attribute(NetsConstants.ErrorCode).Value.Equals(errorCode)
                                       select new NetsError
                                       {
                                           ShortName = errorMessage.Attribute(NetsConstants.ShortName).Value,
                                           ErrorCode = errorMessage.Attribute(NetsConstants.ErrorCode).Value,
                                           ErrorDescription = errorMessage.Attribute(NetsConstants.ErrorDesc).Value,
                                           IsEmailToBeSent = Boolean.Parse(errorMessage.Attribute(NetsConstants.IsEmailToBeSent).Value)
                                       }).FirstOrDefault();
                if (netserror == null)
                {
                    string errorCodeFromQuery = string.Empty;
                    string errorDescFromQuery = string.Empty;
                    if (!string.IsNullOrEmpty(transactionId) && !string.IsNullOrEmpty(merchantId) && !string.IsNullOrEmpty(merchantToken))
                    {
                        PaymentError paymentError = GetPaymentError(transactionId, merchantId, merchantToken);
                        if (paymentError != null)
                        {
                            errorCodeFromQuery = paymentError.ResponseCode;
                            errorDescFromQuery = paymentError.ResponseText;
                        }
                    }
                    netserror = (from errorMessage in xdoc.Root.Descendants(financialTransactions.ToString()).Descendants(NetsConstants.ErrorMessage)
                                 where errorMessage.Attribute(NetsConstants.ErrorCode).Value.Equals(NetsConstants.GenericErrorCode)
                                 select new NetsError
                                 {
                                     ShortName = errorMessage.Attribute(NetsConstants.ShortName).Value,
                                     ErrorCode = !string.IsNullOrEmpty(errorCodeFromQuery) ? errorCodeFromQuery : errorMessage.Attribute(NetsConstants.ErrorCode).Value,
                                     ErrorDescription = !string.IsNullOrEmpty(errorDescFromQuery) ? errorDescFromQuery : errorMessage.Attribute(NetsConstants.ErrorDesc).Value,
                                     IsEmailToBeSent = Boolean.Parse(errorMessage.Attribute(NetsConstants.IsEmailToBeSent).Value)
                                 }).FirstOrDefault();
                }
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:ParseXML() End");
                return netserror;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: ParseXML Method");
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionID"></param>
        /// <param name="merchantId"></param>
        /// <param name="merchantToken"></param>
        /// <returns></returns>
        public string GetResponseCode(string transactionID, string merchantId, string merchantToken)
        {
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:GetResponseCode() Begin");
                PaymentError[] paymentError = Query(transactionID, merchantId, merchantToken).ErrorLog;
                #region Sorting Logic
                Array.Sort(paymentError, delegate(PaymentError paymentErrorOne, PaymentError paymentErrorTwo)
                {
                    return paymentErrorOne.DateTime.CompareTo(paymentErrorTwo.DateTime);
                });
                #endregion
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:GetResponseCode() End");
                string responseCode = string.Empty;
                if ((paymentError != null) && (paymentError.Length > 0))
                {
                    responseCode = paymentError[paymentError.Length - 1].ResponseCode;
                }
                return responseCode;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: GetResponseCode Method");
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionID"></param>
        /// <param name="merchantId"></param>
        /// <param name="merchantToken"></param>
        /// <returns></returns>
        public PaymentError GetPaymentError(string transactionID, string merchantId, string merchantToken)
        {
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:GetResponseCode() Begin");
                PaymentError[] paymentError = Query(transactionID, merchantId, merchantToken).ErrorLog;
                #region Sorting Logic
                Array.Sort(paymentError, delegate(PaymentError paymentErrorOne, PaymentError paymentErrorTwo)
                {
                    return paymentErrorOne.DateTime.CompareTo(paymentErrorTwo.DateTime);
                });
                #endregion
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:GetResponseCode() End");
                if ((paymentError != null) && (paymentError.Length > 0))
                {
                    return paymentError[paymentError.Length - 1];
                }
                return null;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: GetResponseCode Method");
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionID"></param>
        /// <param name="merchantId"></param>
        /// <param name="merchantToken"></param>
        /// <returns></returns>
        private string GetResponseCodeWithResponseText(string transactionID, string merchantId, string merchantToken)
        {
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:GetResponseCodeWithResponseText() Begin");
                PaymentError[] paymentError = Query(transactionID, merchantId, merchantToken).ErrorLog;
                #region Sorting Logic
                Array.Sort(paymentError, delegate(PaymentError paymentErrorOne, PaymentError paymentErrorTwo)
                {
                    return paymentErrorOne.DateTime.CompareTo(paymentErrorTwo.DateTime);
                });
                #endregion
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:GetResponseCodeWithResponseText() End");
                return paymentError[paymentError.Length - 1].ResponseCode + NetsConstants.Hyphen + paymentError[paymentError.Length - 1].ResponseText;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: GetResponseCodeWithResponseText Method");
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paymentRegisterRequest"></param>
        /// <returns></returns>
        private KeyValuePair[] GetkeyValuePair(NetsRegisterRequest paymentRegisterRequest)
        {
            try
            {
                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:GetkeyValuePair() Begin");
                Type typeofNetsKeyValue = typeof(NetsKeyValue);
                int attributeCountForKeyValue = typeofNetsKeyValue.GetProperties().Count();

                Type typeNetsRoom = typeof(NetsRoom);
                int attributeCountForNetsRoom = typeNetsRoom.GetProperties().Count();

                int roomLength = 0;
                roomLength = (paymentRegisterRequest.RoomDetails == null) ? 0 : paymentRegisterRequest.RoomDetails.Count;
                int keyValuePairLength = attributeCountForKeyValue + ((attributeCountForNetsRoom) * roomLength);

                KeyValuePair[] resultDictionary = new KeyValuePair[keyValuePairLength + 2];
                KeyValuePair objKeyValuePair;

                int k = 0;
                #region Passing the Dynamic HTML content for the right Panel
                objKeyValuePair = new KeyValuePair();
                objKeyValuePair.Key = NetsConstants.HTML;
                objKeyValuePair.Value = ReturnHtmlForRightPanel(roomLength, attributeCountForNetsRoom, paymentRegisterRequest);
                resultDictionary[k] = objKeyValuePair;
                #endregion
                k++;
                //Reading and Assigning Dynamically property name as key and room value to the KeyValue Pair object.
                if (roomLength > 0)
                {
                    for (int i = 1; i <= paymentRegisterRequest.RoomDetails.Count; i++)
                    {
                        foreach (var prop in typeNetsRoom.GetProperties())
                        {
                            objKeyValuePair = new KeyValuePair();
                            objKeyValuePair.Key = prop.Name + (i).ToString();
                            if (prop.Name == NetsConstants.Children)
                            {
                                objKeyValuePair.Value = string.IsNullOrEmpty(Convert.ToString(prop.GetValue(paymentRegisterRequest.RoomDetails[i - 1], null)))
                                                               ? NetsConstants.HtmlSpace
                                                               : NetsConstants.Comma + NetsConstants.HtmlSpace + prop.GetValue(paymentRegisterRequest.RoomDetails[i - 1], null).ToString();
                            }
                            else
                            {
                                objKeyValuePair.Value = string.IsNullOrEmpty(Convert.ToString(prop.GetValue(paymentRegisterRequest.RoomDetails[i - 1], null)))
                                                                 ? NetsConstants.HtmlSpace
                                                                 : prop.GetValue(paymentRegisterRequest.RoomDetails[i - 1], null).ToString();
                            }
                            resultDictionary[k] = objKeyValuePair;
                            k++;
                        }
                    }
                }

                //Reading and Assigning Dynamically property name as key and their value room value to the KeyValue Pair object.
                foreach (var prop in typeofNetsKeyValue.GetProperties())
                {
                    objKeyValuePair = new KeyValuePair();
                    objKeyValuePair.Key = prop.Name;
                    objKeyValuePair.Value = string.IsNullOrEmpty(Convert.ToString(prop.GetValue(paymentRegisterRequest.KeyValues, null)))
                                                            ? NetsConstants.HtmlSpace
                                                            : prop.GetValue(paymentRegisterRequest.KeyValues, null).ToString();
                    resultDictionary[k] = objKeyValuePair;
                    k++;
                }
                KeyValuePair divCustomPaymentDetails = new KeyValuePair();
                divCustomPaymentDetails.Key = NetsConstants.DivCustomPaymentInfo;
                if (!string.IsNullOrEmpty(paymentRegisterRequest.PanHash) && ConfigHelper.HideCustomPaymentSectionForPanHashPayment)
                {
                    divCustomPaymentDetails.Value = NetsConstants.DivCustomPaymentDisplayNoneHTML;
                }
                else
                {
                    divCustomPaymentDetails.Value = string.Format(NetsConstants.DivCustomPaymentDisplayBlocKHTML,
                                                                  paymentRegisterRequest.KeyValues.MerchantNameHeader,
                                                                  paymentRegisterRequest.KeyValues.MerchantName,
                                                                  paymentRegisterRequest.KeyValues.AmountHeader,
                                                                  paymentRegisterRequest.KeyValues.Amount,
                                                                  paymentRegisterRequest.KeyValues.CurrencyCode,
                                                                  paymentRegisterRequest.KeyValues.OrderNumberHeader,
                                                                  paymentRegisterRequest.KeyValues.OrderNumber,
                                                                  paymentRegisterRequest.KeyValues
                                                                                        .OrderDescriptionHeader,
                                                                  paymentRegisterRequest.KeyValues.OrderDescription,
                                                                  paymentRegisterRequest.KeyValues.CreditCardIconsUrl);
                }
                resultDictionary[k] = divCustomPaymentDetails;

                AppLogger.LogOnlinePaymentInfoMessage("PaymentHelper:GetkeyValuePair() End");

                return resultDictionary;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: Register Method");
                return null;
            }
        }

        /// <summary>
        /// Creating the Dynamic HTML with KeyValuePair based on the Room Details, e.g Room1, Room2, Room3.
        /// </summary>
        /// <param name="roomCount"></param>
        /// <param name="attributeCountForNetsRoomEntity"></param>
        /// <returns></returns>
        private string ReturnHtmlForRightPanel(int roomCount, int attributeCountForNetsRoomEntity, NetsRegisterRequest netsRegisterRequest)
        {
            try
            {
                NetsKeyValue netsKeys = netsRegisterRequest.KeyValues;
                string content = string.Format(NetsConstants.TemplateHeader, netsKeys.RightHeaderHeading, netsKeys.HotelName,
                                    netsKeys.StartDayandDate, netsKeys.EndDayandDate, netsKeys.HotelImage);
                object[] result;
                Type typeNetsRoom = typeof(NetsRoom);
                int k = 0;
                for (int i = 1; i <= roomCount; i++)
                {
                    result = new object[attributeCountForNetsRoomEntity + 1];
                    foreach (var prop in typeNetsRoom.GetProperties())
                    {
                        result[k] = NetsConstants.StartCurlyBracesForKey + prop.Name + (i).ToString() + NetsConstants.EndCurlyBracesForKey;
                        k++;
                    }
                    result[k] = NetsConstants.StartCurlyBracesForKey + NetsConstants.CurrencyCode + NetsConstants.EndCurlyBracesForKey;
                    content += string.Format(NetsConstants.Room, result);
                    k = 0;
                }
                content += NetsConstants.CloseDiv;

                if (!netsRegisterRequest.IsAllRoomBookingsArePrepaid)
                    content += string.Format(NetsConstants.ChargedNowFooter, netsKeys.ChargedNowCaption,
                                             netsKeys.ChargedTotal, netsKeys.CurrencyCode, netsKeys.TaxandFeesCaption);

                content += string.Format(NetsConstants.BookingFooter, netsKeys.BookingTotalCaption,
                    netsKeys.BookingTotal, netsKeys.CurrencyCode, netsKeys.TaxandFeesCaption);
                return content;
            }
            catch (Exception ex)
            {
                AppLogger.LogOnlinePaymentFatalException(ex, "Exception occured in PaymentHelper: ReturnHtmlForRightPanel Method");
                throw ex;
            }
        }
    }
}
