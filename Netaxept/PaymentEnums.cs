﻿namespace Netaxept
{
    #region Netaxept Enum

    enum FinancialTransactions
    {
        AUTH,
        SALE,
        CAPTURE,
        CREDIT,
        ANNUL
    }

    #endregion
}
