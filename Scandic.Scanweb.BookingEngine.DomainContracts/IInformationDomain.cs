﻿//  Description					: InformationDomain contract, holds all required members  //
//								  that are needed to fetch information from OWS.          //
//                                                                                        //			            
//----------------------------------------------------------------------------------------//
//  Author						: Sapient                                                 //
//  Author email id				:                           							  //
//  Creation Date				: 20th August 2007									      //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Information;

namespace Scandic.Scanweb.BookingEngine.DomainContracts
{
    /// <summary>
    /// InformationDomain contract, holds all required members that are needed to fetch information from OWS.   
    /// </summary>
    public interface IInformationDomain
    {
        /// <summary>
        /// GetHotelInformationResponse
        /// </summary>
        /// <param name="HotelCode"></param>
        /// <returns>HotelInformationResponse</returns>
        HotelInformationResponse GetHotelInformationResponse(string HotelCode);

        /// <summary>
        /// Get the Hotel Country Code for the specified hotel code
        /// </summary>
        /// <param name="HotelCode">
        /// HotelCode
        /// </param>
        /// <returns>
        /// HotelCountry Code
        /// </returns>
        string GetHotelCountryCode(string HotelCode);

        /// <summary>
        /// Get the Currency Conversion rate for the destionation Code against the Source currency Code
        /// </summary>
        /// <param name="sourceCurrencyCode">
        /// Source Currency Code
        /// </param>
        /// <param name="destinationCurrencyCode">
        /// Destionation Currency Code
        /// </param>
        /// <param name="value">
        /// Value to be converted
        /// </param>
        /// <returns>
        /// Currency Convertion rate
        /// </returns>
        double GetCurrency(string sourceCurrencyCode, string destinationCurrencyCode, double value);

        /// <summary>
        /// Get the List of Membership Types
        /// </summary>
        Dictionary<string, string> GetMembershipTypes();
        /// <summary>
        /// Gets the list of user interests
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> GetUserInterestTypes();

        /// <summary>
        /// Gets list of configured titles. 
        /// </summary>
        /// <param name="languageCode"></param>
        /// <returns>list of configured titles. </returns>
        Dictionary<string, string> GetConfiguredTitles(string languageCode);
    }
}
