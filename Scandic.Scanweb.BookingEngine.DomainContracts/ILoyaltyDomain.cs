﻿//  Description					: LoyaltyDomain contract, holds all required members that //
//								  are needed to process loyality business operations.     //
//                                 											              //			            
//----------------------------------------------------------------------------------------//
//  Author						: Sapient                                                 //
//  Author email id				:                           							  //
//  Creation Date				: 20th August 2007									      //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using Scandic.Scanweb.BookingEngine.ServiceProxies.Membership;
using Scandic.Scanweb.Entity;
using System.Collections.Generic;
using System.Collections;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Name;

namespace Scandic.Scanweb.BookingEngine.DomainContracts
{
    /// <summary>
    /// LoyaltyDomain contract, holds all required members that are needed to process loyality business operations.
    /// </summary>
    public interface ILoyaltyDomain
    {
        /// <summary>
        /// Get/Set the Primary Language ID.
        /// Fix done for artifact no - artf713949
        /// </summary>
        string PrimaryLanguageID { get; set; }

        /// <summary>
        /// This is method to update the membership with promotion code.
        /// </summary>
        /// <param name="membershipOperaId">
        /// Membership Opera ID
        /// </param>
        /// <param name="promoCode">
        /// Promotion code to be applied to this membership number.
        /// </param>
        /// <returns>
        /// AddPromoSubscriptionResponse
        /// </returns>
        /// <remarks>
        /// </remarks>
        AddPromoSubscriptionResponse UpdateMembershipWithPromoCode(string membershipOperaId, string promoCode);

        /// <summary>
        /// OWS method to fetch all transactions of a member.
        /// </summary>
        /// <param name="membershipOperaId"></param>
        /// <returns></returns>
        FetchMembershipTransactionsResponse FetchMembershipTransactions(string membershipOperaId);

        /// <summary>
        /// Call method to Fetch Privacy Option from OWS
        /// </summary>
        /// <param name="nameId">
        /// NameID
        /// </param>
        /// <param name="informationEmailType">
        /// News Letter option type
        /// </param>
        /// /// <param name="response">
        /// UserProfile response object from Opera
        /// </param>
        /// <returns>
        /// True if option Selected or false if not selected
        /// </returns>
        bool FetchPrivacyOptionUserProfile(string nameId, string informationEmailType, FetchProfileResponse response);

        /// <summary>
        /// Fetch Name Details
        /// </summary>
        /// <param name="nameID">
        /// Name ID of the user
        /// </param>
        /// <returns>
        /// GuestInformationEntity
        /// </returns>
        UserProfileEntity FetchNameUserProfile(string nameID);

        /// <summary>
        /// Fethes the primary address of a profile for a specific language passed.
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="primaryLangID"></param>
        /// <returns></returns>
        UserProfileEntity GetAddressForLanguage(string nameID, string primaryLangID);

        /// <summary>
        /// Fetches the address of a profile which is primary and can be associated with any language.
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        UserProfileEntity FetchAddressUserProfile(string nameID);

        /// <summary>
        /// Fetch the Phone Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// List of PhoneDetailsEntity
        /// </returns>        
        List<PhoneDetailsEntity> FetchPhoneUserProfile(string nameID);

        /// <summary>
        /// Fetch the preference List Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// FetchPreferenceListResponse
        /// </returns>
        UserPreferenceEntity[] FetchPreferencesListUserProfile(string nameID);

        #region Fetch User Details Methods for Booking Detail

        /// <summary>
        /// It will return all membership details by passing nameId and membership type.
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="membershipType"></param>
        /// <returns></returns>
        ArrayList FetchGuestCardList(string nameId, string membershipType);

        /// <summary>
        /// Fetch the Email for the specified Name Id
        /// </summary>
        /// <param name="nameID">
        /// Name Id
        /// </param>
        /// <returns>
        /// EmailDetails Entity
        /// </returns>
        EmailDetailsEntity FetchEmail(string nameID);

        /// <summary>
        /// Fetch the non business Email for the specified Name Id
        /// </summary>
        /// <param name="nameID">
        /// Name Id
        /// </param>
        /// <remarks>
        /// </remarks>
        /// <returns>
        /// EmailDetails Entity
        /// </returns>
        EmailDetailsEntity FetchNonBusinessEmail(string nameID);

        /// <summary>
        /// Fetch the Comments for the specified Name Id
        /// </summary>
        /// <param name="nameID">
        /// Name Id
        /// </param>
        /// <returns>
        /// Comments Entity
        /// </returns>
        CommentsEntity FetchComment(string nameID);

        #endregion

        #region Update User Details Methods

        /// <summary>
        /// Update First name, Last name, Preferred language 
        /// </summary>
        /// <param name="nameID">NameID</param>
        /// <param name="profile">User Profile Entity</param>
        /// <returns>True if the updation is successful else false.</returns>
        bool UpdateName(string nameID, UserProfileEntity profile);

        /// <summary>
        /// Update users Preferred language 
        /// </summary>
        /// <param name="nameID">NameID</param>
        /// <param name="profile">User Profile Entity</param>
		/// <param name="response">FetchName Response</param>
        /// <returns>True if the updation is successful else false.</returns>
        bool UpdatePreferredLanguage(string nameID, UserProfileEntity profile, FetchNameResponse response);

        /// <summary>
        /// Update Comment
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="commentsEntity"></param>
        /// <returns></returns>
        bool InsertComment(string nameId, CommentsEntity commentsEntity);

        /// <summary>
        /// Update Comment
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="commentsEntity"></param>
        /// <returns></returns>
        bool UpdateComment(string nameId, CommentsEntity commentsEntity);

        /// <summary>
        /// Update the Address into the Opera
        /// </summary>
        /// <param name="userProfile">
        /// UserProfileEntity
        /// </param>
        /// <returns>
        /// True if the updation is successful else false.
        /// </returns>
        bool UpdateAddress(UserProfileEntity userProfile);

        /// <summary>
        /// Insert the Profile into OWS
        /// </summary>
        /// <param name="userProfile">
        /// UserProfileEntity
        /// </param>
        /// <returns>
        /// True if the updation is successful else false.
        /// </returns>
        /// <remarks>
        /// </remarks>
        bool InsertAddress(string nameID, UserProfileEntity userProfile);

        /// <summary>
        /// Makes Update Phone OWS Call
        /// </summary>
        /// <param name="phoneID">Phone opera ID for updationP</param>
        /// <param name="phoneNumber">hone Number</param>
        /// <returns>True if the updation is successful else false.</returns>
        bool UpdatePhone(string nameId, long phoneOperaID, string phoneNumber, string phoneType, string phoneRole);

        /// <summary>
        /// Makes Update Email OWS Call
        /// </summary>
        /// <param name="emailOperaID">Email Opera ID</param>
        /// <param name="email">Email</param>
        /// <returns>True if the updation is successful else false.</returns>
        bool UpdateEmail(string nameId, long emailOperaID, string email, bool isPrimary);

        /// <summary>
        /// Make Update Guest Card OWS call.
        /// </summary>
        /// <param name="nameId">Name id for this profile.</param>
        /// <param name="cardOperaID">Member card Opera ID</param>
        /// <param name="membershipType">Membership Type</param>
        /// <param name="membershipNumber">Membership Number</param>
        /// <param name="memberName">Member Name</param>
        /// <param name="displaySequence">Display sequence for this partner program.</param>
        /// <returns>True if the updation is successful else false.</returns>
        bool UpdateGuestCardDetails(string nameId,
                                    long cardOperaID, string membershipType,
                                    string membershipNumber, string memberName,
                                    int displaySequence);

        /// <summary>
        /// This method will delete the specified guest card identified with parameter 'guestCardOperaId' from the profile.
        /// </summary>
        /// <param name="guestCardOperaId">Opera id for this Guest card </param>
        bool DeleteGuestCard(string guestCardOperaId);


        /// <summary>
        /// To update Preferences this method will call delete existing preferences then Insert preferences.
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="userInterests"></param>
		/// <param name="userInterestsListOld"></param>
        /// <returns></returns>
        bool UpdateInterestsListUserProfile(string nameId, UserInterestsEntity[] userInterestsList, UserInterestsEntity[] userInterestsListOld);

        /// <summary>
        /// To update Preferences this method will call delete existing preferences then Insert preferences.
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="userPreferences"></param>
		/// <param name=userPreferencesOld"></param>
        /// <returns></returns>
        bool UpdatePreferencesListUserProfile(string nameId, UserPreferenceEntity[] userPreferences, UserPreferenceEntity[] userPreferencesOld);

        /// <summary>
        /// Updates password
        /// </summary>
        /// <param name="MembershipId"></param>
        /// <param name="OldPassword"></param>
        /// <param name="NewPassword"></param>
        /// <returns></returns>
        bool UpdatePassword(string MembershipId, string OldPassword, string NewPassword);

        #endregion

        #region Delete OWS calls

        /// <summary>
        /// Deletes preferences
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="preferenceType"></param>
        /// <param name="preferenceValue"></param>
        /// <returns></returns>
        bool DeletePreferences(string nameID, string preferenceType, string preferenceValue);

        /// <summary>
        /// Delete the Phone Number for the specified Opera Id
        /// </summary>
        /// <param name="phoneOperaID">
        /// Opera ID
        /// </param>
        /// <returns>
        /// True, if successful else false
        /// </returns>
        bool DeletePhone(long phoneOperaID);

        /// <summary>
        /// Delete the GuestCard Details for the specified Opera Id
        /// </summary>
        /// <param name="creditCardOperaID">
        /// GuestCard Opera ID</param>
        /// <returns>
        /// True, if successful else false
        /// </returns>
        bool DeleteGuestCardDetails(long partnerProgramOperaID);

        #endregion

        #region Enroll Guest Program

        /// <summary>
        ///  Method to Register User & getting the registration number.
        /// </summary>
        /// <param name="profile">User Detail </param>        
        /// <returns>Confirmation Id</returns>        
        string EnrollUser(UserProfileEntity profile);

        #endregion

        #region Register User

        /// <summary>
        /// To Enroll guest profile & returns NameID
        /// </summary>
        /// <param name="profile">Guest Profile to be registered</param>
        /// <returns>Returns NameID if successfully register user in OWS.</returns>
        string RegisterUser(UserProfileEntity profile);

        #endregion

        #region Insert User detail Name services.

        /// <summary>
        /// This method is used for both Insert & Update the Privacy options
        /// </summary>
        /// <param name="nameID">nameID</param>
        /// <param name="scandicEmail">Scandic Email as True or False</param>
        /// <param name="thirdPartyEmail">Third party Email as True or False</param>
        /// <returns></returns>
        bool InsertUpdatePrivacyUserDetail(string nameID, bool scandicEmail, bool thirdPartyEmail, bool scandicMailList);

        /// <summary>
        /// To fetch the Registration Number to the guest
        /// </summary>
        /// <param name="membershipType">Membership Type</param>
        /// <returns>Registration Number assigned to the guest</returns>
        string FetchNextCardNumber(string membershipType);

        /// <summary>
        /// To create a login & password for the user
        /// </summary>
        /// <param name="loginName">The registration Number  for the guest</param>
        /// <param name="password">Password provided by the guest</param>
        /// <param name="nameID">NameId returned from Register user method.</param>
        /// <returns>Status of the operation, True if successful.</returns>
        bool CreateUser(string loginName, string password, string nameID);

        /// <summary>
        /// Insert User Card Detail with membership level
        /// </summary>
        /// <param name="nameID">NameID of the guest returned from Register User method</param>
        /// <param name="membershipType">Registration type to be inserted e.g. GUESTPR</param>
        /// <param name="membershipNumber">Registration number to be inserted</param>
        /// <param name="membershipLevel">MembershipLevel e.g. FirstFloor etc...</param>
        /// <param name="memberName">Member name</param>
        /// <param name="displaySequence">Display sequence</param>        
        /// <returns>Status of the operation, True if successful.</returns>        
        bool InsertUserGuestCard(string nameID, string membershipType, string membershipNumber,
                                 string membershipLevel, string memberName, int displaySequence);

        /// <summary>
        /// Insert Email ID against the specified NameID
        /// </summary>
        /// <param name="email">
        /// Email ID to the inserted
        /// </param>
        /// <param name="nameID">
        /// Name ID of the Guest
        /// </param>
        /// <returns>
        /// Status of the operation, True if successful.
        /// </returns>
        bool InsertEmail(string email, string nameID);

        /// <summary>
        /// Insert Phone Number against the specified NameID
        /// </summary>
        /// <param name="nameID">
        /// Name ID of the guest
        /// </param>
        /// <param name="phNumber">
        /// Phone number to be inserted
        /// </param>
        /// <param name="phType">
        /// Phone Type
        /// </param>
        /// <param name="phRole">
        /// Phone Role
        /// </param>
        /// <returns>
        /// Status of the operation, True if successful.
        /// </returns>
        bool InsertPhone(string nameID, string phoneNumber, string phoneType, string phoneRole);

        /// <summary>
        /// Method to insert guest Preferences
        /// </summary>
        /// <param name="nameID">NameID of the guest returned from Register User method</param>
        /// <param name="preferenceType">Guest Preference Type</param>
        /// <param name="preferenceValue">Guest Preference value</param>
        /// <returns>Status of the operation, True if successful.</returns>
        bool InsertPreference(string nameID, string preferenceType, string preferenceValue);

        /// <summary>
        /// Method to insert question & answer to retrieve password.
        /// </summary>
        /// <param name="nameID">NameID of the guest returned from Register User method</param>
        /// <param name="question">Security Question Code chosen by guest</param>
        /// <param name="answer">Answer provided by guest</param>
        /// <returns>Status of the operation, True if successful.</returns>
        bool UpdateQuestionNAnswer(string nameID, string question, string answer);

        #region Fetch Password

        /// <summary>
        /// Method returns password of a given membership id.
        /// It returns password if found else return empty string.
        /// </summary>
        /// <param name="membershipId">Membership ID of the User.</param>
        /// <param name="questionId">Unique Id for a question.</param>
        /// <param name="questionString">Question String.</param>
        /// <param name="answer">Answer String.</param>
        /// <returns></returns>
        string FetchPassword(string membershipId, string questionId, string questionString, string answer);

        #endregion Fetch Password

        #endregion

        #region Fetch Name Services

        /// <summary>
        /// Fetch Name Details
        /// </summary>
        /// <param name="nameID">
        /// Name ID of the user
        /// </param>
        /// <returns>
        /// FetchNameResponse
        /// </returns>
        /// <remarks>
        /// This method is made public as it needs to be accessed from the NUnit Component
        /// </remarks>        
        FetchNameResponse FetchNameService(string nameID);

        /// <summary>
        /// Fetches all addresses of a profile for a language.
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="primaryLangID"></param>
        /// <returns></returns>
        FetchAddressListResponse FetchAddressNameService(string nameID, string primaryLangID);

        /// <summary>
        /// Fetch the Phone Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// FetchPhoneListResponse
        /// </returns>
        /// <remarks>
        /// This method is made public as it needs to be accessed from the NUnit Component
        /// </remarks>                
        FetchPhoneListResponse FetchPhoneNameService(string nameID);

        /// <summary>
        /// Fetch the Privacy Option
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// FetchPrivacyOptionResponse
        /// </returns>
        /// <remarks>
        /// This method is made public as it needs to be accessed from the NUnit Component
        /// </remarks>                
        FetchPrivacyOptionResponse FetchPrivacyOption(string nameID);

        /// <summary>
        /// Fetch the preference List Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// FetchPreferenceListResponse
        /// </returns>
        /// <remarks>
        /// This method is made public as it needs to be accessed from the NUnit Component
        /// </remarks>                
        FetchPreferenceListResponse FetchPreferencesListNameService(string nameID);

        /// <summary>
        /// Fetch Partner Program Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// FetchGuestCardListResponse
        /// </returns>
        /// <remarks>
        /// This method is made public as it needs to be accessed from the NUnit Component
        /// </remarks>                
        FetchGuestCardListResponse FetchPartnerProgramNameService(string nameID);

        #endregion

        /// <summary>
        /// Fetch the CreditCard Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// FetchCreditCardListResponse
        /// </returns>
        /// <remarks>
        /// This method is made public as it needs to be accessed from the NUnit Component
        /// </remarks>                
        FetchCreditCardListResponse FetchCreditCardDetailsNameService(string nameID);

        /// <summary>
        /// Fetch the CreditCard Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// Array of CreditCardEntity
        /// </returns>
        Dictionary<string, CreditCardEntity> FetchCreditCardDetailsUserProfile(string nameID);

        /// <summary>
        /// Make Update Credit Card OWS Call.
        /// </summary>
        /// <param name="cardOperaID">Opera Id for updating credit card</param>
        /// <param name="holderName">Holder Name</param>
        /// <param name="cardType">Card Type</param>
        /// <param name="cardNumber">Card Number</param>
        /// <param name="expDate">Expiry Date</param>
        /// <returns>True if the updation is successful else false.</returns>
        bool UpdateCreditCardDetails(CreditCardEntity creditCard,
                                     long creditCardOperaID, bool isPrimary);

        /// <summary>
        /// Insert Credit Card Details
        /// </summary>
        /// <param name="nameID">NameID of the guest returned from Register User method</param>
        /// <param name="userCreditCard">Credit Card Details</param>
        /// <returns>Status of the operation, True if successful.</returns>
        bool InsertCreditCard(string nameID,
                              CreditCardEntity userCreditCard);

        /// <summary>
        /// Delete the Credit Card Details for the specified Opera Id
        /// </summary>
        /// <param name="creditCardOperaID">
        /// Credit Card Opera ID</param>
        /// <returns>
        /// True, if successful else false
        /// </returns>
        bool DeleteCreditCard(long creditCardOperaID);

        #region GetNewPassword

        /// <summary>
        /// Used for resetting password for a particular membership number.
        /// This happens if the guest forgets his password along with secret question and secret answer.
        /// </summary>
        /// <param name="membershipNumber">Membership number for which the password needs to be rested.</param>
        /// <returns>New password.</returns>
        string GetNewPassword(string membershipNumber);
        #endregion GetNewPassword


        #region OWSMonitoring

        /// <summary>
        ///Method call to check the health of OWS-Name service
        /// </summary>
        /// <param name="nameID">Name Id</param>
        /// <returns>True if service is up else False</returns>
        bool FetchNameForOWSMonitor(string nameID);

        /// <summary>
        ///Method call to check the health of OWS-Membership service  
        /// </summary>
        /// <returns>True if service is up else False</returns>
        bool FetchPromoSubscriptionForOWSMonitor();

        /// <summary>
        ///Method call to check the health of OWS-Security service
        /// </summary>
        /// <param name="userName">Membership number as username</param>
        /// <param name="password">Password</param>
        /// <returns>True if service is up else False</returns>
        bool AuthenticateUserForOWSMonitor(string userName, string password);
        #endregion OWSMonitoring

        UserInterestsEntity[] FetchUserInterestsListUserProfile(string nameID);
       
	   /// Fetches only the Prepaid card types if the fetchOnlyPrepaidCCTypes is set to true. otherwise returns all the credit cards.
        /// </summary>
        /// <param name="nameID">Name ID</param>
        /// <param name="fetchOnlyPrepaidCCTypes">sets only prepaid card types only are required or not</param>
        /// <returns>Array of CreditCardEntity</returns>
        Dictionary<string, CreditCardEntity> FetchCreditCardDetailsUserProfile(string nameID,
                                                                               bool fetchOnlyPrepaidCCTypes);

        /// <summary>
        /// Fetches the credit card details which matches with the pan hash
        /// </summary>
        /// <param name="nameID">Name ID</param>
        /// <param name="panHash">the pan hash value of the credit card (NameonCard)</param>
        /// <returns>Array of CreditCardEntity</returns>
        CreditCardEntity FetchCreditCardDetailsByPanHash(string nameID, string panHash);
		
		UserProfileEntity FetchUserProfile(string nameID);
    }
}
