﻿//  Description					: AvailabilityDomain contract, holds all required members //
//								  that are needed to process availability business        //
//                                operations. 											  //			            
//----------------------------------------------------------------------------------------//
//  Author						: Sapient                                                 //
//  Author email id				:                           							  //
//  Creation Date				: 20th August 2007									      //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using System.Collections;

namespace Scandic.Scanweb.BookingEngine.DomainContracts
{
    /// <summary>
    /// AvailabilityDomain contract, holds all required members that are needed to process availability business operations.
    /// </summary>
    public interface IAvailabilityDomain
    {
        #region GeneralAvailability
        /// <summary>
        /// GeneralAvailability
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <param name="roomSearch"></param>
        /// <param name="hotelCode"></param>
        /// <param name="chainCode"></param>
        /// <returns>AvailabilityResponse</returns>
        AvailabilityResponse GeneralAvailability(HotelSearchEntity hotelSearchEntity,
                                                        HotelSearchRoomEntity roomSearch, string hotelCode,
                                                        string chainCode);

        /// <summary>
        /// This method will does the OWS Availability Request on the given
        /// HotelSearchEntity and returns the list of rooms returned from OWS for Redemption Booking
        /// </summary>
        /// <param name="hotelSearchEntity,roomSearch,hotelCode,chainCode,membershipLevel"></param>
        /// <returns>AvailabilityResponse</returns>
        AvailabilityResponse RedemptionAvailability(HotelSearchEntity hotelSearchEntity,
                                                           HotelSearchRoomEntity roomSearch, string hotelCode,
                                                           string chainCode, string membershipLevel);

        /// <summary>
        ///  This method facilitates in creating an instance of corresponding service and makes an asynchronous webservice call.
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <param name="roomSearch"></param>
        /// <param name="hotelCode"></param>
        /// <param name="chainCode"></param>
        /// <param name="availabilityServiceRequestState"></param>
        /// <param name="isMultiDateSearch"></param>
        void GeneralAvailability(HotelSearchEntity hotelSearchEntity,
                                 HotelSearchRoomEntity roomSearch, string hotelCode,
                                 string chainCode, Hashtable availabilityServiceRequestState,
                                 bool isMultiDateSearch);

        /// <summary>
        /// This method facilitates in creating an instance of corresponding service and makes an asynchronous webservice call.
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <param name="roomSearch"></param>
        /// <param name="hotelCode"></param>
        /// <param name="chainCode"></param>
        /// <param name="membershipLevel"></param>
        /// <param name="availabilityServiceRequestState"></param>
        /// <param name="isMultiDateSearch"></param>
        void RedemptionAvailability(HotelSearchEntity hotelSearchEntity,
                                           HotelSearchRoomEntity roomSearch, string hotelCode,
                                           string chainCode, string membershipLevel,
                                           Hashtable availabilityServiceRequestState,
                                           bool isMultiDateSearch);

        #endregion

        #region AuthenticateUser

        /// <summary>
        /// It returns the NameID or OperaID by passing login and password.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>NameID or OperaID</returns>
        string AuthenticateUser(string userName, string password);

        /// <summary>
        ///Method call to check the health of OWS-Availability service 
        /// </summary>
        /// <returns>True if service is up else False</returns>
        bool AvailabilityForOWSMonitor();
        #endregion
    }
}
