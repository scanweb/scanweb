﻿//  Description					: AvailabilityDomain contract, holds all required members //
//								  that are needed to process reservation business         //
//                                operations. 											  //			            
//----------------------------------------------------------------------------------------//
//  Author						: Sapient                                                 //
//  Author email id				:                           							  //
//  Creation Date				: 20th August 2007									      //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Reservation;

namespace Scandic.Scanweb.BookingEngine.DomainContracts
{
    /// <summary>
    /// AvailabilityDomain contract, holds all required members that are needed to process reservation business operations.
    /// </summary>
    public interface IReservationDomain
    {
        /// <summary>
        /// Validate FGPNumber from opera
        /// </summary>
        /// <param name="guestInformation"></param>
        /// <returns>Returns true if FGP Number is valid</returns>
        bool ValidateFGPNumber(GuestInformationEntity guestInformation);

        /// <summary>
        /// Makes the reservation.
        /// </summary>
        /// <param name="hotelSearch">The hotel search.</param>
        /// <param name="roomSearch">The room search.</param>
        /// <param name="roomRate">The room rate.</param>
        /// <param name="guestInformation">The guest information.</param>
        /// <param name="reservationNumber">The reservation number.</param>
        /// <param name="sourceCode">The source code.</param>
        /// <param name="createNameID">The create name ID.</param>
        /// <param name="isSessionBooking">if set to <c>true</c> [is session booking].</param>
        /// <param name="partnerID">The partner ID.</param>
        /// <returns>SortedList</returns>
        SortedList<string, string> MakeReservation(HotelSearchEntity hotelSearch,
                                                   HotelSearchRoomEntity roomSearch, HotelRoomRateEntity roomRate,
                                                   GuestInformationEntity guestInformation,
                                                   string reservationNumber, string sourceCode,
                                                   out string createNameID, bool isSessionBooking,
                                                   string partnerID);

        /// <summary>
        /// Modify the specified Reservation
        /// </summary>
        /// <param name="hotelSearch">
        /// HotelSearch Entity
        /// </param>
        /// <param name="roomSearch"></param>
        /// <param name="roomRate">
        /// HotelRoomRateEntity
        /// </param>
        /// <param name="guestInformation">
        /// GuestInformationEntity
        /// </param>
        /// <param name="prevReservationNumber">
        /// Previous Reservation Number
        /// </param>
        /// <param name="sourceCode"></param>
        /// <param name="createdNameID">
        /// Created Named ID
        /// </param>
        /// <returns>
        /// New Reservation Number
        /// </returns>        
        SortedList<string, string> ModifyReservation(HotelSearchEntity hotelSearch,
                                                            HotelSearchRoomEntity roomSearch,
                                                            HotelRoomRateEntity roomRate,
                                                            GuestInformationEntity guestInformation,
                                                            string prevReservationNumber, string sourceCode,
                                                            out string createdNameID);

        /// <summary>
        /// Cancel the Booking
        /// </summary>
        /// <param name="cancelDetailsEntity">
        /// CancelDetails Entity
        /// </param>
        /// <param name="flag"></param>
        /// <returns>
        /// Cancellation Number
        /// </returns>
        string CancelReservation(ref CancelDetailsEntity cancelDetailsEntity, bool flag);

        /// <summary>
        /// Fetch Reservation for the specified Reservation Number        
        /// </summary>
        /// <param name="reservationNumber">
        /// Reservation Number
        /// </param>
        /// <param name="legNumber"></param>
        /// <returns>BookingDetailsEntity</returns>
        BookingDetailsEntity FetchReservation(string reservationNumber, string legNumber);

        /// <summary>
        /// Call Future Booking Summary Web service Method.
        /// </summary>
        /// <param name="nameId">Name Id</param>
        /// <returns>FutureBookingSummaryResponse</returns>
        FutureBookingSummaryResponse GetCurrentBookingList(string nameId);

        /// <summary>
        /// This method will send the summary for the booking.
        /// Basically this method will get all the bookings.
        /// </summary>
        /// <param name="bookingNumber">
        /// Booking number for which the summary will be needed.
        /// </param>
        /// <returns>Fetch booking summary response.</returns>
        SortedList<string, string> FetchBookingSummary(string bookingNumber);

        /// <summary>
        /// FetchReservationPackages
        /// </summary>
        /// <param name="reservationNumber"></param>
        /// <param name="legNumber"></param>
        /// <param name="hotelCode"></param>
        /// <returns>List of BedAccommodation</returns>
        List<BedAccommodation> FetchReservationPackages(string reservationNumber, string legNumber,
                                                        string hotelCode);

        /// <summary>
        ///Method call to check the health of OWS-Information service
        /// </summary>
        /// <param name="reservationNumber">Reservation number</param>
        /// <returns>True if service is up else False</returns>
        bool FetchReservationForOWSMonitor(string reservationNumber);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ignoreDetailsEntity"></param>
        /// <returns></returns>
        string IgnoreBooking(IgnoreBookingEntity ignoreDetailsEntity);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="confirmBookingEntity"></param>
        /// <returns></returns>
        string ConfirmBooking(ConfirmBookingEntity confirmBookingEntity);

        /// <summary>
        /// AddChildrenAccommodations
        /// </summary>
        /// <param name="reservationNumber"></param>
        /// <param name="legNumber"></param>
        /// <param name="selectedHotelCode"></param>
        /// <param name="guestInfoEntity"></param>
        void AddChildrenAccommodations(string reservationNumber, string legNumber, string selectedHotelCode,
                                              GuestInformationEntity guestInfoEntity);

        /// <summary>
        /// Delete the Child Accommodation Preferences
        /// </summary>
        /// <param name="reservationNumber">Reservation Number</param>
        /// <param name="legNumber">Reservation's Leg Number</param>
        /// <param name="selectedHotelCode">Hotel Code</param>
        /// <param name="guestInfoEntity">GuestInformation Entity</param>
        void DeleteChildAccommodations(string reservationNumber, string legNumber, string selectedHotelCode,
                                              GuestInformationEntity guestInfoEntity);

        SortedList<string, string> ModifyGuaranteeReservation(HotelSearchEntity hotelSearch,
                                                                     HotelSearchRoomEntity roomSearch,
                                                                     HotelRoomRateEntity roomRate,
                                                                     GuestInformationEntity guestInformation,
                                                                     string prevReservationNumber, string sourceCode,
                                                                     out string createdNameID);
    }
}
