﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.DomainContracts
{
    public interface IAdvancedReservationDomain
    {
        /// <summary>
        /// Does MakePayment call to update Opera after Nets capture.
        /// </summary>
        /// <param name="reservationNumber"></param>
        /// <param name="hotelOperaCode"></param>
        /// <param name="hotelChainCode"></param>
        /// <param name="totalAmount"></param>
        /// <param name="paymentInfo"></param>
        /// <param name="transactionId"> </param>
        bool MakePayment(string reservationNumber, string legNumber, string hotelOperaCode, string hotelChainCode,
                         string totalAmount, PaymentInfo paymentInfo, string transactionId);
    }
}
