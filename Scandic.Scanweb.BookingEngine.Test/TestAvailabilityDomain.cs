using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Security;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Membership;
using Scandic.Scanweb.BookingEngine.Entity;
using NUnit.Framework;



namespace Scandic.Scanweb.BookingEngine.Test
{
    [TestFixture]
    public class AvailabilityDomain
    {

        [Test]
        public void RegionalAvailability(HotelSearchEntity SechHotEnt)
        {
        }

        [Test]
        public void GeneralAvailability(HotelSearchEntity hotelSearchEnt, string hotelCode, string chainCode)
        {

        }

        [Test]
        public void AuthenticateUser(string userName, string password)
        {
 
        }

        [Test]
        public void FetchName(string nameId)
        {
        }

        [Test]
        public void FetchGuestCardList(string nameId, string membershipType)
        {

        }

        [Test]
        public void FetchRateAwards(HotelSearchEntity SechHotEnt,
           RegionalAvailableProperty[] RegionalAvailPropList, string MembershipLevel)
        {

        }
    }

}