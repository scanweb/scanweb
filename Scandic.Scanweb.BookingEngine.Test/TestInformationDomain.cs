using System;
using System.Text;
using NUnit.Framework;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Information;
namespace Scandic.Scanweb.BookingEngine.Test
{
    /// <summary>
    /// TestInformationDomain
    /// </summary>
    [TestFixture]
    public class TestInformationDomain
    {
        /// <summary>
        /// TestGetHotelInformationResponse
        /// </summary>
        [Test]
        public void TestGetHotelInformationResponse()
        {
            string hotelCode = "807";
            string CityName = string.Empty;
            string ActualCityName = "STOCKHOLM";
            InformationDomain informationDoimain = new InformationDomain();
            HotelInformationResponse response = informationDoimain.GetHotelInformationResponse(hotelCode);
            if (response.Result.resultStatusFlag != Domain.OWSProxy.Information.ResultStatusFlag.FAIL)
                CityName= response.HotelInformation.HotelContactInformation.Addresses[0].cityName;
            Assert.AreEqual(ActualCityName, CityName);
        }


    }
}
