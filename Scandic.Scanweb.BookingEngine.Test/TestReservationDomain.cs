using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Reservation;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Security;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name;
using Scandic.Scanweb.BookingEngine.ExceptionManager;
using Scandic.Scanweb.BookingEngine.Entity;
namespace Scandic.Scanweb.BookingEngine.Test
{
    [TestFixture]
    public class TestReservationDomain
    {
        #region Test Methods
        #region Existing Methods
        [Test]
        public void TestCurrentBookingList()
        {
            //This test case need to be updated
            string nameId = "44023396";//Any name Id containing any booking
            bool resultStatus = false;
            ReservationDomain reservationDomain = new ReservationDomain();
            FutureBookingSummaryResponse response= reservationDomain.CurrentBookingList(nameId);
            if (response.Result.resultStatusFlag != Domain.OWSProxy.Reservation.ResultStatusFlag.FAIL)
                resultStatus = true;

            Assert.AreEqual(true, resultStatus);


        }
        #endregion Existing Methods

        #region Modify Cancel Module
        [Test]
        public void TestFetchBooking()
        {
            ReservationController rc = new ReservationController();
            HotelSearchEntity hse1 = GetHotelSearchEntity();
            HotelRoomRateEntity hre1 = GetHotelRoomRateEntity();
            GuestInformationEntity gie1 = GetGuestInformationEntity();
            string nameID;
            string resevation_number = rc.CreateBooking(hse1, hre1, gie1, out nameID);

            ReservationDomain rd = new ReservationDomain();

            BookingDetailsEntity bookingDetailsEntity = rd.FetchReservation(resevation_number);
            HotelSearchEntity hse2 = bookingDetailsEntity.HotelSearch;
            HotelRoomRateEntity hre2 = bookingDetailsEntity.HotelRoomRate;
            GuestInformationEntity gie2 = bookingDetailsEntity.GuestInformation;

            #region Assert
            Assert.AreEqual(hse1.AdultsPerRoom, hse2.AdultsPerRoom);
            //Assert.AreEqual(hse1.ArrivalDate, hse2.ArrivalDate); -- Response from OWS is not correct while create booking
            Assert.AreEqual(hse1.CampaignCode, hse2.CampaignCode);
            Assert.AreEqual(hse1.ChildrenPerRoom, hse2.ChildrenPerRoom);
            //Assert.AreEqual(hse1.DepartureDate, hse2.DepartureDate); -- Response from OWS is not correct while create booking
            Assert.AreEqual(hse1.NameID, hse2.NameID);
            Assert.AreEqual(hse1.NoOfNights, hse2.NoOfNights);
            Assert.AreEqual(hse1.RoomsPerNight, hse2.NoOfNights);
            Assert.AreEqual(hse1.SearchingType, hse2.SearchingType);
            Assert.AreEqual(hse1.SelectedHotelCode, hse2.SelectedHotelCode);
            Assert.AreEqual(hse1.UserName, hse2.UserName);

            Assert.AreEqual(hre1.AwardType, hre2.AwardType);
            Assert.AreEqual(hre1.Points, hre2.Points);
            Assert.AreEqual(hre1.Rate.CurrencyCode , hre2.Rate.CurrencyCode );
            //Assert.AreEqual(hre1.Rate.Rate, hre2.Rate.Rate); -- Response from OWS is not correct while create booking
            Assert.AreEqual(hre1.RatePlanCode, hre2.RatePlanCode);
            Assert.AreEqual(hre1.RoomtypeCode, hre2.RoomtypeCode);
            //Assert.AreEqual(hre1.TotalRate, hre2.TotalRate); -- Response from OWS is not correct while create booking

            Assert.AreEqual(gie1.AddressID, gie2.AddressID);
            Assert.AreEqual(gie1.AddressLine1, gie2.AddressLine1);
            Assert.AreEqual(gie1.AddressLine2, gie2.AddressLine2);
            Assert.AreEqual(gie1.City, gie2.City);
            Assert.AreEqual(gie1.Comments, gie2.Comments);
            Assert.AreEqual(gie1.Country, gie2.Country);
            Assert.AreEqual(gie1.GuestAccountNumber, gie2.GuestAccountNumber);
            Assert.AreEqual(gie1.MembershipID, gie2.MembershipID);
            Assert.AreEqual(gie1.PostCode, gie2.PostCode);
            Assert.AreEqual(gie1.SpecialRequests, gie2.SpecialRequests);
            //Assert.AreEqual(gie1.Telephone1, gie2.Telephone1); -- Response from OWS is not correct while create booking
            //Assert.AreEqual(gie1.Telephone2, gie2.Telephone2); -- Response from OWS is not correct while create booking
            Assert.AreEqual(gie1.Title, gie2.Title);
            #endregion Assert
        }

        [Test]
        [ExpectedException(typeof(OWSException))]
        public void TestFetchBookingIfResultStatusFlagFails()
        {
            string resevation_number = "1111111";
            ReservationDomain rd = new ReservationDomain();
            BookingDetailsEntity bookingDetailsEntity = rd.FetchReservation(resevation_number);

        }

        [Test]
        public void TestModifyReservation()
        {
            ReservationController rc = new ReservationController();
            HotelSearchEntity hse1 = GetHotelSearchEntity();
            HotelRoomRateEntity hre1 = GetHotelRoomRateEntity();
            GuestInformationEntity gie1 = GetGuestInformationEntity();
            string nameID;
            string resevation_number = rc.CreateBooking(hse1, hre1, gie1, out nameID);

            ReservationDomain rd = new ReservationDomain();

            hse1.ChildrenPerRoom = 0;
            hse1.ArrivalDate = DateTime.Today.AddDays(80);
            hse1.DepartureDate = DateTime.Today.AddDays(81);

            string createdNameID;
            string confirmationNumber = rd.ModifyReservation(hse1, hre1, gie1, resevation_number, out createdNameID);

            Assert.AreEqual(confirmationNumber, resevation_number);

        }

        [Test]
        public void TestCancelReservation()
        {
            ReservationController rc = new ReservationController();
            HotelSearchEntity hse1 = GetHotelSearchEntity();
            HotelRoomRateEntity hre1 = GetHotelRoomRateEntity();
            GuestInformationEntity gie1 = GetGuestInformationEntity();
            string nameID;
            string resevation_number = rc.CreateBooking(hse1, hre1, gie1, out nameID);

            ReservationDomain rd = new ReservationDomain();

            CancelDetailsEntity cancelDetailsEntity = GetCancelDetailEntity();
            cancelDetailsEntity.ReservationNumber = resevation_number;

            bool result = rd.CancelReservation(cancelDetailsEntity);
            Assert.AreEqual(true, result);


        }
        #endregion Modify Cancel 
        #endregion Test Methods

        #region Private Entity Creation Methods

        #region Modify Cancel Module
        private HotelSearchEntity GetHotelSearchEntity()
        {

            HotelSearchEntity hse = new HotelSearchEntity();
            hse.AdultsPerRoom = 1;
            hse.ArrivalDate = DateTime.Today.AddDays(76);
            hse.ChildrenPerRoom = 1;
            hse.DepartureDate = DateTime.Today.AddDays(77);
            hse.NoOfNights = 1;
            hse.RoomsPerNight = 1;
            hse.SearchingType = SearchType.REGULAR;
            hse.SelectedHotelCode = "609";


            return hse;

        }

        private HotelRoomRateEntity GetHotelRoomRateEntity()
        {
            HotelRoomRateEntity hre = new HotelRoomRateEntity();
            RateEntity re = new RateEntity(211, "EUR");
            hre.Rate = re;
            hre.RatePlanCode = "RA3";
            hre.RoomtypeCode = "TR";
            RateEntity ret = new RateEntity(211, "EUR");
            hre.Rate = ret;
            hre.TotalRate = ret;
            return hre;

        }

        private GuestInformationEntity GetGuestInformationEntity()
        {
            GuestInformationEntity gie = new GuestInformationEntity();
            //gie.AddressID = "AddressID";
            gie.AddressLine1 = "AddressLine";
            gie.AddressLine2 = "AddressLine";
            gie.City = "Bangalore";
            gie.Comments = "Comments";
            gie.CompanyName = "CompanyName";
            gie.Country = "IN";
            gie.Email = "Email";
            gie.FirstName = "NunitFirstName";
            //gie.Gender = "Gender";
            //gie.GuestAccountNumber = "GuestAccountNumber";
            //gie.GuranteeInformation = null;
            gie.LastName = "NunitLastName";
            gie.PostCode = "34534534";
            gie.Telephone1 = "03583947";
            gie.Telephone2 = "Telephone2";
            return gie;

        }

        private CancelDetailsEntity GetCancelDetailEntity()
        {
            CancelDetailsEntity cancelDetailsEntity = null;

            cancelDetailsEntity = new CancelDetailsEntity();
            cancelDetailsEntity.HotelCode = "609";
            cancelDetailsEntity.ReservationNumber = "ReservationNumber";
            cancelDetailsEntity.ChainCode = AppConstants.CHAIN_CODE;
            cancelDetailsEntity.CancelType = Scandic.Scanweb.BookingEngine.Entity.CancelTermType.Cancel;
            return cancelDetailsEntity;


        }
        #endregion Modify Cancel Module

        #endregion

    }
}
