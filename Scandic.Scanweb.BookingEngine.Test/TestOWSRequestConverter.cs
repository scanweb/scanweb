using System;
using System.Collections.Generic;
using System.Text;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Security;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Reservation;
using Scandic.Scanweb.BookingEngine.Entity;
using Scandic.Scanweb.BookingEngine.ExceptionManager;
using NUnit.Framework;

namespace Scandic.Scanweb.BookingEngine.Test
{
    [TestFixture]
    public class TestOWSRequestConverter
    {

        #region Test Methods

        #region Existing Methods
        [Test]
        public void GetAuthenticateUserRequest()
        {
            string userName = "1111111111";
            string password = "PASS";
            AuthenticateUserRequest methodRequest =
                OWSRequestConverter.GetAuthenticateUserRequest(userName, password);
            Assert.AreEqual(userName, methodRequest.membershipNumber);
            Assert.AreEqual(password, methodRequest.password);
        }

        [Test]
        public void GetFetchNameRequest()
        {
            string nameID = "1111111111";
            FetchNameRequest request = OWSRequestConverter.GetFetchNameRequest(nameID);
            Assert.AreEqual(nameID, request.NameID.Value);
            Assert.AreEqual(Domain.OWSProxy.Name.UniqueIDType.INTERNAL, request.NameID.type);
        }

        [Test]
        public void GetFetchGuestcardListRequest()
        {
            String nameID = "1111111111";
            FetchGuestCardListRequest request = OWSRequestConverter.GetFetchGuestcardListRequest(nameID);
            Assert.AreEqual(Domain.OWSProxy.Name.UniqueIDType.INTERNAL, request.NameID.type);
            Assert.AreEqual(nameID, request.NameID.Value);
        }

        [Test]
        public void RegularSearchGetRegionalAvailabilityRequest()
        {
            HotelSearchEntity searchEntity = RegularCitySearchEntity();
            RegionalAvailabilityRequest request = OWSRequestConverter.GetRegionalAvailabilityRequest(searchEntity);
            TestRegionalAvailabilityRequest(searchEntity, request);
        }

        [Test]
        public void PromotionSearchGetRegionalAvailabilityRequest()
        {
            HotelSearchEntity searchEntity = PromotionCitySearchEntity();
            RegionalAvailabilityRequest request = OWSRequestConverter.GetRegionalAvailabilityRequest(searchEntity);
            TestRegionalAvailabilityRequest(searchEntity, request);
        }

        [Test]
        public void CorporateSearchGetRegionalAvailabilityRequest()
        {
            HotelSearchEntity searchEntity = CorporateCitySearchEntity();
            RegionalAvailabilityRequest request = OWSRequestConverter.GetRegionalAvailabilityRequest(searchEntity);
            TestRegionalAvailabilityRequest(searchEntity, request);
        }

        [Test]
        public void BonusChequeSearchGetRegionalAvailabilityRequest()
        {
            HotelSearchEntity searchEntity = BonusChequeCitySearchEntity();
            RegionalAvailabilityRequest request = OWSRequestConverter.GetRegionalAvailabilityRequest(searchEntity);
            TestRegionalAvailabilityRequest(searchEntity, request);
        }

        [Test]
        public void VoucherSearchGetRegionalAvailabilityRequest()
        {
            HotelSearchEntity searchEntity = VoucherCitySearchEntity();
            RegionalAvailabilityRequest request = OWSRequestConverter.GetRegionalAvailabilityRequest(searchEntity);
            TestRegionalAvailabilityRequest(searchEntity, request);
        }

        public void TestRegionalAvailabilityRequest(HotelSearchEntity searchEntity, RegionalAvailabilityRequest request)
        {
            RegionalSearchCode searchCode = (RegionalSearchCode)request.Item;
            Assert.AreEqual(RegionalSearchCodeType.CITY, searchCode.regionalSearchCodeType);
            Assert.AreEqual(searchEntity.SearchedFor.SearchCode, searchCode.Value);
            Assert.AreEqual(searchEntity.ArrivalDate, request.StayDateRange.StartDate);
            Assert.AreEqual(searchEntity.DepartureDate, request.StayDateRange.Item);
            Assert.AreEqual(searchEntity.AdultsPerRoom + searchEntity.ChildrenPerRoom, request.NumberOfGuests);
            Assert.AreEqual(searchEntity.RoomsPerNight, request.NumberOfRooms);
            Assert.AreEqual(true, request.NumberOfRoomsSpecified);
        }

        [Test]
        public void RegularSearchGetGeneralAvailRequest()
        {
            HotelSearchEntity searchEntity = RegularCitySearchEntity();
            Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability.HotelReference hotelReference = GetHotelReference();
            AvailabilityRequest request = OWSRequestConverter.GetGeneralAvailabilityRequest(searchEntity, hotelReference);
            TestGeneralAvailabilityRequest(searchEntity, hotelReference, request);
            Assert.IsNull(request.AvailRequestSegment[0].RatePlanCandidates);
        }

        [Test]
        public void PromotionSearchGetGeneralAvailRequest()
        {
            HotelSearchEntity searchEntity = PromotionCitySearchEntity();
            Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability.HotelReference hotelReference = GetHotelReference();
            AvailabilityRequest request = OWSRequestConverter.GetGeneralAvailabilityRequest(searchEntity, hotelReference);
            TestGeneralAvailabilityRequest(searchEntity, hotelReference, request);
            Assert.AreEqual(searchEntity.CampaignCode, request.AvailRequestSegment[0].RatePlanCandidates[0].promotionCode);
        }

        [Test]
        public void CorporateSearchGetGeneralAvailRequest()
        {
            HotelSearchEntity searchEntity = CorporateCitySearchEntity();
            Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability.HotelReference hotelReference = GetHotelReference();
            AvailabilityRequest request = OWSRequestConverter.GetGeneralAvailabilityRequest(searchEntity, hotelReference);
            TestGeneralAvailabilityRequest(searchEntity, hotelReference, request);
            Assert.AreEqual("CORPORATE", request.AvailRequestSegment[0].RatePlanCandidates[0].qualifyingIdType);
            Assert.AreEqual(searchEntity.CampaignCode, request.AvailRequestSegment[0].RatePlanCandidates[0].qualifyingIdValue);
        }

        [Test]
        public void BonusChequeSearchGetGeneralAvailRequest()
        {
            HotelSearchEntity searchEntity = BonusChequeCitySearchEntity();
            Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability.HotelReference hotelReference = GetHotelReference();
            AvailabilityRequest request = OWSRequestConverter.GetGeneralAvailabilityRequest(searchEntity, hotelReference);
            TestGeneralAvailabilityRequest(searchEntity, hotelReference, request);
            Assert.IsNull(request.AvailRequestSegment[0].RatePlanCandidates);
        }

        [Test]
        public void VoucherSearchGetGeneralAvailRequest()
        {
            HotelSearchEntity searchEntity = VoucherCitySearchEntity();
            Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability.HotelReference hotelReference = GetHotelReference();
            AvailabilityRequest request = OWSRequestConverter.GetGeneralAvailabilityRequest(searchEntity, hotelReference);
            TestGeneralAvailabilityRequest(searchEntity, hotelReference, request);
            Assert.AreEqual(searchEntity.CampaignCode, request.AvailRequestSegment[0].RatePlanCandidates[0].ratePlanCode);
        }

        public void TestGeneralAvailabilityRequest(HotelSearchEntity searchEntity, Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability.HotelReference hotelReference ,AvailabilityRequest request)
        {
            Assert.AreEqual(true, request.summaryOnly);
            AvailRequestSegment requestSegment = request.AvailRequestSegment[0];
            Assert.AreEqual(AvailRequestType.Room, requestSegment.availReqType);
            Assert.AreEqual(searchEntity.ArrivalDate, requestSegment.StayDateRange.StartDate);
            Assert.AreEqual(searchEntity.DepartureDate, requestSegment.StayDateRange.Item);
            Assert.AreEqual(searchEntity.RoomsPerNight, requestSegment.numberOfRooms);
            Assert.AreEqual(true, requestSegment.numberOfRoomsSpecified);
            Assert.AreEqual(searchEntity.AdultsPerRoom, requestSegment.totalNumberOfGuests);
            Assert.AreEqual(true, requestSegment.totalNumberOfGuestsSpecified);
            Assert.AreEqual(searchEntity.ChildrenPerRoom, requestSegment.numberOfChildren);
            Assert.AreEqual(true, requestSegment.numberOfChildrenSpecified);
            HotelSearchCriterion searchCriteria = requestSegment.HotelSearchCriteria[0];
            Assert.AreEqual(hotelReference.chainCode, searchCriteria.HotelRef.chainCode);
            Assert.AreEqual(hotelReference.hotelCode, searchCriteria.HotelRef.hotelCode);
            Assert.IsNull(request.AvailRequestSegment[0].RateRange);
            Assert.IsNull(request.AvailRequestSegment[0].RoomStayCandidates);
        }

        [Test]
        public void GetFetchRateAwardsRequest()
        {
            Assert.Fail("The test case need to be updated");
        }

        [Test]
        public void GetBookingRequest()
        {
            Assert.Fail("The test case need to be update");
        }

        #endregion Existing Methods

        #region Modify Cancel Module
        [Test]
        public void TestFetchBookingRequest()
        {
            ReservationController rc = new ReservationController();
            HotelSearchEntity hse1 = GetHotelSearchEntity();
            HotelRoomRateEntity hre1 = GetHotelRoomRateEntity();
            GuestInformationEntity gie1 = GetGuestInformationEntity();
            string nameID;
            string resevation_number = rc.CreateBooking(hse1, hre1, gie1, out nameID);
            FetchBookingRequest fbr = OWSRequestConverter.GetFetchBookingRequest(resevation_number);
            Assert.AreEqual(resevation_number, fbr.ConfirmationNumber.Value);

        }

        [Test]
        public void TestGetCancelBookingRequest()
        {
            ReservationController rc = new ReservationController();
            HotelSearchEntity hse1 = GetHotelSearchEntity();
            HotelRoomRateEntity hre1 = GetHotelRoomRateEntity();
            GuestInformationEntity gie1 = GetGuestInformationEntity();
            string nameID;
            string resevation_number = rc.CreateBooking(hse1, hre1, gie1, out nameID);

            CancelDetailsEntity cancelDetailsEntity = GetCancelDetailEntity();
            cancelDetailsEntity.ReservationNumber = resevation_number;

            CancelBookingRequest cbr = OWSRequestConverter.GetCancelBookingRequest(cancelDetailsEntity);
            Assert.AreEqual(resevation_number, cbr.ConfirmationNumber.Value);

        }

        [Test]
        public void TestGetModifyBookingRequest()
        {
            ReservationController rc = new ReservationController();
            HotelSearchEntity hse1 = GetHotelSearchEntity();
            HotelRoomRateEntity hre1 = GetHotelRoomRateEntity();
            GuestInformationEntity gie1 = GetGuestInformationEntity();
            string nameID;
            string resevation_number = rc.CreateBooking(hse1, hre1, gie1, out nameID);
            hse1.ChildrenPerRoom = 0;
            hse1.ArrivalDate = DateTime.Today.AddDays(77);
            hse1.DepartureDate = DateTime.Today.AddDays(78);
            ModifyBookingRequest mbr = OWSRequestConverter.GetModifyBookingRequest(hse1, hre1, gie1, resevation_number);
            Assert.AreEqual(hse1.ArrivalDate, mbr.HotelReservation.RoomStays[0].TimeSpan.StartDate);
            Assert.AreEqual(hse1.DepartureDate, (DateTime) mbr.HotelReservation.RoomStays[0].TimeSpan.Item);

        }

        #endregion Modify Cancel Module

        #endregion Test Methods

        #region Entity Methods

        #region Existing Methods
        public HotelSearchEntity RegularCitySearchEntity()
        {
            HotelSearchEntity searchEntity = new HotelSearchEntity();
            SearchedForEntity searchedFor = new SearchedForEntity("STOCKHOLM");
            searchEntity.ArrivalDate = new DateTime(2007, 12, 31);
            searchEntity.DepartureDate = new DateTime(2008, 01, 01);
            searchEntity.NoOfNights = 1;
            searchEntity.RoomsPerNight = 2;
            searchEntity.AdultsPerRoom = 1;
            searchEntity.ChildrenPerRoom = 1;
            searchEntity.SearchingType = SearchType.REGULAR;
            searchEntity.SearchedFor = searchedFor;

            return searchEntity;
        }

        public HotelSearchEntity PromotionCitySearchEntity()
        {
            HotelSearchEntity searchEntity = RegularCitySearchEntity();
            searchEntity.CampaignCode = "AAAAAAAA";
            searchEntity.SearchingType = SearchType.REGULAR;
            return searchEntity;
        }

        public HotelSearchEntity CorporateCitySearchEntity()
        {
            HotelSearchEntity searchEntity = RegularCitySearchEntity();
            searchEntity.CampaignCode = "AAAAAAAA";
            searchEntity.SearchingType = SearchType.CORPORATE;
            return searchEntity;
        }

        public HotelSearchEntity RedemptionCitySearchEntity()
        {
            HotelSearchEntity searchEntity = RegularCitySearchEntity();
            searchEntity.SearchingType = SearchType.REDEMPTION;
            return searchEntity;
        }

        public HotelSearchEntity BonusChequeCitySearchEntity()
        {
            HotelSearchEntity searchEntity = RegularCitySearchEntity();
            searchEntity.SearchingType = SearchType.BONUSCHEQUE;
            return searchEntity;
        }

        public HotelSearchEntity VoucherCitySearchEntity()
        {
            HotelSearchEntity searchEntity = RegularCitySearchEntity();
            searchEntity.CampaignCode = "AAAAAAAA";
            searchEntity.SearchingType = SearchType.VOUCHER;
            return searchEntity;
        }

        public Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability.HotelReference GetHotelReference()
        {
            Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability.HotelReference reference = new Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability.HotelReference();
            reference.chainCode = AppConstants.CHAIN_CODE;
            reference.hotelCode = "813";

            return reference;
        }
        #endregion Existing Methods

        #region Modify Cancel Module
        private HotelSearchEntity GetHotelSearchEntity()
        {

            HotelSearchEntity hse = new HotelSearchEntity();
            hse.AdultsPerRoom = 1;
            hse.ArrivalDate = DateTime.Today.AddDays(75);
            hse.ChildrenPerRoom = 1;
            hse.DepartureDate = DateTime.Today.AddDays(76);
            hse.NoOfNights = 1;
            hse.RoomsPerNight = 1;
            hse.SearchingType = SearchType.REGULAR;
            hse.SelectedHotelCode = "603"; 
           

            return hse;

        }

        private HotelRoomRateEntity GetHotelRoomRateEntity()
        {
            HotelRoomRateEntity hre = new HotelRoomRateEntity();
            RateEntity re = new RateEntity(165, "EUR");
            hre.Rate = re;
            hre.RatePlanCode = "RA3";
            hre.RoomtypeCode = "TR";
            RateEntity ret = new RateEntity(165, "EUR");
            hre.Rate = ret;
            hre.TotalRate = ret;
            return hre;

        }

        private GuestInformationEntity GetGuestInformationEntity()
        {
            GuestInformationEntity gie = new GuestInformationEntity();
            gie.AddressLine1 = "AddressLine";
            gie.AddressLine2 = "AddressLine";
            gie.City = "Bangalore";
            gie.Comments = "Comments";
            gie.CompanyName = "CompanyName";
            gie.Country = "IN";
            gie.Email = "Email";
            gie.FirstName = "NunitFirstName";
            gie.LastName = "NunitLastName";
            gie.PostCode = "34534534";
            gie.Telephone1 = "03583947";
            gie.Telephone2 = "Telephone2";
            return gie;

        }

        private CancelDetailsEntity GetCancelDetailEntity()
        {
            CancelDetailsEntity cancelDetailsEntity = null;

            cancelDetailsEntity = new CancelDetailsEntity();
            cancelDetailsEntity.HotelCode = "603";
            cancelDetailsEntity.ReservationNumber = "ReservationNumber";
            cancelDetailsEntity.ChainCode = AppConstants.CHAIN_CODE;
            cancelDetailsEntity.CancelType = Scandic.Scanweb.BookingEngine.Entity.CancelTermType.Cancel;
            return cancelDetailsEntity;


        }
        #endregion Modify Cancel Module

        #endregion Entity Methods
    }
}