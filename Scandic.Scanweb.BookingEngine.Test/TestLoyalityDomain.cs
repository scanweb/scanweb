using System;
using System.Text;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Entity;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Availability;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Security;
using Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name;
namespace Scandic.Scanweb.BookingEngine.Test
{
    /// <summary>
    /// TestLoyalityDomain
    /// </summary>
    [TestFixture]
    public class TestLoyalityDomain
    {

        [Test]
        public void TestFetchMembershipTransactions()
        {
            string membershipOperaId = "44023496";
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            Domain.OWSProxy.Membership.FetchMembershipTransactionsResponse response = 
                LoyaltyDomain.FetchMembershipTransactions(membershipOperaId);
            Assert.AreEqual(membershipOperaId, response.MembershipTransactionList.CardInfo.operaId);
        }
        
        /// <summary>
        /// TestRegisterUser
        /// </summary>
        [Test]
        public void TestRegisterUser()
        {
            UserProfileEntity profile = PopuplateUserProfile();
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            String NameID = loyaltyDomain.RegisterUser(profile);
            GuestInformationEntity testProfile = loyaltyDomain.FetchName(NameID);
            Assert.AreEqual(profile.FirstName, testProfile.FirstName);
            Assert.AreEqual(profile.LastName, testProfile.LastName);
        }

        /// <summary>
        /// TestInsertEmail
        /// </summary>
        [Test]
        public void TestInsertEmail()
        {
            string email= "Test@Test.com";
            string nameID = "44023496";
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            bool insertEmail = loyaltyDomain.InsertEmail(email, nameID);
            string testEmail = loyaltyDomain.FetchEmail(nameID);
            Assert.AreEqual(email, testEmail);
        }

        /// <summary>
        /// TestInsertPhone
        /// </summary>
        [Test]
        public void TestInsertPhone()
        {
            string nameID = "44023496";
            string phoneNumber="049282389";
            string phoneType="HOME";
            string phoneRole="PHONE";
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            bool insertph = loyaltyDomain.InsertPhone(nameID, phoneNumber, phoneType, phoneRole);
            Assert.IsTrue(insertph);
        }

        /// <summary>
        /// TestInsertCreditCard
        /// </summary>
        [Test]
        public void TestInsertCreditCard()
        {
            string nameID = "44023496";
            ExpiryDateEntity expiryDateEnt = new ExpiryDateEntity(10, 2010);
            CreditCardEntity creditCardEnt = new CreditCardEntity("HolderName","VA", "2352345234534", expiryDateEnt);
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            bool insertph = loyaltyDomain.InsertCreditCard(nameID,creditCardEnt);
            CreditCardEntity[] testCC = loyaltyDomain.FetchCreditCardDetails(nameID);
            Assert.AreEqual(creditCardEnt.CardNumber, testCC[0].CardNumber);
            Assert.AreEqual(creditCardEnt.CardType, testCC[0].CardType);
            Assert.AreEqual(creditCardEnt.NameOnCard, testCC[0].NameOnCard);
            Assert.AreEqual(creditCardEnt.ExpiryDate, testCC[0].ExpiryDate);
        }

        /// <summary>
        /// TestInsertPreference
        /// </summary>
        [Test]
        public void TestInsertPreference()
        {
            string nameID = "44023496";
            string preferenceType = "SPECIALS";
            string preferenceValue = "LF";
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            bool insertPref = loyaltyDomain.InsertPreference(nameID, preferenceType, preferenceValue);
            Assert.IsTrue(insertPref);

        }

        /// <summary>
        /// TestInsertUpdatePrivacyUserDetail
        /// </summary>
        [Test]
        public void TestInsertUpdatePrivacyUserDetail()
        {
            string nameID = "44023496";
            bool scandicEmail = true;
            bool thirdPartyEmail = true;
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            bool insertUpdatePrivacyOption = 
                loyaltyDomain.InsertUpdatePrivacyUserDetail(nameID, scandicEmail, thirdPartyEmail);
            Assert.IsTrue(insertUpdatePrivacyOption);
        }

        /// <summary>
        /// TestFetchNameService
        /// </summary>
        [Test]
        public void TestFetchNameService()
        {
            string nameId = "44936737";
            string FirstName = "NUnit";
            string LastName = "Test Case";
            DateTime DOB = new DateTime(1960, 09, 15);
            string genderExpected = "MALE";
            
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            FetchNameResponse response = loyaltyDomain.FetchNameService(nameId);

            Assert.AreEqual(FirstName, response.PersonName.firstName);
            Assert.AreEqual(LastName, response.PersonName.lastName);
            Assert.AreEqual(DOB, response.Birthdate);
            Assert.AreEqual(genderExpected, response.Gender.ToString());
        }

        /// <summary>
        /// TestFetchAddressNameService
        /// </summary>
        [Test]
        public void TestFetchAddressNameService()
        {
            string nameId = "44936737";
            string city = "Naples";
            string country = "US";
            string postcode = "25442";
            long operaId = 29446149;
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            FetchAddressListResponse response = loyaltyDomain.FetchAddressNameService(nameId);
            Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name.NameAddress nameAddress = response.NameAddressList[0];
            Assert.AreEqual(operaId, nameAddress.operaId);
            Assert.AreEqual(city, nameAddress.cityName);
            Assert.AreEqual(country, nameAddress.countryCode);
            Assert.AreEqual(postcode, nameAddress.postalCode);

        }

        /// <summary>
        /// TestFetchPhoneNameService
        /// </summary>
        [Test]
        public void TestFetchPhoneNameService()
        {
            string nameId = "44936737";
            long operaId = 25618799;
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            FetchPhoneListResponse response = loyaltyDomain.FetchPhoneNameService(nameId);
            Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name.NamePhone namePhone = response.NamePhoneList[0];

            Assert.AreEqual(operaId, namePhone.operaId);
        }

        /// <summary>
        /// TestFetchEmailNameService
        /// </summary>
        [Test]
        public void TestFetchEmailNameService()
        {
            string nameId = "44936737";
            long operaId = 25617268;
            string email = "Nuint@test.com";
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            FetchEmailListResponse response = loyaltyDomain.FetchEmailNameService(nameId);
            Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name.NameEmail nameEmail = response.NameEmailList[0];

            Assert.AreEqual(email, nameEmail.Value);
            Assert.AreEqual(operaId, nameEmail.operaId);
        }

        /// <summary>
        /// TestFetchCreditCardDetailsNameService
        /// </summary>
        [Test]
        public void TestFetchCreditCardDetailsNameService()
        {
            string nameId = "44936737";
            long operaId = 25617268;//correct opera ID to be written
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            FetchCreditCardListResponse response = loyaltyDomain.FetchCreditCardDetailsNameService(nameId);
            Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name.NameCreditCard nameCreditCard = response.CreditCardList[0];

            Assert.AreEqual(operaId, nameCreditCard.operaId);
        }

        /// <summary>
        /// TestFetchPreferencesListNameService
        /// </summary>
        [Test]
        public void TestFetchPreferencesListNameService()
        {
            string nameId = "44936737";
            string preferenceType="SPECIALS";
            string preferenceValue = "LF";
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            FetchPreferenceListResponse response = loyaltyDomain.FetchPreferencesListNameService(nameId);

            Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name.Preference Preference = response.PreferenceList[0];

            Assert.AreEqual(preferenceType, Preference.preferenceType);
            Assert.AreEqual(preferenceValue, Preference.preferenceValue);
        }

        /// <summary>
        /// TestFetchNameUserProfile
        /// </summary>
        [Test]
        public void TestFetchNameUserProfile()
        {
            string nameId = "44936737";
            string FirstName = "NUnit";
            string LastName = "Test Case";
            UserGender genderExpected = UserGender.MALE;

            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            UserProfileEntity userProfile = loyaltyDomain.FetchNameUserProfile(nameId);

            Assert.AreEqual(FirstName, userProfile.FirstName);
            Assert.AreEqual(LastName, userProfile.LastName);
            Assert.AreEqual(genderExpected, userProfile.Gender);
        }

        /// <summary>
        /// TestFetchAddressUserProfile
        /// </summary>
        [Test]
        public void TestFetchAddressUserProfile()
        {
            string nameId = "44936737";
            string city = "Naples";
            string country = "US";
            string postcode = "25442";
            long operaId = 29446149;
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            UserProfileEntity response = loyaltyDomain.FetchAddressUserProfile(nameId);
            Assert.AreEqual(operaId, response.AddressOperaID);
            Assert.AreEqual(city, response.City);
            Assert.AreEqual(country, response.Country);
            Assert.AreEqual(postcode, response.PostCode);
        }

        /// <summary>
        /// TestFetchPrivacyOptionUserProfile
        /// </summary>
        [Test]
        public void TestFetchPrivacyOptionUserProfile()
        {
            string nameId = "44936737";
            Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name.PrivacyOptionTypeOptionValue Status 
                = Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name.PrivacyOptionTypeOptionValue.YES;
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            FetchPrivacyOptionResponse response = loyaltyDomain.FetchPrivacyOption(nameId);
            Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name.PrivacyOptionType[] privacyOption = response.Privacy;
            if (privacyOption != null && privacyOption.Length > 0)
            {
                for (int count = 0; count < privacyOption.Length; count++)
                {

                    if (privacyOption[count].OptionType == 
                        Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name.PrivacyOptionTypeOptionType.Promotions)
                    {
                        Assert.AreEqual(Status, privacyOption[count].OptionValue);
                          
                    }
                    else if (privacyOption[count].OptionType == 
                        Scandic.Scanweb.BookingEngine.Domain.OWSProxy.Name.PrivacyOptionTypeOptionType.ThirdParties)
                    {
                        Assert.AreEqual(Status, privacyOption[count].OptionValue);
                    }
                }

            }
            
        }

        /// <summary>
        /// TestUpdateName
        /// </summary>
        [Test]
        public void TestUpdateName()
        {
            string nameID = "44936737";
            string FirstName = "NUnit Name";
            string LastName = "Test Case";
            LoyaltyDomain loyalty = new LoyaltyDomain();
            bool nameUpdated = loyalty.UpdateName(nameID, FirstName, LastName);

            Assert.IsTrue(nameUpdated);
        }

        /// <summary>
        /// TestUpdateAddress
        /// </summary>
        [Test]
        public void TestUpdateAddress()
        {
            string add1="Address Line 1";
            string add2 = "Address Line 1";
            string city = "Naples";
            string country = "US";
            string postcode = "25442";
            long operaId = 29446149;
            LoyaltyDomain loyalty = new LoyaltyDomain();
        }

        /// <summary>
        /// TestUpdatePhone
        /// </summary>
        [Test]
        public void TestUpdatePhone()
        {
            string nameId = "44936737";
            long operaId = 25618799;
            string phNumber = "0001456789";
            string phType = "HOME";
            string phRole = "PHONE";
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            bool updatedPhone = loyaltyDomain.UpdatePhone(nameId,operaId, phNumber, phType, phRole);

            Assert.IsTrue(updatedPhone);
        }

        /// <summary>
        /// TestUpdateEmail
        /// </summary>
        [Test]
        public void TestUpdateEmail()
        {
            string nameId = "44936737";
            long operaId = 25617268;
            string email = "Nuint@test.com";
            
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
            bool updatedEmail = loyaltyDomain.UpdateEmail(nameId, operaId, email);

            Assert.IsTrue(updatedEmail);

        }

        /// <summary>
        /// TestUpdateCreditCardDetails
        /// </summary>
        [Test]
        public void TestUpdateCreditCardDetails()
        {
            string nameId = "44936737";
            long operaId = 25617268;
            ExpiryDateEntity ExpDt = new ExpiryDateEntity(11, 2011);
            LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
        }

        /// <summary>
        /// PopuplateUserProfile
        /// </summary>
        /// <returns>UserProfileEntity</returns>
        public UserProfileEntity PopuplateUserProfile()
        {
            UserProfileEntity userProfileEntity = new UserProfileEntity();
            userProfileEntity.FirstName = "TestFirstName";
            userProfileEntity.LastName = "TestLastName";
            userProfileEntity.Gender = UserGender.MALE;
            userProfileEntity.DateOfBirth = new DateOfBirthEntity(15, 07, 1989);
            userProfileEntity.EmailID = "test@testemail.com";
            
            userProfileEntity.HomePhone = "00123456";
            userProfileEntity.MobilePhone = "001653432";
            userProfileEntity.AddressType = "HOME";
            userProfileEntity.AddressLine1 = "Add line 1";
            userProfileEntity.AddressLine2 = "Add line 1";
            userProfileEntity.PostCode = "12345";
            userProfileEntity.Country = "SE";
            ExpiryDateEntity expiryDateEnt = new ExpiryDateEntity(09, 2010);
            CreditCardEntity creditCardEnt = 
                new CreditCardEntity("TestCardHolder", "VA", "999999999999999", expiryDateEnt);

            userProfileEntity.UserPreference = GetGuestPreference();
            userProfileEntity.PartnerProgram = "SAS";
            userProfileEntity.PartnerAccountNumber = "98989898989898";
            userProfileEntity.Password = "testpwd";
            userProfileEntity.RetypePassword = "testpwd";
            userProfileEntity.Question = "Q1";
            userProfileEntity.Answer = "password";
            return userProfileEntity;
        }

        /// <summary>
        /// GetGuestPreference
        /// </summary>
        /// <returns>List of UserPreferenceEntity</returns>
        private UserPreferenceEntity[] GetGuestPreference()
        {
            ArrayList userPrefList = new ArrayList();

            UserPreferenceEntity guestPrefEntity = new UserPreferenceEntity();
            guestPrefEntity.RequestType = "SPECIAL";
            guestPrefEntity.RequestValue = "LF";
            userPrefList.Add(guestPrefEntity);


            UserPreferenceEntity guestPrefEntity1 = new UserPreferenceEntity();
            guestPrefEntity.RequestType = "SPECIAL";
            guestPrefEntity.RequestValue = "NS";
            userPrefList.Add(guestPrefEntity1);

            return userPrefList.ToArray(typeof(UserPreferenceEntity)) as UserPreferenceEntity[];
        }
    }
}
