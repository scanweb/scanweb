//  Description					:   BinaryTrie                                            //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:   Aneesh Lal G A                                        //
/// Author email id				:                           							  //
/// Creation Date				:   19-March-2010                						  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System NameSpaces

using System;
using System.Collections;

#endregion

namespace Scandic.Scanweb.IP2Country
{
    /// <summary>
    /// Represents a trie with keys that are binary values of
    /// length up to 32.
    /// </summary>
    public class BinaryTrie
    {
        internal BinaryTrieNode[] _roots;
        private Int32 _indexLength = 0;
        private Int32 _count = 0;

        #region Public instance constructors

        /// <summary>
        /// Constructs a <see cref="BinaryTrie"/> with an index length
        /// of 1.
        /// </summary>
        public BinaryTrie()
        {
            _indexLength = 1;
            _roots = new BinaryTrieNode[2];
        }

        /// <summary>
        /// Constructs a <see cref="BinaryTrie"/> with a given index length.
        /// </summary>
        /// <param name="indexLength">The index length.</param>
        public BinaryTrie(Int32 indexLength)
        {
            if ((indexLength < 1) || (indexLength > 18))
                throw new ArgumentOutOfRangeException("indexLength");
            _indexLength = indexLength;
            _roots = new BinaryTrieNode[1 << indexLength];
        }

        #endregion

        #region Protected instance members

        /// <summary>
        /// Gets the collection of root <see cref="BinaryTrieNode"/>
        /// objects in this <see cref="BinaryTrie"/>.
        /// </summary>
        /// <value>The roots.</value>
        protected BinaryTrieNode[] Roots
        {
            get { return _roots; }
        }

        /// <summary>
        /// Gets or sets the number of keys in the trie.
        /// </summary>
        /// <value>The count internal.</value>
        protected Int32 CountInternal
        {
            get { return _count; }
            set { _count = value; }
        }

        /// <summary>
        /// Adds a key with the given index to the trie.
        /// </summary>
        /// <param name="index">The index of the root <see cref="BinaryTrieNode"/>
        /// for the given key value.</param>
        /// <param name="key">An <see cref="Int32"/> key value.</param>
        /// <param name="keyLength">The length in bits of the significant
        /// portion of the key.</param>
        /// <returns>
        /// The <see cref="BinaryTrieNode"/> that was added to the
        /// trie.
        /// </returns>
        protected BinaryTrieNode AddInternal(Int32 index, Int32 key, Int32 keyLength)
        {
            CountInternal++;
            BinaryTrieNode root = Roots[index];
            if (null == root)
                return _roots[index] = new BinaryTrieNode(key, keyLength);
            else
                return root.AddInternal(key, keyLength);
        }

        /// <summary>
        /// Finds the best match internal.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        protected Object FindBestMatchInternal(Int32 index, Int32 key)
        {
            BinaryTrieNode root = _roots[index];
            if (null == root)
                return null;
            return root.FindBestMatch(key).UserData;
        }

        /// <summary>
        /// Finds the exact match internal.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        protected Object FindExactMatchInternal(Int32 index, Int32 key)
        {
            BinaryTrieNode root = _roots[index];
            if (null == root)
                return null;
            return root.FindExactMatch(key).UserData;
        }

        #endregion

        #region Public instance properties

        /// <summary>
        /// Gets the index length of this <see cref="BinaryTrie"/>.
        /// </summary>
        /// <value>The length of the index.</value>
        /// <remarks>The index length indicates the number of bits
        /// that is to be used to preselect the root nodes.
        /// </remarks>
        public Int32 IndexLength
        {
            get { return _indexLength; }
        }

        /// <summary>
        /// Gets the number of keys in the trie.
        /// </summary>
        /// <value>The count.</value>
        public Int32 Count
        {
            get { return _count; }
        }

        #endregion

        #region Public instance methods

        /// <summary>
        /// Minimizes this instance.
        /// </summary>
        /// <returns></returns>
        public Int32 Minimize()
        {
            Int32 nodesEliminated = 0;
            for (Int32 index = 0; index < _roots.Length; index++)
            {
                BinaryTrieNode root = _roots[index];
                if (null != root)
                    nodesEliminated += root.Minimize();
            }
            return nodesEliminated;
        }

        /// <summary>
        /// Adds a node to the trie.
        /// </summary>
        /// <param name="key">An <see cref="Int32"/> key value.</param>
        /// <param name="keyLength">The length in bits of the significant
        /// portion of the key.</param>
        /// <returns>
        /// The <see cref="BinaryTrieNode"/> that was added to the
        /// trie.
        /// </returns>
        public BinaryTrieNode Add(Int32 key, Int32 keyLength)
        {
            Int32 index = (Int32) (key >> (32 - _indexLength));
            return AddInternal(index, key, keyLength);
        }

        /// <summary>
        /// Finds the best match.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public Object FindBestMatch(Int32 key)
        {
            Int32 index = (Int32) (key >> (32 - _indexLength));
            return FindBestMatchInternal(index, key);
        }

        #endregion
    }

    /// <summary>
    /// Represents an entry in an <see cref="IPCountryLookup"/> table.
    /// </summary>
    public class BinaryTrieNode
    {
        protected static readonly Object EmptyData = new Object();

        private static Int32[] _bit
            = {
                  0x7FFFFFFF, 0x7FFFFFFF, 0x40000000, 0x20000000, 0x10000000,
                  0x8000000, 0x4000000, 0x2000000, 0x1000000,
                  0x800000, 0x400000, 0x200000, 0x100000,
                  0x80000, 0x40000, 0x20000, 0x10000,
                  0x8000, 0x4000, 0x2000, 0x1000,
                  0x800, 0x400, 0x200, 0x100,
                  0x80, 0x40, 0x20, 0x10,
                  0x8, 0x4, 0x2, 0x1, 0
              };

        private Int32 _key;
        private Int32 _keyLength;
        private BinaryTrieNode _zero = null;
        private BinaryTrieNode _one = null;
        private Object _userData;

        #region Public instance properties

        /// <summary>
        /// Gets or sets the country code for this entry.
        /// </summary>
        /// <value>The user data.</value>
        public Object UserData
        {
            get
            {
                if (IsKey)
                    return _userData;
                else
                    return null;
            }
            set { _userData = value; }
        }

        /// <summary>
        /// Gets the key associated with this <b>BinaryTrieNode</b>.
        /// </summary>
        /// <value>The key.</value>
        public Int32 Key
        {
            get { return _key; }
        }

        /// <summary>
        /// Gets a value indicating whether the <b>BinaryTrieNode</b>
        /// contains a key (<b>true</b>) or is a helper node that
        /// does not contain a key (<b>false</b>).
        /// </summary>
        /// <value><c>true</c> if this instance is key; otherwise, <c>false</c>.</value>
        public Boolean IsKey
        {
            get { return (!Object.ReferenceEquals(_userData, EmptyData)); }
        }

        /// <summary>
        /// Gets the child node in the zero position.
        /// </summary>
        /// <value>The zero node.</value>
        public BinaryTrieNode ZeroNode
        {
            get { return _zero; }
        }

        /// <summary>
        /// Gets the child node in the one position.
        /// </summary>
        /// <value>The one node.</value>
        public BinaryTrieNode OneNode
        {
            get { return _one; }
        }

        /// <summary>
        /// Gets the key length.
        /// </summary>
        /// <value>The length of the key.</value>
        public Int32 KeyLength
        {
            get { return _keyLength; }
        }

        #endregion

        #region Internal instance members

        /// <summary>
        /// Constructs an <see cref="BinaryTrieNode"/> object.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="keyLength">Length of the key</param>
        internal BinaryTrieNode(Int32 key, Int32 keyLength)
        {
            _key = key;
            _keyLength = keyLength;
            _userData = EmptyData;
        }

        /// <summary>
        /// Adds a record to the trie using the internal representation
        /// of an IP address.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="keyLength">Length of the key.</param>
        /// <returns></returns>
        internal BinaryTrieNode AddInternal(Int32 key, Int32 keyLength)
        {
            Int32 difference = key ^ _key;
            Int32 commonKeyLength = Math.Min(_keyLength, keyLength);
            while (difference >= _bit[commonKeyLength])
                commonKeyLength--;

            if ((keyLength < commonKeyLength)
                || ((keyLength == commonKeyLength) && (keyLength < _keyLength)))
            {
                BinaryTrieNode copy = (BinaryTrieNode) this.MemberwiseClone();

                if ((_key & _bit[keyLength + 1]) != 0)
                {
                    _zero = null;
                    _one = copy;
                }
                else
                {
                    _zero = copy;
                    _one = null;
                }
                _key = key;
                _keyLength = keyLength;
                UserData = EmptyData;
                return this;
            }

            if (commonKeyLength == _keyLength)
            {
                if (keyLength == _keyLength)
                    return this;

                if ((key & _bit[_keyLength + 1]) == 0)
                {
                    if (null == _zero)
                        return _zero = new BinaryTrieNode(key, keyLength);
                    else
                        return _zero.AddInternal(key, keyLength);
                }
                else
                {
                    if (null == _one)
                        return _one = new BinaryTrieNode(key, keyLength);
                    else
                        return _one.AddInternal(key, keyLength);
                }
            }
            else
            {
                BinaryTrieNode copy = (BinaryTrieNode) this.MemberwiseClone();
                BinaryTrieNode newEntry = new BinaryTrieNode(key, keyLength);
                if ((_key & _bit[commonKeyLength + 1]) != 0)
                {
                    _zero = newEntry;
                    _one = copy;
                }
                else
                {
                    _zero = copy;
                    _one = newEntry;
                }
                _keyLength = commonKeyLength;
                UserData = EmptyData;
                return newEntry;
            }
        }

        #endregion

        #region Public instance members

        /// <summary>
        /// Minimizes a <see cref="BinaryTrieNode"/> by removing
        /// redundant child nodes.
        /// </summary>
        /// <returns>
        /// The number of nodes that were eliminated.
        /// </returns>
        public Int32 Minimize()
        {
            return Minimize(_userData);
        }

        /// <summary>
        /// Helper for the public <see cref="Minimize"/> method.
        /// </summary>
        /// <param name="userData">The user data.</param>
        /// <returns></returns>
        private Int32 Minimize(Object userData)
        {
            Int32 eliminatedNodes = 0;

            if (_userData == EmptyData)
                _userData = userData;

            if (null != _zero)
            {
                eliminatedNodes += _zero.Minimize(_userData);
                if (_zero._userData == _userData)
                {
                    eliminatedNodes++;
                    if ((_zero._zero == null) && (_zero._one == null))
                        _zero = null;
                    else if (_zero._one == null)
                        _zero = _zero._zero;
                    else if (_zero._zero == null)
                        _zero = _zero._one;
                    else
                        eliminatedNodes--;
                }
            }
            if (null != _one)
            {
                eliminatedNodes += _one.Minimize(_userData);
                if (_one._userData == _userData)
                {
                    eliminatedNodes++;
                    if ((_one._zero == null) && (_one._one == null))
                        _one = null;
                    else if (_one._one == null)
                        _one = _one._zero;
                    else if (_one._zero == null)
                        _one = _one._one;
                    else
                        eliminatedNodes--;
                }
            }
            return eliminatedNodes;
        }

        /// <summary>
        /// Finds an exact match to a key.
        /// </summary>
        /// <param name="key">The key to look up.</param>
        /// <returns>
        /// The <see cref="BinaryTrieNode"/> that
        /// matches the specified <paramref name="key"/>,
        /// or <b>null</b> (<b>Nothing</b> in Visual Basic)
        /// if the key can't be found.
        /// </returns>
        public BinaryTrieNode FindExactMatch(Int32 key)
        {
            if ((key ^ _key) == 0)
                return this;

            if ((key & _bit[_keyLength + 1]) == 0)
            {
                if (null != _zero)
                {
                    if ((key ^ _zero._key) < _bit[_zero._keyLength])
                        return _zero.FindExactMatch(key);
                }
            }
            else
            {
                if (null != _one)
                {
                    if ((key ^ _one._key) < _bit[_one._keyLength])
                        return _one.FindExactMatch(key);
                }
            }
            return null;
        }

        /// <summary>
        /// Looks up a key value in the trie.
        /// </summary>
        /// <param name="key">The key to look up.</param>
        /// <returns>
        /// The best matching <see cref="BinaryTrieNode"/>
        /// in the trie.
        /// </returns>
        public BinaryTrieNode FindBestMatch(Int32 key)
        {
            BinaryTrieNode best = this;
            BinaryTrieNode current = this;

            while (true)
            {
                if ((key & _bit[current._keyLength + 1]) == 0)
                {
                    current = current._zero;
                    if (null == current)
                        return best;
                    else if ((key ^ current._key) >= _bit[current._keyLength])
                        return best;
                    else
                        best = current;
                }
                else
                {
                    current = current._one;
                    if (null == current)
                        return best;
                    else if ((key ^ current._key) >= _bit[current._keyLength])
                        return best;
                    else
                        best = current;
                }
            }
        }

        #endregion
    }

    /// <summary>
    /// Iterates over the nodes of a <see cref="BinaryTrie"/>
    /// </summary>
    public class BinaryTrieEnumerator : IEnumerator
    {
        private BinaryTrieNode[] _stack = new BinaryTrieNode[32];
        private Int32 _stackPtr;
        private BinaryTrieNode _root;
        private BinaryTrieNode _currentNode;

        /// <summary>
        /// Constructs a new <b>BinaryTrieEnumerator</b>.
        /// </summary>
        /// <param name="root">The root node of the trie.</param>
        public BinaryTrieEnumerator(BinaryTrieNode root)
        {
            _root = root;
        }

        /// <summary>
        /// Implements the <b>Reset</b> method of
        /// <see cref="IEnumerator"/>.
        /// </summary>
        /// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created. </exception>
        public void Reset()
        {
            _stackPtr = 0;
            _stack[0] = _currentNode = _root;
        }

        /// <summary>
        /// Implements the <b>Current</b> property of
        /// <see cref="IEnumerator"/>.
        /// </summary>
        /// <value>
        /// The <see cref="BinaryTrieNode"/> at
        /// the current position in the iteration.
        /// </value>
        /// <returns>The current element in the collection.</returns>
        /// <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the
        /// first element of the collection or after the last element. </exception>
        public Object Current
        {
            get { return _stack[_stackPtr]; }
        }

        /// <summary>
        /// Implements the <b>MoveNext</b> method of
        /// <see cref="IEnumerator"/>.
        /// </summary>
        /// <returns>
        /// 	<b>false</b> if the end of the iteration
        /// has been reached, <b>true</b> otherwise.
        /// </returns>
        /// <exception cref="T:System.InvalidOperationException">The collection was modified after
        /// the enumerator was created. </exception>
        public Boolean MoveNext()
        {
            if (null != _currentNode.ZeroNode)
            {
                _stack[++_stackPtr] = _currentNode = _currentNode.ZeroNode;
                return true;
            }
            if (null != _currentNode.OneNode)
            {
                _stack[++_stackPtr] = _currentNode = _currentNode.OneNode;
                return true;
            }

            while (_stackPtr > 0)
            {
                _stackPtr--;
                BinaryTrieNode newNode = _stack[_stackPtr];

                if (newNode.ZeroNode == _currentNode)
                    if (null != newNode.OneNode)
                    {
                        _stack[++_stackPtr] = _currentNode = newNode.OneNode;
                        return true;
                    }
                _currentNode = newNode;
            }
            return false;
        }
    }
}