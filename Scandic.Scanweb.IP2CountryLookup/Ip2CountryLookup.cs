// <copyright file="Ip2CountryLookup.cs">
// Copyright � 2010 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>19-March-2010</date>
// <version>Hygiene release</version>
// <summary>static class; whose instance will only created when the first call 
// is been made. This will ensure lazy loading and make sure absolute performance.
// Do make changes to this class wisely.
// </summary>

using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Web;
using Scandic.Scanweb.Core.Core;

namespace Scandic.Scanweb.IP2Country
{
    /// <summary>
    /// ststic class through which the ip to country code mapping happens.
    /// </summary>
    public static class Ip2CountryLookup
    {
        /// <summary>
        /// RIR database cache key.
        /// </summary>
        private static string RIR_DATABASE_LOOKUPTABLE_CACHE = "RIRDatabaseLookupTrie";

        /// <summary>
        /// Initializes the <see cref="Ip2CountryLookup"/> class.
        /// </summary>
        static Ip2CountryLookup()
        {
            LoadIpDatabase(16);
        }

        /// <summary>
        /// ip to country table
        /// </summary>
        private static IPCountryTable table;

        /// <summary>
        /// Loads the ip database.
        /// </summary>
        /// <param name="indexLength">Length of the index.</param>
        public static void LoadIpDatabase(Int32 indexLength)
        {
            try
            {
                if (ScanwebCacheManager.Instance.LookInCache<IPCountryTable>(RIR_DATABASE_LOOKUPTABLE_CACHE) == null)
                {
                    table = new IPCountryTable(indexLength);

                    string codeFileName =
                        Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
                    codeFileName = codeFileName.Substring(0, codeFileName.LastIndexOf("\\"));

                    if (codeFileName.Contains("file:\\"))
                    {
                        codeFileName = codeFileName.Replace("file:\\", string.Empty);
                    }

                    table.LoadStatisticsFile(codeFileName + "\\RIR\\database\\ripencc.latest", true);
                    table.LoadStatisticsFile(codeFileName + "\\RIR\\database\\arin.latest", true);
                    table.LoadStatisticsFile(codeFileName + "\\RIR\\database\\apnic.latest", true);
                    table.LoadStatisticsFile(codeFileName + "\\RIR\\database\\lacnic.latest", true);

                    if (table != null)
                    {
                        ScanwebCacheManager.Instance.AddToCache(RIR_DATABASE_LOOKUPTABLE_CACHE, table);
                    }
                }
                else
                {
                    table = ScanwebCacheManager.Instance.LookInCache<IPCountryTable>(RIR_DATABASE_LOOKUPTABLE_CACHE);
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        /// <summary>
        /// Gets the country code from ip.
        /// </summary>
        /// <param name="ip">The ip.</param>
        /// <returns></returns>
        public static string GetCountryCodeFromIp(string ip)
        {
            if (table != null)
            {
                return table.GetCountry(ip);
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Comments by Aneesh : Use this method only if We need to check the speed with
        /// which 2,50,000 ip addresses can be resolved to country code.
        /// Loads the ip list.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static ArrayList LoadIpList(string name)
        {
            ArrayList result = new ArrayList(10000);

            FileStream fs = new FileStream(name, FileMode.Open);
            StreamReader sr = new StreamReader(fs);

            string line;

            while ((line = sr.ReadLine()) != null)
            {
                result.Add(line);
            }

            sr.Close();
            fs.Close();

            return result;
        }
    }
}