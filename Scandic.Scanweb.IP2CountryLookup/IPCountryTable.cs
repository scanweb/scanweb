//  Description					:   IPCountryTable                                        //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:   Aneesh Lal G A                                        //
/// Author email id				:                           							  //
/// Creation Date				:   19-March-2010                						  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;

namespace Scandic.Scanweb.IP2Country
{
    #region System NameSpaces

    

    #endregion

    /// <summary>
    /// Represents a trie that can be used to look up the country
    /// corresponding to an IP address.
    /// </summary>
    public class IPCountryTable : BinaryTrie
    {
        private Int32 _extraNodes = 0;

        /// <summary>
        /// Gets the length of the key.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        protected static Int32 GetKeyLength(Int32 length)
        {
            if (length < 0)
                return 1;
            Int32 keyLength = 33;
            while (length != 0)
            {
                length >>= 1;
                keyLength--;
            }
            return keyLength;
        }

        private Int32 _indexOffset;
        private Int32 _indexLength;

        /// <summary>
        /// Constructs an <see cref="IPCountryTable3"/> object.
        /// </summary>
        /// <param name="indexLength">Length of the index.</param>
        public IPCountryTable(Int32 indexLength) : base(indexLength)
        {
            _indexOffset = 32 - indexLength;
            _indexLength = 1 << _indexOffset;
        }

        /// <summary>
        /// Loads an IP-country database file into the trie.
        /// </summary>
        /// <param name="filename">The path and filename of the file
        /// that holds the database.</param>
        /// <param name="calculateKeyLength">A boolean value that
        /// indicates whether the <em>size</em> field in the database
        /// contains the total length (<strong>true</strong>) or the
        /// exponent of the length (<strong>false</strong> of the
        /// allocated segment.</param>
        public void LoadStatisticsFile(String filename, Boolean calculateKeyLength)
        {
            try
            {
                StreamReader reader = new StreamReader(filename);

                try
                {
                    String record;
                    while (null != (record = reader.ReadLine()))
                    {
                        String[] fields = record.Split('|');

                        if (fields.Length != 7)
                            continue;
                        if (fields[2] != "ipv4")
                            continue;
                        if (fields[1] == "*")
                            continue;

                        String ip = fields[3];

                        String countryCode = String.Intern(fields[1]);

                        Int32 length = Int32.Parse(fields[4]);

                        if (!calculateKeyLength)
                            length = 1 << length;

                        String[] parts = ip.Split('.');
                        Int32 indexBase = ((Int32.Parse(parts[0]) << 8)
                                           + Int32.Parse(parts[1]));
                        Int32 keyBase = (indexBase << 16)
                                        + (Int32.Parse(parts[2]) << 8)
                                        + Int32.Parse(parts[3]);
                        indexBase >>= (_indexOffset - 16);

                        Int32 currentLength;
                        Int32 keyLength;

                        Int32 lengthToFill = keyBase & (_indexLength - 1);
                        if (lengthToFill != 0)
                        {
                            lengthToFill = Math.Min(length, _indexLength - lengthToFill);
                            length -= lengthToFill;
                        }

                        currentLength = _indexLength;
                        keyLength = 32 - _indexOffset;
                        while (lengthToFill > 0)
                        {
                            currentLength >>= 1;
                            keyLength++;
                            if ((lengthToFill & currentLength) != 0)
                            {
                                base.AddInternal(indexBase, keyBase, keyLength).UserData = countryCode;
                                _extraNodes++;
                                keyBase += currentLength;
                                lengthToFill -= currentLength;
                                if (lengthToFill == 0)
                                {
                                    indexBase++;
                                    break;
                                }
                            }
                        }

                        while (length >= _indexLength)
                        {
                            base.AddInternal(indexBase, keyBase, _indexOffset).UserData = countryCode;
                            _extraNodes++;
                            indexBase++;
                            keyBase += _indexLength;
                            length -= _indexLength;
                        }

                        currentLength = _indexLength;
                        keyLength = 32 - _indexOffset;
                        while (length > 0)
                        {
                            currentLength >>= 1;
                            keyLength++;
                            if ((length & currentLength) != 0)
                            {
                                base.AddInternal(indexBase, keyBase, keyLength).UserData = countryCode;
                                _extraNodes++;
                                keyBase += currentLength;
                                length -= currentLength;
                            }
                        }
                        _extraNodes--;
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        /// <summary>
        /// Gets the total number of entries in the trie.
        /// </summary>
        /// <value>The network code count.</value>
        public Int32 NetworkCodeCount
        {
            get { return base.Count - _extraNodes; }
        }

        /// <summary>
        /// Attempts to find the country code corresponding to
        /// a given IP address.
        /// </summary>
        /// <param name="address">A <see cref="String"/> value
        /// representing the</param>
        /// <returns>
        /// The two letter country code corresponding to
        /// the IP address, or <strong>"??"</strong> if it was not
        /// found.
        /// </returns>
        public String GetCountry(String address)
        {
            String[] parts = address.Split('.');

            Int32 indexBase = ((Int32.Parse(parts[0]) << 8)
                               + Int32.Parse(parts[1]));
            Int32 index = indexBase >> (_indexOffset - 16);

            BinaryTrieNode root = base.Roots[index];
            if (null == root)
                return null;

            Int32 key = (indexBase << 16)
                        + (Int32.Parse(parts[2]) << 8)
                        + Int32.Parse(parts[3]);
            return (String) root.FindBestMatch(key).UserData;
        }
    }
}