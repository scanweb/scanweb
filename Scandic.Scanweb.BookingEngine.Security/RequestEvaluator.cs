///  Description					  : This class helps evaluate the security requirements
///                                     of a page reuqest, based on the configuration.
///----------------------------------------------------------------------------------------
/// Author						      : Nandkishore M
/// Creation Date				      : 28-Dec-2007									  
///	Version	#					      : 1.0													  
///---------------------------------------------------------------------------------------
/// Revision History				  : -NA-												
///	Last Modified Date			      :														 

using System;
using System.Globalization;
using System.Web;
using System.Web.Configuration;
using Scandic.Scanweb.BookingEngine.Security.Configuration;

namespace Scandic.Scanweb.BookingEngine.Security
{
    /// <summary>
    /// Represents an evaluator for requests that determines if it should be secure
    /// based on the settings in the configuration.
    /// </summary>
    public sealed class RequestEvaluator
    {
        /// <summary>
        /// Evaluates a given request against specified settings for the type of security action required
        /// to fulfill the request properly.
        /// </summary>
        /// <param name="request">The request to evaluate.</param>
        /// <param name="settings">The settings to evaluate against.</param>
        /// <param name="forceEvaluation">
        /// A flag indicating whether or not to force evaluation, despite the mode set.
        /// </param>
        /// <returns>A SecurityType value for the appropriate action.</returns>
        public static SecurityType Evaluate(HttpRequest request, SecureWebPageSettings settings, bool forceEvaluation)
        {
            SecurityType Result = SecurityType.Ignore;

            if (forceEvaluation || RequestMatchesMode(request, settings.Mode))
            {
                if (settings.IgnoreHandlers == SecureWebPageIgnoreHandlers.BuiltIn && !IsBuiltInHandlerRequest(request) ||
                    settings.IgnoreHandlers == SecureWebPageIgnoreHandlers.WithStandardExtensions &&
                    !IsStandardHandlerRequest(request) ||
                    settings.IgnoreHandlers == SecureWebPageIgnoreHandlers.None)
                {
                    string RelativeFilePath =
                        HttpUtility.UrlDecode(request.Url.AbsolutePath).
                            Remove(0, request.ApplicationPath.Length).ToLower(CultureInfo.CurrentCulture);
                    if (RelativeFilePath.StartsWith("/"))
                        RelativeFilePath = RelativeFilePath.Substring(1);

                    string RelativeDirectory = string.Empty;
                    int i = RelativeFilePath.LastIndexOf('/');
                    if (i >= 0)
                        RelativeDirectory = RelativeFilePath.Substring(0, i);

                    i = settings.Files.IndexOf(RelativeFilePath);
                    if (i >= 0)
                        Result = settings.Files[i].Secure;
                    else
                    {
                        int j = -1;
                        i = 0;
                        while (i < settings.Directories.Count)
                        {
                            if ((settings.Directories[i].Recurse
                                 &&
                                 RelativeDirectory.StartsWith(settings.Directories[i].Path,
                                                              StringComparison.CurrentCultureIgnoreCase) ||
                                 RelativeDirectory.Equals(settings.Directories[i].Path,
                                                          StringComparison.CurrentCultureIgnoreCase)) &&
                                (j == -1 || settings.Directories[i].Path.Length > settings.Directories[j].Path.Length))
                                j = i;

                            i++;
                        }

                        if (j > -1)
                            Result = settings.Directories[j].Secure;
                        else
                            Result = SecurityType.Insecure;
                    }
                }
            }

            return Result;
        }

        /// <summary>
        /// Evaluates a given request against configured settings for the type of security action required
        /// to fulfill the request properly.
        /// </summary>
        /// <param name="request">The request to evaluate.</param>
        /// <returns>A SecurityType value for the appropriate action.</returns>
        public static SecurityType Evaluate(HttpRequest request)
        {
            SecureWebPageSettings Settings =
                WebConfigurationManager.GetSection("secureWebPages") as SecureWebPageSettings;
            return Evaluate(request, Settings, false);
        }

        /// <summary>
        /// Determines if the specified request is for one of the built-in HTTP handlers.
        /// </summary>
        /// <param name="request">The HttpRequest to test.</param>
        /// <returns>True if the request is for a built-in HTTP handler; false otherwise.</returns>
        private static bool IsBuiltInHandlerRequest(HttpRequest request)
        {
            string FileName = request.Url.Segments[request.Url.Segments.Length - 1];
            return (
                       string.Compare(FileName, "trace.axd", true, CultureInfo.InvariantCulture) == 0 ||
                       string.Compare(FileName, "webresource.axd", true, CultureInfo.InvariantCulture) == 0
                   );
        }

        /// <summary>
        /// Determines if the specified request is for a standard HTTP handler (.axd).
        /// </summary>
        /// <param name="request">The HttpRequest to test.</param>
        /// <returns>True if the request is for a standard HTTP handler (.axd or .ashx); false otherwise.</returns>
        private static bool IsStandardHandlerRequest(HttpRequest request)
        {
            string Path = request.Url.AbsolutePath;
            return (Path.EndsWith(".axd", true, CultureInfo.InvariantCulture)
                    || Path.EndsWith(".ashx", true, CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Tests the given request to see if it matches the specified mode.
        /// </summary>
        /// <param name="request">A HttpRequest to test.</param>
        /// <param name="mode">The SecureWebPageMode used in the test.</param>
        /// <returns>
        ///		Returns true if the request matches the mode as follows:
        ///		<list type="disc">
        ///			<item>If mode is On.</item>
        ///			<item>If mode is set to RemoteOnly and the request is from a computer other than the server.</item>
        ///			<item>If mode is set to LocalOnly and the request is from the server.</item>
        ///		</list>
        ///	</returns>
        private static bool RequestMatchesMode(HttpRequest request, SecureWebPageMode mode)
        {
            switch (mode)
            {
                case SecureWebPageMode.On:
                    return true;

                case SecureWebPageMode.RemoteOnly:
                    return (request.ServerVariables["REMOTE_ADDR"] != request.ServerVariables["LOCAL_ADDR"]);

                case SecureWebPageMode.LocalOnly:
                    return (request.ServerVariables["REMOTE_ADDR"] == request.ServerVariables["LOCAL_ADDR"]);

                default:
                    return false;
            }
        }
    }
}