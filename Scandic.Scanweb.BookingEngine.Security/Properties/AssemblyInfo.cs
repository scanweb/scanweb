﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Scandic.Scanweb.BookingEngine.Security")]
[assembly: AssemblyDescription("A module to help switch between secure and unsecure requests.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("Scandic.Scanweb.BookingEngine.Security")]
[assembly: CLSCompliant(true)]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("C272C6C9-3DDE-459c-93C3-BB7FEE40221D")]