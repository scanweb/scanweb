using System;
using System.Globalization;
using System.Web;
using Scandic.Scanweb.BookingEngine.Security.Configuration;

namespace Scandic.Scanweb.BookingEngine.Security {

	/// <summary>
	/// Provides static methods for ensuring that a page is rendered 
	/// securely via SSL or unsecurely.
	/// </summary>
	public sealed class SslHelper {
		private const string UnsecureProtocolPrefix = "http://";
		private const string SecureProtocolPrefix = "https://";

		/// <summary>
		/// Prevent creating an instance of this class.
		/// </summary>
		private SslHelper() {
		}

		/// <summary>
		/// Determines the secure page that should be requested if a redirect occurs.
		/// </summary>
		/// <param name="settings">The SecureWebPageSettings to use in determining.</param>
		/// <param name="ignoreCurrentProtocol">
		/// A flag indicating whether or not to ingore the current protocol when determining.
		/// </param>
		/// <returns>A string containing the absolute URL of the secure page to redirect to.</returns>
		/// <exception cref="ArgumentNullException"></exception>
		public static string DetermineSecurePage(SecureWebPageSettings settings, bool ignoreCurrentProtocol) {
			if (settings == null)
				throw new ArgumentNullException("settings");

			string Result = null;
			HttpRequest Request = HttpContext.Current.Request;
			string RequestPath = Request.Url.AbsoluteUri;
			if (ignoreCurrentProtocol || RequestPath.StartsWith(UnsecureProtocolPrefix)) {
				if (string.IsNullOrEmpty(settings.EncryptedUri))
					Result = string.Concat(
						SecureProtocolPrefix,
                        Request.Url.Authority, HttpContext.Current.Response.ApplyAppPathModifier(Request.RawUrl)
					);
				else
					Result = BuildUrl(true, settings.MaintainPath, settings.EncryptedUri, settings.UnencryptedUri);
			}

			return Result;
		}

		/// <summary>
		/// Determines the unsecure page that should be requested if a redirect occurs.
		/// </summary>
		/// <param name="settings">The SecureWebPageSettings to use in determining.</param>
		/// <param name="ignoreCurrentProtocol">
		/// A flag indicating whether or not to ingore the current protocol when determining.
		/// </param>
		/// <returns>A string containing the absolute URL of the unsecure page to redirect to.</returns>
		public static string DetermineUnsecurePage(SecureWebPageSettings settings, bool ignoreCurrentProtocol) {
			if (settings == null)
				throw new ArgumentNullException("settings");
			
			string Result = null;
			HttpRequest Request = HttpContext.Current.Request;
			string RequestPath = Request.Url.AbsoluteUri;
			if (ignoreCurrentProtocol || RequestPath.StartsWith(SecureProtocolPrefix)) {
				if (string.IsNullOrEmpty(settings.UnencryptedUri))
					Result = string.Concat(
						UnsecureProtocolPrefix,
                        Request.Url.Authority, HttpContext.Current.Response.ApplyAppPathModifier(Request.RawUrl)
					);
				else
					Result = BuildUrl(false, settings.MaintainPath, settings.EncryptedUri, settings.UnencryptedUri);
			}

			return Result;
		}

		/// <summary>
		/// Requests the current page over a secure connection, if it is not already.
		/// </summary>
		/// <param name="settings">The SecureWebPageSettings to use for this request.</param>
		public static void RequestSecurePage(SecureWebPageSettings settings) {
			string ResponsePath = DetermineSecurePage(settings, false);
			if (!string.IsNullOrEmpty(ResponsePath))
				HttpContext.Current.Response.Redirect(ResponsePath, true);
		}

		/// <summary>
		/// Requests the current page over an unsecure connection, if it is not already.
		/// </summary>
		/// <param name="settings">The SecureWebPageSettings to use for this request.</param>
		public static void RequestUnsecurePage(SecureWebPageSettings settings) {
			if (settings == null)
				throw new ArgumentNullException("settings");
			
			string ResponsePath = DetermineUnsecurePage(settings, false);
			if (!string.IsNullOrEmpty(ResponsePath)) {
				HttpRequest Request = HttpContext.Current.Request;
				bool Bypass;
				if (settings.WarningBypassMode == SecurityWarningBypassMode.AlwaysBypass)
					Bypass = true;
				else if (settings.WarningBypassMode == SecurityWarningBypassMode.BypassWithQueryParam &&
						Request.QueryString[settings.BypassQueryParamName] != null) {
					Bypass = true;
					System.Text.StringBuilder NewPath = new System.Text.StringBuilder(ResponsePath);
					int i = ResponsePath.LastIndexOf(string.Format("?{0}=", settings.BypassQueryParamName));
					if (i < 0)
						i = ResponsePath.LastIndexOf(string.Format("&{0}=", settings.BypassQueryParamName));
					NewPath.Remove(i, settings.BypassQueryParamName.Length + Request.QueryString[settings.BypassQueryParamName].Length + 1);

					if (i >= NewPath.Length)
						i = NewPath.Length - 1;
					if (NewPath[i] == '&')
						NewPath.Remove(i, 1);
					i = NewPath.Length - 1;
					if (NewPath[i] == '?')
						NewPath.Remove(i, 1);

					ResponsePath = NewPath.ToString();
				} else
					Bypass = false;

				HttpResponse Response = HttpContext.Current.Response;
				if (Bypass) {
					Response.Clear();

					Response.AddHeader("Refresh", string.Concat("0;URL=", ResponsePath));
					Response.Write("<html><head><title></title>");
					Response.Write("<!-- <script language=\"javascript\">window.location.replace(\"");
					Response.Write(ResponsePath);
					Response.Write("\");</script> -->");
					Response.Write("</head><body></body></html>");

					Response.End();
				} else
					Response.Redirect(ResponsePath, true);
			}
		}

		/// <summary>
		/// Builds a URL from the given protocol and appropriate host path. The resulting URL 
		/// will maintain the current path if requested.
		/// </summary>
		/// <param name="secure">Is this to be a secure URL?</param>
		/// <param name="maintainPath">Should the current path be maintained during transfer?</param>
		/// <param name="encryptedUri">The URI to redirect to for encrypted requests.</param>
		/// <param name="unencryptedUri">The URI to redirect to for standard requests.</param>
        /// <returns>URL</returns>
		private static string BuildUrl(bool secure, bool maintainPath, string encryptedUri, string unencryptedUri) {

			encryptedUri = CleanHostUri(string.IsNullOrEmpty(encryptedUri) ? unencryptedUri : encryptedUri);
			unencryptedUri = CleanHostUri(string.IsNullOrEmpty(unencryptedUri) ? encryptedUri : unencryptedUri);
			HttpRequest Request = HttpContext.Current.Request;
			System.Text.StringBuilder Url = new System.Text.StringBuilder();
			if (secure)
				Url.Append(encryptedUri);
			else
				Url.Append(unencryptedUri);

			if (maintainPath)
				Url.Append(Request.CurrentExecutionFilePath).Append(Request.Url.Query);
			else {
				string CurrentUrl = Request.Url.AbsolutePath;
				Url.Append(CurrentUrl.Substring(CurrentUrl.LastIndexOf('/') + 1)).Append(Request.Url.Query);
			}
			Url.Replace("//", "/");
			if (secure)
				Url.Insert(0, SecureProtocolPrefix);
			else
				Url.Insert(0, UnsecureProtocolPrefix);

			return Url.ToString();
		}

		/// <summary>
		/// Cleans a host path by stripping out any unneeded elements.
		/// </summary>
		/// <param name="uri">The host URI to validate.</param>
		/// <returns>Returns a string that is stripped as needed.</returns>
		private static string CleanHostUri(string uri) {
			string Result = string.Empty;
			if (!string.IsNullOrEmpty(uri)) {
				if (!uri.StartsWith(UnsecureProtocolPrefix) && !uri.StartsWith(SecureProtocolPrefix))
					uri = UnsecureProtocolPrefix + uri;

				Uri HostUri = new Uri(uri);
				Result = string.Concat(HostUri.Authority, HostUri.AbsolutePath);
				if (!Result.EndsWith("/"))
					Result += "/";
			}

			return Result;
		}

	}

}
