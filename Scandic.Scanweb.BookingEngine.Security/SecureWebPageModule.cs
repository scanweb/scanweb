///  Description					  : This class implements HTTP Modules to screen
///                                     requests and to help in switiching between secure
///                                     and non-secure calls to the server.
///----------------------------------------------------------------------------------------
/// Author						      : Nandkishore M
/// Creation Date				      : 28-Dec-2007									  
///	Version	#					      : 1.0													  
///---------------------------------------------------------------------------------------
/// Revision History				  : -NA-												
///	Last Modified Date			      :														 

using System;
using System.Web;
using System.Web.Configuration;
using System.Web.SessionState;
using Scandic.Scanweb.BookingEngine.Security.Configuration;

namespace Scandic.Scanweb.BookingEngine.Security
{
    /// <summary>
    /// Helps to evaluate a request and switch between http and https.
    /// </summary>
    public class SecureWebPageModule : IHttpModule, IRequiresSessionState
    {
        private const string SECURE_PROTOCOL_STRING = "https://";
        private const string INSECURE_PROTOCOL_STRING = "http://";
        private const string LOGIN_STATUS_VAR = "IsLoggedInUser";
        private const string SECURE_CONFIG_SECTION = "secureWebPages";
        private const string SECURE_SETTINGS_VAR = "SecureWebPageSettings";

        /// <summary>
        /// Initializes an instance of this class.
        /// </summary>
        public SecureWebPageModule()
        {
        }

        /// <summary>
        /// Disposes of any resources used.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Initializes the module.
        /// </summary>
        /// <param name="context">The HttpApplication object this module is bound to.</param>
        public void Init(HttpApplication context)
        {
            if (context != null)
            {
                SecureWebPageSettings Settings =
                    WebConfigurationManager.GetSection(SECURE_CONFIG_SECTION) as SecureWebPageSettings;
                if (Settings != null && Settings.Mode != SecureWebPageMode.Off)
                {
                    context.Application[SECURE_SETTINGS_VAR] = Settings;

                    context.AcquireRequestState -= new EventHandler(this.Application_ProcessRequest);
                    context.AcquireRequestState += new EventHandler(this.Application_ProcessRequest);
                }
            }
        }

        /// <summary>
        /// Evaluate the security configuration of this request.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="e">Event arguments.</param>
        private void Application_ProcessRequest(Object source, EventArgs e)
        {
            HttpApplication application = source as HttpApplication;
            HttpContext Context = application.Context;

            if (Context != null && Context.Session != null && Context.Session.Count > 0)
            {
                string Result = string.Empty;

                if (Context.Session.IsNewSession)
                {
                    string cookieHeader = Context.Request.Headers["Cookie"];
                    string cookieValue = string.Empty;
                    if (Context.Request.Cookies[LOGIN_STATUS_VAR] != null)
                    {
                        cookieValue = Context.Request.Cookies[LOGIN_STATUS_VAR].Value;
                    }

                    if (cookieValue == bool.TrueString &&
                        !string.IsNullOrEmpty(cookieHeader) && cookieHeader.IndexOf("ASP.NET_SessionId") >= 0)
                    {
                        if (HttpContext.Current.Response.Cookies["ActiveTab"] != null)
                        {
                            HttpContext.Current.Response.Cookies["ActiveTab"].Value = BookingTab.Tab1;
                        }
                        Context.Response.Cookies[LOGIN_STATUS_VAR].Value = bool.FalseString;
                        //Result = "/?SessionExpired=true";
                    }
                }
                //if (!string.IsNullOrEmpty(Result))
                //    Context.Response.Redirect(Result);
            }
        }
    }
}