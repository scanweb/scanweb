using System;
using BVNetwork.FileNotFound.Redirects;
using EPiServer.PlugIn;
using EPiServer.Security;
using BVNetwork.FileNotFound.DataStore;
using System.Collections.Generic;

namespace BVNetwork.FileNotFound.Admin
{
    /// <summary>
    /// </summary>
    [GuiPlugIn(DisplayName = "File Not Found Handler",
           Description = "File Not Found Handler for customized redirects",
               Area = EPiServer.PlugIn.PlugInArea.AdminMenu,
               Url = "~/bvn/filenotfound/admin/default.aspx")]
    public partial class Default : EPiServer.UI.SystemPageBase
    {

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            if (!PrincipalInfo.HasAdminAccess)
            {
                AccessDenied();
            }

            this.MasterPageFile = ResolveUrlFromUI("MasterPages/EPiServerUI.Master");
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //List all redirects in repeater
            CustomRedirectHandler handler = CustomRedirectHandler.Current;

            DataStoreHandler dsHandler = new DataStoreHandler();
            List<CustomRedirect> redirectList = dsHandler.GetCustomRedirects(false);

            if (redirectList.Count == 0 && handler.CustomRedirects.Count == 0)
            {
                lblInfo.Text = "No custom redirects could be found in the dynamic data store, and no custom redirects could be loaded from default XML-file.";
                rptRedirects.Visible = false;
                lblListInfo.Visible = false;

            }

            else
            {
                rptRedirects.Visible = true;
                lblListInfo.Visible = true;
                rptRedirects.DataSource = handler.CustomRedirects;
                rptRedirects.DataBind();

                if (redirectList.Count != 0)
                    lblInfo.Text = "You currently have " + redirectList.Count + " custom redirects stored in the dynamic data store. Use the Custom Redirects Manager gadget to manage your custom redirects.";
                else if (redirectList.Count == 0 && handler.CustomRedirects.Count != 0)
                {
                    lblInfo.Text = "You do not have any custom redirects stored in the dynamic data store. Values have been loaded from the default XML file.";
                }
            }
            


        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            SystemMessageContainer.Heading = "Active redirects";
            lblHandlerStatus.Text = Configuration.Configuration.FileNotFoundHandlerMode.ToString();
        }

        public override void ValidatePageTemplate()
        {
            // Do not validate
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            DataStoreHandler dynamicHandler = new DataStoreHandler();
            // Read all redirects from xml file
            RedirectsXmlParser parser = new RedirectsXmlParser(xmlInput.PostedFile.InputStream);
            // Save all redirects from xml file
            CustomRedirectCollection redirects = parser.Load();
            if (redirects != null || redirects.Count != 0)
                CustomRedirectHandler.Current.SaveCustomRedirects(redirects);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DataStoreHandler dynamicHandler = new DataStoreHandler();
            dynamicHandler.DeleteAllCustomRedirects();
            DataStoreEventHandlerHook.DataStoreUpdated();
        }
    }
}
