using EPiServer.Core;

namespace BVNetwork.FileNotFound.Content
{
	/// <summary>
	/// Summary description for PageContent.
	/// </summary>
	public class PageContent
	{
		const string DEF_TITLE = "Page Not Found";
		const string DEF_TOPTEXT = "We're sorry, but the page you requested could not be found<br><br>"; 
		const string DEF_LOOKING_FOR = "You were looking for:<br>";
		const string DEF_CAME_FROM = "<br>You came from:<br>";
		const string DEF_BOTTOM_TEXT = "<br><br><b>Visit <a href=\"/\">the front page</a><b>";

		private string _title = DEF_TITLE;
		private string _topText = DEF_TOPTEXT;
		private string _lookingFor = DEF_LOOKING_FOR;
		private string _cameFrom = DEF_CAME_FROM;
		private string _bottomText = DEF_BOTTOM_TEXT;

		public PageContent()
		{
			InitializeFromLanguageFile();
		}

		/// <summary>
		/// Initializes text strings from the language file.
		/// </summary>
		private void InitializeFromLanguageFile()
		{
            LanguageManager lang = LanguageManager.Instance;
			_title = lang.TranslateFallback("/templates/notfound/title", DEF_TITLE);
			_topText = lang.TranslateFallback("/templates/notfound/toptext", DEF_TOPTEXT);
			_lookingFor = lang.TranslateFallback("/templates/notfound/lookingfor", DEF_LOOKING_FOR);
			_cameFrom = lang.TranslateFallback("/templates/notfound/referer", DEF_CAME_FROM);
			_bottomText = lang.TranslateFallback("/templates/notfound/bottomtext", DEF_BOTTOM_TEXT);
		}

		/// <summary>
		/// Gets or sets the bottom text.
		/// </summary>
		/// <value></value>
		public string BottomText
		{
			get
			{
				return _bottomText;
			}
			set
			{
				_bottomText = value;
			}
		}
		/// <summary>
		/// Gets or sets the came from text.
		/// </summary>
		/// <value></value>
		public string CameFrom
		{
			get
			{
				return _cameFrom;
			}
			set
			{
				_cameFrom = value;
			}
		}
		/// <summary>
		/// Gets or sets the looking for text.
		/// </summary>
		/// <value></value>
		public string LookingFor
		{
			get
			{
				return _lookingFor;
			}
			set
			{
				_lookingFor = value;
			}
		}
		/// <summary>
		/// Gets or sets the top text.
		/// </summary>
		/// <value></value>
		public string TopText
		{
			get
			{
				return _topText;
			}
			set
			{
				_topText = value;
			}
		}

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value></value>
		public string Title
		{
			get
			{
				return _title;
			}
			set
			{
				_title = value;
			}
		}
	}
}
