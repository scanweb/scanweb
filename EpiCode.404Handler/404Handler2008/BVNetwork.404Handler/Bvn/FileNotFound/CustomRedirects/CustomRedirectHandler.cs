using System.Web;
using System.Web.Caching;
using EPiServer.Web.Hosting;
using EPiServer.Framework.Initialization;
using System.Web.Hosting;
using System.IO;
using System.Collections;
using BVNetwork.FileNotFound.DataStore;
using EPiServer.Framework;
using System;
using EPiServer;
using BVNetwork.Bvn.FileNotFound.Upgrade;

namespace BVNetwork.FileNotFound.Redirects
{
    /// <summary>
    /// Handler for custom redirects. Loads and caches the list of custom redirects
    /// to ensure performance. 
    /// </summary>
    public class CustomRedirectHandler
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string CACHE_KEY_CUSTOM_REDIRECT_HANDLER_INSTANCE = "BvnCustomRedirectHandler";
        private CustomRedirectCollection _customRedirects = null;

        #region constructors...
        // Should only be instanciated by the static Current method
        protected CustomRedirectHandler()
        {
        }
        #endregion

        /// <summary>
        /// The collection of custom redirects
        /// </summary>
        public CustomRedirectCollection CustomRedirects
        {
            get
            {
                return _customRedirects;
            }
        }

        /// <summary>
        /// Save a collection of redirects, and call method to raise an event in order to clear cache on all servers.
        /// </summary>
        /// <param name="redirects"></param>
        public void SaveCustomRedirects(CustomRedirectCollection redirects)
        {
            DataStoreHandler dynamicHandler = new DataStoreHandler();
            foreach (CustomRedirect redirect in redirects)
            {
                // Add redirect 
                dynamicHandler.SaveCustomRedirect(redirect);
            }
            DataStoreEventHandlerHook.DataStoreUpdated();
        }

        /// <summary>
        /// Read the custom redirects from the dynamic data store, and 
        /// stores them in the CustomRedirect property
        /// </summary>
        protected void LoadCustomRedirects()
        {
            DataStoreHandler dynamicHandler = new DataStoreHandler();
            _customRedirects = new CustomRedirectCollection();

            foreach (CustomRedirect redirect in dynamicHandler.GetCustomRedirects(false))
                _customRedirects.Add(redirect);
        }

        /// <summary>
        /// Read the custom redirects from the static XML file, and 
        /// stores them in the CustomRedirect property
        /// </summary>
        protected void LoadCustomRedirectsFromStaticXML()
        {
            Stream xmlContent = RedirectsXmlHelper.GetStaticXmlFile(Configuration.Configuration.CustomRedirectsXmlFile);
            RedirectsXmlParser xmlParser = new RedirectsXmlParser(xmlContent);
            foreach (CustomRedirect redirect in xmlParser.Load())
                _customRedirects.Add(redirect);
        }

        /// <summary>
        /// Static method for getting the current custom redirects handler. Will store
        /// the handler in cache after it has been used.
        /// </summary>
        /// <returns>An instanciated CustomRedirectHandler</returns>
        public static CustomRedirectHandler Current
        {
            get
            {
                _log.Debug("Begin: Get Current CustomRedirectHandler");
                // First check if there is a cached version of
                // this object
                CustomRedirectHandler handler = null;
                handler = GetHandlerFromCache();
                if (handler != null)
                {
                    _log.Debug("Returning cached handler.");
                    _log.Debug("End: Get Current CustomRedirectHandler");
                    // Got the cached version, return it
                    return handler;
                }

                // Not cached, we need to create it
                handler = new CustomRedirectHandler();
                // Load redirects with standard settings
                _log.Debug("Begin: Load custom redirects from dynamic data store");
                try
                {



                    handler.LoadCustomRedirects();
                }
                catch (Exception ex)
                {
                    _log.Error("An error occured while loading redirects from dds. CustomRedirectHandlerException has now been set. Exception:" + ex);
                    CustomRedirectHandlerException = ex.ToString();
                    Upgrader.Valid = false;
                }
                _log.Debug("End: Load custom redirects from dynamic data store");

                //if there are no data stored in the dynamic data store, read from the default XML-file which is defined in the web.config
                if (handler.CustomRedirects == null || handler.CustomRedirects.Count == 0)
                {
                    _log.Debug("No custom redirects stored in dynamic data store. Reading data from static XML-file instead.");
                    handler.LoadCustomRedirectsFromStaticXML();

                }

                // Store in cache
                StoreHandlerInCache(handler);

                _log.Debug("End: Get Current CustomRedirectHandler");
                return handler;
            }
        }

        /// <summary>
        /// Clears the redirect cache.
        /// </summary>
        public static void ClearCache()
        {
            EPiServer.CacheManager.Remove(CACHE_KEY_CUSTOM_REDIRECT_HANDLER_INSTANCE);
        }

        /// <summary>
        /// Gets the handler from the cache, if it has been stored there.
        /// </summary>
        /// <returns>An instanciated CustomRedirectHandler if found in the cache, null if not found</returns>
        private static CustomRedirectHandler GetHandlerFromCache()
        {
            CustomRedirectHandler handler = null;
            handler = EPiServer.CacheManager.Get(CACHE_KEY_CUSTOM_REDIRECT_HANDLER_INSTANCE) as CustomRedirectHandler;
            return handler;
        }


        /// <summary>
        /// Stores the redirect handler in the cache
        /// </summary>
        private static void StoreHandlerInCache(CustomRedirectHandler handler)
        {
            EPiServer.CacheManager.RuntimeCacheInsert(CACHE_KEY_CUSTOM_REDIRECT_HANDLER_INSTANCE,
                                                handler,
                                                null,
                                                Cache.NoAbsoluteExpiration,
                                                Cache.NoSlidingExpiration,
                                                CacheItemPriority.AboveNormal);
        }

        public static string CustomRedirectHandlerException { get; set; }


    }
}
