<configuration>
	<configSections>
        ...
        <section name="bvn404Handler"
                 type="BVNetwork.FileNotFound.Configuration.Bvn404HandlerConfiguration, BVNetwork.EPi404"/>
    </configSections>

	<!--
		Settings on bvn404Handler section:
		
		fileNotFoundPage
		----------------
		Optional
		You can override the location of the aspx file like this:
		fileNotFoundPage="/notfound.aspx"
		Default value: "~/bvn/filenotfound/notfound.aspx"

		handlerMode
		-----------
		Optional
		Flag to control if module should be enabled or not
		Values: Off, On, RemoteOnly
		Default value: "On"
		
		redirectsXmlFile
		----------------
		Optional
		Redirects are stored in the Dynamic Data Store, but you
		can still use the xml file. Mainly for backwards compatibility
		redirectsXmlFile="~/CustomRedirects.config"
		Default value: none

		logging
		----------------
		Optional
		Flag to control if logging of custom redirect suggestions should be enabled or not
		Values: Off, On.
		Default value: "On"

		bufferSize
		----------------
		Optional
		Determines how many log events should be buffered before saving to the database.
		Value type:int
		Default value: "30"

		threshold
		----------------
		Optional
		Determines how frequent the 404 errors are allowed to get logged. 
		Value type: int
		Default value: "5"



		bufferSize and threshold info and examples:
		-If the bufferSize is set to 0, the threshold value will be ignored, and every request will be logged immediately.
		
		-Example 1:
         100 errors in 5 seconds -
         (diff = seconds between first logged request and the last logged request in the buffer).
         buffer is 100, diff is 5 seconds, Threshold is 10 
         100 / 5 = 20.  Value is higher than threshold value. Do not log.
     
	    -Example 2:
         100 errors in 15 seconds -
         buffer is 100, diff is 15 seconds, Threshold is 10 
         100 / 15 = 6. Value is within threshold value. Log requests. 
		  
        -Average maximum allowed requests per second = Buffer / Threshold
         

	-->
	<bvn404Handler redirectsXmlFile="~/CustomRedirects.config"
				   handlerMode="On"
				   >
		<providers>
			<!--
				Register your own handler that will be given a chance to
				rewrite the url. The type must implement INotFoundHandler
				<add name="NameOfHandler"
				 type="Your.Handler.Here" />
			 -->
		</providers>
	</bvn404Handler>
</configuration>