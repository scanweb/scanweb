mkdir deployment
mkdir deployment\404
mkdir deployment\bin
mkdir deployment\bvn
mkdir deployment\bvn\FileNotFound
mkdir deployment\bvn\FileNotFound\Admin
mkdir deployment\lang
mkdir deployment\modules
mkdir deployment\modules\BVNetwork.FileNotFound.RedirectGadget
mkdir deployment\modules\BVNetwork.FileNotFound.RedirectGadget\Views
mkdir deployment\modules\BVNetwork.FileNotFound.RedirectGadget\Views\Redirect

mkdir deployment\modules\BVNetwork.FileNotFound.RedirectGadget\Content\
mkdir deployment\modules\BVNetwork.FileNotFound.RedirectGadget\Content\Images

mkdir deployment\modules\BVNetwork.FileNotFound.RedirectGadget\Scripts\



copy Gadgets\BVNetwork.404Handler.RedirectGadget\modules\BVNetwork.FileNotFound.RedirectGadget\Views\Redirect\*.* deployment\modules\BVNetwork.FileNotFound.RedirectGadget\Views\Redirect
copy Gadgets\BVNetwork.404Handler.RedirectGadget\modules\BVNetwork.FileNotFound.RedirectGadget\Content\Images\*.* deployment\modules\BVNetwork.FileNotFound.RedirectGadget\Content\Images
copy Gadgets\BVNetwork.404Handler.RedirectGadget\modules\BVNetwork.FileNotFound.RedirectGadget\Scripts\*.* deployment\modules\BVNetwork.FileNotFound.RedirectGadget\Scripts
copy Gadgets\BVNetwork.404Handler.RedirectGadget\modules\BVNetwork.FileNotFound.RedirectGadget\Content\RedirectGadget.css deployment\modules\BVNetwork.FileNotFound.RedirectGadget\Content
copy 404Handler\lang\*.* deployment\lang\
copy Gadgets\BVNetwork.404Handler.RedirectGadget\lang\*.* deployment\lang\
Copy Gadgets\BVNetwork.404Handler.RedirectGadget\bin\BVNetwork.EPi404.dll deployment\bin\
Copy Gadgets\BVNetwork.404Handler.RedirectGadget\bin\BVNetwork.FileNotFound.RedirectGadget.dll deployment\bin\
Copy 404Handler\Bvn\FileNotFound\Admin\default.aspx deployment\bvn\FileNotFound\Admin\
copy 404Handler\Bvn\FileNotFound\NotFound.aspx deployment\404\404notfound.aspx