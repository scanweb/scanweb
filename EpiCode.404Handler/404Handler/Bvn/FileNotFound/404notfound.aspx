﻿<%@ Page Language="c#" AutoEventWireup="false" Inherits="BVNetwork.FileNotFound.NotFoundBase" %>

<%-- 
    Note! This file has no code-behind. It inherits from the NotFoundBase class. You can 
    make a copy of this file into your own project, change the design and keep the inheritance 
    WITHOUT having to reference the BVNetwork.EPi404.dll assembly.
    
    If you want to use your own Master Page, inherit from SimplePageNotFoundBase instead of
    NotFoundBase, as that will bring in what is needed by EPiServer. Note! you do not need to
    create a page type for this 404 page. If you use the EPiServer API, and inherit from  
    SimplePageNotFoundBase, this page will run in the context of the site start page.
    
    Be very careful with the code you write here. If you reference resources that cannot be found
    you could end up in an infinite loop.
    
    The code behind file might do a redirect to a new page based on the redirect configuration if
    it matches the url not found. The Error event (where the rest of the redirection is done)
    might not run for .aspx files that are not found, instead it redirects here with the url it
    could not find in the query string.
    
    Available properties:
        Content (BVNetwork.FileNotFound.Content.PageContent)
            // Labels you can use - fetched from the language file
            Content.BottomText
            Content.CameFrom
            Content.LookingFor
            Content.TopText
            Content.Title
            
        UrlNotFound (string)
            The url that cannot be found
        
        Referer (string)
            The url that brought the user here
            It no referer, the string is empty (not null)
            
    If you are using a master page, you should add this:
        <meta content="noindex, nofollow" name="ROBOTS">
    to your head tag for this page (NOT all pages)
 --%>

<script runat="server" type="text/C#">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        // Add your own logic (like databinding) here
    }
</script>

<%@ Register TagPrefix="EPiServer" Namespace="EPiServer.WebControls" Assembly="EPiServer" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<meta charset="utf-8">
<title>404 Page not found : Scandic</title>
<script src="//assets.adobedtm.com/f82933439235c9f8e3572eb6b459bec214cebdec/satelliteLib-00edd51bf243e207e85a7f8b8b34d47ffcfbf6a0.js"></script>
<link rel="stylesheet" href="<%=ResolveUrl("~/404/error.css")%>?v=<%=CmsUtil.GetCSSVersion()%>" />
<style>
	/* custom links; shown on same line */
	a {display: inline}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="page-wrap">

        <div id="header">
            <img id="logo" src="/ImageVault/Images/id_14602/conversionFormatType_WebSafe/scope_0/ImageVaultHandler.aspx" alt="Scandic logo">
        </div>
        
        <div id="content" class="clearfix">
        
            <div id="sidebar">
                <h2>Contact us</h2>
                <p><strong>Scandic</strong><br>
                P.O. Box 6197<br>
                SE-102 33 Stockholm<br>
                Sweden<br>
                Visiting address: Sveav&#228;gen 167<br>
                Telephone: +46 8 517 350 00</p>
                
                <p><strong>If you like to make a reservation please contact Scandic Reservation and customer service at:</strong></p>
                
                <p>International: +46 8 517 517 20<br>
                Sweden:	08-517 517 00<br>
                Denmark: 33 48 04 00<br>
                Norway:	23 15 50 00<br>
                Finland: 0 200 818 00<br>
                or the hotel directly.</p>
            </div>
            
            <div id="main">
                <h1>Page not found</h1>
                <p><strong>Page is not found, please go to: </strong><a href="http://www.scandichotels.com">www.scandichotels.com</a></p>
                
                <p><strong>Sidan kan inte hittas, g&#229; till: </strong><a href="http://www.scandichotels.se">www.scandichotels.se</a></p>
                
                <p><strong>Siden findes ikke. Besøg </strong><a href="http://www.scandichotels.dk">www.scandichotels.dk</a></p>
                
                <p><strong>Sivua ei l&#228;ydy: </strong><a href="http://www.scandichotels.fi">www.scandichotels.fi</a></p>

		<p><strong>Seite nicht gefunden. Bitte gehen Sie zu: </strong><a href="http://www.scandichotels.de">www.scandichotels.de</a></p>

                <p><strong>Страница не найдена, пожалуйста, перейдите на: </strong><a href="http://www.scandichotels.ru">www.scandichotels.ru</a></p>
                
                <p><strong>Finner ikke siden: </strong>
                <a href="http://www.scandichotels.no">www.scandichotels.no</a></p>
            </div>
        </div>
        
        <div id="footer">
        	<p>&copy;Scandic</p>
        </div>
    
    </div>
    </div>
    </form>


<!-- Web Analythics JavaScripts -->
<script type="text/javascript" language="javascript" src="/Templates/Scanweb/Javascript/SiteCatalyst/s_code.js?v=<%=CmsUtil.GetJSVersion()%>"></script>
<!-- End Web Analythics JavaScripts -->
<script type="text/javascript" language="javascript">
    s.pageName = "";
    s.pageType = "errorPage";
    s.prop41 = "404page";


    var s_code = s.t();
    if (s_code) 
    document.write(s_code)//-->
</script>
<script type="text/javascript">    
    _satellite.pageBottom();
</script>
</body>
</html>
