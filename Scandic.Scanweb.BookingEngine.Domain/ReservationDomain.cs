//  Description					: Domain class for Reservation OWS Calls.			  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Himansu Senapati / Santhosh Yamsani / Shankar Dasgutpa  //
//  Author email id				:                           							  //
//  Creation Date				: 05th November  2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Services.Protocols;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Reservation;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using System.Xml.Serialization;
using System.IO;
using System.Linq;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;

namespace Scandic.Scanweb.BookingEngine.Domain
{
    /// <summary>
    /// Domain for Reservation functionality
    /// </summary>
    public class ReservationDomain : IReservationDomain
    {
        #region Public Methods

        /// <summary>
        /// Validate FGPNumber from opera
        /// </summary>
        /// <param name="guestInformation"></param>
        /// <returns>Returns true if FGP Number is valid</returns>
        public bool ValidateFGPNumber(GuestInformationEntity guestInformation)
        {
            ServiceProxies.Name.NameService service = OWSUtility.GetNameService();
            bool validFGP = true;
            ServiceProxies.Name.NameLookupRequest request = null;
            ServiceProxies.Name.NameLookupResponse response = null;
            AppLogger.LogInfoMessage(
                "Creating request object: OWSRequestConverter.ValidateFGPNumber(guestInfo, isSessionBooking)");
            try
            {
                request =
                    OWSRequestConverter.GetFGPNumberValidateRequest(guestInformation.LastName,
                                                                    guestInformation.GuestAccountNumber,
                                                                    AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE, false);
                AppLogger.LogInfoMessage("Fetching response: service.ValidateFGPNumber(request)");
                response = service.NameLookup(request);
                AppLogger.LogInfoMessage("Response received: service.ValidateFGPNumber(request)");

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL && response.Profiles.Length > 0
                    && response.Profiles[0] != null && ((response.Profiles[0].Item as ServiceProxies.Name.Customer) != null))
                {
                    if (((response.Profiles[0].Item as ServiceProxies.Name.Customer).PersonName != null) && string.Equals(((ServiceProxies.Name.Customer)response.Profiles[0].Item).PersonName.firstName, guestInformation.FirstName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        AppLogger.LogInfoMessage("ReservationDomain-ValidateFGPNumber resultStatusFlag: success");
                        validFGP = true;
                    }
                    else if (((response.Profiles[0].Item as ServiceProxies.Name.Customer).NativeName != null) && string.Equals(((ServiceProxies.Name.Customer)response.Profiles[0].Item).NativeName.firstName, guestInformation.FirstName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        AppLogger.LogInfoMessage("ReservationDomain-ValidateFGPNumber resultStatusFlag: success");
                        validFGP = true;
                    }
                    if (response.Profiles[0].ProfileIDs != null && response.Profiles[0].ProfileIDs.Length > 0)
                        guestInformation.NameID = response.Profiles[0].ProfileIDs[0].Value;
                }
                else
                {
                    request = OWSRequestConverter.GetFGPNumberValidateRequest(guestInformation.LastName, guestInformation.GuestAccountNumber,
                                                                              AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE, true);
                    response = service.NameLookup(request);

                    if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL && response.Profiles.Length > 0
                    && response.Profiles[0] != null && ((response.Profiles[0].Item as ServiceProxies.Name.Customer) != null))
                    {
                        if (((response.Profiles[0].Item as ServiceProxies.Name.Customer).PersonName != null) && string.Equals(((ServiceProxies.Name.Customer)response.Profiles[0].Item).PersonName.firstName, guestInformation.FirstName, StringComparison.CurrentCultureIgnoreCase))
                        {
                            validFGP = true;
                        }
                        else if (((response.Profiles[0].Item as ServiceProxies.Name.Customer).NativeName != null) && string.Equals(((ServiceProxies.Name.Customer)response.Profiles[0].Item).NativeName.firstName, guestInformation.FirstName, StringComparison.CurrentCultureIgnoreCase))
                        {
                            validFGP = true;
                        }
                        if (response.Profiles[0].ProfileIDs != null && response.Profiles[0].ProfileIDs.Length > 0)
                            guestInformation.NameID = response.Profiles[0].ProfileIDs[0].Value;
                    }
                    else
                    {
                        UserNavTracker.TrackOWSRequestResponse<ServiceProxies.Name.NameLookupRequest, ServiceProxies.Name.NameLookupResponse>(request, response);
                        AppLogger.LogInfoMessage("ReservationDomain-ValidateFGPNumber resultStatusFlag: failed");
                        validFGP = false;
                        Hashtable exceptionData = new Hashtable();
                        exceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                    }
                }
            }
            catch (Exception)
            {
                UserNavTracker.TrackOWSRequestResponse<ServiceProxies.Name.NameLookupRequest, ServiceProxies.Name.NameLookupResponse>(request, response);
            }
            finally
            {
                request = null;
                response = null;
            }
            if (validFGP)
                guestInformation.IsCreatebookingRequestWithNativeName = true;
            return validFGP;
        }

        /// <summary>
        /// Makes the reservation.
        /// </summary>
        /// <param name="hotelSearch">The hotel search.</param>
        /// <param name="roomSearch">The room search.</param>
        /// <param name="roomRate">The room rate.</param>
        /// <param name="guestInformation">The guest information.</param>
        /// <param name="reservationNumber">The reservation number.</param>
        /// <param name="sourceCode">The source code.</param>
        /// <param name="createNameID">The create name ID.</param>
        /// <param name="isSessionBooking">if set to <c>true</c> [is session booking].</param>
        /// <param name="partnerID">The partner ID.</param>
        /// <returns>SortedList</returns>
        public SortedList<string, string> MakeReservation(HotelSearchEntity hotelSearch,
                                                          HotelSearchRoomEntity roomSearch, HotelRoomRateEntity roomRate,
                                                          GuestInformationEntity guestInformation,
                                                          string reservationNumber, string sourceCode,
                                                          out string createNameID, bool isSessionBooking,
                                                          string partnerID)
        {
            AppLogger.LogInfoMessage("In ReservationDomain\\MakeReservation()");
            createNameID = string.Empty;
            SortedList<string, string> legConfirmationMap = new SortedList<string, string>();
            CreateBookingRequest request = null;
            CreateBookingResponse response = null;
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogInfoMessage("OWSUtility.GetReservationService()");
                if (!guestInformation.IsNewlyEnrolled && guestInformation.GuestAccountNumber != null &&
                    guestInformation.GuestAccountNumber != string.Empty)
                {
                    guestInformation.IsValidFGPNumber = this.ValidateFGPNumber(guestInformation);
                }
                ReservationService service = OWSUtility.GetReservationService(isSessionBooking);
                AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetBookingRequest(hotelSearch, roomRate, guestInformation)");
                AppLogger.LogInfoMessage("Fetching response: service.CreateBooking(request)");
                request = OWSRequestConverter.GetBookingRequest(hotelSearch, roomSearch, roomRate, guestInformation, reservationNumber, sourceCode, partnerID, isSessionBooking);

                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);

                response = service.CreateBooking(request);

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();
                string currentSessionId = string.Empty;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    currentSessionId = HttpContext.Current.Session.SessionID;
                }
                string bookingType = string.Empty;
                if (hotelSearch.SearchingType != SearchType.REDEMPTION)
                    bookingType = "Create Booking";
                else
                    bookingType = "Create Booking - Redemption";
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = currentSessionId + ", " + request.GetType().Name + ", Sync, " + bookingType + ", " + reqStartTime + ", " + responseEndTime + ", " +
                    timer.Elapsed.ToString() + ", " + System.Environment.MachineName + ", " + requestSource;

                AppLogger.CSVInfoLogger(strLog);

                AppLogger.LogInfoMessage("Response received: service.CreateBooking(request)");

                if (response.Result.resultStatusFlag == ServiceProxies.Reservation.ResultStatusFlag.SUCCESS)
                {
                    if (hotelSearch.SearchingType != SearchType.REDEMPTION)
                        LogRateMismatchInCreatebooking(request, response);

                    AppLogger.LogInfoMessage("ReservationDomain-MakeReservation resultStatusFlag: success");

                    UniqueID[] uniqueIDList = response.HotelReservation.UniqueIDList;
                    string legNumber = string.Empty;
                    string confirmationNumber = string.Empty;
                    for (int i = 0; i < uniqueIDList.Length; i++)
                    {
                        UniqueID uniqueID = uniqueIDList[i];
                        if ((uniqueID.source != AppConstants.ID_SOURCE_RESERVATION) && (string.IsNullOrEmpty(confirmationNumber)))
                        {
                            confirmationNumber = uniqueID.Value;
                            createNameID = response.HotelReservation.ResGuests[0].Profiles[0].ProfileIDs[0].Value;
                        }
                        if (uniqueID.source == AppConstants.ID_LEGNUMBER)
                        {
                            legNumber = uniqueID.Value;
                        }
                    }

                    if (string.IsNullOrEmpty(legNumber) && (string.IsNullOrEmpty(reservationNumber)))
                    {
                        legConfirmationMap.Add(AppConstants.ONE, confirmationNumber);
                    }
                    else
                    {
                        legConfirmationMap.Add(legNumber, confirmationNumber);
                    }
                    Profile[] profileList = response.HotelReservation.ResGuests[0].Profiles;
                    for (int profileCount = 0; profileCount < profileList.Length; profileCount++)
                    {
                        ServiceProxies.Reservation.Company company = profileList[profileCount].Item as Company;
                        if (company != null)
                        {
                            guestInformation.IsValidDNumber = true;
                            break;
                        }
                    }

                    #region Gets Cancel by Date

                    RoomStay[] roomStayList = response.HotelReservation.RoomStays;
                    RoomStay roomStay = null;
                    if ((roomStayList != null) && (roomStayList.Length > 0))
                    {
                        roomStay = roomStayList[0];
                        RatePlan[] ratePlanList = roomStay.RatePlans;
                        if ((ratePlanList != null) && (ratePlanList.Length > 0))
                        {
                            AdditionalDetail[] additionalDetailsList = ratePlanList[0].AdditionalDetails;
                            if ((additionalDetailsList != null) && (additionalDetailsList.Length > 0))
                            {
                                AdditionalDetailType additionalDetailType = additionalDetailsList[0].detailType;
                                if (additionalDetailType == AdditionalDetailType.CancelPolicy)
                                {
                                    Paragraph additionalDetailParagraph =
                                        additionalDetailsList[0].AdditionalDetailDescription;
                                    Text cancelByItem = additionalDetailParagraph.Items[0] as Text;
                                    if (cancelByItem != null)
                                    {
                                        string cancelByDate = cancelByItem.Value;
                                        if (cancelByDate != string.Empty)
                                        {
                                            cancelByDate = cancelByDate.ToUpper();
                                            cancelByDate = cancelByDate.Replace(AppConstants.CANCEL_BY, "");
                                            try
                                            {
                                                DateTime cancelDate = Core.DateUtil.ConvertDDMMYYToDateTime(cancelByDate);
                                                hotelSearch.CancelByDate = cancelDate;
                                            }
                                            catch (Exception Ex)
                                            {
                                                AppLogger.LogFatalException(Ex, Ex.Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion Gets Cancel by Date
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<CreateBookingRequest, CreateBookingResponse>(request, response);
                    var strSearchType = hotelSearch.SearchingType.ToString();
                    var bkngCode = hotelSearch.CampaignCode != null ? hotelSearch.CampaignCode : string.Empty;

                    if (response != null && response.Result != null && !string.IsNullOrEmpty(response.Result.OperaErrorCode))
                    {
                        if (!string.Equals(response.Result.OperaErrorCode, OWSExceptionConstants.ROOM_UNAVAILABLE, StringComparison.InvariantCultureIgnoreCase) &&
                            !string.Equals(response.Result.OperaErrorCode, OWSExceptionConstants.INSUFFICIENT_AWARD_POINTS_FOR_RESERVATION, StringComparison.InvariantCultureIgnoreCase))
                        {
                            var serializedRequestObj = Serializer<CreateBookingRequest>(request);
                            ScanWebGenericException expnReq = new ScanWebGenericException(string.Format("Failed Booking Request: searchType = {0} bookingCode = {1} {2}", strSearchType, bkngCode, serializedRequestObj));
                            AppLogger.LogFatalException(expnReq, serializedRequestObj);

                            var serializedResponseObj = Serializer<CreateBookingResponse>(response);
                            ScanWebGenericException expnRes = new ScanWebGenericException(string.Format("Failed Booking Response: {0}", serializedResponseObj));
                            AppLogger.LogFatalException(expnRes, serializedResponseObj);
                        }
                    }
                    AppLogger.LogInfoMessage("ReservationDomain-MakeReservation resultStatusFlag: failed");
                    Hashtable exceptionData = new Hashtable();
                    exceptionData.Add("REQ:ReservationStatus", request.HotelReservation.reservationStatusSpecified);
                    exceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                    OWSUtility.RaiseReservationOWSException(response, exceptionData);
                }
            }
            catch (SoapException soapEx)
            {
                UserNavTracker.TrackOWSRequestResponse<CreateBookingRequest, CreateBookingResponse>(request, response);
                throw new OWSException(soapEx.Message, soapEx);
            }
            catch (Exception ex)
            {
                UserNavTracker.TrackOWSRequestResponse<CreateBookingRequest, CreateBookingResponse>(request, response);
                throw ex;
            }
            finally
            {
                request = null;
                response = null;
                timer = null;
            }
            return legConfirmationMap;
        }

        private void LogRateMismatchInCreatebooking(CreateBookingRequest request, CreateBookingResponse response)
        {
            try
            {
                double rateInRequest = request.HotelReservation.RoomStays[0].RoomRates[0].Total.Value;
                double rateInResponse = response.HotelReservation.RoomStays[0].Total.Value;
                if (!double.Equals(rateInRequest, rateInResponse))
                {
                    string messageToLog = string.Format("HotelId:{0} | ResvNumber:{1} | TotalRateInRequest:{2}| TotalRateInResponse:{3}",
                                                 response.HotelReservation.RoomStays[0].HotelReference.hotelCode, response.HotelReservation.UniqueIDList[0].Value, rateInRequest, rateInResponse);
                    AppLogger.LogReservationRatesInfoLogger(messageToLog);
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, string.Format("Error in LogRateMismatchInCreatebooking {0}", ex.Message));
            }
        }

        private string Serializer<T>(T objectToSerialize)
        {
            string serializedObject = string.Empty;
            try
            {
                XmlSerializer xmlSer = new XmlSerializer(objectToSerialize.GetType());
                StringWriter sWriter = new StringWriter();
                xmlSer.Serialize(sWriter, objectToSerialize);
                serializedObject = sWriter.ToString();
            }
            catch (Exception ex)
            {

            }

            return serializedObject;
        }

        /// <summary>
        /// Modify the specified Reservation
        /// </summary>
        /// <param name="hotelSearch">
        /// HotelSearch Entity
        /// </param>
        /// <param name="roomSearch"></param>
        /// <param name="roomRate">
        /// HotelRoomRateEntity
        /// </param>
        /// <param name="guestInformation">
        /// GuestInformationEntity
        /// </param>
        /// <param name="prevReservationNumber">
        /// Previous Reservation Number
        /// </param>
        /// <param name="sourceCode"></param>
        /// <param name="createdNameID">
        /// Created Named ID
        /// </param>
        /// <returns>
        /// New Reservation Number
        /// </returns>        
        public SortedList<string, string> ModifyReservation(HotelSearchEntity hotelSearch,
                                                            HotelSearchRoomEntity roomSearch,
                                                            HotelRoomRateEntity roomRate,
                                                            GuestInformationEntity guestInformation,
                                                            string prevReservationNumber, string sourceCode,
                                                            out string createdNameID)
        {
            SortedList<string, string> legConfirmationMap = new SortedList<string, string>();
            createdNameID = string.Empty;
            ModifyBookingRequest request = null;
            ModifyBookingResponse response = null;
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogInfoMessage("In ReservationDomain\\ModifyReservation()");
                ReservationService service = OWSUtility.GetReservationService();
                AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetModifyBookingRequest(hotelSearch, roomRate, guestInformation)");
                AppLogger.LogInfoMessage("Fetching response: service.ModifyBooking(request)");
                request = OWSRequestConverter.GetModifyBookingRequest(hotelSearch, roomSearch, roomRate, guestInformation, prevReservationNumber, sourceCode);

                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);

                response = service.ModifyBooking(request);
                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();
                string currentSessionId = string.Empty;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    currentSessionId = HttpContext.Current.Session.SessionID;
                }
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = currentSessionId + ", " + request.GetType().Name + ", Sync, ModifyReservation, " + reqStartTime + ", " + responseEndTime + ", " +
                    timer.Elapsed.ToString() + ", " + System.Environment.MachineName + ", " + requestSource;

                AppLogger.CSVInfoLogger(strLog);
                AppLogger.LogInfoMessage("Response received: service.ModifyBooking(request)");
                if (response.Result.resultStatusFlag != ServiceProxies.Reservation.ResultStatusFlag.FAIL)
                {
                    AppLogger.LogInfoMessage("ReservationDomain-ModifyReservation resultStatusFlag: success");
                    UniqueID[] uniqueIDList = response.HotelReservation.UniqueIDList;
                    string legNumber = string.Empty;
                    string confirmationNumber = string.Empty;
                    for (int i = 0; i < uniqueIDList.Length; i++)
                    {
                        UniqueID uniqueID = uniqueIDList[i];
                        if ((uniqueID.source != AppConstants.ID_SOURCE_RESERVATION) && (string.IsNullOrEmpty(confirmationNumber)))
                        {
                            confirmationNumber = uniqueID.Value;
                            createdNameID = response.HotelReservation.ResGuests[0].Profiles[0].ProfileIDs[0].Value;
                        }
                        if (uniqueID.source == AppConstants.ID_LEGNUMBER)
                        {
                            legNumber = uniqueID.Value;
                        }
                    }

                    if (string.IsNullOrEmpty(legNumber) && (string.IsNullOrEmpty(prevReservationNumber)))
                    {
                        legConfirmationMap.Add(AppConstants.ONE, confirmationNumber);
                    }
                    else
                    {
                        legConfirmationMap.Add(legNumber, confirmationNumber);
                    }
                    Profile[] profileList = response.HotelReservation.ResGuests[0].Profiles;
                    for (int profileCount = 0; profileCount < profileList.Length; profileCount++)
                    {
                        ServiceProxies.Reservation.Company company = profileList[profileCount].Item as Company;
                        if (company != null)
                        {
                            guestInformation.IsValidDNumber = true;
                            break;
                        }
                    }
                }
                else
                {
                    AppLogger.LogInfoMessage("ReservationDomain-ModifyReservation resultStatusFlag: failed");
                    UserNavTracker.TrackOWSRequestResponse<ModifyBookingRequest, ModifyBookingResponse>(request, response);
                    Hashtable exceptionData = new Hashtable();
                    exceptionData.Add("REQ:ReservationStatus", request.HotelReservation.reservationStatusSpecified);
                    exceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                    OWSUtility.RaiseModifyBookingConfirmOWSException(response, exceptionData);
                }
            }
            catch (SoapException soapEx)
            {
                UserNavTracker.TrackOWSRequestResponse<ModifyBookingRequest, ModifyBookingResponse>(request, response);
                throw new OWSException(soapEx.Message, soapEx);
            }
            finally
            {
                request = null;
                response = null;
                timer = null;
            }
            return legConfirmationMap;
        }

        /// <summary>
        /// Cancel the Booking
        /// </summary>
        /// <param name="cancelDetailsEntity">
        /// CancelDetails Entity
        /// </param>
        /// <param name="flag"></param>
        /// <returns>
        /// Cancellation Number
        /// </returns>
        public string CancelReservation(ref CancelDetailsEntity cancelDetailsEntity, bool flag)
        {
            CancelBookingRequest request = null;
            CancelBookingResponse response = null;
            ConfirmBookingRequest confirmRequest = null;
            ConfirmBookingResponse confirmResponse = null;
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            System.Diagnostics.Stopwatch timerConfBook = new System.Diagnostics.Stopwatch();
            string cancelNumber = string.Empty;
            try
            {
                AppLogger.LogInfoMessage("In ReservationDomain\\CancelReservation()");
                ReservationService service = OWSUtility.GetReservationService(true);
                AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetCancelBookingRequest(confirmationNumber)");
                request = OWSRequestConverter.GetCancelBookingRequest(cancelDetailsEntity);
                AppLogger.LogInfoMessage("Fetching response: service.CreateBooking(request)");

                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);

                response = service.CancelBooking(request);

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();
                string currentSessionId = string.Empty;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    currentSessionId = HttpContext.Current.Session.SessionID;
                }
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = currentSessionId + ", " + request.GetType().Name + ", Sync, Cancel Reservation, " + reqStartTime + ", " + responseEndTime + ", " +
                    timer.Elapsed.ToString() + ", " + System.Environment.MachineName + ", " + requestSource;

                AppLogger.CSVInfoLogger(strLog);
                AppLogger.LogInfoMessage("Response received: service.CreateBooking(request)");
                if (response.Result.resultStatusFlag != ServiceProxies.Reservation.ResultStatusFlag.FAIL)
                {
                    if (!flag)
                    {
                        ReservationService serviceObj = OWSUtility.GetReservationService(true);
                        ConfirmBookingEntity confirmBookingEntity = new ConfirmBookingEntity();
                        confirmBookingEntity.ReservationNumber = cancelDetailsEntity.ReservationNumber;
                        confirmBookingEntity.LegNumber = cancelDetailsEntity.LegNumber;
                        confirmBookingEntity.HotelCode = cancelDetailsEntity.HotelCode;
                        confirmBookingEntity.ChainCode = cancelDetailsEntity.ChainCode;
                        confirmRequest = OWSRequestConverter.GetConfirmBookingRequest(confirmBookingEntity);
                        timerConfBook.Start();
                        string reqConfBookStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                        confirmResponse = serviceObj.ConfirmBooking(confirmRequest);
                        timerConfBook.Stop();
                        string responseConfBookEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                        //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                        string strConfBookLog = currentSessionId + ", " + confirmRequest.GetType().Name + ", Sync, Cancel Reservation - Conf Booking, " + reqConfBookStartTime + ", " + responseConfBookEndTime + ", " +
                            timerConfBook.Elapsed.ToString() + ", " + System.Environment.MachineName + ", " + requestSource;
                        AppLogger.CSVInfoLogger(strConfBookLog);

                        if (confirmResponse.Result.resultStatusFlag != ResultStatusFlag.FAIL)
                        {
                            IDPair[] idPairs = response.Result.IDs;
                            if ((idPairs != null) && (idPairs.Length > 0))
                            {
                                cancelNumber = idPairs[0].operaId.ToString();
                                cancelDetailsEntity.CancelationStatus = true;
                                cancelDetailsEntity.CancelNumber = cancelNumber;
                            }
                        }
                        else
                        {
                            UserNavTracker.TrackOWSRequestResponse<ConfirmBookingRequest, ConfirmBookingResponse>(confirmRequest, confirmResponse);
                            AppLogger.LogInfoMessage("ReservationDomain-CancelReservation-ConfirmCancel resultStatusFlag: failed");
                            Hashtable exceptionData = new Hashtable();
                            exceptionData.Add("REQ:ConfirmationNumber", confirmResponse.ConfirmationNumber);
                            exceptionData.Add("RES:ResultStatusFlag", confirmResponse.Result.resultStatusFlag.ToString());
                            OWSUtility.RaiseConfirmOWSException(confirmResponse, exceptionData);
                        }
                    }
                    else
                    {
                        IDPair[] idPairs = response.Result.IDs;
                        if ((idPairs != null) && (idPairs.Length > 0))
                        {
                            cancelNumber = idPairs[0].operaId.ToString();
                            cancelDetailsEntity.CancelationStatus = true;
                            cancelDetailsEntity.CancelNumber = cancelNumber;
                        }
                    }
                }

                else
                {
                    UserNavTracker.TrackOWSRequestResponse<CancelBookingRequest, CancelBookingResponse>(request, response);
                    AppLogger.LogInfoMessage("ReservationDomain-CancelReservation resultStatusFlag: failed");
                    Hashtable exceptionData = new Hashtable();
                    exceptionData.Add("REQ:ReservationStatus", request.HotelReference.hotelCode);
                    exceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                    OWSUtility.RaiseCancelOWSException(response, exceptionData);
                }
            }

            catch (SoapException soapEx)
            {
                UserNavTracker.TrackOWSRequestResponse<CancelBookingRequest, CancelBookingResponse>(request, response);
                UserNavTracker.TrackOWSRequestResponse<ConfirmBookingRequest, ConfirmBookingResponse>(confirmRequest, confirmResponse);
                throw new OWSException(soapEx.Message, soapEx);
            }
            finally
            {
                request = null;
                response = null;
                confirmRequest = null;
                confirmResponse = null;
                timer = null;
                timerConfBook = null;
            }
            return cancelNumber;
        }

        /// <summary>
        /// Fetch Reservation for the specified Reservation Number        
        /// </summary>
        /// <param name="reservationNumber">
        /// Reservation Number
        /// </param>
        /// <param name="legNumber"></param>
        /// <returns>BookingDetailsEntity</returns>
        public BookingDetailsEntity FetchReservation(string reservationNumber, string legNumber)
        {
            BookingDetailsEntity bookingDetails = null;
            FetchBookingRequest request = null;
            FetchBookingResponse response = null;
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogInfoMessage("In ReservationDomain\\FetchReservation()");
                ReservationService service = OWSUtility.GetReservationService();
                AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetFetchBookingRequest(reservationNumber)");
                AppLogger.LogInfoMessage("Fetching response: service.FetchBooking(request)");

                request = OWSRequestConverter.GetFetchLegBooking(reservationNumber, legNumber);

                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);

                response = service.FetchBooking(request);

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();
                string currentSessionId = string.Empty;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    currentSessionId = HttpContext.Current.Session.SessionID;
                }

                //SessionID-RequestName-RequestType-SearchType-StartTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = currentSessionId + ", " + request.GetType().Name + ", Sync, Fetch Reservation, " + reqStartTime + ", " + responseEndTime + ", " +
                    timer.Elapsed.ToString() + ", " + System.Environment.MachineName + ", " + requestSource;

                AppLogger.CSVInfoLogger(strLog);
                AppLogger.LogInfoMessage("Response received: service.FetchBooking(request)");
                if (response.Result.resultStatusFlag != ServiceProxies.Reservation.ResultStatusFlag.FAIL)
                {
                    bookingDetails = CreateFetchReservationBusinessEntities(response.HotelReservation);
                }
                else
                {
                    AppLogger.LogInfoMessage("ReservationDomain-FetchReservation resultStatusFlag: failed");
                    UserNavTracker.TrackOWSRequestResponse<FetchBookingRequest, FetchBookingResponse>(request, response);
                    Hashtable exceptionData = new Hashtable();
                    exceptionData.Add("REQ:ReservationStatus", request.ConfirmationNumber);
                    exceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                    OWSUtility.RaiseFetchReservationOWSException(response, exceptionData);
                }
            }
            catch (SoapException soapEx)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchBookingRequest, FetchBookingResponse>(request, response);
                throw new OWSException(soapEx.Message, soapEx);
            }
            finally
            {
                request = null;
                response = null;
                timer = null;
            }
            return bookingDetails;
        }


        /// <summary>
        /// Call Future Booking Summary Web service Method.
        /// </summary>
        /// <param name="nameId">Name Id</param>
        /// <returns>FutureBookingSummaryResponse</returns>
        public FutureBookingSummaryResponse GetCurrentBookingList(string nameId)
        {
            FutureBookingSummaryRequest request = null;
            FutureBookingSummaryResponse response = null;
            try
            {
                ReservationService service = OWSUtility.GetReservationService();
                request = OWSRequestConverter.GetFutureBookingSummaryRequest(nameId);
                response = service.FutureBookingSummary(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Reservation.ResultStatusFlag.FAIL)
                {
                    return response;
                }
                else
                {
                    AppLogger.LogInfoMessage("CurrentBookingList resultStatusFlag: failed");
                    UserNavTracker.TrackOWSRequestResponse<FutureBookingSummaryRequest, FutureBookingSummaryResponse>(request, response);
                    Hashtable exceptionData = new Hashtable();
                    exceptionData.Add("REQ:ReservationStatus", request.Item);
                    exceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                    OWSUtility.RaiseCurrentBookingListOWSException(response, exceptionData);
                }
            }
            catch (SoapException soapEx)
            {
                UserNavTracker.TrackOWSRequestResponse<FutureBookingSummaryRequest, FutureBookingSummaryResponse>(request, response);
                OWSUtility.RaiseOWSException(soapEx.Message, soapEx);
            }
            finally
            {
                request = null;
            }
            return null;
        }

        /// <summary>
        /// This method will send the summary for the booking.
        /// Basically this method will get all the bookings.
        /// </summary>
        /// <param name="bookingNumber">
        /// Booking number for which the summary will be needed.
        /// </param>
        /// <returns>Fetch booking summary response.</returns>
        public SortedList<string, string> FetchBookingSummary(string bookingNumber)
        {
            SortedList<string, string> bookingNumberList = null;
            FetchSummaryRequest request = null;
            FetchSummaryResponse response = null;
            try
            {
                ReservationService service = OWSUtility.GetReservationService();
                request = OWSRequestConverter.GetFetchBookingSummary(bookingNumber);
                response = service.FetchSummary(request);
                if (response.HotelReservations != null)
                {
                    bookingNumberList = GetBookingList(response.HotelReservations);
                }
                else if (response.Result.resultStatusFlag == ServiceProxies.Reservation.ResultStatusFlag.FAIL)
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchSummaryRequest, FetchSummaryResponse>(request, response);
                    AppLogger.LogInfoMessage("ReservationDomain-FetchBookingSummary resultStatusFlag: failed");
                    Hashtable exceptionData = new Hashtable();
                    exceptionData.Add("REQ:ReservationStatus", request.ConfirmationNumber);
                    exceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                    OWSUtility.RaiseFetchSummaryOWSException(response, exceptionData);
                }
            }
            catch (SoapException soapEx)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchSummaryRequest, FetchSummaryResponse>(request, response);
                OWSUtility.RaiseOWSException(soapEx.Message, soapEx);
            }
            finally
            {
                request = null;
                response = null;
            }
            return bookingNumberList;
        }


        /// <summary>
        /// FetchReservationPackages
        /// </summary>
        /// <param name="reservationNumber"></param>
        /// <param name="legNumber"></param>
        /// <param name="hotelCode"></param>
        /// <returns>List of BedAccommodation</returns>
        public List<BedAccommodation> FetchReservationPackages(string reservationNumber, string legNumber,
                                                               string hotelCode)
        {
            AppLogger.LogInfoMessage("OWSUtility.GetReservationService()");
            ReservationService service = OWSUtility.GetReservationService();
            AppLogger.LogInfoMessage(
                "Creating request object: OWSRequestConverter.GetFetchBookedPackagesRequest(reservationNumber, legNumber, hotelCode");

            FetchBookedPackagesRequest request = null;
            FetchBookedPackagesResponse response = null;
            List<BedAccommodation> listBedAccommodation = new List<BedAccommodation>();

            try
            {
                request = OWSRequestConverter.GetFetchBookedPackagesRequest(reservationNumber,
                                                                                                       legNumber, hotelCode);

                AppLogger.LogInfoMessage("Fetching response: service.FetchBookedPackages(request)");
                response = service.FetchBookedPackages(request);
                AppLogger.LogInfoMessage("Response received: service.FetchBookedPackages(request)");

                if (response.Result.resultStatusFlag != ServiceProxies.Reservation.ResultStatusFlag.FAIL)
                {
                    foreach (PackageDetail packDetails in response.BookedPackageList)
                    {
                        PackageCharge[] listPackageChanges = packDetails.ExpectedCharges;
                        if (listPackageChanges != null && listPackageChanges.Length > 0)
                        {
                            try
                            {
                                if (listPackageChanges[0].RateCode == null && listPackageChanges[0].PackageCode != null &&
                                    (listPackageChanges[0].PackageCode == ChildAccommodationType.CRIB.ToString() ||
                                     listPackageChanges[0].PackageCode == ChildAccommodationType.CIPB.ToString() ||
                                     listPackageChanges[0].PackageCode == ChildAccommodationType.XBED.ToString()))
                                {
                                    BedAccommodation bedAccd = new BedAccommodation();
                                    bedAccd.BedType =
                                        (ChildAccommodationType)
                                        Enum.Parse(typeof(ChildAccommodationType), listPackageChanges[0].PackageCode,
                                                   true);
                                    bedAccd.Quantity = listPackageChanges[0].Quantity;
                                    listBedAccommodation.Add(bedAccd);
                                }
                            }
                            catch (Exception ex)
                            {
                                UserNavTracker.TrackOWSRequestResponse<FetchBookedPackagesRequest, FetchBookedPackagesResponse>(request, response);
                                AppLogger.LogInfoMessage(ex.InnerException.ToString());
                            }
                        }
                    }
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchBookedPackagesRequest, FetchBookedPackagesResponse>(request, response);
                }
            }
            catch (Exception ex)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchBookedPackagesRequest, FetchBookedPackagesResponse>(request, response);
            }
            finally
            {
                request = null;
                response = null;
            }
            return listBedAccommodation;
        }

        public string IgnoreBooking(IgnoreBookingEntity ignoreDetailsEntity)
        {
            string ignoreBookingNumber = string.Empty;
            IgnoreBookingRequest request = null;
            IgnoreBookingResponse response = null;
            try
            {
                AppLogger.LogInfoMessage("In ReservationDomain\\IgnoreBooking()");
                ReservationService service = OWSUtility.GetReservationService(true);
                AppLogger.LogInfoMessage(
                    "Creating request object: OWSRequestConverter.GetIgnoreBookingRequest(confirmationNumber)");
                request = OWSRequestConverter.GetIgnoreBookingRequest(ignoreDetailsEntity);
                AppLogger.LogInfoMessage("Fetching response: service.IgnoreBooking(request)");
                response = service.IgnoreBooking(request);
                AppLogger.LogInfoMessage("Response received: service.IgnoreBooking(request)");

                if (response.Result.resultStatusFlag != ServiceProxies.Reservation.ResultStatusFlag.FAIL)
                {
                    IDPair[] idPairs = response.Result.IDs;
                    if ((idPairs != null) && (idPairs.Length > 0))
                    {
                        ignoreBookingNumber = idPairs[0].operaId.ToString();
                    }
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<IgnoreBookingRequest, IgnoreBookingResponse>(request, response);
                    AppLogger.LogInfoMessage("ReservationDomain-IgnoreBooking resultStatusFlag: failed");
                    Hashtable exceptionData = new Hashtable();
                    exceptionData.Add("REQ:ReservationStatus", request.HotelReference.hotelCode);
                    exceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                    OWSUtility.RaiseIgnoreOWSException(response, exceptionData);
                }
            }
            catch (SoapException soapEx)
            {
                UserNavTracker.TrackOWSRequestResponse<IgnoreBookingRequest, IgnoreBookingResponse>(request, response);
                throw new OWSException(soapEx.Message, soapEx);
            }
            finally
            {
                request = null;
                response = null;
            }
            return ignoreBookingNumber;
        }

        public string ConfirmBooking(ConfirmBookingEntity confirmBookingEntity)
        {
            string confirmBookingNumber = string.Empty;
            ConfirmBookingRequest request = null;
            ConfirmBookingResponse response = null;
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogInfoMessage("In ReservationDomain\\ConfirmBooking()");
                ReservationService service = OWSUtility.GetReservationService(true);
                AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetConfirmBookingRequest(confirmationNumber)");
                AppLogger.LogInfoMessage("Fetching response: service.ConfirmBooking(request)");

                request = OWSRequestConverter.GetConfirmBookingRequest(confirmBookingEntity);

                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);

                response = service.ConfirmBooking(request);
                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();
                string currentSessionId = string.Empty;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    currentSessionId = HttpContext.Current.Session.SessionID;
                }
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = currentSessionId + ", " + request.GetType().Name + ", Sync, Confirm Booking, " + reqStartTime + ", " + responseEndTime + ", " +
                    timer.Elapsed.ToString() + ", " + System.Environment.MachineName + ", " + requestSource;

                AppLogger.CSVInfoLogger(strLog);
                AppLogger.LogInfoMessage("Response received: service.IgnoreBooking(request)");

                if (response.Result.resultStatusFlag == ServiceProxies.Reservation.ResultStatusFlag.SUCCESS)
                {
                    IDPair[] idPairs = response.Result.IDs;
                    if ((idPairs != null) && (idPairs.Length > 0))
                    {
                        confirmBookingNumber = idPairs[0].operaId.ToString();
                    }
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<ConfirmBookingRequest, ConfirmBookingResponse>(request, response);
                    AppLogger.LogInfoMessage("ReservationDomain-IgnoreBooking resultStatusFlag: failed");
                    Hashtable exceptionData = new Hashtable();
                    exceptionData.Add("REQ:ReservationStatus", request.HotelReference.hotelCode);
                    exceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                    OWSUtility.RaiseConfirmOWSException(response, exceptionData);
                }
            }
            catch (SoapException soapEx)
            {
                UserNavTracker.TrackOWSRequestResponse<ConfirmBookingRequest, ConfirmBookingResponse>(request, response);
                throw new OWSException(soapEx.Message, soapEx);
            }
            finally
            {
                request = null;
                response = null;
                timer = null;
            }
            return confirmBookingNumber;
        }

        public SortedList<string, string> ModifyGuaranteeReservation(HotelSearchEntity hotelSearch,
                                                     HotelSearchRoomEntity roomSearch,
                                                     HotelRoomRateEntity roomRate,
                                                     GuestInformationEntity guestInformation,
                                                     string prevReservationNumber, string sourceCode,
                                                     out string createdNameID)
        {
            SortedList<string, string> legConfirmationMap = new SortedList<string, string>();
            createdNameID = string.Empty;
            ModifyBookingRequest request = null;
            ModifyBookingResponse response = null;
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AppLogger.LogInfoMessage("In ReservationDomain\\ModifyGuaranteeReservation()");
                ReservationService service = OWSUtility.GetPrepaidReservationService();
                AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetModifyBookingRequest(hotelSearch, roomRate, guestInformation)");

                request = OWSRequestConverter.GetModifyGuaranteeBookingRequest(hotelSearch, roomSearch,
                                                                                           roomRate, guestInformation,
                                                                                           prevReservationNumber, sourceCode);
                AppLogger.LogInfoMessage("Fetching response: service.ModifyBooking(request)");

                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                response = service.ModifyBooking(request);
                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();
                string currentSessionId = string.Empty;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    currentSessionId = HttpContext.Current.Session.SessionID;
                }
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = currentSessionId + ", " + request.GetType().Name + ", Sync, ModifyGuaranteeReservation, " + reqStartTime + ", " + responseEndTime + ", " +
                    timer.Elapsed.ToString() + ", " + System.Environment.MachineName + ", " + requestSource;

                AppLogger.CSVInfoLogger(strLog);
                AppLogger.LogInfoMessage("Response received: service.ModifyBooking(request)");
                if (response.Result.resultStatusFlag != ServiceProxies.Reservation.ResultStatusFlag.FAIL)
                {
                    AppLogger.LogInfoMessage("ReservationDomain-ModifyReservation resultStatusFlag: success");

                    UniqueID[] uniqueIDList = response.HotelReservation.UniqueIDList;
                    string legNumber = string.Empty;
                    string confirmationNumber = string.Empty;
                    for (int i = 0; i < uniqueIDList.Length; i++)
                    {
                        UniqueID uniqueID = uniqueIDList[i];
                        if ((uniqueID.source != AppConstants.ID_SOURCE_RESERVATION) &&
                            (string.IsNullOrEmpty(confirmationNumber)))
                        {
                            confirmationNumber = uniqueID.Value;
                            createdNameID =
                                response.HotelReservation.ResGuests[0].Profiles[0].ProfileIDs[0].Value;

                        }
                        if (uniqueID.source == AppConstants.ID_LEGNUMBER)
                        {
                            legNumber = uniqueID.Value;
                        }
                    }

                    if (string.IsNullOrEmpty(legNumber) && (string.IsNullOrEmpty(prevReservationNumber)))
                    {
                        legConfirmationMap.Add(AppConstants.ONE, confirmationNumber);
                    }
                    else
                    {
                        legConfirmationMap.Add(legNumber, confirmationNumber);
                    }
                    Profile[] profileList = response.HotelReservation.ResGuests[0].Profiles;
                    for (int profileCount = 0; profileCount < profileList.Length; profileCount++)
                    {
                        ServiceProxies.Reservation.Company company = profileList[profileCount].Item as Company;
                        if (company != null)
                        {
                            guestInformation.IsValidDNumber = true;
                            break;
                        }
                    }
                }
                else
                {
                    AppLogger.LogInfoMessage("ReservationDomain-ModifyReservation resultStatusFlag: failed");

                    Hashtable exceptionData = new Hashtable();
                    exceptionData.Add("REQ:ReservationStatus", request.HotelReservation.reservationStatusSpecified);
                    exceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                    OWSUtility.RaiseModifyBookingConfirmOWSException(response, exceptionData);
                }
            }
            catch (SoapException soapEx)
            {
                throw new OWSException(soapEx.Message, soapEx);
            }
            finally
            {
                request = null;
                response = null;
                timer = null;
            }
            return legConfirmationMap;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Get the List of Bookings
        /// </summary>
        /// <param name="hotelReservations">
        /// HotelReservations
        /// </param>
        /// <returns>
        /// Sorted List of Leg-Reservation
        /// </returns>
        private SortedList<string, string> GetBookingList(HotelReservation[] hotelReservations)
        {
            SortedList<string, string> bookingNumberList = null;
            if (hotelReservations != null && hotelReservations.Length > 0)
            {
                int totalReservations = hotelReservations.Length;
                string reservationNumber;
                string legNumber;
                bookingNumberList = new SortedList<string, string>();
                for (int reservationCount = 0; reservationCount < totalReservations; reservationCount++)
                {
                    reservationNumber = string.Empty;
                    legNumber = FetchReservationNumber(hotelReservations[reservationCount], out reservationNumber);
                    bookingNumberList[legNumber] = reservationNumber;
                }
            }
            return bookingNumberList;
        }

        /// <summary>
        /// Create the Business Entities for Fetch Reservation
        /// </summary>
        /// <param name="hotelReservation">
        /// FetchBookingResponse
        /// </param>
        /// <returns>BookingDetailsEntity</returns>
        private BookingDetailsEntity CreateFetchReservationBusinessEntities(HotelReservation hotelReservation)
        {
            HotelSearchEntity hotelSearchEntity = new HotelSearchEntity();
            HotelRoomRateEntity hotelRoomRateEntity = new HotelRoomRateEntity();
            GuestInformationEntity guestInformationEntity = new GuestInformationEntity();

            BookingDetailsEntity bookingDetails = null;
            if (hotelReservation != null)
            {
                RoomStay[] roomStayList = hotelReservation.RoomStays;
                RoomStay roomStay = null;
                if (roomStayList != null && roomStayList.Length > 0)
                {
                    roomStay = roomStayList[0];
                    RatePlan[] ratePlanList = roomStay.RatePlans;
                    if (ratePlanList != null && ratePlanList.Length > 0)
                    {
                        hotelRoomRateEntity.RatePlanCode = ratePlanList[0].ratePlanCode;
                        hotelSearchEntity.CampaignCode = ratePlanList[0].promotionCode;
                        AdditionalDetail[] additionalDetailsList = ratePlanList[0].AdditionalDetails;
                        if (additionalDetailsList != null && additionalDetailsList.Length > 0)
                        {
                            AdditionalDetailType additionalDetailType = additionalDetailsList[0].detailType;
                            if (additionalDetailType == AdditionalDetailType.CancelPolicy)
                            {
                                Paragraph additionalDetailParagraph =
                                    additionalDetailsList[0].AdditionalDetailDescription;
                                Text cancelByItem = additionalDetailParagraph.Items[0] as Text;
                                if (cancelByItem != null)
                                {
                                    //string cancelByDate = cancelByItem.Value;
                                    //string guaranteeType = roomStay.Guarantee.guaranteeType;

                                    string[] nonModifiableGuaranteeTypes = AppConstants.NON_MODIFIABLE_GURANTEE_TYPES;
                                    if ((nonModifiableGuaranteeTypes != null) && (!nonModifiableGuaranteeTypes.Contains(roomStay.Guarantee.guaranteeType)))
                                    {
                                        if ((!string.IsNullOrEmpty(cancelByItem.Value)))
                                        {
                                            try
                                            {
                                                //hotelSearchEntity.CancelByDate = Core.DateUtil.ConvertDDMMYYToDateTime(cancelByItem.Value.ToUpper().Replace(AppConstants.CANCEL_BY, ""));
                                                hotelSearchEntity.CancelByDate = Core.DateUtil.ConvertDDMMYYToDateTime(Convert.ToString(Regex.Replace(cancelByItem.Value.ToUpper(), @"^[a-zA-Z\s_-]*", "")));
                                            }

                                            catch (Exception Ex)
                                            {
                                                AppLogger.LogFatalException(Ex, string.Format("{0} for the date - {1}", Ex.Message, cancelByItem.Value));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        hotelSearchEntity.CancelPolicy = AppConstants.NON_CANCELLABLE_RESERVATION;
                                    }
                                }
                            }
                        }
                    }
                }

                List<BedAccommodation> listBedAccommodation = new List<BedAccommodation>();
                if (roomStayList != null && roomStayList.Length > 0)
                {
                    PackageElement[] packages = roomStayList[0].Packages;
                    if (packages != null && packages.Length > 0)
                    {
                        foreach (PackageElement packDetails in packages)
                        {
                            //if (packDetails != null &&
                            //    packDetails.source.Equals(packageCodeSource.RESERVATION.ToString()))
                            if (packDetails != null && (string.Equals(packDetails.packageCode, ChildAccommodationType.CIPB.ToString(), StringComparison.InvariantCultureIgnoreCase) || string.Equals(packDetails.packageCode, ChildAccommodationType.CRIB.ToString(),
                                StringComparison.InvariantCultureIgnoreCase) || string.Equals(packDetails.packageCode, ChildAccommodationType.XBED.ToString(), StringComparison.InvariantCultureIgnoreCase)) &&
                                string.Equals(packDetails.source, packageCodeSource.RESERVATION.ToString(), StringComparison.InvariantCultureIgnoreCase))
                            {
                                BedAccommodation bedAccd = new BedAccommodation();
                                bedAccd.BedType =
                                    (ChildAccommodationType)
                                    Enum.Parse(typeof(ChildAccommodationType), packDetails.packageCode, true);
                                bedAccd.Quantity = packDetails.quantity;
                                listBedAccommodation.Add(bedAccd);
                            }
                        }
                    }
                }
                ServiceProxies.Reservation.RoomType[] roomTypeList = roomStay.RoomTypes;
                if (roomTypeList != null && roomTypeList.Length > 0)
                {
                    hotelRoomRateEntity.RoomtypeCode = roomTypeList[0].roomTypeCode;
                    hotelSearchEntity.RoomsPerNight = roomTypeList[0].numberOfUnits;
                    guestInformationEntity.BedTypePreferenceCode = roomTypeList[0].roomTypeCode;
                }
                RoomRate[] roomRateList = roomStayList[0].RoomRates;
                if (roomRateList != null && roomRateList.Length > 0)
                {
                    if (roomStayList[0].MemberAwardInfo != null)
                    {
                        MemberAwardInfo memberAwardInfo = roomStay.MemberAwardInfo;
                        if (memberAwardInfo != null)
                        {
                            hotelRoomRateEntity.AwardType = memberAwardInfo.awardType;
                            hotelRoomRateEntity.Points = memberAwardInfo.pointsUsedForReservation;
                            guestInformationEntity.MembershipID = memberAwardInfo.membershipID;
                        }
                    }
                    else
                    {
                        ServiceProxies.Reservation.Rate[] rateList = roomRateList[0].Rates;
                        if (rateList != null && rateList.Length > 0)
                        {
                            RateEntity baseRateEntity = null;
                            if (hotelRoomRateEntity.Rate == null)
                            {
                                baseRateEntity = new RateEntity(rateList[0].Base.Value,
                                                                rateList[0].Base.currencyCode);
                                hotelRoomRateEntity.Rate = baseRateEntity;
                            }
                        }
                        Amount totalRate = roomStay.Total;
                        if (totalRate != null)
                        {
                            RateEntity totalRateEntity = new RateEntity(totalRate.Value, totalRate.currencyCode);
                            hotelRoomRateEntity.TotalRate = totalRateEntity;
                        }

                        if (roomStay.CreditCardDeposit != null && roomStay.CreditCardDeposit.Length > 0)
                        {
                            Amount depositAmount = roomStay.CreditCardDeposit[0].DepositAmount;
                            if (depositAmount != null)
                            {
                                RateEntity depositAmtRateEntity = new RateEntity(depositAmount.Value, depositAmount.currencyCode);
                                hotelRoomRateEntity.DepositAmount = depositAmtRateEntity;
                            }
                        }

                        Amount currentBalance = roomStay.CurrentBalance;
                        if (currentBalance != null)
                        {
                            RateEntity currentBalRateEntity = new RateEntity(currentBalance.Value, currentBalance.currencyCode);
                            hotelRoomRateEntity.CurrentBalance = currentBalRateEntity;
                        }

                        if (roomStay.ExpectedCharges != null)
                            if (roomStay.ExpectedCharges.ChargesForPostingDate != null)
                                if (roomStay.ExpectedCharges.ChargesForPostingDate.Length > 0)
                                {
                                    double subTotal = roomStay.ExpectedCharges.TotalRoomRateAndPackages;
                                    hotelRoomRateEntity.Taxes = new TaxesEntity();
                                    hotelRoomRateEntity.Taxes.Charges = new List<ChargeEntity>();

                                    for (int i = 0; i < roomStay.ExpectedCharges.ChargesForPostingDate.Length; i++)
                                    {
                                        ChargesForTheDay dayCharges = roomStay.ExpectedCharges.ChargesForPostingDate[i];

                                        //if (dayCharges.RoomRateAndPackages != null)
                                        //{
                                        //    subTotal += dayCharges.RoomRateAndPackages.TotalCharges;
                                        //}

                                        //adding all the room VAT details with no of nights details
                                        if (dayCharges.TaxesAndFees != null)
                                        {
                                            if (dayCharges.TaxesAndFees.Charges != null)
                                            {
                                                if (dayCharges.TaxesAndFees.Charges.Length > 0)
                                                {
                                                    foreach (Charge charge in dayCharges.TaxesAndFees.Charges)
                                                    {
                                                        ChargeEntity chargeEntity = new ChargeEntity();
                                                        chargeEntity.Description = charge.Description;

                                                        if (charge.Amount != null)
                                                            chargeEntity.Amount = new RateEntity(charge.Amount.Value, charge.Amount.currencyCode);

                                                        hotelRoomRateEntity.Taxes.Charges.Add(chargeEntity);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (totalRate != null)
                                        hotelRoomRateEntity.SubTotal = new RateEntity(subTotal, totalRate.currencyCode);
                                    else
                                        hotelRoomRateEntity.SubTotal = new RateEntity(subTotal, string.Empty);
                                }
                    }
                }
                GuestCountList guestCountList = roomStay.GuestCounts;
                if ((guestCountList != null))
                {
                    GuestCount[] guests = guestCountList.GuestCount;
                    if (guests != null)
                    {
                        List<HotelSearchRoomEntity> roomEntityList = new List<HotelSearchRoomEntity>();
                        HotelSearchRoomEntity roomEntity = new HotelSearchRoomEntity();
                        foreach (GuestCount guestCount in guests)
                        {
                            if (guestCount.ageQualifyingCode == AgeQualifyingCode.ADULT)
                            {
                                hotelSearchEntity.AdultsPerRoom = guestCount.count;
                                roomEntity.AdultsPerRoom = guestCount.count;
                            }
                            else if (guestCount.ageQualifyingCode == AgeQualifyingCode.CHILD)
                            {
                                hotelSearchEntity.ChildrenPerRoom = guestCount.count;
                                hotelSearchEntity.ChildrenOccupancyPerRoom = guestCount.count; // Scanweb 1.6 - ADD
                                roomEntity.ChildrenPerRoom = guestCount.count;
                                roomEntity.ChildrenOccupancyPerRoom = guestCount.count;
                            }
                        }
                        roomEntityList.Add(roomEntity);
                        hotelSearchEntity.ListRooms = roomEntityList;
                    }
                }
                if (roomStay.TimeSpan != null)
                {
                    hotelSearchEntity.ArrivalDate = roomStay.TimeSpan.StartDate;
                    hotelSearchEntity.DepartureDate = (DateTime)roomStay.TimeSpan.Item;
                    if ((hotelSearchEntity.DepartureDate != null) && (hotelSearchEntity.ArrivalDate != null))
                    {
                        System.TimeSpan dateDifference =
                            hotelSearchEntity.DepartureDate.Subtract(hotelSearchEntity.ArrivalDate);
                        if (dateDifference != null)
                        {
                            hotelSearchEntity.NoOfNights = dateDifference.Days;
                        }
                    }
                }
                Guarantee guarantee = roomStay.Guarantee;
                string[] prepaidGuranteeTypes = AppConstants.GUARANTEE_TYPE_PRE_PAID;
                if (guarantee != null)
                {
                    guestInformationEntity.GuranteeInformation = new GuranteeInformationEntity();
                    if (guarantee.GuaranteesAccepted != null)
                    {
                        GuaranteeAccepted[] guaranteeAcception = guarantee.GuaranteesAccepted;
                        if ((guaranteeAcception != null) && (guaranteeAcception.Length > 0))
                        {
                            CreditCard creditCard = guaranteeAcception[0].GuaranteeCreditCard;
                            if (creditCard != null)
                            {
                                CreditCardEntity creditCardEntity = null;
                                ExpiryDateEntity expiryDateEntity = null;
                                DateTime creditCardExpiryDate = creditCard.expirationDate;
                                if (creditCardExpiryDate != null)
                                {
                                    expiryDateEntity =
                                        new ExpiryDateEntity(creditCardExpiryDate.Month,
                                                             creditCardExpiryDate.Year);
                                    if (expiryDateEntity != null)
                                    {
                                        creditCardEntity = new CreditCardEntity(creditCard.cardHolderName,
                                                                                creditCard.cardType,
                                                                                creditCard.cardNumber, expiryDateEntity);
                                    }
                                }
                                guestInformationEntity.GuranteeInformation.CreditCard = creditCardEntity;
                                if ((prepaidGuranteeTypes != null) && (prepaidGuranteeTypes.Contains(guarantee.guaranteeType)))
                                {
                                    guestInformationEntity.GuranteeInformation.GuranteeType = GuranteeType.PREPAID;
                                }
                                else
                                {
                                    guestInformationEntity.GuranteeInformation.GuranteeType = GuranteeType.CREDITCARD;
                                }
                            }
                        }
                    }
                    else if ((prepaidGuranteeTypes != null) && (prepaidGuranteeTypes.Contains(guarantee.guaranteeType)))
                    {
                        guestInformationEntity.GuranteeInformation.GuranteeType = GuranteeType.PREPAID;
                    }
                    else
                    {
                        guestInformationEntity.GuranteeInformation.GuranteeType = GuranteeType.HOLD;
                    }
                    
                    if(string.Equals(guarantee.guaranteeType, AppConstants.GUARANTEETYPE_FOR_SESSION_BOOKING, StringComparison.InvariantCultureIgnoreCase))
                        hotelRoomRateEntity.IsSessionBooking = true;
                }

                if (roomStay.HotelReference != null)
                {
                    hotelSearchEntity.SelectedHotelCode = roomStay.HotelReference.hotelCode;
                    SearchedForEntity searchedFor = new SearchedForEntity(roomStay.HotelReference.Value);
                    searchedFor.SearchCode = roomStay.HotelReference.hotelCode;
                    searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.Hotel;
                    hotelSearchEntity.SearchedFor = searchedFor;
                }
                if (roomStay.HotelContact != null && roomStay.HotelContact.Addresses != null && roomStay.HotelContact.Addresses.Length > 0)
                {
                    hotelSearchEntity.HotelCountryCode = roomStay.HotelContact.Addresses[0].countryCode;
                }



                SpecialRequest[] spRequestList = roomStay.SpecialRequests;
                SpecialRequestEntity[] spRequestEntityList = null;
                if (spRequestList != null)
                {
                    int totalRequestCount = spRequestList.Length;
                    spRequestEntityList = new SpecialRequestEntity[totalRequestCount];
                    for (int count = 0; count < totalRequestCount; count++)
                    {
                        spRequestEntityList[count] = new SpecialRequestEntity()
                        {
                            RequestValue = spRequestList[count].requestCode,
                            RequestType = UserPreferenceConstants.SPECIALREQUEST
                        };
                    }
                    guestInformationEntity.SpecialRequests = spRequestEntityList;
                }



                ResGuest[] resGuest = hotelReservation.ResGuests;
                if (resGuest != null && resGuest.Length > 0)
                {
                    Profile[] profilesList = resGuest[0].Profiles;
                    if (profilesList != null && profilesList.Length > 0)
                    {
                        Profile profile = profilesList[0];
                        if (profilesList.Length > 1 && profilesList[1] != null)
                        {
                            Profile companyProfile = profilesList[1];
                            Company company = companyProfile.Item as Company;
                            if (company != null) guestInformationEntity.CompanyName = company.CompanyName;

                        }


                        if ((profile.ProfileIDs != null) && (profile.ProfileIDs.Length > 0))
                            guestInformationEntity.NameID = profile.ProfileIDs[0].Value;

                        Customer customer = profile.Item as Customer;
                        if (customer != null)
                        {
                            PersonName person = customer.PersonName;
                            guestInformationEntity.Title = (person.nameTitle != null && person.nameTitle.Length > 0)
                                                               ? person.nameTitle[0]
                                                               : string.Empty;
                            guestInformationEntity.FirstName = person.firstName;
                            guestInformationEntity.LastName = person.lastName;
                            guestInformationEntity.Gender = GetGender(customer);

                            if (customer.NativeName != null)
                            {
                                guestInformationEntity.NativeFirstName = customer.NativeName.firstName;
                                guestInformationEntity.NativeLastName = customer.NativeName.lastName;
                            }
                        }

                        NameAddress[] nameAddressList = profile.Addresses;
                        if (nameAddressList != null && (nameAddressList.Length > 0))
                        {
                            string[] addressLines = nameAddressList[0].AddressLine;
                            if (addressLines != null && (addressLines.Length > 0))
                            {
                                guestInformationEntity.AddressLine1 = addressLines[0];
                                if (addressLines.Length > 1)
                                    guestInformationEntity.AddressLine2 = addressLines[1];
                            }
                            guestInformationEntity.City = nameAddressList[0].cityName;
                            guestInformationEntity.Country = nameAddressList[0].countryCode;
                            guestInformationEntity.PostCode = nameAddressList[0].postalCode;
                        }

                        NameMembership[] memberships = profile.Memberships;
                        if (memberships != null && (memberships.Length > 0))
                        {
                            for (int i = 0; i < memberships.Length && memberships[i].membershipType.Equals(AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE); ++i)
                            {
                                guestInformationEntity.GuestAccountNumber = memberships[i].membershipNumber;
                                guestInformationEntity.MembershipOperaId = memberships[i].operaId;
                                break;
                            }
                        }
                    }
                }

                RewardNightGiftGuestInformationEntity rewardNightGiftGuestInfo = null;
                if (roomStay.Comments != null)
                {
                    bool isRewardNightGiftBooking = false;
                    int totalComments = roomStay.Comments.Length;
                    if (totalComments > 0)
                    {
                        for (int i = 0; i < totalComments; i++)
                        {
                            if (
                                roomStay.Comments[i].Items != null &&
                                roomStay.Comments[i].Items.Length > 0
                                )
                            {
                                int itemLength = roomStay.Comments[i].Items.Length;
                                for (int j = 0; j < itemLength; j++)
                                {
                                    Text commentText = roomStay.Comments[i].Items[j] as Text;
                                    string strRewardNightGiftGuestInfo = commentText.Value;
                                    if (
                                        strRewardNightGiftGuestInfo.StartsWith(
                                            RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_START +
                                            RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR,
                                            StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        isRewardNightGiftBooking = true;
                                        guestInformationEntity.IsRewardNightGift = true;
                                        rewardNightGiftGuestInfo = new RewardNightGiftGuestInformationEntity();

                                        string[] arRewardNightGiftGuestInfo =
                                            strRewardNightGiftGuestInfo.Split(
                                                new char[] { RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR },
                                                StringSplitOptions.RemoveEmptyEntries);
                                        if (arRewardNightGiftGuestInfo.Length ==
                                            RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_COUNT)
                                        {
                                            string name = arRewardNightGiftGuestInfo[1];
                                            string[] names =
                                                name.Split(
                                                    new char[]
                                                        {
                                                            RewardNightGiftBooking.
                                                                REWARD_NIGHT_GIFT_COMMENT_FORMAT_NAME_SEPARATOR
                                                        }, 2,
                                                    StringSplitOptions.RemoveEmptyEntries);
                                            if (names.Length > 1)
                                            {
                                                rewardNightGiftGuestInfo.FirstName = names[0];
                                                rewardNightGiftGuestInfo.LastName = names[1];
                                            }

                                            rewardNightGiftGuestInfo.Country = arRewardNightGiftGuestInfo[2];
                                            rewardNightGiftGuestInfo.MobileNumber = arRewardNightGiftGuestInfo[3];
                                            rewardNightGiftGuestInfo.EmailAddress = arRewardNightGiftGuestInfo[4];
                                        }

                                        break;
                                    }
                                }
                            }

                            if (isRewardNightGiftBooking)
                            {
                                break;
                            }
                        }
                    }
                }
                IdentifyBookingType(hotelReservation, hotelSearchEntity, hotelRoomRateEntity, guestInformationEntity);
                CancelDetailsEntity cancelDetails = null;
                if (hotelReservation.reservationStatus == ReservationStatusType.CANCELED)
                {
                    cancelDetails = GetCancellationDetails(roomStay);
                }

                string reservationNumber = string.Empty;
                string legNumber = FetchReservationNumber(hotelReservation, out reservationNumber);
                bookingDetails = new BookingDetailsEntity(hotelSearchEntity, hotelRoomRateEntity,
                                                          guestInformationEntity, cancelDetails,
                                                          rewardNightGiftGuestInfo);
                bookingDetails.ReservationNumber = reservationNumber;
                bookingDetails.LegNumber = legNumber;
                if (listBedAccommodation != null && listBedAccommodation.Count > 0)
                    bookingDetails.ListBedAccommodation = listBedAccommodation;

                if (hotelReservation.ReservationHistory != null)
                {
                    bookingDetails.ReservationCreateDate = hotelReservation.ReservationHistory.insertDate;
                }
            }
            return bookingDetails;
        }



        /// <summary>
        /// Fetch the Reservation Number
        /// </summary>
        /// <param name="hotelReservation">
        /// HotelReservation
        /// </param>
        /// <param name="reservationNumber">
        /// Out Reservation Number</param>
        /// <returns>
        /// Leg-Number
        /// </returns>
        private string FetchReservationNumber(HotelReservation hotelReservation, out string reservationNumber)
        {
            string legNumber = string.Empty;
            reservationNumber = string.Empty;
            if (hotelReservation != null)
            {
                UniqueID[] uniqueIDList = hotelReservation.UniqueIDList;
                for (int i = 0; i < uniqueIDList.Length; i++)
                {
                    UniqueID uniqueID = uniqueIDList[i];
                    if (uniqueID.source != AppConstants.ID_SOURCE_RESERVATION &&
                        (string.IsNullOrEmpty(reservationNumber)))
                    {
                        reservationNumber = uniqueID.Value;
                    }
                    if (uniqueID.source == AppConstants.ID_LEGNUMBER)
                    {
                        legNumber = uniqueID.Value;
                    }
                }
            }
            return legNumber;
        }

        /// <summary>
        /// Get the details of the cancelled reservation.
        /// </summary>
        /// <param name="roomStay">
        /// RoomStay
        /// </param>
        /// <returns>
        /// CancelDetailsEntity
        /// </returns>
        private CancelDetailsEntity GetCancellationDetails(
            ServiceProxies.Reservation.RoomStay roomStay)
        {
            CancelDetailsEntity cancelDetails = null;
            if (roomStay != null && (roomStay.CancelTerm != null))
            {
                cancelDetails = new CancelDetailsEntity();
                cancelDetails.CancelDate = roomStay.CancelTerm.cancelDate;
                cancelDetails.CancelDateSpecified = true;
                cancelDetails.CancelNumber = roomStay.CancelTerm.cancelNumber;
                switch (roomStay.CancelTerm.cancelType)
                {
                    case ServiceProxies.Reservation.CancelTermType.Cancel:
                        {
                            cancelDetails.CancelType = Scandic.Scanweb.Entity.CancelTermType.Cancel;
                            break;
                        }
                    case ServiceProxies.Reservation.CancelTermType.NoShow:
                        {
                            cancelDetails.CancelType = Scandic.Scanweb.Entity.CancelTermType.NoShow;
                            break;
                        }
                    case ServiceProxies.Reservation.CancelTermType.Other:
                        {
                            cancelDetails.CancelType = Scandic.Scanweb.Entity.CancelTermType.Other;
                            break;
                        }
                }
            }
            return cancelDetails;
        }

        /// <summary>
        /// Identify the Booking Type
        /// </summary>
        /// <param name="hotelReservation">
        /// </param>
        /// <param name="hotelSearch">
        /// HotelSearch Entity
        /// </param>
        /// <param name="hotelRoomRateEntity">
        /// HotelRoomRate Entity
        /// </param>
        private void IdentifyBookingType(HotelReservation hotelReservation,
                                         HotelSearchEntity hotelSearch, HotelRoomRateEntity hotelRoomRateEntity,
                                         GuestInformationEntity guestInformationEntity)
        {
            if (CheckRedemptionBooking(hotelReservation, hotelSearch, hotelRoomRateEntity))
            {
            }
            else if (CheckBlockBooking(hotelReservation, hotelSearch, hotelRoomRateEntity))
            {
            }
            else if (CheckVoucher(hotelReservation, hotelSearch, hotelRoomRateEntity))
            {
            }
            else if ((CheckBonusCheck(hotelReservation, hotelSearch, hotelRoomRateEntity, guestInformationEntity)))
            {
            }
            else if ((CheckTravelAgent(hotelReservation, hotelSearch, hotelRoomRateEntity)))
            {
            }
            else if ((CheckNegotiated(hotelReservation, hotelSearch, hotelRoomRateEntity)))
            {
            }
            else if ((CheckPromotion(hotelReservation, hotelSearch, hotelRoomRateEntity)))
            {
            }
            else if ((CheckRegular(hotelReservation, hotelSearch, hotelRoomRateEntity)))
            {
            }
        }

        /// <summary>
        /// Check if the Booking is a Voucher booking
        /// </summary>
        /// <param name="hotelReservation">
        /// </param>
        /// <param name="hotelSearch">
        /// HotelSearchEntity
        /// </param>
        /// <param name="hotelRoomRateEntity">
        /// HotelRoomRateEntity
        /// </param>
        /// <returns>
        /// True if Booking is Voucher booking, else false.
        /// </returns>
        private bool CheckRedemptionBooking(HotelReservation hotelReservation,
                                            HotelSearchEntity hotelSearch, HotelRoomRateEntity hotelRoomRateEntity)
        {
            bool isRedemption = false;
            if (hotelReservation.redemReservationFlag && hotelReservation.redemReservationFlagSpecified)
            {
                hotelSearch.SearchingType = SearchType.REDEMPTION;
                isRedemption = true;
            }
            return isRedemption;
        }

        /// <summary>
        /// Check if this is a block code booking.
        /// </summary>
        /// <param name="hotelReservation">Hotel reservation</param>
        /// <param name="hotelSearch">Hotel search</param>
        /// <param name="hotelRoomRateEntity">Room rate entity</param>
        /// <returns>True if block code booking/False if not block code booking</returns>
        private bool CheckBlockBooking(HotelReservation hotelReservation,
                                       HotelSearchEntity hotelSearch, HotelRoomRateEntity hotelRoomRateEntity)
        {
            bool isBlockBooking = false;
            RoomStay[] roomStayList = hotelReservation.RoomStays;
            if (!string.IsNullOrEmpty(roomStayList[0].RoomTypes[0].invBlockCode))
            {
                hotelSearch.CampaignCode = roomStayList[0].RoomTypes[0].invBlockCode;
                hotelSearch.QualifyingType = AppConstants.BLOCK_CODE_QUALIFYING_TYPE;
                hotelSearch.SearchingType = SearchType.CORPORATE;
                isBlockBooking = true;
            }
            return isBlockBooking;
        }

        /// <summary>
        /// Check if the Booking is a Voucher booking
        /// </summary>
        /// <param name="hotelReservation">
        /// </param>
        /// <param name="hotelSearch">
        /// HotelSearchEntity
        /// </param>
        /// <param name="hotelRoomRateEntity">
        /// HotelRoomRateEntity
        /// </param>
        /// <returns>
        /// True if Booking is Voucher booking, else false.
        /// </returns>
        private bool CheckVoucher(HotelReservation hotelReservation,
                                  HotelSearchEntity hotelSearch, HotelRoomRateEntity hotelRoomRateEntity)
        {
            bool isVoucher = false;
            string promotionCode = string.Empty;
            RoomStay[] roomStayList = hotelReservation.RoomStays;
            RoomStay roomStay = null;
            if ((roomStayList != null) && (roomStayList.Length > 0))
            {
                roomStay = roomStayList[0];
                RatePlan[] ratePlanList = roomStay.RatePlans;
                if ((ratePlanList != null) && (ratePlanList.Length > 0))
                {
                    hotelRoomRateEntity.RatePlanCode = ratePlanList[0].ratePlanCode;
                    promotionCode = ratePlanList[0].promotionCode;
                }
            }
            if ((promotionCode != null) && (promotionCode != string.Empty))
            {
                if (promotionCode.StartsWith(AppConstants.VOUCHER_BOOKING_CODE))
                {
                    hotelSearch.SearchingType = SearchType.VOUCHER;
                    hotelRoomRateEntity.IsSpecialRate = true;
                    isVoucher = true;
                }
            }
            return isVoucher;
        }

        /// <summary>
        /// Check if the Booking is a Bonus Cheque booking
        /// </summary>
        /// <param name="hotelReservation">
        /// </param>
        /// <param name="hotelSearch">
        /// HotelSearchEntity
        /// </param>
        /// <param name="hotelRoomRateEntity">
        /// HotelRoomRateEntity
        /// </param>
        /// <param name="guestInformationEntity"></param>
        /// <returns>
        /// True if Booking is Bonus cheque booking, else false.
        /// </returns>
        private bool CheckBonusCheck(HotelReservation hotelReservation,
                                     HotelSearchEntity hotelSearch, HotelRoomRateEntity hotelRoomRateEntity,
                                     GuestInformationEntity guestInformationEntity)
        {
            bool isBonusCheque = false;
            string ratePlanCode = string.Empty;
            if (hotelReservation != null)
            {
                RoomStay[] roomStayList = hotelReservation.RoomStays;
                RoomStay roomStay = null;
                if ((roomStayList != null) && (roomStayList.Length > 0))
                {
                    roomStay = roomStayList[0];
                    RatePlan[] ratePlanList = roomStay.RatePlans;
                    if ((ratePlanList != null) && (ratePlanList.Length > 0))
                    {
                        hotelRoomRateEntity.RatePlanCode = ratePlanList[0].ratePlanCode;
                        ratePlanCode = ratePlanList[0].ratePlanCode;
                    }
                }
                string[] bonusChequeRates = AppConstants.BONUS_CHEQUE_RATES;
                if (Core.StringUtil.IsStringInArray(bonusChequeRates, ratePlanCode))
                {
                    hotelSearch.SearchingType = SearchType.BONUSCHEQUE;
                    isBonusCheque = true;
                    ServiceProxies.Reservation.ResGuest[] reservationGuest = hotelReservation.ResGuests;
                    if ((reservationGuest != null) && (reservationGuest.Length > 0))
                    {
                        Profile[] profileList = reservationGuest[0].Profiles;
                        if ((profileList != null) & (profileList.Length > 0))
                        {
                            foreach (Profile profile in profileList)
                            {
                                Company company = profile.Item as Company;
                                if (company != null)
                                {
                                    guestInformationEntity.D_Number = company.CompanyID;
                                }
                            }
                        }
                    }
                }
            }
            return isBonusCheque;
        }

        /// <summary>
        /// check if the Booking is a Travel Agent booking
        /// </summary>
        /// <param name="hotelReservation"></param>
        /// <param name="hotelSearch"></param>
        /// <param name="hotelRoomRateEntity"></param>
        /// <returns></returns>
        private bool CheckTravelAgent(HotelReservation hotelReservation,
                                      HotelSearchEntity hotelSearch, HotelRoomRateEntity hotelRoomRateEntity)
        {
            bool isTravelAgent = false;
            ResGuest[] resGuestList = hotelReservation.ResGuests;
            if (resGuestList != null && (resGuestList.Length > 0))
            {
                Profile[] profileList = resGuestList[0].Profiles;
                if (profileList != null)
                {
                    for (int count = 0; count < profileList.Length; count++)
                    {
                        Company company = profileList[count].Item as Company;
                        if (company != null)
                        {
                            if (company.CompanyType.ToString() == AppConstants.QUALIFYING_TYPE_TRAVEL_AGENT && company.CompanyID.ToString().StartsWith("L"))
                            {
                                if (string.IsNullOrEmpty(hotelSearch.CampaignCode))
                                {
                                    hotelSearch.CampaignCode = company.CompanyID;
                                }
                                hotelSearch.SearchingType = SearchType.CORPORATE;
                                hotelSearch.QualifyingType = AppConstants.QUALIFYING_TYPE_TRAVEL_AGENT;
                                hotelRoomRateEntity.IsSpecialRate = true;
                                isTravelAgent = true;
                                break;
                            }
                        }
                    }
                }
            }
            return isTravelAgent;
        }

        /// <summary>
        /// Check if the Booking is a Negotiated booking
        /// </summary>
        /// <param name="hotelReservation">
        /// </param>
        /// <param name="hotelSearch">
        /// HotelSearchEntity
        /// </param>
        /// <param name="hotelRoomRateEntity">
        /// HotelRoomRateEntity
        /// </param>
        /// <returns>
        /// True if Booking is Negotiated booking, else false.
        /// </returns>
        private bool CheckNegotiated(HotelReservation hotelReservation,
                                     HotelSearchEntity hotelSearch, HotelRoomRateEntity hotelRoomRateEntity)
        {
            bool isNegotiated = false;
            ResGuest[] resGuestList = hotelReservation.ResGuests;
            if (resGuestList != null && (resGuestList.Length > 0))
            {
                Profile[] profileList = resGuestList[0].Profiles;
                if (profileList != null)
                {
                    for (int count = 0; count < profileList.Length; count++)
                    {
                        Company company = profileList[count].Item as Company;
                        if (company != null && company.CompanyType.ToString() != AppConstants.QUALIFYING_TYPE_TRAVEL_AGENT)
                        {
                            if (string.IsNullOrEmpty(hotelSearch.CampaignCode))
                            {
                                hotelSearch.CampaignCode = company.CompanyID;
                            }
                            hotelSearch.SearchingType = SearchType.CORPORATE;
                            hotelSearch.QualifyingType = AppConstants.QUALIFYING_TYPE_CORPORATE;
                            hotelRoomRateEntity.IsSpecialRate = true;
                            isNegotiated = true;
                            break;
                        }
                    }
                }
            }
            return isNegotiated;
        }

        /// <summary>
        /// Check whether the booking is Promotion Booking
        /// </summary>
        /// <param name="hotelReservation">
        /// </param>
        /// <param name="hotelSearch">
        /// HotelSearchEntity
        /// </param>
        /// <param name="hotelRoomRateEntity">
        /// HotelRoomRateEntity
        /// </param>
        /// <returns>
        /// True if Booking is Promotion, else false.
        /// </returns>
        private bool CheckPromotion(HotelReservation hotelReservation,
                                    HotelSearchEntity hotelSearch, HotelRoomRateEntity hotelRoomRateEntity)
        {
            bool isPromotion = false;
            string promotionCode = string.Empty;
            RoomStay[] roomStayList = hotelReservation.RoomStays;
            RoomStay roomStay = null;
            if ((roomStayList != null) && (roomStayList.Length > 0))
            {
                roomStay = roomStayList[0];
                RatePlan[] ratePlanList = roomStay.RatePlans;
                if ((ratePlanList != null) && (ratePlanList.Length > 0))
                {
                    hotelRoomRateEntity.RatePlanCode = ratePlanList[0].ratePlanCode;
                    promotionCode = ratePlanList[0].promotionCode;
                }
            }
            if ((promotionCode != null) && (promotionCode != string.Empty))
            {
                hotelSearch.CampaignCode = promotionCode;
                hotelSearch.SearchingType = SearchType.REGULAR;
                hotelRoomRateEntity.IsSpecialRate = true;
                isPromotion = true;
            }
            return isPromotion;
        }


        /// <summary>
        /// Check whether the booking is Regular Booking
        /// </summary>
        /// <param name="hotelReservation">
        /// </param>
        /// <param name="hotelSearch">
        /// HotelSearchEntity
        /// </param>
        /// <param name="hotelRoomRateEntity">
        /// HotelRoomRateEntity
        /// </param>
        /// <returns>
        /// True if Booking is Regular, else false.
        /// </returns>
        private bool CheckRegular(HotelReservation hotelReservation,
                                  HotelSearchEntity hotelSearch, HotelRoomRateEntity hotelRoomRateEntity)
        {
            bool isRegular = false;
            string promotionCode = string.Empty;
            RoomStay[] roomStayList = hotelReservation.RoomStays;
            RoomStay roomStay = null;
            if ((roomStayList != null) && (roomStayList.Length > 0))
            {
                roomStay = roomStayList[0];
                RatePlan[] ratePlanList = roomStay.RatePlans;
                if ((ratePlanList != null) && (ratePlanList.Length > 0))
                {
                    hotelRoomRateEntity.RatePlanCode = ratePlanList[0].ratePlanCode;
                    promotionCode = ratePlanList[0].promotionCode;
                }
            }
            if (promotionCode == null || promotionCode == string.Empty)
            {
                hotelSearch.SearchingType = SearchType.REGULAR;
                isRegular = true;
            }
            return isRegular;
        }

        /// <summary>
        /// Identify the Gender
        /// </summary>
        /// <param name="customer">
        /// Customer Entity
        /// </param>
        /// <returns>
        /// Gender Name
        /// </returns>
        private string GetGender(Customer customer)
        {
            string gender = string.Empty;
            if (customer.gender == Gender.MALE)
                gender = AppConstants.MALE;
            else if (customer.gender == Gender.FEMALE)
                gender = AppConstants.FEMALE;
            return gender;
        }

        /// <summary>
        /// Replaces the special character with special character mapping
        /// </summary>
        /// <param name="stringClean">String to be cleaned</param>
        /// <returns>Clean string</returns>
        //private string GetCleanString(string stringClean)
        //{
        //    string nameCharacterMapping = AppConstants.NAME_CHARACTER_MAPPING;
        //    if (nameCharacterMapping != null)
        //    {
        //        string[] nameCharacterMappingArray = nameCharacterMapping.Split(AppConstants.CHAR_COMMA);
        //        string[] keys = new string[2];
        //        for (int counter = 0; counter < nameCharacterMappingArray.Length; counter++)
        //        {
        //            keys = nameCharacterMappingArray[counter].Split(AppConstants.CHAR_EQUAL);
        //            stringClean = stringClean.Replace(keys[0].Trim(), keys[1].Trim());
        //        }
        //    }
        //    return stringClean;
        //}

        /// <summary>
        /// AddChildrenAccommodations
        /// </summary>
        /// <param name="reservationNumber"></param>
        /// <param name="legNumber"></param>
        /// <param name="selectedHotelCode"></param>
        /// <param name="guestInfoEntity"></param>
        public void AddChildrenAccommodations(string reservationNumber, string legNumber, string selectedHotelCode,
                                              GuestInformationEntity guestInfoEntity)
        {
            UpdatePackagesRequest request = null;
            UpdatePackagesResponse response = null;
            try
            {
                AppLogger.LogInfoMessage("In ReservationDomain\\AddChildrenAccommodations()");
                if (guestInfoEntity != null && guestInfoEntity.ChildrensDetails != null)
                {
                    AppLogger.LogInfoMessage("OWSUtility.GetReservationService()");
                    ReservationService service = OWSUtility.GetReservationService();

                    List<BedAccommodation> listBedAccommodation =
                        guestInfoEntity.ChildrensDetails.GetAccommodationList();
                    if (listBedAccommodation != null)
                    {
                        foreach (BedAccommodation bedtype in listBedAccommodation)
                        {
                            AppLogger.LogInfoMessage(
                                "Creating request object: OWSRequestConverter.GetUpdatePackages(reservationNumber, hotelCode, guestInformation)");
                            //Reseting Header values
                            service.OGHeaderValue.timeStamp = DateTime.Now;
                            service.OGHeaderValue.timeStampSpecified = true;
                            service.OGHeaderValue.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
                            service.OGHeaderValue.Origin.systemType = AppConstants.OWS_ORIGIN_SYSTEM_TYPE;
                            service.OGHeaderValue.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
                            service.OGHeaderValue.Destination.systemType = AppConstants.OWS_DESTINATION_SYSTEM_TYPE;

                            request =
                                OWSRequestConverter.GetUpdatePackagesRequest(reservationNumber, legNumber,
                                                                             selectedHotelCode, bedtype);

                            AppLogger.LogInfoMessage("Fetching response: service.UpdatePackages(request)");
                            response = service.UpdatePackages(request);
                            AppLogger.LogInfoMessage("Response received: service.UpdatePackages(request)");
                            if (response.Result.resultStatusFlag != ServiceProxies.Reservation.ResultStatusFlag.FAIL)
                            {
                                AppLogger.LogInfoMessage(
                                    "ReservationDomain-AddChildrenAccommodations resultStatusFlag: success");
                            }
                            else
                            {
                                UserNavTracker.TrackOWSRequestResponse<UpdatePackagesRequest, UpdatePackagesResponse>(request, response);
                                AppLogger.LogInfoMessage(
                                    "ReservationDomain-AddChildrenAccommodations resultStatusFlag: failed");
                                AppLogger.LogInfoMessage(request.ProductCode);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UserNavTracker.TrackOWSRequestResponse<UpdatePackagesRequest, UpdatePackagesResponse>(request, response);
                AppLogger.LogFatalException(ex);
            }
            finally
            {
                request = null;
                response = null;
            }
        }


        /// <summary>
        /// Delete the Child Accommodation Preferences
        /// </summary>
        /// <param name="reservationNumber">Reservation Number</param>
        /// <param name="legNumber">Reservation's Leg Number</param>
        /// <param name="selectedHotelCode">Hotel Code</param>
        /// <param name="guestInfoEntity">GuestInformation Entity</param>
        public void DeleteChildAccommodations(string reservationNumber, string legNumber, string selectedHotelCode,
                                              GuestInformationEntity guestInfoEntity)
        {
            DeletePackagesResponse response = null;
            DeletePackagesRequest request = null;
            try
            {
                AppLogger.LogInfoMessage("In ReservationDomain\\DeleteChildAccommodations()");

                if (guestInfoEntity != null && guestInfoEntity.ChildrensDetails != null)
                {
                    AppLogger.LogInfoMessage("OWSUtility.GetReservationService()");
                    ReservationService service = OWSUtility.GetReservationService();
                    AppLogger.LogInfoMessage(
                        "Creating request object: OWSRequestConverter.GetDeletePackages(reservationNumber, hotelCode, guestInformation)");

                    List<BedAccommodation> listBedAccommodation =
                        guestInfoEntity.ChildrensDetails.GetAccommodationList();
                    if (listBedAccommodation != null)
                    {
                        foreach (BedAccommodation bedtype in listBedAccommodation)
                        {
                            AppLogger.LogInfoMessage(
                                "Creating request object: OWSRequestConverter.GetDeletePackagesRequest(reservationNumber, hotelCode, guestInformation)");
                            //Reseting Header values
                            service.OGHeaderValue.timeStamp = DateTime.Now;
                            service.OGHeaderValue.timeStampSpecified = true;
                            service.OGHeaderValue.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
                            service.OGHeaderValue.Origin.systemType = AppConstants.OWS_ORIGIN_SYSTEM_TYPE;
                            service.OGHeaderValue.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
                            service.OGHeaderValue.Destination.systemType = AppConstants.OWS_DESTINATION_SYSTEM_TYPE;
                            request = OWSRequestConverter.GetDeletePackagesRequest(reservationNumber, legNumber,
                                                                                   selectedHotelCode, bedtype);
                            AppLogger.LogInfoMessage("Fetching response: service.DeletePackages(request)");
                            response = service.DeletePackages(request);
                            AppLogger.LogInfoMessage("Response received: service.DeletePackages(request)");
                            if (response.Result.resultStatusFlag != ServiceProxies.Reservation.ResultStatusFlag.FAIL)
                            {
                                AppLogger.LogInfoMessage(
                                    "ReservationDomain-DeleteChildAccommodations resultStatusFlag: success");
                            }
                            else
                            {
                                UserNavTracker.TrackOWSRequestResponse<DeletePackagesRequest, DeletePackagesResponse>(request, response);
                                AppLogger.LogInfoMessage(
                                    "ReservationDomain-DeleteChildAccommodations resultStatusFlag: failed");
                                AppLogger.LogInfoMessage(request.ProductCode);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UserNavTracker.TrackOWSRequestResponse<DeletePackagesRequest, DeletePackagesResponse>(request, response);
                AppLogger.LogFatalException(ex);
            }
            finally
            {
                request = null;
                response = null;
            }
        }

        #endregion

        #region OWSMonitoring

        /// <summary>
        ///Method call to check the health of OWS-Information service
        /// </summary>
        /// <param name="reservationNumber">Reservation number</param>
        /// <returns>True if service is up else False</returns>
        public bool FetchReservationForOWSMonitor(string reservationNumber)
        {
            ReservationService service = OWSUtility.GetReservationService();
            FetchSummaryRequest request = null;
            FetchSummaryResponse response = null;
            bool result = true;
            try
            {
                request = OWSRequestConverter.GetFetchBookingSummary(reservationNumber);
                response = service.FetchSummary(request);

                if ((response != null) && (response.Result.resultStatusFlag == ServiceProxies.Reservation.ResultStatusFlag.FAIL))
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchSummaryRequest, FetchSummaryResponse>(request, response);
                    if ((response.Result.Text != null) && (response.Result.Text[0].Value == AppConstants.SYSTEM_ERROR))
                    {
                        result = false;
                    }
                    else if ((response.Result.GDSError != null) &&
                             (response.Result.GDSError.Value == AppConstants.SYSTEM_ERROR))
                    {
                        result = false;
                    }
                }
            }
            catch (Exception)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchSummaryRequest, FetchSummaryResponse>(request, response);
            }

            finally
            {
                request = null;
                response = null;
            }
            return result;
        }


        #endregion OWSMonitoring


    }
}