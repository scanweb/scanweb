using System;
using System.Collections;
using Scandic.Scanweb.BookingEngine.ServiceProxies.AdvancedReservationService;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Information;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Membership;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Name;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Reservation;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Security;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.ExceptionManager;

namespace Scandic.Scanweb.BookingEngine.Domain
{
    /// <summary>
    /// OWSUtility
    /// </summary>
    internal class OWSUtility
    {
        #region Public Static Methods    

        /// <summary>
        /// Get the Name Service Proxy
        /// </summary>
        /// <param name="primaryLangaugeID">Primary LanguageID</param>
        /// <returns>NameService</returns>
        public static NameService GetNameService(params string[] primaryLangaugeID)
        {
            NameService service = new NameService();
            service.OGHeaderValue = GetNameHeader();
            if (primaryLangaugeID != null && primaryLangaugeID.Length > 0)
            {
                service.OGHeaderValue.primaryLangID = primaryLangaugeID[0];
            }

            service.Url = AppConstants.OWS_NAME_SERVICE;

            return service;
        }

        /// <summary>
        /// GetMembershipService
        /// </summary>
        /// <returns>MembershipService</returns>
        public static MembershipService GetMembershipService()
        {
            MembershipService service = new MembershipService();
            service.OGHeaderValue = GetMembershipHeader();
            service.Url = AppConstants.OWS_MEMBERSHIP_SERVICE;

            return service;
        }

        /// <summary>
        /// GetSecurityService
        /// </summary>
        /// <returns>SecurityService</returns>
        public static SecurityService GetSecurityService()
        {
            SecurityService service = new SecurityService();
            service.OGHeaderValue = GetSecurityHeader();
            service.Url = AppConstants.OWS_SECURITY_SERVICE;

            return service;
        }

        /// <summary>
        /// GetAvailabilityService
        /// </summary>
        /// <returns>AvailabilityService</returns>
        public static AvailabilityService GetAvailabilityService()
        {
            AvailabilityService service = new AvailabilityService();
            service.OGHeaderValue = GetAvailabilityHeader();
            service.Url = AppConstants.OWS_AVAILABILITY_SERVICE;
            return service;
        }

        /// <summary>
        /// GetReservationService
        /// </summary>
        /// <returns>ReservationService</returns>
        public static ReservationService GetReservationService()
        {
            ReservationService service = new ReservationService();
            service.OGHeaderValue = GetReservationHeader();
            service.Url = AppConstants.OWS_RESERVATION_SERVICE;
            return service;
        }
        /// <summary>
        /// GetReservationService
        /// </summary>
        /// <returns>ReservationService</returns>
        public static ReservationService GetPrepaidReservationService()
        {
            ReservationService service = new ReservationService();
            service.OGHeaderValue = GetReservationHeader();
            service.Url = AppConstants.OWS_PREPAID_RESERVATION_SERVICE;
            return service;
        }

        /// <summary>
        /// GetReservationService
        /// </summary>
        /// <param name="isSessionBooking"></param>
        /// <returns>ReservationService</returns>
        public static ReservationService GetReservationService(bool isSessionBooking)
        {
            ReservationService service = new ReservationService();
            service.OGHeaderValue = GetReservationHeader(isSessionBooking);
            service.Url = AppConstants.OWS_RESERVATION_SERVICE;
            return service;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isSessionBooking"></param>
        /// <param name="isIntermediaries"></param>
        /// <returns></returns>
        public static ReservationService GetReservationService(bool isSessionBooking, bool isIntermediaries)
        {
            ReservationService service = new ReservationService();
            service.OGHeaderValue = GetReservationHeader(isSessionBooking, isIntermediaries);
            service.Url = AppConstants.OWS_RESERVATION_SERVICE;
            return service;
        }
       
        /// <summary>
        /// GetNameService
        /// </summary>
        /// <returns>NameService</returns>
        public static NameService GetNameService()
        {
            NameService service = new NameService();
            service.OGHeaderValue = GetNameHeader();
            service.Url = AppConstants.OWS_NAME_SERVICE;
            return service;
        }

        /// <summary>
        /// GetInformationService
        /// </summary>
        /// <returns>InformationService</returns>
        public static InformationService GetInformationService()
        {
            InformationService service = new InformationService();
            service.OGHeaderValue = GetInformationHeader();
            service.Url = AppConstants.OWS_INFORMATION_SERVICE;
            return service;
        }

        /// <summary>
        /// GetNameServiceUniqueID
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns>Unique Id</returns>
        public static ServiceProxies.Name.UniqueID GetNameServiceUniqueID(string nameID)
        {
            ServiceProxies.Name.UniqueID id =
                new ServiceProxies.Name.UniqueID();
            id.Value = nameID;
            id.type = ServiceProxies.Name.UniqueIDType.INTERNAL;

            return id;
        }

        /// <summary>
        /// GetSecurityServiceUniqueID
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns>Unique Id</returns>
        public static ServiceProxies.Security.UniqueID GetSecurityServiceUniqueID
            (string nameID)
        {
            ServiceProxies.Security.UniqueID id = 
                new ServiceProxies.Security.UniqueID();
            id.Value = nameID;
            id.type = ServiceProxies.Security.UniqueIDType.INTERNAL;

            return id;
        }

        /// <summary>
        /// GetInformationService
        /// </summary>
        /// <returns>InformationService</returns>
        public static ResvAdvancedService GetAdvancedReservationService()
        {
            ResvAdvancedService service = new ResvAdvancedService();
            service.OGHeaderValue = GetAdvancedReservationHeader();
            service.Url = AppConstants.OWS_ADVANCEDRESERVATION_SERVICE;
            return service;
        }

        public static bool IsPartnerCardExcluded(string cardName)
        {
            string partnerProgramKey = AppConstants.EXCLUDEDPARTNERPROGRAM;
            return (!string.IsNullOrEmpty(partnerProgramKey) && partnerProgramKey.Contains(";" + cardName.ToUpper() + ";"));
        }

        #region RaiseOWSException

        /// <summary>
        /// RaiseOWSException
        /// </summary>
        /// <param name="exceptionMessage"></param>
        /// <param name="generalException"></param>
        public static void RaiseOWSException(string exceptionMessage, Exception generalException)
        {
            throw new OWSException(exceptionMessage, generalException);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        /// <param name="exceptionData"></param>
        public static void RaiseRegionalAvailOWSException(RegionalAvailabilityResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }

                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }
            throw new OWSException(errorMessage, errorCode, exceptionData, true);
        }
       
        /// <summary>
        /// RaiseGeneralAvailOWSException
        /// </summary>
        /// <param name="response"></param>
        /// <param name="exceptionData"></param>
        public static void RaiseGeneralAvailOWSException(AvailabilityResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }

                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }
            throw new OWSException(errorMessage, errorCode, exceptionData, true);
        }

        /// <summary>
        /// RaiseMembershipOWSException
        /// </summary>
        /// <param name="response"></param>
        /// <param name="exceptionData"></param>
        public static void RaiseMembershipOWSException
            (FetchMembershipTransactionsResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;
            ServiceProxies.Membership.GDSResultStatus resultStatus = response.Result as ServiceProxies.Membership.GDSResultStatus;
            if (resultStatus != null && resultStatus.GDSError != null)
            {
                if (!String.IsNullOrEmpty(resultStatus.GDSError.errorCode))
                {
                    errorCode = resultStatus.GDSError.errorCode;
                }
                if (!String.IsNullOrEmpty(resultStatus.GDSError.Value))
                {
                    errorMessage = resultStatus.GDSError.Value;
                }
            }

            throw new OWSException(errorMessage, errorCode, exceptionData);
        }

        /// <summary>
        /// Raise the Hotel not found exception
        /// </summary>
        /// <param name="response">
        /// HotelInformationResponse
        /// </param>
        public static void RaiseHotelInformationOWSException(HotelInformationResponse response)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }
                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }
            else if (response.Result.Text[0] != null)
            {
                errorMessage = response.Result.Text[0].Value;
            }
            throw new OWSException(errorMessage, errorCode);
        }

        public static void RaiseSecurityOWSException()
        {

        }

        /// <summary>
        /// Raise Reservation Exception
        /// </summary>
        /// <param name="response">
        /// CreateBookingResponce
        /// </param>
        /// <param name="exceptionData">
        /// HashTable of exceptionData
        /// </param>
        public static void RaiseReservationOWSException(CreateBookingResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }
                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }
            else if (response.Result != null && response.Result.Text != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].Value == null ? string.Empty : response.Result.Text[0].Value;
                }
                if (!string.IsNullOrEmpty(response.Result.OperaErrorCode))
                {
                    if (string.IsNullOrEmpty(errorMessage) && !string.IsNullOrEmpty(response.Result.OperaErrorCode) &&
                            response.Result.OperaErrorCode == OWSExceptionConstants.ROOM_UNAVAILABLE)
                        errorMessage = response.Result.OperaErrorCode;

                    errorCode = response.Result.OperaErrorCode;
                }
            }
            throw new OWSException(errorMessage, errorCode, exceptionData, true);
        }

        /// <summary>
        /// This method throws the  OWS Exception for Cancel
        /// </summary>
        /// <param name="response"></param>
        /// <param name="exceptionData"></param>
        public static void RaiseCancelOWSException(CancelBookingResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }
                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }
            else if (response.Result.Text != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].Value;
                }
            }
            throw new OWSException(errorMessage, errorCode, exceptionData);
        }

        /// <summary>
        /// Raises custom exception if there is any.
        /// </summary>
        /// <param name="response">AddPromoSubscriptionResponse response</param>
        /// <param name="promoCode"></param>
        /// <param name="exceptionData"></param>
        public static void RaiseAddPromoSubscriptionResponse
            (AddPromoSubscriptionResponse response, string promoCode, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;
            ServiceProxies.Membership.GDSError gDSError = null;

            gDSError =
                ((ServiceProxies.Membership.GDSResultStatus) response.Result).GDSError;

            if (gDSError != null)
            {
                if (gDSError.Value != null)
                {
                    errorMessage = gDSError.Value;
                }
                if (gDSError.errorCode != null)
                {
                    errorCode = gDSError.errorCode;
                }
            }
            else if (response.Result.Text != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].Value;
                }
            }

            if (!String.IsNullOrEmpty(errorMessage))
            {
                errorMessage = errorMessage.Trim().ToLower();
                string PromoCodeExpiredOperaMessage =
                    String.Format(FastTrackEnrolmentMessageConstants.PromoCodeExpiredOperaMessage, promoCode);
                PromoCodeExpiredOperaMessage = PromoCodeExpiredOperaMessage.Trim().ToLower();

                if (errorMessage == PromoCodeExpiredOperaMessage)
                {
                    errorMessage = errorMessage.Replace("(" + promoCode.ToLower() + ")", String.Empty);
                }
            }

            throw new OWSException(errorMessage, errorCode, exceptionData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gdsError"></param>
        /// <param name="text"></param>
        /// <param name="exceptionData"></param>
        public static void RaiseReservationOWSException(ServiceProxies.Reservation.GDSError gdsError, ServiceProxies.Reservation.Text[] text, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (gdsError != null)
            {
                if (gdsError.Value != null)
                {
                    errorMessage = gdsError.Value;
                }
                if (gdsError.errorCode != null)
                {
                    errorCode = gdsError.errorCode;
                }
            }
            else if (text != null)
            {
                if (text.Length > 0)
                {
                    errorMessage = text[0].Value;
                }
            }
            throw new OWSException(errorMessage, errorCode, exceptionData);
        }


        /// <summary>
        /// Raise FetchReservation OWSException
        /// </summary>
        /// <param name="response"></param>
        /// <param name="exceptionData"></param>
        public static void RaiseFetchReservationOWSException(FetchBookingResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }
                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }
            else if (response.Result.Text != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].Value;
                }
            }
            throw new OWSException(errorMessage, errorCode, exceptionData, true);
        }


        /// <summary>
        /// Raise FetchSummary OWSException
        /// </summary>
        /// <param name="response">
        /// FetchSummaryResponse
        /// </param>
        /// <param name="exceptionData">
        /// ExceptionData
        /// </param>
        public static void RaiseFetchSummaryOWSException(FetchSummaryResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }
                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }
            else if (response.Result.Text != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].Value;
                }
            }
            throw new OWSException(errorMessage, errorCode, exceptionData, true);
        }

        /// <summary>
        /// Raise Future Booking Summary OWS Exception
        /// </summary>
        /// <param name="response">FutureBookingSummary Response</param>
        /// <param name="exceptionData"></param>
        public static void RaiseCurrentBookingListOWSException(FutureBookingSummaryResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }
                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }
            else if (response.Result.Text != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].Value;
                }
            }
            throw new OWSException(errorMessage, errorCode, exceptionData, true);
        }

        public static void RaiseNameOWSException()
        {

        }

        /// <summary>
        /// Raise Register User Exception
        /// </summary>
        /// <param name="response">
        /// RegisterUserResponse
        /// </param>
        public static void RaiseRegisterUserOWSException(RegisterNameResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].Value.ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Fetch Next Card Number Exception
        /// </summary>
        /// <param name="response">
        /// FetchNextCardNumberResponce
        /// </param>
        /// <param name="exceptionData"></param>
        public static void RaiseFetchNextCardNumberOWSException
            (FetchNextCardNumberResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;
            ServiceProxies.Membership.GDSResultStatus resultStatus = response.Result as ServiceProxies.Membership.GDSResultStatus;
            if (resultStatus != null && resultStatus.GDSError != null)
            {
                if (!String.IsNullOrEmpty(resultStatus.GDSError.errorCode))
                {
                    errorCode = resultStatus.GDSError.errorCode;
                }
                if (!String.IsNullOrEmpty(resultStatus.GDSError.Value))
                {
                    errorMessage = resultStatus.GDSError.Value;
                }
            }
            throw new OWSException(errorMessage, errorCode, exceptionData);
        }

        /// <summary>
        /// Create user Exception
        /// </summary>
        /// <param name="responce">
        /// CreateUserResponse
        /// </param>
        public static void RaiseCreateUserOWSException(CreateUserResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].Value.ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Insert Guest Card Exception
        /// </summary>
        /// <param name="response">
        /// InsertGuestCardResponse
        /// </param>
        public static void RaiseInsertUserGuestCardOWSException(InsertGuestCardResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].Value.ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise CreditCard Exception
        /// </summary>
        /// <param name="response">
        /// InsertCreditCardResponse
        /// </param>
        public static void RaiseInsertCreditCardOWSException(InsertCreditCardResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].Value.ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }



        /// <summary>
        /// Raise InsertPhone Exception
        /// </summary>
        /// <param name="response">
        /// InsertPhoneResponse
        /// </param>        
        public static void RaiseInsertPhoneOWSException(InsertPhoneResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].Value.ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Insert Preference Exception
        /// </summary>
        /// <param name="responce">
        /// InsertPreferenceResponse
        /// </param>
        public static void RaiseInsertPreferenceOWSException(InsertPreferenceResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].Value.ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise InsertEmail Exception
        /// </summary>
        /// <param name="responce">
        /// InsertEmailResponse
        /// </param>        
        public static void RaiseInsertEmailOWSException(InsertEmailResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].Value.ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise InsertUpdatePrivacy Exception
        /// </summary>
        /// <param name="response"></param>
        public static void RaiseInsertUpdatePrivacyException(InsertUpdatePrivacyOptionResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].Value.ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Update Question and Answer Exception
        /// </summary>
        /// <param name="response">
        /// UpdateQuestionResponse
        /// </param>        
        public static void RaiseUpdateQuestionNAnswerOWSException(UpdateQuestionResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].Value.ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise FetchPhone Exception
        /// </summary>
        /// <param name="response">
        /// FetchPhoneListResponse
        /// </param>
        public static void RaiseFetchPhoneOWSException(FetchPhoneListResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Fetch Email Exception
        /// </summary>
        /// <param name="response">
        /// FetchEmailListResponse
        /// </param>
        public static void RaiseFetchEmailOWSException(FetchEmailListResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise FetchName Exception
        /// </summary>
        /// <param name="response">
        /// FetchNameResponse
        /// </param>        
        public static void RaiseFetchNameOWSException(FetchNameResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise FetchProfile Exception
        /// </summary>
        /// <param name="responce">
        /// FetchProfileResponse
        /// </param>        
        public static void RaiseFetchProfileOWSException(FetchProfileResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }
        /// <summary>
        /// Raise FetchAddressResponse Exception
        /// </summary>
        /// <param name="response">
        /// FetchAddressListResponse
        /// </param>
        public static void RaiseFetchAddressOWSException(FetchAddressListResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Fetch CreditCard Exception
        /// </summary>
        /// <param name="response">
        /// FetchCreditCardListResponse
        /// </param>
        public static void RaiseFetchCreditCardOWSException(FetchCreditCardListResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Fetch CreditCard Exception
        /// </summary>
        /// <param name="response">
        /// FetchCreditCardListResponse
        /// </param>
        public static void RaiseCommentException(FetchCommentListResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Update Comments Exception
        /// </summary>
        /// <param name="response"></param>
        public static void RaiseUpdateCommentException(UpdateCommentResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Insert Comments Exception
        /// </summary>
        /// <param name="response">InsertCommentResponse</param>
        public static void RaiseInsertCommentException(InsertCommentResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Fetch PreferenceList Exception
        /// </summary>
        /// <param name="response">
        /// FetchPreferenceListResponse
        /// </param>
        public static void RaisePreferenceListException(FetchPreferenceListResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Fetch Guest Card Exception
        /// </summary>
        /// <param name="response">FetchGuestCardListResponse</param>
        public static void RaiseFetchPartnerProgramException(FetchGuestCardListResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise FetchPrivacyOption Exception
        /// </summary>
        /// <param name="response">FetchPrivacyOptionResponse</param>
        public static void RaiseFetchPrivacyOptionException(FetchPrivacyOptionResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise UpdateName Exception
        /// </summary>
        /// <param name="response">
        /// UpdateNameResponse
        /// </param>
        public static void RaiseUpdateNameOWSException(UpdateNameResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].ToString();
                    }
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise UpdateAddress Exception
        /// </summary>
        /// <param name="response">
        /// UpdateAddressResponse
        /// </param>
        public static void RaiseUpdateAddressOWSException(UpdateAddressResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if ((response.Result.Text != null) && (response.Result.Text.Length > 0))
                {
                    errorMessage = response.Result.Text[0].Value.ToString();
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise UpdatePhone Exception
        /// </summary>
        /// <param name="response">UpdatePhoneResponse</param>
        public static void RaiseUpdatePhoneOWSException(UpdatePhoneResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].ToString();
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise UpdateEmail Exception
        /// </summary>
        /// <param name="response">UpdateEmailResponse</param>
        public static void RaiseUpdateEmailOWSException(UpdateEmailResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].ToString();
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// RaiseU pdateCreditCard Exception
        /// </summary>
        /// <param name="response">UpdateCreditCardResponse</param>
        public static void RaiseUpdateCreditCardOWSException(UpdateCreditCardResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].Value.ToString();
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise UpdateGuestCard Exception
        /// </summary>
        /// <param name="response">UpdateGuestCardResponse</param>
        public static void RaiseUpdateGuestCardOWSException(UpdateGuestCardResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].Value;
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// RaisePassowordUpdateOWSException
        /// </summary>
        /// <param name="response">UpdatePasswordResponse</param>
        public static void RaisePassowordUpdateOWSException(UpdatePasswordResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].Value.ToString();
                }
            }
            throw new OWSException(errorMessage);
        }


        /// <summary>
        /// Raise DeletePreferences Exception
        /// </summary>
        /// <param name="response">DeletePreferenceResponse</param>
        public static void RaiseDeletePreferencesOWSException(DeletePreferenceResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].ToString();
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Delete Phone OWS Exception
        /// </summary>
        /// <param name="response">
        /// DeletePhoneResponse
        /// </param>
        public static void RaiseDeletePhoneOWSException(DeletePhoneResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].ToString();
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Delete CreditCard OWS Exception
        /// </summary>
        /// <param name="response">
        /// DeleteCreditCardResponse
        /// </param>
        public static void RaiseDeleteCreditCardOWSException(DeleteCreditCardResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].Value.ToString();
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Delete CreditCard OWS Exception
        /// </summary>
        /// <param name="response">
        /// DeleteCreditCardResponse
        /// </param>
        public static void RaiseDeleteGuestCardOWSException(DeleteGuestCardResponse response)
        {
            string errorMessage = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].ToString();
                }
            }
            throw new OWSException(errorMessage);
        }

        /// <summary>
        /// Raise Get Currency OWS Exception
        /// </summary>
        /// <param name="response">
        /// CurrencyConverterResponse
        /// </param>
        /// <param name="exceptionData">Hashtable</param>
        public static void RaiseGetCurrencyOWSException(CurrencyConverterResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;
            if (response.Result != null)
            {
                if (response.Result.GDSError != null)
                {
                    if (response.Result.GDSError.Value != null)
                    {
                        errorMessage = response.Result.GDSError.Value;
                    }
                    if (response.Result.GDSError.errorCode != null)
                    {
                        errorCode = response.Result.GDSError.errorCode;
                    }
                }
                else if (response.Result.Text != null)
                {
                    if (response.Result.Text.Length > 0)
                    {
                        errorMessage = response.Result.Text[0].Value;
                    }
                }
                throw new OWSException(errorMessage, errorCode, exceptionData);
            }
        }

        #endregion RaiseOWSException

        #endregion Public Static Methods

        #region Private Static Methods

        private static string GetTransactionID()
        {
            byte[] tID = new Byte[2];
            Random rnd = new Random(~unchecked((int) DateTime.Now.Ticks));
            rnd.NextBytes(tID);
            return tID[0].ToString() + tID[1].ToString();
        }

        /// <summary>
        /// GetNameHeader
        /// </summary>
        /// <returns>NameHeader</returns>
        private static ServiceProxies.Name.OGHeader GetNameHeader()
        {
            ServiceProxies.Name.OGHeader soapHeader =
                new ServiceProxies.Name.OGHeader();

            soapHeader.transactionID = GetTransactionID();
            soapHeader.timeStamp = DateTime.Now;
            soapHeader.timeStampSpecified = true;

            soapHeader.Origin = new ServiceProxies.Name.EndPoint();
            soapHeader.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
            soapHeader.Origin.systemType = AppConstants.OWS_ORIGIN_SYSTEM_TYPE;

            soapHeader.Destination = new ServiceProxies.Name.EndPoint();
            soapHeader.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
            soapHeader.Destination.systemType = AppConstants.OWS_DESTINATION_SYSTEM_TYPE;

            return soapHeader;
        }

        /// <summary>
        /// GetMembershipHeader
        /// </summary>
        /// <returns>MembershipHeader</returns>
        private static ServiceProxies.Membership.OGHeader GetMembershipHeader()
        {
            ServiceProxies.Membership.OGHeader soapHeader =
                new ServiceProxies.Membership.OGHeader();

            soapHeader.transactionID = GetTransactionID();
            soapHeader.timeStamp = DateTime.Now;
            soapHeader.timeStampSpecified = true;

            soapHeader.Origin = new ServiceProxies.Membership.EndPoint();
            soapHeader.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
            soapHeader.Origin.systemType = AppConstants.OWS_ORIGIN_SYSTEM_TYPE;

            soapHeader.Destination = new ServiceProxies.Membership.EndPoint();
            soapHeader.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
            soapHeader.Destination.systemType = AppConstants.OWS_DESTINATION_SYSTEM_TYPE;

            return soapHeader;
        }

        /// <summary>
        /// GetSecurityHeader
        /// </summary>
        /// <returns>SecurityHeader</returns>
        private static ServiceProxies.Security.OGHeader GetSecurityHeader()
        {
            ServiceProxies.Security.OGHeader soapHeader =
                new ServiceProxies.Security.OGHeader();

            soapHeader.transactionID = GetTransactionID();
            soapHeader.timeStamp = DateTime.Now;
            soapHeader.timeStampSpecified = true;

            soapHeader.Origin = new ServiceProxies.Security.EndPoint();
            soapHeader.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
            soapHeader.Origin.systemType = AppConstants.OWS_ORIGIN_SYSTEM_TYPE;

            soapHeader.Destination = new ServiceProxies.Security.EndPoint();
            soapHeader.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
            soapHeader.Destination.systemType = AppConstants.OWS_DESTINATION_SYSTEM_TYPE;

            return soapHeader;
        }

        /// <summary>
        /// GetAvailabilityHeader
        /// </summary>
        /// <returns>AvailabilityHeader</returns>
        private static ServiceProxies.Availability.OGHeader GetAvailabilityHeader()
        {
            ServiceProxies.Availability.OGHeader soapHeader =
                new ServiceProxies.Availability.OGHeader();

            soapHeader.transactionID = GetTransactionID();
            soapHeader.timeStamp = DateTime.Now;
            soapHeader.timeStampSpecified = true;

            soapHeader.Origin = new ServiceProxies.Availability.EndPoint();
            soapHeader.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
            soapHeader.Origin.systemType = AppConstants.OWS_ORIGIN_SYSTEM_TYPE;

            soapHeader.Destination = new ServiceProxies.Availability.EndPoint();
            soapHeader.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
            soapHeader.Destination.systemType = AppConstants.OWS_DESTINATION_SYSTEM_TYPE;

            return soapHeader;
        }

        /// <summary>
        /// GetReservationHeader
        /// </summary>
        /// <returns>ReservationHeader</returns>
        private static ServiceProxies.Reservation.OGHeader GetReservationHeader()
        {
            ServiceProxies.Reservation.OGHeader soapHeader =
                new ServiceProxies.Reservation.OGHeader();

            soapHeader.transactionID = GetTransactionID();
            soapHeader.timeStamp = DateTime.Now;
            soapHeader.timeStampSpecified = true;

            soapHeader.Origin = new ServiceProxies.Reservation.EndPoint();
            soapHeader.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
            soapHeader.Origin.systemType = AppConstants.OWS_ORIGIN_SYSTEM_TYPE;

            soapHeader.Destination = new ServiceProxies.Reservation.EndPoint();
            soapHeader.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
            soapHeader.Destination.systemType = AppConstants.OWS_DESTINATION_SYSTEM_TYPE;

            return soapHeader;
        }

        /// <summary>
        /// GetReservationHeader
        /// </summary>
        /// <param name="isSessionBooking"></param>
        /// <returns>ReservationHeader</returns>
        private static ServiceProxies.Reservation.OGHeader GetReservationHeader
            (bool isSessionBooking)
        {
            ServiceProxies.Reservation.OGHeader soapHeader =
                new ServiceProxies.Reservation.OGHeader();

            soapHeader.transactionID = GetTransactionID();
            soapHeader.timeStamp = DateTime.Now;
            soapHeader.timeStampSpecified = true;

            if (isSessionBooking)
            {
                soapHeader.Origin = new ServiceProxies.Reservation.EndPoint();
                soapHeader.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
                soapHeader.Origin.systemType = AppConstants.OWS_SESSION_ORIGIN_SYSTEM_TYPE;

                soapHeader.Destination = new ServiceProxies.Reservation.EndPoint();
                soapHeader.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
                soapHeader.Destination.systemType = AppConstants.OWS_SESSION_DESTINATION_SYSTEM_TYPE;

                ServiceProxies.Reservation.EndPoint endPoint = null;
                soapHeader.Intermediaries = new ServiceProxies.Reservation.EndPoint[2];
                endPoint = new ServiceProxies.Reservation.EndPoint();
                soapHeader.Intermediaries[0] = endPoint;
                ServiceProxies.Reservation.EndPoint endPoint1 =
                    new ServiceProxies.Reservation.EndPoint();
                soapHeader.Intermediaries[1] = endPoint1;
                soapHeader.Intermediaries[0].systemType = AppConstants.OWS_SESSION_INTERMEDIARIES_SYSTEM_TYPE1;
                soapHeader.Intermediaries[1].systemType = AppConstants.OWS_SESSION_INTERMEDIARIES_SYSTEM_TYPE2;
            }
            else
            {
                soapHeader.Origin = new ServiceProxies.Reservation.EndPoint();
                soapHeader.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
                soapHeader.Origin.systemType = AppConstants.OWS_ORIGIN_SYSTEM_TYPE;

                soapHeader.Destination = new ServiceProxies.Reservation.EndPoint();
                soapHeader.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
                soapHeader.Destination.systemType = AppConstants.OWS_DESTINATION_SYSTEM_TYPE;
            }


            return soapHeader;
        }
        
        /// <summary>
        /// GetInformationHeader
        /// </summary>
        /// <returns>InformationHeader</returns>
        private static ServiceProxies.Information.OGHeader GetInformationHeader()
        {
            ServiceProxies.Information.OGHeader soapHeader =
                new ServiceProxies.Information.OGHeader();

            soapHeader.transactionID = GetTransactionID();
            soapHeader.timeStamp = DateTime.Now;
            soapHeader.timeStampSpecified = true;

            soapHeader.Origin = new ServiceProxies.Information.EndPoint();
            soapHeader.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
            soapHeader.Origin.systemType = AppConstants.OWS_ORIGIN_SYSTEM_TYPE;

            soapHeader.Destination = new ServiceProxies.Information.EndPoint();
            soapHeader.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
            soapHeader.Destination.systemType = AppConstants.OWS_DESTINATION_SYSTEM_TYPE;

            return soapHeader;
        }

        private static ServiceProxies.Reservation.OGHeader GetReservationHeader(bool isSessionBooking, bool isIntermediaries)
        {
            ServiceProxies.Reservation.OGHeader soapHeader = new ServiceProxies.Reservation.OGHeader();

            soapHeader.transactionID = GetTransactionID();
            soapHeader.timeStamp = DateTime.Now;
            soapHeader.timeStampSpecified = true;

            if (isSessionBooking)
            {
                soapHeader.Origin = new ServiceProxies.Reservation.EndPoint();
                soapHeader.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
                soapHeader.Origin.systemType = AppConstants.OWS_SESSION_ORIGIN_SYSTEM_TYPE;

                soapHeader.Destination = new ServiceProxies.Reservation.EndPoint();
                soapHeader.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
                soapHeader.Destination.systemType = AppConstants.OWS_SESSION_DESTINATION_SYSTEM_TYPE;

                if (isIntermediaries)
                {
                    ServiceProxies.Reservation.EndPoint endPoint = null;
                    //ToDo:Ranajit added this code and needs to review with aneesh.
                    soapHeader.Intermediaries = new ServiceProxies.Reservation.EndPoint[2];
                    endPoint = new ServiceProxies.Reservation.EndPoint();
                    soapHeader.Intermediaries[0] = endPoint;
                    ServiceProxies.Reservation.EndPoint endPoint1 = new ServiceProxies.Reservation.EndPoint();
                    soapHeader.Intermediaries[1] = endPoint1;
                    soapHeader.Intermediaries[0].systemType = AppConstants.OWS_SESSION_INTERMEDIARIES_SYSTEM_TYPE1;
                    soapHeader.Intermediaries[1].systemType = AppConstants.OWS_SESSION_INTERMEDIARIES_SYSTEM_TYPE2;
                }
            }
            else
            {
                soapHeader.Origin = new ServiceProxies.Reservation.EndPoint();
                soapHeader.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
                soapHeader.Origin.systemType = AppConstants.OWS_ORIGIN_SYSTEM_TYPE;

                soapHeader.Destination = new ServiceProxies.Reservation.EndPoint();
                soapHeader.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
                soapHeader.Destination.systemType = AppConstants.OWS_DESTINATION_SYSTEM_TYPE;
            }


            return soapHeader;
        }

        /// <summary>
        /// GetInformationHeader
        /// </summary>
        /// <returns>InformationHeader</returns>
        private static ServiceProxies.AdvancedReservationService.OGHeader GetAdvancedReservationHeader()
        {
            ServiceProxies.AdvancedReservationService.OGHeader soapHeader =
                new ServiceProxies.AdvancedReservationService.OGHeader();

            soapHeader.transactionID = GetTransactionID();
            soapHeader.primaryLangID = PaymentConstants.PrimaryLangID;
            soapHeader.timeStamp = DateTime.Now;
            soapHeader.timeStampSpecified = true;

            soapHeader.Origin = new ServiceProxies.AdvancedReservationService.EndPoint();
            soapHeader.Origin.entityID = AppConstants.OWS_ORIGIN_ENTITY_ID;
            soapHeader.Origin.systemType = AppConstants.OWS_ORIGIN_SYSTEM_TYPE;

            soapHeader.Destination = new ServiceProxies.AdvancedReservationService.EndPoint();
            soapHeader.Destination.entityID = AppConstants.OWS_DESTINATION_ENTITY_ID;
            soapHeader.Destination.systemType = AppConstants.OWS_DESTINATION_SYSTEM_TYPE;
            soapHeader.Authentication = new Scandic.Scanweb.BookingEngine.ServiceProxies.AdvancedReservationService.OGHeaderAuthentication();
            soapHeader.Authentication.UserCredentials = new Scandic.Scanweb.BookingEngine.ServiceProxies.AdvancedReservationService.OGHeaderAuthenticationUserCredentials();
            soapHeader.Authentication.UserCredentials.UserName = AppConstants.OWS_ResvAdvancedService_USER_NAME;
            soapHeader.Authentication.UserCredentials.UserPassword = AppConstants.OWS_ResvAdvancedService_USER_PASSWORD;

            return soapHeader;
        }

        #endregion Private Static Methods        

        /// <summary>
        /// RaiseIgnoreOWSException
        /// </summary>
        /// <param name="response"></param>
        /// <param name="exceptionData"></param>
        internal static void RaiseIgnoreOWSException(IgnoreBookingResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }
                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }
            else if (response.Result.Text != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].Value;
                }
            }
            if (!string.IsNullOrEmpty(errorMessage) || !string.IsNullOrEmpty(errorCode))
            {
                if(string.Equals(errorCode, OWSExceptionConstants.BOOKING_NOT_FOUND_IGNORE, StringComparison.InvariantCultureIgnoreCase))
                    throw new OWSException(errorMessage, errorCode, exceptionData, true);
                else
                    throw new OWSException(errorMessage, errorCode, exceptionData);
            }
        }

        /// <summary>
        /// RaiseConfirmOWSException
        /// </summary>
        /// <param name="response"></param>
        /// <param name="exceptionData"></param>
        internal static void RaiseConfirmOWSException(ConfirmBookingResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }
                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }
            else if (response.Result.Text != null)
            {
                if (response.Result.Text.Length > 0)
                {
                    errorMessage = response.Result.Text[0].Value;
                }
            }
            throw new OWSException(errorMessage, errorCode, exceptionData);
        }

        /// <summary>
        /// Raise Modify Booking Confirm OWSException
        /// </summary>
        /// <params>
        /// ModifyBookingResponse ,exceptionData
        /// </params>
        /// <param name="exceptionData">Hashtable</param>
        internal static void RaiseModifyBookingConfirmOWSException
            (ModifyBookingResponse response, Hashtable exceptionData)
        {
            string errorMessage = string.Empty;
            string errorCode = string.Empty;

            if (response.Result.GDSError != null)
            {
                if (response.Result.GDSError.Value != null)
                {
                    errorMessage = response.Result.GDSError.Value;
                }
                if (response.Result.GDSError.errorCode != null)
                {
                    errorCode = response.Result.GDSError.errorCode;
                }
            }
            else if (response.Result.Text != null && response.Result.Text.Length > 0)
            {
                errorMessage = string.IsNullOrEmpty(response.Result.Text[0].Value) ? string.Empty : response.Result.Text[0].Value;
            }
            else if (response.Result.OperaErrorCode != null)
            {
                if (response.Result.OperaErrorCode.Length > 0)
                {
                    errorCode = response.Result.OperaErrorCode;
                }
            }
            throw new OWSException(errorMessage, errorCode, exceptionData);
        }
    }
}