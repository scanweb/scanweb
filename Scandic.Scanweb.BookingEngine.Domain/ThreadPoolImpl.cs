using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Threading;
using Scandic.Scanweb.BookingEngine.Entity;
using Scandic.Scanweb.BookingEngine.ExceptionManager;

namespace Scandic.Scanweb.BookingEngine.Domain
{
    public sealed class ThreadPoolImpl
    {
        #region Public Methods

        #region SpawnThreadPool
        public static void SpawnThreadPool(WaitCallback Callback, ArrayList ParamList)
        {
            ThreadPool.SetMinThreads(AppConstants.MIN_WORKER_THREADS, AppConstants.MIN_IO_THREADS);
            ThreadPool.SetMaxThreads(AppConstants.MAX_WORKER_THREADS, AppConstants.MAX_IO_THREADS);

            for (int i = 0; i < ParamList.Count; i++)
            {
                ArrayList ParameterForEachCallBack = (ArrayList)ParamList[i];
                ThreadPool.QueueUserWorkItem(Callback, ParameterForEachCallBack);
            }
        }
        #endregion SpawnThreadPool

        #endregion Public Methods
    }
}
