//  Description					: Controller class for Availability OWS Calls.			  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Himansu Senapati / Santhosh Yamsani / Shankar Dasgutpa  //
//  Author email id				:                           							  //
//  Creation Date				: 05th November  2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using System.Collections;
using System.Web.Services.Protocols;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Security;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using Scandic.Scanweb.Core.Core;
using System.Web.SessionState;

namespace Scandic.Scanweb.BookingEngine.Domain
{
    /// <summary>
    /// AvailabilityDomain
    /// </summary>
    public class AvailabilityDomain : IAvailabilityDomain
    {
        #region Delegates
        public delegate void ProcessAvailabilityServiceResponse(
            AvailabilityResponse response, Hashtable availabilitySearchData, int? roomAvailability);
        #endregion

        #region GeneralAvailability

        /// <summary>
        /// GeneralAvailability
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <param name="roomSearch"></param>
        /// <param name="hotelCode"></param>
        /// <param name="chainCode"></param>
        /// <returns>AvailabilityResponse</returns>
        public AvailabilityResponse GeneralAvailability(HotelSearchEntity hotelSearchEntity,
                                                               HotelSearchRoomEntity roomSearch, string hotelCode, string chainCode)
        {
            AppLogger.LogInfoMessage("In AvailabilityDomain\\GeneralAvailability()");
            ServiceProxies.Availability.HotelReference hotelRef = new ServiceProxies.Availability.HotelReference();
            hotelRef.chainCode = chainCode;
            hotelRef.hotelCode = hotelCode;
            AvailabilityRequest request = null;
            AvailabilityResponse response = null;
            AppLogger.LogInfoMessage("Chain Code: " + chainCode + "Hotel Code: " + hotelCode);
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AvailabilityService service = OWSUtility.GetAvailabilityService();
                request = OWSRequestConverter.GetGeneralAvailabilityRequest(hotelSearchEntity, roomSearch, hotelRef);

                AppLogger.LogInfoMessage("Fetching the response: service.Availability(request)");

                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);

                response = service.Availability(request);

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();
                string currentSessionId = string.Empty;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    currentSessionId = HttpContext.Current.Session.SessionID;
                }
                string searchType = string.Empty;
                if (hotelSearchEntity != null)
                    searchType = GetSearchTypeForLogging(hotelSearchEntity);

                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = currentSessionId + ", " + request.GetType().Name + ", Sync, " + searchType + ", " + reqStartTime + ", " + responseEndTime + ", " +
                    timer.Elapsed.ToString() + ", " + System.Environment.MachineName + ", " + requestSource;

                AppLogger.CSVInfoLogger(strLog);

                //Handling Decimal Rates
                if (Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings[AppConstants.ENABLE_DECIMAL_RATES])
                    && hotelSearchEntity != null && hotelSearchEntity.SearchingType != SearchType.REDEMPTION && hotelSearchEntity.SearchingType != SearchType.VOUCHER)
                    response = ProcessDecimalPrices(response);

                AppLogger.LogInfoMessage("Received the response: service.Availability(request)");

                if (hotelSearchEntity != null && hotelSearchEntity.QualifyingType != null && hotelSearchEntity.QualifyingType.Equals(AppConstants.BLOCK_CODE_QUALIFYING_TYPE))
                {
                    if (response.AvailResponseSegments != null)
                    {
                        int count = response.AvailResponseSegments.Length;
                        for (int i = 0; i < count; i++)
                        {
                            if (response.AvailResponseSegments[i].RoomStayList[i].RoomRates[0].ratePlanCode != null)
                            {
                                Hashtable expDataMsg = new Hashtable();
                                expDataMsg.Add("RES:ResultStatusFlag", "FAIL");
                                throw new OWSException(string.Empty, OWSExceptionConstants.PROMOTION_CODE_INVALID, expDataMsg, true);
                            }
                        }
                    }
                }
                if (response.Result.resultStatusFlag != ServiceProxies.Availability.ResultStatusFlag.FAIL)
                {
                    AppLogger.LogInfoMessage("ServiceProxies.Availability.ResultStatusFlag: Success");
                    return response;
                }
                else
                {
                    AppLogger.LogInfoMessage("ServiceProxies.Availability.ResultStatusFlag: Failed");

                    AvailRequestSegment[] RequestSegment = request.AvailRequestSegment;

                    Hashtable ExceptionData = new Hashtable();
                    if (RequestSegment != null)
                    {
                        ExceptionData.Add("REQ:AvailReqType", RequestSegment[0].availReqType);
                        ExceptionData.Add("REQ:NoOfChildren", RequestSegment[0].numberOfChildren);
                        ExceptionData.Add("REQ:NumberOfRooms", RequestSegment[0].numberOfRooms);
                        ExceptionData.Add("REQ:TotalNoOfGuests", RequestSegment[0].totalNumberOfGuests);
                        ExceptionData.Add("REQ:StayDateRange", RequestSegment[0].StayDateRange);
                    }
                    if (RequestSegment[0].RatePlanCandidates != null)
                    {
                        ExceptionData.Add("REQ:PromotionCode", RequestSegment[0].RatePlanCandidates[0].promotionCode);
                        ExceptionData.Add("REQ:QualifyingIdType", RequestSegment[0].RatePlanCandidates[0].qualifyingIdType);
                        ExceptionData.Add("REQ:QualifyingIdValue", RequestSegment[0].RatePlanCandidates[0].qualifyingIdValue);
                    }
                    if (response.AvailResponseSegments != null)
                        ExceptionData.Add("RES:AvailableSegments Count", response.AvailResponseSegments.Length);
                    else
                        ExceptionData.Add("RES:AvailableSegments Count", "null");
                    ExceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                    UserNavTracker.TrackOWSRequestResponse<AvailabilityRequest, AvailabilityResponse>(request, response);
                    OWSUtility.RaiseGeneralAvailOWSException(response, ExceptionData);
                }
            }
            catch (SoapException SoapEx)
            {
                UserNavTracker.TrackOWSRequestResponse<AvailabilityRequest, AvailabilityResponse>(request, response);
                OWSUtility.RaiseOWSException(SoapEx.Message, SoapEx);
            }
            finally
            {
                request = null;
                response = null;
                timer = null;
            }
            return null;
        }

        /// <summary>
        ///  This method facilitates in creating an instance of corresponding service and makes an asynchronous webservice call.
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <param name="roomSearch"></param>
        /// <param name="hotelCode"></param>
        /// <param name="chainCode"></param>
        /// <param name="availabilityServiceRequestState"></param>
        /// <param name="isMultiDateSearch"></param>
        public void GeneralAvailability(HotelSearchEntity hotelSearchEntity,
                                                               HotelSearchRoomEntity roomSearch, string hotelCode,
                                                               string chainCode, Hashtable availabilityServiceRequestState,
                                                               bool isMultiDateSearch)
        {
            AppLogger.LogInfoMessage("In AvailabilityDomain\\GeneralAvailability()");
            HotelReference hotelRef = new HotelReference();
            hotelRef.chainCode = chainCode;
            hotelRef.hotelCode = hotelCode;

            AppLogger.LogInfoMessage("Chain Code: " + chainCode + "Hotel Code: " + hotelCode);
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AvailabilityService service = OWSUtility.GetAvailabilityService();
                AvailabilityRequest request = OWSRequestConverter.GetGeneralAvailabilityRequest(hotelSearchEntity, roomSearch, hotelRef);
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                availabilityServiceRequestState.Add(AppConstants.SERVICE_REQUEST, request);
                availabilityServiceRequestState.Add(AppConstants.IsMultiDateSearch, isMultiDateSearch);
                string requestSource = CoreUtil.GetRequestSource();
                availabilityServiceRequestState.Add(AppConstants.REQUEST_SOURCE, requestSource);
                timer.Start();
                availabilityServiceRequestState.Add(AppConstants.TIMER, timer);
                availabilityServiceRequestState.Add(AppConstants.REQUEST_START_TIME, reqStartTime);
                AppLogger.LogInfoMessage("Fetching the response: service.Availability(request)");
                service.BeginAvailability(request, GeneralAvailabilityCallback, availabilityServiceRequestState);
            }
            catch (SoapException SoapEx)
            {
                OWSUtility.RaiseOWSException(SoapEx.Message, SoapEx);
            }
        }

        /// <summary>
        /// method t round off (Ceiling) the hotel rates coming with decimals decimal.
        /// </summary>
        /// <param name="response">original response with decimal rates</param>
        /// <returns></returns>
        private static AvailabilityResponse ProcessDecimalPrices(AvailabilityResponse response)
        {
            if (response.AvailResponseSegments != null && response.AvailResponseSegments.Length > 0)
            {
                AvailResponseSegment seg = response.AvailResponseSegments[0];

                if (seg != null && seg.RoomStayList != null && seg.RoomStayList.Length > 0)
                {
                    if (seg.RoomStayList[0].RoomRates != null && seg.RoomStayList[0].RoomRates.Length > 0)
                    {
                        foreach (RoomRate rr in seg.RoomStayList[0].RoomRates)
                        {
                            if (rr.Rates != null && rr.Rates.Length > 0)
                            {
                                foreach (Scandic.Scanweb.BookingEngine.ServiceProxies.Availability.Rate r in rr.Rates)
                                {
                                    if (r.Base != null)
                                    {
                                        r.Base.Value = Math.Ceiling(r.Base.Value);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return response;
        }

        private string GetSearchTypeForLogging(HotelSearchEntity hotelSearchEntity)
        {
            string searchType = string.Empty;
            if (hotelSearchEntity != null)
            {
                if (hotelSearchEntity.SearchingType == SearchType.REGULAR && !string.IsNullOrEmpty(hotelSearchEntity.CampaignCode))
                    searchType = "PROMO-" + hotelSearchEntity.CampaignCode;
                else
                    searchType = hotelSearchEntity.SearchingType.ToString();
            }
            return searchType;
        }

        /// <summary>
        /// This method acts as a call back for availability method of availability service.
        /// </summary>
        /// <param name="asyncResult"></param>
        private void GeneralAvailabilityCallback(IAsyncResult asyncResult)
        {
            AppLogger.LogInfoMessage("Received the response: service.Availability(request)");
            AvailabilityResponse response = null;
            Hashtable availabilityServiceRequestState = null;
            int roomAvailability = -1;
            bool isMultiDateSearch = false;
            Delegate processAvailabilityServiceResponse = null;
            AvailabilityRequest request = null;
            System.Diagnostics.Stopwatch timer = null;
            bool isCitySearch = false;
            try
            {
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                AvailabilityService service = OWSUtility.GetAvailabilityService();
                availabilityServiceRequestState = (Hashtable)asyncResult.AsyncState;
                request = availabilityServiceRequestState[AppConstants.SERVICE_REQUEST] as AvailabilityRequest;
                var searchCriteria = ((Scandic.Scanweb.Entity.HotelSearchEntity)(availabilityServiceRequestState["HOTEL_SEARCH_ENTITY"]));
                string reqStartTime = availabilityServiceRequestState[AppConstants.REQUEST_START_TIME] as string;

                timer = availabilityServiceRequestState[AppConstants.TIMER] as System.Diagnostics.Stopwatch;
                timer.Stop();
                string requestSource = availabilityServiceRequestState[AppConstants.REQUEST_SOURCE] as string;

                HttpSessionState session = availabilityServiceRequestState[AppConstants.THREAD_SESSION] as HttpSessionState;
                string sessionID = session != null ? session.SessionID : string.Empty;
                string searchType = string.Empty;
                if (searchCriteria != null)
                {
                    searchType = GetSearchTypeForLogging(searchCriteria);
                    if(searchCriteria.SearchedFor !=null && searchCriteria.SearchedFor.UserSearchType== SearchedForEntity.LocationSearchType.City)
                        isCitySearch = true;
                }

                //SessionID-RequestName-RequestType-SearchType-StartTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = sessionID + ", " + request.GetType().Name + ", Async, " + searchType + ",  " + reqStartTime + ", " + responseEndTime + ", " +
                    timer.Elapsed.ToString() + ", " + System.Environment.MachineName + ", " + requestSource;

                AppLogger.CSVInfoLogger(strLog);

                isMultiDateSearch = Convert.ToBoolean(availabilityServiceRequestState[AppConstants.IsMultiDateSearch]);
                processAvailabilityServiceResponse = (Delegate)availabilityServiceRequestState[AppConstants.SEARCHSERVICE_CALLBACK];
                availabilityServiceRequestState[AppConstants.IS_PROMO_CITY_FAIL] = false;
                response = service.EndAvailability(asyncResult);

                //Handling Decimal Rates
                if (Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings[AppConstants.ENABLE_DECIMAL_RATES])
                    && searchCriteria != null && searchCriteria.SearchingType != SearchType.REDEMPTION && searchCriteria.SearchingType != SearchType.VOUCHER)
                    response = ProcessDecimalPrices(response);

                //Merchandising:R3:Display ordinary rates for unavailable promo rates 
                if (response != null && response.Result != null && response.Result.resultStatusFlag == ServiceProxies.Availability.ResultStatusFlag.FAIL
                    && response.Result.GDSError.errorCode == OWSExceptionConstants.PROMOTION_CODE_INVALID
                    && searchCriteria != null && searchCriteria.SearchedFor != null && searchCriteria.SearchedFor.UserSearchType == SearchedForEntity.LocationSearchType.City
                    && searchCriteria.SearchingType == SearchType.REGULAR && searchCriteria.CampaignCode != null)
                {
                    availabilityServiceRequestState[AppConstants.IS_PROMO_CITY_FAIL] = true;
                    service.OGHeaderValue.Origin.entityID = System.Configuration.ConfigurationSettings.AppSettings["OWS.Origin.EntityId"];
                    Scandic.Scanweb.BookingEngine.ServiceProxies.Availability.AvailRequestSegmentList availReqSegmentList = ((Scandic.Scanweb.BookingEngine.ServiceProxies.Availability.AvailRequestSegmentList)(availabilityServiceRequestState["SERVICE_REQUEST"]));
                    if (availReqSegmentList != null && availReqSegmentList.AvailRequestSegment != null && availReqSegmentList.AvailRequestSegment.Length > 0)
                    {
                        availReqSegmentList.AvailRequestSegment[0].RatePlanCandidates = null;
                        //((Scandic.Scanweb.BookingEngine.ServiceProxies.Availability.AvailRequestSegmentList)(availabilityServiceRequestState["SERVICE_REQUEST"])).AvailRequestSegment[0].RatePlanCandidates = null;
                        service.BeginAvailability(request, GeneralAvailabilityCallback, availabilityServiceRequestState);
                        UserNavTracker.TrackOWSRequestResponse<AvailabilityRequest, AvailabilityResponse>(request, response);
                    }
                    return;
                }

                availabilityServiceRequestState.Remove(AppConstants.SERVICE_REQUEST);
                if (response.Result.resultStatusFlag != ServiceProxies.Availability.ResultStatusFlag.FAIL)
                {
                    AppLogger.LogInfoMessage("ServiceProxies.Availability.ResultStatusFlag: Success");
                    roomAvailability = 1;
                }
                else
                {
                    AppLogger.LogInfoMessage("ServiceProxies.Availability.ResultStatusFlag: Failed");
                    AvailRequestSegment[] RequestSegment = request.AvailRequestSegment;

                    Hashtable ExceptionData = new Hashtable();
                    if (RequestSegment != null)
                    {
                        ExceptionData.Add("REQ:AvailReqType", RequestSegment[0].availReqType);
                        ExceptionData.Add("REQ:NoOfChildren", RequestSegment[0].numberOfChildren);
                        ExceptionData.Add("REQ:NumberOfRooms", RequestSegment[0].numberOfRooms);
                        ExceptionData.Add("REQ:TotalNoOfGuests", RequestSegment[0].totalNumberOfGuests);
                        ExceptionData.Add("REQ:StayDateRange", RequestSegment[0].StayDateRange);
                    }
                    if (RequestSegment != null && RequestSegment.Length > 0 && RequestSegment[0].RatePlanCandidates != null)
                    {
                        ExceptionData.Add("REQ:PromotionCode", RequestSegment[0].RatePlanCandidates[0].promotionCode);
                        ExceptionData.Add("REQ:QualifyingIdType", RequestSegment[0].RatePlanCandidates[0].qualifyingIdType);
                        ExceptionData.Add("REQ:QualifyingIdValue", RequestSegment[0].RatePlanCandidates[0].qualifyingIdValue);
                    }
                    if (response.AvailResponseSegments != null)
                        ExceptionData.Add("RES:AvailableSegments Count", response.AvailResponseSegments.Length);
                    else
                        ExceptionData.Add("RES:AvailableSegments Count", "null");
                    ExceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                    OWSUtility.RaiseGeneralAvailOWSException(response, ExceptionData);
                }
            }
            catch (OWSException oe)
            {
                bool isCritical = true;
                UserNavTracker.TrackOWSRequestResponse<AvailabilityRequest, AvailabilityResponse>(request, response);
                if (isMultiDateSearch || isCitySearch)
                {
                    switch (oe.ErrCode)
                    {
                        case OWSExceptionConstants.PROMOTION_CODE_INVALID:
                        case OWSExceptionConstants.PROPERTY_NOT_AVAILABLE:
                        case OWSExceptionConstants.INVALID_PROPERTY:
                        case OWSExceptionConstants.PROPERTY_RESTRICTED:
                        case OWSExceptionConstants.PRIOR_STAY:
                            isCritical = false;
                            AppLogger.LogInfoMessage(AppConstants.OWS_EXCEPTION + " \n Availability calendar - OWS Error Message: " +
                                                     oe.Message + "\n OWS Stack trace: " + oe.StackTrace);
                            break;
                        default:
                            AppLogger.LogCustomException(oe, AppConstants.OWS_EXCEPTION + "Availability calendar - OWS Exception occured");
                            break;
                    }
                }
                if (asyncResult == null)
                {
                    AppLogger.LogCustomException(oe, AppConstants.APPLICATION_EXCEPTION,
                                                     "asyncResult Object is null in - GeneralAvailabilityCallback Method - OWS exception");
                }
                else if (processAvailabilityServiceResponse == null)
                {
                    AppLogger.LogCustomException(oe, AppConstants.APPLICATION_EXCEPTION,
                                                     "processAvailabilityServiceResponse Object is null in - GeneralAvailabilityCallback Method - OWS exception");
                }
                else
                {
                    if (isCritical)
                        AppLogger.LogCustomException(oe, AppConstants.OWS_EXCEPTION, "OWS Exception occured in AvailabilityDomain - GeneralAvailabilityCallback Method");
                }
                roomAvailability = 0;
            }
            catch (SoapException SoapEx)
            {
                if (asyncResult == null)
                {
                    AppLogger.LogCustomException(SoapEx, AppConstants.APPLICATION_EXCEPTION,
                                                     "asyncResult Object is null in - GeneralAvailabilityCallback Method - Soap exception");
                }
                else if (processAvailabilityServiceResponse == null)
                {
                    AppLogger.LogCustomException(SoapEx, AppConstants.APPLICATION_EXCEPTION,
                                                     "processAvailabilityServiceResponse Object is null in - GeneralAvailabilityCallback Method - Soap exception");
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<AvailabilityRequest, AvailabilityResponse>(request, response);
                    AppLogger.LogCustomException(SoapEx, AppConstants.OWS_EXCEPTION, "Soap Exception occured in AvailabilityDomain - GeneralAvailabilityCallback Method");
                }
                roomAvailability = 0;
            }
            catch (Exception ex)
            {
                if (asyncResult == null)
                {
                    AppLogger.LogCustomException(ex, AppConstants.APPLICATION_EXCEPTION,
                                                     "asyncResult Object is null in - GeneralAvailabilityCallback Method - Generic exception");
                }
                else if (processAvailabilityServiceResponse == null)
                {
                    AppLogger.LogCustomException(ex, AppConstants.APPLICATION_EXCEPTION,
                                                     "processAvailabilityServiceResponse Object is null in - GeneralAvailabilityCallback Method - Generic exception");
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<AvailabilityRequest, AvailabilityResponse>(request, response);
                    AppLogger.LogCustomException(ex, AppConstants.APPLICATION_EXCEPTION, "General Exception occured in AvailabilityDomain - GeneralAvailabilityCallback Method");
                }
                roomAvailability = 0;
            }
            finally
            {
                if (processAvailabilityServiceResponse != null)
                    processAvailabilityServiceResponse.DynamicInvoke(response, availabilityServiceRequestState, roomAvailability);
                response = null;
                request = null;
                timer = null;
            }
        }

        /// <summary>
        /// This method facilitates in creating an instance of corresponding service and makes an asynchronous webservice call.
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <param name="roomSearch"></param>
        /// <param name="hotelCode"></param>
        /// <param name="chainCode"></param>
        /// <param name="membershipLevel"></param>
        /// <param name="availabilityServiceRequestState"></param>
        /// <param name="isMultiDateSearch"></param>
        public void RedemptionAvailability(HotelSearchEntity hotelSearchEntity,
                                                  HotelSearchRoomEntity roomSearch, string hotelCode,
                                                  string chainCode, string membershipLevel,
                                                  Hashtable availabilityServiceRequestState,
                                                  bool isMultiDateSearch)
        {
            AppLogger.LogInfoMessage("In AvailabilityDomain\\RedemptionAvailability()");
            HotelReference hotelRef = new HotelReference();
            hotelRef.chainCode = chainCode;
            hotelRef.hotelCode = hotelCode;
            AvailabilityRequest request = null;
            AppLogger.LogInfoMessage("Chain Code: " + chainCode + "Hotel Code: " + hotelCode);
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AvailabilityService service = OWSUtility.GetAvailabilityService();
                request = OWSRequestConverter.GetRedemptionAvailabilityRequest(hotelSearchEntity, roomSearch, hotelRef, membershipLevel);

                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);

                availabilityServiceRequestState.Add(AppConstants.SERVICE_REQUEST, request);
                availabilityServiceRequestState.Add(AppConstants.IsMultiDateSearch, isMultiDateSearch);
                string requestSource = CoreUtil.GetRequestSource();
                availabilityServiceRequestState.Add(AppConstants.REQUEST_SOURCE, requestSource);
                timer.Start();
                availabilityServiceRequestState.Add(AppConstants.TIMER, timer);
                availabilityServiceRequestState.Add(AppConstants.REQUEST_START_TIME, reqStartTime);

                AppLogger.LogInfoMessage("Fetching the response: service.Availability(request)");
                service.BeginAvailability(request, GeneralAvailabilityCallback, availabilityServiceRequestState);
            }
            catch (SoapException SoapEx)
            {
                OWSUtility.RaiseOWSException(SoapEx.Message, SoapEx);
            }
        }


        /// <summary>
        /// This method will does the OWS Availability Request on the given
        /// HotelSearchEntity and returns the list of rooms returned from OWS for Redemption Booking
        /// </summary>
        /// <param name="hotelSearchEntity,roomSearch,hotelCode,chainCode,membershipLevel"></param>
        /// <returns>AvailabilityResponse</returns>
        public AvailabilityResponse RedemptionAvailability(HotelSearchEntity hotelSearchEntity,
                                                                  HotelSearchRoomEntity roomSearch, string hotelCode,
                                                                  string chainCode, string membershipLevel)
        {
            AppLogger.LogInfoMessage("In AvailabilityDomain\\RedemptionAvailability()");
            ServiceProxies.Availability.HotelReference hotelRef = new ServiceProxies.Availability.HotelReference();
            hotelRef.chainCode = chainCode;
            hotelRef.hotelCode = hotelCode;
            AvailabilityRequest request = null;
            AvailabilityResponse response = null;
            AppLogger.LogInfoMessage("Chain Code: " + chainCode + "Hotel Code: " + hotelCode);
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                AvailabilityService service = OWSUtility.GetAvailabilityService();
                AppLogger.LogInfoMessage("Fetching the response: service.Availability(request)");
                request = OWSRequestConverter.GetRedemptionAvailabilityRequest(hotelSearchEntity, roomSearch, hotelRef, membershipLevel);


                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);

                response = service.Availability(request);

                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();
                string currentSessionId = string.Empty;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    currentSessionId = HttpContext.Current.Session.SessionID;
                }

                string searchType = string.Empty;
                if (hotelSearchEntity != null)
                    searchType = hotelSearchEntity.SearchingType.ToString();

                //SessionID-RequestName-RequestType-SearchType-StartTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = currentSessionId + ", " + request.GetType().Name + ", Sync, " + searchType + ",  " + reqStartTime + ", " + responseEndTime + ", " +
                    timer.Elapsed.ToString() + ", " + System.Environment.MachineName + ", " + requestSource;

                AppLogger.CSVInfoLogger(strLog);
                AppLogger.LogInfoMessage("Received the response: service.Availability(request)");
                if (response.Result.resultStatusFlag != ServiceProxies.Availability.ResultStatusFlag.FAIL)
                {
                    AppLogger.LogInfoMessage("ServiceProxies.Availability.ResultStatusFlag: Success");
                    return response;
                }
                else
                {
                    AppLogger.LogInfoMessage("ServiceProxies.Availability.ResultStatusFlag: Failed");
                    UserNavTracker.TrackOWSRequestResponse<AvailabilityRequest, AvailabilityResponse>(request, response);
                    AvailRequestSegment[] RequestSegment = request.AvailRequestSegment;

                    Hashtable ExceptionData = new Hashtable();
                    if (RequestSegment != null)
                    {
                        ExceptionData.Add("REQ:AvailReqType", RequestSegment[0].availReqType);
                        ExceptionData.Add("REQ:NoOfChildren", RequestSegment[0].numberOfChildren);
                        ExceptionData.Add("REQ:NumberOfRooms", RequestSegment[0].numberOfRooms);
                        ExceptionData.Add("REQ:TotalNoOfGuests", RequestSegment[0].totalNumberOfGuests);
                        ExceptionData.Add("REQ:StayDateRange", RequestSegment[0].StayDateRange);
                    }
                    if (RequestSegment[0].RatePlanCandidates != null)
                    {
                        ExceptionData.Add("REQ:PromotionCode", RequestSegment[0].RatePlanCandidates[0].promotionCode);
                        ExceptionData.Add("REQ:QualifyingIdType", RequestSegment[0].RatePlanCandidates[0].qualifyingIdType);
                        ExceptionData.Add("REQ:QualifyingIdValue", RequestSegment[0].RatePlanCandidates[0].qualifyingIdValue);
                    }
                    if (response.AvailResponseSegments != null)
                        ExceptionData.Add("RES:AvailableSegments Count", response.AvailResponseSegments.Length);
                    else
                        ExceptionData.Add("RES:AvailableSegments Count", "null");
                    ExceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());

                    OWSUtility.RaiseGeneralAvailOWSException(response, ExceptionData);
                }
            }
            catch (SoapException SoapEx)
            {
                UserNavTracker.TrackOWSRequestResponse<AvailabilityRequest, AvailabilityResponse>(request, response);
                OWSUtility.RaiseOWSException(SoapEx.Message, SoapEx);
            }
            finally
            {
                request = null;
                response = null;
                timer = null;
            }
            return null;
        }
        #endregion

        #region AuthenticateUser

        /// <summary>
        /// It returns the NameID or OperaID by passing login and password.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>NameID or OperaID</returns>
        public string AuthenticateUser(string userName, string password)
        {
            AuthenticateUserResponse resAuthUser = null;
            AuthenticateUserRequest reqAuthUser = null;
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                SecurityService securitySvc = OWSUtility.GetSecurityService();
                reqAuthUser = OWSRequestConverter.GetAuthenticateUserRequest(userName, password);

                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);

                ServiceProxies.Security.UniqueID operaID = null;
                resAuthUser = securitySvc.AuthenticateUser(reqAuthUser);
                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();
                string currentSessionId = string.Empty;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    currentSessionId = HttpContext.Current.Session.SessionID;
                }
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = currentSessionId + ", " + reqAuthUser.GetType().Name + ", Sync, Authenticate User, " + reqStartTime + ", " + responseEndTime + ", " +
                    timer.Elapsed.ToString() + ", " + System.Environment.MachineName + ", " + requestSource;

                AppLogger.CSVInfoLogger(strLog);
                if (resAuthUser.Result.resultStatusFlag != ServiceProxies.Security.ResultStatusFlag.FAIL)
                {
                    operaID = resAuthUser.NameID;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<AuthenticateUserRequest, AuthenticateUserResponse>(reqAuthUser, resAuthUser);
                    //throw new OWSException(resAuthUser.Result.Text[0].Value);
                    throw new OWSException();
                }

                return operaID.Value;
            }
            catch (SoapException SoapEx)
            {
                UserNavTracker.TrackOWSRequestResponse<AuthenticateUserRequest, AuthenticateUserResponse>(reqAuthUser, resAuthUser);
                throw new OWSException(SoapEx.Message, SoapEx);
            }
            finally
            {
                reqAuthUser = null;
                resAuthUser = null;
                timer = null;
            }
        }

        #endregion

        #region OWSMonitoring

        /// <summary>
        ///Method call to check the health of OWS-Availability service 
        /// </summary>
        /// <returns>True if service is up else False</returns>
        public bool AvailabilityForOWSMonitor()
        {
            AvailabilityService service = OWSUtility.GetAvailabilityService();
            ServiceProxies.Availability.HotelReference hotelRef = new ServiceProxies.Availability.HotelReference();
            hotelRef.chainCode = AppConstants.CHAIN_CODE;
            hotelRef.hotelCode = AppConstants.HOTEL_CODE;
            HotelSearchEntity hotelSearch = new HotelSearchEntity();
            SearchedForEntity searchedFor = new SearchedForEntity(AppConstants.SEARCH_STRING);
            searchedFor.SearchCode = AppConstants.SEARCH_CODE;
            searchedFor.SearchString = AppConstants.SEARCH_STRING;
            searchedFor.UserSearchType = SearchedForEntity.LocationSearchType.Hotel;
            hotelSearch.SelectedHotelCode = searchedFor.SearchCode;
            hotelSearch.ArrivalDate = DateTime.Today.AddDays(1);
            hotelSearch.DepartureDate = DateTime.Today.AddDays(2);
            hotelSearch.NoOfNights = AppConstants.ROOM_PER_NIGHT;
            hotelSearch.AdultsPerRoom = AppConstants.PER_ROOM;
            hotelSearch.ChildrenPerRoom = AppConstants.ZERO_INT;
            hotelSearch.AdultsPerRoom = AppConstants.PER_ROOM;
            hotelSearch.SearchingType = SearchType.REGULAR;

            HotelSearchRoomEntity roomSearch = new HotelSearchRoomEntity();
            roomSearch.AdultsPerRoom = AppConstants.PER_ROOM;
            roomSearch.ChildrenPerRoom = AppConstants.ZERO_INT;
            roomSearch.ChildrenOccupancyPerRoom = AppConstants.ZERO_INT;
            AvailabilityRequest request = OWSRequestConverter.GetGeneralAvailabilityRequest(hotelSearch, roomSearch, hotelRef);
            AvailabilityResponse response = service.Availability(request);

            bool result = true;
            if ((response != null)
                && (response.Result.resultStatusFlag == ServiceProxies.Availability.ResultStatusFlag.FAIL))
            {
                if ((response.Result.Text != null) && (response.Result.Text[0].Value == AppConstants.SYSTEM_ERROR))
                {
                    result = false;
                }
                else if ((response.Result.GDSError != null) && (response.Result.GDSError.Value == AppConstants.SYSTEM_ERROR))
                {
                    result = false;
                }
            }
            return result;
        }

        #endregion OWSMonitoring
    }
}