//  Description					: Request Converter class for Reservation OWS Calls.	  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Himansu Senapati / Santhosh Yamsani / Shankar Dasgutpa  //
//  Author email id				:                           							  //
//  Creation Date				: 05th November  2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using OWSAvailability = Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Information;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Membership;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Name;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Reservation;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Security;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using ServiceProxies = Scandic.Scanweb.BookingEngine.ServiceProxies;
using System.Web;


namespace Scandic.Scanweb.BookingEngine.Domain
{
    /// <summary>
    /// This class contains members of OWSRequestConverter
    /// </summary>
    public class OWSRequestConverter
    {
        #region Availability Request Getters

        /// <summary>
        /// Gets the Request Object for General availability
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="roomSearch"></param>
        /// <param name="hotelReference"></param>
        /// <returns></returns>
        public static OWSAvailability.AvailabilityRequest GetGeneralAvailabilityRequest(
            HotelSearchEntity hotelSearch,
            HotelSearchRoomEntity roomSearch, OWSAvailability.HotelReference hotelReference)
        {
            AvailRequestSegment requestSegment = new AvailRequestSegment();
            requestSegment.availReqType = AvailRequestType.Room;
            requestSegment.numberOfRoomsSpecified = true;
            requestSegment.numberOfRooms = 1;

            //if (default(long) != hotelSearch.MembershipID)
            //{
            //    requestSegment.membershipIdSpecified = true;
            //    requestSegment.membershipIdSpecified1 = true;
            //    requestSegment.membershipId = hotelSearch.MembershipID;
            //}

            requestSegment.totalNumberOfGuestsSpecified = true;
            if (roomSearch != null)
            {
                requestSegment.totalNumberOfGuests = roomSearch.AdultsPerRoom;
            }

            requestSegment.numberOfChildrenSpecified = true;

            if (roomSearch != null)
            {
                requestSegment.numberOfChildren = roomSearch.ChildrenOccupancyPerRoom;
            }

            OWSAvailability.TimeSpan dateRange = new OWSAvailability.TimeSpan();
            dateRange.StartDate = hotelSearch.ArrivalDate;
            dateRange.Item = hotelSearch.DepartureDate;
            requestSegment.StayDateRange = dateRange;

            requestSegment.HotelSearchCriteria = new HotelSearchCriterion[1];
            requestSegment.HotelSearchCriteria[0] = new HotelSearchCriterion();
            requestSegment.HotelSearchCriteria[0].HotelRef = hotelReference;

            AvailabilityRequest request = new AvailabilityRequest();
            request.summaryOnly = true;
            request.AvailRequestSegment = new AvailRequestSegment[1];
            request.AvailRequestSegment[0] = requestSegment;

            RatePlanCandidate ratePlanCandidate = new RatePlanCandidate();
            switch (hotelSearch.SearchingType)
            {
                case SearchType.REGULAR:
                    if (!string.IsNullOrEmpty(hotelSearch.CampaignCode))
                    {
                        ratePlanCandidate.promotionCode = hotelSearch.CampaignCode;
                        requestSegment.RatePlanCandidates = new RatePlanCandidate[] { ratePlanCandidate };
                    }
                    break;

                case SearchType.CORPORATE:
                    if (AppConstants.BLOCK_CODE_QUALIFYING_TYPE == hotelSearch.QualifyingType)
                    {
                        RoomStayCandidate roomStayCandidate = new RoomStayCandidate();
                        roomStayCandidate.invBlockCode = hotelSearch.CampaignCode;
                        requestSegment.RoomStayCandidates = new RoomStayCandidate[] { roomStayCandidate };
                    }
                    else
                    {
                        ratePlanCandidate.qualifyingIdType = hotelSearch.QualifyingType;
                        ratePlanCandidate.qualifyingIdValue = hotelSearch.CampaignCode;
                        requestSegment.RatePlanCandidates = new RatePlanCandidate[] { ratePlanCandidate };
                    }
                    break;

                case SearchType.VOUCHER:
                    ratePlanCandidate.promotionCode = hotelSearch.CampaignCode;
                    requestSegment.RatePlanCandidates = new RatePlanCandidate[] { ratePlanCandidate };
                    break;
            }
            return request;
        }

        /// <summary>
        /// AMS Patch7 - Naresh artf1275786 : Scanweb - Redemption availability problem
        /// This method will build the OWS Availability Request on the given
        /// HotelSearchEntity and returns the AvailabilityRequest
        /// </summary>
        /// <param name="hotelSearch,roomSearch,hotelCode,hotelReference,membershipLevel"></param>
        /// <returns>AvailabilityRequest</returns>
        public static OWSAvailability.AvailabilityRequest GetRedemptionAvailabilityRequest(
            HotelSearchEntity hotelSearch,
            HotelSearchRoomEntity roomSearch,
            OWSAvailability.HotelReference hotelReference,
            string membershipLevel)
        {
            AvailRequestSegment requestSegment = new AvailRequestSegment();
            requestSegment.availReqType = AvailRequestType.Room;
            requestSegment.numberOfRoomsSpecified = true;
            requestSegment.numberOfRooms = 1;
            requestSegment.totalNumberOfGuestsSpecified = true;
            if (roomSearch != null)
            {
                requestSegment.totalNumberOfGuests = roomSearch.AdultsPerRoom;
            }

            requestSegment.numberOfChildrenSpecified = true;
            if (roomSearch != null)
            {
                requestSegment.numberOfChildren = roomSearch.ChildrenOccupancyPerRoom;
            }

            requestSegment.roomOccupancy = 1;
            requestSegment.membershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;
            requestSegment.membershipLevel = membershipLevel;


            OWSAvailability.TimeSpan dateRange = new OWSAvailability.TimeSpan();
            dateRange.StartDate = hotelSearch.ArrivalDate;
            dateRange.Item = hotelSearch.DepartureDate;
            requestSegment.PointsDateRange = dateRange;

            requestSegment.HotelSearchCriteria = new HotelSearchCriterion[1];
            requestSegment.HotelSearchCriteria[0] = new HotelSearchCriterion();
            requestSegment.HotelSearchCriteria[0].HotelRef = hotelReference;

            AvailabilityRequest request = new AvailabilityRequest();
            request.summaryOnly = true;
            request.AvailRequestSegment = new AvailRequestSegment[1];
            request.AvailRequestSegment[0] = requestSegment;

            return request;
        }

        /// <summary>
        /// Gets rate awards for requested criteria.
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="membershipLevel"></param>
        /// <param name="regionalAvailPropList"></param>
        /// <returns></returns>
        public static ServiceProxies.Membership.FetchRateAwardsRequest GetFetchRateAwardsRequest(
            HotelSearchEntity hotelSearch,
            string membershipLevel, RegionalAvailableProperty[] regionalAvailPropList)
        {
            ServiceProxies.Membership.HotelReference[] hotelReferences =
                new ServiceProxies.Membership.HotelReference[regionalAvailPropList.Length];

            FetchRateAwardsRequest reqFetchRateAwards = new FetchRateAwardsRequest();
            reqFetchRateAwards.MembershipLevel = membershipLevel;
            reqFetchRateAwards.MembershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;

            ServiceProxies.Membership.TimeSpan timeSpan = new ServiceProxies.Membership.TimeSpan();
            timeSpan.StartDate = hotelSearch.ArrivalDate;
            timeSpan.Item = hotelSearch.DepartureDate;
            reqFetchRateAwards.StayDateRange = timeSpan;

            for (int ctr = 0; ctr < regionalAvailPropList.Length; ctr++)
            {
                hotelReferences[ctr] = new ServiceProxies.Membership.HotelReference();
                hotelReferences[ctr].chainCode = regionalAvailPropList[ctr].HotelReference.chainCode;
                hotelReferences[ctr].hotelCode = regionalAvailPropList[ctr].HotelReference.hotelCode;
            }
            reqFetchRateAwards.HotelReferences = hotelReferences;

            return reqFetchRateAwards;
        }

        /// <summary>
        /// Gets rate awards for requested criteria.
        /// </summary>
        /// <param name="hotelSearch"></param>
        /// <param name="membershipLevel"></param>
        /// <param name="hotelReferences"></param>
        /// <returns></returns>
        public static ServiceProxies.Membership.FetchRateAwardsRequest GetFetchRateAwardsRequestNew(
            HotelSearchEntity hotelSearch,
            string membershipLevel, ServiceProxies.Availability.HotelReference[] hotelReferences)
        {
            ServiceProxies.Membership.HotelReference[] fetchHotelReferences =
                new ServiceProxies.Membership.HotelReference[hotelReferences.Length];

            FetchRateAwardsRequest reqFetchRateAwards = new FetchRateAwardsRequest();
            reqFetchRateAwards.MembershipLevel = membershipLevel;
            reqFetchRateAwards.MembershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;

            ServiceProxies.Membership.TimeSpan timeSpan = new ServiceProxies.Membership.TimeSpan();
            timeSpan.StartDate = hotelSearch.ArrivalDate;
            timeSpan.Item = hotelSearch.DepartureDate;
            reqFetchRateAwards.StayDateRange = timeSpan;

            for (int ctr = 0; ctr < hotelReferences.Length; ctr++)
            {
                fetchHotelReferences[ctr] = new ServiceProxies.Membership.HotelReference();
                fetchHotelReferences[ctr].chainCode = hotelReferences[ctr].chainCode;
                fetchHotelReferences[ctr].hotelCode = hotelReferences[ctr].hotelCode;
            }
            reqFetchRateAwards.HotelReferences = fetchHotelReferences;

            return reqFetchRateAwards;
        }

        #region R1.4

        /// <summary>
        /// This method would be used to create FetchRateAwardsRequest
        /// </summary>
        /// <param name="hotelSearch">contains the hotelsearchInformation</param>
        /// <param name="membershipLevel">membershipLevel</param>
        /// <param name="hotels">contains the information about the hotels</param>
        /// <returns>FetchRateAwardsRequest</returns>
        public static ServiceProxies.Membership.FetchRateAwardsRequest GetFetchRateAwardsRequest(
            HotelSearchEntity hotelSearch,
            string membershipLevel, List<HotelDestination> hotels)
        {
            FetchRateAwardsRequest reqFetchRateAwards = new FetchRateAwardsRequest();
            reqFetchRateAwards.MembershipLevel = membershipLevel;
            reqFetchRateAwards.MembershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;

            ServiceProxies.Membership.TimeSpan timeSpan = new ServiceProxies.Membership.TimeSpan();
            timeSpan.StartDate = hotelSearch.ArrivalDate;
            timeSpan.Item = hotelSearch.DepartureDate;
            reqFetchRateAwards.StayDateRange = timeSpan;

            ServiceProxies.Membership.HotelReference[] hotelReferences
                = new ServiceProxies.Membership.HotelReference[hotels.Count];
            for (int ctr = 0; ctr < hotels.Count; ctr++)
            {
                hotelReferences[ctr] = new ServiceProxies.Membership.HotelReference();
                hotelReferences[ctr].chainCode = AppConstants.CHAIN_CODE;
                hotelReferences[ctr].hotelCode = hotels[ctr].OperaDestinationId;
            }

            reqFetchRateAwards.HotelReferences = hotelReferences;

            return reqFetchRateAwards;
        }

        #endregion R1.4

        #endregion

        #region Loyalty Request Getters

        /// <summary>
        /// This will convert the AddPromotionRequest. 
        /// </summary>
        /// <param name="membershipOperaId">Membership Opera Id</param>
        /// <param name="promoCode">Promotion Code</param>
        /// <returns>AddPromoSubscriptionRequest</returns>
        /// <remarks>Added for Release 1.5 | artf809466 | FGP � Fast Track Enrolment</remarks>
        public static ServiceProxies.Membership.AddPromoSubscriptionRequest GetPromoSubscriptionRequest(
            string membershipOperaId, string promoCode)
        {
            ServiceProxies.Membership.AddPromoSubscriptionRequest request =
                new ServiceProxies.Membership.AddPromoSubscriptionRequest();
            request.PromoSubscription = new PromotionSubscription();
            request.PromoSubscription.MembershipId = int.Parse(membershipOperaId);
            ServiceProxies.Membership.Promotion promotion =
                new ServiceProxies.Membership.Promotion();
            promotion.Code = promoCode;

            ServiceProxies.Membership.TimeSpan span = new ServiceProxies.Membership.TimeSpan();
            span.StartDate = DateTime.Now;
            promotion.Dates = span;

            request.PromoSubscription.Promotion = promotion;
            return request;
        }

        /// <summary>
        /// Get the Fetch Promo Subscription Request
        /// </summary>
        /// <param name="membershipOperaID">
        /// Membership ID
        /// </param>
        /// <returns>
        /// FetchPromoSubscriptionsRequest
        /// </returns>
        public static ServiceProxies.Membership.FetchPromoSubscriptionsRequest GetFetchPromoSubscriptionRequest(
            string membershipOperaID)
        {
            ServiceProxies.Membership.FetchPromoSubscriptionsRequest request = new FetchPromoSubscriptionsRequest();
            ServiceProxies.Membership.UniqueID fetchUniqueID =
                new ServiceProxies.Membership.UniqueID();
            fetchUniqueID.type = ServiceProxies.Membership.UniqueIDType.INTERNAL;
            fetchUniqueID.Value = membershipOperaID;
            request.MembershipId = fetchUniqueID;

            return request;
        }

        /// <summary>
        /// Method to create a request to call Name web service to register User
        /// </summary>
        /// <param name="ProfileToEnroll">User profile entity</param>
        /// <returns>This opeation returns request object to call Register Name web service</returns>
        public static ServiceProxies.Name.RegisterNameRequest GetRegisterNameRequest(UserProfileEntity ProfileToEnroll)
        {
            ServiceProxies.Name.RegisterNameRequest request = new ServiceProxies.Name.RegisterNameRequest();

            request.PersonName = new ServiceProxies.Name.PersonName();
            request.PersonName.nameTitle = new string[1];
            request.PersonName.nameTitle.SetValue(ProfileToEnroll.NameTitle, 0);
            //request.PersonName.firstName = ProfileToEnroll.FirstName;
            //request.PersonName.lastName = ProfileToEnroll.LastName;
            request.NativeName = new ServiceProxies.Name.NativeName();
            request.NativeName.languageCode = ProfileToEnroll.PreferredLanguage;
            request.Language = ProfileToEnroll.PreferredLanguage;
            //Added as part of SCANAM-537
            SetNameFields(request, ProfileToEnroll);

            request.BirthdateSpecified = true;

            if (ProfileToEnroll.DateOfBirth != null)
            {
                request.Birthdate = ProfileToEnroll.DateOfBirth.Date;
            }

            request.GenderSpecified = true;
            UserGender gender = ProfileToEnroll.Gender;
            if (gender == UserGender.MALE)
                request.Gender = ServiceProxies.Name.Gender.MALE;
            else if (gender == UserGender.FEMALE)
                request.Gender = ServiceProxies.Name.Gender.FEMALE;
            else
                request.Gender = ServiceProxies.Name.Gender.UNKNOWN;

            request.Address = new ServiceProxies.Name.Address();
            request.Address.addressType = ProfileToEnroll.AddressType;
            request.Address.AddressLine = new String[2];
            if (ProfileToEnroll.AddressLine1 != null)
            {
                request.Address.AddressLine[0] = ProfileToEnroll.AddressLine1;
            }
            if (ProfileToEnroll.AddressLine2 != null)
            {
                request.Address.AddressLine[1] = ProfileToEnroll.AddressLine2;
            }
            if (ProfileToEnroll.PostCode != null)
            {
                request.Address.postalCode = ProfileToEnroll.PostCode;
            }
            request.Address.cityName = ProfileToEnroll.City;
            request.Address.countryCode = ProfileToEnroll.Country;

            if (ProfileToEnroll.HomePhone != null &&
                ProfileToEnroll.HomePhone != string.Empty)
            {
                request.Phone = new ServiceProxies.Name.Phone();
                request.Phone.phoneType = AppConstants.PHONETYPE_HOME;
                request.Phone.Item = ProfileToEnroll.HomePhone;
                request.Phone.phoneRole = AppConstants.PHONEROLE_PHONE;
            }
            return request;
        }
        //Added as part of SCANAM-537
        public static ServiceProxies.Name.RegisterNameRequest SetNameFields(ServiceProxies.Name.RegisterNameRequest request, UserProfileEntity profile)
        {
            request.PersonName.firstName = string.Empty;
            request.PersonName.lastName = string.Empty;
            request.NativeName.firstName = profile.FirstName;
            request.NativeName.lastName = profile.LastName;
            return request;
        }

        /// <summary>
        /// Create FetchGuestCardListRequest
        /// </summary>
        /// <param name="nameId">NameID</param>
        /// <returns>FetchGuestCardListRequest</returns>
        public static ServiceProxies.Name.FetchGuestCardListRequest GetFetchGuestcardListRequest(string nameId)
        {
            FetchGuestCardListRequest reqFetchGuestcardList = new FetchGuestCardListRequest();
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();

            uniqueId.Value = nameId;
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            reqFetchGuestcardList.NameID = uniqueId;

            return reqFetchGuestcardList;
        }

        /// <summary>
        /// Create AuthenticateUserRequest
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="password">User Password</param>
        /// <returns>AuthenticateUserRequest</returns>
        public static ServiceProxies.Security.AuthenticateUserRequest GetAuthenticateUserRequest(string userName,
                                                                                           string password)
        {
            AuthenticateUserRequest request = new AuthenticateUserRequest();
            request.membershipNumber = userName;
            request.password = password;

            return request;
        }

        /// <summary>
        /// Create InsertEmail Request Entity 
        /// </summary>
        /// <param name="email">Email Id </param>
        /// <param name="nameID">NameId</param>
        /// <param name="emailType">Email type= "EMAIL" or "BUSINESSEMAIL"</param>
        /// <param name="isPrimary">"true" If its a primary email else "false"</param>
        /// <returns>InsertEmailRequest</returns>
        public static ServiceProxies.Name.InsertEmailRequest GetInsertEmailRequest(string email, string nameID,
                                                                             string emailType, bool isPrimary)
        {
            InsertEmailRequest request = new InsertEmailRequest();
            request.NameID = OWSUtility.GetNameServiceUniqueID(nameID);
            request.NameEmail = new ServiceProxies.Name.NameEmail();
            request.NameEmail.displaySequence = 1;
            request.NameEmail.primary = isPrimary;

            request.NameEmail.primarySpecified = isPrimary;
            request.NameEmail.Value = email;

            request.NameEmail.emailType = emailType;
            return request;
        }

        /// <summary>
        /// Create InsertPhone Request Entity
        /// </summary>
        /// <param name="nameID">nameId</param>
        /// <param name="phoneNumber">Phone number to be inserted</param>
        /// <param name="phoneType">Phone type</param>
        /// <param name="phoneRole">Phone Role</param>
        /// <returns>InsertPhoneRequest</returns>
        public static ServiceProxies.Name.InsertPhoneRequest GetInsertPhoneRequest(string nameID, string phoneNumber,
                                                                             string phoneType, string phoneRole)
        {
            InsertPhoneRequest request = new InsertPhoneRequest();
            request.NameID = OWSUtility.GetNameServiceUniqueID(nameID);
            request.NamePhone = new ServiceProxies.Name.NamePhone();
            request.NamePhone.phoneType = phoneType;
            request.NamePhone.phoneRole = phoneRole;
            request.NamePhone.Item = phoneNumber;
            if (phoneType == PhoneContants.PHONETYPE_MOBILE)
            {
                request.NamePhone.primary = true;
                request.NamePhone.primarySpecified = true;
                request.NamePhone.displaySequence = 1;
            }

            return request;
        }

        /// <summary>
        /// Create FetchName Request Entity 
        /// </summary>
        /// <param name="nameID">nameID</param>
        /// <returns>FetchNameRequest</returns>
        public static ServiceProxies.Name.FetchNameRequest GetFetchNameRequest(string nameID)
        {
            FetchNameRequest request = new FetchNameRequest();
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = nameID;
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            request.NameID = uniqueId;
            return request;
        }

        //Sateesh
        /// <summary>
        /// Create FetchProfile Request Entity 
        /// </summary>
        /// <param name="nameID">nameID</param>
        /// <returns>FetchProfileRequest</returns>
        public static FetchProfileRequest GetFetchProfileRequest(string nameID)
        {
            FetchProfileRequest request = new FetchProfileRequest();
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = nameID;
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            request.NameID = uniqueId;
            return request;
        }

        /// <summary>
        /// Create FetchAddressList Request Entity 
        /// </summary>
        /// <param name="nameID">nameId</param>
        /// <returns>FetchAddressListRequest</returns>
        public static ServiceProxies.Name.FetchAddressListRequest GetFetchAddressRequest(string nameID)
        {
            FetchAddressListRequest request = new FetchAddressListRequest();
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = nameID;
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            request.NameID = uniqueId;
            return request;
        }

        /// <summary>
        /// Create FetchPhoneList Request Entity
        /// </summary>
        /// <param name="nameID">nameID</param>
        /// <returns>FetchPhoneListRequest</returns>
        public static ServiceProxies.Name.FetchPhoneListRequest GetFetchPhoneRequest(string nameID)
        {
            FetchPhoneListRequest request = new FetchPhoneListRequest();
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = nameID;
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            request.NameID = uniqueId;
            return request;
        }

        /// <summary>
        /// Create FetchEmailList Request Entity
        /// </summary>
        /// <param name="nameID">nameID</param>
        /// <returns>FetchEmailListRequest</returns>
        public static ServiceProxies.Name.FetchEmailListRequest GetFetchEmailRequest(string nameID)
        {
            FetchEmailListRequest request = new FetchEmailListRequest();
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = nameID;
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            request.NameID = uniqueId;
            return request;
        }

        /// <summary>
        /// Create FetchCreditCardList Request Entity 
        /// </summary>
        /// <param name="nameID">nameID</param>
        /// <returns>FetchCreditCardListRequest</returns>
        public static ServiceProxies.Name.FetchCreditCardListRequest GetFetchCreditCardRequest(string nameID)
        {
            FetchCreditCardListRequest request = new FetchCreditCardListRequest();
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = nameID;
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            request.NameID = uniqueId;
            return request;
        }

        /// <summary>
        /// Create FetchPreferenceListRequest
        /// </summary>
        /// <param name="nameID">NameID</param>
        /// <returns>FetchPreferenceListRequest</returns>
        public static ServiceProxies.Name.FetchPreferenceListRequest GetFetchPreferenceRequest(string nameID)
        {
            FetchPreferenceListRequest request = new FetchPreferenceListRequest();
            ServiceProxies.Name.UniqueID uniqueID = new ServiceProxies.Name.UniqueID();
            uniqueID.Value = nameID;
            uniqueID.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            request.NameID = uniqueID;
            return request;
        }

        /// <summary>
        /// Create FetchCommentListRequest  
        /// </summary>
        /// <param name="nameID">NameID</param>
        /// <returns>FetchCommentListRequest</returns>
        public static ServiceProxies.Name.FetchCommentListRequest GetFetchCommentListRequest(string nameID)
        {
            FetchCommentListRequest request = new FetchCommentListRequest();
            ServiceProxies.Name.UniqueID uniqueID = new ServiceProxies.Name.UniqueID();
            uniqueID.Value = nameID;
            uniqueID.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            request.NameID = uniqueID;
            return request;
        }

        /// <summary>
        /// Create Update Name Request Entity 
        /// </summary>
        /// <param name="nameID">nameID</param>
        /// <param name="firstname">First name</param>
        /// <param name="lastname">Last Name</param>
        /// <returns>UpdateNameRequest</returns>
        public static ServiceProxies.Name.UpdateNameRequest GetUpdateNameRequest(string nameID, UserProfileEntity profile)
        {
            UpdateNameRequest request = new UpdateNameRequest();
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = nameID;
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            request.NameID = uniqueId;
            ServiceProxies.Name.PersonName personName = new ServiceProxies.Name.PersonName();

            personName.nameTitle = new string[1];
            personName.nameTitle[0] = profile.NameTitle;
            personName.firstName = profile.FirstName;
            personName.lastName = profile.LastName;

            request.PersonName = personName;

            request.GenderSpecified = true;
            UserGender userGender = profile.Gender;
            if (userGender == UserGender.MALE)
                request.Gender = ServiceProxies.Name.Gender.MALE;
            else if (userGender == UserGender.FEMALE)
                request.Gender = ServiceProxies.Name.Gender.FEMALE;
            else
                request.Gender = ServiceProxies.Name.Gender.UNKNOWN;

            request.NativeName = new ServiceProxies.Name.NativeName();
            request.NativeName.languageCode = profile.PreferredLanguage;
            if (profile.DateOfBirth != null)
            {
                DateTime DOB = new DateTime(profile.DateOfBirth.Year, profile.DateOfBirth.Month, profile.DateOfBirth.Day);
                request.Birthdate = DOB;
                request.BirthdateSpecified = true;
            }
            return request;
        }

        /// <summary>
        /// Create Update PreferredLanguage Request Entity. This method is added to Update the Preferred language selected by the user on Registration,
        /// UpdateProfile and Enrollment section of bookingdetails page. This method needs to be modified when Scorpio is upgraded and there is no denpendency
        /// on Preferred language field with Firstname, lastname, DOB, Gender, PrimaryLanguageID and Title fields.
        /// </summary>
        /// <param name="nameID">nameID</param>
        /// <param name="firstname">First name</param>
        /// <param name="lastname">Last Name</param>
        /// <param name="preferredLanguage">Preferred Language</param>
        /// <returns>UpdatePreferredLanguageRequest</returns>
        public static ServiceProxies.Name.UpdateNameRequest GetUpdatePreferredLanguageRequest(string nameID, UserProfileEntity profile, FetchNameResponse response)
        {
            UpdateNameRequest request = new UpdateNameRequest();
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = nameID;
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            request.NameID = uniqueId;
            ServiceProxies.Name.PersonName personName = new ServiceProxies.Name.PersonName();

            personName.nameTitle = new string[1];
            personName.nameTitle[0] = profile.NameTitle;
            personName.firstName = response.PersonName.firstName;
            personName.lastName = response.PersonName.lastName;
            request.PersonName = personName;

            request.Language = profile.PreferredLanguage;

            if (response.NativeName != null)
            {
                request.NativeName = response.NativeName;
                request.NativeName.languageCode = profile.PreferredLanguage;
            }
            else
            {
                request.NativeName = new ServiceProxies.Name.NativeName();
                request.NativeName.languageCode = profile.PreferredLanguage;
            }

            request.Birthdate = response.Birthdate;
            request.BirthdateSpecified = response.BirthdateSpecified;

            request.Gender = response.Gender;
            request.GenderSpecified = response.GenderSpecified;

            return request;
        }

        /// <summary>
        /// Get the Update Comment Request
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="commentEntity"></param>
        /// <returns></returns>
        public static ServiceProxies.Name.UpdateCommentRequest GetUpdateCommentRequest(string nameId,
                                                                                 CommentsEntity commentEntity)
        {
            UpdateCommentRequest request = null;
            if (commentEntity != null)
            {
                request = new UpdateCommentRequest();

                ServiceProxies.Name.UniqueID uniqueID = new ServiceProxies.Name.UniqueID();
                uniqueID.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
                uniqueID.Value = nameId;
                request.NameID = uniqueID;

                ServiceProxies.Name.Comment comment = new ServiceProxies.Name.Comment();
                comment.commentTitle = AppConstants.RESERVATION_COMMENT_TITLE;
                ServiceProxies.Name.Text commentText = new ServiceProxies.Name.Text();
                commentText.Value = commentEntity.Comment;
                comment.operaId = commentEntity.CommentOperaId;
                comment.operaIdSpecified = true;
                comment.Item = commentText;
                comment.ItemElementName = ServiceProxies.Name.ItemChoiceType.Text;
                request.Comment = comment;
            }
            return request;
        }

        /// <summary>
        /// Get the Update Comment Request
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="commentEntity"></param>
        /// <returns></returns>
        public static ServiceProxies.Name.InsertCommentRequest GetInsertCommentRequest(string nameId,
                                                                                 CommentsEntity commentEntity)
        {
            InsertCommentRequest request = null;
            if (commentEntity != null)
            {
                request = new InsertCommentRequest();

                ServiceProxies.Name.UniqueID uniqueID = new ServiceProxies.Name.UniqueID();
                uniqueID.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
                uniqueID.Value = nameId;
                request.NameID = uniqueID;

                ServiceProxies.Name.Comment comment = new ServiceProxies.Name.Comment();
                comment.commentTitle = AppConstants.RESERVATION_COMMENT_TITLE;

                ServiceProxies.Name.TextElement[] textElements = new ServiceProxies.Name.TextElement[1];
                textElements[0] = new ServiceProxies.Name.TextElement();
                textElements[0].Value = commentEntity.Comment;
                comment.Item = textElements;

                comment.ItemElementName = ServiceProxies.Name.ItemChoiceType.Text;

                request.Comment = comment;
            }
            return request;
        }

        /// <summary>
        /// Create the UpdateAddress Request Entity
        /// </summary>
        /// <param name="profile">
        /// UserProfileEntity
        /// </param>
        /// <returns>
        /// UpdateAddressRequest
        /// </returns>
        public static ServiceProxies.Name.UpdateAddressRequest GetUpdateAddressRequest(UserProfileEntity profile)
        {
            UpdateAddressRequest request = new UpdateAddressRequest();
            ServiceProxies.Name.NameAddress nameAddress = new ServiceProxies.Name.NameAddress();

            nameAddress.operaId = profile.AddressOperaID;
            nameAddress.operaIdSpecified = true;
            //Set the Address Lines
            string[] addressLines = new string[2];
            addressLines[0] = profile.AddressLine1;
            addressLines[1] = profile.AddressLine2;

            nameAddress.AddressLine = addressLines;
            nameAddress.countryCode = profile.Country;
            nameAddress.cityName = profile.City;
            nameAddress.postalCode = profile.PostCode;
            nameAddress.primary = true;
            nameAddress.primarySpecified = true;
            nameAddress.displaySequence = 1;
            request.NameAddress = nameAddress;

            return request;
        }

        /// <summary>
        /// Create the Insert Address Request Entity
        /// </summary>
        /// <param name="profile">
        /// UserProfileEntity
        /// </param>
        /// <returns>
        /// InsertAddressRequest
        /// </returns>
        /// <remarks>
        /// Fix for the artifact (Artf713949)
        /// </remarks>
        public static ServiceProxies.Name.InsertAddressRequest GetInsertAddressRequest(string nameID,
                                                                                 UserProfileEntity profile)
        {
            InsertAddressRequest request = new InsertAddressRequest();

            ServiceProxies.Name.NameAddress nameAddress = new ServiceProxies.Name.NameAddress();

            string[] addressLines = new string[2];
            addressLines[0] = profile.AddressLine1;
            addressLines[1] = profile.AddressLine2;

            nameAddress.AddressLine = addressLines;
            nameAddress.countryCode = profile.Country;
            nameAddress.cityName = profile.City;
            nameAddress.postalCode = profile.PostCode;
            nameAddress.primary = true;
            nameAddress.primarySpecified = true;
            nameAddress.displaySequence = 1;
            nameAddress.addressType = profile.AddressType;
            request.NameAddress = nameAddress;

            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = nameID;
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            request.NameID = uniqueId;

            return request;
        }


        /// <summary>
        /// Create Update Phone Request Entity 
        /// </summary>
        /// <param name="phOperaID">Phone Opera ID</param>
        /// <param name="phNumber">Phone Number</param>
        /// <param name="phType">Phone type</param>
        /// <returns>UpdatePhoneRequest</returns>
        public static ServiceProxies.Name.UpdatePhoneRequest GetUpdatePhoneRequest(long phOperaID, string phNumber,
                                                                             string phType, string phRole)
        {
            UpdatePhoneRequest request = new UpdatePhoneRequest();
            if (phOperaID != 0)
            {
                ServiceProxies.Name.NamePhone phone = new ServiceProxies.Name.NamePhone();
                phone.phoneType = phType;
                phone.operaId = phOperaID;
                phone.operaIdSpecified = true;
                if (phType == PhoneContants.PHONETYPE_MOBILE)
                {
                    phone.primary = true;
                    phone.primarySpecified = true;
                }
                phone.Item = phNumber;
                phone.phoneRole = phRole;
                request.NamePhone = phone;
            }
            return request;
        }

        /// <summary>
        /// Create Update Email Request Entity
        /// </summary>
        /// <param name="emailOperaID">Email Opera ID</param>
        /// <param name="email">Email</param>
        /// <param name="isPrimary">"true" If its a primary email else "false"</param>
        /// <returns>UpdateEmailRequest</returns>
        public static ServiceProxies.Name.UpdateEmailRequest GetUpdateEmailRequest(long emailOperaID, string email,
                                                                             bool isPrimary)
        {
            UpdateEmailRequest request = new UpdateEmailRequest();
            if (emailOperaID != 0)
            {
                ServiceProxies.Name.NameEmail Email = new ServiceProxies.Name.NameEmail();
                Email.operaId = emailOperaID;
                Email.operaIdSpecified = true;
                Email.Value = email;

                if (isPrimary)
                {
                    Email.primary = true;
                    Email.displaySequence = 1;
                }
                request.NameEmail = Email;
            }
            return request;
        }

        /// <summary>
        /// Inserting a new credit card for an user.
        /// </summary>
        /// <param name="holderName"></param>
        /// <param name="cardType"></param>
        /// <param name="cardCode"></param>
        /// <param name="cardNumber"></param>
        /// <param name="expDate"></param>
        /// <returns></returns>
        public static ServiceProxies.Name.InsertCreditCardRequest GetInsertCreditCardRequest
            (string nameID, string holderName, string cardType,
             string cardCode, string cardNumber, DateTime expDate)
        {
            InsertCreditCardRequest request = new InsertCreditCardRequest();

            request.NameID = OWSUtility.GetNameServiceUniqueID(nameID);

            ServiceProxies.Name.NameCreditCard creditCard = new ServiceProxies.Name.NameCreditCard();
            creditCard.cardHolderName = holderName;
            creditCard.cardType = cardType;
            creditCard.cardCode = cardCode;
            creditCard.cardNumber = cardNumber;
            creditCard.expirationDate = expDate;
            creditCard.primary = true;
            creditCard.primarySpecified = true;
            creditCard.displaySequence = 1;
            creditCard.displaySequenceSpecified = true;

            request.NameCreditCard = creditCard;

            return request;
        }

        /// <summary>
        /// Create Update Credit Card Request Entity
        /// </summary>
        /// <param name="cardOperaID">Opera Id for updating credit card</param>
        /// <param name="holderName">Holder Name</param>
        /// <param name="cardType">Card Type</param>
        /// <param name="cardNumber">Card Number</param>
        /// <param name="expDate">Expiry Date</param>
        /// <returns>UpdateCreditCardRequest</returns>
        public static ServiceProxies.Name.UpdateCreditCardRequest GetUpdateCreditCardRequest
            (long cardOperaID, string holderName, string cardType, string cardCode,
             string cardNumber, DateTime expDate, int displaySequence, bool isPrimary)
        {
            UpdateCreditCardRequest request = new UpdateCreditCardRequest();
            if (cardOperaID != 0)
            {
                ServiceProxies.Name.NameCreditCard creditCard =
                    new ServiceProxies.Name.NameCreditCard();
                creditCard.operaId = cardOperaID;
                creditCard.operaIdSpecified = true;
                creditCard.cardHolderName = holderName;
                creditCard.cardNumber = cardNumber;
                creditCard.cardType = cardType;
                creditCard.cardCode = cardCode;
                creditCard.expirationDate = expDate;
                creditCard.primary = isPrimary;
                creditCard.primarySpecified = true;
                creditCard.displaySequence = displaySequence;
                creditCard.displaySequenceSpecified = true;
                request.NameCreditCard = creditCard;
            }
            return request;
        }

        /// <summary>
        /// Create DeleteCreditCardRequest for the specified opera ID
        /// </summary>
        /// <param name="nameId">
        /// Opera ID for the request
        /// </param>
        /// <returns>
        /// DeleteCreditCardRequest
        /// </returns>
        public static ServiceProxies.Name.DeleteCreditCardRequest GetDeleteCreditCardRequest(long operaID)
        {
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = operaID.ToString();
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;

            ServiceProxies.Name.DeleteCreditCardRequest request = new DeleteCreditCardRequest();
            request.CreditCardID = uniqueId;
            return request;
        }

        /// <summary>
        /// Create Update Guest Card Request
        /// </summary>
        /// <param name="cardOperaID">Member card Opera ID</param>
        /// <param name="membershipType">Membership Type</param>
        /// <param name="membershipNumber">Membership Number</param>
        /// <param name="membershipLevel">Membership Level</param>
        /// <param name="memberName">Member Name</param>
        /// <returns>UpdateGuestCardRequest</returns>
        public static ServiceProxies.Name.UpdateGuestCardRequest GetUpdateGuestcardRequest(long cardOperaID,
                                                                                     string membershipType,
                                                                                     string membershipNumber,
                                                                                     string memberName,
                                                                                     int displaySequence)
        {
            UpdateGuestCardRequest request = new UpdateGuestCardRequest();
            if (cardOperaID != 0)
            {
                ServiceProxies.Name.NameMembership guestCard =
                    new ServiceProxies.Name.NameMembership();
                guestCard.operaId = cardOperaID;
                guestCard.operaIdSpecified = true;
                guestCard.memberName = memberName;
                guestCard.membershipType = membershipType;
                guestCard.membershipNumber = membershipNumber;
                guestCard.displaySequence = displaySequence;
                guestCard.displaySequenceSpecified = true;
                request.NameMembership = guestCard;
            }
            return request;
        }

        /// <summary>
        /// Delete the partner program cards.
        /// </summary>
        /// <param name="guestCardOperaId">Opera id for this guest card.</param>
        /// <returns>DeleteGuestCardRequest</returns>
        /// <remarks>Release 1.5 | artf799586 | FGP - Enrollment Partnerprogram list</remarks>
        public static ServiceProxies.Name.DeleteGuestCardRequest DeleteGuestCardRequest(string guestCardOperaId)
        {
            DeleteGuestCardRequest request = new DeleteGuestCardRequest();
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = guestCardOperaId;
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            request.GuestCardID = uniqueId;
            return request;
        }

        public static ServiceProxies.Security.UpdatePasswordRequest GetUpdatePasswordRequest(string MembershipNumber,
                                                                                       string OldPassword,
                                                                                       string NewPassword)
        {
            UpdatePasswordRequest request = new UpdatePasswordRequest();
            request.membershipNumber = MembershipNumber;
            request.oldPassword = OldPassword;
            request.newPassword = NewPassword;

            return request;
        }

        /// <summary>
        /// Create Delete Preference request
        /// </summary>
        /// <param name="nameID">Name ID</param>
        /// <param name="preferenceType">Preference Type</param>
        /// <param name="preferenceValue">Preference value</param>
        /// <returns>DeletePreferenceRequest</returns>
        public static ServiceProxies.Name.DeletePreferenceRequest GetDeletePreferenceRequest(string nameID,
                                                                                       string preferenceType,
                                                                                       string preferenceValue)
        {
            DeletePreferenceRequest request = new DeletePreferenceRequest();
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = nameID;
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;
            request.NameID = uniqueId;
            ServiceProxies.Name.Preference preference = new ServiceProxies.Name.Preference();

            preference.preferenceType = preferenceType;
            preference.preferenceValue = preferenceValue;

            request.Preference = preference;
            return request;
        }

        /// <summary>
        /// Create DeletePhoneRequest for the specified opera ID
        /// </summary>
        /// <param name="nameId">
        /// Opera ID for the request
        /// </param>
        /// <returns>
        /// DeletePhoneRequest
        /// </returns>
        public static ServiceProxies.Name.DeletePhoneRequest GetDeletePhoneRequest(long operaID)
        {
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = operaID.ToString();
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;

            ServiceProxies.Name.DeletePhoneRequest request = new DeletePhoneRequest();
            request.PhoneID = uniqueId;
            return request;
        }

        /// <summary>
        /// Create DeleteGuestCardRequest for the specified opera ID
        /// </summary>
        /// <param name="nameId">
        /// Opera ID for the request
        /// </param>
        /// <returns>
        /// DeleteGuestCardRequest
        /// </returns>
        public static ServiceProxies.Name.DeleteGuestCardRequest GetDeleteGuestCardDetailsRequest(long operaID)
        {
            ServiceProxies.Name.UniqueID uniqueId = new ServiceProxies.Name.UniqueID();
            uniqueId.Value = operaID.ToString();
            uniqueId.type = ServiceProxies.Name.UniqueIDType.INTERNAL;

            ServiceProxies.Name.DeleteGuestCardRequest request = new DeleteGuestCardRequest();
            request.GuestCardID = uniqueId;
            return request;
        }

        #endregion

        #region Reservation Request Getters

        /// <summary>
        /// Create GetFetchBooking Request
        /// </summary>
        /// <param name="confirmationNumber">
        /// Booking Confirmation Number
        /// </param>
        /// <returns>
        /// FetchBookingRequest
        /// </returns>
        public static FetchBookingRequest GetFetchBookingRequest(string confirmationNumber)
        {
            FetchBookingRequest request = new FetchBookingRequest();
            ServiceProxies.Reservation.UniqueID uniqueID = new ServiceProxies.Reservation.UniqueID();
            uniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            uniqueID.Value = confirmationNumber;
            request.ConfirmationNumber = uniqueID;
            return request;
        }

        /// <summary>
        /// Get CreateBooking  Request
        /// </summary>
        /// <param name="hotelSearch">HotelSearch Entity</param>
        /// <param name="roomSearch">The room search.</param>
        /// <param name="roomRate">HotelRoomRate Entity</param>
        /// <param name="guestInformation">GuestInformation Entity</param>
        /// <param name="reservationNumber">The reservation number.</param>
        /// <param name="sourceCode">The source code.</param>
        /// <param name="partnerID">The partner ID.</param>
        /// <returns>
        /// CreateBookingRequest
        /// </returns>
        public static CreateBookingRequest GetBookingRequest(HotelSearchEntity hotelSearch,
                                                             HotelSearchRoomEntity roomSearch,
                                                             HotelRoomRateEntity roomRate,
                                                             GuestInformationEntity guestInformation,
                                                             string reservationNumber, string sourceCode,
                                                             string partnerID, bool isSessionBooking)
        {
            CreateBookingRequest request = null;
            if ((hotelSearch != null) && (roomRate != null) && (guestInformation != null))
            {
                request = new CreateBookingRequest();
                HotelReservation reservation = null;
                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                {
                    reservation = GetRedemptionBooking(hotelSearch, roomSearch, roomRate, guestInformation,
                                                       reservationNumber, sourceCode, BookingFlow.Booking);
                }
                else
                {
                    reservation = GetHotelReservation(hotelSearch, roomSearch, roomRate, guestInformation,
                                                      reservationNumber, sourceCode, BookingFlow.Booking, partnerID);
                }
                reservation.reservationAction = ReservationActionType.ADD;
                reservation.reservationActionSpecified = true;
                if (isSessionBooking && HttpContext.Current != null && HttpContext.Current.Request != null
                    && !string.IsNullOrEmpty(HttpContext.Current.Request.Headers["X-Forwarded-For"]))
                {
                    var arrayIP = HttpContext.Current.Request.Headers["X-Forwarded-For"].Split(new char[] { ',' });
                    if (arrayIP.Length > 0)
                    {
                        var clientIP = arrayIP[0];
                        reservation.UserDefinedValues = new Scandic.Scanweb.BookingEngine.ServiceProxies.Reservation.UserDefinedValue[1];
                        reservation.UserDefinedValues[0] = new Scandic.Scanweb.BookingEngine.ServiceProxies.Reservation.UserDefinedValue();
                        reservation.UserDefinedValues[0].Item = clientIP;
                        reservation.UserDefinedValues[0].valueName = "ip_address";
                    }
                }
                request.HotelReservation = reservation;
            }
            return request;
        }

        /// <summary>
        /// Get ModifyBooking Request
        /// </summary>
        /// <param name="hotelSearch">
        /// HotelSearch Entity
        /// </param>
        /// <param name="roomRate">
        /// HotelRoomRate Entity
        /// </param>
        /// <param name="guestInformation">
        /// GuestInformation Entity
        /// </param>
        /// <returns>
        /// ModifyBookingRequest
        /// </returns>
        public static ModifyBookingRequest GetModifyBookingRequest(HotelSearchEntity hotelSearch,
                                                                   HotelSearchRoomEntity roomSearch,
                                                                   HotelRoomRateEntity roomRate,
                                                                   GuestInformationEntity guestInformation,
                                                                   string prevReservationNumber, string sourceCode)
        {
            ModifyBookingRequest request = null;
            if ((hotelSearch != null) && (roomRate != null) && (guestInformation != null))
            {
                request = new ModifyBookingRequest();

                HotelReservation reservation = GetHotelReservation(hotelSearch, roomSearch, roomRate, guestInformation,
                                                                   string.Empty, sourceCode, BookingFlow.ModifyBooking,
                                                                   string.Empty);
                reservation.reservationAction = ReservationActionType.EDIT;
                reservation.reservationActionSpecified = true;

                ServiceProxies.Reservation.UniqueID uniqueIDReservationNumber = new ServiceProxies.Reservation.UniqueID();
                uniqueIDReservationNumber.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
                uniqueIDReservationNumber.Value = prevReservationNumber;

                ServiceProxies.Reservation.UniqueID uniqueIDLegNumber = new ServiceProxies.Reservation.UniqueID();
                uniqueIDLegNumber.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
                uniqueIDLegNumber.source = "LEGNUMBER";
                uniqueIDLegNumber.Value = guestInformation.LegNumber;

                ServiceProxies.Reservation.UniqueID[] uniqueIDList = new ServiceProxies.Reservation.UniqueID[2];
                uniqueIDList[0] = uniqueIDReservationNumber;
                uniqueIDList[1] = uniqueIDLegNumber;
                reservation.UniqueIDList = uniqueIDList;
                request.HotelReservation = reservation;
            }
            return request;
        }

        public static ModifyBookingRequest GetModifyGuaranteeBookingRequest(HotelSearchEntity hotelSearch,
                                                                          HotelSearchRoomEntity roomSearch,
                                                                          HotelRoomRateEntity roomRate,
                                                                          GuestInformationEntity guestInformation,
                                                                          string prevReservationNumber, string sourceCode)
        {
            ModifyBookingRequest request = null;
            if ((hotelSearch != null) && (roomRate != null) && (guestInformation != null))
            {
                request = new ModifyBookingRequest();

                HotelReservation reservation = GetGuaranteForHotelReservation(hotelSearch, roomSearch, roomRate, guestInformation,
                                                                   string.Empty, sourceCode, BookingFlow.ModifyBooking,
                                                                   string.Empty);
                reservation.reservationAction = ReservationActionType.EDIT;
                reservation.reservationActionSpecified = true;

                ServiceProxies.Reservation.UniqueID uniqueIDReservationNumber = new ServiceProxies.Reservation.UniqueID();
                uniqueIDReservationNumber.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
                uniqueIDReservationNumber.Value = prevReservationNumber;

                ServiceProxies.Reservation.UniqueID uniqueIDLegNumber = new ServiceProxies.Reservation.UniqueID();
                uniqueIDLegNumber.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
                uniqueIDLegNumber.source = "LEGNUMBER";
                uniqueIDLegNumber.Value = guestInformation.LegNumber;

                ServiceProxies.Reservation.UniqueID[] uniqueIDList = new ServiceProxies.Reservation.UniqueID[2];
                uniqueIDList[0] = uniqueIDReservationNumber;
                uniqueIDList[1] = uniqueIDLegNumber;
                reservation.UniqueIDList = uniqueIDList;
                request.HotelReservation = reservation;
            }
            return request;
        }
        ///<summary>
        /// Returns the request object to validate FGPNumber
        /// Author : Ruman Khan
        /// </summary>
        /// <param name="lastName">Last Name of the member</param>
        /// <param name="membershipNo">Membership Number of the User</param>
        /// <param name="membershipType">Membership Type</param>
        /// <returns>NameLookupRequest object</returns>
        public static NameLookupRequest GetFGPNumberValidateRequest(string lastName, string membershipNo,
                                                                    string membershipType, bool IsAlternameRequest)
        {
            NameLookupRequest nameRequest = new NameLookupRequest();
            ServiceProxies.Name.NameLookupCriteriaMembership nameLkpCriteria =
                new ServiceProxies.Name.NameLookupCriteriaMembership();

            if (IsAlternameRequest)
            {
                nameLkpCriteria.NativeName = new Scandic.Scanweb.BookingEngine.ServiceProxies.Name.NativeName();
                nameLkpCriteria.NativeName.lastName = lastName;
            }
            else
            {
                nameLkpCriteria.LastName = lastName;
            }

            nameLkpCriteria.MembershipNumber = membershipNo;
            nameLkpCriteria.MembershipType = membershipType;



            nameRequest.NameLookupCriteria = new ServiceProxies.Name.NameLookupInput();
            nameRequest.NameLookupCriteria.Item = nameLkpCriteria;

            return nameRequest;
        }

        /// <summary>
        /// Get the CancelBooking Request
        /// </summary>
        /// <param name="cancelDetailsEntity">
        /// CancelDetailsEntity
        /// </param>
        /// <returns>
        /// CancelBookingRequest
        /// </returns>
        public static CancelBookingRequest GetCancelBookingRequest(CancelDetailsEntity cancelDetailsEntity)
        {
            CancelBookingRequest request = new CancelBookingRequest();

            ServiceProxies.Reservation.UniqueID uniqueID = new ServiceProxies.Reservation.UniqueID();
            uniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            uniqueID.Value = cancelDetailsEntity.ReservationNumber;
            request.ConfirmationNumber = uniqueID;

            ServiceProxies.Reservation.UniqueID uniqueLegID = new ServiceProxies.Reservation.UniqueID();
            uniqueLegID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            uniqueLegID.Value = cancelDetailsEntity.LegNumber;
            request.LegNumber = uniqueLegID;

            ServiceProxies.Reservation.HotelReference hotelReference = new ServiceProxies.Reservation.HotelReference();
            hotelReference.chainCode = cancelDetailsEntity.ChainCode;
            hotelReference.hotelCode = cancelDetailsEntity.HotelCode;
            request.HotelReference = hotelReference;

            request.CancelTerm = new ServiceProxies.Reservation.CancelTerm();
            request.CancelTerm.cancelDate = DateTime.Today;
            request.CancelTerm.cancelDateSpecified = true;

            ServiceProxies.Reservation.Text reason = new ServiceProxies.Reservation.Text();
            reason.Value = "Plans Changed to different hotel";
            object[] items = new object[1];
            items[0] = reason;

            request.CancelTerm.CancelReason = new ServiceProxies.Reservation.Paragraph();
            request.CancelTerm.CancelReason.Items = items;
            ServiceProxies.Reservation.ItemsChoiceType[] itemsChoice = new ServiceProxies.Reservation.ItemsChoiceType[1];
            itemsChoice[0] = ServiceProxies.Reservation.ItemsChoiceType.Text;
            request.CancelTerm.CancelReason.ItemsElementName = itemsChoice;

            switch (cancelDetailsEntity.CancelType)
            {
                case Scandic.Scanweb.Entity.CancelTermType.Cancel:
                    {
                        request.CancelTerm.cancelType = ServiceProxies.Reservation.CancelTermType.Cancel;
                        break;
                    }
                case Scandic.Scanweb.Entity.CancelTermType.NoShow:
                    {
                        request.CancelTerm.cancelType = ServiceProxies.Reservation.CancelTermType.NoShow;
                        break;
                    }
                case Scandic.Scanweb.Entity.CancelTermType.Other:
                    {
                        request.CancelTerm.cancelType = ServiceProxies.Reservation.CancelTermType.Other;
                        break;
                    }
            }
            return request;
        }

        /// <summary>
        /// Get the Hotel Reservation
        /// </summary>
        /// <param name="hotelSearch">Hotel search entity</param>
        /// <param name="roomSearch">The room search.</param>
        /// <param name="roomRate">Room rate entity</param>
        /// <param name="guestInformation">GuestprogramInformation</param>
        /// <param name="reservationNumber">Reservation number</param>
        /// <param name="sourceCode">Source code</param>
        /// <param name="flow">Identify which flow is this</param>
        /// <param name="partnerID">The partner ID.</param>
        /// <returns>
        /// Return new Hotel reservation
        /// </returns>        
        private static HotelReservation GetHotelReservation(HotelSearchEntity hotelSearch,
                                                            HotelSearchRoomEntity roomSearch,
                                                            HotelRoomRateEntity roomRate,
                                                            GuestInformationEntity guestInformation,
                                                            string reservationNumber, string sourceCode,
                                                            BookingFlow flow, string partnerID)
        {
            HotelReservation reservation = null;
            if ((hotelSearch != null) && (roomSearch != null) && (roomRate != null) && (guestInformation != null))
            {
                reservation = new HotelReservation();

                if (!string.IsNullOrEmpty(reservationNumber))
                {
                    ServiceProxies.Reservation.UniqueID uniqueID = new ServiceProxies.Reservation.UniqueID();
                    uniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
                    uniqueID.Value = reservationNumber;

                    reservation.UniqueIDList = new ServiceProxies.Reservation.UniqueID[1];
                    reservation.UniqueIDList[0] = uniqueID;
                }

                if (sourceCode != null)
                {
                    reservation.sourceCode = sourceCode;
                }
                else
                {
                    reservation.sourceCode = AppConstants.OWS_DEFAULT_SOURCE_CODE;
                }

                #region RoomStays

                ServiceProxies.Reservation.RoomStay roomStay = new ServiceProxies.Reservation.RoomStay();

                if (!string.IsNullOrEmpty(roomRate.RoomtypeCode) && (!string.IsNullOrEmpty(roomRate.RatePlanCode)))
                {
                    ServiceProxies.Reservation.RatePlan ratePlan = new ServiceProxies.Reservation.RatePlan();
                    ratePlan.ratePlanCode = roomRate.RatePlanCode;

                    if (roomRate.IsSpecialRate)
                    {
                        if (hotelSearch.SearchingType == SearchType.CORPORATE)
                        {
                            ratePlan.qualifyingIdType = hotelSearch.QualifyingType;
                            ratePlan.qualifyingIdValue = hotelSearch.CampaignCode;
                        }
                        else if (hotelSearch.SearchingType == SearchType.VOUCHER ||
                                 (hotelSearch.SearchingType == SearchType.REGULAR &&
                                  hotelSearch.CampaignCode != null))
                        {
                            ratePlan.promotionCode = hotelSearch.CampaignCode;
                        }
                    }

                    ServiceProxies.Reservation.RatePlan[] ratePlans = new ServiceProxies.Reservation.RatePlan[1];
                    ratePlans[0] = ratePlan;
                    roomStay.RatePlans = ratePlans;
                }

                ServiceProxies.Reservation.RoomType roomType = new ServiceProxies.Reservation.RoomType();
                roomType.isRoom = true;
                roomType.isRoomSpecified = true;
                roomType.roomTypeCode = roomRate.RoomtypeCode;
                roomType.numberOfUnitsSpecified = true;

                if (hotelSearch.QualifyingType == AppConstants.BLOCK_CODE_QUALIFYING_TYPE)
                {
                    if (hotelSearch.IsBlockCodeRate)
                    {
                        roomType.invBlockCode = hotelSearch.CampaignCode;
                    }
                    else
                    {
                        roomType.invBlockCode = string.Empty;
                    }
                }


                roomType.numberOfUnits = Int32.Parse(AppConstants.ONE);
                ServiceProxies.Reservation.RoomType[] roomTypes = new ServiceProxies.Reservation.RoomType[1];
                roomTypes[0] = roomType;
                roomStay.RoomTypes = roomTypes;

                ServiceProxies.Reservation.RoomRate bookingRoomRate = new ServiceProxies.Reservation.RoomRate();
                bookingRoomRate.roomTypeCode = roomRate.RoomtypeCode;
                bookingRoomRate.ratePlanCode = roomRate.RatePlanCode;

                ServiceProxies.Reservation.Rate rate = new ServiceProxies.Reservation.Rate();
                rate.Base = new ServiceProxies.Reservation.Amount();
                rate.Base.currencyCode = roomRate.Rate.CurrencyCode;
                rate.Base.Value = roomRate.Rate.Rate;
                ServiceProxies.Reservation.Rate[] rates = new ServiceProxies.Reservation.Rate[1];
                rates[0] = rate;
                bookingRoomRate.Rates = rates;

                bookingRoomRate.Total = new ServiceProxies.Reservation.Amount();
                bookingRoomRate.Total.currencyCode = roomRate.TotalRate.CurrencyCode;
                bookingRoomRate.Total.Value = roomRate.TotalRate.Rate;

                ServiceProxies.Reservation.RoomRate[] roomRates = new ServiceProxies.Reservation.RoomRate[1];
                roomRates[0] = bookingRoomRate;
                roomStay.RoomRates = roomRates;

                ServiceProxies.Reservation.GuestCountList guestCounts = new ServiceProxies.Reservation.GuestCountList();

                ServiceProxies.Reservation.GuestCount adults = new ServiceProxies.Reservation.GuestCount();
                adults.ageQualifyingCode = ServiceProxies.Reservation.AgeQualifyingCode.ADULT;
                adults.ageQualifyingCodeSpecified = true;
                adults.count = roomSearch.AdultsPerRoom;
                adults.countSpecified = true;

                ServiceProxies.Reservation.GuestCount[] guests;

                ServiceProxies.Reservation.GuestCount children = new ServiceProxies.Reservation.GuestCount();
                children.ageQualifyingCode = ServiceProxies.Reservation.AgeQualifyingCode.CHILD;
                children.ageQualifyingCodeSpecified = true;
                children.count = roomSearch.ChildrenOccupancyPerRoom;
                children.countSpecified = true;

                guests = new ServiceProxies.Reservation.GuestCount[2];
                guests[0] = adults;
                guests[1] = children;

                guestCounts.GuestCount = guests;
                guestCounts.isPerRoom = true;
                guestCounts.isPerRoomSpecified = true;
                roomStay.GuestCounts = guestCounts;

                ServiceProxies.Reservation.TimeSpan timeSpan =
                    new ServiceProxies.Reservation.TimeSpan();
                timeSpan.StartDate = hotelSearch.ArrivalDate;
                timeSpan.Item = hotelSearch.DepartureDate;
                roomStay.TimeSpan = timeSpan;

                if (null != guestInformation.GuranteeInformation)
                {
                    ServiceProxies.Reservation.Guarantee gurantee = new ServiceProxies.Reservation.Guarantee();
                    GuranteeInformationEntity guranteeInformation =
                        guestInformation.GuranteeInformation;
                    if (((guranteeInformation.GuranteeType == GuranteeType.CREDITCARD) || (guranteeInformation.GuranteeType == GuranteeType.PREPAIDINPROGRESS)
                        || (guranteeInformation.GuranteeType == GuranteeType.PREPAIDSUCCESS)
                        || (guranteeInformation.GuranteeType == GuranteeType.PREPAIDFAILURE)
                        || (guranteeInformation.GuranteeType == GuranteeType.MAKEPAYMENTFAILURE))
                        && (guranteeInformation.CreditCard != null))
                    {
                        ServiceProxies.Reservation.GuaranteeAccepted guranteeAccepted =
                            new ServiceProxies.Reservation.GuaranteeAccepted();
                        ServiceProxies.Reservation.CreditCard creditCard = new ServiceProxies.Reservation.CreditCard();
                        creditCard.cardCode = guranteeInformation.CreditCard.CardType;
                        creditCard.cardHolderName = guranteeInformation.CreditCard.NameOnCard;
                        creditCard.cardNumber = guranteeInformation.CreditCard.CardNumber;
                        creditCard.expirationDate = guranteeInformation.CreditCard.ExpiryDate.ToDateTime();
                        guranteeAccepted.GuaranteeCreditCard = creditCard;

                        ServiceProxies.Reservation.GuaranteeAccepted[] guranteesAccepted =
                            new ServiceProxies.Reservation.GuaranteeAccepted[1];
                        guranteesAccepted[0] = guranteeAccepted;
                        gurantee.GuaranteesAccepted = guranteesAccepted;
                    }
                    gurantee.guaranteeType = guranteeInformation.ChannelGuranteeCode;
                    roomStay.Guarantee = gurantee;
                }

                ServiceProxies.Reservation.HotelReference hotelReference = new ServiceProxies.Reservation.HotelReference();
                hotelReference.chainCode = AppConstants.CHAIN_CODE;
                hotelReference.hotelCode = hotelSearch.SelectedHotelCode;
                roomStay.HotelReference = hotelReference;

                ServiceProxies.Reservation.ReservationComment commentChildAge = new ServiceProxies.Reservation.ReservationComment();
                ServiceProxies.Reservation.ReservationComment commentRewardNightGift =
                    new ServiceProxies.Reservation.ReservationComment();
                ServiceProxies.Reservation.ReservationComment additionalRequest =
                    new ServiceProxies.Reservation.ReservationComment();
                if (guestInformation.ChildrensDetails != null)
                {
                    commentChildAge.guestViewable = true;
                    commentChildAge.guestViewableSpecified = true;
                    ServiceProxies.Reservation.Text text = new ServiceProxies.Reservation.Text();

                    text.Value = guestInformation.ChildrensDetails.AgeInComments;
                    commentChildAge.Items = new ServiceProxies.Reservation.Text[] { text };
                    commentChildAge.ItemsElementName = new ServiceProxies.Reservation.ItemsChoiceType[] { ServiceProxies.Reservation.ItemsChoiceType.Text };
                }
                if (guestInformation.IsRewardNightGift)
                {
                    commentRewardNightGift.guestViewable = true;
                    commentRewardNightGift.guestViewableSpecified = true;
                    ServiceProxies.Reservation.Text text = new ServiceProxies.Reservation.Text();
                    StringBuilder sb = new StringBuilder();
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_START);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.FirstName);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_NAME_SEPARATOR);
                    sb.Append(guestInformation.LastName);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.City);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.Country);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.Mobile.Number);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.EmailDetails.EmailID);
                    text.Value = sb.ToString();
                    commentRewardNightGift.Items = new ServiceProxies.Reservation.Text[] { text };
                    commentRewardNightGift.ItemsElementName = new ServiceProxies.Reservation.ItemsChoiceType[] { ServiceProxies.Reservation.ItemsChoiceType.Text };
                }

                string additionallReq = guestInformation.AdditionalRequest;
                if (!string.IsNullOrEmpty(additionallReq))
                {
                    additionalRequest.guestViewable = true;
                    additionalRequest.guestViewableSpecified = true;
                    ServiceProxies.Reservation.Text text = new ServiceProxies.Reservation.Text();
                    text.Value = additionallReq;
                    additionalRequest.Items = new ServiceProxies.Reservation.Text[] { text };
                    additionalRequest.ItemsElementName = new ServiceProxies.Reservation.ItemsChoiceType[] { ServiceProxies.Reservation.ItemsChoiceType.Text };
                }

                ServiceProxies.Reservation.ReservationComment commentFGPNumber = new ServiceProxies.Reservation.ReservationComment();
                if (guestInformation.GuestAccountNumber != null && guestInformation.GuestAccountNumber != string.Empty
                    && !guestInformation.IsValidFGPNumber)
                {
                    commentFGPNumber.guestViewable = true;
                    ServiceProxies.Reservation.Text cmtText = new ServiceProxies.Reservation.Text();
                    cmtText.Value = guestInformation.GuestAccountNumber;
                    commentFGPNumber.Items = new ServiceProxies.Reservation.Text[] { cmtText };
                    commentFGPNumber.ItemsElementName = new ServiceProxies.Reservation.ItemsChoiceType[] { ServiceProxies.Reservation.ItemsChoiceType.Text };
                }

                ServiceProxies.Reservation.ReservationComment[] comments = new ServiceProxies.Reservation.ReservationComment[4];
                comments[0] = commentChildAge;
                comments[1] = commentRewardNightGift;
                comments[2] = additionalRequest;
                comments[3] = commentFGPNumber;
                roomStay.Comments = comments;

                if (null != guestInformation.SpecialRequests)
                {
                    int totalRequest = guestInformation.SpecialRequests.Length;
                    ServiceProxies.Reservation.SpecialRequest[] requestList =
                        new ServiceProxies.Reservation.SpecialRequest[totalRequest];
                    for (int requestCount = 0; requestCount < totalRequest; requestCount++)
                    {
                        ServiceProxies.Reservation.SpecialRequest spRequest = new ServiceProxies.Reservation.SpecialRequest();
                        spRequest.requestCode = guestInformation.SpecialRequests[requestCount].RequestValue;
                        requestList[requestCount] = spRequest;
                    }
                    roomStay.SpecialRequests = requestList;
                }

                ServiceProxies.Reservation.RoomStay[] roomStays = new ServiceProxies.Reservation.RoomStay[1];
                roomStays[0] = roomStay;
                reservation.RoomStays = roomStays;

                #endregion

                #region ResGuests

                ServiceProxies.Reservation.Profile customerProfile = new ServiceProxies.Reservation.Profile();
                switch (flow)
                {
                    case BookingFlow.ModifyBooking:
                        {
                            if (!string.IsNullOrEmpty(guestInformation.NameID))
                            {
                                ServiceProxies.Reservation.UniqueID[] profileId = new ServiceProxies.Reservation.UniqueID[1];
                                profileId[0] = new ServiceProxies.Reservation.UniqueID();
                                profileId[0].type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
                                profileId[0].Value = guestInformation.NameID;
                                customerProfile.ProfileIDs = profileId;
                            }
                            //New changes for membership
                            if (!string.IsNullOrEmpty(guestInformation.GuestAccountNumber))
                            {
                                ServiceProxies.Reservation.NameMembership membership = new ServiceProxies.Reservation.NameMembership();
                                membership.membershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;
                                membership.membershipNumber = guestInformation.GuestAccountNumber;
                                if (guestInformation.MembershipOperaId > 0)
                                {
                                    membership.operaId = guestInformation.MembershipOperaId;
                                    membership.operaIdSpecified = true;
                                }
                                ServiceProxies.Reservation.NameMembership[] memberships = new ServiceProxies.Reservation.NameMembership[1];
                                memberships[0] = membership;
                                customerProfile.Memberships = memberships;
                            }
                            break;
                        }
                }

                ServiceProxies.Reservation.Customer customer = new ServiceProxies.Reservation.Customer();
                customer.gender = GetReservationGenderFromString(guestInformation.Gender);
                customer.genderSpecified = true;

                if (guestInformation.IsCreatebookingRequestWithNativeName)
                {
                    ServiceProxies.Reservation.NativeName nativeName = new ServiceProxies.Reservation.NativeName();
                    nativeName.nameTitle = new string[1];
                    nativeName.nameTitle.SetValue(guestInformation.Title, 0);
                    nativeName.firstName = guestInformation.FirstName;
                    nativeName.lastName = guestInformation.LastName;
                    customer.NativeName = nativeName;
                    guestInformation.IsCreatebookingRequestWithNativeName = false;
                }
                else
                {
                    ServiceProxies.Reservation.PersonName person = new ServiceProxies.Reservation.PersonName();
                    person.nameTitle = new string[1];
                    person.nameTitle.SetValue(guestInformation.Title, 0);
                    person.firstName = guestInformation.FirstName;
                    person.lastName = guestInformation.LastName;
                    customer.PersonName = person;

                    //Native Name
                    System.Globalization.TextInfo tInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
                    ServiceProxies.Reservation.NativeName nativeName = new ServiceProxies.Reservation.NativeName();
                    nativeName.nameTitle = new string[1];
                    nativeName.nameTitle.SetValue(guestInformation.Title, 0);
                    nativeName.firstName = tInfo.ToTitleCase(guestInformation.FirstName);
                    nativeName.lastName = tInfo.ToTitleCase(guestInformation.LastName);
                    customer.NativeName = nativeName;

                    string domain = System.Web.HttpContext.Current.Request.Url.Host;
                    int iPos = domain.LastIndexOf('.') + 1;
                    domain = domain.Substring(iPos, domain.Length - iPos).ToUpper();

                    switch (domain)
                    { 
                        case "COM":
                            customer.NativeName.languageCode = LanguageConstant.LANGUAGE_ENGLISH_CODE;
                            customerProfile.languageCode = LanguageConstant.LANGUAGE_ENGLISH_CODE;
                            break;
                        case "SE":
                            customer.NativeName.languageCode = LanguageConstant.LANGUAGE_SWEDISH_CODE;
                            customerProfile.languageCode = LanguageConstant.LANGUAGE_SWEDISH_CODE; 
                            break;
                        case "FI":
                            customer.NativeName.languageCode = LanguageConstant.LANGUAGE_FINNISH_CODE;
                            customerProfile.languageCode = LanguageConstant.LANGUAGE_FINNISH_CODE; 
                            break;
                        case "DA":
                            customer.NativeName.languageCode = LanguageConstant.LANGUAGE_DANISH_CODE;
                            customerProfile.languageCode = LanguageConstant.LANGUAGE_DANISH_CODE;
                            break;
                        case "DE":
                            customer.NativeName.languageCode = LanguageConstant.LANGUAGE_GERMAN_CODE;
                            customerProfile.languageCode = LanguageConstant.LANGUAGE_GERMAN_CODE;
                            break;
                        case "NO":
                            customer.NativeName.languageCode = LanguageConstant.LANGUAGE_NORWEGIAN_CODE;
                            customerProfile.languageCode = LanguageConstant.LANGUAGE_NORWEGIAN_CODE;
                            break;
                        case "RU":
                            customer.NativeName.languageCode = LanguageConstant.LANGUAGE_RUSSIA_CODE;
                            customerProfile.languageCode = LanguageConstant.LANGUAGE_RUSSIA_CODE;
                            break;
                        default:
                            customer.NativeName.languageCode = LanguageConstant.LANGUAGE_ENGLISH_CODE;
                            customerProfile.languageCode = LanguageConstant.LANGUAGE_ENGLISH_CODE;
                            break;
                    }

                    //if (!string.IsNullOrEmpty(guestInformation.Title) && !string.Equals(guestInformation.Title.ToUpper(), SessionBookingConstants.SESSION_BOOKING_NAME_TITLE, StringComparison.InvariantCultureIgnoreCase))
                    //    customer.NativeName.languageCode = LanguageConstant.LANGUAGE_GERMAN_CODE;
                    //else
                    //    customer.NativeName.languageCode = LanguageConstant.LANGUAGE_ENGLISH_CODE;

                }
                customerProfile.Item = customer;

                ServiceProxies.Reservation.NameAddress address = new ServiceProxies.Reservation.NameAddress();
                address.AddressLine = guestInformation.AddressLines;
                address.cityName = guestInformation.City;
                address.countryCode = guestInformation.Country;
                address.postalCode = guestInformation.PostCode;
                ServiceProxies.Reservation.NameAddress[] addresses = new ServiceProxies.Reservation.NameAddress[1];
                addresses[0] = address;
                customerProfile.Addresses = addresses;


                ServiceProxies.Reservation.NamePhone phoneMobile = null;
                if (null != guestInformation.Mobile)
                {
                    phoneMobile = new ServiceProxies.Reservation.NamePhone();
                    phoneMobile.displaySequence = 1;
                    phoneMobile.displaySequenceSpecified = true;
                    phoneMobile.phoneType = AppConstants.PHONETYPE_MOBILE;
                    phoneMobile.phoneRole = AppConstants.PHONEROLE_PHONE;
                    phoneMobile.Item = guestInformation.Mobile.Number;
                    customerProfile.Phones = new ServiceProxies.Reservation.NamePhone[] { phoneMobile };
                }

                switch (flow)
                {
                    case BookingFlow.Booking:
                        {
                            ServiceProxies.Reservation.NameMembership membership = new ServiceProxies.Reservation.NameMembership();
                            membership.membershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;
                            membership.membershipNumber = guestInformation.GuestAccountNumber;
                            if (guestInformation.MembershipOperaId > 0)
                            {
                                membership.operaId = guestInformation.MembershipOperaId;
                                membership.operaIdSpecified = true;
                            }
                            ServiceProxies.Reservation.NameMembership[] memberships = new ServiceProxies.Reservation.NameMembership[1];
                            memberships[0] = membership;
                            customerProfile.Memberships = memberships;
                            break;
                        }
                }

                ServiceProxies.Reservation.Profile[] listOfProfiles = null;

                bool numberWithL = false;
                if (!string.IsNullOrEmpty(hotelSearch.CampaignCode) && hotelSearch.CampaignCode.StartsWith("L"))
                    numberWithL = true;

                if (hotelSearch != null && hotelSearch.SearchingType != SearchType.BONUSCHEQUE && hotelSearch.SearchingType != SearchType.REDEMPTION
                   && !numberWithL && !string.IsNullOrEmpty(hotelSearch.IataProfileCode))
                {
                    listOfProfiles = new ServiceProxies.Reservation.Profile[3];
                }
                else
                {
                    listOfProfiles = new ServiceProxies.Reservation.Profile[2];
                }

                if (partnerID != null && !string.IsNullOrEmpty(partnerID))
                {
                    ServiceProxies.Reservation.Profile corporateProfile = new ServiceProxies.Reservation.Profile();
                    ServiceProxies.Reservation.Company company
                        = new ServiceProxies.Reservation.Company();
                    company.CompanyType = ServiceProxies.Reservation.CompanyCompanyType.TRAVEL_AGENT;
                    company.CompanyID = partnerID;
                    corporateProfile.Item = company;
                    listOfProfiles[1] = corporateProfile;
                }
                else if (
                    ((hotelSearch.SearchingType == SearchType.BONUSCHEQUE) &&
                     (!string.IsNullOrEmpty(guestInformation.D_Number)))
                    ||
                    ((hotelSearch.SearchingType == SearchType.CORPORATE) &&
                     (!string.IsNullOrEmpty(hotelSearch.CampaignCode)) && (!hotelSearch.CampaignCode.StartsWith("B"))))
                {
                    ServiceProxies.Reservation.Profile corporateProfile = new ServiceProxies.Reservation.Profile();
                    ServiceProxies.Reservation.Company company
                        = new ServiceProxies.Reservation.Company();
                    company.CompanyType = ServiceProxies.Reservation.CompanyCompanyType.COMPANY;

                    if (hotelSearch.SearchingType == SearchType.BONUSCHEQUE)
                    {
                        company.CompanyID = guestInformation.D_Number;
                    }
                    else if (hotelSearch.SearchingType == SearchType.CORPORATE)
                    {
                        company.CompanyID = hotelSearch.CampaignCode;
                    }
                    corporateProfile.Item = company;
                    listOfProfiles[1] = corporateProfile;
                    if ((!string.IsNullOrEmpty(hotelSearch.CampaignCode)) && hotelSearch.CampaignCode.StartsWith("L"))
                    {
                        company.CompanyType = ServiceProxies.Reservation.CompanyCompanyType.TRAVEL_AGENT;
                    }
                }
                else
                {
                    if (hotelSearch != null && hotelSearch.SearchingType != SearchType.BONUSCHEQUE && hotelSearch.SearchingType != SearchType.REDEMPTION
                   && !numberWithL && !string.IsNullOrEmpty(hotelSearch.IataProfileCode))
                    {
                        listOfProfiles = new ServiceProxies.Reservation.Profile[2];
                    }
                    else
                        listOfProfiles = new ServiceProxies.Reservation.Profile[1];
                }

                if (hotelSearch != null && hotelSearch.SearchingType != SearchType.BONUSCHEQUE && hotelSearch.SearchingType != SearchType.REDEMPTION
                   && !numberWithL && !string.IsNullOrEmpty(hotelSearch.IataProfileCode))
                {
                    listOfProfiles = GetIATAProfile(hotelSearch.IataProfileCode, listOfProfiles);
                }

                listOfProfiles[0] = customerProfile;
                ResGuest resGuest = new ResGuest();
                resGuest.Profiles = listOfProfiles;
                ResGuest[] resGuests = new ResGuest[1];
                resGuests[0] = resGuest;
                reservation.ResGuests = resGuests;

                #endregion
            }
            return reservation;
        }

        private static ServiceProxies.Reservation.Profile[] GetIATAProfile(string iataNumber, ServiceProxies.Reservation.Profile[] listOfProfiles)
        {
            ServiceProxies.Reservation.Profile iataProfile = new ServiceProxies.Reservation.Profile();
            ServiceProxies.Reservation.Company iataCompany
                = new ServiceProxies.Reservation.Company();
            iataCompany.CompanyType = ServiceProxies.Reservation.CompanyCompanyType.TRAVEL_AGENT;
            iataCompany.CompanyID = iataNumber;
            iataProfile.Item = iataCompany;
            listOfProfiles[listOfProfiles.Length - 1] = iataProfile;
            return listOfProfiles;
        }

        private static HotelReservation GetGuaranteForHotelReservation(HotelSearchEntity hotelSearch,
                                                                    HotelSearchRoomEntity roomSearch,
                                                                    HotelRoomRateEntity roomRate,
                                                                    GuestInformationEntity guestInformation,
                                                                    string reservationNumber, string sourceCode,
                                                                    BookingFlow flow, string partnerID)
        {
            HotelReservation reservation = null;
            if ((hotelSearch != null) && (roomSearch != null) && (roomRate != null) && (guestInformation != null))
            {
                reservation = new HotelReservation();

                if (!string.IsNullOrEmpty(reservationNumber))
                {
                    ServiceProxies.Reservation.UniqueID uniqueID = new ServiceProxies.Reservation.UniqueID();
                    uniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
                    uniqueID.Value = reservationNumber;

                    reservation.UniqueIDList = new ServiceProxies.Reservation.UniqueID[1];
                    reservation.UniqueIDList[0] = uniqueID;
                }

                if (sourceCode != null)
                {
                    reservation.sourceCode = sourceCode;
                }
                else
                {
                    reservation.sourceCode = AppConstants.OWS_DEFAULT_SOURCE_CODE;
                }
            }
            #region RoomStays

            ServiceProxies.Reservation.RoomStay roomStay = new ServiceProxies.Reservation.RoomStay();

            if (null != guestInformation.GuranteeInformation)
            {
                ServiceProxies.Reservation.Guarantee gurantee = new ServiceProxies.Reservation.Guarantee();
                GuranteeInformationEntity guranteeInformation =
                    guestInformation.GuranteeInformation;
                if (((guranteeInformation.GuranteeType == GuranteeType.CREDITCARD) || (guranteeInformation.GuranteeType == GuranteeType.PREPAIDINPROGRESS)
                    || (guranteeInformation.GuranteeType == GuranteeType.PREPAIDSUCCESS)
                    || (guranteeInformation.GuranteeType == GuranteeType.PREPAIDFAILURE)
                    || (guranteeInformation.GuranteeType == GuranteeType.MAKEPAYMENTFAILURE))
                    && (guranteeInformation.CreditCard != null))
                {
                    ServiceProxies.Reservation.GuaranteeAccepted guranteeAccepted =
                        new ServiceProxies.Reservation.GuaranteeAccepted();
                    ServiceProxies.Reservation.CreditCard creditCard = new ServiceProxies.Reservation.CreditCard();
                    creditCard.cardCode = guranteeInformation.CreditCard.CardType;
                    creditCard.cardHolderName = guranteeInformation.CreditCard.NameOnCard;
                    creditCard.cardNumber = guranteeInformation.CreditCard.CardNumber;
                    creditCard.expirationDate = guranteeInformation.CreditCard.ExpiryDate.ToDateTime();
                    guranteeAccepted.GuaranteeCreditCard = creditCard;

                    ServiceProxies.Reservation.GuaranteeAccepted[] guranteesAccepted =
                        new ServiceProxies.Reservation.GuaranteeAccepted[1];
                    guranteesAccepted[0] = guranteeAccepted;
                    gurantee.GuaranteesAccepted = guranteesAccepted;
                }
                gurantee.guaranteeType = guranteeInformation.ChannelGuranteeCode;
                roomStay.Guarantee = gurantee;

                ServiceProxies.Reservation.ReservationComment commentChildAge = new ServiceProxies.Reservation.ReservationComment();
                ServiceProxies.Reservation.ReservationComment commentRewardNightGift =
                    new ServiceProxies.Reservation.ReservationComment();
                ServiceProxies.Reservation.ReservationComment additionalRequest =
                    new ServiceProxies.Reservation.ReservationComment();
                if (guestInformation.ChildrensDetails != null)
                {
                    commentChildAge.guestViewable = true;
                    commentChildAge.guestViewableSpecified = true;
                    ServiceProxies.Reservation.Text text = new ServiceProxies.Reservation.Text();

                    text.Value = guestInformation.ChildrensDetails.AgeInComments;
                    commentChildAge.Items = new ServiceProxies.Reservation.Text[] { text };
                    commentChildAge.ItemsElementName = new ServiceProxies.Reservation.ItemsChoiceType[] { ServiceProxies.Reservation.ItemsChoiceType.Text };
                }
                if (guestInformation.IsRewardNightGift)
                {
                    commentRewardNightGift.guestViewable = true;
                    commentRewardNightGift.guestViewableSpecified = true;
                    ServiceProxies.Reservation.Text text = new ServiceProxies.Reservation.Text();
                    StringBuilder sb = new StringBuilder();
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_START);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.FirstName);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_NAME_SEPARATOR);
                    sb.Append(guestInformation.LastName);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.City);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.Country);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.Mobile.Number);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.EmailDetails.EmailID);
                    text.Value = sb.ToString();
                    commentRewardNightGift.Items = new ServiceProxies.Reservation.Text[] { text };
                    commentRewardNightGift.ItemsElementName = new ServiceProxies.Reservation.ItemsChoiceType[] { ServiceProxies.Reservation.ItemsChoiceType.Text };
                }

                string additionallReq = guestInformation.AdditionalRequest;
                if (!string.IsNullOrEmpty(additionallReq))
                {
                    additionalRequest.guestViewable = true;
                    additionalRequest.guestViewableSpecified = true;
                    ServiceProxies.Reservation.Text text = new ServiceProxies.Reservation.Text();
                    text.Value = additionallReq;
                    additionalRequest.Items = new ServiceProxies.Reservation.Text[] { text };
                    additionalRequest.ItemsElementName = new ServiceProxies.Reservation.ItemsChoiceType[] { ServiceProxies.Reservation.ItemsChoiceType.Text };
                }

                ServiceProxies.Reservation.ReservationComment commentFGPNumber = new ServiceProxies.Reservation.ReservationComment();
                if (guestInformation.GuestAccountNumber != null && guestInformation.GuestAccountNumber != string.Empty
                    && !guestInformation.IsValidFGPNumber)
                {
                    commentFGPNumber.guestViewable = true;
                    ServiceProxies.Reservation.Text cmtText = new ServiceProxies.Reservation.Text();
                    cmtText.Value = guestInformation.GuestAccountNumber;
                    commentFGPNumber.Items = new ServiceProxies.Reservation.Text[] { cmtText };
                    commentFGPNumber.ItemsElementName = new ServiceProxies.Reservation.ItemsChoiceType[] { ServiceProxies.Reservation.ItemsChoiceType.Text };
                }

                ServiceProxies.Reservation.ReservationComment[] comments = new ServiceProxies.Reservation.ReservationComment[4];
                comments[0] = commentChildAge;
                comments[1] = commentRewardNightGift;
                comments[2] = additionalRequest;
                comments[3] = commentFGPNumber;
                roomStay.Comments = comments;
            }
            if (guestInformation.SpecialRequests != null)
            {
                int totalRequest = guestInformation.SpecialRequests.Length;
                ServiceProxies.Reservation.SpecialRequest[] requestList =
                    new ServiceProxies.Reservation.SpecialRequest[totalRequest];
                for (int requestCount = 0; requestCount < totalRequest; requestCount++)
                {
                    ServiceProxies.Reservation.SpecialRequest spRequest = new ServiceProxies.Reservation.SpecialRequest();
                    spRequest.requestCode = guestInformation.SpecialRequests[requestCount].RequestValue;
                    requestList[requestCount] = spRequest;
                }
                roomStay.SpecialRequests = requestList;
            }

            ServiceProxies.Reservation.RoomStay[] roomStays = new ServiceProxies.Reservation.RoomStay[1];
            roomStays[0] = roomStay;
            reservation.RoomStays = roomStays;

            //New changes for membership
            if (guestInformation != null && !string.IsNullOrEmpty(guestInformation.GuestAccountNumber))
            {
                ServiceProxies.Reservation.Profile customerProfile = new ServiceProxies.Reservation.Profile();

                ServiceProxies.Reservation.NameMembership membership = new ServiceProxies.Reservation.NameMembership();

                membership.membershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;
                membership.membershipNumber = guestInformation.GuestAccountNumber;
                if (guestInformation.MembershipOperaId > 0)
                {
                    membership.operaId = guestInformation.MembershipOperaId;
                    membership.operaIdSpecified = true;
                }
                ServiceProxies.Reservation.NameMembership[] memberships = new ServiceProxies.Reservation.NameMembership[1];
                memberships[0] = membership;
                customerProfile.Memberships = memberships;

                if (!string.IsNullOrEmpty(guestInformation.NameID))
                {
                    ServiceProxies.Reservation.UniqueID[] profileId = new ServiceProxies.Reservation.UniqueID[1];
                    profileId[0] = new ServiceProxies.Reservation.UniqueID();
                    profileId[0].type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
                    profileId[0].Value = guestInformation.NameID;
                    customerProfile.ProfileIDs = profileId;
                }
                ServiceProxies.Reservation.Profile[] listOfProfiles = new ServiceProxies.Reservation.Profile[1];
                listOfProfiles[0] = customerProfile;
                ResGuest resGuest = new ResGuest();
                resGuest.Profiles = listOfProfiles;
                ResGuest[] resGuests = new ResGuest[1];
                resGuests[0] = resGuest;
                reservation.ResGuests = resGuests;
            }

            #endregion


            return reservation;
        }

        /// Gets the new redemption booking format.
        /// </summary>
        /// <param name="hotelSearch">The hotel search.</param>
        /// <param name="roomRate">The room rate.</param>
        /// <param name="guestInformation">The guest information.</param>
        /// <param name="reservationNumber">The reservation number.</param>
        /// <param name="sourceCode">The source code.</param>
        /// <param name="flow">The flow.</param>
        /// <returns>HotelReservation object</returns>
        /// <remarks>
        /// This method is created out seperately only for Redemption booking
        /// Once the format is changed for other booking then merge it with method "GetHotelReservation"
        /// </remarks>
        private static HotelReservation GetRedemptionBooking(HotelSearchEntity hotelSearch,
                                                             HotelSearchRoomEntity roomSearch,
                                                             HotelRoomRateEntity roomRate,
                                                             GuestInformationEntity guestInformation,
                                                             string reservationNumber, string sourceCode,
                                                             BookingFlow flow)
        {
            HotelReservation reservation = null;
            if ((hotelSearch != null) && (roomRate != null) && (guestInformation != null) && (roomSearch != null))
            {
                reservation = new HotelReservation();

                if (!string.IsNullOrEmpty(reservationNumber))
                {
                    ServiceProxies.Reservation.UniqueID uniqueID = new ServiceProxies.Reservation.UniqueID();
                    uniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
                    uniqueID.Value = reservationNumber;

                    reservation.UniqueIDList = new ServiceProxies.Reservation.UniqueID[1];
                    reservation.UniqueIDList[0] = uniqueID;
                }

                if (sourceCode != null)
                {
                    reservation.sourceCode = sourceCode;
                }
                else
                {
                    reservation.sourceCode = AppConstants.OWS_DEFAULT_SOURCE_CODE;
                }
                reservation.redemReservationFlag = true;
                reservation.redemReservationFlagSpecified = true;

                #region RoomStays

                ServiceProxies.Reservation.RoomStay roomStay = new ServiceProxies.Reservation.RoomStay();
                ServiceProxies.Reservation.RatePlan ratePlan = new ServiceProxies.Reservation.RatePlan();
                ratePlan.ratePlanCode = roomRate.RatePlanCode;
                ServiceProxies.Reservation.RatePlan[] ratePlans = new ServiceProxies.Reservation.RatePlan[1];
                ratePlans[0] = ratePlan;
                roomStay.RatePlans = ratePlans;

                ServiceProxies.Reservation.RoomType roomType = new ServiceProxies.Reservation.RoomType();
                roomType.isRoom = true;
                roomType.isRoomSpecified = true;
                roomType.roomTypeCode = roomRate.RoomtypeCode;
                roomType.numberOfUnitsSpecified = true;
                roomType.numberOfUnits = Int32.Parse(AppConstants.ONE);
                ServiceProxies.Reservation.RoomType[] roomTypes = new ServiceProxies.Reservation.RoomType[1];
                roomTypes[0] = roomType;
                roomStay.RoomTypes = roomTypes;

                ServiceProxies.Reservation.RoomRate bookingRoomRate = new ServiceProxies.Reservation.RoomRate();
                bookingRoomRate.roomTypeCode = roomRate.RoomtypeCode;
                bookingRoomRate.ratePlanCode = roomRate.RatePlanCode;

                ServiceProxies.Reservation.MemberAwardInfo memberAwardInfo = new ServiceProxies.Reservation.MemberAwardInfo();
                memberAwardInfo.awardType = roomRate.AwardType;
                memberAwardInfo.pointsUsedForReservation = roomRate.Points;
                memberAwardInfo.membershipID = guestInformation.MembershipID;

                ServiceProxies.Reservation.TimeSpan timeSpan =
                    new ServiceProxies.Reservation.TimeSpan();
                timeSpan.StartDate = hotelSearch.ArrivalDate;
                timeSpan.Item = hotelSearch.DepartureDate;
                memberAwardInfo.stayDate = timeSpan;
                memberAwardInfo.redemRateCode = ratePlan.ratePlanCode;

                roomStay.MemberAwardInfo = memberAwardInfo;
                ServiceProxies.Reservation.RoomRate[] roomRates = new ServiceProxies.Reservation.RoomRate[1];
                roomRates[0] = bookingRoomRate;
                roomStay.RoomRates = roomRates;


                ServiceProxies.Reservation.GuestCountList guestCounts = new ServiceProxies.Reservation.GuestCountList();

                ServiceProxies.Reservation.GuestCount adults = new ServiceProxies.Reservation.GuestCount();
                adults.ageQualifyingCode = ServiceProxies.Reservation.AgeQualifyingCode.ADULT;
                adults.ageQualifyingCodeSpecified = true;
                adults.count = roomSearch.AdultsPerRoom;
                adults.countSpecified = true;
                ServiceProxies.Reservation.GuestCount[] guests;
                ServiceProxies.Reservation.GuestCount children = new ServiceProxies.Reservation.GuestCount();
                children.ageQualifyingCode = ServiceProxies.Reservation.AgeQualifyingCode.CHILD;
                children.ageQualifyingCodeSpecified = true;
                children.count = roomSearch.ChildrenOccupancyPerRoom;
                children.countSpecified = true;
                guests = new ServiceProxies.Reservation.GuestCount[2];
                guests[0] = adults;
                guests[1] = children;
                guestCounts.GuestCount = guests;
                guestCounts.isPerRoom = true;
                guestCounts.isPerRoomSpecified = true;
                roomStay.GuestCounts = guestCounts;

                ServiceProxies.Reservation.Guarantee gurantee = new ServiceProxies.Reservation.Guarantee();
                GuranteeInformationEntity guranteeInformation = guestInformation.GuranteeInformation;
                if (null != guestInformation.GuranteeInformation)
                {
                    gurantee.guaranteeType = guranteeInformation.ChannelGuranteeCode;
                    roomStay.Guarantee = gurantee;
                }

                ServiceProxies.Reservation.HotelReference hotelReference = new ServiceProxies.Reservation.HotelReference();
                hotelReference.chainCode = AppConstants.CHAIN_CODE;
                hotelReference.hotelCode = hotelSearch.SelectedHotelCode;
                roomStay.HotelReference = hotelReference;

                ServiceProxies.Reservation.ReservationComment commentChildAge = new ServiceProxies.Reservation.ReservationComment();
                ServiceProxies.Reservation.ReservationComment commentRewardNightGift =
                    new ServiceProxies.Reservation.ReservationComment();
                ServiceProxies.Reservation.ReservationComment additionalRequest =
                    new ServiceProxies.Reservation.ReservationComment();
                if (guestInformation.ChildrensDetails != null)
                {
                    commentChildAge.guestViewable = true;
                    commentChildAge.guestViewableSpecified = true;
                    ServiceProxies.Reservation.Text text = new ServiceProxies.Reservation.Text();
                    text.Value = guestInformation.ChildrensDetails.AgeInComments;
                    commentChildAge.Items = new ServiceProxies.Reservation.Text[] { text };
                    commentChildAge.ItemsElementName = new ServiceProxies.Reservation.ItemsChoiceType[] { ServiceProxies.Reservation.ItemsChoiceType.Text };
                }
                if (guestInformation.IsRewardNightGift)
                {
                    commentRewardNightGift.guestViewable = true;
                    commentRewardNightGift.guestViewableSpecified = true;
                    ServiceProxies.Reservation.Text text = new ServiceProxies.Reservation.Text();
                    StringBuilder sb = new StringBuilder();
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_START);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.FirstName);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_NAME_SEPARATOR);
                    sb.Append(guestInformation.LastName);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.City);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.Country);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.Mobile.Number);
                    sb.Append(RewardNightGiftBooking.REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR);
                    sb.Append(guestInformation.EmailDetails.EmailID);
                    text.Value = sb.ToString();
                    commentRewardNightGift.Items = new ServiceProxies.Reservation.Text[] { text };
                    commentRewardNightGift.ItemsElementName = new ServiceProxies.Reservation.ItemsChoiceType[] { ServiceProxies.Reservation.ItemsChoiceType.Text };
                }

                string additionallReq = guestInformation.AdditionalRequest;
                if (!string.IsNullOrEmpty(additionallReq))
                {
                    additionalRequest.guestViewable = true;
                    additionalRequest.guestViewableSpecified = true;
                    ServiceProxies.Reservation.Text text = new ServiceProxies.Reservation.Text();
                    text.Value = additionallReq;
                    additionalRequest.Items = new ServiceProxies.Reservation.Text[] { text };
                    additionalRequest.ItemsElementName = new ServiceProxies.Reservation.ItemsChoiceType[] { ServiceProxies.Reservation.ItemsChoiceType.Text };
                }
                ServiceProxies.Reservation.ReservationComment commentFGPNumber = new ServiceProxies.Reservation.ReservationComment();
                if (guestInformation.GuestAccountNumber != null && guestInformation.GuestAccountNumber != string.Empty
                    && !guestInformation.IsValidFGPNumber)
                {
                    commentFGPNumber.guestViewable = true;
                    ServiceProxies.Reservation.Text cmtText = new ServiceProxies.Reservation.Text();
                    cmtText.Value = guestInformation.GuestAccountNumber;
                    commentFGPNumber.Items = new ServiceProxies.Reservation.Text[] { cmtText };
                    commentFGPNumber.ItemsElementName = new ServiceProxies.Reservation.ItemsChoiceType[] { ServiceProxies.Reservation.ItemsChoiceType.Text };
                }

                ServiceProxies.Reservation.ReservationComment[] comments = new ServiceProxies.Reservation.ReservationComment[4];
                comments[0] = commentChildAge;
                comments[1] = commentRewardNightGift;
                comments[2] = additionalRequest;
                comments[3] = commentFGPNumber;

                roomStay.Comments = comments;

                if (null != guestInformation.SpecialRequests)
                {
                    int totalRequest = guestInformation.SpecialRequests.Length;
                    ServiceProxies.Reservation.SpecialRequest[] requestList =
                        new ServiceProxies.Reservation.SpecialRequest[totalRequest];
                    for (int requestCount = 0; requestCount < totalRequest; requestCount++)
                    {
                        ServiceProxies.Reservation.SpecialRequest spRequest = new ServiceProxies.Reservation.SpecialRequest();
                        spRequest.requestCode = guestInformation.SpecialRequests[requestCount].RequestValue;
                        requestList[requestCount] = spRequest;
                    }
                    roomStay.SpecialRequests = requestList;
                }

                ServiceProxies.Reservation.RoomStay[] roomStays = new ServiceProxies.Reservation.RoomStay[1];
                roomStays[0] = roomStay;
                reservation.RoomStays = roomStays;

                #endregion

                #region ResGuests

                ServiceProxies.Reservation.Profile customerProfile = new ServiceProxies.Reservation.Profile();

                ServiceProxies.Reservation.Customer customer = new ServiceProxies.Reservation.Customer();
                customer.gender = GetReservationGenderFromString(guestInformation.Gender);
                customer.genderSpecified = true;

                //ServiceProxies.Reservation.PersonName person = new ServiceProxies.Reservation.PersonName();
                //person.nameTitle = new string[1];
                //person.nameTitle[0] = guestInformation.Title;
                //person.firstName = guestInformation.FirstName;
                //person.lastName = guestInformation.LastName;
                //customer.PersonName = person;
                //customerProfile.Item = customer;

                //Added as part of SCANAM-537
                if (guestInformation.IsCreatebookingRequestWithNativeName)
                {
                    ServiceProxies.Reservation.NativeName nativeName = new ServiceProxies.Reservation.NativeName();
                    nativeName.nameTitle = new string[1];
                    nativeName.nameTitle.SetValue(guestInformation.Title, 0);
                    nativeName.firstName = guestInformation.FirstName;
                    nativeName.lastName = guestInformation.LastName;
                    customer.NativeName = nativeName;
                    guestInformation.IsCreatebookingRequestWithNativeName = false;
                }
                else
                {
                    ServiceProxies.Reservation.PersonName person = new ServiceProxies.Reservation.PersonName();
                    person.nameTitle = new string[1];
                    person.nameTitle.SetValue(guestInformation.Title, 0);
                    person.firstName = guestInformation.FirstName;
                    person.lastName = guestInformation.LastName;
                    customer.PersonName = person;
                }
                customerProfile.Item = customer;

                ServiceProxies.Reservation.NameAddress address = new ServiceProxies.Reservation.NameAddress();
                address.AddressLine = guestInformation.AddressLines;
                address.cityName = guestInformation.City;
                address.countryCode = guestInformation.Country;
                address.postalCode = guestInformation.PostCode;
                ServiceProxies.Reservation.NameAddress[] addresses = new ServiceProxies.Reservation.NameAddress[1];
                addresses[0] = address;
                customerProfile.Addresses = addresses;

                ServiceProxies.Reservation.NamePhone phoneMobile = null;
                if (null != guestInformation.Mobile)
                {
                    phoneMobile = new ServiceProxies.Reservation.NamePhone();
                    phoneMobile.displaySequence = 1;
                    phoneMobile.displaySequenceSpecified = true;
                    phoneMobile.phoneType = AppConstants.PHONETYPE_MOBILE;
                    phoneMobile.phoneRole = AppConstants.PHONEROLE_PHONE;
                    phoneMobile.Item = guestInformation.Mobile.Number;
                    customerProfile.Phones = new ServiceProxies.Reservation.NamePhone[] { phoneMobile };
                }

                switch (flow)
                {
                    case BookingFlow.Booking:
                        {
                            ServiceProxies.Reservation.NameMembership membership = new ServiceProxies.Reservation.NameMembership();
                            membership.membershipType = AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE;
                            membership.membershipNumber = guestInformation.GuestAccountNumber;
                            if (guestInformation.MembershipOperaId > 0)
                            {
                                membership.operaId = guestInformation.MembershipOperaId;
                                membership.operaIdSpecified = true;
                            }
                            ServiceProxies.Reservation.NameMembership[] memberships =
                                new ServiceProxies.Reservation.NameMembership[1];
                            memberships[0] = membership;
                            customerProfile.Memberships = memberships;
                            break;
                        }
                }

                ServiceProxies.Reservation.Profile[] listOfProfiles = null;
                listOfProfiles = new ServiceProxies.Reservation.Profile[1];

                listOfProfiles[0] = customerProfile;


                ResGuest resGuest = new ResGuest();
                resGuest.Profiles = listOfProfiles;
                ResGuest[] resGuests = new ResGuest[1];
                resGuests[0] = resGuest;
                reservation.ResGuests = resGuests;

                #endregion
            }
            return reservation;
        }

        /// <summary>
        /// Gets updated packages.
        /// </summary>
        /// <param name="reservationNumber"></param>
        /// <param name="legNumber"></param>
        /// <param name="hotelCode"></param>
        /// <param name="bedaccommodation"></param>
        /// <returns></returns>
        public static UpdatePackagesRequest GetUpdatePackagesRequest(string reservationNumber, string legNumber,
                                                                     string hotelCode, BedAccommodation bedaccommodation)
        {
            UpdatePackagesRequest request = new UpdatePackagesRequest();

            ServiceProxies.Reservation.UniqueID confirmationUniqueID =
                new ServiceProxies.Reservation.UniqueID();
            confirmationUniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            confirmationUniqueID.Value = reservationNumber;
            request.ConfirmationNumber = confirmationUniqueID;

            ServiceProxies.Reservation.UniqueID legNumberUniqueID =
                new ServiceProxies.Reservation.UniqueID();
            legNumberUniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            legNumberUniqueID.Value = legNumber;
            request.LegNumber = legNumberUniqueID;

            ServiceProxies.Reservation.HotelReference hotelReference =
                new ServiceProxies.Reservation.HotelReference();
            hotelReference.chainCode = AppConstants.CHAIN_CODE;
            hotelReference.hotelCode = hotelCode;
            request.HotelReference = hotelReference;

            request.ProductCode = bedaccommodation.BedType.ToString();
            request.Quantity = bedaccommodation.Quantity;
            request.QuantitySpecified1 = true;
            request.QuantitySpecified = true;
            return request;
        }

        /// <summary>
        /// Gets deleted packages.
        /// </summary>
        /// <param name="reservationNumber"></param>
        /// <param name="legNumber"></param>
        /// <param name="hotelCode"></param>
        /// <param name="bedaccommodation"></param>
        /// <returns></returns>
        public static DeletePackagesRequest GetDeletePackagesRequest(string reservationNumber, string legNumber,
                                                                     string hotelCode,
                                                                     BedAccommodation bedaccommodation)
        {
            DeletePackagesRequest request = new DeletePackagesRequest();

            ServiceProxies.Reservation.UniqueID confirmationUniqueID =
                new ServiceProxies.Reservation.UniqueID();
            confirmationUniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            confirmationUniqueID.Value = reservationNumber;
            request.ConfirmationNumber = confirmationUniqueID;

            ServiceProxies.Reservation.UniqueID legNumberUniqueID =
                new ServiceProxies.Reservation.UniqueID();
            legNumberUniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            legNumberUniqueID.Value = legNumber;
            request.LegNumber = legNumberUniqueID;

            ServiceProxies.Reservation.HotelReference hotelReference =
                new ServiceProxies.Reservation.HotelReference();
            hotelReference.chainCode = AppConstants.CHAIN_CODE;
            hotelReference.hotelCode = hotelCode;
            request.HotelReference = hotelReference;

            request.ProductCode = bedaccommodation.BedType.ToString();

            return request;
        }

        /// <summary>
        /// Gets booked packages.
        /// </summary>
        /// <param name="reservationNumber"></param>
        /// <param name="legNumber"></param>
        /// <param name="hotelCode"></param>
        /// <returns></returns>
        public static FetchBookedPackagesRequest GetFetchBookedPackagesRequest(string reservationNumber,
                                                                               string legNumber, string hotelCode)
        {
            FetchBookedPackagesRequest request = new FetchBookedPackagesRequest();

            ServiceProxies.Reservation.UniqueID confirmationUniqueID =
                new ServiceProxies.Reservation.UniqueID();
            confirmationUniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            confirmationUniqueID.Value = reservationNumber;
            request.ConfirmationNumber = confirmationUniqueID;

            ServiceProxies.Reservation.UniqueID legNumberUniqueID =
                new ServiceProxies.Reservation.UniqueID();
            legNumberUniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            legNumberUniqueID.Value = legNumber;
            request.LegNumber = legNumberUniqueID;

            ServiceProxies.Reservation.HotelReference hotelReference =
                new ServiceProxies.Reservation.HotelReference();
            hotelReference.chainCode = AppConstants.CHAIN_CODE;
            hotelReference.hotelCode = hotelCode;
            request.HotelReference = hotelReference;

            return request;
        }

        /// <summary>
        /// Get FutureBookingSummary Request
        /// </summary>
        /// <param name="nameId">Name ID</param>
        /// <returns>FutureBookingSummaryRequest</returns>
        public static FutureBookingSummaryRequest GetFutureBookingSummaryRequest(string nameId)
        {
            FutureBookingSummaryRequest request = new FutureBookingSummaryRequest();

            ServiceProxies.Reservation.UniqueID uniqueID = new ServiceProxies.Reservation.UniqueID();
            uniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            uniqueID.Value = nameId;

            request.Item = uniqueID;
            request.ItemElementName = ItemChoiceType1.NameID;

            request.QueryDateRange = new FutureBookingSummaryRequestQueryDateRange();
            request.QueryDateRange.StartDate = DateTime.Today.Date;
            request.QueryDateRange.Item = DateTime.Today.Date.AddYears(2);
            return request;
        }

        private static ServiceProxies.Reservation.Gender GetReservationGenderFromString(string gender)
        {
            if (gender == AppConstants.MALE)
                return ServiceProxies.Reservation.Gender.MALE;
            else if (gender == AppConstants.FEMALE)
                return ServiceProxies.Reservation.Gender.FEMALE;
            else
                return ServiceProxies.Reservation.Gender.UNKNOWN;
        }

        #endregion

        #region FetchMembershipTransactionsRequest

        /// <summary>
        /// Create FetchMembershipTransactionsRequest object  
        /// </summary>
        /// <param name="nameID">nameID</param>
        /// <returns>FetchMembershipTransactionsRequest</returns>
        public static ServiceProxies.Membership.FetchMembershipTransactionsRequest
            GetFetchMembershipTransactionsRequest(string nameID)
        {
            FetchMembershipTransactionsRequest request = new FetchMembershipTransactionsRequest();

            ServiceProxies.Membership.UniqueID uniqueId = new ServiceProxies.Membership.UniqueID();
            uniqueId.Value = nameID;
            uniqueId.type = ServiceProxies.Membership.UniqueIDType.INTERNAL;

            request.Item = uniqueId;

            return request;
        }

        #endregion FetchMembershipTransactionsRequest

        #region Validate Question Request

        /// <summary>
        /// Method used to create Validate Question Request object.
        /// After creating this we are using in Loyalty Domain.
        /// </summary>
        /// <param name="membershipNo">Membership number.</param>
        /// <param name="questionId">Question ID.</param>
        /// <param name="questionString">Question String.</param>
        /// <param name="answer">Answer String.</param>
        /// <returns></returns>
        public static ServiceProxies.Security.ValidateQuestionRequest GetValidateQuestionRequest
            (string membershipNo, string questionId, string questionString, string answer)
        {
            ValidateQuestionRequest request = new ServiceProxies.Security.ValidateQuestionRequest();
            request.membershipNumber = membershipNo;

            ServiceProxies.Security.Question question = new Question();
            question.questionId = questionId;
            question.Value = questionString;

            request.Question = question;
            request.Answer = answer;

            return request;
        }

        #endregion Validate Question Request

        #region GetGeneratePasswordRequest

        /// <summary>
        /// This will convert the request for generate password. 
        /// </summary>
        /// <param name="membershipNumber">Membership number for which the password needs to be reseted.</param>
        /// <returns>GeneratePasswordRequest request.</returns>
        public static ServiceProxies.Security.GeneratePasswordRequest GetGeneratePasswordRequest
            (string membershipNumber)
        {
            GeneratePasswordRequest request = new ServiceProxies.Security.GeneratePasswordRequest();
            request.loginName = membershipNumber;

            return request;
        }

        #endregion GetGeneratePasswordRequest

        #region Implementation

        #region GetFetchBookingSummary

        /// <summary>
        /// Gets booking summary.
        /// </summary>
        /// <param name="bookingNumber"></param>
        /// <returns></returns>
        public static FetchSummaryRequest GetFetchBookingSummary(string bookingNumber)
        {
            FetchSummaryRequest request = new FetchSummaryRequest();
            ServiceProxies.Reservation.UniqueID uniqueID = new ServiceProxies.Reservation.UniqueID();
            uniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            uniqueID.Value = bookingNumber;
            request.ConfirmationNumber = uniqueID;

            return request;
        }

        #endregion GetFetchBookingSummary

        /// <summary>
        /// Gets leg booking.
        /// </summary>
        /// <param name="bookingNumber"></param>
        /// <param name="legNumber"></param>
        /// <returns></returns>
        public static FetchBookingRequest GetFetchLegBooking(string bookingNumber, string legNumber)
        {
            FetchBookingRequest request = new FetchBookingRequest();
            ServiceProxies.Reservation.UniqueID uniqueConfirmationID = new ServiceProxies.Reservation.UniqueID();
            uniqueConfirmationID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            uniqueConfirmationID.Value = bookingNumber;

            request.ConfirmationNumber = uniqueConfirmationID;

            ServiceProxies.Reservation.UniqueID uniqueLegID = new ServiceProxies.Reservation.UniqueID();
            uniqueLegID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            uniqueLegID.Value = legNumber;
            request.LegNumber = uniqueLegID;

            return request;
        }

        #endregion Implementation

        #region Information Service

        /// <summary>
        /// Create the Currency Converter Request object
        /// </summary>
        /// <param name="sourceCurrencyCode">
        /// Source Currency Code
        /// </param>
        /// <param name="destinationCurrencyCode">
        /// Destination Currency Code
        /// </param>
        /// <param name="value">
        /// Value to be converted
        /// </param>
        /// <returns>
        /// CurrencyConverterRequest
        /// </returns>
        /// <remarks>
        /// This changes has been added for CR-4(Currency Converter) - Release 1.3
        /// </remarks>
        public static CurrencyConverterRequest GetCurrencyConveterRequest(string sourceCurrencyCode,
                                                                          string destinationCurrencyCode, double value)
        {
            CurrencyConverterRequest request = new CurrencyConverterRequest();

            ServiceProxies.Information.Amount frmCurrency = new ServiceProxies.Information.Amount();
            request.FromCurrency = frmCurrency;
            request.FromCurrency.currencyCode = sourceCurrencyCode;
            request.FromCurrency.Value = value;

            ServiceProxies.Information.HotelReference resort =
                new ServiceProxies.Information.HotelReference();
            resort.chainCode = "";
            resort.hotelCode = "TEMPLATE";
            request.Resort = resort;

            request.ExchangeType = CurrencyExchangeType.EXCHANGE_CHECK;
            request.ExchangeTypeSpecified = true;

            ServiceProxies.Information.Amount toCurrency = new ServiceProxies.Information.Amount();
            request.ToCurrency = toCurrency;
            request.ToCurrency.currencyCode = destinationCurrencyCode;

            return request;
        }


        /// <summary>
        /// Create the Membership Type Request object
        /// </summary>
        /// <returns>
        /// LovRequest
        /// </returns>
        /// <remarks>
        ///  </remarks>
        public static LovRequest GetMembershipTypeRequest()
        {
            ServiceProxies.Information.LovRequest request = new LovRequest();

            LovQueryType queryType = new LovQueryType();
            string[] queryText = new string[1];
            queryText[0] = "MEMBERSHIPTYPE";

            queryType.Text = queryText;
            request.Item = queryType;

            return request;
        }

        public static LovRequest GetUserInterestTypeRequest()
        {
            var request = new LovRequest();

            var queryType = new LovQueryType2 { LovIdentifier = "PREFERENCEVALUE" };
            var queryQualifierType = new LovQueryQualifierType[1];
            queryQualifierType[0] = new LovQueryQualifierType { qualifierType = "PREFERENCETYPE", Value = "INTERESTS" };

            queryType.LovQueryQualifier = queryQualifierType;
            request.Item = queryType;

            return request;
        }
        /// <summary>
        /// Gets configured titles
        /// </summary>
        /// <returns></returns> 
        public static LovRequest GetConfiguredTitlesRequest()
        {
            ServiceProxies.Information.LovRequest request = new LovRequest();

            LovQueryType queryType = new LovQueryType();
            string[] queryText = new string[1];
            queryText[0] = "TITLE";

            queryType.Text = queryText;
            request.Item = queryType;

            return request;
        }

        #endregion

        /// <summary>
        /// Gets ignore bookings.
        /// </summary>
        /// <param name="ignoreDetailsEntity"></param>
        /// <returns></returns>
        internal static IgnoreBookingRequest GetIgnoreBookingRequest(IgnoreBookingEntity ignoreDetailsEntity)
        {
            IgnoreBookingRequest request = new IgnoreBookingRequest();

            ServiceProxies.Reservation.UniqueID uniqueID = new ServiceProxies.Reservation.UniqueID();
            uniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            uniqueID.Value = ignoreDetailsEntity.ReservationNumber;
            request.ConfirmationNumber = uniqueID;

            ServiceProxies.Reservation.UniqueID uniqueLegID = new ServiceProxies.Reservation.UniqueID();
            uniqueLegID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            uniqueLegID.Value = ignoreDetailsEntity.LegNumber;
            request.LegNumber = uniqueLegID;

            ServiceProxies.Reservation.HotelReference hotelReference = new ServiceProxies.Reservation.HotelReference();
            hotelReference.chainCode = ignoreDetailsEntity.ChainCode;
            hotelReference.hotelCode = ignoreDetailsEntity.HotelCode;
            request.HotelReference = hotelReference;

            return request;
        }

        /// <summary>
        /// Gets confirmed bookings.
        /// </summary>
        /// <param name="confirmBookingEntity"></param>
        /// <returns></returns>
        internal static ConfirmBookingRequest GetConfirmBookingRequest(ConfirmBookingEntity confirmBookingEntity)
        {
            ConfirmBookingRequest request = new ConfirmBookingRequest();

            ServiceProxies.Reservation.UniqueID uniqueID = new ServiceProxies.Reservation.UniqueID();
            uniqueID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            uniqueID.Value = confirmBookingEntity.ReservationNumber;
            request.ConfirmationNumber = uniqueID;

            ServiceProxies.Reservation.UniqueID uniqueLegID = new ServiceProxies.Reservation.UniqueID();
            uniqueLegID.type = ServiceProxies.Reservation.UniqueIDType.INTERNAL;
            uniqueLegID.Value = confirmBookingEntity.LegNumber;
            request.LegNumber = uniqueLegID;

            ServiceProxies.Reservation.HotelReference hotelReference = new ServiceProxies.Reservation.HotelReference();
            hotelReference.chainCode = confirmBookingEntity.ChainCode;
            hotelReference.hotelCode = confirmBookingEntity.HotelCode;
            request.HotelReference = hotelReference;

            return request;
        }
    }
}