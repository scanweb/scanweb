//  Description					: Domain class for Name OWS Calls.			              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Himansu Senapati / Santhosh Yamsani / Priya Singh       //
//  Author email id				:                           							  //
//  Creation Date				: 06th November  2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: Raj k. Marandi										  //
//	Last Modified Date			: 15th Dec 2008								              //
////////////////////////////////////////////////////////////////////////////////////////////

#region System NameSpaces
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Services.Protocols;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Membership;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Name;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Security;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using System.Linq;
using System.Web;

#endregion

namespace Scandic.Scanweb.BookingEngine.Domain
{
    /// <summary>
    /// Domain class for Loyalty functionality
    /// </summary>
    public class LoyaltyDomain : ILoyaltyDomain
    {
        #region Private Variable

        /// <summary>
        /// Fix done for artifact no - artf713949
        /// Indicates the primaryLanguageID for be send to NameService header
        /// Defaulted to English "E"
        /// </summary>
        private string primaryLanguageID = LanguageConstant.LANGUAGE_ENGLISH_CODE;

        #endregion

        #region Property

        /// <summary>
        /// Get/Set the Primary Language ID.
        /// Fix done for artifact no - artf713949
        /// </summary>
        public string PrimaryLanguageID
        {
            get { return primaryLanguageID; }
            set { primaryLanguageID = value; }
        }

        #endregion

        /// <summary>
        /// This is method to update the membership with promotion code.
        /// </summary>
        /// <param name="membershipOperaId">
        /// Membership Opera ID
        /// </param>
        /// <param name="promoCode">
        /// Promotion code to be applied to this membership number.
        /// </param>
        /// <returns>
        /// AddPromoSubscriptionResponse
        /// </returns>
        /// <remarks>
        /// </remarks>
        public AddPromoSubscriptionResponse UpdateMembershipWithPromoCode(string membershipOperaId, string promoCode)
        {
            if (IsPromoCodeAlreadyAdded(membershipOperaId, promoCode))
            {
                string errorText = "Error: Promotion code for this membership already exists , ";
                OWSException owsException = new OWSException(errorText);
                throw owsException;
            }
            AddPromoSubscriptionResponse response = null;
            AddPromoSubscriptionRequest request = OWSRequestConverter.GetPromoSubscriptionRequest(membershipOperaId,
                                                                                                  promoCode);
            MembershipService service = OWSUtility.GetMembershipService();
            try
            {
                response = service.AddPromoSubscription(request);
                if ((response != null) && (response.Result != null))
                {
                    if (response.Result.resultStatusFlag == ServiceProxies.Membership.ResultStatusFlag.FAIL)
                    {
                        AppLogger.LogInfoMessage("resultStatusFlag: failed");

                        Hashtable exceptionData = new Hashtable();
                        exceptionData.Add("REQ:Membership Opera ID", request.PromoSubscription.MembershipId);
                        exceptionData.Add("REQ:Promotion Code", request.PromoSubscription.Promotion.Code);
                        exceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                        OWSUtility.RaiseAddPromoSubscriptionResponse(response, promoCode, exceptionData);
                    }
                }
            }
            catch (SoapException soapException)
            {
                throw new OWSException(soapException.Message, soapException);
            }
            return response;
        }

        #region FetchMembershipTransactions

        /// <summary>
        /// OWS method to fetch all transactions of a member.
        /// </summary>
        /// <param name="membershipOperaId"></param>
        /// <returns></returns>
        public FetchMembershipTransactionsResponse
            FetchMembershipTransactions(string membershipOperaId)
        {
            try
            {
                MembershipService service = OWSUtility.GetMembershipService();

                FetchMembershipTransactionsRequest request =
                    OWSRequestConverter.GetFetchMembershipTransactionsRequest(membershipOperaId);

                AppLogger.LogInfoMessage("Fetching the response: service.FetchMembershipTransactions(request)");
                FetchMembershipTransactionsResponse response = service.FetchMembershipTransactions(request);
                AppLogger.LogInfoMessage("Received the response from FetchMembershipTransactions");

                if (response.Result.resultStatusFlag != ServiceProxies.Membership.ResultStatusFlag.FAIL)
                {
                    AppLogger.LogInfoMessage("ServiceProxies.Membership.ResultStatusFlag: Success");
                    return response;
                }
                else
                {
                    AppLogger.LogInfoMessage("ServiceProxies.Membership.ResultStatusFlag: Failed");
                    Hashtable exceptionData = new Hashtable();
                    exceptionData.Add("REQ:membershipOperaId: ", membershipOperaId);
                    OWSUtility.RaiseMembershipOWSException(response, exceptionData);
                }
            }
            catch (SoapException SoapEx)
            {
                OWSUtility.RaiseOWSException(SoapEx.Message, SoapEx);
            }
            return null;
        }

        #endregion FetchMembershipTransactions

        /// <summary>
        /// Call method to Fetch Privacy Option from OWS
        /// </summary>
        /// <param name="nameId">
        /// NameID
        /// </param>
        /// <param name="informationEmailType">
        /// News Letter option type
        /// </param>
        /// <returns>
        /// True if option Selected or false if not selected
        /// </returns>
        public bool FetchPrivacyOptionUserProfile(string nameId, string informationEmailType, FetchProfileResponse response)
        {
            int responseLength = response.ProfileDetails.Privacy.Length;
            ServiceProxies.Name.PrivacyOptionType[] privacyOption = new ServiceProxies.Name.PrivacyOptionType[responseLength];
            privacyOption = response.ProfileDetails.Privacy;
            bool returnVal = false;
            if (response != null && response.ProfileDetails != null && response.ProfileDetails.Privacy != null)
            {
                if (privacyOption != null && privacyOption.Length > 0)
                {
                    if (string.Equals(informationEmailType, AppConstants.SCANDIC_EMAIL, StringComparison.InvariantCultureIgnoreCase))
                    {
                        for (int count = 0; count < privacyOption.Length; count++)
                        {
                            if (privacyOption[count].OptionType == ServiceProxies.Name.PrivacyOptionTypeOptionType.Promotions)
                            {
                                if (privacyOption[count].OptionValue == ServiceProxies.Name.PrivacyOptionTypeOptionValue.YES)
                                    returnVal = true;
                                else
                                    returnVal = false;
                            }
                        }
                    }
                    else if (string.Equals(informationEmailType, AppConstants.SCANDIC_PARTNER_EMAIL, StringComparison.InvariantCultureIgnoreCase))
                    {
                        for (int count = 0; count < privacyOption.Length; count++)
                        {
                            if (privacyOption[count].OptionType == ServiceProxies.Name.PrivacyOptionTypeOptionType.ThirdParties)
                            {
                                if (privacyOption[count].OptionValue == ServiceProxies.Name.PrivacyOptionTypeOptionValue.YES)
                                    returnVal = true;
                                else
                                    returnVal = false;
                            }
                        }
                    }
                }
            }
            return returnVal;
        }

        /// <summary>
        /// Fetch Name Details
        /// </summary>
        /// <param name="nameID">
        /// Name ID of the user
        /// </param>
        /// <returns>
        /// GuestInformationEntity
        /// </returns>
        public UserProfileEntity FetchNameUserProfile(string nameID)
        {
            UserProfileEntity userProfileEntity = null;
            try
            {
                FetchNameResponse response = FetchNameService(nameID);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    userProfileEntity = new UserProfileEntity();
                    userProfileEntity.NameTitle = (response.PersonName.nameTitle != null &&
                                                   response.PersonName.nameTitle.Length > 0)
                                                      ? response.PersonName.nameTitle[0]
                                                      : string.Empty;
                    userProfileEntity.FirstName = response.PersonName.firstName;
                    userProfileEntity.LastName = response.PersonName.lastName;
                    string gender = response.Gender.ToString();

                    if (gender == AppConstants.MALE_FROMOPERA)
                        userProfileEntity.Gender = UserGender.MALE;
                    else if (gender == AppConstants.FEMALE_FROMOPERA)
                        userProfileEntity.Gender = UserGender.FEMALE;

                    //BUG FIX: artf638381
                    if (response.BirthdateSpecified == true)
                    {
                        DateTime dateOfBirth = response.Birthdate;
                        DateOfBirthEntity dateOfBirthEntity = new DateOfBirthEntity(dateOfBirth);
                        userProfileEntity.DateOfBirth = dateOfBirthEntity;
                    }

                    if (response.NativeName != null)
                    {
                        userProfileEntity.PreferredLanguage = response.NativeName.languageCode;

                        //Added as part of SCANAM-537
                        userProfileEntity.NativeFirstName = response.NativeName.firstName;
                        userProfileEntity.NativeLastName = response.NativeName.lastName;
                    }
                }
                else
                {
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseFetchNameOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                throw new OWSException(soapException.Message, soapException);
            }
            return userProfileEntity;
        }

        public UserProfileEntity FetchUserProfile(string nameID)
        {
            UserProfileEntity userProfileEntity = null;
            PhoneDetailsEntity phoneDetailsEntity = null;
            try
            {
                FetchProfileResponse response = FetchProfileService(nameID);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    userProfileEntity = new UserProfileEntity();

                    //Name Details
                    ServiceProxies.Name.Customer customer = response.ProfileDetails.Item as ServiceProxies.Name.Customer;
                    if (customer != null)
                    {
                        if (customer.PersonName != null)
                        {
                            userProfileEntity.NameTitle = (customer.PersonName.nameTitle != null && customer.PersonName.nameTitle.Length > 0) ? customer.PersonName.nameTitle[0] : string.Empty;
                            userProfileEntity.FirstName = customer.PersonName.firstName;
                            userProfileEntity.LastName = customer.PersonName.lastName;
                            string gender = customer.gender.ToString();
                            if (gender == AppConstants.MALE_FROMOPERA)
                                userProfileEntity.Gender = UserGender.MALE;
                            else if (gender == AppConstants.FEMALE_FROMOPERA)
                                userProfileEntity.Gender = UserGender.FEMALE;

                            if (customer.birthDateSpecified == true)
                                userProfileEntity.DateOfBirth = new DateOfBirthEntity(customer.birthDate);

                            if (customer.NativeName != null)
                            {
                                userProfileEntity.PreferredLanguage = customer.NativeName.languageCode;
                                userProfileEntity.NativeFirstName = customer.NativeName.firstName;
                                userProfileEntity.NativeLastName = customer.NativeName.lastName;
                            }
                            else if (!string.IsNullOrEmpty(response.ProfileDetails.languageCode))
                                userProfileEntity.PreferredLanguage = response.ProfileDetails.languageCode;
                        }
                        else if (customer.NativeName != null)
                        {
                            userProfileEntity = FetchNameUserProfile(nameID);
                            //userProfileEntity.NameTitle = (customer.NativeName.nameTitle != null && customer.NativeName.nameTitle.Length > 0) ? customer.NativeName.nameTitle[0] : string.Empty;
                            //userProfileEntity.FirstName = customer.NativeName.firstName;
                            //userProfileEntity.LastName = customer.NativeName.lastName;
                        }
                        
                        if (!string.IsNullOrEmpty(response.ProfileDetails.languageCode))
                            userProfileEntity.PreferredLanguage = response.ProfileDetails.languageCode;

                        //Address Details
                        userProfileEntity = getAddressDetails(response, userProfileEntity);

                        //Phone Details
                        List<PhoneDetailsEntity> phoneList = new List<PhoneDetailsEntity>();
                        if (response.ProfileDetails.Phones != null)
                        {
                            int totalPhones = response.ProfileDetails.Phones.Length;
                            ServiceProxies.Name.NamePhone[] phones = response.ProfileDetails.Phones;
                            bool flagMobileInserted = false;
                            bool FirstMobileInserted = false;
                            bool flagPhoneInserted = false;
                            bool flagFaxInserted = false;
                            //Loop to Fetch Home Phone & Fax
                            for (int phCount = 0; phCount < totalPhones; phCount++)
                            {
                                if (!flagPhoneInserted &&
                                    string.Equals(phones[phCount].phoneType, PhoneContants.PHONETYPE_HOME, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    //Fetch the Home Phone Number
                                    //Create the Home PhoneDetails Entity 
                                    PhoneDetailsEntity homePhone = new PhoneDetailsEntity();
                                    homePhone.Number = phones[phCount].Item as string;
                                    homePhone.OperaId = phones[phCount].operaId;
                                    homePhone.PhoneType = PhoneContants.PHONETYPE_HOME;
                                    flagPhoneInserted = true;
                                    //Add the Home phonedetails Entity to the Phone List
                                    phoneList.Add(homePhone);

                                }
                                if (phones[phCount].primary && flagFaxInserted == false &&
                                    string.Equals(phones[phCount].phoneType, PhoneContants.PHONETYPE_FAX, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    //Create the Fax PhoneDetails Entity 
                                    PhoneDetailsEntity faxPhone = new PhoneDetailsEntity();
                                    faxPhone.Number = phones[phCount].Item as string;
                                    faxPhone.OperaId = phones[phCount].operaId;
                                    faxPhone.PhoneType = PhoneContants.PHONETYPE_FAX;
                                    flagFaxInserted = true;
                                    //Add the Fax phonedetails Entity to the Phone List
                                    phoneList.Add(faxPhone);
                                }
                            }

                            //Loop to Fetch the Primary mobile Phone
                            //Or the top most mobile phone from the Phone list
                            PhoneDetailsEntity primaryMobile = null;
                            PhoneDetailsEntity nonPrimaryMobile = null;
                            for (int phCount = 0; phCount < totalPhones; phCount++)
                            {
                                if (!flagMobileInserted &&
                                    string.Equals(phones[phCount].phoneType, PhoneContants.PHONETYPE_MOBILE, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    //Fetch the Mobile Number
                                    if (phones[phCount].primary)
                                    {
                                        //Create the Primary Mobile Phone Entity
                                        primaryMobile = new PhoneDetailsEntity();
                                        primaryMobile.Number = phones[phCount].Item as string;
                                        primaryMobile.OperaId = phones[phCount].operaId;
                                        primaryMobile.PhoneType = PhoneContants.PHONETYPE_MOBILE;
                                        flagMobileInserted = true;
                                        break;
                                    }
                                    else if (!FirstMobileInserted)
                                    {
                                        nonPrimaryMobile = new PhoneDetailsEntity();
                                        nonPrimaryMobile.Number = phones[phCount].Item as string;
                                        nonPrimaryMobile.OperaId = phones[phCount].operaId;
                                        nonPrimaryMobile.PhoneType = PhoneContants.PHONETYPE_MOBILE;
                                        FirstMobileInserted = true;
                                    }
                                }
                            }

                            //Add the Mobile Phone to the Phone List
                            //Check if the Primary Mobile Phone is available. If true, add it to the list
                            if (primaryMobile != null)
                            {
                                phoneList.Add(primaryMobile);
                            }
                            else if (nonPrimaryMobile != null)
                            {
                                //If Primary Mobile phone is not available. Check if any other Mobile is available.
                                //If true, add it to the list
                                // AMS | BUG Fix: artf757889 | HD032895
                                phoneList.Add(nonPrimaryMobile);
                            }
                        }
                        if (phoneList != null && phoneList.Count > 0)
                        {
                            int totalPhoneCount = phoneList.Count;

                            for (int phCount = 0; phCount < totalPhoneCount; phCount++)
                            {
                                phoneDetailsEntity = phoneList[phCount] as PhoneDetailsEntity;
                                if (phoneDetailsEntity != null)
                                {
                                    if (string.Equals(phoneDetailsEntity.PhoneType, PhoneContants.PHONETYPE_HOME, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        userProfileEntity.HomePhoneOperaID = phoneDetailsEntity.OperaId;
                                        userProfileEntity.HomePhone = phoneDetailsEntity.Number;
                                    }
                                    else if (string.Equals(phoneDetailsEntity.PhoneType, PhoneContants.PHONETYPE_MOBILE, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        userProfileEntity.MobilePhoneOperaID = phoneDetailsEntity.OperaId;
                                        userProfileEntity.MobilePhone = phoneDetailsEntity.Number;
                                    }
                                    else if (string.Equals(phoneDetailsEntity.PhoneType, PhoneContants.PHONETYPE_FAX, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        userProfileEntity.FaxOperaID = phoneDetailsEntity.OperaId;
                                        userProfileEntity.Fax = phoneDetailsEntity.Number;
                                    }
                                }
                            }
                        }
                        //Email and NobBusinees Email Details
                        if (response.ProfileDetails.EMails != null)
                        {
                            ServiceProxies.Name.NameEmail[] emailList = response.ProfileDetails.EMails;
                            int totalEmails = emailList.Length;
                            for (int emailCount = 0; emailCount < totalEmails; emailCount++)
                            {
                                if (emailList[emailCount].primary)
                                {
                                    userProfileEntity.EmailOperaId = emailList[emailCount].operaId;
                                    userProfileEntity.EmailID = emailList[emailCount].Value;
                                }
                                if (string.Equals(emailList[emailCount].emailType, AppConstants.EMAILTYPE_EMAIL, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    userProfileEntity.NonBusinessEmailOperaId = emailList[emailCount].operaId;
                                    userProfileEntity.NonBusinessEmailID = emailList[emailCount].Value;
                                }
                            }
                        }
                        // Creditcard Details
                        Dictionary<string, CreditCardEntity> creditCards = new Dictionary<string, CreditCardEntity>();
                        ServiceProxies.Name.NameCreditCard[] creditCardList = response.ProfileDetails.CreditCards;
                        if (creditCardList != null)
                        {
                            int totalCreditCards = creditCardList.Length;
                            //creditCards = new CreditCardEntity[totalCreditCards];
                            for (int cardCount = 0; cardCount < totalCreditCards; cardCount++)
                            {

                                ExpiryDateEntity expiryDate = new ExpiryDateEntity(creditCardList[cardCount].expirationDate.Month,
                                    creditCardList[cardCount].expirationDate.Year);

                                CreditCardEntity creditCardsEnt =
                                   new CreditCardEntity(creditCardList[cardCount].cardHolderName, creditCardList[cardCount].cardCode,
                                    creditCardList[cardCount].cardNumber, expiryDate, creditCardList[cardCount].displaySequence);
                                creditCards.Add(creditCardList[cardCount].operaId.ToString(), creditCardsEnt);
                            }
                        }
                        if (creditCards != null && creditCards.Count > 0)
                        {
                            KeyValuePair<string, CreditCardEntity> creditCard =
                                creditCards.OrderBy(q => q.Value.DisplaySequence).FirstOrDefault();
                            userProfileEntity.CreditCard = creditCard.Value;
                            userProfileEntity.CreditCardOperaId = long.Parse(creditCard.Key);
                        }

                        //Partner Preferences
                        List<PartnershipProgram> partnershipProgramList = new List<PartnershipProgram>();
                        PartnershipProgram partnershipProgram;
                        ServiceProxies.Name.NameMembership[] guestcardlist = response.ProfileDetails != null ?
                                                                             response.ProfileDetails.Memberships : null;
                        userProfileEntity.GuestCardList = new List<MembershipEntity>();
                        if (guestcardlist != null && guestcardlist.Length > 0)
                        {

                            for (int count = 0; count < guestcardlist.Length; count++)
                            {
                                MembershipEntity membershipEntity = null;
                                if (guestcardlist[count].membershipLevel != AppConstants.MEMBERSHIPLEVEL_1STFLOOR && guestcardlist[count].membershipLevel != AppConstants.MEMBERSHIPLEVEL_2NDFLOOR &&
                                    guestcardlist[count].membershipLevel != AppConstants.MEMBERSHIPLEVEL_3RDFLOOR && guestcardlist[count].membershipLevel != AppConstants.MEMBERSHIPLEVEL_TOPFLOOR &&
                                    guestcardlist[count].membershipLevel != AppConstants.MEMBERSHIPLEVEL_BYINVIT)
                                {
                                    if (!OWSUtility.IsPartnerCardExcluded(guestcardlist[count].membershipType))
                                    {
                                        partnershipProgram = new PartnershipProgram();
                                        partnershipProgram.PartnerProgram = guestcardlist[count].membershipType;
                                        partnershipProgram.PartnerAccountNo = guestcardlist[count].membershipNumber;
                                        partnershipProgram.DisplaySequence = guestcardlist[count].displaySequence;
                                        partnershipProgram.OperaId = guestcardlist[count].operaId;
                                        partnershipProgram.Operation = OperationType.SELECT;
                                        partnershipProgramList.Add(partnershipProgram);
                                    }
                                }

                                if (string.Equals(guestcardlist[count].membershipType, AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    membershipEntity = new MembershipEntity();
                                    membershipEntity.OperaId = guestcardlist[count].operaId.ToString();
                                    membershipEntity.CurrentPoints = guestcardlist[count].currentPoints;
                                    membershipEntity.EffectiveDate = guestcardlist[count].effectiveDate.ToString();
                                    membershipEntity.ExpirationDate = guestcardlist[count].expirationDate.ToString();
                                    membershipEntity.MembershipLevel = guestcardlist[count].membershipLevel;
                                    membershipEntity.MembershipNumber = guestcardlist[count].membershipNumber;
                                    membershipEntity.MembershipType = guestcardlist[count].membershipType;
                                    membershipEntity.Status = guestcardlist[count].status;

                                    if (guestcardlist[count].awardPointsToExpires != null)
                                    {
                                        AwardPointsToExpire[] awdExpPoints =
                                        GetSortedOrderOfAwardsPointOnDateAsc(guestcardlist[count].awardPointsToExpires);
                                        int i = 0;
                                        ExpiryPointEntity expiryPointOfMember = null;
                                        foreach (AwardPointsToExpire awdExpPoint in awdExpPoints)
                                        {

                                            if (awdExpPoint != null)
                                            {
                                                expiryPointOfMember = new ExpiryPointEntity();
                                                expiryPointOfMember.ExpiryDate = awdExpPoint.expirationDate;
                                                expiryPointOfMember.ExpiryPoint = awdExpPoint.expireByDate;
                                                expiryPointOfMember.TotalPoint = awdExpPoint.totalToExpire;
                                                if (i == 0)
                                                {
                                                    if (string.IsNullOrEmpty(expiryPointOfMember.ExpiryPoint.ToString()) ||
                                                        (expiryPointOfMember.ExpiryPoint == 0.0) ||
                                                        (expiryPointOfMember.ExpiryPoint == 0))
                                                    {
                                                        if (Convert.ToDateTime(expiryPointOfMember.ExpiryDate) >= DateTime.Today)
                                                        {
                                                            expiryPointOfMember.ExpiryPoint = expiryPointOfMember.TotalPoint;
                                                            membershipEntity.ExpiryPointCollection.Add(expiryPointOfMember);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        membershipEntity.ExpiryPointCollection.Add(expiryPointOfMember);
                                                    }
                                                }
                                                else
                                                {
                                                    if (!string.IsNullOrEmpty(expiryPointOfMember.ExpiryPoint.ToString()) &&
                                                        (expiryPointOfMember.ExpiryPoint != 0.0) &&
                                                        (expiryPointOfMember.ExpiryPoint != 0))
                                                    {
                                                        membershipEntity.ExpiryPointCollection.Add(expiryPointOfMember);
                                                    }
                                                }
                                                i++;
                                            }

                                        }
                                    }

                                }
                                if (membershipEntity != null)
                                    userProfileEntity.GuestCardList.Add(membershipEntity);
                            }

                        }

                        partnershipProgramList.Sort(new PartnershipProgramComparer());
                        userProfileEntity.PartnerProgramList = partnershipProgramList;

                        //User Preference Details
                        ServiceProxies.Name.Preference[] preferencesList = response.ProfileDetails.Preferences;
                        if (preferencesList != null)
                        {
                            userProfileEntity.UserPreference = CreateUserPreference(preferencesList);
                            userProfileEntity.Interests = CreateUserInterestObject(preferencesList);
                        }
                        //Privacy Option Details
                        userProfileEntity.ScandicEmail = FetchPrivacyOptionUserProfile(nameID, AppConstants.SCANDIC_EMAIL, response);
                        userProfileEntity.ThridPartyEmail = FetchPrivacyOptionUserProfile(nameID, AppConstants.SCANDIC_PARTNER_EMAIL, response);
                    }
                    else
                    {
                        AppLogger.LogInfoMessage("resultStatusFlag: failed");
                        OWSUtility.RaiseFetchProfileOWSException(response);
                    }
                }
            }
            catch (SoapException soapException)
            {
                throw new OWSException(soapException.Message, soapException);
            }
            return userProfileEntity;
        }
        private UserProfileEntity getAddressDetails(FetchProfileResponse response, UserProfileEntity userProfileEntity)
        {
            var userProfile = userProfileEntity;

            if (response != null && response.ProfileDetails != null && response.ProfileDetails.Addresses != null)
            {
                int cnt = response.ProfileDetails.Addresses.Length;
                for (int i = 0; i < cnt; i++)
                {
                    ServiceProxies.Name.NameAddress nameAddress = response.ProfileDetails.Addresses[i];

                    if (nameAddress.primary)
                    {
                        userProfile.AddressType = nameAddress.addressType;

                        if (nameAddress.AddressLine != null)
                        {
                            if (nameAddress.AddressLine.Length > 0)
                                userProfile.AddressLine1 = nameAddress.AddressLine[0];
                            if (nameAddress.AddressLine.Length > 1)
                                userProfile.AddressLine2 = nameAddress.AddressLine[1];
                        }
                        userProfile.City = nameAddress.cityName;
                        userProfile.Country = nameAddress.countryCode;
                        userProfile.PostCode = nameAddress.postalCode;
                        userProfile.AddressOperaID = nameAddress.operaId;
                        break;
                    }
                }
            }
            return userProfile;
        }

        /// <summary>
        /// Fethes the primary address of a profile for a specific language passed.
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="primaryLangID"></param>
        /// <returns></returns>
        public UserProfileEntity GetAddressForLanguage(string nameID, string primaryLangID)
        {
            UserProfileEntity userProfileEntity = null;

            FetchAddressListResponse response = FetchAddressNameService(nameID, primaryLangID);

            if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
            {
                if (response.NameAddressList != null)
                {
                    int totalAddressCount = response.NameAddressList.Length;

                    for (int addressCount = 0; addressCount < totalAddressCount; addressCount++)
                    {
                        ServiceProxies.Name.NameAddress nameAddress = response.NameAddressList[addressCount];

                        if (nameAddress.primary == true)
                        {
                            userProfileEntity = new UserProfileEntity();
                            userProfileEntity.AddressType = nameAddress.addressType.ToString();

                            if (nameAddress.AddressLine != null)
                            {
                                if (nameAddress.AddressLine.Length > 0)
                                {
                                    userProfileEntity.AddressLine1 = nameAddress.AddressLine[0];
                                }
                                if (nameAddress.AddressLine.Length > 1)
                                {
                                    userProfileEntity.AddressLine2 = nameAddress.AddressLine[1];
                                }
                            }
                            userProfileEntity.City = nameAddress.cityName;
                            userProfileEntity.Country = nameAddress.countryCode;
                            userProfileEntity.PostCode = nameAddress.postalCode;
                            userProfileEntity.AddressOperaID = nameAddress.operaId;
                            break;
                        }
                    }
                }
            }
            else
            {
                AppLogger.LogInfoMessage("resultStatusFlag: failed");
                OWSUtility.RaiseFetchAddressOWSException(response);
            }
            return userProfileEntity;
        }

        /// <summary>
        /// Fetches the address of a profile which is primary and can be associated with any language.
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        public UserProfileEntity FetchAddressUserProfile(string nameID)
        {
            UserProfileEntity userProfileEntity = new UserProfileEntity();

            String[] languageArray = new string[7];
            languageArray[0] = LanguageConstant.LANGUAGE_ENGLISH_CODE;
            languageArray[1] = LanguageConstant.LANGUAGE_DANISH_CODE;
            languageArray[2] = LanguageConstant.LANGUAGE_FINNISH_CODE;
            languageArray[3] = LanguageConstant.LANGUAGE_NORWEGIAN_CODE;
            languageArray[4] = LanguageConstant.LANGUAGE_SWEDISH_CODE;
            languageArray[5] = LanguageConstant.LANGUAGE_GERMAN_CODE;
            languageArray[6] = LanguageConstant.LANGUAGE_RUSSIA_CODE;

            userProfileEntity = GetAddressForLanguage(nameID, this.primaryLanguageID);

            if (userProfileEntity == null)
            {
                for (int ctr = 0; ctr < languageArray.Length; ctr++)
                {
                    if (languageArray[ctr] != this.primaryLanguageID)
                    {
                        userProfileEntity = new UserProfileEntity();
                        userProfileEntity = GetAddressForLanguage(nameID, languageArray[ctr]);

                        if (userProfileEntity != null)
                            break;
                    }
                }
            }

            return userProfileEntity;
        }

        /// <summary>
        /// Fetch the Phone Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// List of PhoneDetailsEntity
        /// </returns>        
        public List<PhoneDetailsEntity> FetchPhoneUserProfile(string nameID)
        {
            List<PhoneDetailsEntity> phoneList = new List<PhoneDetailsEntity>();
            try
            {
                FetchPhoneListResponse response = FetchPhoneNameService(nameID);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    if (response.NamePhoneList != null)
                    {
                        int totalPhones = response.NamePhoneList.Length;
                        ServiceProxies.Name.NamePhone[] phones = response.NamePhoneList;
                        bool flagMobileInserted = false;
                        bool FirstMobileInserted = false;
                        bool flagPhoneInserted = false;
                        bool flagFaxInserted = false;
                        for (int phCount = 0; phCount < totalPhones; phCount++)
                        {
                            if (!flagPhoneInserted &&
                                string.Equals(phones[phCount].phoneType, PhoneContants.PHONETYPE_HOME, StringComparison.InvariantCultureIgnoreCase))
                            {
                                PhoneDetailsEntity homePhone = new PhoneDetailsEntity();
                                homePhone.Number = phones[phCount].Item as string;
                                homePhone.OperaId = phones[phCount].operaId;
                                homePhone.PhoneType = PhoneContants.PHONETYPE_HOME;
                                flagPhoneInserted = true;
                                phoneList.Add(homePhone);
                            }
                            if (!flagFaxInserted && phones[phCount].primary &&
                                string.Equals(phones[phCount].phoneType, PhoneContants.PHONETYPE_FAX, StringComparison.InvariantCultureIgnoreCase))
                            {
                                PhoneDetailsEntity faxPhone = new PhoneDetailsEntity();
                                faxPhone.Number = phones[phCount].Item as string;
                                faxPhone.OperaId = phones[phCount].operaId;
                                faxPhone.PhoneType = PhoneContants.PHONETYPE_FAX;
                                flagFaxInserted = true;
                                phoneList.Add(faxPhone);
                            }
                        }
                        PhoneDetailsEntity primaryMobile = null;
                        PhoneDetailsEntity nonPrimaryMobile = null;
                        for (int phCount = 0; phCount < totalPhones; phCount++)
                        {
                            if (!flagMobileInserted)
                            {
                                if (string.Equals(phones[phCount].phoneType, PhoneContants.PHONETYPE_MOBILE, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    if (phones[phCount].primary)
                                    {
                                        primaryMobile = new PhoneDetailsEntity();
                                        primaryMobile.Number = phones[phCount].Item as string;
                                        primaryMobile.OperaId = phones[phCount].operaId;
                                        primaryMobile.PhoneType = PhoneContants.PHONETYPE_MOBILE;
                                        flagMobileInserted = true;
                                        break;
                                    }
                                    else if (!FirstMobileInserted)
                                    {
                                        nonPrimaryMobile = new PhoneDetailsEntity();
                                        nonPrimaryMobile.Number = phones[phCount].Item as string;
                                        nonPrimaryMobile.OperaId = phones[phCount].operaId;
                                        nonPrimaryMobile.PhoneType = PhoneContants.PHONETYPE_MOBILE;
                                        FirstMobileInserted = true;
                                    }
                                }
                            }
                        }
                        if (primaryMobile != null)
                        {
                            phoneList.Add(primaryMobile);
                        }
                        else if (nonPrimaryMobile != null)
                        {
                            phoneList.Add(nonPrimaryMobile);
                        }
                    }
                }
                else
                {
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseFetchPhoneOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                throw new OWSException(soapException.Message, soapException);
            }
            return phoneList;
        }


        /// <summary>
        /// To fetch the user personal interests object
        /// Added by Vaibhav- Merch R4 - Itr 2
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        public UserInterestsEntity[] FetchUserInterestsListUserProfile(string nameID)
        {
            UserInterestsEntity[] userInterest = null;
            try
            {
                FetchPreferenceListResponse response = FetchPreferencesListNameService(nameID);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    ServiceProxies.Name.Preference[] preferencesList = response.PreferenceList;
                    if (preferencesList != null)
                    {
                        userInterest = CreateUserInterestObject(preferencesList);
                    }
                }
                else
                {
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaisePreferenceListException(response);
                }
            }
            catch (SoapException soapException)
            {
                throw new OWSException(soapException.Message, soapException);
            }
            return userInterest;


        }


        /// <summary>
        /// To return the user interest list object parsed from the response object.
        /// Added by Vaibhav- Merch R4 - Itr 2
        /// </summary>
        /// <param name="interestList"></param>
        /// <returns></returns>
        private UserInterestsEntity[] CreateUserInterestObject(ServiceProxies.Name.Preference[] interestList)
        {

            ArrayList userInterestList = new ArrayList();
            for (var preferCount = 0; preferCount < interestList.Length; preferCount++)
            {

                if (interestList[preferCount].preferenceType == UserInterestsConstants.INTERESTS)
                {

                    var userInterestFamily = new UserInterestsEntity
                                                 {
                                                     RequestType = UserInterestsConstants.INTERESTS,
                                                     RequestValue = interestList[preferCount].preferenceValue
                                                 };
                    userInterestList.Add(userInterestFamily);
                }
            }
            return userInterestList.ToArray(typeof(UserInterestsEntity)) as UserInterestsEntity[];
        }
        /// <summary>
        /// Fetch the preference List Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// FetchPreferenceListResponse
        /// </returns>
        public UserPreferenceEntity[] FetchPreferencesListUserProfile(string nameID)
        {
            UserPreferenceEntity[] userPreference = null;
            try
            {
                FetchPreferenceListResponse response = FetchPreferencesListNameService(nameID);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    ServiceProxies.Name.Preference[] preferencesList = response.PreferenceList;
                    if (preferencesList != null)
                    {
                        userPreference = CreateUserPreference(preferencesList);
                    }
                }
                else
                {
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaisePreferenceListException(response);
                }
            }
            catch (SoapException soapException)
            {
                throw new OWSException(soapException.Message, soapException);
            }
            return userPreference;
        }

        /// <summary>
        /// Insert the partner program against this profile.
        /// </summary>
        /// <param name="profile">User profile.</param>
        /// <param name="nameID">name id for this profile</param>
        /// <param name="memberName">Member name for this profile.</param>
        private void InsertPartnerProgram(UserProfileEntity profile, string nameID, string memberName)
        {
            int partnerProgramLength = profile.PartnerProgramList.Count;
            int partnerProgramSequenceNo = 3;
            for (int partnerProgramCounter = 0; partnerProgramCounter < partnerProgramLength; partnerProgramCounter++)
            {
                if (!string.IsNullOrEmpty(profile.PartnerProgramList[partnerProgramCounter].PartnerAccountNo))
                {
                    if (0 == partnerProgramCounter)
                    {
                        bool partnerPreferenceInserted = InsertUserGuestCard(nameID,
                                                                             profile.PartnerProgramList[
                                                                                 partnerProgramCounter].PartnerProgram,
                                                                             profile.PartnerProgramList[
                                                                                 partnerProgramCounter].PartnerAccountNo,
                                                                             null, memberName, 2);
                    }
                    else
                    {
                        bool partnerPreferenceInserted = InsertUserGuestCard(nameID,
                                                                             profile.PartnerProgramList[
                                                                                 partnerProgramCounter].PartnerProgram,
                                                                             profile.PartnerProgramList[
                                                                                 partnerProgramCounter].PartnerAccountNo,
                                                                             null, memberName, partnerProgramSequenceNo);
                        partnerProgramSequenceNo++;
                    }
                }
            }
        }

        /// <summary>
        /// Create the user preference Entities
        /// </summary>
        /// <param name="preferenceList">
        /// PreferenceList
        /// </param>
        /// <returns>
        /// UserPreferenceEntity[]
        /// </returns>
        private UserPreferenceEntity[] CreateUserPreference(ServiceProxies.Name.Preference[] preferenceList)
        {
            int totalPreferences = preferenceList.Length;
            UserPreferenceEntity[] userPreferenceEntityList = null;
            ArrayList userPreferenceList = new ArrayList();
            for (int perferCount = 0; perferCount < totalPreferences; perferCount++)
            {
                if (preferenceList[perferCount].preferenceType == UserPreferenceConstants.SPECIALREQUEST)
                {
                    UserPreferenceEntity userPrefEntityForFloor = new UserPreferenceEntity();
                    userPrefEntityForFloor.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                    if (preferenceList[perferCount].preferenceValue == UserPreferenceConstants.LOWERFLOOR)
                    {
                        userPrefEntityForFloor.RequestValue = UserPreferenceConstants.LOWERFLOOR;
                    }
                    else if (preferenceList[perferCount].preferenceValue == UserPreferenceConstants.HIGHFLOOR)
                    {
                        userPrefEntityForFloor.RequestValue = UserPreferenceConstants.HIGHFLOOR;
                    }
                    if (userPrefEntityForFloor.RequestValue != null)
                    {
                        userPreferenceList.Add(userPrefEntityForFloor);
                    }
                    UserPreferenceEntity userPrefEntityForElevator = new UserPreferenceEntity();
                    userPrefEntityForElevator.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                    if (preferenceList[perferCount].preferenceValue == UserPreferenceConstants.AWAYFROMELEVATOR)
                    {
                        userPrefEntityForElevator.RequestValue = UserPreferenceConstants.AWAYFROMELEVATOR;
                    }
                    else if (preferenceList[perferCount].preferenceValue == UserPreferenceConstants.NEARELEVATOR)
                    {
                        userPrefEntityForElevator.RequestValue = UserPreferenceConstants.NEARELEVATOR;
                    }
                    if (userPrefEntityForElevator.RequestValue != null)
                    {
                        userPreferenceList.Add(userPrefEntityForElevator);
                    }

                    UserPreferenceEntity userPrefEntityForSmoking = new UserPreferenceEntity();
                    userPrefEntityForSmoking.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                    if (preferenceList[perferCount].preferenceValue == UserPreferenceConstants.NOSMOKING)
                    {
                        userPrefEntityForSmoking.RequestValue = UserPreferenceConstants.NOSMOKING;
                    }
                    else if (preferenceList[perferCount].preferenceValue == UserPreferenceConstants.SMOKING)
                    {
                        userPrefEntityForSmoking.RequestValue = UserPreferenceConstants.SMOKING;
                    }
                    if (userPrefEntityForSmoking.RequestValue != null)
                    {
                        userPreferenceList.Add(userPrefEntityForSmoking);
                    }
                    if (preferenceList[perferCount].preferenceValue == UserPreferenceConstants.ACCESSIBLEROOM)
                    {
                        UserPreferenceEntity userPreferenceEntity = new UserPreferenceEntity();
                        userPreferenceEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                        userPreferenceEntity.RequestValue = UserPreferenceConstants.ACCESSIBLEROOM;
                        userPreferenceList.Add(userPreferenceEntity);
                    }
                    if (preferenceList[perferCount].preferenceValue == UserPreferenceConstants.ALLERGYROOM)
                    {
                        UserPreferenceEntity userPreferenceEntity = new UserPreferenceEntity();
                        userPreferenceEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                        userPreferenceEntity.RequestValue = UserPreferenceConstants.ALLERGYROOM;
                        userPreferenceList.Add(userPreferenceEntity);
                    }
                    if (preferenceList[perferCount].preferenceValue == UserPreferenceConstants.EARLYCHECKIN)
                    {
                        UserPreferenceEntity userPreferenceEntity = new UserPreferenceEntity();
                        userPreferenceEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                        userPreferenceEntity.RequestValue = UserPreferenceConstants.EARLYCHECKIN;
                        userPreferenceList.Add(userPreferenceEntity);
                    }
                    if (preferenceList[perferCount].preferenceValue == UserPreferenceConstants.LATECHECKOUT)
                    {
                        UserPreferenceEntity userPreferenceEntity = new UserPreferenceEntity();
                        userPreferenceEntity.RequestType = UserPreferenceConstants.SPECIALREQUEST;
                        userPreferenceEntity.RequestValue = UserPreferenceConstants.LATECHECKOUT;
                        userPreferenceList.Add(userPreferenceEntity);
                    }
                }
            }
            if (userPreferenceList != null)
            {
                userPreferenceEntityList =
                    userPreferenceList.ToArray(typeof(UserPreferenceEntity)) as UserPreferenceEntity[];
            }
            return userPreferenceEntityList;
        }

        #region Fetch User Details Methods for Booking Detail

        /// <summary>
        /// It will return all membership details by passing nameId and membership type.
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="membershipType"></param>
        /// <returns></returns>
        public ArrayList FetchGuestCardList(string nameId, string membershipType)
        {
            NameService nameSvc = OWSUtility.GetNameService(primaryLanguageID);

            FetchGuestCardListRequest reqFetchGuestcardList = OWSRequestConverter.GetFetchGuestcardListRequest(nameId);
            FetchGuestCardListResponse resFetchGuestcardList = null;

            ServiceProxies.Name.NameMembership[] membershipList;
            string membershipLevel = string.Empty;
            string membershipDetail = string.Empty;

            ArrayList arrMembershipList = new ArrayList();
            MembershipEntity membershipEntity;
            try
            {
                resFetchGuestcardList = nameSvc.FetchGuestCardList(reqFetchGuestcardList);
                if (resFetchGuestcardList.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    ExpiryPointEntity expiryPointOfMember;
                    membershipList = resFetchGuestcardList.GuestCardList;

                    for (int ctr = 0; ctr < membershipList.Length; ctr++)
                    {
                        ServiceProxies.Name.NameMembership nameMembership = membershipList[ctr];

                        if (nameMembership.membershipType == membershipType)
                        {
                            membershipEntity = new MembershipEntity();
                            membershipEntity.OperaId = nameMembership.operaId.ToString();
                            membershipEntity.CurrentPoints = nameMembership.currentPoints;
                            membershipEntity.EffectiveDate = nameMembership.effectiveDate.ToString();
                            membershipEntity.ExpirationDate = nameMembership.expirationDate.ToString();
                            membershipEntity.MembershipLevel = nameMembership.membershipLevel;
                            membershipEntity.MembershipNumber = nameMembership.membershipNumber;
                            membershipEntity.MembershipType = nameMembership.membershipType;
                            membershipEntity.Status = nameMembership.status;
                            if (nameMembership.awardPointsToExpires != null)
                            {
                                AwardPointsToExpire[] awdExpPoints =
                                    GetSortedOrderOfAwardsPointOnDateAsc(nameMembership.awardPointsToExpires);
                                int i = 0;
                                foreach (AwardPointsToExpire awdExpPoint in awdExpPoints)
                                {
                                    if (awdExpPoint != null)
                                    {
                                        expiryPointOfMember = new ExpiryPointEntity();
                                        expiryPointOfMember.ExpiryDate = awdExpPoint.expirationDate;
                                        expiryPointOfMember.ExpiryPoint = awdExpPoint.expireByDate;
                                        expiryPointOfMember.TotalPoint = awdExpPoint.totalToExpire;
                                        if (i == 0)
                                        {
                                            if (string.IsNullOrEmpty(expiryPointOfMember.ExpiryPoint.ToString()) ||
                                                (expiryPointOfMember.ExpiryPoint == 0.0) ||
                                                (expiryPointOfMember.ExpiryPoint == 0))
                                            {
                                                if (Convert.ToDateTime(expiryPointOfMember.ExpiryDate) >= DateTime.Today)
                                                {
                                                    expiryPointOfMember.ExpiryPoint = expiryPointOfMember.TotalPoint;
                                                    membershipEntity.ExpiryPointCollection.Add(expiryPointOfMember);
                                                }
                                            }
                                            else
                                            {
                                                membershipEntity.ExpiryPointCollection.Add(expiryPointOfMember);
                                            }
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(expiryPointOfMember.ExpiryPoint.ToString()) &&
                                                (expiryPointOfMember.ExpiryPoint != 0.0) &&
                                                (expiryPointOfMember.ExpiryPoint != 0))
                                            {
                                                membershipEntity.ExpiryPointCollection.Add(expiryPointOfMember);
                                            }
                                        }
                                        i++;
                                    }
                                }
                            }
                            arrMembershipList.Add(membershipEntity);
                        }
                    }
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchGuestCardListRequest, FetchGuestCardListResponse>(reqFetchGuestcardList, resFetchGuestcardList);
                }
            }
            catch (Exception ex)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchGuestCardListRequest, FetchGuestCardListResponse>(reqFetchGuestcardList, resFetchGuestcardList);
                AppLogger.LogFatalException(ex, ex.Message);
            }
            return arrMembershipList;
        }

        /// <summary>
        /// Function that sort it via adding to the SortedDictionary
        /// </summary>
        /// <param name="awardPointsToExpire">raw array of the AwardPointsToExpire</param>
        /// <returns>Processed Array of the AwardPointsToExpire</returns>
        private AwardPointsToExpire[] GetSortedOrderOfAwardsPointOnDateAsc(AwardPointsToExpire[] awardPointsToExpire)
        {
            SortedDictionary<DateTime, AwardPointsToExpire> dictPointDateExpiry =
                new SortedDictionary<DateTime, AwardPointsToExpire>();
            for (int awardCount = 0; awardCount < awardPointsToExpire.Length; awardCount++)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(awardPointsToExpire[awardCount].expirationDate)))
                {
                    dictPointDateExpiry.Add(awardPointsToExpire[awardCount].expirationDate,
                                            awardPointsToExpire[awardCount]);
                }
            }
            AwardPointsToExpire[] awdExpPointstDateAsc = new AwardPointsToExpire[dictPointDateExpiry.Count];
            int iDictCount = 0;
            foreach (KeyValuePair<DateTime, AwardPointsToExpire> pairPointDateExpiry in dictPointDateExpiry)
            {
                awdExpPointstDateAsc.SetValue((AwardPointsToExpire)pairPointDateExpiry.Value, iDictCount);
                iDictCount++;
            }
            return awdExpPointstDateAsc;
        }

        /// <summary>
        /// Fetch the Email for the specified Name Id
        /// </summary>
        /// <param name="nameID">
        /// Name Id
        /// </param>
        /// <returns>
        /// EmailDetails Entity
        /// </returns>
        public EmailDetailsEntity FetchEmail(string nameID)
        {
            EmailDetailsEntity emailDetails = null;
            FetchEmailListRequest request = null;
            FetchEmailListResponse response = null;
            try
            {
                AppLogger.LogInfoMessage("In LoyaltyDomain\\FetchEmail()");
                AppLogger.LogInfoMessage("OWSUtility.GetNameService()");
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetFetchEmailRequest(nameID)");
                request = OWSRequestConverter.GetFetchEmailRequest(nameID);

                AppLogger.LogInfoMessage("Fetching response: service.FetchPhoneList(request)");
                response = service.FetchEmailList(request);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    if (response.NameEmailList != null)
                    {
                        ServiceProxies.Name.NameEmail[] emailList = response.NameEmailList;
                        int totalEmails = emailList.Length;
                        for (int emailCount = 0; emailCount < totalEmails; emailCount++)
                        {
                            if (emailList[emailCount].primary == true)
                            {
                                emailDetails = new EmailDetailsEntity();
                                emailDetails.EmailOperaID = emailList[emailCount].operaId;
                                emailDetails.EmailID = emailList[emailCount].Value;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchEmailListRequest, FetchEmailListResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseFetchEmailOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchEmailListRequest, FetchEmailListResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;
            }
            return emailDetails;
        }

        /// <summary>
        /// Fetch the non business Email for the specified Name Id
        /// </summary>
        /// <param name="nameID">
        /// Name Id
        /// </param>
        /// <remarks>
        /// </remarks>
        /// <returns>
        /// EmailDetails Entity
        /// </returns>
        public EmailDetailsEntity FetchNonBusinessEmail(string nameID)
        {
            EmailDetailsEntity emailDetails = null;
            FetchEmailListRequest request = null;
            FetchEmailListResponse response = null;
            try
            {
                AppLogger.LogInfoMessage("In LoyaltyDomain\\FetchNonBusinessEmail()");
                AppLogger.LogInfoMessage("OWSUtility.GetNameService()");

                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetFetchEmailRequest(nameID)");
                request = OWSRequestConverter.GetFetchEmailRequest(nameID);

                AppLogger.LogInfoMessage("Fetching response: service.FetchPhoneList(request)");
                response = service.FetchEmailList(request);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    if (response.NameEmailList != null)
                    {
                        ServiceProxies.Name.NameEmail[] emailList = response.NameEmailList;
                        int totalEmails = emailList.Length;
                        for (int emailCount = 0; emailCount < totalEmails; emailCount++)
                        {
                            if (emailList[emailCount].emailType == AppConstants.EMAILTYPE_EMAIL)
                            {
                                emailDetails = new EmailDetailsEntity();
                                emailDetails.EmailOperaID = emailList[emailCount].operaId;
                                emailDetails.EmailID = emailList[emailCount].Value;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchEmailListRequest, FetchEmailListResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseFetchEmailOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchEmailListRequest, FetchEmailListResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;
            }
            return emailDetails;
        }

        /// <summary>
        /// Fetch the Comments for the specified Name Id
        /// </summary>
        /// <param name="nameID">
        /// Name Id
        /// </param>
        /// <returns>
        /// Comments Entity
        /// </returns>
        public CommentsEntity FetchComment(string nameID)
        {
            CommentsEntity commentEntity = null;
            FetchCommentListRequest request = null;
            FetchCommentListResponse response = null;
            try
            {
                AppLogger.LogInfoMessage("In LoyaltyDomain\\FetchComment()");
                AppLogger.LogInfoMessage("OWSUtility.GetNameService()");

                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetNameRequest(nameID)");
                request = OWSRequestConverter.GetFetchCommentListRequest(nameID);

                AppLogger.LogInfoMessage("Fetching response: service.FetchCommentList(request)");
                response = service.FetchCommentList(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    ServiceProxies.Name.Comment[] commentList = response.CommentList;
                    if (commentList != null)
                    {
                        if (commentList.Length > 0)
                        {
                            ServiceProxies.Name.Text commentText = commentList[0].Item as ServiceProxies.Name.Text;
                            if (commentText != null)
                            {
                                commentEntity = new CommentsEntity();
                                commentEntity.Comment = commentText.Value;
                                commentEntity.CommentOperaId = commentList[0].operaId;
                            }
                        }
                    }
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchCommentListRequest, FetchCommentListResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseCommentException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchCommentListRequest, FetchCommentListResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;
            }
            return commentEntity;
        }

        #endregion

        #region Update User Details Methods

        /// <summary>
        /// Update First name, Last name, Preferred language 
        /// </summary>
        /// <param name="nameID">NameID</param>
        /// <param name="profile">User Profile Entity</param>
        /// <returns>True if the updation is successful else false.</returns>
        public bool UpdateName(string nameID, UserProfileEntity profile)
        {
            bool result = false;
            UpdateNameRequest request = null;
            UpdateNameResponse response = null;

            try
            {
                AppLogger.LogInfoMessage("In LoyaltyDomain\\UpdateName()");
                AppLogger.LogInfoMessage("OWSUtility.GetNameService()");

                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetNameRequest(nameID)");
                request = OWSRequestConverter.GetUpdateNameRequest(nameID, profile);

                AppLogger.LogInfoMessage("Fetching response: service.FetchPhoneList(request)");
                response = service.UpdateName(request);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<UpdateNameRequest, UpdateNameResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseUpdateNameOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<UpdateNameRequest, UpdateNameResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;
            }
            return result;
        }

        /// <summary>
        /// Update First name, Last name, Preferred language 
        /// </summary>
        /// <param name="nameID">NameID</param>
        /// <param name="profile">User Profile Entity</param>
		/// <param name="response">FetchName Response</param>
        /// <returns>True if the updation is successful else false.</returns>
        public bool UpdatePreferredLanguage(string nameID, UserProfileEntity profile, FetchNameResponse fetchNameResponse)
        {
            bool result = false;
            UpdateNameRequest request = null;
            UpdateNameResponse response = null;

            try
            {
                AppLogger.LogInfoMessage("In LoyaltyDomain\\UpdateName()");
                AppLogger.LogInfoMessage("OWSUtility.GetNameService()");

                NameService service = OWSUtility.GetNameService(primaryLanguageID);
                AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetNameRequest(nameID)");
                request = OWSRequestConverter.GetUpdatePreferredLanguageRequest(nameID, profile, fetchNameResponse);

                AppLogger.LogInfoMessage("Fetching response: service.FetchPhoneList(request)");
                response = service.UpdateName(request);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<UpdateNameRequest, UpdateNameResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseUpdateNameOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<UpdateNameRequest, UpdateNameResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;
            }
            return result;
        }
        /// <summary>
        /// Update Comment
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="commentsEntity"></param>
        /// <returns></returns>
        public bool InsertComment(string nameId, CommentsEntity commentsEntity)
        {
            bool result = false;
            InsertCommentRequest request = null;
            InsertCommentResponse response = null;
            if ((nameId != string.Empty) && (nameId != null) && (commentsEntity != null))
            {
                try
                {
                    NameService service = OWSUtility.GetNameService(primaryLanguageID);

                    request = OWSRequestConverter.GetInsertCommentRequest(nameId, commentsEntity);
                    response = service.InsertComment(request);
                    if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                    {
                        result = true;
                    }
                    else
                    {
                        UserNavTracker.TrackOWSRequestResponse<InsertCommentRequest, InsertCommentResponse>(request, response);
                        AppLogger.LogInfoMessage("resultStatusFlag: failed");
                        OWSUtility.RaiseInsertCommentException(response);
                    }
                }
                catch (SoapException soapException)
                {
                    UserNavTracker.TrackOWSRequestResponse<InsertCommentRequest, InsertCommentResponse>(request, response);
                    throw new OWSException(soapException.Message, soapException);
                }
                catch (Exception ex)
                {
                    UserNavTracker.TrackOWSRequestResponse<InsertCommentRequest, InsertCommentResponse>(request, response);
                    AppLogger.LogInfoMessage(ex.InnerException.ToString());
                }
                finally
                {
                    request = null;
                    response = null;
                }
            }
            return result;
        }

        /// <summary>
        /// Update Comment
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="commentsEntity"></param>
        /// <returns></returns>
        public bool UpdateComment(string nameId, CommentsEntity commentsEntity)
        {
            bool result = false;
            if ((nameId != string.Empty) && (nameId != null) &&
                (commentsEntity != null) && (commentsEntity.CommentOperaId > 0))
            {
                UpdateCommentRequest request = null;
                UpdateCommentResponse response = null;
                try
                {
                    NameService service = OWSUtility.GetNameService(primaryLanguageID);

                    request = OWSRequestConverter.GetUpdateCommentRequest(nameId, commentsEntity);
                    response = service.UpdateComment(request);
                    if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                    {
                        result = true;
                    }
                    else
                    {
                        UserNavTracker.TrackOWSRequestResponse<UpdateCommentRequest, UpdateCommentResponse>(request, response);
                        AppLogger.LogInfoMessage("resultStatusFlag: failed");
                        OWSUtility.RaiseUpdateCommentException(response);
                    }
                }
                catch (SoapException soapException)
                {
                    UserNavTracker.TrackOWSRequestResponse<UpdateCommentRequest, UpdateCommentResponse>(request, response);
                    throw new OWSException(soapException.Message, soapException);
                }
                finally
                {
                    request = null;
                    response = null;
                }
            }
            return result;
        }

        /// <summary>
        /// Update the Address into the Opera
        /// </summary>
        /// <param name="userProfile">
        /// UserProfileEntity
        /// </param>
        /// <returns>
        /// True if the updation is successful else false.
        /// </returns>
        public bool UpdateAddress(UserProfileEntity userProfile)
        {
            bool result = false;
            UpdateAddressRequest request = null;
            UpdateAddressResponse response = null;
            if (userProfile.AddressOperaID > 0)
            {
                try
                {
                    NameService service = OWSUtility.GetNameService(primaryLanguageID);

                    request = OWSRequestConverter.GetUpdateAddressRequest(userProfile);
                    response = service.UpdateAddress(request);

                    if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                    {
                        result = true;
                    }
                    else
                    {
                        UserNavTracker.TrackOWSRequestResponse<UpdateAddressRequest, UpdateAddressResponse>(request, response);
                        AppLogger.LogInfoMessage("resultStatusFlag: failed");
                        OWSUtility.RaiseUpdateAddressOWSException(response);
                    }
                }
                catch (SoapException soapException)
                {
                    UserNavTracker.TrackOWSRequestResponse<UpdateAddressRequest, UpdateAddressResponse>(request, response);
                    throw new OWSException(soapException.Message, soapException);
                }
                finally
                {
                    request = null;
                    response = null;
                }
            }
            return result;
        }

        /// <summary>
        /// Insert the Profile into OWS
        /// </summary>
        /// <param name="userProfile">
        /// UserProfileEntity
        /// </param>
        /// <returns>
        /// True if the updation is successful else false.
        /// </returns>
        /// <remarks>
        /// </remarks>
        public bool InsertAddress(string nameID, UserProfileEntity userProfile)
        {
            bool result = false;
            InsertAddressRequest request = null;
            InsertAddressResponse response = null;
            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request = OWSRequestConverter.GetInsertAddressRequest(nameID, userProfile);
                response = service.InsertAddress(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<InsertAddressRequest, InsertAddressResponse>(request, response);
                    result = false;
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<InsertAddressRequest, InsertAddressResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;
            }
            return result;
        }

        /// <summary>
        /// Makes Update Phone OWS Call
        /// </summary>
        /// <param name="phoneID">Phone opera ID for updationP</param>
        /// <param name="phoneNumber">hone Number</param>
        /// <returns>True if the updation is successful else false.</returns>
        public bool UpdatePhone(string nameId, long phoneOperaID, string phoneNumber, string phoneType, string phoneRole)
        {
            bool result = false;
            UpdatePhoneRequest request = null;
            UpdatePhoneResponse response = null;
            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request = OWSRequestConverter.GetUpdatePhoneRequest(phoneOperaID, phoneNumber,
                                                                                       phoneType, phoneRole);
                response = service.UpdatePhone(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<UpdatePhoneRequest, UpdatePhoneResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseUpdatePhoneOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<UpdatePhoneRequest, UpdatePhoneResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;
            }
            return result;
        }

        /// <summary>
        /// Makes Update Email OWS Call
        /// </summary>
        /// <param name="emailOperaID">Email Opera ID</param>
        /// <param name="email">Email</param>
        /// <returns>True if the updation is successful else false.</returns>
        public bool UpdateEmail(string nameId, long emailOperaID, string email, bool isPrimary)
        {
            bool result = false;
            UpdateEmailRequest request = null;
            UpdateEmailResponse response = null;

            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request = OWSRequestConverter.GetUpdateEmailRequest(emailOperaID, email, isPrimary);
                response = service.UpdateEmail(request);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<UpdateEmailRequest, UpdateEmailResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseUpdateEmailOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<UpdateEmailRequest, UpdateEmailResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;
            }
            return result;
        }

        /// <summary>
        /// Make Update Guest Card OWS call.
        /// </summary>
        /// <param name="nameId">Name id for this profile.</param>
        /// <param name="cardOperaID">Member card Opera ID</param>
        /// <param name="membershipType">Membership Type</param>
        /// <param name="membershipNumber">Membership Number</param>
        /// <param name="memberName">Member Name</param>
        /// <param name="displaySequence">Display sequence for this partner program.</param>
        /// <returns>True if the updation is successful else false.</returns>
        public bool UpdateGuestCardDetails(string nameId,
                                           long cardOperaID, string membershipType,
                                           string membershipNumber, string memberName,
                                           int displaySequence)
        {
            bool result = false;
            UpdateGuestCardRequest request = null;
            UpdateGuestCardResponse response = null;
            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);
                request = OWSRequestConverter.GetUpdateGuestcardRequest(cardOperaID,
                                                                                               membershipType,
                                                                                               membershipNumber,
                                                                                               memberName,
                                                                                               displaySequence);
                response = service.UpdateGuestCard(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<UpdateGuestCardRequest, UpdateGuestCardResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseUpdateGuestCardOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<UpdateGuestCardRequest, UpdateGuestCardResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;
            }
            return result;
        }

        /// <summary>
        /// This method will delete the specified guest card identified with parameter 'guestCardOperaId' from the profile.
        /// </summary>
        /// <param name="guestCardOperaId">Opera id for this Guest card </param>
        public bool DeleteGuestCard(string guestCardOperaId)
        {
            AppLogger.LogInfoMessage("In LoyaltyDomain\\DeleteGuestCard()");
            AppLogger.LogInfoMessage("OWSUtility.GetNameService()");
            NameService service = OWSUtility.GetNameService(primaryLanguageID);
            DeleteGuestCardRequest deleteGuestCardRequest = OWSRequestConverter.DeleteGuestCardRequest(guestCardOperaId);
            bool result = false;
            DeleteGuestCardResponse response = null;

            try
            {
                response = service.DeleteGuestCard(deleteGuestCardRequest);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<DeleteGuestCardRequest, DeleteGuestCardResponse>(deleteGuestCardRequest, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<DeleteGuestCardRequest, DeleteGuestCardResponse>(deleteGuestCardRequest, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                deleteGuestCardRequest = null;
                response = null;

            }
            return result;
        }


        /// <summary>
        /// To update Interests List this method will call delete existing interests then Insert new Interests.
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="userInterests"></param>
        /// <returns></returns>
        public bool UpdatePreferencesListUserProfile(string nameId, UserPreferenceEntity[] userPreferences, UserPreferenceEntity[] userPreferencesOld)
        {
            AppLogger.LogInfoMessage("In LoyaltyDomain\\UpdatePreferencesListUserProfile()");
            AppLogger.LogInfoMessage("OWSUtility.GetNameService()");
            NameService service = OWSUtility.GetNameService(primaryLanguageID);

            //UserPreferenceEntity[] userExistingPreferences = userPreferencesOld;
            List<UserPreferenceEntity> deletionList = new List<UserPreferenceEntity>();
            List<UserPreferenceEntity> insertionList = new List<UserPreferenceEntity>();
            List<UserPreferenceEntity> listUserPreferencesNew = new List<UserPreferenceEntity>();
            List<UserPreferenceEntity> listUserPreferenceOld = new List<UserPreferenceEntity>();
            
            if (userPreferences != null && userPreferences.Length > 0)
                listUserPreferencesNew = userPreferences.ToList();

            if (userPreferencesOld != null && userPreferencesOld.Length > 0)
                listUserPreferenceOld = userPreferencesOld.ToList();

            foreach (UserPreferenceEntity newPreference in listUserPreferencesNew)
            {
                int insertionCount = listUserPreferenceOld.Count(oldPreference => string.Equals(newPreference.RequestType, oldPreference.RequestType) && string.Equals(newPreference.RequestValue, oldPreference.RequestValue));
                if (insertionCount == 0)
                {
                    insertionList.Add(newPreference);
                }
            }
            foreach (UserPreferenceEntity oldPreference in listUserPreferenceOld)
            {
                int deletionCount = listUserPreferencesNew.Count(newPreference => string.Equals(oldPreference.RequestType, newPreference.RequestType) && string.Equals(oldPreference.RequestValue, newPreference.RequestValue));
                if (deletionCount == 0)
                {
                    deletionList.Add(oldPreference);
                }
            }
            if (deletionList.Count > 0)
            {
                foreach (UserPreferenceEntity deletionItem in deletionList)
                {
                    bool DeletedPreference = DeletePreferences(nameId, deletionItem.RequestType, deletionItem.RequestValue);
                }
            }
            if (insertionList.Count > 0)
            {
                foreach (UserPreferenceEntity insertionItem in insertionList)
                {
                    bool InsertedPreference = InsertPreference(nameId, insertionItem.RequestType, insertionItem.RequestValue);
                }
            }
            
            //if (userExistingPreferences != null && userExistingPreferences.Length > 0)
            //{
            //    int maxCount = userExistingPreferences.Length;
            //    for (int count = 0; count < maxCount; count++)
            //    {
            //        bool DeletedPreference = DeletePreferences(nameId, userExistingPreferences[count].RequestType,
            //                                                   userExistingPreferences[count].RequestValue);
            //    }
            //}
            //if (userPreferences != null && userPreferences.Length > 0)
            //{
            //    int maxCount = userPreferences.Length;
            //    for (int count = 0; count < maxCount; count++)
            //    {
            //        bool InsertedPreference = InsertPreference(nameId, userPreferences[count].RequestType,
            //                                                   userPreferences[count].RequestValue);
            //    }
            //}
            return true;
        }

        /// <summary>
        /// To update Preferences this method will call delete existing preferences then Insert preferences.
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="userPreferences"></param>
        /// <returns></returns>
        public bool UpdateInterestsListUserProfile(string nameId, UserInterestsEntity[] userInterestsList, UserInterestsEntity[] userInterestsListOld)
        {
            AppLogger.LogInfoMessage("In LoyaltyDomain\\UpdateInterestsListUserProfile()");
            AppLogger.LogInfoMessage("OWSUtility.GetNameService()");
            NameService service = OWSUtility.GetNameService(primaryLanguageID);

            //UserInterestsEntity[] userExistingInterests = FetchUserInterestsListUserProfile(nameId);
            List<UserInterestsEntity> insertionList = new List<UserInterestsEntity>();
            List<UserInterestsEntity> deletionList = new List<UserInterestsEntity>();
            List<UserInterestsEntity> listUserInterestsOld = new List<UserInterestsEntity>();
            List<UserInterestsEntity> listUserInterestsNew = new List<UserInterestsEntity>();

            if (userInterestsList != null && userInterestsList.Length > 0)
                listUserInterestsNew = userInterestsList.ToList();

            if (userInterestsListOld != null && userInterestsListOld.Length > 0)
                listUserInterestsOld = userInterestsListOld.ToList();

            foreach (UserInterestsEntity newInterest in listUserInterestsNew)
            {
                int insertionCount = listUserInterestsOld.Count(oldInterest => string.Equals(newInterest.RequestType, oldInterest.RequestType) && string.Equals(newInterest.RequestValue, oldInterest.RequestValue));
                if (insertionCount == 0)
                {
                    insertionList.Add(newInterest);
                }
            }

            foreach (UserInterestsEntity oldInterest in listUserInterestsOld)
            {
                int deletionCount = listUserInterestsNew.Count(newInterest => string.Equals(oldInterest.RequestType, newInterest.RequestType) && string.Equals(oldInterest.RequestValue, newInterest.RequestValue));
                if (deletionCount == 0)
                {
                    deletionList.Add(oldInterest);
                }
            }

            if (deletionList.Count > 0)
            {
                foreach (UserInterestsEntity deletionItem in deletionList)
                {
                    bool DeletedInterest = DeletePreferences(nameId, deletionItem.RequestType, deletionItem.RequestValue);
                }
            }

            if (insertionList.Count > 0)
            {
                foreach (UserInterestsEntity insertionItem in insertionList)
                {
                    bool InsertedInterest = InsertPreference(nameId, insertionItem.RequestType, insertionItem.RequestValue);
                }
            }
            //UserInterestsEntity[] userExistingInterests = userInterestsListOld;
            //if (userExistingInterests != null && userExistingInterests.Length > 0)
            //{
            //    int maxCount = userExistingInterests.Length;
            //    for (int count = 0; count < maxCount; count++)
            //    {
            //        bool DeletedPreference = DeletePreferences(nameId, userExistingInterests[count].RequestType,
            //                                                   userExistingInterests[count].RequestValue);
            //    }
            //}
            //if (userInterestsList != null && userInterestsList.Length > 0)
            //{
            //    int maxCount = userInterestsList.Length;
            //    for (int count = 0; count < maxCount; count++)
            //    {
            //        bool InsertedPreference = InsertPreference(nameId, userInterestsList[count].RequestType,
            //                                                   userInterestsList[count].RequestValue);
            //    }
            //}
            return true;
        }

        /// <summary>
        /// Updates password
        /// </summary>
        /// <param name="MembershipId"></param>
        /// <param name="OldPassword"></param>
        /// <param name="NewPassword"></param>
        /// <returns></returns>
        public bool UpdatePassword(string MembershipId, string OldPassword, string NewPassword)
        {
            bool passwordUpdated = false;
            UpdatePasswordRequest request = null;
            UpdatePasswordResponse response = null;
            try
            {

                SecurityService service = OWSUtility.GetSecurityService();
                request = OWSRequestConverter.GetUpdatePasswordRequest(MembershipId, OldPassword,
                                                                                             NewPassword);
                response = service.UpdatePassword(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Security.ResultStatusFlag.FAIL)
                {
                    passwordUpdated = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<UpdatePasswordRequest, UpdatePasswordResponse>(request, response);
                    AppLogger.LogInfoMessage("UpdatePassword resultStatusFlag: failed");
                    OWSUtility.RaisePassowordUpdateOWSException(response);
                }
            }
            finally
            {
                request = null;
                response = null;

            }
            return passwordUpdated;
        }

        #endregion

        #region Delete OWS calls
        /// <summary>
        /// Deletes preferences
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="preferenceType"></param>
        /// <param name="preferenceValue"></param>
        /// <returns></returns>
        public bool DeletePreferences(string nameID, string preferenceType, string preferenceValue)
        {
            bool result = false;
            DeletePreferenceRequest request = null;
            DeletePreferenceResponse response = null;

            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request = OWSRequestConverter.GetDeletePreferenceRequest(nameID, preferenceType,
                                                                                                 preferenceValue);
                response = service.DeletePreference(request);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<DeletePreferenceRequest, DeletePreferenceResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseDeletePreferencesOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<DeletePreferenceRequest, DeletePreferenceResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;
            }
            return result;
        }

        /// <summary>
        /// Delete the Phone Number for the specified Opera Id
        /// </summary>
        /// <param name="phoneOperaID">
        /// Opera ID
        /// </param>
        /// <returns>
        /// True, if successful else false
        /// </returns>
        public bool DeletePhone(long phoneOperaID)
        {
            bool result = false;
            DeletePhoneRequest request = null;
            DeletePhoneResponse response = null;
            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request = OWSRequestConverter.GetDeletePhoneRequest(phoneOperaID);
                response = service.DeletePhone(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<DeletePhoneRequest, DeletePhoneResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseDeletePhoneOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<DeletePhoneRequest, DeletePhoneResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;
            }
            return result;
        }

        /// <summary>
        /// Delete the GuestCard Details for the specified Opera Id
        /// </summary>
        /// <param name="creditCardOperaID">
        /// GuestCard Opera ID</param>
        /// <returns>
        /// True, if successful else false
        /// </returns>
        public bool DeleteGuestCardDetails(long partnerProgramOperaID)
        {
            bool result = false;
            DeleteGuestCardRequest request = null;
            DeleteGuestCardResponse response = null;
            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request =
                    OWSRequestConverter.GetDeleteGuestCardDetailsRequest(partnerProgramOperaID);
                response = service.DeleteGuestCard(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<DeleteGuestCardRequest, DeleteGuestCardResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseDeleteGuestCardOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<DeleteGuestCardRequest, DeleteGuestCardResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;

            }
            return result;
        }

        #endregion

        #region Enroll Guest Program

        /// <summary>
        ///  Method to Register User & getting the registration number.
        /// </summary>
        /// <param name="profile">User Detail </param>        
        /// <returns>Confirmation Id</returns>        
        public string EnrollUser(UserProfileEntity profile)
        {
            string nameID = string.Empty;
            string ConfirmationNumber = string.Empty;
            try
            {
                AppLogger.LogInfoMessage("LoyaltyDomain: EnrollUser: Calling RegisterUser");
                nameID = RegisterUser(profile);
                AppLogger.LogInfoMessage("NameID : " + nameID);

                AppLogger.LogInfoMessage("LoyaltyDomain: EnrollUser: Calling FetchNextCardNumber");
                string nextCardNumber =
                    FetchNextCardNumber(AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE);
                ConfirmationNumber = nextCardNumber;

                AppLogger.LogInfoMessage("LoyaltyDomain: EnrollUser: Calling userCreated");
                bool userCreated = CreateUser(nextCardNumber, profile.Password, nameID);

                string memberName = profile.FirstName + AppConstants.SPACE + profile.LastName;

                AppLogger.LogInfoMessage("LoyaltyDomain: EnrollUser: Calling InsertUserGuestCard");

                bool guestCardInserted = InsertUserGuestCard(nameID, AppConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE,
                                                             nextCardNumber,
                                                             AppConstants.DEFAULT_MEMBERSHIPLEVEL, memberName, 1);

                AppLogger.LogInfoMessage("LoyaltyDomain: EnrollUser: Calling InsertEmail");
                if (null != profile.EmailID)
                {
                    bool emailInserted = InsertEmail(profile.EmailID, nameID);
                }

                AppLogger.LogInfoMessage("LoyaltyDomain: EnrollUser: Calling InsertPhone");
                if (null != profile.HomePhone)
                {
                    bool homePhoneInserted = InsertPhone(nameID, profile.HomePhone, AppConstants.PHONETYPE_HOME,
                                                         AppConstants.PHONEROLE_PHONE);
                }

                if (null != profile.MobilePhone)
                {
                    bool mobileInserted = InsertPhone(nameID, profile.MobilePhone, AppConstants.PHONETYPE_MOBILE,
                                                      AppConstants.PHONEROLE_PHONE);
                }

                AppLogger.LogInfoMessage("LoyaltyDomain: EnrollUser: Calling InsertCreditCard");
                if (null != profile.CreditCard)
                {
                    bool creditCardInserted = InsertCreditCard(nameID, profile.CreditCard);
                }

                AppLogger.LogInfoMessage("LoyaltyDomain: EnrollUser: Calling InsertUserGuestCard");
                if (profile.PartnerProgramList != null)
                {
                    InsertPartnerProgram(profile, nameID, memberName);
                }
                //added by Vaibhav- Merch R4 - ITR2
                AppLogger.LogInfoMessage("LoyaltyDomain: EnrollUser: Calling InsertPerferences for Interests");
                if (null != profile.Interests)
                {
                    bool userInterestsInserted;
                    for (int i = 0; i < profile.Interests.Length; i++)
                    {
                        userInterestsInserted = InsertPreference(nameID, profile.Interests[i].RequestType,
                                                                  profile.Interests[i].RequestValue);
                    }
                }

                AppLogger.LogInfoMessage("LoyaltyDomain: EnrollUser: Calling InsertPreference");
                if (null != profile.UserPreference)
                {
                    bool userPreferenceInserted;
                    for (int i = 0; i < profile.UserPreference.Length; i++)
                    {
                        userPreferenceInserted = InsertPreference(nameID, profile.UserPreference[i].RequestType,
                                                                  profile.UserPreference[i].RequestValue);
                    }
                }

                AppLogger.LogInfoMessage("LoyaltyDomain: EnrollUser: Calling UpdateQuestionNAnswer");
                if (null != profile.Question && null != profile.Answer)
                {
                    bool securityDetailsInserted = UpdateQuestionNAnswer(nameID, profile.Question, profile.Answer);
                }

                bool scandicInfoEmailInserted = InsertUpdatePrivacyUserDetail(nameID, profile.ScandicEmail,
                                                                              profile.ThridPartyEmail, profile.ScandicEMailPrev);
            }
            catch (SoapException SoapEx)
            {
                throw new OWSException(SoapEx.Message, SoapEx);
            }
            return ConfirmationNumber;
        }

        #endregion

        #region Register User

        /// <summary>
        /// To Enroll guest profile & returns NameID
        /// </summary>
        /// <param name="profile">Guest Profile to be registered</param>
        /// <returns>Returns NameID if successfully register user in OWS.</returns>
        public string RegisterUser(UserProfileEntity profile)
        {
            NameService service = OWSUtility.GetNameService(primaryLanguageID);
            RegisterNameRequest request = OWSRequestConverter.GetRegisterNameRequest(profile);
            RegisterNameResponse response = service.RegisterName(request);

            string NameID = null;
            if (response.Result.resultStatusFlag == ServiceProxies.Name.ResultStatusFlag.SUCCESS)
            {
                NameID = response.Result.IDs[0].operaId + String.Empty;
            }
            else
            {
                UserNavTracker.TrackOWSRequestResponse<RegisterNameRequest, RegisterNameResponse>(request, response);
                AppLogger.LogInfoMessage("RegisterName resultStatusFlag: failed");
                OWSUtility.RaiseRegisterUserOWSException(response);
            }

            return NameID;
        }

        #endregion

        #region Insert User detail Name services.

        /// <summary>
        /// This method is used for both Insert & Update the Privacy options
        /// </summary>
        /// <param name="nameID">nameID</param>
        /// <param name="scandicEmail">Scandic Email as True or False</param>
        /// <param name="thirdPartyEmail">Third party Email as True or False</param>
        /// <returns></returns>
        public bool InsertUpdatePrivacyUserDetail(string nameID, bool scandicEmail, bool thirdPartyEmail, bool scandicEMailPrev)
        {
            AppLogger.LogInfoMessage("In LoyaltyDomain\\InsertUpdatePrivacyUserDetail()");
            InsertUpdatePrivacyOptionRequest request = null;
            InsertUpdatePrivacyOptionResponse response = null;
            bool result = false;
            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request = new InsertUpdatePrivacyOptionRequest();
                request.NameID = OWSUtility.GetNameServiceUniqueID(nameID);
                ServiceProxies.Name.PrivacyOptionType[] privacyOption = new ServiceProxies.Name.PrivacyOptionType[3];

                privacyOption[0] = new ServiceProxies.Name.PrivacyOptionType();
                privacyOption[1] = new ServiceProxies.Name.PrivacyOptionType();
                privacyOption[2] = new ServiceProxies.Name.PrivacyOptionType();

                privacyOption[0].OptionType = new ServiceProxies.Name.PrivacyOptionTypeOptionType();
                privacyOption[0].OptionValue = new ServiceProxies.Name.PrivacyOptionTypeOptionValue();

                privacyOption[1].OptionType = new ServiceProxies.Name.PrivacyOptionTypeOptionType();
                privacyOption[1].OptionValue = new ServiceProxies.Name.PrivacyOptionTypeOptionValue();

                privacyOption[2].OptionType = new ServiceProxies.Name.PrivacyOptionTypeOptionType();
                privacyOption[2].OptionValue = new ServiceProxies.Name.PrivacyOptionTypeOptionValue();

                privacyOption[0].OptionType = ServiceProxies.Name.PrivacyOptionTypeOptionType.Promotions;
                privacyOption[1].OptionType = ServiceProxies.Name.PrivacyOptionTypeOptionType.ThirdParties;
                privacyOption[2].OptionType = ServiceProxies.Name.PrivacyOptionTypeOptionType.Email;

                if (scandicEmail)
                    privacyOption[0].OptionValue = ServiceProxies.Name.PrivacyOptionTypeOptionValue.YES;
                else
                    privacyOption[0].OptionValue = ServiceProxies.Name.PrivacyOptionTypeOptionValue.NO;

                if (thirdPartyEmail)
                    privacyOption[1].OptionValue = ServiceProxies.Name.PrivacyOptionTypeOptionValue.YES;
                else
                    privacyOption[1].OptionValue = ServiceProxies.Name.PrivacyOptionTypeOptionValue.NO;

                if (scandicEMailPrev)
                    privacyOption[2].OptionValue = ServiceProxies.Name.PrivacyOptionTypeOptionValue.YES;
                else
                    privacyOption[2].OptionValue = ServiceProxies.Name.PrivacyOptionTypeOptionValue.NO;

                request.Privacy = privacyOption;
                response = service.InsertUpdatePrivacyOption(request);
                if (response.Result.resultStatusFlag == ServiceProxies.Name.ResultStatusFlag.SUCCESS)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<InsertUpdatePrivacyOptionRequest, InsertUpdatePrivacyOptionResponse>(request, response);
                    AppLogger.LogInfoMessage("InsertUpdatePrivacy resultStatusFlag: failed");
                    OWSUtility.RaiseInsertUpdatePrivacyException(response);
                }
            }
            finally
            {
                request = null;
                response = null;
            }
            return result;
        }


        /// <summary>
        /// To fetch the Registration Number to the guest
        /// </summary>
        /// <param name="membershipType">Membership Type</param>
        /// <returns>Registration Number assigned to the guest</returns>
        public string FetchNextCardNumber(string membershipType)
        {
            AppLogger.LogInfoMessage("In LoyaltyDomain\\FetchNextCardNumber()");
            FetchNextCardNumberRequest request = null;
            FetchNextCardNumberResponse response = null;
            string nextCardNumber = string.Empty;
            try
            {
                MembershipService service = OWSUtility.GetMembershipService();
                request = new FetchNextCardNumberRequest();
                request.MembershipCardType = membershipType;
                response = service.FetchNextCardNumber(request);


                if (response.Result.resultStatusFlag == ServiceProxies.Membership.ResultStatusFlag.SUCCESS)
                {
                    nextCardNumber = response.CardNumber;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchNextCardNumberRequest, FetchNextCardNumberResponse>(request, response);
                    AppLogger.LogInfoMessage("FetchNextCardNumber resultStatusFlag: failed");
                    Hashtable exceptionData = new Hashtable();
                    exceptionData.Add("FetchNextCardNumber failed for card type:", response.CardType);
                    exceptionData.Add("FetchNextCardNumber failed for Membership type:", membershipType);
                    OWSUtility.RaiseFetchNextCardNumberOWSException(response, exceptionData);
                }
            }
            finally
            {
                request = null;
                response = null;
            }

            return nextCardNumber;
        }

        /// <summary>
        /// To create a login & password for the user
        /// </summary>
        /// <param name="loginName">The registration Number  for the guest</param>
        /// <param name="password">Password provided by the guest</param>
        /// <param name="nameID">NameId returned from Register user method.</param>
        /// <returns>Status of the operation, True if successful.</returns>
        public bool CreateUser(string loginName, string password, string nameID)
        {
            AppLogger.LogInfoMessage("In LoyaltyDomain\\CreateUser()");
            CreateUserRequest request = new CreateUserRequest();
            CreateUserResponse response = null;
            bool result = false;
            try
            {
                SecurityService service = OWSUtility.GetSecurityService();


                request.loginName = loginName;
                request.password = password;
                ServiceProxies.Security.UniqueID id = new ServiceProxies.Security.UniqueID();
                id.Value = nameID;
                id.type = ServiceProxies.Security.UniqueIDType.INTERNAL;
                request.NameID = id;

                response = service.CreateUser(request);
                if (response.Result.resultStatusFlag == ServiceProxies.Security.ResultStatusFlag.SUCCESS)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<CreateUserRequest, CreateUserResponse>(request, response);
                    AppLogger.LogInfoMessage("CreateUser resultStatusFlag: failed");
                    OWSUtility.RaiseCreateUserOWSException(response);

                }
            }
            finally
            {

            }
            return result;
        }

        /// <summary>
        /// Insert User Card Detail with membership level
        /// </summary>
        /// <param name="nameID">NameID of the guest returned from Register User method</param>
        /// <param name="membershipType">Registration type to be inserted e.g. GUESTPR</param>
        /// <param name="membershipNumber">Registration number to be inserted</param>
        /// <param name="membershipLevel">MembershipLevel e.g. FirstFloor etc...</param>
        /// <param name="memberName">Member name</param>
        /// <param name="displaySequence">Display sequence</param>        
        /// <returns>Status of the operation, True if successful.</returns>        
        public bool InsertUserGuestCard(string nameID, string membershipType, string membershipNumber,
                                        string membershipLevel, string memberName, int displaySequence)
        {
            NameService service = OWSUtility.GetNameService(primaryLanguageID);

            InsertGuestCardRequest request = new InsertGuestCardRequest();
            request.NameID = OWSUtility.GetNameServiceUniqueID(nameID);
            request.NameMembership = new ServiceProxies.Name.NameMembership();
            request.NameMembership.membershipType = membershipType;
            request.NameMembership.membershipNumber = membershipNumber;
            request.NameMembership.displaySequence = displaySequence;
            request.NameMembership.displaySequenceSpecified = true;

            if (null != membershipLevel)
                request.NameMembership.membershipLevel = membershipLevel;

            if (null != memberName)
                request.NameMembership.memberName = memberName;

            InsertGuestCardResponse response = service.InsertGuestCard(request);
            bool result = false;
            if (response.Result.resultStatusFlag == ServiceProxies.Name.ResultStatusFlag.SUCCESS)
            {
                result = true;
            }
            else
            {
                UserNavTracker.TrackOWSRequestResponse<InsertGuestCardRequest, InsertGuestCardResponse>(request, response);
                AppLogger.LogInfoMessage("InsertUserGuestCard resultStatusFlag: failed");
                OWSUtility.RaiseInsertUserGuestCardOWSException(response);
            }
            return result;
        }

        /// <summary>
        /// Insert Email ID against the specified NameID
        /// </summary>
        /// <param name="email">
        /// Email ID to the inserted
        /// </param>
        /// <param name="nameID">
        /// Name ID of the Guest
        /// </param>
        /// <returns>
        /// Status of the operation, True if successful.
        /// </returns>
        public bool InsertEmail(string email, string nameID)
        {
            AppLogger.LogInfoMessage("In LoyaltyDomain\\InsertEmail()");
            AppLogger.LogInfoMessage("OWSUtility.GetNameService()");
            NameService service = OWSUtility.GetNameService(primaryLanguageID);

            AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetInsertEmailRequest(email, nameID");
            InsertEmailRequest requestBusinessEmail = OWSRequestConverter.GetInsertEmailRequest(email, nameID,
                                                                                                AppConstants.
                                                                                                    EMAILTYPE_BUSINESSEMAIL,
                                                                                                true);
            InsertEmailRequest requestEmail = OWSRequestConverter.GetInsertEmailRequest(email, nameID,
                                                                                        AppConstants.EMAILTYPE_EMAIL,
                                                                                        false);

            AppLogger.LogInfoMessage("Fetching response: service.InsertEmail(request)");
            InsertEmailResponse responseBusinessEmail = service.InsertEmail(requestBusinessEmail);
            InsertEmailResponse responseEmail = service.InsertEmail(requestEmail);

            bool result = false;
            if ((responseBusinessEmail.Result.resultStatusFlag == ServiceProxies.Name.ResultStatusFlag.SUCCESS)
                && (responseEmail.Result.resultStatusFlag == ServiceProxies.Name.ResultStatusFlag.SUCCESS))
            {
                result = true;
            }
            else
            {
                UserNavTracker.TrackOWSRequestResponse<InsertEmailRequest, InsertEmailResponse>(requestEmail, responseEmail);
                UserNavTracker.TrackOWSRequestResponse<InsertEmailRequest, InsertEmailResponse>(requestBusinessEmail, responseBusinessEmail);
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Insert Phone Number against the specified NameID
        /// </summary>
        /// <param name="nameID">
        /// Name ID of the guest
        /// </param>
        /// <param name="phNumber">
        /// Phone number to be inserted
        /// </param>
        /// <param name="phType">
        /// Phone Type
        /// </param>
        /// <param name="phRole">
        /// Phone Role
        /// </param>
        /// <returns>
        /// Status of the operation, True if successful.
        /// </returns>
        public bool InsertPhone(string nameID, string phoneNumber, string phoneType, string phoneRole)
        {
            AppLogger.LogInfoMessage("In LoyaltyDomain\\InsertEmail()");
            AppLogger.LogInfoMessage("OWSUtility.GetNameService()");
            NameService service = OWSUtility.GetNameService(primaryLanguageID);

            AppLogger.LogInfoMessage(
                "Creating request object: OWSRequestConverter.InsertPhoneRequest(nameID, phNumber, phType, phRole)");
            InsertPhoneRequest request = OWSRequestConverter.GetInsertPhoneRequest(nameID, phoneNumber, phoneType,
                                                                                   phoneRole);

            AppLogger.LogInfoMessage("Fetching response: service.InsertPhone(request)");
            InsertPhoneResponse response = service.InsertPhone(request);
            bool result = false;
            if (response.Result.resultStatusFlag == ServiceProxies.Name.ResultStatusFlag.SUCCESS)
            {
                result = true;
            }
            else
            {
                UserNavTracker.TrackOWSRequestResponse<InsertPhoneRequest, InsertPhoneResponse>(request, response);
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Method to insert guest Preferences
        /// </summary>
        /// <param name="nameID">NameID of the guest returned from Register User method</param>
        /// <param name="preferenceType">Guest Preference Type</param>
        /// <param name="preferenceValue">Guest Preference value</param>
        /// <returns>Status of the operation, True if successful.</returns>
        public bool InsertPreference(string nameID, string preferenceType, string preferenceValue)
        {
            AppLogger.LogInfoMessage("In LoyaltyDomain\\InsertPreference()");

            NameService service = OWSUtility.GetNameService(primaryLanguageID);

            InsertPreferenceRequest request = new InsertPreferenceRequest();
            request.NameID = OWSUtility.GetNameServiceUniqueID(nameID);
            request.Preference = new ServiceProxies.Name.Preference();
            request.Preference.preferenceType = preferenceType;
            request.Preference.preferenceValue = preferenceValue;

            InsertPreferenceResponse response = service.InsertPreference(request);
            bool result = false;
            if (response.Result.resultStatusFlag == ServiceProxies.Name.ResultStatusFlag.SUCCESS)
            {
                result = true;
            }
            else
            {
                UserNavTracker.TrackOWSRequestResponse<InsertPreferenceRequest, InsertPreferenceResponse>(request, response);
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Method to insert question & answer to retrieve password.
        /// </summary>
        /// <param name="nameID">NameID of the guest returned from Register User method</param>
        /// <param name="question">Security Question Code chosen by guest</param>
        /// <param name="answer">Answer provided by guest</param>
        /// <returns>Status of the operation, True if successful.</returns>
        public bool UpdateQuestionNAnswer(string nameID, string question, string answer)
        {
            AppLogger.LogInfoMessage("In LoyaltyDomain\\UpdateQuestionNAnswer()");
            SecurityService service = OWSUtility.GetSecurityService();
            UpdateQuestionRequest request = new UpdateQuestionRequest();
            request.NameID = OWSUtility.GetSecurityServiceUniqueID(nameID);
            request.Question = new Question();
            request.Question.Value = question;
            request.Answer = answer;

            UpdateQuestionResponse response = service.UpdateQuestion(request);
            bool result = false;
            if (response.Result.resultStatusFlag == ServiceProxies.Security.ResultStatusFlag.SUCCESS)
            {
                result = true;
            }
            else
            {
                UserNavTracker.TrackOWSRequestResponse<UpdateQuestionRequest, UpdateQuestionResponse>(request, response);
                AppLogger.LogInfoMessage("UpdateQuestionNAnswer resultStatusFlag: failed");
                OWSUtility.RaiseUpdateQuestionNAnswerOWSException(response);
            }
            return result;
        }

        #region Fetch Password

        /// <summary>
        /// Method returns password of a given membership id.
        /// It returns password if found else return empty string.
        /// </summary>
        /// <param name="membershipId">Membership ID of the User.</param>
        /// <param name="questionId">Unique Id for a question.</param>
        /// <param name="questionString">Question String.</param>
        /// <param name="answer">Answer String.</param>
        /// <returns></returns>
        public string FetchPassword(string membershipId, string questionId, string questionString, string answer)
        {
            SecurityService service = OWSUtility.GetSecurityService();
            ValidateQuestionRequest request = null;
            ValidateQuestionResponse response = null;
            try
            {
                request =
                   Scandic.Scanweb.BookingEngine.Domain.OWSRequestConverter.GetValidateQuestionRequest(membershipId,
                                                                                                       questionId,
                                                                                                       questionString,
                                                                                                       answer);
                response = service.ValidateQuestion(request);
                if (response.Result.resultStatusFlag == ServiceProxies.Security.ResultStatusFlag.FAIL)
                {
                    return string.Empty;
                }
                else
                {
                    return response.Password;
                }
            }
            catch (Exception)
            {
                UserNavTracker.TrackOWSRequestResponse<ValidateQuestionRequest, ValidateQuestionResponse>(request, response);
            }
            finally
            {
                request = null;
                response = null;
            }
            return string.Empty;
        }

        #endregion Fetch Password

        #endregion

        #region Fetch Name Services

        /// <summary>
        /// Fetch Name Details
        /// </summary>
        /// <param name="nameID">
        /// Name ID of the user
        /// </param>
        /// <returns>
        /// FetchNameResponse
        /// </returns>
        /// <remarks>
        /// This method is made public as it needs to be accessed from the NUnit Component
        /// </remarks>        
        public FetchNameResponse FetchNameService(string nameID)
        {
            FetchNameResponse response = new FetchNameResponse();
            FetchNameRequest request = null;
            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request = OWSRequestConverter.GetFetchNameRequest(nameID);
                response = service.FetchName(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    return response;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchNameRequest, FetchNameResponse>(request, response);
                    AppLogger.LogInfoMessage("FetchName resultStatusFlag: failed");
                    OWSUtility.RaiseFetchNameOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchNameRequest, FetchNameResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
            }
            return response;
        }
        
        //Sateesh
        public FetchProfileResponse FetchProfileService(string nameID)
        {
            FetchProfileResponse response = new FetchProfileResponse();
            FetchProfileRequest request = null;
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            try
            {
                //Fix for artifact number - artf713949
                //Now GetNameService method takes a parameter Primary Language Id.This is used to initialize
                //the web methods header attribute primaryLanguageId.
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request = OWSRequestConverter.GetFetchProfileRequest(nameID);
                
                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);

                response = service.FetchProfile(request);
                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();
                string currentSessionId = string.Empty;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    currentSessionId = HttpContext.Current.Session.SessionID;
                }
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = currentSessionId + ", " + request.GetType().Name + ", Sync, Fetch Profile, " + reqStartTime + ", " + responseEndTime + ", " +
                    timer.Elapsed.ToString() + ", " + System.Environment.MachineName + ", " + requestSource;

                AppLogger.CSVInfoLogger(strLog);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    return response;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchProfileRequest, FetchProfileResponse>(request, response);
                    AppLogger.LogInfoMessage("FetchProfile resultStatusFlag: failed");
                    //TODO: the below method needs to be implemented by Sateesh
                    //OWSUtility.RaiseFetchProfileOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchProfileRequest, FetchProfileResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                timer = null;
            }
            return response;
        }

        /// <summary>
        /// Fetches all addresses of a profile for a language.
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="primaryLangID"></param>
        /// <returns></returns>
        public FetchAddressListResponse FetchAddressNameService(string nameID, string primaryLangID)
        {
            FetchAddressListResponse response = null;
            FetchAddressListRequest request = null;
            try
            {
                NameService service = OWSUtility.GetNameService(primaryLangID);

                request = OWSRequestConverter.GetFetchAddressRequest(nameID);
                response = service.FetchAddressList(request);
                if ((response != null) && (response.Result != null))
                {
                    if (response.Result.resultStatusFlag == ServiceProxies.Name.ResultStatusFlag.FAIL)
                    {
                        UserNavTracker.TrackOWSRequestResponse<FetchAddressListRequest, FetchAddressListResponse>(request, response);
                        AppLogger.LogInfoMessage("resultStatusFlag: failed");
                        OWSUtility.RaiseFetchAddressOWSException(response);
                    }
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchAddressListRequest, FetchAddressListResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
            }

            return response;
        }

        /// <summary>
        /// Fetch the Phone Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// FetchPhoneListResponse
        /// </returns>
        /// <remarks>
        /// This method is made public as it needs to be accessed from the NUnit Component
        /// </remarks>                
        public FetchPhoneListResponse FetchPhoneNameService(string nameID)
        {
            FetchPhoneListResponse response = new FetchPhoneListResponse();
            FetchPhoneListRequest request = null;
            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request = OWSRequestConverter.GetFetchPhoneRequest(nameID);
                response = service.FetchPhoneList(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    return response;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchPhoneListRequest, FetchPhoneListResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseFetchPhoneOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchPhoneListRequest, FetchPhoneListResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
            }
            return response;
        }

        /// <summary>
        /// Fetch the Privacy Option
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// FetchPrivacyOptionResponse
        /// </returns>
        /// <remarks>
        /// This method is made public as it needs to be accessed from the NUnit Component
        /// </remarks>                
        public FetchPrivacyOptionResponse FetchPrivacyOption(string nameID)
        {
            FetchPrivacyOptionResponse response = new FetchPrivacyOptionResponse();
            FetchPrivacyOptionRequest request = null;
            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request = new FetchPrivacyOptionRequest();

                request.NameID = request.NameID = OWSUtility.GetNameServiceUniqueID(nameID);
                response = service.FetchPrivacyOption(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    return response;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchPrivacyOptionRequest, FetchPrivacyOptionResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseFetchPrivacyOptionException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchPrivacyOptionRequest, FetchPrivacyOptionResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
            }
            return response;
        }


        /// <summary>
        /// Fetch the preference List Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// FetchPreferenceListResponse
        /// </returns>
        /// <remarks>
        /// This method is made public as it needs to be accessed from the NUnit Component
        /// </remarks>                
        public FetchPreferenceListResponse FetchPreferencesListNameService(string nameID)
        {
            FetchPreferenceListResponse response = new FetchPreferenceListResponse();
            FetchPreferenceListRequest request = null;
            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request = OWSRequestConverter.GetFetchPreferenceRequest(nameID);
                response = service.FetchPreferenceList(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    return response;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchPreferenceListRequest, FetchPreferenceListResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaisePreferenceListException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchPreferenceListRequest, FetchPreferenceListResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
            }
            return response;
        }

        /// <summary>
        /// Fetch Partner Program Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// FetchGuestCardListResponse
        /// </returns>
        /// <remarks>
        /// This method is made public as it needs to be accessed from the NUnit Component
        /// </remarks>                
        public FetchGuestCardListResponse FetchPartnerProgramNameService(string nameID)
        {
            NameService service = OWSUtility.GetNameService(primaryLanguageID);

            FetchGuestCardListRequest request = new FetchGuestCardListRequest();
            FetchGuestCardListResponse response = null;
            try
            {
                request.NameID = OWSUtility.GetNameServiceUniqueID(nameID);
                response = service.FetchGuestCardList(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    return response;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<FetchGuestCardListRequest, FetchGuestCardListResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseFetchPartnerProgramException(response);
                }
            }
            catch (Exception ex)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchGuestCardListRequest, FetchGuestCardListResponse>(request, response);
            }
            finally
            {
                request = null;
            }
            return response;
        }

        #endregion

        /// <summary>
        /// Fetch the CreditCard Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// FetchCreditCardListResponse
        /// </returns>
        /// <remarks>
        /// This method is made public as it needs to be accessed from the NUnit Component
        /// </remarks>                
        public FetchCreditCardListResponse FetchCreditCardDetailsNameService(string nameID)
        {
            FetchCreditCardListResponse response = new FetchCreditCardListResponse();
            FetchCreditCardListRequest request = null;
            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request = OWSRequestConverter.GetFetchCreditCardRequest(nameID);
                response = service.FetchCreditCardList(request);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    return response;
                }
                else
                {
                    ServiceProxies.Name.Text[] textArray = response.Result.Text;
                    if ((textArray[0] != null) &&
                        (textArray[0].Value.ToLower() == AppConstants.NOCREDITCARDRECORDMSG.ToLower()))
                    {
                        return response;
                    }
                    else
                    {
                        UserNavTracker.TrackOWSRequestResponse<FetchCreditCardListRequest, FetchCreditCardListResponse>(request, response);
                        AppLogger.LogInfoMessage("resultStatusFlag: failed");
                        OWSUtility.RaiseFetchCreditCardOWSException(response);
                    }
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<FetchCreditCardListRequest, FetchCreditCardListResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
            }
            return response;
        }

        /// <summary>
        /// Fetch the CreditCard Details
        /// </summary>
        /// <param name="nameID">
        /// NameID
        /// </param>
        /// <returns>
        /// Array of CreditCardEntity
        /// </returns>
        public Dictionary<string, CreditCardEntity> FetchCreditCardDetailsUserProfile(string nameID)
        {
            Dictionary<string, CreditCardEntity> creditCards = new Dictionary<string, CreditCardEntity>();

            try
            {
                FetchCreditCardListResponse response = FetchCreditCardDetailsNameService(nameID);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    ServiceProxies.Name.NameCreditCard[] creditCardList = response.CreditCardList;
                    if (creditCardList != null)
                    {
                        int totalCreditCards = creditCardList.Length;
                        for (int cardCount = 0; cardCount < totalCreditCards; cardCount++)
                        {
                            if (AppConstants.ModifyCreditCardNumberForMakePayment)
                            {
                                if (creditCardList[cardCount].cardNumber.Contains('x'))
                                    continue;
                            }
                            ExpiryDateEntity expiryDate =
                                new ExpiryDateEntity(creditCardList[cardCount].expirationDate.Month,
                                                     creditCardList[cardCount].expirationDate.Year);

                            CreditCardEntity creditCardsEnt =
                                new CreditCardEntity(creditCardList[cardCount].cardHolderName,
                                                     creditCardList[cardCount].cardCode,
                                                     creditCardList[cardCount].cardNumber, expiryDate,
                                                     creditCardList[cardCount].displaySequence);
                            string creditOperaID = creditCardList[cardCount].operaId.ToString();
                            creditCards.Add(creditOperaID, creditCardsEnt);
                        }
                    }
                }
                else
                {
                    ServiceProxies.Name.Text[] textArray = response.Result.Text;
                    if ((textArray[0] != null) &&
                        (textArray[0].Value.ToLower() == AppConstants.NOCREDITCARDRECORDMSG.ToLower()))
                    {
                        return creditCards;
                    }
                    else
                    {
                        AppLogger.LogInfoMessage("resultStatusFlag: failed");
                        OWSUtility.RaiseFetchCreditCardOWSException(response);
                    }
                }
            }
            catch (SoapException soapException)
            {
                throw new OWSException(soapException.Message, soapException);
            }
            return creditCards;
        }

        /// <summary>
        /// Fetches only the Prepaid card types if the fetchOnlyPrepaidCCTypes is set to true. otherwise returns all the credit cards.
        /// </summary>
        /// <param name="nameID">Name ID</param>
        /// <param name="fetchOnlyPrepaidCCTypes">sets only prepaid card types only are required or not</param>
        /// <returns>Array of CreditCardEntity</returns>
        public Dictionary<string, CreditCardEntity> FetchCreditCardDetailsUserProfile(string nameID, bool fetchOnlyPrepaidCCTypes)
        {
            var creditCards = FetchCreditCardDetailsUserProfile(nameID);

            //filter with valid expiration date
            creditCards = creditCards.Where(p => p.Value.ExpiryDate.ToDateTime() > DateTime.Now).ToDictionary(p => p.Key, p => p.Value);

            if (!string.IsNullOrEmpty(AppConstants.PREPAID_CCTYPE_PREFIX))
            {
                //pick only the pan hash credit cards
                if (fetchOnlyPrepaidCCTypes)
                {

                    creditCards = creditCards.Where(p => p.Value.CardType.StartsWith(AppConstants.PREPAID_CCTYPE_PREFIX)).ToDictionary(p => p.Key, p => p.Value);
                }
                else
                {
                    creditCards = creditCards.Where(p => !p.Value.CardType.StartsWith(AppConstants.PREPAID_CCTYPE_PREFIX)).ToDictionary(p => p.Key, p => p.Value);
                }
            }

            return creditCards;
        }

        /// <summary>
        /// Fetches the credit card details which matches with the pan hash
        /// </summary>
        /// <param name="nameID">Name ID</param>
        /// <param name="panHash">the pan hash value of the credit card (NameonCard)</param>
        /// <returns>Array of CreditCardEntity</returns>
        public CreditCardEntity FetchCreditCardDetailsByPanHash(string nameID, string panHash)
        {
            var creditCards = FetchCreditCardDetailsUserProfile(nameID);
            //return creditCards.Where(p => p.Value.NameOnCard.Equals(panHash)).First().Value;

            foreach (string cardKay in creditCards.Keys)
            {
                if ((creditCards[cardKay] != null) && (creditCards[cardKay].NameOnCard == panHash))
                {
                    if (AppConstants.ModifyCreditCardNumberForMakePayment && !string.IsNullOrEmpty(creditCards[cardKay].CardNumber))
                    {
                        creditCards[cardKay].CardNumber = creditCards[cardKay].CardNumber.Replace('*', 'x');
                    }
                    return creditCards[cardKay];
                }


            }

            return null;
        }

        /// <summary>
        /// Make Update Credit Card OWS Call.
        /// </summary>
        /// <param name="cardOperaID">Opera Id for updating credit card</param>
        /// <param name="holderName">Holder Name</param>
        /// <param name="cardType">Card Type</param>
        /// <param name="cardNumber">Card Number</param>
        /// <param name="expDate">Expiry Date</param>
        /// <returns>True if the updation is successful else false.</returns>
        public bool UpdateCreditCardDetails(CreditCardEntity creditCard,
                                            long creditCardOperaID, bool isPrimary)
        {
            bool result = false;
            UpdateCreditCardRequest request = null;
            UpdateCreditCardResponse response = null;
            try
            {
                AppLogger.LogInfoMessage("In LoyaltyDomain\\UpdatePhone()");
                AppLogger.LogInfoMessage("OWSUtility.GetNameService()");

                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                DateTime expiryDate = new DateTime(creditCard.ExpiryDate.Year, creditCard.ExpiryDate.Month, 01);

                request = OWSRequestConverter.GetUpdateCreditCardRequest(
                    creditCardOperaID, creditCard.NameOnCard, AppConstants.CREDIT_CARD_TYPE,
                    creditCard.CardType, creditCard.CardNumber, expiryDate,
                    creditCard.DisplaySequence, isPrimary);

                AppLogger.LogInfoMessage("Fetching response: service.UpdatePhone(request)");
                response = service.UpdateCreditCard(request);

                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<UpdateCreditCardRequest, UpdateCreditCardResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseUpdateCreditCardOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<UpdateCreditCardRequest, UpdateCreditCardResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;
            }

            return result;
        }

        /// <summary>
        /// Insert Credit Card Details
        /// </summary>
        /// <param name="nameID">NameID of the guest returned from Register User method</param>
        /// <param name="userCreditCard">Credit Card Details</param>
        /// <returns>Status of the operation, True if successful.</returns>
        public bool InsertCreditCard(string nameID,
                                     CreditCardEntity userCreditCard)
        {
            AppLogger.LogInfoMessage("In LoyaltyDomain\\InsertCreditCard()");
            NameService service = OWSUtility.GetNameService(primaryLanguageID);

            InsertCreditCardRequest request = OWSRequestConverter.GetInsertCreditCardRequest(
                nameID, userCreditCard.NameOnCard, AppConstants.CREDIT_CARD_TYPE,
                userCreditCard.CardType, userCreditCard.CardNumber,
                userCreditCard.ExpiryDate.ToDateTime());

            InsertCreditCardResponse response = service.InsertCreditCard(request);
            bool result = false;
            if (response.Result.resultStatusFlag == ServiceProxies.Name.ResultStatusFlag.SUCCESS)
            {
                result = true;
            }
            else
            {
                UserNavTracker.TrackOWSRequestResponse<InsertCreditCardRequest, InsertCreditCardResponse>(request, response);
                AppLogger.LogInfoMessage("InsertCreditCard resultStatusFlag: failed");
                OWSUtility.RaiseInsertCreditCardOWSException(response);
            }
            return result;
        }

        /// <summary>
        /// Delete the Credit Card Details for the specified Opera Id
        /// </summary>
        /// <param name="creditCardOperaID">
        /// Credit Card Opera ID</param>
        /// <returns>
        /// True, if successful else false
        /// </returns>
        public bool DeleteCreditCard(long creditCardOperaID)
        {
            bool result = false;
            DeleteCreditCardRequest request = null;
            DeleteCreditCardResponse response = null;
            try
            {
                NameService service = OWSUtility.GetNameService(primaryLanguageID);

                request = OWSRequestConverter.GetDeleteCreditCardRequest(creditCardOperaID);
                response = service.DeleteCreditCard(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Name.ResultStatusFlag.FAIL)
                {
                    result = true;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<DeleteCreditCardRequest, DeleteCreditCardResponse>(request, response);
                    AppLogger.LogInfoMessage("resultStatusFlag: failed");
                    OWSUtility.RaiseDeleteCreditCardOWSException(response);
                }
            }
            catch (SoapException soapException)
            {
                UserNavTracker.TrackOWSRequestResponse<DeleteCreditCardRequest, DeleteCreditCardResponse>(request, response);
                throw new OWSException(soapException.Message, soapException);
            }
            finally
            {
                request = null;
                response = null;
            }


            return result;
        }

        #region GetNewPassword

        /// <summary>
        /// Used for resetting password for a particular membership number.
        /// This happens if the guest forgets his password along with secret question and secret answer.
        /// </summary>
        /// <param name="membershipNumber">Membership number for which the password needs to be rested.</param>
        /// <returns>New password.</returns>
        public string GetNewPassword(string membershipNumber)
        {
            string password = string.Empty;
            SecurityService service = OWSUtility.GetSecurityService();
            GeneratePasswordRequest request = null;
            GeneratePasswordResponse response = null;
            try
            {
                request = OWSRequestConverter.GetGeneratePasswordRequest(membershipNumber);
                response = service.GeneratePassword(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Security.ResultStatusFlag.FAIL)
                {
                    password = response.Password;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<GeneratePasswordRequest, GeneratePasswordResponse>(request, response);
                }
            }
            catch (Exception)
            {
                UserNavTracker.TrackOWSRequestResponse<GeneratePasswordRequest, GeneratePasswordResponse>(request, response);
            }
            finally
            {
                request = null;
                response = null;
            }
            return password;
        }

        #endregion GetNewPassword

        #region Private Methods

        /// <summary>
        /// Check if the supplied promotion code, already exists
        /// </summary>
        /// <param name="membershipOperaID">
        /// MemeberShip Opera ID
        /// </param>
        /// <param name="promoCode">
        /// Promotion Code to be checked
        /// </param>
        /// <returns>
        /// True if already Exists else False
        /// </returns>
        private bool IsPromoCodeAlreadyAdded(string membershipOperaID, string promoCode)
        {
            bool promoCodeExists = false;
            FetchPromoSubscriptionsResponse response = null;
            FetchPromoSubscriptionsRequest request =
                OWSRequestConverter.GetFetchPromoSubscriptionRequest(membershipOperaID);
            ServiceProxies.Membership.MembershipService service = OWSUtility.GetMembershipService();
            try
            {
                response = service.FetchPromoSubscriptions(request);
                if ((response != null) && (response.Result != null))
                {
                    if (response.Result.resultStatusFlag.Equals(ServiceProxies.Membership.ResultStatusFlag.SUCCESS))
                    {
                        if ((response.PromotionSubscriptions != null) &&
                            (response.PromotionSubscriptions.PromotionSubscription != null))
                        {
                            ServiceProxies.Membership.Promotion[] promtions =
                                response.PromotionSubscriptions.PromotionSubscription;
                            foreach (
                                ServiceProxies.Membership.Promotion promo in promtions)
                            {
                                if (promoCode.Equals(promo.Code))
                                {
                                    promoCodeExists = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (SoapException soapException)
            {
                AppLogger.LogInfoMessage("resultStatusFlag: failed in the method - IsPromoCodeAlreadyAdded");
            }
            return promoCodeExists;
        }

        #endregion

        #region OWSMonitoring

        /// <summary>
        ///Method call to check the health of OWS-Name service
        /// </summary>
        /// <param name="nameID">Name Id</param>
        /// <returns>True if service is up else False</returns>
        public bool FetchNameForOWSMonitor(string nameID)
        {
            FetchNameResponse response = FetchNameService(nameID);

            bool result = true;
            if ((response != null)
                && (response.Result.resultStatusFlag == ServiceProxies.Name.ResultStatusFlag.FAIL))
            {
                if ((response.Result.Text != null) && (response.Result.Text[0].Value == AppConstants.SYSTEM_ERROR))
                {
                    result = false;
                }
            }
            return result;
        }

        /// <summary>
        ///Method call to check the health of OWS-Membership service  
        /// </summary>
        /// <returns>True if service is up else False</returns>
        public bool FetchPromoSubscriptionForOWSMonitor()
        {
            MembershipService service = OWSUtility.GetMembershipService();

            FetchPromoSubscriptionsRequest request = new FetchPromoSubscriptionsRequest();
            ServiceProxies.Membership.UniqueID uniqueId = new ServiceProxies.Membership.UniqueID();
            uniqueId.Value = AppConstants.UNIQUE_ID_VALUE;
            uniqueId.type = ServiceProxies.Membership.UniqueIDType.INTERNAL;
            request.MembershipId = uniqueId;

            FetchPromoSubscriptionsResponse response = service.FetchPromoSubscriptions(request);
            bool result = true;

            if ((response != null)
                && (response.Result.resultStatusFlag == ServiceProxies.Membership.ResultStatusFlag.FAIL))
            {
                if ((response.Result.Text != null) && (response.Result.Text[0].Value == AppConstants.SYSTEM_ERROR))
                {
                    result = false;
                }
                else if (((ServiceProxies.Membership.GDSResultStatus)response.Result).
                             GDSError.Value
                         == AppConstants.SYSTEM_ERROR)
                {
                    result = false;
                }
            }
            return result;
        }

        /// <summary>
        ///Method call to check the health of OWS-Security service
        /// </summary>
        /// <param name="userName">Membership number as username</param>
        /// <param name="password">Password</param>
        /// <returns>True if service is up else False</returns>
        public bool AuthenticateUserForOWSMonitor(string userName, string password)
        {
            SecurityService securitySvc = OWSUtility.GetSecurityService();

            AuthenticateUserRequest request = OWSRequestConverter.GetAuthenticateUserRequest(userName, password);

            AuthenticateUserResponse response = securitySvc.AuthenticateUser(request);
            bool result = true;

            if ((response != null)
                && (response.Result.resultStatusFlag == ServiceProxies.Security.ResultStatusFlag.FAIL))
            {
                if ((response.Result.Text != null) && (response.Result.Text[0].Value == AppConstants.SYSTEM_ERROR))
                {
                    result = false;
                }
            }
            return result;
        }

        #endregion OWSMonitoring
    }
}