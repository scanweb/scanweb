//  Description					: Domain class for Information OWS Calls.			      //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Priya Singh                                             //
//  Author email id				:                           							  //
//  Creation Date				: 12th December 2007									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			: 							                              //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces

using System.Collections;
using System.Collections.Generic;
using System.Web.Services.Protocols;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Information;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using System;
#endregion

    #region Scandic Namespaces

#endregion

namespace Scandic.Scanweb.BookingEngine.Domain
{
    /// <summary>
    /// InformationDomain
    /// </summary>
    public class InformationDomain : IInformationDomain
    {
        /// <summary>
        /// GetHotelInformationResponse
        /// </summary>
        /// <param name="HotelCode"></param>
        /// <returns>HotelInformationResponse</returns>
        public HotelInformationResponse GetHotelInformationResponse(string HotelCode)
        {
            HotelInformationRequest request = new HotelInformationRequest();
            HotelInformationResponse response = null;
            try
            {
                InformationService service = OWSUtility.GetInformationService();
                AppLogger.LogInfoMessage("----------Logging Information Service URL---------");
                AppLogger.LogInfoMessage(service.Url);
                request = new HotelInformationRequest();
                request.HotelInformationQuery = new HotelReference() { hotelCode = HotelCode, chainCode = AppConstants.CHAIN_CODE };
                response = service.QueryHotelInformation(request);
                if (response.Result.resultStatusFlag != ServiceProxies.Information.ResultStatusFlag.FAIL)
                {
                    return response;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<HotelInformationRequest, HotelInformationResponse>(request, response);
                    Scandic.Scanweb.Core.AppLogger.LogInfoMessage("GetHotelInformationResponse resultStatusFlag: failed");
                }
            }
            catch (SoapException SoapEx)
            {
                UserNavTracker.TrackOWSRequestResponse<HotelInformationRequest, HotelInformationResponse>(request, response);
                OWSUtility.RaiseOWSException(SoapEx.Message, SoapEx);
            }
            finally
            {
                request = null;
            }
            return null;
        }


        /// <summary>
        /// Get the Hotel Country Code for the specified hotel code
        /// </summary>
        /// <param name="HotelCode">
        /// HotelCode
        /// </param>
        /// <returns>
        /// HotelCountry Code
        /// </returns>
        public string GetHotelCountryCode(string HotelCode)
        {
            string countryCode = string.Empty;
            HotelInformationResponse response = GetHotelInformationResponse(HotelCode);
            if ((response != null) && (response.Result.resultStatusFlag != ServiceProxies.Information.ResultStatusFlag.FAIL))
            {
                if (response.HotelInformation != null)
                {
                    ServiceProxies.Information.HotelInformationResponseHotelInformation hotelInfo =
                        response.HotelInformation as HotelInformationResponseHotelInformation;
                    if ((hotelInfo != null) && (hotelInfo.HotelContactInformation != null))
                    {
                        ServiceProxies.Information.Address[] addressList = hotelInfo.HotelContactInformation.Addresses;
                        if (addressList != null && (addressList.Length > 0))
                        {
                            countryCode = addressList[0].countryCode;
                        }
                    }
                }
            }
            return countryCode;
        }

        /// <summary>
        /// Get the Currency Conversion rate for the destionation Code against the Source currency Code
        /// </summary>
        /// <param name="sourceCurrencyCode">
        /// Source Currency Code
        /// </param>
        /// <param name="destinationCurrencyCode">
        /// Destionation Currency Code
        /// </param>
        /// <param name="value">
        /// Value to be converted
        /// </param>
        /// <returns>
        /// Currency Convertion rate
        /// </returns>
        public double GetCurrency(string sourceCurrencyCode, string destinationCurrencyCode, double value)
        {
            double convertedValue = 0.0;
            AppLogger.LogInfoMessage("In InformationDomain\\GetCurrency()");
            AppLogger.LogInfoMessage("OWSUtility.GetInformationService()");
            InformationService service = OWSUtility.GetInformationService();
            CurrencyConverterRequest request = null;
            CurrencyConverterResponse response = null;
            AppLogger.LogInfoMessage(
                "Creating request object: OWSRequestConverter.GetCurrencyConveterRequest(sourceCurrencyCode, destinationCurrencyCode, value)");
            try
            {
                request =
                    OWSRequestConverter.GetCurrencyConveterRequest(sourceCurrencyCode, destinationCurrencyCode, value);

                AppLogger.LogInfoMessage("Fetching response: service.CurrencyConverter(request)");
                response = service.CurrencyConverter(request);

                if ((response != null) && (response.Result.resultStatusFlag != ServiceProxies.Information.ResultStatusFlag.FAIL))
                {
                    convertedValue = response.ToCurrencyAmt.Value;
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<CurrencyConverterRequest, CurrencyConverterResponse>(request, response);
                    AppLogger.LogInfoMessage("InformationDomain-GetCurrency resultStatusFlag: failed");
                    Hashtable exceptionData = new Hashtable();
                    exceptionData.Add("REQ:FromCurrencyCode", request.FromCurrency.currencyCode);
                    exceptionData.Add("REQ:ToCurrencyCode", request.ToCurrency.currencyCode);
                    exceptionData.Add("RES:ResultStatusFlag", response.Result.resultStatusFlag.ToString());
                    OWSUtility.RaiseGetCurrencyOWSException(response, exceptionData);
                }
            }
            catch (Exception ex)
            {
                UserNavTracker.TrackOWSRequestResponse<CurrencyConverterRequest, CurrencyConverterResponse>(request, response);
            }
            finally
            {
                request = null;
                response = null;
            }
            return convertedValue;
        }


        /// <summary>
        /// Get the List of Membership Types
        /// </summary>
        public Dictionary<string, string> GetMembershipTypes()
        {
            Dictionary<string, string> parterProgramCodeMap = new Dictionary<string, string>();

            AppLogger.LogInfoMessage("In InformationDomain\\GetMembershipTypes()");
            AppLogger.LogInfoMessage("OWSUtility.GetInformationService()");

            InformationService service = OWSUtility.GetInformationService();
            AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetMembershipTypeRequest()");
            LovRequest request = null;
            LovResponse response = null;
            try
            {
                request = OWSRequestConverter.GetMembershipTypeRequest();

                AppLogger.LogInfoMessage("Fetching response: service.QueryLov(request)");
                response = service.QueryLov(request);

                if ((response != null) && (response.Result.resultStatusFlag != ServiceProxies.Information.ResultStatusFlag.FAIL))
                {
                    LovQueryResultType[] results = response.LovQueryResult;

                    if (results != null && results.Length > 0)
                    {
                        LovValueType[] values = results[0].LovValue;
                        if (values != null && values.Length > 0)
                        {
                            int totalCount = values.Length;
                            for (int count = 0; count < totalCount; count++)
                            {
                                if (!OWSUtility.IsPartnerCardExcluded(values[count].Value))
                                    parterProgramCodeMap.Add(values[count].Value, values[count].description);
                            }
                        }
                    }
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<LovRequest, LovResponse>(request, response);
                }
            }
            catch (Exception ex)
            {
                UserNavTracker.TrackOWSRequestResponse<LovRequest, LovResponse>(request, response);
            }
            finally
            {
                request = null;
                response = null;
            }

            return parterProgramCodeMap;
        }

        public Dictionary<string, string> GetUserInterestTypes()
        {
            Dictionary<string, string> userInterestType = new Dictionary<string, string>();

            AppLogger.LogInfoMessage("In InformationDomain\\GetUserInterestTypes()");
            AppLogger.LogInfoMessage("OWSUtility.GetInformationService()");

            InformationService service = OWSUtility.GetInformationService();
            AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetUserInterestTypes()");
            LovRequest request = null;
            LovResponse response = null;
            try
            {
                request = OWSRequestConverter.GetUserInterestTypeRequest();

                AppLogger.LogInfoMessage("Fetching response: service.QueryLov(request)");
                response = service.QueryLov(request);

                if ((response != null) && (response.Result.resultStatusFlag != ResultStatusFlag.FAIL))
                {
                    LovQueryResultType[] results = response.LovQueryResult;

                    if (results != null && results.Length > 0)
                    {
                        LovValueType[] values = results[0].LovValue;
                        if (values != null && values.Length > 0)
                        {
                            int totalCount = values.Length;
                            for (int count = 0; count < totalCount; count++)
                            {
                                userInterestType.Add(values[count].Value, values[count].description);
                            }
                        }
                    }
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<LovRequest, LovResponse>(request, response);
                }
            }
            catch
            {
                UserNavTracker.TrackOWSRequestResponse<LovRequest, LovResponse>(request, response);
            }
            finally
            {
                request = null;
                response = null;
            }

            return userInterestType;
        }

        /// <summary>
        /// Gets list of configured titles. 
        /// </summary>
        /// <param name="languageCode"></param>
        /// <returns>list of configured titles. </returns>
        public Dictionary<string, string> GetConfiguredTitles(string languageCode)
        {
            Dictionary<string, string> nameTitleCodeMap = new Dictionary<string, string>();
            AppLogger.LogInfoMessage("In InformationDomain\\GetConfiguredTitles()");
            AppLogger.LogInfoMessage("OWSUtility.GetInformationService()");

            InformationService service = OWSUtility.GetInformationService();
            service.OGHeaderValue.primaryLangID = languageCode;

            AppLogger.LogInfoMessage("Creating request object: OWSRequestConverter.GetConfiguredTitlesRequest()");
            LovRequest request = null;
            LovResponse response = null;
            try
            {
                request = OWSRequestConverter.GetConfiguredTitlesRequest();

                AppLogger.LogInfoMessage("Fetching response: service.QueryLov(request)");
                response = service.QueryLov(request);

                if ((response != null) && (response.Result.resultStatusFlag != ServiceProxies.Information.ResultStatusFlag.FAIL))
                {
                    LovQueryResultType[] results = response.LovQueryResult;

                    if (results != null && results.Length > 0)
                    {
                        LovValueType[] values = results[0].LovValue;
                        if (values != null && values.Length > 0)
                        {
                            int totalCount = values.Length;
                            for (int count = 0; count < totalCount; count++)
                            {
                                nameTitleCodeMap.Add(values[count].Value, values[count].description);
                            }
                        }
                    }
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<LovRequest, LovResponse>(request, response);
                }
            }
            catch (Exception ex)
            {
                UserNavTracker.TrackOWSRequestResponse<LovRequest, LovResponse>(request, response);
            }
            finally
            {
                request = null;
                response = null;
            }
            return nameTitleCodeMap;
        }

        #region OWSMonitoring

        /// <summary>
        ///Method call to check the health of OWS-Information service
        /// </summary>
        /// <returns>True if service is up else False</returns>
        public bool GetCreditCardTypeForOWSMonitor()
        {
            InformationService service = OWSUtility.GetInformationService();
            LovRequest request = null;
            LovResponse response = null;
            bool result = true;
            try
            {
                request = new LovRequest();
                LovQueryType queryType = new LovQueryType();
                string[] queryText = new string[1];
                queryText[0] = "CREDITCARDTYPE";
                queryType.Text = queryText;
                request.Item = queryType;
                response = service.QueryLov(request);
               
                if ((response != null)
                    && (response.Result.resultStatusFlag == ServiceProxies.Information.ResultStatusFlag.FAIL))
                {
                    if ((response.Result.Text != null) && (response.Result.Text[0].Value == AppConstants.SYSTEM_ERROR))
                    {
                        result = false;
                    }
                }
                else
                {
                    UserNavTracker.TrackOWSRequestResponse<LovRequest, LovResponse>(request, response);
                }
            }
            catch (Exception ex)
            {
                UserNavTracker.TrackOWSRequestResponse<LovRequest, LovResponse>(request, response);
            }
            finally
            {
                request = null;
                response = null;
            }
            return result;
        }

        #endregion OWSMonitoring
    }
}