﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using Scandic.Scanweb.BookingEngine.ServiceProxies.AdvancedReservationService;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System.Web;

namespace Scandic.Scanweb.BookingEngine.Domain
{
    public class AdvancedReservationDomain : IAdvancedReservationDomain
    {
        /// <summary>
        /// Does MakePayment call to update Opera after Nets capture.
        /// </summary>
        /// <param name="reservationNumber"></param>
        /// <param name="hotelOperaCode"></param>
        /// <param name="hotelChainCode"></param>
        /// <param name="totalAmount"></param>
        /// <param name="paymentInfo"></param>
        /// <param name="transactionId"></param>
        public bool MakePayment(string reservationNumber, string legNumber, string hotelOperaCode, string hotelChainCode,
                                string totalAmount, PaymentInfo paymentInfo, string transactionId)
        {
            bool makePaymentStatus = true;
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            MakePaymentRequest makePaymentRequest = null;
            MakePaymentResponse makePaymentResponse = null;
            try
            {
                ResvAdvancedService advancedReservationService = OWSUtility.GetAdvancedReservationService();
                makePaymentRequest = new MakePaymentRequest();
                makePaymentRequest.CreditCardInfo = new CreditCardInfo();
                makePaymentRequest.CreditCardInfo.Item = new CreditCardPayment();
                CreditCardPayment creditCardPayment = new CreditCardPayment();
                creditCardPayment.chipAndPinSpecified = true;
                creditCardPayment.chipAndPin = false;
                creditCardPayment.cardType = paymentInfo.CardType;
                creditCardPayment.cardHolderName = paymentInfo.PanHash;
                creditCardPayment.Item = AppConstants.ModifyCreditCardNumberForMakePayment ? paymentInfo.CreditCardNumberMasked.Replace('*', 'x') :
                                         paymentInfo.CreditCardNumberMasked;
                creditCardPayment.expirationDate = paymentInfo.ExpiryDate;
                creditCardPayment.ApprovalCode = PaymentConstants.StringNeeded;
                makePaymentRequest.CreditCardInfo.Item = creditCardPayment;

                makePaymentRequest.Posting = new Posting();
                makePaymentRequest.Posting.ChargeSpecified = true;
                makePaymentRequest.Posting.Charge = Convert.ToDecimal(totalAmount);
                makePaymentRequest.Posting.ShortInfo = PaymentConstants.ShortInfo;
                makePaymentRequest.Posting.LongInfo = PaymentConstants.LongInfo;
                makePaymentRequest.Posting.StationID = PaymentConstants.Scanweb;
                makePaymentRequest.Posting.ReservationRequestBase = new ReservationRequestBase();
                makePaymentRequest.Posting.ReservationRequestBase.HotelReference = new HotelReference();
                makePaymentRequest.Posting.ReservationRequestBase.HotelReference.hotelCode = hotelOperaCode;
                makePaymentRequest.Posting.ReservationRequestBase.HotelReference.chainCode = hotelChainCode;

                UniqueID reservationUniqueID = new UniqueID();
                reservationUniqueID.type = UniqueIDType.INTERNAL;
                reservationUniqueID.source = PaymentConstants.PMSID;
                reservationUniqueID.Value = reservationNumber;
                UniqueID legNumberUniqueID = new UniqueID();
                legNumberUniqueID.type = UniqueIDType.INTERNAL;
                legNumberUniqueID.source = PaymentConstants.PMSLEGNO;
                legNumberUniqueID.Value = legNumber;
                makePaymentRequest.Posting.ReservationRequestBase.ReservationID = new UniqueID[2];
                makePaymentRequest.Posting.ReservationRequestBase.ReservationID[0] = reservationUniqueID;
                makePaymentRequest.Posting.ReservationRequestBase.ReservationID[1] = legNumberUniqueID;
                makePaymentRequest.Reference = transactionId;

                timer.Start();
                string reqStartTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                makePaymentResponse = advancedReservationService.MakePayment(makePaymentRequest);
                timer.Stop();
                string responseEndTime = DateTime.Now.ToString(AppConstants.REQUEST_DATETIME_FORMAT);
                string requestSource = CoreUtil.GetRequestSource();
                string currentSessionId = string.Empty;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    currentSessionId = HttpContext.Current.Session.SessionID;
                }
                //SessionID-RequestName-RequestType-SearchType-StatTime-EndTime-ElapsedTime-ServerName-Source
                string strLog = currentSessionId + ", " + makePaymentRequest.GetType().Name + ", Sync, MakePayment, " + reqStartTime + ", " + responseEndTime + ", " +
                    timer.Elapsed.ToString() + ", " + System.Environment.MachineName + ", " + requestSource;

                AppLogger.CSVInfoLogger(strLog);
            }
            catch (Exception ex)
            {
                makePaymentStatus = false;
                AppLogger.LogOnlinePaymentTransactionsInfoMessage(
                      string.Format("In MakePayment for Nets Payment Flow with TransactionID: {0}, Payment Status: {5} and the BookingID: {1} for the hotel: {2}, from the WebServer: {3} at {4}. Exception message is: {6}, and Exception stacktrace is {7}.",
                     transactionId, reservationNumber, hotelOperaCode, System.Environment.MachineName, DateTime.Now, makePaymentStatus.ToString(), ex.Message, ex.StackTrace));

                AppLogger.LogOnlinePaymentFatalException(ex, ex.Message);
            }
            finally
            {
                makePaymentRequest = null;
                makePaymentResponse = null;
                timer = null;
            }
            return makePaymentStatus;
        }
    }
}
