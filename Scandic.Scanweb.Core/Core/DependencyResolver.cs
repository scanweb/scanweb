﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// This class use MEF for dependency injection, each type that needs to be exported
    /// should be marked with Export attribute, which will be loaded in container at time
    /// on initializtion
    /// </summary>
    public sealed class DependencyResolver
    {
        #region Declaration

        private static DependencyResolver instance;
        private static object syncLock = new object();
        private CompositionContainer container;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public DependencyResolver()
        {
            ConfigureContainer();
        }

        #endregion

        #region Property

        public static DependencyResolver Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncLock)
                    {
                        if (instance == null)
                        {
                            instance = new DependencyResolver();
                        }
                    }
                }

                return instance;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// ConfigureContainer
        /// </summary>
        private void ConfigureContainer()
        {
            var catalogs = new AggregateCatalog();
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Plugins");

            catalogs.Catalogs.Add(new DirectoryCatalog(path, "*.dll"));
            container = new CompositionContainer(catalogs, true);
        }

        /// <summary>
        /// Resolves singly registered service/type that support arbitrary object creation.
        /// </summary>
        /// <param name="serviceType">The type of the requested service or object.</param>
        /// <returns>The requested service or object.</returns>
        public object GetService(Type serviceType)
        {
            var _exports = container.GetExports(serviceType, null, null);
            var _export = _exports.FirstOrDefault();
            object returnExport = _export == null ? null : _export.Value;
            if (_exports != null)
                container.ReleaseExports(_exports);
            return returnExport;
        }

        /// <summary>
        /// Resolves multiply registered services/types.
        /// </summary>
        /// <param name="serviceType">The type of the requested services.</param>
        /// <returns>The requested services.</returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            var _exports = container.GetExportedValues<object>(serviceType.FullName);
            if (_exports.Any())
            {
                return _exports.AsEnumerable();
            }
            return new List<object>();
        }

        /// <summary>
        /// 
        /// </summary>
        public void ClearResources()
        {
            this.container.Dispose();
            syncLock = null;
        }
        #endregion
    }
}