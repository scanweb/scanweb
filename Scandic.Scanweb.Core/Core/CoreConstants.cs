//  Description					: CoreConstants                                           //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// This class holds all constants.
    /// </summary>
    public class CoreConstants
    {
        /// <summary>
        /// COMMA
        /// </summary>
        public const string COMMA = ",";
        /// <summary>
        /// SPACE
        /// </summary>
        public const string SPACE = " ";
        /// <summary>
        /// SLASH
        /// </summary>
        public const string SLASH = "/";
        /// <summary>
        /// HYPHEN
        /// </summary>
        public const string HYPHEN = "-";
        /// <summary>
        /// YEARPREFIX
        /// </summary>
        public const string YEARPREFIX = "20";
        /// <summary>
        /// PMCONSTANT
        /// </summary>
        public const string PMCONSTANT = "PM";
        /// <summary>
        /// AMCONSTANT
        /// </summary>
        public const string AMCONSTANT = "AM";
        /// <summary>
        /// DOTCONSTANT
        /// </summary>
        public const string DOTCONSTANT = ".";
    }
}