﻿//  Description					:   CacheManager                                          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//  Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace Scandic.Scanweb.Core.Core.Interface
{
    /// <summary>
    /// This abstract class defines the contract for all the operations that will be used for working with Cache.
    /// It also provides the implementation for some of the common methods in the contract.
    /// </summary>
    public abstract class CacheManager
    {
        #region Abstract Methods

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets StopCacheMonitor Value. 
        /// </summary>
        public bool StopCacheMonitor
        {
            get { return AppConstants.StopCacheMonitor; }
        }

        /// <summary>
        /// Gets CacheMonitorTimeOut Value.
        /// </summary>
        public int CacheMonitorTimeOut
        {
            get { return AppConstants.CacheMonitorTimeOut; }
        }

        /// <summary>
        /// Gets CacheMonitorTimeOut Value.
        /// </summary>
        public int CacheDataTimeOut
        {
            get
            {
                int cacheDataTimeOut = default(int);
                if (StopCacheMonitor)
                {
                    cacheDataTimeOut = AppConstants.CacheMonitorTimeOut;
                }
                else
                {
                    cacheDataTimeOut = AppConstants.CacheDataTimeOut;
                }
                return cacheDataTimeOut;
            }
        }

        /// <summary>
        /// Gets Cache Value for the Cache Key passed.
        /// </summary>
        /// <typeparam name="T">DataType of Cache Value.</typeparam>
        /// <param name="cacheKey">Cache Key</param>
        /// <returns>Cache Value</returns>
        public T LookInCache<T>(string cacheKey)
        {
            
            var cacheMonitorData = HttpRuntime.Cache.Get(string.Format("{0}{1}", cacheKey, AppConstants.MonitorText)) as CacheMonitorData;
            if (cacheMonitorData != null)
            {
                cacheMonitorData.NumberOfHitsToCache++;
            }
            var cacheData = (T)HttpRuntime.Cache.Get(cacheKey);
            if (cacheData == null)
            {
                AppLogger.LogOnlinePaymentInfoMessage(string.Format("Cache data doesn't exist for the CacheKey: {0} at {1}",cacheKey,DateTime.Now));
            }
            return cacheData;
        }

        /// <summary>
        /// Adds data to cache. 
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <param name="value">Cache Data</param>
        /// <param name="cacheFillerParameters">Parameters required for re-filling Cache.</param>
        /// <param name="cacheFiller">Delegate responsible for re-filling Cache,</param>
        public void AddToCache(string key, object value, Collection<object> cacheFillerParameters, Delegate cacheFiller)
        {
            AppLogger.LogOnlinePaymentInfoMessage(string.Format("Item with the Key: {0} is added to the Cache at {1}", key, DateTime.Now));
            HttpRuntime.Cache.Insert(key, value, null, DateTime.Now.AddSeconds(CacheDataTimeOut), Cache.NoSlidingExpiration);
            if (!StopCacheMonitor && (HttpRuntime.Cache.Get(string.Format("{0}{1}", key, AppConstants.MonitorText)) == null))
            {
                var cacheMonitorData = new CacheMonitorData();
                cacheMonitorData.CacheDataKey = key;
                cacheMonitorData.CacheFiller = cacheFiller;
                cacheMonitorData.CacheFillerParameters = cacheFillerParameters;
                HttpRuntime.Cache.Insert(string.Format("{0}{1}", key, AppConstants.MonitorText), cacheMonitorData, null, DateTime.Now.AddSeconds(CacheMonitorTimeOut), Cache.NoSlidingExpiration, CacheItemPriority.Default, CacheMonitor);
            }
        }

        /// <summary>
        /// Adds data to cache with a file dependency. 
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <param name="value">Cache Data</param>
        ///<param name="fileName">File Name</param>
        public void AddToCache(string key, object value, string fileName)
        {
            HttpRuntime.Cache.Insert(key, value, new CacheDependency(fileName));
        }

        /// <summary>
        /// Adds data to cache with a file dependency. 
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <param name="value">Cache Data</param>
        ///<param name="fileName">File Name</param>
        ///<param name="expirationTimeInHours">ExpirationTimeInHours</param>
        public void AddToCache(string key, object value, string fileName, int expirationTimeInHours)
        {
            HttpRuntime.Cache.Insert(key, value, new CacheDependency(fileName), DateTime.Now.AddHours(expirationTimeInHours), Cache.NoSlidingExpiration); 
        }

        /// <summary>
        /// Adds data to cache with a file dependency. 
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <param name="value">Cache Data</param>
        ///<param name="fileName">File Name</param>
        ///<param name="expiryDate">expiryDate</param>
        public void AddToCache(string key, object value, string fileName, DateTime expiryDate)
        {
            HttpRuntime.Cache.Insert(key, value, new CacheDependency(fileName), expiryDate, Cache.NoSlidingExpiration);
        }

        /// <summary>
        /// Adds data to cache with an expiration time. 
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <param name="value">Cache Data</param>
        ///<param name="expirationTimeInHours">Expiration time in hours.</param>
        public void AddToCache(string key, object value, int expirationTimeInHours)
        {
            HttpRuntime.Cache.Insert(key, value, null, DateTime.Now.AddHours(expirationTimeInHours), Cache.NoSlidingExpiration);
        }

        /// <summary>
        /// Adds data to cache. 
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <param name="value">Cache Data</param>
        public void AddToCache(string key, object value)
        {
            HttpRuntime.Cache.Insert(key, value);
        }


        /// <summary>
        /// Removes a key from Cache.
        /// </summary>
        /// <param name="key">Key</param>
        public void RemoveFromCache(string key)
        {
            HttpRuntime.Cache.Remove(key);
        }

        /// <summary>
        /// Callback which gets called on the Cache expiration.
        /// </summary>
        /// <param name="k">Cacke Key of CacheMonitor</param>
        /// <param name="v">Cacke data of CacheMonitor</param>
        /// <param name="r">Cache expiration reason</param>
        public void CacheMonitor(String key, Object value, CacheItemRemovedReason reason)
        {
            var cacheMonitorData = (CacheMonitorData)value;
            try
            {
                if (reason == CacheItemRemovedReason.Expired)
                {
                    if (cacheMonitorData.NumberOfHitsToCache > 0)
                    {
                        AppLogger.LogOnlinePaymentInfoMessage(string.Format("Refilling request for the CacheKey: {0} at {1}.", key, DateTime.Now));
                        cacheMonitorData.CacheFiller.DynamicInvoke(cacheMonitorData.CacheFillerParameters.ToArray());
                    }
                    else
                    {
                        AppLogger.LogOnlinePaymentInfoMessage(string.Format("CacheKey: {0} is removed by the CacheMonitor at {1}; as there are no hits for this CacheItem.", key, DateTime.Now));
                        HttpRuntime.Cache.Remove(cacheMonitorData.CacheDataKey);
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, string.Format("Refilling failed for the CacheKey: {0} at {1}.",key,DateTime.Now));
                HttpRuntime.Cache.Remove(cacheMonitorData.CacheDataKey);
            }
        }
        #endregion
    }
}
