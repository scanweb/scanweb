﻿//  Description					:   IApplication                                          //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Core.Interface
{
    /// <summary>
    /// This interface define Application level functions that each scanweb 
    /// application(Scanweb or Mobile etc) should impliment to allow parent
    /// application to control it behavior.
    /// </summary>
    public interface IApplication
    {
        /// <summary>
        /// This method will be called by the parent application at the
        /// time when application starts for the first time. This will
        /// allow implimenting application to perform any intializing
        /// activities or tasks before any request comes to it.
        /// </summary>
        /// <param name="data">Any piece of information
        /// that parent application may want to pass to 
        /// child applications.</param>
        void Initialize(object data);
    }
}