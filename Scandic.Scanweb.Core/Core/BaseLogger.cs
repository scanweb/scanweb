// ------------------------------------------------------------------------------------------------------
// Description:                This class is intended to define the base class for all loggers in Scanweb.
// Created By:                 Nandkishore M
// Created Date:               20-Dec-07
// Version:                    1.0
// ------------------------------------------------------------------------------------------------------
// Modification History       
// Modified By:
// Modification Description:
// ------------------------------------------------------------------------------------------------------

using log4net;

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// This class defines the core attributes required by all loggers in Scanweb.
    /// </summary>
    public class BaseLogger
    {
        protected static ILog RootLogger;
        protected static ILog FatalLogger;
        protected static ILog OWSLogger;
        protected static ILog GeneralInfoLogger;
        protected static ILog OnlinePaymentInfoLogger;
        protected static ILog OnlinePaymentFatalLogger;
        protected static ILog OnlinePaymentTransactionsInfoLogger;
        protected static ILog SessionNotValidLogger;
        protected static ILog SmtpAppenderLogger;
        protected static ILog ReservationRatesLogger;
        protected static ILog CSVLogger;
    }
}