﻿

using System.Xml;
using log4net.Core;
using log4net.Layout;
using System.Web;

namespace Scandic.Scanweb.Core
{
    public class CustomLayout : XmlLayoutBase
    {
        protected override void FormatXml(XmlWriter writer, LoggingEvent loggingEvent)
        {

            var currentSessionId = string.Empty;
            if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
            {
                currentSessionId = HttpContext.Current.Session.SessionID;
            } 
            writer.WriteStartElement("Scanweb-LogInfo");

            writer.WriteStartElement("TraceID");
            writer.WriteString(currentSessionId);
            writer.WriteEndElement();

            writer.WriteStartElement("LogType");
            writer.WriteString(loggingEvent.LoggerName);
            writer.WriteEndElement();

            writer.WriteStartElement("LogLevel");
            writer.WriteString(loggingEvent.Level.Name);
            writer.WriteEndElement();

            writer.WriteStartElement("TimeStamp");
            writer.WriteString(loggingEvent.TimeStamp.ToString("dd MMM yyyy HH:mm:ss,fff"));
            writer.WriteEndElement();

            var message = loggingEvent.ExceptionObject != null
                              ? string.Format("{0}_{1}", loggingEvent.ExceptionObject.Message, loggingEvent.RenderedMessage.Replace("<![CDATA[", string.Empty).Replace("]]", string.Empty))
                              : loggingEvent.RenderedMessage.Replace("<![CDATA[", string.Empty).Replace("]]", string.Empty);
            writer.WriteStartElement("Message");
            writer.WriteCData(message);
            writer.WriteEndElement();

            var stackTrace = loggingEvent.ExceptionObject != null
                              ? loggingEvent.ExceptionObject.StackTrace
                              : string.Empty;
            writer.WriteStartElement("StackTrace");
            writer.WriteCData(stackTrace);
            writer.WriteEndElement();

            writer.WriteEndElement();
        }
    } 
}
