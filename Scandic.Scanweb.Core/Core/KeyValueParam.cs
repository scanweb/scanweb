﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Web;
using System.Xml.Serialization;
using System.IO;
using Scandic.Scanweb.Core;
using System.Runtime.Serialization;
using System.Text;
using System.ComponentModel;
using System.Xml;
using System.Linq;

namespace Scandic.Scanweb.Core
{
    [Serializable]
    public class KeyValueParam
    {
        [XmlAttribute("Key")]
        public string Key { get; set; }

        [XmlText()]
        public string Value { get; set; }

        public KeyValueParam(string key, string val)
        {
            this.Key = key;
            this.Value = val;
        }
        public KeyValueParam()
        {
            this.Key = string.Empty;
            this.Value = string.Empty;
        }

    }

    [Serializable]
    public class ActionItem
    {
        [XmlAttribute("Date")]
        public DateTime ActionDate
        { get; set; }

        [XmlElement("Current")]
        public string CurrentURL { get; set; }
        [XmlElement("Previous")]
        public string PreviousURL { get; set; }

        [XmlArrayItem("Parameter", typeof(KeyValueParam))]
        [XmlArray("Parameters")]
        public List<KeyValueParam> Parameters { get; set; }

        [XmlElement("OWS", IsNullable = true)]
        public OWSReqRes OWSS { get; set; }

        public ActionItem()
        {
        }

    }

    [Serializable]
    public class OWSReqRes
    {
        [XmlElement("OWSRequest")]
        public XmlElement OperaRequest { get; set; }
        [XmlElement("OWSResponse")]
        public XmlElement OperaResponse { get; set; }


        private Object operaRequestObject;
        private Object operaResponseObject;

        private Type requestType { get; set; }
        private Type responseType { get; set; }

        public OWSReqRes()
        {
        }

        public void SetOWSRequestRespone<reqT, resT>(reqT owsRequest, resT owsResponse)
        {
            requestType = typeof(reqT);
            responseType = typeof(resT);
            operaRequestObject = owsRequest;
            operaResponseObject = owsResponse;
        }
        public void SearializeAll()
        {
            this.OperaRequest = SearializeReqRes(requestType, operaRequestObject);
            this.OperaResponse = SearializeReqRes(responseType, operaResponseObject);
        }
        public XmlElement SearializeReqRes(Type T, Object obj)
        {
            StringWriter sWriter = new StringWriter();
            XmlDocument doc = new XmlDocument();
            try
            {
                XmlSerializer xmlSer = new XmlSerializer(T);
                xmlSer.Serialize(sWriter, obj);
                doc.Load(new StringReader(sWriter.ToString()));
                return doc.DocumentElement;
            }
            catch (Exception ex)
            {
                AppLogger.LogCustomException(ex, TrackerConstants.OWSREQRES, ex.Message);
            }
            finally
            {
                sWriter.Flush();
                doc = null;
            }
            return null;
        }



    }

    [Serializable]
    [XmlRoot(TrackerConstants.TRACKINGUSERACTION)]
    public class ActionItems
    {
        [XmlElement("SessionIdentity")]
        public string SessionIdentifier { get; set; }

        [XmlArrayItem("UserActivity", typeof(ActionItem))]
        [XmlArray("UserActivityList")]
        public List<ActionItem> Items { get; set; }

        public ActionItems()
        {
            this.SessionIdentifier = Guid.NewGuid().ToString();
            Items = new List<ActionItem>();
        }
    }

    public interface INavigationTraking
    {
        List<KeyValueParam> GenerateInput(string actionName);
    }
    public class UserNavTracker
    {
        public static void TrackAction(INavigationTraking navTrack, string actionName)
        {
            try
            {
                if (TrackerConstants.Is_Tracker_Enabled)
                {
                    ActionItem actionItem = new ActionItem();
                    actionItem.ActionDate = DateTime.Now;
                    actionItem.CurrentURL = HttpContext.Current.Request.Url.AbsolutePath;
                    actionItem.PreviousURL = (HttpContext.Current.Request.UrlReferrer != null) ? HttpContext.Current.Request.UrlReferrer.AbsolutePath : string.Empty;
                    actionItem.Parameters = navTrack.GenerateInput(actionName);
                    UNTSessionWrapper.UserActionStore.Items.Add(actionItem);
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogCustomException(ex, TrackerConstants.TRACKINGUSERACTION, ex.Message);
            }

        }

        public static void TrackAction(ActionItem trackingItem, bool clearSession)
        {
            try
            {
                if (TrackerConstants.Is_Tracker_Enabled)
                {
                    if (clearSession || UNTSessionWrapper.UserActionStore == null)
                        UNTSessionWrapper.UserActionStore = new ActionItems();

                    UNTSessionWrapper.UserActionStore.Items.Add(trackingItem);
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogCustomException(ex, TrackerConstants.TRACKINGUSERACTION, ex.Message);
            }
        }

        public static void TrackNavigation()
        {
            try
            {
                if (TrackerConstants.Is_Tracker_Enabled)
                {
                    ActionItem actionItem = new ActionItem();
                    actionItem.ActionDate = DateTime.Now;
                    actionItem.CurrentURL = HttpContext.Current.Request.Url.AbsolutePath;
                    actionItem.PreviousURL = (HttpContext.Current.Request.UrlReferrer != null) ? HttpContext.Current.Request.UrlReferrer.AbsolutePath : string.Empty;
                    UNTSessionWrapper.UserActionStore.Items.Add(actionItem);
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogCustomException(ex, TrackerConstants.TRACKINGNAVIGATION, ex.Message);
            }
        }

        public static void TrackOWSRequestResponse<T, U>(T OwsRequest, U OwsResponse)
        {
            try
            {
                if (TrackerConstants.Is_Tracker_Enabled && UNTSessionWrapper.UserActionStore != null)
                {
                    ActionItem actionItem = new ActionItem();
                    actionItem.ActionDate = DateTime.Now;
                    actionItem.CurrentURL = string.Empty;
                    actionItem.PreviousURL = string.Empty;
                    actionItem.Parameters = new List<KeyValueParam>();
                    actionItem.OWSS = new OWSReqRes();
                    actionItem.OWSS.SetOWSRequestRespone<T, U>(OwsRequest, OwsResponse);
                    UNTSessionWrapper.UserActionStore.Items.Add(actionItem);
                }

            }
            catch (Exception ex)
            {
                AppLogger.LogCustomException(ex, TrackerConstants.TRACKINGOWS, ex.Message);
            }
        }

        public static string GetTrackedData()
        {
            StringWriter sWriter = new StringWriter();
            XmlSerializer xmlSer = null;
            try
            {
                var trackedData = UNTSessionWrapper.UserActionStore;
                trackedData.Items.ForEach(c => { if (c.OWSS != null) c.OWSS.SearializeAll(); });

                xmlSer = new XmlSerializer(trackedData.GetType());
                xmlSer.Serialize(sWriter, trackedData);
                return sWriter.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(TrackerConstants.SEARILIZATIONERROR, ex);
            }
            finally
            {
                xmlSer = null;
                sWriter.Flush();
            }
        }

        public static void LogAndClearTrackedData(Exception exception)
        {
            try
            {
                if (TrackerConstants.Is_Tracker_Enabled)
                {
                    AppLogger.LogFatalException(exception, GetTrackedData());
                    UserNavTracker.ClearTrackedData();
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogCustomException(ex, TrackerConstants.TRACKINGERROR, ex.Message);
            }
        }
        public static void ClearTrackedData()
        {
            UNTSessionWrapper.UserActionStore = null;

        }
        public static void LogTrackedData(string message)
        {
            try
            {
                if (TrackerConstants.Is_Tracker_Enabled)
                {
                    AppLogger.LogFatalException(new Exception(message), GetTrackedData());
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogCustomException(ex, TrackerConstants.TRACKINGERROR, ex.Message);
            }
        }



    }
}

