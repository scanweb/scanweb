//  Description					: DestinationInfo                                         //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// Stores destination name and Opera Id
    /// </summary>
    public class DestinationInfo
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public DestinationInfo()
        {
        }

        private string _destinationName = string.Empty;

        /// <summary>
        /// Gets/Sets DestinationName
        /// </summary>
        public string DestinationName
        {
            get { return _destinationName; }
            set { _destinationName = value; }
        }

        private string _operaId = string.Empty;


        /// <summary>
        /// Gets/Sets OperaID
        /// </summary>
        public string OperaID
        {
            get { return _operaId; }
            set { _operaId = value; }
        }
    }
}