﻿//  Description					:   CacheMonitorData                                      //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//  Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Scandic.Scanweb.Core.Core
{
    /// <summary>
    /// This class contains data members required for the CacheMonitor operations.
    /// </summary>
    public class CacheMonitorData
    {
        /// <summary>
        /// Parameters required for re-filling the cache.
        /// </summary>
        public Collection<object> CacheFillerParameters;
        /// <summary>
        /// Delegate responsible for re-filling the cache.
        /// </summary>
        public Delegate CacheFiller;
        /// <summary>
        /// Count of number of hits to the Cache Key.
        /// </summary>
        public int NumberOfHitsToCache;
        /// <summary>
        /// Cache Key the monitor is responsible for tracking. 
        /// </summary>
        public string CacheDataKey;
    }
}
