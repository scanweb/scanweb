using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// This class is used for identifying event location happened for SiteCatalyst.
    /// </summary>
    public static class SiteCatalystLocation
    {
        /// <summary>
        /// SiteCatalyst Deepliking location
        /// </summary>
        private static string deepLinking = "unavailable";

        public static string DEEPLINKING
        {
            get { return deepLinking; }
        }

        /// <summary>
        /// SiteCatalyst tree view location
        /// </summary>
        private static string treeView = "Tree View";

        public static string TREEVIEW
        {
            get { return treeView; }
        }

        /// <summary>
        /// SiteCatalyst search box location
        /// </summary>
        private static string searchBox = "Search Box";

        public static string SEARCHBOX
        {
            get { return searchBox; }
        }

        /// <summary>
        /// SiteCatalyst map location
        /// </summary>
        private static string map = "Map";

        public static string MAP
        {
            get { return map; }
        }
    }

    /// <summary>
    /// AppConstants
    /// </summary>
    public class AppConstants
    {
        public const string TITLE_DEFAULT_VALUE = "Ausw�hlen";
        public const string ZERO = "0";
        public const string ONE = "1";
        public const string TWO = "2";
        public const string THREE = "3";
        public const string FOUR = "4";
        public const string FIVE = "5";
        public const string SIX = "6";
        public const string SEVEN = "7";
        public const string EIGHT = "8";
        public const string NINE = "9";
        public const string TEN = "10";
        public const int ONE_INT = 1;
        public const int TWO_INT = 2;

        public const string COMMA = ",";
        public const char CHAR_COMMA = ',';
        public const char CHAR_EQUAL = '=';
        public const string SPACE = " ";
        public const char SPACE_CHAR = ' ';
        public const string HYPHEN = "-";
        public const string COLON = ":";
        public const string DUMMYZIPCODE = "-----";
        public const string AT = "@";
        public const string SLASH = "/";
        public const string BRACKET_OPEN = "(";
        public const string BRACKET_CLOSE = ")";
        public const string DOT = ".";
        public const string STAR = "*";

        public const string FALSE = "FALSE";
        public const string TRUE = "TRUE";

        public const int MAX_LENGTH_SHORTDESCRIPTION_LISTING_CONTROL = 150;
        public const string TRIPPLE_DOTS = "...";

        public const string LANGUAGE = "language";
        public const string CULTURE = "culture";
        public const string DEFAULT_LANGUAGE = "en";
        public const string DEFAULT_CULTURE = "en-US";
        public const string BOLDSTART_TAG = "<b>";
        public const string BOLDEND_TAG = "</b>";
        public const string BREAK_TAG = "<br/>";
        public const string APPROX = "~";
        public const string PLUS = "+";
        public const string TRIP_ADVISOR_DEFAULT_URL_LANG = "EN_US";
        public const string PAGE_SEGMENT_KEY = "PageSegmentKey";
        public const string REWARD_NIGHT_SEGMENT = "RewardNights";
        public const string USE_POINTS_SEGMENT = "UsePoints";
        public const string DEEPLINK_VIEW_MODIFY_RESERVATION_ID = "RID";
        public const string DEEPLINK_VIEW_MODIFY_LAST_NAME = "LNM";
        public const string DLREDIRECT = "dlredirect";
        /// <summary>
        /// higher
        /// </summary>
        public const string HIGHERVALUE = "Higher Rate";

        /// <summary>
        /// lower
        /// </summary>
        public const string LOWERVALUE = "Lower Rate";

        /// <summary>
        /// same
        /// </summary>
        public const string SAMEVALUE = "Same Rate";
        public const string EMAILTYPE_EMAIL = "EMAIL";
        public const string EMAILTYPE_BUSINESSEMAIL = "BUSINESSEMAIL";
        public const int THREE_INT = 3;
        public const string DisableMeCSSClass = "disableMe";

        public const string EarlyColor = "ORANGE";
        public const string FlexColor = "BLUE";
        public const string NONE = "none";
        public const string RECENT_HOTEL_SEARCH_COOKIE_NAME = "recent_Hotel";
        public const string RECENT_SEARCH_HOTEL_COOKIE_LENGTH = "RecentSearchHotelCookieLength";
        public const string MOBILE_CAROUSEL_IMAGE_WIDTH = "MobileCarouselImageWidth";
        public const string MOBILE_OFFER_IMAGE_WIDTH = "MobileOfferImageWidth";
        public const string MOBILE_SELECT_RATE_CAROUSEL_IMAGE_COUNT_PER_CATEGORY = "MobileSelectRateImageCountPerCategory";
        /// <summary>
        /// Constants for OWS monitor
        /// </summary>
        public const int ZERO_INT = 0;

        public static string MEMBERSHIP_NO
        {
            get { return ConfigurationManager.AppSettings["Scandic.OWSMonitor.MembershipNo"]; }
        }

        public static string PASSWORD
        {
            get { return ConfigurationManager.AppSettings["Scandic.OWSMonitor.Password"]; }
        }

        public static string NAMEID
        {
            get { return ConfigurationManager.AppSettings["Scandic.OWSMonitor.NameId"]; }
        }

        public static string RESERVATION_NO
        {
            get { return ConfigurationManager.AppSettings["Scandic.OWSMonitor.ReservationNo"]; }
        }

        public static string UNIQUE_ID_VALUE
        {
            get { return ConfigurationManager.AppSettings["Scandic.OWSMonitor.uniqueId"]; }
        }

        public static string SEARCH_STRING
        {
            get { return ConfigurationManager.AppSettings["Scandic.OWSMonitor.SearchString"]; }
        }

        public static string SEARCH_CODE
        {
            get { return ConfigurationManager.AppSettings["Scandic.OWSMonitor.SearchCode"]; }
        }

        public static string HOTEL_CODE
        {
            get { return ConfigurationManager.AppSettings["Scandic.OWSMonitor.HotelCode"]; }
        }

        public const string VOUCHER_BOOKING_CODE = "VO";

        public const string PHONETYPE_HOME = "HOME";
        public const string PHONETYPE_MOBILE = "MOBILE";
        public const string PHONEROLE_PHONE = "PHONE";
        public const string ADDRESS_TYPE_HOME = "HOME";
        public const string SYSTEM_ERROR = "SYSTEM ERROR";
        public const string RESERVATION_COMMENT_TITLE = "Reservation Comment Title";
        public const string NON_CANCELLABLE_RESERVATION = "NON CANCELLABLE RESERVATION";
        public const string CANCEL_BY = "CANCEL BY ";
        public const string NOCREDITCARDRECORDMSG = "No credit card records found.";
        public const string OPERANOTCONNECTED = "Unable to connect to the remote server";
        public const string BONUS_CHEQUE_NOT_FOUND = "BCNF";
        public const string CONFIRMATION = "Confirmation";

        public static string SCANDIC_LOYALTY_MEMBERSHIPTYPE
        {
            get { return ConfigurationManager.AppSettings["Scandic.Loyalty.Membershiptype"]; }
        }

        public static string DEFAULT_MEMBERSHIPLEVEL
        {
            get { return ConfigurationManager.AppSettings["Scandic.Loyalty.Defaultlevel"]; }
        }
        private static string NameCharacterMapping = "NameCharacterMapping";
        public static string NAME_CHARACTER_MAPPING
        {
            get { return ConfigurationManager.AppSettings[NameCharacterMapping]; }
        }


        public const string MEMBERSHIPLEVEL_1STFLOOR = "1STFLOOR";
        public const string MEMBERSHIPLEVEL_2NDFLOOR = "2NDFLOOR";
        public const string MEMBERSHIPLEVEL_3RDFLOOR = "3RDFLOOR";
        public const string MEMBERSHIPLEVEL_TOPFLOOR = "TOPFLOOR";
        public const string MEMBERSHIPLEVEL_BYINVIT = "BYINVIT";

        public const string RESORT_CODE = "ORS";
        public const string CREDIT_CARD_TYPE = "CREDIT";

        public static string CHAIN_CODE
        {
            get { return ConfigurationManager.AppSettings["OWS.Chain.Code"]; }
        }

        public static string LOGOPATH
        {
            get { return ConfigurationManager.AppSettings["LogoPath"]; }
        }

        public const string GUARANTEETYPE_FOR_SESSION_BOOKING = "GDS-SESSION";
        public const string SCANDIC_EMAIL = "Promotions";
        public const string SCANDIC_PARTNER_EMAIL = "ThirdParties";
        public const string MALE = "M";
        public const string FEMALE = "F";
        public const string MALE_FROMOPERA = "MALE";
        public const string FEMALE_FROMOPERA = "FEMALE";
        public const string HTML_SPACE = "&nbsp;";
        public static string QUALIFYING_TYPE_CORPORATE = "CORPORATE";
        public static string QUALIFYING_TYPE_COMPANY = "COMPANY";
        public static string QUALIFYING_TYPE_TRAVEL_AGENT = "TRAVEL_AGENT";
        public static string ID_SOURCE_RESERVATION = "RESVID";
        public static string ID_LEGNUMBER = "LEGNUMBER";
        public static string GURANTEE_TYPE_CREDIT_CARD
        {
            get { return ConfigurationManager.AppSettings["Credit.Card.Gurantee"]; }
        }

        public static string GURANTEE_TYPE_HOLD
        {
            get { return ConfigurationManager.AppSettings["6PM.Gurantee"]; }
        }

        public static string[] NON_MODIFIABLE_GURANTEE_TYPES
        {
            get { return ConfigurationManager.AppSettings["NonModifiableGuranteeTypes"].Split(','); }
        }

        public static string GURANTEE_TYPE_EARLY_PAY
        {
            get { return ConfigurationManager.AppSettings["Earlypay.Gurantee"]; }
        }

        public static bool RECEIPT_IN_ENGLISH_ONLY
        {
            get { return ConfigurationManager.AppSettings["ReceiptInEnglishOnly"] != null && ConfigurationManager.AppSettings["ReceiptInEnglishOnly"].ToLower() == "true"; }
        }

        public static string PREPAID_CCTYPE_PREFIX
        {
            get { return ConfigurationManager.AppSettings["PrepaidCCTypePrefix"]; }
        }

        /// <summary>
        /// Gets Prepaid in progress guarantee type. 
        /// </summary>
        public static string GUARANTEETYPE_PREAPIDINPROGRESS
        {
            get { return ConfigurationManager.AppSettings["PrepaidInProgress.Guarantee"]; }
        }

        /// <summary>
        /// Gets Prepaid success guarantee type. 
        /// </summary>
        public static string GUARANTEETYPE_PREAPIDSUCCESS
        {
            get { return ConfigurationManager.AppSettings["PrepaidSuccess.Guarantee"]; }
        }

        /// <summary>
        /// Gets Prepaid failure guarantee type. 
        /// </summary>
        public static string GUARANTEETYPE_PREAPIDFAILURE
        {
            get { return ConfigurationManager.AppSettings["PrepaidFailure.Guarantee"]; }
        }

        /// <summary>
        /// Gets Makepayment failure guarantee type. 
        /// </summary>
        public static string GUARANTEETYPE_MAKEPAYMENTFAILURE
        {
            get { return ConfigurationManager.AppSettings["MakePaymentFailure.Guarantee"]; }
        }

        /// <summary>
        /// Gets Checkout URL.
        /// </summary>
        public static string CHECKOUTURL
        {
            get { return ConfigurationManager.AppSettings["CheckoutURL"]; }
        }

        /// <summary>
        /// Gets Checkout URL.
        /// </summary>
        public static string MODIFYBOOKINGDETAILSURL
        {
            get { return ConfigurationManager.AppSettings["ModifyBookingDetailsURL"]; }
        }

        public static string HotelFloorPlanWidth
        {
            get { return ConfigurationManager.AppSettings["HotelFloorPlanWidth"]; }
        }

        public static string BenfitImageWidth
        {
            get { return ConfigurationManager.AppSettings["BenfitImageWidth"]; }
        }

        public static string ScandicFriendsLogoWidth
        {
            get { return ConfigurationManager.AppSettings["ScandicFriendsLogoWidth"]; }
        }

        public static string LeftColumnImageWidth
        {
            get { return ConfigurationManager.AppSettings["LeftColumnImageWidth"]; }
        }

        public static string LandingPageTopImageWidth
        {
            get { return ConfigurationManager.AppSettings["LandingPageTopImageWidth"]; }
        }

        public static string GenricImageWidth
        {
            get { return ConfigurationManager.AppSettings["GenricImageWidth"]; }
        }

        public static string PageStartTimeCookieId
        {
            get { return "pageStartTime"; }
        }

        public static int PageRestoreTimeWindow
        {
            get {
                    int pageRestoreTimeWindow;
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["pageRestoreTimeWindow"]))
                    {
                        if (!int.TryParse(ConfigurationManager.AppSettings["pageRestoreTimeWindow"], out pageRestoreTimeWindow))
                        {
                            pageRestoreTimeWindow = int.MaxValue;
                        }
                    }
                    else
                    {
                        pageRestoreTimeWindow = int.MaxValue;
                    }
                    return pageRestoreTimeWindow;
                }
        }

        public static string HotelTopImageWidth
        {
            get { return ConfigurationManager.AppSettings["HotelTopImageWidth"]; }
        }

        /// <summary>
        /// Gets ModifyCreditCardNumberForMakePayment flag.
        /// </summary>
        public static bool ModifyCreditCardNumberForMakePayment
        {
            get
            {
                bool modifyCreditCardNumberForMakePayment = false;
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ModifyCreditCardNumberForMakePayment"]))
                {
                    bool.TryParse(ConfigurationManager.AppSettings["ModifyCreditCardNumberForMakePayment"],
                                             out modifyCreditCardNumberForMakePayment);
                }
                return modifyCreditCardNumberForMakePayment;
            }
        }

        /// <summary>
        /// Gets email recipients for online payment failures. 
        /// </summary>
        public static string[] EmailRecipientsForOnlinePaymentFailures
        {
            get { return ConfigurationManager.AppSettings["EmailRecipientsForOnlinePaymentFailures"].Split(','); }
        }

        /// <summary>
        /// Gets email sender for online payment failures. 
        /// </summary>
        public static string EmailSenderForOnlinePaymentFailures
        {
            get { return ConfigurationManager.AppSettings["EmailSenderForOnlinePaymentFailures"]; }
        }

        /// <summary>
        /// Gets FamilyAndFriends DNumbers as a string separated by semicolon(;) 
        /// </summary>
        public static string FAMILTY_AND_FRIENDS_DNUMBER
        {
            get { return ConfigurationManager.AppSettings["FamilyAndFriendsDNumbers"]; }
        }

        /// <summary>
        /// Gets Valid Intranet Ip Addresses for FamilyAndFriends DNumber booking separated by semicolon(;)
        /// </summary>
        public static string VALID_INTRANET_IP_ADDRESSES_FOR_FAMILY_AND_FRIENDS_BOOKING
        {
            get { return ConfigurationManager.AppSettings["ValidIntranetIpAddressesForFamilyAndFriendsBooking"]; }
        }

        /// <summary>
        /// Gets Prepaid success guarantee type. 
        /// </summary>
        public static Dictionary<string, string> NETS_OPERA_CCTYPE_MAPPING
        {
            get
            {
                string[] ccTypeMappings = ConfigurationManager.AppSettings["Nets-Opera-CCTypeMapping"].Split(';');
                Dictionary<string, string> netsOperaCCTypes = null;

                if ((ccTypeMappings != null) && (ccTypeMappings.Length > 0))
                {
                    netsOperaCCTypes = new Dictionary<string, string>();

                    foreach (string ccTypeMapping in ccTypeMappings)
                    {
                        string[] ccType = ccTypeMapping.Split(',');
                        if (!netsOperaCCTypes.ContainsKey(ccType[0]))
                            netsOperaCCTypes.Add(ccType[0], ccType[1]);
                    }
                }
                return netsOperaCCTypes;
            }
        }

        /// <summary>
        /// Get mapping values between opera cc types and scanweb cc types. 
        /// </summary>
        public static Dictionary<string, string> OPERA_SCANWEB_CCTYPE_MAPPING
        {
            get
            {
                string[] ccTypeMappings = ConfigurationManager.AppSettings["Nets-Opera-CCTypeMapping"].Split(';');
                Dictionary<string, string> operaScanwebCCTypes = null;

                if ((ccTypeMappings != null) && (ccTypeMappings.Length > 0))
                {
                    operaScanwebCCTypes = new Dictionary<string, string>();

                    foreach (string ccTypeMapping in ccTypeMappings)
                    {
                        string[] ccType = ccTypeMapping.Split(',');
                        if (!operaScanwebCCTypes.ContainsKey(ccType[1]))
                            operaScanwebCCTypes.Add(ccType[1], ccType[2]);
                    }
                }
                return operaScanwebCCTypes;
            }
        }


        public static string[] GUARANTEE_TYPE_PRE_PAID
        {
            get
            {
                string[] guranteeTypes = null;
                string commaDelimitedGuranteeTypes = ConfigurationManager.AppSettings["PrePaid.Gurantee"];
                if (!string.IsNullOrEmpty(commaDelimitedGuranteeTypes))
                {
                    guranteeTypes = commaDelimitedGuranteeTypes.Split(',');
                }
                return guranteeTypes;
            }
        }



        public static string NEGOTITATED_BOOKING_QUALIFYING_TYPE = "CORPORATE";

        public static string NEG_BOOKING_QUALIFYING_TYPE_TRAVEL_AGENT = "TRAVEL_AGENT";

        public static string BLOCK_CODE_QUALIFYING_TYPE = "BLOCKCODE_BOOKING";
        public static string MISC = "Miscellaneous";

        public static int ROOM_PER_NIGHT = 1;

        public enum AvailabilityType
        {
            REGULAR_AVAILABLITY,
            REDEMP_AVAILABILITY,
            PROMO_AVAILABILITY,
            CORP_AVAILABILTY,
            BONUS_CHEQUE_AVAILABILTY
        }

        public const double BASE_CURRENCY_CONVERSION = 1;

        public static string BASE_CURRENCY_CODE
        {
            get { return ConfigurationManager.AppSettings["BaseCurrencyCode"]; }
        }

        public static bool DISPLAY_CURRENCYCONVERTER_LINK
        {
            get
            {
                bool displayCurConverter = false;
                string strDisplayCurConverter = ConfigurationManager.AppSettings["DisplayCurrencyConverterLink"];
                if ((strDisplayCurConverter != null) && (strDisplayCurConverter != string.Empty))
                {
                    if (strDisplayCurConverter.Equals("true"))
                    {
                        displayCurConverter = true;
                    }
                }
                return displayCurConverter;
            }
        }

        public const string OWS_EXCEPTION = "OWS EXCEPTION";
        public const string BUSINESS_EXCEPTION = "BUSINESS EXCEPTION";
        public const string CDAL_EXCEPTION = "CDAL EXCEPTION";
        public const string GENERAL_EXCEPTION = "GENERAL EXCEPTION";
        public const string APPLICATION_EXCEPTION = "APPLICATION EXCEPTION";
        public static string RATE_CATEGORY_EARLY = ConfigurationManager.AppSettings["Booking.SelectRate.EarlyRateCategory"].ToString();
        public static string RATE_CATEGORY_FLEX = ConfigurationManager.AppSettings["Booking.SelectRate.FlexRateCategory"].ToString();
        public static string SECURE_PROTOCOL = ConfigurationManager.AppSettings["SecureProtocol"].ToString();
        public static string HTTP_PROTOCOL = ConfigurationManager.AppSettings["HTTPProtocol"].ToString();
        public static string LOGIN_VALIDATOR_PATH = ConfigurationManager.AppSettings["LoginValidatorPath"].ToString();
        public static string MAINMENU_SELECTED_PAGE_REFERENCE = "MMPREF";
        public static string SUBMENU_SELECTED_PAGE_NAME = "SMPREF";
        public static string CURRENT_BOOKINGS_COUNT = ConfigurationManager.AppSettings["CurrentBookingsDisplayCount"].ToString();
        public static string SUBMENU_SELECTED_PAGE_DATA = "SMPDATA";
        public static string SUBMENU_SELECTED_PAGE_LINK = "SMPLINK";

        //Flag to check if Public Rates search for promotions is On or OFF
        public static string ENABLE_PUBLIC_RATE_SEARCH_FOR_PROMOTIONS = Convert.ToString(ConfigurationManager.AppSettings["EnablePublicRateSearchForPromotions"]);

        //Maximum Arrival date is 396 days from current date
        public static int ALLOWED_DAYS_FOR_DATE_OF_ARRIVAL
        {
            get { 
                    int retVal = 396;
                    if(!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AllowedDaysForDateOfArrival"]))
                        retVal = Convert.ToInt32(ConfigurationManager.AppSettings["AllowedDaysForDateOfArrival"]);
                    return retVal;
                }
        }


        #region CONSTANTS USED FOR THREADS

        public static int MIN_WORKER_THREADS =
            Convert.ToInt32(ConfigurationManager.AppSettings["MinWorkerThreads"].ToString());

        public static int MIN_IO_THREADS = Convert.ToInt32(ConfigurationManager.AppSettings["MinIOThreads"].ToString());

        public static int MAX_WORKER_THREADS =
            Convert.ToInt32(ConfigurationManager.AppSettings["MaxWorkerThreads"].ToString());

        public static int MAX_IO_THREADS = Convert.ToInt32(ConfigurationManager.AppSettings["MaxIOThreads"].ToString());

        public const string THREAD_HOTEL_SEARCH_ENTITY = "HOTEL_SEARCH_ENTITY";
        public const string THREAD_HOTEL_DESTINATION = "HOTEL_DESTINATION";
        public const string THREAD_HOTEL_CHAIN_CODE = "HOTEL_CHAIN_CODE";
        public const string THREAD_SESSION = "SESSION";
        public const string THREAD_RATE_AWARD = "RATE_AWARD";
        public const string THREAD_COUNTRY_CODE = "COUNTRY_CODE";
        public const string THREAD_ROOM_NO = "ROOM_NO";
        public const string THREAD_HOTEL_NO = "HOTEL_NO";
        public const string THREAD_ROOM_COUNT = "ROOM_COUNT";
        public const string THREAD_HOTEL_COUNT = "HOTEL_COUNT";
        public const string THREAD_AVAIL_CALENDAR = "AVAIL_CALENDAR";
        public const string THREAD_AVAIL_CALENDAR_ITEM_ENTITY = "AVAIL_CALENDAR_ITEM_ENTITY";
        public const string SERVICE_REQUEST = "SERVICE_REQUEST";
        public const string IsMultiDateSearch = "IsMultiDateSearch";
        public const string SEARCHSERVICE_CALLBACK = "SEARCHSERVICE_CALLBACK";

        public const string REQUEST_SOURCE = "REQUEST_SOURCE";
        public const string REQUEST_START_TIME = "REQUEST_START_TIME";
        public const string TIMER = "TIMER";
        public const string REQUEST_DATETIME_FORMAT = "MM/dd/yyyy hh:mm:ss fff";
        #endregion

        #region CONSTANTS USED FOR GOOGLEMAP

        public const string GM_HOTEL_NAME = "HotelName";
        public const string GM_HOTEL_ADDRESS = "HotelAddress";
        public const string GM_LANDING_PAGE_URL = "LandingPageURL";
        public const string GM_BOOKING_PAGE_URL = "BookingPageURL";
        public const string GM_LONGITUDE_X = "X";
        public const string GM_LATITUDE_Y = "Y";
        public const string GM_HOTEL_ID = "HotelID";
        //Start : Google map to display price per stay and price per night information | Reservation 2.0
        public const string GM_PRICE_PER_STAY = "PerStay";
        public const string GM_PRICE_PER_NIGHT = "PerNight";
        public const string GM_CURRENCY_CODE = "CurrencyCode";
        public const string GM_IHOTELDETAILS = "HotelDetails";
        //End : Google map to display price per stay and price per night information | Reservation 2.0

        #endregion

        #region CONSTANTS USED FOR OWS SERVICES

        public static string OWS_NAME_SERVICE
        {
            get { return ConfigurationManager.AppSettings["OWS.Name.Service"]; }
        }

        public static string OWS_MEMBERSHIP_SERVICE
        {
            get { return ConfigurationManager.AppSettings["OWS.Membership.Service"]; }
        }

        public static string OWS_SECURITY_SERVICE
        {
            get { return ConfigurationManager.AppSettings["OWS.Security.Service"]; }
        }

        public static string OWS_AVAILABILITY_SERVICE
        {
            get { return ConfigurationManager.AppSettings["OWS.Availability.Service"]; }
        }

        public static string OWS_RESERVATION_SERVICE
        {
            get { return ConfigurationManager.AppSettings["OWS.Reservation.Service"]; }
        }
        public static string OWS_PREPAID_RESERVATION_SERVICE
        {
            get { return ConfigurationManager.AppSettings["OWS.ReservationPrepaid.Service"]; }
        }

        public static string NETS_PAYMENT_SERVICE
        {
            get { return ConfigurationManager.AppSettings["Nets.Payment.Service"]; }
        }

        public static string OWS_INFORMATION_SERVICE
        {
            get { return ConfigurationManager.AppSettings["OWS.Information.Service"]; }
        }

        public static string OWS_ADVANCEDRESERVATION_SERVICE
        {
            get { return ConfigurationManager.AppSettings["OWS.AdvancedReservation.Service"]; }
        }

        public static string OWS_ORIGIN_ENTITY_ID
        {
            get { return ConfigurationManager.AppSettings["OWS.Origin.EntityId"]; }
        }

        //public static string OWS_ORIGIN_SCANCOM_ENTITY_ID
        //{
        //    get
        //    {
        //        return ConfigurationManager.AppSettings["OWS.Origin.Scancom.EntityId"];
        //    }
        //}

        public static string OWS_ORIGIN_SYSTEM_TYPE
        {
            get { return ConfigurationManager.AppSettings["OWS.Origin.SystemType"]; }
        }

        public static string OWS_SESSION_ORIGIN_SYSTEM_TYPE
        {
            get { return ConfigurationManager.AppSettings["OWS.Session.Origin.SystemType"]; }
        }

        public static string OWS_DESTINATION_ENTITY_ID
        {
            get { return ConfigurationManager.AppSettings["OWS.Desitination.EntityId"]; }
        }

        public static string OWS_DESTINATION_SYSTEM_TYPE
        {
            get { return ConfigurationManager.AppSettings["OWS.Desitination.SystemType"]; }
        }

        public static string OWS_SESSION_DESTINATION_SYSTEM_TYPE
        {
            get { return ConfigurationManager.AppSettings["OWS.Session.Desitination.SystemType"]; }
        }

        public static string OWS_SESSION_INTERMEDIARIES_SYSTEM_TYPE1
        {
            get { return ConfigurationManager.AppSettings["OWS.Session.Intermediaries.SystemType1"]; }
        }

        public static string OWS_SESSION_INTERMEDIARIES_SYSTEM_TYPE2
        {
            get { return ConfigurationManager.AppSettings["OWS.Session.Intermediaries.SystemType2"]; }
        }
        public static string OWS_DEFAULT_SOURCE_CODE
        {
            get { return ConfigurationManager.AppSettings["OWS.Defualt.SourceCode"]; }
        }

        public static string OWS_ResvAdvancedService_USER_NAME
        {
            get { return ConfigurationManager.AppSettings["OWS.ResvAdvancedService.UserName"]; }
        }

        public static string OWS_ResvAdvancedService_USER_PASSWORD
        {
            get { return ConfigurationManager.AppSettings["OWS.ResvAdvancedService.UserPassword"]; }
        }
        #endregion

        #region CONSTANTS USED FOR SMS SERVICES

        private static int smsMaxLength = 160;

        /// <summary>
        /// Get the maximum length of SMS supported by the provider.
        /// </summary>
        public static int SMS_MAX_LENGTH
        {
            get
            {
                try
                {
                    string maxlen = ConfigurationManager.AppSettings["SMSLength"].ToString().Trim();
                    smsMaxLength = Int32.Parse(maxlen);
                }
                catch (Exception ex)
                {
                    AppLogger.LogFatalException(ex,
                                                     "The SMSLength specified in AppSettings is not in the correct format.");
                }
                return smsMaxLength;
            }
        }

        #endregion

        #region UrlRewrite
        public const int HotelSubStringOccurences = 2;
        public const int CountryContainerPageID = 123;
        public const int HotelContainerPageID = 697;
        public const int HotelOverviewPage = 3580;
        public const int HotelOverviewPageTypeId = 87;
        public const int HotelLandingPageTypeId = 33;
        public const int CountryLandingPageTypeId = 29;
        public const int CityLandingPagetypeId = 31;
        public static string HotelRegex = ConfigurationManager.AppSettings["HotelRegex"].ToString();
        public static string CountryRegex = ConfigurationManager.AppSettings["CountryRegex"].ToString();
        public static string HotelPageTypeName = "[Scanweb] Hotel";
        public enum UrlFragmentLength
        {
            HotelUrlFragmentLength = 4,
            CityUrlFragmentLength = 3,
            CountryUrlFragmentLength = 2
        }

        public enum HotelCountrySubstringPos
        {
            HotelSubstring = 1,
            Country = 3
        }

        #endregion

        /// <summary>
        /// Used in Digitial Platform Release
        /// Added for Dynamic display of error messages
        /// </summary>

        #region Hotel Page Property
        public const string COMINGSOON = "Coming Soon";

        public const string RECENTLYOPENED = "Recently Opened";
        public const string DEFAULT = "Default";
        public const string COMINGSOON_VALUE = "ComingSoon";
        public const string RECENTLYOPENED_VALUE = "RecentlyOpened";

        #endregion

        private static string[] bonusChequeRates;

        public static string[] BONUS_CHEQUE_RATES
        {
            get
            {
                if (bonusChequeRates == null)
                {
                    string bonusChequeConfig = ConfigurationManager.AppSettings["Booking.BonusCheque.Rates"];
                    bonusChequeRates = GetCSVList(bonusChequeConfig);
                }
                return bonusChequeRates;
            }
        }


        private static string[] ajaxDestinationTextExclusions;

        public static string[] AJAX_DESTINATION_TEXT_EXCLUSIONS
        {
            get
            {
                if (ajaxDestinationTextExclusions == null)
                {
                    string ajaxDestinationTextExclusionConfig =
                        ConfigurationManager.AppSettings["Booking.AjaxDestination.Exclusion"];
                    ajaxDestinationTextExclusions = GetCSVList(ajaxDestinationTextExclusionConfig);
                }
                return ajaxDestinationTextExclusions;
            }
        }

        private static string[] destinationAlphabetMap;

        public static string[] DESTINATION_ALPHABET_MAP
        {
            get
            {
                if (destinationAlphabetMap == null)
                {
                    string alphabetMapInConfig = ConfigurationManager.AppSettings["Booking.AjaxDestination.AlphabetMap"];
                    char[] punctuationLetter = { ';' };
                    destinationAlphabetMap = GetPunctuationSeparatedList(alphabetMapInConfig, punctuationLetter);
                }
                return destinationAlphabetMap;
            }
        }

        private static char[] csv = { ',' };

        public static string[] GetCSVList(string csvString)
        {
            if (csvString == null)
            {
                return null;
            }
            else
            {
                string[] values = csvString.Split(csv);
                return values;
            }
        }


        private static char[] punctuation;

        public static string[] GetPunctuationSeparatedList(string csvString, char[] punctuation)
        {
            if (csvString == null)
            {
                return null;
            }
            else
            {
                string[] values = csvString.Split(punctuation);
                return values;
            }
        }

        private static int hotelsPerPage = int.MinValue;

        public static int HOTELS_PER_PAGE
        {
            get
            {
                if (hotelsPerPage == int.MinValue)
                {
                    hotelsPerPage = int.Parse(ConfigurationManager.AppSettings["Booking.SelectHotel.HotelPerPage"]);
                }
                return hotelsPerPage;
            }
        }

        private static string[] selectRateDefaultRoomTypeIds;

        public static string[] SELECT_RATE_DEFAULT_ROOM_TYPE_IDS
        {
            get
            {
                if (selectRateDefaultRoomTypeIds == null)
                {
                    string selectRateDefaultRoomTypeIdsConfig =
                        ConfigurationManager.AppSettings["Booking.SelectRate.DefaultRoomTypeIds"];
                    selectRateDefaultRoomTypeIds = GetCSVList(selectRateDefaultRoomTypeIdsConfig);

                    for (int i = 0; i < selectRateDefaultRoomTypeIds.Length; i++)
                    {
                        selectRateDefaultRoomTypeIds[i] = selectRateDefaultRoomTypeIds[i].Trim();
                    }
                }
                return selectRateDefaultRoomTypeIds;
            }
        }

        public static string ROOM_CATEGORY_ROOM
        {
            get { return ConfigurationManager.AppSettings["Booking.SelectRate.Room"] as string; }
        }

        public static string ROOM_CATEGORY_SUPERIOR
        {
            get { return ConfigurationManager.AppSettings["Booking.SelectRate.Superior"] as string; }
        }

        public const string SITE_WIDE_ERROR_HEADER = "/bookingengine/booking/errorpage/apperrhead";
        public const string SITE_WIDE_ERROR = "/scanweb/bookingengine/errormessages/sitewideerror";
        public const string UPDATE_PROFILE_ERROR = "/scanweb/bookingengine/errormessages/updateprofileerror";
        public const string NO_HOTELS_FOUND = "/scanweb/bookingengine/errormessages/nohotelsfound";
        public const string ALT_CITY_HOTELS_MSG = "/scanweb/bookingengine/errormessages/altcityhotelsmsg";
        public const string NO_HOTELS_WITH_DNUMBER_MSG = "/scanweb/bookingengine/errormessages/businesserror/nohotelswithdnumber";
        public const string NO_HOTELS_WITH_PROMOCODE_MSG = "/scanweb/bookingengine/errormessages/businesserror/nohotelswithpromocode";
        public const string NO_HOTELS_WITH_VOUCHERCODE_MSG = "/scanweb/bookingengine/errormessages/businesserror/nohotelswithvouchercode";
        public const string HOTEL_CLOSED = "/bookingengine/booking/currentbooking/hotelclosed";

        public const string DATE_OF_BIRTH_NOT_AVAILABLE = "/scanweb/bookingengine/booking/updateprofile/dateofbirthnotavailable";

        public const int TOTALYEARS = 15;
        public const int TOTALMONTHS = 12;
        public const int TOTALDAYS = 31;
        public const int MINAGE = 18;
        public const int TOTAL_DOB_YEAR_START = 1900;
        public const int MAX_LENGTH_PHONECODE = 4;
        public const int MAX_BOOKING_ROOMS_ALLOWED = 4;

        public const int MAX_ADD_A_STAY_ALLOWED = 5;

        public const string COUNTRYCODE_FILENAME = "CountryCode_";
        public const string PHONECODE_FILENAME = "PhoneCode_";
        public const string QUESTION_FILENAME = "Question_";
        public const string PARTNERPROGRAM_FILENAME = "PartnerProgram_";
        public const string PREFERREDLANGUAGE_FILENAME = "PreferredLanguage_";
        public const string CREDITCARDSTYPES_FILENAME = "CreditCardTypes_";
        public const string CURRENCYCODES_FILENAME = "CurrencyCodes_";
        public const string CURRENCYVALUES_FILENAME = "CurrencyValues";
        public const string COUNTRYPHONECODESMAP_FILENAME = "CountryPhoneCodesMap";
        public const string CODE_FILE_EXTN = ".xml";
        public const string DEFAULT_VALUE_CONST = "DFT";
        public const string DEFAULT_SEPARATOR_CONST = "SEPR";
        public const string CARD_MASK = "************";
        public const string CARD_MASK_CHAR = "*";
        public const string CARD_MASK_NEW = "XXXX-XXXX-XXXX";
        public const int CREDIT_CARD_DISPLAY_CHARS = 4;
        public const string EMAILSUBJECT = "ContactUsSubject_";
        public const int PER_NIGHT = 1;
        public const int PER_ROOM = 1;
        public const string BOOOKING_CODE_COOKIE = "BookingCodeCookie";

        public static string EXCLUDEDPARTNERPROGRAM = Convert.ToString(ConfigurationManager.AppSettings["ExcludedPartnerProgram"]);

        public static bool SCANDIC_BOOKING_CODE_COOKIES
        {
            get
            {
                bool saveBookingCodeCookies = false;
                string strsaveBookingCodeCookies = ConfigurationManager.AppSettings["Scandic.Booking.Code.Cookies"];
                if ((strsaveBookingCodeCookies != null) && (strsaveBookingCodeCookies != string.Empty))
                {
                    if (strsaveBookingCodeCookies.Equals("true"))
                    {
                        saveBookingCodeCookies = true;
                    }
                }
                return saveBookingCodeCookies;
            }
        }

        public static int SCANDIC_BOOKING_CODE_EXPIRYYEAR
        {
            get
            {
                int bookingCodeExpYr = 0;
                string strbookingCodeExpYr = ConfigurationManager.AppSettings["Scandic.Booking.Code.ExpiryYear"];
                if ((strbookingCodeExpYr != null) && (strbookingCodeExpYr != string.Empty))
                {
                    bookingCodeExpYr = int.Parse(strbookingCodeExpYr);
                }
                return bookingCodeExpYr;
            }
        }

        public const string USERID_COOKIE = "UesrIDCookie";
        public const string PASSWORD_COOKIE = "PasswordCookie";
        public const string NEW_PASSWORD_COOKIE = "NewPasswordCookie";
        public const string USER_LOCALE_COOKIE = "UserLocaleCookie";
        public const string DONTREMEMBERME_COOKIE = "DontRememberMeCookie";
        public const string SMS_COOKIE = "SMSCookie";
        //bug: 462312 Res 2.0: reduced the calender item count to 15
        public const int CAROUSEL_COUNT = 5;
        public const int VISIBLE_CAROUSEL_COUNT = 5;
        public const double CALENDARITEM_GAP_DAYS = 1.0;
        public const int NO_OF_YEARS_IN_AVAILCALENDAR = 2;

        public const int DEFAULT_LEFT_VISIBLECAROUSAL_INDEX = 5;

        public enum EMAILRECIPENTS
        {
            MAIN_RECIPENT,
            FIRST_RECIPENT,
            SECOND_RECIPENT,
            THIRD_RECIPENT
        }

        public const string Room = "room";
        public const string Apartment = "Apartment";
        public const string Superior = "superior";
        public const string Cabin = "cabin";
        public const string Economy = "economy";
        public const string Suites = "suite";
        public const string Studio = "studio";
        public const string Default = "room";

        public const string SystemGenerated = "SystemGenerated";
        public const string Select = "Select";
        public const string ManuallyGenerated = "ManuallyGenerated";
        public const string TrackingCodeInitial = "pa_api_";
        public const string APIKEY_DEFAULT_TEXT = "Field is System Generated!!";

        //Merchandising:R3:Display ordinary rates for unavailable promo rates 
        public static string ALTERNATE_HOTELS_PROMO_MESSAGE1 = "/bookingengine/booking/selecthotel/alternatePromoHotelMessage1";
        public static string ALTERNATE_HOTELS_PROMO_MESSAGE2 = "/bookingengine/booking/selecthotel/alternatePromoHotelMessage2";
        public static string ALTERNATE_HOTELS_PROMO_MESSAGE3 = "/bookingengine/booking/selecthotel/alternatePromoHotelMessage3";
        public static string ALTERNATE_HOTELS_PROMO_MESSAGE4 = "/bookingengine/booking/selecthotel/alternatePromoHotelMessage4";
        public static string PROMO_RATE_MESSAGE = "/bookingengine/booking/selecthotel/PromoRateMessage";
        public static string NO_DISCOUNT_MESSAGE = "/bookingengine/booking/selecthotel/NoDiscountMessage";
        public static string REGULAR_PRICE_FROM = "/bookingengine/booking/selecthotel/RegularPriceFrom";
        public static string ALTERNATE_DESTINATION_PROMO_MESSAGE1 = "/bookingengine/booking/selecthotel/alternatePromoDestinationMessage1";
        public static string ALTERNATE_DESTINATION_PROMO_MESSAGE2 = "/bookingengine/booking/selecthotel/alternatePromoDestinationMessage2";
        public static string ALTERNATE_DESTINATION_PROMO_MESSAGE3 = "/bookingengine/booking/selecthotel/alternatePromoDestinationMessage3";
        public static string ALTERNATE_DESTINATION_PROMO_MESSAGE4 = "/bookingengine/booking/selecthotel/alternatePromoDestinationMessage4";

        //public const string PROMO_SEARCHSERVICE_CALLBACK = "PROMO_SEARCHSERVICE_CALLBACK";
        public const string IS_PROMO_CITY_FAIL = "IS_PROMO_CITY_FAIL";

        #region PageTypeIds

        public static string CountryPageTypeID = "CountryPageTypeID";
        public static string CityPageTypeID = "CityPageTypeID";
        public static string HotelPageTypeID = "HotelPageTypeID";
        public static string HotelRedemptionPointsPageTypeID = "HotelRedemptionPointsPageTypeID";
        public static string StandardPageTypeID = "StandardPageTypeID";

        #endregion

        public static string HotelRedemptionsCacheKeyFormat = "HotelRedemptionPoints - {0}";

        public const string RESERVATION_MODIFY_BOOKING_DETAILS = "Reservation/Modify-Booking-Details/";
        public const string RESERVATION_BOOKING_DETAILS = "Reservation/Booking-Details/";
        public const string RESERVATION_MODIFY_SELECT_RATE = "Reservation/Modify-Select-Rate/";
        public static string CacheExpiryTimeoutForCurrency = "CurrencyCacheTimeOut";
        public static string ECBCurrencyConverterFeedUrl = "ECBCurrencyConverterFeedUrl";
        public static string ExchangeRatesProvider = "ExchangeRatesProvider";
        public static string ECB = "ECB";
        public static string LocaleCurrencyMapping = "LocaleCurrencyMapping";
        public static string ExchageRateCacheKey = "ExchageRateCacheKey";
        public static string ECBExchageRateBaseCurrency = "EUR";
        public static string AdditinalExchangeAmount = "AdditinalExchangeAmount";
        public static string EnableAlternateCurrencyDisplay = "EnableAlternateCurrencyDisplay";
        public static string CurrentGeoLocationCordinate = "CurrentGeoLocationCordinate";
        public static string SearchRadiusOption1 = "SearchRadiusOption1";
        public static string SearchRadiusOption2 = "SearchRadiusOption2";
        public static string ExceptionCodesFilePath = "ExceptionCodesFilePath";
        public static string BOOKING_DETAILS = "booking-detail";
        public static string SELECT_RATE = "select-rate";
        public static string SELECT_HOTEL = "select-hotel";
        public const string CITY_COORDINATES_MISSING = "Co-ordinates are either wrong or missing for the City {0}";
        public const string ENABLE_MOBILE_CUSTOM_SEARCH = "EnableMobileCustomSearch";
        public const string ENABLE_DECIMAL_RATES = "EnableDecimalRates";
        
        #region TripAdvisor Rating
        public static string TripAdvisorRatingFeedsUrl = "TripAdvisorRatingFeedsUrl";
        public static string TripAdvisorRatingPopupBaseUrl = "TripAdvisorRatingPopupBaseUrl";
        public static string TripAdvisorRatingMobilePopupBaseUrl = "TripAdvisorRatingMobilePopupBaseUrl";
        public static string TripAdvisorRatingCacheKey = "TripAdvisorRatingCacheKey";
        public static string CacheExpiryTimeoutForTripAdvisor = "CacheExpiryTimeoutForTripAdvisor";
        public static string TripAdvisorRatingPartnerID = "TripAdvisorRatingPartnerID";

        #endregion

        /// <summary>
        /// Gets StopCacheMonitor value from the config.
        /// </summary>
        public static bool StopCacheMonitor
        {
            get
            {
                bool stopCacheMonitor = false;
                if (ConfigurationManager.AppSettings["StopCacheMonitor"] != null)
                {
                    Boolean.TryParse(ConfigurationManager.AppSettings["StopCacheMonitor"], out stopCacheMonitor);
                }
                return stopCacheMonitor;
            }
        }

        /// <summary>
        /// Gets CacheMonitorTimeOut value from the config.
        /// </summary>
        public static int CacheMonitorTimeOut
        {
            get
            {
                int cacheMonitorTimeout = default(int);
                if (ConfigurationManager.AppSettings["CDALCacheTimeout"] != null)
                {
                    int.TryParse(ConfigurationManager.AppSettings["CDALCacheTimeout"], out cacheMonitorTimeout);
                }
                return cacheMonitorTimeout;
            }
        }

        /// <summary>
        /// Gets CacheDataTimeOut value from the config.
        /// </summary>
        public static int CacheDataTimeOut
        {
            get
            {
                int cacheDataTimeout = default(int);
                if (ConfigurationManager.AppSettings["CacheDataTimeOut"] != null)
                {
                    int.TryParse(ConfigurationManager.AppSettings["CacheDataTimeOut"], out cacheDataTimeout);
                }
                return cacheDataTimeout;
            }
        }

        public static string MonitorText = "Monitor";

        #region PaymentProcessing TimeOut

        public static int PAYMENT_PROCESSING_TIMEOUT
        {
            get
            {
                int paymentProcessingTimeout = default(int);
                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["PaymentProcessingTimeout"]))
                {
                    int.TryParse(ConfigurationManager.AppSettings["PaymentProcessingTimeout"], out paymentProcessingTimeout);
                }
                return paymentProcessingTimeout;
            }
        }


        #endregion

        /// <summary>
        /// Gets UseNetsTestMerchant setting from Config file.
        /// </summary>
        public static bool UseNetsTestMerchant
        {
            get
            {
                bool useNetsTestMerchant = false;
                if (ConfigurationManager.AppSettings["UseNetsTestMerchant"] != null)
                {
                    bool.TryParse(ConfigurationManager.AppSettings["UseNetsTestMerchant"], out useNetsTestMerchant);
                }
                return useNetsTestMerchant;
            }
        }

        public const string ScanWebMembershipNoPrefix = "308123";

        public static bool ShowMembershipDetailsLink
        {
            get
            {
                bool showMembershipDetailsLink = false;
                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ShowMembershipDetailsLink"]))
                {
                    bool.TryParse(ConfigurationManager.AppSettings["ShowMembershipDetailsLink"], out showMembershipDetailsLink);
                }
                return showMembershipDetailsLink;
            }
        }
        public static string VIEW_MEMBERSHIP_DETAILS
        {
            get
            {
                string showMembershipDetailsLink = string.Empty;
                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["MembershipDetailsLinkURL"]))
                {
                   showMembershipDetailsLink = Convert.ToString(ConfigurationManager.AppSettings["MembershipDetailsLinkURL"]);
                }
                return showMembershipDetailsLink;
            }
        }

        public static string BOOKING_RECEIPT_TAX_ENTRIES_TO_HIDE
        {
            get { return ConfigurationManager.AppSettings["BookingReceiptTaxEntriesToHide"]; }
        }
    }
}

/// <summary>
/// Contains the Constant for Language 
/// And Language code configured in opera for Native languages
/// </summary>
public class LanguageConstant
{
    public const string LANGUAGE_GERMAN = "DE";
    public const string LANGUAGE_DANISH = "DA";
    public const string LANGUAGE_ENGLISH = "EN";
    public const string LANGUAGE_FINNISH = "FI";
    public const string LANGUAGE_NORWEGIAN = "NB";
    public const string LANGUAGE_SWEDISH = "SV";
    public const string LANGUAGE_RUSSIA = "RU";
    public const string LANGUAGE_RUSSIA_RU = "RU-RU";

    public const string LANGUAGE_RUSSIAN = "RUSS";

    public const string LANGUAGE_DANISH_CODE = "DAN";
    public const string LANGUAGE_ENGLISH_CODE = "E";
    public const string LANGUAGE_FINNISH_CODE = "FIN";
    public const string LANGUAGE_NORWEGIAN_CODE = "NOR";
    public const string LANGUAGE_SWEDISH_CODE = "SWE";
    public const string LANGUAGE_GERMAN_CODE = "GER";
    public const string LANGUAGE_RUSSIA_CODE = "R";

    public const string LANGUAGE_NORWEGIAN_EXTENSION = "NO";

    public const string LANGUAGE_SV_MAP_COUNTRY_CODE = "SE";
    public const string LANGUAGE_DA_MAP_COUNTRY_CODE = "DK";
}

/// <summary>
/// Contains the Constant for Language 
/// And Language code configured in opera for Native languages
/// </summary>
public class CurrentPageLanguageConstant
{
    public const string LANGUAGE_DANISH = "DA";
    public const string LANGUAGE_ENGLISH = "EN";
    public const string LANGUAGE_FINNISH = "FI";
    public const string LANGUAGE_NORWEGIAN = "NO";
    public const string LANGUAGE_SWEDISH = "SV";
    public const string LANGUAGE_GERMANY = "DE";
    public const string LANGAUGE_RUSSIAN = "ru-RU";
}

/// <summary>
/// Country code of the countries; where we have different domains.
/// </summary>
public class CountryCodeConstant
{
    public const string COUNTRY_SWEDEN = "SE";
    public const string COUNTRY_DANMARK = "DK";
    public const string COUNTRY_NORWAY = "NO";
    public const string COUNTRY_FINLAND = "FI";
    public const string COUNTRY_GERMANY = "DE";
    public const string COUNTRY_RUSSIA = "RU";
}

/// <summary>
/// Contains the Constant for Language 
/// And Language code configured in opera for Native languages
/// </summary>
public class LanguageNameConstant
{
    public const string LANGUAGE_DANISH = "danish";
    public const string LANGUAGE_ENGLISH = "english";
    public const string LANGUAGE_FINNISH = "finnish";
    public const string LANGUAGE_NORWEGIAN = "norwegian";
    public const string LANGUAGE_SWEDISH = "swedish";
    public const string LANGUAGE_RUSSIA = "russian";
    public const string LANGUAGE_GERMANY = "dutch";
}

/// <summary>
/// 
/// </summary>
public class SiteHostConstants
{
    public const string SITEHOST_DANISH = "dk";
    public const string SITEHOST_COM = "com";
    public const string SITEHOST_FINNISH = "fi";
    public const string SITEHOST_NORWEGIAN = "no";
    public const string SITEHOST_SWEDISH = "se";
    public const string SITEHOST_RUSSIAN = "ru-RU";
    public const string SITEHOST_GERMANY = "de";
}

/// <summary>
/// Contains the Constant for KOMPOST PAGE ACTION 
/// </summary>
public class ActionConstant
{
    public const string ACTION_DANISH = "23_b908f043";
    public const string ACTION_ENGLISH = "10_42b24c1d";
    public const string ACTION_FINNISH = "22_c27b63c3";
    public const string ACTION_NORWEGIAN = "24_3037defb";
    public const string ACTION_SWEDISH = "21_ed0f2b48";
    public const string ACTION_GERMAN = "312_d84e4eb9";
    public const string ACTION_RUSSIAN = "313_244f4861";
}

/// <summary>
/// Contains the Constant for KOMPOST HTTP Request 
/// </summary>
public class HTTPRequestConstants
{
    public const string POST = "POST";
    public const string URLENCODED = "application/x-www-form-urlencoded";
    public const string QUESTION = "?";
    public const string GET = "GET";
    public const string AND = "&";
    public const string EQUAL = "=";
    public const string URL = "http://www2.carmamail.com/Forms/Action.aspx?form=";
}

/// <summary>
/// Contains the Constant for KOMPOST HTTP Request 
/// </summary>
public class KOMPOSTItemConstants
{
    public const string EMAIL = "email";
    public const string ZIPCODE = "zipcode";
    public const string COUNTRY = "country";
    public const string CORPORATE_INFORMATION = "corporateinformation";
    public const string PRESS_RELEASES = "pressreleases";
    public const string MEETING_AND_CONFERENCE = "meetingandconference";
    public const string LEISURE = "leisure";

    public const string COMPANY_ADDRESS = "companyaddress";
    public const string COMPANY_ORGANIZATION = "companyorganization";
    public const string MOBILE_NUMBER = "mobilenumber";
    public const string GENDER = "gender";
    public const string BIRTH_YEAR = "birthyear";
    public const string CITY = "city";

    public const string STREET = "street";
    public const string TITLE = "title";
    public const string LAST_NAME = "lastname";
    public const string FIRST_NAME = "firstname";
    public const string SITE_LANGUAGE = "sitelanguage";
}

public class OWSExceptionConstants
{
    public const string PROMOTION_CODE_INVALID = "41";
    public const string PROPERTY_NOT_AVAILABLE = "10";
    public const string INVALID_PROPERTY = "06";
    public const string PROPERTY_RESTRICTED = "25";
    public const string BOOKING_NOT_FOUND = "105";
    public const string BOOKING_NOT_FOUND_IGNORE = "15";
    public const string SEE_CONTACT_DETAILS_ON_CHAIN_INFO = "15";
    public const string BOOKING_ALREADY_CANCELED = "100";
    public const string CANCELLATION_TOO_LATE = "24";
    public const string RESERVATION_EXISTS_FOR_CREDITCARD = "1000";
    public const string BOOKING_LOCKED_BY_STAFF_MEMBER = "84";
    public const string GENERAL_UPD_FAILURE = "GENERAL_UPD_FAILURE";
    public const string ROOM_UNAVAILABLE = "ROOM_UNAVAILABLE";
    public const string INVALID_CREDIT_CARD = "INVALID_CREDIT_CARD";
    public const string GUARANTEE_NOT_ACCEPTED = "GUARANTEE NOT ACCEPTED";
    public const string PRIOR_STAY = "07";
    public const string INSUFFICIENT_AWARD_POINTS_FOR_RESERVATION = "INSUFFICIENT_AWARD_POINTS_FOR_RESERVATION";
}

/// <summary>
/// CMSExceptionConstants
/// </summary>
public class CMSExceptionConstants
{
    public const string BLOCK_NOT_CONFIGURED = "BLOCKNAMENOTFOUND001";
}


/// <summary>
/// Contains the text to be translated
/// </summary>
public class TranslatedTextConstansts
{
    public const string EMAILNOTIFICATION = "/bookingengine/booking/bookingdetail/EmailConfirmationTobeSent";
    public const string SMSNOTIFICATION = "/bookingengine/booking/bookingdetail/SMSConfirmationTobeSent";
    public const string LOWERFLOOR = "/bookingengine/booking/bookingdetail/lowerfloor";
    public const string HIGHERFLOOR = "/bookingengine/booking/bookingdetail/highfloor";
    public const string AWAYFROMELEVATOR = "/bookingengine/booking/bookingdetail/awayfromelevator";
    public const string NEARELEVATOR = "/bookingengine/booking/bookingdetail/nearelevator";
    public const string NOSMOKING = "/bookingengine/booking/bookingdetail/nosmoking";
    public const string SMOKING = "/bookingengine/booking/bookingdetail/smoking";
    public const string ACCESSABLEROOM = "/bookingengine/booking/bookingdetail/accessableroom";
    public const string ACCESSABLEROOMDESC = "/bookingengine/booking/bookingdetail/accessableroomhelptext";
    public const string ALLERYROOM = "/bookingengine/booking/bookingdetail/allergyroom";
    public const string ALLERYROOMDESC = "/bookingengine/booking/bookingdetail/allergyroomhelptext";
    public const string PETFRIENDLYROOM = "/bookingengine/booking/bookingdetail/PetFriendlyRoom";
    public const string PETFRIENDLYROOMDESC = "/bookingengine/booking/bookingdetail/PetFriendlyRoomhelptext";
    public const string EARLYCHECKIN = "/bookingengine/booking/bookingdetail/earlycheckin";
    public const string EARLYCHECKINDESC = "/bookingengine/booking/bookingdetail/earlycheckinhelptext";
    public const string LATECHECKOUT = "/bookingengine/booking/bookingdetail/latecheckout";
    public const string LATECHECKOUTDESC = "/bookingengine/booking/bookingdetail/latecheckouthelptext";
    public const string MALE = "/bookingengine/booking/bookingdetail/male";
    public const string FEMALE = "/bookingengine/booking/bookingdetail/female";
    public const string LATEARRIVAL = "/bookingengine/booking/bookingdetail/LateArrivalGurantee";
    public const string PLEASEGURANTEELATEARRIVAL = "/bookingengine/booking/bookingdetail/pleaseguranteelatearrival";
    public const string GURANTEE_TEXT = "/bookingengine/booking/bookingdetail/";
    public const string GURANTEEPOLICYTEXTWITHOUTCREDITCARD =
        "/bookingengine/booking/bookingdetail/guaranteepolicytext/withoutcreditcard/";

    public const string GURANTEEPOLICYTEXTWITHCREDITCARD =
        "/bookingengine/booking/bookingdetail/guaranteepolicytext/withcreditcard/";
    public const string GURANTEEPOLICYTEXTPREPAID = "/bookingengine/booking/bookingdetail/guaranteepolicytext/";

    public const string HOLDROOM = "/bookingengine/booking/bookingdetail/HoldRoomMessage";
    public const string ROOM_TYPE_AND_RATE = "/bookingengine/booking/selectrate/roomtypeandrate";
    public const string SPECIAL_RATE = "/bookingengine/booking/selectrate/splrate";
    public const string EARLY_RATE = "/bookingengine/booking/selectrate/elyrate";
    public const string FLEX_RATE = "/bookingengine/booking/selectrate/flxrate";
    public const string SELECT_RATE_PER_NIGHT = "/bookingengine/booking/selectrate/pernight";
    public const string SELECT_RATE_PER_ROOM_PER_NIGHT = "/bookingengine/booking/selecthotel/perroompernight";
    public const string SELECT_RATE_PER_STAY = "/bookingengine/booking/selectrate/perstay";
    public const string SELECT_RATE_VIEW_PRICE_PER_NIGHT = "/bookingengine/booking/selectrate/pricepernight";
    public const string SELECT_RATE_VIEW_PRICE_PER_STAY = "/bookingengine/booking/selectrate/priceperstay";
    public const string SELECT_RATE_SPECIFIC_ROOM_TYPES = "/bookingengine/booking/selectrate/specificroomtypes";
    public const string SELECT_RATE_ALL_ROOM_TYPES = "/bookingengine/booking/selectrate/allroomtypes";

    public const string BOOKING_CONFIRMATION_FAILED_EMAIL_RECIPIENT =
        "/bookingengine/booking/bookingconfirmation/failedEmailrecipient";

    public const string BOOKING_CONFIRMATION_FAILED_SMS_RECIPIENT =
        "/bookingengine/booking/bookingconfirmation/failedSMSrecipient";

    public const string ALTERNATE_HOTELS_MESSAGE = "/bookingengine/booking/selecthotel/alternateHotelMessage";
    public const string NO_HOTELS_AVAILABLE_MESSAGE = "/bookingengine/booking/selecthotel/noHotelAvailableMessage";
    public const string NO_ROOMS_AVAILABLE_MESSAGE = "/bookingengine/booking/selecthotel/noRoomsAvailableMessage";

    public const string NO_ROOMS_AVAILABLE_MESSAGE_FOR_BLOCK_CODE =
        "/bookingengine/booking/selecthotel/noRoomsAvailableMessageForBlockCode";

    public const string YOUR_BOOKING_RESERVATION_MESSAGE = "/bookingengine/booking/ModifyBookingDates/YourBooking";

    public const string YOUR_BOOKING_RESERVATION_MESSAGE_WITHOUT_NUMBER =
        "/bookingengine/booking/ModifyBookingDates/YourBookingWithOutNumber";

    public const string YOUR_PREVIOUS_BOOKING_RESERVATION_MESSAGE =
        "/bookingengine/booking/ModifyBookingDates/YourPreviousBooking";

    public const string YOUR_NEW_BOOKING_RESERVATION_MESSAGE =
        "/bookingengine/booking/ModifyBookingDates/YourNewBooking";

    public const string MODIFICATION_CONFIRMAION_MESSAGE = "/bookingengine/booking/bookingconfirmation/message";
    public const string CANCELLATION_CONFIRMAION_MESSAGE = "/bookingengine/booking/CancelledBooking/message";
    public const string BOOKING_CANCELLED_MESSAGE = "/bookingengine/booking/CancelledBooking/bookingcancelledmessage";

    public const string BOOKING_ALREADY_CANCELED =
        "/scanweb/bookingengine/errormessages/businesserror/bookingalreadyCancelled";

    public const string BOOKING_LOCKED_BY_STAFF_MEMBER =
        "/scanweb/bookingengine/errormessages/businesserror/bookinglockedbystaffmember";

    public const string INVALID_SURNAME =
        "/scanweb/bookingengine/errormessages/businesserror/booking_search_invalid_surname";

    public const string INVALID_BOOKING_NUMBER =
        "/scanweb/bookingengine/errormessages/businesserror/booking_search_invalid_booking_number";

    public const string NOT_ONLINE_BOOKING = "/scanweb/bookingengine/errormessages/businesserror/not_online_booking";
    public const string BOOKING_NOT_CANCELABLE = "/bookingengine/booking/ModifyBookingDates/BookingNotCancellable";
    public const string BLOCK_NOT_CANCELABLE = "/bookingengine/booking/ModifyBookingDates/PrePaidBlockNotCancellable";
    public const string BOOKING_NOT_MODIFIABLE = "/bookingengine/booking/ModifyBookingDates/BookingNotModifiable";
    public const string CANCALLATION_TOO_LATE = "/scanweb/bookingengine/errormessages/businesserror/cancellationtoolate";
    public const string BOOKING_ALREADY_DONE = "/scanweb/bookingengine/errormessages/businesserror/bookingAlreadyDone";

    public const string RESERVATION_EXISTS_FOR_CREDITCARD =
        "/scanweb/bookingengine/errormessages/owserror/ReservationExistsForCreditCard";

    public const string CURRENCY_CONVERTER_AMOUNT = "/bookingengine/booking/currencyConverter/ConvertedAmount";

    public const string CURRENCY_CONVERTER_ENTER_VALID_AMOUNT =
        "/scanweb/bookingengine/errormessages/requiredfielderror/entervalidAmount";

    public const string VOUCHER_CODE_INVALID =
        "/scanweb/bookingengine/errormessages/requiredfielderror/voucher_booking_code";

    public const string BLOCK_CODE_OUT_SIDE_DATE =
        "/scanweb/bookingengine/errormessages/owserror/invalid_allotment_code";

    public const string PROPERTY_NOT_AVAILABLE =
        "/scanweb/bookingengine/errormessages/owserror/property_not_available_block_code";

    public const string BOOKING_RESERVATION_GUARANTEED =
        "/bookingengine/booking/bookingconfirmation/reservationGuaranteed";
    public const string ROOM_UNAVAILABLE = "/scanweb/bookingengine/errormessages/owserror/room_notavailable";
    public const string END_DATE_INVALID = "/scanweb/bookingengine/errormessages/requiredfielderror/endDateInvalid";
}


/// <summary>
/// Contains the constants for Phone Details
/// </summary>
public class PhoneContants
{
    public const string PHONETYPE_MOBILE = "MOBILE";
    public const string PHONETYPE_HOME = "HOME";
    public const string PHONEROLE = "PHONE";
    public const string PHONETYPE_FAX = "FAX";
}

/// ////////////////////////////////////////////////////////////////////////////////////////
//  Purpose 					: Provides Constants for Identifying templates for Email 
//                                messages.                     		                  //
//																						  //
//----------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------//
// Additional Information		: 
//                              
//                                                                                       //
//	Last Modified By			:														  //
//	Last Modified Date          :                                                         //     
////////////////////////////////////////////////////////////////////////////////////////////             
public class TemplateType
{
    public const string BOOKING_CONFIRMATION_EMAIL_TEMPLATE = "BookingEngine_Email_";
    public const string BOOKING_CONFIRMATION_SMS_TEMPLATE = "BookingEngine_SMS_";
    public const string BOOKING_CANCELLATION_SMS_TEMPLATE = "BookingCancellation_SMS_";
    public const string BOOKING_CONFIRMATION_TEMPLATE_TYPE_XML = "XML";
    public const string BOOKING_CONFIRMATION_TEMPLATE_TYPE_CMS = "CMS";

    public const string BOOKING_CONFIRMATION_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/booking/confirmation/template";

    public const string BOOKING_CONFIRMATION_TEXT_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/booking/confirmation/texttemplate";

    public const string BOOKING_CONFIRMATION_SMS_BODY_TEMPLATE = "languages/language/bookingengine/booking/confirmation";
    public const string BOOKING_CANCELLATION_SMS_BODY_TEMPLATE = "languages/language/bookingengine/booking/cancellation";

    public const string BOOKING_CONFIRMATION_EMAIL_SUBJECT =
        "languages/language/bookingengine/booking/confirmation/subject";

    public const string BOOKING_CONFIRMATION_EMAIL_SENDER =
        "languages/language/bookingengine/booking/confirmation/fromEmail";

    public const string MODIFY_BOOKING_CONFIRMATION_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/booking/modifyconfirmation/template";

    public const string MODIFY_BOOKING_CONFIRMATION_TEXT_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/booking/modifyconfirmation/texttemplate";

    public const string MODIFY_BOOKING_CONFIRMATION_EMAIL_SUBJECT =
        "languages/language/bookingengine/booking/modifyconfirmation/subject";

    public const string MODIFY_BOOKING_CONFIRMATION_EMAIL_SENDER =
        "languages/language/bookingengine/booking/modifyconfirmation/fromEmail";

    public const string CANCEL_BOOKING_CONFIRMATION_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/booking/cancelconfirmation/template";

    public const string CANCEL_BOOKING_CONFIRMATION_TEXT_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/booking/cancelconfirmation/texttemplate";

    public const string CANCEL_BOOKING_CONFIRMATION_EMAIL_SUBJECT =
        "languages/language/bookingengine/booking/cancelconfirmation/subject";

    public const string CANCEL_BOOKING_CONFIRMATION_EMAIL_SENDER =
        "languages/language/bookingengine/booking/cancelconfirmation/fromEmail";

    public const string ENROLL_CONFIRMATION_EMAIL_TEMPLATE = "BookingEngine_Email_";
    public const string ENROLL_CONFIRMATION_TEMPLATE_TYPE_XML = "XML";
    public const string ENROLL_CONFIRMATION_TEMPLATE_TYPE_CMS = "CMS";

    public const string ENROLL_CONFIRMATION_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/booking/enrollconfirmation/template";

    public const string ENROLL_CONFIRMATION_EMAIL_SUBJECT =
        "languages/language/bookingengine/booking/enrollconfirmation/subject";

    public const string ENROLL_CONFIRMATION_EMAIL_SENDER =
        "languages/language/bookingengine/booking/enrollconfirmation/fromEmail";

    public const string MISSING_POINT_CONFIRMATION_SC_EMAIL_TEMPLATE = "BookingEngine_Email_";
    public const string MISSING_POINT_CONFIRMATION_SC_SMS_TEMPLATE = "BookingEngine_SMS_";
    public const string MISSING_POINT_CONFIRMATION_SC_TEMPLATE_TYPE_XML = "XML";
    public const string MISSING_POINT_CONFIRMATION_SC_TEMPLATE_TYPE_CMS = "CMS";

    public const string MISSING_POINT_CONFIRMATION_SC_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/missingpointconfirmationsc/template";

    public const string MISSING_POINT_CONFIRMATION_SC_SMS_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/missingpointconfirmationsc";

    public const string MISSING_POINT_CONFIRMATION_SC_EMAIL_SUBJECT =
        "languages/language/bookingengine/loyalty/missingpointconfirmationsc/subject";

    public const string MISSING_POINT_CONFIRMATION_SC_EMAIL_RECIEVER =
        "languages/language/bookingengine/loyalty/missingpointconfirmationsc/toEmail";

    public const string MISSING_POINT_CONFIRMATION_SC_EMAIL_SENDER =
        "languages/language/bookingengine/loyalty/missingpointconfirmationsc/fromEmail";

    public const string MISSING_POINT_CONFIRMATION_EMAIL_TEMPLATE = "BookingEngine_Email_";
    public const string MISSING_POINT_CONFIRMATION_SMS_TEMPLATE = "BookingEngine_SMS_";
    public const string MISSING_POINT_CONFIRMATION_TEMPLATE_TYPE_XML = "XML";
    public const string MISSING_POINT_CONFIRMATION_TEMPLATE_TYPE_CMS = "CMS";

    public const string MISSING_POINT_CONFIRMATION_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/missingpointconfirmation/template";

    public const string MISSING_POINT_CONFIRMATION_SMS_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/missingpointconfirmation";

    public const string MISSING_POINT_CONFIRMATION_EMAIL_SUBJECT =
        "languages/language/bookingengine/loyalty/missingpointconfirmation/subject";

    public const string MISSING_POINT_CONFIRMATION_EMAIL_SENDER =
        "languages/language/bookingengine/loyalty/missingpointconfirmation/fromEmail";


    public const string CONTACTUS_CONFIRMATION_SC_EMAIL_TEMPLATE = "BookingEngine_Email_";
    public const string CONTACTUS_CONFIRMATION_SC_SMS_TEMPLATE = "BookingEngine_SMS_";
    public const string CONTACTUS_CONFIRMATION_SC_TEMPLATE_TYPE_XML = "XML";
    public const string CONTACTUS_CONFIRMATION_SC_TEMPLATE_TYPE_CMS = "CMS";

    public const string CONTACTUS_CONFIRMATION_SC_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/contactusconfirmationsc/template";

    public const string CONTACTUS_CONFIRMATION_SC_SMS_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/contactusconfirmationsc";

    public const string CONTACTUS_CONFIRMATION_SC_EMAIL_SUBJECT =
        "languages/language/bookingengine/loyalty/contactusconfirmationsc/subject";

    public const string CONTACTUS_CONFIRMATION_SC_EMAIL_RECIEVER =
        "languages/language/bookingengine/loyalty/contactusconfirmationsc/toEmail";

    public const string CONTACTUS_CONFIRMATION_SC_EMAIL_SENDER =
        "languages/language/bookingengine/loyalty/contactusconfirmationsc/fromEmail";

    public const string CONTACTUS_CONFIRMATION_EMAIL_TEMPLATE = "BookingEngine_Email_";
    public const string CONTACTUS_CONFIRMATION_SMS_TEMPLATE = "BookingEngine_SMS_";
    public const string CONTACTUS_CONFIRMATION_TEMPLATE_TYPE_XML = "XML";
    public const string CONTACTUS_CONFIRMATION_TEMPLATE_TYPE_CMS = "CMS";

    public const string CONTACTUS_CONFIRMATION_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/contactusconfirmation/template";

    public const string CONTACTUS_CONFIRMATION_SMS_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/contactusconfirmation";

    public const string CONTACTUS_CONFIRMATION_EMAIL_SUBJECT =
        "languages/language/bookingengine/loyalty/contactusconfirmation/subject";

    public const string CONTACTUS_CONFIRMATION_EMAIL_SENDER =
        "languages/language/bookingengine/loyalty/contactusconfirmation/fromEmail";


    public const string LOSTCARDNOLOGIN_CONFIRMATION_EMAIL_TEMPLATE = "BookingEngine_Email_";
    public const string LOSTCARDNOLOGIN_CONFIRMATION_SMS_TEMPLATE = "BookingEngine_SMS_";
    public const string LOSTCARDNOLOGIN_CONFIRMATION_TEMPLATE_TYPE_XML = "XML";
    public const string LOSTCARDNOLOGIN_CONFIRMATION_TEMPLATE_TYPE_CMS = "CMS";

    public const string LOSTCARDNOLOGIN_CONFIRMATION_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/lostcardnologinconfirmation/template";

    public const string LOSTCARDNOLOGIN_CONFIRMATION_EMAIL_BODY_TEXT_TEMPLATE =
        "languages/language/bookingengine/loyalty/lostcardnologinconfirmation/texttemplate";

    public const string LOSTCARDNOLOGIN_CONFIRMATION_SMS_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/lostcardnologinconfirmation";

    public const string LOSTCARDNOLOGIN_CONFIRMATION_EMAIL_SUBJECT =
        "languages/language/bookingengine/loyalty/lostcardnologinconfirmation/subject";

    public const string LOSTCARDNOLOGIN_CONFIRMATION_EMAIL_SENDER =
        "languages/language/bookingengine/loyalty/lostcardnologinconfirmation/fromEmail";

    public const string LOSTCARDNOLOGIN_CONFIRMATION_SC_EMAIL_TEMPLATE = "BookingEngine_Email_";
    public const string LOSTCARDNOLOGIN_CONFIRMATION_SC_SMS_TEMPLATE = "BookingEngine_SMS_";
    public const string LOSTCARDNOLOGIN_CONFIRMATION_SC_TEMPLATE_TYPE_XML = "XML";
    public const string LOSTCARDNOLOGIN_CONFIRMATION_SC_TEMPLATE_TYPE_CMS = "CMS";

    public const string LOSTCARDNOLOGIN_CONFIRMATION_SC_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/lostcardnologinconfirmationsc/template";

    public const string LOSTCARDNOLOGIN_CONFIRMATION_SC_SMS_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/lostcardnologinconfirmationsc";

    public const string LOSTCARDNOLOGIN_CONFIRMATION_SC_EMAIL_SUBJECT =
        "languages/language/bookingengine/loyalty/lostcardnologinconfirmationsc/subject";

    public const string LOSTCARDNOLOGIN_CONFIRMATION_SC_EMAIL_RECIEVER =
        "languages/language/bookingengine/loyalty/lostcardnologinconfirmationsc/toEmail";

    public const string LOSTCARDNOLOGIN_CONFIRMATION_SC_EMAIL_SENDER =
        "languages/language/bookingengine/loyalty/lostcardnologinconfirmationsc/fromEmail";

    public const string LOSTCARD_CONFIRMATION_EMAIL_TEMPLATE = "BookingEngine_Email_";
    public const string LOSTCARD_CONFIRMATION_SMS_TEMPLATE = "BookingEngine_SMS_";
    public const string LOSTCARD_CONFIRMATION_TEMPLATE_TYPE_XML = "XML";
    public const string LOSTCARD_CONFIRMATION_TEMPLATE_TYPE_CMS = "CMS";

    public const string LOSTCARD_CONFIRMATION_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/lostcardconfirmation/template";

    public const string LOSTCARD_CONFIRMATION_SMS_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/lostcardconfirmation";

    public const string LOSTCARD_CONFIRMATION_EMAIL_SUBJECT =
        "languages/language/bookingengine/loyalty/lostcardconfirmation/subject";

    public const string LOSTCARD_CONFIRMATION_EMAIL_SENDER =
        "languages/language/bookingengine/loyalty/lostcardconfirmation/fromEmail";

    public const string LOSTCARD_CONFIRMATION_SC_EMAIL_TEMPLATE = "BookingEngine_Email_";
    public const string LOSTCARD_CONFIRMATION_SC_SMS_TEMPLATE = "BookingEngine_SMS_";
    public const string LOSTCARD_CONFIRMATION_SC_TEMPLATE_TYPE_XML = "XML";
    public const string LOSTCARD_CONFIRMATION_SC_TEMPLATE_TYPE_CMS = "CMS";

    public const string LOSTCARD_CONFIRMATION_SC_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/lostcardconfirmationsc/template";

    public const string LOSTCARD_CONFIRMATION_SC_SMS_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/lostcardconfirmationsc";

    public const string LOSTCARD_CONFIRMATION_SC_EMAIL_SUBJECT =
        "languages/language/bookingengine/loyalty/lostcardconfirmationsc/subject";

    public const string LOSTCARD_CONFIRMATION_SC_EMAIL_RECIEVER =
        "languages/language/bookingengine/loyalty/lostcardconfirmationsc/toEmail";

    public const string LOSTCARD_CONFIRMATION_SC_EMAIL_SENDER =
        "languages/language/bookingengine/loyalty/lostcardconfirmationsc/fromEmail";

    public const string FORGOTTEN_PASSWORD_CONFIRMATION_EMAIL_TEMPLATE = "BookingEngine_Email_";
    public const string FORGOTTEN_PASSWORD_CONFIRMATION_SMS_TEMPLATE = "BookingEngine_SMS_";
    public const string FORGOTTEN_PASSWORD_CONFIRMATION_TEMPLATE_TYPE_XML = "XML";
    public const string FORGOTTEN_PASSWORD_CONFIRMATION_TEMPLATE_TYPE_CMS = "CMS";

    public const string FORGOTTEN_PASSWORD_CONFIRMATION_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/forgottenpin/template";

    public const string FORGOTTEN_PASSWORD_CONFIRMATION_SMS_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/forgottenpin";

    public const string FORGOTTEN_PASSWORD_CONFIRMATION_EMAIL_SUBJECT =
        "languages/language/bookingengine/loyalty/forgottenpin/subject";

    public const string FORGOTTEN_PASSWORD_CONFIRMATION_EMAIL_SENDER =
        "languages/language/bookingengine/loyalty/forgottenpin/fromEmail";

    public const string INVITEAFRIEND_CONFIRMATION_EMAIL_SC_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/inviteafriendsc/template";

    public const string INVITEAFRIEND_CONFIRMATION_EMAIL_SC_SUBJECT =
        "languages/language/bookingengine/loyalty/inviteafriendsc/subject";

    public const string INVITEAFRIEND_CONFIRMATION_EMAIL_SC_TEMPLATE = "BookingEngine_Email_";

    public const string INVITEAFRIEND_CONFIRMATION_EMAIL_SC_PLACEHOLDERTEMPLATE =
        "languages/language/bookingengine/loyalty/inviteafriend_placeholdertemplate";

    public const string INVITEAFRIEND_CONFIRMATION_SC_SMS_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/inviteafriendsc";

    public const string INVITEAFRIEND_CONFIRMATION_SC_SMS_TEMPLATE = "BookingEngine_SMS_";
    public const string INVITEAFRIEND_CONFIRMATION_SC_TEMPLATE_TYPE_CMS = "CMS";
    public const string INVITEAFRIEND_CONFIRMATION_SC_TEMPLATE_TYPE_XML = "XML";

    public const string INVITEAFRIEND_CONFIRMATION_SC_TEMPLATE_TYPE_RECIEVER =
        "languages/language/bookingengine/loyalty/inviteafriendsc/toEmail";

    public const string INVITEAFRIEND_CONFIRMATION_SC_TEMPLATE_TYPE_SENDER =
        "languages/language/bookingengine/loyalty/inviteafriendsc/fromEmail";

    public const string INVITEAFRIEND_CONFIRMATION_EMAIL_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/inviteafriendconfirmation/template";

    public const string INVITEAFRIEND_CONFIRMATION_EMAIL_SUBJECT =
        "languages/language/bookingengine/loyalty/inviteafriendconfirmation/subject";

    public const string INVITEAFRIEND_CONFIRMATION_EMAIL_TEMPLATE = "BookingEngine_Email_";

    public const string INVITEAFRIEND_CONFIRMATION_SMS_BODY_TEMPLATE =
        "languages/language/bookingengine/loyalty/inviteafriendconfirmation";

    public const string INVITEAFRIEND_CONFIRMATION_SMS_TEMPLATE = "BookingEngine_SMS_";
    public const string INVITEAFRIEND_CONFIRMATION_TEMPLATE_TYPE_CMS = "CMS";
    public const string INVITEAFRIEND_CONFIRMATION_TEMPLATE_TYPE_XML = "XML";

    public const string INVITEAFRIEND_CONFIRMATION_TEMPLATE_TYPE_SENDER =
        "languages/language/bookingengine/loyalty/inviteafriendconfirmation/fromEmail";
}

/// <summary>
/// CommunicationTemplateConstants
/// </summary>
public class CommunicationTemplateConstants
{
    public const string BLANK_SPACES = "  ";
    public const string LOGOPATH = "LOGOPATH";
    public const string CONFIRMATION_NUMBER = "CONFIRMATIONNUMBER";
    public const string CANCEL_CONFIRMATION_NUMBER = "CANCELCONFIRMATIONNUMBER";
    public const string CITY_HOTEL = "CITYHOTEL";
    public const string HOTEL = "HOTEL";
    public const string HOTELADDRESS = "HOTELADDRESS";
    public const string HOTELCITY = "HOTELCITY";
    public const string HOTELPOSTCODE = "HOTELPOSTCODE";
    public const string HOTELCOUNTRY = "HOTELCOUNTRY";
    public const string HOTELTELEPHONE = "HOTELTELEPHONE";
    public const string HOTELEMAIL = "HOTELEMAIL";
    public const string NUMBER_ROOM = "NUMBERROOM";
    public const string ROOM_RATE = "ROOMRATE";
    public const string RATE_CATEGORY = "RATECATEGORY";
    public const string ROOM_RATE_SMS = "ROOMRATEFORSMS";

    public const string ROOM_TYPE = "ROOMTYPE";
    public const string ARRIVAL_DATE = "ARRIVALDATE";
    public const string NUMBER_OF_ADULTS = "NUMBEROFADULTS";
    public const string NUMBER_OF_DAYS = "NUMBEROFDAYS";
    public const string DEPARTURE_DATE = "DEPARTUREDATE";
    public const string NUMBER_OF_CHILDREN = "NUMBEROFCHILDREN";
    public const string TOTAL_PRICE = "TOTALPRICE";
    public const string TITLE = "TITLE";
    public const string FIRST_NAME = "FIRSTNAME";
    public const string COMPANY_NAME = "COMPANYNAME";
    public const string ADDRESS_LINE1 = "ADDRESSLINE1";
    public const string ADDRESS_LINE2 = "ADDRESSLINE2";
    public const string CITY = "CITY";
    public const string POST_CODE = "POSTCODE";
    public const string COUNTRY = "COUNTRY";
    public const string LASTNAME = "LASTNAME";
    public const string GENDER = "GENDER";
    public const string TELEPHONE1 = "TELEPHONE1";
    public const string TELEPHONE2 = "TELEPHONE2";
    public const string EMAIL = "EMAIL";
    public const string EMAIL2 = "EMAIL2";
    public const string EMAILCONFIRM = "EMAILCONFIRM";
    public const string ACCOUNTNUMBER = "ACCOUNTNUMBER";
    public const string CARD_HOLDER = "CARDHOLDER";
    public const string CARD_NUMBER = "CARDNUMBER";
    public const string CARD_TYPE = "CARDTYPE";
    public const string EXPIRY_DATE = "EXPIRYDATE";
    public const string GURANTEE_BOOKING = "GURANTEEBOOKING";
    public const string OTHER_PREFERENCE = "OTHERPREFERENCE";
    public const string TEXTFORMAT_OTHER_PREFERENCE = "TEXTFORMATOTHERPREFERENCE";
    public const string SPECIAL_SERVICE_REQUESTS = "SPECIALSERVICEREQUESTS";
    public const string TEXTFORMAT_SPECIAL_SERVICE_REQUESTS = "TEXTFORMATSPECIALSERVICEREQUESTS";
    public const string FAX = "FAX";
    public const string LEGNUMBER = "LEGNUMBER";
    public const string MAINRESERVATIONNUMBER = "MAINRESERVATIONNUMBER";

    public const string WITHOUTCREDITCARD = "WithoutCreditcard";
    public const string WITHCREDITCARD = "WithCreditcard";

    public const string START_MEMBERID = "START:MEMBERID";
    public const string END_MEMBERID = "END:MEMBERID";
    public const string NO_MEMBERID = "NO_MEMBERID";

    public const string START_PHONE = "START:PHONE";
    public const string END_PHONE = "END:PHONE";
    public const string NO_PHONE = "NO_PHONE";

    public const string START_FAX = "START:FAX";
    public const string END_FAX = "END:FAX";
    public const string NO_FAX = "NO_FAX";
    public const string DNUMBER = "DNUMBER";
    public const string START_DNUMBER = "START:DNUMBER";
    public const string END_DNUMBER = "END:DNUMBER";
    public const string NO_DNUMBER = "NO_DNUMBER";

    public const string START_RESERVATION_NUMBER = "START:RESERVATION_NUMBER";
    public const string END_RESERVATION_NUMBER = "END:RESERVATION_NUMBER";
    public const string NO_RESERVATION_NUMBER = "NO_RESERVATION_NUMBER";

    public const string START_GUSTPRG_NUMBER = "START:GUSTPRG_NUMBER";
    public const string END_GUSTPRG_NUMBER = "END:GUSTPRG_NUMBER";
    public const string NO_GUSTPRG_NUMBER = "NO_GUSTPRG_NUMBER";

    public const string COMMENTS = "COMMENTS";
    public const string EMAIL_NOTIFICATION = "EMAILNOTIFICATION";
    public const string SMS_NOTIFICATION = "SMSNOTIFICATION";
    public const string BED_TYPE_PREFERENCE = "BEDTYPEPREFERENCE";
    public const string MESSAGE = "EMAILMESSAGE";
    public const string SUBJECT = "CHOOSENSUBJECT";
    public const string MEMBERSHIPNUMBER = "MEMBERSHIPNUMBER";
    public const string FREQUENT_GUEST_PROGRAMME = "GUESTPROGRAMMENUMBER";
    public const string PASSWORD = "PASSWORD";
    public const string BOOKING_CANCALLATION_NUMBER = "CANCELLATIONNUMBER";
    public const string FRIENDSNAME = "FRIENDSNAME";
    public const string FRIENDSEMAILID = "FRIENDSEMAILID";
    public const string PLACEHOLDER_POINT = "[PLACEHOLDER]";
    public const string BOOKING_DATE = "BOOKINGDATE";
    public const string EARLIEST_CHECKIN_TIME = "EARLIESTCHECKINTIME";
    public const string LATEST_CHECKOUT_TIME = "LATESTCHECKOUTTIME";
    public const string POLICY_TEXT = "POLICYTEXT";

    public const string CONTACTUS_TEXT = "CONTACTUSTEXT";

    public const string CONFIRMATION_TEXT = "CONFIRMATIONTEXT";

    public const string STORYBOX_TEXT = "STORYBOXTEXT";

    public const string HTMLPOLICYHEADER = "HTMLCONFIRMATIONHEADER";
    public const string CANCELBYDATE = "{DATE}";
    public const string START_CHILDRENAGES = "START:CHILDRENAGES";
    public const string CHILDREN_AGES = "CHILDRENAGES";
    public const string NO_CHILDREN_AGES = "NO_CHILDRENAGES";
    public const string END_CHILDRENAGES = "END:CHILDRENAGES";

    public const string START_CHILDREN_ACCOMMODATION = "START:CHILDRENACCOMMODATION";
    public const string CHILDREN_ACCOMMODATION = "CHILDRENACCOMMODATION";
    public const string NO_CHILDREN_ACCOMMODATION = "NO_CHILDRENACCOMMODATION";
    public const string END_CHILDREN_ACCOMMODATION = "END:CHILDRENACCOMMODATION";
    public const string SMS_DEEPLINK = "SMS_DEEPLINK";

    public const string MEMBERSHIPNO_BOOKMARK_LINK_DISPLAY_STYLE = "DISPLAYSTYLE";
    public const string MEMBERSHIP_CARD_URL = "MEMBERSHIPCARDURL";
}

/// <summary>
/// SelectRateConstants
/// </summary>
public class SelectRateConstants
{
    public const string COL1_TO_SHOW = "col1ToShow";
    public const string COL2_TO_SHOW = "col2ToShow";
    public const string COL3_TO_SHOW = "col3ToShow";
    public const string COL1_RATE_CAPTION = "col1RateCaption";
    public const string COL2_RATE_CAPTION = "col2RateCaption";
    public const string COL3_RATE_CAPTION = "col3RateCaption";
    public const string COL1_RATE_CATEGORY = "col1RateCatStr";
    public const string COL2_RATE_CATEGORY = "col2RateCatStr";
    public const string COL3_RATE_CATEGORY = "col3RateCatStr";
    public const string COL1_RATE_CAT_DESC = "col1RateCatDesc";
    public const string COL2_RATE_CAT_DESC = "col2RateCatDesc";
    public const string COL3_RATE_CAT_DESC = "col3RateCatDesc";
    public const string FLEX_RATECATEGORY_COLOR = "Blue";
}

/// <summary>
/// ModifyBookingConstants
/// </summary>
public class ModifyBookingConstants
{
    public const string HOTEL_SEARCH = "HOTEL_SEARCH";
    public const string HOTEL_ROOM_RATE = "HOTEL_ROOM_RATE";
    public const string GUEST_INFORMATION = "GUEST_INFORMATION";
    public const string NEW_BOOKING_DIV = "NewBookingInfo";
}

/// <summary>
/// TransactionTypeCode
/// </summary>
public class TransactionTypeCode
{
    public const string AW = "AW";
    public const string AWC = "AWC";
    public const string ST = "ST";
    public const string OT = "OT";
}

/// <summary>
/// BookingTab
/// </summary>
public class BookingTab
{
    public const string Tab1 = "Tab1";
    public const string Tab2 = "Tab2";
    public const string Tab3 = "Tab3";
    public const string Tab4 = "Tab4";
}

/// <summary>
/// PageId
/// </summary>
public class PageId
{
    public const int HOME_PAGE = 26;
    public const int ENROLL_PAGE = 24620;
    public const int LOGIN_ERROR_PAGE = 24800;
    public const int LOGOUT_CONFIRMATION_PAGE = 24801;
    public const int LOST_CARD_NOT_LOGGEDIN = 24410;
    public const int FORGOTTEN_PASSWORD_PAGE = 24413;
    public const int SELECT_HOTEL = 735;
    public const int SELECT_RATE = 736;
    public const int BOOKING_DETAIL = 737;
    public const int MODIFY_SELECT_RATE = 6184;
    public const int MODIFY_BOOKING_DETAIL = 6186;
}

/// <summary>
/// DateFormatConstants
/// </summary>
public class DateFormatConstants
{
    public const string DD_MM_YYYY = "dd.MM.yyyy";
}

/// <summary>
/// PageIdentifier
/// </summary>
public class TrackerConstants
{
    public const string TRACKINGERROR = "Tracking Error";
    public const string SEARILIZATIONERROR = "Error In Searilization";
    public const string TRACKINGOWS = "Tracking OWS Error";
    public const string OWSREQRES = "Searializing OWS Requst And Response";

    public const string TRACKINGNAVIGATION = "Tracking Navigation";
    public const string TRACKINGUSERACTION = "Tracking User Action";
    public const string USERPAGEACTIONS = "UserPageActions";
    public const string IS_TRACKER_ENABLED = "Enable_Tracker";
    public const string SESSION_IDENTITY_FORMAT = "{0}<BR>{1}<BR>";
    public const string SESSION_IDENTITY = "Session Identity:{0}:";
    public static bool Is_Tracker_Enabled
    {
        get { return bool.Parse(ConfigurationManager.AppSettings[IS_TRACKER_ENABLED]); }
    }
    public const string NO_RATE_RETURNED = "No rate returned for Hotel Id ={0} - Hotel Name = {1}";
    public const string MISSING_OPERAID = "Could not find a HotelDestination with the specified OperaID {0}.";
    

    //String Constants
    public const string LOG_TYPE = "Log Type";
    public const string FIRST_NAME = "First Name";
    public const string LAST_NAME = "Last Name";
    public const string GENDER = "Gender";
    public const string DOB_DAY = "Day";
    public const string DOB_MONTH = "Month";
    public const string DOB_YEAR = "Year";

    public const string TITLE = "Log Type";
    public const string E_MAIL = "E-Mail";
    public const string PHONE_TYPE_1 = "Phone Type 1";
    public const string TELEPHONE1 = "Telephone1 ";
    public const string PHONETYPE2 = "Phone Type2";
    public const string TELEPHONE2 = "Telephone2";
    public const string ADDRESS_LINE1 = "Address Line1";
    public const string ADDRESS_LINE2 = "Address Line2";
    public const string POST_CODE = "Post Code";
    public const string CITY = "City";
    public const string COUNTRY = "Country";
    public const string PARTNER_PROGRAM_VALUE = "Partner Program Value";
    public const string PREFERENCELOWERFLOOR = "Preference Lower floor";
    public const string PREFERENCEHIGHFLOOR = "Preference High floor";
    public const string PREFERENCEAWAYFROMELEVATOR = "Preference AwayFromElevator";
    public const string PREFERENCEAWAYNEARELEVATOR = "Preference AwayNearElevator";
    public const string SPECIALPREFNOSMOKING = "SpecialPref NoSmoking";
    public const string SPECIALPREFACCESSABLEROOM = "SpecialPref AccessableRoom";
    public const string SPECIALPREFALLERGYROOMS = "SpecialPref AllergyRooms";
    public const string PREFERREDLANGUAGE = "Preferred Language";
    public const string RECEIVESCANDICINFO = "Receive ScandicInfo";
    public const string RECEIVE_PARTNERSINFO = "Receive PartnersInfo";
    public const string CREDIT_CARD = "Credit Card";
    public const string CREDIT_CARD_MASK = "XXXXXXXXXXXXXXXX";
}

/// <summary>
/// PageIdentifier
/// </summary>
public class PageIdentifier
{
    public const string BookingPageIdentifier = "Booking";
    public const string ModifyBookingPageIdentifier = "ModifyBooking";
    public const string CancelBookingPageIdentifier = "CancelBooking";
    public const string FGPOverviewPage = "Frequent Guest Program Overview Page";
    public const string ScanwebStartpage = "[Scanweb] Start page";
    public const string ScanwebStandardpage = "[Scanweb] Standard page";
    public const string ReservationConfirmation = "Reservation - Confirmation";
    public const string ModifyReservationCancelledBooking = "Modify Reservation - Cancelled Booking";
}

/// <summary>
/// FastTrackEnrolmentMessageConstants
/// </summary>
public class FastTrackEnrolmentMessageConstants
{
    public const string SuccessfulSubscription = "/bookingengine/booking/campaignLanding/successfulUpgradeMessage";
    public const string InvalidPromoCodeNewMember = "/scanweb/bookingengine/errormessages/owserror/promo_code_invalid";

    public const string InvalidPromoCodeExistingMember =
        "/scanweb/bookingengine/errormessages/owserror/promo_code_invalid_for_existing_member";

    public const string PromoCodeExpiredOperaMessage = "Error: Promotion code({0}) does not exists. ,";

    public const string PromoCodeGenericMessageForNewUser =
        "/scanweb/bookingengine/errormessages/businesserror/promo_code_generic_message_new_user";

    public const string PromoCodeGenericMessageForExistingUser =
        "/scanweb/bookingengine/errormessages/businesserror/promo_code_generic_message_existing_user";
}

/// <summary>
/// This enum represents the type of the booking page
/// This is used while navigating to/fro booking flow
/// </summary>
public enum BookingEnginePosition
{
    MAIN_SEARCH_PAGE,
    CHILDRENS_DETAILS_PAGE,
    SELECT_HOTEL_PAGE,
    SELECT_RATE_PAGE,
    BOOKING_DETAILS_PAGE
}


public enum packageCodeSource
{
    RATE,
    RESERVATION
}

//public enum HotelRateDiscount
//{
//    WithDiscount = 1,
//    NoDiscount = 2

//}
/// <summary>
/// Constants used by google map and info bubbles
/// </summary>
public class GoogleMapConstants
{
    public const int MAX_LENGTH_ADVANCED_INFOBOX_HEADING = 41;
    public const int MAX_LENGTH_ADVANCED_INFOBOX_ADDRESS = 39;
    public const int MAX_LENGTH_ADVANCED_INFOBOX_SHORTDESCRIPTION = 125;
    public const int MAX_LENGTH_ADVANCED_INFOBOX_PROMOLINKTEXT = 25;
    public const string TRIPPLE_DOTS = "...";
    public const int BASIC_MARKER_WIDTH = 19;
    public const int BASIC_MARKER_HEIGHT = 21;
    public const int BASIC_MARKER_SHADOW_WIDTH = 27;
    public const int BASIC_MARKER_SHADOW_HEIGHT = 27;
    public const int BASIC_MARKER_ICON_ANCHOR_X = 15;
    public const int BASIC_MARKER_ICON_ANCHOR_Y = 13;
    public const int BASIC_MARKER_INFOBOX_ANCHOR_X = 15;
    public const int BASIC_MARKER_INFOBOX_ANCHOR_Y = 13;

    public const int ADVANCED_MARKER_WIDTH = 19;
    public const int ADVANCED_MARKER_HEIGHT = 21;
    public const int ADVANCED_MARKER_SHADOW_WIDTH = 45;
    public const int ADVANCED_MARKER_SHADOW_HEIGHT = 29;
    public const int ADVANCED_MARKER_ICON_ANCHOR_X = 15;
    public const int ADVANCED_MARKER_ICON_ANCHOR_Y = 13;
    public const int ADVANCED_MARKER_INFOBOX_ANCHOR_X = 15;
    public const int ADVANCED_MARKER_INFOBOX_ANCHOR_Y = 170;

    public const int MIN_WIDTH_BASIC_INFOBOX = 225;
    public const int MIN_WIDTH_ADVANCED_INFOBOX = 350;
    public const int MIN_HEIGHT_ADVANCED_INFOBOX = 145;
    public const int MAX_WIDTH_BASIC_INFOBOX = 204;
    public const int MAX_WIDTH_ADVANCED_INFOBOX = 350;
    public const int ARROW_POSITION = 50;

    public const int MAX_HEIGHT_ADVANCED_INFOBOX = 160;
    public const int MIN_HEIGHT_BASIC_INFOBOX = 120;
    public const int MAX_HEIGHT_BASIC_INFOBOX = 125;

    public const int PADDING_X_HOTELFILTER = 4;
    public const int PADDING_Y_HOTELFILTER = 4;
    public const int PADDING_X_ADVANCED_MARKER_POPUP = 5;
    public const int PADDING_Y_ADVANCED_MARKER_POPUP = 20;


    public const int SH_MAX_WIDTH_ADVANCED_INFOBOX = 350;
    public const int SH_ADVANCED_MARKER_WIDTH = 126;
    public const int SH_ADVANCED_MARKER_HEIGHT = 70;
    public const int SH_ADVANCED_MARKER_WIDTH_DNUMBER = 126;
    public const int SH_ADVANCED_MARKER_HEIGHT_DNUMBER = 86;

    public const int SH_ADVANCED_MARKER_SHADOW_WIDTH = 45;
    public const int SH_ADVANCED_MARKER_SHADOW_HEIGHT = 29;
    public const int SH_ADVANCED_MARKER_ICON_ANCHOR_X = 68;
    public const int SH_ADVANCED_MARKER_ICON_ANCHOR_Y = 63;
    public const int SH_ADVANCED_MARKER_INFOBOX_ANCHOR_X = 5;
    public const int SH_ADVANCED_MARKER_INFOBOX_ANCHOR_Y = 27;
    public const int SH_PADDING_X_ADVANCED_MARKER_POPUP = 35;
    public const int SH_PADDING_Y_ADVANCED_MARKER_POPUP = 5;
}

/// <summary>
/// Constants used for 'Reward Night Gift' booking.
/// </summary>
public class RewardNightGiftBooking
{
    public const string REWARD_NIGHT_GIFT_COMMENT_FORMAT_START = "Reward night give away";
    public const char REWARD_NIGHT_GIFT_COMMENT_FORMAT_SEPARATOR = '/';
    public const char REWARD_NIGHT_GIFT_COMMENT_FORMAT_NAME_SEPARATOR = ' ';
    public const int REWARD_NIGHT_GIFT_COMMENT_FORMAT_COUNT = 5;
}

/// <summary>
/// SessionBookingConstants
/// </summary>
public class SessionBookingConstants
{
    public const string SESSION_BOOKING_NAME_TITLE = "SessionNameTitle";
    public const string SESSION_BOOKING_FIRST_NAME = "SessionFName";
    public const string SESSION_BOOKING_LAST_NAME = "SessionLName";
    public const string SESSION_BOOKING_CITY = "SessionCity";
    public const string SESSION_BOOKING_COUNTRY = "SessionCountry";
    public const string SESSION_CREDITCARD_TYPE = "VA";
    public const string SESSION_CREDITCARD_NUM = "4444111144441111";
    public const int SESSION_CREDITCARD_EXP_MONTH = 12;
    public const int SESSION_CREDITCARD_EXP_YEAR = 2024;
    public const string NETS_PAYMENT_RESPONSECODE = "ok";
    public const string SAVE_CREDITCARD_RESPONSECODE = "ok";
}

/// <summary>
/// QueryStringConstants
/// </summary>
public class QueryStringConstants
{
    public const string QUERY_STRING_PRINT = "print";
    public const string QUERY_STRING_CANCEL_PRINT = "CancelPageprint";
    public const string QUERY_STRING_PARTNER_ID = "PID";
    public const string PAYMENT_TRANSACTIONID = "transactionId";
    public const string PAYMENT_RESPONSECODE = "responseCode";
    public const string SESSION_EXPIRED = "SessionExpired";
    public const string SESSION_TIMEOUT_PAGE_REFERENCEKEY = "referenceKey";
    public const string QUERY_STRING_MODE = "mode";
}

public class PaymentConstants
{
    public const string PMSID = "PMSID";
    public const string PMSLEGNO = "PMSLEGNO";
    public const string Scanweb = "Scanweb";
    public const string LongInfo = "LongInfo-Deposit";
    public const string ShortInfo = "ShortInfo-Deposit";
    public const string StringNeeded = "StringNeeded";
    public const string PrimaryLangID = "E";
    public const string PaymentWindowErrorSource = "PaymentWindow";
    public const string PaymentProcessingPageReferenceKeyValue = "PaymentProcessing";
    public const string Payment = "Payment";
}

/// <summary>
/// ScanwebMobile
/// </summary>
public class ScanwebMobile
{
    public static string MOBILE_CONTENT_LANGUAGE = "MobileContentLanguage";
    public static string MOBILE_LANGUAGE_KEY = "Mobile.LanguageKey";
    public static string ERROR_PAGE_CMS_REFERENCE = "MobileErrorPage";
    public static string IS_MOBILE_ERROR = "IsMobileError";
}
/// <summary>
/// Displaying Warning in Edit Mode
/// </summary>
public class EditModeWarning
{
    public static string VALIDATION_FOR_AVAILABILITY = "'Available in Booking' Property has been changed for this Hotel";
    public static string OPERAID = "OperaID";
    public static string SCANWEB_HOTEL_PAGETYPE = "[Scanweb] Hotel";
    public static string AVAILABLEINBOOKING_PROP_NAME = "AvailableInBooking";

    /// <summary>
    /// Gets email recipients for Hotel Not available in booking email alert 
    /// </summary>
    public static string[] EmailRecipientsForHotelNotAvailableInBookingEmailAlert
    {
        get { return ConfigurationManager.AppSettings["EmailRecipientsForHotelNotAvailableInBookingEmailAlert"].Split(','); }
    }

    /// <summary>
    /// Gets email sender for Hotel Not available in booking email alert 
    /// </summary>
    public static string EmailSenderForHotelNotAvailableInBookingEmailAlert
    {
        get { return ConfigurationManager.AppSettings["EmailSenderForHotelNotAvailableInBookingEmailAlert"]; }
    }

    /// <summary>
    /// Gets Email subject when a hotel is configured as Not Available in booking 
    /// </summary>
    public static string HotelNotAvailableInBookingEmailSubject
    {
        get { return ConfigurationManager.AppSettings["HotelNotAvailableInBookingEmailSubject"]; }
    }

    /// <summary>
    /// Gets Email body when a hotel is configured as Not Available in booking 
    /// </summary>
    public static string HotelNotAvailableInBookingEmailBody
    {
        get { return ConfigurationManager.AppSettings["HotelNotAvailableInBookingEmailBody"]; }
    }
    
}