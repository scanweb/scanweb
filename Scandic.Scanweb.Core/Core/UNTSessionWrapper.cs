﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml.Serialization;
using System.Web.SessionState;

namespace Scandic.Scanweb.Core
{
    public class UNTSessionWrapper
    {
        public static ActionItems UserActionStore
        {
            get
            {
                if (HttpContext.Current == null) return null;
                var actionItems = HttpContext.Current.Session["UserActionStore"] as ActionItems;
                if (actionItems != null)
                {
                    return actionItems;
                }
                else
                {
                    actionItems = new ActionItems();
                    HttpContext.Current.Session.Add("UserActionStore", actionItems);
                }
                return actionItems;
            }
            set { HttpContext.Current.Session.Add("UserActionStore", value); }
        }
    }
}
