﻿//  Description					:   ScanwebCacheManager                                   //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//  Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scandic.Scanweb.Core.Core.Interface;

namespace Scandic.Scanweb.Core.Core
{
    /// <summary>
    /// This class provides all the operations for managing Cache.
    /// </summary>
    public class ScanwebCacheManager : CacheManager
    {
        private static CacheManager cacheManager;

        /// <summary>
        /// Gets the instance of Singleton CacheManager class.
        /// </summary>
        public static CacheManager Instance
        {
            get
            {
                if (cacheManager == null)
                {
                    object syncLock = new object();
                    lock (syncLock)
                    {
                        cacheManager = new ScanwebCacheManager();    
                    }
                }
                return cacheManager;
            }
        }

        private ScanwebCacheManager()
        {
        }
    }
}
