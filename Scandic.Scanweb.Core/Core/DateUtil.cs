using System;

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// DateUtil
    /// </summary>
    public class DateUtil
    {
        #region DateToDDMMYYYYWords

        /// <summary>
        /// Convert the DateTime element to a string dd MMMM yyyy format i.e., 11 November 2007 etc.,
        /// </summary>
        private const string DDMMMMYYYY = "dd MMMM yyyy";

        public static string DateToDDMMYYYYWords(DateTime date)
        {
            return date.ToString(DDMMMMYYYY);
        }

        #endregion DateToDDMMYYYYWords

        #region DateToMMMMDDYYYYWords

        /// <summary>
        /// Convert the DateTime element to a string dd MMMM yyyy format i.e.,11 November 2007 etc.,
        /// </summary>
        private const string MMMMDDYYYY = "dd MMMM yyyy";

        public static string DateToDDMMMMYYYYWords(DateTime date)
        {
            return date.ToString(MMMMDDYYYY);
        }

        #endregion DateToMMMMDDYYYYWords

        #region DateToMMMMYYYYWords

        /// <summary>
        /// Convert the DateTime element to a string MMMM yyyy format i.e., November 2007 etc.,
        /// </summary>
        private const string MMMMYYYY = "MMMM yyyy";

        public static string DateToMMMMYYYYWords(DateTime date)
        {
            return date.ToString(MMMMYYYY);
        }

        #endregion DateToDDMMYYYYWords

        #region DateToDDMMYYYYString

        /// <summary>
        /// Convert the DateTime element to a string in dd/MM/yyyy format
        /// </summary>
        /// <param name="date"></param>
        /// <returns>DateTimeString</returns>
        public static string DateToDDMMYYYYString(DateTime date)
        {
            string strDay = string.Empty;
            string strMonth = string.Empty;
            if (date.Day < 10)
            {
                strDay = "0" + date.Day.ToString();
            }
            else
            {
                strDay = date.Day.ToString();
            }
            if (date.Month < 10)
            {
                strMonth = "0" + date.Month.ToString();
            }
            else
            {
                strMonth = date.Month.ToString();
            }
            string strYear = date.Year.ToString();
            string dtStr = strDay + CoreConstants.SLASH + strMonth + CoreConstants.SLASH + strYear;
            return dtStr;
        }

        #endregion DateToDDMMYYYYString

        #region StringToDDMMYYYDate

        /// <summary>
        /// Convert the string in DD/MM/YYYY format to DateTimeObject
        /// </summary>
        /// <param name="date">
        /// Date in string
        /// </param>
        /// <returns>
        /// DateTime object
        /// </returns>
        public static DateTime StringToDDMMYYYDate(string date)
        {
            date = RemoveDayFormDate(date);
            int pos1 = date.IndexOf(CoreConstants.SLASH);
            int pos2 = date.IndexOf(CoreConstants.SLASH, pos1 + 1);

            string strDay = date.Substring(0, pos1);
            string strMonth = date.Substring(pos1 + 1, pos2 - (pos1 + 1));
            string strYear = date.Substring(pos2 + 1);
            DateTime dateTime = new DateTime(Int32.Parse(strYear), Int32.Parse(strMonth), (Int32.Parse(strDay)));
            return dateTime;
        }

        /// <summary>
        /// Convert the string in DD-MM-YY format to DateTime
        /// </summary>
        /// <param name="date">
        /// Date in string Format(DD-MM-YY)
        /// </param>
        /// <returns>
        /// DateTime object
        /// </returns>
        public static DateTime ConvertDDMMYYToDateTime(string date)
        {
            string dateSeparator = string.Empty;
            if (date.Contains(CoreConstants.DOTCONSTANT))
                dateSeparator = CoreConstants.DOTCONSTANT;
            else if (date.Contains(CoreConstants.HYPHEN))
                dateSeparator = CoreConstants.HYPHEN;
            else if (date.Contains(CoreConstants.SLASH))
                dateSeparator = CoreConstants.SLASH;

            int pos1 = date.IndexOf(dateSeparator);
            int pos2 = date.IndexOf(dateSeparator, pos1 + 1);

            string[] dateArr = date.Split(dateSeparator.ToCharArray());
            string strDay = string.Empty;
            string strMonth = string.Empty;
            string strYear = string.Empty;

            if (dateArr != null && dateArr.Length > 0)
            {
                strDay = dateArr[0];
                strMonth = dateArr[1];
                strYear = dateArr[2];

                if (strMonth.Length < 3 && Convert.ToInt32(strMonth) > 12)
                {
                    string strtempDay = strDay;
                    strDay = strMonth;
                    strMonth = strtempDay;
                }
            }

            //int pos1 = date.IndexOf(CoreConstants.HYPHEN);
            //int pos2 = date.IndexOf(CoreConstants.HYPHEN, pos1 + 1);
            //string strDay = date.Substring(0, pos1);
            //DateTime parsedDate = DateTime.Parse(date);
            //int month = parsedDate.Month;
            //string strYear = date.Substring(pos2 + 1);

            string[] separator = { CoreConstants.SPACE };
            string[] splitedYear = strYear.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            int hour = 0;
            if (splitedYear.Length > 0)
            {
                strYear = splitedYear[0];
                strYear = CoreConstants.YEARPREFIX + strYear;
                string strhour = string.Empty;
                if (splitedYear.Length > 1)
                {
                    strhour = splitedYear[1];
                    if (strhour.Contains(CoreConstants.PMCONSTANT))
                    {
                        string[] hourSeparator = { CoreConstants.PMCONSTANT };
                        string[] splitedHour = strhour.Split(hourSeparator, StringSplitOptions.RemoveEmptyEntries);
                        if (splitedHour.Length > 0)
                        {
                            hour = 12 + Int32.Parse(splitedHour[0]);
                        }
                    }
                    else if (strhour.Contains(CoreConstants.AMCONSTANT))
                    {
                        string[] hourSeparator = { CoreConstants.AMCONSTANT };
                        string[] splitedHour = strhour.Split(hourSeparator, StringSplitOptions.RemoveEmptyEntries);
                        if (splitedHour.Length > 0)
                        {
                            hour = Int32.Parse(splitedHour[0]);
                        }
                    }
                }
            }

            if (strMonth.Length > 2)
                strMonth = Convert.ToString(DateTime.Parse(date).Month);

            //DateTime dateTime = new DateTime((Int32.Parse(strYear)), month, (Int32.Parse(strDay)), hour, 0, 0);
            DateTime dateTime = new DateTime((Int32.Parse(strYear)), (Int32.Parse(strMonth)), (Int32.Parse(strDay)), hour, 0, 0);
            return dateTime;
        }

        #endregion StringToDDMMYYYDate

        #region DateDifference

        /// <summary>
        /// Returns the differenc in number of Days of toDate to fromDate
        /// </summary>
        /// <param name="toDate"></param>
        /// <param name="fromDate"></param>
        /// <returns>Date Difference</returns>
        public static int DateDifference(DateTime toDate, DateTime fromDate)
        {
            TimeSpan timeSpan = toDate - fromDate;
            return timeSpan.Days;
        }

        #endregion DateDifference

        #region GetFirstDayOfMonth

        /// <summary>
        /// Get the first day of the month for
        /// any full date submitted
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns>Date Time</returns>
        public static DateTime GetFirstDayOfMonth(DateTime dtDate)
        {
            DateTime dtFrom = dtDate;
            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1));
            return dtFrom;
        }

        #endregion GetFirstDayOfMonth

        #region GetLastDayOfMonth

        /// <summary>
        /// Get the last day of the month for any
        /// full date
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns>DateTime</returns>
        public static DateTime GetLastDayOfMonth(DateTime dtDate)
        {
            DateTime dtTo = dtDate;
            dtTo = dtTo.AddMonths(1);
            dtTo = dtTo.AddDays(-(dtTo.Day));
            return dtTo;
        }

        #endregion GetLastDayOfMonth

        /// <summary>
        /// Removes day from Date string if it contains day.
        /// e.g. "Fri 20/05/2010" will be removed to "20/05/2010".
        /// </summary>
        /// <param name="date">Date string from the booking module</param>
        /// <returns>Date string without day</returns>
        private static string RemoveDayFormDate(string date)
        {
            int positionSlash = date.IndexOf(CoreConstants.SLASH);
            string dateString = date.Substring(positionSlash - 2);
            return dateString;
        }

        public static string DateToDDMMMYYYYWords(DateTime date)
        {
            string monthName = char.ToUpper(date.ToString("MMM")[0]) + date.ToString("MMM").Substring(1);
            monthName = Convert.ToString(monthName.Substring(0, 3));
            string dateString = string.Format("{0} {1} {2}", date.Day, monthName, date.Year);
            return dateString;
        }
    }
}