// --------------------------------------------------------------------------------------------------
// Description:                This class is intended to define the logging functionality in Scanweb.
// Created By:                 Nandkishore M
// Created Date:               20-Dec-07
// Version:                    1.0
// --------------------------------------------------------------------------------------------------
// Modification History       
// Modified By:
// Modification Description:
// --------------------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Text;
using System.Threading;
using System.Web;
using log4net;

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// This class implements the logging functionality in Scanweb.
    /// </summary>
    public class AppLogger : BaseLogger
    {
        /// <summary>
        /// Static constructor.
        /// </summary>
        static AppLogger()
        {
            RootLogger = LogManager.GetLogger("root");
            FatalLogger = LogManager.GetLogger("scanweb-critical");
            OWSLogger = LogManager.GetLogger("scanweb-ows");
            GeneralInfoLogger = LogManager.GetLogger("scanweb-info");
            OnlinePaymentInfoLogger = LogManager.GetLogger("scanweb-onlinepayment-info");
            OnlinePaymentFatalLogger = LogManager.GetLogger("scanweb-onlinepayment-critical");
            OnlinePaymentTransactionsInfoLogger = LogManager.GetLogger("scanweb-onlinepaymenttransactions-info");
            SessionNotValidLogger = LogManager.GetLogger("scanweb-sessionnotvalidscenarios-info");
            SmtpAppenderLogger = LogManager.GetLogger("scanweb-smtpappenderlogger-info");
            ReservationRatesLogger = LogManager.GetLogger("scanweb-reservationrateslogger-info");
            CSVLogger = LogManager.GetLogger("csv-logger");
        }

        /// <summary>
        /// Protected constructor.
        /// </summary>
        protected AppLogger()
        {
        }

        #region Public Methods

        /// <summary>
        /// This method is called to log any fatal exceptions.
        /// </summary>
        /// <param name="Ex">The Exception that is thrown or caught.</param>
        /// <param name="CustomMessage">Any custom message to provide more information on the error/exception.</param>
        public static void LogFatalException(Exception Ex, string CustomMessage)
        {
            using (log4net.NDC.Push(Thread.CurrentThread.Name))
            {
                FatalLogger.Fatal(CustomMessage, Ex);
            }
        }

        /// <summary>
        /// This method is called to log any fatal exceptions.
        /// </summary>
        /// <param name="Ex">The Exception that is thrown or caught.</param>
        public static void LogFatalException(Exception Ex)
        {
            using (log4net.NDC.Push(Thread.CurrentThread.Name))
            {
                LogFatalException(Ex, string.Empty);
            }
        }

        /// <summary>
        /// This method is called to log OWS and Business exceptions.
        /// </summary>
        /// <param name="Ex">The OWS/Business exception object.</param>
        /// <param name="ExceptionType">This parameter specifies the type of exception - OWS/Business.</param>
        /// <param name="CustomMessage">Any additional information about the exception.</param>
        public static void LogCustomException(Exception Ex, string ExceptionType, string CustomMessage)
        {
            using (log4net.NDC.Push(Thread.CurrentThread.Name))
            {
                string exMessage = GetExceptionData(Ex, ExceptionType, CustomMessage);
                FatalLogger.Fatal(exMessage, Ex);
            }
        }

        /// <summary>
        /// This method is called to log OWS and Business exceptions.
        /// </summary>
        /// <param name="Ex">The OWS/Business exception object.</param>
        /// <param name="ExceptionType">This parameter specifies the type of exception - OWS/Business.</param>
        public static void LogCustomException(Exception Ex, string ExceptionType)
        {
            using (log4net.NDC.Push(Thread.CurrentThread.Name))
            {
                LogCustomException(Ex, ExceptionType, string.Empty);
            }
        }

        /// <summary>
        /// This method is used to log OWS request/response information.
        /// </summary>
        /// <param name="message">The formatted Request/Response information to be logged.</param>
        public static void LogOWSInfoMessage(string message)
        {
            using (log4net.NDC.Push(Thread.CurrentThread.Name))
            {
                OWSLogger.Info(message);
            }
        }

        /// <summary>
        /// This method is used to log general debug information.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        public static void LogInfoMessage(string message)
        {
            using (log4net.NDC.Push(Thread.CurrentThread.Name))
            {
                GeneralInfoLogger.Info(message);
            }
        }

        /// <summary>
        /// This method is used to log online payment info messages.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        public static void LogOnlinePaymentInfoMessage(string message)
        {
            using (log4net.NDC.Push(Thread.CurrentThread.Name))
            {
                OnlinePaymentInfoLogger.Info(message);
            }
        }

        /// This method is called to log online payment fatal exceptions.
        /// <param name="ex">The Exception that is thrown or caught.</param>
        /// <param name="customMessage">Any custom message to provide more information on the error/exception.</param>
        public static void LogOnlinePaymentFatalException(Exception ex, string customMessage)
        {
            using (log4net.NDC.Push(Thread.CurrentThread.Name))
            {
                OnlinePaymentFatalLogger.Fatal(customMessage, ex);
            }
        }

        /// <summary>
        /// This method is used to log online payment transactions info messages.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        public static void LogOnlinePaymentTransactionsInfoMessage(string message)
        {
            using (log4net.NDC.Push(Thread.CurrentThread.Name))
            {
                OnlinePaymentTransactionsInfoLogger.Info(message);
            }
        }

        /// <summary>
        /// This method is used to log messages when Session is not valid.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        public static void LogSessionNotValidScenarios(string message)
        {
            using (log4net.NDC.Push(Thread.CurrentThread.Name))
            {
                SessionNotValidLogger.Fatal(message);
            }
        }
        /// <summary>
        /// This method is used to send SMS while logging.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        public static void LogSmtpAppenderLogger(string message)
        {
            using (log4net.NDC.Push(Thread.CurrentThread.Name))
            {
                SmtpAppenderLogger.Fatal(message);
            }
        }

        /// <summary>
        /// This method is used for reservation rates logging.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        public static void LogReservationRatesInfoLogger(string message)
        {
            using (log4net.NDC.Push(Thread.CurrentThread.Name))
            {
                ReservationRatesLogger.Info(message);
            }
        }

        /// <summary>
        /// This method is used to log request response execution time in CSV file.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        public static void CSVInfoLogger(string message)
        {
            using (log4net.NDC.Push(Thread.CurrentThread.Name))
            {
                CSVLogger.Info(message);
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// This method returns a string that concatinates all the exception data values.
        /// </summary>
        /// <param name="Ex">The exception object.</param>
        /// <param name="ExceptionType">The type of exception as in OWS/Business exception.</param>
        /// <param name="CustomMessage">Any additional information that needs to be logged.</param>
        /// <returns>String containing the exception data values.</returns>
        private static string GetExceptionData(Exception Ex, string ExceptionType, string CustomMessage)
        {
            StringBuilder exceptionBuilder = new StringBuilder();
            try
            {
                exceptionBuilder.Append(Environment.NewLine);
                exceptionBuilder.Append(ExceptionType + Environment.NewLine);
                exceptionBuilder.Append("--------------------------------------------------------------------" +
                                        Environment.NewLine);
                exceptionBuilder.Append("Exception Message : " + Ex.Message + Environment.NewLine);
                exceptionBuilder.Append("Custom Message : " + CustomMessage + Environment.NewLine);
                string ExceptionTime = DateTime.Now.ToString();
                exceptionBuilder.Append("Exception Time : " + ExceptionTime + Environment.NewLine);
                exceptionBuilder.Append("Exception Source : " + Ex.StackTrace + Environment.NewLine);

                if (Ex.Data != null && Ex.Data.Count > 0)
                {
                    exceptionBuilder.Append("Exception Data : " + Environment.NewLine);

                    IDictionaryEnumerator exEnumerator = Ex.Data.GetEnumerator();
                    string dataKey, dataValue;
                    while (exEnumerator.MoveNext())
                    {
                        dataKey = string.Empty;
                        dataValue = string.Empty;

                        dataKey = exEnumerator.Key.ToString();
                        if (exEnumerator.Value != null)
                            dataValue = exEnumerator.Value.ToString();
                        exceptionBuilder.Append(dataKey + " : " + dataValue + Environment.NewLine);
                    }
                }
                exceptionBuilder.Append("=====================================================================" +
                                        Environment.NewLine);
                exceptionBuilder.Append(Environment.NewLine);
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    HttpContext.Current.Session["ExceptionMessage"] = Ex.Message;
                    HttpContext.Current.Session["ExceptionTime"] = ExceptionTime;
                } 
            }
            catch (Exception genEx)
            {
                AppLogger.LogFatalException(genEx, genEx.Message);
            }
            return exceptionBuilder.ToString();
        }

        #endregion Private Methods
    }
}