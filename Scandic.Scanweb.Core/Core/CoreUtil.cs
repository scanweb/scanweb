using System.Collections;
using System.Collections.Generic;
using System.Web;
using System;
namespace Scandic.Scanweb.Core
{
    public class CoreUtil
    {
        /// <summary>
        /// This method will replace the supplied token in the string with the supplied value
        /// if the Token is not found in the string the original string will be returned.
        /// </summary>
        /// <param name="actualString">The string with embeds the tokens</param>
        /// <param name="token">the token to be replaced</param>
        /// <param name="tokenvalue">The token value</param>
        /// <returns>String</returns>
        public static string TokenReplacer(string actualString, string token, string tokenvalue)
        {
            if (actualString.IndexOf(token) != -1)
            {
                return actualString.Replace(token, tokenvalue);
            }
            else
            {
                return actualString;
            }
        }

        /// <summary>
        /// This method will replace the list of tokens supplied with the corresponding values
        /// The list of tokens to be replaced will be passed in the Dictionary : token as key 
        /// token value as value
        /// This method internally uses the other overloaded method
        /// </summary>
        /// <param name="actualString">The string containing the tokens</param>
        /// <param name="tokenValues">The dictionary object containing the token as key and token value as value</param>
        /// <returns>String</returns>
        public static string TokenReplacer(string actualString, Dictionary<string, string> tokenValues)
        {
            ArrayList tokens = new ArrayList(tokenValues.Keys);

            for (int i = 0; i < tokens.Count; i++)
            {
                string token = (string) tokens[i];
                string value = tokenValues[token];
                actualString = TokenReplacer(actualString, token, value);
            }

            return actualString;
        }

        public static string GetRequestSource()
        {
            string requestSource = string.Empty;
            try
            {
                if (HttpContext.Current.Request.RawUrl.Contains("/mobile"))
                    requestSource = "Mobile";
                else
                    requestSource = "Scanweb";
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Exception in GetRequestSource method for CSV Logging");
            }
            return requestSource;
        }
    }
}