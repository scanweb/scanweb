////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  StringUtil                                             //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// String utility class.
    /// </summary>
    public class StringUtil
    {
        /// <summary>
        /// Checks whether the given string exists in the array of strings. 
        /// </summary>
        /// <param name="strings"></param>
        /// <param name="stringToSearch"></param>
        /// <returns></returns>
        public static bool IsStringInArray(string[] strings, string stringToSearch)
        {
            if (strings == null)
            {
                return false;
            }
            else
            {
                for (int i = 0; i < strings.Length; i++)
                {
                    if (stringToSearch == strings[i]) return true;
                }
            }
            return false;
        }
    }
}