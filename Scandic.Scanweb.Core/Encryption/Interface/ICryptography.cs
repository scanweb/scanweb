﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Scanweb.Core.Encryption.Interface
{
    public interface ICryptography
    {
        /// <summary>
        /// Encrypts the specified data within ICryptoData implementation object
        /// using default key and preset initialization vector
        /// </summary>
        /// <param name="sourceData">ICryptoData implementation object that contains data to be encrypted</param>
        /// <param name="encryptionKey">String key to encrypt data</param>
        /// <returns>Encrypted data as ICryptoData implementation object</returns>
        ICryptoData Encrypt(ICryptoData sourceData, string encryptionKey);

        /// <summary>
        /// Decrypts the specified data with the provided encription key
        /// </summary>
        /// <param name="encryptedData">ICryptoData implementation object that contains data to be decrypted</param>
        /// <param name="encryptionKey">Decryption key as string 16-32 characters long (safest to use ASCII chars)</param>
        /// <returns>ICryptoData implementation object that contain decrypted data</returns>
        ICryptoData Decrypt(ICryptoData encryptedData, string encryptionKey);
    }
}
