﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Scanweb.Core.Encryption.Interface
{
    /// <summary>
    /// This interface defines the data would be passed as parameter for 
    /// the encryption and decryption funtionality.
    /// </summary>
    public interface ICryptoData
    {
        #region Properties
        /// <summary>
        /// Determines the default text encoding across ALL Data instances
        /// </summary>
        Encoding DefaultEncoding { get; set; }

        /// <summary>
        /// Determines the default text encoding for current Data instance
        /// </summary>
        Encoding Encoding { get; set; }

        ///  <summary>
        ///  Minimum number of bytes allowed for this data; if 0, no limit
        ///  </summary>
        int MinBytes { get; set; }

        ///  <summary>
        ///  Maximum number of bytes allowed for this data; if 0, no limit
        ///  </summary>
        int MaxBytes { get; set; }

        ///  <summary>
        ///  Set an array of bytes or gets an array of bytes
        ///  </summary>
        byte[] Bytes { get; set; }

        ///  <summary>
        ///  Sets or returns text representation of bytes using the default text encoding
        ///  </summary>
        string Text { get; set; }

        ///  <summary>
        ///  Sets or returns Base64 string representation of this data
        ///  </summary>
        string Base64 { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Changes the length of the byte array appending null values
        /// </summary>
        /// <param name="length"></param>
        void AdjustDataLength(int length);
        #endregion

    }
}
