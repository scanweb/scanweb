﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scandic.Scanweb.Core.Encryption.Interface;

namespace Scandic.Scanweb.Core.Encryption
{
    public class CryptoData : ICryptoData
    {
        #region Declarations

        private byte[] m_byData;
        private int m_i32MaxBytes;
        private int m_i32MinBytes;

        ///  <summary>
        ///  Determines the default text encoding across ALL Data instances
        ///  </summary>
        private static System.Text.Encoding m_defaultEncoding = System.Text.Encoding.ASCII;

        ///  <summary>
        ///  Determines the default text encoding for this Data instance
        ///  </summary>
        private System.Text.Encoding m_encoding = m_defaultEncoding;

        #endregion

        #region Constructors

        ///  <summary>
        ///  Creates new, empty encryption data
        ///  </summary>
        public CryptoData()
        {
        }

        /// <summary>
        /// Creates new encryption data with the specified byte array
        /// </summary>
        /// <param name="byData"></param>
        public CryptoData(byte[] byData)
        {
            m_byData = byData;
        }

        /// <summary>
        ///  Creates new encryption data with the specified string; 
        ///  will be converted to byte array using default encoding
        /// </summary>
        /// <param name="data"></param>
        public CryptoData(string data)
        {
            ICryptoData cryptoData = (ICryptoData)this;
            cryptoData.Text = data;
        }

        /// <summary>
        /// Creates new encryption data using the specified string and the 
        /// specified encoding to convert the string to a byte array.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="encoding"></param>
        public CryptoData(string data, System.Text.Encoding encoding)
        {
            this.m_encoding = encoding;
            ICryptoData cryptoData = (ICryptoData)this;
            cryptoData.Text = data;
        }

        #endregion

        #region ICryptoData Members Implementations

        #region Properties

        ///  <summary>
        ///  Sets or returns Base64 string representation of this data
        ///  </summary>
        string ICryptoData.Base64
        {
            get
            {
                if (m_byData == null || m_byData.Length == 0)
                {
                    return string.Empty;
                }
                return Convert.ToBase64String(m_byData);
            }
            set
            {
                if (value == null || value.Length == 0)
                {
                    m_byData = null;
                }
                else
                {
                    m_byData = Convert.FromBase64String(value);
                }
            }
        }

        ///  <summary>
        ///  Set an array of bytes or gets an array of bytes
        ///  </summary>
        byte[] ICryptoData.Bytes
        {
            get
            {
                return m_byData;
            }
            set
            {
                m_byData = value;
            }
        }

        /// <summary>
        /// Determines the default text encoding across ALL Data instances
        /// </summary>
        Encoding ICryptoData.DefaultEncoding
        {
            get
            {
                return m_defaultEncoding;
            }
            set
            {
                m_defaultEncoding = value;
            }
        }

        /// <summary>
        /// Determines the default text encoding for this Data instance
        /// </summary>
        Encoding ICryptoData.Encoding
        {
            get
            {
                return m_encoding;
            }
            set
            {
                m_encoding = value;
            }
        }

        ///  <summary>
        ///  Maximum number of bytes allowed for this data; if 0, no limit
        ///  </summary>
        int ICryptoData.MaxBytes
        {
            get
            {
                return m_i32MaxBytes;
            }
            set
            {
                m_i32MaxBytes = value;
            }
        }

        ///  <summary>
        ///  Minimum number of bytes allowed for this data; if 0, no limit
        ///  </summary>
        int ICryptoData.MinBytes
        {
            get
            {
                return m_i32MinBytes;
            }
            set
            {
                m_i32MinBytes = value;
            }
        }

        ///  <summary>
        ///  Sets or returns text representation of bytes using the default text encoding
        ///  </summary>
        string ICryptoData.Text
        {
            get
            {
                if (m_byData == null)
                {
                    return "";
                }
                else
                {
                    int i = Array.IndexOf(m_byData, System.Convert.ToByte(0));
                    if (i >= 0)
                    {
                        return this.m_encoding.GetString(m_byData, 0, i);
                    }
                    else
                    {
                        return this.m_encoding.GetString(m_byData);
                    }
                }
            }
            set
            {
                m_byData = this.m_encoding.GetBytes(value);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Changes the length of the byte array appending null values
        /// </summary>
        /// <param name="length"></param>
        void ICryptoData.AdjustDataLength(int length)
        {
            Array.Resize<byte>(ref m_byData, length);
        }

        #endregion

        #endregion
    }
}
