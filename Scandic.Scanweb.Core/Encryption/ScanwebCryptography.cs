﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scandic.Scanweb.Core.Encryption.Interface;
using System.Security.Cryptography;

namespace Scandic.Scanweb.Core.Encryption
{
    public class ScanwebCryptography : ICryptography
    {
        #region Declaration

        private static ScanwebCryptography instance;
        private static object syncLock = new object();
        private const string DEFAULT_INITIALIZATION_VECTOR = "!2Gz=($qP&5P};%7";//must be 16 ASCI characters

        #endregion
       
        #region Constructor
        
        public ScanwebCryptography()
        {
            
        }

        #endregion

        #region Property

        public static ScanwebCryptography Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncLock)
                    {
                        if (instance == null)
                        {
                            instance = new ScanwebCryptography();
                        }
                    }
                }

                return instance;
            }
        }

        #endregion

        #region ICryptography Members

        public ICryptoData Encrypt(ICryptoData sourceData, string encryptionKey)
        {
            if (string.IsNullOrEmpty(encryptionKey))
                throw new ArgumentNullException("encryption Key not supplied.");

            if (sourceData == null || sourceData.Bytes == null)
                throw new ArgumentNullException("source Data should not be null.");

            ICryptoData cd = EncryptDirect(sourceData, new CryptoData(encryptionKey));
            return cd;
        }

        public ICryptoData Decrypt(ICryptoData encryptedData, string encryptionKey)
        {
            if (string.IsNullOrEmpty(encryptionKey))
                throw new ArgumentNullException("dencryption Key not supplied.");

            if (encryptedData == null)
                throw new ArgumentNullException("encrypted data should not be null.");

            return DecryptDirect(encryptedData, new CryptoData(encryptionKey));
        }

        #endregion

        ///  <summary>
        ///  Decrypts the specified data using provided key data and preset initialization vector
        ///  </summary>
        ///  <param name="encryptedData">ICryptoData implementation's object that contains data to be decrypted</param>
        ///  <param name="encryptionKeyData">ICryptoData implementation's object that contains the key</param>
        ///  <returns>ICryptoData implementation's object that contain decrypted data</returns>
        ///  <remarks></remarks>
        private static ICryptoData DecryptDirect(ICryptoData encryptedData, ICryptoData encryptionKeyData)
        {

            if (encryptedData == null || encryptedData.Bytes == null || encryptedData.Bytes.Length == 0)
                throw new ArgumentNullException("encrypted data should not be null.");

            
            if (encryptionKeyData == null || encryptionKeyData.Bytes == null)
                throw new ArgumentNullException("dencryption Key not supplied.");

            int i32KeyLength = encryptionKeyData.Bytes.Length;
            if (i32KeyLength < 16 ||
                i32KeyLength > 32)
                throw new ArgumentException("Encryption key lenght should not be less then 16 or greater than 32 bytes.");


            SymmetricAlgorithm crypto = new RijndaelManaged();
            ICryptoData initializingVector = new CryptoData(DEFAULT_INITIALIZATION_VECTOR);
            System.IO.MemoryStream ms = new System.IO.MemoryStream(encryptedData.Bytes, 0, encryptedData.Bytes.Length);
            byte[] byBuffer = new byte[encryptedData.Bytes.Length];

            InitializeData(ref encryptionKeyData, crypto);
            InitializeData(ref initializingVector, crypto);

            crypto.Key = encryptionKeyData.Bytes;
            crypto.IV = initializingVector.Bytes;
            try
            {
                using (CryptoStream cs = new CryptoStream(ms, crypto.CreateDecryptor(), CryptoStreamMode.Read))
                {
                    cs.Read(byBuffer, 0, encryptedData.Bytes.Length - 1);
                }
            }
            catch (CryptographicException ex)
            {
                throw new ApplicationException("Fail to decrypt encrypted data.", ex);
            }

            return new CryptoData(byBuffer);
        }

        /// <summary>
        /// Encrypt the passed data with the supplied key data and preset initializing vector
        /// </summary>
        /// <param name="sourceData">ICryptoData implementation's object that contains data to be encrypted</param>
        /// <param name="encryptionKeyData">Key to encrypt data</param>
        /// <returns>Encrypted data as CryptoData object</returns>
        /// <remarks></remarks>
        private static ICryptoData EncryptDirect(ICryptoData sourceData, ICryptoData encryptionKeyData)
        {
            if (sourceData == null || sourceData.Bytes == null)
                throw new ArgumentNullException("encryptionData cannot be null");

            if (encryptionKeyData == null || encryptionKeyData.Bytes == null)
                throw new ArgumentNullException("encryptionKeyData cannot be null");

            int i32KeyLength = encryptionKeyData.Bytes.Length;
            if (i32KeyLength < 16 ||
                    i32KeyLength > 32)
                throw new ArgumentException("Encryption key lenght should not be less then 16 or greater than 32 bytes.");


            SymmetricAlgorithm crypto = new RijndaelManaged();
            ICryptoData initializingVector = new CryptoData(DEFAULT_INITIALIZATION_VECTOR);

            // initialize the min and max size of key and vector
            InitializeData(ref encryptionKeyData, crypto);
            InitializeData(ref initializingVector, crypto);

            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                // assign key and initilizing vector to cryptography algorithm
                crypto.Key = encryptionKeyData.Bytes;
                crypto.IV = initializingVector.Bytes;

                // encrypt data
                using (CryptoStream cs = new CryptoStream(ms, crypto.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(sourceData.Bytes, 0, sourceData.Bytes.Length);
                }

                return new CryptoData(ms.ToArray());
            }
        }

        /// <summary>
        /// Verifies the key and initialization vector are of correct length.
        /// </summary>
        /// <param name="data">Data to be initialized</param>
        /// <param name="crypto">Algorithm default parameters</param>
        /// <remarks></remarks>
        private static void InitializeData(ref ICryptoData data, SymmetricAlgorithm crypto)
        {
            if (crypto != null && data != null)
            {
                data.MaxBytes = crypto.LegalKeySizes[0].MaxSize / 8;
                data.MinBytes = crypto.LegalKeySizes[0].MinSize / 8;

                int i32KeyLength = data.Bytes.Length;
                if (i32KeyLength > data.MinBytes && i32KeyLength < data.MaxBytes)
                {
                    int i32KeyNewLength = crypto.LegalKeySizes[0].MinSize / 8;
                    while ((i32KeyLength > i32KeyNewLength) && i32KeyNewLength < crypto.LegalKeySizes[0].MaxSize / 8)
                    {
                        i32KeyNewLength += crypto.LegalKeySizes[0].SkipSize / 8;
                    }

                    if (i32KeyNewLength != i32KeyLength)
                        data.AdjustDataLength(i32KeyNewLength);
                }
            }
        }
    }
}
