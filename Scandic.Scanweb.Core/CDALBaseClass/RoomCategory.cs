//  Description					: RoomCategory                                            //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// This class represents room cateogry entity.
    /// </summary>
    public class RoomCategory : ICloneable
    {
        /// <summary>
        /// ImageURL property for room image
        /// </summary>
        public string ImageURL { get; set; }

        /// <summary>
        /// Room Category Tooltip Text
        /// </summary>
        public string RoomCategoryTooltipText { get; set; }

        /// <summary>
        /// The room category like Superior, Room....
        /// </summary>
        private string roomCategoryId;

        /// <summary>
        /// The translated roomCategoryName. The translated text 
        /// for Superior, Room etc.,
        /// </summary>
        private string roomCategoryName;

        /// <summary>
        /// This is the URL, which will take to the room category page in the website
        /// </summary>
        private string roomCategoryURL;

        /// <summary>
        /// The list of room types associated to the room category
        /// </summary>
        private List<RoomType> roomTypes = new List<RoomType>();

        /// <summary>
        /// Gets RoomCategoryId
        /// </summary>
        public string RoomCategoryId
        {
            get { return roomCategoryId; }
        }
        
        /// <summary>
        /// Gets RoomCategoryName
        /// </summary>
        public string RoomCategoryName
        {
            get { return roomCategoryName; }
        }

        /// <summary>
        /// Teaser Text for Room Category
        /// </summary>
        public string RoomCategoryteserText { get; set; }

        /// <summary>
        /// Gets/Sets RoomCategoryDescription 
        /// </summary>
        public string RoomCategoryDescription { get; set; }

        /// <summary>
        /// Gets RoomCategoryURL
        /// </summary>
        public string RoomCategoryURL
        {
            get { return roomCategoryURL; }
        }

        /// <summary>
        /// Gets RoomTypes
        /// </summary>
        public List<RoomType> RoomTypes
        {
            get { return roomTypes; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="roomCategoryId"></param>
        /// <param name="roomCategoryName"></param>
        /// <param name="roomCategoryDescription"></param>
        /// <param name="roomCategoryURL"></param>
        /// <param name="roomTypes"></param>
        public RoomCategory(string roomCategoryId, string roomCategoryName,
                            string roomCategoryDescription, string roomCategoryURL,
                            List<RoomType> roomTypes)
        {
            this.roomCategoryId = roomCategoryId;
            this.roomCategoryName = roomCategoryName;
            this.RoomCategoryDescription = roomCategoryDescription;
            this.roomCategoryURL = roomCategoryURL;
            this.roomTypes = roomTypes;
        }


        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="roomCategory"></param>
        public RoomCategory(RoomCategory roomCategory)
        {
            this.roomCategoryId = roomCategory.RoomCategoryId;
            this.roomCategoryName = roomCategory.RoomCategoryName;
            this.RoomCategoryDescription = roomCategory.RoomCategoryDescription;
            this.roomCategoryURL = roomCategory.RoomCategoryURL;
            this.roomTypes = new List<RoomType>(roomCategory.RoomTypes);
        }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new RoomCategory(this);
        }
    }
}