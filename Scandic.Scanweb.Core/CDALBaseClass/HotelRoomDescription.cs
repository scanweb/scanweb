namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// HotelRoomDescription
    /// </summary>
    public class HotelRoomDescription
    {
        /// <summary>
        /// ImageURL property for room image
        /// </summary>
        public string ImageURL { get; set; }

        /// <summary>
        /// The room category description, as stored in CMS
        /// </summary>
        private string roomCategoryDescription;

        /// <summary>
        /// The room category containing the room category details
        /// </summary>
        private RoomCategory hotelRoomCategory;

        public string RoomCategoryDescription
        {
            get { return roomCategoryDescription; }
        }

        public RoomCategory HotelRoomCategory
        {
            get { return hotelRoomCategory; }
        }

        public string RoomDescriptionUrl { get; set; }

        public bool IsDefault { get; set; }

        /// <summary>
        /// Room Teaser Text for Room Category
        /// </summary>
        public string RoomTeaserText { get; set; }

        /// <summary>
        /// Room Category TooltipText
        /// </summary>
        public string RoomCategoryTooltipText { get; set; }

        /// <summary>
        /// Added parameter isDefault to know if the room category is default room catagory or not.
        /// </summary>
        /// <param name="roomDescription"></param>
        /// <param name="hotelRoomCategory"></param>
        /// <param name="roomDescriptionUrl"></param>
        /// <param name="isDefault"></param>
        public HotelRoomDescription(string roomDescription, RoomCategory hotelRoomCategory,
                                    string roomDescriptionUrl, bool isDefault)
        {
            this.roomCategoryDescription = roomDescription;
            this.hotelRoomCategory = hotelRoomCategory;
            this.RoomDescriptionUrl = roomDescriptionUrl;
            this.IsDefault = isDefault;
        }

        /// <summary>
        /// Added parameter isDefault to know if the room category is default room catagory or not.
        /// Copy Constructor
        /// </summary>
        /// <param name="hotelRoom"></param>
        public HotelRoomDescription(HotelRoomDescription hotelRoom)
        {
            this.roomCategoryDescription = hotelRoom.RoomCategoryDescription;
            this.hotelRoomCategory = (RoomCategory) hotelRoom.HotelRoomCategory.Clone();
            this.RoomDescriptionUrl = hotelRoom.RoomDescriptionUrl;
            this.IsDefault = hotelRoom.IsDefault;
        }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>object</returns>
        public object Clone()
        {
            return new HotelRoomDescription(this);
        }
    }
}