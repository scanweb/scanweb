using System;

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// Rate
    /// </summary>
    public class Rate : ICloneable
    {
        /// <summary>
        /// The Rate code as stored in Opera
        /// </summary>
        private string operaRateId;

        /// <summary>
        /// The translated description of the rate, 
        /// as stored in CMS
        /// </summary>
        private string rateCodeDescription;

        /// <summary>
        /// The Rate category to which this rate is associated to
        /// as stored in CMS
        /// </summary>
        private string rateCategoryName;

        /// <summary>
        /// Whether a booking made with this rate can be modified by user,
        /// as stored in CMS
        /// </summary>
        private bool isModifiable;

        /// <summary>
        /// Whether a booking made with this rate can be cancelled by user,
        /// as stored in CMS
        /// </summary>
        private bool isCancellable;

        /// <summary>
        /// Whether the rate value returned from Opera to be displayed to user,
        /// or should be hidden to the user,
        /// as Stored in CMS
        /// </summary>
        private bool isDisplayable;

        /// <summary>
        /// Rate
        /// </summary>
        /// <param name="operaRateId"></param>
        /// <param name="rateCodeDescription"></param>
        /// <param name="rateCategoryName"></param>
        /// <param name="isModifiable"></param>
        /// <param name="isCancellable"></param>
        /// <param name="isDisplayable"></param>
        public Rate(string operaRateId, string rateCodeDescription, string rateCategoryName,
                    bool isModifiable, bool isCancellable, bool isDisplayable)
        {
            this.operaRateId = operaRateId;
            this.rateCodeDescription = rateCodeDescription;
            this.rateCategoryName = rateCategoryName;
            this.isModifiable = isModifiable;
            this.isCancellable = isCancellable;
            this.isDisplayable = isDisplayable;
        }

        public string OperaRateId
        {
            get { return operaRateId; }
        }

        public string RateCodeDescription
        {
            get { return rateCodeDescription; }
        }

        public string RateCategoryName
        {
            get { return rateCategoryName; }
        }

        public bool IsModifiable
        {
            get { return isModifiable; }
        }

        public bool IsCancellable
        {
            get { return isCancellable; }
        }

        public bool IsDisplayable
        {
            get { return isDisplayable; }
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="rate"></param>
        public Rate(Rate rate)
        {
            this.operaRateId = rate.OperaRateId;
            this.rateCodeDescription = rate.RateCodeDescription;
            this.rateCategoryName = rate.RateCategoryName;
            this.isModifiable = rate.IsModifiable;
            this.isCancellable = rate.IsCancellable;
            this.isDisplayable = rate.IsDisplayable;
        }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>object</returns>
        public object Clone()
        {
            return new Rate(this);
        }
    }
}