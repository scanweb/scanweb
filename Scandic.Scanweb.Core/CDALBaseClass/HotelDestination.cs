using System;
using System.Collections.Generic;

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// The HotelDestination object containing the hotel details
    /// </summary>
    public class HotelDestination : Destination, ICloneable, IComparable
    {
        /// <summary>
        /// The Episerver CMS ID
        /// </summary>
        private string id;

        /// <summary>
        /// The searchable string text stored for the hotel in CMS. 
        /// This is the text against which the string typed in by
        /// user in "Hotel, destination" is searched against.
        /// </summary>
        private string searchableString;

        /// <summary>
        /// The short description of the hotel, this is the teaser text
        /// to be displayed on the "Select a Hotel" page
        /// </summary>
        private string hotelDescription;

        /// <summary>
        /// The address of the hotel containing the details
        /// </summary>
        private Address hotelAddress;

        /// <summary>
        /// The URL path of the image
        /// </summary>
        private string imageURL;
        /// <summary>
        /// The URL path of the "Hotel HomePage"
        /// </summary>
        private string hotelPageURL;
        /// <summary>
        /// The latitude and longitude positions where the hotel is located.
        /// </summary>
        private Point coordinate;

        /// <summary>
        /// The name of the airport 1 located near by hotel
        /// </summary>
        private string airport1Name;

        /// <summary>
        /// The distance of the airport 1 from the hotel
        /// </summary>
        private double airport1Distance;

        /// <summary>
        /// The name of the airport 2 located near by hotel
        /// </summary>
        private string airport2Name;

        /// <summary>
        /// The distance of airport 2 from the hotel
        /// </summary>
        private double airport2Distance;

        /// <summary>
        /// The name of the city centre near by hotel
        /// </summary>
        private string cityCenterName;

        /// <summary>
        /// The distance of the city centre from the hotel
        /// </summary>
        private double cityCenterDistance;

        /// <summary>
        /// The list of HotelRoomDescriptions associated for this hotel
        /// Each HotelRoomDescription will contain the room category and 
        /// list of room types associated with the category
        /// </summary>
        private List<HotelRoomDescription> hotelRoomDescriptions = new List<HotelRoomDescription>();

        /// <summary>
        /// The list of Alternate Hotel references containing the details
        /// of alternate hotels to hotel
        /// </summary>
        private List<AlternativeHotelReference> alternativeHotels = new List<AlternativeHotelReference>();

        /// <summary>
        /// Telephone number of the hotel
        /// </summary>
        private string telephone;

        /// <summary>
        /// Email ID of the hotel
        /// </summary>
        private string email;

        /// <summary>
        /// Fax number of the hotel
        /// </summary>
        private string fax;

        /// <summary>
        /// Customer Care number
        /// </summary>
        private string customerCarePhone;

        /// <summary>
        /// Total no of rooms in the hotel
        /// </summary>
        private int? noOfRooms;

        /// <summary>
        /// Total no of non-smoking rooms
        /// </summary>
        private int? noOfNonSmokingRooms;

        /// <summary>
        /// disabled room availability
        /// </summary>
        private bool isRoomsForDisabledAvailable;

        /// <summary>
        /// relax center availability
        /// </summary>
        private bool isRelaxCenterAvailable;

        /// <summary>
        /// restaurant and bar availability
        /// </summary>
        private bool isRestaurantAndBarAvailable;

        /// <summary>
        /// garage availability
        /// </summary>
        private bool isGarageAvailable;

        /// <summary>
        /// parking fascility availability
        /// </summary>
        private bool isOutDoorParkingAvailable;

        /// <summary>
        /// shop availability
        /// </summary>
        private bool isShopAvailable;

        /// <summary>
        /// meeting room availability
        /// </summary>
        private bool isMeetingRoomAvailable;

        /// <summary>
        /// Eco labeled
        /// </summary>
        private string ecoLabeled;

        /// <summary>
        /// distance to railway station
        /// </summary>
        private double? trainStationDistance;

        /// <summary>
        /// wifi availability
        /// </summary>
        private bool isWiFiAvailable;

        public string ShortURL { get; set; }

        public string UrlSegment { get; set; }
        public string PageTypeName { get; set; }

        /// <summary>
        /// True if this hotel is available for booking.
        /// Else false.
        /// artf1045081 : Hotel landing page | User is able to book a non bookable hotel. |Release DP
        /// If available in booking then make 'IsAvailableInBooking' property of 'HotelDestination' to true.
        /// </summary>
        private bool isAvailableInBooking;

        public bool IsAvailableInBooking
        {
            get { return isAvailableInBooking; }
        }

        /// <summary>
        /// Is this hotel displayed in footer
        /// </summary>
        private bool isDisplayedInFooter;

        public bool IsDisplayedInFooter
        {
            get { return isDisplayedInFooter; }
            set { isDisplayedInFooter = value; }
        }

        public bool IgnoreOpeningDate { get; set; }

        /// <summary>
        /// This will hold opening Date for all hotels
        /// </summary>
        private DateTime openingDate;

        public DateTime OpeningDate
        {
            get { return openingDate; }
            set { openingDate = value; }
        }

        /// <summary>
        /// Sort Index set for New Hotels displayed on site Footer
        /// </summary>
        public int footerNewHotelsSortIndex;

        public int FooterNewHotelsSortIndex
        {
            get { return footerNewHotelsSortIndex; }
            set { footerNewHotelsSortIndex = value; }
        }

        /// <summary>
        /// Promotional Page Link New Hotels displayed on site Footer
        /// </summary>
        public string promotionalPageLink;

        public string PromotionalPageLink
        {
            get { return promotionalPageLink; }
            set { promotionalPageLink = value; }
        }

        /// <summary>
        /// Promotional Page Link Text New Hotels displayed on site Footer
        /// </summary>
        public string promotionalPageLinkText;

        public string PromotionalPageLinkText
        {
            get { return promotionalPageLinkText; }
            set { promotionalPageLinkText = value; }
        }


        public string HotelCategory { get; set; }
        public string CountryCode { get; set; }
        public string AltCityDistance { get; set; }
        public string AltCityDirection { get; set; }
        public string AltCityDrivingTime { get; set; }
        public string PostalCity { get; set; }
        public string SpecialAlert { get; set; }

        public int TALocationID { get; set; }
        public bool HideHotelTARating { get; set; }
        public bool HideCityTARating { get; set; }
        public double TripAdvisorHotelRating { get; set; }
        public string Id
        {
            get { return id; }
        }

        public string SearchableString
        {
            get { return searchableString; }
        }

        public string HotelDescription
        {
            get { return hotelDescription; }
            set { hotelDescription = value; }
        }

        public Address HotelAddress
        {
            get { return hotelAddress; }
        }

        public string ImageURL
        {
            get { return imageURL; }
        }
        public string HotelPageURL
        {
            get { return hotelPageURL; }
            set { hotelPageURL = value; }
        }
        public Point Coordinate
        {
            get { return coordinate; }
        }

        public string Airport1Name
        {
            get { return airport1Name; }
        }

        public double Airport1Distance
        {
            get { return airport1Distance; }
        }

        public string Airport2Name
        {
            get { return airport2Name; }
        }

        public double Airport2Distance
        {
            get { return airport2Distance; }
        }

        public string CityCenterName
        {
            get { return cityCenterName; }
        }

        public double CityCenterDistance
        {
            get { return cityCenterDistance; }
        }

        public string Telephone
        {
            get { return telephone; }
        }

        public string Email
        {
            get { return email; }
        }

        /// <summary>
        /// Gets the fax.
        /// </summary>
        /// <value>The fax.</value>
        public string Fax
        {
            get { return fax; }
        }

        /// <summary>
        /// Gets the customer care phone.
        /// </summary>
        /// <value>The customer care phone.</value>
        public string CustomerCarePhone
        {
            get { return customerCarePhone; }
        }

        /// <summary>
        /// Gets the eco labeled.
        /// </summary>
        /// <value>The eco labeled.</value>
        public string EcoLabeled
        {
            get { return ecoLabeled; }
        }

        /// <summary>
        /// Gets the no of rooms.
        /// </summary>
        /// <value>The no of rooms.</value>
        public int? NoOfRooms
        {
            get { return noOfRooms; }
        }

        /// <summary>
        /// Gets the no of non smoking rooms.
        /// </summary>
        /// <value>The no of non smoking rooms.</value>
        public int? NoOfNonSmokingRooms
        {
            get { return noOfNonSmokingRooms; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is rooms for disabled available.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is rooms for disabled available; otherwise, <c>false</c>.
        /// </value>
        public bool IsRoomsForDisabledAvailable
        {
            get { return isRoomsForDisabledAvailable; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is relax center available.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is relax center available; otherwise, <c>false</c>.
        /// </value>
        public bool IsRelaxCenterAvailable
        {
            get { return isRelaxCenterAvailable; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is restaurant and bar available.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is restaurant and bar available; otherwise, <c>false</c>.
        /// </value>
        public bool IsRestaurantAndBarAvailable
        {
            get { return isRestaurantAndBarAvailable; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is garage available.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is garage available; otherwise, <c>false</c>.
        /// </value>
        public bool IsGarageAvailable
        {
            get { return isGarageAvailable; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is out door parking available.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is out door parking available; otherwise, <c>false</c>.
        /// </value>
        public bool IsOutDoorParkingAvailable
        {
            get { return isOutDoorParkingAvailable; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is shop available.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is shop available; otherwise, <c>false</c>.
        /// </value>
        public bool IsShopAvailable
        {
            get { return isShopAvailable; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is meeting room available.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is meeting room available; otherwise, <c>false</c>.
        /// </value>
        public bool IsMeetingRoomAvailable
        {
            get { return isMeetingRoomAvailable; }
        }

        public string City { get; set; }

        /// <summary>
        /// Gets the train station distance.
        /// </summary>
        /// <value>The train station distance.</value>
        public double? TrainStationDistance
        {
            get { return trainStationDistance; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is wi fi available.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is wi fi available; otherwise, <c>false</c>.
        /// </value>
        public bool IsWiFiAvailable
        {
            get { return isWiFiAvailable; }
        }

        public bool HideARBPrice { get; set; }

        /// <summary>
        /// Gets the hotel room descriptions.
        /// </summary>
        /// <value>The hotel room descriptions.</value>
        public List<HotelRoomDescription> HotelRoomDescriptions
        {
            get { return hotelRoomDescriptions; }
        }

        /// <summary>
        /// Gets the alternative hotels.
        /// </summary>
        /// <value>The alternative hotels.</value>
        public List<AlternativeHotelReference> AlternativeHotels
        {
            get { return alternativeHotels; }
        }

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="obj">An object to compare with this instance.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance is less than obj. Zero This instance is equal to obj. Greater than zero This instance is greater than obj.
        /// </returns>
        /// <exception cref="T:System.ArgumentException">obj is not the same type as this instance. </exception>
        public int CompareTo(object obj)
        {
            if (obj is HotelDestination)
            {
                HotelDestination p2 = (HotelDestination)obj;

                return openingDate.CompareTo(p2.openingDate);
            }
            else
                throw new ArgumentException("Object is not a Person.");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HotelDestination"/> class.
        /// </summary>
        /// <param name="operaHotelId">The opera hotel id.</param>
        /// <param name="hotelName">Name of the hotel.</param>
        /// <param name="id">The id.</param>
        /// <param name="searchableString">The searchable string.</param>
        /// <param name="hotelDescription">The hotel description.</param>
        /// <param name="hotelAddress">The hotel address.</param>
        /// <param name="imageURL">The image URL.</param>
        /// <param name="hotelPageURL">The hotel page URL.</param>
        /// <param name="coordinate">The coordinate.</param>
        /// <param name="airport1Name">Name of the airport1.</param>
        /// <param name="airport1Distance">The airport1 distance.</param>
        /// <param name="airport2Name">Name of the airport2.</param>
        /// <param name="airport2Distance">The airport2 distance.</param>
        /// <param name="cityCenterName">Name of the city center.</param>
        /// <param name="cityCenterDistance">The city center distance.</param>
        /// <param name="telephone">The telephone.</param>
        /// <param name="email">The email.</param>
        /// <param name="hotelRoomDescriptions">The hotel room descriptions.</param>
        /// <param name="alternativeHotels">The alternative hotels.</param>
        /// <param name="isAvailableInBooking">if set to <c>true</c> [is available in booking].</param>
        /// <param name="displayedInSiteFooter">if set to <c>true</c> [displayed in site footer].</param>
        /// <param name="footerNewHotelsSortIndex">Index of the footer new hotels sort.</param>
        /// <param name="promotionalPageLink">The promotional page link.</param>
        /// <param name="promotionalPageLinkText">The promotional page link text.</param>
        /// <param name="customerCarePhone">The customer care phone.</param>
        /// <param name="fax">The fax.</param>
        /// <param name="ecoLabeled">The eco labeled.</param>
        /// <param name="noOfRooms">The no of rooms.</param>
        /// <param name="noOfNonSmokingRooms">The no of non smoking rooms.</param>
        /// <param name="isRoomsForDisabledAvailable">if set to <c>true</c> [is rooms for disabled available].</param>
        /// <param name="isRelaxCenterAvailable">if set to <c>true</c> [is relax center available].</param>
        /// <param name="isRestaurantAndBarAvailable">if set to <c>true</c> [is restaurant and bar available].</param>
        /// <param name="isGarageAvailable">if set to <c>true</c> [is garage available].</param>
        /// <param name="isOutDoorParkingAvailable">if set to <c>true</c> [is out door parking available].</param>
        /// <param name="isShopAvailable">if set to <c>true</c> [is shop available].</param>
        /// <param name="isMeetingRoomAvailable">if set to <c>true</c> [is meeting room available].</param>
        /// <param name="trainStationDistance">The train station distance.</param>
        /// <param name="isWiFiAvailable">if set to <c>true</c> [is wi fi available].</param>
        /// <param name="SpecialAlert">if set to <c>true</c> [then special alerts will be displayed in booking flow].</param>
        /// <param name="taLocationID">Location id of Hotel set in CMS</param>
        public HotelDestination(string operaHotelId, string hotelName, string id, string searchableString,
                                string hotelDescription,
                                Address hotelAddress,
                                string imageURL, string hotelPageURL, Point coordinate,
                                string airport1Name, double airport1Distance,
                                string airport2Name, double airport2Distance,
                                string cityCenterName, double cityCenterDistance,
                                string city,
                                string postalCity,
                                string telephone, string email,
                                List<HotelRoomDescription> hotelRoomDescriptions,
                                List<AlternativeHotelReference> alternativeHotels, bool isAvailableInBooking,
                                bool displayedInSiteFooter,
                                int footerNewHotelsSortIndex,
                                string promotionalPageLink, string promotionalPageLinkText,
                                string customerCarePhone,
                                string fax,
                                string ecoLabeled,
                                int? noOfRooms,
                                int? noOfNonSmokingRooms,
                                bool isRoomsForDisabledAvailable,
                                bool isRelaxCenterAvailable,
                                bool isRestaurantAndBarAvailable,
                                bool isGarageAvailable,
                                bool isOutDoorParkingAvailable,
                                bool isShopAvailable,
                                bool isMeetingRoomAvailable,
                                double? trainStationDistance,
                                bool isWiFiAvailable,
                                string SpecialAlert,
                                bool hideARBPrice,
                                string shortURL,
                                int taLocationID,
                                bool hideHotelTARating, string urlsegment,string pagetypename)
        {
            this.operaDestinationId = operaHotelId;
            this.name = hotelName;
            this.id = id;
            this.searchableString = searchableString;
            this.hotelDescription = hotelDescription;
            this.hotelAddress = hotelAddress;
            this.imageURL = imageURL;
            this.hotelPageURL = hotelPageURL;
            this.coordinate = coordinate;
            this.airport1Name = airport1Name;
            this.airport1Distance = airport1Distance;
            this.airport2Name = airport2Name;
            this.airport2Distance = airport2Distance;
            this.cityCenterName = cityCenterName;
            this.cityCenterDistance = cityCenterDistance;
            this.City = city;
            this.PostalCity = postalCity;
            this.telephone = telephone;
            this.email = email;
            this.hotelRoomDescriptions = hotelRoomDescriptions;
            this.alternativeHotels = alternativeHotels;
            this.isAvailableInBooking = isAvailableInBooking;
            this.isDisplayedInFooter = displayedInSiteFooter;
            this.footerNewHotelsSortIndex = footerNewHotelsSortIndex;
            this.promotionalPageLink = promotionalPageLink;
            this.promotionalPageLinkText = promotionalPageLinkText;
            this.customerCarePhone = customerCarePhone;
            this.fax = fax;
            this.ecoLabeled = ecoLabeled;
            this.noOfRooms = noOfRooms;
            this.noOfNonSmokingRooms = noOfNonSmokingRooms;
            this.isRoomsForDisabledAvailable = isRoomsForDisabledAvailable;
            this.isRelaxCenterAvailable = isRelaxCenterAvailable;
            this.isRestaurantAndBarAvailable = isRestaurantAndBarAvailable;
            this.isGarageAvailable = isGarageAvailable;
            this.isOutDoorParkingAvailable = isOutDoorParkingAvailable;
            this.isShopAvailable = isShopAvailable;
            this.isMeetingRoomAvailable = isMeetingRoomAvailable;
            this.trainStationDistance = trainStationDistance;
            this.isWiFiAvailable = isWiFiAvailable;
            this.SpecialAlert = SpecialAlert;
            this.HideARBPrice = hideARBPrice;
            this.ShortURL = shortURL;
            this.TALocationID = taLocationID;
            this.HideHotelTARating = hideHotelTARating;
            this.UrlSegment = urlsegment;
            this.PageTypeName = pagetypename;
        }

        /// <summary>
        /// The copy constructor which will take the HotelDestination
        /// "hotel" object and copies all the properties
        /// </summary>
        /// <param name="hotel">The hotel object to be copied</param>
        public HotelDestination(HotelDestination hotel)
        {
            this.PostalCity = hotel.PostalCity;
            this.City = hotel.City;
            this.operaDestinationId = hotel.OperaDestinationId;
            this.name = hotel.Name;
            this.id = hotel.Id;
            this.searchableString = hotel.SearchableString;
            this.hotelDescription = hotel.HotelDescription;
            if (hotel.HotelAddress != null)
            {
                this.hotelAddress = (Address)hotel.HotelAddress.Clone();
            }
            this.imageURL = hotel.ImageURL;
            this.hotelPageURL = hotel.HotelPageURL;
            if (hotel.Coordinate != null)
            {
                this.coordinate = (Point)hotel.Coordinate.Clone();
            }
            this.airport1Name = hotel.Airport1Name;
            this.airport1Distance = hotel.Airport1Distance;
            this.airport2Name = hotel.Airport2Name;
            this.airport2Distance = hotel.Airport2Distance;
            this.cityCenterName = hotel.CityCenterName;
            this.cityCenterDistance = hotel.CityCenterDistance;
            this.CountryCode = hotel.CountryCode;
            this.hotelRoomDescriptions = new List<HotelRoomDescription>(hotel.HotelRoomDescriptions);
            this.alternativeHotels = new List<AlternativeHotelReference>(hotel.AlternativeHotels);
            this.isAvailableInBooking = hotel.isAvailableInBooking;
            this.isDisplayedInFooter = hotel.isDisplayedInFooter;
            this.promotionalPageLinkText = hotel.promotionalPageLinkText;
            this.promotionalPageLink = hotel.promotionalPageLink;
            this.customerCarePhone = hotel.customerCarePhone;
            this.fax = hotel.fax;
            this.ecoLabeled = hotel.ecoLabeled;
            this.noOfRooms = hotel.noOfRooms;
            this.noOfNonSmokingRooms = hotel.noOfNonSmokingRooms;
            this.isRoomsForDisabledAvailable = hotel.isRoomsForDisabledAvailable;
            this.isRelaxCenterAvailable = hotel.isRelaxCenterAvailable;
            this.isRestaurantAndBarAvailable = hotel.isRestaurantAndBarAvailable;
            this.isGarageAvailable = hotel.isGarageAvailable;
            this.isOutDoorParkingAvailable = hotel.isOutDoorParkingAvailable;
            this.isShopAvailable = hotel.isShopAvailable;
            this.isMeetingRoomAvailable = hotel.isMeetingRoomAvailable;
            this.trainStationDistance = hotel.trainStationDistance;
            this.isWiFiAvailable = hotel.isWiFiAvailable;
            this.SpecialAlert = hotel.SpecialAlert;
            this.HideARBPrice = hotel.HideARBPrice;
            this.ShortURL = hotel.ShortURL;
            this.TALocationID = hotel.TALocationID;
            this.HideHotelTARating = hotel.HideHotelTARating ==  true ? hotel.HideHotelTARating : false;
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public HotelDestination()
        {
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            return new HotelDestination(this);
        }


        public Dictionary<string, HotelImageDetailsItemEntity> ImageDetailsList { get; set; }
    }


    #region Class HotelImageDetailsItemEntity region

    /// <summary>
    /// holds the details of the image associate d withe the hotel
    /// </summary>
    public class HotelImageDetailsItemEntity
    {
        public string ImageUrl { get; set; }

        public string ImageTitle { get; set; }

        public string ImageDescription { get; set; }

        public string ByLineText { get; set; }

        public string CMSImagePath { get; set; }

        public string CMSLanguageId { get; set; }
    }

    #endregion
}