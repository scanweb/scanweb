//  Description					:   CityDestination                                       //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;

namespace Scandic.Scanweb.Core

{
    /// <summary>
    /// The CityDestination containing the City details
    /// </summary>
    public class CityDestination : Destination, ICloneable
    {
        /// <summary>
        /// The list of hotels corresponding to the city.
        /// The values of this list is dependent on the search string
        /// passed by the user.
        /// 
        /// Case I. If the search string entered by the user matches the City
        /// All the hotels in the city will be set to this property
        /// 
        /// Case II. If the search string entered by the user matches either 
        /// one or many hotels in a city. Then only the matched list of hotels 
        /// will be set to this field.
        /// </summary>
        private List<HotelDestination> searchedHotels = new List<HotelDestination>();
        private bool topDestination = false;

        /// <summary>
        /// Gets/Sets MapID
        /// </summary>
        public string MapID { get; set; }

        public string AltCityNames { get; set; }
        public string CityLinkUrl { get; set; }
        public string PageNameInWebAddress { get; set; }
        public bool TopDestination { get { return topDestination; } set {topDestination =value ;} }
        public int CityID { get; set; }

        /// <summary>
        /// The latitude and longitude positions where the City is located.
        /// </summary>
        public Point Coordinate { get; set; }

        /// <summary>
        /// Constructor to get the city deestination with mapId of the city
        /// </summary>
        /// <param name="operaCityId"></param>
        /// <param name="cityName"></param>
        /// <param name="searchedHotels"></param>
        /// <param name="mapId"></param>
        public CityDestination(string operaCityId, string cityName, List<HotelDestination> searchedHotels, string mapId,string linkUrl)
        {
            this.operaDestinationId = operaCityId;
            this.name = cityName;
            this.searchedHotels = searchedHotels;
            this.MapID = mapId;
            this.CityLinkUrl = linkUrl;
        }

        /// <summary>
        /// Constructor to get the city deestination
        /// </summary>
        /// <param name="operaCityId"></param>
        /// <param name="cityName"></param>
        /// <param name="searchedHotels"></param>
        public CityDestination(string operaCityId, string cityName, List<HotelDestination> searchedHotels)
        {
            this.operaDestinationId = operaCityId;
            this.name = cityName;
            this.searchedHotels = searchedHotels;
        }

        public CityDestination(string operaCityId, string cityName, List<HotelDestination> searchedHotels, bool topDestination,string linkUrl,int cityID)
        {
            this.operaDestinationId = operaCityId;
            this.name = cityName;
            this.searchedHotels = searchedHotels;
            this.TopDestination = topDestination;
            this.CityLinkUrl = linkUrl;
            this.CityID = cityID;
        }

        public CityDestination(string operaCityId, string cityName, List<HotelDestination> searchedHotels, bool topDestination, string linkUrl, string pageNameInWebAddress)
        {
            this.operaDestinationId = operaCityId;
            this.name = cityName;
            this.searchedHotels = searchedHotels;
            this.TopDestination = topDestination;
            this.CityLinkUrl = linkUrl;
            this.PageNameInWebAddress = pageNameInWebAddress;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="city"></param>
        public CityDestination(CityDestination city)
        {
            this.name = city.Name;
            this.operaDestinationId = city.OperaDestinationId;
            this.searchedHotels = city.SearchedHotels;
        }

        /// <summary>
        /// Gets SearchedHotels
        /// </summary>
        public List<HotelDestination> SearchedHotels
        {
            get { return searchedHotels; }
        }

        /// <summary>
        /// Gets clonable object instance of CityDestination
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new CityDestination(this);
        }
    }
}