using System;
using System.Collections.Generic;

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// RateCategory
    /// </summary>
    public class RateCategory : ICloneable
    {
        /// <summary>
        /// The Rate Categroy name static names given for
        /// different rate categories like Early, Flex....
        /// This will be common for all languages
        /// </summary>
        private string rateCategoryId;

        /// <summary>
        /// The translated Rate category name like Early, Flex....
        /// </summary>
        private string rateCategoryName;

        /// <summary>
        /// The translated description of the rate category
        /// to be displayed to the user
        /// </summary>
        private string rateCategoryDescription;

        /// <summary>
        /// The rate category URL taking the rate category specific page
        /// </summary>
        private string rateCategoryURL;

        /// <summary>
        /// If the rateCategory has 6PM Hold Available, based on this, the option will be displayed to the user
        /// </summary>
        private bool holdGuranteeAvailable;

        /// <summary>
        /// The Gruantee type to be used for the OWS when credit card booking to be used
        /// </summary>
        private string creditCardGuranteeType;

        /// <summary>
        /// Which coulmn number this rate category need to be displayed in select rate page.
        /// </summary>
        private int coulmnNumber;

        /// <summary>
        /// This will hold the value if the more info link for this rate caegory need to be displayed in
        ///  select rate page.
        /// </summary>
        private bool displayMoreInfoLink;

        private List<string> headerList;

        private string rateHighlightTextWeb;
        private string rateHighlightTextMobile;
        private string rateCategoryLanguage;
        /// <summary>
        /// The list of Rates associated to the RateCateogry
        /// </summary>
        private List<Rate> rates = new List<Rate>();

        private string rateCategoryColor = string.Empty;

        public string RateCategoryId
        {
            get { return rateCategoryId; }
        }

        public string RateCategoryName
        {
            get { return rateCategoryName; }
        }

        public string RateCategoryDescription
        {
            get { return rateCategoryDescription; }
        }

        public string RateCategoryURL
        {
            get { return rateCategoryURL; }
        }

        public List<Rate> Rates
        {
            get { return rates; }
            set { rates = value; }
        }

        public bool HoldGuranteeAvailable
        {
            get { return holdGuranteeAvailable; }
        }

        public string CreditCardGuranteeType
        {
            get { return creditCardGuranteeType; }
        }

        public int CoulmnNumber
        {
            get { return coulmnNumber; }
            set { coulmnNumber = value; }
        }

        public bool DisplayMoreInfoLink
        {
            get { return displayMoreInfoLink; }
        }

        /// <summary>
        /// Gets the color of the rate category.
        /// </summary>
        /// <value>The color of the rate category.</value>
        public string RateCategoryColor
        {
            get { return rateCategoryColor; }
        }
        /// <summary>
        /// Gets rate highlight text for web.
        /// </summary>
        /// <value>The color of the rate category.</value>
        public string RateHighlightTextWeb
        {
            get { return rateHighlightTextWeb; }
        }
        /// <summary>
        /// Gets rate highlight text for mobile.
        /// </summary>
        /// <value>The color of the rate category.</value>
        public string RateHighlightTextMobile
        {
            get { return rateHighlightTextMobile; }
        }

        public string RateCategoryLanguage
        {
            get { return rateCategoryLanguage; }
        }
        
        public List<string> HeaderList
        {
            get { return headerList; }
        }

        public RateCategory(string rateCategoryId, string rateCategoryName,
                            string rateCategoryDescription, string rateCategoryURL,
                            bool holdGuranteeAvailable, string creditCardGuranteeType,
                            List<Rate> rates, int coulmnNumber, bool displayMoreLink,
                            string rateCategoryColor, string rateHighlightTextWeb, string rateHighlightTextMobile,string rateCategoryLanguage, List<string> headers)
        {
            this.rateCategoryId = rateCategoryId;
            this.rateCategoryName = rateCategoryName;
            this.rateCategoryDescription = rateCategoryDescription;
            this.rateCategoryURL = rateCategoryURL;
            this.holdGuranteeAvailable = holdGuranteeAvailable;
            this.creditCardGuranteeType = creditCardGuranteeType;
            this.rates = rates;
            this.coulmnNumber = coulmnNumber;
            this.displayMoreInfoLink = displayMoreLink;
            this.rateCategoryColor = rateCategoryColor;
            this.rateHighlightTextWeb = rateHighlightTextWeb;
            this.rateHighlightTextMobile = rateHighlightTextMobile;
            this.rateCategoryLanguage = rateCategoryLanguage;
            this.headerList = headers;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="rateCategory"></param>
        public RateCategory(RateCategory rateCategory)
        {
            this.rateCategoryId = rateCategory.rateCategoryId;
            this.rateCategoryDescription = rateCategory.RateCategoryDescription;
            this.rateCategoryName = rateCategory.rateCategoryName;
            this.rateCategoryURL = rateCategory.RateCategoryURL;
            this.holdGuranteeAvailable = rateCategory.holdGuranteeAvailable;
            this.creditCardGuranteeType = rateCategory.creditCardGuranteeType;
            //this.rates = new List<Rate>(rateCategory.Rates);
            this.rates = new List<Rate>();
            foreach (Rate rte in rateCategory.rates)
            {
                this.rates.Add((Rate)rte.Clone());
            }

            //Release R 1.8.1 | hygiene Release | Multirate display.
            this.coulmnNumber = rateCategory.CoulmnNumber;
            this.displayMoreInfoLink = rateCategory.DisplayMoreInfoLink;
            this.headerList = rateCategory.headerList;
        }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new RateCategory(this);
        }

        /// <summary>
        /// Equals
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>True/False</returns>
        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType())
            {
                return false;
            }
            else
            {
                RateCategory rateCategory = obj as RateCategory;
                return (this.rateCategoryId == rateCategory.RateCategoryId);
            }
        }
    }
}