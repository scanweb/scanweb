using System.Text;

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// Address
    /// </summary>
    public class Address
    {
        /// <summary>
        /// The street adddress, , translated text as in CMS
        /// </summary>
        private string streetAddress;

        /// <summary>
        /// The postal code
        /// </summary>
        private string postCode;

        /// <summary>
        /// City, , translated text as in CMS
        /// </summary>
        private string city;

        /// <summary>
        /// Country name, translated text as in CMS
        /// </summary>
        private string country;

        /// <summary>
        /// PostalCity
        /// </summary>
        private string postalCity;


        public string PostalCity
        {
            get { return postalCity; }
        }

        public string StreetAddress
        {
            get { return streetAddress; }
        }

        public string PostCode
        {
            get { return postCode; }
        }

        public string City
        {
            get { return city; }
        }

        public string Country
        {
            get { return country; }
        }

        /// <summary>
        /// The constructor with all properties set
        /// </summary>
        /// <param name="streetAddress"></param>
        /// <param name="postCode"></param>
        /// <param name="city"></param>
        /// <param name="country"></param>
        public Address(string streetAddress, string postCode, string postalCity, string city, string country)
        {
            this.streetAddress = streetAddress;
            this.postCode = postCode;
            this.postalCity = postalCity;
            this.city = city;
            this.country = country;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="address"></param>
        public Address(Address address)
        {
            this.streetAddress = address.StreetAddress;
            this.postCode = address.PostCode;
            this.postalCity = address.postalCity;
            this.city = address.City;
            this.country = address.Country;
        }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>Address object</returns>
        public object Clone()
        {
            return new Address(this);
        }

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(streetAddress))
            {
                sb.Append(streetAddress);
                sb.Append(CoreConstants.COMMA).Append(CoreConstants.SPACE);
            }
            if (!string.IsNullOrEmpty(postCode))
            {
                sb.Append(postCode);
                sb.Append(CoreConstants.COMMA).Append(CoreConstants.SPACE);
            }
            if (PostalCity.ToLower() == City.ToLower())
            {
                if (!string.IsNullOrEmpty(postalCity))
                {
                    sb.Append(postalCity);
                    sb.Append(CoreConstants.COMMA).Append(CoreConstants.SPACE);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(postalCity))
                {
                    sb.Append(postalCity);
                    sb.Append(CoreConstants.SPACE);
                }
                if (!string.IsNullOrEmpty(city))
                {
                    sb.Append(city);
                    sb.Append(CoreConstants.COMMA).Append(CoreConstants.SPACE);
                }
            }

            if (!string.IsNullOrEmpty(country))
            {
                sb.Append(country);
            }
            else
            {
                if (sb.Length > 0)
                    sb.Remove(sb.Length - 2, 1);
            }
            return sb.ToString();
        }
    }
}