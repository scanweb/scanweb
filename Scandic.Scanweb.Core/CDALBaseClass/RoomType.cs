//  Description					:   RoomType                                              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
namespace Scandic.Scanweb.Core
{
    public class RoomType : ICloneable
    {
        /// <summary>
        /// The Opera room type code
        /// </summary>
        private string operaRoomTypeId;

        /// <summary>
        /// The translated description of the room type,
        /// as stored in CMS
        /// </summary>
        private string roomDescription;

        /// <summary>
        /// The room category associated to this room type,
        /// as stored in CMS
        /// </summary>
        private string roomCategoryName;

        /// <summary>
        /// There will be one to one mapping between the room type and bedding type
        /// this code will be the beddingTypeCode mapped to the 
        /// corresponding room type
        /// </summary>
        private string bedTypeCode;

        /// <summary>
        /// The bedding type description like king size bed, queen size bed
        /// in different languages
        /// </summary>
        private string bedTypeDescription;

        /// <summary>
        /// Gets OperaRoomTypeId
        /// </summary>
        public string OperaRoomTypeId
        {
            get { return operaRoomTypeId; }
        }

        /// <summary>
        /// Gets RoomDescription
        /// </summary>
        public string RoomDescription
        {
            get { return roomDescription; }
        }


        /// <summary>
        /// Gets RoomCategoryName
        /// </summary>
        public string RoomCategoryName
        {
            get { return roomCategoryName; }
        }


        /// <summary>
        /// Gets/Sets Priority
        /// </summary>
        public int Priority { get; 
            set; }


        /// <summary>
        /// Gets BedTypeCode
        /// </summary>
        public string BedTypeCode
        {
            get { return bedTypeCode; }
        }


        /// <summary>
        /// Gets BedTypeDescription
        /// </summary>
        public string BedTypeDescription
        {
            get { return bedTypeDescription; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="operaRoomTypeId"></param>
        /// <param name="roomDescription"></param>
        /// <param name="roomCategoryName"></param>
        /// <param name="priority"></param>
        /// <param name="bedTypeCode"></param>
        /// <param name="bedTypeDescription"></param>
        public RoomType(string operaRoomTypeId, string roomDescription, string roomCategoryName, int priority,
                        string bedTypeCode, string bedTypeDescription)
        {
            this.operaRoomTypeId = operaRoomTypeId;
            this.roomDescription = roomDescription;
            this.roomCategoryName = roomCategoryName;
            this.Priority = priority;
            this.bedTypeCode = bedTypeCode;
            this.bedTypeDescription = bedTypeDescription;
        }

        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="roomType"></param>
        public RoomType(RoomType roomType)
        {
            this.operaRoomTypeId = roomType.OperaRoomTypeId;
            this.roomDescription = roomType.RoomDescription;
            this.roomCategoryName = roomType.RoomCategoryName;
            this.Priority = roomType.Priority;
            this.bedTypeCode = roomType.BedTypeCode;
            this.bedTypeDescription = roomType.BedTypeDescription;
        }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new RoomType(this);
        }
    }

    /// <summary>
    /// The Comparer used to sort the room types on the priority provided in CMS
    /// </summary>
    public class RoomTypePriorityComparer : IComparer<RoomType>
    {
        public int Compare(RoomType first, RoomType second)
        {
            return first.Priority.CompareTo(second.Priority);
        }
    }
    public class RoomTypeBedTypeComparer : IEqualityComparer<RoomType>
    {

        #region IEqualityComparer<RoomType> Members

        public bool Equals(RoomType first, RoomType second)
        {

            return LowerOrderedString(first.BedTypeCode.Trim()).Equals(LowerOrderedString(second.BedTypeCode.Trim()));
        }

        public int GetHashCode(RoomType obj)
        {
            return LowerOrderedString(obj.BedTypeCode.Trim()).GetHashCode();
        }
        private string LowerOrderedString(string a)
        {
            return new string(a.ToLower().OrderBy(s => s).ToArray());
        }
        #endregion
    }
}