﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Scanweb.Core
{
    public class CountryDestinatination : Destination, ICloneable
    {
        private List<CityDestination> searchedCitys = new List<CityDestination>();
        
        public string CountryCode { get; set; }
        public string DisplayName { get; set; }
        public int SortOrder { get; set; }
        public string ClientMsg { get; set; }
        public string CountryLinkUrl { get; set; }
        public string CountryID { get; set; }

        public CountryDestinatination(string countryCode, string countryName, List<CityDestination> searchedCitys,string linkUrl,string countryID)
        {
            this.operaDestinationId = countryCode;
            this.name = countryName;
            this.searchedCitys = searchedCitys;
            this.CountryCode = countryCode;
            this.DisplayName = string.Empty;
            this.SortOrder = -1;
            this.ClientMsg = string.Empty;
            this.CountryLinkUrl = linkUrl;
            this.CountryID = countryID;
        }

        public CountryDestinatination(CountryDestinatination country)
        {
            this.name = country.Name;
            this.operaDestinationId = country.OperaDestinationId;
            this.searchedCitys = country.searchedCitys;
        }

        /// <summary>
        /// Gets SearchedHotels
        /// </summary>
        public List<CityDestination> SearchedCities
        {
            get { return searchedCitys; }
        }
        public object Clone()
        {
            return new CountryDestinatination(this);
        }
    }
}
