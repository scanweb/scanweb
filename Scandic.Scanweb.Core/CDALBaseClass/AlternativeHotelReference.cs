namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// AlternativeHotelReference
    /// </summary>
    public class AlternativeHotelReference
    {
        /// <summary>
        /// The Opera Hotel ID of the alternate hotel 
        /// </summary>
        private string operaHotelId;

        /// <summary>
        /// The distance of the alternate hotel from the hotel searched
        /// </summary>
        private double distance;

        /// <summary>
        /// The distance of city centre of searched hotel for which this hotel is alternate hotel.
        /// </summary>
        private string distanceToCityCentreOfSearchedHotel;

        public string OperaHotelId
        {
            get { return operaHotelId; }
        }

        public double Distance
        {
            get { return distance; }
        }

        /// <summary>
        /// The distance to the city center of searched Hotel.
        /// </summary>
        public string DistanceToCityCentreOfSearchedHotel
        {
            get { return distanceToCityCentreOfSearchedHotel; }
        }

        /// <summary>
        /// AlternativeHotelReference
        /// </summary>
        /// <param name="operaHotelId"></param>
        /// <param name="distance"></param>
        /// <param name="distanceToCityCentreOfSearchedHotel"></param>
        public AlternativeHotelReference(string operaHotelId, double distance,
                                         string distanceToCityCentreOfSearchedHotel)
        {
            this.operaHotelId = operaHotelId;
            this.distance = distance;
            this.distanceToCityCentreOfSearchedHotel = distanceToCityCentreOfSearchedHotel;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="alternateHotel"></param>
        public AlternativeHotelReference(AlternativeHotelReference alternateHotel)
        {
            this.distance = alternateHotel.Distance;
            this.operaHotelId = alternateHotel.OperaHotelId;
            this.distanceToCityCentreOfSearchedHotel = alternateHotel.DistanceToCityCentreOfSearchedHotel;
        }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>object</returns>
        public object Clone()
        {
            return new AlternativeHotelReference(this);
        }
    }
}