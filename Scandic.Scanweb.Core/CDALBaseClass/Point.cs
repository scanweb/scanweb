//  Description					:   Point                                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// Contains members of a point
    /// </summary>
    public class Point
    {
        /// <summary>
        /// The longitude or the x coordinate
        /// </summary>
        private double x;

        /// <summary>
        /// The latitude or the y coordinate
        /// </summary>
        private double y;

        /// <summary>
        /// The longitude or the x coordinate
        /// </summary>
        public double X
        {
            get { return x; }
        }

        /// <summary>
        /// The latitude or the y coordinate
        /// </summary>
        public double Y
        {
            get { return y; }
        }

        /// <summary>
        /// Constructor 
        /// </summary>
        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="point"></param>
        public Point(Point point)
        {
            this.x = point.x;
            this.y = point.y;
        }

        /// <summary>
        /// Clone
        /// </summary>
        public object Clone()
        {
            return new Point(this);
        }
    }
}