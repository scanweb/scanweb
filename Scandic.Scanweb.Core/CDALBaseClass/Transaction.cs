//  Description					:   Transaction                                           //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// This entity stores the details of the Transaction page 
    /// type in EPiServer under Transaction Container.
    /// </summary>
    public class Transaction
    {
        #region Private Members

        #endregion Private Members

        #region Public Properties

        /// <summary>
        /// Gets/Sets OperaID
        /// </summary>
        public string OperaID { get; set; }

        /// <summary>
        /// Gets/Sets TransactionName
        /// </summary>
        public string TransactionName { get; set; }

        #endregion Public Properties
    }
}