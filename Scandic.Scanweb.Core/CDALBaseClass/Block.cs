using System;
using System.Collections.Generic;

namespace Scandic.Scanweb.Core
{
    public class Block
    {
        /// <summary>
        /// Block OperaID.
        /// </summary>
        private string blockID;

        /// <summary>
        /// Block name
        /// </summary>
        private string blockName;

        /// <summary>
        /// Gurantee type accepted for this block
        /// </summary>
        private string guranteeType;

        /// <summary>
        /// Block category under which this block falls.
        /// </summary>
        private string blockCategoryName;

        /// <summary>
        /// Description about this block.
        /// </summary>
        private string blockDescription;

        /// <summary>
        /// Holds the date value upto which this block can be modified and canceled.
        /// </summary>
        private DateTime modifyAndCancellableDate;

        /// <summary>
        /// Page Id of the block in CMS.
        /// </summary>
        /// <remarks>This is included if we need ant further enhancement</remarks>
        private int pageId;

        /// <summary>
        /// If guarantee information need to be captured during booking.
        /// </summary>
        private bool captureGuarantee;

        /// <summary>
        /// True if 6 PM hold is available.
        /// </summary>
        private bool sixPMHoldAvailable;

        /// <summary>
        ///Dynamic header in Select rate page
        /// </summary>
        private List<string> headers;

        /// <summary>
        /// variable for storing the rateCategory Color
        /// </summary>
        private string blockCategoryColor;

        private string aboutOurRate;

        public string BlockID
        {
            get { return blockID; }
        }

        public string BlockName
        {
            get { return blockName; }
        }

        public string GuranteeType
        {
            get { return guranteeType; }
        }

        public string BlockCategoryName
        {
            get { return blockCategoryName; }
        }

        public string BlockDescription
        {
            get { return blockDescription; }
        }

        public DateTime ModifyAndCancellableDate
        {
            get { return modifyAndCancellableDate; }
        }

        public int PageId
        {
            get { return pageId; }
        }

        public bool SixPMHoldAvailable
        {
            get { return sixPMHoldAvailable; }
        }

        public bool CaptureGuarantee
        {
            get { return captureGuarantee; }
        }

        public List<string> Headers
        {
            get { return headers; }
        }

        public string AboutOurRate
        {
            get { return aboutOurRate; }
        }

        public bool HideARBPrice { get; set; }
        public string BlockCategoryColor
        {
            get { return blockCategoryColor; }
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="blockID">Block ID</param>
        /// <param name="blockName">Block name</param>
        /// <param name="guranteeType">Gurantee type this block accept</param>
        /// <param name="blockCategoryName">Block category name this block belongs to</param>
        /// <param name="blockDescription">Block Description</param>
        /// <param name="modifyAndCancellableDate">Modify and cancellable date</param>
        /// <param name="captureGuarantee">Do this block code needs to capture guarantee information</param>
        /// <param name="sixPMHoldAvailable">True if 6PM hold is available</param>
        /// <param name="pageId">Page id of this block in CMS</param>
        /// <param name="headers">Contains list of category headers</param>
        /// <param name="blockCategoryColor">block Category Color</param>
        public Block(string blockID, string blockName, string guranteeType, string blockCategoryName,
                     string blockDescription, DateTime modifyAndCancellableDate, bool captureGuarantee,
                     bool sixPMHoldAvailable, int pageId, List<string> headers,
                     string blockCategoryColor, bool hideARBPrice)
        {
            this.blockID = blockID;
            this.blockName = blockName;
            this.guranteeType = guranteeType;
            this.blockCategoryName = blockCategoryName;
            this.blockDescription = blockDescription;
            this.modifyAndCancellableDate = modifyAndCancellableDate;
            this.captureGuarantee = captureGuarantee;
            this.sixPMHoldAvailable = sixPMHoldAvailable;
            this.pageId = pageId;
            this.headers = headers;
            this.blockCategoryColor = blockCategoryColor;
            this.HideARBPrice = hideARBPrice;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="blockID"></param>
        /// <param name="blockName"></param>
        /// <param name="guranteeType"></param>
        /// <param name="blockCategoryName"></param>
        /// <param name="blockDescription"></param>
        /// <param name="modifyAndCancellableDate"></param>
        /// <param name="captureGuarantee"></param>
        /// <param name="sixPMHoldAvailable"></param>
        /// <param name="headers"></param>
        /// <param name="blockRateToolTip"></param>
        /// <param name="blockCategoryColor"></param>
        public Block(string blockID, string blockName, string guranteeType, string blockCategoryName,
                     string blockDescription, DateTime modifyAndCancellableDate, bool captureGuarantee,
                     bool sixPMHoldAvailable,
                     List<string> headers, string blockCategoryColor, bool hideARBPrice)
            : this(blockID, blockName, guranteeType, blockCategoryName, blockDescription, modifyAndCancellableDate,
                   captureGuarantee, sixPMHoldAvailable, 0, headers, blockCategoryColor, hideARBPrice)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="blockID"></param>
        /// <param name="blockName"></param>
        public Block(string blockID, string blockName)
        {
            this.blockID = blockID;
            this.blockName = blockName;
        }
    }
}