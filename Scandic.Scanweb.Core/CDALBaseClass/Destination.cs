namespace Scandic.Scanweb.Core
{
    /// <summary>
    /// Destination
    /// </summary>
    public abstract class Destination
    {
        /// <summary>
        /// The OperaID for the destination i.e., City / Hotel / Region
        /// </summary>
        protected string operaDestinationId;

        /// <summary>
        /// The name of the destination. 
        /// This will be translated for each languague on the site.
        /// </summary>
        protected string name;

        public string OperaDestinationId
        {
            get { return operaDestinationId; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }
}