using System;
using System.Collections.Generic;
using System.Text;

namespace Scandic.Scanweb.BookingEngine.ExceptionManager
{
    /// <summary>
    /// BaseException
    /// </summary>
    class BaseException : Exception
    {
        #region Private Variables

        private static string ExceptionLogPath = ConfigurationManager.AppSettings["LogPath"].ToString();
        private static string ExceptionLogFile = ConfigurationManager.AppSettings["LogFileName"].ToString();
        private static string ExceptionCodeFile = ConfigurationManager.AppSettings["ExceptionCodeFile"].ToString();

        private LogFileTraceListener exceptionListener;        

        private string errCode = string.Empty;
        private string errMessage = string.Empty;
        private string friendlyMessage = string.Empty;

        public string ErrCode
        {
            get { return errCode; }
            set { errCode = value; }
        }

        public string ErrMessage
        {
            get { return errMessage; }
            set { errMessage = value; }
        }

        public string FriendlyMessage
        {
            get { return friendlyMessage; }
            set { friendlyMessage = value; }
        }

        #endregion Private Variables

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ErrMessage"></param>
        /// <param name="Ex"></param>
        public BaseException(string ErrMessage, Exception Ex) : base(ErrMessage, Ex)
        {
            LogException(Ex);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ErrMessage"></param>
        /// <param name="Ex"></param>
        public BaseException(string ErrMessage) : base(ErrMessage)
        {
            this.ErrMessage = ErrMessage;
            LogException(this);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ErrMessage"></param>
        /// <param name="Ex"></param>
        public BaseException(string ErrCode) : base(ErrMessage)
        {
            errCode = Code;
            errMessage = Message;
            friendlyMessage = GetFriendlyMessage();
            
            string CustomMessage = "ERROR CODE: " + errCode + " ERROR DESC: " + errMessage + " FRIENDLY MSG: " + friendlyMessage;
            Exception Ex = new Exception(CustomMessage);
            LogException(Ex);
        }

        #endregion Constructor
    }
}
