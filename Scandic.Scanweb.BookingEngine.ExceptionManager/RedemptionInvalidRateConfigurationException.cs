namespace Scandic.Scanweb.ExceptionManager
{
    /// <summary>
    /// RedemptionInvalidRateConfigurationException
    /// </summary>
    public class RedemptionInvalidRateConfigurationException : BusinessException
    {
        private const string ERROR_CODE = "SELECTRATE005";

        /// <summary>
        /// Constructor
        /// </summary>
        public RedemptionInvalidRateConfigurationException()
        {
            this.errCode = ERROR_CODE;
            base.SetupException();
        }
    }
}