﻿using System;
namespace Scandic.Scanweb.ExceptionManager
{
    public class ScanWebGenericException : Exception
    {
        public ScanWebGenericException(string message)
            : base(message)
        {
            
        }
    }
}
