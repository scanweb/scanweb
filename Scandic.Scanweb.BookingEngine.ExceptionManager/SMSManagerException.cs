﻿namespace Scandic.Scanweb.ExceptionManager
{
    /// <summary>
    /// Exception class for SMSManager
    /// </summary>
    public class SMSManagerException : System.Exception
    {
        public SMSManagerException(string Message)
            : base(Message)
        {
        }
    }
}