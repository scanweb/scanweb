//  Description					:   OWSException                                          //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.ExceptionManager
{
    /// <summary>
    /// This class contains members to handle OWS exception.
    /// </summary>
    public class OWSException : Exception
    {
        #region Private Variables

        private string errCode = string.Empty;
        private string errMessage = string.Empty;
        private string translatePath = string.Empty;

        /// <summary>
        /// Gets/Sets ErrCode
        /// </summary>
        public string ErrCode
        {
            get { return errCode; }
            set { errCode = value; }
        }

        /// <summary>
        /// Gets/Sets ErrMessage
        /// </summary>
        public string ErrMessage
        {
            get { return errMessage; }
            set { errMessage = value; }
        }

        /// <summary>
        /// Gets/Sets TranslatePath
        /// </summary>
        public string TranslatePath
        {
            get { return translatePath; }
            set { translatePath = value; }
        }

        #endregion Private Variables

        #region Constructor

        /// <summary>
        /// Default Constructor of OWSException class
        /// </summary>
        /// <param name="Message"></param>
        public OWSException()
            : base()
        {

        }
        /// <summary>
        /// Constructor of OWSException class
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="Ex"></param>
        public OWSException(string Message, Exception Ex)
            : base(Message, Ex)
        {
            AppLogger.LogCustomException(Ex, AppConstants.OWS_EXCEPTION, Ex.Message);
        }

        /// <summary>
        /// Constructor of OWSException class
        /// </summary>
        /// <param name="Message"></param>
        public OWSException(string Message)
            : base(Message)
        {
            errMessage = Message;

            ArrayList Parameters = new ArrayList();
            Parameters.Add(AppConstants.OWS_EXCEPTION);
            Parameters.Add(string.Empty);
            Parameters.Add(errMessage);

            translatePath = Utils.GetTranslatePath(Parameters);
            string CustomMessage = " ERROR DESC: " + errMessage;

            AppLogger.LogCustomException(this, AppConstants.OWS_EXCEPTION, CustomMessage);
        }

        /// <summary>
        /// Constructor of OWSException class
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="Code"></param>
        public OWSException(string Message, string Code)
            : base(Message)
        {
            errCode = Code;
            errMessage = Message;

            ArrayList Parameters = new ArrayList();
            Parameters.Add(AppConstants.OWS_EXCEPTION);
            Parameters.Add(errCode);
            Parameters.Add(errMessage);
            translatePath = Utils.GetTranslatePath(Parameters);

            string CustomMessage = "ERROR CODE: " + errCode + " ERROR DESC: " + errMessage;
            AppLogger.LogCustomException(this, AppConstants.OWS_EXCEPTION, CustomMessage);
        }

        /// <summary>
        /// Constructor of OWSException class
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="Code"></param>
        /// <param name="ExceptionData"></param>
        public OWSException(string Message, string Code, Hashtable ExceptionData)
            : base(Message)
        {
            errCode = Code;
            errMessage = Message;

            ArrayList Parameters = new ArrayList();
            Parameters.Add(AppConstants.OWS_EXCEPTION);
            Parameters.Add(errCode);
            Parameters.Add(errMessage);
            translatePath = Utils.GetTranslatePath(Parameters);

            AddDataToException(ExceptionData);
            string CustomMessage = "ERROR CODE: " + errCode + " ERROR DESC: " + errMessage;
            AppLogger.LogCustomException(this, AppConstants.OWS_EXCEPTION, CustomMessage);
        }

        /// <summary>
        /// Constructor of OWSException class
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="Code"></param>
        /// <param name="ExceptionData"></param>
        /// <param name="paramCritical"></param>
        public OWSException(string Message, string Code, Hashtable ExceptionData, bool paramCritical)
            : base(Message)
        {
            errCode = !string.IsNullOrEmpty(Code) ? Code : string.Empty;
            errMessage = !string.IsNullOrEmpty(Message) ? Message : string.Empty;

            ArrayList Parameters = new ArrayList();
            Parameters.Add(AppConstants.OWS_EXCEPTION);
            Parameters.Add(errCode);
            Parameters.Add(errMessage);
            translatePath = Utils.GetTranslatePath(Parameters);

            AddDataToException(ExceptionData);
            string CustomMessage = "ERROR CODE: " + errCode + " ERROR DESC: " + errMessage;

            bool isCritical = paramCritical;
            switch (errCode)
            {
                case OWSExceptionConstants.PROMOTION_CODE_INVALID:
                case OWSExceptionConstants.PROPERTY_NOT_AVAILABLE:
                case OWSExceptionConstants.INVALID_PROPERTY:
                case OWSExceptionConstants.PROPERTY_RESTRICTED:
                case OWSExceptionConstants.PRIOR_STAY:
                case OWSExceptionConstants.INSUFFICIENT_AWARD_POINTS_FOR_RESERVATION:
                case OWSExceptionConstants.ROOM_UNAVAILABLE:
                case OWSExceptionConstants.BOOKING_NOT_FOUND_IGNORE:
                    isCritical = false;
                    break;
            }

            if (isCritical)
            {
                AppLogger.LogCustomException(this, AppConstants.OWS_EXCEPTION, CustomMessage);
            }
            else
            {
                AppLogger.LogInfoMessage("OWS Exception | Stack Trace: " + this.StackTrace + "\n Error Message: " + CustomMessage);
            }
        }

        #endregion Constructor

        #region Private Members

        #region AddDataToException

        private void AddDataToException(Hashtable ExceptionData)
        {
            IDictionaryEnumerator en = ExceptionData.GetEnumerator();
            string key = string.Empty;
            string value = string.Empty;

            while (en.MoveNext())
            {
                key = en.Key.ToString();
                if (en.Value != null)
                    value = en.Value.ToString();
                this.Data.Add(key, value);
            }
        }

        #endregion AddDataToException

        #endregion Private Members
    }
}