using System;
using System.IO;
using System.Text;
using System.Web.Services.Protocols;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.ExceptionManager
{
    /// <summary>
    /// Define a SOAP Extension that traces the SOAP request and SOAP
    /// response for the Web service method the SOAP extension is
    /// applied to.
    /// </summary>
    public class TraceSoapExtension : SoapExtension
    {
        private Stream oldStream;
        private Stream newStream;
        private string filename;

        public override Stream ChainStream(Stream stream)
        {
            oldStream = stream;
            newStream = new MemoryStream();
            return newStream;
        }
 
        /// <summary>
        /// When the SOAP extension is accessed for the first time, the XML Web
        /// service method it is applied to is accessed to store the file
        /// name passed in, using the corresponding SoapExtensionAttribute. 
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
        {
            return ((TraceSoapExtensionAttribute) attribute).Filename;
        }

        /// <summary>
        /// The SOAP extension was configured to run using a configuration file
        /// instead of an attribute applied to a specific Web service
        /// method.
        /// </summary>
        /// <param name="WebServiceType"></param>
        /// <returns></returns>
        public override object GetInitializer(Type WebServiceType)
        {
            return "C:\\" + WebServiceType.FullName + ".log";
        }

        /// <summary>
        /// Receive the file name stored by GetInitializer and store it in a
        /// member variable for this specific instance.
        /// </summary>
        /// <param name="initializer"></param>
        public override void Initialize(object initializer)
        {
            filename = (string) initializer;
        }

        /// <summary>
        /// If the SoapMessageStage is such that the SoapRequest or
        ///  SoapResponse is still in the SOAP format to be sent or received,
        ///  save it out to a file.
        /// </summary>
        /// <param name="message"></param>
        public override void ProcessMessage(SoapMessage message)
        {
            switch (message.Stage)
            {
                case SoapMessageStage.BeforeSerialize:
                    break;
                case SoapMessageStage.AfterSerialize:
                    WriteOutput(message);
                    break;
                case SoapMessageStage.BeforeDeserialize:
                    WriteInput(message);
                    break;
                case SoapMessageStage.AfterDeserialize:
                    break;
                default:
                    throw new ScanWebGenericException("invalid stage");
            }
        }

        /// <summary>
        /// WriteOutput
        /// </summary>
        /// <param name="message"></param>
        public void WriteOutput(SoapMessage message)
        {
            newStream.Position = 0;
            StringBuilder soapString = new StringBuilder();
            soapString.Append("-----");
            soapString.Append((message is SoapServerMessage) ? "SoapResponse" : "SoapRequest");
            soapString.Append(Environment.NewLine);
            TextReader reader = new StreamReader(newStream);
            soapString.Append(reader.ReadToEnd());
            newStream.Position = 0;
            Copy(newStream, oldStream);

            AppLogger.LogOWSInfoMessage(soapString.ToString());
        }

        /// <summary>
        /// WriteInput
        /// </summary>
        /// <param name="message"></param>
        public void WriteInput(SoapMessage message)
        {
            Copy(oldStream, newStream);
            StringBuilder soapString = new StringBuilder();
            soapString.Append("-----");
            soapString.Append((message is SoapServerMessage) ? "SoapRequest" : "SoapResponse");
            soapString.Append(Environment.NewLine);
            newStream.Position = 0;
            TextReader reader = new StreamReader(newStream);
            soapString.Append(reader.ReadToEnd());
            newStream.Position = 0;

            AppLogger.LogOWSInfoMessage(soapString.ToString());
        }

        /// <summary>
        /// Copy
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        private void Copy(Stream from, Stream to)
        {
            TextReader reader = new StreamReader(from);
            TextWriter writer = new StreamWriter(to);
            writer.WriteLine(reader.ReadToEnd());
            writer.Flush();
        }
    }

    /// <summary>
    /// Create a SoapExtensionAttribute for the SOAP Extension that can be
    /// applied to a Web service method.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class TraceSoapExtensionAttribute : SoapExtensionAttribute
    {
        private string filename = "c:\\OWSRequestResponse.log";

        public override Type ExtensionType
        {
            get { return typeof (TraceSoapExtension); }
        }

        public override int Priority { get; set; }

        public string Filename
        {
            get { return filename; }
            set { filename = value; }
        }
    }
}