using System;
using System.Collections;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.ExceptionManager
{
    /// <summary>
    /// BusinessException
    /// </summary>
    public class BusinessException : Exception
    {
        #region Private Variables

        protected string errCode = string.Empty;
        private string translatePath = string.Empty;

        public string ErrCode
        {
            get { return errCode; }
            set { errCode = value; }
        }

        public string TranslatePath
        {
            get { return translatePath; }
            set { translatePath = value; }
        }

        #endregion Private Variables

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ErrCode"></param>
        public BusinessException(string ErrCode) : base(ErrCode)
        {
            this.errCode = ErrCode;

            SetupException();
        }

        /// <summary>
        /// SetupException
        /// </summary>
        protected void SetupException()
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(AppConstants.BUSINESS_EXCEPTION);
            Parameters.Add(errCode);
            Parameters.Add(string.Empty);

            translatePath = Utils.GetTranslatePath(Parameters);
            string CustomMessage = "ERROR CODE: " + this.errCode;

            AppLogger.LogCustomException(this, AppConstants.BUSINESS_EXCEPTION, CustomMessage);
        }

        public BusinessException()
        {
        }

        #endregion Constructor
    }
}