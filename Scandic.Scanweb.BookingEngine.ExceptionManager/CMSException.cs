//  Description					: CMSException                                            //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.ExceptionManager
{
    /// <summary>
    /// This class represents CMSException
    /// </summary>
    public class CMSException : Exception
    {
        private string errCode;
        private string translatePath = string.Empty;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="errCode"></param>
        /// <param name="translatedPath"></param>
        public CMSException(string errCode, string translatedPath)
            : base(errCode)
        {
            this.errCode = errCode;
            SetupException();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="errCode"></param>
        public CMSException(string errCode)
            : this(errCode, string.Empty)
        {
        }

        /// <summary>
        /// Gets TranslatePath
        /// </summary>
        public string TranslatePath
        {
            get { return translatePath; }
        }

        /// <summary>
        /// Gets ErrCode
        /// </summary>
        public string ErrCode
        {
            get { return errCode; }
        }

        private void SetupException()
        {
            ArrayList Parameters = new ArrayList();
            Parameters.Add(AppConstants.CDAL_EXCEPTION);
            Parameters.Add(errCode);
            Parameters.Add(string.Empty);

            translatePath = Utils.GetTranslatePath(Parameters);
            string CustomMessage = "ERROR CODE: " + this.errCode;

            AppLogger.LogCustomException(this, AppConstants.CDAL_EXCEPTION, CustomMessage);
        }
    }
}