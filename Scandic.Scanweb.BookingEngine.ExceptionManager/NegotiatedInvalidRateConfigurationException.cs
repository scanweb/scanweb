namespace Scandic.Scanweb.ExceptionManager
{
    /// <summary>
    /// NegotiatedInvalidRateConfigurationException
    /// </summary>
    public class NegotiatedInvalidRateConfigurationException : BusinessException
    {
        private const string ERROR_CODE = "SELECTRATE002";

        /// <summary>
        /// Constructor
        /// </summary>
        public NegotiatedInvalidRateConfigurationException()
        {
            this.errCode = ERROR_CODE;
            base.SetupException();
        }
    }
}