namespace Scandic.Scanweb.ExceptionManager
{
    public class ContentDataAccessException : System.Exception
    {
        public string ErrorCode { get; set; }
        public ContentDataAccessException(string message) : base(message)
        {
        }
        public ContentDataAccessException(string message,string errorCode)
            : base(message)
        {
            ErrorCode = errorCode;
        }
    }
}