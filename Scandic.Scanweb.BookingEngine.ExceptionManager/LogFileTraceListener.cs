////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:Class is a Trace Listener which writes into a Daily      //
//								 Log file												  //
//----------------------------------------------------------------------------------------//
/// Author						: Ganesh Natarajan										  //
/// Author email id				: gnatarajan@sapient.com        						  //
/// Creation Date				: 24th September 2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Diagnostics;
using System.Text;

namespace Scandic.Scanweb.BookingEngine.ExceptionManager
{
    public class LogFileTraceListener : TextWriterTraceListener
    {

        #region Private Variables
        private static string baseFileName;
        private static string logFileName;
        private static DateTime logDate;
        private static StreamWriter writer;
        #endregion Private Variables

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the LogFileTraceListener class
        /// </summary>
        /// <remarks>
        /// Uses one-parameter constructor
        /// Default file path is the current directory of running process
        /// </remarks>
        public LogFileTraceListener() : this(Environment.CurrentDirectory) { }

        /// <summary>
        /// Initializes a new instance of the LogFileTraceListener class
        /// </summary>
        /// <remarks>
        /// Uses two-parameter constructor
        /// Default file name prefix: �Trace�
        /// </remarks>
        /// <param name="filePath">The path to the folder where log files will be created</param>
        public LogFileTraceListener(string filePath) : this(filePath, "Trace") { }

        /// <summary>
        /// Initializes a new instance of the LogFileTraceListener class
        /// </summary>
        /// <remarks>
        /// Checks for / creates a folder to store log files based on the <paramref name="filePath"/> parameter provided
        /// File names will be prefixed with the <paramref name="fileName"/> parameter provided
        /// Initially creates a log file (Format:  fileName + .yyyy-MM-dd.log) in the folder specified.
        /// </remarks>
        /// <param name="filePath">The path to the folder where log files will be created</param>
        /// <param name="fileName">The base file name used for the log files</param>
        public LogFileTraceListener(string filePath, string fileName)
        {
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            if (!filePath.EndsWith("\\"))
            {
                filePath += "\\";
            }

            baseFileName = filePath + fileName;                        
            CreateNewLogFile();
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Creates a new log file and writer for the current day
        /// </summary>
        private static void CreateNewLogFile()
        {
            logDate = DateTime.Today;
            logFileName = baseFileName + "." + logDate.ToString("yyyy-MM-dd") + ".log";
            writer = new StreamWriter(logFileName, true);            
        }

        /// <summary>
        /// Checks to see whether a new file needs to be created because the day has changed
        /// </summary>
        private static void CheckNextDay()
        {
            if (DateTime.Today > logDate)
            {
                writer.Close();
                CreateNewLogFile();
            }
        }

        /// <summary>
        /// Writes indention whitespace in the output stream, if indented
        /// </summary>
        protected override void WriteIndent()
        {
            if (IndentLevel > 0)
            {
                writer.Write(new String(' ', IndentLevel * IndentSize));
            }
        }

        /// <summary>
        /// Writes a message text to the writer and flushes the output
        /// </summary>
        /// <remarks>
        /// The date will be checked to see if a new file needs to be started
        /// </remarks>
        /// <param name="message">A message to write</param>
        public override void Write(string message)
        {
            CheckNextDay();
            writer.Write(message);
            writer.Flush();
            writer.Close();
        }

        /// <summary>
        /// Writes a message text and category to the writer and flushes the output
        /// </summary>
        /// <remarks>
        /// The date will be checked to see if a new file needs to be started
        /// </remarks>
        /// <param name="message">A message to write</param>
        /// <param name="category">A category name used to prefix the output</param>
        public override void Write(string message, string category)
        {
            CheckNextDay();
            writer.Write(category + ":  " + message);
            writer.Flush();
        }

        /// <summary>
        /// Writes a datetime stamp, indention and message text followed by a
        /// line terminator to the writer and flushes the output
        /// </summary>
        /// <remarks>
        /// The date will be checked to see if a new file needs to be started
        /// </remarks>
        /// <param name="message">A message to write</param>
        public override void WriteLine(string message)
        {
            CheckNextDay();
            writer.Write(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "  ");
            if (NeedIndent)
            {
                WriteIndent();
            }
            writer.WriteLine(message);
            writer.Flush();
        }

        /// <summary>
        /// Writes a datetime stamp, indention, category and message text followed
        /// by a line terminator to the writer and flushes the output
        /// </summary>
        /// <remarks>
        /// The date will be checked to see if a new file needs to be started
        /// </remarks>
        /// <param name="message">A message to write</param>
        /// <param name="category">A category name used to prefix the output</param>
        public override void WriteLine(string message, string category)
        {
            CheckNextDay();
            writer.Write(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "  ");
            if (NeedIndent)
            {
                WriteIndent();
            }
            writer.WriteLine(category + ":  " + message);
            writer.Flush();
        }

        /// <summary>
        /// Deallocates the LogFileTraceListener resources
        /// </summary>
        /// <remarks>
        /// Closes the stream writer used
        /// </remarks>
        ~LogFileTraceListener()
        {
            try
            {
                writer.Close();
            }
            catch
            {
            }
        }

        /// <summary>
        /// Fetches the source Info from the exception raised
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private string GetSourceInfo(Exception ex)
        {
            if (ex == null) return "Default";
            System.Diagnostics.StackTrace oStackTrace = new System.Diagnostics.StackTrace(ex);
            if (oStackTrace.FrameCount > 0)
                return oStackTrace.GetFrame(0).GetMethod().DeclaringType.FullName + ".cs,--> Method Name: " + oStackTrace.GetFrame(0).GetMethod().Name;
            else
                return "Unable to retrieve source info";
        }        
        #endregion Private Methods

        #region Public Methods

        public void LogException(Exception ex, string ExceptionType, string CustomMessage)
        {
            try
            {
                StringBuilder exceptionBuilder = new StringBuilder();

                exceptionBuilder.Append(ExceptionType + Environment.NewLine);
                exceptionBuilder.Append("-------------------" + Environment.NewLine);
                exceptionBuilder.Append("Exception Message : " + ex.Message + Environment.NewLine);
                exceptionBuilder.Append("Custom Message : " + CustomMessage + Environment.NewLine);
                exceptionBuilder.Append("Exception Time : " + DateTime.Now.ToString() + Environment.NewLine);
                exceptionBuilder.Append("Exception Source : " + GetSourceInfo(ex) + Environment.NewLine);
                exceptionBuilder.Append("Exception Data : " + Environment.NewLine);

                Object[] keysArray = new Object[ex.Data.Keys.Count];
                Object[] valuesArray = new Object[ex.Data.Values.Count];
                ex.Data.Keys.CopyTo(keysArray, 0);
                ex.Data.Values.CopyTo(valuesArray, 0);

                for (int i = 0; i < keysArray.Length; i++)
                {
                    exceptionBuilder.Append(keysArray.GetValue(i).ToString() + " : " + valuesArray.GetValue(i).ToString() + Environment.NewLine);
                }
                exceptionBuilder.Append("===================" + Environment.NewLine);
                Write(exceptionBuilder.ToString());

                exceptionBuilder = null;
            }
            catch
            {
            }
        }

        public void LogTrace(string Msg)
        {
            try
            {
                StringBuilder TraceBuilder = new StringBuilder();
                TraceBuilder.Append(Environment.NewLine + DateTime.Now.ToString() + " : " + Msg);
                Write(TraceBuilder.ToString());
                TraceBuilder = null;
            }
            catch
            {
            }
        }

        public void LogSoapMessage(string Msg)
        {
            try
            {
                StringBuilder TraceBuilder = new StringBuilder();
                TraceBuilder.Append(Environment.NewLine + DateTime.Now.ToString() + " : " + Msg);
                Write(TraceBuilder.ToString());
                TraceBuilder = null;
            }
            catch
            {
            }
        }
        #endregion Public Methods

    }
}
