////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  BonusChequeInvalidRateConfigurationException           //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.ExceptionManager
{
    /// <summary>
    /// BonusChequeInvalidRateConfigurationException
    /// </summary>
    public class BonusChequeInvalidRateConfigurationException : BusinessException
    {
        private const string ERROR_CODE = "SELECTRATE003";

        /// <summary>
        /// Contructor of BonusChequeInvalidRateConfigurationException
        /// </summary>
        public BonusChequeInvalidRateConfigurationException()
        {
            this.errCode = ERROR_CODE;
            base.SetupException();
        }
    }
}