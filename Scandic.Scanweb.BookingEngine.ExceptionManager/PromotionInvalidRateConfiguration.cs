//  Description					: PromotionInvalidRateConfiguration                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.ExceptionManager
{
    /// <summary>
    /// This class represents promotion invalid rate configuration manager.
    /// </summary>
    public class PromotionInvalidRateConfiguration : BusinessException
    {
        private const string ERROR_CODE = "SELECTRATE001";

        /// <summary>
        /// Constructor of PromotionInvalidRateConfiguration class.
        /// </summary>
        public PromotionInvalidRateConfiguration()
        {
            this.errCode = ERROR_CODE;
            base.SetupException();
        }
    }
}