//  Description					: Utils                                                   //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.IO;
using System.Reflection;
using System.Xml;
using Scandic.Scanweb.Core;
using System.Configuration;
using System;

namespace Scandic.Scanweb.ExceptionManager
{
    /// <summary>
    /// Contains members of Utils
    /// </summary>
    public sealed class Utils
    {
        #region Declaration
        private static string ExceptionCodeFile = string.Empty;
        #endregion Declaration

        #region GetTranslatePath
        /// <summary>
        /// Gets translated path.
        /// </summary>
        /// <param name="Parameters"></param>
        /// <returns></returns>
        public static string GetTranslatePath(ArrayList Parameters)
        {
            string ExceptionType = string.Empty;
            string ErrCode = string.Empty;
            string InternErrCode = string.Empty;

            ExceptionType = Parameters[0].ToString();
            ErrCode = Parameters[1].ToString().ToLower();
            if (ErrCode != string.Empty && ErrCode != null)
                ErrCode = ErrCode.Trim(' ').ToLower();

            InternErrCode = Parameters[2].ToString().Trim().ToLower();

            string Code = string.Empty;
            string ErrCodePrefix = string.Empty;
            string ErrCodeSuffix = string.Empty;
            string InternalErrCode = string.Empty;
            string ConvertedDesc = string.Empty;
            string TranslatePath = string.Empty;

            string NodeToSearch = string.Empty;
            XmlNode ExceptionTypeNode, Node;

            //ExceptionCodeFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            //ExceptionCodeFile = ExceptionCodeFile.Substring(0, ExceptionCodeFile.LastIndexOf("\\"));
            //ExceptionCodeFile += "\\ExceptionCodes.xml";
            ExceptionCodeFile = Convert.ToString(ConfigurationManager.AppSettings[AppConstants.ExceptionCodesFilePath]);

            XmlTextReader Reader = new XmlTextReader(ExceptionCodeFile);
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Reader);

            switch (ExceptionType)
            {
                case AppConstants.BUSINESS_EXCEPTION:
                    NodeToSearch = "business";
                    break;
                case AppConstants.OWS_EXCEPTION:
                    NodeToSearch = "ows";
                    break;
                case AppConstants.CDAL_EXCEPTION:
                    NodeToSearch = "cdal";
                    break;
            }

            ExceptionTypeNode = XmlDoc.DocumentElement.SelectSingleNode(NodeToSearch);
            foreach (XmlNode ExceptionNode in ExceptionTypeNode.ChildNodes)
            {
                Node = ExceptionNode.SelectSingleNode("errorcode");
                Code = GetStringFromNode(Node);
                Code = Code.ToLower();

                Node = ExceptionNode.SelectSingleNode("errorcodeprefix");
                ErrCodePrefix = GetStringFromNode(Node);
                ErrCodePrefix = ErrCodePrefix.ToLower();

                Node = ExceptionNode.SelectSingleNode("errorcodesuffix");
                ErrCodeSuffix = GetStringFromNode(Node);
                ErrCodeSuffix = ErrCodeSuffix.ToLower();

                Node = ExceptionNode.SelectSingleNode("internalerrorcode");
                InternalErrCode = GetStringFromNode(Node);
                InternalErrCode = InternalErrCode.ToLower();

                Node = ExceptionNode.SelectSingleNode("converteddescription");
                ConvertedDesc = GetStringFromNode(Node);
                ConvertedDesc = ConvertedDesc.ToLower();

                if (NodeToSearch == "business")
                {
                    if (ErrCode.Equals(Code))
                    {
                        Node = ExceptionNode.SelectSingleNode("translatepath");
                        TranslatePath = GetStringFromNode(Node);
                        break;
                    }
                }
                else if (NodeToSearch == "ows")
                {
                    if (ErrCode != string.Empty)
                    {
                        if (InternErrCode != string.Empty)
                        {
                            if ((ErrCode.Equals(ErrCodePrefix + ErrCodeSuffix) || ErrCode.Equals(ErrCodeSuffix)) &&
                                (InternErrCode.Equals(InternalErrCode) || InternErrCode.Equals(ConvertedDesc)))
                            {
                                {
                                    Node = ExceptionNode.SelectSingleNode("translatepath");
                                    TranslatePath = GetStringFromNode(Node);
                                    break;
                                }
                            }
                        }
                   else
                        {
                            if (ErrCode.Equals(ErrCodePrefix + ErrCodeSuffix) || ErrCode.Equals(ErrCodeSuffix))
                            {
                                Node = ExceptionNode.SelectSingleNode("translatepath");
                                TranslatePath = GetStringFromNode(Node);
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (InternErrCode != string.Empty)
                        {
                            if (InternErrCode.Equals(InternalErrCode) || InternErrCode.Equals(ConvertedDesc))
                            {
                                {
                                    Node = ExceptionNode.SelectSingleNode("translatepath");
                                    TranslatePath = GetStringFromNode(Node);
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (NodeToSearch == "cdal")
                {
                    if (ErrCode.Equals(Code))
                    {
                        Node = ExceptionNode.SelectSingleNode("translatepath");
                        TranslatePath = GetStringFromNode(Node);
                        break;
                    }
                }
            }

            if (TranslatePath == string.Empty)
            {
                TranslatePath = AppConstants.SITE_WIDE_ERROR;
            }

            return TranslatePath;
        }

        #endregion GetTranslatePath

        #region Private Methods

        private static string GetStringFromNode(XmlNode node)
        {
            if ((node != null) && (node.InnerText != null))
            {
                if (node.InnerText.Length > 0)
                {
                    return node.InnerText;
                }
            }
            return string.Empty;
        }

        #endregion Private Methods
    }
}