//  Description					:   VoucherInvalidRateConfigurationException              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.ExceptionManager
{
    /// <summary>
    /// This class represents voucher invalid rate configuration exception.
    /// </summary>
    public class VoucherInvalidRateConfigurationException : BusinessException
    {
        private const string ERROR_CODE = "SELECTRATE004";

        /// <summary>
        /// Constructor
        /// </summary>
        public VoucherInvalidRateConfigurationException()
        {
            this.errCode = ERROR_CODE;
            base.SetupException();
        }
    }
}