//  Description					: RedeemptionPointsEntity                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// The class abstracting the points and award type to do the Redemption booking
    /// </summary>
    public class RedeemptionPointsEntity
    {
        #region Variables

        /// <summary>
        /// The Award type as returned for Redemption booking
        /// </summary>
        private string awardType;

        /// <summary>
        /// Number points required to do the booking per night per room
        /// </summary>
        private double pointsRequired;

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pointsRequired"></param>
        /// <param name="awardType"></param>
        public RedeemptionPointsEntity(double pointsRequired, string awardType)
        {
            this.awardType = awardType;
            this.pointsRequired = pointsRequired;
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets AwardType
        /// </summary>
        public string AwardType
        {
            get { return awardType; }
        }

        /// <summary>
        /// Gets PointsRequired
        /// </summary>
        public double PointsRequired
        {
            get { return pointsRequired; }
        }

        #endregion

        /// <summary>
        /// Equals
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            RedeemptionPointsEntity redemptionEntity = obj as RedeemptionPointsEntity;
            return (redemptionEntity != null && this.pointsRequired == redemptionEntity.PointsRequired);
        }
    }
}