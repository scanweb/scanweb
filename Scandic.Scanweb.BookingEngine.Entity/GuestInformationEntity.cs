namespace Scandic.Scanweb.Entity
{
    public enum BookingFlow
    {
        Booking,
        ModifyBooking
    };

    /// <summary>
    /// Entity containing the guest information 
    /// for reservation. 
    /// </summary>    
    public class GuestInformationEntity
    {
        /// <summary>
        /// The title of the guest
        /// </summary>
        private string title;

        /// <summary>
        /// The Gender of the guest
        /// </summary>
        private string gender;

        /// <summary>
        /// First Name
        /// </summary>
        private string firstName;

        /// <summary>
        /// Last Name
        /// </summary>
        private string lastName;

        /// <summary>
        /// Company name
        /// </summary>
        private string companyName;

        /// <summary>
        /// Address Id 
        /// </summary>
        private string addressID;

        /// <summary>
        /// Address Line 1
        /// </summary>
        private string addressLine1;

        /// <summary>
        /// Address Line 2
        /// </summary>
        private string addressLine2;

        /// <summary>
        /// City
        /// </summary>
        private string city;

        /// <summary>
        /// Postal code
        /// </summary>
        private string postCode;

        /// <summary>
        /// Country should be the value in Opera
        /// </summary>
        private string country;

        /// <summary>
        /// Stores the Email Details Entity
        /// </summary>
        private EmailDetailsEntity emailDetails;

        /// <summary>
        /// Stores the Non business Email Details Entity
        /// </summary>
        private EmailDetailsEntity nonBusinessEmailDetails;

        /// <summary>
        /// Stores the IsSMSconfirmation Checked
        /// </summary>
        private bool isSMSChecked;

        /// <summary>
        /// Guest Loyalty Account number
        /// </summary>
        private string guestAccountNumber;

        /// <summary>
        /// Gurantee information provided by user for reservation
        /// </summary>
        private GuranteeInformationEntity guranteeInformation;

        /// <summary>
        /// The special request chose by user during the booking
        /// </summary>
        private SpecialRequestEntity[] specialRequests;

        /// <summary>
        /// ChildrensDetailsEntity member
        /// </summary>
        private ChildrensDetailsEntity childrensDetails;

        /// <summary>
        /// To be sent during the booking process for redemption booking.
        /// </summary>
        private long membershipID;

        /// <summary>
        /// Holds the name ID of the profile attached with the reservation
        /// </summary>
        private string nameID;

        /// <summary>
        /// Membership Opera ID
        /// </summary>
        private long membershipOperaId;

        /// <summary>
        /// Home Phone 
        /// </summary>
        private PhoneDetailsEntity landline;

        /// <summary>
        /// Mobile Phone
        /// </summary>
        private PhoneDetailsEntity mobile;

        /// <summary>
        /// Language Preference selected by the user
        /// </summary>
        private string preferredLanguage;

        /// <summary>
        /// Address type either HOME or BUSINESS
        /// </summary>
        private string addressType;

        /// <summary>
        /// Leg Number of the Booking which this Guest is a part.
        /// </summary>
        private string legNumber;

        /// <summary>
        /// Reservation Number of the Booking which this GuestInformation is a part
        /// </summary>
        private string reservationNumber;

        /// <summary>
        /// RK: Indicates if the FGP Number is valid or not. This is based on GuestAccountNumber
        /// </summary>
        private bool isValidFGPNumber = true;

        /// <summary>
        /// Clone the GuestInformationEntity
        /// </summary>
        /// <returns>GuestInformationEntity</returns>
        public GuestInformationEntity Clone()
        {
            GuestInformationEntity cloneEntity = new GuestInformationEntity();

            cloneEntity.title = this.title;
            cloneEntity.gender = this.gender;
            cloneEntity.firstName = this.firstName;
            cloneEntity.lastName = this.lastName;
            cloneEntity.companyName = this.companyName;
            cloneEntity.addressID = this.addressID;
            cloneEntity.addressLine1 = this.addressLine1;
            cloneEntity.addressLine2 = this.addressLine2;
            cloneEntity.city = this.city;
            cloneEntity.postCode = this.postCode;
            cloneEntity.country = this.country;
            cloneEntity.preferredLanguage = this.preferredLanguage;
            cloneEntity.addressType = this.addressType;
            cloneEntity.legNumber = this.legNumber;
            cloneEntity.reservationNumber = this.reservationNumber;
            cloneEntity.isSMSChecked = this.isSMSChecked;
            cloneEntity.BedTypePreference = this.BedTypePreference;
            cloneEntity.BedTypePreferenceCode = this.BedTypePreferenceCode;
            if (this.emailDetails != null)
            {
                EmailDetailsEntity cloneEmailEntity = new EmailDetailsEntity();
                cloneEmailEntity.EmailID = this.emailDetails.EmailID;
                cloneEmailEntity.EmailOperaID = this.emailDetails.EmailOperaID;

                cloneEntity.emailDetails = cloneEmailEntity;
            }
            if (this.nonBusinessEmailDetails != null)
            {
                EmailDetailsEntity cloneNonBusinessEmailEntity = new EmailDetailsEntity();
                cloneNonBusinessEmailEntity.EmailID = this.nonBusinessEmailDetails.EmailID;
                cloneNonBusinessEmailEntity.EmailOperaID = this.nonBusinessEmailDetails.EmailOperaID;

                cloneEntity.nonBusinessEmailDetails = cloneNonBusinessEmailEntity;
            }

            cloneEntity.guestAccountNumber = this.guestAccountNumber;
            if (this.guranteeInformation != null)
            {
                cloneEntity.guranteeInformation = new GuranteeInformationEntity();
                cloneEntity.guranteeInformation.GuranteeType = this.GuranteeInformation.GuranteeType;
                cloneEntity.GuranteeInformation.ChannelGuranteeCode = this.GuranteeInformation.ChannelGuranteeCode;
                cloneEntity.GuranteeInformation.CreditCardOperaId = this.GuranteeInformation.CreditCardOperaId;
                CreditCardEntity newCreditCard = null;
                CreditCardEntity orginalCreditCard = this.GuranteeInformation.CreditCard;
                if (orginalCreditCard != null)
                {
                    if (orginalCreditCard.ExpiryDate != null)
                    {
                        ExpiryDateEntity newExpiryDate = new ExpiryDateEntity(orginalCreditCard.ExpiryDate.Month,
                                                                              orginalCreditCard.ExpiryDate.Year);

                        newCreditCard = new CreditCardEntity(orginalCreditCard.NameOnCard, orginalCreditCard.CardType,
                                                             orginalCreditCard.CardNumber, newExpiryDate);
                    }
                    cloneEntity.guranteeInformation.CreditCard = newCreditCard;
                }
            }

            if (this.landline != null)
            {
                PhoneDetailsEntity newLandLine = new PhoneDetailsEntity();
                newLandLine.Number = this.landline.Number;
                newLandLine.OperaId = this.landline.OperaId;
                newLandLine.PhoneType = this.landline.PhoneType;
                cloneEntity.landline = newLandLine;
            }
            if (this.mobile != null)
            {
                PhoneDetailsEntity newMobile = new PhoneDetailsEntity();
                newMobile.Number = this.mobile.Number;
                newMobile.OperaId = this.mobile.OperaId;
                newMobile.PhoneType = this.mobile.PhoneType;
                cloneEntity.mobile = newMobile;
            }

            if (this.specialRequests != null)
            {
                int totalRequest = this.specialRequests.Length;
                SpecialRequestEntity[] cloneSpecialRequestlist = new SpecialRequestEntity[totalRequest];
                for (int requestCount = 0; requestCount < totalRequest; requestCount++)
                {
                    cloneSpecialRequestlist[requestCount] = new SpecialRequestEntity();
                    cloneSpecialRequestlist[requestCount].RequestType =
                        this.specialRequests[requestCount].RequestType;
                    cloneSpecialRequestlist[requestCount].RequestValue =
                        this.specialRequests[requestCount].RequestValue;
                }
                cloneEntity.specialRequests = cloneSpecialRequestlist;
            }

            if (this.childrensDetails != null)
            {
                cloneEntity.childrensDetails = new ChildrensDetailsEntity();
                cloneEntity.childrensDetails.AdultsPerRoom = this.childrensDetails.AdultsPerRoom;
                cloneEntity.childrensDetails.ChildrensDetailsInText = this.childrensDetails.ChildrensDetailsInText;
                if (this.childrensDetails.ListChildren != null)
                {
                    if (this.childrensDetails.ListChildren.Count > 0)
                    {
                        foreach (ChildEntity child in this.childrensDetails.ListChildren)
                        {
                            ChildEntity cloneChild = new ChildEntity();
                            cloneChild.Age = child.Age;
                            cloneChild.ChildAccommodationType = child.ChildAccommodationType;
                            cloneEntity.childrensDetails.ListChildren.Add(cloneChild);
                        }
                    }
                }
            }

            cloneEntity.membershipID = this.membershipID;
            cloneEntity.nameID = this.nameID;
            return cloneEntity;
        }

        public PhoneDetailsEntity Mobile
        {
            get { return mobile; }
            set { mobile = value; }
        }


        public PhoneDetailsEntity Landline
        {
            get { return landline; }
            set { landline = value; }
        }

        public string NameID
        {
            get { return nameID; }
            set { nameID = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string Gender
        {
            get { return gender; }
            set { gender = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string CompanyName
        {
            get { return companyName; }
            set { companyName = value; }
        }

        public string AddressID
        {
            get { return addressID; }
            set { addressID = value; }
        }

        public string AddressLine1
        {
            get { return addressLine1; }
            set { addressLine1 = value; }
        }

        public string AddressLine2
        {
            get { return addressLine2; }
            set { addressLine2 = value; }
        }

        public string LegNumber
        {
            get { return legNumber; }
            set { legNumber = value; }
        }

        public string ReservationNumber
        {
            get { return reservationNumber; }
            set { reservationNumber = value; }
        }


        public string D_Number { get; set; }

        /// <summary>
        /// Indicates if the DNumber is valid or not
        /// This needs to be set while booking and retrieving booking if the DNumber has been saved in Opera
        /// </summary>
        public bool IsValidDNumber { get; set; }

        /// <summary>
        /// Indicates if the FGPNumber is valid or not
        /// This needs to be set when booking as if the FGP is invalid, we send the number
        /// in the comments node
        /// </summary>
        public bool IsValidFGPNumber
        {
            get { return isValidFGPNumber; }
            set { isValidFGPNumber = value; }
        }

        /// <summary>
        /// Indicates if the FGPNumber is valid against alternate lastname
        /// </summary>
        public bool IsCreatebookingRequestWithNativeName { get; set; }

        /// <summary>
        /// Gets/Sets alternate first name from users profile
        /// </summary>
        public string NativeFirstName { get; set; }

        /// <summary>
        /// Gets/Sets alternate last name from users profile
        /// </summary>
        public string NativeLastName { get; set; }

        /// <summary>
        /// Added new property to indicate if the user has newly enrolled
        /// </summary>
        public bool IsNewlyEnrolled { get; set; }

        /// <summary>
        /// Returns the address line 1 and address line2 as a string array
        /// if one of the element is null then it is set as empty string else
        /// both the address lines are returned as string array.
        /// </summary>
        public string[] AddressLines
        {
            get
            {
                string[] addressLines;
                if (addressLine1 != null && addressLine2 != null)
                {
                    addressLines = new string[2];
                    addressLines[0] = addressLine1;
                    addressLines[1] = addressLine2;
                }
                else if (addressLine1 != null)
                {
                    addressLines = new string[1];
                    addressLines[0] = addressLine1;
                }
                else if (addressLine2 != null)
                {
                    addressLines = new string[2];
                    addressLines[0] = string.Empty;
                    addressLines[1] = addressLine2;
                }
                else
                {
                    addressLines = null;
                }
                return addressLines;
            }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string PostCode
        {
            get { return postCode; }
            set { postCode = value; }
        }

        public string Country
        {
            get { return country; }
            set { country = value; }
        }

        public EmailDetailsEntity EmailDetails
        {
            get { return emailDetails; }
            set { emailDetails = value; }
        }

        public EmailDetailsEntity NonBusinessEmailDetails
        {
            get { return nonBusinessEmailDetails; }
            set { nonBusinessEmailDetails = value; }
        }

        public bool IsSMSChecked
        {
            get { return isSMSChecked; }
            set { isSMSChecked = value; }
        }

        public string GuestAccountNumber
        {
            get { return guestAccountNumber; }
            set { guestAccountNumber = value; }
        }

        public GuranteeInformationEntity GuranteeInformation
        {
            get { return guranteeInformation; }
            set { guranteeInformation = value; }
        }

        /// <summary>
        /// The bed type preference chosed by user.
        /// This value will derive the room type to be sent to the
        /// Opera during the booking
        /// </summary>
        public string BedTypePreference { get; set; }

        public string BedTypePreferenceCode { get; set; }

        public string AdditionalRequest { get; set; }

        public SpecialRequestEntity[] SpecialRequests
        {
            get { return specialRequests; }
            set { specialRequests = value; }
        }

        public UserPreferenceEntity[] UserPreferences { get; set; }

        /// <summary>
        /// Gets or sets childrensDetails
        /// </summary>
        public ChildrensDetailsEntity ChildrensDetails
        {
            get { return childrensDetails; }
            set { childrensDetails = value; }
        }

        public long MembershipID
        {
            get { return membershipID; }
            set { membershipID = value; }
        }

        public long MembershipOperaId
        {
            get { return membershipOperaId; }
            set { membershipID = value; }
        }

        public string PreferredLanguage
        {
            get { return preferredLanguage; }
            set { preferredLanguage = value; }
        }

        public string AddressType
        {
            get { return addressType; }
            set { addressType = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is reward night gift.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is reward night gift; otherwise, <c>false</c>.
        /// </value>
        public bool IsRewardNightGift { get; set; }
    }

    /// <summary>
    /// GuranteeInformationEntity
    /// </summary>
    public class GuranteeInformationEntity
    {
        public GuranteeType GuranteeType { get; set; }


        public string ChannelGuranteeCode { get; set; }

        public CreditCardEntity CreditCard { get; set; }

        public long CreditCardOperaId { get; set; }
    }

    /// <summary>
    /// SpecialRequestEntity
    /// </summary>
    public class SpecialRequestEntity
    {
        public string RequestType { get; set; }

        public string RequestValue { get; set; }
    }

    /// <summary>
    /// CommentsEntity
    /// </summary>
    public class CommentsEntity
    {
        public string Comment { get; set; }

        public long CommentOperaId { get; set; }
    }


    public enum GuranteeType
    {
        CREDITCARD,
        HOLD,
        PREPAID,
        REDEMPTION,
        PREPAIDINPROGRESS,
        PREPAIDSUCCESS,
        PREPAIDFAILURE,
        MAKEPAYMENTFAILURE
    }
}