﻿using System;
using System.Collections.Generic;

namespace Scandic.Scanweb.Entity
{
    [Serializable]
    public class CurrentBooking
    {
        public string HotelName { get; set; }

        public string City { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public string ConfirmationId { get; set; }

        public string DateRange()
        {
            return FromDate.ToString("dd.MM.yyyy") + " - " + ToDate.ToString("dd.MM.yyyy");
        }
    }

    public class CurrentBookingComparer : IComparer<CurrentBooking>
    {
        public int Compare(CurrentBooking first, CurrentBooking second)
        {
            return first.FromDate.CompareTo(second.FromDate);
        }
    }
}