//  Description					: DateOfBirthEntity                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// The class capturing the Date of Birth fields
    /// </summary>
    public class DateOfBirthEntity
    {
        /// <summary>
        /// The day
        /// </summary>
        private int day;

        /// <summary>
        /// The Month
        /// </summary>
        private int month;

        /// <summary>
        /// The Year
        /// </summary>
        private int year;

        /// <summary>
        /// The DateTime equivalent of the day, month and year. 
        /// </summary>
        private DateTime date;

        /// <summary>
        /// The consturtor taking day, month, year and creating the derived field "date"
        /// </summary>
        /// <param name="day"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        public DateOfBirthEntity(int day, int month, int year)
        {
            this.day = day;
            this.month = month;
            this.year = year;
            this.date = new DateTime(year, month, day);
        }

        /// <summary>
        /// The consturtor taking the DateTime object and creating the derived fields
        /// day, month and year.
        /// </summary>
        /// <param name="date"></param>
        public DateOfBirthEntity(DateTime date)
        {
            this.date = date;
            this.day = date.Day;
            this.month = date.Month;
            this.year = date.Year;
        }

        /// <summary>
        /// Gets 
        /// </summary>
        public int Day
        {
            get { return day; }
        }


        /// <summary>
        /// Gets Month
        /// </summary>
        public int Month
        {
            get { return month; }
        }


        /// <summary>
        /// Gets Year
        /// </summary>
        public int Year
        {
            get { return year; }
        }


        /// <summary>
        /// Gets Date
        /// </summary>
        public DateTime Date
        {
            get { return date; }
        }


        /// <summary>
        /// Gets ToString
        /// </summary>
        public override string ToString()
        {
            return date.ToShortDateString();
        }
    }
}