//  Description					: MembershipEntity                                        //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.ObjectModel;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// This class represents membership entity.
    /// </summary>
    public class MembershipEntity
    {
        /// <summary>
        /// Gets/Sets CurrentPoints
        /// </summary>
        public double CurrentPoints { get; set; }

        /// <summary>
        /// Gets/Sets EffectiveDate
        /// </summary>
        public string EffectiveDate { get; set; }

        /// <summary>
        /// Gets/Sets ExpirationDate
        /// </summary>
        public string ExpirationDate { get; set; }

        /// <summary>
        /// Gets/Sets MembershipLevel
        /// </summary>
        public string MembershipLevel { get; set; }

        /// <summary>
        /// Gets/Sets MembershipNumber
        /// </summary>
        public string MembershipNumber { get; set; }

        /// <summary>
        /// Gets/Sets MembershipType
        /// </summary>
        public string MembershipType { get; set; }

        /// <summary>
        /// Gets/Sets OperaId
        /// </summary>
        public string OperaId { get; set; }

        /// <summary>
        /// Gets/Sets Status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Default contructor of MembershipEntity
        /// </summary>
        public MembershipEntity()
        {
        }

        /// <summary>
        /// ExpiryPointCollection
        /// </summary>
        public Collection<ExpiryPointEntity> ExpiryPointCollection = new Collection<ExpiryPointEntity>();
    }
}