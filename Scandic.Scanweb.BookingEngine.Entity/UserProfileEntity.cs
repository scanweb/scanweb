using System.Collections.Generic;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// Entity containing the Loyalty Member Profile
    /// </summary>
    public class UserProfileEntity
    {
        public List<MembershipEntity> GuestCardList { get; set; }
        
        /// <summary>
        /// The UserGender enumeration containing
        /// the Gender of the Loyalty member
        /// </summary>
        private UserGender gender;


        /// <summary>
        /// Gets or sets childrensDetails
        /// </summary>
        public ChildrensDetailsEntity ChildrensDetails { get; set; }

        /// <summary>
        /// Gets or Sets Title
        /// </summary>
        public string NameTitle { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        //Added as part of SCANAM-537
        public string NativeFirstName { get; set; }

        public string NativeLastName { get; set; }

        public UserGender Gender
        {
            get { return gender; }
            set { gender = value; }
        }

        /// <summary>
        /// SetGender
        /// </summary>
        /// <param name="gender"></param>
        public void SetGender(string gender)
        {
            if (gender == "M")
                this.gender = UserGender.MALE;
            else if (gender == "F")
                this.gender = UserGender.FEMALE;
        }

        public DateOfBirthEntity DateOfBirth { get; set; }

        public long EmailOperaId { get; set; }

        public string EmailID { get; set; }
        public long NonBusinessEmailOperaId { get; set; }
        public string NonBusinessEmailID { get; set; }

        public long HomePhoneOperaID { get; set; }

        public long MobilePhoneOperaID { get; set; }

        public long FaxOperaID { get; set; }

        public string HomePhone { get; set; }

        public string MobilePhone { get; set; }

        public string Fax { get; set; }

        public long AddressOperaID { get; set; }

        public string AddressType { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string PostCode { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public long CreditCardOperaId { get; set; }

        public CreditCardEntity CreditCard { get; set; }

        
        public List<PartnershipProgram> PartnerProgramList { get; set; }

        public UserProfileType ProfileType { get; set; }

        public System.Collections.Hashtable Preferences { get; set; }

        public string Password { get; set; }

        public string RetypePassword { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public UserInterestsEntity[] Interests { get; set; }

        public UserPreferenceEntity[] UserPreference { get; set; }

        public bool ScandicEmail { get; set; }

        public bool ScandicEMailPrev { get; set; }

        public bool ThridPartyEmail { get; set; }

        public string MembershipId { get; set; }

        public string PreferredLanguage { get; set; }
    }

    ///<summary>
    /// User Interests
    /// </summary>

    public class UserInterestsEntity
    {

        public string RequestType { get; set; }
        public string  RequestValue { get; set; }
    
    }

    /// <summary>
    /// UserPreferenceEntity
    /// </summary>
    public class UserPreferenceEntity
    {
        public string RequestType { get; set; }

        public string RequestValue { get; set; }
    }


    //<summary>
    //User Interests Constants 
    //Storing dummy values in the constants
    //Added By Vaibhav: Merch 4:Itr 2
    // </summary>
    public class UserInterestsConstants
    {
        
        public const string INTERESTS = "INTERESTS";
        public const string FAMILY = "BQT";
        public const string SPORTSEVENT = "CON";
        public const string FOODANDBEVERAGE = "FAB";
        public const string THEATREANDCONCERTS = "CRW";
        public const string CITYBREAK = "SPM";

    }

    /// <summary>
    /// UserPreferenceConstants
    /// </summary>
    public class UserPreferenceConstants
    {
        public const string SPECIALREQUEST = "SPECIALS";
        public const string LOWERFLOOR = "LF";
        public const string HIGHFLOOR = "HF";
        public const string AWAYFROMELEVATOR = "AE";
        public const string NEARELEVATOR = "NE";
        public const string NOSMOKING = "NS";
        public const string SMOKING = "SM";
        public const string ACCESSIBLEROOM = "CR";
        public const string ALLERGYROOM = "AL";
        public const string EARLYCHECKIN = "EA";
        public const string LATECHECKOUT = "LD";
        public const string NEWSLETTERPERFERENCE = "NLR"; 
        public const string PETFRIENDLYROOM = "PFR"; 
    }

    /// <summary>
    /// Enum to indicate the Gender Type
    /// </summary>
    public enum UserGender
    {
        UNKNOWN,
        MALE,
        FEMALE
    };

    /// <summary>
    /// Enum to indicate from which flow the userprofile entity is created.
    /// </summary>
    public enum UserProfileType
    {
        UPDATE_PROFILE,
        BOOKING_DETAILS
    }
}