using System;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// The class capturing the expiry date of the Credit card
    /// </summary>
    public class ExpiryDateEntity
    {
        /// <summary>
        /// The month of expiry of the credit card
        /// </summary>
        private int month;

        /// <summary>
        /// The year of expiry of the credit card
        /// </summary>
        private int year;

        /// <summary>
        /// The DateTime equivalent of the "1", month, year
        /// This is a derived field of the above parameter with
        /// the day set to as "1". The value of this property is set
        /// in the constructor.
        /// </summary>
        private DateTime date;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        public ExpiryDateEntity(int month, int year)
        {
            this.month = month;
            this.year = year;
            this.date = new DateTime(year, month, 1).AddMonths(1).AddDays(-1);;
        }

        public int Month
        {
            get { return month; }
        }

        public int Year
        {
            get { return year; }
        }

        public DateTime ToDateTime()
        {
            return date;
        }
    }
}