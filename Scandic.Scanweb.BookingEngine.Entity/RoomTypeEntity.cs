//  Description					: RoomTypeEntity                                          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// This entity encaptulates Room Type
    /// </summary>
    public class RoomTypeEntity
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="roomTypeCode"></param>
        public RoomTypeEntity(string roomTypeCode)
        {
            this.code = roomTypeCode;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="roomTypeCode"></param>
        /// <param name="description"></param>
        /// <param name="sortOrder"></param>
        public RoomTypeEntity(string roomTypeCode, string description, int sortOrder)
        {
            this.code = roomTypeCode;
            this.Description = description;
            this.SortOrder = sortOrder;
        }

        #endregion

        #region Variables

        /// <summary>
        /// The room type code
        /// </summary>
        private string code;

        #endregion

        #region Properties
        /// <summary>
        /// Gets Code
        /// </summary>
        public string Code
        {
            get { return code; }
        }

        /// <summary>
        /// Gets/Sets Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets/Sets order
        /// </summary>
        public int SortOrder { get; set; }

        #endregion
    }
}