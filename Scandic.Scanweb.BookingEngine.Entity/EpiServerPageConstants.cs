////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  EpiServerPageConstants                                 //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// This class <see cref="EpiServerPageConstants"/> holds all EpiServer Page constants.
    /// </summary>
    public class EpiServerPageConstants
    {
        public const string HOME_PAGE = "ReservationFindDestinationPage";
        public const string SELECT_HOTEL_PAGE = "ReservationSelectHotelPage";
        public const string SELECT_RATE_PAGE = "ReservationSelectRatePage";

        /// <summary>
        /// This string will identify the page type of the
        /// children's details page.
        /// </summary>
        public const string CHILDRENS_DETAILS_PAGE = "ReservationChildrensDetailsPage";

        public const string MODIFY_CHILDREN_DETAILS_PAGE = "ModifyReservationChildrenDetailsPage";
        public const string BOOKING_DETAILS_PAGE = "ReservationBookingDetailsPage";
        public const string BOOKING_CONFIRMATION_PAGE = "ReservationConfirmationPage";
        public const string SEARCH_USING_MAP_PAGE = "HotelOverviewPage";
        public const string BEST_RATES_PAGE = "BestRatesPage";
        public const string AJAX_SEARCH_PAGE = "ReservationAjaxSearchPage";
        public const string FAMILY_BOOKINGS_PAGE = "FamilyBookingsPage";
        public const string ABOUT_LOCAL_CURRENCIES_PAGE = "LocalCurrencyRatesPage";
        public const string RESERVATION_TERMS_AND_CONDITIONS_PAGE = "TermsAndConditionsPage";
        public const string NEED_HELP_PAGE = "BookingHelpPage";
        public const string MODIFY_CANCEL_BOOKING_SEARCH = "ModifyOrCancelBookingPage";
        public const string MODIFY_CANCEL_CHANGE_DATES = "ModifyOrCancelBookingDatesPage";
        public const string MODIFY_CANCEL_SELECT_RATE = "ModifySelectRate";
        public const string MODIFY_CANCEL_BOOKING_DETAILS = "ModifyBookingPage";
        public const string MODIFY_CANCEL_BOOKING_CONFIRMATION = "ModifyConfirmationPage";
        public const string CONFIRM_CANCELLATION = "ConfirmCancellationPage";
        public const string CANCELLED_BOOKING = "CancelledBookingPage";
        public const string FREQUENT_GUEST_PROGRAMME = "FrequentGuestProgrammePage";
        public const string OFFERS_OVERVIEW = "OfferOverviewPage";
        public const string ENROLL_LOYALTY = "EnrollLoyaltyPage";
        public const string FORGOTTEN_PASSWORD = "ForgottenPasswordPage";
        public const string LOGIN_ERROR = "LoginErrorPage";
        public const string LOGIN_FOR_DEEPLINK = "LoginForDeepLink";
        public const string LOGOUT_CONFIRMATION = "LogoutConfirmationPage";
        public const string GENERIC_ERROR_PAGE = "ErrorPage";
        public const string MYACCOUNT_BOOKING_PAGE = "MyAccountAndBookingsPage";
        public const string MORE_IMAGES_PAGE = "MoreImagesPopUp";
        public const string ENROLL_TERMS_AND_CONDITION = "EnrollTermsAndConditionsPage";
        public const string CS_CONTACTUS_THANKYOU = "CustomerServiceContactUsThankyouPage";
        public const string EXPANSION_PAGE = "ExpansionPage";
        public const string SITE_FOOTER_PAGE = "SiteFooterPage";
        public const string ABOUT_US_PAGE = "AboutUsPage";
        public const string CUSTOMER_SERVICE_PAGE = "CustomerServicePage";
        public const string QUICK_LINKS_PAGE = "QuickLinksPage";
        public const string OFFERS_LIST_PAGE = "OfferContainer";

        public const string PRINTER_FRIENDLY_CONFIRMATION = "ConfirmationPrinterFriendly";
        public const string CANCEL_PRINTER_FRIENDLY_CONFIRMATION = "CancelConfirmationPrinterFriendly";

        public const string SESSION_TIME_OUT = "SessionTimeout";
        public const string APP_ERROR = "AppError";

        public const string MODIFY_BOOKING_SELECT_RATE = "ModifyBookingSelectRatePage";
        public const string RECEIPT = "Receipt";
        public const string SESSION_TIMEOUT_PAGE = "SessionTimeoutPage";
        public const string PAYMENT_PROCESSING_PAGE = "PaymentProcessingPage";
        public const string FIND_YOUR_DESTINATION = "HotelOverviewPage";
        public const string CITY_LANDING = "City";
        public const string COUNTRY_LANDING = "Country";
        public const string FILE_NOT_FOUND = "FileNotFound";
        public const string MY_PROFILE_PAGE = "MyProfilePage";

    }
}