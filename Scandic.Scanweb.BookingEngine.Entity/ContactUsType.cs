﻿namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// This entity hold the value of items to be displayed 
    /// in the drop downs with associated email ids.
    /// </summary>
    public class ContactUsType
    {
        public ContactUsType(string displayText, string email)
        {
            DisplayText = displayText;
            Email = email;
        }

        public string DisplayText { get; set; }
        public string Email { get; set; }
    }
}