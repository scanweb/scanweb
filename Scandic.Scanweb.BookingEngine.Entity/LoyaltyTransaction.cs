﻿using System;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.Entity
{
    public class LoyaltyTransaction
    {
        public string TransactionInfo { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public string Points { get; set; }

        public int DateDifference()
        {
            return DateUtil.DateDifference(ToDate, FromDate);
        }

        public string DateRange()
        {
            return FromDate.ToString("dd/MM/yyyy") + " - " + ToDate.ToString("dd/MM/yyyy");
        }
    }
}