//  Description					: SMS Class for SMS                    			  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Shankar Dasgupta                                  	  //
/// Author email id				:                           							  //
/// Creation Date				: 14th November  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// Entity class for Email Details used for sending Confirmation to user
    /// </summary>
    public class SMSEntity
    {
        /// <summary>
        /// Get/Set the Phone Number
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Get/Set the Message to be Send
        /// </summary>
        public string Message { get; set; }
    }
}