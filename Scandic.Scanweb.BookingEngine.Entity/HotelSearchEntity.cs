//  Description					:   HotelSearchEntity                                     //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// This class represents hotel search entity.
    /// </summary>
    public class HotelSearchEntity
    {
        /// <summary>
        /// The SearchForEntity containing the details what user
        /// has initially searched for. It contains the
        /// 1. User has entered in the city/hotel name
        /// 2. The corresponding the string type i.e, search for hotel or hotels in a city
        /// 3. The Opera code for the search string
        /// </summary>
        private SearchedForEntity searchedFor;

        /// <summary>
        /// The Arrival Date selected by user for Search/Booking
        /// </summary>
        private DateTime arrivalDate;

        /// <summary>
        /// The Departure Date selected by user of Search/Booking
        /// </summary>
        private DateTime departureDate;

        /// <summary>
        /// Number of Nights is the Derived field of the difference
        /// between the <code>departureDate</code> and <code>arrivalDate</code>
        /// </summary>
        private int noOfNights;

        /// <summary>
        /// Number of Rooms per night user has selected for booking/search
        /// </summary>
        private int roomsPerNight;

        /// <summary>
        /// Number of Adults per room user has selected
        /// </summary>
        private int adultsPerRoom;

        /// <summary>
        /// Number of Chidren per room user has selected
        /// </summary>
        private int childrenPerRoom;

        // Scanweb 1.6 - START
        /// <summary>
        /// Since Child in a Parent's bed is not considered
        /// as an occupancy, we need to exclude them.
        /// This field will be used while doing the availab-
        /// ility search.
        /// </summary>
        private int childrenOccupancyPerRoom;

        // Scanweb 1.6 - END

        // Deeplink Cr related changes.
        private IList<HotelSearchRoomEntity> listRooms = null;

        /// <summary>
        /// Gets/Sets ListRooms
        /// </summary>
        public IList<HotelSearchRoomEntity> ListRooms
        {
            get { return listRooms; }
            set { listRooms = value; }
        }

        /// <summary>
        /// The search type object containing the type of search user has done.
        /// </summary>
        private SearchType searchingType;

        /// <summary>
        /// The Campaign code valid for the corresponding type of search user has done
        /// If Regular Search/Booking => Promotion code enter by user need to be set
        /// If Negotiated Search/Booking => D-Number/Corporate code entered by user need to be set
        /// If Voucher Search/Booking => The Voucher special code to be set
        /// </summary>
        private string campaignCode;

        /// <summary>
        /// //R1.4: CR2: Travel Agent Booking
        /// To store the Qualifying Type permanently
        /// </summary>
        private string qualifyingType;

        /// <summary>
        /// This field is added to set the userName if user has done a Redemption search
        /// </summary>
        private string userName;

        /// <summary>
        /// The OWS Opera ID for the Redemption search, loaylty user
        /// </summary>
        private string nameID;

        /// <summary>
        /// This the Opera ID for the hotel user has selected for doing a booking.
        /// I.
        /// If user has initiated the search for a city, this field will be set after
        /// user selects a hotel in the select a hotel page
        /// II. 
        /// If user has initiated the search for the hotel directly, this field will be
        /// set directly after user does the search.
        /// </summary>
        private string selectedHotelCode;

        /// <summary>
        /// Country Code of the Hotel
        /// </summary>
        private string hotelCountryCode;

        /// <summary>
        /// Cancel By date of the booking
        /// </summary>
        private DateTime cancelByDate;

        /// <summary>
        /// Get/Set the Cancel By Date of the booking
        /// </summary>
        public DateTime CancelByDate
        {
            get { return cancelByDate; }
            set { cancelByDate = value; }
        }

        /// <summary>
        /// Get/Set the Cancel Policy of the Booking
        /// </summary>
        /// <remarks>
        /// </remarks>
        public string CancelPolicy { get; set; }

        /// <summary>
        /// Gets/Sets HotelCountryCode
        /// </summary>
        public string HotelCountryCode
        {
            get { return hotelCountryCode; }
            set { hotelCountryCode = value; }
        }

        /// <summary>
        /// Gets/Sets SearchedFor
        /// </summary>
        public SearchedForEntity SearchedFor
        {
            get { return searchedFor; }
            set { searchedFor = value; }
        }

        /// <summary>
        /// Gets/Sets ArrivalDate
        /// </summary>
        public DateTime ArrivalDate
        {
            get { return arrivalDate; }
            set { arrivalDate = value; }
        }

        /// <summary>
        /// Gets/Sets DepartureDate
        /// </summary>
        public DateTime DepartureDate
        {
            get { return departureDate; }
            set { departureDate = value; }
        }

        /// <summary>
        /// Gets/Sets NoOfNights
        /// </summary>
        public int NoOfNights
        {
            get { return noOfNights; }
            set { noOfNights = value; }
        }

        /// <summary>
        /// Gets/Sets RoomsPerNight
        /// </summary>
        public int RoomsPerNight
        {
            get { return roomsPerNight; }
            set { roomsPerNight = value; }
        }

        /// <summary>
        /// Gets/Sets AdultsPerRoom
        /// </summary>
        public int AdultsPerRoom
        {
            get { return adultsPerRoom; }
            set { adultsPerRoom = value; }
        }

        /// <summary>
        /// Gets/Sets ChildrenPerRoom
        /// </summary>
        public int ChildrenPerRoom
        {
            get { return childrenPerRoom; }
            set { childrenPerRoom = value; }
        }

        /// <summary>
        /// Gets or sets childrenPerRoomToBeAccommodated.
        /// </summary>
        public int ChildrenOccupancyPerRoom
        {
            get { return childrenOccupancyPerRoom; }
            set { childrenOccupancyPerRoom = value; }
        }

        /// <summary>
        /// Gets/Sets CampaignCode
        /// </summary>
        public string CampaignCode
        {
            get { return campaignCode; }
            set { campaignCode = value; }
        }

        /// <summary>
        /// Gets/Sets IsBlockCodeRate
        /// </summary>
        public bool IsBlockCodeRate { get; set; }

        /// <summary>
        /// Gets/Sets QualifyingType
        /// </summary>
        public string QualifyingType
        {
            get { return qualifyingType; }
            set { qualifyingType = value; }
        }

        /// <summary>
        /// Gets/Sets SearchingType
        /// </summary>
        public SearchType SearchingType
        {
            get { return searchingType; }
            set { searchingType = value; }
        }

        /// <summary>
        /// Gets/Sets UserName
        /// </summary>
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        /// <summary>
        /// Gets/Sets NameID
        /// </summary>
        public string NameID
        {
            get { return nameID; }
            set { nameID = value; }
        }

        /// <summary>
        /// Gets/Sets SelectedHotelCode
        /// </summary>
        public string SelectedHotelCode
        {
            get { return selectedHotelCode; }
            set { selectedHotelCode = value; }
        }

        /// <summary>
        /// Gets/Sets MembershipID
        /// </summary>
        public long MembershipID { get; set; }

        /// <summary>
        /// Gets/Sets IsSearchEntityPopulatedFromDeeplink
        /// </summary>
        public bool IsSearchEntityPopulatedFromDeeplink { get; set; }

        public string IataProfileCode { get; set; }

        /// <summary>
        /// Extended to string method
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                sb.Append("Arrival Date: " + arrivalDate.ToString() + Environment.NewLine);
                sb.Append("Departure Date: " + departureDate.ToString() + Environment.NewLine);
                sb.Append("No Of Nights: " + noOfNights.ToString() + Environment.NewLine);
                sb.Append("Rooms Per Night: " + roomsPerNight.ToString() + Environment.NewLine);
                sb.Append("Adults Per Room: " + adultsPerRoom.ToString() + Environment.NewLine);
                sb.Append("Children Per Room: " + childrenPerRoom.ToString() + Environment.NewLine);
                sb.Append("Children Occupancy Per Room: " + childrenOccupancyPerRoom.ToString() + Environment.NewLine);
                if (listRooms != null && listRooms.Count > 0)
                {
                    for (int i = 0; i < listRooms.Count; i++)
                    {
                        sb.AppendFormat("Adults in Room{0}: {1}{2}", i + 1, listRooms[i].AdultsPerRoom.ToString(),
                                        Environment.NewLine);
                        sb.AppendFormat("Children in Room{0}: {1}{2}", i + 1, listRooms[i].ChildrenPerRoom.ToString(),
                                        Environment.NewLine);
                        sb.AppendFormat("Children Occupancy in Room{0}: {1}{2}", i + 1,
                                        listRooms[i].ChildrenOccupancyPerRoom.ToString(), Environment.NewLine);

                        if (listRooms[i].ChildrenDetails != null)
                        {
                            sb.AppendFormat("Adults per room in Children Details of Room{0}: {1}{2}", i + 1,
                                            listRooms[i].ChildrenDetails.AdultsPerRoom.ToString(), Environment.NewLine);
                            sb.AppendFormat("Children details text in Children Details of Room{0}: {1}{2}", i + 1,
                                            listRooms[i].ChildrenDetails.ChildrensDetailsInText, Environment.NewLine);

                            if (listRooms[i].ChildrenDetails.ListChildren != null &&
                                listRooms[i].ChildrenDetails.ListChildren.Count > 0)
                            {
                                for (int j = 0; j < listRooms[i].ChildrenDetails.ListChildren.Count; j++)
                                {
                                    sb.AppendFormat("Age of Child{0} of Room{1}: {2}{3}", j + 1, i + 1,
                                                    listRooms[i].ChildrenDetails.ListChildren[j].Age.ToString(),
                                                    Environment.NewLine);
                                    sb.AppendFormat("Bed type of Child{0} of Room{1}: {2}{3}", j + 1, i + 1,
                                                    listRooms[i].ChildrenDetails.ListChildren[j].ChildAccommodationType.
                                                        ToString(), Environment.NewLine);
                                    sb.AppendFormat("Accomodation string of Child{0} of Room{1}: {2}{3}", j + 1, i + 1,
                                                    listRooms[i].ChildrenDetails.ListChildren[j].AccommodationString,
                                                    Environment.NewLine);
                                }
                            }
                        }
                    }
                }
                sb.Append("Search Type: " + searchingType.ToString() + Environment.NewLine);
                if (searchedFor != null)
                {
                    sb.Append("SearchedFor Code: " + searchedFor.SearchCode.ToString() + Environment.NewLine);
                    sb.Append("SearchedFor Search String: " + searchedFor.SearchString.ToString() + Environment.NewLine);
                    sb.Append("SearchedFor UserSearchType: " + searchedFor.UserSearchType.ToString() +
                              Environment.NewLine);
                }

                sb.Append("Selected Hotel : " + selectedHotelCode);
            }
            catch (Exception Ex)
            {
                AppLogger.LogFatalException(Ex, "Could not Form string from Search entity.");
                return "Could not Form string from Search entity: " + Ex.Message;
            }

            return sb.ToString();
        }

        /// <summary>
        /// Clone the Entity
        /// </summary>
        /// <returns>
        /// HotelSearch Entity
        /// </returns>
        public HotelSearchEntity Clone()
        {
            HotelSearchEntity cloneEntity = new HotelSearchEntity();

            cloneEntity.arrivalDate = this.arrivalDate;
            cloneEntity.departureDate = this.departureDate;
            cloneEntity.noOfNights = this.noOfNights;
            cloneEntity.roomsPerNight = this.roomsPerNight;
            cloneEntity.adultsPerRoom = this.adultsPerRoom;
            cloneEntity.childrenPerRoom = this.childrenPerRoom;
            cloneEntity.childrenOccupancyPerRoom = this.childrenOccupancyPerRoom;
            if (this.listRooms != null && this.listRooms.Count > 0)
            {
                cloneEntity.listRooms = new List<HotelSearchRoomEntity>();
                HotelSearchRoomEntity cloneHotelSearchRoomEntity = null;
                for (int i = 0; i < this.listRooms.Count; i++)
                {
                    cloneHotelSearchRoomEntity = new HotelSearchRoomEntity();
                    cloneHotelSearchRoomEntity.AdultsPerRoom = this.listRooms[i].AdultsPerRoom;
                    cloneHotelSearchRoomEntity.ChildrenPerRoom = this.listRooms[i].ChildrenPerRoom;
                    cloneHotelSearchRoomEntity.ChildrenOccupancyPerRoom = this.listRooms[i].ChildrenOccupancyPerRoom;

                    if (this.listRooms[i].ChildrenDetails != null)
                    {
                        ChildrensDetailsEntity cloneChildrenDetails = new ChildrensDetailsEntity();
                        cloneChildrenDetails.AdultsPerRoom = this.listRooms[i].ChildrenDetails.AdultsPerRoom;
                        cloneChildrenDetails.ChildrensDetailsInText =
                            this.listRooms[i].ChildrenDetails.ChildrensDetailsInText;

                        cloneChildrenDetails.ListChildren = new List<ChildEntity>();
                        for (int j = 0; j < this.listRooms[i].ChildrenDetails.ListChildren.Count; j++)
                        {
                            ChildEntity cloneChild = new ChildEntity();
                            cloneChild.AccommodationString =
                                this.listRooms[i].ChildrenDetails.ListChildren[j].AccommodationString;
                            cloneChild.Age = this.listRooms[i].ChildrenDetails.ListChildren[j].Age;
                            cloneChild.ChildAccommodationType =
                                this.listRooms[i].ChildrenDetails.ListChildren[j].ChildAccommodationType;

                            cloneChildrenDetails.ListChildren.Add(cloneChild);
                        }

                        cloneHotelSearchRoomEntity.ChildrenDetails = cloneChildrenDetails;
                    }

                    cloneEntity.listRooms.Add(cloneHotelSearchRoomEntity);
                }
            }

            cloneEntity.searchingType = this.searchingType;
            cloneEntity.campaignCode = this.campaignCode;
            cloneEntity.qualifyingType = this.qualifyingType;
            cloneEntity.userName = this.userName;
            cloneEntity.nameID = this.nameID;
            cloneEntity.selectedHotelCode = this.selectedHotelCode;
            cloneEntity.hotelCountryCode = this.hotelCountryCode;
            cloneEntity.cancelByDate = this.cancelByDate;

            SearchedForEntity cloneSearchForEntity = null;
            if (this.searchedFor != null)
            {
                cloneSearchForEntity = new SearchedForEntity(this.searchedFor.SearchString);
                cloneSearchForEntity.SearchCode = this.searchedFor.SearchCode;
                cloneSearchForEntity.UserSearchType = this.searchedFor.UserSearchType;
            }
            cloneEntity.searchedFor = cloneSearchForEntity;
            cloneEntity.MembershipID = this.MembershipID;
            cloneEntity.IataProfileCode = this.IataProfileCode;
            return cloneEntity;
        }

        /// <summary>
        /// Gets/Sets IsModifyComboBooking
        /// </summary>
        public bool IsModifyComboBooking { set; get; }
    }

    /// <summary>
    /// The searched for Entity containing the details of what user has searched for
    /// i.e, city or hotel
    /// </summary>
    public class SearchedForEntity
    {
        /// <summary>
        /// Enumeration containing the different kinds of locations types
        /// user can search for.
        /// Currently only two are user
        /// 1. Hotel
        /// 2. City
        /// The other option 3.Region is added for future user if we include it.
        /// </summary>
        public enum LocationSearchType
        {
            Hotel,
            City,
            Region
        }

        /// <summary>
        /// Gets/Sets SearchString
        /// </summary>
        public string SearchString { get; set; }

        /// <summary>
        /// Gets/Sets InitialSearchString
        /// </summary>
        public string CitySearchString { get; set; }
        
        /// <summary>
        /// Gets/Sets UserSearchType
        /// </summary>
        public LocationSearchType UserSearchType { get; set; }

        /// <summary>
        /// Gets/Sets SearchCode
        /// </summary>
        public string SearchCode { get; set; }

        /// <summary>
        /// Sets the search string.
        /// </summary>
        public SearchedForEntity(string searchString)
        {
            this.SearchString = searchString;
        }
    }

    /// <summary>
    /// The enumerated type containing the different values
    /// for 5 different kinds of search user can do in the site
    /// </summary>
    public enum SearchType
    {
        REGULAR,
        CORPORATE,
        BONUSCHEQUE,
        REDEMPTION,
        VOUCHER, 
        MODIFY,
        BLOCKCODE
    }
}