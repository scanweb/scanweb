// <copyright file="RewardNightGiftGuestInformationEntity.cs" company="Sapient">
// Copyright (c) 2008 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>01-April-2010</date>
// <version>Hygiene</version>
// <summary>Entity class to hold the actual guest info, when a reward night gift booking is performed</summary>

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// A subset of guest information entity to carry the 'reward night gift booking' guest info
    /// </summary>
    public class RewardNightGiftGuestInformationEntity
    {
        /// <summary>
        /// Temporary holder for the FGP user's email, this is used to send mail on cancel confirmation
        /// </summary>
        private string emailAddressFgpUser = string.Empty;

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The city.</value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the mobile number.
        /// </summary>
        /// <value>The mobile number.</value>
        public string MobileNumber { get; set; }

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        /// <value>The email address.</value>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the email address FGP user.
        /// </summary>
        /// <value>The email address FGP user.</value>
        public string EmailAddressFgpUser
        {
            get { return emailAddressFgpUser; }
            set { emailAddressFgpUser = value; }
        }
    }
}