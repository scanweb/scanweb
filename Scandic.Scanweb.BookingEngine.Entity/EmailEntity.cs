//  Description					: Entity Class for Email                    			  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Shankar Dasgupta                                  	  //
/// Author email id				:                           							  //
/// Creation Date				: 14th November  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// Entity class for Email Details used for sending Confirmation to user
    /// </summary>
    public class EmailEntity
    {
        /// <summary>
        /// Get or set the email body in text format.
        /// </summary>
        public string TextEmailBody { get; set; }

        /// <summary>
        /// Get or set the credit card receipt in text format.
        /// </summary>
        public string CreditCardReceiptHtmlText { get; set; }

        /// <summary>
        /// Get/Set the receipt reseravtion number 
        /// </summary>
        public string ReceiptReservationNumber { get; set; }

        /// <summary>
        /// Get/Set the Body of the Email 
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Get/Set the PlaceHolder of the Email 
        /// </summary>
        public string PlaceHolder { get; set; }

        /// <summary>
        /// Get/Set the Subject of the Email 
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Get/Set the Sender of the Email 
        /// </summary>
        public string Sender { get; set; }

        /// <summary>
        /// Get/Set the Receiver of the Email 
        /// </summary>
        public string[] Recipient { get; set; }

        /// <summary>
        /// Get/Set the Receiver of the Email 
        /// </summary>
        public string[] CarbonCopy { get; set; }
    }
}