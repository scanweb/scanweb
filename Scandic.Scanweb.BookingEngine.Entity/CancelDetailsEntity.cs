//  Description					: Phone Details Entity                      			  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Girish Krishnan                                   	  //
/// Author email id				:                           							  //
/// Creation Date				: 14th December  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// This is used to store the cancel booking details inforamtion
    /// </summary>
    public class CancelDetailsEntity
    {
        #region Private Members

        /// <summary>
        /// Phone number to send sms after cancelation
        /// </summary>
        private string smsNumber = string.Empty;

        #endregion Private Members

        #region Public Properties

        public string LegNumber { get; set; }

        public bool CancelationStatus { get; set; }

        public string SMSNumber
        {
            get { return smsNumber; }
            set { smsNumber = value; }
        }

        public string HotelCode { get; set; }

        public string ChainCode { get; set; }

        public string ReservationNumber { get; set; }

        public DateTime CancelDate { get; set; }

        public bool CancelDateSpecified { get; set; }

        public CancelTermType CancelType { get; set; }

        public string CancelNumber { get; set; }

        #endregion Public properties
    }

    /// <summary>
    /// Enum for CancelTerm Type
    /// </summary>
    public enum CancelTermType
    {
        Cancel,
        NoShow,
        Other
    }
}