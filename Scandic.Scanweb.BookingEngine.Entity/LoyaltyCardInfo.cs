﻿namespace Scandic.Scanweb.Entity
{
    public class LoyaltyCardInfo
    {
        public string MembershipNo { get; set; }
        public string NoOfStays { get; set; }
        public string Level { get; set; }
        public string NoOfPoints { get; set; }
    }
}