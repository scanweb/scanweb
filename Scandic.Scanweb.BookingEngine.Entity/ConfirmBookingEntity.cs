//  Description					: Phone Details Entity                      			  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Girish Krishnan?                                   	  //
/// Author email id				:                           							  //
/// Creation Date				: 14th December  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// ConfirmBookingEntity
    /// </summary>
    public class ConfirmBookingEntity
    {
        #region Private Members

        /// <summary>
        /// Used to store the cancellation number
        /// </summary>
        private string ignoreBookingNumber;

        #endregion Private Members

        #region Public Properties

        public string ReservationNumber { get; set; }


        public string LegNumber { get; set; }

        public string HotelCode { get; set; }

        public string ChainCode { get; set; }

        #endregion Public properties
    }
}