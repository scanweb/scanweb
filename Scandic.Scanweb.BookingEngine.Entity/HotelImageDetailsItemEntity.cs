namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// This entitily holds the details of the individual images attached to the hotel
    /// </summary>
    public class HotelImageDetailsItemEntity
    {
        /// <summary>
        /// This holds the url of the image associated with the hotel
        /// </summary>
        private string imageUrl;

        public string ImageUrl
        {
            get { return imageTitle; }
            set { imageTitle = value; }
        }

        /// <summary>
        /// This holds the title of the image associated with the hotel
        /// </summary>
        private string imageTitle;

        public string ImageTitle
        {
            get { return imageTitle; }
            set { imageTitle = value; }
        }

        /// <summary>
        /// This holds the Description of the image associated with the hotel
        /// </summary>
        private string imageDescription;

        public string ImageDescription
        {
            get { return imageTitle; }
            set { imageTitle = value; }
        }
    }
}