using System.Collections.Generic;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// The Room Category class containing a room category for which
    /// the different rate types of a Rate category will have common rate
    /// </summary>
    public class RoomCategoryEntity
    {
        #region Variables

        /// <summary>
        /// The Rate Category Id
        /// </summary>
        private string id;

        /// <summary>
        /// The dictionary object containing the following details
        /// 
        /// Key as RateCategory ID
        /// Value as List of RoomRateEntity (Each of these RoomRateEntity will have common rate and belongs
        /// to the RateCateogry)
        /// </summary>
        private Dictionary<string, List<RoomRateEntity>> rateCategories
            = new Dictionary<string, List<RoomRateEntity>>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets Id
        /// </summary>
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Gets/Sets Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets/Sets Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets/Sets Url
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets/Sets RateCategories
        /// </summary>
        public Dictionary<string, List<RoomRateEntity>> RateCategories
        {
            get { return rateCategories; }
            set { rateCategories = value; }
        }

        #endregion

        /// <summary>
        /// Returns the list of unique roomtype codes available with this RoomCategory Entity
        /// 
        /// Iterates through the list of rateCategories and adds the element to <code>roomTypes</code>
        /// if it is not existing already in the list.
        /// </summary>
        public List<string> RoomTypes
        {
            get
            {
                Dictionary<string, string> roomTypesDictionaryForUniqueList = new Dictionary<string, string>();
                List<string> roomTypes = new List<string>();

                if (null != rateCategories)
                {
                    foreach (string rateCategoryId in rateCategories.Keys)
                    {
                        List<RoomRateEntity> roomRates = rateCategories[rateCategoryId];
                        foreach (RoomRateEntity roomRate in roomRates)
                        {
                            string roomTypeCode = roomRate.RoomTypeCode;
                            if (!roomTypesDictionaryForUniqueList.ContainsKey(roomTypeCode))
                            {
                                roomTypesDictionaryForUniqueList.Add(roomTypeCode, roomTypeCode);
                                roomTypes.Add(roomTypeCode);
                            }
                        }
                    }
                }
                return roomTypes;
            }
        }

        #region Merge Methods

        /// <summary>
        /// The passed <code>RoomCategoryEntity</code> equals to this RoomCategory
        /// then the RateCategory details of the passed object will be merged into 
        /// this RateCategory details
        /// 
        /// This will compare the Base Rate (Rate per night)
        /// Returns true if they are equal, else returns false
        /// </summary>
        /// <param name="roomCateogry">The RoomCategoryEntity to be compared and merged</param>
        /// <returns>True if the passed object is merged with this object</returns>
        public bool MergeRoomCateogryEntityWithBaseRate(RoomCategoryEntity roomCateogry)
        {
            if (this.Equals(roomCateogry))
            {
                MergeRoomCategory(roomCateogry);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Refer to <see cref="MergeRoomCategoryEntityWithBaseRate"/> Except this class will consider the total rate
        /// </summary>
        /// <param name="roomCategory">The RoomCategory Entity to be compared and merged with</param>
        /// <returns></returns>
        public bool MergeRoomCategoryEntityWithTotalRate(RoomCategoryEntity roomCategory)
        {
            if (EqualsWithTotalRate(roomCategory))
            {
                MergeRoomCategory(roomCategory);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Refer to <see cref="MergeRoomCategoryEntityWithBaseRate"/> Except this call will consider
        /// the points.
        /// </summary>
        /// <param name="roomCateogry"></param>
        /// <returns></returns>
        public bool MergeRoomCateogryEntityWithPoints(RoomCategoryEntity roomCateogry)
        {
            if (this.EqualsWithRedemptionPoints(roomCateogry))
            {
                MergeRoomCategory(roomCateogry);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Merges the roomCategory to this roomCategory by combining the rate categories
        /// </summary>
        /// <param name="roomCategory">The room category to be merged</param>
        private void MergeRoomCategory(RoomCategoryEntity roomCategory)
        {
            foreach (string rateCategoryName in this.rateCategories.Keys)
            {
                List<RoomRateEntity> thisRoomRateList = rateCategories[rateCategoryName];
                try
                {
                    List<RoomRateEntity> objRoomRateList = roomCategory.rateCategories[rateCategoryName];
                    thisRoomRateList.AddRange(objRoomRateList);
                }
                catch (KeyNotFoundException knfe)
                {
                }
            }
        }

        #endregion

        #region Equality Methods

        /// <summary>
        /// The equality method which compares the common rates of all rate categories of the <code>obj</code>
        /// with <code>this</code> rate categories, if all the rate categories are having equal rates
        /// then it will return true else return false
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            RoomCategoryEntity roomCategory = obj as RoomCategoryEntity;
            if (this.id == roomCategory.id)
            {
                foreach (string rateCategoryName in this.rateCategories.Keys)
                {
                    RateEntity thisCommonRate = GetCommonBaseRate(rateCategoryName);
                    RateEntity objCommonRate = roomCategory.GetCommonBaseRate(rateCategoryName);

                    if (thisCommonRate != null && objCommonRate != null &&
                        false == thisCommonRate.Rate.Equals(objCommonRate.Rate))
                        return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Compares the total rate of the roomcategory with this room category rates returns
        /// ture if all are equal
        /// 
        /// Refer <see cref="Equals"/> But the total rate (Total stay rate) are checked
        /// For future use
        /// </summary>
        /// <param name="roomCategory"></param>
        /// <returns></returns>
        private bool EqualsWithTotalRate(RoomCategoryEntity roomCategory)
        {
            if (this.id == roomCategory.id)
            {
                foreach (string rateCategoryName in this.rateCategories.Keys)
                {
                    RateEntity thisCommonRate = GetCommonTotalRate(rateCategoryName);
                    RateEntity objCommonRate = roomCategory.GetCommonTotalRate(rateCategoryName);

                    if (thisCommonRate != null &&
                        objCommonRate != null &&
                        false == thisCommonRate.Equals(objCommonRate))
                        return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Refer <see cref="Equals"/> But here the points are considered
        /// </summary>
        /// <param name="roomCategory"></param>
        /// <returns></returns>
        private bool EqualsWithRedemptionPoints(RoomCategoryEntity roomCategory)
        {
            if (this.id == roomCategory.id)
            {
                foreach (string rateCategoryName in this.rateCategories.Keys)
                {
                    RedeemptionPointsEntity thisCommonRate = GetCommonPoints(rateCategoryName);
                    RedeemptionPointsEntity objCommonRate = roomCategory.GetCommonPoints(rateCategoryName);

                    if (thisCommonRate != null &&
                        objCommonRate != null &&
                        false == thisCommonRate.Equals(objCommonRate))
                        return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region Common Rate/Points Methods

        /// <summary>
        /// As all the rates for a rate category will have common rate, this method will return
        /// the rate of the first object
        /// This will return the Base Rate (Per Night rate)
        /// </summary>
        /// <param name="rateCategoryName"></param>
        /// <returns></returns>
        public RateEntity GetCommonBaseRate(string rateCategoryName)
        {
            RateEntity toReturn = null;
            try
            {
                List<RoomRateEntity> roomRates = rateCategories[rateCategoryName];
                if (null != roomRates && roomRates.Count > 0 && roomRates[0].BaseRate != null)
                {
                    return roomRates[0].BaseRate;
                }
            }
            catch (KeyNotFoundException knfe)
            {
            }
            return toReturn;
        }


        /// <summary>
        /// Gets the common base rate for prepaid block.
        /// </summary>
        /// <param name="roomCategoryName">Name of the room category.</param>
        /// <returns></returns>
        public RateEntity GetCommonBaseRateForPrepaidBlock(string roomCategoryName)
        {
            RateEntity toReturn = null;
            try
            {
                List<RoomRateEntity> roomRates = rateCategories[roomCategoryName];
                if (null != roomRates && roomRates.Count > 0)
                {
                    return roomRates[0].BaseRate;
                }
            }
            catch (KeyNotFoundException knfe)
            {
            }
            return toReturn;
        }


        /// <summary>
        /// Gets the common total rate for prepaid block.
        /// </summary>
        /// <param name="roomCategoryName">Name of the room category.</param>
        /// <returns></returns>
        public RateEntity GetCommonTotalRateForPrepaidBlock(string roomCategoryName)
        {
            RateEntity toReturn = null;
            try
            {
                List<RoomRateEntity> roomRates = rateCategories[roomCategoryName];
                if (null != roomRates && roomRates.Count > 0 && roomRates[0].TotalRate != null)
                {
                    return roomRates[0].TotalRate;
                }
            }
            catch (KeyNotFoundException knfe)
            {
            }
            return toReturn;
        }

        /// <summary>
        /// Refer to <see cref="GetCommonBaseRate"/> but the Total rate is considered here
        /// </summary>
        /// <param name="rateCategoryName"></param>
        /// <returns></returns>
        public RateEntity GetCommonTotalRate(string rateCategoryName)
        {
            RateEntity toReturn = null;
            try
            {
                List<RoomRateEntity> roomRates = rateCategories[rateCategoryName];
                if (null != roomRates && roomRates.Count > 0 && roomRates[0].TotalRate != null)
                {
                    return roomRates[0].TotalRate;
                }
            }
            catch (KeyNotFoundException knfe)
            {
            }
            return toReturn;
        }

        /// <summary>
        /// Refer to <see cref="GetCommonPoints"/> but the points are considered here
        /// </summary>
        /// <param name="rateCategoryName"></param>
        /// <returns></returns>
        public RedeemptionPointsEntity GetCommonPoints(string rateCategoryName)
        {
            RedeemptionPointsEntity toReturn = null;
            try
            {
                List<RoomRateEntity> roomRates = rateCategories[rateCategoryName];
                if (null != roomRates && roomRates.Count > 0 && roomRates[0].PointsDetails != null)
                {
                    return roomRates[0].PointsDetails;
                }
            }
            catch (KeyNotFoundException knfe)
            {
            }
            return toReturn;
        }

        #endregion
    }
}