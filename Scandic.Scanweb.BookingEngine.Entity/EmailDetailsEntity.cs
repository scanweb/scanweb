//  Description					: EmailDetailsEntity                        	 		  //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						: Shankar Dasgupta                                  	  //
// Author email id				:                           							  //
// Creation Date				: 24th December  2007									  //
//	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
// Revison History				: -NA-													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// Used for storing email details
    /// </summary>    
    [Serializable]
    public class EmailDetailsEntity
    {
        #region private memebers

        /// <summary>
        /// store opera id
        /// </summary>
        private long emailOperaId;

        /// <summary>
        /// store email
        /// </summary>
        private string emailId;

        #endregion private members

        #region Public properties
        /// <summary>
        /// Gets/Sets EmailOperaID
        /// </summary>
        public long EmailOperaID
        {
            get { return emailOperaId; }
            set { emailOperaId = value; }
        }

        /// <summary>
        /// Gets/Sets EmailID
        /// </summary>
        public string EmailID
        {
            get { return emailId; }
            set { emailId = value; }
        }

        #endregion Public properties
    }
}