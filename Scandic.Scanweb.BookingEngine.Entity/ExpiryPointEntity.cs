////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  ExpiryPointEntity                                      //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                                               		  //
// Creation Date				:           	    								      //
// Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
// Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// This class captures the expiry date and respective point from the
    /// Membership number.
    /// </summary>
    public class ExpiryPointEntity
    {
        public ExpiryPointEntity()
        {
        }

        /// <summary>
        /// Gets/Sets ExpiryDate
        /// </summary>
        public DateTime ExpiryDate { get; set; }

        /// <summary>
        /// Gets/Sets TotalPointExpire
        /// </summary>
        public DateTime TotalPointExpire { get; set; }

        /// <summary>
        /// Gets/Sets ExpiryPoint
        /// </summary>
        public double ExpiryPoint { get; set; }

        /// <summary>
        /// Gets/Sets TotalPoint
        /// </summary>
        public double TotalPoint { get; set; }
    }
}