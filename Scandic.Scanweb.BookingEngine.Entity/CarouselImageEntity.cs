﻿
namespace Scandic.Scanweb.Entity
{
    public class CarouselImageEntity
    {
        public string ImageUrl { get; set; }
        public string AltText { get; set; }
    }
}
