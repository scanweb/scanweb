﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Scanweb.Entity
{
    public class MerchantProfileEntity
    {
        public string MerchantID { get;set;}
        public string MerchantToken { get; set; }
        public string CurrencyCode { get; set; }
        public string LegalInformation { get; set; }
        public string OrganizationNumber { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }      
        public string CompanyName { get; set; }
        public bool VisaCard { get; set; }
        public bool MasterCard { get; set; }
        public bool DinersCard { get; set; }
        public bool AmexCard { get; set; }
        public bool DankortCard { get; set; }
        public bool JCBCard { get; set; }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="MerchantID"></param>
        /// <param name="MerchantToken"></param>
        /// <param name="CurrencyCode"></param>
        /// <param name="LegalInformation"></param>
        public MerchantProfileEntity(string MerchantID, string name, string MerchantToken, string CurrencyCode, string LegalInformation,
                                     string oranizationNumber,  string address, string phone, string fax, string email, string companyName,
                                     bool visaCard, bool masterCard, bool dinersCard, bool amexCard, bool dankortCard, bool JCBcard)
        {
            this.MerchantID = MerchantID;
            Name = name; 
            this.MerchantToken = MerchantToken;
            this.CurrencyCode = CurrencyCode;
            this.LegalInformation = LegalInformation;
            OrganizationNumber = oranizationNumber;
            Address = address;
            Phone = phone;
            Fax = fax;
            Email = email;        
            this.CompanyName = companyName;
            this.VisaCard = visaCard;
            this.MasterCard = masterCard;
            this.DinersCard = dinersCard;
            this.AmexCard = amexCard;
            this.DankortCard = dankortCard;
            this.JCBCard = JCBCard;
        }
    }
}
