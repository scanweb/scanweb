using System.Collections.Generic;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// The RatePlan class abstracting the rate details returned from OWS
    /// </summary>
    public class RatePlanEntity
    {
        #region Variables

        /// <summary>
        /// The RatePlan code
        /// </summary>
        private string code;

        /// <summary>
        /// Any additional information returned from OWS
        /// </summary>
        private Dictionary<string, string> additionalInformation = new Dictionary<string, string>();

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="code"></param>
        public RatePlanEntity(string code)
        {
            this.code = code;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="code"></param>
        /// <param name="description"></param>
        public RatePlanEntity(string code, string description)
        {
            this.code = code;
            this.Description = description;
        }

        #endregion

        #region Properties

        public string Code
        {
            get { return code; }
        }

        public string Description { get; set; }

        public Dictionary<string, string> AdditionalInformation
        {
            get { return additionalInformation; }
            set { additionalInformation = value; }
        }

        #endregion

        /// <summary>
        /// AddAdditionalInformation
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddAdditionalInformation(string key, string value)
        {
            additionalInformation.Add(key, value);
        }
    }
}