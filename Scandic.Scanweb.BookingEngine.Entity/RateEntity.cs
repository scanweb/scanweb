//  Description					: RateEntity                                              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// The Rate Entity abstracting the rate to stay at hotel
    /// </summary>
    public class RateEntity
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="rate"></param>
        /// <param name="currencyCode"></param>
        public RateEntity(double rate, string currencyCode)
        {
            this.rate = rate;
            this.currencyCode = currencyCode;
        }

        /// <summary>
        /// The Currency Code
        /// </summary>
        private string currencyCode;

        /// <summary>
        /// The rate value
        /// </summary>
        private double rate;

        /// <summary>
        /// Gets/Sets CurrencyCode
        /// </summary>
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }

        /// <summary>
        /// Gets/Sets Rate
        /// </summary>
        public double Rate
        {
            get { return rate; }
            set { rate = value; }
        }

        /// <summary>
        /// Equals
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            RateEntity rateEntity = obj as RateEntity;
            return (rateEntity != null && this.rate == rateEntity.Rate && this.currencyCode == rateEntity.CurrencyCode);
        }
    }
}