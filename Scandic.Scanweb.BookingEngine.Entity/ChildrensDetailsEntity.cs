// <copyright file="ChildrensDetailsEntity.cs" company="Sapient">
// Copyright (c) 2008 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>08-April-2009</date>
// <version>1.0(Scanweb 1.6)</version>
// <summary>Entity class carry the information about the children of the guest and the type of bed-type</summary>

#region System NameSpaces

using System;
using System.Collections.Generic;
using System.Text;
using Scandic.Scanweb.Core;

#endregion

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// Entity class carry the information about the children
    /// of the guest and the type of bed-type. The list member
    /// of this class is used to keep the information about
    /// each and every child.
    /// </summary>
    public class ChildrensDetailsEntity
    {
        #region Constructors

        /// <summary>
        /// ctor for the class ChildrensDetailsEntity
        /// </summary>
        public ChildrensDetailsEntity()
        {
            listChildren = new List<ChildEntity>();
        }

        #endregion

        #region Private Fields

        /// <summary>
        /// List carrying information about all the children
        /// Each element in the list will be used for capturing
        /// one child's info.
        /// </summary>
        private IList<ChildEntity> listChildren = null;

        /// <summary>
        /// Number of adults per room
        /// </summary>
        private uint adultsPerRoom;

        /// <summary>
        /// This field is populated from the html hidden input which
        /// carries all the information about children(such as age, 
        /// type of acco, ages)
        /// </summary>
        private string childrensDetailsInText;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets listChildren.
        /// </summary>
        public IList<ChildEntity> ListChildren
        {
            get { return listChildren; }
            set { listChildren = value; }
        }

        /// <summary>
        /// Gets or sets adultsPerRoom
        /// </summary>
        public uint AdultsPerRoom
        {
            get { return adultsPerRoom; }
            set { adultsPerRoom = value; }
        }

        /// <summary>
        /// Gets or sets the childrens details in text.
        /// </summary>
        /// <value>The childrens details in text.</value>
        public string ChildrensDetailsInText
        {
            get { return childrensDetailsInText; }
            set { childrensDetailsInText = value; }
        }

        /// <summary>
        /// This will build the age string to be stored
        /// in opera comments field
        /// </summary>
        public string AgeInComments
        {
            get
            {
                string returnValue = string.Empty;
                if (listChildren != null)
                {
                    StringBuilder age = new StringBuilder("Age :");
                    int count = listChildren.Count;
                    for (int childCount = 0; childCount < count; childCount++)
                    {
                        age.Append(listChildren[childCount].Age.ToString());
                        if (childCount + 1 != count)
                        {
                            age.Append(", ");
                        }
                    }
                    returnValue = age.ToString();
                }
                return returnValue;
            }
        }

        #endregion

        #region Methods

        #region Public Methods

        /// <summary>
        /// This will set Children list and adult per each room. 
        /// </summary>
        /// <param name="adultsPerRoom"></param>
        /// <param name="children"></param>
        public void SetChildrensEntity(uint adultsPerRoom, List<ChildEntity> children)
        {
            this.adultsPerRoom = adultsPerRoom;
            if (listChildren == null)
            {
                listChildren = new List<ChildEntity>();
            }
            listChildren = children;
        }

        /// <summary>
        /// The string aupplied here is parsed and 'ChildEntity's
        /// are created. This will be added to the list.
        /// </summary>
        /// <param name="childrensDetails">parsable string usually read from the childrenInfoHidden field</param>
        public void SetChildrensDetailsEntity(uint adultsPerRoom, string childrensDetails, string cot, string sharingBed,
                                              string extraBed)
        {
            try
            {
                this.adultsPerRoom = adultsPerRoom;
                this.childrensDetailsInText = childrensDetails;

                string[] child = childrensDetails.Split('|');

                if (listChildren == null)
                {
                    listChildren = new List<ChildEntity>();
                }
                for (int i = 0; i < child.Length - 1; i++)
                {
                    string[] childInfo = child[i].Split(',');
                    if (childInfo.Length > 0)
                    {
                        ChildEntity childEntity = new ChildEntity();
                        int age = 0;
                        int.TryParse(childInfo[0], out age);
                        childEntity.Age = age;
                        if (string.Compare(childInfo[1], cot, false) == 0)
                        {
                            childEntity.ChildAccommodationType = ChildAccommodationType.CRIB;
                            childEntity.AccommodationString = cot;
                        }
                        else if (string.Compare(childInfo[1], sharingBed, false) == 0)
                        {
                            childEntity.ChildAccommodationType = ChildAccommodationType.CIPB;
                            childEntity.AccommodationString = sharingBed;
                        }
                        else if (string.Compare(childInfo[1], extraBed, false) == 0)
                        {
                            childEntity.ChildAccommodationType = ChildAccommodationType.XBED;
                            childEntity.AccommodationString = extraBed;
                        }

                        listChildren.Add(childEntity);
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Could not create the list of children from the hidden text passed in the method SetChildrensDetailsEntity");
                throw;
            }
        }

        /// <summary>
        /// Get the ages of all children in the list in a 
        /// string format age1, age2, .., agen
        /// </summary>
        /// <returns>The string in the format age1, age2, .., agen</returns>
        public string GetChildrensAgesInString()
        {
            StringBuilder ages = new StringBuilder();
            try
            {
                if (listChildren != null)
                {
                    if (listChildren.Count > 0)
                    {
                        for (int i = 0; i < listChildren.Count; i++)
                        {
                            ages.Append(listChildren[i].Age);
                            if (i + 1 != listChildren.Count)
                            {
                                ages.Append(", ");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Could not create a string of ages from childrens list. Proceeding... in the method GetChildrensAgesInString");
            }
            return ages.ToString();
        }

        /// <summary>
        /// Children's accommodation will be formatted in the
        /// form "2 Extra bed, 1 Crib, 1 Child in parent's bed"
        /// and returned
        /// </summary>
        /// <param name="cipb">translated "Child in parent's bed" string</param>
        /// <param name="cot">translated "Crib" string</param>
        /// <param name="extrabed">translated "Extra bed" string</param>
        /// <returns>string in the form of "2 Extra bed, 1 Crib, 1 Child in parent's bed"</returns>
        public string GetChildrensAccommodationInString(string cipb, string cot, string extrabed)
        {
            StringBuilder accommodations = new StringBuilder();
            try
            {
                if (listChildren != null)
                {
                    if (listChildren.Count > 0)
                    {
                        int noOfCIPBs = 0;
                        int noOfCots = 0;
                        int noOfExtraBed = 0;

                        for (int i = 0; i < listChildren.Count; i++)
                        {
                            switch (listChildren[i].ChildAccommodationType)
                            {
                                case ChildAccommodationType.CIPB:
                                    {
                                        noOfCIPBs++;
                                    }
                                    break;
                                case ChildAccommodationType.CRIB:
                                    {
                                        noOfCots++;
                                    }
                                    break;
                                case ChildAccommodationType.XBED:
                                    {
                                        noOfExtraBed++;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                        if (noOfExtraBed > 0)
                        {
                            accommodations.Append(noOfExtraBed);
                            accommodations.Append(" ");
                            accommodations.Append(extrabed);
                            if (noOfCots > 0 || noOfCIPBs > 0)
                                accommodations.Append(", ");
                        }
                        if (noOfCots > 0)
                        {
                            accommodations.Append(noOfCots);
                            accommodations.Append(" ");
                            accommodations.Append(cot);
                            if (noOfCIPBs > 0)
                                accommodations.Append(", ");
                        }
                        if (noOfCIPBs > 0)
                        {
                            accommodations.Append(noOfCIPBs);
                            accommodations.Append(" ");
                            accommodations.Append(cipb);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Could not create a string of accommodation types from childrens list. Proceeding... in the method GetChildrensAccommodationInString");
            }
            return accommodations.ToString();
        }

        //Vrushali - Res2.0 - This function is used to show the previously selected childern details in shopping cart.
        /// <summary>
        /// Children's accommodation will be formatted in the
        /// form "2 Extra bed, 1 Crib, 1 Child in parent's bed"
        /// and returned
        /// </summary>
        /// <param name="cipb">translated "Child in parent's bed" string</param>
        /// <param name="cot">translated "Crib" string</param>
        /// <param name="extrabed">translated "Extra bed" string</param>
        /// <returns>string in the form of "2 Extra bed, 1 Crib, 1 Child in parent's bed"</returns>
        public string GetChildrensAccommodationInString(string cipb, string cot, string extrabed, int childCount)
        {
            StringBuilder accommodations = new StringBuilder();
            try
            {
                if (listChildren != null)
                {
                    if (listChildren.Count > 0 && listChildren[childCount] != null)
                    {
                        int noOfCIPBs = 0;
                        int noOfCots = 0;
                        int noOfExtraBed = 0;
                        switch (listChildren[childCount].ChildAccommodationType)
                        {
                            case ChildAccommodationType.CIPB:
                                {
                                    noOfCIPBs++;
                                }
                                break;
                            case ChildAccommodationType.CRIB:
                                {
                                    noOfCots++;
                                }
                                break;
                            case ChildAccommodationType.XBED:
                                {
                                    noOfExtraBed++;
                                }
                                break;
                            default:
                                break;
                        }
                        if (noOfExtraBed > 0)
                        {
                            accommodations.Append(extrabed);
                            if (noOfCots > 0 || noOfCIPBs > 0)
                                accommodations.Append(", ");
                        }
                        if (noOfCots > 0)
                        {
                            accommodations.Append(cot);
                            if (noOfCIPBs > 0)
                                accommodations.Append(", ");
                        }
                        if (noOfCIPBs > 0)
                        {
                            accommodations.Append(cipb);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Could not create a string of accommodation types from childrens list. Proceeding... in the method GetChildrensAccommodationInString");
            }
            return accommodations.ToString();
        }

        /// <summary>
        /// Get the Accommodation List of the Children
        /// </summary>
        /// <returns></returns>
        public List<BedAccommodation> GetAccommodationList()
        {
            List<BedAccommodation> accommodationList = null;
            if (listChildren != null)
            {
                if (listChildren.Count > 0)
                {
                    int noOfCIPBs = 0;
                    int noOfCots = 0;
                    int noOfExtraBed = 0;

                    for (int i = 0; i < listChildren.Count; i++)
                    {
                        switch (listChildren[i].ChildAccommodationType)
                        {
                            case ChildAccommodationType.CIPB:
                                {
                                    noOfCIPBs++;
                                }
                                break;
                            case ChildAccommodationType.CRIB:
                                {
                                    noOfCots++;
                                }
                                break;
                            case ChildAccommodationType.XBED:
                                {
                                    noOfExtraBed++;
                                }
                                break;
                            default:
                                break;
                        }
                    }

                    accommodationList = new List<BedAccommodation>();
                    if (noOfCIPBs > 0)
                    {
                        BedAccommodation accommodation = new BedAccommodation();
                        accommodation.BedType = ChildAccommodationType.CIPB;
                        accommodation.Quantity = noOfCIPBs;
                        accommodationList.Add(accommodation);
                    }
                    if (noOfCots > 0)
                    {
                        BedAccommodation accommodation = new BedAccommodation();
                        accommodation.BedType = ChildAccommodationType.CRIB;
                        accommodation.Quantity = noOfCots;
                        accommodationList.Add(accommodation);
                    }
                    if (noOfExtraBed > 0)
                    {
                        BedAccommodation accommodation = new BedAccommodation();
                        accommodation.BedType = ChildAccommodationType.XBED;
                        accommodation.Quantity = noOfExtraBed;
                        accommodationList.Add(accommodation);
                    }
                }
            }
            return accommodationList;
        }

        /// <summary>
        /// Set the Accommodation List
        /// </summary>
        /// <param name="listBedAccommodation"></param>
        public void SetAccommodationList(List<BedAccommodation> listBedAccommodation)
        {
            try
            {
                if (listBedAccommodation != null && listBedAccommodation.Count > 0)
                {
                    listChildren = new List<ChildEntity>();
                    foreach (BedAccommodation bedAccd in listBedAccommodation)
                    {
                        int childrenWithThisAccm = bedAccd.Quantity;
                        for (int count = 0; count < childrenWithThisAccm; count++)
                        {
                            switch (bedAccd.BedType)
                            {
                                case ChildAccommodationType.CIPB:
                                    {
                                        ChildEntity child = new ChildEntity();
                                        child.ChildAccommodationType = ChildAccommodationType.CIPB;
                                        listChildren.Add(child);
                                        break;
                                    }
                                case ChildAccommodationType.CRIB:
                                    {
                                        ChildEntity child = new ChildEntity();
                                        child.ChildAccommodationType = ChildAccommodationType.CRIB;
                                        listChildren.Add(child);
                                        break;
                                    }
                                case ChildAccommodationType.XBED:
                                    {
                                        ChildEntity child = new ChildEntity();
                                        child.ChildAccommodationType = ChildAccommodationType.XBED;
                                        listChildren.Add(child);
                                        break;
                                    }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Could not create the list of children from the hidden text passed, in the method SetAccommodationList");
                throw;
            }
        }

        /// <summary>
        /// From the bed type enum, figures out the locale specific string and
        /// storing it.
        /// </summary>
        /// <param name="cot">locale for cot type</param>
        /// <param name="extraBed">locale for extra bed</param>
        /// <param name="sharingBed">locale for child in adult's bed</param>
        public void SetAccommodationString(string cot, string extraBed, string sharingBed)
        {
            IList<ChildEntity> childEntityList = this.listChildren;
            int childCount = childEntityList.Count;

            for (int listCount = 0; listCount < childCount; listCount++)
            {
                string accommodationString = childEntityList[listCount].ChildAccommodationType.ToString();
                switch (childEntityList[listCount].ChildAccommodationType)
                {
                    case ChildAccommodationType.CRIB:
                        childEntityList[listCount].AccommodationString = cot;
                        break;
                    case ChildAccommodationType.XBED:
                        childEntityList[listCount].AccommodationString = extraBed;
                        break;
                    case ChildAccommodationType.CIPB:
                        childEntityList[listCount].AccommodationString = sharingBed;
                        break;
                }
            }
        }

        #endregion

        #endregion
    }

    /// <summary>
    /// Information of a single child is stored in 
    /// instances of this class.
    /// </summary>
    public class ChildEntity
    {
        #region Private Fields

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets age field
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets childAccommodationType field
        /// </summary>
        public ChildAccommodationType ChildAccommodationType { get; set; }

        /// <summary>
        /// Gets or sets childAccommodationType field
        /// </summary>
        public string AccommodationString { get; set; }

        #endregion
    }

    /// <summary>
    /// Bed-type of the child is indicated by this enum
    /// </summary>
    public enum ChildAccommodationType
    {
        /// <summary>
        /// Select age default.
        /// </summary>
        SelectBedType,

        /// <summary>
        /// Child In Parent's Bed
        /// Occupancy is 0
        /// </summary>
        CIPB,

        /// <summary>
        /// Seperate Cot/Crib for the Child
        /// Occupancy is 1
        /// </summary>
        CRIB,

        /// <summary>
        /// An Extra Bed for the Child
        /// Occupancy is 1
        /// </summary>
        XBED
    }

    /// <summary>
    /// This class holds the bed type accommodation.
    /// </summary>
    public class BedAccommodation
    {
        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>The quantity.</value>
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the type of the bed.
        /// </summary>
        /// <value>The type of the bed.</value>
        public ChildAccommodationType BedType { get; set; }
    }
}