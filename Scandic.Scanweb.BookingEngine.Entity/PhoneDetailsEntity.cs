//  Description					: Phone Details Entity                      			  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Girish Krishnan                                   	  //
/// Author email id				:                           							  //
/// Creation Date				: 24th December  2007									  //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// Used to store the phone related information
    /// </summary>    
    [Serializable]
    public class PhoneDetailsEntity
    {
        #region Private Members

        /// <summary>
        /// Hold the phone number
        /// </summary>
        private string number;

        /// <summary>
        /// Hold the phone type (Mobile/HomePhone)
        /// </summary>
        private string phoneType;

        /// <summary>
        /// Hold the opera ID
        /// </summary>
        private long operaId;

        #endregion Private Members

        #region Public Properties

        public string Number
        {
            get { return number; }
            set { number = value; }
        }

        public long OperaId
        {
            get { return operaId; }
            set { operaId = value; }
        }

        public string PhoneType
        {
            get { return phoneType; }
            set { phoneType = value; }
        }

        #endregion Public Properties
    }
}