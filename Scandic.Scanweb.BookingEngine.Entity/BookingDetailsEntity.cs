//  Description					: Entity Class for Email                    			  //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						: Shankar Dasgupta                                  	  //
// Author email id				:                           							  //
// Creation Date				: 14th November  2007									  //
// Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// Encaptulates Booking Details 
    /// </summary>
    public class BookingDetailsEntity
    {
        /// <summary>
        /// Guest Information Entity
        /// </summary>
        private GuestInformationEntity guestInformationEntity;

        /// <summary>
        /// Hotel Room Rate Entity
        /// </summary>
        private HotelRoomRateEntity hotelRoomRateEntity;

        /// <summary>
        /// Hotel Search Entity
        /// </summary>
        private HotelSearchEntity hotelSearchEntity;

        /// <summary>
        /// Cancel Details Entity
        /// </summary>
        private CancelDetailsEntity cancelDetailsEntity;

        /// <summary>
        /// Guest Info for 'Reward Night Gift' type booking
        /// </summary>
        private RewardNightGiftGuestInformationEntity rewardNightGiftGuestInfo;

        /// <summary>
        /// Flag to indicate booking cacellation status
        /// </summary>
        private bool isCancelledByUser;

        /// <summary>
        /// Flag to indicate if booking is cancelled due to time lapse
        /// </summary>
        private bool isCancelledBySystem;

        private bool isBookingModifiable = true;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotelSearch">
        /// HotelSearchEntity
        /// </param>
        /// <param name="hotelRoomRate">
        /// HotelRoomRate Entity
        /// </param>
        /// <param name="guestInformation">
        /// GuestInformation Entity
        /// </param>
        /// <param name="cancelDetailsEntity">
        /// CancelDetails Entity
        /// </param>
        public BookingDetailsEntity(HotelSearchEntity hotelSearch, HotelRoomRateEntity hotelRoomRate,
                                    GuestInformationEntity guestInformation, CancelDetailsEntity cancelDetailsEntity)
        {
            this.hotelSearchEntity = hotelSearch;
            this.hotelRoomRateEntity = hotelRoomRate;
            this.guestInformationEntity = guestInformation;
            this.cancelDetailsEntity = cancelDetailsEntity;
            SetDependentStatusFlags();
        }

        /// <summary>
        /// This method sets the flags dependent on the cancellation and hotel entities
        /// Author: Ruman Khan
        /// </summary>
        private void SetDependentStatusFlags()
        {
            if (cancelDetailsEntity != null)
            {
                this.isCancelledByUser = true;
            }
            if (hotelSearchEntity != null && cancelDetailsEntity == null)
            {
                DateTime bookingCancelDate = hotelSearchEntity.CancelByDate;
                if (bookingCancelDate != DateTime.MinValue &&
                    this.guestInformationEntity.GuranteeInformation.GuranteeType == GuranteeType.HOLD
                    )
                {
                    TimeSpan diffTime = bookingCancelDate.Subtract(DateTime.Now);
                    if (diffTime.TotalDays < 0)
                    {
                        this.isCancelledBySystem = true;
                    }
                }
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotelSearch">HotelSearchEntity</param>
        /// <param name="hotelRoomRate">HotelRoomRate Entity</param>
        /// <param name="guestInformation">GuestInformation Entity</param>
        /// <param name="cancelDetailsEntity">CancelDetails Entity</param>
        /// <param name="rewardNightGiftGuestInfo">The reward night gift guest info.</param>
        public BookingDetailsEntity(
            HotelSearchEntity hotelSearch,
            HotelRoomRateEntity hotelRoomRate,
            GuestInformationEntity guestInformation,
            CancelDetailsEntity cancelDetailsEntity,
            RewardNightGiftGuestInformationEntity rewardNightGiftGuestInfo
            )
        {
            this.hotelSearchEntity = hotelSearch;
            this.hotelRoomRateEntity = hotelRoomRate;
            this.guestInformationEntity = guestInformation;
            this.cancelDetailsEntity = cancelDetailsEntity;
            this.rewardNightGiftGuestInfo = rewardNightGiftGuestInfo;
            SetDependentStatusFlags();
        }

        /// <summary>
        /// Get GuestInformation Entity
        /// </summary>
        public GuestInformationEntity GuestInformation
        {
            get { return guestInformationEntity; }
        }

        /// <summary>
        /// Gets the reward night gift guest info.
        /// </summary>
        /// <value>The reward night gift guest info.</value>
        public RewardNightGiftGuestInformationEntity RewardNightGiftGuestInfo
        {
            get { return rewardNightGiftGuestInfo; }
        }

        /// <summary>
        /// Get HotelRoomRate Entity
        /// </summary>
        public HotelRoomRateEntity HotelRoomRate
        {
            get { return hotelRoomRateEntity; }
        }

        /// <summary>
        /// Get HotelSearch Entity
        /// </summary>
        public HotelSearchEntity HotelSearch
        {
            get { return hotelSearchEntity; }
        }

        /// <summary>
        /// Get Cancel Details Entity
        /// </summary>
        public CancelDetailsEntity CancelDetails
        {
            get { return cancelDetailsEntity; }
        }

        /// <summary>
        /// Get/Set the Reservation Number for the concerned booking
        /// </summary>
        public string ReservationNumber { get; set; }

        /// <summary>
        /// Gets/Sets D_Number 
        /// </summary>
        public string D_Number { get; set; }

        /// <summary>
        /// Gets/Sets IsCancelledByUser
        /// </summary>
        public bool IsCancelledByUser
        {
            get { return isCancelledByUser; }
            set { isCancelledByUser = value; }
        }

        /// <summary>
        /// Gets/Sets IsCancelledBySytem
        /// </summary>
        public bool IsCancelledBySytem
        {
            get { return isCancelledBySystem; }
            set { isCancelledBySystem = value; }
        }

        /// <summary>
        /// Get/Set the Leg Number of the concerned number
        /// </summary>
        public string LegNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is current booking modifiable.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is current booking modifiable; otherwise, <c>false</c>.
        /// </value>
        public bool IsCurrentBookingModifiable
        {
            get { return isBookingModifiable; }
            set { isBookingModifiable = value; }
        }

        /// <summary>
        /// Gets/Sets ListBedAccommodation
        /// </summary>
        public List<BedAccommodation> ListBedAccommodation { get; set; }

        public DateTime ReservationCreateDate { get; set; }
    }
}