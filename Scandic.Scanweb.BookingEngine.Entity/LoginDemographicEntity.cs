//  Description					: Entity Class for Login Demographics          			  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Himansu Senapati                                  	  //
//  Author email id				:                           							  //
//  Creation Date				: 19th May 2008									          //
//	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// Holds the different modules from all possible 
    /// pages where an user can login to the site.
    /// </summary>
    public enum LoginSourceModule
    {
        NONE,
        BOOKING_SEARCH_MODULE,
        BOOKING_DETAIL_MODULE,
        BOOKING_FLOW_MODULE,
        LOGIN_ERROR_MODULE,
        LOGIN_POPUP_MODULE,
        LOGIN_CONFLOGOUT_MODULE,
        LOGIN_FOR_DEEPLINK
    }

    /// <summary>
    /// Holds the names of the controls from where user is loging in to the system
    /// </summary>
    public class LoginDemographicEntity
    {
        /// <summary>
        /// Property of userNameControl to access out side the class.
        /// </summary>
        public string UserNameControlPrefix { get; set; }
        /// <summary>
        /// Property of userNameControl to access out side the class.
        /// </summary>
        public string UserNameControl { get; set; }

        /// <summary>
        /// Property of passwordControl to access out side the class.
        /// </summary>
        public string PasswordControl { get; set; }

        /// <summary>
        /// Property of remember check box to access out side the class.
        /// </summary>
        public string RememberMeChkBox { get; set; }

        /// <summary>
        /// Page Identifier.
        /// </summary>
        public string PageIdentifier { get; set; }

        /// <summary>
        /// Constructor method
        /// </summary>
        public LoginDemographicEntity()
        {
        }

        /// <summary>
        /// Parameterised Constructor method to assign all values to the members of the entity.
        /// </summary>
        public LoginDemographicEntity(string userNameControlPrefix, string userNameControl, string passwordControl, string rememberMeChkBox,
                                      string pageIdentifier)
        {
            this.UserNameControlPrefix = userNameControlPrefix;
            this.UserNameControl = userNameControl;
            this.PasswordControl = passwordControl;
            this.RememberMeChkBox = rememberMeChkBox;
            this.PageIdentifier = pageIdentifier;
        }
    }
}