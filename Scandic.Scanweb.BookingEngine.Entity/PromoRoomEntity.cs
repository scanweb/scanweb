﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Scanweb.Entity
{
    public class PromoRoomEntity : RoomEntity
    {
        public bool RoomHasPromoRates { get; set; }
    }
}
