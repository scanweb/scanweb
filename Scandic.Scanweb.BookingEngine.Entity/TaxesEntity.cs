﻿
using System.Collections.Generic;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// TaxesEntity
    /// </summary>
    public class TaxesEntity
    {
        /// <summary>
        /// The charges for VAT information
        /// </summary>
        private List<ChargeEntity> charges;

        /// <summary>
        /// Gets/Sets Charges
        /// </summary>
        public List<ChargeEntity> Charges
        {
            get { return charges; }
            set { charges = value; }
        }

    }
}
