using System;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// LoyaltyDetailsEntity
    /// </summary>
    public class LoyaltyDetailsEntity
    {
        /// <summary>
        /// Address Line 1
        /// </summary>
        private string addressLine1;

        /// <summary>
        /// Address Line 2
        /// </summary>
        private string addressLine2;

        public double CurrentPoints { get; set; }

        public string Fax { get; set; }

        public string UserName { get; set; }

        public string Pin { get; set; }

        public bool MemberToSignIn { get; set; }

        public string NameID { get; set; }

        public string MembershipID { get; set; }

        public string MembershipLevel { get; set; }

        public DateTime ArrivalDate { get; set; }

        public DateTime DepartureDate { get; set; }

        public string City { get; set; }

        public string Hotel { get; set; }

        public string Email { get; set; }

        public string ReservationNumber { get; set; }

        public string Title { get; set; }

        public string FirstName { get; set; }

        public string SurName { get; set; }

        //Added as part of SCANAM-537
        public string NativeFirstName { get; set; }

        public string NativeLastName { get; set; }

        public string AddressLine1
        {
            get { return addressLine1; }
            set { addressLine1 = value; }
        }

        public string AddressLine2
        {
            get { return addressLine2; }
            set { addressLine2 = value; }
        }

        /// <summary>
        /// Returns the address line 1 and address line2 as a string array
        /// if one of the element is null then it is set as empty string else
        /// both the address lines are returned as string array.
        /// </summary>
        public string[] AddressLines
        {
            get
            {
                string[] addressLines;
                if (addressLine1 != null && addressLine2 != null)
                {
                    addressLines = new string[2];
                    addressLines[0] = addressLine1;
                    addressLines[1] = addressLine2;
                }
                else if (addressLine1 != null)
                {
                    addressLines = new string[1];
                    addressLines[0] = addressLine1;
                }
                else if (addressLine2 != null)
                {
                    addressLines = new string[2];
                    addressLines[0] = string.Empty;
                    addressLines[1] = addressLine2;
                }
                else
                {
                    addressLines = null;
                }
                return addressLines;
            }
        }

        public string PostCode { get; set; }

        public string Country { get; set; }

        public string Telephone1 { get; set; }

        public string PreferredLanguage { get; set; }
    }
}