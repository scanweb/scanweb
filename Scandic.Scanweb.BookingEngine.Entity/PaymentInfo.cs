﻿//  Description					: PaymentInfo entity                                      //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


using System;
namespace Scandic.Scanweb.Entity
{
    public class PaymentInfo
    {

        /// <summary>
        /// Gets/Sets PanHash
        /// </summary>
        public string PanHash { get; set; }

        /// <summary>
        /// Gets/Sets CreditCardNumberMasked 
        /// </summary>
        public string CreditCardNumberMasked { get; set; }

        /// <summary>
        /// Gets/Sets ExpiryDate
        /// </summary>
        public DateTime ExpiryDate { get; set; }

        /// <summary>
        /// Gets/Sets CardType 
        /// </summary>
        public string CardType { get; set; }

        /// <summary>
        /// Gets/Sets CaptureStatus
        /// </summary>
        public bool CaptureSuccess { get; set; }

        /// <summary>
        /// Gets/Sets MakePaymentFailure
        /// </summary>
        public bool MakePaymentFailure { get; set; }

        /// <summary>
        /// Get/Set reservation state 
        /// </summary>
        public ReservationState ReservationStateInfo { get; set; }
    }

    public enum ReservationState
    {
        CreateReservationInProgress,
        CreateReservationSuccess,
        CreateReservationFailure,
        CaptureCallSuccess,
        CatprureCallFailure,
        MakePaymentSuccess,
        MakePaymentFailure,
        ReservationSuccessful
    }
}
