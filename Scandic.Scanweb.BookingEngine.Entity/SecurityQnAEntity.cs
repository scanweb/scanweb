using System;
using System.Data;
using System.Configuration;

namespace Scandic.Scanweb.BookingEngine.Entity
{
    /// <summary>
    /// Summary description for SecurityQnA
    /// </summary>
    public class SecurityQnAEntity
    {
        private String question;
        private String answer;

        public SecurityQnAEntity(String question, String answer)
        {
            this.question = question;
            this.answer = answer;
        }

        public String GetQuestion()
        {
            return question;
        }

        public String GetAnswer()
        {
            return answer;
        }
    }
}