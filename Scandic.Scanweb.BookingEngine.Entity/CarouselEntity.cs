﻿using System.Collections.Generic;


namespace Scandic.Scanweb.Entity
{
    public class CarouselEntity
    {
        public string PageTypeId { get; set; }
        public bool ActivateImageCaraousel { get; set; }
        public List<CarouselImageEntity> ImageList { get; set; }
        public string Alignment { get; set; }
        public bool AutoSlideRequired { get; set; }
        public int TimeOut { get; set; }
        public string TransitionEffect { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string SlideCss { get; set; }
        public string TopCss { get; set; }
        public string RightCss { get; set; }
        public string LeftCss { get; set; }
        public string PlaceHolder { get; set; }
    }
}

