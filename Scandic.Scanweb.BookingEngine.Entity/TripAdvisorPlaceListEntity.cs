﻿using System.Collections.Generic;

namespace Scandic.Scanweb.Entity
{

    public class TripAdvisorPlaceListEntity
    {
       public List<TripAdvisorPlaceEntity> TAPlaces { get; set; }
    }

    public class TripAdvisorPlaceEntity
    {
        public int Id { get; set; }

        public string LocationId { get; set; }

        public int ReviewCount { get; set; }

        public double AverageRating { get; set; }

        public string PartnerID { get; set; }

        public string TripAdvisorImageURL { get; set; }

        public string TripAdvisorImageDefaultURL { get; set; }

        public Dictionary<string, string> ImageURL { get; set; }

    }
}
