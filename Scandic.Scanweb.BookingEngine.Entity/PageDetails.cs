//  Description					: Entity Class for PageDetails                			  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Himansu Senapati                                  	  //
/// Author email id				:                           							  //
/// Creation Date				: 19th May 2008									          //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// PageDetails
    /// </summary>
    public class PageDetails
    {
        /// <summary>
        /// Public property for pageId
        /// </summary>
        public int PageId { get; set; }

        /// <summary>
        /// Public property for pageUrl
        /// </summary>
        public string PageUrl { get; set; }

        /// <summary>
        /// Constructor method
        /// </summary>
        public PageDetails()
        {
        }

        /// <summary>
        /// Parameterised Constructor method to assign all values to the members of the entity.
        /// </summary>
        /// <param name="pageId"><Page Id/param>
        /// <param name="pageUrl">Page url</param>
        public PageDetails(int pageId, string pageUrl)
        {
            this.PageId = pageId;
            this.PageUrl = pageUrl;
        }
    }
}