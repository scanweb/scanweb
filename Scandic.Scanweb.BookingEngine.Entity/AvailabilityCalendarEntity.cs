// <copyright file="AvailCalendarEntity.cs" company="Sapient">
// Copyright (c) 2010 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>12-July-2010</date>
// <version>R 2.0</version>
// <summary>Entity class carry the information about availability rate calendar</summary>

using System.Collections.Generic;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// AvailabilityCalendarEntity
    /// </summary>
    [System.Runtime.Remoting.Contexts.Synchronization()]
    public class AvailabilityCalendarEntity : System.ContextBoundObject
    {
        /// <summary>
        /// Gets or sets the left index of the carousel.
        /// </summary>
        /// <value>The index of the left carousel.</value>
        public int LeftCarouselIndex { get; set; }

        /// <summary>
        /// Gets or sets the right index of the carousel.
        /// </summary>
        /// <value>The index of the right carousel.</value>
        public int RightCarouselIndex { get; set; }

        /// <summary>
        /// Gets or sets the left index of the visible carousel.
        /// </summary>
        /// <value>The index of the left visible carousel.</value>
        public int LeftVisibleCarouselIndex { get; set; }

        /// <summary>
        /// Gets or sets the right index of the visible carousel.
        /// </summary>
        /// <value>The index of the right visible carousel.</value>
        public int RightVisibleCarouselIndex { get; set; }

        /// <summary>
        /// Gets or sets the state of the search.
        /// </summary>
        /// <value>The state of the search.</value>
        public SearchState SearchState { get; set; }

        /// <summary>
        /// Gets or sets the list avail calendar items.
        /// </summary>
        /// <value>The list avail calendar items.</value>
        public IList<AvailCalendarItemEntity> ListAvailCalendarItems { get; set; }

        /// <summary>
        /// Gets or sets the index of the selected item.
        /// </summary>
        /// <value>The index of the selected item.</value>
        public int SelectedItemIndex { get; set; }

        /// <summary>
        /// Gets or sets the running thread count.
        /// </summary>
        /// <value>The running thread count.</value>
        public int RunningThreadCount { get; set; }

        /// <summary>
        /// Gets/Sets TotalMultiSearchRequests 
        /// </summary>
        public int TotalMultiSearchRequests { get; set; }

        /// <summary>
        /// Gets/Sets TotalMultiSearchRequests 
        /// </summary>
        public int TotalMultiSearchResponses { get; set; }
    }


    /// <summary>
    /// This enum indicates the availibility search status 
    /// </summary>
    public enum SearchState
    {
        /// <summary>
        /// 
        /// </summary>
        NONE,

        /// <summary>
        /// indicates that search is in progress;spawned threads execution is in progress.
        /// </summary>
        PROGRESS,

        /// <summary>
        /// 
        /// </summary>
        VISIBLE_COMPLETED,

        /// <summary>
        /// 
        /// </summary>
        NEXT_BATCH,

        /// <summary>
        /// 
        /// </summary>
        PREV_BATCH,

        /// <summary>
        /// indicates that all the spawned threads have completed the execution
        /// </summary>
        COMPLETED
    }
}