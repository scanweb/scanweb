//  Description					: SelectedRoomAndRateEntity                               //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// The class abstracting the room category and the rate category
    /// selected by the user and also captures the list room rate combinations
    /// as returned from OWS
    /// </summary>
    public class SelectedRoomAndRateEntity
    {
        #region Variables

        /// <summary>
        /// The Room category selected by the user
        /// </summary>
        private string roomCategoryID;

        /// <summary>
        /// The Rate Cateogry selected by the user
        /// </summary>
        private string rateCategoryID;

        /// <summary>
        /// The List of room Rates returned from OWS
        /// </summary>
        private List<RoomRateEntity> roomRates;

        /// <summary>
        /// The hotel destination object, containing the hotel details
        /// </summary>
        private HotelDestination hotelDestination;

        /// <summary>
        /// The Country code of the selected hotel
        /// </summary>
        private string countryCode;

        /// <summary>
        /// Room description
        /// </summary>
        private string roomDescription;

        #endregion

        #region Properties
        /// <summary>
        /// Gets/Sets RoomCategoryID
        /// </summary>
        public string RoomCategoryID
        {
            get { return roomCategoryID; }
            set { roomCategoryID = value; }
        }

        /// <summary>
        /// Gets/Sets RateCategoryID
        /// </summary>
        public string RateCategoryID
        {
            get { return rateCategoryID; }
            set { rateCategoryID = value; }
        }

        /// <summary>
        /// Gets/Sets RoomRates
        /// </summary>
        public List<RoomRateEntity> RoomRates
        {
            get { return roomRates; }
            set { roomRates = value; }
        }

        /// <summary>
        /// Gets/Sets HotelDestination
        /// </summary>
        public HotelDestination HotelDestination
        {
            get { return hotelDestination; }
            set { hotelDestination = value; }
        }

        /// <summary>
        /// Gets/Sets CountryCode
        /// </summary>
        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }

        /// <summary>
        /// Gets/Sets SelectedRoomCategoryName
        /// </summary>
        public string SelectedRoomCategoryName { get; set; }

        //This property added by Vrushali. its required in shopping cart.
        /// <summary>
        /// Gets or sets the selected room category entity.
        /// </summary>
        /// <value>The selected room category entity.</value>
        public RoomCategoryEntity SelectedRoomCategoryEntity { get; set; }

        /// <summary>
        /// Gets/Sets RoomDescription
        /// </summary>
        public string RoomDescription
        {
            get { return roomDescription; }
            set { roomDescription = value; }
        }

        #endregion

        #region Clone SelectRoomAndRateEntity

        /// <summary>
        /// This is used to clone the SelectRoomAndRateEntity.This is used in booking Confirmation control.
        /// Why Cloned ? 
        /// After booking SelectRoomAndRateEntity is made to null because this is causing problem
        /// </summary>
        /// <returns></returns>
        public SelectedRoomAndRateEntity Clone()
        {
            SelectedRoomAndRateEntity clonedSelectedRoomAndRateEntity = new SelectedRoomAndRateEntity();

            clonedSelectedRoomAndRateEntity.roomCategoryID = this.roomCategoryID;
            clonedSelectedRoomAndRateEntity.rateCategoryID = this.rateCategoryID;
            clonedSelectedRoomAndRateEntity.hotelDestination = this.hotelDestination;
            clonedSelectedRoomAndRateEntity.countryCode = this.countryCode;
            clonedSelectedRoomAndRateEntity.SelectedRoomCategoryName = this.SelectedRoomCategoryName;
            clonedSelectedRoomAndRateEntity.roomDescription = this.roomDescription;

            clonedSelectedRoomAndRateEntity.roomRates = new List<RoomRateEntity>();
            int roomRatesCount = this.roomRates.Count;
            for (int count = 0; count < roomRatesCount; count++)
            {
                RoomRateEntity cloneroomRateEntity = new RoomRateEntity(this.roomRates[count].RoomTypeCode,
                                                                        this.roomRates[count].RatePlanCode);

                RateEntity baseRate = this.roomRates[count].BaseRate;
                if (baseRate != null)
                {
                    cloneroomRateEntity.BaseRate = new RateEntity(baseRate.Rate, baseRate.CurrencyCode);
                }

                RateEntity totalRate = this.roomRates[count].TotalRate;
                if (totalRate != null)
                {
                    cloneroomRateEntity.TotalRate = new RateEntity(totalRate.Rate, totalRate.CurrencyCode);
                }

                RedeemptionPointsEntity pointDetails = this.roomRates[count].PointsDetails;
                if (pointDetails != null)
                {
                    cloneroomRateEntity.PointsDetails = new RedeemptionPointsEntity(pointDetails.PointsRequired,
                                                                                    pointDetails.AwardType);
                }
                clonedSelectedRoomAndRateEntity.roomRates.Add(cloneroomRateEntity);
            }

            return clonedSelectedRoomAndRateEntity;
        }

        #endregion Clone SelectRoomAndRateEntity

        #region Utility Methods

        /// <summary>
        /// The selected room and rate will all contains the common rates
        /// this method will return the rate of the first object
        /// </summary>
        /// <returns></returns>
        public RateEntity GetCommonRate()
        {
            if (null != roomRates && roomRates.Count > 0)
            {
                return roomRates[0].BaseRate;
            }
            return null;
        }

        /// <summary>
        /// The selected room and rate will all contains common rates
        /// this method will return the total rate of the first objecgt
        /// Future use
        /// </summary>
        /// <returns></returns>
        public RateEntity GetCommonTotalRate()
        {
            if (null != roomRates && roomRates.Count > 0)
            {
                return roomRates[0].TotalRate;
            }
            return null;
        }

        /// <summary>
        /// The selected room and rate will all contains the common rates
        /// this method will return the points of the first room rate object
        /// </summary>
        /// <returns></returns>
        public RedeemptionPointsEntity GetCommonBasePoints()
        {
            if (null != roomRates && roomRates.Count > 0)
            {
                return roomRates[0].PointsDetails;
            }
            return null;
        }

        #endregion
    }
}