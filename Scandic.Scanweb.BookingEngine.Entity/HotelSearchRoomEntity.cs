//  Description					:   HotelSearchRoomEntity                                 //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// This class represents HotelSearchRoom entity. 
    /// </summary>
    public class HotelSearchRoomEntity
    {
        /// <summary>
        /// Gets/Sets AdultsPerRoom
        /// </summary>
        public int AdultsPerRoom { get; set; }

        /// <summary>
        /// Gets/Sets ChildrenPerRoom
        /// </summary>
        public int ChildrenPerRoom { get; set; }

        /// <summary>
        /// Gets/Sets ChildrenOccupancyPerRoom
        /// </summary>
        public int ChildrenOccupancyPerRoom { get; set; }

        /// <summary>
        /// Gets/Sets ChildrenDetails
        /// </summary>
        public ChildrensDetailsEntity ChildrenDetails { get; set; }

        /// <summary>
        /// Gets/Sets IsBlocked
        /// </summary>
        public bool IsBlocked { get; set; }

        private string roomBlockReservationNumber = string.Empty;

        /// <summary>
        /// Gets/Sets RoomBlockReservationNumber
        /// </summary>
        public string RoomBlockReservationNumber
        {
            get { return roomBlockReservationNumber; }
            set { roomBlockReservationNumber = value; }
        }

        /// <summary>
        /// Gets/Sets IsRoomConfirmed
        /// </summary>
        public bool IsRoomConfirmed { get; set; }

        /// <summary>
        /// Gets/Sets IsSearchDone
        /// </summary>
        public bool IsSearchDone { get; set; }

        private bool isRoomModifiable = true;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is room modifiable.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is room modifiable; otherwise, <c>false</c>.
        /// </value>
        public bool IsRoomModifiable
        {
            get { return isRoomModifiable; }
            set { isRoomModifiable = value; }
        }

        // Deeplink Cr related changes.
        private string selectedRoomCategory = string.Empty;

        /// <summary>
        /// Gets/Sets SelectedRoomCategory
        /// </summary>
        public string SelectedRoomCategory
        {
            get { return selectedRoomCategory; }
            set { selectedRoomCategory = value; }
        }

        private string selectedRateCategory = string.Empty;

        /// <summary>
        /// Gets/Sets SelectedRateCategory
        /// </summary>
        public string SelectedRateCategory
        {
            get { return selectedRateCategory; }
            set { selectedRateCategory = value; }
        }

        /// <summary>
        /// Gets/Sets SelectedRate
        /// </summary>
        public double SelectedRate { get; set; }

        /// <summary>
        /// Sets/Gets RoomBlockReservationLegNumber
        /// </summary>
        public string RoomBlockReservationLegNumber { get; set; }
    }
}