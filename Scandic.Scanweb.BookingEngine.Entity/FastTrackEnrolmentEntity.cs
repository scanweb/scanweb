//  Description					: FastTrackEnrolmentEntity                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// FastTrackEnrolment entity
    /// </summary>
    public class FastTrackEnrolmentEntity
    {
        //This is set in the enroll profile and displayed in the FGP landing page if this is a fast track Enrollment.
        public string ErrorMessage { get; set; }

        // Fast track code is the promotion code that is needed for a guest to be upgraded.
        public string PromotionCode { get; set; }

        // This is the flag according to which the fast track code will be prepopulated. 
        public bool PrePopulatePromotionCode { get; set; }

        // This is the flag which indicates that wheather the user is directly coming from 
        // campaign landing page or directly hiting the enroll page.
        public bool EnrolmentThroughCampaignLandingPage { get; set; }
    }
}