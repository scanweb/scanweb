using System;
using System.Collections.Generic;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// DeepLinkingEntity
    /// </summary>
    public class DeepLinkingEntity
    {
        private const string VOUCHER_START_IDENTIFIER = "VO";

        public string CO { get; set; }

        public string HO { get; set; }

        public string CUO { get; set; }

        /// <summary>
        /// Arrival day
        /// </summary>
        private int ad;

        public int AD
        {
            get { return ad; }
            set { ad = value; }
        }

        /// <summary>
        /// Arrival Month
        /// </summary>
        private int am;

        public int AM
        {
            get { return am; }
            set { am = value; }
        }

        /// <summary>
        /// Arrival year
        /// </summary>
        private int ay;

        public int AY
        {
            get { return ay; }
            set { ay = value; }
        }

        /// <summary>
        /// Departure Day
        /// </summary>
        private int dd;

        public int DD
        {
            get { return dd; }
            set { dd = value; }
        }

        /// <summary>
        /// Departure Month
        /// </summary>
        private int dm;

        public int DM
        {
            get { return dm; }
            set { dm = value; }
        }

        /// <summary>
        /// Departure Year
        /// </summary>
        private int dy;

        public int DY
        {
            get { return dy; }
            set { dy = value; }
        }

        /// <summary>
        /// Number of Nights
        /// </summary>
        private int nn;

        public int NN
        {
            get { return nn; }
            set
            {
                if (value >= 1 && value <= 99)
                {
                    nn = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Number of nights should be between 1-99");
                }
            }
        }

        /// <summary>
        /// Number of rooms
        /// </summary>
        private int rn = 1;

        public int RN
        {
            get { return rn; }
            set
            {
                if (value >= 1 && value <= 6)
                {
                    rn = value;
                }
                else
                {
                    rn = 1;
                }
            }
        }


        /// <summary>
        /// Gets or sets the list deep linking rooms.
        /// </summary>
        /// <value>The list deep linking rooms.</value>
        public List<DeepLinkingRoomEntity> ListDeepLinkingRooms { get; set; }

        /// <summary>
        /// Special booking code
        /// </summary>
        private string sc;

        public string SC
        {
            get { return sc; }
            set
            {
                if (null == value || value.ToUpper().StartsWith(VOUCHER_START_IDENTIFIER))
                {
                    throw new ArgumentOutOfRangeException("Promotion code should not start with VO");
                }
                sc = value;
            }
        }

        /// <summary>
        /// Corporate/Negotiated code
        /// </summary>
        private string nc;

        public string NC
        {
            get { return nc; }
            set { nc = value; }
        }

        /// <summary>
        /// Is Bonus cheque
        /// </summary>
        private bool bc;

        public bool BC
        {
            get { return bc; }
            set { bc = value; }
        }

        /// <summary>
        /// Voucher code
        /// </summary>
        private string vc;

        public string VC
        {
            get { return vc; }
            set
            {
                if (null == value || !value.ToUpper().StartsWith(VOUCHER_START_IDENTIFIER))
                {
                    throw new ArgumentOutOfRangeException("Voucher code should always start with VO");
                }
                vc = value;
            }
        }

        /// <summary>
        /// Rate Block 
        /// </summary>
        private string rateblock;

        /// <summary>
        /// Get/set the rate block
        /// </summary>
        public string RB
        {
            get { return rateblock; }
            set { rateblock = value; }
        }


        /// <summary>
        /// Gets or sets cmpid
        /// </summary>
        public string CMPID { get; set; }

        public DeepLinkingEntity()
        {
            DateTime arrivalDate = GetDefaultArrivalDate();
            ad = arrivalDate.Day;
            am = arrivalDate.Month;
            ay = arrivalDate.Year;
            DateTime departureDate = GetDefaultDepartureDate();
            dd = departureDate.Day;
            dm = departureDate.Month;
            dy = departureDate.Year;
            rn = 1;
        }

        public DateTime ArrivalDate
        {
            get
            {
                DateTime arrivalDate;
                try
                {
                    arrivalDate = new DateTime(ay, am, ad);
                    DateTime maxDateAllowed = DateTime.Now.AddYears(2);
                    if (arrivalDate < DateTime.Today || arrivalDate > maxDateAllowed)
                    {
                        arrivalDate = GetDefaultArrivalDate();
                    }
                }
                catch (ArgumentOutOfRangeException aore)
                {
                    arrivalDate = GetDefaultArrivalDate();
                }
                catch (ArgumentException ae)
                {
                    arrivalDate = GetDefaultArrivalDate();
                }

                return arrivalDate;
            }
        }

        public DateTime DepartureDate
        {
            get
            {
                DateTime arrivalDate = ArrivalDate;
                DateTime departureDate;
                if (nn != 0)
                {
                    departureDate = new DateTime(arrivalDate.Year, arrivalDate.Month, arrivalDate.Day).AddDays(nn);
                }
                else
                {
                    try
                    {
                        departureDate = new DateTime(dy, dm, dd);
                        DateTime maxDateAllowed = DateTime.Now.AddYears(2).AddDays(99);
                        if (departureDate < DateTime.Today || departureDate > maxDateAllowed)
                        {
                            departureDate = GetDefaultDepartureDate();
                        }
                    }
                    catch (ArgumentOutOfRangeException aore)
                    {
                        departureDate = GetDefaultDepartureDate();
                    }
                    catch (ArgumentException ae)
                    {
                        departureDate = GetDefaultDepartureDate();
                    }
                }
                return departureDate;
            }
        }

        public SearchType TypeOfSearch
        {
            get
            {
                // If all or more than one of these codes is provided, 
                // following will the order of preference in which one of the codes will be picked and
                // corresponding booking type will be considered
                // a. Corporate code � Negotiated booking
                // b. Promotion code � Promotion search
                // c. Voucher code � Voucher search
                // d. Bonus cheque � Bonus cheque search
                if (null != nc)
                    return SearchType.CORPORATE;
                else if (null != sc)
                    return SearchType.REGULAR;
                else if (null != vc)
                    return SearchType.VOUCHER;
                else if (true == bc)
                    return SearchType.BONUSCHEQUE;
                else if (null != rateblock)
                    return SearchType.CORPORATE;
                else
                    return SearchType.REGULAR;
            }
        }

        public string CampaignCode
        {
            get
            {
                switch (TypeOfSearch)
                {
                    case SearchType.CORPORATE:
                        {
                            string returnValue = nc;
                            if (nc == null)
                            {
                                returnValue = rateblock;
                            }
                            return returnValue;
                        }
                    case SearchType.REGULAR:
                        {
                            return sc;
                        }
                    case SearchType.VOUCHER:
                        {
                            return vc;
                        }
                    default:
                        {
                            return null;
                        }
                }
            }
        }

        private string reservationID;

        /// <summary>
        /// Property to get/set Reservation ID for Deeplinks
        /// </summary>
        public string RID
        {
            get { return reservationID; }
            set { reservationID = value; }
        }

        private string lastName;

        /// <summary>
        /// Property to get/set Last Name for Deeplinks
        /// </summary>
        public string LNM
        {
            get { return lastName; }
            set { lastName = value; }
        }

        private string freshSearch;

        /// <summary>
        /// Property to know if Fresh Search needs to initiate via Deeplinks
        /// </summary>
        public string FS
        {
            get { return freshSearch; }
            set { freshSearch = value; }
        }


        /// <summary>
        /// Arrival Date is defaulted to the current date
        /// </summary>
        /// <returns>DateTime</returns>
        private DateTime GetDefaultArrivalDate()
        {
            DateTime now = DateTime.Now;
            return new DateTime(now.Year, now.Month, now.Day);
        }

        /// <summary>
        /// Departure date is defaulted to the Current date + 1
        /// </summary>
        /// <returns>DateTime</returns>
        private DateTime GetDefaultDepartureDate()
        {
            DateTime arrivalDate = GetDefaultArrivalDate();
            return new DateTime(arrivalDate.Year, arrivalDate.Month, arrivalDate.Day).AddDays(1);
        }

        public int DeepLinkingPageId { get; set; }
    }

    /// <summary>
    /// room entity for deep linking
    /// </summary>
    public class DeepLinkingRoomEntity
    {
        /// <summary>
        /// Number of Adults in room
        /// </summary>
        private int an;

        /// <summary>
        /// Number of children in room
        /// </summary>
        private int cn;

        /// <summary>
        /// Gets or sets the AN.
        /// </summary>
        /// <value>The AN.</value>
        public int AN
        {
            get { return an; }
            set
            {
                if (value >= 1 && value <= 6)
                {
                    an = value;
                }
                else
                {
                    an = 1;
                }
            }
        }

        /// <summary>
        /// Gets or sets the CN.
        /// </summary>
        /// <value>The CN.</value>
        public int CN
        {
            get { return cn; }
            set
            {
                if (value >= 0 && value < 6)
                {
                    cn = value;
                }
                else
                {
                    cn = 0;
                }
            }
        }

        public string SelectedRoomCategory { get; set; }

        public string SelectedRateCategory { get; set; }

        public double SelectedRate { get; set; }

        /// <summary>
        /// Gets or sets the list deep linking children.
        /// </summary>
        /// <value>The list deep linking children.</value>
        public List<DeepLinkingChildEntity> ListDeepLinkingChildren { get; set; }
    }

    /// <summary>
    /// Child entity for deep linking
    /// </summary>
    public class DeepLinkingChildEntity
    {
        /// <summary>
        /// Age of the child
        /// </summary>
        private int age;

        /// <summary>
        /// Gets or sets age field
        /// </summary>
        public int Age
        {
            get { return age; }
            set
            {
                if (value >= 0 && value < 13)
                {
                    age = value;
                }
                else
                {
                    age = 0;
                }
            }
        }

        /// <summary>
        /// Gets or sets childAccommodationType field
        /// </summary>
        public ChildAccommodationType ChildAccommodationType { get; set; }
    }
}