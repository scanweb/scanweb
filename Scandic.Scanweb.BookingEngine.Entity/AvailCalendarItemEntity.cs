// <copyright file="AvailCalendarItemEntity.cs" company="Sapient">
// Copyright (c) 2010 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>12-July-2010</date>
// <version>R 2.0</version>
// <summary>Entity class carry the information about each of the availability 
// entities to be displayed in the vailability rate calendar</summary>

using System;
using System.Text;
using System.Collections.Generic;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// AvailCalendarItemEntity
    /// </summary>
    [System.Runtime.Remoting.Contexts.Synchronization()]
    public class AvailCalendarItemEntity : System.ContextBoundObject
    {
        private bool isSearchDone = false;

        /// <summary>
        /// Gets or sets the arrival date.
        /// </summary>
        /// <value>The arrival date.</value>
        public DateTime ArrivalDate { get; set; }

        /// <summary>
        /// Gets or sets the departure date.
        /// </summary>
        /// <value>The departure date.</value>
        public DateTime DepartureDate { get; set; }

        /// <summary>
        /// Gets or sets the min rate text.
        /// </summary>
        /// <value>The min rate text.</value>
        public string MinPerNightRateString { get; set; }

        /// <summary>
        /// Gets or sets the max per night rate string.
        /// </summary>
        /// <value>The max per night rate string.</value>
        public string MaxPerNightRateString { get; set; }

        /// <summary>
        /// Gets or sets the min per stay rate string.
        /// </summary>
        /// <value>The min per stay rate string.</value>
        public string MinPerStayRateString { get; set; }

        /// <summary>
        /// Gets or sets the max per stay rate string.
        /// </summary>
        /// <value>The max per stay rate string.</value>
        public string MaxPerStayRateString { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is search done.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is search done; otherwise, <c>false</c>.
        /// </value>
        public bool IsSearchDone
        {
            get { return isSearchDone; }
            set { isSearchDone = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is rates available.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is rates available; otherwise, <c>false</c>.
        /// </value>
        public bool IsRatesAvailable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is rate displayed.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is rate displayed; otherwise, <c>false</c>.
        /// </value>
        public bool IsRateDisplayed { get; set; }

        /// <summary>
        /// Gets or sets the hotel details.
        /// </summary>
        /// <value>The hotel details.</value>
        public object HotelDetails { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is per stay selected in select rate page.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is per stay selected in select rate page; otherwise, <c>false</c>.
        /// </value>
        public bool IsPerStaySelectedInSelectRatePage { get; set; }

        /// <summary>
        /// Gets the XML with ArrivalDate and  MinRateString.
        /// </summary>     
        public string XML
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<ArrivalDate>").Append(Core.DateUtil.DateToDDMMYYYYString(ArrivalDate)).Append(
                    "</ArrivalDate>");
                if (IsPerStaySelectedInSelectRatePage && isSearchDone)
                {
                    sb.Append("<MinRateString>").Append(MinPerStayRateString).Append("</MinRateString>");
                }
                else
                {
                    sb.Append("<MinRateString>").Append(MinPerNightRateString).Append("</MinRateString>");
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// Gets/Sets RoomStays 
        /// </summary>
        public List<object> RoomStays { get; set; }

        /// <summary>
        /// Gets/Sets TotalSearchRequests
        /// </summary>
        public int TotalSearchRequests { get; set;}


        /// <summary>
        /// Gets/Sets TotalSearchRequests
        /// </summary>
        public int TotalSearchResponses { get; set; }
    }
}