//  Description					: RoomRateEntity                                          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// This class represents room rate.
    /// </summary>
    public class RoomRateEntity
    {
        #region Properties
        /// <summary>
        /// Gets/Sets RoomTypeCode
        /// </summary>
        public string RoomTypeCode { get; set; }
        /// <summary>
        /// Gets/Sets RatePlanCode
        /// </summary>
        public string RatePlanCode { get; set; }
        /// <summary>
        /// Gets/Sets IsSpecialRate
        /// </summary>
        public bool IsSpecialRate { get; set; }
        /// <summary>
        /// Gets/Sets BaseRate
        /// </summary>
        public RateEntity BaseRate { get; set; }
        /// <summary>
        /// Gets/Sets TotalRate
        /// </summary>
        public RateEntity TotalRate { get; set; }
        /// <summary>
        /// Gets/Sets PointsDetails
        /// </summary>
        public RedeemptionPointsEntity PointsDetails { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="roomTypeCode"></param>
        /// <param name="ratePlanCode"></param>
        public RoomRateEntity(string roomTypeCode, string ratePlanCode)
        {
            this.RoomTypeCode = roomTypeCode;
            this.RatePlanCode = ratePlanCode;
        }

        #endregion
    }
}