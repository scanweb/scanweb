namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// This entity holds the POSTItems to the KOMPOST
    /// </summary>
    public class KOMPOSTRequestEntity
    {
        #region Private Methods

        private string url;

        #endregion Private Methods

        #region Public Properies

        public string Email { get; set; }

        public string Zipcode { get; set; }

        public string Country { get; set; }



        public string Sitelanguage { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Title { get; set; }

        public string Street { get; set; }

        public string City { get; set; }

        public string Form { get; set; }

        public string Birthyear { get; set; }

        public string Mobilenumber { get; set; }

        public string Gender { get; set; }

        public string Companyorganization { get; set; }

        public string Companyaddress { get; set; }

        public string Leisure { get; set; }

        public string Meetingandconference { get; set; }

        public string Pressreleases { get; set; }

        public string Corporateinformation { get; set; }

        #endregion Public Properties
    }
}