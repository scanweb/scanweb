//  Description					: PartnershipProgram                                      //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

namespace Scandic.Scanweb.Entity
{

    #region OperationType
    /// <summary>
    /// Holds different operation types
    /// </summary>
    public enum OperationType
    {
        SELECT,
        INSERT,
        UPDATE,
        DELETE
    }

    #endregion OperationType

    #region PartnershipProgram
    /// <summary>
    /// PartnershipProgram entity
    /// </summary>
    public class PartnershipProgram
    {
        /// <summary>
        /// Gets/Sets DisplaySequence 
        /// </summary>
        public int DisplaySequence { get; set; }

        /// <summary>
        /// Gets/Sets OperaId 
        /// </summary>
        public long OperaId { get; set; }

        /// <summary>
        /// Gets/Sets PartnerProgram
        /// </summary>
        public string PartnerProgram { get; set; }

        /// <summary>
        /// Gets/Sets PartnerAccountNo
        /// </summary>
        public string PartnerAccountNo { get; set; }

        /// <summary>
        /// Gets/Sets Operation
        /// </summary>
        public OperationType Operation { get; set; }
    }

    #endregion PartnershipProgram

    #region Loyalty Transaction Comparers
    /// <summary>
    /// PartnershipProgramComparer
    /// </summary>
    public class PartnershipProgramComparer : IComparer<PartnershipProgram>
    {
        /// <summary>
        /// Compares to partnership programs.
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public int Compare(PartnershipProgram first, PartnershipProgram second)
        {
            return first.DisplaySequence.CompareTo(second.DisplaySequence);
        }
    }

    #endregion Loyalty Transaction Comparers
}