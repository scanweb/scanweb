
namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// HotelRoomRateEntity
    /// </summary>
    public class HotelRoomRateEntity
    {
        #region Used for Redemption booking

        #endregion

        public string RoomtypeCode { get; set; }

        public string RatePlanCode { get; set; }

        public bool IsSpecialRate { get; set; }

        public string AwardType { get; set; }

        public double Points { get; set; }

        public RateEntity Rate { get; set; }

        public RateEntity TotalRate { get; set; }

        public RateEntity SubTotal{ get; set;}

        public TaxesEntity Taxes { get; set; }

        public RateEntity DepositAmount { get; set; }

        public RateEntity CurrentBalance { get; set; }

        public bool IsSessionBooking { get; set; }
    }
}