//  Description					:   RoomEntity                                             //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// This class represents the class entity.
    /// </summary>
    public class RoomEntity
    {
        protected RateEntity minRateForEachRoom;

        /// <summary>
        /// The Maximum avialable rate per night
        /// </summary>
        protected RateEntity maxRateForEachRoom;

        /// <summary>
        /// The Minimum available rate per stay
        /// </summary>
        /// <remarks>
        /// </remarks>
        protected RateEntity minRatePerStayForEachRoom;

        /// <summary>
        /// The Maximum avialable rate per stay
        /// </summary>
        /// <remarks>
        /// </remarks>
        protected RateEntity maxRatePerStayForEachRoom;

        /// <summary>
        /// minPointsForEachRoom
        /// </summary>
        protected RedeemptionPointsEntity minPointsForEachRoom;

        /// <summary>
        /// maxPointsForEachRoom
        /// </summary>
        protected RedeemptionPointsEntity maxPointsForEachRoom;


        /// <summary>
        /// Gets/Sets <see cref="minRate"/>
        /// </summary>
        public RateEntity MinRateForEachRoom
        {
            get { return minRateForEachRoom; }
            set { minRateForEachRoom = value; }
        }

        /// <summary>
        /// Gets/Sets <see cref="maxRate"/>
        /// </summary>
        public RateEntity MaxRateForEachRoom
        {
            get { return maxRateForEachRoom; }
            set { maxRateForEachRoom = value; }
        }

        /// <summary>
        /// Gets/Sets MinRatePerStayForEachRoom
        /// </summary>
        public RateEntity MinRatePerStayForEachRoom
        {
            get { return minRatePerStayForEachRoom; }
            set { minRatePerStayForEachRoom = value; }
        }

        /// <summary>
        /// Gets/Sets MaxRatePerStayForEachRoom <see cref="maxRate"/>
        /// </summary>        
        public RateEntity MaxRatePerStayForEachRoom
        {
            get { return maxRatePerStayForEachRoom; }
            set { maxRatePerStayForEachRoom = value; }
        }

        /// <summary>
        /// Gets/Sets MinPointsForEachRoom
        /// </summary>
        public RedeemptionPointsEntity MinPointsForEachRoom
        {
            get { return minPointsForEachRoom; }
            set { minPointsForEachRoom = value; }
        }

        /// <summary>
        /// Gets/Sets MaxPointsForEachRoom
        /// </summary>
        public RedeemptionPointsEntity MaxPointsForEachRoom
        {
            get { return maxPointsForEachRoom; }
            set { maxPointsForEachRoom = value; }
        }

        /// <summary>
        /// List of available room types in OWS
        /// </summary>
        private List<RoomTypeEntity> roomTypes = new List<RoomTypeEntity>();

        /// <summary>
        /// Gets/Sets RoomTypes
        /// </summary>
        public List<RoomTypeEntity> RoomTypes
        {
            get { return roomTypes; }
            set { roomTypes = value; }
        }

        /// <summary>
        /// List of available rate plans for the selected dates and search criteria
        /// </summary>
        private List<RatePlanEntity> ratePlans = new List<RatePlanEntity>();

        /// <summary>
        /// Gets/Sets RatePlans
        /// </summary>
        public List<RatePlanEntity> RatePlans
        {
            get { return ratePlans; }
            set { ratePlans = value; }
        }

        /// <summary>
        /// List of available room rate combinations for the selected dates and search criteria
        /// </summary>
        private List<RoomRateEntity> roomRates = new List<RoomRateEntity>();

        /// <summary>
        /// Gets/Sets RoomRates
        /// </summary>
        public List<RoomRateEntity> RoomRates
        {
            get { return roomRates; }
            set { roomRates = value; }
        }

        /// <summary>
        /// Gets/Sets RateCategories
        /// </summary>
        public List<RateCategory> RateCategories { get; set; }


        /// <summary>
        /// The minium available rate in each Rate category list for a room type.
        /// For example if the room type TR has multiple rate types of a rate cateogry Flex
        /// are mapped and returned from OWS only the minimum value of the rate category should
        /// be considered
        /// 
        /// 1. TR ==> RA1 = 120SEK
        /// 2. TR ==> RA2 = 130SEK
        /// 3. TR ==> RA3 = 160SEK
        /// 4. TR ==> RA4 = 110SEK
        /// 
        /// Then the minimum rate i.e, TR ==> RA4 will be considered and added to the
        /// <code>minBaseRateInEachRateCategoryList</code>
        /// 
        /// The base rate or the rate per night will be considered here
        /// </summary>
        private List<RoomRateEntity> minBaseRateInEachRateCategoryList = new List<RoomRateEntity>();

        /// <summary>
        /// Gets/Sets MinBaseRateInEachRateCategoryList
        /// </summary>
        public List<RoomRateEntity> MinBaseRateInEachRateCategoryList
        {
            get { return minBaseRateInEachRateCategoryList; }
            set { minBaseRateInEachRateCategoryList = value; }
        }

        /// <summary>
        /// Same as <code>minBaseRateInEachCategoryList</code> except the change is the TotalRate
        /// will be considered
        /// This is used for future use if there is any difference in the BaseRate and TotalRate
        /// </summary>
        private List<RoomRateEntity> minTotalRateInEachRateCategoryList = new List<RoomRateEntity>();

        /// <summary>
        /// Gets/Sets MinTotalRateInEachRateCategoryList
        /// </summary>
        public List<RoomRateEntity> MinTotalRateInEachRateCategoryList
        {
            get { return minTotalRateInEachRateCategoryList; }
            set { minTotalRateInEachRateCategoryList = value; }
        }
    }
}