﻿
namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// ChargesEntity
    /// </summary>
    public class ChargeEntity
    {
        /// <summary>
        /// The description
        /// </summary>
        private string description;

        /// <summary>
        /// The amount value
        /// </summary>
        private RateEntity amount;

        /// <summary>
        /// Gets/Sets Description
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        /// <summary>
        /// Gets/Sets Amount value
        /// </summary>
        public RateEntity Amount
        {
            get { return amount; }
            set { amount = value; }
        }
    }
}
