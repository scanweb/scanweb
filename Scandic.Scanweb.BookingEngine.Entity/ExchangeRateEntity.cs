﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scandic.Scanweb.Entity
{
    public class ExchangeRateEntity
    {
        public string FromCurrency { get; set; }
        public List<ConvertedCurrencyEntity> ConvertedCurrencies { get; set; }
    }
    public class ConvertedCurrencyEntity
    {
        public string ToCurrency { get; set; }
        public double UnitAmount { get; set; }
    }
}
