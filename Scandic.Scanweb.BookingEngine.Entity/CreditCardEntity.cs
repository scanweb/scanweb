using System;

namespace Scandic.Scanweb.Entity
{
    /// <summary>
    /// The class capturing the all the credit card related fields.
    /// </summary>
    [Serializable]
    public class CreditCardEntity
    {
        /// <summary>
        /// Name on the Card
        /// </summary>
        private string nameOnCard;

        /// <summary>
        /// The card company name like Visa, Mastercard etc., the Opera equivalent
        /// of the card types like (VA, MX etc.,) to be assigned to this field
        /// </summary>
        private string cardType;

        /// <summary>
        /// The credit card number
        /// </summary>
        private string cardNumber;

        /// <summary>
        /// Display Sequence of the credit card in opera.
        /// </summary>
        private int displaySequence;

        /// <summary>
        /// The expiry date of the credit card. The expiry is encapsulated into 
        /// a entity ExpiryDateEntiry consisting of the month and year
        /// </summary>
        private ExpiryDateEntity expiryDate;

        /// <summary>
        /// CreditCardEntity
        /// </summary>
        /// <param name="nameOnCard"></param>
        /// <param name="cardType"></param>
        /// <param name="cardNumber"></param>
        /// <param name="expiryDate"></param>
        public CreditCardEntity(string nameOnCard, string cardType, string cardNumber, ExpiryDateEntity expiryDate)
        {
            this.nameOnCard = nameOnCard;
            this.cardType = cardType;
            this.cardNumber = cardNumber;
            this.expiryDate = expiryDate;
        }

        /// <summary>
        /// CreditCardEntity
        /// </summary>
        /// <param name="nameOnCard"></param>
        /// <param name="cardType"></param>
        /// <param name="cardNumber"></param>
        /// <param name="expiryDate"></param>
        /// <param name="displaySequence"></param>
        public CreditCardEntity
            (string nameOnCard, string cardType, string cardNumber, ExpiryDateEntity expiryDate, int displaySequence)
        {
            this.nameOnCard = nameOnCard;
            this.cardType = cardType;
            this.cardNumber = cardNumber;
            this.expiryDate = expiryDate;
            this.displaySequence = displaySequence;
        }

        public string NameOnCard
        {
            get { return nameOnCard; }
        }

        public string CardType
        {
            get { return cardType; }
        }

        public string CardNumber
        {
            get { return cardNumber; }
            set { cardNumber = value; }
        }

        public ExpiryDateEntity ExpiryDate
        {
            get { return expiryDate; }
        }

        public int DisplaySequence
        {
            get { return displaySequence; }
        }
    }
}