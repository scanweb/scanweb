using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;


namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Functions to provide SiteCatalyst with logging information
    /// </summary>
    public partial class SiteCatalyst : ScandicUserControlBase
    {
        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            int confirmationPageID = ((PageReference)RootPage["ReservationConfirmationPage"]).ID;
            if (CurrentPage.PageLink.ID == confirmationPageID)
            {
                SetConfirmationPageVariables();
            }
            DataBind();
        }

        /// <summary>
        /// SetConfirmationPageVariables
        /// </summary>
        private void SetConfirmationPageVariables()
        {
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            if (null != hotelSearch)
            {
                string amount = "";
                string currencyCode = "";
                if (SogetiVariableSessionWrapper.TotalAmountForThirdParty != null)
                {
                    try
                    {
                        string currencyString = SogetiVariableSessionWrapper.TotalAmountForThirdParty;
                        string[] currencyStringArray = currencyString.Split(' ');
                        if (currencyStringArray.Length > 0)
                        {
                            amount = currencyStringArray[0];
                        }
                        if (currencyStringArray.Length > 1)
                        {
                            currencyCode = currencyStringArray[1];
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
                if (hotelSearch.SearchingType == SearchType.REGULAR || hotelSearch.SearchingType == SearchType.CORPORATE)
                {
                    currencyCodeLiteral.Text = currencyCode;
                }
                else
                {
                    currencyCodeLiteral.Text = "EUR";
                }

                if (WebUtil.IsAnyRoomHavingSaveCategory(HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable))
                {
                    eVar25Literal.Text = "Credit Card";
                }

                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                    eVar25Literal.Text = "Points";

                if (hotelSearch.SearchingType == SearchType.VOUCHER)
                    eVar25Literal.Text = "Gift Voucher";

                if (hotelSearch.SearchingType == SearchType.BONUSCHEQUE)
                    eVar25Literal.Text = "Bonus";
                if (hotelSearch.SearchingType == SearchType.BONUSCHEQUE ||
                    hotelSearch.SearchingType == SearchType.REDEMPTION)
                    eVar26Literal.Text = amount;
                if (hotelSearch.SearchingType == SearchType.BONUSCHEQUE)
                    eVar27Literal.Text = "Bonus Code";

                if (hotelSearch.SearchingType == SearchType.VOUCHER)
                    eVar27Literal.Text = "Voucher Code";
            }
        }

        /// <summary>
        /// Returns the name of the current page
        /// </summary>
        /// <returns>Page name</returns>
        protected string GetPageName()
        {
            string pageName =
                DataFactory.Instance.GetPage(CurrentPage.PageLink,
                                             new LanguageSelector(CurrentPage.MasterLanguageBranch.ToString())).PageName;
            if (ReservationNumberSessionWrapper.IsAppErrorPageTriggeredFromThePaymentProcessingPage)
            {
                pageName = PaymentConstants.Payment + pageName;
                ReservationNumberSessionWrapper.IsAppErrorPageTriggeredFromThePaymentProcessingPage = false;
            }
            if (String.Equals(Request.QueryString["SessionInvalid"], AppConstants.TRUE, StringComparison.CurrentCultureIgnoreCase))
            {
                pageName = string.Format("{0}{1}", pageName, "-Scanweb-SessionInvalid");
            }
            if (string.Equals(Convert.ToString(Request.QueryString["ScanwebCnfrmn"]), AppConstants.TRUE, StringComparison.InvariantCultureIgnoreCase))
            {
                pageName = string.Format("{0}{1}{2}", pageName, AppConstants.HYPHEN, AppConstants.CONFIRMATION);
            }
            return pageName;
        }

        /// <summary>
        /// Returns the Channel name for the current page
        /// </summary>
        /// <returns>Channel name as string</returns>        
        protected string GetChannel()
        {
            string sChannel = string.Empty;

            bool isRoot = false;
            int iLevels = 1;
            PageData pd = CurrentPage;
            sChannel = CurrentPage.PageName;
            do
            {
                pd = GetPage(pd.ParentLink);
                if (pd.PageLink != PageReference.StartPage && pd.PageLink != PageReference.RootPage)
                {
                    sChannel = pd.PageName + ";" + sChannel;
                    iLevels++;
                }
                else
                    isRoot = true;
            } while (!isRoot);

            if (iLevels > 1)
            {
                string[] channels = sChannel.Split(';');
                sChannel = channels[0];
            }
            return sChannel;
        }

        /// <summary>
        /// GetProp20
        /// </summary>
        /// <returns>Prop20</returns>
        protected string GetProp20()
        {
            string sProp20 = string.Empty;

            bool isRoot = false;
            int iLevels = 1;
            PageData pd = CurrentPage;
            sProp20 = CurrentPage.PageName;
            do
            {
                pd = GetPage(pd.ParentLink);
                if (pd.PageLink != PageReference.StartPage && pd.PageLink != PageReference.RootPage)
                {
                    sProp20 = pd.PageName + ";" + sProp20;
                    iLevels++;
                }
                else
                    isRoot = true;
            } while (!isRoot);

            if (iLevels > 1)
            {
                string[] channels = sProp20.Split(';');
                sProp20 = channels[1];
            }
            else
                sProp20 = string.Empty;

            return sProp20;
        }

        /// <summary>
        /// Returns scCheckout for report Commerce>Shopping cart>Checkouts
        /// </summary>
        /// <returns>"scCheckout"</returns>
        protected string GetScCheckout()
        {
            PageData oPage = GetPage(PageReference.RootPage);
            int pageLinkID = ((PageReference)oPage["ReservationBookingDetailsPage"]).ID;
            string scCheckout = string.Empty;
            if (CurrentPage.PageLink.ID == pageLinkID)
            {
                scCheckout = "scCheckout";
            }
            return scCheckout;
        }

        /// <summary>
        /// GetProp
        /// </summary>
        /// <returns>Prop</returns>
        protected string GetProp(string sID)
        {
            try
            {
                switch (sID)
                {
                    case "11":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string location = string.Empty;

                            if (!SearchDestinationSessionWrapper.SearchDestinationUpdated)
                            {
                                if (CurrentPage.PageLink.ID == ratePageLinkID || CurrentPage.PageLink.ID == hotelPageLinkID)
                                {
                                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                    if (null != hotelSearch && hotelSearch.SearchedFor != null)
                                    {
                                        location = SearchDestinationSessionWrapper.SearchDestination;
                                    }
                                }
                            }
                            return location;
                        }
                    case "12":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string noOfNights = string.Empty;
                            if (CurrentPage.PageLink.ID == ratePageLinkID || CurrentPage.PageLink.ID == hotelPageLinkID)
                            {
                                HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                if (null != hotelSearch)
                                {
                                    noOfNights = hotelSearch.NoOfNights.ToString();
                                }
                            }
                            return noOfNights;
                        }
                    case "13":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string noOfRooms = string.Empty;
                            if (CurrentPage.PageLink.ID == ratePageLinkID || CurrentPage.PageLink.ID == hotelPageLinkID)
                            {
                                HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                if (null != hotelSearch)
                                {
                                    noOfRooms = hotelSearch.RoomsPerNight.ToString();
                                }
                            }
                            return noOfRooms;
                        }
                    case "14":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string arrivalDate = string.Empty;
                            if (CurrentPage.PageLink.ID == ratePageLinkID || CurrentPage.PageLink.ID == hotelPageLinkID)
                            {
                                HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                if (null != hotelSearch)
                                {
                                    arrivalDate = hotelSearch.ArrivalDate.ToShortDateString();
                                }
                            }
                            return arrivalDate;
                        }
                    case "15":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string noOfAdults = string.Empty;
                            int noofAdults = 0;
                            if (CurrentPage.PageLink.ID == ratePageLinkID || CurrentPage.PageLink.ID == hotelPageLinkID)
                            {
                                HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                if (null != hotelSearch)
                                {
                                    if (hotelSearch.ListRooms != null && hotelSearch.ListRooms.Count > 0)
                                    {
                                        foreach (HotelSearchRoomEntity room in hotelSearch.ListRooms)
                                        {
                                            if (room != null)
                                                noofAdults = noofAdults + room.AdultsPerRoom;
                                        }
                                    }
                                    noOfAdults = noofAdults.ToString();
                                }
                            }
                            return noOfAdults;
                        }
                    case "16":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string noOfChildren = string.Empty;
                            int noofChild = 0;
                            if (CurrentPage.PageLink.ID == ratePageLinkID || CurrentPage.PageLink.ID == hotelPageLinkID)
                            {
                                HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                if (null != hotelSearch)
                                {
                                    if (hotelSearch.ListRooms != null && hotelSearch.ListRooms.Count > 0)
                                    {
                                        foreach (HotelSearchRoomEntity room in hotelSearch.ListRooms)
                                        {
                                            if (room != null)
                                                noofChild = noofChild + room.ChildrenPerRoom;
                                        }
                                    }
                                    noOfChildren = noofChild.ToString();
                                }
                            }
                            return noOfChildren;
                        }
                    case "17":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string arvDateDepDate = string.Empty;
                            if (CurrentPage.PageLink.ID == ratePageLinkID || CurrentPage.PageLink.ID == hotelPageLinkID)
                            {
                                HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                if (null != hotelSearch)
                                {
                                    arvDateDepDate = hotelSearch.ArrivalDate.ToShortDateString() + "-" +
                                                     hotelSearch.DepartureDate.ToShortDateString();
                                }
                            }
                            return arvDateDepDate;
                        }
                    case "18":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string noOfResults = string.Empty;
                            IList<BaseRoomRateDetails> listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                            if (CurrentPage.PageLink.ID == hotelPageLinkID)
                            {
                                if (listRoomRateDetails != null)
                                {
                                    if (listRoomRateDetails.Count > 0)
                                        noOfResults = "1";
                                    else
                                        noOfResults = "Zero";
                                }
                            }
                            return noOfResults;
                        }
                    case "21":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            string userLoginUsingEnroll = string.Empty;
                            SortedList<string, GuestInformationEntity> guestsList =
                                GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
                            if (CurrentPage.PageLink.ID == pageLinkID)
                            {
                                bool flag = false;
                                GuestInformationEntity guest = null;

                                if (guestsList != null)
                                {
                                    foreach (string key in guestsList.Keys)
                                    {
                                        guest = guestsList[key];
                                        if (null != guest)
                                        {
                                            if (!string.IsNullOrEmpty(guest.GuestAccountNumber))
                                            {
                                                flag = true;
                                                break;
                                            }
                                        }
                                    }
                                }

                                if ((bool)Reservation2SessionWrapper.IsUserEnrollInBookingModule)
                                    userLoginUsingEnroll = "Booking enroll checkbox";
                                else if (flag && !(bool)UserLoggedInSessionWrapper.UserLoggedIn)
                                    userLoginUsingEnroll = "FGP number entered manually";
                            }

                            if (IsEnrolledUserSessionWrapper.IsEnrolledUserFromTopRightButton)
                            {
                                userLoginUsingEnroll = "Top right corner";
                            }
                            else if (IsEnrolledUserSessionWrapper.IsEnrolledUserFromScandicFriends)
                            {
                                userLoginUsingEnroll = "Scandic Friends page";
                            }

                            return userLoginUsingEnroll;
                        }
                    case "22":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            string userLoginUsingEnroll = string.Empty;
                            SortedList<string, GuestInformationEntity> guestsList =
                                GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
                            if (CurrentPage.PageLink.ID == pageLinkID)
                            {
                                if (((bool)Reservation2SessionWrapper.IsUserEnrollInBookingModule) ||
                                    (bool)Reservation2SessionWrapper.IsLoginFromBookingPageLoginOrTopLogin)
                                {
                                    userLoginUsingEnroll = "booking page";
                                }
                                else
                                {
                                    bool flag = false;
                                    GuestInformationEntity guest = null;
                                    if (guestsList != null)
                                    {
                                        foreach (string key in guestsList.Keys)
                                        {
                                            guest = guestsList[key];
                                            if (null != guest)
                                            {
                                                if (!string.IsNullOrEmpty(guest.GuestAccountNumber))
                                                {
                                                    flag = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (flag && !(bool)UserLoggedInSessionWrapper.UserLoggedIn)
                                        userLoginUsingEnroll = "booking page";
                                }
                            }
                            return userLoginUsingEnroll;
                        }
                    case "24":
                        {
                            return CurrentPage.PageName;
                        }
                    case "34":
                        {
                            if (UserLoggedInSessionWrapper.UserLoggedIn)
                            {
                                return "FGP Logged In";
                            }
                            else
                                return "Logged Out";
                        }
                    default:
                        {
                            return string.Empty;
                        }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected string GetEvar(string sID)
        {
            try
            {
                switch (sID)
                {
                    case "2":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageTypeID =
                                PageType.Load(new Guid(ConfigurationManager.AppSettings["OfferPageTypeGUID"])).ID;
                            int confPageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            int selectRatePageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            int bookingDetailsPageLinkID = ((PageReference)oPage["ReservationBookingDetailsPage"]).ID;

                            string campaignID = string.Empty;
                            try
                            {
                                if (CurrentPage.PageTypeID == pageTypeID)
                                {
                                    campaignID = CurrentPage["PromotionCode"] as string ?? string.Empty;
                                }
                                else if (CurrentPage.PageLink.ID == selectRatePageLinkID ||
                                            CurrentPage.PageLink.ID == bookingDetailsPageLinkID ||
                                            CurrentPage.PageLink.ID == confPageLinkID)
                                {
                                    if (!BookingEngineSessionWrapper.IsModifyBooking)
                                    {
                                        campaignID = SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode;
                                    }
                                    else
                                    {
                                        campaignID = BookingEngineSessionWrapper.BookingDetails.HotelSearch.CampaignCode;
                                    }

                                    //campaignID = (campaignID == null) ? string.Empty : campaignID;
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return campaignID;
                        }
                    case "4":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            string bookingWindow = string.Empty;
                            TimeSpan spanDuration;
                            try
                            {
                                if (CurrentPage.PageLink.ID == pageLinkID)
                                {
                                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.CloneSearchCriteria;
                                    if (null != hotelSearch)
                                    {
                                        spanDuration = hotelSearch.ArrivalDate.Subtract(System.DateTime.Now);
                                        bookingWindow = spanDuration.Days.ToString() + " days";
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return bookingWindow;
                        }
                    case "5":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            string depDate = string.Empty;
                            try
                            {
                                if (CurrentPage.PageLink.ID == pageLinkID)
                                {
                                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.CloneSearchCriteria;
                                    if (null != hotelSearch)
                                    {
                                        depDate = hotelSearch.DepartureDate.ToShortDateString();
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return depDate;
                        }
                    case "6":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            string tripDuration = string.Empty;
                            TimeSpan spanDuration;
                            try
                            {
                                if (CurrentPage.PageLink.ID == pageLinkID)
                                {
                                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.CloneSearchCriteria;
                                    if (null != hotelSearch)
                                    {
                                        spanDuration = hotelSearch.DepartureDate.Subtract(hotelSearch.ArrivalDate);
                                        tripDuration = spanDuration.Days.ToString() + " days";
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return tripDuration;
                        }
                    case "7":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            string membershipLevel = string.Empty;
                            try
                            {
                                if (CurrentPage.PageLink.ID == pageLinkID)
                                {
                                    if (SogetiVariableSessionWrapper.MemberShipLevelForThirdParty != null)
                                        membershipLevel = SogetiVariableSessionWrapper.MemberShipLevelForThirdParty;
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return membershipLevel;
                        }

                    case "8":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            string roomsPerNight = string.Empty;
                            try
                            {
                                if (CurrentPage.PageLink.ID == pageLinkID)
                                {
                                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.CloneSearchCriteria;
                                    if (null != hotelSearch)
                                    {
                                        roomsPerNight = hotelSearch.RoomsPerNight.ToString();
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return roomsPerNight;
                        }
                    case "9":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            string roomType = string.Empty;
                            string roomTypeCode = string.Empty;
                            if (CurrentPage.PageLink.ID == pageLinkID)
                            {
                                try
                                {
                                    roomTypeCode = BookingEngineSessionWrapper.BookingDetails.HotelRoomRate.RoomtypeCode as string ??
                                                   string.Empty;
                                    if (roomTypeCode.Length > 1)
                                    {
                                        Dictionary<string, RoomCategory> roomCategoryMap =
                                            ContentDataAccess.GetRoomTypeCategoryMap();
                                        if (roomCategoryMap.ContainsKey(roomTypeCode))
                                            return roomCategoryMap[roomTypeCode].RoomCategoryId;
                                    }
                                }
                                catch (Exception)
                                {
                                    return string.Empty;
                                }
                            }
                            return roomType;
                        }
                    case "10":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            int bookingTypeID = 0;
                            string bookingType = string.Empty;
                            try
                            {
                                if (CurrentPage.PageLink.ID == pageLinkID)
                                {
                                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.CloneSearchCriteria;
                                    if (null != hotelSearch)
                                    {
                                        bookingTypeID = (int)hotelSearch.SearchingType;
                                        if (bookingTypeID == 0 && string.IsNullOrEmpty(hotelSearch.CampaignCode))
                                            bookingType = "Regular Booking";
                                        else if (bookingTypeID == 0 && !string.IsNullOrEmpty(hotelSearch.CampaignCode))
                                            bookingType = "Promotion Booking";
                                        else if (bookingTypeID == 1)
                                            bookingType = "Negotiated rate Booking";
                                        else if (bookingTypeID == 2)
                                            bookingType = "Bonus Cheque";
                                        else if (bookingTypeID == 3)
                                            bookingType = "Reward Night";
                                        else if (bookingTypeID == 4)
                                            bookingType = "Gift Voucher";
                                        else
                                            bookingType = "Undefined booking type";
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return bookingType;
                        }

                    case "11":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string location = string.Empty;
                            try
                            {
                                if (!SearchDestinationSessionWrapper.SearchDestinationUpdated)
                                {
                                    if (CurrentPage.PageLink.ID == ratePageLinkID ||
                                        CurrentPage.PageLink.ID == hotelPageLinkID)
                                    {
                                        HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                        if (null != hotelSearch && hotelSearch.SearchedFor != null)
                                        {
                                            location = SearchDestinationSessionWrapper.SearchDestination;
                                            SearchDestinationSessionWrapper.SearchDestinationUpdated = true;
                                        }
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return location;
                        }
                    case "12":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string noOfNights = string.Empty;
                            try
                            {
                                if (CurrentPage.PageLink.ID == ratePageLinkID ||
                                    CurrentPage.PageLink.ID == hotelPageLinkID)
                                {
                                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                    if (null != hotelSearch)
                                    {
                                        noOfNights = hotelSearch.NoOfNights.ToString() as string ?? string.Empty;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return noOfNights;
                        }
                    case "13":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string noOfRooms = string.Empty;
                            try
                            {
                                if (CurrentPage.PageLink.ID == ratePageLinkID ||
                                    CurrentPage.PageLink.ID == hotelPageLinkID)
                                {
                                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                    if (null != hotelSearch)
                                    {
                                        noOfRooms = hotelSearch.RoomsPerNight.ToString() as string ?? string.Empty;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return noOfRooms;
                        }
                    case "14":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string arrivalDate = string.Empty;
                            try
                            {
                                if (CurrentPage.PageLink.ID == ratePageLinkID ||
                                    CurrentPage.PageLink.ID == hotelPageLinkID)
                                {
                                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                    if (null != hotelSearch)
                                    {
                                        arrivalDate = hotelSearch.ArrivalDate.ToShortDateString();
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return arrivalDate;
                        }
                    case "15":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string noOfAdults = string.Empty;
                            int noofAdults = 0;
                            try
                            {
                                if (CurrentPage.PageLink.ID == ratePageLinkID ||
                                    CurrentPage.PageLink.ID == hotelPageLinkID)
                                {
                                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                    if (null != hotelSearch)
                                    {
                                        if (hotelSearch.ListRooms != null && hotelSearch.ListRooms.Count > 0)
                                        {
                                            foreach (HotelSearchRoomEntity room in hotelSearch.ListRooms)
                                            {
                                                if (room != null)
                                                    noofAdults = noofAdults + room.AdultsPerRoom;
                                            }
                                        }
                                        noOfAdults = noofAdults.ToString();
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return noOfAdults;
                        }
                    case "16":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string noOfChildren = string.Empty;
                            int noofChild = 0;
                            try
                            {
                                if (CurrentPage.PageLink.ID == ratePageLinkID ||
                                    CurrentPage.PageLink.ID == hotelPageLinkID)
                                {
                                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                    if (null != hotelSearch)
                                    {
                                        if (hotelSearch.ListRooms != null && hotelSearch.ListRooms.Count > 0)
                                        {
                                            foreach (HotelSearchRoomEntity room in hotelSearch.ListRooms)
                                            {
                                                if (room != null)
                                                    noofChild = noofChild + room.ChildrenPerRoom;
                                            }
                                        }
                                        noOfChildren = noofChild.ToString();
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return noOfChildren;
                        }
                    case "17":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string arvDateDepDate = string.Empty;
                            try
                            {
                                if (CurrentPage.PageLink.ID == ratePageLinkID ||
                                    CurrentPage.PageLink.ID == hotelPageLinkID)
                                {
                                    HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                                    if (null != hotelSearch)
                                    {
                                        arvDateDepDate = hotelSearch.ArrivalDate.ToShortDateString() + "-" +
                                                         hotelSearch.DepartureDate.ToShortDateString();
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return arvDateDepDate;
                        }
                    case "18":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int ratePageLinkID = ((PageReference)oPage["ReservationSelectHotelPage"]).ID;
                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            IList<BaseRoomRateDetails> listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                            string noOfResults = string.Empty;
                            try
                            {
                                if (CurrentPage.PageLink.ID == hotelPageLinkID)
                                {
                                    if (listRoomRateDetails != null)
                                    {
                                        if (listRoomRateDetails.Count > 0)
                                            noOfResults = "1";
                                        else
                                            noOfResults = "Zero";
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return noOfResults;
                        }
                    case "19":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            string purchaseID = string.Empty;
                            try
                            {
                                if (CurrentPage.PageLink.ID == pageLinkID)
                                {
                                    if (SogetiVariableSessionWrapper.ReservationNumberForThirdParty != string.Empty)
                                        purchaseID = SogetiVariableSessionWrapper.ReservationNumberForThirdParty;
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return purchaseID;
                        }
                    case "20":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            string purchaseID = string.Empty;
                            bool test = false;
                            try
                            {
                                if (CurrentPage.PageLink.ID == pageLinkID &&
                                    BookingEngineSessionWrapper.IsModifyBooking)
                                {
                                    if (SogetiVariableSessionWrapper.ReservationNumberForThirdParty != string.Empty)
                                        purchaseID = SogetiVariableSessionWrapper.ReservationNumberForThirdParty;
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return purchaseID;
                        }
                    case "21":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["CancelledBookingPage"]).ID;
                            string purchaseID = string.Empty;
                            try
                            {
                                if (CurrentPage.PageLink.ID == pageLinkID)
                                {
                                    if (SogetiVariableSessionWrapper.ReservationNumberForThirdParty != string.Empty)
                                        purchaseID = SogetiVariableSessionWrapper.ReservationNumberForThirdParty;
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return purchaseID;
                        }
                    case "28":
                        {
                            string returnValue = string.Empty;
                            PageData rootPage = GetPage(PageReference.RootPage);
                            PageReference pgReference = rootPage["XFormPage"] as PageReference;
                            if (pgReference != null)
                            {
                                PageData formPageData = GetPage(pgReference);
                                if (formPageData != null)
                                {
                                    if (CurrentPage.PageTypeID == formPageData.PageTypeID)
                                    {
                                        returnValue = CurrentPage.PageName;
                                    }
                                }
                            }
                            return returnValue;
                        }
                    case "29":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageTypeID =
                                PageType.Load(new Guid(ConfigurationManager.AppSettings["OfferPageTypeGUID"])).ID;
                            string offerPagename = string.Empty;
                            try
                            {
                                if (CurrentPage.PageTypeID == pageTypeID)
                                {
                                    offerPagename = CurrentPage.PageName;
                                }
                            }
                            catch (Exception)
                            {
                            }
                            return offerPagename;
                        }
                    case "30":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            string guestCountryName = string.Empty;
                            OrderedDictionary countryCodes = null;
                            if (CurrentPage.PageLink.ID == pageLinkID)
                            {
                                try
                                {
                                    if (GuestBookingInformationSessionWrapper.GuestBookingInformation != null)
                                    {
                                        countryCodes = DropDownService.GetCountryCodes();
                                        if (countryCodes != null)
                                        {
                                            guestCountryName =
                                                countryCodes[GuestBookingInformationSessionWrapper.GuestBookingInformation.Country] as string ??
                                                string.Empty;
                                            guestCountryName = guestCountryName.Trim();
                                        }
                                    }
                                }
                                catch (Exception)
                                {
                                    return string.Empty;
                                }
                            }
                            return guestCountryName;
                        }
                    case "34":
                        {
                            if (UserLoggedInSessionWrapper.UserLoggedIn)
                            {
                                return "FGP Logged In";
                            }
                            else
                                return "Logged Out";
                        }

                    case "37":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);

                            int hotelPageLinkID = ((PageReference)oPage["ReservationSelectRatePage"]).ID;
                            string rateValue = string.Empty;
                            try
                            {
                                if (CurrentPage.PageLink.ID == hotelPageLinkID)
                                {
                                    rateValue = Reservation2SessionWrapper.SelectedCalenderItemRateValueStatus as string ??
                                                string.Empty;
                                }
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                            return rateValue;
                        }
                    case "38":
                        {
                            PageData oPage = GetPage(PageReference.RootPage);
                            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
                            string userLoginUsingEnroll = string.Empty;
                            SortedList<string, GuestInformationEntity> guestsList =
                                GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
                            if (CurrentPage.PageLink.ID == pageLinkID)
                            {
                                bool flag = false;
                                GuestInformationEntity guest = null;

                                if (guestsList != null)
                                {
                                    foreach (string key in guestsList.Keys)
                                    {
                                        guest = guestsList[key];
                                        if (null != guest)
                                        {
                                            if (!string.IsNullOrEmpty(guest.GuestAccountNumber))
                                            {
                                                flag = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if ((bool)Reservation2SessionWrapper.IsUserEnrollInBookingModule)
                                {
                                    userLoginUsingEnroll = "Booking enroll checkbox";
                                    Reservation2SessionWrapper.IsUserEnrollInBookingModule = false;
                                }
                                else if (flag && !(bool)UserLoggedInSessionWrapper.UserLoggedIn)
                                    userLoginUsingEnroll = "FGP number entered manually";

                            }
                            if (IsEnrolledUserSessionWrapper.IsEnrolledUserFromTopRightButton)
                            {
                                userLoginUsingEnroll = "Top right corner";
                                //IsEnrolledUserSessionWrapper.IsEnrolledUserFromTopRightButton = false;
                            }
                            else if (IsEnrolledUserSessionWrapper.IsEnrolledUserFromScandicFriends)
                            {
                                userLoginUsingEnroll = "Scandic Friends page";
                                //IsEnrolledUserSessionWrapper.IsEnrolledUserFromScandicFriends = false;
                            }
                            return userLoginUsingEnroll;
                        }
                    case "61":
                        {
                            string membershipNo = string.Empty;
                            if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null && (bool)UserLoggedInSessionWrapper.UserLoggedIn)
                                membershipNo = LoyaltyDetailsSessionWrapper.LoyaltyDetails.UserName;
                            else
                                membershipNo = "No Membership Number";

                            return membershipNo;
                        }
                    default:
                        {
                            return string.Empty;
                        }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the correct value of the s.events variable
        /// </summary>
        /// <returns>The value of the s.events variable</returns>
        protected string GetEvent()
        {
            try
            {
                SortedList<string, GuestInformationEntity> guestsList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
                string result = string.Empty;
                PageReference reservationSelectHotelPageLink = (RootPage["ReservationSelectHotelPage"] as PageReference != null)
                                                                  ? RootPage["ReservationSelectHotelPage"] as PageReference : null;

                PageReference reservationSelectRatePageLink = (RootPage["ReservationSelectRatePage"] as PageReference != null)
                                                                  ? RootPage["ReservationSelectRatePage"] as PageReference : null;

                PageReference reservationBookingDetailsPage = (RootPage["ReservationBookingDetailsPage"] as PageReference != null)
                        ? RootPage["ReservationBookingDetailsPage"] as PageReference : null;

                PageReference reservationConfirmationPage = (RootPage["ReservationConfirmationPage"] as PageReference != null)
                                                                ? RootPage["ReservationConfirmationPage"] as PageReference : null;

                PageReference reservationChildrenDetailPage = (RootPage["ReservationChildrensDetailsPage"] as PageReference != null)
                        ? RootPage["ReservationChildrensDetailsPage"] as PageReference : null;

                //PageReference frequentGuestProgrammePage = (RootPage["FrequentGuestProgrammePage"] as PageReference != null)
                //                                               ? RootPage["FrequentGuestProgrammePage"] as PageReference : null;

                PageReference starndardPage = (RootPage["StandardPage"] as PageReference != null) ? RootPage["StandardPage"] as PageReference : null;
                if ((reservationChildrenDetailPage != null) && ((CurrentPage != null) && (CurrentPage.PageLink != null) &&
                     (reservationChildrenDetailPage.ID == CurrentPage.PageLink.ID)))
                {
                    result = "scAdd";
                }
                if ((reservationSelectHotelPageLink != null) && ((CurrentPage != null) && (CurrentPage.PageLink != null) &&
                     (reservationSelectHotelPageLink.ID == CurrentPage.PageLink.ID)))
                {
                    result = "scCheckout";
                }
                bool IsEditYourStaySearchTriggered = false;
                IsEditYourStaySearchTriggered = Reservation2SessionWrapper.IsEditYourStaySearchTriggered;
                if ((reservationSelectRatePageLink != null) &&
                    ((CurrentPage != null) && (CurrentPage.PageLink != null) &&
                     (reservationSelectRatePageLink.ID == CurrentPage.PageLink.ID))
                    && ((Request != null)
                        && (
                               (Request.UrlReferrer == null || Request.UrlReferrer.AbsoluteUri == null)
                               ||
                               ((Request.RawUrl != null) &&
                                (!Request.UrlReferrer.AbsoluteUri.ToLower().Contains(Request.RawUrl.ToLower()))
                                ||
                                (Request.Form["__EVENTTARGET"] != null &&
                                 Request.Form["__EVENTTARGET"].Contains("btnSearch")))
                           )
                       )
                    )
                {
                    result = "event1";
                }

                if (IsEditYourStaySearchTriggered && ((CurrentPage != null) && (CurrentPage.PageLink != null) &&
                     (reservationSelectRatePageLink.ID == CurrentPage.PageLink.ID)))
                {
                    result = "event1";
                    if ((Request != null) && (Request.UrlReferrer.AbsoluteUri.ToLower().Contains(Request.RawUrl.ToLower())))
                    {
                        Reservation2SessionWrapper.IsEditYourStaySearchTriggered = false;
                    }
                }
                if (reservationBookingDetailsPage != null && ((CurrentPage != null) && (CurrentPage.PageLink != null) &&
                     (reservationBookingDetailsPage.ID == CurrentPage.PageLink.ID))
                    && ((Request != null) && (Request.UrlReferrer == null || Request.UrlReferrer.AbsoluteUri == null ||
                                              (Request.RawUrl != null) && (!Request.UrlReferrer.AbsoluteUri.ToLower().Contains(
                                                  Request.RawUrl.ToLower())))))
                {
                    result = "event2";
                }
                //if (frequentGuestProgrammePage != null &&
                //    ((CurrentPage != null) && (CurrentPage.PageLink != null) && (CurrentPage.PageLink.ID != null) &&
                //     (frequentGuestProgrammePage.ID == CurrentPage.PageLink.ID)))
                //{
                //    result = "event3";
                //}
                if (reservationConfirmationPage != null &&
                    ((CurrentPage != null) && (CurrentPage.PageLink != null) && (reservationConfirmationPage.ID == CurrentPage.PageLink.ID)))
                {
                    bool flag = false;
                    GuestInformationEntity guest = null;
                    if (guestsList != null)
                    {
                        if (guestsList.Keys != null && guestsList.Keys.Count > 0)
                        {
                            foreach (string key in guestsList.Keys)
                            {
                                if (key != null && guestsList[key] != null)
                                {
                                    guest = guestsList[key];
                                    if (guest != null && guest.ChildrensDetails != null &&
                                        guest.ChildrensDetails.ListChildren != null &&
                                        guest.ChildrensDetails.ListChildren.Count > 0)
                                    {
                                        flag = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (flag)
                    {
                        if (Reservation2SessionWrapper.IsUserEnrollInBookingModule)
                            result = "event3,purchase,event17,event18";
                        // R 1.6.1 - site catalyst - tracking confirmation page event17 - ADDED event17
                        else
                            result = "purchase,event17,event18";
                    }
                    else
                    {
                        if (Reservation2SessionWrapper.IsUserEnrollInBookingModule)
                            result = "event3,purchase,event17";

                        else
                            result = "purchase,event17";
                    }
                }

                if (starndardPage != null)
                {
                    PageData standardPageData = GetPage(starndardPage);
                    if ((standardPageData != null) &&
                        ((CurrentPage != null) && (CurrentPage.PageLink != null) && standardPageData.PageLink != null &&
                         (standardPageData.PageLink.ID == CurrentPage.PageLink.ID)) && (HygieneSessionWrapper.IsXFormSubmitted))
                    {
                        result = "event9";
                        HygieneSessionWrapper.IsXFormSubmitted = false;
                    }
                }

                //Registration from Top Right Corner Botton
                if ((bool)IsEnrolledUserSessionWrapper.IsEnrolledUserFromTopRightButton ||
                    (bool)IsEnrolledUserSessionWrapper.IsEnrolledUserFromScandicFriends)
                {
                    result = "event3";
                }

                if ((bool)Reservation2SessionWrapper.IsLoginFromBookingDetailsPage ||
                    (bool)Reservation2SessionWrapper.IsLoginFromUpperRight || 
                    (bool)Reservation2SessionWrapper.IsLoginFromFGP)
                {
                    result = "event19";
                }
                return result;
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, ex.Message);
                return string.Empty;
            }
        }

        /// <summary>
        /// GetProducts
        /// </summary>
        /// <returns>Products</returns>
        protected string GetProducts()
        {
            try
            {
                TimeSpan spanDuration;
                string semiColon = ";";
                string hotelID = string.Empty;
                string nights = string.Empty;
                string bookingAmount = string.Empty;
                string noOfRooms = string.Empty;
                StringBuilder sb = new StringBuilder();
                PageReference reservationBookingDetailsPage = RootPage["ReservationBookingDetailsPage"] as PageReference;
                PageReference reservationConfirmationPage = RootPage["ReservationConfirmationPage"] as PageReference;
                SortedList<string, GuestInformationEntity> guestsList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
                SelectedRoomAndRateEntity selectedRoomRates = null;
                Hashtable selectedRoomRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
                LoyaltyDetailsEntity loyaltyDetailsEntity = LoyaltyDetailsSessionWrapper.LoyaltyDetails;

                if (reservationConfirmationPage != null && CurrentPage.PageLink.ID == reservationConfirmationPage.ID)
                {
                    bool flag = false;
                    HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
                    int count = 0;
                    int bookingTypeID = 0;
                    string bookingType = string.Empty;
                    GuestInformationEntity guest = null;
                    Rate rate = null;
                    Block block = null;
                    if (guestsList != null)
                    {
                        foreach (string key in guestsList.Keys)
                        {
                            guest = guestsList[key];
                            if (null != guest)
                            {
                                int roomNumber = count;
                                if (selectedRoomRatesHashTable != null &&
                                    selectedRoomRatesHashTable.ContainsKey(roomNumber))
                                {
                                    selectedRoomRates =
                                        selectedRoomRatesHashTable[roomNumber] as SelectedRoomAndRateEntity;
                                }
                                sb.Append(semiColon + searchCriteria.SelectedHotelCode);
                                sb.Append(semiColon + searchCriteria.NoOfNights);
                                if (selectedRoomRates != null && selectedRoomRates.RoomRates != null)
                                {
                                    if (Utility.IsBlockCodeBooking)
                                    {
                                        if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                                        {
                                            block =
                                                Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(
                                                    SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                                        }
                                    }
                                    else if (selectedRoomRates.RoomRates.Count > 0 &&
                                             selectedRoomRates.RoomRates[0].RatePlanCode != null)
                                    {
                                        string RatePlanCode = selectedRoomRates.RoomRates[0].RatePlanCode;
                                        rate = RoomRateUtil.GetRate(RatePlanCode);
                                    }
                                    if (searchCriteria != null &&
                                        (searchCriteria.SearchingType == SearchType.REGULAR ||
                                         searchCriteria.SearchingType == SearchType.CORPORATE))
                                    {
                                        sb.Append(semiColon + selectedRoomRates.RoomRates[0].TotalRate.Rate.ToString());
                                    }
                                    else
                                    {
                                        sb.Append(semiColon + 0);
                                    }
                                }

                                sb.Append(semiColon + "event17=1");

                                if (guest.MembershipID != null && guest.MembershipID > 0 && UserLoggedInSessionWrapper.UserLoggedIn &&
                                    loyaltyDetailsEntity != null)
                                {
                                    sb.Append(semiColon + "eVar40=" + loyaltyDetailsEntity.MembershipLevel + "|");
                                }
                                else
                                    sb.Append(semiColon);
                                if (selectedRoomRates != null)
                                    sb.Append("eVar41=" + selectedRoomRates.RoomCategoryID);
                                if (block != null)
                                {
                                    sb.Append("|" + "eVar42=" + "Blockcode booking");
                                }
                                else
                                {
                                    bookingTypeID = (int)searchCriteria.SearchingType;
                                    if (bookingTypeID == 0 && string.IsNullOrEmpty(searchCriteria.CampaignCode))
                                        bookingType = "Regular Booking";
                                    else if (bookingTypeID == 0 && !string.IsNullOrEmpty(searchCriteria.CampaignCode))
                                        bookingType = "Promotion Booking";
                                    else if (bookingTypeID == 1)
                                        bookingType = "Negotiated rate Booking";
                                    else if (bookingTypeID == 2)
                                        bookingType = "Bonus Cheque";
                                    else if (bookingTypeID == 3)
                                        bookingType = "Reward Night";
                                    else if (bookingTypeID == 4)
                                        bookingType = "Gift Voucher";
                                    else
                                        bookingType = "Undefined booking type";
                                    sb.Append("|" + "eVar42=" + bookingType);
                                }
                                if (guestsList.Count > 1)
                                {
                                    sb.Append("|" + "eVar43=" + guest.ReservationNumber + AppConstants.HYPHEN +
                                              guest.LegNumber);
                                }
                                else
                                    sb.Append("|" + "eVar43=" + guest.ReservationNumber);
                                if (block != null)
                                {
                                    sb.Append("|" + "eVar39=" + block.BlockCategoryName);
                                }
                                else if (rate != null)
                                    sb.Append("|" + "eVar39=" + rate.RateCategoryName);
                            }
                            if (guestsList.Count > 0 && count < guestsList.Count - 1)
                                sb.Append(",");
                            count = count + 1;
                        }
                    }
                }
                else if (reservationBookingDetailsPage != null &&
                         CurrentPage.PageLink.ID == reservationBookingDetailsPage.ID)
                {
                    HotelSearchEntity clonedSearchCriteria = SearchCriteriaSessionWrapper.CloneSearchCriteria;
                    HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
                    if (null != searchCriteria)
                    {
                        if (searchCriteria.SelectedHotelCode != null)
                        {
                            hotelID = ";" + searchCriteria.SelectedHotelCode;
                        }
                        else
                        {
                            if (clonedSearchCriteria != null)
                            {
                                hotelID = ";" + clonedSearchCriteria.SelectedHotelCode;
                            }
                        }
                    }
                    sb.Append(hotelID);
                }
                return sb.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// GetPurchaseID
        /// </summary>
        /// <returns>PurchaseID</returns>
        protected string GetPurchaseID()
        {
            PageData oPage = GetPage(PageReference.RootPage);
            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
            string purchaseID = string.Empty;
            if (CurrentPage.PageLink.ID == pageLinkID)
            {
                if (SogetiVariableSessionWrapper.ReservationNumberForThirdParty != string.Empty)
                    purchaseID = SogetiVariableSessionWrapper.ReservationNumberForThirdParty;
            }
            return purchaseID;
        }

        /// <summary>
        /// GetCampaign
        /// </summary>
        /// <returns>Campaign</returns>
        protected string GetCampaign()
        {
            return string.Empty;
        }

        /// <summary>
        /// GetState
        /// </summary>
        /// <returns>State</returns>
        protected string GetState()
        {
            PageData oPage = GetPage(PageReference.RootPage);
            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
            string userState = string.Empty;
            if (CurrentPage.PageLink.ID == pageLinkID)
            {
                IList<BaseRoomRateDetails> listHotelRoomRate = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                BaseRoomRateDetails details = (listHotelRoomRate != null && listHotelRoomRate.Count > 0)
                                                  ? listHotelRoomRate[0]
                                                  : null;


                if (null != details)
                    userState = details.CountryCode as string ?? string.Empty;
            }
            return userState;
        }

        /// <summary>
        /// GetZip
        /// </summary>
        /// <returns>Zip</returns>
        protected string GetZip()
        {
            PageData oPage = GetPage(PageReference.RootPage);
            int pageLinkID = ((PageReference)oPage["ReservationConfirmationPage"]).ID;
            string postCode = string.Empty;
            if (CurrentPage.PageLink.ID == pageLinkID)
            {
                if (SogetiVariableSessionWrapper.PostCodeForThirdParty != string.Empty)
                    postCode = SogetiVariableSessionWrapper.PostCodeForThirdParty;
            }
            return postCode;
        }

        protected void ResetFlags()
        {
            if (!this.Page.IsPostBack)
            {
                Reservation2SessionWrapper.IsLoginFromFGP = false;
                Reservation2SessionWrapper.IsLoginFromBookingDetailsPage = false;
                IsEnrolledUserSessionWrapper.IsEnrolledUserFromTopRightButton = false;
                IsEnrolledUserSessionWrapper.IsEnrolledUserFromScandicFriends = false;
                Reservation2SessionWrapper.IsLoginFromUpperRight = false;
            }
        }
    }
}