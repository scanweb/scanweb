//  Description					: MainMenu                                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using EPiServer;
using EPiServer.Core;
using EPiServer.Web.WebControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.CMS.Util.Filters;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using System;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// Lists the pages visible in the main (top) menu.
    /// </summary>
    public partial class MainMenu : UserControlBase
    {
        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            rptMainMenu.ItemDataBound += new RepeaterItemEventHandler(rptMainMenu_ItemDataBound);

            PageDataCollection mainMenuPages = ContentDataAccessManager.GetChildren(PageReference.StartPage);
            if (mainMenuPages != null)
            {
                GuestProgramPageFilter guestProgramPageFilter = new GuestProgramPageFilter();
                mainMenuPages = guestProgramPageFilter.Filter(mainMenuPages);

                PageCollection visiblePages = new PageCollection();
                rptMainMenu.DataSource = visiblePages.FilterInvisible(mainMenuPages);
                rptMainMenu.DataBind();
            }
        }

        /// <summary>
        /// Binds data to each row of repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rptMainMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                PageData pageData = e.Item.DataItem as PageData;
                var mainMenuContainerLink = e.Item.FindControl("mainMenuContainerLink") as HtmlGenericControl;
                HyperLink mainMenuItemLink = e.Item.FindControl("mainMenuItemLink") as HyperLink;
                MainMenuSubMenu mainMenuSubMenu = e.Item.FindControl("mainMenuSubMenu") as MainMenuSubMenu;
                mainMenuSubMenu.MainMenuSelectedPageLink = pageData.PageLink;
                string navigateURL = string.Empty;
                bool isPageSecured = false;
                if ((pageData != null) && (mainMenuItemLink != null))
                {
                    string pageExternalURL = WebUtil.GetExternalUrl(pageData.LinkURL);

                    if (pageData.Property["IsTopNavigationMenuImage"] != null &&
                        pageData.Property["IsTopNavigationMenuImage"].Value != null &&
                        pageData.Property["IsTopNavigationMenuImage"].Value.Equals(true) &&
                        pageData.Property["TopNavigationMenuIcon"] != null &&
                        pageData.Property["TopNavigationMenuIcon"].Value != null)
                    {
                        mainMenuItemLink.CssClass = "hmIcon scansprite";
                        mainMenuSubMenu.Visible = false;
                        pageExternalURL = GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE);
                    }
                    else
                    {
                        mainMenuItemLink.Text = pageData.PageName;
                    }

                    if (pageExternalURL != null)
                    {
                        isPageSecured = (pageData["Secured"] != null) ? Convert.ToBoolean(pageData["Secured"]) : false;
                        navigateURL = WebUtil.GetCompletePageUrl(pageExternalURL, isPageSecured);
                    }

                    mainMenuItemLink.NavigateUrl = navigateURL;

                    if (mainMenuContainerLink != null)
                    {
                        var masterpage = this.Page.Master as ScandicMasterPageBase;
                        var style = "unselected";
                        if (LastVisitedPageSessionWrapper.SelectedMainMenuLink != null &&
                            string.Equals(LastVisitedPageSessionWrapper.SelectedMainMenuLink.ID, pageData.PageLink.ID))
                        {
                            style = "selected";
                        }
                        mainMenuContainerLink.Attributes.Add("class", style);
                    }
                }
            }
        }

    }
}