<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelExpansionHotelList.ascx.cs" 
Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.HotelExpansionHotelList" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register TagPrefix="Scanweb" TagName="HotelExpansionGoogleMap" Src="~/Templates/Scanweb/Units/Static/HotelExpansionGoogleMap.ascx" %>
<div id="hotels-wrap">
      	<!-- title, select country, and map button -->
        <div id="hotels-header">
        	<!-- title div -->
            <div id="hotels-header-title">
                <h1><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/ExpansionPage/WeareExpanding") %></h1>
                <h3><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/ExpansionPage/Scandicforfuture") %></h3>
            </div>
            <!-- google map button -->
           <%-- <div id="btnMap"><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/ExpansionPage/Map")%></div>--%>
            <div id="btnMap">
                <div id="btnMapLabel"><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/ExpansionPage/Map") %></div>
            </div>
            <!-- dropdown for selecting country -->
            <div id="country-dropdown">
                <p><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/ExpansionPage/SelectBy") %></p>
                <div id="country-dropdown-options" class="dropdown">
                    <div class="selected" id="ddCountry"><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/ExpansionPage/ShowALL") %></div>
                    <!-- list of countries -->
                    <%= CreateCountryDropdownList() %>
                </div>
            </div>
            
            <div class="clear"></div>
        </div>
    
    	<!-- hotels content -->
        <div id="content-wrap">
            <div id="content-content"><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/Loading") %></div>
        </div>
        
        <!-- scrollbar -->
        <div id="scroll-wrap">
            <div id="scroll-bar">
                <div id="scroll-btn-left"></div>
                <div id="scrollbar-btn-center">
                    <div id="scrollbar-btn"></div>
                </div>
                <div id="scroll-btn-right"></div>
            </div>
        </div>
        
        <!-- GoogleMap markup-->
        <div id="googleMapBackground"></div>
        <div id="googleMapContainer">
            <div id="btnCloseGoogleMap"></div>
            <div id="ExpansionMap">
                <Scanweb:HotelExpansionGoogleMap id="HotelExpansionGoogleMap" runat="server" />
            </div>
        </div>
	</div>
	<!-- end hotels-wrap -->