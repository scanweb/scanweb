<%@ Control Language="C#" AutoEventWireup="true" Codebehind="MeetingSearchGoogleMap.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.MeetingSearchGoogleMap" %>
<%--<%@ Register Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMap"
    TagPrefix="Scandic" %>--%>
     <%@ Register Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3"
    TagPrefix="cc1" %>
    
<%--<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Scanweb/Javascript/gmaps-utility-library/markermanager_packed.js")%>"></script>
--%>
<div id="MeetingSearchGoogleMap">
    <div class="MeetingSearchGoogleMapTop">
    </div>
    <div class="MeetingSearchGoogleMapContent">
       
        
        <div class="map">
                <div id="GMapV3" style="height:327px;width:349px;"></div>  
                        <cc1:Map ID="GMapControl1"  runat="server" />
                
            </div>
    </div>
    <div class="MeetingSearchGoogleMapBottom">
    </div>
</div>
