//  Description					:   PopUp                                                 //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// Code behind of popup control.
    /// </summary>
    public partial class PopUp : EPiServer.UserControlBase
    {
        private PageDataCollection listPages;

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            PageHeading.Text = CurrentPage["Heading"] as string ?? CurrentPage.PageName;

            PopUpHyperLink.Text =
                LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Static/PopUp/ReturnLink");

            if (!IsPostBack)
            {
                if (CurrentPage["ShowParentLink"] != null)
                {
                    ReturnLinkPlaceHolder.Visible = true;
                }

                if (CurrentPage["listingContainer"] as PageReference != null)
                {
                    PageReference pageLink = (PageReference) CurrentPage["listingContainer"];

                    listPages = DataFactory.Instance.GetChildren(pageLink);
                    poplist.DataSource = listPages;
                    poplist.DataBind();
                    panel1.Visible = true;
                }
            }
        }

        /// <summary>
        /// Checks whether it's a last item.
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns>True/False</returns>
        protected bool IsLastItem(int itemID)
        {
            if (listPages.Count > 0)
            {
                return (listPages[listPages.Count - 1].PageLink.ID == itemID);
            }
            else
            {
                return false;
            }
        }
    }
}