<%@ Control Language="C#" AutoEventWireup="true" Codebehind="TreeViewControl.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.TreeViewControl" %>
<h3 class="darkHeading"> 
<episerver:translate id="Translate1" text="/Templates/Scanweb/Units/Static/FindHotelSearch/SearchBy"
      runat="server" /></h3>   
<asp:TreeView ID="countryTreeView" runat="server" SelectedNodeStyle-Font-Bold="true" ShowExpandCollapse="true"   
    OnSelectedNodeChanged="countryTreeView_GetSelectedNode" ExpandImageToolTip="" CollapseImageToolTip="" ExpandImageUrl="/templates/Scanweb/Styles/Default/Images/Icons/blueArrow.gif" CollapseImageUrl="/templates/Scanweb/Styles/Default/Images/Icons/blueArrow.gif" NoExpandImageUrl="/templates/Scanweb/Styles/Default/Images/Icons/blueArrow.gif">
</asp:TreeView>

<script type="text/javascript" language="javascript">

   function ChangeCSSForHotels(){ 
	var tables=document.getElementById("search").getElementsByTagName("table");
	
      for(j=0;j<tables.length;j++){
            if(tables[j]){             
              var trs = tables[j].getElementsByTagName("tr");
              var nodeCount = trs.length;             
              for(i=0;i<nodeCount;i++){
                 if(trs[i]){
                 var tds=trs[i].getElementsByTagName("td");
                
				if(tds.length > 2){
					 if((tds[3])&&(tds[3].firstChild.tagName.toLowerCase() == "a")){
                       tds[3].style.whiteSpace="normal";
                       tds[3].style.paddingLeft="5px";
                       tds[3].style.width="220px";
                       tds[3].className="";
                       tds[3].parentNode.parentNode.parentNode.style.background="#fff";
                       tds[3].style.fontSize="0.9em";
                    }
                else{
					 if((tds[2])&&(tds[2].firstChild.tagName.toLowerCase() == "a")){
                       tds[2].style.fontSize="0.9em";
                       tds[2].parentNode.parentNode.parentNode.style.background="#fff";
					   var container=tds[2].parentNode.parentNode.parentNode.parentNode;
					   
                       container.style.border="1px solid #EEEEEE";
                       container.style.marginTop="-5px";
                       container.style.marginBottom="5px";
                       container.style.width="224px";                   
                      }
					}
				}
                }
                 
             }
           }     
         }
         
         //Cufon.replace(".BoxContainer .stoolHeading");
      }
</script>