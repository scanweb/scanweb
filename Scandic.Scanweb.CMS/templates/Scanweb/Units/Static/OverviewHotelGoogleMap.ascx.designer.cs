//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1433
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Scandic.Scanweb.CMS.Templates.Units.Static {
    
    
    /// <summary>
    /// OverviewHotelGoogleMap class.
    /// </summary>
    /// <remarks>
    /// Auto-generated class.
    /// </remarks>
    public partial class OverviewHotelGoogleMap {
        
        /// <summary>
        /// GoogleMapsPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder GoogleMapsPlaceHolder;
        
        /// <summary>
        /// GMapControl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3.Map GMapControl1;
        
        /// <summary>
        /// AlternativeMapPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder AlternativeMapPlaceHolder;
        
        /// <summary>
        /// Property1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Property Property1;
        
        /// <summary>
        /// HotelsPageListLeft control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.PageList HotelsPageListLeft;
        
        /// <summary>
        /// HotelsPageListRight control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.PageList HotelsPageListRight;
    }
}
