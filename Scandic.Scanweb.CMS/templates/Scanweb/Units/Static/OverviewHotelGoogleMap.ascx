<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OverviewHotelGoogleMap.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.OverviewHotelGoogleMap" %>


<%@ Register TagPrefix="GoogleMapV3" Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3" %>
<%--<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Scanweb/Javascript/gmaps-utility-library/markermanager_packed.js")%>"></script>
--%>
<div id="DestinationGoogleMap">
    <div class="DestinationGoogleMapTop"></div>
    <div class="DestinationGoogleMapContent">
    <div>
    <asp:PlaceHolder ID="GoogleMapsPlaceHolder" runat="server">
       
        <div id="GMapV3" style="height:300px;width:450px;"></div>  
            <GoogleMapV3:Map ID="GMapControl1" runat="server" />
        
                                
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="AlternativeMapPlaceHolder" runat="server" Visible="false">
        <EPiServer:Property ID="Property1" PropertyName="AlternativeGoogleMapImage" ImageWidth="450" runat="server" />       
    </asp:PlaceHolder>
    </div>
    
    <div class="DestinationListColumn">
    <EPiServer:PageList ID="HotelsPageListLeft" runat="server">
        <ItemTemplate>
             <div class="LinkListItem">
                 <div class="<%# !IsLastItemLeft(Container.CurrentPage.PageLink.ID) ? "NotLastLink" : "LastLink" %>">
                    <a class="IconLink" href="<%# GetFriendlyURL(Container.CurrentPage.PageLink, Container.CurrentPage.LinkURL) %>" title="<%# Container.CurrentPage["Heading"] as string ?? Container.CurrentPage.PageName %>"><%# Container.CurrentPage["Heading"] as string ?? Container.CurrentPage.PageName %></a>
                 </div>
             </div>
        </ItemTemplate>
    </EPiServer:PageList>
    </div>
    <div class="DestinationListColumn">
    <EPiServer:PageList ID="HotelsPageListRight" runat="server">
        <ItemTemplate>
            <div class="LinkListItem">
                 <div class="<%# !IsLastItemRight(Container.CurrentPage.PageLink.ID) ? "NotLastLink" : "LastLink" %>">
                    <a class="IconLink" href="<%# GetFriendlyURL(Container.CurrentPage.PageLink, Container.CurrentPage.LinkURL) %>" title="<%# Container.CurrentPage["Heading"] as string ?? Container.CurrentPage.PageName %>"><%# Container.CurrentPage["Heading"] as string ?? Container.CurrentPage.PageName %></a>
                 </div>
             </div>
        </ItemTemplate>
    </EPiServer:PageList>
    </div>
    </div>
    <div class="DestinationGoogleMapBottom"></div>
</div>