<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GuestProgramRedemptionHotels.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.GuestProgramRedemptionHotels" %>
<asp:Repeater ID="HotelsRepeater" runat="server">
    <ItemTemplate>
        <div>
            <EPiServer:Property ID="Property1" PropertyName="PageLink" title='<%# DataBinder.Eval(Container.DataItem, "PageName") %>'
                runat="server" />
        </div>
    </ItemTemplate>
</asp:Repeater>