<%@ Control Language="C#" EnableViewState="false" AutoEventWireup="False" Codebehind="Header.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.Header" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<link id="link123" runat="server" rel="shortcut icon" href="/Templates/Booking/Styles/Default/Images/favicon.ico" type="image/x-icon" />
<link id="link456" runat="server" rel="icon" href="/Templates/Booking/Styles/Default/Images/favicon.ico" type="image/ico" />
	
    <link   type="text/css" 
            rel="Stylesheet" 
            href="/Templates/Booking/Styles/Default/combined.css?v=<%=CmsUtil.GetCSSVersion()%>" />			
<link   type="text/css" 
            rel="Stylesheet" 
            href="/Templates/Booking/Styles/Default/flexslider.css?v=<%=CmsUtil.GetCSSVersion()%>" />			
	<link rel="stylesheet" type="text/css" href="/Templates/Scanweb/Styles/<%=GetBrowserCSS()%>/BrowserSpecific.css?v=<%=CmsUtil.GetCSSVersion()%>" />
	

<asp:PlaceHolder ID="plhLinkOffer" runat="server" />	
	
<asp:PlaceHolder ID="plhMetaDataArea" runat="server" />

<!-- To fetch server date and to be used in the JS files across the site --->
<script type="text/javascript" language="JavaScript">

<% DateTime ServerDate = DateTime.Now;%>
var maxDates = <%= GetMaxNoOfAllowedDaysForArrival()%>;
var serverYear      = '<%= ServerDate.Year %>';
var serverMonth     = '<%= ServerDate.Month %>';
// Month sequence stats from 0 (ZERO). 
// In .NET code, month sequence starts at 1. 
// So we are deducting 1 from the month value returned by .NET.
serverMonth         = serverMonth - 1;
var serverDay       = '<%= ServerDate.Day %>';
var serverHour      = '<%= ServerDate.Hour %>';
var serverMinute    = '<%= ServerDate.Minute %>';
var serverSecond    = '<%= ServerDate.Second %>';
var serverMilliSec  = '<%= ServerDate.Millisecond %>';

var serverDate = new Date(serverYear, serverMonth, serverDay, serverHour, serverMinute, serverSecond, serverMilliSec);

</script>
<!-- End To fetch server date and to be used in the JS files across the site --->

<!-- Booking stylesheets --->
<%--<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/booking.css")%>" />--%>
<%--<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/reservation2.0.css")%>" />
<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/validationEngine.jquery.css")%>" />--%>
<%if (GetBrowserCSS() == "IE7") { %>
<link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/IE7/ie7.css")%>" />
<% } %>
	<!--[if IE 8 ]>
        <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/ie8.css")%>" />
<![endif]-->
<!-- Booking JavaScripts --->
	<!--[if IE 9 ]>
        <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Templates/Booking/Styles/Default/ie9.css")%>" />
<![endif]-->
<!--[if !IE]> -->
<style type="text/css">
#Loyalty input, #Loyalty label, .tabCnt label, .tabCnt input, .rbList input, .rbList label {vertical-align:middle;}
#Loyalty .PINchoice input {	
	margin:3px!important;
	vertical-align:top!important;
}
</style>
<!-- <![endif]-->
    <script     
        type="text/javascript" 
        src="/Templates/Booking/JavaScript/final.js?v=<%=CmsUtil.GetJSVersion()%>" >
    </script>
    


<!--<script type="text/javascript">
$(document).ready(function(){
if ($.fn.flash.hasFlash(7)) {
 
// your code for sifr should go here
 $('#footerWrapper h3.title').sifr({
		    path: "Templates/Scanweb/Styles/Default/Images/",
		    font: "sifr",
		    textAlign: "left"
	    });
    
}

});
</script>
-->
<!-- START: Load localised calendar -->
<% string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToLower();
   string url = "~/Templates/Booking/JavaScript/plugin/locales/ui.datepicker-" + siteLanguage + ".js";
   if (siteLanguage != "en")
   {%>
<script type="text/javascript" language="JavaScript" src="<%= ResolveUrl(url) %>?v=<%=CmsUtil.GetJSVersion()%>"></script>
<% } %>
<!-- END: Load localised calendar -->

<!-- Compost News letter JavaScripts -->

<!--<script type="text/javascript" language="javascript" src="/Templates/Scanweb/Javascript/Compost/newsletter.js"></script>-->

<!-- End Compost News letter JavaScripts -->
