<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelOverviewPageGoogleMap.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.HotelOverviewPageGoogleMap" %>
<%@ Register TagPrefix="GoogleMap" Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMap" %>
<%@ Register TagPrefix="GoogleMapV3" Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>

<div id="HotelOverviewPageGoogleMap">
<%-- Merchandising - Google Maps V3--%>

<div id="GMapV3" style="height:374px;width:716px;"></div>  
<GoogleMapV3:Map ID="GMapControl1"  runat="server" />
    </div>
       <div id="HotelFilter" class="hotelFilter">
    <h3><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/OurHotels")%></h3>
        <p>
            <img src="<%=ResolveUrl("~/Templates/Scanweb/Styles/Default/Images/Icons/filter_green.gif")%>" title="<%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/RecentlyOpened")%>" alt="<%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/RecentlyOpened")%>"/>
            <label><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/RecentlyOpened")%></label>
            <input id="RecentlyOpened" type="checkbox" checked="checked" onclick="SwitchHotelFilter(this,'RecentlyOpened')"  />
        </p>
       
       
       <p>
       <img src="<%=ResolveUrl("~/Templates/Scanweb/Styles/Default/Images/Icons/filter_orange.gif")%>" title="<%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/ComingSoon")%>" alt="<%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/ComingSoon")%>"/>
       <label><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/ComingSoon")%></label>
       <input id="ComingSoon" type="checkbox" checked="checked" onclick="SwitchHotelFilter(this,'ComingSoon')" />
       </p>
       
       <p>
        <img src="<%=ResolveUrl("~/Templates/Scanweb/Styles/Default/Images/Icons/purple_filter_icon.png")%>" title="<%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/Others")%>" alt="<%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/Others")%>"/>
        <label><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/Others")%></label>
        <input id="Default" type="checkbox"  checked="checked" onclick="SwitchHotelFilter(this,'Default')" />
       </p>
       
        
        
    </div>
