#region Revision history

//  Description					: New Page footer to show the links
//	The earlier page footer was only showing the links which was an HTML text picked up from CMS property.
//  The new suggested page footer is implemented in this class																					  
//----------------------------------------------------------------------------------------
/// Author						: Kshipra Thombre  
/// Author email id				:                           							  
/// Creation Date				: 18th March  2010									  
///	Version	#					: 2.0													  
///---------------------------------------------------------------------------------------
/// Revison History				: -NA-													  
///	Last Modified Date			:														  
////////////////////////////////////////////////////////////////////////////////////////////

#endregion

#region Using

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static;
using Scandic.Scanweb.CMS.Util.Filters;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;

#endregion Using

namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// A common footer for the website where links common for the whole site are listed.
    /// All links will be taken from CMS and can be configured there
    /// There are various sections in the footer to display different data
    /// </summary>
    public partial class PageFooter : UserControlBase
    {
        #region Constants

        private const string SITE_FOOTER_STOOL_IMAGE = "SiteFooterStoolImage";
        private const string SITE_FOOTER_TEXT_AREA = "SiteFooterTextArea";
        private const string SITE_FOOTER_CONTAINER_STOOL_IMAGE = "ContainerStoolImage";
        private const string SITE_FOOTER_CONTAINER_TEXT_AREA = "ContainerTextArea";
        private const string LINK_TO_ANOTHER_PAGE = "LinkToAnotherPage";
        private const string FGP_LINK_SIGN_UP = "/Templates/Scanweb/Units/Placeable/Pagefooter/FGPSignUp";
        private const string SITE_FOOTER_HEADING = "SiteFooterHeading";
        private const string DISPLAY_IN_COLUMN_NUMBER = "DisplayInColumnNumber";
        private const string SITE_FOOTER_CONATINER_PAGE_TYPE_GUID = "SiteFooterContainerPageTypeGUID";
        private const string ABOUT_US_SITE_FOOTER_CONATINER_PAGE_TYPE_GUID = "AboutUsSiteFooterContainerPageTypeGUID";
        private const string PROMOTIONAL_PAGE_LINK_TEXT = "PromotionalPageLinkText";
        private const string PROMOTIONAL_PAGE_LINK = "PromotionalPageLink";
        private const string NEW_HOTELS_LINK_TEXT = "NewHotelsLinkText";
        private const string NEW_HOTELS_LINK = "NewHotelsLink";
        private const string CONTAINER_POSITION_IN_COLUMN = "ContainerPositionInColumn";
        #endregion

        #region Protected Methods

        /// <summary>
        /// Onload method for PageFooter class
        /// The footer consists of 4 columns
        /// Col 1: About Scandic
        /// Col 2: About Scandic Links + Configurable area below it
        /// Col 3: Configurable Area
        /// Col 4: FGP + New hotels links
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            try
            {
                rptAboutUsContainerFooter.ItemDataBound += new RepeaterItemEventHandler(RptAboutUsContainerFooter_ItemDataBound);
                rptNewHotelsPageList.ItemDataBound += new RepeaterItemEventHandler(RptNewHotelsPageList_ItemDataBound);
                DisplayAboutScandic();
                DisplayAboutScandicLinks();
                DisplayFGPLinks();
                DisplayNewHotelLinks();
                DisplayFooterControls();
            }
            catch (Exception ex)
            {
                AppLogger.LogCustomException(ex, ex.Message);
            }
        }

        /// <summary>
        /// This method is used to get the link Name for the new hotels
        /// 1. Returns the link Name for the New Hotels Page if it exists
        /// 2. Returns the Promopage link Name if set for the Hotel
        /// [This requires the properties 'PromotionalPageLink','PromotionalPageLinkText' set]
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        protected string GetNewHotelsLinksName(PageData page)
        {
            string newHotelsLinkName = string.Empty;
            PageData referencedPage;
            if (page[LINK_TO_ANOTHER_PAGE] != null)
            {
                referencedPage = DataFactory.Instance.GetPage(page[LINK_TO_ANOTHER_PAGE] as PageReference);
                if (referencedPage[PROMOTIONAL_PAGE_LINK_TEXT] != null && referencedPage[PROMOTIONAL_PAGE_LINK] != null)
                {
                    newHotelsLinkName = referencedPage[PROMOTIONAL_PAGE_LINK_TEXT] as string;
                }
                else
                {
                    newHotelsLinkName = page.PageName;
                }
            }
            return newHotelsLinkName;
        }

        /// <summary>
        /// Repeater, AboutUsContainerFooter, item data bound event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RptAboutUsContainerFooter_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                string pageURL = string.Empty;
                PageData pageData = e.Item.DataItem as PageData;
                HyperLink lnkAboutUsContainerFooter = e.Item.FindControl("lnkAboutUsContainerFooter") as HyperLink;
                if ((pageData != null) && (lnkAboutUsContainerFooter != null))
                {

                    if (pageData[LINK_TO_ANOTHER_PAGE] != null)
                    {
                        pageURL = GlobalUtil.GetUrlToPage(pageData[LINK_TO_ANOTHER_PAGE] as PageReference);
                    }
                    else if (pageData.LinkURL != null)
                    {
                        pageURL = pageData.LinkURL;
                    }
                    lnkAboutUsContainerFooter.Text = pageData.PageName;
                    lnkAboutUsContainerFooter.NavigateUrl = pageURL;
                }

            }
        }

        /// <summary>
        /// Repeater, NewHotelsPageList, item data bound event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RptNewHotelsPageList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                string pageURL = string.Empty;
                PageData pageData = e.Item.DataItem as PageData;
                HyperLink lnkAboutUsContainerFooter = e.Item.FindControl("lnkNewHotel") as HyperLink;
                if ((pageData != null) && (lnkAboutUsContainerFooter != null))
                {

                    if (pageData[LINK_TO_ANOTHER_PAGE] != null)
                    {
                        pageURL = GlobalUtil.GetUrlToPage(pageData[LINK_TO_ANOTHER_PAGE] as PageReference);
                        PageData referencedPage =
                        DataFactory.Instance.GetPage(pageData[LINK_TO_ANOTHER_PAGE] as PageReference);

                        if (referencedPage[PROMOTIONAL_PAGE_LINK_TEXT] != null && referencedPage[PROMOTIONAL_PAGE_LINK] != null)
                        {
                            referencedPage = DataFactory.Instance.GetPage(referencedPage[PROMOTIONAL_PAGE_LINK] as PageReference);
                            pageURL = (referencedPage.LinkURL != null) ? referencedPage.LinkURL : string.Empty;
                        }
                    }
                    else if (pageData.LinkURL != null)
                    {
                        pageURL = pageData.LinkURL;
                    }
                    lnkAboutUsContainerFooter.Text = pageData.PageName;
                    lnkAboutUsContainerFooter.NavigateUrl = pageURL;
                }

            }
        }
        #endregion Protected Methods

        #region Private Methods

        /// <summary>
        /// Method Displays the Column 1:About scandic
        /// </summary>
        private void DisplayAboutScandic()
        {
            PageData rootPage = ContentDataAccess.GetPageData(PageReference.RootPage, false);
            PageReference siteFooterPageReference = rootPage[EpiServerPageConstants.SITE_FOOTER_PAGE] as PageReference;
            if (siteFooterPageReference != null)
            {
                PageType aboutUsSiteFooterContainerPageType = ContentDataAccess.GetContainerPageType(ConfigurationManager.AppSettings[ABOUT_US_SITE_FOOTER_CONATINER_PAGE_TYPE_GUID], false);
                int aboutUsSiteFooterContainerPageTypeID = aboutUsSiteFooterContainerPageType != null ? aboutUsSiteFooterContainerPageType.ID : 0;
                PageDataCollection aboutUsSiteFooterContainerPagesCollection =
                    ContentDataAccess.GetPagesWithPageTypeID(aboutUsSiteFooterContainerPageTypeID,
                                                             siteFooterPageReference);

                if (aboutUsSiteFooterContainerPagesCollection != null &&
                    aboutUsSiteFooterContainerPagesCollection.Count != 0)
                {
                    PageData aboutUsPage = (aboutUsSiteFooterContainerPagesCollection[0] == null)
                                               ? null
                                               : aboutUsSiteFooterContainerPagesCollection[0] as PageData;

                    if (aboutUsPage != null)
                    {
                        //Merchandising:R3:Initial Design Document - Removal of Images from Footer
                        //if (aboutUsPage[SITE_FOOTER_CONTAINER_STOOL_IMAGE] != null)
                        //{
                        //    string imageString = aboutUsPage[SITE_FOOTER_CONTAINER_STOOL_IMAGE] as string;

                        //    ImageStoreNET.Classes.Util.UrlBuilder urlBuilder =
                        //        ImageStoreNET.Classes.Util.UrlBuilder.ParseUrl(imageString);
                        //    if (urlBuilder != null)
                        //    {
                        //        urlBuilder.PreserveAspectRatio = true;
                        //        urlBuilder.ConversionFormatType =
                        //            ImageStoreNET.Classes.Data.ConversionFormatTypes.WebSafe;
                        //        AboutScandicStoolImage.ImageUrl = urlBuilder.ToString();
                        //    }
                        //    AboutScandicStoolImagePlaceHolder.Visible = true;
                        //}

                        //Merchandising:R3:Initial Design Document - Removal of Images from Footer
                        if (aboutUsPage[SITE_FOOTER_HEADING] != null)
                        {
                            string headingString = aboutUsPage[SITE_FOOTER_HEADING] as string;
                            AboutScandicHeadingPlaceHolder.Visible = true;
                            AboutScandicHeading.Text = headingString;
                        }
                        if (aboutUsPage[SITE_FOOTER_CONTAINER_TEXT_AREA] != null)
                        {
                            string siteFooterHeading = aboutUsPage[SITE_FOOTER_CONTAINER_TEXT_AREA] as string;
                            AboutScandicTextArea.Text = siteFooterHeading;
                            AboutScandicTextAreaPlaceHolder.Visible = true;

                            PlaceHolder phMetaOrganizationDescriptionForSchemaOrg = (PlaceHolder)this.Parent.FindControl("phMetaOrganizationDescriptionForSchemaOrg");
                            if (phMetaOrganizationDescriptionForSchemaOrg != null)
                            {
                                //Schema.Org Organizational Description
                                Utility.AddMetaInfoForSchemaOrg(phMetaOrganizationDescriptionForSchemaOrg, "itemprop", "description", siteFooterHeading);
                            }
                            
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Displays the Links Related to About Scandic. This Displays all the child links under 'About Scandic'
        /// The Links are displayed in Column 2 of the Site Footer
        /// </summary>
        private void DisplayAboutScandicLinks()
        {
            rptAboutUsContainerFooter.Visible = false;
            PageData rootPage = ContentDataAccess.GetPageData(PageReference.RootPage, false);
            PageReference siteFooterPageReference = rootPage[EpiServerPageConstants.SITE_FOOTER_PAGE] as PageReference;
            if (siteFooterPageReference != null)
            {
                PageType aboutUsSiteFooterContainerPageType = ContentDataAccess.GetContainerPageType(ConfigurationManager.AppSettings[ABOUT_US_SITE_FOOTER_CONATINER_PAGE_TYPE_GUID], false);
                int aboutUsSiteFooterContainerPageTypeID = aboutUsSiteFooterContainerPageType != null ? aboutUsSiteFooterContainerPageType.ID : 0;
                PageDataCollection aboutUsSiteFooterContainerPagesCollection =
                    ContentDataAccess.GetPagesWithPageTypeID(aboutUsSiteFooterContainerPageTypeID,
                                                             siteFooterPageReference);
                if (aboutUsSiteFooterContainerPagesCollection != null &&
                    aboutUsSiteFooterContainerPagesCollection.Count != 0)
                {
                    PageData aboutUsPage = (aboutUsSiteFooterContainerPagesCollection[0] == null)
                                               ? null
                                               : aboutUsSiteFooterContainerPagesCollection[0] as PageData;
                    if (aboutUsPage != null)
                    {
                        PageDataCollection aboutUSPageContainerPages =
                            DataFactory.Instance.GetChildren(aboutUsPage.PageLink);
                        GuestProgramPageFilter guestProgramPageFilter = new GuestProgramPageFilter();
                        PageCollection invisiblePageFilter = new PageCollection();
                        aboutUSPageContainerPages = guestProgramPageFilter.Filter(aboutUSPageContainerPages);
                        aboutUSPageContainerPages = invisiblePageFilter.FilterInvisible(aboutUSPageContainerPages);
                        rptAboutUsContainerFooter.DataSource = aboutUSPageContainerPages;
                        rptAboutUsContainerFooter.DataBind();
                        rptAboutUsContainerFooter.Visible = true;
                    }
                }
            }
        }

        /// <summary>
        /// This method Will load the Control 'PageFooterLinks'. Each coltrol represents a section displayed in Column 2 and column 3 of the site footer.
        /// The  control is loaded dynamically depending uplon the number of 'Site footer Containers' created under '[Site Foot]'
        /// </summary>
        private void DisplayFooterControls()
        {
            PageData rootPage = ContentDataAccess.GetPageData(PageReference.RootPage, false);
            PageReference siteFooterPageReference = rootPage[EpiServerPageConstants.SITE_FOOTER_PAGE] as PageReference;

            if (siteFooterPageReference != null)
            {
                PageType siteFooterContainerPageType = ContentDataAccess.GetContainerPageType(ConfigurationManager.AppSettings[SITE_FOOTER_CONATINER_PAGE_TYPE_GUID], false);
                int siteFooterContainerPageTypeID = siteFooterContainerPageType != null ? siteFooterContainerPageType.ID : 0;
                PageDataCollection siteFooterContainerPagesCollection =
                    ContentDataAccess.GetPagesWithPageTypeID(siteFooterContainerPageTypeID, siteFooterPageReference);

                PageDataCollection siteFooterContainerColumnTwoPages = new PageDataCollection();
                PageDataCollection siteFooterContainerColumnThreePages = new PageDataCollection();

                foreach (PageData siteFooterPage in siteFooterContainerPagesCollection)
                {
                    if (siteFooterPage[DISPLAY_IN_COLUMN_NUMBER] != null)
                    {
                        if (siteFooterPage[DISPLAY_IN_COLUMN_NUMBER].ToString().Equals("2"))
                        {
                            siteFooterContainerColumnTwoPages.Add(siteFooterPage);
                        }
                        else if (siteFooterPage[DISPLAY_IN_COLUMN_NUMBER].ToString().Equals("3"))
                        {
                            siteFooterContainerColumnThreePages.Add(siteFooterPage);
                        }
                    }
                }

                if (siteFooterContainerColumnTwoPages != null)
                {
                    siteFooterContainerColumnTwoPages.Sort(new ContainerPositionComparer());

                    foreach (PageData siteFooterPage in siteFooterContainerColumnTwoPages)
                    {
                        PageFooterLinks pageFooterLinks =
                            LoadControl("PageFooterLinks.ascx") as PageFooterLinks;

                        if (pageFooterLinks != null)
                        {
                            pageFooterLinks.SetFooterLinks = siteFooterPage.PageLink;
                            Col2LinksPlaceHolder.Controls.Add(pageFooterLinks);
                        }
                    }
                }

                if (siteFooterContainerColumnThreePages != null)
                {
                    siteFooterContainerColumnThreePages.Sort(new ContainerPositionComparer());
                    foreach (PageData siteFooterPage in siteFooterContainerColumnThreePages)
                    {
                        PageFooterLinks pageFooterLinks =
                            LoadControl("PageFooterLinks.ascx") as PageFooterLinks;

                        if (pageFooterLinks != null)
                        {
                            pageFooterLinks.SetFooterLinks = siteFooterPage.PageLink;
                            Col3LinksPlaceHolder.Controls.Add(pageFooterLinks);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Display the Frequent Guest program Section in Column 4 section of Site footer
        /// </summary>
        private void DisplayFGPLinks()
        {
            if (UserLoggedInSessionWrapper.UserLoggedIn)
                lnkSignUpFGP.Visible = false;
            else
            {
                lnkSignUpFGP.Visible = true;
                lnkSignUpFGP.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.ENROLL_LOYALTY);
                lnkSignUpFGP.Attributes.Add("onclick", "SiteCatalystforFooterSignUP('" + CurrentPage.PageName + "');");
                lnkSignUpFGP.InnerText = WebUtil.GetTranslatedText(FGP_LINK_SIGN_UP);
            }
            PageData rootPage = ContentDataAccess.GetPageData(PageReference.RootPage, false);
            PageReference frequentGuestProgPageReference =
                rootPage[EpiServerPageConstants.FREQUENT_GUEST_PROGRAMME] as PageReference;

            if (frequentGuestProgPageReference != null)
            {
                PageData frequentGuestProgPage = DataFactory.Instance.GetPage(frequentGuestProgPageReference);

                if (frequentGuestProgPage != null)
                {
                    //Merchandising:R3:Initial Design Document - Removal of Images from Footer
                    //if (frequentGuestProgPage[SITE_FOOTER_STOOL_IMAGE] != null)
                    //{
                    //    string imageString = frequentGuestProgPage[SITE_FOOTER_STOOL_IMAGE] as string;

                    //    ImageStoreNET.Classes.Util.UrlBuilder urlBuilder =
                    //        ImageStoreNET.Classes.Util.UrlBuilder.ParseUrl(imageString);
                    //    if (urlBuilder != null)
                    //    {
                    //        urlBuilder.PreserveAspectRatio = true;
                    //        urlBuilder.ConversionFormatType = ImageStoreNET.Classes.Data.ConversionFormatTypes.WebSafe;
                    //        FGPStoolImage.ImageUrl = urlBuilder.ToString();
                    //    }
                    //    FGPStoolImagePlaceHolder.Visible = true;
                    //}

                    if (frequentGuestProgPage[SITE_FOOTER_HEADING] != null)
                    {
                        string siteFooterHeading = frequentGuestProgPage[SITE_FOOTER_HEADING] as string;
                        if (siteFooterHeading != null)
                        {
                            FGPBecomeAMemberHeader.Text = siteFooterHeading;
                            FGPBecomeAMemberHeaderPlaceHolder.Visible = true;
                        }
                    }

                    if (frequentGuestProgPage[SITE_FOOTER_TEXT_AREA] != null)
                    {
                        string siteFooterTextArea = frequentGuestProgPage[SITE_FOOTER_TEXT_AREA] as string;
                        if (siteFooterTextArea != null)
                        {
                            FGPTextArea.Text = siteFooterTextArea;
                            FGPTextAreaPlaceHolder.Visible = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates the list of new hotels to be displayed under New Hotels section 
        /// This is the Column 4 section of Site Footer
        /// </summary>
        private void DisplayNewHotelLinks()
        {
            NewHotelsList.Visible = false;
            PageData rootPage = ContentDataAccess.GetPageData(PageReference.RootPage, false);
            PageReference siteFooterPageReference = rootPage[EpiServerPageConstants.SITE_FOOTER_PAGE] as PageReference;

            if (siteFooterPageReference != null)
            {
                PageType siteFooterContainerPageType = ContentDataAccess.GetContainerPageType(ConfigurationManager.AppSettings[SITE_FOOTER_CONATINER_PAGE_TYPE_GUID], false);
                int siteFooterContainerPageTypeID = siteFooterContainerPageType != null ? siteFooterContainerPageType.ID : 0;
                PageDataCollection siteFooterContainerPagesCollection =
                    ContentDataAccess.GetPagesWithPageTypeID(siteFooterContainerPageTypeID, siteFooterPageReference);

                PageDataCollection siteFooterContainerColumnFourPages = new PageDataCollection();

                foreach (PageData siteFooterPage in siteFooterContainerPagesCollection)
                {
                    if (siteFooterPage[DISPLAY_IN_COLUMN_NUMBER] != null)
                    {
                        if (siteFooterPage[DISPLAY_IN_COLUMN_NUMBER].ToString().Equals("4"))
                        {
                            siteFooterContainerColumnFourPages.Add(siteFooterPage);
                        }
                    }
                }

                if (siteFooterContainerColumnFourPages != null)
                {
                    if (siteFooterContainerColumnFourPages != null && siteFooterContainerColumnFourPages.Count != 0)
                    {
                        PageData siteFooterNewHotelPage = (siteFooterContainerColumnFourPages[0] == null)
                                                              ? null
                                                              : siteFooterContainerColumnFourPages[0] as PageData;

                        if (siteFooterNewHotelPage != null)
                        {
                            //Merchandising:R3:Initial Design Document - Removal of Images from Footer
                            //if (siteFooterNewHotelPage[SITE_FOOTER_CONTAINER_STOOL_IMAGE] != null)
                            //{
                            //    string imageString = siteFooterNewHotelPage[SITE_FOOTER_CONTAINER_STOOL_IMAGE] as string;

                            //    ImageStoreNET.Classes.Util.UrlBuilder urlBuilder =
                            //        ImageStoreNET.Classes.Util.UrlBuilder.ParseUrl(imageString);
                            //    if (urlBuilder != null)
                            //    {
                            //        urlBuilder.PreserveAspectRatio = true;
                            //        urlBuilder.ConversionFormatType =
                            //            ImageStoreNET.Classes.Data.ConversionFormatTypes.WebSafe;
                            //        NewHotelsStoolImage.ImageUrl = urlBuilder.ToString();
                            //    }
                            //    this.NewHotelsStoolImagePlaceHolder.Visible = true;
                            //}

                            //Merchandising:R3:Initial Design Document - Removal of Images from Footer
                            if (siteFooterNewHotelPage[SITE_FOOTER_HEADING] != null)
                            {
                                NewHotelsHeading.Text = siteFooterNewHotelPage[SITE_FOOTER_HEADING] as string;
                                this.NewHotelsHeadingPlaceHolder.Visible = true;
                            }
                            else
                                this.NewHotelsHeadingPlaceHolder.Visible = false;

                            if (siteFooterNewHotelPage[SITE_FOOTER_CONTAINER_TEXT_AREA] != null)
                            {
                                ContainerDescription.Visible = true;
                                Description.Visible = true;
                                Description.Text = siteFooterNewHotelPage[SITE_FOOTER_CONTAINER_TEXT_AREA] as string;
                            }
                            else
                            {
                                ContainerDescription.Visible = false;
                                Description.Visible = false;
                            }

                            NewHotelsLinksPlaceHolder.Visible = false;

                            if (siteFooterNewHotelPage[NEW_HOTELS_LINK_TEXT] != null)
                            {
                                string expansionPageLinkText = siteFooterNewHotelPage[NEW_HOTELS_LINK_TEXT] as string;

                                if (!String.IsNullOrEmpty(expansionPageLinkText))
                                {
                                    NewHotelsLinks.Visible = true;
                                    NewHotelsLinks.Attributes.Add("class", "list");
                                    NewHotelsLinksPlaceHolder.Visible = true;

                                    ExpansionPageLink.InnerText = expansionPageLinkText;
                                    if (siteFooterNewHotelPage[NEW_HOTELS_LINK] != null)
                                    {
                                        ExpansionPageLink.HRef =
                                            GlobalUtil.GetUrlToPage(
                                                siteFooterNewHotelPage[NEW_HOTELS_LINK] as PageReference);
                                    }
                                }
                                else
                                {
                                    NewHotelsLinksPlaceHolder.Visible = false;
                                    NewHotelsLinks.Attributes.Add("class", "list hideElem");
                                }
                            }

                            if (siteFooterNewHotelPage.PageLink != null)
                            {
                                NewHotelsList.Visible = true;
                                PageDataCollection siteFooterNewHotelPages =
                                DataFactory.Instance.GetChildren(siteFooterNewHotelPage.PageLink);
                                GuestProgramPageFilter guestProgramPageFilter = new GuestProgramPageFilter();
                                PageCollection invisiblePageFilter = new PageCollection();
                                siteFooterNewHotelPages = guestProgramPageFilter.Filter(siteFooterNewHotelPages);
                                siteFooterNewHotelPages = invisiblePageFilter.FilterInvisible(siteFooterNewHotelPages);
                                rptNewHotelsPageList.DataSource = siteFooterNewHotelPages;
                                rptNewHotelsPageList.DataBind();
                            }
                            else
                            {
                                NewHotelsList.Visible = false;
                            }
                        }
                    }
                }
            }
        }
        #endregion Private Methods

        #region ContainerPositionComparer Class

        /// <summary>
        /// Used to sort the Container Position in the columns
        /// </summary>
        private class ContainerPositionComparer : IComparer<PageData>
        {
            /// <summary>
            /// ICompare Implementation
            /// </summary>
            /// <param name="first">left side to compare</param>
            /// <param name="second">right side to compare</param>
            /// <returns>0 if eqal -ve if first LESS THAN second +ve if first GREATER THAN second</returns>
            public int Compare(PageData first, PageData second)
            {
                int returnValue = 1;
                if (first[CONTAINER_POSITION_IN_COLUMN] != null && second[CONTAINER_POSITION_IN_COLUMN] != null)
                {
                    if (((int)first[CONTAINER_POSITION_IN_COLUMN]) < ((int)second[CONTAINER_POSITION_IN_COLUMN]))
                        returnValue = -1;
                    else if (((int)first[CONTAINER_POSITION_IN_COLUMN]) == ((int)second[CONTAINER_POSITION_IN_COLUMN]))
                        returnValue = 0;
                    else
                        returnValue = 1;
                }


                return returnValue;
            }
        }

        #endregion ContainerPositionComparer Class
    }
}