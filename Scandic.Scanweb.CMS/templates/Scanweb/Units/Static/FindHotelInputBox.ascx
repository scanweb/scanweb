<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FindHotelInputBox.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.FindHotelInputBox" %>
<%@ Register Src="~/Templates/Booking/Units/DestinationSearch.ascx" TagName="DestinationSearch" TagPrefix="uc1" %>

<div><input id="txtHotelName" type="text" class="inputTextBig" autocomplete="off" runat="server" /></div>
<div id="autosuggest1" class="autosuggest"><ul></ul></div>
 
 <uc1:DestinationSearch runat="server" />
 <script type="text/javascript">
 new AutoSuggest(document.getElementById('autosuggest1'));
 </script>