using System;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// HotelOverviewTop
    /// </summary>
    public partial class HotelOverviewTop : EPiServer.UserControlBase
    {
        private const string ImageMapPropertyPrefix = "ImageMap";
        private const string HotelLinksPropertyPrefix = "HotelLinks";

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            HotelSearch.SearchedHotel +=
                new EventHandler<FindHotelSearch.SearchedHotelEventArg>(HotelSearch_SearchedHotel);

            string hotelMapID = Request.QueryString["MapID"] as string ?? string.Empty;
            ImageMapContent.PageLink = CurrentPage.PageLink;
            ImageMapContent.PropertyName = ImageMapPropertyPrefix + hotelMapID;
            HotelLinksContent.PageLink = CurrentPage.PageLink;
            HotelLinksContent.PropertyName = HotelLinksPropertyPrefix + hotelMapID;
        }

        /// <summary>
        /// HotelSearch_SearchedHotel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HotelSearch_SearchedHotel(object sender, FindHotelSearch.SearchedHotelEventArg e)
        {
            HotelLinksContent.Visible = false;
            string hotelMapID = e.GetMapID;
            ImageMapContent.PageLink = CurrentPage.PageLink;
            ImageMapContent.PropertyName = ImageMapPropertyPrefix + hotelMapID;
        }
    }
}