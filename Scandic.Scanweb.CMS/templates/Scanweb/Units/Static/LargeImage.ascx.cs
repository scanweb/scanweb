//  Description					:   LargeImage                                            //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.Util.ImageVault;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// Code behind of LargeImage control.
    /// </summary>
    public partial class LargeImage : ScandicUserControlBase
    {
        private int imageWidth;

        /// <summary>
        /// Gets/Sets ContentPage
        /// </summary>
        public PageData ContentPage { get; set; }

        /// <summary>
        /// Gets/Sets ImagePropertyName
        /// </summary>
        public string ImagePropertyName { get; set; }

        /// <summary>
        /// Gets/Sets TopCssClass
        /// </summary>
        public string TopCssClass { get; set; }

        /// <summary>
        /// Gets/Sets ImageCssClass
        /// </summary>
        public string ImageCssClass { get; set; }

        /// <summary>
        /// Gets/Sets BottomCssClass
        /// </summary>
        public string BottomCssClass { get; set; }

        /// <summary>
        /// Gets/Sets ImageWidth
        /// </summary>
        public int ImageWidth
        {
            get { return imageWidth; }
            set { imageWidth = value; }
        }


        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Visible = (ContentPage[ImagePropertyName] != null);

            DataBind();
        }

        /// <summary>
        /// Gets image url.
        /// </summary>
        /// <returns></returns>
        protected string GetImageURL()
        {
            string imageString = ContentPage[ImagePropertyName] as string;

            if (imageString != null)
            {
                return WebUtil.GetImageVaultImageUrl(imageString, imageWidth);                
            }
            return string.Empty;
        }

        /// <summary>
        /// Gets alt text
        /// </summary>
        /// <returns></returns>
        protected string GetAltText()
        {
            LangAltText altText = new LangAltText();
            return altText.GetAltText(CurrentPage.LanguageID, ContentPage[ImagePropertyName].ToString());
        }
    }
}