using System.Configuration;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// EpiserverSEO
    /// </summary>
    public partial class EpiserverSEO : ScandicUserControlBase
    {
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            DataBind();
        }

        /// <summary>
        /// Returns the name of the current page
        /// </summary>
        /// <returns>Page name</returns>
        protected string GetSeoPxId()
        {
            return (ConfigurationManager.AppSettings["episerverseo." + Request.Url.Host] as string ??
                    (ConfigurationManager.AppSettings["EpiserverSeoPxId"] as string));
        }
    }
}