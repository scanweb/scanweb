using System;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Entity;
using log4net;

namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// Tradedoubler
    /// </summary>
    public partial class Tradedoubler : ScandicUserControlBase
    {
        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            try
            {
                HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
                if (hotelSearch != null)
                {
                    if (hotelSearch.SearchingType == SearchType.REGULAR)
                    {
                        PlaceTradedoublerTrackingCode();
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage
                    ("Tradedoubler - Error when placing Tradedoubler tracking code on booking confirmation page. Exception: "
                     + ex.ToString());
            }
        }

        /// <summary>
        /// Sends information back to Tradedoubler if the current booking was
        /// initiated from a Tradedoubler affiliate
        /// </summary>
        protected void PlaceTradedoublerTrackingCode()
        {
            string organization = "927706";
            string checksumCode = "3685";
            string orderValue = GetOrderValue();
            string currency = GetOrderCurrency();
            string eventId = "32541";
            bool isSale = true;
            bool isSecure = true;
            string orderNumber = string.Empty;
            if (SogetiVariableSessionWrapper.ReservationNumberForThirdParty != string.Empty)
                orderNumber = SogetiVariableSessionWrapper.ReservationNumberForThirdParty;
            string tduid = "";
            if (MiscellaneousSessionWrapper.TradeDoubler != null)
                tduid = MiscellaneousSessionWrapper.TradeDoubler;
            string reportInfo =
                "f1=" + GetHotelID() + "&f2=" + GetHotelName() + "&f3=" + orderValue + "&f4=" + GetNumberOfNights();
            reportInfo = Server.UrlEncode(reportInfo);
            if (Request.Cookies["TRADEDOUBLER"] != null)
                tduid = Request.Cookies["TRADEDOUBLER"].Value;
            string domain, checkNumberName;
            string scheme, trackBackUrl;
            if (isSale)
            {
                domain = "tbs.tradedoubler.com";
                checkNumberName = "orderNumber";
            }
            else
            {
                domain = "tbl.tradedoubler.com";
                checkNumberName = "leadNumber";
                orderValue = "1";
            }
            string checksumInput = checksumCode + orderNumber + orderValue;
            System.Security.Cryptography.MD5 md5Crypto = new
                System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bytes = md5Crypto.ComputeHash(new
                                                     System.Text.UTF8Encoding().GetBytes(checksumInput));
            string checksum = "v04";
            foreach (byte singleByte in bytes)
                checksum += singleByte.ToString("x2");
            if (isSecure)
                scheme = "https";
            else
                scheme = "http";
            trackBackUrl = scheme + "://" + domain + "/report"
                           + "?organization=" + organization
                           + "&amp;event=" + eventId
                           + "&amp;" + checkNumberName + "=" + orderNumber
                           + "&amp;checksum=" + checksum
                           + "&amp;tduid=" + tduid
                           + "&amp;reportInfo=" + reportInfo;
            if (isSale)
            {
                trackBackUrl
                    += "&amp;orderValue=" + orderValue
                       + "&amp;currency=" + currency;
            }

            ltTradedoubler.Text = "<img src=\"" + trackBackUrl + "\" alt=\"\" style=\"border: none\"/>";
        }

        /// <summary>
        /// Gets the order value from the session object
        /// </summary>
        /// <returns>Order value as string</returns>
        protected string GetOrderValue()
        {
            string sValue = "0.00";
            if (SogetiVariableSessionWrapper.TotalAmountForThirdParty != string.Empty)
            {
                try
                {
                    string valueString = SogetiVariableSessionWrapper.TotalAmountForThirdParty;
                    string[] valueStringArray = valueString.Split(' ');
                    if (valueStringArray.Length > 0)
                        sValue = valueStringArray[0];
                }
                catch (Exception e)
                {
                }
            }
            return sValue;
        }

        /// <summary>
        /// Gets the currency from the session object
        /// </summary>
        /// <returns>Currency as string</returns>
        protected string GetOrderCurrency()
        {
            string sCurrency = "";
            if (SogetiVariableSessionWrapper.TotalAmountForThirdParty != string.Empty)
            {
                try
                {
                    string currencyString = SogetiVariableSessionWrapper.TotalAmountForThirdParty;
                    string[] currencyStringArray = currencyString.Split(' ');
                    if (currencyStringArray.Length > 1)
                        sCurrency = currencyStringArray[1];
                }
                catch (Exception e)
                {
                }
            }
            return sCurrency;
        }

        /// <summary>
        /// Gets the Hotel ID
        /// </summary>
        /// <returns>Hotel ID as string</returns>
        protected string GetHotelID()
        {
            string hotelID = string.Empty;
            HotelSearchEntity clonedSearchCriteria = SearchCriteriaSessionWrapper.CloneSearchCriteria;
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
            if (null != searchCriteria)
            {
                if (searchCriteria.SelectedHotelCode != null)
                {
                    hotelID = searchCriteria.SelectedHotelCode as string ?? string.Empty;
                }
                else
                {
                    if (clonedSearchCriteria != null)
                    {
                        hotelID = clonedSearchCriteria.SelectedHotelCode as string ?? string.Empty;
                    }
                }
            }
            return hotelID;
        }

        /// <summary>
        /// Gets the number of night in the booking
        /// </summary>
        /// <returns>Number of nights as string</returns>
        protected string GetNumberOfNights()
        {
            string noOfNights = string.Empty;
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.CloneSearchCriteria;
            if (null != hotelSearch)
            {
                noOfNights = hotelSearch.NoOfNights.ToString() as string ?? string.Empty;
            }
            return noOfNights;
        }

        /// <summary>
        /// GetArrivalDate
        /// </summary>
        /// <returns>ArrivalDate</returns>
        protected string GetArrivalDate()
        {
            string arrivalDate = string.Empty;
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.CloneSearchCriteria;
            if (null != hotelSearch)
            {
                arrivalDate =
                    hotelSearch.ArrivalDate != null ? hotelSearch.ArrivalDate.ToString("yyyy-MM-dd") : string.Empty;
            }
            return arrivalDate;
        }

        /// <summary>
        /// GetDepartureDate
        /// </summary>
        /// <returns>DepartureDate</returns>
        protected string GetDepartureDate()
        {
            string departureDate = string.Empty;
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.CloneSearchCriteria;
            if (null != hotelSearch)
            {
                departureDate =
                    hotelSearch.DepartureDate != null ? hotelSearch.DepartureDate.ToString("yyyy-MM-dd") : string.Empty;
            }
            return departureDate;
        }

        /// <summary>
        /// GetHotelName
        /// </summary>
        /// <returns>HotelName</returns>
        protected string GetHotelName()
        {
            string hotelName = string.Empty;
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.CloneSearchCriteria;
            if (null != hotelSearch)
            {
                hotelName = hotelSearch.SearchedFor.SearchString;
            }
            return hotelName;
        }

        /// <summary>
        /// LogMessage
        /// </summary>
        /// <param name="message"></param>
        private static void LogMessage(string message)
        {
            ILog customLog = LogManager.GetLogger("sogeti");
            if (customLog.IsInfoEnabled)
                customLog.Info(message);
        }
    }
}