//  Description					:   Header                                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.Core.Html;
using EPiServer.DataAbstraction;
using EPiServer.Globalization;
using EPiServer.Security;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util.DetectJavascript;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// The header of the website. Generates metadata tags, rss and css links.
    /// </summary>
    public partial class Header : UserControlBase
    {
        /// <summary>
        /// Oninit event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);            
            
        }
        
        /// <summary>
        /// Onload event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            PopulateMetadaData();
            if (MiscellaneousSessionWrapper.IsJavascriptEnabled == null)
            {
                MiscellaneousSessionWrapper.IsJavascriptEnabled = JavascriptControl.JavaScriptState.Enabled;
            }
        }

        /// <summary>
        /// Returns maximum no of days allowed to book in advance
        /// </summary>
        protected int GetMaxNoOfAllowedDaysForArrival()
        {
            return AppConstants.ALLOWED_DAYS_FOR_DATE_OF_ARRIVAL;
        }

        /// <summary>
        /// This will populate metadata info for selected pages.
        /// </summary>
        private void PopulateMetadaData()
        {
            PageBase page = (PageBase) Page;
            if (!String.IsNullOrEmpty(Request.QueryString.Get("hotelpage")))
            {
                switch (Request.QueryString.Get("hotelpage"))
                {
                    case "location":
                        CreateMetaDataforOfferAndMeetingConf("location");
                        if (CurrentPage != null)
                        {
                            if (CurrentPage["Title_Location"] != null)
                            {
                                page.Title = CurrentPage["Title_Location"].ToString();
                            }
                        }
                        break;
                    case "facilities":
                        int facilityPageID = -1;
                        PageData facilityPage = null;
                        if (int.TryParse(Request.QueryString.Get("facilityid"), out facilityPageID))
                        {
                            if (facilityPageID != -1)
                            {
                                try
                                {
                                    facilityPage = DataFactory.Instance.GetPage(new PageReference(facilityPageID),
                                                                                AccessLevel.NoAccess);
                                    if (facilityPage.CheckPublishedStatus(PagePublishedStatus.Published) &&
                                        facilityPage.QueryDistinctAccess(AccessLevel.Read))
                                        CreateMetaDataforHotel(facilityPage);
                                    else
                                        CreateMetaData();
                                }
                                catch (PageNotFoundException)
                                {
                                    Server.Transfer("/404/404notfound.aspx");
                                }
                            }
                        }
                        else
                            CreateMetaData();
                        if (facilityPage != null)
                        {
                            if (facilityPage["Title"] != null)
                            {
                                page.Title = facilityPage["Title"].ToString();
                            }
                        }
                        else
                            page.Title = GetPageTitle();
                        break;
                    case "additionalfacilities":
                        PageData contentPage = null;
                        if (PageIsHotelAdditionalInfoPage("facilityid"))
                        {
                            int contentPageID = -1;

                            if (int.TryParse(Request.QueryString.Get("facilityid"), out contentPageID))
                            {
                                if (contentPageID != -1)
                                {
                                    PageReference contentPageLink = new PageReference(contentPageID);
                                    contentPage = DataFactory.Instance.GetPage(contentPageLink);
                                    PageType hotelAdditionalInformationPageType =
                                        PageType.Load(
                                            new Guid(
                                                ConfigurationManager.AppSettings[
                                                    "HotelAdditionalInformationPageTypeGUID"]));
                                    if (contentPage.PageTypeID == hotelAdditionalInformationPageType.ID)
                                    {
                                        CreateMetaDataforHotel(contentPage);
                                    }
                                    else
                                        CreateMetaData();
                                }
                            }
                        }
                        else
                        {
                           if (int.TryParse(Request.QueryString.Get("facilityid"), out facilityPageID))
                            {
                                if (facilityPageID != -1)
                                {
                                    try
                                    {
                                        contentPage = DataFactory.Instance.GetPage(new PageReference(facilityPageID),
                                                                                   AccessLevel.NoAccess);
                                        if (contentPage.CheckPublishedStatus(PagePublishedStatus.Published) &&
                                            contentPage.QueryDistinctAccess(AccessLevel.Read))
                                            CreateMetaDataforHotel(contentPage);
                                        else
                                            CreateMetaData();
                                    }
                                    catch (PageNotFoundException)
                                    {
                                    Server.Transfer("/404/404notfound.aspx");
                                    }
                                }
                            }
                        }
                        if (contentPage != null)
                        {
                            if (contentPage["Title"] != null)
                            {
                                page.Title = contentPage["Title"].ToString();
                            }
                        }
                        else
                            page.Title = GetPageTitle();
                        break;
                    case "rooms":
                        int roomPageID = -1;
                        PageData roomPage = null;
                        if (int.TryParse(Request.QueryString.Get("roomid"), out roomPageID))
                        {
                            if (roomPageID != -1)
                            {
                                roomPage = DataFactory.Instance.GetPage(new PageReference(roomPageID),
                                                                        AccessLevel.NoAccess);
                                if (roomPage.CheckPublishedStatus(PagePublishedStatus.Published) &&
                                    roomPage.QueryDistinctAccess(AccessLevel.Read))
                                    CreateMetaDataforHotel(roomPage);
                                else
                                    CreateMetaData();
                            }
                            else
                                CreateMetaData();
                        }
                        if (roomPage != null)
                        {
                            if (roomPage["Title"] != null)
                            {
                                page.Title = roomPage["Title"].ToString();
                            }
                        }
                        else
                            page.Title = GetPageTitle();
                        break;
                    case "meetings":
                        CreateMetaDataforOfferAndMeetingConf("meetings");
                        if (CurrentPage != null)
                        {
                            if (CurrentPage["Title_MeetingAndConf"] != null)
                            {
                                page.Title = CurrentPage["Title_MeetingAndConf"].ToString();
                            }
                        }
                        break;
                    case "offers":
                        CreateMetaDataforOfferAndMeetingConf("offers");
                        if (CurrentPage != null)
                        {
                            if (CurrentPage["Title_Offers"] != null)
                            {
                                page.Title = CurrentPage["Title_Offers"].ToString();
                            }
                        }
                        break;
                    case "TravellerReviews":
                        CreateMetaDataforOfferAndMeetingConf("TravellerReviews");
                        if (CurrentPage != null)
                        {
                            if (CurrentPage["Title_TripAdvisor"] != null)
                            {
                                page.Title = CurrentPage["Title_TripAdvisor"].ToString();
                            }
                        }
                        break;
                    default:
                        CreateMetaData();
                        page.Title = GetPageTitle();
                        break;
                }
            }
            else
            {
                CreateMetaData();
                page.Title = GetPageTitle();
            }
        }

        /// <summary>
        /// Returns the right title tag. Title, if it is set, and PageName otherwise.
        /// </summary>
        protected string GetTitle()
        {
            PageBase page = (PageBase) Page;

            if (page.CurrentPage["Title"] != null)
            {
                return page.CurrentPage["Title"].ToString();
            }
            else
            {
                if (page.CurrentPage["Heading"] != null)
                {
                    return page.CurrentPage["Heading"].ToString();
                }
                else
                {
                    return page.CurrentPage.PageName.ToString();
                }
            }
        }

        /// <summary>
        /// Gets browser specific CSS
        /// </summary>
        /// <returns></returns>
        public string GetBrowserCSS()
        {
            if (Request.Browser.Type.ToString().Substring(0, 2) == "IE" &&
                Request.Browser.MajorVersion.ToString().Equals("6"))
                return "IE6";
            else if (Request.Browser.Type.ToString().Substring(0, 2) == "IE" &&
                     Request.Browser.MajorVersion.ToString().Equals("7"))
                return "IE7";
            else if (Request.Browser.Type.ToString().Substring(0, 2) == "Fi" &&
                     Request.Browser.MajorVersion.ToString().Equals("2"))
                return "Firefox20";
            else
                return "Firefox15";
        }

        /// <summary>
        /// Creates the metadata tags for the website
        /// </summary>
        private void CreateMetaData()
        {
            string metaDescription = GetPropertyString("MetaDescription", CurrentPage);
            if (string.IsNullOrEmpty(metaDescription))
            {
                if (CurrentPage.PageTypeName == "Offer" || CurrentPage.PageTypeName == "Hotel Additional Information")
                    metaDescription = GetPropertyString("MainIntroXHTML", CurrentPage);
                else
                metaDescription = GetPropertyString("MainIntro", CurrentPage);
                if (!metaDescription.Equals(string.Empty))
                {
                    metaDescription = TextIndexer.StripHtml(metaDescription, 0);
                    if (metaDescription.Length > 255)
                        metaDescription = metaDescription.Substring(0, 252) + "...";
                }
            }
            if (!metaDescription.Equals(string.Empty))
            {
                CreateMetaTag("description", metaDescription, false);
            }
            //string MetaDesc = GetPropertyString("MetaDescription", CurrentPage);
            //if (!MetaDesc.Equals(string.Empty))
            //    CreateMetaTag("MetaDescription", MetaDesc, true);
            string MetaKey = GetPropertyString("MetaKeywords", CurrentPage);
            if (!string.IsNullOrEmpty(MetaKey))
                //CreateMetaTag("MetaKeywords", MetaKey, true);
                CreateMetaTag("keywords", MetaKey, true);
            else
                CreateMetaTag("keywords", "Keywords", CurrentPage, false);

            CreateMetaTag("author", "MetaAuthor", CurrentPage, false);

            CreateMetaTag("rating", "General", false);

            CreateMetaTag("revisit-after", "4 weeks", false);

            CreateMetaTag("generator", "EPiServer", false);

            var page = HttpContext.Current.Handler as PageBase;
            string currentLanguage = EPiServer.Globalization.ContentLanguage.PreferredCulture.Name;
            if ((page != null && page.CurrentPage != null && currentLanguage != page.CurrentPage.LanguageID)
                || (CurrentPage["isNoindexSet"] != null && bool.Parse(CurrentPage["isNoindexSet"].ToString())))
                CreateMetaTag("robots", "noindex", false);
            else
                CreateMetaTag("robots", "all", false);

            CreateMetaTag("Content-Type", string.Format("text/html; charset={0}", "UTF-8"), true);

            if (CurrentPage.Created != DateTime.MinValue)
            {
                CreateMetaTag("creation_date", CurrentPage.Created.ToString("R"), false);
            }
            if (CurrentPage.Changed != DateTime.MinValue)
            {
                CreateMetaTag("last-modified", CurrentPage.Changed.ToString("R"), false);
            }
            if (CurrentPage.Changed != DateTime.MinValue)
            {
                CreateMetaTag("revised", CurrentPage.Changed.ToString("R"), false);
            }
            CreateMetaTag("Content-Language", CurrentPage.LanguageBranch, true);
        }

        /// <summary>
        /// Creates the metadata tags for the Hotel Pages
        /// </summary>
        private void CreateMetaDataforHotel(PageData hotelPage)
        {
            string metaDescription = GetPropertyString("MetaDescription", hotelPage);
            if (metaDescription.Equals(string.Empty))
            {
                if (hotelPage.PageTypeName == "Offer" || hotelPage.PageTypeName == "Hotel Additional Information")
                    metaDescription = GetPropertyString("MainIntroXHTML", hotelPage);
                else
                metaDescription = GetPropertyString("MainIntro", hotelPage);
                if (!metaDescription.Equals(string.Empty))
                {
                    metaDescription = TextIndexer.StripHtml(metaDescription, 0);
                    if (metaDescription.Length > 255)
                        metaDescription = metaDescription.Substring(0, 252) + "...";
                }
            }
            if (!string.IsNullOrEmpty(metaDescription))
            {
                CreateMetaTag("description", metaDescription, false);
            }

            //string MetaDesc = GetPropertyString("MetaDescription", hotelPage);
            //if (!MetaDesc.Equals(string.Empty))
            //    CreateMetaTag("MetaDescription", MetaDesc, true);
            string MetaKey = GetPropertyString("MetaKeywords", hotelPage);
            if (!string.IsNullOrEmpty(MetaKey))
                CreateMetaTag("keywords", MetaKey, true);
            else
                CreateMetaTag("keywords", "Keywords", hotelPage, false);

            CreateMetaTag("author", "MetaAuthor", hotelPage, false);

            CreateMetaTag("rating", "General", false);

            CreateMetaTag("revisit-after", "4 weeks", false);

            CreateMetaTag("generator", "EPiServer", false);

            if (!LanguageSelection.IsHostLanguageMatch(CurrentPage.LanguageID))
                CreateMetaTag("robots", "noindex", false);
            else
                CreateMetaTag("robots", "all", false);

            CreateMetaTag("Content-Type", string.Format("text/html; charset={0}", "UTF-8"), true);

            if (hotelPage.Created != DateTime.MinValue)
            {
                CreateMetaTag("creation_date", hotelPage.Created.ToString("R"), false);
            }
            if (hotelPage.Changed != DateTime.MinValue)
            {
                CreateMetaTag("last-modified", hotelPage.Changed.ToString("R"), false);
            }
            if (hotelPage.Changed != DateTime.MinValue)
            {
                CreateMetaTag("revised", hotelPage.Changed.ToString("R"), false);
            }
            CreateMetaTag("Content-Language", hotelPage.LanguageBranch, true);
        }

        /// <summary>
        /// Creates the metadata tags for Meetings and offers tab in hotel page.
        /// </summary>
        private void CreateMetaDataforOfferAndMeetingConf(string selectedPage)
        {
            string metaDescription = string.Empty;
            if (selectedPage.Equals("meetings"))
            {
                if (CurrentPage.Property["MetaDescription_MeetingAndConf"] != null)
                    metaDescription = CurrentPage.Property["MetaDescription_MeetingAndConf"].ToString();
            }
            else if (selectedPage.Equals("offers"))
            {
                if (CurrentPage.Property["MetaDescription_HotelOffers"] != null)
                    metaDescription = CurrentPage.Property["MetaDescription_HotelOffers"].ToString();
            }
            else if (selectedPage.Equals("location"))
            {
                if (CurrentPage.Property["MetaDescription_Location"] != null)
                    metaDescription = CurrentPage.Property["MetaDescription_Location"].ToString();
            }
            else if (selectedPage.Equals("TravellerReviews"))
            {
                if (CurrentPage.Property["MetaDescription_TripAdvisor"] != null)
                    metaDescription = CurrentPage.Property["MetaDescription_TripAdvisor"].ToString();
            }
            if (string.IsNullOrEmpty(metaDescription))
            {
                if (selectedPage.Equals("meetings"))
                    metaDescription = GetPropertyString("MeetingIntro", CurrentPage);
                if (!metaDescription.Equals(string.Empty))
                {
                    metaDescription = TextIndexer.StripHtml(metaDescription, 0);
                    if (metaDescription.Length > 255)
                        metaDescription = metaDescription.Substring(0, 252) + "...";
                }
            }
            if (!string.IsNullOrEmpty(metaDescription))
            {
                CreateMetaTag("description", metaDescription, false);
            }

            //string MetaDesc = string.Empty;
            //if (selectedPage.Equals("meetings"))
            //{
            //    if (CurrentPage.Property["MetaDescription_MeetingAndConf"] != null)
            //        MetaDesc = CurrentPage.Property["MetaDescription_MeetingAndConf"].ToString();
            //}
            //else if (selectedPage.Equals("offers"))
            //{
            //    if (CurrentPage.Property["MetaDescription_HotelOffers"] != null)
            //        MetaDesc = CurrentPage.Property["MetaDescription_HotelOffers"].ToString();
            //}
            //else if (selectedPage.Equals("location"))
            //{
            //    if (CurrentPage.Property["MetaDescription_Location"] != null)
            //        MetaDesc = CurrentPage.Property["MetaDescription_Location"].ToString();
            //}
            ////else if (selectedPage.Equals("TravellerReviews"))
            ////{
            ////    if (CurrentPage.Property["MetaDescription_TripAdvisor"] != null)
            ////        MetaDesc = CurrentPage.Property["MetaDescription_TripAdvisor"].ToString();
            ////}
            //if (!MetaDesc.Equals(string.Empty))
            //    CreateMetaTag("MetaDescription", MetaDesc, true);
            string MetaKey = string.Empty;
            if (selectedPage.Equals("meetings"))
            {
                if (CurrentPage.Property["MetaKeywords_MeetingAndConf"] != null)
                    MetaKey = CurrentPage.Property["MetaKeywords_MeetingAndConf"].ToString();
            }
            else if (selectedPage.Equals("offers"))
            {
                if (CurrentPage.Property["MetaKeywords_HotelOffers"] != null)
                    MetaKey = CurrentPage.Property["MetaKeywords_HotelOffers"].ToString();
            }
            else if (selectedPage.Equals("location"))
            {
                if (CurrentPage.Property["MetaKeywords_HotelLocation"] != null)
                    MetaKey = CurrentPage.Property["MetaKeywords_HotelLocation"].ToString();
            }
            //else if (selectedPage.Equals("TravellerReviews"))
            //{
            //    if (CurrentPage.Property["MetaKeywords_TripAdvisor"] != null)
            //        MetaKey = CurrentPage.Property["MetaKeywords_TripAdvisor"].ToString();
            //}
            if (!string.IsNullOrEmpty(MetaKey))
                //CreateMetaTag("MetaKeywords", MetaKey, true);
                CreateMetaTag("keywords", MetaKey, true);
            else
                CreateMetaTag("keywords", "Keywords", CurrentPage, false);

           CreateMetaTag("author", "MetaAuthor", CurrentPage, false);

           CreateMetaTag("rating", "General", false);

           CreateMetaTag("revisit-after", "4 weeks", false);

           CreateMetaTag("generator", "EPiServer", false);

            if (!LanguageSelection.IsHostLanguageMatch(CurrentPage.LanguageID))
                CreateMetaTag("robots", "noindex", false);
            else
                CreateMetaTag("robots", "all", false);

            CreateMetaTag("Content-Type", string.Format("text/html; charset={0}", "UTF-8"), true);

           if (CurrentPage.Created != DateTime.MinValue)
            {
                CreateMetaTag("creation_date", CurrentPage.Created.ToString("R"), false);
            }
            if (CurrentPage.Changed != DateTime.MinValue)
            {
                CreateMetaTag("last-modified", CurrentPage.Changed.ToString("R"), false);
            }
            if (CurrentPage.Changed != DateTime.MinValue)
            {
                CreateMetaTag("revised", CurrentPage.Changed.ToString("R"), false);
            }
            CreateMetaTag("Content-Language", CurrentPage.LanguageBranch, true);
        }

        /// <summary>
        /// Adds a meta tag control to the page header
        /// </summary>
        /// <param name="name">The name of the meta tag</param>
        /// <param name="content">The content of the meta tag</param>
        /// <param name="httpEquiv">True if the meta tag should be HTTP-EQUIV</param>
        private void CreateMetaTag(string name, string content, bool httpEquiv)
        {
            HtmlMeta tag = new HtmlMeta();
            if (httpEquiv)
            {
                tag.HttpEquiv = name;
            }
            else
            {
                tag.Name = name;
            }
            tag.Content = content;
            plhMetaDataArea.Controls.Add(tag);
        }

        /// <summary>
        /// Gets page title
        /// </summary>
        /// <returns></returns>
        protected string GetPageTitle()
        {
            if (CurrentPage["Title"] != null)
            {
                return CurrentPage["Title"].ToString();
            }
            else
            {
                return CurrentPage.PageName.ToString();
            }
        }

        /// <summary>
        /// Adds a meta tag control to the page header
        /// </summary>
        /// <param name="name">The name of the meta tag</param>
        /// <param name="propertyName">The name of the me tag page property</param>
        /// <param name="pageData">The page from where to get the property</param>
        /// <param name="httpEquiv">True if the meta tag should be HTTP-EQUIV</param>
        private void CreateMetaTag(string name, string propertyName, PageData pageData, bool httpEquiv)
        {
            string property = pageData[propertyName] as string;
            if (property != null)
            {
                CreateMetaTag(name, property, httpEquiv);
            }
        }
        /// <summary>
        /// Adds a meta tag control to the page header
        /// </summary>
        /// <param name="name">The name of the meta tag</param>
        /// <param name="propertyName">The name of the me tag page property</param>
        /// <param name="pageData">The page from where to get the property</param>
        /// <param name="httpEquiv">True if the meta tag should be HTTP-EQUIV</param>
        private void CreateMetaTagforHotel(string name, string propertyName, PageDataCollection pages, bool httpEquiv)
        {
            StringBuilder property = new StringBuilder();
            foreach (PageData pageData in pages)
            {
                property.Append(pageData[propertyName] as string);
            }
                if (property != null)
                {
                    CreateMetaTag(name, property.ToString(), httpEquiv);
                }
            
        }
        /// <summary>
        /// Gets a page property as a string
        /// </summary>
        /// <param name="PropertyName">The name of the property to get</param>
        /// <param name="pageData">The page from where to get the property</param>
        /// <returns>A string representation of the page property</returns>
        private static string GetPropertyString(string PropertyName, PageData pageData)
        {
            return pageData[PropertyName] as string ?? String.Empty;
        }

        /// <summary>
        /// This will check if page is additional hotel additional page type.
        /// </summary>
        /// <param name="idstring"></param>
        /// <returns></returns>
        private bool PageIsHotelAdditionalInfoPage(string idstring)
        {
            if (Request.QueryString[idstring] != null)
            {
                int pageID = -1;

                if (int.TryParse(Request.QueryString.Get(idstring), out pageID))
                {
                    try
                    {
                        PageData showPage = DataFactory.Instance.GetPage(new PageReference(pageID), AccessLevel.NoAccess);
                        PageType hotelAdditionalInformationPageType =
                            PageType.Load(
                                new Guid(ConfigurationManager.AppSettings["HotelAdditionalInformationPageTypeGUID"]));
                        return showPage.PageTypeID == hotelAdditionalInformationPageType.ID;
                    }
                    catch (PageNotFoundException)
                    {
                        return false;
                    }
                }
            }
            return false;
        }
    }
}