<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfirmationHotelMap.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.ConfirmationHotelMap" %>

<%@ Import Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3" %>
<%@ Register Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3" TagPrefix="Scandic" %>

<%--<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Scanweb/Javascript/gmaps-utility-library/markermanager_packed.js")%>"></script>
<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Scanweb/Javascript/gmaps-utility-library/labeledmarker_packed.js")%>"></script>
<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Scanweb/Javascript/gmaps-utility-library/extinfowindow_packed.js")%>"></script>--%>

<div id="confirmationMap" style="position:relative;">
   <div id="GMapV3" style="width:718px;height:399px">
    
    <div  class="jqmWindow" id="progressBar">
        <div id="BookingProgressDiv" runat="server" class="loadingIcon">
	        <img src="<%=ResolveUrl("~/templates/Scanweb/Styles/Default/Images/loadingIcon.gif")%>" alt="image" align="top" />
		    <strong class="loadingText"><%= EPiServer.Core.LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Static/FindHotelSearch/Loading")%></strong>
        </div>
    </div>
    <Scandic:GMapControl ID="ConfHotelGMapControl"/></div>
</div>