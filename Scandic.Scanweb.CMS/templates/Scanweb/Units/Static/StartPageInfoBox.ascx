<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StartPageInfoBox.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.StartPageInfoBox" %>


<div id="boxcontainer">
    <div class="toprow">
        <div class="toprowleft"></div>
        <div class="toprowmiddle">
            <div class="toprowmiddleleft"><EPiServer:Property PropertyName="InfoBoxHeading" runat="server" /></div>
            <div class="toprowmiddleright">
                <div class="rightlistimage"><img src="~/Templates/Scanweb/Styles/Default/Images/white_arrow_right.gif" alt=" " /></div>
                <div style="float:left;"><EPiServer:Property PropertyName="InfoBoxPagelink" runat="server" /></div>
            </div>
        </div>
        <div class="toprowright"></div>
    </div>
</div>

<div class="startPageInfoBoxContent" >
    <EPiServer:Property PropertyName="InfoBoxContent" runat="server" />
</div>

<div class="startPageInfoBoxBottomImg" ></div>