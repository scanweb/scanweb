<%@ Control Language="C#" EnableViewState="false" AutoEventWireup="true" CodeBehind="InfoBox.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.InfoBox" %>
<div class="InfoBox">
    <asp:PlaceHolder ID="HeaderPlaceHolder" runat="server">
     <h3 class="darkHeading"><%= Header %></h3>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="BodyPlaceHolder" runat="server">        
        <EPiServer:Property ID="BodyProperty" runat="server" />
    </asp:PlaceHolder>
</div>