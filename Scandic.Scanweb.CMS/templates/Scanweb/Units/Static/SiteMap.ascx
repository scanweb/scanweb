<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteMap.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.SiteMap" %>

 <div id="SiteMap" class="siteMapMainArea">
    <EPiServer:PageList runat="server" ID="SiteMapList" PageLink="<%# IndexRoot %>">
        <ItemTemplate>
        <asp:PlaceHolder ID="PlaceHolder1" Visible="<%# ShouldBeVisible(Container.CurrentPage) %>" runat="server">
            <ul class="SiteMapColumn">
                
            <EPiServer:Property ID="Property1" CssClass="IconLink" PropertyName="PageLink" runat="server" />
                <EPiServer:PageTree runat="server" ExpandAll="true" id="SiteMapTree" PageLink="<%# Container.CurrentPage.PageLink %>">
			        <HeaderTemplate>
				        <ul class="SiteMapInner">
			        </HeaderTemplate>
			        <IndentTemplate>
				        <ul>
			        </IndentTemplate>
			        <ItemHeaderTemplate>
				        <li class="<%# Container.CurrentPage["GuestProgramVisibleWhenLoggedIn"] != null
                                         ? "SiteMapElementNotVisible"
                                         : "SiteMapElementVisible" %>">
			        </ItemHeaderTemplate>
			        <ItemTemplate>
				        <EPiServer:Property ID="Property1" PropertyName="PageLink" runat="server" />
			        </ItemTemplate>
			        <ItemFooterTemplate>
				        </li>
			        </ItemFooterTemplate>
			        <UnindentTemplate>
				        </ul>
			        </UnindentTemplate>
			        <FooterTemplate>
				        </ul>
			        </FooterTemplate>
		        </EPiServer:PageTree>
		    
		    </ul>
		</asp:PlaceHolder>
        </ItemTemplate>
    </EPiServer:PageList>
    <script type="text/javascript">
        $(document).ready(function() {
            $('ul.SiteMapColumn:eq(2),ul.SiteMapColumn:eq(4)').addClass('SiteMapLastColumn');
            $('ul.SiteMapColumn:eq(2)').css({ 'left': '241px', 'top': $('ul.SiteMapColumn:eq(1)').height() + 20 });
            $('ul.SiteMapColumn:eq(4)').css({ 'left': '482px', 'top': $('ul.SiteMapColumn:eq(3)').height() + 20 });
        });
    </script>
    </div>