<%@ Control Language="C#" AutoEventWireup="true" Codebehind="PopUp.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.PopUp" %>
<div class="PopUp" id="Top">
    <div class="PopUpTop">
        <div class="ClosePopUp">
            <a class="CloseWindowPic" onclick="self.close();">
                <EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Units/Static/PopUp/CloseLink"
                    runat="server" />
            </a>
        </div>
    </div>
</div>
<div class="smallPopup">
    <div class="PopUpHeader1">
     		<div class="HeaderLeft"> </div>
            <h1><asp:Literal ID="PageHeading" runat="server"> </asp:Literal></h1>
    </div>
    <!--Naresh AMS Patch7 artf1258221 : Scanweb - Issue regarding "back"-function -->
   <div class="PopUpBody">
    <asp:PlaceHolder ID="ReturnLinkPlaceHolder" Visible="false" runat="server">
        <div class="PopUpReturnLink">
            <asp:HyperLink ID="PopUpHyperLink" CssClass="IconLink" href="javascript:history.go(-1)" runat="server"> 
            </asp:HyperLink>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="ContentPlaceHolder" runat="server">
        
            <EPiServer:Property PropertyName="MainBody" runat="server" />

    </asp:PlaceHolder>
    <asp:Panel ID="panel1" CssClass="PopUpList" Visible="false" runat="server">
        <EPiServer:PageList ID="poplist" runat="server">
            <ItemTemplate>
                <div class="LinkListItem">
                    <div class="<%# !IsLastItem(Container.CurrentPage.PageLink.ID) ? "NotLastLink" : "LastLink" %>">
                        <EPiServer:Property ID="Property1" CssClass="IconLink" PropertyName="PageLink" runat="server" />
                    </div>
                </div>
            </ItemTemplate>
        </EPiServer:PageList>
    </asp:Panel>
	<div style="clear:both;">&nbsp;</div>
	 </div>
</div>
