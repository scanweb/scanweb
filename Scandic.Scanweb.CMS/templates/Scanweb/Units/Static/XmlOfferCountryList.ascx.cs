using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.CMS.code.Util.HotelOfferList;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// XmlOfferCountryList
    /// </summary>
    public partial class XmlOfferCountryList : EPiServer.UserControlBase
    {
        public XMLOfferCountry country;
        public string PriceType;
        
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            

            foreach (XMLOfferCity city in country.GetListOfCity())
            {
                Scandic.Scanweb.CMS.Templates.Units.Static.XmlOfferCityList CityControl = (Scandic.Scanweb.CMS.Templates.Units.Static.XmlOfferCityList)LoadControl("\\Templates\\Scanweb\\Units\\Static\\XmlOfferCityList.ascx");
                CityControl.City = city;
                CityControl.PriceType = PriceType;
                CityListPlaceHolder.Controls.Add(CityControl);
            }
            
        }
        /// <summary>
        /// GetCountryName
        /// </summary>
        /// <returns>CountryName</returns>
        protected string GetCountryName()
        {
            return this.country.CountryName;
        }

       

    }
}