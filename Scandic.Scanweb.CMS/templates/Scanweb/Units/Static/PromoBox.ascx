<%@ Control Language="C#" Codebehind="PromoBox.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.PromoBox" %>
<div class="<%= GetCssClass() %><%= GetAlertBoxStyle() %>">
    <asp:PlaceHolder ID="PromoBoxOfferPlaceHolder" runat="server">
    <div class="regular">
    <div class="stoolHeader">
    <div class="hd sprite"></div>
    <div class="cnt">
     <asp:PlaceHolder ID="HeadingPlaceHolder" Visible="false" runat="server">
                <h2 class="stoolHeading">
                    <asp:Literal ID="HeadingText" runat="server" /></h2>
            </asp:PlaceHolder>
    </div>
    <div class="ft sprite">&nbsp;</div>
    </div>
    
    <div class="cnt">
    
    <asp:PlaceHolder ID="ImagePlaceHolder" Visible="false" runat="server">
                <asp:Image ID="BoxImage" runat="server" CssClass="smallPBoxStartImage" />
            </asp:PlaceHolder>
            
            <div class="innerContent">
            <asp:PlaceHolder ID="HotelDescriptionPlaceHolder" Visible="false" runat="server">
                <p>
                    <asp:Literal ID="HotelDescription" runat="server"></asp:Literal></p>
            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="PageLinkPlaceHolder1" Visible="false" runat="server">
            
                <asp:Literal ID="PageLink1" runat="server" />
                
            </asp:PlaceHolder>
            </div>
        </div>
        
        <div class="ft sprite"></div>
            </div>
            
   
    </asp:PlaceHolder>
</div>


