// <copyright file="HotelOverviewPageGoogleMap.ascx.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>01-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Contains code behind logic for the control HotelOverviewPageGoogleMap.ascx</summary>

#undef FINDAHOTEL_PERFORMANCE

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.Map;
using Scandic.Scanweb.CMS.code.Util.Map.GoogleMap;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// This control shows the google map on HotelOverView Page
    /// This page is also called as FindAHotel page.
    /// </summary>
    public partial class HotelExpansionGoogleMap : ScandicUserControlBase
    {
        #region Protected Events

        /// <summary>
        /// Page_PreRender Event of the Page.
        /// Calls a function RenderGoogleMap to render the google map
        /// on the page with selected country/city/hotel location
        /// after the tree view loads and fills up the session with
        /// selected node.
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event params</param>
        /// <remarks>Find a hotel release</remarks>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Page.Request.Url.ToString().StartsWith("https"))
            {
                RenderGoogleMap(false);
            }
        }
       #endregion 

        #region Private Methods

        /// <summary>
        /// This method does the following things
        /// 1. Creates two list of selected and other hotel 'Node's
        /// 2. Create hotel units with out numbering(OTHER than selected)
        /// 3. Sort and create hotel units with numbering(selected)
        /// 4. Set map options
        /// 5. Set map data
        /// </summary>
        /// <param name="enableDirections">Determines if directions should be enabled</param>
        private void RenderGoogleMap(bool enableDirections)
        {
#if FINDAHOTEL_PERFORMANCE
            long startTime = DateTime.Now.Ticks;
#endif
            try
            {
                List<Node> allHotelNodes = new List<Node>();

                Node rootNode = ContentDataAccess.GetPageDataNodeTree(false);
                if (rootNode != null)
                {
                    GetHotelNodesRecursive(rootNode, allHotelNodes);
                }

                IList<MapUnit> hotels = new List<MapUnit>();

                AddHotelNodes(allHotelNodes, hotels, InfoBoxType.ADVANCED, false);

                SetMapOptions();

                SetMapData(null, hotels);
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
            }
#if FINDAHOTEL_PERFORMANCE
            long endTime = DateTime.Now.Ticks;
            TimeSpan tp = new TimeSpan(endTime - startTime);
            AppLogger.LogInfoMessage("HotelOverviewPageGoogleMap.ascx.cs:RenderGoogleMap() running time     ->  Minutes:" + tp.Minutes + " Seconds:" + tp.Seconds + " Milliseconds:" + tp.Milliseconds);
#endif
        }

        /// <summary>
        /// Get all the hotel nodes under the supplied rootPage recursively
        /// </summary>
        /// <param name="rootPage">Node underwhich search has to be taken place</param>
        /// <param name="hotelNodes">list to be filled up with results</param>
        private void GetHotelNodesRecursive(Node rootPage, List<Node> hotelNodes)
        {
            if (rootPage != null)
            {
                IList<Node> subNodes = rootPage.GetChildren();
                if (subNodes != null && subNodes.Count > 0)
                {
                    foreach (Node node in subNodes)
                    {
                        GetHotelNodesRecursive(node, hotelNodes);
                    }
                }
                else
                {
                    PageData pagaData = rootPage.GetPageData();
                    if (pagaData != null)
                    {
                        if ((PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID) ==
                            pagaData.PageTypeID)
                        {
                            hotelNodes.Add(rootPage);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds a new map unit to the list of hotels
        /// </summary>
        /// <param name="hotelNodes">Node list carrying hotel data</param>
        /// <param name="hotels">list on which new units has to be added</param>
        /// <param name="infoBoxType">type of infobox(basic/advanced)</param>
        /// <param name="enableUnitNumbering">true if unit has to be numbered false otherwise</param>
        private void AddHotelNodes(List<Node> hotelNodes, IList<MapUnit> hotels, InfoBoxType infoBoxType,
                                   bool enableUnitNumbering)
        {
            try
            {
                if (hotelNodes != null && hotelNodes.Count > 0)
                {
                    for (int i = 0; i < hotelNodes.Count; i++)
                    {
                        Node hotelNode = hotelNodes[i];
                        if (hotelNode != null)
                        {
                            PageData hotelPageData = hotelNode.GetPageData();
                            if ((PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID) ==
                                hotelPageData.PageTypeID)
                            {
                                string iconUrl = string.Empty;
                                string shadowUrl = string.Empty;
                                string hotelCategory = string.Empty;
                                PageReference promotionalPageLink = null;
                                string promotionalPageLinkText = string.Empty;
                                PageData pageDataPromoPageLink = null;
                                string promotionalPageLinkUrl = string.Empty;
                                bool displayPromoLink = false;

                                if (hotelPageData["HotelCategory"] != null &&
                                    !string.IsNullOrEmpty(hotelPageData["HotelCategory"].ToString()))
                                {
                                    hotelCategory = hotelPageData["HotelCategory"].ToString();
                                }

                                if (hotelPageData["DisplayPromoLink"] != null)
                                {
                                    displayPromoLink = true;

                                    if (hotelPageData["PromotionalPageLinkText"] != null &&
                                        !string.IsNullOrEmpty(hotelPageData["PromotionalPageLinkText"].ToString()))
                                    {
                                        promotionalPageLinkText = hotelPageData["PromotionalPageLinkText"] as string ??
                                                                  string.Empty;
                                    }

                                    if (hotelPageData["PromotionalPageLink"] != null &&
                                        !string.IsNullOrEmpty(hotelPageData["PromotionalPageLink"].ToString()))
                                    {
                                        promotionalPageLink = hotelPageData["PromotionalPageLink"] as PageReference ??
                                                              null;
                                        if (promotionalPageLink != null)
                                        {
                                            pageDataPromoPageLink = DataFactory.Instance.GetPage(promotionalPageLink,
                                                                                                 EPiServer.Security.
                                                                                                     AccessLevel.
                                                                                                     NoAccess);
                                            promotionalPageLinkUrl = GetFriendlyURLToPage(
                                                pageDataPromoPageLink.PageLink, pageDataPromoPageLink.LinkURL);
                                        }
                                    }
                                }

                                GetMarkerUrls(hotelCategory, out iconUrl, out shadowUrl);
                                MapUnit hotelDatasource = new MapHotelUnit(
                                    (double) hotelPageData["GeoX"],
                                    (double) hotelPageData["GeoY"],
                                    -1,
                                    iconUrl,
                                    shadowUrl,
                                    hotelPageData["Heading"] as string,
                                    hotelPageData["HotelBookingImage"] as string,
                                    hotelPageData["HotelBookingDescription"] as string,
                                    hotelPageData["StreetAddress"] as string,
                                    hotelPageData["PostCode"] as string,
                                    hotelPageData["PostalCity"] as string,
                                    hotelPageData["City"] as string,
                                    hotelPageData["Country"] as string,
                                    GetFriendlyURLToPage(hotelPageData.PageLink, hotelPageData.LinkURL) as string,
                                    GetDeepLinkingURL(hotelPageData["OperaID"] as string ?? string.Empty),
                                    hotelPageData["CityCenterDistance"] != null
                                        ? (double) hotelPageData["CityCenterDistance"]
                                        : 0,
                                    infoBoxType,
                                    enableUnitNumbering,
                                    enableUnitNumbering == true ? i + 1 : 0,
                                    hotelPageData["OperaID"] as string ?? string.Empty,
                                    hotelCategory,
                                    promotionalPageLinkText,
                                    promotionalPageLinkUrl,
                                    displayPromoLink
                                    );

                                hotels.Add(hotelDatasource);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
            }
        }

        /// <summary>
        /// Gets the marker urls.
        /// </summary>
        /// <param name="hotelCategory">The hotel category.</param>
        /// <param name="iconUrl">The icon URL.</param>
        /// <param name="shadowUrl">The shadow URL.</param>
        private void GetMarkerUrls(string hotelCategory, out string iconUrl, out string shadowUrl)
        {
            switch (hotelCategory)
            {
                case AppConstants.DEFAULT:
                    {
                        iconUrl = string.Format("http://{0}/{1}", Request.Url.Host,
                                                "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png");
                        shadowUrl = string.Empty;
                    }
                    break;
                case AppConstants.RECENTLYOPENED_VALUE:
                    {
                        iconUrl = string.Format("http://{0}/{1}", Request.Url.Host,
                                                "/Templates/Scanweb/Styles/Default/Images/Icons/recentlyopened_hotel.png");
                        shadowUrl = string.Empty;
                    }
                    break;
                case AppConstants.COMINGSOON_VALUE:
                    {
                        iconUrl = string.Format("http://{0}/{1}", Request.Url.Host,
                                                "/Templates/Scanweb/Styles/Default/Images/Icons/comingsoon_hotel.png");
                        shadowUrl = string.Empty;
                    }
                    break;
                default:
                    {
                        iconUrl = string.Format("http://{0}/{1}", Request.Url.Host,
                                                "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png");
                        shadowUrl = string.Empty;
                    }
                    break;
            }
        }

        /// <summary>
        /// Set different generic parameters to the map
        /// </summary>
        private void SetMapOptions()
        {
            CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            GMapControl1.GoogleMapKey = (ConfigurationManager.AppSettings["googlemaps." + Request.Url.Host] as string ?? (ConfigurationManager.AppSettings["DevKey"] as string));
            GMapControl1.MarkerLatitudeField = "latitude";
            GMapControl1.MarkerLongitudeField = "longitude";
            GMapControl1.InfoBoxType = InfoBoxType.ADVANCED;
            GMapControl1.MapPageType = MapPageType.EXPANSION;
            GMapControl1.RecentlyOpenedHotels = true;
            GMapControl1.ComingSoonHotels = true;
        }

        /// <summary>
        /// Set zoom and center of the map according to the selected node
        /// and sets the hotelunit list data to the map
        /// </summary>
        /// <param name="selectedNode">selected node</param>
        /// <param name="hotels">list of map unit</param>
        private void SetMapData(Node selectedNode, IList<MapUnit> hotels)
        {
            try
            {
                GMapControl1.DataSource = hotels;
                GMapControl1.AutoCenterAndZoom();
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
                HandleCmsCoOrdinateDataError(hotels);
            }
            finally
            {
                GMapControl1.DataBind();
            }
        }
        
        /// <summary>
        /// Gracefully handles all exception cases and CMS data issues
        /// Simply shows all the hotels in the map and zooms and center(AUTO)
        /// </summary>
        /// <param name="hotels">all hotels</param>
        private void HandleCmsCoOrdinateDataError(IList<MapUnit> hotels)
        {
            GMapControl1.DataSource = hotels;
            GMapControl1.AutoCenterAndZoom();
        }
        #endregion 
    }
}