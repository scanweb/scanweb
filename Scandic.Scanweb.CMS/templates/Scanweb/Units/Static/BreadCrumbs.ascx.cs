//  Description					: BreadCrumbs                                             //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Provide links back to each previous page that the user navigated through in order 
    /// to get to the current page. In thew case of the trail being too long, only the start page,
    /// parent page and the current page are displayed.
    /// </summary>
    public partial class BreadCrumbs : ScandicUserControlBase
    {
        private static string _link = "<a href=\"{0}\" title=\"{1}\">{2}</a>";
        private static string _separator = " <span class='white'>></span> ";
        private static int _maxLength = 1000;
        private static string startPageURL = string.Empty;
        private static string wastePageUrl = string.Empty;

        #region UpdateLoginStatus

        /// <summary>
        /// Display the Points of the User
        /// </summary>
        /// <returns></returns>
        public void DisplayPoints()
        {
            string pointString = WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/pointsheading");
            if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null) // Refactor - Added
            {
                double noOfPoints = LoyaltyDetailsSessionWrapper.LoyaltyDetails.CurrentPoints;
                string generatedString = string.Format(pointString, noOfPoints);
                placeholderPoint.InnerHtml = generatedString;
            }
        }

        #endregion UpdateLoginStatus

        /// <summary>
        /// In case the breadcrumb string is longer then the specified MaxLength, 
        /// a shorter alternative is used where only the start page, parent page
        /// and the current page are shown.
        /// </summary>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            //DisplayPoints();

            PageData startPage = DataFactory.Instance.GetPage(PageReference.StartPage);
            startPageURL = GetFriendlyURLToPage(startPage.PageLink, startPage.LinkURL);

            PageData wastePage = DataFactory.Instance.GetPage(PageReference.WasteBasket);
            wastePageUrl = GetFriendlyURLToPage(wastePage.PageLink, wastePage.LinkURL);

            string crumbs = GetParentLink(CurrentPage);
            if (crumbs.Length > MaxLength)
            {
                Breadcrumbs.Text = GetShortCrumbs(CurrentPage);
            }
            else
            {
                Breadcrumbs.Text = crumbs;
            }
        }

        /// <summary>
        /// This is a new method after Find A Hotel release which creates a string of breadcrumbs by iterating upwards in the page tree structure
        /// </summary>
        /// <returns>A string of bredcrumb links</returns>
        private string GetParentLink(PageData page)
        {
            if (page != null)
            {
                string boldStart = string.Empty;
                string boldEnd = string.Empty;

                if (page.PageLink.ID == CurrentPage.PageLink.ID)
                {
                    boldStart = "<b>";
                    boldEnd = "</b>";
                }
                string link = GetLink(page);
                if (page.PageLink == PageReference.StartPage)
                {
                    return string.Format(_link, startPageURL, Translate("/navigation/startpagelinktitle"),
                                         Translate("/navigation/startpage"));
                }

                if (page.PageLink == PageReference.WasteBasket)
                {
                    return string.Format(_link, wastePageUrl, Translate("/navigation/wastepagelinktitle"),
                                         Translate("/navigation/wastepage"));
                }

                if (page.VisibleInMenu)
                {
                    return GetParentLink(GetPage(page.ParentLink)) + Separator + boldStart + link + boldEnd;
                }
                else
                {
                    if (page.PageLink.ID != CurrentPage.PageLink.ID)
                    {
                        return GetParentLink(GetPage(page.ParentLink));
                    }
                    else
                    {
                        return GetParentLink(GetPage(page.ParentLink)) + Separator + boldStart + link + boldEnd;
                    }
                }
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// A shorter alternative to the breadcrumb string
        /// </summary>
        /// <returns>A breadcrumb string with the start page, the parent page and the current page</returns>
        private string GetShortCrumbs(PageData page)
        {
            string startpage = string.Format(_link, startPageURL, Translate("/navigation/startpagelinktitle"),
                                             Translate("/navigation/startpage"));
            string parent = GetLink(GetPage(page.ParentLink));
            string current = GetLink(page);
            return startpage + Separator + "....." + Separator + parent + Separator + current;
        }

        /// <summary>
        /// Initializes a link string based on the Page Type
        /// </summary>
        private string GetLink(PageData page)
        {
            int countryPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CountryPageTypeGUID"])).ID;
            int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
            int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
            string linkToRedirect = string.Empty;
            string pageName = page.PageName;
            if (page != null)
            {
                if (page.LinkURL.Contains("CityLandingPage.aspx") || page.LinkURL.Contains("CountryLandingPage.aspx"))
                {
                    linkToRedirect = string.Format(_link, Server.HtmlEncode(page.LinkURL), pageName, pageName);
                }
                else if (page.PageTypeID.Equals(cityPageTypeID) || page.PageTypeID.Equals(countryPageTypeID))
                {
                    string pageToRedirect = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SEARCH_USING_MAP_PAGE);
                    string linkUrl = pageToRedirect + "?MapID=" + pageName;
                    linkToRedirect = string.Format(_link, Server.HtmlEncode(linkUrl), pageName, pageName);
                }
                else
                {
                    if (page.PageTypeID.Equals(hotelPageTypeID))
                    {
                        if (page.PendingPublish)
                        {
                            PageData pageData = PageValue.GetPageData(page.PageLink, page.LanguageBranch);
                            if (null != pageData)
                                linkToRedirect = string.Format(_link, Server.HtmlEncode(pageData.LinkURL),
                                                               pageData["Heading"].ToString(),
                                                               pageData["Heading"].ToString());
                        }
                        else
                        {
                            linkToRedirect = string.Format(_link, Server.HtmlEncode(page.LinkURL),
                                                           page["Heading"].ToString(), page["Heading"].ToString());
                        }
                    }
                    else
                    {
                        linkToRedirect = string.Format(_link, Server.HtmlEncode(page.LinkURL), pageName, pageName);
                    }
                }
            }
            return linkToRedirect;
        }


        /// <summary>
        /// A string used to separate the page links (default = " / ")
        /// </summary>
        /// <returns>The breadcrumbs separator string</returns>
        public static string Separator
        {
            get { return _separator; }
            set { _separator = value; }
        }

        /// <summary>
        /// Setts the max length on the breadcrumb string (default = 500)
        /// </summary>
        public static int MaxLength
        {
            get { return _maxLength; }
            set { _maxLength = value; }
        }
    }
}