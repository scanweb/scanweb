﻿//  Description					:   HotelOverviewPageGoogleMap                            //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


#undef FINDAHOTEL_PERFORMANCE
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.Web;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.Map;
using Scandic.Scanweb.CMS.code.Util.Map.GoogleMap;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Core.Core;
using System.Web.SessionState;
using Scandic.Scanweb.ExceptionManager;
using Scandic.Scanweb.BookingEngine.Web;


namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// This control shows the google map on HotelOverView Page
    /// This page is also called as FindAHotel page.
    /// </summary>
    public partial class DestinationLandingPageGoogleMap : ScandicUserControlBase
    {

        #region Private Members
        private const string GoogleMapAllHotelsCacheKey = "GoogleMapAllHotelsCacheKey";
        private const string GoogleMapHotelsJSONs = "GoogleMapHotelsJSONsCacheKey";
        #endregion

        private static string currentLanguageString
        {
            get { return EPiServer.Globalization.ContentLanguage.PreferredCulture.Name; }
        }

        #region Protected Events
        string mapSearch = string.Empty;
        /// <summary>
        /// Page_PreRender Event of the Page.
        /// Calls a function RenderGoogleMap to render the google map
        /// on the page with selected country/city/hotel location
        /// after the tree view loads and fills up the session with
        /// selected node.
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event params</param>
        /// <remarks>Find a hotel release</remarks>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Page.Request.Url.ToString().StartsWith("https"))
            {
                SelectedNodeForMapSession();
                RenderGoogleMap(false);
            }
        }

        private void SelectedNodeForMapSession()
        {
            string operaId = string.Empty;
            try
            {
                Node resultNode = null;
                List<Node> mainNodeList = ContentDataAccess.GetPageDataNodeTree(false).GetChildren();
                string pageLinkId = null;

                mapSearch = Request.QueryString["MapSearch"];

                string[] pagelinksplit = null;
                if (mapSearch != null)
                    pagelinksplit = mapSearch.Split(':');

                PageData pageData = null;
                if (mapSearch != string.Empty && mapSearch != null && mapSearch != "undefined" && pagelinksplit != null)
                {

                    if (pagelinksplit[0] == "CITY")
                    {
                        operaId = Convert.ToString(pagelinksplit[1]);
                        pageData = ContentDataAccess.GetCityPageDataByOperaID(Convert.ToString(pagelinksplit[1]));
                    }
                    else if (pagelinksplit[0] == "HOTEL")
                    {
                        operaId = Convert.ToString(pagelinksplit[1]);
                        pageData = ContentDataAccess.GetHotelPageDataByOperaID(Convert.ToString(pagelinksplit[1]), string.Empty);
                    }


                    pageLinkId = Convert.ToString(pageData.PageLink.ID);
                }
                else if (FindAHotelSessionVariablesSessionWrapper.SelectedMapId != null)
                    pageLinkId = FindAHotelSessionVariablesSessionWrapper.SelectedMapId;

                if (pageLinkId != null)
                    FindPageNode(mainNodeList, ref resultNode, ref pageLinkId);

                if (resultNode != null)
                {
                    FindAHotelSessionVariablesSessionWrapper.SelectedNodeForMap = resultNode;
                }
            }
            catch (ContentDataAccessException excpn)
            {
                string customMsg = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/businesserror/operaIdnotfound");
                errorMsg.InnerText = string.Format("{0} - {1}", customMsg, operaId);
                errorAlertDiv.Visible = true;
            }
        }

        private void FindPageNode(List<Node> mainNodeList, ref Node resultNode, ref string pageLinkId)
        {
            if (mainNodeList != null)
            {
                foreach (Node node in mainNodeList)
                {
                    if (null == resultNode)
                    {
                        if (pageLinkId.Equals(node.GetPageData().PageLink.ID.ToString()))
                        {
                            resultNode = node;
                        }
                        else if (node.GetChildren().Count > 0)
                        {
                            FindPageNode(node.GetChildren(), ref resultNode, ref pageLinkId);
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// This method does the following things
        /// 1. Creates two list of selected and other hotel 'Node's
        /// 2. Create hotel units with out numbering(OTHER than selected)
        /// 3. Sort and create hotel units with numbering(selected)
        /// 4. Set map options
        /// 5. Set map data
        /// </summary>
        /// <param name="enableDirections">Determines if directions should be enabled</param>
        private void RenderGoogleMap(bool enableDirections)
        {
            try
            {
                Node selectedNode = null;
                IList<MapUnit> hotels = 
                    GetHotelsJSONs(FindAHotelSessionVariablesSessionWrapper.SelectedNodeForMap, mapSearch, FindAHotelSessionVariablesSessionWrapper.SelectedMapId, false);
                SetMapOptions();
                SetMapData((Node)FindAHotelSessionVariablesSessionWrapper.SelectedNodeForMap, hotels);
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
            }
        }

        /// <summary>
        /// Get all the hotel nodes under the supplied rootPage recursively
        /// </summary>
        /// <param name="rootPage">Node underwhich search has to be taken place</param>
        /// <param name="hotelNodes">list to be filled up with results</param>
        private void GetHotelNodesRecursive(Node rootPage, List<Node> hotelNodes)
        {
            if (rootPage != null)
            {
                IList<Node> subNodes = rootPage.GetChildren();
                if (subNodes != null && subNodes.Count > 0)
                {
                    foreach (Node node in subNodes)
                    {
                        GetHotelNodesRecursive(node, hotelNodes);
                    }
                }
                else
                {
                    PageData pagaData = rootPage.GetPageData();
                    if (pagaData != null)
                    {
                        if ((PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID) ==
                            pagaData.PageTypeID)
                        {
                            hotelNodes.Add(rootPage);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds a new map unit to the list of hotels
        /// </summary>
        /// <param name="hotelNodes">Node list carrying hotel data</param>
        /// <param name="hotels">list on which new units has to be added</param>
        /// <param name="infoBoxType">type of infobox(basic/advanced)</param>
        /// <param name="enableUnitNumbering">true if unit has to be numbered false otherwise</param>
        private void AddHotelNodes(List<Node> hotelNodes, IList<MapUnit> hotels, InfoBoxType infoBoxType,
                                   bool enableUnitNumbering)
        {
            try
            {
                if (hotelNodes != null && hotelNodes.Count > 0)
                {
                    for (int i = 0; i < hotelNodes.Count; i++)
                    {
                        Node hotelNode = hotelNodes[i];
                        if (hotelNode != null)
                        {
                            PageData hotelPageData = hotelNode.GetPageData();
                            if ((PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID) ==
                                hotelPageData.PageTypeID)
                            {
                                string iconUrl = string.Empty;
                                string shadowUrl = string.Empty;
                                string hotelCategory = string.Empty;
                                string promotionalPageLinkText = string.Empty;
                                string promotionalPageLinkUrl = string.Empty;

                                if (hotelPageData["HotelCategory"] != null &&
                                    !string.IsNullOrEmpty(hotelPageData["HotelCategory"].ToString()))
                                {
                                    hotelCategory = hotelPageData["HotelCategory"].ToString();
                                }

                                GetMarkerUrls(hotelCategory, enableUnitNumbering, out iconUrl, out shadowUrl);
                                MapUnit hotelDatasource = new MapHotelUnit(
                                    (double)hotelPageData["GeoX"],
                                    (double)hotelPageData["GeoY"],
                                    -1,
                                    iconUrl,
                                    shadowUrl,
                                    hotelPageData["Heading"] as string,
                                    hotelPageData["HotelBookingImage"] as string,
                                    hotelPageData["HotelBookingDescription"] as string,
                                    hotelPageData["StreetAddress"] as string,
                                    hotelPageData["PostCode"] as string,
                                    hotelPageData["PostalCity"] as string,
                                    hotelPageData["City"] as string,
                                    hotelPageData["Country"] as string,
                                    GetFriendlyURLToPage(hotelPageData.PageLink, hotelPageData.LinkURL) as string,
                                    GetDeepLinkingURL(hotelPageData["OperaID"] as string ?? string.Empty),
                                    hotelPageData["CityCenterDistance"] != null
                                        ? (double)hotelPageData["CityCenterDistance"]
                                        : 0,
                                    infoBoxType,
                                    enableUnitNumbering,
                                    enableUnitNumbering == true ? i + 1 : 0,
                                    hotelPageData["OperaID"] as string ?? string.Empty,
                                    hotelCategory,
                                    string.Empty,
                                    string.Empty,
                                    false
                                    );

                                hotels.Add(hotelDatasource);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
            }
        }

        /// <summary>
        /// Gets the marker urls.
        /// </summary>
        /// <param name="hotelCategory">The hotel category.</param>
        /// <param name="enableUnitNumbering">if set to <c>true</c> [enable unit numbering].</param>
        /// <param name="iconUrl">The icon URL.</param>
        /// <param name="shadowUrl">The shadow URL.</param>
        private void GetMarkerUrls(string hotelCategory, bool enableUnitNumbering, out string iconUrl,
                                   out string shadowUrl)
        {
            switch (hotelCategory)
            {
                case AppConstants.DEFAULT:
                    {
                        iconUrl = string.Format("/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png");
                        shadowUrl = string.Empty;
                    }
                    break;
                case AppConstants.RECENTLYOPENED_VALUE:
                    {
                        iconUrl = string.Format("/Templates/Scanweb/Styles/Default/Images/Icons/recentlyopened_hotel.png");
                        shadowUrl = string.Empty;
                    }
                    break;
                case AppConstants.COMINGSOON_VALUE:
                    {
                        iconUrl = string.Format("/Templates/Scanweb/Styles/Default/Images/Icons/comingsoon_hotel.png");
                        shadowUrl = string.Empty;
                    }
                    break;
                default:
                    {
                        iconUrl = string.Format("/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png");
                        shadowUrl = string.Empty;
                    }
                    break;
            }
        }

        /// <summary>
        /// Sort all of the hotels with numbering(selected) based on
        /// the criteria which is been updated in the session by the
        /// hotel listing control, based on user selects
        /// 'Alphabetic'/'Distance to city center' in the sorting drop-down)
        /// </summary>
        /// <param name="selectedHotelNodes">The selected hotel nodes.</param>
        private void SortSelectedHotelNodes(List<Node> selectedHotelNodes)
        {
            try
            {
                if (selectedHotelNodes.Count > 0)
                {
                    string hotelSortOrder = FindAHotelSessionVariablesSessionWrapper.HotelSortOrder;
                    if (0 == string.Compare(hotelSortOrder, SortType.ALPHABETIC.ToString(), true))
                    {
                        selectedHotelNodes.Sort(new PageNameComparer());
                    }
                    else if (0 ==
                             string.Compare(hotelSortOrder, SortType.DISTANCETOCITYCENTREOFSEARCHEDHOTEL.ToString(), true))
                    {
                        selectedHotelNodes.Sort(new CityCenterDistanceComparer());
                    }
                    else
                    {
                        selectedHotelNodes.Sort(new PageNameComparer());
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
            }
        }

        /// <summary>
        /// Set different generic parameters to the map
        /// </summary>
        private void SetMapOptions()
        {
            CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            GoogleMapControl.GoogleMapKey = (ConfigurationManager.AppSettings["googlemaps." + Request.Url.Host] as string ?? (ConfigurationManager.AppSettings["DevKey"] as string));
            GoogleMapControl.MarkerLatitudeField = "latitude";
            GoogleMapControl.MarkerLongitudeField = "longitude";
            GoogleMapControl.MapPageType = MapPageType.FINDAHOTEL;
            GoogleMapControl.OtherHotels = true;
            GoogleMapControl.RecentlyOpenedHotels = true;
            GoogleMapControl.ComingSoonHotels = true;
        }

        /// <summary>
        /// Set zoom and center of the map according to the selected node
        /// and sets the hotelunit list data to the map
        /// </summary>
        /// <param name="selectedNode">selected node</param>
        /// <param name="hotels">list of map unit</param>
        private void SetMapData(Node selectedNode, IList<MapUnit> hotels)
        {
            try
            {
                if (selectedNode != null)
                {
                    int countryPageTypeID =
                        PageType.Load(new Guid(ConfigurationManager.AppSettings["CountryPageTypeGUID"])).ID;
                    int cityPageTypeID =
                        PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                    int hotelPageTypeID =
                        PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                    PageData selectedPageData = selectedNode.GetPageData();

                    if (selectedPageData.PageTypeID == countryPageTypeID)
                    {
                        SetCountryMapDataZoomAndCenter(selectedPageData, hotels);
                    }
                    else if (selectedPageData.PageTypeID == cityPageTypeID)
                    {
                        SetCityMapDataZoomAndCenter(selectedPageData, hotels);
                    }
                    else if (selectedPageData.PageTypeID == hotelPageTypeID)
                    {
                        SetHotelMapDataZoomAndCenter(selectedPageData, hotels);
                    }
                }
                else
                {
                    GoogleMapControl.InfoBoxType = InfoBoxType.FIND_YOUR_DESTINATION;
                    GoogleMapControl.DataSource = hotels;
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
                HandleCmsCoOrdinateDataError(hotels);
            }
            finally
            {
                GoogleMapControl.DataBind();
            }
        }

        /// <summary>
        /// 1. Set the country data
        /// 2. Zoom and center to show the country(ZOOM LEVEL - AUTO ZOOM)
        /// </summary>
        /// <param name="country">selected country node</param>
        /// <param name="hotels">map unit list to be set as country data</param>
        private void SetCountryMapDataZoomAndCenter(PageData country, IList<MapUnit> hotels)
        {
            if (country != null && hotels != null && hotels.Count > 0)
            {
                if (country["NothEastGeoY"] != null && country["NothEastGeoX"] != null &&
                    country["SouthWestGeoY"] != null && country["SouthWestGeoX"] != null
                    )
                {
                    try
                    {
                        List<MapUnit> boundaryList = new List<MapUnit>();
                        double countryNorthEastLatitude;
                        double countryNorthEastLongitude;
                        double countrySouthWestLatitude;
                        double countrySouthWestLongitude;

                        double.TryParse(country["NothEastGeoY"].ToString(), out countryNorthEastLatitude);
                        double.TryParse(country["NothEastGeoX"].ToString(), out countryNorthEastLongitude);
                        double.TryParse(country["SouthWestGeoY"].ToString(), out countrySouthWestLatitude);
                        double.TryParse(country["SouthWestGeoX"].ToString(), out countrySouthWestLongitude);

                        MapUnit northEast = new MapUnit(countryNorthEastLongitude, countryNorthEastLatitude, -1,
                                                        string.Empty, string.Empty);
                        MapUnit southWest = new MapUnit(countrySouthWestLongitude, countrySouthWestLatitude, -1,
                                                        string.Empty, string.Empty);
                        boundaryList.Add(northEast);
                        boundaryList.Add(southWest);
                        GoogleMapControl.NELatitude = countryNorthEastLatitude;
                        GoogleMapControl.NELongitude = countryNorthEastLongitude;
                        GoogleMapControl.SWLatitude = countrySouthWestLatitude;
                        GoogleMapControl.SWLongitude = countrySouthWestLongitude;
                        GoogleMapControl.CenterAndZoomLevel = CenterAndZoomLevel.COUNTRY;
                        GoogleMapControl.AutoCenterAndZoom();

                        GoogleMapControl.DataSource = hotels;
                    }
                    catch (System.ArgumentException ex)
                    {
                        AppLogger.LogFatalException(ex, "Country co-ordinates are either wrong or missing");
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    Exception ex = new Exception("Country co-ordinates are either wrong or missing");
                    AppLogger.LogFatalException(ex);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// 1. Set the city data
        /// 2. Zoom and center to show the city(ZOOM LEVEL 9)
        /// </summary>
        /// <param name="city">selected city node</param>
        /// <param name="hotels">map unit list to be set as city data</param>
        private void SetCityMapDataZoomAndCenter(PageData city, IList<MapUnit> hotels)
        {
            if (city != null && hotels != null && hotels.Count > 0)
            {
                double cityCenterLatitude;
                double cityCenterLongitude;

                if (city["CenterGeoY"] != null && city["CenterGeoX"] != null)
                {
                    try
                    {
                        double.TryParse(city["CenterGeoY"].ToString(), out cityCenterLatitude);
                        double.TryParse(city["CenterGeoX"].ToString(), out cityCenterLongitude);
                        GoogleMapControl.CenterAndZoomLevel = CenterAndZoomLevel.CITY;
                        GoogleMapControl.ZoomLevel = 9;
                        GoogleMapControl.Latitude = cityCenterLatitude;
                        GoogleMapControl.Longitude = cityCenterLongitude;

                        GoogleMapControl.DataSource = hotels;
                    }
                    catch (System.ArgumentException ex)
                    {
                        AppLogger.LogFatalException(ex, string.Format(AppConstants.CITY_COORDINATES_MISSING, city["OperaID"]));
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    Exception ex = new Exception(string.Format(AppConstants.CITY_COORDINATES_MISSING, city["OperaID"]));
                    //AppLogger.LogFatalException(ex);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// 1. Set the hotel data
        /// 2. Zoom and center to show the hotel(ZOOM LEVEL 16)
        /// </summary>
        /// <param name="hotel">selected hotel node</param>
        /// <param name="hotels">map unit list to be set as hotel data</param>
        private void SetHotelMapDataZoomAndCenter(PageData hotel, IList<MapUnit> hotels)
        {
            if (hotel != null && hotels != null && hotels.Count > 0)
            {
                double hotelLatitude;
                double hotelLongitude;

                if (hotel["GeoY"] != null && hotel["GeoX"] != null)
                {
                    try
                    {
                        double.TryParse(hotel["GeoY"].ToString(), out hotelLatitude);
                        double.TryParse(hotel["GeoX"].ToString(), out hotelLongitude);
                        GoogleMapControl.CenterAndZoomLevel = CenterAndZoomLevel.HOTEL;
                        GoogleMapControl.Latitude = hotelLatitude;
                        GoogleMapControl.Longitude = hotelLongitude;
                        GoogleMapControl.ZoomLevel = 16;


                        GoogleMapControl.DataSource = hotels;
                    }
                    catch (System.ArgumentException ex)
                    {
                        AppLogger.LogFatalException(ex, "Hotel co-ordinates are either wrong or missing");
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    Exception ex = new Exception("Hotel co-ordinates are either wrong or missing");
                    AppLogger.LogFatalException(ex);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Gracefully handles all exception cases and CMS data issues
        /// Simply shows all the hotels in the map and zooms and center(AUTO)
        /// </summary>
        /// <param name="hotels">all hotels</param>
        private void HandleCmsCoOrdinateDataError(IList<MapUnit> hotels)
        {
        }

        /// <summary>
        /// Gets all hotels from Cache, if it's availble in Cache; if not it fetches from CMS and adds to Cache.
        /// </summary>
        /// <param name="rootNode"></param>
        /// <param name="isCacheFillerRequest"></param>
        /// <returns></returns>
        private List<Node> GetAllHotels(Node rootNode, bool isCacheFillerRequest)
        {
            string cacheKey = string.Format("{0}{1}", GoogleMapAllHotelsCacheKey, currentLanguageString);
            List<Node> allHotels = new List<Node>();
            if (ScanwebCacheManager.Instance.LookInCache<List<Node>>(cacheKey) == null || isCacheFillerRequest)
            {
                GetHotelNodesRecursive(rootNode, allHotels);
                ScanwebCacheManager.Instance.AddToCache(cacheKey, allHotels, new Collection<object> { rootNode, true },
                    (Func<Node, bool, List<Node>>)GetAllHotels);
            }
            else
            {
                allHotels = ScanwebCacheManager.Instance.LookInCache<List<Node>>(cacheKey);
            }
            return allHotels;
        }

        /// <summary>
        /// Gets hotels' JSONs from Cache, if it's available in Cache. If not it creates the JSONs and add it to cache.
        /// </summary>
        /// <param name="selectedMapId"> </param>
        /// <param name="isCacheFillerRequest"> </param>
        /// <param name="selectedNode"> </param>
        /// <param name="mapSearchParameter"> </param>
        /// <returns></returns>
        private IList<MapUnit> GetHotelsJSONs(Node selectedNode, string mapSearchParameter, string selectedMapId,  bool isCacheFillerRequest)
        {
            IList<MapUnit> hotels = new List<MapUnit>();
            try
            {
                string cacheKey = string.Format("{0}{1}{2}", GoogleMapHotelsJSONs, mapSearch, currentLanguageString);
                if ((selectedNode != null) &&
                    (selectedNode.GetPageData() != null))
                {
                    cacheKey = string.Format("{0}{1}", cacheKey, selectedNode.GetPageData().PageGuid);
                }
                if (ScanwebCacheManager.Instance.LookInCache<IList<MapUnit>>(cacheKey) == null || isCacheFillerRequest)
                {
                    List<Node> allHotelNodes = new List<Node>();
                    List<Node> unSelectedHotelNodes = new List<Node>();
                    List<Node> selectedHotelNodes = new List<Node>();
                    Node rootNode = ContentDataAccess.GetPageDataNodeTree(false);
                    if (rootNode != null)
                    {
                        allHotelNodes = GetAllHotels(rootNode, false);
                    }
                    if (selectedNode != null)
                    {
                        GetHotelNodesRecursive(selectedNode, selectedHotelNodes);
                    }

                    unSelectedHotelNodes.AddRange(allHotelNodes);
                    for (int i = 0; i < selectedHotelNodes.Count; i++)
                    {
                        unSelectedHotelNodes.Remove(selectedHotelNodes[i]);
                    }
                    if (mapSearchParameter == string.Empty && selectedMapId == null)
                        AddHotelNodes(unSelectedHotelNodes, hotels, InfoBoxType.FIND_YOUR_DESTINATION, false);

                    //SortSelectedHotelNodes(selectedHotelNodes);
                    AddHotelNodes(selectedHotelNodes, hotels, InfoBoxType.FIND_YOUR_DESTINATION, false);
                    ScanwebCacheManager.Instance.AddToCache(cacheKey, hotels, new Collection<object>{selectedNode,mapSearchParameter,selectedMapId,true},
                        (Func<Node, string, string, bool, IList<MapUnit>>)GetHotelsJSONs);
                }
                else
                {
                    hotels = ScanwebCacheManager.Instance.LookInCache<IList<MapUnit>>(cacheKey);
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Exception occured in DestinationLandingPageGoogleMap control - GetHotelsJSONs method.");
            }
            return hotels;
        }
        #endregion
    }
}