<%@ Control Language="C#" AutoEventWireup="true" Codebehind="DestinationSearch.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.DestinationSearch" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<h3 class="darkHeading"><episerver:translate id="Translate1" text="/Templates/Scanweb/Units/Static/FindHotelSearch/WhereDoYouWantToStay"
                runat="server" /></h3>
<div>

    <input type="hidden" id="selectedFAHDestId" runat="server" />
    <input type="hidden" id="destinationError" name="destinationError" value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name") %>' />
    <input type="hidden" id="duplicateDestinationError" name="duplicateDestinationError"
        value='<%=
                WebUtil.GetTranslatedText(
                    "/scanweb/bookingengine/errormessages/requiredfielderror/duplicate_destination") %>' />
    <div id="destinationErrorDiv" runat="server">
    </div>
    <div id="destContWrapper">
        <div id="destinationDiv">
        <label for="Find a Hotel"><strong><episerver:translate id="Translate2" text="/Templates/Scanweb/Units/Static/FindHotelSearch/HotelNameAndDestination"
                runat="server" /></strong>
            </label>
        <input id="txtDestinationName" tabindex="1" type="text" class="inputTextBig" onfocus="javascript:initAutosuggest();" autocomplete="off"
            runat="server" /></div>
           
        <!-- CSS hack for Mac Safari and Safari version 3 browser -->
        <style type="text/css">
						#autosuggest ul{ 
						margin-top: 35px;
						overflow: scroll;
						}
						/* Mac Safari hack - the below css will be ignored by Mac safari less than version3*/
						#autosuggest ul{ 
						overflow-x: hidden;
						overflow-y: auto;
						margin-top: 0px;# 
						}
						/* Safari 3 hack -the below css  works only on safari 3 browser */
						@media screen and (-webkit-min-device-pixel-ratio:0) {
						#autosuggest { margin-top: 35px;}
						}
				</style>
        <!-- END - CSS hack for Mac Safari and Safari version 3 browser -->
        <div id="autosuggestFAH" class="autosuggest"><ul></ul><iframe src="javascript:false;" frameborder="0" scrolling="no"></iframe>
        </div>
        <script type="text/javascript" language="JavaScript">
         //Added for Find a hotel release
        function initAutosuggest()
        {
            var Destination1 = _endsWith("txtDestinationName");
            new AutoSuggest($fn(Destination1),'autosuggestFAH','selectedFAHDestId',true);
        }
        </script>
    </div>
</div>
