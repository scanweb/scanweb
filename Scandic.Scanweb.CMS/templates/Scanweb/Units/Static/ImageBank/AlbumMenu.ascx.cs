//  Description					: AlbumMenu                                               //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Web;
using ImageStoreNET.Developer.Core;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.ImageBank
{
    /// <summary>
    /// Code behind of AlbumMenu page.
    /// </summary>
    public partial class AlbumMenu : ScandicUserControlBase
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int albumID = 0;
            string albumIDString = Request.QueryString["IVAlbumID"] as string;
            if (albumIDString != null && Int32.TryParse(albumIDString, out albumID))
            {
                AlbumTreeControl.OpenAlbum = albumID;
            }
            AlbumTreeControl.DataBind();
        }

        /// <summary>
        /// Checks whether there are any child albums.
        /// </summary>
        /// <param name="album"></param>
        /// <returns>True/False</returns>
        protected bool HasChildAlbums(IVAlbumData album)
        {
            return album.GetDirectories().Length > 0;
        }

        /// <summary>
        /// Gets Indent.
        /// </summary>
        /// <param name="indentLevel"></param>
        /// <returns>Indent.</returns>
        protected string GetIndent(int indentLevel)
        {
            string indentString = string.Empty;
            for (int i = 0; i < indentLevel; i++)
            {
                indentString = indentString + "&nbsp;";
            }
            return indentString;
        }

        /// <summary>
        /// Gets link url
        /// </summary>
        /// <param name="albumID"></param>
        /// <returns>link url</returns>
        protected string GetLinkURL(int albumID)
        {
            string openAlbum = "IVAlbumID";

            StringBuilder builder = new StringBuilder();
            bool flag = true;
            string[] allKeys = HttpContext.Current.Request.QueryString.AllKeys;
            if (allKeys != null)
            {
                for (int i = 0; i < allKeys.Length; i++)
                {
                    if ((allKeys[i] != null) && !allKeys[i].ToLower().Equals(openAlbum.ToLower()))
                    {
                        string[] values = HttpContext.Current.Request.QueryString.GetValues(allKeys[i]);
                        if (values != null)
                        {
                            for (int j = 0; j < values.Length; j++)
                            {
                                string str = values[j];
                                if (str.IndexOf("\"") > -1)
                                {
                                    str = HttpUtility.UrlEncode(str);
                                }
                                if (flag)
                                {
                                    builder.Append("?" + allKeys[i] + "=" + str);
                                    flag = false;
                                }
                                else
                                {
                                    builder.Append("&" + allKeys[i] + "=" + str);
                                }
                            }
                        }
                    }
                }
            }
            builder.Append(string.Concat(new object[] {"&", openAlbum, "=", albumID}));
            return builder.ToString();
        }
    }
}