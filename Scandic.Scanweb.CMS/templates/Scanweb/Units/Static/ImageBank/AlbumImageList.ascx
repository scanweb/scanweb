<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AlbumImageList.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.ImageBank.AlbumImageList" %>
<%@ Register TagPrefix="ImageVault" Namespace="ImageStoreNET.Developer.WebControls" Assembly="ImageVault.Episerver6" %>

<script type="text/javascript" language="JavaScript">

    var imageSearchCurrentPage = 1;
    
    function ImageSearchSelectPage(pageID, numberOfPages, numberOfHits){
    
        imageSearchCurrentPage = pageID;
        
        // Make current selected page bold
        for(var x=1; x<(numberOfPages+1); x++){
            var currentPageItem = document.getElementById('ImageSearchSelectPage'+x);
            currentPageItem.style.fontWeight = 'normal';
        }
        var currentPageItem = document.getElementById('ImageSearchSelectPage'+pageID);
        currentPageItem.style.fontWeight = 'bold';
        
        // Hide or show previous/next page
        var previousArrow = document.getElementById('ImageSearchPreviousArrow');
        var nextArrow = document.getElementById('ImageSearchNextArrow');
               
        if (pageID==1)
            previousArrow.style.display = 'none';
        else
            previousArrow.style.display = 'inline';
             
        if (pageID==numberOfPages)
            nextArrow.style.display = 'none';
        else
            nextArrow.style.display = 'inline';
 
        // Display the hits that belongs to the current page   
        for(var x=1; (x<numberOfHits+1); x++){
             var currentPageItem = document.getElementById('ImageSearchResult'+x);
             if ((x <= pageID*10) && (x > (pageID -1)*10)){
                currentPageItem.style.display = 'block';
             }
             else{
                currentPageItem.style.display = 'none';
             }
        } 
    }   
    
     function ImageSearchPreviousPage(numberOfPages, numberOfHits){
        imageSearchCurrentPage--;
        ImageSearchSelectPage(imageSearchCurrentPage, numberOfPages, numberOfHits);
    }
    
    function ImageSearchNextPage(numberOfPages, numberOfHits){
        imageSearchCurrentPage++;
        ImageSearchSelectPage(imageSearchCurrentPage, numberOfPages, numberOfHits);
    }
    
</script>

<asp:Literal runat="server" ID="SearchResultPagingLiteral" /><br /><br />

<ImageVault:IVFileListData runat="server" ID="FileListDataControl" />
<ImageVault:IVFileList runat="server" id="FileListControl">
    <HeaderTemplate>
        <div id="AlbumImageListContainer">
       <table class="ScandicTableTop" cellpadding="0" width="100%" border="0">
<tbody>
<tr valign="top">
<td></td></tr></tbody></table>
<table class="ScandicTable" cellspacing="0" cellpadding="10" width="100%" align="left" border="0">
<tbody>
    </HeaderTemplate>
    <ItemTemplate>
     <% CurrentCount++; %>
    <tr valign="top" id="ImageSearchResult<%= CurrentCount.ToString() %>" style="display:<%= ((CurrentCount > 10) ? "none" : "block") %>">
        <td width="120"><ImageVault:IVImage runat="server" ImageWidth="100" ImageFormat="jpg"/></td>
        <td width="400"><strong><%# Container.CurrentFile.Name %></strong><br />  
           <div class="AlbumImageListMetaDataContainer">
           <div class="AlbumImageListMetaData">
               <div class="AlbumImageListMetaDataLeft"><EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Pages/ImageBank/ImageID" runat="server" /></div>
               <div class="AlbumImageListMetaDataRight"><%# Container.CurrentFile.Id %></div>
           </div>
           <div class="AlbumImageListMetaData">
                <div class="AlbumImageListMetaDataLeft"><EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Pages/ImageBank/FileSize" runat="server" /></div>
                <div class="AlbumImageListMetaDataRight"><%# ((int) (Container.CurrentFile.FileSize/1024)) %> kb</div>
           </div>
           <div class="AlbumImageListMetaData">
               <div class="AlbumImageListMetaDataLeft"><EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Pages/ImageBank/FileExtension" runat="server" /></div>
               <div class="AlbumImageListMetaDataRight"><%# Container.CurrentFile.Extension %></div>
           </div>
           <div class="AlbumImageListMetaData">
                <div class="AlbumImageListMetaDataLeft"><EPiServer:Translate ID="Translate4" Text="/Templates/Scanweb/Pages/ImageBank/Height" runat="server" /></div>
                <div class="AlbumImageListMetaDataRight"><%# Container.CurrentFile.Height %> px</div>
           </div>
           <div class="AlbumImageListMetaData">
               <div class="AlbumImageListMetaDataLeft"><EPiServer:Translate ID="Translate7" Text="/Templates/Scanweb/Pages/ImageBank/Width" runat="server" /></div>
               <div class="AlbumImageListMetaDataRight"><%# Container.CurrentFile.Width %> px</div>
           </div>
           <div class="AlbumImageListMetaData">
               <div class="AlbumImageListMetaDataLeft"><EPiServer:Translate ID="Translate8" Text="/Templates/Scanweb/Pages/ImageBank/Copyright" runat="server" /></div>
               <div class="AlbumImageListMetaDataRight"><%# GetMetaData(Container.CurrentFile, "Copyright") %></div>
           </div>
           <div class="AlbumImageListMetaData">
               <div class="AlbumImageListMetaDataLeft"><EPiServer:Translate ID="Translate9" Text="/Templates/Scanweb/Pages/ImageBank/LegalIssues" runat="server" /></div>
               <div class="AlbumImageListMetaDataRight"><%# GetMetaData(Container.CurrentFile, "Legal Issues") %></div>
           </div>
           <div class="AlbumImageListMetaData">
               <div class="AlbumImageListMetaDataLeft"><EPiServer:Translate ID="Translate10" Text="/Templates/Scanweb/Pages/ImageBank/Source" runat="server" /></div>
               <div class="AlbumImageListMetaDataRight"><%# GetMetaData(Container.CurrentFile, "Source") %></div>
          </div>
          <div class="AlbumImageListMetaData">
              <div class="AlbumImageListMetaDataLeft"><EPiServer:Translate ID="Translate11" Text="/Templates/Scanweb/Pages/ImageBank/DateAdded" runat="server" /></div>
               <div class="AlbumImageListMetaDataRight"><%# Container.CurrentFile.Created.ToString("yyyy-MM-dd") %> </div>
           </div>
         </div>
        </td>
        <td>
            <strong><EPiServer:Translate ID="Translate5" Text="/Templates/Scanweb/Pages/ImageBank/Download" runat="server" /></strong><br />
            <a class="IconLink" href="<%# Container.CurrentFile.LinkURL %>" onclick="window.open('<%# Container.CurrentFile.LinkURL %>');return false"><EPiServer:Translate ID="Translate6" Text="/Templates/Scanweb/Pages/ImageBank/Original" runat="server" /></a><br />
             <ImageVault:IVConversionFormatList runat="server">
				                <ItemTemplate>
					                <a class="IconLink" href="<%# Container.CurrentFormat.LinkURL %>" onclick="window.open('<%# Container.CurrentFormat.LinkURL %>');return false"><%# Container.CurrentFormat.Name %></a><br/>
				                </ItemTemplate>
			</ImageVault:IVConversionFormatList>		
        </td>
       
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </tbody>
       <table class="ScandicTableBottom" style="CLEAR: both" width="100%" border="0">
        <tbody>
        <tr valign="top">
        <td></td></tr></tbody></table>
        </div>
    </FooterTemplate>
</ImageVault:IVFileList>