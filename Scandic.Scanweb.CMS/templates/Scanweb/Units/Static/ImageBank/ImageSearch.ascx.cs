using System;
using System.Text;
using EPiServer.Core;
using ImageStoreNET.Developer.Core;
using ImageStoreNET.Developer.Filters;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.ImageBank
{
    /// <summary>
    /// ImageSearch
    /// </summary>
    public partial class ImageSearch : System.Web.UI.UserControl
    {
        public int CurrentCount = 0;

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SearchText.Text.Length > 0)
            {
                FileSearch.SortBy = IVFileSortBy.AlbumName;
                FileSearch.SearchQuery = SearchText.Text;
                FileSearch.DataBind();

                SearchResultPagingLiteral.Text = string.Empty;
                NumberOfHits.Text = string.Empty;

                if (FileSearch.DataCount > 0)
                {
                    NumberOfHits.Text =
                        string.Format
                            (LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/ImageBank/FoundImages"),
                             FileSearch.DataCount.ToString());
                    SearchResultPagingLiteral.Text = CreatePagingString(FileSearch.DataCount);
                }
            }
        }

        /// <summary>
        /// CreatePagingString
        /// </summary>
        /// <param name="noOfHits"></param>
        /// <returns>Paging String</returns>
        protected string CreatePagingString(int noOfHits)
        {
            int noOfPages = ((int) ((noOfHits - 1)/10) + 1);

            StringBuilder sb = new StringBuilder();
            sb.Append("<a href='#' style='display:none;' onClick='ImageSearchPreviousPage(" + noOfPages + "," +
                      noOfHits + ")' ID='ImageSearchPreviousArrow'>&lt;</a>&nbsp;&nbsp;");
            for (int i = 1; i < noOfPages + 1; i++)
            {
                sb.Append("<a href='#' style='font-weight:" + ((i == 1) ? "bold" : "normal") +
                          "' onClick='ImageSearchSelectPage(" + i + "," + noOfPages + "," +
                          noOfHits + ")' ID='ImageSearchSelectPage" + i + "'>" + i + "</a>&nbsp;&nbsp;");
            }
            sb.Append("<a href='#' style='display:" + (noOfHits > 10 ? "inline" : "none") +
                      ";' onClick='ImageSearchNextPage(" + noOfPages + "," +
                      noOfHits + ")' ID='ImageSearchNextArrow'>&gt;</a>&nbsp;&nbsp;");
            return sb.ToString();
        }

        /// <summary>
        /// GetMetaData
        /// </summary>
        /// <param name="file"></param>
        /// <param name="metaDataName"></param>
        /// <returns>Meta Data</returns>
        protected string GetMetaData(IVFileData file, string metaDataName)
        {
            if (file != null && file.MetaData[metaDataName] != null)
            {
                return file.MetaData[metaDataName].Value;
            }
            return string.Empty;
        }
    }
}