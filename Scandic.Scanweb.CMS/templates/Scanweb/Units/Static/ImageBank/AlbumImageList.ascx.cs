using System;
using System.Text;
using ImageStoreNET.Developer.Core;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.ImageBank
{
    /// <summary>
    /// AlbumImageList
    /// </summary>
    public partial class AlbumImageList : ScandicUserControlBase
    {
        public int CurrentCount = 0;

        /// <summary>
        /// Page_PreRender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            int albumID = 0;
            string albumIDString = Request.QueryString["IVAlbumID"] as string;
            if (albumIDString != null && Int32.TryParse(albumIDString, out albumID))
            {
                FileListControl.AlbumId = albumID;
            }
            FileListControl.DataBind();

            if (FileListControl.DataCount > 0)
                SearchResultPagingLiteral.Text = CreatePagingString(FileListControl.DataCount);
        }

        /// <summary>
        /// CreatePagingString
        /// </summary>
        /// <param name="noOfHits"></param>
        /// <returns>PagingString</returns>
        protected string CreatePagingString(int noOfHits)
        {
            int noOfPages = ((int) ((noOfHits - 1)/10) + 1);

            StringBuilder sb = new StringBuilder();
            sb.Append("<a href='#' style='display:none;' onClick='ImageSearchPreviousPage(" + noOfPages + "," + noOfHits +
                      ")' ID='ImageSearchPreviousArrow'>&lt;</a>&nbsp;&nbsp;");
            for (int i = 1; i < noOfPages + 1; i++)
            {
                sb.Append("<a href='#' style='font-weight:" + ((i == 1) ? "bold" : "normal") +
                          "' onClick='ImageSearchSelectPage(" + i + "," + noOfPages + "," + noOfHits +
                          ")' ID='ImageSearchSelectPage" + i + "'>" + i + "</a>&nbsp;&nbsp;");
            }
            sb.Append("<a href='#' style='display:" + (noOfHits > 10 ? "inline" : "none") +
                      ";' onClick='ImageSearchNextPage(" + noOfPages + "," + noOfHits +
                      ")' ID='ImageSearchNextArrow'>&gt;</a>&nbsp;&nbsp;");
            return sb.ToString();
        }

        /// <summary>
        /// GetMetaData
        /// </summary>
        /// <param name="file"></param>
        /// <param name="metaDataName"></param>
        /// <returns>MetaData</returns>
        protected string GetMetaData(IVFileData file, string metaDataName)
        {
            if (file != null && file.MetaData[metaDataName] != null)
            {
                return file.MetaData[metaDataName].Value;
            }
            return string.Empty;
        }
    }
}