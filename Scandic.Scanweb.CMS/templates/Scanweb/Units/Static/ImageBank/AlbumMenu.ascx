<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AlbumMenu.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.ImageBank.AlbumMenu" %>
<%@ Register TagPrefix="ImageVault" Namespace="ImageStoreNET.Developer.WebControls" Assembly="ImageVault.Episerver6" %>

<ImageVault:IVAlbumTree runat="server" id="AlbumTreeControl" AlbumProperty="ImageAlbum" ExpandAll="false">
    <HeaderTemplate>
             <%# Container.CurrentAlbum.Name %><br />
    </HeaderTemplate>
	
	<ItemTemplate>    
	    <%# GetIndent(Container.CurrentAlbum.Indent) %><a href="<%# GetLinkURL(Container.CurrentAlbum.Id) %>" class="<%# (HasChildAlbums(Container.CurrentAlbum) ? "AlbumMenuLinkHasChildren" : "AlbumMenuLink") %>"><%# Container.CurrentAlbum.Name %></a><br />
	</ItemTemplate>
	
	<ExpandedItemTemplate>
	    <%# GetIndent(Container.CurrentAlbum.Indent) %><a href="<%# GetLinkURL(Container.CurrentAlbum.Id) %>" class="AlbumMenuLinkExpanded"><%# Container.CurrentAlbum.Name %></a><br />
	</ExpandedItemTemplate>
	
	<SelectedItemTemplate>
				 <%# GetIndent(Container.CurrentAlbum.Indent) %><span class="AlbumMenuLink"><strong><%# Container.CurrentAlbum.Name %></strong></span><br />
	</SelectedItemTemplate>
	
	<SelectedExpandedItemTemplate>
				<%# GetIndent(Container.CurrentAlbum.Indent) %><span class="AlbumMenuLinkExpanded"><strong><%# Container.CurrentAlbum.Name %></strong></span><br />
	</SelectedExpandedItemTemplate>

	<FooterTemplate>
	        
	</FooterTemplate>
</ImageVault:IVAlbumTree>

