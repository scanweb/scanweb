<%@ Control Language="C#" AutoEventWireup="true" Codebehind="FindHotelSearch.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.FindHotelSearch" %>
<div class="FindHotel DottedUnderLineBlocknew">
    <div class="FindHotelSearchText">
        <episerver:translate text="/Templates/Scanweb/Units/Static/FindHotelSearch/SearchText"
            runat="server" />
    </div>
    <asp:TextBox ID="FindHotelTextBox" CssClass="FindHotelTextBox" runat="server" onfocus="this.value='';"></asp:TextBox>
    <div class="FindHotelSearchButton">
        <div class="actionBtn">
            <asp:Button ID="FindHotelButton" CssClass="submit buttonInner sprite" runat="server"
                OnClick="FindHotelButton_Click" />                
                <asp:Button ID="spnFindHotelButton" CssClass="buttonRt sprite" runat="server"
                OnClick="FindHotelButton_Click" />            
        </div>
    </div>
</div>
<asp:PlaceHolder ID="SearchResultsPlaceHolder" runat="server" Visible="false">
    <h2>
        <episerver:translate text="/Templates/Scanweb/Units/Static/FindHotelSearch/SearchResults"
            runat="server" />
    </h2>
    <div id="HotelOverviewSearchResults">
        <div id="HotelOverviewSearchResultsInner">
            <asp:Literal ID="SearchResults" runat="server"></asp:Literal>
        </div>
    </div>
</asp:PlaceHolder>
