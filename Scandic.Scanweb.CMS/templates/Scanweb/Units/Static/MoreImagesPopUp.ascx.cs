//  Description					: MoreImagesPopUp                                         //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.Security;
using Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.Util.ImageVault;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Code behind of MoreImagesPopUp control.
    /// </summary>
    public partial class MoreImagesPopUp : ScandicUserControlBase
    {
        private PageData moreImagesPopUpPD;

        /// <summary>
        /// Gets/Sets MoreImagesPopUpPD
        /// </summary>
        public PageData MoreImagesPopUpPD
        {
            get
            {
                PageData pd = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                           EPiServer.Security.AccessLevel.NoAccess);
                moreImagesPopUpPD = ((PageReference) pd["MoreImagesPopUp"] != null)
                                        ? DataFactory.Instance.GetPage((PageReference) pd["MoreImagesPopUp"])
                                        : null;
                return moreImagesPopUpPD;
            }
        }

        private PageData hotelPage;

        /// <summary>
        /// Gets/Sets HotelPage
        /// </summary>
        public PageData HotelPage
        {
            get { return hotelPage; }
            set { hotelPage = value; }
        }

        /// <summary>
        /// Gets/Sets ZoomedInInnerProperty
        /// </summary>
        public PropertyData ZoomedInInnerProperty { get; set; }

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int hotelID = -1;

            if (Request.QueryString["popuptype"] != null)
            {
                if (int.TryParse(Request.QueryString.Get("hotelid"), out hotelID))
                {
                    if (hotelID != -1)
                    {
                        HotelPage = DataFactory.Instance.GetPage(new PageReference(hotelID), AccessLevel.NoAccess);
                        bool exist360Image = (HotelPage["360Image"] != null) ? true : false;

                        if (Request.QueryString.Get("popuptype") == "images")
                        {
                            ImagesPopUpPH.Visible = true;
                            Image360PH.Visible = false;

                            if (exist360Image)
                            {
                                Image360LinkPH.Visible = true;
                                Image360LinkLiteral.Text =
                                    LanguageManager.Instance.Translate(
                                        "/Templates/Scanweb/Units/Static/MoreImagesPopUp/Image360");
                            }
                            else
                                Image360LinkPH.Visible = false;
                            if (!IsPostBack)
                                ShowLargeImage("GeneralImage1");
                        }
                        else if (Request.QueryString.Get("popuptype") == "image360" && exist360Image)
                        {
                            ImagesPopUpPH.Visible = false;
                            Image360PH.Visible = true;

                            Image360LinkPH.Visible = true;
                            Image360LinkLiteral.Text =
                                LanguageManager.Instance.Translate(
                                    "/Templates/Scanweb/Units/Static/MoreImagesPopUp/Images");
                        }
                    }
                }

                #region LinkButtons EventHandling

                GeneralLinkButton1.Command += new CommandEventHandler(PopUpImageLinkClick);
                GeneralLinkButton2.Command += new CommandEventHandler(PopUpImageLinkClick);
                GeneralLinkButton3.Command += new CommandEventHandler(PopUpImageLinkClick);
                GeneralLinkButton4.Command += new CommandEventHandler(PopUpImageLinkClick);
                GeneralLinkButton5.Command += new CommandEventHandler(PopUpImageLinkClick);
                GeneralLinkButton6.Command += new CommandEventHandler(PopUpImageLinkClick);
                GeneralLinkButton7.Command += new CommandEventHandler(PopUpImageLinkClick);
                GeneralLinkButton8.Command += new CommandEventHandler(PopUpImageLinkClick);
                GeneralLinkButton9.Command += new CommandEventHandler(PopUpImageLinkClick);
                GeneralLinkButton10.Command += new CommandEventHandler(PopUpImageLinkClick);


                RoomLinkButton1.Command += new CommandEventHandler(PopUpImageLinkClick);
                RoomLinkButton2.Command += new CommandEventHandler(PopUpImageLinkClick);
                RoomLinkButton3.Command += new CommandEventHandler(PopUpImageLinkClick);
                RoomLinkButton4.Command += new CommandEventHandler(PopUpImageLinkClick);
                RoomLinkButton5.Command += new CommandEventHandler(PopUpImageLinkClick);
                RoomLinkButton6.Command += new CommandEventHandler(PopUpImageLinkClick);
                RoomLinkButton7.Command += new CommandEventHandler(PopUpImageLinkClick);
                RoomLinkButton8.Command += new CommandEventHandler(PopUpImageLinkClick);
                RoomLinkButton9.Command += new CommandEventHandler(PopUpImageLinkClick);
                RoomLinkButton10.Command += new CommandEventHandler(PopUpImageLinkClick);

                RestLinkButton1.Command += new CommandEventHandler(PopUpImageLinkClick);
                RestLinkButton2.Command += new CommandEventHandler(PopUpImageLinkClick);
                RestLinkButton3.Command += new CommandEventHandler(PopUpImageLinkClick);
                RestLinkButton4.Command += new CommandEventHandler(PopUpImageLinkClick);
                RestLinkButton5.Command += new CommandEventHandler(PopUpImageLinkClick);
                RestLinkButton6.Command += new CommandEventHandler(PopUpImageLinkClick);
                RestLinkButton7.Command += new CommandEventHandler(PopUpImageLinkClick);
                RestLinkButton8.Command += new CommandEventHandler(PopUpImageLinkClick);
                RestLinkButton9.Command += new CommandEventHandler(PopUpImageLinkClick);
                RestLinkButton10.Command += new CommandEventHandler(PopUpImageLinkClick);

                LeisureLinkButton1.Command += new CommandEventHandler(PopUpImageLinkClick);
                LeisureLinkButton2.Command += new CommandEventHandler(PopUpImageLinkClick);
                LeisureLinkButton3.Command += new CommandEventHandler(PopUpImageLinkClick);
                LeisureLinkButton4.Command += new CommandEventHandler(PopUpImageLinkClick);
                LeisureLinkButton5.Command += new CommandEventHandler(PopUpImageLinkClick);
                LeisureLinkButton6.Command += new CommandEventHandler(PopUpImageLinkClick);
                LeisureLinkButton7.Command += new CommandEventHandler(PopUpImageLinkClick);
                LeisureLinkButton8.Command += new CommandEventHandler(PopUpImageLinkClick);
                LeisureLinkButton9.Command += new CommandEventHandler(PopUpImageLinkClick);
                LeisureLinkButton10.Command += new CommandEventHandler(PopUpImageLinkClick);

                ImagesPopUpPrevious.Command += new CommandEventHandler(PopUpImageLinkClick);
                ImagesPopUpNext.Command += new CommandEventHandler(PopUpImageLinkClick);

                #endregion

                DataBind();
            }
        }

        /// <summary>
        /// Gets the PropertyData object for a specific ImageVault image property
        /// </summary>
        /// <param name="imageVaultPropertyName"> </param>
        /// <param name="placeholdername"> </param>
        /// <returns>A PropertyData object with the image</returns>
        protected PropertyData GetPopUpImageFromImageVault(string imageVaultPropertyName, string placeholdername)
        {
            PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET", "ImageStoreNET.ImageType");
            string strImage = HotelPage[imageVaultPropertyName] as string;

            if (!String.IsNullOrEmpty(strImage))
            {
                pd.Value = strImage;
            }
            else
            {
                #region Switch Placeholder visibility

                switch (placeholdername)
                {
                    case "GeneralImage1PH":
                        GeneralImage1PH.Visible = false;
                        break;
                    case "GeneralImage2PH":
                        GeneralImage2PH.Visible = false;
                        break;
                    case "GeneralImage3PH":
                        GeneralImage3PH.Visible = false;
                        break;
                    case "GeneralImage4PH":
                        GeneralImage4PH.Visible = false;
                        break;
                    case "GeneralImage5PH":
                        GeneralImage5PH.Visible = false;
                        break;
                    case "GeneralImage6PH":
                        GeneralImage6PH.Visible = false;
                        break;
                    case "GeneralImage7PH":
                        GeneralImage7PH.Visible = false;
                        break;
                    case "GeneralImage8PH":
                        GeneralImage8PH.Visible = false;
                        break;
                    case "GeneralImage9PH":
                        GeneralImage9PH.Visible = false;
                        break;
                    case "GeneralImage10PH":
                        GeneralImage10PH.Visible = false;
                        break;
                    case "RoomImage1PH":
                        RoomImage1PH.Visible = false;
                        break;
                    case "RoomImage2PH":
                        RoomImage2PH.Visible = false;
                        break;
                    case "RoomImage3PH":
                        RoomImage3PH.Visible = false;
                        break;
                    case "RoomImage4PH":
                        RoomImage4PH.Visible = false;
                        break;
                    case "RoomImage5PH":
                        RoomImage5PH.Visible = false;
                        break;
                    case "RoomImage6PH":
                        RoomImage6PH.Visible = false;
                        break;
                    case "RoomImage7PH":
                        RoomImage7PH.Visible = false;
                        break;
                    case "RoomImage8PH":
                        RoomImage8PH.Visible = false;
                        break;
                    case "RoomImage9PH":
                        RoomImage9PH.Visible = false;
                        break;
                    case "RoomImage10PH":
                        RoomImage10PH.Visible = false;
                        break;
                    case "RestImage1PH":
                        RestImage1PH.Visible = false;
                        break;
                    case "RestImage2PH":
                        RestImage2PH.Visible = false;
                        break;
                    case "RestImage3PH":
                        RestImage3PH.Visible = false;
                        break;
                    case "RestImage4PH":
                        RestImage4PH.Visible = false;
                        break;
                    case "RestImage5PH":
                        RestImage5PH.Visible = false;
                        break;
                    case "RestImage6PH":
                        RestImage6PH.Visible = false;
                        break;
                    case "RestImage7PH":
                        RestImage7PH.Visible = false;
                        break;
                    case "RestImage8PH":
                        RestImage8PH.Visible = false;
                        break;
                    case "RestImage9PH":
                        RestImage9PH.Visible = false;
                        break;
                    case "RestImage10PH":
                        RestImage10PH.Visible = false;
                        break;
                    case "LeisureImage1PH":
                        LeisureImage1PH.Visible = false;
                        break;
                    case "LeisureImage2PH":
                        LeisureImage2PH.Visible = false;
                        break;
                    case "LeisureImage3PH":
                        LeisureImage3PH.Visible = false;
                        break;
                    case "LeisureImage4PH":
                        LeisureImage4PH.Visible = false;
                        break;
                    case "LeisureImage5PH":
                        LeisureImage5PH.Visible = false;
                        break;
                    case "LeisureImage6PH":
                        LeisureImage6PH.Visible = false;
                        break;
                    case "LeisureImage7PH":
                        LeisureImage7PH.Visible = false;
                        break;
                    case "LeisureImage8PH":
                        LeisureImage8PH.Visible = false;
                        break;
                    case "LeisureImage9PH":
                        LeisureImage9PH.Visible = false;
                        break;
                    case "LeisureImage10PH":
                        LeisureImage10PH.Visible = false;
                        break;
                    default:
                        break;
                }

                #endregion
            }

            return pd;
        }

        /// <summary>
        /// Gets the PropertyData object for a specific ImageVault image property
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns>A PropertyData object with the image</returns>
        protected PropertyData GetPopUp360FromImageVault(string propertyName)
        {
            PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET",
                                                                    "ImageStoreNET.Developer.WebControls.PropertyImageVaultComplexMedia");

            string strImage = HotelPage[propertyName] as string;

            if (!String.IsNullOrEmpty(strImage))
            {
                pd.Value = strImage;
            }

            return pd;
        }

        /// <summary>
        /// Gets image 360 url.
        /// </summary>
        /// <returns></returns>
        protected string GetImage360URL()
        {
            string imagesPopUpURL = MoreImagesPopUpPD.LinkURL ?? String.Empty;
            string hotelID = HotelPage.PageLink.ID.ToString() ?? String.Empty;
            UrlBuilder url = new UrlBuilder(String.Empty);
            url = new UrlBuilder(UriSupport.AddQueryString(imagesPopUpURL, "hotelid", hotelID));

            if (Request.QueryString.Get("popuptype").Equals("images") && HotelPage["360Image"] != null)
            {
                url = new UrlBuilder(UriSupport.AddQueryString(url.ToString(), "popuptype", "image360"));
                return url.ToString();
            }
            else if (Request.QueryString.Get("popuptype").Equals("image360") && HotelPage["360Image"] != null)
            {
                url = new UrlBuilder(UriSupport.AddQueryString(url.ToString(), "popuptype", "images"));
                return url.ToString();
            }
            return String.Empty;
        }

        /// <summary>
        /// Event raised when clicked on a LinkButton
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments of linkbutton, these pass IVImage propertyname</param>
        protected void PopUpImageLinkClick(object sender, CommandEventArgs e)
        {
            ShowLargeImage(e.CommandArgument.ToString());
        }

        /// <summary>
        /// This image will show up the Large image for the specified image on the right hand side of the More images popup.
        /// </summary>
        /// <param name="imagePropertyName">This is the property name for the Images in Scanweb [Hotel] page type</param>
        private void ShowLargeImage(string imagePropertyName)
        {
            Control controlLoad = new Control();
            controlLoad = LoadControl("~\\Templates\\Scanweb\\Units\\Static\\LargeImage.ascx");

            ((LargeImage) controlLoad).ImageWidth = 300;
            ((LargeImage) controlLoad).ContentPage = HotelPage;
            ((LargeImage) controlLoad).ImagePropertyName = imagePropertyName;

            ZoomedInPopUpPH.Visible = true;
            ZoomedInPopUpImage.Controls.Add(controlLoad);

            ZoomedInDescription.Text = GetImageDescription(imagePropertyName);

            string imageProperty = imagePropertyName;
            int imageNumber = -1;
            int prevNumber = -1;
            int nextNumber = -1;


            prevNumber = GetPreviousImageNumber(imageProperty);

            nextNumber = GetNextImageNumber(imageProperty);

            if (prevNumber == 0)
            {
                ImagesPopUpPrevious.Enabled = false;
                ImagesPopUpPrevious.CssClass = "disableLink";
            }
            else
            {
                ImagesPopUpPrevious.Enabled = true;
                ImagesPopUpPrevious.CssClass = "MoreImagesPopUpPrevLink";
            }

            if (nextNumber == 11)
            {
                ImagesPopUpNext.Enabled = false;
                ImagesPopUpNext.CssClass = "disableLink";
            }
            else
            {
                ImagesPopUpNext.Enabled = true;
                ImagesPopUpNext.CssClass = "MoreImagesPopUpNextLink";
            }

            imageProperty = GetStringPart(imageProperty);
            ImagesPopUpPrevious.CommandArgument = imageProperty + prevNumber.ToString();
            ImagesPopUpNext.CommandArgument = imageProperty + nextNumber.ToString();
        }

        /// <summary>
        /// Gets next image number
        /// </summary>
        /// <param name="image"></param>
        /// <returns>next image number</returns>
        protected int GetNextImageNumber(string image)
        {
            int pageNumber = int.Parse(GetDigitPart(image));

            string page = GetStringPart(image);

            int nextNumber = 11;

            for (int i = pageNumber + 1; i <= 10; i++)
            {
                if (HotelPage[page + i.ToString()] != null)
                {
                    nextNumber = i;
                    break;
                }
            }

            return nextNumber;
        }

        /// <summary>
        /// Gets previous image number
        /// </summary>
        /// <param name="image"></param>
        /// <returns>previous image number</returns>
        protected int GetPreviousImageNumber(string image)
        {
            int pageNumber = int.Parse(GetDigitPart(image));

            string page = GetStringPart(image);
            int prevNumber = 0;

            for (int i = pageNumber - 1; i > 0; i--)
            {
                if (HotelPage[page + i.ToString()] != null)
                {
                    prevNumber = i;
                    break;
                }
            }

            return prevNumber;
        }

        /// <summary>
        /// GetDigitPart
        /// </summary>
        /// <param name="image"></param>
        /// <returns>DigitPart</returns>
        private string GetDigitPart(string image)
        {
            string newresult = "";
            foreach (char c in image)
            {
                if (char.IsDigit(c))
                {
                    newresult += c.ToString();
                }
            }
            return newresult;
        }

        /// <summary>
        /// GetStringPart
        /// </summary>
        /// <param name="image"></param>
        /// <returns>StringPart</returns>
        private string GetStringPart(string image)
        {
            string newresult = "";
            foreach (char c in image)
            {
                if (char.IsLetter(c))
                {
                    newresult += c.ToString();
                }
            }
            return newresult;
        }

        /// <summary>
        /// Gets alt text
        /// </summary>
        /// <param name="ImagePropertyName"></param>
        /// <returns>alt text</returns>
        protected string GetAltText(string ImagePropertyName)
        {
            PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET", "ImageStoreNET.ImageType");
            string strImage = HotelPage[ImagePropertyName] as string;
            string strAltText = String.Empty;

            if (!String.IsNullOrEmpty(strImage))
            {
                LangAltText altText = new LangAltText();
                strAltText = altText.GetAltText(CurrentPage.LanguageID, HotelPage[ImagePropertyName].ToString());
            }

            return strAltText;
        }

        /// <summary>
        /// Gets image title
        /// </summary>
        /// <param name="ImagePropertyName"></param>
        /// <returns>image title</returns>
        protected string GetImageTitle(string ImagePropertyName)
        {
            GetProperties IVProp = new GetProperties();
            return IVProp.GetImageTitle(CurrentPage.LanguageID, HotelPage[ImagePropertyName].ToString());
        }

        /// <summary>
        /// Gets image description
        /// </summary>
        /// <param name="ImagePropertyName"></param>
        /// <returns>image description</returns>
        protected string GetImageDescription(string ImagePropertyName)
        {
            GetProperties IVProp = new GetProperties();
            return IVProp.GetImageDescription(CurrentPage.LanguageID, HotelPage[ImagePropertyName].ToString());
        }

        /// <summary>
        /// Shows 360 link
        /// </summary>
        /// <returns>True/False</returns>
        protected bool Show360Link()
        {
            bool retVal = true;
            if (RootPage["Hide360ImageLink"] != null)
            {
                retVal = false;
            }
            return retVal;
        }
    }
}