<%@ Control Language="C#" AutoEventWireup="true" Codebehind="XmlOfferCityList.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.XmlOfferCityList" %>

<asp:PlaceHolder ID="CityListControl" runat="server">
    <table class="OfferListTable" cellpadding="0" cellspacing="0" border="0"  >
    <tr >
            <td class="CitynameSquare">
                <%=GetCityName()%>
            </td>
            <asp:PlaceHolder ID="PlaceHolderFlex" runat="server">
                <td class="FlexOrMeeting">
                  <asp:Image ImageUrl="~/Templates/Scanweb/Styles/Default/Images/Icons/box_information.gif" CssClass="InfoIcon" onClick="javascript:alert('no information-url set yet.')" runat="server" />&nbsp;Early
                </td>
                <td class="FlexOrMeeting">
                  <asp:Image ImageUrl="~/Templates/Scanweb/Styles/Default/Images/Icons/box_information.gif" CssClass="InfoIcon" onClick="javascript:alert('no information-url set yet.')" runat="server" />&nbsp;Flex
                </td>
            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="PlaceHolderMeeting" runat="server">
                <td class="FlexOrMeeting">24h package</td>
                <td class="FlexOrMeeting">24h package</td>
            </asp:PlaceHolder>            
   </tr>
        <asp:PlaceHolder ID="HotelPlaceHolder" runat="server"> 
        </asp:PlaceHolder>
    </table>
</asp:PlaceHolder>
