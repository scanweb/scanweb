//  Description					:   OfferCategoryListing                                  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.Util;
using EPiServer.SpecializedProperties;
using System.Web;
using System.Collections.Specialized;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Code behind of OfferCategoryListing control.
    /// </summary>
    public partial class OfferCategoryListing : ScandicUserControlBase
    {
        /// <summary>
        /// Gets/Sets OfferCategoryPage
        /// </summary>
        public PageData OfferCategoryPage { get; set; }

        public string OfferCategories { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.OffersPageList.ItemDataBound += new RepeaterItemEventHandler(Repeater_ItemDataBound);
        }
        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            PageDataCollection pageCollection = DataFactory.Instance.GetChildren(OfferCategoryPage.PageLink);
            PageDataCollection newPageCollection = new PageDataCollection();
            foreach (PageData eachPage in pageCollection)
            {
                TimeSpan span = DateTime.Now.Subtract(eachPage.StopPublish);
                if (eachPage["InvisibleInOfferList"] == null && span.Days < 0)
                {
                    if (eachPage["OfferEndTime"] == null)
                        newPageCollection.Add(eachPage);
                    else if (eachPage["OfferEndTime"] != null && Convert.ToDateTime(eachPage["OfferEndTime"]) > DateTime.Now)
                        newPageCollection.Add(eachPage);
                }
            }
            OffersPageList.DataSource = newPageCollection;
            OffersPageList.DataBind();
        }        

        /// Gets price text
        /// </summary>
        /// <param name="offerPage"></param>
        /// <returns></returns>
        protected string GetPriceText(PageData offerPage)
        {
            string returnString = string.Empty;
            if (offerPage["PromoBoxPriceText"] != null)
            {
                returnString = offerPage["PromoBoxPriceText"] as string;
            }
            return returnString;
        }

        /// <summary>
        /// Gets expand collapse class name
        /// </summary>
        /// <returns></returns>
        public string GetExpandCollapseClassName()
        {
            if (CurrentPage.PageTypeName == "Offer Category")
            {
                return "OfferCategoryListExpanded";
            }
            else
            {
                return "OfferCategoryListNotExpanded";
            }
        }

        protected void Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var pageData = e.Item.DataItem as PageData;

                if (pageData != null)
                {
                    OfferCategories = Convert.ToString(OfferCategoryPage.PageLink.ID);
                    LinkItemCollection categoriesCollection = pageData["OfferCategoryMapper"] as LinkItemCollection;

                    if (categoriesCollection != null)
                    {
                        for (int i = 0; i < categoriesCollection.Count; i++)
                        {
                            NameValueCollection categoryCollection = HttpUtility.ParseQueryString(categoriesCollection[i].GetMappedHref());
                            for (int j = 0; j < categoryCollection.Count; j++)
                            {
                                if (categoryCollection.Keys[j].Contains("id"))
                                    OfferCategories += "," + categoryCollection[j];
                            }
                        }
                    }
                    var squaredImage = e.Item.FindControl("SquaredImage") as Scandic.Scanweb.CMS.Templates.Units.Placeable.SquaredCornerImage;
                    squaredImage.ContentPage = pageData;

                    var categoryListing = e.Item.FindControl("OfferCategoryListing") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    categoryListing.Attributes.Add("data-id", OfferCategories);

                    var priceText = e.Item.FindControl("priceText") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    priceText.InnerHtml = GetPriceText(pageData);

                    string readMoreLinksTitle = WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/Box/BookOfferText");
                    var readMoreLink1 = e.Item.FindControl("ReadMoreLink1") as System.Web.UI.HtmlControls.HtmlAnchor;
                    readMoreLink1.HRef = pageData.LinkURL;
                    readMoreLink1.Title = readMoreLinksTitle;

                    var readMoreLink2 = e.Item.FindControl("ReadMoreLink2") as System.Web.UI.HtmlControls.HtmlAnchor;
                    readMoreLink2.HRef = pageData.LinkURL;
                    readMoreLink2.Title = readMoreLinksTitle;

                    var offerCategoryListing = e.Item.FindControl("OfferCategoryListing") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (offerCategoryListing != null)
                    {
                        string contentName = string.Empty;
                        if (pageData["BoxHeading"] != null)
                            contentName = Convert.ToString(pageData["BoxHeading"]);
                        var trackingMethod = string.Format("trackPromobox(this,'{0}',setSelectedCategoryText());", contentName);
                        offerCategoryListing.Attributes.Add("onclick", string.Format("{0}javascript:window.location.href = '{1}{2}", trackingMethod, pageData.LinkURL, "';return false"));
                    }
                }
            }
        }
    }
}