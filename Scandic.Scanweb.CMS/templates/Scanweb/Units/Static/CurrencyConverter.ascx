<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CurrencyConverter.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.CurrencyConverter" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/templates/Scanweb/Styles/Default/Popup.css") %>?v=<%=CmsUtil.GetCSSVersion()%>" />
<head id="head1">
<title><%= WebUtil.GetTranslatedText("/bookingengine/booking/currencyConverter/CurrencyConverterText") %></title>
</head>
 <div class="PopUp">
    <div class="PopUpTop">
      <div class="ClosePopUp"> <a class="CloseWindowPic" onclick="self.close();"> Close window </a> </div>
    </div>
    <div class="HeaderLeft"></div>
    <div class="PopUpHeader">
      <h1><%= WebUtil.GetTranslatedText("/bookingengine/booking/currencyConverter/CurrencyConverterHeader") %></h1>
    </div>
    <div class="PopUpBody">
      <p><%= WebUtil.GetTranslatedText("/bookingengine/booking/currencyConverter/CurrencyConverterText") %>
      </p>
      <div id="CurrencyConverterPopup">
        <div class="CurrencyConverterPopupTop">&nbsp;</div>
        <div class="CurrencyConverterPopupContent">
            <div class="formRow">
            <label>
            <%= WebUtil.GetTranslatedText("/bookingengine/booking/currencyConverter/change") %>
            </label><br />
            <asp:TextBox TabIndex="1" ID="txtCurrencyChange" MaxLength="8" runat="server"></asp:TextBox><br />      
            </div>            
            <div class="formRow">
            <label>
            <%= WebUtil.GetTranslatedText("/bookingengine/booking/currencyConverter/BaseCurrency") %>
            </label><br />
            <asp:DropDownList TabIndex="2" Width="250" ID="ddlSourceCurrency" runat="server">
            </asp:DropDownList><br />   
            </div>
            <div class="formRow">
            <label>
              <%= WebUtil.GetTranslatedText("/bookingengine/booking/currencyConverter/OtherCurrency") %>
              </label><br />
            <asp:DropDownList TabIndex="3" Width="250" ID="ddlDestinationCurrency" runat="server">
            </asp:DropDownList><br />  
            </div>                        
            <div class="footerDiv">                         
                <div  class="ButtonLink">
                    <div>
                    <asp:LinkButton TabIndex="4" ID="btnConvert" Text="Convert" OnClick="btnConvertCurrency_Click" runat="server"></asp:LinkButton>                
                    </div>
                </div>
                <br style="clear: both" />       
              </div>      
            </div>          
          <div class="CurrencyConverterPopupBottom">&nbsp;</div>
     </div>    
      <p><asp:Label ID="lblConvertedAmount" runat="server"></asp:Label>  
      <strong><asp:Label ID="lblConvertedValue" runat="server" ></asp:Label></strong>
      </p> 
      <div class="formGroupError" id="errorReport">
         <div class="redAlertIcon">
            <strong><asp:Label ID="lblErrorMessage" runat="server" ></asp:Label></strong>
        </div>
     </div>   
     </div>
  </div>
  
  


