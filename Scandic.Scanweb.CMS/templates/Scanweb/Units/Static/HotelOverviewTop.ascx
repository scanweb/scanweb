<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelOverviewTop.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.HotelOverviewTop" %>

<%@ Register TagPrefix="Scanweb" TagName="FindHotelSearch" Src="~/Templates/Scanweb/Units/Static/FindHotelSearch.ascx" %>

<div id="OverviewMapArea">
    <div id="OverviewMapTop"></div>
    <div id="OverviewMapContent">
        <div id="OverviewMapImageMapArea">
            <%--<asp:Literal ID="ImageMapContent" runat="server" />--%>
            <EPiServer:Property ID="ImageMapContent" runat="server" />
        </div>
        <div id="OverviewMapHotelLinksArea">
           <%-- <Scanweb:FindHotelInputBox runat="server" /> --%>
            <Scanweb:FindHotelSearch ID="HotelSearch" runat="server" />
            <%--<asp:Literal ID="HotelLinksContent" runat="server" />--%>
             <EPiServer:Property ID="HotelLinksContent" runat="server" />
        </div>
    </div>
    <div id="OverviewMapBottom"></div>
</div>