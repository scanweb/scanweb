<%@ Control Language="C#" AutoEventWireup="true" Codebehind="XmlOfferHotelList.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.XmlOfferHotelList" %>
<asp:PlaceHolder ID="Hotelcontrol" runat="server">
    
   <tr class="<%=IsLastRow()%>">
            <td class="HotelSquare">
                <%=GetHotelName() %><br />
                
                <asp:PlaceHolder ID="PlaceHolderBookbtn" runat="server">
                    <asp:Button Text="Book" runat="server" CssClass="HotelBookingButton" OnClick="BookBtn_Click" />
                </asp:PlaceHolder>
               
                <asp:PlaceHolder ID="PlaceHolderMeetingBtn" runat="server">
                    <asp:Button Text="Send RFP" runat="server" CssClass="HotelBookingButton" OnClick="RFPButton_Click"  />
                </asp:PlaceHolder>
                
            </td>
            <td class="firstSquare">
               From&nbsp;<%=GetHotelFirstPrice()%>&nbsp;<%=Hotel.Currency%><br />per room/night
            </td>
            <td class="secondSquare">
              From&nbsp;<%=GetHotelSecondPrice()%>&nbsp;<%=Hotel.Currency%><br />per room/night
            </td>
   </tr>
  
</asp:PlaceHolder>