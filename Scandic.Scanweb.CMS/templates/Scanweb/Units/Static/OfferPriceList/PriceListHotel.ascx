<%@ Control Language="C#" AutoEventWireup="true" Codebehind="PriceListHotel.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.PriceListHotel" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<asp:PlaceHolder ID="Hotelcontrol" runat="server">
    
   <tr class="<%= IsLastRow() %>">
            <td class="HotelSquare">
                <%= GetHotelName() %><br />
                
                <asp:PlaceHolder ID="PlaceHolderMeetingBtn" runat="server">
                <div class="actionBtn">
                    <asp:Button translate="/Templates/Scanweb/Units/Static/PriceListHotel/SendRFP" runat="server" CssClass="submit buttonInner sprite" OnClick="RFPButton_Click"  />
                    <asp:Button ID="Button1" runat="server" CssClass="buttonRt sprite noBorder" OnClick="RFPButton_Click"  />
                    </div>
                </asp:PlaceHolder>
                
            </td>
            <td class="firstSquare">
                <% if (GetHotelFirstPrice().Trim().Length > 0)
                   {%>
               <EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Units/Static/PriceListHotel/PriceFrom"  runat="server" /> &nbsp;<%= GetHotelFirstPrice() %>&nbsp;<%= Hotel.Currency %><br /><EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Units/Static/PriceListHotel/HotelPrice"  runat="server" /> 
               <% } %>
            </td>
            <td class="secondSquare">
                <% if (GetHotelSecondPrice().Trim().Length > 0)
                   {%>
             <EPiServer:Translate ID="Translate4" Text="/Templates/Scanweb/Units/Static/PriceListHotel/PriceFrom"  runat="server" />&nbsp;<%= GetHotelSecondPrice() %>&nbsp;<%= Hotel.Currency %><br /><EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Units/Static/PriceListHotel/HotelPrice"  runat="server" /> 
                <% } %>
            </td>
            <!--R1.4: CR13: Extend Price List
                Added colum to display the Special rate -->
                <% if (ContentDataAccess.ShowSpecialRates(CurrentPage.PageLink))
                   { %>
            <td class="firstSquare">
                <% if (GetHotelThirdPrice().Trim().Length > 0)
                   {%>
                &nbsp;<% if (GetSpecialRateName().Trim().Length > 0)
                         {%><strong><%= GetSpecialRateName() %></strong><br /><% } %>
             <EPiServer:Translate ID="Translate5" Text="/Templates/Scanweb/Units/Static/PriceListHotel/PriceFrom"  runat="server" />&nbsp;<%= GetHotelThirdPrice() %>&nbsp;<%= Hotel.Currency %><br /><EPiServer:Translate ID="Translate6" Text="/Templates/Scanweb/Units/Static/PriceListHotel/HotelPrice"  runat="server" /> 
                <% } %>
            </td>
            <% } %>

   </tr>
  
</asp:PlaceHolder>