//  Description					: PriceListCity                                           //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.CMS.code.Util.HotelOfferList;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Code behind of PriceListCity control.
    /// </summary>
    public partial class PriceListCity : EPiServer.UserControlBase
    {
        /// <summary>
        /// City
        /// </summary>
        public OfferCity City;

        /// <summary>
        /// PriceTypes
        /// </summary>
        public string PriceType;

        /// <summary>
        /// Gets RootPage
        /// </summary>
        public static PageData RootPage
        {
            get { return DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess); }
        }

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (PriceType.Equals("FlexEarlySpecial"))
            {
                PlaceHolderFlex.Visible = true;
                PlaceHolderMeeting.Visible = false;
            }
            else
            {
                PlaceHolderFlex.Visible = false;
                PlaceHolderMeeting.Visible = true;
            }

            foreach (OfferHotel Hotel in City.GetListOfHotels())
            {
                Scandic.Scanweb.CMS.Templates.Units.Static.PriceListHotel HotelControl =
                    (Scandic.Scanweb.CMS.Templates.Units.Static.PriceListHotel)
                    LoadControl("\\Templates\\Scanweb\\Units\\Static\\OfferPriceList\\PriceListHotel.ascx");
                HotelControl.Hotel = Hotel;
                HotelControl.PriceType = PriceType;

                if (City.GetListOfHotels()[City.GetListOfHotels().Count - 1].Equals(Hotel))
                    HotelControl.LastItem = true;
                else
                    HotelControl.LastItem = false;
                HotelPlaceHolder.Controls.Add(HotelControl);
            }
        }

        /// <summary>
        /// Gets City name
        /// </summary>
        /// <returns></returns>
        protected string GetCityName()
        {
            return this.City.CityName;
        }

        /// <summary>
        /// Gets City ID
        /// </summary>
        /// <returns></returns>
        protected string GetCityID()
        {
            return this.City.CityID;
        }

        /// <summary>
        /// Gets early
        /// </summary>
        /// <returns></returns>
        protected string GetEarly()
        {
            PageReference earlyLink = RootPage["EarlyRateCategory"] as PageReference;
            if (earlyLink != null)
            {
                PageData early = DataFactory.Instance.GetPage(earlyLink, EPiServer.Security.AccessLevel.NoAccess);
                return early["RateCategoryDescription"] as string;
            }
            else
                return string.Empty;
        }

        /// <summary>
        /// Gets Flex 
        /// </summary>
        /// <returns></returns>
        protected string GetFlex()
        {
            PageReference flexLink = RootPage["FlexRateCategory"] as PageReference;
            if (flexLink != null)
            {
                PageData flex = DataFactory.Instance.GetPage(flexLink, EPiServer.Security.AccessLevel.NoAccess);
                return flex["RateCategoryDescription"] as string;
            }
            else
                return string.Empty;
        }

       /// <summary>
       /// Gets special
       /// </summary>
       /// <returns></returns>
        protected string GetSpecial()
        {
            PageReference specialLink = RootPage["SpecialRateCategory"] as PageReference;
            if (specialLink != null)
            {
                PageData special = DataFactory.Instance.GetPage(specialLink, EPiServer.Security.AccessLevel.NoAccess);
                return special["RateCategoryDescription"] as string;
            }
            else
                return string.Empty;
        }
    }
}