//  Description					:   PriceListHotel                                        //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.HotelOfferList;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.ExceptionManager;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// PriceListHotel
    /// </summary>
    public partial class PriceListHotel : ScandicUserControlBase
    {
        /// <summary>
        /// Hotel
        /// </summary>
        public OfferHotel Hotel;

        /// <summary>
        /// PriceType
        /// </summary>
        public string PriceType;

        /// <summary>
        /// LastItem
        /// </summary>
        public bool LastItem;

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (PriceType.Equals("FlexEarlySpecial"))
            {                
                PlaceHolderMeetingBtn.Visible = false;
            }
            else
            {
                PlaceHolderMeetingBtn.Visible = true;
            }
        }

        /// <summary>
        /// Gets hotel name.
        /// </summary>
        /// <returns></returns>
        protected string GetHotelName()
        {
            return this.Hotel.HotelName;
        }

        /// <summary>
        /// Gets hotel first price.
        /// </summary>
        /// <returns></returns>
        protected string GetHotelFirstPrice()
        {
            return this.Hotel.FirstPrice;
        }

        /// <summary>
        /// Gets hotel second price.
        /// </summary>
        /// <returns></returns>
        protected string GetHotelSecondPrice()
        {
            return this.Hotel.SecondPrice;
        }

        /// <summary>
        ///  Gets hotel third price.
        /// </summary>
        /// <returns></returns>
        protected string GetHotelThirdPrice()
        {
            return this.Hotel.ThirdPrice;
        }

        /// <summary>
        /// Gets special rate name.
        /// </summary>
        /// <returns></returns>
        protected string GetSpecialRateName()
        {
            return this.Hotel.SpecialRateName;
        }

        /// <summary>
        /// Gets IsLastRow
        /// </summary>
        /// <returns></returns>
        protected string IsLastRow()
        {
            if (LastItem)
                return "noBorder";

            return
                string.Empty;
        }

        /// <summary>
        /// RFP button click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RFPButton_Click(object sender, EventArgs e)
        {
            Dictionary<int, string> selectedHotelsDictionary = new Dictionary<int, string>();
            PageData HotelPageData = Hotel.GetHotelPageData();

            selectedHotelsDictionary.Add(HotelPageData.PageLink.ID, GetHotelName());

            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);

            PageReference RFPageRef = rootPage["RequestForPorposalPage"] as PageReference;

            if (RFPageRef != null)
            {
                PageData RFPage = DataFactory.Instance.GetPage(RFPageRef, EPiServer.Security.AccessLevel.NoAccess);
                MiscellaneousSessionWrapper.MeetingsSelectedHotels = selectedHotelsDictionary;
                Response.Redirect(RFPage.LinkURL.ToString());
            }
        }               
    }
}