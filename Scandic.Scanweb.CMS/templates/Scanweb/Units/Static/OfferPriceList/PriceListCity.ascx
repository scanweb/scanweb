<%@ Control Language="C#" AutoEventWireup="true" Codebehind="PriceListCity.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.PriceListCity" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>

<asp:PlaceHolder ID="CityListControl" runat="server">
    
    <table class="OfferListTable" cellpadding="0" cellspacing="0" border="0"  >
    <tr >
            <td class="CitynameSquare">
                <%= GetCityName() %>
            </td>
            <asp:PlaceHolder ID="PlaceHolderFlex" runat="server">
                
                <td class="FlexOrMeeting">
                <div>
               <%--<asp:Image ImageUrl="~/Templates/Scanweb/Styles/Default/Images/Icons/box_information.gif" CssClass="InfoIcon" onClick="SwitchClassName('EarlyColumn<%=GetCityName()%>','EarlyNotExpanded','EarlyExpanded')" runat="server" />&nbsp;<EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Units/Static/PriceListCity/Early"   runat="server" />--%>
                   <a id="Early<%= GetCityID() %>" class="InfoIcon" onclick="showDesc(this,'EarlyColumn<%= GetCityID() %>')">&nbsp;&nbsp;&nbsp;<EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Units/Static/PriceListCity/Early" runat="server" /></a>
                 </div>                
                <div id="EarlyColumn<%= GetCityID() %>" class="Desc_Room">
               <a class="InfoIcon" onclick="hideDesc('EarlyColumn<%= GetCityID() %>')">&nbsp;&nbsp;&nbsp;<EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Units/Static/PriceListCity/Early" runat="server" /></a>
                <div>
                <%= GetEarly() %>   
                </div>            
               </div>
                </td>
               
                
                <td class="FlexOrMeeting">
                <div>
                 <a id="Flex<%= GetCityID() %>" class="InfoIcon" onclick="showDesc(this,'FlexColumn<%= GetCityID() %>')">&nbsp;&nbsp;&nbsp;<EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Units/Static/PriceListCity/Flexy"  runat="server" /></a>
                </div>
                <div id="FlexColumn<%= GetCityID() %>" class="Desc_Room">                 
               <a class="InfoIcon" onclick="hideDesc('FlexColumn<%= GetCityID() %>')">&nbsp;&nbsp;&nbsp;<EPiServer:Translate ID="Translate4" Text="/Templates/Scanweb/Units/Static/PriceListCity/Flexy"  runat="server" /></a>
              <div>
                <%= GetFlex() %>
                </div>
              </div>
             
                </td>
                <!--R1.4: CR13: Extend Price List
                Added code to display the Special rate heading for City-->
                <% if (ContentDataAccess.ShowSpecialRates(CurrentPage.PageLink))
                   { %>
                 <td class="FlexOrMeeting">
                <div>
                 <a id="Special<%= GetCityID() %>" class="InfoIcon" onclick="showDesc(this,'SpecialColumn<%= GetCityID() %>')">&nbsp;&nbsp;&nbsp;<EPiServer:Translate ID="Translate5" Text="/Templates/Scanweb/Units/Static/PriceListCity/SpecialRate"  runat="server" /></a>
                </div>
                <div id="SpecialColumn<%= GetCityID() %>" class="Desc_Room">                 
               <a class="InfoIcon" onclick="hideDesc('SpecialColumn<%= GetCityID() %>')">&nbsp;&nbsp;&nbsp;<EPiServer:Translate ID="Translate6" Text="/Templates/Scanweb/Units/Static/PriceListCity/SpecialRate"  runat="server" /></a>
              <div>
                <%= GetSpecial() %>
                </div>
              </div>             
                </td>
                <% } %>
                
            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="PlaceHolderMeeting" runat="server">
                <td class="FlexOrMeeting"><EPiServer:Translate Text="/Templates/Scanweb/Units/Static/PriceListCity/FlexTime1"  runat="server" /> </td>
                <td class="FlexOrMeeting"><EPiServer:Translate ID="FlexTime2" Text="/Templates/Scanweb/Units/Static/PriceListCity/FlexTime2"  runat="server" /> </td>
            </asp:PlaceHolder>            
   </tr>
  
   
        <asp:PlaceHolder ID="HotelPlaceHolder" runat="server"> 
        </asp:PlaceHolder>
    </table>
   
</asp:PlaceHolder>
