//  Description					:   PriceListCountry                                      //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.CMS.code.Util.HotelOfferList;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Code behind of PriceListCountry control.
    /// </summary>
    public partial class PriceListCountry : EPiServer.UserControlBase
    {
        public OfferCountry country;
        public string PriceType;

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            foreach (OfferCity city in country.GetListOfCity())
            {
                Scandic.Scanweb.CMS.Templates.Units.Static.PriceListCity CityControl =
                    (Scandic.Scanweb.CMS.Templates.Units.Static.PriceListCity)
                    LoadControl("\\Templates\\Scanweb\\Units\\Static\\OfferPriceList\\PriceListCity.ascx");
                CityControl.City = city;
                CityControl.PriceType = PriceType;
                CityListPlaceHolder.Controls.Add(CityControl);
            }
        }

        /// <summary>
        /// Gets country name.
        /// </summary>
        /// <returns>country name.</returns>
        protected string GetCountryName()
        {
            return this.country.CountryName;
        }
    }
}