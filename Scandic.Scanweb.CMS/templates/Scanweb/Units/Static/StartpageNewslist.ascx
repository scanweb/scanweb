<%@ Control Language="C#" AutoEventWireup="False" CodeBehind="StartpageNewslist.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.StartpageNewslist" %>
<div id="boxcontainer">
    <div class="toprow">
        <div class="toprowleft"></div>
        <div class="toprowmiddle">
            <div class="toprowmiddleleft"><EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Units/Static/StartpageNewslist/Header" runat="server" /></div>
            <div class="toprowmiddleright">
                <div class="rightlistimage"><img src="~/Templates/Scanweb/Styles/Default/Images/white_arrow_right.gif" alt=" " /></div>
                <div><a href="<%= SpecialOffersURL() %>" title='<%= Translate("/navigation/readmore") %>'><EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Units/Static/StartpageNewslist/MoreSpecialOffers" runat="server" /></a></div>
            </div>
        </div>
        <div class="toprowright"></div>
    </div>
</div>
<div class="Contentboxcontainer">    
    <div id="Leftlist">    
        <EPiServer:MenuList id="menuList1" runat="server">
          
             <ItemTemplate>
                <div class="spl_LinkListItem">
                    <div class="<%# printalastitem(Container.CurrentPage) %>">
                        <EPiServer:Property ID="Property1" CssClass="spl_IconLink" PropertyName="PageLink" ToolTip='<%# Translate("/navigation/readmore") %>' runat="server" />
                    </div>
                </div>
            </ItemTemplate>
        </EPiServer:MenuList>  
    
    <asp:Label ID="flist" runat="server" />
    <asp:Label ID="slist" runat="server" />
    <asp:Label ID="nlist" runat="server" />
    <asp:Label ID="tlist" runat="server" />
    
    <EPiServer:PageList ID="temp" runat="server">
    <ItemTemplate></ItemTemplate>
    </EPiServer:PageList>
    
    <asp:Label ID="lastitemtext" runat="server" />
    
    </div>
    <div id="Rightlist">
       <EPiServer:MenuList id="menuList2" runat="server">
            <ItemTemplate>
                <div class="spl_LinkListItem">
                    <div class="spl_NotLastLink">
                        <EPiServer:Property ID="Property1" CssClass="spl_IconLink" PropertyName="PageLink" ToolTip='<%# Translate("/navigation/readmore") %>' runat="server" />
                    </div>
                </div>
            </ItemTemplate>
        </EPiServer:MenuList> 
    </div>
</div>












   
