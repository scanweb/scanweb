////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  HotelExpansionHotelList                                //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//---------------------------------------------------------------------------------------//
// Revision History			:                                                             //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// This class will have methods to load all hotels and methods for traversing next set and previous set of hotels.
    /// </summary>
    public partial class HotelExpansionHotelList : ScandicUserControlBase
    {
        /// <summary>
        /// This will load all control in Hotel List control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (DigitalPlatformSessionWrapper.ExpansionPageHotelList != null)
                {
                    List<HotelDestination> allHotelsList =
                        (List<HotelDestination>) DigitalPlatformSessionWrapper.ExpansionPageHotelList;
                }
            }
        }

        /// <summary>
        /// This will read country information from CMS and populate in country dropdownlist.
        /// </summary>
        /// <returns>collection of country</returns>
        public string CreateCountryDropdownList()
        {
            List<HotelDestination> allHotelsList = null;
            if (DigitalPlatformSessionWrapper.ExpansionPageHotelList != null)
                allHotelsList = (List<HotelDestination>) DigitalPlatformSessionWrapper.ExpansionPageHotelList;
            List<string> countryList = new List<string>();
            string countryCodeAndCountryName = string.Empty;
            if (allHotelsList != null)
            {
                if (allHotelsList.Count > 0)
                {
                    foreach (HotelDestination hotel in allHotelsList)
                    {
                        string countryName = ContentDataAccess.GetCountryNamebyCountryID(hotel.CountryCode);
                        if (!countryList.Contains(countryName + ";" + hotel.CountryCode))
                        {
                            countryList.Add(countryName + ";" + hotel.CountryCode);
                        }
                    }
                }
            }
            string country = @"<ul><li><a onclick=""filterHotels('')"">" +
                             WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/ExpansionPage/ShowALL") +
                             "</a></li>";
            try
            {
                if (countryList != null && countryList.Count > 0)
                {
                    countryList.Sort();
                    foreach (string countryCodeAndName in countryList)
                    {
                        country = country + @"<li><a onclick=""filterHotels('" +
                                  countryCodeAndName.Split(';')[1].ToString() + @"')"">" +
                                  countryCodeAndName.Split(';')[0].ToString() + "</a></li>";
                    }
                }
            }
            catch (Exception exp)
            {
            }
            return country;
        }
    }
}