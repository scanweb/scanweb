<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalInformation.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.AdditionalInformation" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>

    <asp:PlaceHolder ID="FacilityImgPH" runat="server">
   <Scanweb:SquaredCornerImage  ImagePropertyName="MainImage" 
                                 TopCssClass="RoundedCornersTop472" 
                                 ImageCssClass="RoundedCornersImage472" 
                                 BottomCssClass="RoundedCornersBottom472" ImageWidth="472" runat="server" />
    </asp:PlaceHolder>
 <div style="padding:20px;width:440px;">
   
    <h1>
   
        <EPiServer:Property  PropertyName="PageName" runat="server" /> 
        </h1>
        <h2>
          <EPiServer:Property  PropertyName="MainIntroXHTML" runat="server" />
        </h2>
            <EPiServer:Property  PropertyName="MainBody" runat="server" />
        </div>