<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MasterPage.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.MasterPage" %>

<EPiServer:MenuList runat="server" id="TopMenuCtrl" >
	<HeaderTemplate>
		<ul id="MasterPage">
	
	</HeaderTemplate>
	<ItemTemplate>
			<li class="unselected">
			<div id="menu_container">
			    <ul id="topmenu">
			        <li>
			            <EPiServer:Property  ID="epitopmenu" PropertyName="PageLink" runat="server"/>
			        <ul id="submenu">
			        <li class="fly">
			        <EPiServer:Property ID="episubmenu" PropertyName="PageLink" runat="server" />
			        </li>
			        </ul>
			        </li>
			    </ul>
			</li>
           </div>
	</ItemTemplate>
	<SelectedTemplate>
			<li class="selected"><EPiServer:Property ID="Property1" runat="server" PropertyName="PageLink" />
               
			</li>
	</SelectedTemplate>
	<FooterTemplate>
		</ul>
	</FooterTemplate>
</EPiServer:MenuList>

<EPiServer:PageTree ShowRootPage="false" runat="server" id="SubMenuCtrl" ExpandAll="true"  >
	<IndentTemplate>
		<ul>
	</IndentTemplate>
	
	<ItemHeaderTemplate>
			<li>
	</ItemHeaderTemplate>
		
	<ItemTemplate>
				<EPiServer:Property ID="Property3"  PropertyName="PageLink" title="<%#Container.CurrentPage.PageName%>" runat="server" />
	</ItemTemplate>
	
	<SelectedItemTemplate>
				<span class="selected"><%#Container.CurrentPage.PageName%></span>
	</SelectedItemTemplate>
	
	<ItemFooterTemplate>
			</li>
	</ItemFooterTemplate>
	
	<UnindentTemplate>
		</ul>
	</UnindentTemplate>
</EPiServer:PageTree>