<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchMeetingList.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.SearchMeetingList" %>
<%@ Register TagPrefix="Scanweb" TagName="PageList" Src="~/Templates/Scanweb/Units/Placeable/PageList.ascx" %>
<%--<EPiServer:PageList ID="SearchResultList" Paging="true" runat="server">--%>

<script type="text/javascript" language="JavaScript">

    var meetingSearchCurrentPage = 1;

    function MeetingSearchSelectPage(pageID, numberOfPages, numberOfHits) {

        meetingSearchCurrentPage = pageID;

        // Make current selected page bold
        for (var x = 1; x < (numberOfPages + 1); x++) {
            var currentPageItem = document.getElementById('MeetingSearchSelectPage' + x);
            currentPageItem.style.fontWeight = 'normal';
        }
        var currentPageItem = document.getElementById('MeetingSearchSelectPage' + pageID);
        currentPageItem.style.fontWeight = 'bold';

        // Hide or show previous/next page
        var previousArrow = document.getElementById('MeetingSearchPreviousArrow');
        var nextArrow = document.getElementById('MeetingSearchNextArrow');

        if (pageID == 1)
            previousArrow.style.display = 'none';
        else
            previousArrow.style.display = 'inline';

        if (pageID == numberOfPages)
            nextArrow.style.display = 'none';
        else
            nextArrow.style.display = 'inline';

        // Display the hits that belongs to the current page   
        for (var x = 1; (x < numberOfHits + 1); x++) {
            var currentPageItem = document.getElementById('MeetingSearchResult' + x);
            if ((x <= pageID * 10) && (x > (pageID - 1) * 10)) {
                currentPageItem.style.display = 'inline';
            }
            else {
                currentPageItem.style.display = 'none';
            }
        }
    }

    function MeetingSearchPreviousPage(numberOfPages, numberOfHits) {
        meetingSearchCurrentPage--;
        MeetingSearchSelectPage(meetingSearchCurrentPage, numberOfPages, numberOfHits);
    }

    function MeetingSearchNextPage(numberOfPages, numberOfHits) {
        meetingSearchCurrentPage++;
        MeetingSearchSelectPage(meetingSearchCurrentPage, numberOfPages, numberOfHits);
    }

    function MeetingSearchShowAll(numberOfItems) {
        document.getElementById('MeetingSearchShowAllContainer').style.display = 'none';
        document.getElementById('MeetingSearchPagingContainer').style.display = 'none';

        for (var x = 1; (x < numberOfItems + 1); x++) {
            var currentPageItem = document.getElementById('MeetingSearchResult' + x);
            currentPageItem.style.display = 'inline';
        }
    }
</script>

<asp:Repeater ID="SearchResultListRepeater" runat="server">
    <HeaderTemplate>
        <div class="MeetingSearchTop">
            <a target="_blank" class="DownloadIconLink" href='<%= GetMeetingGuideURL() %>'>
                <episerver:translate id="Translate3" text="/Templates/Scanweb/Units/Static/SearchMeetingList/DownloadMeetingGuide"
                    runat="server" />
            </a>
        </div>
    </HeaderTemplate>
    <ItemTemplate>
        <% CurrentCount++; %>
        <div id="MeetingSearchResult<%= CurrentCount.ToString() %>" style="display: <%= ((CurrentCount > 10) ? "none" : "inline") %>">
            <div class="hotelInfoCnt MeetingSearchWrapper">
                <div class="MeetingImageHolder">
                    <episerver:property id="SearchMeetingImageProperty" innerproperty="<%# GetImageFromImageVault((EPiServer.Core.PageData) Container.DataItem, 226) %>"
                        imagewidth="226" runat="server" alt="<%# GetAltText((EPiServer.Core.PageData) Container.DataItem) %>" />
                </div>
                <div class="MeetingSearchDesc">
                    <p class="cityCenterDist" id="">
                        <episerver:property propertyname="CityCenterDistance" runat="server" />
                        <span runat="server" id="cityCentreLabel"></span>
                    </p>
                    <h2 id="">
                        <a href='<%# GetHotelPageURL(((EPiServer.Core.PageData) Container.DataItem), "meetings") %>'
                            class="IconLink" target="_self">
                            <%# ((EPiServer.Core.PageData) Container.DataItem)["Heading"] as string ??
                                     ((EPiServer.Core.PageData) Container.DataItem).PageName %>
                        </a>
                    </h2>
                    <p class="meetingHotelAddr">
                        <%# (((EPiServer.Core.PageData)Container.DataItem)["StreetAddress"] as string) ?? String.Empty%>,
                        <%# (((EPiServer.Core.PageData)Container.DataItem)["PostalCity"] as string) ?? String.Empty %>
                    </p>
                    <div id="" class="meetingHotelDesc">
                        <episerver:property propertyname="HotelDescription" runat="server" />
                    </div>
                    <div class="MeetingRoomDetails">
                        <div class="meetingRoomDet fltLft">
                            <strong>
                                <episerver:translate id="NoMeetingRooms" text="/Templates/Scanweb/Units/Static/SearchMeetingList/NoMeetingRooms"
                                    runat="server" />
                            </strong>
                        </div>
                        <div class="fltLft">
                            <span id="numberOfMeetingRooms" runat="server"></span>
                        </div>
                        <div class="meetingRoomDet fltLft">
                            <strong>
                                <episerver:translate id="LargestMeetingRooms" text="/Templates/Scanweb/Units/Static/SearchMeetingList/LargestMeetingRoom"
                                    runat="server" />
                            </strong>
                        </div>
                        <div class="fltLft">
                            <span id="largestMeetingRoom" runat="server"></span><span>m<sup>2</sup></span></div>
                        <asp:Literal ID="ltlPageLinkID" runat="server" Visible="false" Text='<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>' />
                    </div>
                </div>
                <div class="MeetingCatWrapper">
                    <div class="meeting-cat-wrapper">
                        <div id="Div1" class="TwoColMeetingCat">
                            <ul class="meetingCatList" id="meetingCategoriesDiv" runat="server">
                                <asp:Repeater ID="meetingCategories" runat="server">
                                    <ItemTemplate>
                                        <li id="Li1" runat="server"><span class="green_tick"></span><span class="meetingCategory  toolTipMe"
                                            title="Sample Tooltip Text" id="meetingCategory" runat="server"></span><span>
                                                <episerver:translate id="from" text="/Templates/Scanweb/Units/Static/SearchMeetingList/From"
                                                    runat="server" />
                                            </span>&nbsp;<span id="price" runat="server"></span>&nbsp;<span id="currency" runat="server"></span>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                        <div id="divMeetingCatRightCol" runat="server" visible="false" class="TwoColMeetingCat fltLft">
                            <ul class="meetingCatList" id="meetingCategoriesRightDiv" runat="server">
                                <asp:Repeater ID="meetingCategoriesRight" runat="server">
                                    <ItemTemplate>
                                        <li><span class="green_tick"></span><span class="meetingCategory toolTipMe" id="meetingCategoryRight"
                                            runat="server"></span><span>
                                                <episerver:translate id="fromTextRight" text="/Templates/Scanweb/Units/Static/SearchMeetingList/From"
                                                    runat="server" />
                                            </span><span id="priceRight" runat="server"></span>&nbsp;<span id="currencyRight"
                                                runat="server"> </span></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                    </div>
                    <asp:CheckBox ID="cbSelectforRFP" class="select-proposal" runat="server" />
                </div>
            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>
<div id="MeetingSearchBottomPagination" runat="server" class="MeetingSearchBottom">
    <div id="MeetingSearchShowAllContainer">
        <asp:Literal runat="server" ID="SearchResultsShowAllHotels" />
    </div>
    <div id="MeetingSearchPagingContainer">
        <asp:Literal runat="server" ID="SearchResultPagingLiteral" />
    </div>
</div>
<br />
<br>
<br />
<%--</EPiServer:PageList>--%>
<div class="no-meeting-rooms">
    <asp:Literal ID="ltlNoMeetingRooms" runat="server"></asp:Literal>
</div>
<div class="actionBtn btnSendRFPs fltRt">
    <asp:Button ID="btnSendRFPs" Visible="false" runat="server" OnClick="btnSendRFPs_Click"
        CssClass="submit buttonInner" />
    <asp:Button ID="btnSendRt" Visible="false" runat="server" OnClick="btnSendRFPs_Click"
        CssClass="buttonRt scansprite noBorder" />
</div>
