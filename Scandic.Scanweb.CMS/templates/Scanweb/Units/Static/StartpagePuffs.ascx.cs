using System;
using EPiServer;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// A common footer for the website where links common for the whole site are listed. 
    /// </summary>
    public partial class StartpagePuffs : UserControlBase
    {
        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            SetFirstPuff();
            SetSecondPuff();
        }

        /// <summary>
        /// FirstPuffURL
        /// </summary>
        /// <returns>FirstPuffURL</returns>
        public string FirstPuffURL()
        {
            PageReference pageRef = CurrentPage["FirstPuffLink"] as PageReference;
            if (pageRef != null)
            {
                return Server.HtmlEncode(DataFactory.Instance.GetPage(pageRef).LinkURL);
            }
            return string.Empty;
        }

        /// <summary>
        /// SecondPuffURL
        /// </summary>
        /// <returns>SecondPuffURL</returns>
        public string SecondPuffURL()
        {
            PageReference pageRef = CurrentPage["FirstPuffLink"] as PageReference;
            if (pageRef != null)
            {
                return Server.HtmlEncode(DataFactory.Instance.GetPage(pageRef).LinkURL);
            }
            return string.Empty;
        }

        /// <summary>
        /// SetFirstPuff
        /// </summary>
        private void SetFirstPuff()
        {
            PageBase page = (PageBase) Page;
            PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET", "ImageStoreNET.ImageType");
            string strImage = page.CurrentPage["PuffImage1"] as string;

            if (!String.IsNullOrEmpty(strImage))
                pd.Value = strImage;

            FirstPuffImage.InnerProperty = pd;
            FirstPuffImage.Visible = true;
        }

        /// <summary>
        /// SetSecondPuff
        /// </summary>
        private void SetSecondPuff()
        {
            PageBase page = (PageBase) Page;
            PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET", "ImageStoreNET.ImageType");
            string strImage = page.CurrentPage["PuffImage2"] as string;

            if (!String.IsNullOrEmpty(strImage))
                pd.Value = strImage;

            SecondPuff.InnerProperty = pd;
            SecondPuff.Visible = true;
        }
    }
}