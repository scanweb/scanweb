//  Description					: InfoBox                                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using EPiServer;

namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// Info Box
    /// </summary>
    public partial class InfoBox : UserControlBase
    {
        private string bodyPropertyName;

        /// <summary>
        /// Gets/Sets BodyPropertyName
        /// </summary>
        public string BodyPropertyName
        {
            get { return bodyPropertyName; }
            set { bodyPropertyName = value; }
        }

        /// <summary>
        /// Gets/Sets HeaderPropertyName 
        /// </summary>
        public string HeaderPropertyName { get; set; }

        /// <summary>
        /// Gets Header
        /// </summary>
        public string Header
        {
            get { return CurrentPage[HeaderPropertyName] as string ?? string.Empty; }
        }

        /// <summary>
        /// On Load
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            HeaderPlaceHolder.Visible = (Header.Length > 0);
            BodyProperty.PageLink = CurrentPage.PageLink;
            BodyProperty.PropertyName = bodyPropertyName;
            BodyPlaceHolder.Visible = !string.IsNullOrEmpty(CurrentPage[bodyPropertyName] as string);
            this.Visible = ((Header.Length > 0) || (BodyPlaceHolder.Visible));
        }
    }
}