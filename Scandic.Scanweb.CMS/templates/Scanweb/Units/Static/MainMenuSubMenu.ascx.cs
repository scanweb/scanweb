//  Description					:   MainMenuSubMenu                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI.WebControls;
using EPiServer.Core;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.CMS.Util.Filters;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// Code behind of MainMenuSubMenu control.
    /// </summary>
    public partial class MainMenuSubMenu : EPiServer.UserControlBase
    {
        private PageReference mainMenuSelectedPageLink;
        private int numberOfItems = 0;
        private const int pixelsPerItem = 30;
        private PageData pageData;

        /// <summary>
        /// Gets/Sets PageLink
        /// </summary>
        public PageReference MainMenuSelectedPageLink
        {
            get { return mainMenuSelectedPageLink; }
            set { mainMenuSelectedPageLink = value; }
        }

        /// <summary>
        /// Gets/Sets PageLink
        /// </summary>
        public PageData PageData
        {
            get { return pageData; }
            set { pageData = value; }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            rptMainMenu.ItemDataBound += new RepeaterItemEventHandler(rptMainMenu_ItemDataBound);
            rptMainMenu.ItemCommand += new RepeaterCommandEventHandler(rptMainMenu_ItemCommand);

        }

        void rptMainMenu_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("MainMenuSelectedPageLink", StringComparison.InvariantCultureIgnoreCase))
            {
                CmsUtil.RedirectAndSetFlag(e.CommandArgument.ToString());
            }
        }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {  
            if (MainMenuSelectedPageLink != null)
            {
              PageDataCollection subMenuCollection  = ContentDataAccessManager.GetChildren(MainMenuSelectedPageLink);
              if (subMenuCollection != null)
              {
                  GuestProgramPageFilter guestProgramPageFilter = new GuestProgramPageFilter();
                  subMenuCollection = guestProgramPageFilter.Filter(subMenuCollection);
                  PageCollection visiblePages = new PageCollection();
                  rptMainMenu.DataSource = visiblePages.FilterInvisible(subMenuCollection);
                  rptMainMenu.DataSource = subMenuCollection;
                  rptMainMenu.DataBind();
              }
            }             
        }

        /// <summary>
        /// binds data to each row of repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptMainMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                bool isPageSecured = false;
                PageData pageData = e.Item.DataItem as PageData;
                HyperLink mainMenuItemLink = e.Item.FindControl("MainMenuItemLink") as HyperLink;
                if ((pageData != null) && (mainMenuItemLink != null))
                {
                    mainMenuItemLink.Text = pageData.PageName;                   
                
                    string pageExternalURL = WebUtil.GetExternalUrl(pageData.LinkURL);
                    if (!string.IsNullOrEmpty(pageExternalURL))
                    {
                        isPageSecured = (pageData["Secured"] != null) ? Convert.ToBoolean(pageData["Secured"]) : false;
                        mainMenuItemLink.NavigateUrl =  WebUtil.GetCompletePageUrl(pageExternalURL, isPageSecured);                     
                    }
                }
            }
        }

      
        /// <summary>
        /// Gets Iframe string
        /// </summary>
        /// <returns>Iframe string</returns>
        protected string GetIFrameString()
        {
            return
                string.Format(
                    "<!--[if lte IE 6.5]><iframe height=\"{0}\" src=\"/Templates/Scanweb/Pages/Empty.htm\"></iframe><![endif]-->",
                    pixelsPerItem*numberOfItems);
        }

        /// <summary>
        /// Set the flag to true. This flag is used in 'LeftMenu.ascx'
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Button_Clicked(object source, EventArgs e)
        {
            CmsUtil.RedirectAndSetFlag(source as LinkButton);
        }
    }
}
