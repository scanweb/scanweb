<%@ Control Language="C#" EnableViewState="false" AutoEventWireup="true" Codebehind="SubMenu.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.SubMenu" %>

<asp:Repeater ID="rptSubMenu" runat="server">
<HeaderTemplate>
   <!-- SubMenu START -->        
        <div id="SubMenuNavigation">
            <div id="SubMenuNavigationTab">
                <ul>
</HeaderTemplate>
<ItemTemplate><li id="subMenuItemContainerLink" runat="server"> 
<asp:HyperLink ID="subMenuItem" runat="server"></asp:HyperLink>
</li></ItemTemplate>
<FooterTemplate>
 </ul>        
            </div>
        </div>
        <!-- SubMenu END -->
</FooterTemplate>

</asp:Repeater>