<%@ Control Language="C#" AutoEventWireup="False" CodeBehind="StartpagePromotion.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.StartpagePromotion" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<div class="Mainoffercontainer">
  
    <div id="PromotionImage">
        
        <script type="text/javascript" src="/Templates/Scanweb/Javascript/swfobject.js?v=<%=CmsUtil.GetJSVersion()%>"></script>
		
         <div id="StartPageFlashContent">
             <EPiServer:Property PropertyName="AlternativeFlashArea" runat="server" />
         </div>
         
            <% if (CurrentPage["EmbeddedFlashURL"] != null)
               { %>
         <script type="text/javascript">
              var so = new SWFObject('<%= CurrentPage["EmbeddedFlashURL"] %>', 'scandicpromotion', '<%= CurrentPage["EmbeddedFlashWidth"] %>', '<%= CurrentPage["EmbeddedFlashHeight"] %>', '<%= CurrentPage["EmbeddedFlashMinVersion"] %>', '#FFFFFF');
              so.addParam("wmode", "transparent");
              so.write("StartPageFlashContent");
          </script>
            <% } %>
      
        <div id="StartPageMainOffer">
            
            <%--<EPiServer:Property ID="PromoBody" runat="server" PropertyName="MainOfferBody" /> --%>  
            <div id="PromoBody" runat="server"></div>  
            <h2  id="PriceRate" class ="priceRate" runat="server">  
            <asp:Literal ID="PriceText" runat="server"></asp:Literal>
            </h2>
            <!--  artf862568 | UAT 1.5.1| Release 1.5.2 |Start page main promo area Link -->     
            <div class="bottomLinks"><a class="IconLink" href="<%= GetURL() %>" onclick="<%= GetTrackingFunction() %>" title='<%= Translate("/navigation/readmore")%>'><%= CurrentPage["ReadMoreLink"] ?? string.Empty %></a></div>
        </div>
    </div>   
    
</div>

