//  Description					: SubMenu                                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.CMS.Util.Filters;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Util;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using System.Configuration;
using Scandic.Scanweb.CMS.DataAccessLayer;


namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// Code behind of SubMenu control.
    /// </summary>
    public partial class SubMenu : UserControlBase
    {
        /// <summary>
        /// Gets or sets the data source for this control
        /// </summary>
        public Repeater SubMenuList { get { return rptSubMenu; } }

         //<summary>
         //Gets/Sets SubMenuList
         //</summary>
        public Repeater MenuList
        {
            get;
            set;
        }

        public PageData SubmenuList
        {
            get;
            set;
        }

        public PageReference MainMenuSelectedPage
        {
            get;
            set;
        }


        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            rptSubMenu.ItemDataBound += new RepeaterItemEventHandler(rptSubMenu_ItemDataBound);

            
            if (MainMenuSelectedPage == null)
            {
                return;
            }    

            PageDataCollection subMenuCollection = new PageDataCollection();
            if (MainMenuSelectedPage != null)
            {
                subMenuCollection = ContentDataAccessManager.GetChildren(MainMenuSelectedPage);            
                GuestProgramPageFilter guestProgramPageFilter = new GuestProgramPageFilter();
                subMenuCollection = guestProgramPageFilter.Filter(subMenuCollection);
                PageCollection visiblePages = new PageCollection();
                var dataSource = visiblePages.FilterInvisible(subMenuCollection);

                //Filter country & city pages from submenu.
                dataSource = FilterCountryCityPages(dataSource);
                if (dataSource != null && dataSource.Count > 0)
                {
                    rptSubMenu.DataSource = dataSource;
                    rptSubMenu.DataBind();
                }
                else
                {
                    this.Visible = false;
                }
            }
        }

        private PageDataCollection FilterCountryCityPages(PageDataCollection subMenuPages)
        {
            PageDataCollection allPages = subMenuPages;
            if ((allPages != null) && (allPages.Count > 0))
            {

                PageData currPage;

                int CountryPageTypeID = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.CountryPageTypeID]);
                int CityPageTypeID = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.CityPageTypeID]);
                for (int p = 0; p < allPages.Count; p++)
                {
                    currPage = allPages[p];
                    if (currPage.PageTypeID == CountryPageTypeID || currPage.PageTypeID == CityPageTypeID)
                    {
                        allPages.RemoveAt(p);
                        p--;
                    }
                }
            }
            return allPages;
        }

        /// <summary>
        ///  Binds data to each row of repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptSubMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                bool isPageSecured = false;
                PageData pageData = e.Item.DataItem as PageData;
                HyperLink subMenuItem = e.Item.FindControl("subMenuItem") as HyperLink;
                if ((pageData != null) && (subMenuItem != null))
                {
                   string pageExternalURL = string.Empty;
                    subMenuItem.Text = pageData.PageName;
                    if (pageData["LinkToAnotherPage"] != null)
                        pageExternalURL = GlobalUtil.GetUrlToPage(pageData["LinkToAnotherPage"] as PageReference);
                    else
                        pageExternalURL = WebUtil.GetExternalUrl(pageData.LinkURL);                  
                    if (!string.IsNullOrEmpty(pageExternalURL))
                    {
                        isPageSecured = (pageData["Secured"] != null) ? Convert.ToBoolean(pageData["Secured"]) : false;
                        subMenuItem.NavigateUrl =  WebUtil.GetCompletePageUrl(pageExternalURL, isPageSecured);
                    }

                    HtmlGenericControl subMenuItemContainerLink = e.Item.FindControl("subMenuItemContainerLink") as HtmlGenericControl;
                   
                    var style = "Tab";
                    if (LastVisitedPageSessionWrapper.SelectedSubMenuPageName != null &&
                        string.Equals(LastVisitedPageSessionWrapper.SelectedSubMenuPageName, pageData.PageName))
                    {
                        style = "ActiveTab";
                    }

                    subMenuItemContainerLink.Attributes.Add("class", style);
                }
            }
        }

        ///// <summary>
        ///// Set the flag to true. This flag is used in 'LeftMenu.ascx'
        ///// </summary>
        ///// <param name="source">The source of the event.</param>
        ///// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        //protected void Button_Clicked(object source, EventArgs e)
        //{
        //    CmsUtil.RedirectAndSetFlag(source as LinkButton);
        //}
    }
}
