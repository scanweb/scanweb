﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DestinationLandingPageGoogleMapIframe.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.DestinationLandingPageGoogleMapIframe" %>

<div id="DestinationLandingPageGoogleMap">
   <iframe id="DestinationLandingPageMapIframe"  width="100%" height="376px" scrolling="no" frameborder="0"></iframe>
</div>