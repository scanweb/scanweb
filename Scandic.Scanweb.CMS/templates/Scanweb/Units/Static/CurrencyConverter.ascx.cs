//  Description					: CurrencyConverter                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespaces
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Caching;
using System.Web.UI.WebControls;
using System.Xml;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Core.Core;

#endregion

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// CurrencyConverter
    /// </summary>
    public partial class CurrencyConverter : EPiServer.UserControlBase
    {
        /// <summary>
        /// Cache constant for storing currency values
        /// </summary>
        private const string CURRENCY_VALUES_CACHE_KEY = "BECURRENCY_VALUES";

        /// <summary>
        /// Object used for file locking
        /// </summary>
        private object fileWriteLock = new object();

        /// <summary>
        /// Object used for cache locking
        /// </summary>
        private object cacheWriteLock = new object();

        /// <summary>
        /// Event Handler for Page Load event
        /// </summary>
        /// <param name="sender">
        /// Sender of the event
        /// </param>
        /// <param name="args">
        /// Arguments for the event
        /// </param>
        protected void Page_Load(object sender, EventArgs args)
        {
            if (!Page.IsPostBack)
            {
                PopulateCurrencies();
            }
        }

        /// <summary>
        /// Event Handler for Convert button click
        /// </summary>
        /// <param name="sender">
        /// Sender of the object
        /// </param>
        /// <param name="args">
        /// Arguments for the event
        /// </param>
        protected void btnConvertCurrency_Click(object sender, EventArgs args)
        {
            if (IsNumber())
            {
                string cacheKey = CURRENCY_VALUES_CACHE_KEY;
                OrderedDictionary currencyValuesMap = ScanwebCacheManager.Instance.LookInCache<OrderedDictionary>(cacheKey);
                if (currencyValuesMap == null)
                {
                    LoadCurrencyValues();
                    currencyValuesMap = ScanwebCacheManager.Instance.LookInCache<OrderedDictionary>(cacheKey);
                }
                if (currencyValuesMap != null)
                {
                    string sourceCurrencyCode = ddlSourceCurrency.SelectedValue;
                    string destCurrencyCode = ddlDestinationCurrency.SelectedValue;
                    double srcCurrencyConversionValue = 0.0;
                    string strSrcCurrencyConversionValue = currencyValuesMap[sourceCurrencyCode].ToString();
                    if (strSrcCurrencyConversionValue != null)
                    {
                        double.TryParse(strSrcCurrencyConversionValue, out srcCurrencyConversionValue);
                    }
                    double destCurrencyConversionValue = 0.0;
                    string destSrcCurrencyConversionValue = currencyValuesMap[destCurrencyCode].ToString();
                    if (destSrcCurrencyConversionValue != null)
                    {
                        double.TryParse(destSrcCurrencyConversionValue, out destCurrencyConversionValue);
                    }
                    double change = 0.0;
                    double.TryParse(txtCurrencyChange.Text.ToString(), out change);
                    double totalConversionValue = (1/srcCurrencyConversionValue)*destCurrencyConversionValue*change;
                    totalConversionValue = double.Parse(totalConversionValue.ToString("####0.000000"));
                    lblErrorMessage.Visible = false;
                    lblConvertedAmount.Visible = true;
                    lblConvertedValue.Visible = true;
                    lblConvertedAmount.Text =
                        WebUtil.GetTranslatedText(TranslatedTextConstansts.CURRENCY_CONVERTER_AMOUNT);
                    lblConvertedValue.Text = totalConversionValue.ToString() + AppConstants.SPACE +
                                             destCurrencyCode.ToString();
                }
            }
            else
            {
                lblConvertedValue.Visible = false;
                lblConvertedAmount.Visible = false;
                lblErrorMessage.Visible = true;
                lblErrorMessage.Text =
                    WebUtil.GetTranslatedText(TranslatedTextConstansts.CURRENCY_CONVERTER_ENTER_VALID_AMOUNT);
            }
        }

        /// <summary>
        /// Check if Number
        /// </summary>
        /// <returns>
        /// True if Numberic, else false
        /// </returns>
        private bool IsNumber()
        {
            bool result = false;
            double currencyValue = 0;
            bool isNum = Double.TryParse(txtCurrencyChange.Text, out currencyValue);
            if (isNum)
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        /// Populate the list of supported currecies and their conversion rates against the base currency.
        /// </summary>
        private void PopulateCurrencies()
        {
            PopulateCurrencyList();       
            LoadCurrencyValues();
            btnConvert.Text = WebUtil.GetTranslatedText("/bookingengine/booking/currencyConverter/ButtonText");
        }

        /// <summary>
        /// Load the currency list
        /// </summary>
        private void PopulateCurrencyList()
        {
            OrderedDictionary currencyCodesMap = DropDownService.GetCurrencyCodes();
            if (currencyCodesMap != null)
            {
                ddlSourceCurrency.Items.Clear();
                foreach (string key in currencyCodesMap.Keys)
                {
                    ddlSourceCurrency.Items.Add(new ListItem(currencyCodesMap[key].ToString(), key));
                }
                ddlDestinationCurrency.Items.Clear();
                foreach (string key in currencyCodesMap.Keys)
                {
                    ddlDestinationCurrency.Items.Add(new ListItem(currencyCodesMap[key].ToString(), key));
                }
            }
        }

        /// <summary>
        /// Load the Currency values
        /// </summary>
        private void LoadCurrencyValues()
        {
            AppLogger.LogInfoMessage("In CurrencyConverter\\LoadCurrencyValues()");        
            OrderedDictionary currencyValuesMap =  
                ScanwebCacheManager.Instance.LookInCache<OrderedDictionary>(CURRENCY_VALUES_CACHE_KEY);
            if (currencyValuesMap == null)
            {
                AppLogger.LogInfoMessage(
                    string.Format("Currency Values Map {0} is Empty: Fetching currency values Map from XML",
                                  CURRENCY_VALUES_CACHE_KEY));
                string valuesFileName = GetCurrencyValuesFileName();
                if (File.Exists(valuesFileName))
                {
                    CheckCurrencyConversionExpiryDate(valuesFileName);
                    XmlDocument valuesXmlDoc = new XmlDocument();
                    valuesXmlDoc.Load(valuesFileName);

                    XmlNodeList xmlCurrencyValueList = valuesXmlDoc.SelectNodes("Currency/CurrencyValuesList");
                    if ((xmlCurrencyValueList != null) && (xmlCurrencyValueList.Count > 0))
                    {
                        XmlNodeList valuesXMLList = xmlCurrencyValueList[0].ChildNodes;
                        currencyValuesMap = new OrderedDictionary();
                        for (int currencyCount = 0; currencyCount < valuesXMLList.Count; currencyCount++)
                        {
                            string srcCurrencyCode = valuesXMLList[currencyCount].Attributes["SourceCode"].Value;
                            string destCurrencyCode = valuesXMLList[currencyCount].Attributes["DestinationCode"].Value;
                            double currencyValue = 0.0;
                            if (double.TryParse(valuesXMLList[currencyCount].InnerText, out currencyValue))
                            {
                                currencyValuesMap.Add(destCurrencyCode, currencyValue);
                            }
                        }
                    }

                    if (currencyValuesMap == null)
                    {
                        FetchAllCurrenciesConversionRates();
                    }
                    else
                    {
                        XmlNode expiryNode = valuesXmlDoc.SelectSingleNode("Currency/ExpiryDate");
                        string expiryDate = string.Empty;
                        if (expiryNode != null)
                        {
                            expiryDate = expiryNode.InnerText;
                        }  
                        StoreInCache(currencyValuesMap, expiryDate);
                    }
                }
                else
                {
                    FetchAllCurrenciesConversionRates();
                }
            }
            else
            {
                AppLogger.LogInfoMessage(string.Format("Currency Values Map {0} is retrieved from the cache",
                                                       CURRENCY_VALUES_CACHE_KEY));
            }
        }

        /// <summary>
        /// Fetch the currencies from Opera
        /// </summary>
        private void FetchAllCurrenciesConversionRates()
        {
            string baseCurrencyCode = AppConstants.BASE_CURRENCY_CODE;
            double baseCurrencyConversionChange = AppConstants.BASE_CURRENCY_CONVERSION;

            OrderedDictionary currencyCodeMap = DropDownService.GetCurrencyCodes();

            InformationController informationController = new InformationController();
            OrderedDictionary currencyValueList = new OrderedDictionary();
            foreach (string key in currencyCodeMap.Keys)
            {
                double converterValue = informationController.GetCurrency(baseCurrencyCode, key,
                                                                          baseCurrencyConversionChange);
                currencyValueList.Add(key, converterValue);
            }
            StoreInFile(currencyValueList);
        }

        /// <summary>
        /// Store the Currency values list into the file
        /// </summary>
        /// <param name="currencyValueList">
        /// Dictionary of Currency values list
        /// </param>
        private void StoreInFile(OrderedDictionary currencyValueList)
        {
            XmlDocument currencyDocument = new XmlDocument();
            XmlDeclaration declaration = currencyDocument.CreateXmlDeclaration("1.0", "us-ascii", "yes");
            currencyDocument.AppendChild(declaration);
            XmlElement rootNode = currencyDocument.CreateElement("Currency");
            currencyDocument.AppendChild(rootNode);
            XmlElement expiryDateNode = currencyDocument.CreateElement("ExpiryDate");
            string expiryDate = DateTime.Today.AddDays(7).ToString("dd/MM/yyyy");
            expiryDateNode.InnerText = expiryDate;
            rootNode.AppendChild(expiryDateNode);
            XmlElement currencyValueListNode = currencyDocument.CreateElement("CurrencyValuesList");
            rootNode.AppendChild(currencyValueListNode);

            string baseCurrencyCode = AppConstants.BASE_CURRENCY_CODE;
            foreach (string key in currencyValueList.Keys)
            {
                XmlElement element = currencyDocument.CreateElement("CurrencyValue");
                element.SetAttribute("SourceCode", baseCurrencyCode);
                element.SetAttribute("DestinationCode", key);
                element.InnerText = currencyValueList[key].ToString();
                currencyValueListNode.AppendChild(element);
            }
            string currencyFileName = GetCurrencyValuesFileName();
            lock (fileWriteLock)
            {
                XmlWriter xmlWriter = XmlTextWriter.Create(currencyFileName);
                currencyDocument.WriteTo(xmlWriter);
                xmlWriter.Close();
            }         
            StoreInCache(currencyValueList, expiryDate);
        }

        /// <summary>
        /// Store the object into the cache
        /// </summary>
        /// <param name="objectToStore">
        /// Object to Store
        /// </param>
        /// <param name="strExpiryDate">
        /// Expiration Date of the Cache
        /// </param>
        private void StoreInCache(object objectToStore, string strExpiryDate)
        {
            if (objectToStore != null)
            {           
                CacheDependency cacheDependency = new CacheDependency(GetCurrencyValuesFileName());
                if (strExpiryDate != string.Empty)
                {
                    DateTime expiryDate = DateUtil.StringToDDMMYYYDate(strExpiryDate);
                    if (DateTime.Today.CompareTo(expiryDate) < 0)
                    {
                        lock (cacheWriteLock)
                        {
                            ScanwebCacheManager.Instance.AddToCache(CURRENCY_VALUES_CACHE_KEY, objectToStore,
                                                                    GetCurrencyValuesFileName(), expiryDate);
                        }
                    }
                }
                else
                {
                    lock (cacheWriteLock)
                    {
                        ScanwebCacheManager.Instance.AddToCache(CURRENCY_VALUES_CACHE_KEY, objectToStore, GetCurrencyValuesFileName());
                    }
                }
            }
        }

        /// <summary>
        /// Check the Currency Conversion Expirationn Date
        /// </summary>
        /// <param name="valuesFileName">
        /// File name of the currency conversion values
        /// </param>
        private void CheckCurrencyConversionExpiryDate(string valuesFileName)
        {
            XmlDocument valueXmlDoc = new XmlDocument();
            valueXmlDoc.Load(valuesFileName);
            XmlNode expiryNode = valueXmlDoc.SelectSingleNode("Currency/ExpiryDate");

            if (expiryNode != null)
            {
                DateTime expiryDate = DateUtil.StringToDDMMYYYDate(expiryNode.InnerText);
                if (expiryDate.CompareTo(DateTime.Today) < 0)
                {
                    try
                    {
                        File.Delete(valuesFileName);
                    }
                    catch (Exception)
                    {
                        AppLogger.LogInfoMessage("Fails to delete the currency values file: " + valuesFileName);
                    }
                    FetchAllCurrenciesConversionRates();
                }
            }
        }

        /// <summary>
        /// Get the Currency values file name
        /// </summary>
        /// <returns>Currency values file name</returns>
        private string GetCurrencyValuesFileName()
        {
            string currencyFileName =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            currencyFileName = currencyFileName.Substring(0, currencyFileName.LastIndexOf("\\"));

            currencyFileName += "\\lang\\dropdowns\\" + AppConstants.CURRENCYVALUES_FILENAME +
                                AppConstants.CODE_FILE_EXTN;

            if (currencyFileName.StartsWith("file:\\"))
            {
                currencyFileName = currencyFileName.Remove(0, 6);
            }
            return currencyFileName;
        }
    }
}