<%@ Control Language="C#" AutoEventWireup="true" Codebehind="MoreImagesPopUp.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.MoreImagesPopUp" %>
<div class="PopUp">
    <div class="PopUpTop">
        <div class="ClosePopUp">
            <a class="CloseWindowPic" onclick="self.close();">
                <EPiServer:Translate ID="Translate5" Text="/Templates/Scanweb/Units/Static/PopUp/CloseLink"
                    runat="server" />
            </a>
        </div>
    </div>
</div>
<div class="slideShow">
    <div class="PopUpHeader">
		<div class="HeaderLeft"> </div>
        <h1>
            <%# HotelPage["Heading"] as string ?? String.Empty %> | <span class="sectionName"><EPiServer:Translate ID="Translate8" Text="/Templates/Scanweb/Units/Static/MoreImagesPopUp/Images"
                            runat="server" /></span>
        </h1>
		<div class="LinkListItem">
        <asp:PlaceHolder  id ="HideUnhide" Visible = '<%# Show360Link() %>' runat="server" >
            <asp:PlaceHolder ID="Image360LinkPH" Visible="false" runat="server">
                    
                        <div class="LastLink">
                            <a class="IconLink" href='<%= GetImage360URL() %>'>
                                <asp:Literal ID="Image360LinkLiteral" runat="server" />
                            </a>
                        </div>
                    
            </asp:PlaceHolder>
        </asp:PlaceHolder>
        </div>
        </div>
		<div id="MoreImagesPopUp">
		<asp:PlaceHolder ID="ImagesPopUpPH" Visible="false" runat="server">

		<div class="PopUpHeaderRight">
		
		</div>
        <div id="MoreImagesPopUpLeftColumn">
            <div class="MoreImagesPopUpImageThumbRow">
                <asp:PlaceHolder ID="GeneralImage1PH" runat="server">
                    <h2>
                        <EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Units/Static/MoreImagesPopUp/General"
                            runat="server" />
                    </h2>
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="GeneralLinkButton1" runat="server" CommandArgument="GeneralImage1">
                            <EPiServer:Property ID="GeneralProperty1" InnerProperty='<%# GetPopUpImageFromImageVault("GeneralImage1", "GeneralImage1PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("GeneralImage1") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="GeneralImage2PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="GeneralLinkButton2" runat="server" CommandArgument="GeneralImage2">
                            <EPiServer:Property ID="GeneralProperty2" InnerProperty='<%# GetPopUpImageFromImageVault("GeneralImage2", "GeneralImage2PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("GeneralImage2") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="GeneralImage3PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="GeneralLinkButton3" runat="server" CommandArgument="GeneralImage3">
                            <EPiServer:Property ID="GeneralProperty3" InnerProperty='<%# GetPopUpImageFromImageVault("GeneralImage3", "GeneralImage3PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("GeneralImage3") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="GeneralImage4PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="GeneralLinkButton4" runat="server" CommandArgument="GeneralImage4">
                            <EPiServer:Property ID="GeneralProperty4" InnerProperty='<%# GetPopUpImageFromImageVault("GeneralImage4", "GeneralImage4PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("GeneralImage4") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="GeneralImage5PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="GeneralLinkButton5" runat="server" CommandArgument="GeneralImage5">
                            <EPiServer:Property ID="GeneralProperty5" InnerProperty='<%# GetPopUpImageFromImageVault("GeneralImage5", "GeneralImage5PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("GeneralImage5") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="GeneralImage6PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="GeneralLinkButton6" runat="server" CommandArgument="GeneralImage6">
                            <EPiServer:Property ID="GeneralProperty6" InnerProperty='<%# GetPopUpImageFromImageVault("GeneralImage6", "GeneralImage6PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("GeneralImage6") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="GeneralImage7PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="GeneralLinkButton7" runat="server" CommandArgument="GeneralImage7">
                            <EPiServer:Property ID="GeneralProperty7" InnerProperty='<%# GetPopUpImageFromImageVault("GeneralImage7", "GeneralImage7PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("GeneralImage7") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="GeneralImage8PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="GeneralLinkButton8" runat="server" CommandArgument="GeneralImage8">
                            <EPiServer:Property ID="GeneralProperty8" InnerProperty='<%# GetPopUpImageFromImageVault("GeneralImage8", "GeneralImage8PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("GeneralImage8") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="GeneralImage9PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="GeneralLinkButton9" runat="server" CommandArgument="GeneralImage9">
                            <EPiServer:Property ID="GeneralProperty9" InnerProperty='<%# GetPopUpImageFromImageVault("GeneralImage9", "GeneralImage9PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("GeneralImage9") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="GeneralImage10PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="GeneralLinkButton10" runat="server" CommandArgument="GeneralImage10">
                            <EPiServer:Property ID="GeneralProperty10" InnerProperty='<%# GetPopUpImageFromImageVault("GeneralImage10", "GeneralImage10PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("GeneralImage10") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
            </div>
            <div class="MoreImagesPopUpImageThumbRow">
                <asp:PlaceHolder ID="RoomImage1PH" runat="server">
                    <h2>
                        <EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Units/Static/MoreImagesPopUp/OurRooms"
                            runat="server" />
                    </h2>
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RoomLinkButton1" runat="server" CommandArgument="RoomImage1">
                            <EPiServer:Property ID="RoomProperty1" InnerProperty='<%# GetPopUpImageFromImageVault("RoomImage1", "RoomImage1PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RoomImage1") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RoomImage2PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RoomLinkButton2" runat="server" CommandArgument="RoomImage2">
                            <EPiServer:Property ID="RoomProperty2" InnerProperty='<%# GetPopUpImageFromImageVault("RoomImage2", "RoomImage2PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RoomImage2") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RoomImage3PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RoomLinkButton3" runat="server" CommandArgument="RoomImage3">
                            <EPiServer:Property ID="RoomProperty3" InnerProperty='<%# GetPopUpImageFromImageVault("RoomImage3", "RoomImage3PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RoomImage3") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RoomImage4PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RoomLinkButton4" runat="server" CommandArgument="RoomImage4">
                            <EPiServer:Property ID="RoomProperty4" InnerProperty='<%# GetPopUpImageFromImageVault("RoomImage4", "RoomImage4PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RoomImage4") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RoomImage5PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RoomLinkButton5" runat="server" CommandArgument="RoomImage5">
                            <EPiServer:Property ID="RoomProperty5" InnerProperty='<%# GetPopUpImageFromImageVault("RoomImage5", "RoomImage5PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RoomImage5") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RoomImage6PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RoomLinkButton6" runat="server" CommandArgument="RoomImage6">
                            <EPiServer:Property ID="RoomProperty6" InnerProperty='<%# GetPopUpImageFromImageVault("RoomImage6", "RoomImage6PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RoomImage6") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RoomImage7PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RoomLinkButton7" runat="server" CommandArgument="RoomImage7">
                            <EPiServer:Property ID="RoomProperty7" InnerProperty='<%# GetPopUpImageFromImageVault("RoomImage7", "RoomImage7PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RoomImage7") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                 <asp:PlaceHolder ID="RoomImage8PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RoomLinkButton8" runat="server" CommandArgument="RoomImage8">
                            <EPiServer:Property ID="RoomProperty8" InnerProperty='<%# GetPopUpImageFromImageVault("RoomImage8", "RoomImage8PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RoomImage8") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                 <asp:PlaceHolder ID="RoomImage9PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RoomLinkButton9" runat="server" CommandArgument="RoomImage9">
                            <EPiServer:Property ID="RoomProperty9" InnerProperty='<%# GetPopUpImageFromImageVault("RoomImage9", "RoomImage9PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RoomImage9") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                 <asp:PlaceHolder ID="RoomImage10PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RoomLinkButton10" runat="server" CommandArgument="RoomImage10">
                            <EPiServer:Property ID="RoomProperty10" InnerProperty='<%# GetPopUpImageFromImageVault("RoomImage10", "RoomImage10PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RoomImage10") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
            </div>
            <div class="MoreImagesPopUpImageThumbRow">
                <asp:PlaceHolder ID="RestImage1PH" runat="server">
                    <h2>
                        <EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Units/Static/MoreImagesPopUp/RestaurantsAndBars"
                            runat="server" />
                    </h2>
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RestLinkButton1" runat="server" CommandArgument="RestBarImage1">
                            <EPiServer:Property ID="RestProperty1" InnerProperty='<%# GetPopUpImageFromImageVault("RestBarImage1", "RestImage1PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RestBarImage1") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RestImage2PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RestLinkButton2" runat="server" CommandArgument="RestBarImage2">
                            <EPiServer:Property ID="RestProperty2" InnerProperty='<%# GetPopUpImageFromImageVault("RestBarImage2", "RestImage2PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RestBarImage2") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RestImage3PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RestLinkButton3" runat="server" CommandArgument="RestBarImage3">
                            <EPiServer:Property ID="RestProperty3" InnerProperty='<%# GetPopUpImageFromImageVault("RestBarImage3", "RestImage3PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RestBarImage3") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RestImage4PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RestLinkButton4" runat="server" CommandArgument="RestBarImage4">
                            <EPiServer:Property ID="RestProperty4" InnerProperty='<%# GetPopUpImageFromImageVault("RestBarImage4", "RestImage4PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RestBarImage4") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                 <asp:PlaceHolder ID="RestImage5PH" runat="server">
                     <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RestLinkButton5" runat="server" CommandArgument="RestBarImage5">
                            <EPiServer:Property ID="RestProperty5" InnerProperty='<%# GetPopUpImageFromImageVault("RestBarImage5", "RestImage5PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RestBarImage5") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RestImage6PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RestLinkButton6" runat="server" CommandArgument="RestBarImage6">
                            <EPiServer:Property ID="RestProperty6" InnerProperty='<%# GetPopUpImageFromImageVault("RestBarImage6", "RestImage6PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RestBarImage6") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RestImage7PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RestLinkButton7" runat="server" CommandArgument="RestBarImage7">
                            <EPiServer:Property ID="RestProperty7" InnerProperty='<%# GetPopUpImageFromImageVault("RestBarImage7", "RestImage7PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RestBarImage7") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RestImage8PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RestLinkButton8" runat="server" CommandArgument="RestBarImage8">
                            <EPiServer:Property ID="RestProperty8" InnerProperty='<%# GetPopUpImageFromImageVault("RestBarImage8", "RestImage8PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RestBarImage8") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RestImage9PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RestLinkButton9" runat="server" CommandArgument="RestBarImage9">
                            <EPiServer:Property ID="RestProperty9" InnerProperty='<%# GetPopUpImageFromImageVault("RestBarImage9", "RestImage9PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RestBarImage9") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="RestImage10PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="RestLinkButton10" runat="server" CommandArgument="RestBarImage10">
                            <EPiServer:Property ID="RestProperty10" InnerProperty='<%# GetPopUpImageFromImageVault("RestBarImage10", "RestImage10PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("RestBarImage10") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
            </div>
            <div class="MoreImagesPopUpImageThumbRow">
                <asp:PlaceHolder ID="LeisureImage1PH" runat="server">
                    <h2>
                        <EPiServer:Translate ID="Translate4" Text="/Templates/Scanweb/Units/Static/MoreImagesPopUp/LeisureFacilities"
                            runat="server" />
                    </h2>
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="LeisureLinkButton1" runat="server" CommandArgument="LeisureImage1">
                            <EPiServer:Property ID="LeisureProperty1" InnerProperty='<%# GetPopUpImageFromImageVault("LeisureImage1", "LeisureImage1PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("LeisureImage1") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="LeisureImage2PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="LeisureLinkButton2" runat="server" CommandArgument="LeisureImage2">
                            <EPiServer:Property ID="LeisureProperty2" InnerProperty='<%# GetPopUpImageFromImageVault("LeisureImage2", "LeisureImage2PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("LeisureImage2") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="LeisureImage3PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="LeisureLinkButton3" runat="server" CommandArgument="LeisureImage3">
                            <EPiServer:Property ID="LeisureProperty3" InnerProperty='<%# GetPopUpImageFromImageVault("LeisureImage3", "LeisureImage3PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("LeisureImage3") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="LeisureImage4PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="LeisureLinkButton4" runat="server" CommandArgument="LeisureImage4">
                            <EPiServer:Property ID="LeisureProperty4" InnerProperty='<%# GetPopUpImageFromImageVault("LeisureImage4", "LeisureImage4PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("LeisureImage4") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="LeisureImage5PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="LeisureLinkButton5" runat="server" CommandArgument="LeisureImage5">
                            <EPiServer:Property ID="LeisureProperty5" InnerProperty='<%# GetPopUpImageFromImageVault("LeisureImage5", "LeisureImage5PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("LeisureImage5") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="LeisureImage6PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="LeisureLinkButton6" runat="server" CommandArgument="LeisureImage6">
                            <EPiServer:Property ID="LeisureProperty6" InnerProperty='<%# GetPopUpImageFromImageVault("LeisureImage6", "LeisureImage6PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("LeisureImage6") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="LeisureImage7PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="LeisureLinkButton7" runat="server" CommandArgument="LeisureImage7">
                            <EPiServer:Property ID="LeisureProperty7" InnerProperty='<%# GetPopUpImageFromImageVault("LeisureImage7", "LeisureImage7PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("LeisureImage7") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="LeisureImage8PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="LeisureLinkButton8" runat="server" CommandArgument="LeisureImage8">
                            <EPiServer:Property ID="LeisureProperty8" InnerProperty='<%# GetPopUpImageFromImageVault("LeisureImage8", "LeisureImage8PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("LeisureImage8") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="LeisureImage9PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="LeisureLinkButton9" runat="server" CommandArgument="LeisureImage9">
                            <EPiServer:Property ID="LeisureProperty9" InnerProperty='<%# GetPopUpImageFromImageVault("LeisureImage9", "LeisureImage9PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("LeisureImage9") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="LeisureImage10PH" runat="server">
                    <div class="MoreImagesPopUpImageThumb">
                        <asp:LinkButton ID="LeisureLinkButton10" runat="server" CommandArgument="LeisureImage10">
                            <EPiServer:Property ID="LeisureProperty10" InnerProperty='<%# GetPopUpImageFromImageVault("LeisureImage10", "LeisureImage10PH") %>'
                                ImageWidth="70" runat="server" alt='<%# GetAltText("LeisureImage10") %>' />
                        </asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
            </div>
        </div>
        <div id="MoreImagesPopUpRightColumn">
            <asp:PlaceHolder ID="ZoomedInPopUpPH" Visible="false" runat="server">
                <div class="MoreImagesPopUpPageListing">
                    <asp:LinkButton CssClass="MoreImagesPopUpPrevLink" ID="ImagesPopUpPrevious" runat="server">
                        <EPiServer:Translate ID="Translate6" Text="/Templates/Scanweb/Units/Static/MoreImagesPopUp/Previous"
                            runat="server" />
                    </asp:LinkButton>
                    &nbsp;|&nbsp;
                    <asp:LinkButton CssClass="MoreImagesPopUpNextLink"
                        ID="ImagesPopUpNext" runat="server">
                        <EPiServer:Translate ID="Translate7" Text="/Templates/Scanweb/Units/Static/MoreImagesPopUp/Next"
                            runat="server" />
                    </asp:LinkButton>
                </div>
                <asp:PlaceHolder ID="ZoomedInPopUpImage" runat="server"></asp:PlaceHolder>
                <div class="ZoomedInImageContent">
                    <div>
                        <asp:Literal ID="ZoomedInDescription" runat="server"></asp:Literal>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
	</asp:PlaceHolder>
	<asp:PlaceHolder ID="Image360PH" Visible="false" runat="server">
    <div id="ComplexImagePopUpPage">
        <div class="ComplexMediaArea">
            <div class="ComplexMediaContainer">
                <div class="ComplexImagePopUpLeft">
                    <EPiServer:Property ID="Property17" InnerProperty='<%# GetPopUp360FromImageVault("360Image") %>'
                        runat="server" />
                    <!--R1.5.3 | Artifact artf877410 : R1.5.3 | 360 images byline-->
                    <div>
                        <%# HotelPage["ByLineText360Image"] as string ?? String.Empty %>
                    </div>
                </div>
                <div class="ComplexImagePopUpRight">
                    <div class="ZoomedInImageContent">
                        <h2>
                            <%# HotelPage["ComplexMediaTitle"] as string ?? String.Empty %>
                        </h2>
                    </div>
                </div>
                <br />
            </div>
            <div class="ComplexMediaContainer">
                <div class="ComplexImagePopUpLeft">
                    <EPiServer:Property ID="Property18" InnerProperty='<%# GetPopUp360FromImageVault("360Image2") %>'
                        runat="server" />
                    <!--R1.5.3 | Artifact artf877410 : R1.5.3 | 360 images byline-->
                    <div>
                        <%# HotelPage["ByLineText360Image2"] as string ?? String.Empty %>
                    </div>
                </div>
                <div class="ComplexImagePopUpRight">
                    <div class="ZoomedInImageContent">
                        <h2>
                            <%# HotelPage["ComplexMediaTitle2"] as string ?? String.Empty %>
                        </h2>
                    </div>
                </div>
                <br />
            </div>
            <div class="ComplexMediaContainer">
                <div class="ComplexImagePopUpLeft">
                    <EPiServer:Property ID="Property19" InnerProperty='<%# GetPopUp360FromImageVault("360Image3") %>'
                        runat="server" />
                    <!--R1.5.3 | Artifact artf877410 : R1.5.3 | 360 images byline-->
                    <div>
                        <%# HotelPage["ByLineText360Image3"] as string ?? String.Empty %>
                    </div>
                </div>
                <div class="ComplexImagePopUpRight">
                    <div class="ZoomedInImageContent">
                        <h2>
                            <%# HotelPage["ComplexMediaTitle3"] as string ?? String.Empty %>
                        </h2>
                    </div>
                </div>
            </div>
              <div class="ComplexMediaContainer">
                <div class="ComplexImagePopUpLeft">
                    <EPiServer:Property ID="Property20" InnerProperty='<%# GetPopUp360FromImageVault("360Image4") %>'
                        runat="server" />
                    <!--R1.5.3 | Artifact artf877410 : R1.5.3 | 360 images byline-->
                    <div>
                        <%# HotelPage["ByLineText360Image4"] as string ?? String.Empty %>
                    </div>
                </div>
                <div class="ComplexImagePopUpRight">
                    <div class="ZoomedInImageContent">
                        <h2>
                            <%# HotelPage["ComplexMediaTitle4"] as string ?? String.Empty %>
                        </h2>
                    </div>
                </div>
            </div>
		</div>
		</div>
	</asp:PlaceHolder>
    </div>
</div>
