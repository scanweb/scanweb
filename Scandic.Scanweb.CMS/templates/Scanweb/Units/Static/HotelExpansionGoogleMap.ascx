<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelExpansionGoogleMap.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.HotelExpansionGoogleMap" %>
<%@ Register Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3"
    TagPrefix="cc1" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>

<%--<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Scanweb/Javascript/gmaps-utility-library/markermanager_packed.js")%>"></script>
<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Scanweb/Javascript/gmaps-utility-library/labeledmarker_packed.js")%>"></script>
<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Scanweb/Javascript/gmaps-utility-library/extinfowindow_packed.js")%>"></script>--%>

<div id="HotelExpansionGoogleMap">
    <div>
        <div id="GMapV3" style="width:759px;Height:540px">
        <cc1:Map ID="GMapControl1" runat="server" />
        </div>
    </div>
    <div id="HotelFilter">
        <h3><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/OurHotels")%></h3>
         <p>
            <img src="<%=ResolveUrl("~/Templates/Scanweb/Styles/Default/Images/Icons/filter_green.gif")%>" title="<%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/RecentlyOpened")%>" alt="<%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/RecentlyOpened")%>"/>
            <label><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/RecentlyOpened")%></label>
            <input id="RecentlyOpened" type="checkbox" checked="checked" onclick="SwitchHotelFilter(this,'RecentlyOpened')"  />
        </p>
       
       
       <p>
           <img src="<%=ResolveUrl("~/Templates/Scanweb/Styles/Default/Images/Icons/filter_orange.gif")%>" title="<%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/ComingSoon")%>" alt="<%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/ComingSoon")%>"/>
           <label><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/ComingSoon")%></label>
           <input id="ComingSoon" type="checkbox" checked="checked" onclick="SwitchHotelFilter(this,'ComingSoon')" />
       </p>
       
       <p>
            <img src="<%=ResolveUrl("~/Templates/Scanweb/Styles/Default/Images/Icons/purple_filter_icon.png")%>" title="<%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/Others")%>" alt="<%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/Others")%>"/>
            <label><%=WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/GoogleMapFilter/Others")%></label>
            <input id="Default" type="checkbox"  onclick="SwitchHotelFilter(this,'Default')" />
       </p>
               
        
    </div>
    <%--<div>
            <asp:RadioButtonList ID="rbtnToFrom" CssClass="rbList" RepeatDirection="Horizontal"  runat="server" />
            <asp:TextBox ID="tbDirection" CssClass="address"  runat="server" />
            <asp:Button ID="btnDirections" CssClass="DirectionsButton" runat="server" OnClick="btnDirectionsClick" />
    </div>
    <div id="RoadDirectionInstructions"></div>--%>
</div>