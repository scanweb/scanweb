<%@ Control Language="C#" AutoEventWireup="False" CodeBehind="StartpagePuffs.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.StartpagePuffs" %>
<div class="Imagebox1">
    <div class="Imageboxitem"><EPiServer:Property ID="FirstPuffImage" runat="server" visible="false" /></div>
    <div class="Imageboxtext"><EPiServer:Property ID="Property1" PropertyName="FirstPuffBody" EnableViewState="false" runat="server" /></div>
    <div class="Imageboxlinkcontainer">
        <div class="Imageboxlinkarrow"><img src="~/Templates/Scanweb/Styles/Default/Images/red_arrow_right.gif" alt=" " /></div>
        <div class="Imageboxlinktext"><a href="<%= FirstPuffURL() %>" title='<%= Translate("/navigation/readmore") %>'><EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Units/Static/StartpagePuffs/ReadMore" runat="server" /></a></div>
    </div> 
    <div class="Puffcontainer">    
        <div class="roundbottom">
            <div class="r4"></div>
            <div class="r3"></div>
            <div class="r2"></div>
            <div class="r1"></div>
        </div>
    </div>
</div>

<div class="Imagebox2">
    <div class="Imageboxitem"><EPiServer:Property ID="SecondPuff" runat="server" visible="false" /></div>
    <div class="Imageboxtext"><EPiServer:Property ID="Property2" PropertyName="SecondPuffBody" EnableViewState="false" runat="server" /></div>
    <div class="Imageboxlinkcontainer">
        <div class="Imageboxlinkarrow"><img src="~/Templates/Scanweb/Styles/Default/Images/red_arrow_right.gif" alt=" " /></div>
        <div class="Imageboxlinktext"><a href="<%= SecondPuffURL() %>" title='<%= Translate("/navigation/readmore") %>'><EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Units/Static/StartpagePuffs/ReadMore" runat="server" /></a></div>
    </div>
    <div class="Puffcontainer">    
        <div class="roundbottom">
            <div class="r4"></div>
            <div class="r3"></div>
            <div class="r2"></div>
            <div class="r1"></div>
        </div>
    </div>
</div>
  
 