//  Description					: FindHotelSearch                                         //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using EPiServer.Core;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units
{
    /// <summary>
    /// Code behind of FindHotelSearch control.
    /// </summary>
    public partial class FindHotelSearch : System.Web.UI.UserControl
    {
        /// <summary>
        /// SearchedHotel event args
        /// </summary>
        public class SearchedHotelEventArg : EventArgs
        {
            private string mapId;

            public string GetMapID
            {
                get { return mapId; }
            }

            public SearchedHotelEventArg(string MapID)
            {
                mapId = MapID;
            }
        }

        /// <summary>
        /// Search hotel
        /// </summary>
        public event EventHandler<SearchedHotelEventArg> SearchedHotel;

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FindHotelButton.Text =
                    LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Static/FindHotelSearch/Search");
                FindHotelTextBox.Text =
                    LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Units/Static/FindHotelSearch/EnterDestination");
            }
        }

        /// <summary>
        /// Find hotel button click handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FindHotelButton_Click(object sender, EventArgs e)
        {
            List<CityDestination> allDestinations = ContentDataAccess.GetCityAndHotel(true);

            string cityMapID = string.Empty;

            int cityCounter = 0;
            string inputText = FindHotelTextBox.Text;

            StringBuilder mainBuilder = new StringBuilder();

            foreach (CityDestination c in allDestinations)
            {
                if (IsMatch(inputText, c.Name))
                {
                    mainBuilder.Append(string.Format("<div><strong>{0}</strong></div>", c.Name));
                    foreach (HotelDestination h in c.SearchedHotels)
                    {
                        mainBuilder.Append(
                            string.Format("<div><a href=\"{0}\" title=\"{1}\" class=\"IconLink\">{2}</a></div>",
                                          h.HotelPageURL, h.Name, h.Name));
                    }
                    cityMapID = c.MapID;
                    cityCounter++;
                }
                else
                {
                    StringBuilder hotelBuilder = new StringBuilder();
                    bool hotelExists = false;

                    foreach (HotelDestination h in c.SearchedHotels)
                    {
                        if (IsMatch(inputText, h.SearchableString))
                        {
                            hotelBuilder.Append(
                                string.Format("<div><a href=\"{0}\" title=\"{1}\" class=\"IconLink\">{2}</a></div>",
                                              h.HotelPageURL, h.Name, h.Name));
                            hotelExists = true;
                        }
                    }
                    if (hotelExists)
                    {
                        mainBuilder.Append(string.Format("<div><strong>{0}</strong></div>", c.Name));
                        mainBuilder.Append(hotelBuilder.ToString());
                        cityMapID = c.MapID;
                        cityCounter++;
                    }
                }
            }

            string results = mainBuilder.ToString();

            SearchResults.Text = results.Length > 0
                                     ? results
                                     : LanguageManager.Instance.Translate(
                                         "/Templates/Scanweb/Units/Static/FindHotelSearch/NoResults");
            SearchResultsPlaceHolder.Visible = true;
            if (cityCounter != 1)
            {
                cityMapID = string.Empty;
            }
            SearchedHotelEventArg hotelEventArg = new SearchedHotelEventArg(cityMapID);

            if (SearchedHotel != null)
            {
                SearchedHotel(this, hotelEventArg);
            }
        }

        private bool IsMatch(string input, string compareTo)
        {
            string preparedInput = PrepareStringForComparison(input);

            preparedInput = preparedInput.Replace("scandic", "");
            preparedInput = preparedInput.Trim();

            string preparedCompareTo = PrepareStringForComparison(compareTo);
            return preparedCompareTo.StartsWith(preparedInput, StringComparison.InvariantCultureIgnoreCase);
        }

        private string PrepareStringForComparison(string input)
        {
            input = input.ToLower();
            input = input.Replace('�', 'o');
            input = input.Replace('�', 'o');
            input = input.Replace('�', 'a');
            input = input.Replace('�', 'a');
            input = input.Replace('�', 'a');
            input = input.Replace('�', 'a');
            input = input.Replace('�', 'u');
            input = input.Replace('�', 'u');
            input = input.Replace("�", "ss");
            return input;
        }
    }
}