//  Description					: OverviewHotelGoogleMap                                  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.Map;
using Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Overview Hotel Google Map
    /// </summary>
    public partial class OverviewHotelGoogleMap : ScandicUserControlBase
    {
        private PageDataCollection validHotelPagesLeft;
        private PageDataCollection validHotelPagesRight;

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            PageReference hotelContainerPageLink = CurrentPage["OverviewHotelContainer"] as PageReference;
            int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
            if (hotelContainerPageLink != null)
            {
                PageDataCollection hotelPages = DataFactory.Instance.GetChildren(hotelContainerPageLink);

                IList<MapUnit> hotels = new List<MapUnit>();

                validHotelPagesLeft = new PageDataCollection();
                validHotelPagesRight = new PageDataCollection();

                bool alternateColumn = false;

                foreach (PageData hotelPage in hotelPages)
                {
                    if (hotelPage.CheckPublishedStatus(PagePublishedStatus.Published) &&
                        hotelPage.PageTypeID == hotelPageTypeID)
                    {
                        MapHotelUnit hotel = new MapHotelUnit(
                            (double) hotelPage["GeoX"],
                            (double) hotelPage["GeoY"],
                            -1,
                            string.Format("http://{0}/{1}", Request.Url.Host,
                                          "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png"),
                            string.Empty,
                            hotelPage["Heading"] as string,
                            "",
                            hotelPage["StreetAddress"] as string,
                            hotelPage["PostCode"] as string,
                            hotelPage["PostalCity"] as string,
                            hotelPage["City"] as string,
                            hotelPage["Country"] as string,
                            GetFriendlyURLToPage(hotelPage.PageLink, hotelPage.LinkURL),
                            GetDeepLinkingURL(hotelPage["OperaID"] as string ?? string.Empty)
                            );

                        hotels.Add(hotel);

                        if (!alternateColumn)
                            validHotelPagesLeft.Add(hotelPage);
                        else
                            validHotelPagesRight.Add(hotelPage);

                        alternateColumn = !alternateColumn;
                    }
                }
                GMapControl1.GoogleMapKey = (ConfigurationManager.AppSettings["googlemaps." + Request.Url.Host] as string ?? (ConfigurationManager.AppSettings["DevKey"] as string));
                GMapControl1.DataSource = hotels;
                GMapControl1.MarkerLatitudeField = "latitude";
                GMapControl1.MarkerLongitudeField = "longitude";
                GMapControl1.AutoCenterAndZoom();
                GMapControl1.DataBind();


                HotelsPageListLeft.DataSource = validHotelPagesLeft;
                HotelsPageListLeft.DataBind();
                HotelsPageListRight.DataSource = validHotelPagesRight;
                HotelsPageListRight.DataBind();

                if (Page.Request.Url.ToString().StartsWith("https"))
                {
                    GoogleMapsPlaceHolder.Visible = false;
                    AlternativeMapPlaceHolder.Visible = true;
                }
            }
        }

        /// <summary>
        /// Is Last Item Left
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns>true or false</returns>
        protected bool IsLastItemLeft(int itemID)
        {
            if (validHotelPagesLeft.Count > 0)
                return (validHotelPagesLeft[validHotelPagesLeft.Count - 1].PageLink.ID == itemID);
            else
                return false;
        }

        /// <summary>
        /// Is Last Item Right
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns>true or false</returns>
        protected bool IsLastItemRight(int itemID)
        {
            if (validHotelPagesRight.Count > 0)
                return (validHotelPagesRight[validHotelPagesRight.Count - 1].PageLink.ID == itemID);
            else
                return false;
        }

        /// <summary>
        /// Get Friendly URL
        /// </summary>
        /// <param name="pageLink"></param>
        /// <param name="linkURL"></param>
        /// <returns>friendly url</returns>
        protected string GetFriendlyURL(PageReference pageLink, string linkURL)
        {
            UrlBuilder url = new UrlBuilder(linkURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, pageLink, System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }
    }
}