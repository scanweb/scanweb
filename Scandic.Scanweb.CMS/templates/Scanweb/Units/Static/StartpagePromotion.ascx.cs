//  Description					:   StartpagePromotion                                    //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using EPiServer;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// A common footer for the website where links common for the whole site are listed. 
    /// </summary>
    public partial class StartpagePromotion : UserControlBase
    {
        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            string FontColour = string.Empty;
            if (CurrentPage["PriceTextMainOffer"] != null)
            {
                PriceText.Text = CurrentPage["PriceTextMainOffer"] as string;
                FontColour = CurrentPage["FontColor"] as string;
                if (!string.IsNullOrEmpty(FontColour))
                    PriceRate.InnerHtml = CurrentPage["PriceTextMainOffer"] as string;
                PriceRate.Attributes.Add("style", "color:" + FontColour);
            }
            if (CurrentPage["MainOfferBody"] != null)
                PromoBody.InnerHtml = CurrentPage["MainOfferBody"] as string;
        }

        /// <summary>
        /// GetURL
        /// </summary>
        /// <returns>Url</returns>
        public string GetURL()
        {
            PageReference pageRef = CurrentPage["MainOfferLink"] as PageReference;
            if (pageRef != null)
            {
                try
                {
                    return Server.HtmlEncode(DataFactory.Instance.GetPage(pageRef).LinkURL);
                }
                catch (AccessDeniedException)
                {
                    return string.Empty;
                }
            }
            return string.Empty;
        }

        public string GetTrackingFunction()
        {
            string trackingFunction = "return true;";

            if (CurrentPage["TrackMainPromo"] != null && (bool)CurrentPage["TrackMainPromo"])
            {
                var trackInfo = CurrentPage["ContentNameMainPromo"] as string;
                trackingFunction = string.Format("return trackPromobox('Main','{0}');", trackInfo);
            }
            return trackingFunction;
        }
     
    }
}