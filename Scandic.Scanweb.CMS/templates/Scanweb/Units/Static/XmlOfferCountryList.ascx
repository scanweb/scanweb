<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="XmlOfferCountryList.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.XmlOfferCountryList" %>

<asp:PlaceHolder ID="CountryList" runat="server">
<div id="OfferCategoryContainer<%=GetCountryName()%>" class="OfferCategoryListNotExpanded">
    <div class="OfferCategoryContainer">
        <a class="OfferCategoryContractLink" onclick="SwitchClassName('OfferCategoryContainer<%=GetCountryName()%>', 'OfferCategoryListExpanded', 'OfferCategoryListNotExpanded');return false;" ><%=GetCountryName()%> </a>
        <a class="OfferCategoryExpandLink" onclick="SwitchClassName('OfferCategoryContainer<%=GetCountryName()%>', 'OfferCategoryListExpanded', 'OfferCategoryListNotExpanded');return false;" ><%=GetCountryName()%> </a>
            <div class="OfferCategoryListing">
                <asp:PlaceHolder id="CityListPlaceHolder" runat="server">
                </asp:PlaceHolder>
            </div>
           <div class="borderCss" >&nbsp;</div>
    </div>

</div>


</asp:PlaceHolder>