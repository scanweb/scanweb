<%@ Control Language="C#" EnableViewState="false" AutoEventWireup="False" CodeBehind="Crisis.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.Crisis" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>

<input type="hidden" id="CrisisFlag" name="CrisisFlag" value='<%= ShowCrisis() %>'/>
  <!-- Major Crisis Content -->
  <div id="CrisisContainer" style="display: none;">
    <div class="CrisisMessage">
    <div class="top">&nbsp;</div>
      <div class="center">
        <h3><%= GetCrisisHeader() %></h3><!--Major crisis title here -->
        <p class="SkipLink link"><a href="javascript:void(0);" class="skip_btn"><EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Units/Static/Crisis/Skip"  runat="server" /></a> </p>
        <p class="clear"><%= GetCrisisText() %></p>
        <p class="link"> <a href="javascript:void(0);" onclick="openPopupWin('<%= GlobalUtil.GetUrlToPage(GetMoreInfoPage()) %>','width=800,height=500,scrollbars=yes,resizable=yes');return false;"><EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Units/Static/Crisis/ReadMore"  runat="server" /></a> </p>
      </div>
      <div class="bottom">&nbsp;</div>
    </div>
  </div>
  <!-- END: Major Crisis Content -->         

	