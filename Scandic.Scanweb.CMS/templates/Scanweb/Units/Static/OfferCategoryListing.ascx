<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfferCategoryListing.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.OfferCategoryListing" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>

                        


        <!-- <div class="OfferCategoryHeadline DottedUnderLineBlocknew">
           <% if (CurrentPage.PageTypeName != "Offer Category")
              { %>
            <a href="#" class="OfferCategoryContractLink" onclick="SwitchClassName('OfferCategoryContainer<%= OfferCategoryPage.PageLink.ID %>', 'OfferCategoryListExpanded', 'OfferCategoryListNotExpanded');return false;"><EPiServer:Translate Text="/Templates/Scanweb/Pages/OfferList/Hide" runat="server" />&nbsp;<%= OfferCategoryPage.PageName %></a>
            <a href="#" class="OfferCategoryExpandLink" onclick="SwitchClassName('OfferCategoryContainer<%= OfferCategoryPage.PageLink.ID %>', 'OfferCategoryListExpanded', 'OfferCategoryListNotExpanded');return false;"><EPiServer:Translate Text="/Templates/Scanweb/Pages/OfferList/Show" runat="server" />&nbsp;<%= OfferCategoryPage.PageName %>
                <asp:Literal ID="OfferCount" runat="server"></asp:Literal>
            </a>
            <% }
              else
              {%>
            <label>&nbsp;<%= OfferCategoryPage.PageName %></label>
             
            <% } %>
        </div> -->


        
        <asp:Repeater ID="OffersPageList" runat="server">
        <ItemTemplate>
        <div class="OfferCategoryListingItem"  id="OfferCategoryListing" runat="server">
            <div class="OfferCategoryListingLeftColumn">
               <Scanweb:SquaredCornerImage id="SquaredImage" 
               ImagePropertyName="BoxImageMedium" 
                                             TopCssClass="RoundedCornersTop226" 
                                             ImageCssClass="RoundedCornersImage226" 
                                             BottomCssClass="RoundedCornersBottom226" ImageWidth="224" runat="server" />
                                             
              
            </div>      
            <div class="OfferCategoryListingRightColumn">
                <div><strong><EPiServer:Property ID="BoxHeading" PropertyName="BoxHeading" runat="server" /></strong></div>
                <div>
                    <EPiServer:Property PropertyName="BoxContent" runat="server" />
                </div>
                <div class="PriceText" id ="priceText" runat="server">
                </div>
                <div class="OfferCategoryRightColumnRight">
                   <div class="actionBtn fltRt"><a title="Read More" id="ReadMoreLink1" runat="server" class="buttonInner"><EPiServer:Translate Text="/Templates/Scanweb/Units/Static/StartpagePuffs/Readmore" runat="server" /></a><a class="buttonRt scansprite" id="ReadMoreLink2" runat="server" ></a></div>
                </div>
            </div>                           
        </div>
        </ItemTemplate>
        </asp:Repeater>

