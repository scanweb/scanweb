<%@ Control Language="C#" EnableViewState="false" AutoEventWireup="true" CodeBehind="MainMenu.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.MainMenu" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%@ Register TagPrefix="Scanweb" TagName="MainMenuSubMenu" Src="~/Templates/Scanweb/Units/Static/MainMenuSubMenu.ascx" %>
<script type="text/javascript" language="javascript">
var requestedUrl = <%= "\"" + GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") + "\"" %>;
</script>
<div id="MainMenuTopMenu">
    <asp:Repeater runat="server" ID="rptMainMenu">
        <HeaderTemplate>
            <ul id="TopMenu">
        </HeaderTemplate>
        <ItemTemplate>
            <li id="mainMenuContainerLink" runat="server">
            <asp:HyperLink ID="mainMenuItemLink" runat="server"></asp:HyperLink>    
            <Scanweb:MainMenuSubMenu ID="mainMenuSubMenu" runat="server" />
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul></FooterTemplate>
    </asp:Repeater>
</div>

