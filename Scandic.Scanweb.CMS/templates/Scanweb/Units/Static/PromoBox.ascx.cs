//  Description					:   PromoBox                                              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Text.RegularExpressions;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable
{
    /// <summary>
    /// Code behind of promobox control.
    /// </summary>
    public partial class PromoBox : ScandicUserControlBase
    {
        /// <summary>
        /// This is to open the promo box link in the same window.
        /// </summary>
        private const int OPEN_LINK_IN_SAME_WINDOW = 2;

        /// <summary>
        /// This is to open the promo box link in a new window.
        /// </summary>
        private const int OPEN_LINK_IN_NEW_WINDOW = 1;

        private string image;

        /// <summary>
        /// Sets image.
        /// </summary>
        public string Image
        {
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    string imageString = CurrentPage[value] as string;
                    if (!string.IsNullOrEmpty(imageString))
                    {
                        BoxImage.ImageUrl = WebUtil.GetImageVaultImageUrl(imageString, imageMaxWidth);                        
                        ImagePlaceHolder.Visible = true;
                    }
                }
            }
        }

        private int imageMaxWidth;

        /// <summary>
        /// Image maximum width
        /// </summary>
        public int ImageMaxWidth
        {
            get { return imageMaxWidth; }
            set { imageMaxWidth = value; }
        }

        private string heading;

        /// <summary>
        /// Gets/Sets Heading
        /// </summary>
        public string Heading
        {
            get { return heading; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    heading = CurrentPage[value] as string;
                    HeadingText.Text = heading;
                    HeadingPlaceHolder.Visible = true;
                }
                else
                {
                    HeadingPlaceHolder.Visible = false;
                }
            }
        }

        private string description;

        /// <summary>
        /// Gets/Sets Description
        /// </summary>
        public string Description
        {
            get { return description; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    description = CurrentPage[value] as string;
                    HotelDescription.Text = description;
                    HotelDescriptionPlaceHolder.Visible = true;
                }
                else
                    HotelDescriptionPlaceHolder.Visible = false;
            }
        }

        private string linkURL;

        /// <summary>
        /// Sets/Gets LinkURL
        /// </summary>
        public string LinkURL
        {
            get { return linkURL; }
            set
            {
                if ((!string.IsNullOrEmpty(value)) && (null != CurrentPage[value]))
                {
                    string url = string.Empty;
                    if (CurrentPage[value].ToString().StartsWith("/") && CurrentPage[value].ToString().Contains("id="))
                    {
                        url = CurrentPage[value].ToString();
                    }
                    else if (IsUrl(CurrentPage[value].ToString()))
                    {
                        url = CurrentPage[value].ToString();
                    }
                    PageLink1.Text = string.Format(@"<a href=""{0}"" class=""IconLink"" {2}>{1}</a>", url, linkText,
                                                   linkUrlOpenOption);
                    PageLinkPlaceHolder1.Visible = true;
                }
                else
                {
                    PageLinkPlaceHolder1.Visible = false;
                }
            }
        }

        private string linkText;

        /// <summary>
        /// Gets/Sets LinkText
        /// </summary>
        public string LinkText
        {
            get { return linkText; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    linkText = CurrentPage[value] as string;
            }
        }

        /// <summary>
        /// To detemine whether open in same window or not
        /// </summary>
        private string linkUrlOpenOption;

        /// <summary>
        /// Sets the link URL open option.
        /// </summary>
        /// <value>The link URL open option.</value>
        public string LinkUrlOpenOption
        {
            set
            {
                if (value != null)
                {
                    if (CurrentPage[value] == null)
                    {
                        linkUrlOpenOption = string.Empty;
                    }
                    else if ((int) CurrentPage[value] == OPEN_LINK_IN_NEW_WINDOW)
                    {
                        linkUrlOpenOption = "target=\"_blank\"";
                    }
                    else if ((int) CurrentPage[value] == OPEN_LINK_IN_SAME_WINDOW)
                    {
                        linkUrlOpenOption = "target=\"_self\"";
                    }
                }
            }
        }

        private string linkTextPropertyName = "BoxLinkText";

        /// <summary>
        /// Get the Alter css class
        /// </summary>
        /// <returns></returns>
        protected string GetAlertBoxStyle()
        {
            string alertBoxStyle = CurrentPage["AlertBoxStyle"] as string;
            return ((alertBoxStyle != null) ? (" " + alertBoxStyle) : string.Empty);
        }

        /// <summary>
        /// Promo default Css Class
        /// </summary>
        private string cssClass = "smallPromoBoxLight storyBox";

        /// <summary>
        /// Get Css Class
        /// </summary>
        /// <returns>Css class for offer as string</returns>
        protected string GetCssClass()
        {
            return cssClass;
        }

        /// <summary>
        /// Checks if the URL is valid or not.
        /// </summary>
        /// <param name="Url">URL to be testedd against.</param>
        /// <returns>False if invalid else true</returns>
        private bool IsUrl(string Url)
        {
            string pattern =
                @"^(http|https|ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
            Regex strRegex = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            bool returnValue = false;
            if (strRegex.IsMatch(Url))
                returnValue = true;
            return returnValue;
        }
    }
}