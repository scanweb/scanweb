//  Description					:   URLShortcuts                                          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


using System.Configuration;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.CMS.Util;
using System;

namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// A common footer for the website where links common for the whole site are listed. 
    /// </summary>
    public partial class URLShortcuts : ScandicUserControlBase
    {
        private PageDataCollection languageVersions;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);
            Session[ScanwebMobile.MOBILE_CONTENT_LANGUAGE] = null;
            Session[ScanwebMobile.MOBILE_LANGUAGE_KEY] = null;
        }


        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            LanguageBranchCollection activeLanguages = LanguageBranch.ListEnabled();
            PageDataCollection currentPageLanguageVersions =
                DataFactory.Instance.GetLanguageBranches(CurrentPage.PageLink);
            PageDataCollection startPageLanguageVersions =
                DataFactory.Instance.GetLanguageBranches(PageReference.StartPage);
            languageVersions = new PageDataCollection();

            foreach (LanguageBranch activeLanguage in activeLanguages)
            {
                PageData currentPageInActiveLanguage = GetLanguageVersionFromCollection(activeLanguage,
                                                                                        currentPageLanguageVersions);
                PageData startPageInActiveLanguage = GetLanguageVersionFromCollection(activeLanguage,
                                                                                      startPageLanguageVersions);
                if (currentPageInActiveLanguage != null &&
                    currentPageInActiveLanguage.CheckPublishedStatus(PagePublishedStatus.Published) &&
                    (CurrentPage["TopLinksToStartPage"] == null))
                {
                    languageVersions.Add(currentPageInActiveLanguage);
                }
                else if (startPageInActiveLanguage != null)
                {
                    languageVersions.Add(startPageInActiveLanguage);
                }
            }

            languageList.DataSource = languageVersions;
            languageList.DataBind();
        }

        /// <summary>
        /// Checks whether given request is a mobile request.
        /// </summary>
        /// <returns></returns>
        protected bool IsMobileRequest()
        {
            bool isMobileRequest = false;
            var redirectionFlag = ConfigurationManager.AppSettings["EnableMobileDeviceRedirection"];
            if (!string.IsNullOrEmpty(redirectionFlag))
            {
                bool performRedirection = false;
                bool.TryParse(redirectionFlag, out performRedirection);
                if (performRedirection)
                {
                    isMobileRequest =
                        Scandic.Scanweb.CMS.Util.MobileRedirect.Wurfl.Instance.IsMobileDevice(Request.UserAgent);
                }
            }
            return isMobileRequest;
        }

        private PageData GetLanguageVersionFromCollection(LanguageBranch language, PageDataCollection pages)
        {
            foreach (PageData languageVersion in pages)
            {
                if (language.LanguageID == languageVersion.LanguageID)
                    return languageVersion;
            }
            return null;
        }

        /// <summary>
        /// Gets language
        /// </summary>
        /// <param name="pageLang"></param>
        /// <returns></returns>
        protected string GetLang(PageData pageLang)
        {
            return LanguageBranch.Load(pageLang.LanguageBranch).Name;
        }

        /// <summary>
        /// Checks whether given item is the last item.
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        protected bool IsLastItem(PageData page)
        {
            if (languageVersions != null)
            {
                return (page.LanguageID == languageVersions[languageVersions.Count - 1].LanguageID);
            }
            return false;
        }

        /// <summary>
        /// Gets required url.
        /// </summary>
        /// <param name="linkURL"></param>
        /// <param name="language"></param>
        /// <param name="pageLink"></param>
        /// <returns></returns>
        protected string GetURL(string linkURL, string language, PageReference pageLink)
        {
            string finalUrl = string.Empty;
            string languageSelectionURL = EPiServer.UriSupport.AddLanguageSelection(linkURL, language);
            string fullLanguageSelectionURL = EPiServer.UriSupport.AbsoluteUrlBySettings(languageSelectionURL);
            
            UrlBuilder finalUrlBuilder = new UrlBuilder(fullLanguageSelectionURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(finalUrlBuilder, pageLink, System.Text.UTF8Encoding.UTF8);
            finalUrl = finalUrlBuilder.ToString();

            string appSettingsKey = string.Format("ScandicSiteHostName{0}", language.ToUpper());
            string hostURL = ConfigurationManager.AppSettings[appSettingsKey] as string;
            if (CurrentPage != null && string.Equals(CurrentPage.PageName, "apperror", StringComparison.InvariantCultureIgnoreCase))
            {
                string urlScheme = "http";
                if (Request.Url != null)
                    urlScheme = Request.Url.Scheme;
                return string.Format("{0}://{1}", urlScheme, hostURL);
            }
            Url url = new Url(fullLanguageSelectionURL);
            if (hostURL != null)
            {
                finalUrl = finalUrl.Replace(url.Host, hostURL);
                finalUrl = finalUrl.Replace("/" + language + "/", "/");
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["cmpid"])))
                    finalUrl = string.Format("{0}?cmpid={1}", finalUrl, Request.QueryString["cmpid"]);
            }
            return finalUrl;
        }

        /// <summary>
        /// Gets required for mobile
        /// </summary>
        /// <returns></returns>
        protected string GetURLForMobile()
        {
            Session[ScanwebMobile.MOBILE_CONTENT_LANGUAGE] = null;
            Session[ScanwebMobile.MOBILE_LANGUAGE_KEY] = null;
            string mobilePath = string.Empty;
            if (Request.Url.Port == 80)
            {
                mobilePath = string.Format("{0}://{1}/mobile/", Request.Url.Scheme, Request.Url.DnsSafeHost);
            }
            else
            {
                mobilePath = string.Format("{0}://{1}:{2}/mobile/", Request.Url.Scheme, Request.Url.DnsSafeHost, Request.Url.Port);
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["cmpid"])))
                mobilePath = string.Format("{0}?cmpid={1}", mobilePath, Request.QueryString["cmpid"]);

            return mobilePath;
        }
    }
}