//  Description					: TreeViewControl                                         //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System NameSpaces

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion 

namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// Tree View Control which captures the information from CMS and create the ASP.Net TreeView control.
    /// </summary>
    public partial class TreeViewControl : ScandicUserControlBase
    {
        #region Private Members
        private AvailabilityController availabilityController = null;

        /// <summary>
        /// Gets m_AvailabilityController
        /// </summary>
        private AvailabilityController m_AvailabilityController
        {
            get
            {
                if (availabilityController == null)
                {
                    availabilityController = new AvailabilityController();
                }
                return availabilityController;
            }
        }
        #endregion 

        #region Protected Events

        /// <summary>
        /// Page prerender event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            ExpandTree();
            string callChangeCSSforHotel =
                string.Format("<script type=\"text/javascript\" language=\"javascript\">ChangeCSSForHotels();</script>");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ChangeCSSforHotel", callChangeCSSforHotel,
                                                false);
        }

        /// <summary>
        /// Page_Load event of the TreeViewControl
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event params</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            countryTreeView.NodeStyle.CssClass = "tableBg";
            if (!Page.IsPostBack)
            {
                ViewState["SelectedNode"] = null;
                Node mainNode = ContentDataAccess.GetPageDataNodeTree(false);
                CreateTree(countryTreeView.Nodes, mainNode); 
                countryTreeView.CollapseAll();
                string nodeToSelect = string.Empty;
                if (Request.QueryString["MapID"] != null)
                {
                    string destinationID = Request.QueryString["MapID"].ToString();
                    if (destinationID.Length <= AppConstants.THREE_INT)
                    {
                        destinationID = m_AvailabilityController.GetHotelNameFromCMS(destinationID);
                    }
                    SearchTree(destinationID);

                    HtmlForm form1 = (HtmlForm) this.Page.Master.FindControl("Form1");
                    form1.Action = GlobalUtil.GetUrlToPage(EpiServerPageConstants.SEARCH_USING_MAP_PAGE);
                }
                else
                {
                    Node selectedNode = FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView;
                    if (selectedNode != null)
                    {
                        PageData selectedNodePageData = selectedNode.GetPageData();
                        int hotelPageTypeID =
                            PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;

                        string destinationName = string.Empty;
                        if (selectedNodePageData.PageTypeID.Equals(hotelPageTypeID))
                        {
                            destinationName = selectedNodePageData["Heading"].ToString();
                        }
                        else
                        {
                            destinationName = selectedNodePageData.PageName.ToString();
                        }
                        if (!string.IsNullOrEmpty(destinationName))
                        {
                            SearchTree(destinationName);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///  Eventhandler for selected node change.
        /// Fetches the selected node in the mainNode list from CDAL by giving a call to FindPageNode
        /// and stores the node in session.
        /// </summary>
        /// <remarks>Find a hotel release</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void countryTreeView_GetSelectedNode(object sender, EventArgs e)
        {
            ViewState["SelectedNode"] = countryTreeView.SelectedNode.ValuePath;
            StoreSelectedNodeInSession();
            FindAHotelSessionVariablesSessionWrapper.EventSource = SiteCatalystLocation.TREEVIEW;
        }

        #endregion 

        #region Private Methods

        /// <summary>
        /// This will create the tree view and populate tree nodes from the CMS Node Structure on UI.
        /// </summary>
        /// <param name="coll">Collection of tree nodes</param>
        /// <param name="parentNode">Main node which will be having complete tree nodes. Root node of tree.</param>
        /// <remarks>Find a hotel release</remarks>
        private void CreateTree(TreeNodeCollection coll, Node parentNode)
        {
            if (parentNode != null)
            {
                foreach (Node eachNode in parentNode.GetChildren())
                {
                    if (eachNode.GetChildren().Count > 0)
                    {
                        PageData pageData = eachNode.GetPageData();
                        TreeNode node = new TreeNode(pageData.PageName, pageData.PageLink.ID.ToString());
                        if (coll != null)
                            coll.Add(node);

                        CreateTree(node.ChildNodes, eachNode);
                    }
                    else
                    {
                        PageData pageData = eachNode.GetPageData();
                        TreeNode node;
                        int hotelPageTypeID =
                            PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                        if (pageData.PageTypeID.Equals(hotelPageTypeID))
                        {
                            string hotelName = pageData["Heading"] as string ?? string.Empty;
                            if (!string.IsNullOrEmpty(hotelName))
                            {
                                hotelName = hotelName.Trim();
                            }
                            node = new TreeNode(hotelName, pageData.PageLink.ID.ToString());
                        }
                        else
                        {
                            node = new TreeNode(pageData.PageName, pageData.PageLink.ID.ToString());
                        }

                        if (coll != null)
                            coll.Add(node);
                    }
                }
            }
        }

        /// <summary>
        ///  Fetches the selected node in the mainNode list from CDAL.
        /// To get the page details of selected country/city/hotel
        /// </summary>
        /// <param name="mainNodeList">List of CMS Node structure returned from CDAL</param>
        /// <param name="resultNode">node to store the selected node</param>
        /// <param name="pageLinkId">page link id of the selected node.Unique value used to search selected tree node in mainNodeList</param>
        /// <remarks>Find a hotel release</remarks>
        private void FindPageNode(List<Node> mainNodeList, ref Node resultNode, ref string pageLinkId)
        {
            if (mainNodeList != null)
            {
                foreach (Node node in mainNodeList)
                {
                    if (null == resultNode)
                    {
                        if (pageLinkId.Equals(node.GetPageData().PageLink.ID.ToString()))
                        {
                            resultNode = node;
                        }
                        else if (node.GetChildren().Count > 0)
                        {
                            FindPageNode(node.GetChildren(), ref resultNode, ref pageLinkId);
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// This method will traverse through the tree and find out the desired node reference.
        /// </summary>
        /// <param name="nodeCollection">Collection of nodes</param>
        /// <param name="resultNode">Desired node reference</param>
        /// <param name="searchString">Searched node value</param>
        /// <remarks>Find a hotel release</remarks>
        private void FindNode(TreeNodeCollection nodeCollection, ref TreeNode resultNode, ref string searchString)
        {
            foreach (TreeNode node in nodeCollection)
            {
                if (null == resultNode)
                {
                    if (node.Text.ToUpper() == searchString.ToUpper())
                    {
                        resultNode = node;
                    }
                    else if (node.ChildNodes.Count > 0)
                    {
                        FindNode(node.ChildNodes, ref resultNode, ref searchString);
                    }
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// This will do the job of expanding tree.
        /// </summary>
        /// <remarks>Find a hotel release</remarks>
        private void ExpandTree()
        {
            if (ViewState["SelectedNode"] != null)
            {
                countryTreeView.CollapseAll();
                TreeNode treeNode = countryTreeView.FindNode(ViewState["SelectedNode"] as string);
                treeNode.Expand();
                treeNode.Select();
                if (treeNode.Depth != 0) 
                {
                    int depthCount = treeNode.Depth - 1;
                    TreeNode parentNode = treeNode.Parent;
                    for (int count = 0; count <= depthCount; count++)
                    {
                        parentNode.Expand();
                        parentNode = parentNode.Parent;
                    }
                }
            }
        }

        /// <summary>
        /// This will expand the tree and store the selected node in session.
        /// </summary>
        /// <remarks>Find a hotel release</remarks>
        private void StoreSelectedNodeInSession()
        {
            Node resultNode = null; 
            List<Node> mainNodeList = ContentDataAccess.GetPageDataNodeTree(false).GetChildren();
            string pageLinkId = countryTreeView.SelectedNode.Value;
            FindPageNode(mainNodeList, ref resultNode, ref pageLinkId);

            if (resultNode != null)
            {
                FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView = resultNode; 
                SearchForHotelDestinations();
                FindAHotelSessionVariablesSessionWrapper.HotelSortOrder = SortType.ALPHABETIC.ToString();
                FindAHotelSessionVariablesSessionWrapper.IsHotelSorted = true;
                FindAHotelSessionVariablesSessionWrapper.IsShowAll = false;
            }
        }

        /// <summary>
        /// Method called when the control is loading
        /// 1.Gets the HotelDestination for the selected node on the tree
        /// 2.Filters if the selected node is Country, City or Hotel
        /// </summary>
        private void SearchForHotelDestinations()
        {
            if (FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView != null)
            {
                int countryPageTypeID =
                    PageType.Load(new Guid(ConfigurationManager.AppSettings["CountryPageTypeGUID"])).ID;
                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;

                FindAHotelSessionVariablesSessionWrapper.SetHotelDestinationResults(null);

                Node selectedNode = FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView;
                if (selectedNode != null)
                {
                    PageData selectedNodePageData = selectedNode.GetPageData();
                    if (selectedNodePageData.PageTypeID.Equals(countryPageTypeID))
                    {
                        foreach (Node selectedChildNode in selectedNode.GetChildren())
                        {
                            string citySearchCode = selectedChildNode.GetPageData()["OperaId"] as string ?? string.Empty;
                            if (!string.IsNullOrEmpty(citySearchCode))
                            {
                                SelectHotelUtil.InitiateSearchForFindAHotelDestination(citySearchCode.ToUpper(), false);
                            }
                        }
                    }
                    else if (selectedNodePageData.PageTypeID.Equals(cityPageTypeID))
                    {
                        string citySearchCode = selectedNodePageData["OperaID"] as string ?? string.Empty;
                        if (!string.IsNullOrEmpty(citySearchCode))
                        {
                            SelectHotelUtil.InitiateSearchForFindAHotelDestination(citySearchCode.ToUpper(), false);
                        }
                    }
                    else
                    {
                        string hotelSearchCode = selectedNodePageData["Heading"] as string ?? string.Empty;
                        if (!string.IsNullOrEmpty(hotelSearchCode))
                        {
                            SelectHotelUtil.InitiateSearchForFindAHotelDestination(hotelSearchCode.ToUpper(), true);
                        }
                    }
                    SelectHotelUtil.SortHotelsDestinationByName();
                }
            }
        }

        #endregion 

        #region Public Methods

        /// <summary>
        /// This will help to highlight the searched value in Treenode. 
        /// </summary>
        /// <param name="searchString">Node text to be searched in the tree.</param>
        /// <remarks>Find a hotel release</remarks>
        public void SearchTree(string searchString)
        {
            if (!string.IsNullOrEmpty(searchString))
            {
                TreeNode resultNode = null;
                FindNode(countryTreeView.Nodes, ref resultNode, ref searchString);
                if (null != resultNode)
                {
                    resultNode.Expand();
                    resultNode.Select();
                    ViewState["SelectedNode"] = resultNode.ValuePath;
                    StoreSelectedNodeInSession();
                }
            }
        }
        #endregion 
    }
}