using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Filters;
using EPiServer.Security;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.Util.ImageVault;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// SearchMeetingList
    /// </summary>
    public partial class SearchMeetingList : ScandicUserControlBase, INavigationTraking
    {
        /// <summary>
        /// Delegate for meeting google map
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">EventArgs for  meeting google map</param>
        public delegate void MeetingGoogleMapEventHandler(object sender, MeetingGoogleMapEventArgs e);

        /// <summary>
        /// Event for meeting google map
        /// </summary>
        public event MeetingGoogleMapEventHandler MeetingGoogleMapRender;

        protected PagingControl pc;

        public int CurrentCount = 0;

        /// <summary>
        /// The <see cref="PageDataCollection"/> containing the pagedata collection in the search
        /// </summary>
        public PageDataCollection PdcDataSource { get; set; }

        /// <summary>
        /// The <see cref="PageData"/> containing the page in the search
        /// </summary>
        public PageData CityCountrySearchPage { get; set; }

        public PageData CountrySearchPage { get; set; }

        public int Capacity { get; set; }

        public System.Web.UI.HtmlControls.HtmlGenericControl MeetingSearchBottomPagination;
        public string HotelDescription { get; set; }

        /// <summary>
        /// Internal event method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void ChangeSearched(object sender, Scandic.Scanweb.CMS.Templates.Units.Placeable.MeetingSearch.MeetingSearchEventArgs args)
        {
            Capacity = args.Capacity;
            CityCountrySearchPage = args.CityCountrySearchPage;
            CountrySearchPage = args.CountrySearchPage;
            if (CityCountrySearchPage != null)
            {
                PdcDataSource = GetMeetingListFromSearch();

                if (PdcDataSource.Count > 0)
                {
                    MiscellaneousSessionWrapper.SearchedMeetingRoomsPDC = PdcDataSource;
                    MiscellaneousSessionWrapper.SearchedCityCountryPage = CityCountrySearchPage;
                    MiscellaneousSessionWrapper.SearchedCountryPage = CountrySearchPage;
                    MiscellaneousSessionWrapper.SearchedCapacity = Capacity;
                }
                else
                {
                    MiscellaneousSessionWrapper.SearchedMeetingRoomsPDC = null;
                }

                BindData(PdcDataSource);
            }
            else
            {
                btnSendRFPs.Visible = false;
                btnSendRt.Visible = false;
                SearchResultListRepeater.Visible = false;
                ltlNoMeetingRooms.Visible = true;
                MeetingSearchBottomPagination.Visible = false;
                ltlNoMeetingRooms.Text = LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Static/SearchMeetingList/NoAvailableRooms");
            }

        }

        /// <summary>
        /// CreatePagingString
        /// </summary>
        /// <param name="noOfHits"></param>
        /// <returns>PagingString</returns>
        protected string CreatePagingString(int noOfHits)
        {
            int noOfPages = ((int)((noOfHits - 1) / 10) + 1);

            StringBuilder sb = new StringBuilder();
            sb.Append("<a href='#' style='display:none;' onClick='MeetingSearchPreviousPage(" + noOfPages + "," +
                      noOfHits + ")' ID='MeetingSearchPreviousArrow'>&lt;</a>&nbsp;&nbsp;");
            for (int i = 1; i < noOfPages + 1; i++)
            {
                sb.Append("<a href='#' style='font-weight:" + ((i == 1) ? "bold" : "normal") +
                          "' onClick='MeetingSearchSelectPage(" + i + "," + noOfPages + "," + noOfHits +
                          ")' ID='MeetingSearchSelectPage" + i + "'>" + i + "</a>&nbsp;&nbsp;");
            }
            sb.Append("<a href='#' style='display:" + (noOfHits > 10 ? "inline" : "none") +
                      ";'onClick='MeetingSearchNextPage(" + noOfPages + "," + noOfHits +
                      ")' ID='MeetingSearchNextArrow'>&gt;</a>&nbsp;&nbsp;");
            return sb.ToString();
        }

        /// <summary>
        /// GetMeetingGuideURL
        /// </summary>
        /// <returns>MeetingGuideURL</returns>
        protected string GetMeetingGuideURL()
        {
            String meetingGuiddocUrl = String.Empty;

            if (CityCountrySearchPage == null)
                CityCountrySearchPage = (PageData)MiscellaneousSessionWrapper.SearchedCityCountryPage;

            if (CountrySearchPage == null)
                CountrySearchPage = (PageData)MiscellaneousSessionWrapper.SearchedCountryPage;

            if (CityCountrySearchPage["MeetingGuide"] == null)
                CityCountrySearchPage = GetPage(((PageData)MiscellaneousSessionWrapper.SearchedCityCountryPage).ParentLink);

            if(!string.IsNullOrEmpty(Convert.ToString(CountrySearchPage["MeetingGuide"])))
                meetingGuiddocUrl = CountrySearchPage["MeetingGuide"].ToString();
            
            return meetingGuiddocUrl;
        }

        /// <summary>
        /// Gets the child pages from a specific container at the hotel page
        /// </summary>
        /// <param name="hotelPageLink">Link to the hotel page</param>
        /// <param name="containerPageTypeID">Container PageTypeID</param>
        /// <returns>All published child pages that the user has read access to</returns>
        private PageDataCollection GetChildPagesFromContainer(PageReference hotelPageLink, int containerPageTypeID)
        {
            PageDataCollection containerPages = DataFactory.Instance.GetChildren(hotelPageLink);
            foreach (PageData containerPage in containerPages)
            {
                if (containerPage.PageTypeID == containerPageTypeID)
                {
                    PageDataCollection childPages = DataFactory.Instance.GetChildren(containerPage.PageLink);

                    if (childPages != null)
                    {
                        for (int i = childPages.Count - 1; i >= 0; i--)
                        {
                            bool pageIsPublished = childPages[i].CheckPublishedStatus(PagePublishedStatus.Published);
                            bool hasReadAccess = childPages[i].QueryDistinctAccess(AccessLevel.Read);

                            if (!(pageIsPublished && hasReadAccess))
                                childPages.Remove(childPages[i]);
                        }
                    }

                    return childPages;
                }
            }
            return null;
        }

        /// <summary>
        /// Get Meeting Room Pages
        /// </summary>
        /// <returns>A collection of Meeting Room pages associated with this hotel</returns>
        private PageDataCollection GetMeetingRoomPages(PageData hotelPageData)
        {
            int meetingRoomContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["MeetingRoomContainerPageTypeGUID"])).ID;
            return GetChildPagesFromContainer(hotelPageData.PageLink, meetingRoomContainerPageTypeID);
        }

        private System.Collections.Generic.List<MeetingCategoryPrice> GetMeetingCategories(PageData pgData, bool leftCol)
        {
            List<MeetingCategoryPrice> meetingCategoryPrice = new List<MeetingCategoryPrice>();
            if (leftCol)
            {
                GetMeetingCategoryPriceCollection(pgData, meetingCategoryPrice, "1");
                GetMeetingCategoryPriceCollection(pgData, meetingCategoryPrice, "2");
                GetMeetingCategoryPriceCollection(pgData, meetingCategoryPrice, "3");
            }
            else
            {
                GetMeetingCategoryPriceCollection(pgData, meetingCategoryPrice, "4");
                GetMeetingCategoryPriceCollection(pgData, meetingCategoryPrice, "5");
                GetMeetingCategoryPriceCollection(pgData, meetingCategoryPrice, "6");
            }
            return meetingCategoryPrice;
        }


        private void GetMeetingCategoryPriceCollection(PageData pgData, System.Collections.Generic.List<MeetingCategoryPrice> meetingCategoryPrice, string number)
        {
            MeetingCategoryPrice categoryPrice = new MeetingCategoryPrice();
            string categoryData = "MeetingCategory" + number;
            string categoryPriceData = "MeetingCategoryPrice" + number;
            if (pgData[categoryData] != null)
            {
                categoryPrice = new MeetingCategoryPrice();
                categoryPrice.MeetingCategoryHotelPrice = Convert.ToString(pgData[categoryPriceData]);
                categoryPrice.MeetingCategoryPageData = GetPage((PageReference)pgData[categoryData]);
                meetingCategoryPrice.Add(categoryPrice);
            }
        }
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SearchResultListRepeater.ItemDataBound += new RepeaterItemEventHandler(SearchResultListRepeater_ItemDataBound);

            if (!IsPostBack)
            {
                PageDataCollection sessionPDC = MiscellaneousSessionWrapper.SearchedMeetingRoomsPDC as PageDataCollection ?? null;

                if (sessionPDC != null)
                {
                    BindData(sessionPDC);
                }
                else
                {
                    if (MiscellaneousSessionWrapper.SearchedCityCountryPage != null && MiscellaneousSessionWrapper.SearchedCapacity != null)
                    {
                        Capacity = (int)MiscellaneousSessionWrapper.SearchedCapacity;
                        CityCountrySearchPage = (PageData)MiscellaneousSessionWrapper.SearchedCityCountryPage;
                        CountrySearchPage = (PageData)MiscellaneousSessionWrapper.SearchedCountryPage;
                        PageDataCollection pdc = GetMeetingListFromSearch();
                        MiscellaneousSessionWrapper.SearchedMeetingRoomsPDC = pdc;
                        BindData(pdc);
                    }
                    else
                    {
                        MeetingSearchBottomPagination.Visible = false;
                        SearchResultListRepeater.Visible = false;
                        ltlNoMeetingRooms.Text = LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Static/SearchMeetingList/NoAvailableRooms");
                    }
                }
            }

            btnSendRFPs.Text = LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Static/SearchMeetingList/SendRFPToHotels");
        }


        private void SearchResultListRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var pageData = e.Item.DataItem as PageData;
                HotelDescription = string.Empty;
                if (pageData != null)
                {
                    HotelDescription = Convert.ToString(pageData["HotelDescription"]);
                    var numberOfMeetingRooms = e.Item.FindControl("numberOfMeetingRooms") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    var largestMeetingRooms = e.Item.FindControl("largestMeetingRoom") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    numberOfMeetingRooms.InnerHtml = Convert.ToString(GetMeetingRoomPages(pageData).Count);
                    var cityCentreLabel = e.Item.FindControl("cityCentreLabel") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    var sessionLang = MiscellaneousSessionWrapper.SessionLanguage;
                    if (sessionLang == null)
                        cityCentreLabel.InnerHtml = string.Format(EPiServer.Core.LanguageManager.Instance.Translate("/bookingengine/booking/selecthotel/citycenterdistance"), string.Empty, string.Empty);
                    else
                        cityCentreLabel.InnerHtml = string.Format(EPiServer.Core.LanguageManager.Instance.Translate("/bookingengine/booking/selecthotel/citycenterdistance", sessionLang), string.Empty, string.Empty);
                    PageDataCollection collection = GetMeetingRoomPages(pageData);
                    collection.Sort(new HotelsRoomArea());
                    largestMeetingRooms.InnerHtml = Convert.ToString(collection[collection.Count - 1]["Area"]);
                    Repeater meetingCategories = e.Item.FindControl("meetingCategories") as System.Web.UI.WebControls.Repeater;
                    meetingCategories.ItemDataBound += new RepeaterItemEventHandler(meetingCategories_ItemDataBound);
                    meetingCategories.DataSource = GetMeetingCategories(pageData, true);
                    meetingCategories.DataBind();

                    Repeater meetingCategoriesRight = e.Item.FindControl("meetingCategoriesRight") as System.Web.UI.WebControls.Repeater;
                    meetingCategoriesRight.ItemDataBound += new RepeaterItemEventHandler(meetingCategoriesRightCol_ItemDataBound);
                    List<MeetingCategoryPrice> meetingCategoryRight = GetMeetingCategories(pageData, false);
                    if (meetingCategoryRight != null && meetingCategoryRight.Count > 0)
                    {
                        var divMeetingCatReightCol = e.Item.FindControl("divMeetingCatRightCol") as System.Web.UI.HtmlControls.HtmlGenericControl;
                        divMeetingCatReightCol.Visible = true;
                        meetingCategoriesRight.DataSource = meetingCategoryRight;
                        meetingCategoriesRight.DataBind();
                    }

                    var cbSelectforRFP = e.Item.FindControl("cbSelectforRFP") as System.Web.UI.WebControls.CheckBox;
                    cbSelectforRFP.Text = LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Static/SearchMeetingList/SelectForRFP");
                }
            }
        }

        private void meetingCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                MeetingCategoryPrice pageData = e.Item.DataItem as MeetingCategoryPrice;
                if (pageData != null)
                {
                    var meetingCategory = e.Item.FindControl("meetingCategory") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    meetingCategory.InnerHtml = Convert.ToString(pageData.MeetingCategoryPageData["Heading"]);
                    if (!String.IsNullOrEmpty(Convert.ToString(pageData.MeetingCategoryPageData["MeetingMetaDescription"])))
                        meetingCategory.Attributes.Add("title", Convert.ToString(pageData.MeetingCategoryPageData["MeetingMetaDescription"]));
                    else
                        meetingCategory.Attributes.Add("title", HotelDescription);
                    var price = e.Item.FindControl("price") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    price.InnerHtml = pageData.MeetingCategoryHotelPrice;
                    var currency = e.Item.FindControl("currency") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    currency.InnerHtml = Convert.ToString(MiscellaneousSessionWrapper.CountryCurrency);
                }
            }
        }

        private void meetingCategoriesRightCol_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                MeetingCategoryPrice pageData = e.Item.DataItem as MeetingCategoryPrice;
                if (pageData != null)
                {
                    var meetingCategory = e.Item.FindControl("meetingCategoryRight") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (meetingCategory != null)
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(pageData.MeetingCategoryPageData["MeetingMetaDescription"])))
                            meetingCategory.Attributes.Add("title", Convert.ToString(pageData.MeetingCategoryPageData["MeetingMetaDescription"]));
                        else
                            meetingCategory.Attributes.Add("title", HotelDescription);

                        meetingCategory.InnerHtml = Convert.ToString(pageData.MeetingCategoryPageData["Heading"]);
                    }
                    var price = e.Item.FindControl("priceRight") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (price != null)
                        price.InnerHtml = pageData.MeetingCategoryHotelPrice;
                    var currency = e.Item.FindControl("currencyRight") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (currency != null)
                        currency.InnerHtml = Convert.ToString(MiscellaneousSessionWrapper.CountryCurrency);
                }
            }
        }

        /// <summary>
        /// BindData
        /// </summary>
        /// <param name="pdc"></param>
        private void BindData(PageDataCollection pdc)
        {
            if (pdc.Count > 0)
            {
                FilterSort sorter = new FilterSort(FilterSortOrder.Alphabetical);
                sorter.Sort(pdc);

                SearchResultListRepeater.DataSource = pdc;
                SearchResultListRepeater.DataBind();
                SearchResultPagingLiteral.Text = CreatePagingString(pdc.Count);
                if (pdc.Count > 10)
                {
                    SearchResultsShowAllHotels.Text =
                        string.Format(
                            "<a href=\"#\" onclick=\"MeetingSearchShowAll({0})\"><img src=\"/Templates/Scanweb/Styles/Default/Images/Icons/box_plus.gif\" alt=\" \" />&nbsp;Show all hotels</a>",
                            pdc.Count);
                    SearchResultsShowAllHotels.Visible = true;
                }
                else
                {
                    SearchResultsShowAllHotels.Visible = false;
                }

                MeetingGoogleMapEventArgs mGoogleMapEventArgs = new MeetingGoogleMapEventArgs();
                mGoogleMapEventArgs.GoogleMapsHotels = pdc;

                if (MeetingGoogleMapRender != null)
                {
                    MeetingGoogleMapRender(this, mGoogleMapEventArgs);
                }
                btnSendRFPs.Visible = true;
                btnSendRt.Visible = true;
                SearchResultListRepeater.Visible = true;
                ltlNoMeetingRooms.Visible = false;
                MeetingSearchBottomPagination.Visible = true;
            }
            else
            {
                btnSendRFPs.Visible = false;
                btnSendRt.Visible = false;
                SearchResultListRepeater.Visible = false;
                ltlNoMeetingRooms.Visible = true;
                MeetingSearchBottomPagination.Visible = false;
                ltlNoMeetingRooms.Text =
                    LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Units/Static/SearchMeetingList/NoAvailableRooms");
            }
        }

        /// <summary>
        /// GetMeetingListFromSearch
        /// </summary>
        /// <returns>PageDataCollection</returns>
        private PageDataCollection GetMeetingListFromSearch()
        {
            #region Variables

            PageDataCollection pdc = GetChildren(CityCountrySearchPage.PageLink);
            PageDataCollection pdcCities = new PageDataCollection();
            PageDataCollection pdcHotelContainers = new PageDataCollection();
            PageDataCollection pdcHotels = new PageDataCollection();
            PageDataCollection pdcMeetingRoomContainers = new PageDataCollection();
            PageDataCollection pdcMeetingRooms = new PageDataCollection();

            int capacity1, capacity2, capacity3, capacity4;
            int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
            int hotelContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelContainerPageTypeGUID"])).ID;
            int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
            int meetingRoomContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["MeetingRoomContainerPageTypeGUID"])).ID;
            int meetingRoomPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["MeetingRoomPageTypeGUID"])).ID;

            #endregion

            if (CityCountrySearchPage.PageTypeID == hotelPageTypeID)
                pdc.Add(CityCountrySearchPage);

            foreach (PageData pd in pdc)
            {
                if (pd.PageTypeID == cityPageTypeID)
                    pdcCities.Add(pd);

                if (pd.PageTypeID == hotelContainerPageTypeID)
                    pdcHotelContainers.Add(pd);

                if (pd.PageTypeID == hotelPageTypeID)
                    pdcHotels.Add(pd);
            }

            #region Cities

            if (pdcCities.Count != 0)
            {
                foreach (PageData pdCity in pdcCities)
                {
                    foreach (PageData pdHotelContainer in RemovePagesFromContainer(GetChildren(pdCity.PageLink)))
                        if (pdHotelContainer.PageTypeID == hotelContainerPageTypeID)
                            pdcHotelContainers.Add(pdHotelContainer);
                }
            }

            #endregion

            #region HotelContainers

            if (pdcHotelContainers.Count != 0 || pdcHotels.Count != 0)
            {
                PageDataCollection pdcToRemove = new PageDataCollection();

                foreach (PageData pdHotelContainer in pdcHotelContainers)
                {
                    pdcHotels.Add(RemovePagesFromContainer(GetChildren(pdHotelContainer.PageLink)));
                }

                foreach (PageData pdHotel in pdcHotels)
                {
                    foreach (PageData pdMeetingRoomContainer in RemovePagesFromContainer(GetChildren(pdHotel.PageLink)))
                        if (pdMeetingRoomContainer.PageTypeID == meetingRoomContainerPageTypeID)
                            pdcMeetingRoomContainers.Add(pdMeetingRoomContainer);
                }

                foreach (PageData pdMeetingRooms in pdcMeetingRoomContainers)
                {
                    foreach (PageData pdMeetingRoom in RemovePagesFromContainer(GetChildren(pdMeetingRooms.PageLink)))
                        if (pdMeetingRoom.PageTypeID == meetingRoomPageTypeID)
                            pdcMeetingRooms.Add(pdMeetingRoom);
                }

                foreach (PageData pdMeetingRoom in pdcMeetingRooms)
                {
                    capacity1 = (pdMeetingRoom["CapacityUShape"] != null ? (int)pdMeetingRoom["CapacityUShape"] : 0);
                    capacity2 = (pdMeetingRoom["CapacityClassroom"] != null
                                     ? (int)pdMeetingRoom["CapacityClassroom"]
                                     : 0);
                    capacity3 = (pdMeetingRoom["CapacityTheatre"] != null ? (int)pdMeetingRoom["CapacityTheatre"] : 0);
                    capacity4 = (pdMeetingRoom["CapacityBoardRoom"] != null
                                     ? (int)pdMeetingRoom["CapacityBoardRoom"]
                                     : 0);

                    if (capacity1 < Capacity && capacity2 < Capacity && capacity3 < Capacity && capacity4 < Capacity)
                        pdcToRemove.Add(pdMeetingRoom);
                }

                foreach (PageData pdToRemove in pdcToRemove)
                {
                    pdcMeetingRooms.Remove(pdToRemove);
                }
            }

            #endregion

            PageData hotelPage;
            PageDataCollection pdcHotelPages = new PageDataCollection();

            foreach (PageData meetingRoom in pdcMeetingRooms)
            {
                hotelPage = GetPage(GetPage(meetingRoom.ParentLink).ParentLink);
                if (!pdcHotelPages.Contains(hotelPage))
                    pdcHotelPages.Add(hotelPage);
            }

            return pdcHotelPages;
        }

        /// <summary>
        /// RemovePagesFromContainer
        /// </summary>
        /// <param name="pdc"></param>
        /// <returns>PageDataCollection</returns>
        private PageDataCollection RemovePagesFromContainer(PageDataCollection pdc)
        {
            for (int i = pdc.Count - 1; i >= 0; i--)
            {
                bool pageIsPublished = pdc[i].CheckPublishedStatus(PagePublishedStatus.Published);
                bool hasReadAccess = pdc[i].QueryDistinctAccess(AccessLevel.Read);

                if (!(pageIsPublished && hasReadAccess))
                    pdc.Remove(pdc[i]);
            }
            return pdc;
        }

        /// <summary>
        /// GetImageFromImageVault
        /// </summary>
        /// <param name="meetingPage"></param>
        /// <returns>PropertyData</returns>
        protected PropertyData GetImageFromImageVault(PageData meetingPage, int imageWidth)
        {
            PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET", "ImageStoreNET.ImageType");
            string strImage = meetingPage["MeetingImage"] as string ?? String.Empty;

            if (!String.IsNullOrEmpty(strImage))
            {
                pd.Value = WebUtil.GetImageVaultImageUrl(strImage, imageWidth);
            }

            return pd;
        }

        /// <summary>
        /// Gets the alt text from ImageVault in the correct langauge
        /// </summary>
        /// <returns>alt text from ImageVault</returns>
        protected string GetAltText(PageData meetingPage)
        {
            LangAltText altText = new LangAltText();

            if (meetingPage["MeetingImage"] != null)
            {
                return altText.GetAltText(meetingPage.LanguageID, meetingPage["MeetingImage"].ToString());
            }

            return "";
        }

        /// <summary>
        /// btnSendRFPs_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSendRFPs_Click(object sender, EventArgs e)
        {
            Dictionary<int, string> selectedHotelsDictionary = new Dictionary<int, string>();

            foreach (RepeaterItem rpItem in SearchResultListRepeater.Items)
            {
                Literal lit = rpItem.FindControl("ltlPageLinkID") as Literal;

                PageData pd = new PageData();

                if (!String.IsNullOrEmpty(lit.Text))
                    pd = GetPage(new PageReference(int.Parse(lit.Text)));

                CheckBox chkbx = rpItem.FindControl("cbSelectforRFP") as CheckBox;

                if (chkbx.Checked)
                {
                    selectedHotelsDictionary.Add(pd.PageLink.ID,
                                                 pd["Heading"] as string ??
                                                 String.Empty + ", " + pd["City"] as string ?? String.Empty);
                }
            }

            MiscellaneousSessionWrapper.MeetingsSelectedHotels = selectedHotelsDictionary;
            UserNavTracker.TrackAction(this, "Proposal To Selected Hotel");

            PageData pageRFP = DataFactory.Instance.GetPage(RootPage["RequestForPorposalPage"] as PageReference);
            string LinkToRFPage = pageRFP.LinkURL.ToString();
            Response.Redirect(LinkToRFPage);
        }

        #region INavigationTraking Members

        public List<KeyValueParam> GenerateInput(string actionName)
        {
            List<KeyValueParam> parameters = new List<KeyValueParam>();
            Dictionary<int, string> selectedHotelsDictionary = MiscellaneousSessionWrapper.MeetingsSelectedHotels as Dictionary<int, string>;
            if (selectedHotelsDictionary != null)
                foreach (KeyValuePair<int, string> item in selectedHotelsDictionary)
                    parameters.Add(new KeyValueParam(string.Format("For Hotel Id{0}", item.Key.ToString()), item.Value));
            return parameters;
        }

        #endregion
    }

    /// <summary>
    /// Eventargs for Meeting google map
    /// </summary>
    public class MeetingGoogleMapEventArgs : EventArgs
    {
        public PageDataCollection GoogleMapsHotels { get; set; }
    }
}

public class HotelsRoomArea : IComparer<PageData>
{
    public int Compare(PageData first, PageData second)
    {
        return Convert.ToInt32(first["Area"]).CompareTo(Convert.ToInt32(second["Area"]));
    }
}
public class MeetingCategoryPrice
{
    public string MeetingCategoryHotelPrice { get; set; }
    public PageData MeetingCategoryPageData { get; set; }
}
