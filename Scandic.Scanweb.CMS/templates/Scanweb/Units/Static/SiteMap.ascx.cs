using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// SiteMap
    /// </summary>
    public partial class SiteMap : ScandicUserControlBase
    {
        private PageReference _indexRoot;

        /// <summary>
        /// Gets the page used as the root for the site map
        /// </summary>
        /// <remarks>If the IndexRoot page property is not set the start page will be used instead</remarks>
        public PageReference IndexRoot
        {
            get
            {
                if (PageReference.IsNullOrEmpty(_indexRoot))
                {
                    _indexRoot = CurrentPage["IndexRoot"] as PageReference ?? PageReference.StartPage;
                }
                return _indexRoot;
            }
        }

        /// <summary>
        /// Over ride Onload
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            SiteMapList.DataBind();
        }

        /// <summary>
        /// Should be visible
        /// </summary>
        /// <param name="page">Page</param>
        /// <returns>visiblilty</returns>
        protected bool ShouldBeVisible(PageData page)
        {
            return (page.VisibleInMenu && DataFactory.Instance.GetChildren(page.PageLink).Count > 0);
        }
    }
}