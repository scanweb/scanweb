//  Description					: SelectHotelGoogleMapIframe                              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Code behind of SelectHotelGoogleMapIframe control.
    /// </summary>
    public partial class SelectHotelGoogleMapIframe : System.Web.UI.UserControl
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.Request.Url.ToString().StartsWith("https"))
            {
                this.Visible = false;
            }
        }

        public void initializeGoogleMaps()
        {

        }
    }
}