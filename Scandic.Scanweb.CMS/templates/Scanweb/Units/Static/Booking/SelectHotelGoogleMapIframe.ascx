<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectHotelGoogleMapIframe.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.SelectHotelGoogleMapIframe" %>
<%@ Register TagPrefix="Scanweb" TagName="SelectHotelGoogleMap" Src="~/Templates/Scanweb/Units/Static/Booking/SelectHotelGoogleMap.ascx" %>

<div id="SelectHotelGoogleMap">
   <iframe id="SelectHotelGoogleMapIframe"  width="718px" height="399px" scrolling="no" frameborder="0"></iframe>
</div>

