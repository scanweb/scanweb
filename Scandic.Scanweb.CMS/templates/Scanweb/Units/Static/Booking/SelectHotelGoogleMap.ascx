<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectHotelGoogleMap.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.SelectHotelGoogleMap" %>
<%@ Register TagPrefix="GoogleMapV3" Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3" %>


<div id="SelectHotelMapTabGoogleMap">
 <div ID="GMapV3" style="Width:718px; height:399px" ></div>
 <GoogleMapV3:Map ID="GoogleMapControl" runat="server" />
</div>