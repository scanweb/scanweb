//  Description					:   SelectHotelGoogleMap                                  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Configuration;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.Map;
using Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// This class contains members to renders the hotels on google map.
    /// </summary>
    public partial class SelectHotelGoogleMap : ScandicUserControlBase
    {
        /// <summary>
        /// Handles the PreRender event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            SetGoogleMapOptionsAndData();
        }

        public void initializeGoogleMap()
        {

            SetGoogleMapOptionsAndData();
        }

        /// <summary>
        public bool isDelayLoad
        {
            get;
            set;
        }
        /// Sets the google map options and data.
        /// </summary>
        private void SetGoogleMapOptionsAndData()
        {
            if (Page.Request.Url.ToString().StartsWith("https") || SelectHotelUtil.IsSearchToBeDone() == true)
            {
                this.Visible = false;
            }
            else
            {
                this.Visible = true;
                List<Dictionary<string, object>> foundHotels =
                    GoogleMapHotelResultsSessionWrapper.GoogleMapResults;

                IList<MapUnit> hotels = new List<MapUnit>();

                HotelSearchEntity hotelSearch = null;
                if (null != SearchCriteriaSessionWrapper.SearchCriteria)
                    hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;

                //Merchandising:R3:Display ordinary rates for unavailable promo rates
                if (hotelSearch != null && hotelSearch.CampaignCode != null && hotelSearch.SearchingType == SearchType.REGULAR)
                {
                    if (HotelResultsSessionWrapper.HotelDetails != null)
                    {
                        Dictionary<string, object> googleMapsEntry = new Dictionary<string, object>();
                        googleMapsEntry.Add(AppConstants.GM_HOTEL_NAME, HotelResultsSessionWrapper.HotelDetails.HotelDestination.Name);
                        googleMapsEntry.Add(AppConstants.GM_HOTEL_ADDRESS, HotelResultsSessionWrapper.HotelDetails.HotelDestination.HotelAddress.ToString());
                        googleMapsEntry.Add(AppConstants.GM_LANDING_PAGE_URL, HotelResultsSessionWrapper.HotelDetails.HotelDestination.HotelPageURL);
                        googleMapsEntry.Add(AppConstants.GM_BOOKING_PAGE_URL, "http://www.scandic-hotels.com");
                        googleMapsEntry.Add(AppConstants.GM_LONGITUDE_X,
                                            (HotelResultsSessionWrapper.HotelDetails.HotelDestination.Coordinate != null
                                                 ? HotelResultsSessionWrapper.HotelDetails.HotelDestination.Coordinate.X.ToString()
                                                 : string.Empty));
                        googleMapsEntry.Add(AppConstants.GM_LATITUDE_Y,
                                            (HotelResultsSessionWrapper.HotelDetails.HotelDestination.Coordinate != null
                                                 ? HotelResultsSessionWrapper.HotelDetails.HotelDestination.Coordinate.Y.ToString()
                                                 : string.Empty));
                        googleMapsEntry.Add(AppConstants.GM_HOTEL_ID, HotelResultsSessionWrapper.HotelDetails.HotelDestination.OperaDestinationId);

                        googleMapsEntry.Add(AppConstants.GM_IHOTELDETAILS, HotelResultsSessionWrapper.HotelDetails);

                        foundHotels.Add(googleMapsEntry);
                    }
                }


                if (foundHotels != null)
                {
                    foreach (Dictionary<string, object> d in foundHotels)
                    {
                        object hotelID = string.Empty;
                        d.TryGetValue("HotelID", out hotelID);

                        object hotelDetailsObj = null;
                        d.TryGetValue(AppConstants.GM_IHOTELDETAILS, out hotelDetailsObj);
                        IHotelDetails hotelDetails = hotelDetailsObj as IHotelDetails;

                        IHotelDisplayInformation hotelDisplayPerNight =
                            SelectHotelUtil.GetHotelDisplayObject(hotelSearch.SearchingType, hotelDetails,
                                                                  hotelSearch.CampaignCode, false, false);
                        IHotelDisplayInformation hotelDisplayPerStay =
                            SelectHotelUtil.GetHotelDisplayObject(hotelSearch.SearchingType, hotelDetails,
                                                                  hotelSearch.CampaignCode, false, true);

                        if (hotelDisplayPerNight != null && hotelDisplayPerStay != null
                            && !string.IsNullOrEmpty(hotelDisplayPerNight.Price) &&
                            !string.IsNullOrEmpty(hotelDisplayPerStay.Price))
                        {
                            string perNight = string.Empty;
                            string perStay = string.Empty;
                            string currentLanguage = Utility.GetCurrentLanguage();

                            if (hotelSearch.SearchingType.Equals(SearchType.BONUSCHEQUE) ||
                                hotelSearch.SearchingType.Equals(SearchType.REDEMPTION))
                            {
                                perNight = "<div class=\"gmapRates formatText\">" +
                                           WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from",
                                                                     currentLanguage) +
                                           "<br/><strong>" + hotelDisplayPerNight.Price + AppConstants.SPACE +
                                           "</strong></div>";
                                perStay = "<div class=\"gmapRates formatText\">" +
                                          WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from",
                                                                    currentLanguage) +
                                          "<br/><strong>" + hotelDisplayPerStay.Price + AppConstants.SPACE +
                                          "</strong></div>";
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(hotelDisplayPerNight.RateTitle))
                                {
                                    perNight = "<div class=\"gmapRates\">" +
                                               WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from",
                                                                         currentLanguage) +
                                               "<br/><strong>" + hotelDisplayPerNight.Price + AppConstants.SPACE +
                                               "</strong></div>";
                                    perStay = "<div class=\"gmapRates\">" +
                                              WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from",
                                                                        currentLanguage) +
                                              "<br/><strong>" + hotelDisplayPerStay.Price + AppConstants.SPACE +
                                              "</strong></div>";
                                }
                                else
                                {
                                    //Merchandising:R3:Display ordinary rates for unavailable promo rates (Destination)
                                    //string truncatedName = string.Empty;
                                    //if (!string.IsNullOrEmpty(hotelDisplayPerNight.RateTitle))
                                    //    truncatedName = Utility.TruncateAndAppendDots(hotelDisplayPerNight.RateTitle, 12);
                                    perNight = "<div class=\"gmapRates\">" + "<span class=\"dNumberMapIcon\">" +
                                               "<div class=\"dNumberDisplay\">" + hotelDisplayPerNight.RateTitle + "</div>" + 
                                               "</span>" +
                                               WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from",
                                                                         currentLanguage) +
                                               "<br/><strong>" + hotelDisplayPerNight.Price + AppConstants.SPACE +
                                               "</strong></div>";
                                    perStay = "<div class=\"gmapRates\">" + "<span class=\"dNumberMapIcon\">" +
                                              "<div class=\"dNumberDisplay\">" + hotelDisplayPerNight.RateTitle + "</div>" + 
                                              "</span>" +
                                              WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/from",
                                                                        currentLanguage) +
                                              "<br/><strong>" + hotelDisplayPerStay.Price + AppConstants.SPACE +
                                              "</strong></div>";
                                }
                            }
                            if (hotelID != null || hotelID as string != string.Empty)
                            {
                                try
                                {
                                    string imagePath = string.Empty;
                                    imagePath =
                                        "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_selecthotel.png";
                                    InfoBoxType currentInfoBox = InfoBoxType.ADVANCED_SELECTHOTEL;
                                    if (!String.IsNullOrEmpty(hotelDisplayPerNight.RateTitle))
                                    {
                                        imagePath =
                                            "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_selecthotel_DNumber.png";
                                        currentInfoBox = InfoBoxType.ADVANCED_SELECTHOTEL_DNUMBER;
                                    }

                                    HotelDestination h = ContentDataAccess.GetHotelByOperaID(hotelID.ToString());
                                    if (h != null)
                                    {
                                        MapUnit g = new MapHotelUnit(
                                            h.Coordinate.X,
                                            h.Coordinate.Y,
                                            -1,
                                            string.Format("http://{0}/{1}", Request.Url.Host, imagePath),
                                            string.Empty,
                                            h.Name,
                                            h.ImageURL,
                                            h.HotelDescription,
                                            h.HotelAddress.StreetAddress,
                                            h.HotelAddress.PostCode,
                                            h.HotelAddress.PostalCity,
                                            h.HotelAddress.City,
                                            h.HotelAddress.Country,
                                            h.HotelPageURL,
                                            GetDeepLinkingURL(hotelID as string ?? string.Empty),
                                            h.CityCenterDistance,
                                            currentInfoBox,
                                            false,
                                            0,
                                            hotelID as string,
                                            h.HotelCategory,
                                            string.Empty,
                                            string.Empty,
                                            false,
                                            true,
                                            MarkerRateTextType.RATEPERNIGHT,
                                            string.Empty,
                                            perNight,
                                            perStay
                                            );

                                        hotels.Add(g);
                                    }
                                }
                                catch (ContentDataAccessException)
                                {
                                }
                            }
                        }
                    }
                }
                GoogleMapControl.GoogleMapKey = (ConfigurationManager.AppSettings["googlemaps." + Request.Url.Host] as string ?? (ConfigurationManager.AppSettings["DevKey"] as string));
                GoogleMapControl.MarkerLatitudeField = "latitude";
                GoogleMapControl.MarkerLongitudeField = "longitude";
                GoogleMapControl.InfoBoxType = InfoBoxType.ADVANCED;
                GoogleMapControl.MapPageType = MapPageType.SELECTHOTEL;

                if (hotels.Count == 1)
                {
                    GoogleMapControl.Latitude = hotels[0].Latitude;
                    GoogleMapControl.Longitude = hotels[0].Longitude;
                }
                GoogleMapControl.DataSource = hotels;

                GoogleMapControl.DataBind();
            }
        }
    }
}