using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.WebControls;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    public partial class BookingGoogleMap : EPiServer.UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            // Try to retrieve from session
            List<Dictionary<string,string>> foundHotels = Session["BE_GoogleMapHotels"] as List<Dictionary<string,string>>;
            if (foundHotels!=null){
              foreach(Dictionary<string, string> d in foundHotels){
                  TestLiteral.Text = TestLiteral.Text + d["HotelName"]+ "(" + d["X"] +")<br /> ";

              }
            }


        }
    }
}