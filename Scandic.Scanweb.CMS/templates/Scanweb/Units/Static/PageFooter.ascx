<%@ Control Language="C#" AutoEventWireup="False" CodeBehind="PageFooter.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.PageFooter" %>
<div id="footerWrapper">
    <div class="newFooter">
        <div class="doubleCol">
            <!-- Column 1 Display start -->
            <div class="singleCol flushMarginLeft">
                <div id="abtUs">
                    <p class="newFooterTitle">
                       <%-- <asp:PlaceHolder ID="AboutScandicStoolImagePlaceHolder" Visible="false" runat="server">
                            <asp:Image ID="AboutScandicStoolImage" runat="server"></asp:Image>
                        </asp:PlaceHolder>--%>
                         <asp:PlaceHolder ID="AboutScandicHeadingPlaceHolder" Visible="false" runat="server">
                            <asp:Literal ID="AboutScandicHeading" runat="server"></asp:Literal>
                        </asp:PlaceHolder>
                    </p>
                    <div class="padTop15">
                        <asp:PlaceHolder ID="AboutScandicTextAreaPlaceHolder" Visible="false" runat="server">
                            <asp:Literal ID="AboutScandicTextArea" runat="server"></asp:Literal>
                        </asp:PlaceHolder>
                    </div>
                </div>
            </div>
            <!-- Column 1 Display end -->
            <!-- Column 2 Display start -->
            <div class="singleCol clearMgn">
                <!-- Column 2 top Display start -->
                <div id="abtLinks">
                    <ul class="list" id="AboutUsPagePageList" runat="server">
                        <asp:Repeater runat="server" ID="rptAboutUsContainerFooter">
                            <ItemTemplate>
                                <li>
                                    <asp:HyperLink runat="server" ID="lnkAboutUsContainerFooter"></asp:HyperLink>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <!-- Column 2 top Display end -->
                <!-- Column 2 bottom Display start -->
                <asp:PlaceHolder ID="Col2LinksPlaceHolder" Visible="true" runat="server"></asp:PlaceHolder>
                <!-- Column 2 bottom Display end -->
            </div>
            <!-- Column 2 Display end -->
        </div>
        <!-- Column 3 Display start -->
        <div class="singleCol flushMarginLeft">
            <asp:PlaceHolder ID="Col3LinksPlaceHolder" Visible="true" runat="server"></asp:PlaceHolder>
        </div>
        <!-- Column 3 Display end -->
        <!-- Column 4 Display start -->
        <div class="singleCol flushMarginRight flushMarginLeft">
            <!-- FGP Display start-->
            <div id="faqContainer">
                <p class="newFooterTitle">
                   <%-- <asp:PlaceHolder ID="FGPStoolImagePlaceHolder" Visible="false" runat="server">
                        <asp:Image ID="FGPStoolImage" runat="server" />
                    </asp:PlaceHolder>--%>
                    <asp:PlaceHolder ID="FGPBecomeAMemberHeaderPlaceHolder" Visible="false" runat="server">
                        <asp:Literal ID="FGPBecomeAMemberHeader" runat="server"></asp:Literal>
                    </asp:PlaceHolder>
                </p>
                <ul style="padding-top:15px;">
                    <asp:PlaceHolder ID="FGPTextAreaPlaceHolder" Visible="false" runat="server">
                        <asp:Literal ID="FGPTextArea" runat="server"></asp:Literal>
                    </asp:PlaceHolder>
                </ul>
                <div class="wrapper">
                    <div class="usrBtn">
                        <a id="lnkSignUpFGP" target="_top" runat="server" class="buttonInner"></a>
                    </div>
                </div>
            </div>
            <!-- FGP Display End-->
            <!-- New Hotels Display start-->
            <div class="linksWrapper clear" runat="server">
                <p class="newFooterTitle">
                   <%-- <asp:PlaceHolder ID="NewHotelsStoolImagePlaceHolder" Visible="false" runat="server">
                        <asp:Image ID="NewHotelsStoolImage" runat="server" />
                    </asp:PlaceHolder>--%>
                    <asp:PlaceHolder ID="NewHotelsHeadingPlaceHolder" Visible="false" runat="server">
                        <asp:Literal ID="NewHotelsHeading" runat="server" />
                    </asp:PlaceHolder>
                </p>
                <div class="siteContainer" runat="server" id="ContainerDescription">
                    <asp:Literal ID="Description" runat="server" Visible="true" /></div>
                <ul class="list last" id="NewHotelsList" runat="server">
                    <asp:Repeater runat="server" ID="rptNewHotelsPageList">
                        <ItemTemplate>
                            <li>
                                <asp:HyperLink runat="server" ID="lnkNewHotel"></asp:HyperLink>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                <asp:PlaceHolder ID="NewHotelsLinksPlaceHolder" Visible="true" runat="server">
                    <ul class="list" id="NewHotelsLinks" runat="server">
                        <li><strong><a id="ExpansionPageLink" runat="server"></a></strong></li>
                    </ul>
                </asp:PlaceHolder>
            </div>
            <!-- New Hotels Display end-->
        </div>
        <!-- Column 4 Display end -->
    </div>
</div>
