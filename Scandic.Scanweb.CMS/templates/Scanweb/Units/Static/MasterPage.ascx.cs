using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.WebControls;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// MasterPage
    /// </summary>
    public partial class MasterPage : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
           
            TopMenuCtrl.DataBind();
            if (SubMenuCtrl!= null)
            {
                SubMenuCtrl.PageLink = TopMenuCtrl.OpenTopPage;
                SubMenuCtrl.DataBind();
                

            }

        }
    }
}