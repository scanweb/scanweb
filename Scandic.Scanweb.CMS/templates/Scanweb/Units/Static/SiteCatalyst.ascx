<%@ Control Language="C#" AutoEventWireup="False" CodeBehind="SiteCatalyst.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.SiteCatalyst" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>


<!-- SiteCatalyst code version: H.14. Copyright 1997-2007 Omniture, Inc. More info available at http://www.omniture.com -->

<!-- Web Analythics JavaScripts -->
<script type="text/javascript" language="javascript" src="/Templates/Scanweb/Javascript/SiteCatalyst/s_code.js?v=<%=CmsUtil.GetJSVersion()%>"></script>
<!-- End Web Analythics JavaScripts -->


        
<script type="text/javascript" language="JavaScript">

/*  All pages   */
s.pageName="<%# GetPageName() %>";   
s.channel="<%# GetChannel() %>";
s.prop20="<%# GetProp20() %>";

/*  Set on Booking detail page and Confirmation page  */
s.products="<%# GetProducts() %>";

/*  Search Result Page PageTypeID =  */
s.prop11="<%# GetProp("11") %>";/*--Search location--*/              
s.prop12="<%# GetProp("12") %>"; /*--Search # nights--*/               
s.prop13="<%# GetProp("13") %>"; /*--Search # rooms--*/
s.prop14="<%# GetProp("14") %>"; /*--Search arv dates--*/
s.prop15="<%# GetProp("15") %>"; /*--Search # adults--*/
s.prop16="<%# GetProp("16") %>"; /*--Search # children--*/
s.prop17="<%# GetProp("17") %>"; /*--Search arv date - dep date--*/
s.prop21="<%# GetProp("21") %>"; /*-FGP Access--*/
s.prop22="<%# GetProp("22") %>"; /*--Page on FGP Access--*/
s.prop24="<%# GetProp("24") %>"; /*--Sanjetha : Site_Expansion : SiteCatalyst :Gets the Page Name in current Locale being set in this property --*/
s.prop34="<%# GetProp("34") %>"; /*--FGP Logged In/Logged Out--*/

s.eVar11="<%# GetEvar("11") %>"; /*--Search locations--*/
s.eVar12="<%# GetEvar("12") %>"; /*--Search # nights--*/
s.eVar13="<%# GetEvar("13") %>"; /*--Search # rooms--*/
s.eVar14="<%# GetEvar("14") %>"; /*--Search arv date--*/
s.eVar15="<%# GetEvar("15") %>"; /*--Search # adults--*/ 
s.eVar16="<%# GetEvar("16") %>"; /*--Search # children--*/
s.eVar17="<%# GetEvar("17") %>"; /*--Search arv date - dep date--*/

/*  Type of tracking  */
s.events="<%# GetEvent() %>";

/*  Confirmation Page   */
s.purchaseID="<%# GetPurchaseID() %>";   
s.eVar4="<%# GetEvar("4") %>"; /*--Booking window --*/ 
s.eVar5="<%# GetEvar("5") %>"; /*--Booking dep date --*/
s.eVar6="<%# GetEvar("6") %>"; /*--Booking trip duration --*/
s.eVar8="<%# GetEvar("8") %>"; /*--Rooms per night --*/ 
s.eVar25="<asp:Literal ID="eVar25Literal" runat="server" />"; /*--Payment method--*/
s.eVar26="<asp:Literal ID="eVar26Literal" runat="server" />"; /*--Booking type value--*/
s.eVar27="<asp:Literal ID="eVar27Literal" runat="server" />"; /*--Booking type code--*/
s.currencyCode="<asp:Literal ID="currencyCodeLiteral" runat="server" />";

/*  Campaign Landing Page   */
s.eVar2="<%# GetEvar("2") %>";

/*  Thank You Page  */
s.state="<%# GetState() %>"; /*--User country--*/ 
s.zip="<%# GetZip() %>"; /*--Post code--*/ 

/*  Booking change page  */
s.eVar20="<%# GetEvar("20") %>";  

/*  Confirm cancellation page  */
s.eVar21="<%# GetEvar("21") %>"; 

s.eVar28 = "<%# GetEvar("28") %>";

/* START - R 1.6.1 - Site Catalyst - Offer Page Tracking */
s.eVar29="<%= GetEvar("29") %>";

/*Start-R1.8--Site Catalyst----Country of source origin */
s.eVar30="<%= GetEvar("30") %>";

/*Start 2.0-Track for FGP user-----*/
s.eVar34="<%= GetEvar("34") %>";

/*Start 2.0-Track for Price calandar in select Room page-----*/
s.eVar37="<%= GetEvar("37") %>";

/*Start 2.0-Track for If enroll check box is checked in booking details page-----*/
s.eVar38="<%= GetEvar("38") %>";

/*Start 2.0-Track for membership no if user is logged in-----*/
s.eVar61="<%= GetEvar("61") %>";


/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_code=s.t();if(s_code)document.write(s_code)//--></script>
<script type="text/javascript" language="JavaScript"><!--
if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
//--></script><noscript><a href="http://www.omniture.com" title="Web Analytics"><img
src="http://schabdev.112.2O7.net/b/ss/schabdev/1/H.14--NS/0?[AQB]&cdp=3&[AQE]"
height="1" width="1" border="0" alt="" /></a></noscript><!--/DO NOT REMOVE/-->
<!-- End SiteCatalyst code version: H.14. -->

<% ResetFlags(); %>