//  Description					: MeetingSearchGoogleMap                                  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.Map;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Code behind of MeetingSearchGoogleMap control.
    /// </summary>
    public partial class MeetingSearchGoogleMap : ScandicUserControlBase
    {
        /// <summary>
        /// Internal event method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void GoogleMapRender(object sender, MeetingGoogleMapEventArgs args)
        {
            if (args.GoogleMapsHotels != null && (!Page.Request.Url.ToString().StartsWith("https")))
            {
                this.Visible = true;

                PageDataCollection hotelPages = args.GoogleMapsHotels;

                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;

                IList<MapUnit> hotels = new List<MapUnit>();

                foreach (PageData hotelPage in hotelPages)
                {
                    if (hotelPage.CheckPublishedStatus(PagePublishedStatus.Published) &&
                        hotelPage.PageTypeID == hotelPageTypeID)
                    {
                        string hotelHeading = hotelPage["Heading"] as string ?? string.Empty;
                        double geoX = (double) hotelPage["GeoX"];
                        double geoY = (double) hotelPage["GeoY"];

                        MapHotelUnit g = new MapHotelUnit(
                            (double) hotelPage["GeoX"],
                            (double) hotelPage["GeoY"],
                            -1,
                            string.Format("http://{0}/{1}", Request.Url.Host,
                                          "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png"),
                            string.Empty,
                            hotelPage["Heading"] as string,
                            "",
                            hotelPage["StreetAddress"] as string,
                            hotelPage["PostCode"] as string,
                            hotelPage["PostalCity"] as string,
                            hotelPage["City"] as string,
                            hotelPage["Country"] as string,
                            GetFriendlyURL(hotelPage.PageLink, hotelPage.LinkURL) as string,
                            GetDeepLinkingURL(hotelPage["OperaID"] as string ?? string.Empty)
                            );

                        hotels.Add(g);
                    }
                }
                GMapControl1.GoogleMapKey = (ConfigurationManager.AppSettings["googlemaps." + Request.Url.Host] as string ?? (ConfigurationManager.AppSettings["DevKey"] as string));
                GMapControl1.DataSource = hotels;
                GMapControl1.MarkerLatitudeField = "latitude";
                GMapControl1.MarkerLongitudeField = "longitude";
                GMapControl1.AutoCenterAndZoom();
                if (hotels.Count == 1)
                {
                    GMapControl1.Latitude = hotels[0].Latitude;
                    GMapControl1.Longitude = hotels[0].Longitude;
                }
                GMapControl1.DataBind();
            }
            else
            {
                this.Visible = false;
            }
        }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (GMapControl1.DataSource == null || Page.Request.Url.ToString().StartsWith("https"))
                this.Visible = false;
        }

        /// <summary>
        /// Gets friendly url.
        /// </summary>
        /// <param name="pageLink"></param>
        /// <param name="linkURL"></param>
        /// <returns></returns>
        protected string GetFriendlyURL(PageReference pageLink, string linkURL)
        {
            UrlBuilder url = new UrlBuilder(linkURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, pageLink, System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }
    }
}