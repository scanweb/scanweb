<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageFooterLinks.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.PageFooterLinks" %>
<p class="newFooterTitle">
   <%--<asp:Image ID="HeaderImage" runat="server" Visible="true" />--%>
    <asp:Literal ID="HeaderText" runat="server" Visible="true" />
</p>
<div class="siteContainer" runat="server" id="ContainerDescription">
    <asp:Literal ID="Description" runat="server" Visible="true" /></div>
<ul class="list" id="FooterLinks" runat="server" visible="true">
    <asp:Repeater runat="server" ID="rptQuickLinksAndCustomerService">
        <ItemTemplate>
            <li>
                <asp:HyperLink runat="server" ID="lnkQuickLinksAndCustomerService"></asp:HyperLink>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>
