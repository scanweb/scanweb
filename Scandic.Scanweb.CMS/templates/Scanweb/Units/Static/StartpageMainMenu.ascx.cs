//  Description					:   StartpageMainMenu                                     //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.WebControls;


namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// Lists the pages visible in the main (top) menu.
    /// </summary>
    public partial class StartpageMainMenu : UserControlBase
    {
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            rptMainMenu.ItemDataBound += new RepeaterItemEventHandler(rptMainMenu_ItemDataBound);     
            rptMainMenu.DataSource = LoadChildren(PageReference.StartPage);
            rptMainMenu.DataBind();
        }

        protected void rptMainMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {  
                PageData pageData = e.Item.DataItem as PageData;
                HyperLink mainMenuItemLink = e.Item.FindControl("mainMenuItemLink") as HyperLink;
                if ((pageData != null) && (mainMenuItemLink != null))
                {
                    mainMenuItemLink.Text = pageData.PageName;
                    mainMenuItemLink.NavigateUrl = pageData.LinkURL;
                }

            }
        }

        /// <summary>
        /// Creates the collection for the main menu, adding the startpage
        /// </summary>
        private PageDataCollection LoadChildren(PageReference pageLink)
        {
            PageDataCollection pages = DataFactory.Instance.GetChildren(pageLink);
            pages.Insert(0, DataFactory.Instance.GetPage(pageLink));
            return pages;
        }
       
    }
}