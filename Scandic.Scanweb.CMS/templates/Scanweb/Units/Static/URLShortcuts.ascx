<%@ Control Language="C#" AutoEventWireup="False" Codebehind="URLShortcuts.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.URLShortcuts" %>
<episerver:pagelist id="languageList" runat="server">
	<HeaderTemplate>
	    <ul id="Shortcuts">
	</HeaderTemplate>
	<ItemTemplate>
	    <li>
	    <a href="<%# GetURL(Container.CurrentPage.LinkURL, Container.CurrentPage.LanguageID,
                                            Container.CurrentPage.PageLink) %>">
	            <%# GetLang(Container.CurrentPage) %>
	            </a>
	    </li>
	    <%# !IsLastItem(Container.CurrentPage) ? "<li>|</li>" : string.Empty %>
	    
	</ItemTemplate>
	<FooterTemplate>
	    <li>
          <span class="rugermanhide" style="float:left;"> | &nbsp;</span>            
			<a href="<%=GetURLForMobile()%>"  class="link-to-mobile">
              <span class="moblinksc"> <% = Scandic.Scanweb.BookingEngine.Web.WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Content/MobileLinkText")%></span>
             <img src="/ScanwebMobile/Public/img/phone_icon.png" alt="Mobile version" />
             </a>  
	    </li>
	    </ul>
	    
	    
	</FooterTemplate>
</episerver:pagelist>
