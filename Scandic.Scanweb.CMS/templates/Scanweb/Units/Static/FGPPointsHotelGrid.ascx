﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FGPPointsHotelGrid.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.FGPPointsHotelGrid" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
    <h3 class="lightHeading fgpHeading" ><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsHeadingTop")%></h3>
    <p>
        <span ><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsSearchHelpText")%>
                        </span>
    </p>
    <p class="formRow fgpPointInput">    
 
      <label for="txtdestName" style="float:left;" ><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsSearch")%>       </label>
 
        <input type="text" rel="<%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsHeading")%>" value="<%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsHeading")%>" autocomplete="off" class="input defaultColor" tabindex="1" id="txtdestName" name="txtdestName" value="<%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsHeading")%>">
        <input type="hidden" id="selectedDestIdFGP" />
        <input type="hidden" id="notmatchingDest" value="<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name")%>" />
        <input type="hidden" id="redemptionPointsCampaignPeriod" value="<%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsCampaignPeriod")%>"/>
        <input type="hidden" id="hiddenAll" value="<%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsAll")%>"/>
    </p>
    <div id="autosuggestFGP" class="autosuggest">
    <ul>
    </ul>
    <iframe src="javascript:false;" frameborder="0" scrolling="no"></iframe>
    </div>
    <label for="txtdestNamesearch" ><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsMinMax")%></label>
    <p class="formRow">
        <select rel="" class="fgpPointSelect" tabindex="2" id="minFGPPoints" name="minFGPPoints">
	        <option value="0" selected="selected" > <%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsMin")%></option>
	        <option value="5000">5000</option>
	        <option value="10000">10000</option>
	        <option value="15000">15000</option>
	        <option value="20000">20000</option>
	        <option value="25000">25000</option>
	        <option value="30000">30000</option>
	        <option value="40000">40000</option>
	        <option value="50000">50000</option>
        </select>
        <select rel="" class="fgpPointSelect fgpPointSelectmrgLeft" tabindex="2" id="maxFGPPoints" name="maxFGPPoints">
	        <option value="50000" selected="selected" ><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsMax")%></option>
	        <option value="5000">5000</option>
	        <option value="10000">10000</option>
	        <option value="15000">15000</option>
	        <option value="20000">20000</option>
	        <option value="25000">25000</option>
	        <option value="30000">30000</option>
	        <option value="40000">40000</option>
	        <option value="50000">50000</option>
        </select>
    </p>
    
	<div class="actionBtn fltLft mrgTop5">                               
		<a onclick="validateCountryDestination();" tabindex="3" id="viewMoreResults" class="buttonInner fgpSearch scansprite" ><span><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsSearchButton")%></span></a>
	</div>
	<br style="clear:both;">
	
	<div id="ErrorFGP" class="redAlertIcon"></div>
	<div class="searchContainerFGP"><span style="font-weight:bold;"><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsSearchedFor")%></span> <span id="searchedFor" runat="server" style="font-weight:bold;"> </span></div>	
	
    <div style="text-align: center; display: none;" id="fgpPointsProgressDiv" class="booking-progress">
                    <img width="25px" height="25px" align="middle" alt="ProgressBar" src="/Templates/Booking/Styles/Default/Images/rotatingclockani.gif">
    </div>
	<table id="tableFgpPoints" width="100%" border="0" class="fgpPointsRegion tablesort" cellpadding="0" cellspacing="0" style="display:none;">
       <thead> 
            <tr>
               <th width="45%" class="fgpHotel"><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsHotel")%></th>
				<th width="45%" class="fgpDestination"><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsDestination")%></th>
                <th width="10%" class="fgpRedemPoints"><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsPoints")%></th>
            </tr>
        </thead> 
        <tbody>	       
	    </tbody> 
    </table>

<script type="text/javascript" src="/Templates/Booking/Javascript/plugin/jquery.tablesorter.min.js"></script>	
<script type="text/javascript" src="/Templates/Booking/Javascript/autoSuggestforFGP.js"></script>					
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
    
        $('#viewMoreResults').bind('keypress', function(e) {
            if (e.keyCode == 13 || e.which == 13 || e.keyCode == 32 || e.which == 32) {
                validateCountryDestination();
            }
        });
	

        $("table[id$='tableFgpPoints']").tablesorter({ widgets: ['zebra'] });
        $('#ErrorFGP').hide();
        $('#fgpPointsProgressDiv').show();
        var Countrydestination = _endsWith("txtdestName");
        if (Countrydestination != null) {
            new AutoSuggestforFGP($fn(Countrydestination), 'autosuggestFGP', 'selectedDestIdFGP', false);
        }
        $('#minFGPPoints').change(function() {
            var index = $(this).find('option:selected').index();
            if (index != 0) {
                $('#maxFGPPoints').find('option:lt(' + index + ')').attr('disabled', 'disabled');
                $('#maxFGPPoints').find('option:gt(' + index + ')').removeAttr('disabled');
                $('#maxFGPPoints').find('option[value="50000"]').removeAttr('disabled');
            } else {
                $('#maxFGPPoints').find('option').removeAttr('disabled');
            }

        });
        $('#maxFGPPoints').change(function() {
            var index = $(this).find('option:selected').index();
            if (index != 0) {
                var index = $(this).find('option:selected').index();
                $('#minFGPPoints').find('option:gt(' + index + ')').attr('disabled', 'disabled');
                $('#minFGPPoints').find('option:lt(' + index + ')').removeAttr('disabled');

            } else {
                $('#minFGPPoints').find('option').removeAttr('disabled');
            }
        });

        GetRedemptionPoints(true);
    });
</script>					