using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.CMS.code.Util.HotelOfferList;


namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    public partial class XmlOfferHotelList : EPiServer.UserControlBase
    {
        public XMLOfferHotel Hotel;
        public string PriceType;
        public bool LastItem;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (PriceType.Equals("FlexEarly"))
            {
                if (CurrentPage["PromotionCode"] != null)
                    PlaceHolderBookbtn.Visible = true;
                else
                    PlaceHolderBookbtn.Visible = false;
                PlaceHolderMeetingBtn.Visible = false;
            }
            else
            {
                PlaceHolderBookbtn.Visible = false;
                PlaceHolderMeetingBtn.Visible = true;
            }
        }
        protected string GetHotelName()
        {
            return this.Hotel.HotelName;
        }
        protected string GetHotelFirstPrice()
        {
            return this.Hotel.FirstPrice;
        }
        protected string GetHotelSecondPrice()
        {
            return this.Hotel.SecondPrice;
        }

        protected string IsLastRow()
        {
            if (LastItem)
                return "noBorder";

            return
                string.Empty;
        }

        protected void RFPButton_Click(object sender, EventArgs e)
        {
            Dictionary<int, string> selectedHotelsDictionary = new Dictionary<int, string>();
            PageData HotelPageData = Hotel.GetHotelPageData();

            selectedHotelsDictionary.Add(HotelPageData.PageLink.ID, GetHotelName());

            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess);

            PageReference RFPageRef = rootPage["RequestForPorposalPage"] as PageReference;

            if (RFPageRef != null)
            {
                PageData RFPage = GetPage(RFPageRef);
                Session["MeetingsSelectedHotels"] = selectedHotelsDictionary;
                Response.Redirect(RFPage.LinkURL.ToString());
            }
        }

        protected void BookBtn_Click(object sender, EventArgs e)
        {
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess);
            PageReference BookPageRef = rootPage["BookingPage"] as PageReference;

            if (BookPageRef != null)
            {
              //do something.
            }
        }
    }
}