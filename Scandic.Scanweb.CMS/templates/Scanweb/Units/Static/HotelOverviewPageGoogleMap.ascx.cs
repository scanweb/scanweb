//  Description					:   HotelOverviewPageGoogleMap                            //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


#undef FINDAHOTEL_PERFORMANCE
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.Map;
using Scandic.Scanweb.CMS.code.Util.Map.GoogleMap;
using Scandic.Scanweb.Core;



namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// This control shows the google map on HotelOverView Page
    /// This page is also called as FindAHotel page.
    /// </summary>
    public partial class HotelOverviewPageGoogleMap : ScandicUserControlBase
    {

        #region Protected Events

        /// <summary>
        /// Page_PreRender Event of the Page.
        /// Calls a function RenderGoogleMap to render the google map
        /// on the page with selected country/city/hotel location
        /// after the tree view loads and fills up the session with
        /// selected node.
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event params</param>
        /// <remarks>Find a hotel release</remarks>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Page.Request.Url.ToString().StartsWith("https"))
            {
                RenderGoogleMap(false);
            }
        }


        #endregion 

        #region Private Methods

        /// <summary>
        /// This method does the following things
        /// 1. Creates two list of selected and other hotel 'Node's
        /// 2. Create hotel units with out numbering(OTHER than selected)
        /// 3. Sort and create hotel units with numbering(selected)
        /// 4. Set map options
        /// 5. Set map data
        /// </summary>
        /// <param name="enableDirections">Determines if directions should be enabled</param>
        private void RenderGoogleMap(bool enableDirections)
        {
#if FINDAHOTEL_PERFORMANCE
            long startTime = DateTime.Now.Ticks;
#endif
            try
            {
                List<Node> allHotelNodes = new List<Node>();
                List<Node> unSelectedHotelNodes = new List<Node>();
                List<Node> selectedHotelNodes = new List<Node>();
                Node rootNode = ContentDataAccess.GetPageDataNodeTree(false);
                if (rootNode != null)
                {
                    GetHotelNodesRecursive(rootNode, allHotelNodes);
                }
                Node selectedNode = (Node) FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView;
                if (selectedNode != null)
                {
                    GetHotelNodesRecursive(selectedNode, selectedHotelNodes);
                }

                unSelectedHotelNodes.AddRange(allHotelNodes);
                for (int i = 0; i < selectedHotelNodes.Count; i++)
                {
                    unSelectedHotelNodes.Remove(selectedHotelNodes[i]);
                }

                IList<MapUnit> hotels = new List<MapUnit>();
                AddHotelNodes(unSelectedHotelNodes, hotels, InfoBoxType.ADVANCED, false);
                SortSelectedHotelNodes(selectedHotelNodes);
                AddHotelNodes(selectedHotelNodes, hotels, InfoBoxType.ADVANCED, true);
                SetMapOptions();
                SetMapData(selectedNode, hotels);
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
            }
#if FINDAHOTEL_PERFORMANCE
            long endTime = DateTime.Now.Ticks;
            TimeSpan tp = new TimeSpan(endTime - startTime);
            AppLogger.LogInfoMessage("HotelOverviewPageGoogleMap.ascx.cs:RenderGoogleMap() running time     ->  Minutes:" + tp.Minutes + " Seconds:" + tp.Seconds + " Milliseconds:" + tp.Milliseconds);
#endif
        }

        /// <summary>
        /// Get all the hotel nodes under the supplied rootPage recursively
        /// </summary>
        /// <param name="rootPage">Node underwhich search has to be taken place</param>
        /// <param name="hotelNodes">list to be filled up with results</param>
        private void GetHotelNodesRecursive(Node rootPage, List<Node> hotelNodes)
        {
            if (rootPage != null)
            {
                IList<Node> subNodes = rootPage.GetChildren();
                if (subNodes != null && subNodes.Count > 0)
                {
                    foreach (Node node in subNodes)
                    {
                        GetHotelNodesRecursive(node, hotelNodes);
                    }
                }
                else
                {
                    PageData pagaData = rootPage.GetPageData();
                    if (pagaData != null)
                    {
                        if ((PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID) ==
                            pagaData.PageTypeID)
                        {
                            hotelNodes.Add(rootPage);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds a new map unit to the list of hotels
        /// </summary>
        /// <param name="hotelNodes">Node list carrying hotel data</param>
        /// <param name="hotels">list on which new units has to be added</param>
        /// <param name="infoBoxType">type of infobox(basic/advanced)</param>
        /// <param name="enableUnitNumbering">true if unit has to be numbered false otherwise</param>
        private void AddHotelNodes(List<Node> hotelNodes, IList<MapUnit> hotels, InfoBoxType infoBoxType,
                                   bool enableUnitNumbering)
        {
            try
            {
                if (hotelNodes != null && hotelNodes.Count > 0)
                {
                    for (int i = 0; i < hotelNodes.Count; i++)
                    {
                        Node hotelNode = hotelNodes[i];
                        if (hotelNode != null)
                        {
                            PageData hotelPageData = hotelNode.GetPageData();
                            if ((PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID) ==
                                hotelPageData.PageTypeID)
                            {
                                string iconUrl = string.Empty;
                                string shadowUrl = string.Empty;
                                string hotelCategory = string.Empty;
                                string promotionalPageLinkText = string.Empty;
                                string promotionalPageLinkUrl = string.Empty;

                                if (hotelPageData["HotelCategory"] != null &&
                                    !string.IsNullOrEmpty(hotelPageData["HotelCategory"].ToString()))
                                {
                                    hotelCategory = hotelPageData["HotelCategory"].ToString();
                                }

                                GetMarkerUrls(hotelCategory, enableUnitNumbering, out iconUrl, out shadowUrl);
                                MapUnit hotelDatasource = new MapHotelUnit(
                                    (double) hotelPageData["GeoX"],
                                    (double) hotelPageData["GeoY"],
                                    -1,
                                    iconUrl,
                                    shadowUrl,
                                    hotelPageData["Heading"] as string,
                                    hotelPageData["HotelBookingImage"] as string,
                                    hotelPageData["HotelBookingDescription"] as string,
                                    hotelPageData["StreetAddress"] as string,
                                    hotelPageData["PostCode"] as string,
                                    hotelPageData["PostalCity"] as string,
                                    hotelPageData["City"] as string,
                                    hotelPageData["Country"] as string,
                                    GetFriendlyURLToPage(hotelPageData.PageLink, hotelPageData.LinkURL) as string,
                                    GetDeepLinkingURL(hotelPageData["OperaID"] as string ?? string.Empty),
                                    hotelPageData["CityCenterDistance"] != null
                                        ? (double) hotelPageData["CityCenterDistance"]
                                        : 0,
                                    infoBoxType,
                                    enableUnitNumbering,
                                    enableUnitNumbering == true ? i + 1 : 0,
                                    hotelPageData["OperaID"] as string ?? string.Empty,
                                    hotelCategory, 
                                    string.Empty, 
                                    string.Empty, 
                                    false 
                                    );

                                hotels.Add(hotelDatasource);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
            }
        }

        /// <summary>
        /// Gets the marker urls.
        /// </summary>
        /// <param name="hotelCategory">The hotel category.</param>
        /// <param name="enableUnitNumbering">if set to <c>true</c> [enable unit numbering].</param>
        /// <param name="iconUrl">The icon URL.</param>
        /// <param name="shadowUrl">The shadow URL.</param>
        private void GetMarkerUrls(string hotelCategory, bool enableUnitNumbering, out string iconUrl,
                                   out string shadowUrl)
        {
            switch (hotelCategory)
            {
                case AppConstants.DEFAULT:
                    {
                        if (enableUnitNumbering || FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView == null)
                            iconUrl = string.Format("http://{0}/{1}", Request.Url.Host,
                                                    "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png");
                        else
                            iconUrl = string.Format("http://{0}/{1}", Request.Url.Host,
                                                    "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_greyedout.png");

                        shadowUrl = string.Empty;
                    }
                    break;
                case AppConstants.RECENTLYOPENED_VALUE:
                    {
                        if (enableUnitNumbering || FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView == null)
                            iconUrl = string.Format("http://{0}/{1}", Request.Url.Host,
                                                    "/Templates/Scanweb/Styles/Default/Images/Icons/recentlyopened_hotel.png");
                        else
                            iconUrl = string.Format("http://{0}/{1}", Request.Url.Host,
                                                    "/Templates/Scanweb/Styles/Default/Images/Icons/recentlyopened_hotel_greyedout.png");

                        shadowUrl = string.Empty;
                    }
                    break;
                case AppConstants.COMINGSOON_VALUE:
                    {
                        if (enableUnitNumbering || FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView == null)
                            iconUrl = string.Format("http://{0}/{1}", Request.Url.Host,
                                                    "/Templates/Scanweb/Styles/Default/Images/Icons/comingsoon_hotel.png");
                        else
                            iconUrl = string.Format("http://{0}/{1}", Request.Url.Host,
                                                    "/Templates/Scanweb/Styles/Default/Images/Icons/comingsoon_hotel_greyedout.png");

                        shadowUrl = string.Empty;
                    }
                    break;
                default:
                    {
                        if (enableUnitNumbering || FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView == null)
                            iconUrl = string.Format("http://{0}/{1}", Request.Url.Host,
                                                    "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png");
                        else
                            iconUrl = string.Format("http://{0}/{1}", Request.Url.Host,
                                                    "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_greyedout.png");

                        shadowUrl = string.Empty;
                    }
                    break;
            }
        }

        /// <summary>
        /// Sort all of the hotels with numbering(selected) based on
        /// the criteria which is been updated in the session by the
        /// hotel listing control, based on user selects
        /// 'Alphabetic'/'Distance to city center' in the sorting drop-down)
        /// </summary>
        /// <param name="selectedHotelNodes">The selected hotel nodes.</param>
        private void SortSelectedHotelNodes(List<Node> selectedHotelNodes)
        {
            try
            {
                if (selectedHotelNodes.Count > 0)
                {
                    string hotelSortOrder = FindAHotelSessionVariablesSessionWrapper.HotelSortOrder;
                    if (0 == string.Compare(hotelSortOrder, SortType.ALPHABETIC.ToString(), true))
                    {
                        selectedHotelNodes.Sort(new PageNameComparer());
                    }
                    else if (0 ==
                             string.Compare(hotelSortOrder, SortType.DISTANCETOCITYCENTREOFSEARCHEDHOTEL.ToString(), true))
                    {
                        selectedHotelNodes.Sort(new CityCenterDistanceComparer());
                    }
                    else
                    {
                        selectedHotelNodes.Sort(new PageNameComparer());
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
            }
        }

        /// <summary>
        /// Set different generic parameters to the map
        /// </summary>
        private void SetMapOptions()
        {
            CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            GMapControl1.GoogleMapKey = (ConfigurationManager.AppSettings["googlemaps." + Request.Url.Host] as string ?? (ConfigurationManager.AppSettings["DevKey"] as string));
            GMapControl1.MarkerLatitudeField = "latitude";
            GMapControl1.MarkerLongitudeField = "longitude";
            GMapControl1.MapPageType = MapPageType.FINDAHOTEL;
            GMapControl1.OtherHotels = true;
            GMapControl1.RecentlyOpenedHotels = true;
            GMapControl1.ComingSoonHotels = true;
        }

        /// <summary>
        /// Set zoom and center of the map according to the selected node
        /// and sets the hotelunit list data to the map
        /// </summary>
        /// <param name="selectedNode">selected node</param>
        /// <param name="hotels">list of map unit</param>
        private void SetMapData(Node selectedNode, IList<MapUnit> hotels)
        {
            try
            {
                if (selectedNode != null)
                {
                    int countryPageTypeID =
                        PageType.Load(new Guid(ConfigurationManager.AppSettings["CountryPageTypeGUID"])).ID;
                    int cityPageTypeID =
                        PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                    int hotelPageTypeID =
                        PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                    PageData selectedPageData = selectedNode.GetPageData();

                    if (selectedPageData.PageTypeID == countryPageTypeID)
                    {
                        SetCountryMapDataZoomAndCenter(selectedPageData, hotels);
                    }
                    else if (selectedPageData.PageTypeID == cityPageTypeID)
                    {
                        SetCityMapDataZoomAndCenter(selectedPageData, hotels);
                    }
                    else if (selectedPageData.PageTypeID == hotelPageTypeID)
                    {
                        SetHotelMapDataZoomAndCenter(selectedPageData, hotels);
                    }
                }
                else
                {
                    GMapControl1.InfoBoxType = InfoBoxType.ADVANCED;
                    GMapControl1.DataSource = hotels;
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
                HandleCmsCoOrdinateDataError(hotels);
            }
            finally
            {
                GMapControl1.DataBind();
            }
        }

        /// <summary>
        /// 1. Set the country data
        /// 2. Zoom and center to show the country(ZOOM LEVEL - AUTO ZOOM)
        /// </summary>
        /// <param name="country">selected country node</param>
        /// <param name="hotels">map unit list to be set as country data</param>
        private void SetCountryMapDataZoomAndCenter(PageData country, IList<MapUnit> hotels)
        {
            if (country != null && hotels != null && hotels.Count > 0)
            {
                if (country["NothEastGeoY"] != null && country["NothEastGeoX"] != null &&
                    country["SouthWestGeoY"] != null && country["SouthWestGeoX"] != null
                    )
                {
                    try
                    {
                        List<MapUnit> boundaryList = new List<MapUnit>();
                        double countryNorthEastLatitude;
                        double countryNorthEastLongitude;
                        double countrySouthWestLatitude;
                        double countrySouthWestLongitude;

                        double.TryParse(country["NothEastGeoY"].ToString(), out countryNorthEastLatitude);
                        double.TryParse(country["NothEastGeoX"].ToString(), out countryNorthEastLongitude);
                        double.TryParse(country["SouthWestGeoY"].ToString(), out countrySouthWestLatitude);
                        double.TryParse(country["SouthWestGeoX"].ToString(), out countrySouthWestLongitude);

                        MapUnit northEast = new MapUnit(countryNorthEastLongitude, countryNorthEastLatitude, -1,
                                                        string.Empty, string.Empty);
                        MapUnit southWest = new MapUnit(countrySouthWestLongitude, countrySouthWestLatitude, -1,
                                                        string.Empty, string.Empty);
                        boundaryList.Add(northEast);
                        boundaryList.Add(southWest);
                        GMapControl1.NELatitude = countryNorthEastLatitude;
                        GMapControl1.NELongitude = countryNorthEastLongitude;
                        GMapControl1.SWLatitude = countrySouthWestLatitude;
                        GMapControl1.SWLongitude = countrySouthWestLongitude;
                        GMapControl1.CenterAndZoomLevel = CenterAndZoomLevel.COUNTRY;
                        GMapControl1.AutoCenterAndZoom();

                        GMapControl1.DataSource = hotels;
                    }
                    catch (System.ArgumentException ex)
                    {
                        AppLogger.LogFatalException(ex, "Country co-ordinates are either wrong or missing");
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    Exception ex = new Exception("Country co-ordinates are either wrong or missing");
                    AppLogger.LogFatalException(ex);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// 1. Set the city data
        /// 2. Zoom and center to show the city(ZOOM LEVEL 9)
        /// </summary>
        /// <param name="city">selected city node</param>
        /// <param name="hotels">map unit list to be set as city data</param>
        private void SetCityMapDataZoomAndCenter(PageData city, IList<MapUnit> hotels)
        {
            if (city != null && hotels != null && hotels.Count > 0)
            {
                double cityCenterLatitude;
                double cityCenterLongitude;

                if (city["CenterGeoY"] != null && city["CenterGeoX"] != null)
                {
                    try
                    {
                        double.TryParse(city["CenterGeoY"].ToString(), out cityCenterLatitude);
                        double.TryParse(city["CenterGeoX"].ToString(), out cityCenterLongitude);
                        GMapControl1.CenterAndZoomLevel = CenterAndZoomLevel.CITY;
                        GMapControl1.ZoomLevel = 9;
                        GMapControl1.Latitude = cityCenterLatitude;
                        GMapControl1.Longitude = cityCenterLongitude;

                        GMapControl1.DataSource = hotels;
                    }
                    catch (System.ArgumentException ex)
                    {
                        AppLogger.LogFatalException(ex, string.Format(AppConstants.CITY_COORDINATES_MISSING, city["OperaID"]));
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    Exception ex = new Exception(string.Format(AppConstants.CITY_COORDINATES_MISSING, city["OperaID"]));
                    AppLogger.LogFatalException(ex);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// 1. Set the hotel data
        /// 2. Zoom and center to show the hotel(ZOOM LEVEL 16)
        /// </summary>
        /// <param name="hotel">selected hotel node</param>
        /// <param name="hotels">map unit list to be set as hotel data</param>
        private void SetHotelMapDataZoomAndCenter(PageData hotel, IList<MapUnit> hotels)
        {
            if (hotel != null && hotels != null && hotels.Count > 0)
            {
                double hotelLatitude;
                double hotelLongitude;

                if (hotel["GeoY"] != null && hotel["GeoX"] != null)
                {
                    try
                    {
                        double.TryParse(hotel["GeoY"].ToString(), out hotelLatitude);
                        double.TryParse(hotel["GeoX"].ToString(), out hotelLongitude);
                        GMapControl1.CenterAndZoomLevel = CenterAndZoomLevel.HOTEL;
                        GMapControl1.Latitude = hotelLatitude;
                        GMapControl1.Longitude = hotelLongitude;
                        GMapControl1.ZoomLevel = 16;


                        GMapControl1.DataSource = hotels;
                    }
                    catch (System.ArgumentException ex)
                    {
                        AppLogger.LogFatalException(ex, "Hotel co-ordinates are either wrong or missing");
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    Exception ex = new Exception("Hotel co-ordinates are either wrong or missing");
                    AppLogger.LogFatalException(ex);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Gracefully handles all exception cases and CMS data issues
        /// Simply shows all the hotels in the map and zooms and center(AUTO)
        /// </summary>
        /// <param name="hotels">all hotels</param>
        private void HandleCmsCoOrdinateDataError(IList<MapUnit> hotels)
        {
        }


        #endregion 
    }

    /// <summary>
    /// This comparer class is been used to compare the Hotel Page names
    /// </summary>
    internal class PageNameComparer : IComparer<Node>
    {
        /// <summary>
        /// ICompare Implementation
        /// </summary>
        /// <param name="first">left side to compare</param>
        /// <param name="second">right side to compare</param>
        /// <returns>0 if eqal -ve if first LESS THAN second +ve if first GREATER THAN second</returns>
        public int Compare(Node first, Node second)
        {
            return
                first.GetPageData().Property["Heading"].ToString().CompareTo(
                    second.GetPageData().Property["Heading"].ToString());
        }
    }

    /// <summary>
    /// This comparer class is been used to compare the distance to citi center
    /// of map units
    /// </summary>
    internal class CityCenterDistanceComparer : IComparer<Node>
    {
        /// <summary>
        /// ICompare Implementation
        /// </summary>
        /// <param name="first">left side to compare</param>
        /// <param name="second">right side to compare</param>
        /// <returns>0 if eqal -ve if first LESS THAN second +ve if first GREATER THAN second</returns>
        public int Compare(Node first, Node second)
        {
            if ((double) first.GetPageData()["CityCenterDistance"] < (double) second.GetPageData()["CityCenterDistance"])
                return -1;
            else if ((double) first.GetPageData()["CityCenterDistance"] ==
                     (double) second.GetPageData()["CityCenterDistance"])
                return 0;
            else
                return 1;
        }
    }
}