//  Description					:   StartpageNewslist                                     //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer;
using EPiServer.Core;
using EPiServer.Filters;

namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// A common footer for the website where links common for the whole site are listed. 
    /// </summary>
    public partial class StartpageNewslist : UserControlBase
    {
        protected static int firstList;
        protected static int lefttoaddinlist;
        protected static int notinList;
        protected static int totalInList;

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            if (!IsPostBack)
            {
                menuList1.PageLink = PageReference.Parse(CurrentPage["SpecialOffersURL"].ToString());
                menuList1.DataBind();
                menuList2.PageLink = PageReference.Parse(CurrentPage["SpecialOffersURL"].ToString());
                menuList2.DataBind();
            }
        }

        /// <summary>
        /// printalastitem
        /// </summary>
        /// <param name="pd"></param>
        /// <returns>lastitem</returns>
        protected string printalastitem(PageData pd)
        {
            lastitemtext.Text = pd.PageName;
            return "spl_NotLastLink";
        }

        /// <summary>
        /// Gets special offers url.
        /// </summary>
        /// <returns>special offers url.</returns>
        public string SpecialOffersURL()
        {
            PageReference pageRef = CurrentPage["SpecialOffersURL"] as PageReference;
            if (pageRef != null)
            {
                return Server.HtmlEncode(DataFactory.Instance.GetPage(pageRef).LinkURL);
            }
            return string.Empty;
        }

        /// <summary>
        /// menuList2_Filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuList2_Filter(object sender, EPiServer.Filters.FilterEventArgs e)
        {
            if (e.Pages.Count > 5)
            {
                e.Pages.RemoveRange(0, 5);

                lefttoaddinlist = e.Pages.Count;

                if (lefttoaddinlist > 5)
                {
                    slist.Text = "5";
                    notinList = lefttoaddinlist - 5;
                    e.Pages.RemoveRange(5, notinList);
                    nlist.Text = notinList.ToString();
                }
                else
                {
                    slist.Text = e.Pages.Count.ToString();
                    nlist.Text = "0";
                }
            }
            else
            {
                lefttoaddinlist = e.Pages.Count;
                slist.Text = lefttoaddinlist.ToString();
                notinList = 0;
                nlist.Text = "0";
            }
        }

        /// <summary>
        /// menuList1_Filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuList1_Filter(object sender, EPiServer.Filters.FilterEventArgs e)
        {
            tlist.Text = e.Pages.Count.ToString();
            totalInList = e.Pages.Count;
            if (e.Pages.Count > 5)
            {
                FilterCount myfirstfilter = new FilterCount(5);
                menuList1.Filter += new EPiServer.Web.WebControls.FilterEventHandler(myfirstfilter.Filter);
                firstList = 5;
                flist.Text = firstList.ToString();
                notinList = e.Pages.Count - firstList;
            }
            else
            {
                firstList = e.Pages.Count;
                notinList = 0;
            }
        }

        /// <summary>
        /// Oninit event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            menuList1.Filter += new EPiServer.Web.WebControls.FilterEventHandler(menuList1_Filter);
            menuList2.Filter += new EPiServer.Web.WebControls.FilterEventHandler(menuList2_Filter);
        }
    }
}