using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.CMS.code.Util.HotelOfferList;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// XmlOfferCityList
    /// </summary>
    public partial class XmlOfferCityList : EPiServer.UserControlBase
    {

        public XMLOfferCity City;
        public string PriceType;

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (PriceType.Equals("FlexEarly"))
            {
                PlaceHolderFlex.Visible = true;
                PlaceHolderMeeting.Visible = false;
            }
            else
            {
                PlaceHolderFlex.Visible = false;
                PlaceHolderMeeting.Visible = true;
            }
            
            foreach (XMLOfferHotel Hotel in City.GetListOfHotels())
            {
                Scandic.Scanweb.CMS.Templates.Units.Static.XmlOfferHotelList HotelControl = (Scandic.Scanweb.CMS.Templates.Units.Static.XmlOfferHotelList)LoadControl("\\Templates\\Scanweb\\Units\\Static\\XmlOfferHotelList.ascx");
                HotelControl.Hotel = Hotel;
                HotelControl.PriceType = PriceType;

                if (City.GetListOfHotels()[City.GetListOfHotels().Count-1].Equals(Hotel))
                    HotelControl.LastItem = true;
                    else 
                        HotelControl.LastItem = false;
                HotelPlaceHolder.Controls.Add(HotelControl);
            }
        }
        /// <summary>
        /// GetCityName
        /// </summary>
        /// <returns>CityName</returns>
        protected string GetCityName()
        {
            return this.City.CityName;
        }

    }
}