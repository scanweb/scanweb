<%@ Control Language="C#" AutoEventWireup="true" Codebehind="MainMenuSubMenu.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.MainMenuSubMenu" %>

<asp:Repeater ID="rptMainMenu" runat="server">
<HeaderTemplate>
 <ul class="TopSubMenu">
</HeaderTemplate>
<ItemTemplate>
<li class="RightArrow">
    <asp:HyperLink  runat="server" ID="MainMenuItemLink"></asp:HyperLink>
</li>
</ItemTemplate>
<FooterTemplate>
 <%= GetIFrameString() %>        
        </ul></FooterTemplate>
<SeparatorTemplate>
   <li class="Separator"></li>
   </SeparatorTemplate>
</asp:Repeater>