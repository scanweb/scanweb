//  Description					:   ConfirmationHotelMap                                  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.Map;
using Scandic.Scanweb.CMS.code.Util.Map.GoogleMap;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.ExceptionManager;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// ConfirmationHotelMap
    /// </summary>
    public partial class ConfirmationHotelMap : ScandicUserControlBase
    {
        /// <summary>
        /// Handles the PreRender event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            SetGoogleMapOptionsAndData();
        }


        /// <summary>
        /// Sets the google map options and data.
        /// </summary>
        private void SetGoogleMapOptionsAndData()
        {
            if (Page.Request.Url.ToString().StartsWith("https"))
            {
                this.Visible = false;
            }
            else
            {
                this.Visible = true;
                IList<MapUnit> hotels = new List<MapUnit>();
                HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                string hotelID = (search != null) ? search.SelectedHotelCode : string.Empty;
                if (hotelID != null || hotelID != string.Empty)
                {
                    try
                    {
                        HotelDestination h = ContentDataAccess.GetHotelByOperaID(hotelID);
                        if (h != null)
                        {
                            MapUnit g = new MapHotelUnit(
                                h.Coordinate.X,
                                h.Coordinate.Y,
                                -1,
                                string.Format("http://{0}/{1}", Request.Url.Host,
                                              "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_selecthotel.png"),
                                string.Empty,
                                h.Name,
                                h.ImageURL,
                                h.HotelDescription,
                                h.HotelAddress.StreetAddress,
                                h.HotelAddress.PostCode,
                                h.HotelAddress.PostalCity,
                                h.HotelAddress.City,
                                h.HotelAddress.Country,
                                h.HotelPageURL,
                                GetDeepLinkingURL(hotelID as string ?? string.Empty),
                                h.CityCenterDistance,
                                InfoBoxType.ADVANCED_CONFIRMATION,
                                false,
                                0,
                                hotelID,
                                h.HotelCategory, 
                                string.Empty, 
                                string.Empty, 
                                false, 
                                true,
                                MarkerRateTextType.RATEPERNIGHT,
                                string.Empty,
                                @"<div class=""gmapRates"">" + h.Name + "</div>",
                                @"<div class=""gmapRates"">" + h.Name + "</div>"
                                );

                            hotels.Add(g);
                        }
                    }
                    catch (ContentDataAccessException)
                    {
                        
                    }
                }

                ConfHotelGMapControl.GoogleMapKey = (ConfigurationManager.AppSettings["googlemaps." + Request.Url.Host] as string ?? (ConfigurationManager.AppSettings["DevKey"] as string));
                ConfHotelGMapControl.MarkerLatitudeField = "latitude";
                ConfHotelGMapControl.MarkerLongitudeField = "longitude";
                ConfHotelGMapControl.InfoBoxType = InfoBoxType.ADVANCED;
                ConfHotelGMapControl.MapPageType = MapPageType.CONFIRMATION;

                ConfHotelGMapControl.DataSource = hotels;
                ConfHotelGMapControl.CenterAndZoom(new MapPoint(hotels[0].Latitude, hotels[0].Longitude), 16);
                ConfHotelGMapControl.DataBind();
            }
        }
    }
}