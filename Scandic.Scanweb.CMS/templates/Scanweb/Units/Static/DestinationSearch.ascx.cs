//  Description					:   DestinationSearch                                     //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// Code behind of DestinationSearch control.
    /// </summary>
    public partial class DestinationSearch : ScandicUserControlBase
    {
        #region Private Members
        private AvailabilityController availabilityController = null;

        /// <summary>
        /// Gets m_AvailabilityController
        /// </summary>
        private AvailabilityController m_AvailabilityController
        {
            get
            {
                if (availabilityController == null)
                {
                    availabilityController = new AvailabilityController();
                }
                return availabilityController;
            }
        }
        #endregion 

        /// <summary>
        /// Prerender the page.This will help to assign the selected node in tree view to the ajax search box.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event args</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            txtDestinationName.Focus();
            Node selectedNode = FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView;
            int countryPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CountryPageTypeGUID"])).ID;
            if ((null != selectedNode) && (selectedNode.GetPageData().PageTypeID != countryPageTypeID))
            {
                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageData selectedPage = selectedNode.GetPageData();
                if (cityPageTypeID == selectedPage.PageTypeID)
                {
                    txtDestinationName.Value = selectedPage.PageName;
                }
                else if (hotelPageTypeID == selectedPage.PageTypeID)
                {
                    txtDestinationName.Value = selectedPage.Property["Heading"].ToString();
                }
            }
            else
            {
                txtDestinationName.Value = string.Empty;
            }
        }

        /// <summary>
        /// Fetches all destinations with non bookable.
        /// </summary>
        /// <returns></returns>
        protected List<CityDestination> FetchAllDestinationsWithNonBookable()
        {
            List<CityDestination> CityList;
            CityList = m_AvailabilityController.FetchAllDestinationsWithNonBookable();
            return CityList;
        }

        /// <summary>
        /// Sets the destination string.This can be called fromm any where.
        /// </summary>
        /// <remarks>Find a hotel release</remarks>
        public string Destination
        {
            get { return txtDestinationName.Value; }
            set { txtDestinationName.Value = value; }
        }
    }
}