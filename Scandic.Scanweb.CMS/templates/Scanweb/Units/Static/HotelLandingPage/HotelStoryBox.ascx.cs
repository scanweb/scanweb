//  Description					:   HotelStoryBox                                         //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer;
using EPiServer.Core;
using EPiServer.Filters;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Code behind of HotelStoryBox control.
    /// </summary>
    public partial class HotelStoryBox : EPiServer.UserControlBase
    {
        /// <summary>
        /// Gets/Sets CssClass
        /// </summary>
        public string CssClass { get; set; }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            StoryBox.CssClass = CssClass;
            StoryBox.HideBookNow = true;
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            PageReference storyBoxContainerLink = rootPage["StoryBoxContainer"] as PageReference;

            if (storyBoxContainerLink != null)
            {
                PageDataCollection storyBoxPages = GetChildren(storyBoxContainerLink);
                FilterAccess accessFilter = new FilterAccess(EPiServer.Security.AccessLevel.Read);
                accessFilter.Filter(storyBoxPages);
                FilterPublished publishedFilter = new FilterPublished(PagePublishedStatus.Published);
                publishedFilter.Filter(storyBoxPages);

                if (storyBoxPages.Count > 0)
                {
                    Random r = new Random();
                    int randomIndex = r.Next(0, storyBoxPages.Count);

                    PageData storyBoxPage = storyBoxPages[randomIndex];

                    PageReference referencedPageLink = storyBoxPage["LinkPage"] as PageReference;

                    if (referencedPageLink != null)
                    {
                        PageData referencedPage = DataFactory.Instance.GetPage(referencedPageLink,
                                                                               EPiServer.Security.AccessLevel.NoAccess);
                        if (referencedPage.QueryDistinctAccess(EPiServer.Security.AccessLevel.Read) &&
                            referencedPage.CheckPublishedStatus(PagePublishedStatus.Published))
                        {
                            StoryBox.OfferPageLink = referencedPage;
                        }
                    }
                }
            }
        }
    }
}