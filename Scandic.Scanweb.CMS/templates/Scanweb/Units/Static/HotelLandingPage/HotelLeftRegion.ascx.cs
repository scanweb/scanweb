////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  HotelLeftRegion                                        //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Hotel Left Region 
    /// </summary>
    public partial class HotelLeftRegion : EPiServer.UserControlBase
    {
        /// <summary>
        /// Public hotelPage property for visibility in markup
        /// </summary>
        public PageData hotelPage;

        /// <summary>
        /// The <see cref="PageDataCollection"/> of pages for the left menu
        /// </summary>
        public PageDataCollection Pages { get; set; }

        /// <summary>
        /// Querystring identifier to identify if page is facility or room
        /// </summary>
        public string IdentifierQueryString { get; set; }

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            hotelPage = ((HotelLandingPage) this.Page).HotelPage;

            if ((Request.QueryString[IdentifierQueryString] as string) == null)
            {
                if (Pages.Count != 0)
                    Response.Redirect(GetURL(Pages[0].PageLink.ID.ToString()));
            }

            if (!IsPostBack)
            {
                PageList1.DataSource = Pages;
                PageList1.DataBind();
            }

            //LinkList2.PopulateByQueryString(Request.QueryString["hotelpage"] as string ?? string.Empty);
        }

        /// <summary>
        /// Check if last item in collection
        /// </summary>
        /// <param name="itemID">ID of item</param>
        /// <returns>True if last item</returns>
        protected bool IsLastItem(int itemID)
        {
            if (Pages.Count > 0)
            {
                return (Pages[Pages.Count - 1].PageLink.ID == itemID);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check if last item in collection
        /// </summary>
        /// <param name="itemID">ID of item</param>
        /// <returns>True if first item</returns>
        protected bool IsFirstItem(int itemID)
        {
            if (Pages.Count > 0)
            {
                return (Pages[0].PageLink.ID == itemID);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check if current item is the active item
        /// </summary>
        /// <param name="itemid">ID of item</param>
        /// <returns>True if active item</returns>
        public bool IsActive(int itemid)
        {
            string currentItemID = Request.QueryString[IdentifierQueryString] as string;
            return ((currentItemID != null) ? currentItemID.Equals(itemid.ToString()) : IsFirstItem(itemid));
        }

        /// <summary>
        /// This method checks whether given item is active or not.
        /// </summary>
        /// <param name="itemid"></param>
        /// <returns></returns>
        public bool HasActiveChild(int itemid)
        {
            int currentItemID;
            if (int.TryParse(Request.QueryString[IdentifierQueryString] as string, out currentItemID))
            {
                try
                {
                    PageData currentItemPageData = DataFactory.Instance.GetPage(new PageReference(currentItemID),
                                                                                EPiServer.Security.AccessLevel.NoAccess);
                    return (currentItemPageData.ParentLink.ID == itemid);
                }
                catch (PageNotFoundException)
                {
                    Server.Transfer("/404/404notfound.aspx");
                    return false;
                }
            }
            Server.Transfer("/404/404notfound.aspx");
            return false;
        }

        /// <summary>
        /// Returns the URL to the page that displays the facility or room page.
        /// </summary>
        /// <param name="val">PageLink.ID as string</param>
        /// <returns>URL to page</returns>
        protected string GetURL(string val)
        {
            string typeString = String.Empty;
            string hotelSectionString = Request.QueryString.Get("hotelpage");

            switch (hotelSectionString)
            {
                case "facilities":
                    typeString = "facilityid";
                    break;
                case "rooms":
                    typeString = "roomid";
                    break;
                case "additionalfacilities":
                    typeString = "facilityid";
                    break;
                default:
                    return "/";
            }

            string tempUrl = UriSupport.AddQueryString(Request.Url.ToString(), typeString, val);

            UrlBuilder url = new UrlBuilder(tempUrl);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, CurrentPage.PageLink,
                                                                  System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }
    }
}