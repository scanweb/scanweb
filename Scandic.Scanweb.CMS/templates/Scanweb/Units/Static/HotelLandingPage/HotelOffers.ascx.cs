using System;
using System.Collections.Generic;
using System.Configuration;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Filters;
using EPiServer.Security;
using Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// HotelOffers
    /// </summary>
    public partial class HotelOffers : ScandicUserControlBase
    {
        private const int itemsPerRow = 3;

        #region Page_Load

        /// <summary>
        /// On Page load, fetch all offers associated to this hotel and display those.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            PageDataCollection offerPages = GetHotelOfferPages();
            PageReference primaryOffer = CurrentPage["PrimaryOffer"] as PageReference;
            if (primaryOffer != null)
            {
                PageData primaryOfferPage = DataFactory.Instance.GetPage(primaryOffer, AccessLevel.NoAccess);
                PromoBoxMainOffer.OfferPageLink = primaryOfferPage;
                foreach (PageData offerPage in offerPages)
                {
                    if (offerPage.PageLink.ID.Equals(primaryOfferPage.PageLink.ID))
                    {
                        offerPages.Remove(offerPage);
                        break;
                    }
                }
            }
            if (offerPages.Count > 0)
            {
                int count = 0;
                for (int offerPosition = 0; offerPosition < offerPages.Count; offerPosition++)
                {
                    bool read = offerPages[offerPosition].QueryDistinctAccess(EPiServer.Security.AccessLevel.Read);
                    bool published = offerPages[offerPosition].CheckPublishedStatus(PagePublishedStatus.Published);
                    if (read == false || published == false || (offerPages[offerPosition]["OfferEndTime"] != null && Convert.ToDateTime(offerPages[offerPosition]["OfferEndTime"]) < DateTime.Now))
                        continue;
                    count++;
                    PromoBoxOffer promoBoxOffer = LoadControl("../../Placeable/PromoBoxOfferPage.ascx") as PromoBoxOffer;
                    promoBoxOffer.HideBookNow = true;
                    promoBoxOffer.OfferPageLink = offerPages[offerPosition];
                    if (IsMiddleOffer(count))
                    {
                        promoBoxOffer.CssClass = "smallPromoBoxLight centerDiv";
                    }
                    else
                    {
                        promoBoxOffer.CssClass = "smallPromoBoxLight storyBox";
                    }
                    smallOfferAreaInner.Controls.Add(promoBoxOffer);
                }
            }
        }

        #endregion Page_Load

        #region IsFirstInRow

        /// <summary>
        /// Checks if the position of the offer box is in the middle of the row.
        /// </summary>
        /// <param name="offerPosition"></param>
        /// <returns>position of the offer box</returns>
        private bool IsMiddleOffer(int offerPosition)
        {
            return (offerPosition % itemsPerRow == 2);
        }

        #endregion IsFirstInRow

        #region GetAllOffers

        /// <summary>
        /// Returns all offers pages in each Offer category confirgured in CMS.
        /// </summary>
        /// <returns>all offers pages</returns>
        private PageDataCollection GetAllOffers()
        {
            int offerPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["OfferPageTypeGUID"])).ID;
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess);
            PageReference offerContainer = rootPage["OfferContainer"] as PageReference;
            PageDataCollection offerPages = new PageDataCollection();

            PageDataCollection offerCategories = DataFactory.Instance.GetChildren(offerContainer);
            foreach (PageData offerCategoryPage in offerCategories)
            {
                PageDataCollection offersInCategory = DataFactory.Instance.GetChildren(offerCategoryPage.PageLink);
                foreach (PageData offer in offersInCategory)
                {
                    if (offer.StopPublish > DateTime.Today)
                    {
                        if (offer.PageTypeID == offerPageTypeID)
                        {
                            offerPages.Add(offer);
                        }
                    }
                }
            }
            for (int i = offerPages.Count - 1; i >= 0; i--)
            {
                PageData p = offerPages[i];
                if (!p.CheckPublishedStatus(PagePublishedStatus.Published) || !p.QueryDistinctAccess(AccessLevel.Read))
                    offerPages.Remove(offerPages[i]);
            }
            return offerPages;
        }

        #endregion GetAllOffers

        #region GetOffersFromOfferPages

        /// <summary>
        /// Get all offer pages that should be visible on this hotel.
        /// </summary>
        /// <param name="allOffers">All offer pages configured in CMS</param>
        /// <returns>A collection of offers pages associated with this hotel</returns>
        private PageDataCollection GetOffersFromOfferPages(PageDataCollection allOffers)
        {
            PageDataCollection offerPages = new PageDataCollection();

            foreach (PageData offerPage in allOffers)
            {
                offerPages.Add(offerPage);
            }

            // Keep only offers that should be visible on this hotel
            FilterCompareTo offerTypeFilter = new FilterCompareTo("Hotels", "{" + CurrentPage["OperaID"] as string + "}");
            offerTypeFilter.Condition = CompareCondition.Contained;
            offerTypeFilter.StringComparison = StringComparison.CurrentCultureIgnoreCase;
            offerTypeFilter.Filter(offerPages);

            return offerPages;
        }

        #endregion GetOffersFromOfferPages

        #region GetOffersFromHotelPages

        /// <summary>
        /// Get all offer pages those are configured in Hotel landing page.
        /// </summary>
        /// <param name="allOffers">All offer pages configured in CMS</param>
        /// <returns>A collection of offers pages associated with this hotel</returns>
        private PageDataCollection GetOffersFromHotelPages(PageDataCollection allOffers)
        {
            PageDataCollection offerPages = new PageDataCollection();

            if (CurrentPage["PromotionBoxOffers"] != null)
            {
                string selectedOffers = CurrentPage["PromotionBoxOffers"] as string;
                string[] selectedOfferList = selectedOffers.Split(',');
                string selectedOffer = string.Empty;

                for (int ctr = 0; ctr < selectedOfferList.Length; ctr++)
                {
                    selectedOffer = selectedOfferList[ctr].Replace("{", "").Replace("}", "");

                    foreach (PageData eachOffer in allOffers)
                    {
                        if (eachOffer.PageLink.ID.ToString() == selectedOffer.Trim())
                        {
                            offerPages.Add(eachOffer);
                        }
                    }
                }
            }

            return offerPages;
        }

        #endregion GetOffersFromHotelPages

        #region GetConsolidatesOffersListForHotel

        /// <summary>
        /// Get a unique list of offer pages which is a combination of the offers configured in Offer page and offers configured in hotel landing page.
        /// </summary>
        /// <param name="offersFromOfferPages">Collection of offer pages configured in Offer page</param>
        /// <param name="offersFromHotelPages">Collection of offer pages configured in hotel landing page</param>
        /// <returns>A collection of offers pages associated with this hotel</returns>
        private PageDataCollection GetConsolidatesOffersListForHotel(PageDataCollection offersFromOfferPages,
                                                                     PageDataCollection offersFromHotelPages)
        {
            PageDataCollection offerPages = new PageDataCollection();
            foreach (PageData offerPage in offersFromOfferPages)
            {
                if (!offerPages.Contains(offerPage))
                {
                    offerPages.Add(offerPage);
                }
            }
            foreach (PageData offerPage in offersFromHotelPages)
            {
                if (!offerPages.Contains(offerPage))
                {
                    offerPages.Add(offerPage);
                }
            }
            return offerPages;
        }

        #endregion GetConsolidatesOffersListForHotel

        #region GetHotelOfferPages

        /// <summary>
        /// Collects, consolidates, Sort all offers selected for the hotel. 
        /// </summary>
        /// <returns>PageDataCollection</returns>
        private PageDataCollection GetHotelOfferPages()
        {
            PageDataCollection offerPages = new PageDataCollection();
            PageDataCollection allOffers = GetAllOffers();
            PageDataCollection offersFromOfferPages = GetOffersFromOfferPages(allOffers);
            PageDataCollection offersFromHotelPages = GetOffersFromHotelPages(allOffers);
            offerPages = GetConsolidatesOffersListForHotel(offersFromOfferPages, offersFromHotelPages);
            offerPages.Sort(new VersionDateComparer());

            return offerPages;
        }

        #endregion GetHotelOfferPages
    }

    #region class VersionDateComparer

    ///// <summary>
    ///// Comparer class to compare the last published version of the pages and oder it in descending.
    ///// </summary>
    //public class VersionDateComparer : IComparer<PageData>
    //{
    //    /// <summary>
    //    /// Compare
    //    /// </summary>
    //    /// <param name="first"></param>
    //    /// <param name="second"></param>
    //    /// <returns></returns>
    //    public int Compare(PageData first, PageData second)
    //    {
    //        PageVersion pv1 = PageVersion.LoadPublishedVersion(first.PageLink,
    //                                                           EPiServer.Globalization.ContentLanguage.PreferredCulture.
    //                                                               Name);
    //        PageVersion pv2 = PageVersion.LoadPublishedVersion(second.PageLink,
    //                                                           EPiServer.Globalization.ContentLanguage.PreferredCulture.
    //                                                               Name);

    //        return pv2.Saved.CompareTo(pv1.Saved);
    //    }
    //}

    #endregion class VersionDateComparer
}