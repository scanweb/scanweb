﻿using System;
using System.Configuration;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.HotelLandingPage
{
    public partial class HotelTravellerReviews : EPiServer.UserControlBase
    {
        private PageData hotelPage;
        private string TALocationID { get { return hotelPage["TALocationID"].ToString(); } }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the hotelpage from structured information
            hotelPage = ((Scandic.Scanweb.CMS.Templates.HotelLandingPage)this.Page).HotelPage;
           
            RatesandReviewDetails.Attributes.Add("src",
            string.Format( ConfigurationManager.AppSettings["TARatingsandReviews"] ,
                TALocationID,
                ConfigurationManager.AppSettings["TAPartnerID"],
                Utility.CurrentLanguage));
        }       
    }
}