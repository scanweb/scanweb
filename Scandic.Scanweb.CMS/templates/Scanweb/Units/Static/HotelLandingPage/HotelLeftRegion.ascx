<%@ Control Language="C#" AutoEventWireup="true" Codebehind="HotelLeftRegion.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelLeftRegion" %>
<%@ Register TagPrefix="Scanweb" TagName="HotelInformation" Src="~/Templates/Scanweb/Units/Placeable/HotelPageHotelInformation.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="LinkList" Src="~/Templates/Scanweb/Units/Placeable/LinkList.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="HotelLocationLinkList" Src="~/Templates/Scanweb/Units/Static/HotelLandingPage/HotelLocationLinkList.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="MoreImagesLink" Src="~/Templates/Scanweb/Units/Placeable/MoreImagesLink.ascx" %>

<div  id="LocationLeft">
  <div class="LeftNavigation">
    <EPiServer:PageList ID="PageList1" runat="server">
        <ItemTemplate>
            <div class="LinkListItem">
                <div class="<%# !IsLastItem(Container.CurrentPage.PageLink.ID) ? "NotLastLink" : "LastLink" %>">
                    <a class='<%# HasActiveChild(Container.CurrentPage.PageLink.ID) ? "HasActiveChild " : "" %><%# IsActive(Container.CurrentPage.PageLink.ID) ? "IconLink Active" : "IconLink" %>'
                        href="<%# GetURL(Container.CurrentPage.PageLink.ID.ToString()) %>">
                        <EPiServer:Property ID="Property1" PropertyName="PageName" runat="server" />
                    </a>
                </div>
            </div>
            <EPiServer:PageList Visible="<%# IsActive(Container.CurrentPage.PageLink.ID) ||
                                     HasActiveChild(Container.CurrentPage.PageLink.ID) %>" PageLink="<%# Container.CurrentPage.PageLink %>" runat="server">
                <HeaderTemplate><div class="LinkListSubList"></HeaderTemplate>
                <ItemTemplate>
                    <div class="<%# IsActive(Container.CurrentPage.PageLink.ID)
                                         ? "LinkListSubItem Active"
                                         : "LinkListSubItem" %>">
                         <a href="<%# GetURL(Container.CurrentPage.PageLink.ID.ToString()) %>">
                            <EPiServer:Property PropertyName="PageName" runat="server" />
                        </a>
                    </div>
                </ItemTemplate>
                <FooterTemplate></div></FooterTemplate>
            </EPiServer:PageList>
        </ItemTemplate>
    </EPiServer:PageList>
   </div>
    <%--<div class="HotelLinkList">
		<div class="HotelLocationHeadLine">
			<h3 class="darkHeading"><%= Translate("/Templates/Scanweb/Pages/HotelLandingPage/Content/MoreInfoHeading") %></h3>
    </div>
    
        <Scanweb:LinkList ID="LinkList2" runat="server" />
    </div>--%>
    <div class="HotelInfoBox">
        <Scanweb:HotelInformation ID="HotelInformation1" HotelPage="<%# hotelPage %>" runat="server" />
    </div>
   
</div>
