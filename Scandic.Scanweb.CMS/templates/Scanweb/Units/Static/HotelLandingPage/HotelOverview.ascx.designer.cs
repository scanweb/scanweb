//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3053
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Scandic.Scanweb.CMS.Templates.Units.Static {
    
    
    /// <summary>
    /// HotelOverview class.
    /// </summary>
    /// <remarks>
    /// Auto-generated class.
    /// </remarks>
    public partial class HotelOverview {
        
        /// <summary>
        /// LinkList1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Scandic.Scanweb.CMS.Templates.Units.Placeable.LinkList LinkList1;
        
        /// <summary>
        /// Translate1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Translate1;
        
        /// <summary>
        /// StreetAddress control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal StreetAddress;
        
        /// <summary>
        /// PostCode control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal PostCode;
        
        /// <summary>
        /// PostalCity control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
         // Archana | PostalCity Changes for XMLAPI
        protected global::System.Web.UI.WebControls.Literal PostalCity;
        
        /// <summary>
        /// City control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal City;
        
        /// <summary>
        /// Country control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Country;
        
        /// <summary>
        /// Translate4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Translate4;
        /// <summary>
        /// Translate4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Translate5;
        
        /// <summary>
        /// Phone control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Phone;
        
         
        /// <summary>
        /// Phone control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal CentralPhoneNumber;

        /// <summary>
        /// Translate5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Translate10;
        
        /// <summary>
        /// Fax control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Fax;
        
        /// <summary>
        /// Translate6 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Translate6;
        
        /// <summary>
        /// Translate7 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Translate7;
        
        /// <summary>
        /// Translte8 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Translte8;
        
        /// <summary>
        /// Latitude control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Latitude;
        
        /// <summary>
        /// Translate9 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Translate9;
        
        /// <summary>
        /// Longitude control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Longitude;
        
        /// <summary>
        /// Map control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Map;
        
        /// <summary>
        /// GoogleMapsPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder GoogleMapsPlaceHolder;
        
        /// <summary>
        /// GMapControl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3.Map GMapControl1;
        
        /// <summary>
        /// AlternativeMapPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder AlternativeMapPlaceHolder;
        
        /// <summary>
        /// HotelIntro control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Property HotelIntro;
        
        /// <summary>
        /// HotelDescription control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Property HotelDescription;
        
        /// <summary>
        /// Translate2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Translate2;
        
        /// <summary>
        /// FacilitiesDescription control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Property FacilitiesDescription;
        
        /// <summary>
        /// Morefacts control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Morefacts;
        
        /// <summary>
        /// transRoomsForDisabled control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate transRoomsForDisabled;
        
        /// <summary>
        /// transRelax control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate transRelax;
        
        /// <summary>
        /// transShop control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate transShop;
        
        /// <summary>
        /// Translate16 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Translate16;
        
        /// <summary>
        /// Translate17 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Translate17;
        
        /// <summary>
        /// Translate18 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Translate18;

        /// <summary>
        /// lnkViewFullDescription control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        ///protected global::System.Web.UI.WebControls.LinkButton lnkViewFullDescription;
        protected global::System.Web.UI.HtmlControls.HtmlAnchor lnkViewFullDescription;
        /// <summary>
        /// Translate19 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate Translate19;

        protected global::System.Web.UI.WebControls.PlaceHolder PhoneNumberPlaceHolder;
        /// <summary>
        /// transWifi control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Translate transWifi;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divNoOfRoom;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divRoomForDisabled;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divNonSmokingRooms;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divRelaxCenter;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divWireless;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divDistanceToTrainStation;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divDistanceToAirPort;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divDistanceToCityCenter;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divEcoLable;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divMeetingFacilities;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divShop;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divOutdoorParking;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divGarage;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divRestaurantBar;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl StreetAddressDiv;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl FaxDiv;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divFacilityDetails;

        /// <summary>
        /// divSpAlert control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divSpAlert;

        /// <summary>
        /// divSpAlertWrap control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divSpAlertWrap;

        protected global::System.Web.UI.WebControls.Repeater redemptionPoints;

        protected global::System.Web.UI.HtmlControls.HtmlGenericControl FGPPoints;

        protected global::System.Web.UI.HtmlControls.HtmlGenericControl addThis;
        protected global::System.Web.UI.HtmlControls.HtmlAnchor Toggle;
    }
}
