<%@ Control Language="C#" AutoEventWireup="true" Codebehind="TransportOptions.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.TransportOptions" %>
<div id="HotelLocationTransport">
    <div class="TransportContent">
        <div class="HeadLineText">
            <EPiServer:Property ID="Property1" PropertyName="PageName" runat="server" />
        </div>
        <h2>
            <EPiServer:Property PropertyName="HeadLine1" runat="server" />
        </h2>
        <div>
            <EPiServer:Property PropertyName="Description1" runat="server" />
        </div>
        <asp:PlaceHolder ID="ShowOnMap1PH" runat="server">
            <div class="LinkListItem">
                <div class="LastLink">
                    <asp:LinkButton CssClass="IconLink" runat="server">
                        <EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                            runat="server" />
                    </asp:LinkButton>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="TravelInstruction2PH" runat="server">
            <h2>
                <EPiServer:Property ID="Property2" PropertyName="HeadLine2" runat="server" />
            </h2>
            <div>
                <EPiServer:Property ID="Property3" PropertyName="Description2" runat="server" />
            </div>
            <asp:PlaceHolder ID="ShowOnMap2PH" runat="server">
                <div class="LinkListItem">
                    <div class="LastLink">
                        <asp:LinkButton CssClass="IconLink" runat="server">
                            <EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                runat="server" />
                        </asp:LinkButton>
                    </div>
                </div>
            </asp:PlaceHolder>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="TravelInstruction3PH" runat="server">
            <h2>
                <EPiServer:Property ID="Property4" PropertyName="HeadLine3" runat="server" />
            </h2>
            <div>
                <EPiServer:Property ID="Property5" PropertyName="Description3" runat="server" />
            </div>
            <asp:PlaceHolder ID="ShowOnMap3PH" runat="server">
                <div class="LinkListItem">
                    <div class="LastLink">
                        <asp:LinkButton CssClass="IconLink" runat="server">
                            <EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                runat="server" />
                        </asp:LinkButton>
                    </div>
                </div>
            </asp:PlaceHolder>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="TravelInstruction4PH" runat="server">
            <h2>
                <EPiServer:Property ID="Property6" PropertyName="HeadLine4" runat="server" />
            </h2>
            <div>
                <EPiServer:Property ID="Property7" PropertyName="Description4" runat="server" />
            </div>
            <asp:PlaceHolder ID="ShowOnMap4PH" runat="server">
                <div class="LinkListItem">
                    <div class="LastLink">
                        <asp:LinkButton CssClass="IconLink" runat="server">
                            <EPiServer:Translate ID="Translate4" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                runat="server" />
                        </asp:LinkButton>
                    </div>
                </div>
            </asp:PlaceHolder>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="TravelInstruction5PH" runat="server">
            <h2>
                <EPiServer:Property ID="Property8" PropertyName="HeadLine5" runat="server" />
            </h2>
            <div>
                <EPiServer:Property ID="Property9" PropertyName="Description5" runat="server" />
            </div>
            <asp:PlaceHolder ID="ShowOnMap5PH" runat="server">
                <div class="LinkListItem">
                    <div class="LastLink">
                        <asp:LinkButton CssClass="IconLink" runat="server">
                            <EPiServer:Translate ID="Translate5" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                runat="server" />
                        </asp:LinkButton>
                    </div>
                </div>
            </asp:PlaceHolder>
        </asp:PlaceHolder>
    </div>
</div>
