<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RoomDescription.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.RoomDescription" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>
<script type="text/javascript">
    $(document).ready(function() {
        CarouselProcessing('<%=CurrentPageTypeId%>', '<%=CurrentPageLinkId%>');
    });
</script>
<div style="padding:20px;">
<div id="HotelRooms">
      <div id="ImageDiv">
    <asp:PlaceHolder ID="RoomImgPH" runat="server">
  
    <Scanweb:SquaredCornerImage ImagePropertyName="RoomImage" 
                                 TopCssClass="RoundedCornersTop472" 
                                 ImageCssClass="RoundedCornersImage472" 
                                 BottomCssClass="RoundedCornersBottom472" ImageWidth="472" runat="server" />
                        
    </asp:PlaceHolder>
   </div>              
   <div id="slides"></div>
    <asp:PlaceHolder ID="RoomContentPH" runat="server">
        <h1>
            <EPiServer:Property PropertyName="PageName" runat="server" />
        </h1>
        <div class="roomDescription">
         <EPiServer:Property id="roomDescription" PropertyName="RoomDescription" runat="server" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="RoomLinksPH" runat="server">
        <div class="LinkContent">
        </div>
    </asp:PlaceHolder>
</div>
</div>