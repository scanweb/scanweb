<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Facility.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.Facility" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>

    <asp:PlaceHolder ID="FacilityImgPH" runat="server">
   <Scanweb:SquaredCornerImage  ImagePropertyName="FacilityImage" 
                                 TopCssClass="RoundedCornersTop472" 
                                 ImageCssClass="RoundedCornersImage472" 
                                 BottomCssClass="RoundedCornersBottom472" ImageWidth="472" runat="server" />
    </asp:PlaceHolder>
 <div style="padding:20px;width:440px;">
   
    <h1>
   
        <EPiServer:Property  PropertyName="PageName" runat="server" /> 
        </h1>
        <h2>
          <EPiServer:Property  PropertyName="FacilityIntro" runat="server" />
        </h2>
            <EPiServer:Property  PropertyName="FacilityDescription" runat="server" />
            
             <asp:PlaceHolder ID="FacilityURLPH" runat="server">
                <div class="LinkListItem">
                    <div class="LastLink">
                        <a href='<%= CurrentPage["FacilityURL"] %>' class="IconLink" title='<%= Translate("/Templates/Scanweb/Pages/HotelLandingPage/Location/FacilityURL") %>'>
                            <EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/FacilityURL"
                                runat="server" />
                        </a>
                    </div>
                </div>
            </asp:PlaceHolder>
    </div>