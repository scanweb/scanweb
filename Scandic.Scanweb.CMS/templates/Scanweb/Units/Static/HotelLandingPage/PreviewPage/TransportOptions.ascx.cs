using System;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// Transport Options
    /// </summary>
    public partial class TransportOptions : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowOrHidePH();
        }

        /// <summary>
        /// Show or Hide Place Holders
        /// </summary>
        protected void ShowOrHidePH()
        {
            if (CurrentPage["GeoX1"] == null && CurrentPage["GeoY1"] == null)
                ShowOnMap1PH.Visible = false;
            else
                ShowOnMap1PH.Visible = true;

            if ((CurrentPage["Headline2"] != null) && (CurrentPage["Description2"] != null))
                TravelInstruction2PH.Visible = true;
            else
                TravelInstruction2PH.Visible = false;

            if (CurrentPage["GeoX2"] == null && CurrentPage["GeoY2"] == null)
                ShowOnMap2PH.Visible = false;
            else
                ShowOnMap2PH.Visible = true;


            if ((CurrentPage["Headline3"] != null) && (CurrentPage["Description3"] != null))
                TravelInstruction3PH.Visible = true;
            else
                TravelInstruction3PH.Visible = false;
            if (CurrentPage["GeoX3"] == null && CurrentPage["GeoY3"] == null)
                ShowOnMap3PH.Visible = false;
            else
                ShowOnMap3PH.Visible = true;


            if ((CurrentPage["Headline4"] != null) && (CurrentPage["Description4"] != null))
                TravelInstruction4PH.Visible = true;
            else
                TravelInstruction4PH.Visible = false;

            if (CurrentPage["GeoX4"] == null && CurrentPage["GeoY4"] == null)
                ShowOnMap4PH.Visible = false;
            else
                ShowOnMap4PH.Visible = true;

            if ((CurrentPage["Headline5"] != null) && (CurrentPage["Description5"] != null))
                TravelInstruction5PH.Visible = true;
            else
                TravelInstruction5PH.Visible = false;

            if (CurrentPage["GeoX5"] == null && CurrentPage["GeoY5"] == null)
                ShowOnMap5PH.Visible = false;
            else
                ShowOnMap5PH.Visible = true;
        }
    }
}