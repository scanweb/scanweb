//  Description					:   RoomDescription                                       //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// Code behind of RoomDescription
    /// </summary>
    public partial class RoomDescription : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CurrentPage["RoomImage"] == null)
                RoomImgPH.Visible = false;
        }

        public int CurrentPageLinkId
        {
            get { return CurrentPage.PageLink.ID; }

        }


        public string CurrentPageTypeId
        {
            get
            {
                return CurrentPage.PageTypeID.ToString();

            }

        }
    }
}