//  Description					:   MeetingRoom                                           //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// Code behind of MeetingRoom control.
    /// </summary>
    public partial class MeetingRoom : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CurrentPage["Area"] == null)
                MeetingRoomSqmHolder.Visible = false;
            if (CapacityUShape == null)
                CapacityUShapeHolder.Visible = false;
            if (CurrentPage["CapacityClassRoom"] == null)
                ClassRoomHolder.Visible = false;
            if (CurrentPage["CapacityTheatre"] == null)
                TheatreHolder.Visible = false;
            if (CurrentPage["CapacityBoardRoom"] == null)
                BoardRoomHolder.Visible = false;
            if (CurrentPage["LocationInHotel"] == null)
                LocationInHotelHolder.Visible = false;
            if (CurrentPage["Lighting"] == null)
                LightingHolder.Visible = false;
            if (CurrentPage["AccessSizeLength"] == null || CurrentPage["AccessSizeHeight"] == null)
                MeetingRoomAccessSizeHolder.Visible = false;
            if (CurrentPage["DimensionLength"] == null || CurrentPage["DimensionWidth"] == null ||
                CurrentPage["DimensionHeight"] == null)
                MeetingRoomDimensionHolder.Visible = false;
            if (CurrentPage["MeetingRoomImage"] == null)
                MeetingRoomImageDiv.Visible = false;           
            if (CurrentPage["FloorPlanImage"] == null)
                FloorPlanImageDiv.Visible = false;
        }
    }
}