//  Description					:   LocalAttractions                                      //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Util.ImageVault;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// This class contains members of LocalAttractions.
    /// </summary>
    public partial class LocalAttractions : EPiServer.UserControlBase
    {
        /// <summary>
        /// This method is used to return a combined string in a format
        /// streetAddress,PostCode City,Country
        /// </summary>
        /// <returns>addressbulder</returns>
        protected string GetAddress(PageData data)
        {
            StringBuilder addressString = new StringBuilder();
            string commma = ", ";

            if ((null != data["StreetAddress"]))
            {
                addressString.Append(data["StreetAddress"].ToString());


                if (null != data["PostCode"])
                {
                    addressString.Append(commma);
                    addressString.Append(data["PostCode"].ToString());
                    if (null != data["City"])
                    {
                        addressString.Append(commma);
                        addressString.Append(data["City"].ToString());

                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"].ToString());
                        }
                    }
                    else
                    {
                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"]);
                        }
                    }
                }
                else
                {
                    if (null != data["City"])
                    {
                        addressString.Append(commma);
                        addressString.Append(data["City"].ToString());

                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"].ToString());
                        }
                    }
                    else
                    {
                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"].ToString());
                        }
                    }
                }
            }
            else
            {
                if (null != data["PostCode"])
                {
                    addressString.Append(data["PostCode"].ToString());
                    if (null != data["City"])
                    {
                        addressString.Append(commma);
                        addressString.Append(data["City"].ToString());
                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"]);
                        }
                    }
                    else
                    {
                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"]);
                        }
                    }
                }
                else
                {
                    if (null != data["City"])
                    {
                        addressString.Append(data["City"].ToString());
                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"]);
                        }
                    }
                    else
                    {
                        if (null != data["Country"])
                        {
                            addressString.Append(data["Country"]);
                        }
                    }
                }
            }
            return addressString.ToString();
        }

        /// <summary>
        /// Returns the alttext for the attraction image
        /// </summary>
        /// <param name="attractionPage">The <see cref="PageData"/> for the attraction</param>
        /// <returns>The alt text for the attraction image</returns>
        protected string GetAltText(PageData attractionPage)
        {
            LangAltText altText = new LangAltText();

            if (attractionPage["AttractionImage"] != null)
                return altText.GetAltText(attractionPage.LanguageID, attractionPage["AttractionImage"].ToString());

            return "";
        }

        /// <summary>
        /// Gets the URL for the attraction page
        /// </summary>
        /// <param name="attractionPage">The <see cref="PageData"/> for the attraction page</param>
        /// <returns>The URL for the attraction page</returns>
        public string GetURL(PageData attractionPage)
        {
            return (attractionPage["AttractionURL"] as string) ?? String.Empty;
        }

        /// <summary>
        /// Gets the Link Text for the Attraction Page
        /// </summary>
        /// <param name="attractionPage"></param>
        /// <returns>The Text for Link</returns>
        public string GetLinkText(PageData attractionPage)
        {
            return (attractionPage["LinkText"] as string) ?? String.Empty;
        }

        /// <summary>
        /// Page on load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            DataBind();
            if (CurrentPage["GeoX"] == null && CurrentPage["GeoY"] == null)
                ShowOnMapPH.Visible = false;
        }
    }
}