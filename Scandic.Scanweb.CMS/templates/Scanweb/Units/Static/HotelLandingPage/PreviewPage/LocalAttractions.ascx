<%@ Control Language="C#" AutoEventWireup="true" Codebehind="LocalAttractions.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.LocalAttractions" %>
<div id="HotelLocationLocalAttractions">
    <div class="middle">
        <div class="content">
            <div class="LocalAttrRowItem">
                <div class="UpperCont">
                    <div class="HeadLine">
                        <div class="HeadLineText">
                            <EPiServer:Property ID="Property1" PropertyName="PageName" runat="server" />
                        </div>
                    </div>
                      
                         <div id='HotelLocationLocal<%# (CurrentPage.PageLink.ID) %>'>   
                           
                               <div class="LocalAttrInfoHolder">
                                     <%# GetAddress(CurrentPage) %>
                                        
                                </div>  
                                   <div class="LocalAttrDescHolder">
                            <EPiServer:Property ID="LocalAttrImage" PropertyName="AttractionImage" ImageWidth="226"
                                runat="server" alt='<%# GetAltText(CurrentPage) %>' />                         
                        <EPiServer:Property ID="Property9" PropertyName="AttractionDescription" runat="server" />                   
                        <div class="linkListItemHolder">
                        <asp:PlaceHolder ID="ShowOnMapPH" runat="server">
                                    <div class="LinkListItem">
                                        <div class="NotLastLink">
                                            <asp:LinkButton CssClass="IconLink"  ID="lbShowMap"
                                                runat="server">
                                                <EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                                    runat="server" />
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </asp:PlaceHolder>
                                  <asp:PlaceHolder ID="AttractionDetailsPH" runat="server">
                                <div class="LinkListItem">
                                    <div class="LastLink">
                                        <a href='<%# GetURL(CurrentPage) %>' target= "_blank" class="IconLink">
                                            <%# GetLinkText(CurrentPage) %>
                                        </a>
                                    </div>
                                    </div>
                                  </asp:PlaceHolder> 
                            </div>     
                          </div>   
        </div>
    </div>
</div>
