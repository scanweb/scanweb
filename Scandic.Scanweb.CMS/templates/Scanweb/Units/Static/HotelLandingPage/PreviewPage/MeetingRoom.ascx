<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MeetingRoom.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.MeetingRoom" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>

<div id="HotelMeetings">
<div style="padding:20px;width:470px;">
  <div id="MeetingsAndRooms">
   <div class="MeetingRoomContainer">
    <div class="MeetingRoomLeft">
    <div class="MeetingRoomTopLeft">
   
   <div class="MeetingRoomTitle">
   <EPiServer:Property PropertyName="PageName" runat="server" />
   </div>
  <div>
   <asp:PlaceHolder ID="MeetingRoomSqmHolder" runat="server">
  
  <div class="MeetingRoomSqm" style="<%= CurrentPage["PageName"].ToString().Length > 20 ? "float:left" : "float:right" %>">
  <EPiServer:Property ID="Property1" PropertyName="Area" runat="server" /> &nbsp;<EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/Squaremeter"  runat="server"/>
   </div>
  </asp:PlaceHolder> 
  </div>
  </div>


 <div id="MeetingRoomImageDiv" runat="server">
  <EPiServer:Property class="MeetingRoomImage" ID="MeetingRoomImage" PropertyName="MeetingRoomImage" ImageWidth="226"
      runat="server"  />
 </div>
    

   <div  id="FloorPlanImageDiv" runat="server">
   <EPiServer:Property class="FloorPlanImage" ID="FloorPlanImage" PropertyName="FloorPlanImage" ImageWidth="226"
        runat="server" />
   </div>
    
</div>
   
    <div class="MeetingRoomRight">
    <table>
   <asp:PlaceHolder ID="CapacityUShapeHolder" runat="server">
<tr>
<td class="MeetingRightLeftColumn">
   <EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/UShape" runat="server" />
   </td>
   <td class="MeetingRightRightColumn"><EPiServer:Property ID="CapacityUShape" PropertyName="CapacityUShape"  runat="server" />
   &nbsp;<EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/People" runat="server" />
</td>
  
    </tr>
     </asp:PlaceHolder>
     <asp:PlaceHolder ID="ClassRoomHolder" runat="server">
      <tr>
     <td class="MeetingRightLeftColumn">
     <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/Classroom" runat="server" />&nbsp;
   </td>
    <td class="MeetingRightRightColumn">
   <EPiServer:Property PropertyName="CapacityClassRoom" runat="server" />&nbsp;
   <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/People" runat="server" />
         </td>
      </tr>
     </asp:PlaceHolder>
     
     <asp:PlaceHolder ID="BoardRoomHolder" runat="server">
     <tr>
     <td class="MeetingRightLeftColumn">
     <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/Boardroom" runat="server" />&nbsp;
</td>
      <td class="MeetingRightRightColumn">
     <EPiServer:Property PropertyName="CapacityBoardRoom" runat="server" />&nbsp;
     <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/People" runat="server" />
 </td>
                                    </tr>
     </asp:PlaceHolder>
     <asp:PlaceHolder ID="TheatreHolder" runat="server">
    <tr>
    <td class="MeetingRightLeftColumn">
      <EPiServer:Translate ID="Translate5" Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/Theatre" runat="server" />
      </td>
      <td class="MeetingRightRightColumn">
     <EPiServer:Property ID="Property2" PropertyName="CapacityTheatre" runat="server" />&nbsp;
      <EPiServer:Translate ID="Translate7" Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/People" runat="server" />
         </td>
     </tr>
     </asp:PlaceHolder>
     </table>
     <asp:PlaceHolder ID="LocationInHotelHolder" runat="server">
     <div class="MeetingRoomLocation">
     <EPiServer:Translate  Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/LocationInHotel"
                                        runat="server" />:
                                        
        <EPiServer:Property PropertyName="LocationInHotel" runat="server" />
     </div> 
     </asp:PlaceHolder>
     
   <asp:PlaceHolder ID="MeetingRoomAccessSizeHolder" runat="server">
   <div  class="MeetingRoomAccess">
  <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/AccessSize"
                                        runat="server" />:
  <EPiServer:Property PropertyName="AccessSizeLength" runat="server" />m x 
    
  <EPiServer:Property PropertyName="AccessSizeHeight" runat="server" />m 
  (<EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/LxH" runat="server" />)
  </div>
  </asp:PlaceHolder>
  <asp:PlaceHolder ID="LightingHolder" runat="server">
     <div class="MeetingRoomLighting">
     <EPiServer:Translate ID="Translate4" Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/Lighting"
                                        runat="server" />:&nbsp;
    <EPiServer:Property ID="Property3" PropertyName="Lighting" runat="server" />
     </div> 
     </asp:PlaceHolder>
  <asp:PlaceHolder ID="MeetingRoomDimensionHolder" runat="server">
   <div class="MeetingRoomDimension">
    <EPiServer:Translate ID="Translate6" Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/Dimension"
                                        runat="server" />:
  <EPiServer:Property PropertyName="DimensionLength" runat="server" />m x
  
  <EPiServer:Property  PropertyName="DimensionWidth"  runat="server" />m x

  <EPiServer:Property PropertyName="DimensionHeight"   runat="server" />m
</div>
</asp:PlaceHolder>
  </div>
  
  
 </div>
      
      
     </div>   
     </div>                                
</div>