//  Description					:   AdditionalInformation                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// Code behind of AdditionalInformation control.
    /// </summary>
    public partial class AdditionalInformation : EPiServer.UserControlBase
    {
    }
}