//  Description					:   HotelRooms                                            //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer;
using EPiServer.Core;
using EPiServer.Security;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util.ImageVault;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Code behind of HotelRooms control.
    /// </summary>
    public partial class HotelRooms : EPiServer.UserControlBase
    {
        private PageData moreImagesPopUp;

        /// <summary>
        /// Hotel page
        /// </summary>
        public PageData hotelPage;

        public string images = WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/MoreImagesPopUp/Images");
        public string images360 = WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/MoreImagesPopUp/Image360");

        /// <summary>
        /// Gets MoreImagesPopUp
        /// </summary>
        public PageData MoreImagesPopUp
        {
            get
            {
                PageData pd = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                           EPiServer.Security.AccessLevel.NoAccess);
                moreImagesPopUp = ((PageReference) pd["MoreImagesPopUp"] != null)
                                      ? DataFactory.Instance.GetPage((PageReference) pd["MoreImagesPopUp"])
                                      : null;
                return moreImagesPopUp;
            }
        }

        private PageData roomPage;

        /// <summary>
        /// The Hotel Room Description Page that should be displayed
        /// </summary>
        public PageData RoomPage
        {
            get { return roomPage; }
            set { roomPage = value; }
        }


        


        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int HotelId = CurrentPage["OperaID"] != null ? Convert.ToInt32(CurrentPage["OperaID"].ToString()) : 0;
                string HotelName = CurrentPage["PageName"] != null ? Convert.ToString(CurrentPage["PageName"]) : null;
                moreImgLink.Attributes.Add("rel", GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") +
                                                  "?methodToCall=GetImageGallery&HotelID=" + HotelId + "&HotelName=" +
                                                  HotelName);
                moreImgLink.Attributes.Add("href", "#");
            }

            hotelPage = ((HotelLandingPage) this.Page).HotelPage;

            int roomPageID = -1;

            if (int.TryParse(Request.QueryString.Get("roomid"), out roomPageID))
            {
                try
                {
                    if (roomPageID != -1)
                    {
                        roomPage = DataFactory.Instance.GetPage(new PageReference(roomPageID), AccessLevel.NoAccess);
                        WebUtil.CheckUnPublishedPage(roomPage, this.Page); 
                        RoomPage = roomPage;
                    }

                    SetPageLinkForProperty();

                    if (String.IsNullOrEmpty(RoomPage["RoomImage"] as string))
                        RoomImgPH.Visible = false;
                    else
                        SetRoomImage();

                    if (!Utils.IsNoImagesForPopUp(hotelPage))
                        RoomLinksPH.Visible = false;
                }
                catch (PageNotFoundException notFoundEx)
                {
                    RoomImgPH.Visible = false;
                    RoomContentPH.Visible = false;
                    RoomLinksPH.Visible = false;
                }
            }
            else
            {
                RoomImgPH.Visible = false;
                RoomContentPH.Visible = false;
                RoomLinksPH.Visible = false;
            }
        }


        public int CurrentPageLinkId
        {
            get { return !string.IsNullOrEmpty(Request.QueryString.Get("roomid")) ? int.Parse(Request.QueryString.Get("roomid")) : default(int); }

        }


        public string CurrentPageTypeId
        {
            get { return (Scandic.Scanweb.BookingEngine.Carousel.CarouselConstants.HotelRoomDescriptionId); }

        }

        protected void SetRoomImage()
        {
           string strImage = RoomPage["RoomImage"] as string;

            if (!String.IsNullOrEmpty(strImage))
            {
                LangAltText altText = new LangAltText();
                var alternateText = altText.GetAltText(RoomPage.LanguageID, strImage);
                HotelRoomImage.ImageUrl = WebUtil.GetImageVaultImageUrl(strImage, 472);
                HotelRoomImage.AlternateText = alternateText;
                HotelRoomImage.Attributes.Add("title", alternateText);
            }

            
        }
        

        /// <summary>
        /// Gets popup url
        /// </summary>
        /// <returns></returns>
        protected string GetPopUpURL()
        {
            PageData p = DataFactory.Instance.GetPage((DataFactory.Instance.GetPage(roomPage.ParentLink).ParentLink));
            string imagesPopUpURL = MoreImagesPopUp.LinkURL ?? String.Empty;
            string hotelID = p.PageLink.ID.ToString() ?? String.Empty;

            if (!String.IsNullOrEmpty(hotelID) && !String.IsNullOrEmpty(imagesPopUpURL))
            {
                UrlBuilder url = new UrlBuilder(UriSupport.AddQueryString(imagesPopUpURL, "hotelid", hotelID));
                url = new UrlBuilder(UriSupport.AddQueryString(url.ToString(), "popuptype", "images"));
                return url.ToString();
            }

            return String.Empty;
        }

        /// <summary>
        /// Set the Page Link to the Episerver:Properties
        /// <remarks>
        /// This step is required as the roompage page is not the CurrentPage
        /// </remarks>
        /// </summary>
        private void SetPageLinkForProperty()
        {
            RoomDescription.PageLink = roomPage.PageLink;
        }
    }
}