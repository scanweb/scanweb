<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelOffers.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelOffers" %>
<%@ Register TagPrefix="Scanweb" TagName="PromoBoxMainOffer" Src="~/Templates/Scanweb/Units/Placeable/PromoBoxMainOffer.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="PromoBoxOffer" Src="~/Templates/Scanweb/Units/Placeable/PromoBoxOffer.ascx" %>
  
<Scanweb:PromoBoxMainOffer ID="PromoBoxMainOffer" runat="server" /> 
<div id = "smallOfferArea">
    <div id = "smallOfferAreaInner" runat="server" />
      </div>