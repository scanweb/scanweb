﻿using System;
using System.Configuration;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    public partial class TripAdvisorRatingsandAwards : ScandicUserControlBase
    {
        public PageData hotelPage;
        public string TALocationID { get { return hotelPage["TALocationID"].ToString(); } }
        public bool HasAwards = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the hotelpage from structured information
            hotelPage = ((Scandic.Scanweb.CMS.Templates.HotelLandingPage)this.Page).HotelPage;

            if (DisplayTravellersChoice())
            {
                HasAwards = true;
                travellersChoiceAward.Attributes.Add("src",
                    string.Format(ConfigurationManager.AppSettings["TATravellersChoiceAward"], Utility.CurrentLanguage, TALocationID));
            }
            else
            {
                travellersChoiceAward.Visible = false;
            }
            if (DisplayExcellenceAward())
            {
                HasAwards = true;
                excellenceAward.Attributes.Add("src",
                string.Format(ConfigurationManager.AppSettings["TAExcellenceAward"], TALocationID, Utility.CurrentLanguage));
            }
            else
            {
                excellenceAward.Visible = false;
            }

            if (DisplayBravoAward())
            {
                HasAwards = true;
                bravoAward.Attributes.Add("src",
                string.Format(ConfigurationManager.AppSettings["TABravoAward"], TALocationID, Utility.CurrentLanguage));
            }
            else
            {
                bravoAward.Visible = false;
            }

            if (!HasAwards)
            {
                phtravellersChoiceAward.Visible = false;
            }
        }

        

        protected bool DisplayTravellersChoice()
        {
            if (hotelPage["TATravellersChoiceAward"] != null)
            {
               return Convert.ToBoolean(hotelPage["TATravellersChoiceAward"]);
            }

            return false;            
        }

        protected bool DisplayExcellenceAward()
        {
            if (hotelPage["TAExcellenceAward"] != null)
            {
                return Convert.ToBoolean(hotelPage["TAExcellenceAward"]);
            }

            return false;
        }

        protected bool DisplayBravoAward()
        {
            if (hotelPage["TABravoAward"] != null)
            {
                return Convert.ToBoolean(hotelPage["TABravoAward"]);
            }

            return false;
        }
    }
}