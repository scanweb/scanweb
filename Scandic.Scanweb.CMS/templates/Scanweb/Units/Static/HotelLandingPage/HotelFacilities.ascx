<%@ Control Language="C#" AutoEventWireup="true" Codebehind="HotelFacilities.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelFacilities" %>
<div id="HotelFacilities">
    <asp:PlaceHolder ID="FacilityImgPH" runat="server">
    <div class="RoundedCornersTop472">
    </div>
    <div class="RoundedCornersImage472">        
        <asp:Image ID="FacilityImage" runat="server"/>
    </div>
    <div class="RoundedCornersBottom472">
    </div>    
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="FacilityContentPH" runat="server">
        <h1>
            <%= FacilityPage["PageName"] %>
        </h1>
        <h2>            
            <EPiServer:Property ID="FacilityIntro" PropertyName="FacilityIntro" Visible="true" runat="server" />
        </h2>            
            <EPiServer:Property ID="FacilityDescription" PropertyName="FacilityDescription" Visible="true" runat="server" />
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="FacilityLinksPH" runat="server">
        <div class="LinkContent">
            <asp:PlaceHolder ID="FacilityImagesLinkPH" runat="server">
                <div class="LinkListItem">
                    <div class='<%= !String.IsNullOrEmpty(FacilityPage["FacilityURL"] as string) ? "NotLastLink" : "LastLink" %>'>
                        <a href="#" class="IconLink"
                title='<%= Translate("/Templates/Scanweb/Pages/HotelLandingPage/Content/Images360") %>'
                onclick="OpenPopUpwindow('<%= GetPopUpURL() %>','600','768');return false;">
                <EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/Images360"
                    runat="server" />
                        </a>
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="FacilityURLPH" runat="server">
                <div class="LinkListItem">
                    <div class="LastLink">
                        <a href='<%= FacilityPage["FacilityURL"] %>' class="IconLink" title='<%= Translate("/Templates/Scanweb/Pages/HotelLandingPage/Location/FacilityURL") %>'>
                            <EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/FacilityURL"
                                runat="server" />
                        </a>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
    </asp:PlaceHolder>
</div>
