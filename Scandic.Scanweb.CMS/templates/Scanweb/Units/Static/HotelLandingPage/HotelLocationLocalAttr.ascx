<%@ Control Language="C#" AutoEventWireup="true" Codebehind="HotelLocationLocalAttr.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelLocationLocalAttr" %>
<%@ Register Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3"
    TagPrefix="cc1" %>
   
<div id="HotelLocationLocalAttractions">
    <asp:Repeater ID="LocalAttrListRepeater" OnItemDataBound="LocalAttr_ItemDataBound"
        runat="server">
        <HeaderTemplate>
            <div class="borderContainer472">
                <div class="top">
                </div>
                <div class="middle">
                    <div class="content">
        </HeaderTemplate>
        <ItemTemplate>
            <div class="LocalAttrRowItem">
                <div class="UpperCont">
                    <div class="HeadLine">
                        <div class="HeadLineText">
                            <img id='HotelLocationLocal<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>img'
                                src="/Templates/Scanweb/Styles/Default/Images/Icons/box_minus.gif" alt='<%=
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/HotelLandingPage/Content/Minimize") %>'
                                onclick="populate('HotelLocationLocal<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>', hide, 
                        '<%=
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/HotelLandingPage/Content/Maximize") %>', 
                        '<%=
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/HotelLandingPage/Content/Minimize") %>');" />
                            <EPiServer:Property ID="Property1" PropertyName="PageName" runat="server" />
                        </div>
                    </div>
                    <div id='HotelLocationLocal<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>'>
                     
                                <div class="LocalAttrInfoHolder">
                                     <%# GetAddress((EPiServer.Core.PageData) Container.DataItem) %>
                                        
                                </div>                                
                            
                        <div class="LocalAttrDescHolder">
                        <div class="descHolder">
                            <EPiServer:Property ID="LocalAttrImage" PropertyName="AttractionImage" ImageWidth="226"
                                runat="server" alt='<%# GetAltText((EPiServer.Core.PageData) Container.DataItem) %>' />                         
                        <EPiServer:Property ID="Property9" PropertyName="AttractionDescription" runat="server" />                   
                        </div>
                        <div class="linkListItemHolder">
                        <asp:PlaceHolder ID="ShowOnMapPH" runat="server">
                                    <div class="LinkListItem">
                                        <div class="NotLastLink">
                                            <asp:LinkButton CssClass="IconLink" OnCommand="lbShowMap_Command" ID="lbShowMap"
                                                runat="server">
                                                <EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                                    runat="server" />
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </asp:PlaceHolder>
                                  <asp:PlaceHolder ID="AttractionDetailsPH" runat="server">
                                <div class="LinkListItem">
                                    <div class="LastLink">
                                        <a href='<%# GetURL((EPiServer.Core.PageData) Container.DataItem) %>' target= "_blank" class="IconLink">
                                            <%# GetLinkText((EPiServer.Core.PageData) Container.DataItem) %>
                                        </a>
                                    </div>
                                    </div>
                                  </asp:PlaceHolder> 
                            </div>     
                          </div>                       
                        </div>
                    </div>
                </div>            
        </ItemTemplate>
        <AlternatingItemTemplate>
            <div class="LocalAttrRowAlternateItem">
                <div class="UpperCont">
                    <div class="HeadLine">
                        <div class="HeadLineText">
                            <img id='HotelLocationLocal<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>img'
                                src="/Templates/Scanweb/Styles/Default/Images/Icons/box_minus.gif" alt='<%=
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/HotelLandingPage/Content/Minimize") %>'
                                onclick="populate('HotelLocationLocal<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>', hide, 
                        '<%=
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/HotelLandingPage/Content/Maximize") %>', 
                        '<%=
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/HotelLandingPage/Content/Minimize") %>');" />
                            <EPiServer:Property ID="Property1" PropertyName="PageName" runat="server" />
                        </div>
                    </div>
                    <div id='HotelLocationLocal<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>'>
                        
                                <div class="LocalAttrInfoHolder">
                                    <%# GetAddress((EPiServer.Core.PageData) Container.DataItem) %>
                                        
                                </div>
                                
                          
                        <div class="LocalAttrDescHolder">
                        <div class="descHolder">
                            <EPiServer:Property ID="LocalAttrImage" PropertyName="AttractionImage" ImageWidth="226"
                                runat="server" alt='<%# GetAltText((EPiServer.Core.PageData) Container.DataItem) %>' />                        
                            <EPiServer:Property ID="Property9" PropertyName="AttractionDescription" runat="server" />                        
                            </div>
                        <div class="linkListItemHolder">
                        <asp:PlaceHolder ID="ShowOnMapPH" runat="server">
                                    <div class="LinkListItem">
                                        <div class="NotLastLink">
                                            <asp:LinkButton CssClass="IconLink" OnCommand="lbShowMap_Command" ID="lbShowMap"
                                                runat="server">
                                                <EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                                    runat="server" />
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="AttractionDetailsPH" runat="server">
                                <div class="LinkListItem">                                    
                                    <div class="LastLink">                                        
                                        <a href='<%# GetURL((EPiServer.Core.PageData) Container.DataItem) %>' target= "_blank" class="IconLink">
                                             <%# GetLinkText((EPiServer.Core.PageData) Container.DataItem) %>
                                        </a>
                                    </div>
                                </div>
                                </asp:PlaceHolder>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </div> </div>
            <div class="bottom">                
            </div>
            </div>
        </FooterTemplate>
    </asp:Repeater>
    <div class="bottomGoogleMapContainer">
        <div class="borderContainer472map">
            <div class="top">
            </div>
            <div class="middle">
                <%--<div class="map">
                    <cc1:GMapControl ID="GMapControl1" Width="452" Height="250" EnableDragging="true"
                        EnableMouseScrollZoom="true" ScrollControlType="DEFAULTGOOGLEUI" runat="server" />
                </div>--%>
                 <div class="middle">
                <div class="map">
                <div id="GMapV3" style="height:250px;width:452px;"></div>  
                        <cc1:Map ID="GMapControl1"  runat="server" />
                
            </div>
            </div>
            <div class="bottom">
            </div>
        </div>
    </div>
</div>
</div>
