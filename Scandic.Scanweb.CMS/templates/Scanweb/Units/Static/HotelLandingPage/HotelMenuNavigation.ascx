<%@ Control Language="C#" AutoEventWireup="true" Codebehind="HotelMenuNavigation.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelMenuNavigation" %>
    
<div id="SubMenuNavigation">
    <div id="SubMenuNavigationTab">
        <ul>
            <!-- Overview tab --->
            <li  class="<%= GetDefaultTabClassName("overview") %>"><a href="<%= GetHotelPageURL("overview") %>"><span><EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Overview" runat="server" /></span></a></li>
            
            <!-- Location tab --->
            <asp:PlaceHolder Visible="<%# HideOrShowLocationTab() %>" runat="server">
             <li  class="<%= GetTabClassName("location") %>"><a href="<%= GetHotelPageURL("location") %>"><span><EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Location" runat="server" /></span></a></li>
             </asp:PlaceHolder>
             <!-- Facilities tab --->
            <asp:PlaceHolder Visible="<%# HideOrShowFacilitiesTab(GetChildren(CurrentPage.PageLink)) %>" runat="server">
            <li  class="<%= GetTabClassName("facilities") %>"><a href="<%= GetHotelPageURL("facilities") %>"><span><EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Facilities" runat="server" /></span></a></li>
            </asp:PlaceHolder>
            <!-- Rooms tab --->
            <asp:PlaceHolder Visible="<%# HideOrShowRoomsTab(GetChildren(CurrentPage.PageLink)) %>" runat="server">
            <li class="<%= GetTabClassName("rooms") %>"><a href="<%= GetHotelPageURL("rooms") %>"><span><EPiServer:Translate ID="Translate4" Text="/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Rooms" runat="server" /></span></a></li>
            </asp:PlaceHolder>
            <!-- Offers tab --->
            <asp:PlaceHolder Visible="<%# HideOrShowOffersTab() %>" runat="server">
            <li class="<%= GetTabClassName("offers") %>"><a href="<%= GetHotelPageURL("offers") %>"><span>
            <EPiServer:Translate ID="Translate5" Text="/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Offers" runat="server" /></span></a></li>
            </asp:PlaceHolder>
            <!-- Meetings tab --->
            <asp:PlaceHolder Visible="<%# HideOrShowMeetingsTab(GetChildren(CurrentPage.PageLink)) %>" runat="server">
            <li class="<%= GetTabClassName("meetings") %>"><a href="<%= GetHotelPageURL("meetings") %>"><span><EPiServer:Translate ID="Translate6" Text="/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Meetings" runat="server" /></span></a></li>
            </asp:PlaceHolder>
            <!-- Guest tab --->
            <asp:PlaceHolder Visible="<%# HideOrShowGuestTab() %>" runat="server">
            <li class="<%= GetTabClassName("guestprogram") %>"><a href="<%= GetHotelPageURL("guestprogram") %>"><span><EPiServer:Translate ID="Translate7" Text="/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/GuestProgram" runat="server" /></span></a></li>
            </asp:PlaceHolder> 
            <!-- Additionalfacilities tab --->
            <asp:PlaceHolder Visible="<%# HideOrShowAdditionalFacilitiesTab(GetChildren(CurrentPage.PageLink)) %>" runat="server">
            <li class="<%= GetTabClassName("additionalfacilities") %>"><a href="<%= GetHotelPageURL("additionalfacilities") %>">
            <span><%= CurrentPage["AdditionalFacilitiesTabName"] as string ?? string.Empty %></span>
            </a>
            </li>
            </asp:PlaceHolder> 
            <!-- Traveller Reviews tab --->
            <asp:PlaceHolder ID="phTravellerReviews" Visible="<%#HideOrShowTravellerReviewsTab() %>" runat="server">
            <li class="<%=GetTabClassName("TravellerReviews")%>"><a href="<%=GetHotelPageURL("TravellerReviews") %>"><span><EPiServer:Translate ID="Translate8" Text="/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/RatingsandReviews" runat="server" /> </span></a></li>
            </asp:PlaceHolder>
        </ul>
    </div>
</div>