<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelGuestProgramLeftRegion.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelGuestProgramLeftRegion" %>
<%@ Register TagPrefix="Scanweb" TagName="HotelInformation" Src="~/Templates/Scanweb/Units/Placeable/HotelPageHotelInformation.ascx" %>

<div>
    <EPiServer:Property PropertyName="GuestProgramLeftBody" runat="server" />
</div>
<div class="HotelInfoBox">
    <Scanweb:HotelInformation runat="server" />
</div>