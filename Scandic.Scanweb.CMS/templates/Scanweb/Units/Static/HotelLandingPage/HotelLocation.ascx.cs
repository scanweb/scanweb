using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Web.UI.WebControls;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.Map;
using Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// HotelLocation
    /// </summary>
    public partial class HotelLocation : ScandicUserControlBase
    {
        public PageData hotelPage;

        /// <summary>
        /// Property for Destination-To direction
        /// </summary>
        public string DestinationTo { get; set; }

        /// <summary>
        /// Property for Destination-From direction
        /// </summary>
        public string DestinationFrom { get; set; }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            hotelPage = ((HotelLandingPage)this.Page).HotelPage;

            if (!IsPostBack)
            {
                drive.Visible = false;
                btnSubmit.Text = LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/HotelLandingPage/Location/GetDirections");
                //btnSubmit.Attributes.Add("onclick", "validateFindYourWay();");
                rbtnToFrom.Items.Add(new ListItem(LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/HotelLandingPage/Location/To"), "To"));
                rbtnToFrom.Items.Add(new ListItem(LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/HotelLandingPage/Location/From"), "From"));
                rbtnToFrom.Items.FindByValue("From").Selected = true;
                rbtnToFrom.DataBind();
                RenderGoogleMap(false);
            }
        }

        /// <summary>
        /// Renders the Google Map
        /// </summary>
        /// <param name="enableDirections">Determines if directions should be enabled</param>
        private void RenderGoogleMap(bool enableDirections)
        {
            #region GoogleMap

            CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            GMapControl1.GoogleMapKey = (ConfigurationManager.AppSettings["googlemaps." + Request.Url.Host] as string ?? (ConfigurationManager.AppSettings["DevKey"] as string));
            if (enableDirections)
            {
                drive.Visible = true;
                GMapControl1.EnableDirections = true;
                GMapControl1.DestinationFrom = this.DestinationFrom;
                GMapControl1.DestinationTo = this.DestinationTo;
                GMapControl1.DirectionsContainer = "Instructions";
                GMapControl1.Locale = hotelPage.LanguageID;
            }

            #region Collection with GoogleMapsHotelUnit

            IList<MapUnit> hotels = new List<MapUnit>();
            double GoeX = 0;
            double GoeY = 0;
            if (hotelPage.PendingPublish)
            {
                PageData pageData = PageValue.GetPageData(hotelPage.PageLink, hotelPage.LanguageBranch);
                if (null != pageData)
                {
                    GoeX = (double)pageData["GeoX"];
                    GoeY = (double)pageData["GeoY"];
                }
            }
            else
            {
                GoeX = (double)hotelPage["GeoX"];
                GoeY = (double)hotelPage["GeoY"];
            }
            MapHotelUnit hotel = new MapHotelUnit(
                GoeX,
                GoeY,
                -1,
                string.Format("http://{0}/{1}", Request.Url.Host,
                              "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png"),
                string.Empty,
                hotelPage["PageName"] as string,
                "",
                hotelPage["StreetAddress"] as string,
                hotelPage["PostCode"] as string,
                hotelPage["PostalCity"] as string,
                hotelPage["City"] as string,
                hotelPage["Country"] as string,
                hotelPage["LinkURL"] as string,
                GetDeepLinkingURL(hotelPage["OperaID"] as string ?? string.Empty)
                );

            hotels.Add(hotel);

            #endregion

            GMapControl1.DataSource = hotels;
            GMapControl1.Latitude = hotels[0].Latitude;
            GMapControl1.Longitude = hotels[0].Longitude;
            GMapControl1.MarkerLatitudeField = "latitude";
            GMapControl1.MarkerLongitudeField = "longitude";
            GMapControl1.MapPageType = MapPageType.HOTELLOCATION;
            GMapControl1.CenterAndZoom(new MapPoint(hotels[0].Latitude, hotels[0].Longitude), 16);

            #endregion

            DataBind();
        }

        /// <summary>
        /// btnSubmit_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            double GoeX = 0;
            double GoeY = 0;
            if (hotelPage.PendingPublish)
            {
                PageData pageData = PageValue.GetPageData(hotelPage.PageLink, hotelPage.LanguageBranch);
                if (null != pageData)
                {
                    GoeX = (double)pageData["GeoX"];
                    GoeY = (double)pageData["GeoY"];
                }
            }
            else
            {
                GoeX = (double)hotelPage["GeoX"];
                GoeY = (double)hotelPage["GeoY"];
            }
            if (rbtnToFrom.SelectedValue == "To")
            {
                GMapControl1.DestinationWay = DriveDirection.TO;
                this.DestinationTo = ReplaceSpecialChars(tbDirection.Text);
                this.DestinationFrom = GoeY.ToString().Replace(',', '.') + "," + GoeX.ToString().Replace(',', '.');
            }
            else
            {
                GMapControl1.DestinationWay = DriveDirection.FROM;
                this.DestinationTo = GoeY.ToString().Replace(',', '.') + "," + GoeX.ToString().Replace(',', '.');
                this.DestinationFrom = ReplaceSpecialChars(tbDirection.Text);
            }

            RenderGoogleMap(true);
        }

        /// <summary>
        /// ReplaceSpecialChars
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Replaced SpecialChars</returns>
        private string ReplaceSpecialChars(string input)
        {
            return input;
        }
    }
}