﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TripAdvisorRatingsandAwards.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static.TripAdvisorRatingsandAwards" %>


<asp:PlaceHolder ID="phtravellersChoiceAward" runat="server">
    <!-- Iframe 2 start-->
 <div class="BoxMedium" style="margin-left:7px;*margin-left:17px;*margin-top:20px;"> 
        <div class="regular">
            <div class="stoolHeader">
                <div class="hd sprite">
                </div>
                <div class="cnt">
                    <div class="cntWrapper">
                        
                            <h3 class="stoolHeading">
                             <EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Pages/HotelLandingPage/RatingsandReviews/TripAdvisorAwards" runat="server" />                                
                            </h3>
                        
                    </div>
                </div>
                <div class="ft sprite">
                    &nbsp;</div>
            </div>
             <div class="cnt" style="padding:10px 2px 2px;">        
                <div class="innerContent" style="padding:0px; width:222px;">                    
					<div style="margin:0px 20px;">
					<iframe id="travellersChoiceAward" class="tripiframe2" runat="server" frameBorder="0"  horizontalscrolling="no" verticalscrolling="yes"></iframe>
					<iframe id="excellenceAward" class="tripiframe3" runat="server" frameBorder="0" horizontalscrolling="no" verticalscrolling="yes"></iframe>
					<iframe id="bravoAward" class="tripiframe4" runat="server" frameBorder="0" horizontalscrolling="no" verticalscrolling="yes"></iframe>
					</div>       
                </div>
            </div>
            <div class="ft sprite">
            </div>
        </div>
</div>
<!-- Iframe 2 end -->
    
</asp:PlaceHolder>

