using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Web.UI.WebControls;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.Util.DetectJavascript;
using Scandic.Scanweb.CMS.code.Util.Map;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// HotelLocationTransport
    /// </summary>
    public partial class HotelLocationTransport : ScandicUserControlBase
    {
        private PageData hotelPage;

        /// <summary>
        /// The <see cref="PageDataCollection"/> for transport pages
        /// </summary>
        public PageDataCollection TransportPages { get; set; }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            hotelPage = ((HotelLandingPage) this.Page).HotelPage;
            TravelInstructionListRepeater.DataSource = TransportPages;
            TravelInstructionListRepeater.DataBind();

            if (!IsPostBack)
            {
                RenderGoogleMap(true, 0, 0);
            }
        }


        /// <summary>
        /// Renders the Google Map
        /// </summary>
        /// <param name="isHotel"></param>
        /// <param name="GeoX"> </param>
        /// <param name="GeoY"> </param>
        private void RenderGoogleMap(bool isHotel, double GeoX, double GeoY)
        {
            #region GoogleMap

            CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            GMapControl1.GoogleMapKey = (ConfigurationManager.AppSettings["googlemaps." + Request.Url.Host] as string ?? (ConfigurationManager.AppSettings["DevKey"] as string));
            if (isHotel)
            {
                #region Collection with GoogleMapsHotelUnit

                IList<MapUnit> hotels = new List<MapUnit>();
                MapHotelUnit hotel = new MapHotelUnit(
                    (double) hotelPage["GeoX"],
                    (double) hotelPage["GeoY"],
                    -1,
                    string.Format
                        ("http://{0}/{1}", Request.Url.Host,
                         "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png"),
                    string.Empty,
                    hotelPage["PageName"] as string,
                    "",
                    hotelPage["StreetAddress"] as string,
                    hotelPage["PostCode"] as string,
                    hotelPage["PostalCity"] as string,
                    hotelPage["City"] as string,
                    hotelPage["Country"] as string,
                    hotelPage["LinkURL"] as string,
                    GetDeepLinkingURL(hotelPage["OperaID"] as string ?? string.Empty)
                    );

                hotels.Add(hotel);

                #endregion
                GMapControl1.MapPageType = MapPageType.HOTELLOCATION;
                GMapControl1.Latitude = hotels[0].Latitude;
                GMapControl1.Longitude = hotels[0].Longitude;
                GMapControl1.DataSource = hotels;
                GMapControl1.MarkerLatitudeField = "latitude";
                GMapControl1.MarkerLongitudeField = "longitude";

                GMapControl1.CenterAndZoom(new MapPoint(hotels[0].Latitude, hotels[0].Longitude), 16);

                #endregion
            }
            else
            {

                #region Collection with GoogleMapsUnit

                IList<MapUnit> units = new List<MapUnit>();
                MapUnit unit = new MapUnit(
                    GeoX,
                    GeoY,
                    16,
                    string.Format
                        ("http://{0}/{1}", Request.Url.Host,
                         "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png"),
                    string.Empty
                    );

                units.Add(unit);

                #endregion

                GMapControl1.DataSource = units;
                GMapControl1.MapPageType = MapPageType.HOTELLOCATION;
                GMapControl1.Latitude = units[0].Latitude;
                GMapControl1.Longitude = units[0].Longitude;
                GMapControl1.MarkerLatitudeField = "latitude";
                GMapControl1.MarkerLongitudeField = "longitude";
                GMapControl1.CenterAndZoom(new MapPoint(units[0].Latitude, units[0].Longitude), units[0].Zoom);
            }

            GMapControl1.DataBind();
        }

        /// <summary>
        /// Fires on databound for transport repeater
        /// </summary>
        /// <param name="Sender">sender object</param>
        /// <param name="e"><see cref="RepeaterItemEventArgs"/> arguments</param>
        protected void TravelInstruction_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (!String.IsNullOrEmpty(((PageData) e.Item.DataItem)["HeadLine2"] as string) &&
                    !String.IsNullOrEmpty(((PageData) e.Item.DataItem)["Description2"] as string))
                    e.Item.FindControl("TravelInstruction2PH").Visible = true;
                else
                    e.Item.FindControl("TravelInstruction2PH").Visible = false;

                if (((PageData) e.Item.DataItem)["GeoX1"] != null &&
                    ((PageData) e.Item.DataItem)["GeoY1"] != null)
                {
                    e.Item.FindControl("ShowOnMap1PH").Visible = true;
                    ((LinkButton) e.Item.FindControl("lbShowMap1")).ToolTip =
                        LanguageManager.Instance.Translate
                            ("/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap");
                    ((LinkButton) e.Item.FindControl("lbShowMap1")).CommandName = "Coordinates";
                    ((LinkButton) e.Item.FindControl("lbShowMap1")).CommandArgument =
                        ((PageData) e.Item.DataItem)["GeoX1"].ToString() + ":" + ((PageData) e.Item.DataItem)["GeoY1"];
                }
                else
                    e.Item.FindControl("ShowOnMap1PH").Visible = false;

                if (((PageData) e.Item.DataItem)["GeoX2"] != null &&
                    ((PageData) e.Item.DataItem)["GeoY2"] != null)
                {
                    e.Item.FindControl("ShowOnMap2PH").Visible = true;
                    ((LinkButton) e.Item.FindControl("lbShowMap2")).ToolTip =
                        LanguageManager.Instance.Translate
                            ("/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap");
                    ((LinkButton) e.Item.FindControl("lbShowMap2")).CommandName = "Coordinates";
                    ((LinkButton) e.Item.FindControl("lbShowMap2")).CommandArgument =
                        ((PageData) e.Item.DataItem)["GeoX2"].ToString() + ":" + ((PageData) e.Item.DataItem)["GeoY2"];
                }
                else
                    e.Item.FindControl("ShowOnMap2PH").Visible = false;

                if (!String.IsNullOrEmpty(((PageData) e.Item.DataItem)["HeadLine3"] as string) &&
                    !String.IsNullOrEmpty(((PageData) e.Item.DataItem)["Description3"] as string))
                    e.Item.FindControl("TravelInstruction3PH").Visible = true;
                else
                    e.Item.FindControl("TravelInstruction3PH").Visible = false;


                if (((PageData) e.Item.DataItem)["GeoX3"] != null &&
                    ((PageData) e.Item.DataItem)["GeoY3"] != null)
                {
                    e.Item.FindControl("ShowOnMap3PH").Visible = true;
                    ((LinkButton) e.Item.FindControl("lbShowMap3")).ToolTip =
                        LanguageManager.Instance.Translate
                            ("/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap");
                    ((LinkButton) e.Item.FindControl("lbShowMap3")).CommandName = "Coordinates";
                    ((LinkButton) e.Item.FindControl("lbShowMap3")).CommandArgument =
                        ((PageData) e.Item.DataItem)["GeoX3"].ToString() + ":" + ((PageData) e.Item.DataItem)["GeoY3"];
                }
                else
                    e.Item.FindControl("ShowOnMap3PH").Visible = false;

                if (!String.IsNullOrEmpty(((PageData) e.Item.DataItem)["HeadLine4"] as string) &&
                    !String.IsNullOrEmpty(((PageData) e.Item.DataItem)["Description4"] as string))
                    e.Item.FindControl("TravelInstruction4PH").Visible = true;
                else
                    e.Item.FindControl("TravelInstruction4PH").Visible = false;


                if (((PageData) e.Item.DataItem)["GeoX4"] != null &&
                    ((PageData) e.Item.DataItem)["GeoY4"] != null)
                {
                    e.Item.FindControl("ShowOnMap4PH").Visible = true;
                    ((LinkButton) e.Item.FindControl("lbShowMap4")).ToolTip =
                        LanguageManager.Instance.Translate
                            ("/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap");
                    ((LinkButton) e.Item.FindControl("lbShowMap4")).CommandName = "Coordinates";
                    ((LinkButton) e.Item.FindControl("lbShowMap4")).CommandArgument =
                        ((PageData) e.Item.DataItem)["GeoX4"].ToString() + ":" + ((PageData) e.Item.DataItem)["GeoY4"];
                }
                else
                    e.Item.FindControl("ShowOnMap4PH").Visible = false;

                if (!String.IsNullOrEmpty(((PageData) e.Item.DataItem)["HeadLine5"] as string) &&
                    !String.IsNullOrEmpty(((PageData) e.Item.DataItem)["Description5"] as string))
                    e.Item.FindControl("TravelInstruction5PH").Visible = true;
                else
                    e.Item.FindControl("TravelInstruction5PH").Visible = false;


                if (((PageData) e.Item.DataItem)["GeoX5"] != null &&
                    ((PageData) e.Item.DataItem)["GeoY5"] != null)
                {
                    e.Item.FindControl("ShowOnMap5PH").Visible = true;
                    ((LinkButton) e.Item.FindControl("lbShowMap5")).ToolTip =
                        LanguageManager.Instance.Translate
                            ("/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap");
                    ((LinkButton) e.Item.FindControl("lbShowMap5")).CommandName = "Coordinates";
                    ((LinkButton) e.Item.FindControl("lbShowMap5")).CommandArgument =
                        ((PageData) e.Item.DataItem)["GeoX5"].ToString() + ":" + ((PageData) e.Item.DataItem)["GeoY5"];
                }
                else
                    e.Item.FindControl("ShowOnMap5PH").Visible = false;
            }
        }

        /// <summary>
        /// lbShowMap_Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbShowMap_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandName != String.Empty && e.CommandName == "Coordinates")
            {
                string[] coords = e.CommandArgument.ToString().Split(new Char[] {':'});
                RenderGoogleMap(false, double.Parse(coords[0]), double.Parse(coords[1]));
            }
        }

        /// <summary>
        /// JavaScriptEnabled
        /// </summary>
        /// <returns>True/False</returns>
        protected bool JavaScriptEnabled()
        {
            if (JavascriptControl.GetState() == JavascriptControl.JavaScriptState.Enabled)
                return true;

            return false;
        }
    }
}