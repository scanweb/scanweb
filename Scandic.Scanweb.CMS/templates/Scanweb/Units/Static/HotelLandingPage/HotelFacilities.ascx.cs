//  Description					: HotelFacilities                                         //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer;
using EPiServer.Core;
using EPiServer.Security;
using Scandic.Scanweb.CMS.Util.ImageVault;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Hotel Facilities
    /// </summary>
    public partial class HotelFacilities : EPiServer.UserControlBase
    {
        private PageData moreImagesPopUp;

        /// <summary>
        /// Gets/Sets MoreImagesPopUp
        /// </summary>
        public PageData MoreImagesPopUp
        {
            get
            {
                PageData pd = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                           EPiServer.Security.AccessLevel.NoAccess);
                moreImagesPopUp = ((PageReference) pd["MoreImagesPopUp"] != null)
                                      ? DataFactory.Instance.GetPage((PageReference) pd["MoreImagesPopUp"])
                                      : null;
                return moreImagesPopUp;
            }
        }

        private PageData facilityPage;

        /// <summary>
        /// The <see cref="PageData"/> for facility pages
        /// </summary>
        public PageData FacilityPage
        {
            get { return facilityPage; }
            set { facilityPage = value; }
        }

        /// <summary>
        /// Get the facility page from structured information
        /// set the Page Lisk for the Episerver Properties
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int facilityPageID = -1;
            if (int.TryParse(Request.QueryString.Get("facilityid"), out facilityPageID))
            {
                try
                {
                    if (facilityPageID != -1)
                    {
                        try
                        {
                            facilityPage = DataFactory.Instance.GetPage(new PageReference(facilityPageID),
                                                                        AccessLevel.NoAccess);
                            WebUtil.CheckUnPublishedPage(facilityPage, this.Page);                            
                            FacilityPage = facilityPage;
                        }
                        catch (PageNotFoundException)
                        {
                            Server.Transfer("/404/404notfound.aspx");
                        }
                    }
                    SetPageLinkForProperty();

                    if (String.IsNullOrEmpty(FacilityPage["FacilityImage"] as string))
                        FacilityImgPH.Visible = false;
                    else
                        SetRoomImage();

                    if (!Utils.IsNoImagesForPopUp(FacilityPage))
                        FacilityImagesLinkPH.Visible = false;

                    if (String.IsNullOrEmpty(FacilityPage["FacilityURL"] as string))
                        FacilityURLPH.Visible = false;
                }
                catch (PageNotFoundException notFoundEx)
                {
                    FacilityImgPH.Visible = false;
                    FacilityContentPH.Visible = false;
                    FacilityLinksPH.Visible = false;
                }
            }
            else
            {
                FacilityImgPH.Visible = false;
                FacilityContentPH.Visible = false;
                FacilityLinksPH.Visible = false;
            }
        }

        
        protected void SetRoomImage()
        {
            string strImage = FacilityPage["FacilityImage"] as string;

            if (!String.IsNullOrEmpty(strImage))
            {
                LangAltText altText = new LangAltText();
                var alternateText = altText.GetAltText(FacilityPage.LanguageID, strImage);
                FacilityImage.ImageUrl = WebUtil.GetImageVaultImageUrl(strImage, 472);
                FacilityImage.AlternateText = alternateText;
                FacilityImage.Attributes.Add("title", alternateText);
            }


        }

        
        /// <summary>
        /// Get Pop Up URL
        /// </summary>
        /// <returns></returns>
        protected string GetPopUpURL()
        {
            PageData p = DataFactory.Instance.GetPage((DataFactory.Instance.GetPage(facilityPage.ParentLink).ParentLink));
            string imagesPopUpURL = MoreImagesPopUp.LinkURL ?? String.Empty;
            string hotelID = p.PageLink.ID.ToString() ?? String.Empty;

            if (!String.IsNullOrEmpty(hotelID) && !String.IsNullOrEmpty(imagesPopUpURL))
            {
                UrlBuilder url = new UrlBuilder((UriSupport.AddQueryString(imagesPopUpURL, "hotelid", hotelID)));
                url = new UrlBuilder(UriSupport.AddQueryString(url.ToString(), "popuptype", "images"));
                return url.ToString();
            }

            return String.Empty;
        }

        /// <summary>
        /// Set the Page Link to the Episerver:Properties
        /// <remarks>
        /// This step is required as the facility page is not the CurrentPage
        /// </remarks>
        /// </summary>
        private void SetPageLinkForProperty()
        {
            FacilityIntro.PageLink = facilityPage.PageLink;
            FacilityDescription.PageLink = facilityPage.PageLink;
        }
    }
}