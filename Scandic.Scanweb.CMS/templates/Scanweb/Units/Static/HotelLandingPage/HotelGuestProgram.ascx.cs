////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:     HotelGuestProgram                                   //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//---------------------------------------------------------------------------------------//
// Revision History			:                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Code behind of HotelGuestProgram control.
    /// </summary>
    public partial class HotelGuestProgram : EPiServer.UserControlBase
    {
    }
}