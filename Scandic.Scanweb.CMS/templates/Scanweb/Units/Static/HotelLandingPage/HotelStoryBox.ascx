<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelStoryBox.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelStoryBox" %>

<%@ Register TagPrefix="Scanweb" TagName="PromoBoxOffer" Src="~/Templates/Scanweb/Units/Placeable/PromoBoxOffer.ascx" %>
<Scanweb:PromoBoxOffer ID="StoryBox" ImageMaxWidth="226" ImagePropertyName="BoxImageMedium"  runat="server" />
