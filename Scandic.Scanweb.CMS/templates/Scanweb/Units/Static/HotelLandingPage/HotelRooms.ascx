<%@ Control Language="C#" AutoEventWireup="true" Codebehind="HotelRooms.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelRooms" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<script type="text/javascript">
    $(document).ready(function() {
        CarouselProcessing('<%=CurrentPageTypeId%>', '<%=CurrentPageLinkId%>');
    });
</script>

<div id="HotelRooms">
    <asp:PlaceHolder ID="RoomImgPH" runat="server">
        <div class="RoundedCornersTop472">
        </div>
		 <div id="ImageDiv">
        <div class="RoundedCornersImage472">            
            <asp:Image ID="HotelRoomImage" runat="server"/>
        </div>
		</div>
		<div id="slides"></div>
        <div class="RoundedCornersBottom472">
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="RoomContentPH" runat="server">
        <h1>
            <%= RoomPage["PageName"] %>
        </h1>
        <div class="roomDescription">            
           <EPiServer:Property ID="RoomDescription" PropertyName="RoomDescription" Visible="true" runat="server" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="RoomLinksPH" runat="server">
        <div class="LinkContent">
            <%--<a href="#" class="IconLink"
                title='<%= Translate("/Templates/Scanweb/Units/Placeable/MoreImagesLink/MoreImagesLinkText")%>'
                onclick="OpenPopUpwindow('<%= GetPopUpURL() %>','600','768');return false;">
                <EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Units/Placeable/MoreImagesLink/MoreImagesLinkText"
                    runat="server" />
            </a>--%>
            <!--R2.3.2-Parvathi-Artifact artf1193956 : Scanweb - links opens in wrong design-->
            <a href="#" class="IconLink jqModal" runat="server" id="moreImgLink"><EPiServer:Translate Text="/Templates/Scanweb/Units/Placeable/MoreImagesLink/MoreImagesLinkText" runat="server" /></a>
            <div class="jqmWindow dialog imgGalPopUp">
	        <div class="hd sprite"></div>
            <div class="cnt">
                <div id="imgGallery" class="scandicTabs">
                    <ul class="tabsCnt">
                    <!--artf1157344 : Image gallery | Tabs are in English on Swedish page -->
                        <li><a href="#" rel=".images" class="tabs active"><span><strong><%= images %></strong></span></a></li>
	                    <li><a href="#" class="tabs" rel=".galleryView"><span><strong><%= images360 %></strong></span></a></li>
                    </ul>
                </div>
                <div id ="imgGal" runat="server" class="bedTypeListclass">
                </div>
            </div>
            <!--R2.3.2-Parvathi-Artifact artf1193956 : Scanweb - links opens in wrong design-->
            <div class="ft sprite"></div>
        </div>
        </div>
    </asp:PlaceHolder>
      <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/plugin/imageResizer.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>
    </asp:PlaceHolder>
</div>
