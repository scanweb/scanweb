//  Description					:   HotelOverview                                         //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.Map;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// HotelOverview
    /// </summary>
    public partial class HotelOverview : ScandicUserControlBase
    {
        /// <summary>
        /// Public hotelPage property for visibility in markup
        /// </summary>
        public PageData hotelPage;

        #region Private Variable
        private string yes;
        private string no;
        private string km;
        private bool controller;
        private Int32 redemptionPointsCount;
        private IFormatProvider cultureInfo = new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);

        #endregion Private Variable

        #region Properties

        /// <summary>
        /// Get the number of rooms.
        /// </summary>
        private string noOfRooms = string.Empty;

        /// <summary>
        /// Gets/Sets NoOfRooms
        /// </summary>
        protected string NoOfRooms
        {
            get { return noOfRooms; }
        }

        /// <summary>
        /// Get the number of non smoking rooms.
        /// </summary>        
        private string noOfNonSmokingRooms = string.Empty;

        /// <summary>
        /// Gets/Sets NoOfNonSmokingRooms
        /// </summary>
        protected string NoOfNonSmokingRooms
        {
            get { return noOfNonSmokingRooms; }
        }

        /// <summary>
        /// Get if rooms for disabled hotels are available.
        /// </summary>
        private string isRoomsForDisabledAvailable;

        /// <summary>
        /// Gets/Sets IsRoomsForDisabledAvailable
        /// </summary>
        protected string IsRoomsForDisabledAvailable
        {
            get { return isRoomsForDisabledAvailable; }
        }

        /// <summary>
        /// Get if relaxing center are available.
        /// </summary>
        private string isRelaxCenterAvailable;

        /// <summary>
        /// Gets/Sets IsRelaxCenterAvailable
        /// </summary>
        protected string IsRelaxCenterAvailable
        {
            get { return isRelaxCenterAvailable; }
        }

        /// <summary>
        /// Get if resturant and bar are available.
        /// </summary>
        private string isResturantAndBarAvailable;

        /// <summary>
        /// Gets/Sets IsResturantAndBarAvailable
        /// </summary>
        protected string IsResturantAndBarAvailable
        {
            get { return isResturantAndBarAvailable; }
        }

        /// <summary>
        /// Get if Grage is available.
        /// </summary>
        private string isGarageAvailable;

        /// <summary>
        /// Gets/Sets IsGarageAvailable
        /// </summary>
        protected string IsGarageAvailable
        {
            get { return isGarageAvailable; }
        }

        /// <summary>
        /// Get if outdoor parking is available.
        /// </summary>
        private string isOutDoorParkingAvailable;

        /// <summary>
        /// Gets/Sets IsOutDoorParkingAvailable
        /// </summary>
        protected string IsOutDoorParkingAvailable
        {
            get { return isOutDoorParkingAvailable; }
        }

        /// <summary>
        /// Get if shops are available.
        /// </summary>
        private string isShopAvailable;

        /// <summary>
        /// Gets/Sets IsShopAvailable
        /// </summary>
        protected string IsShopAvailable
        {
            get { return isShopAvailable; }
        }

        /// <summary>
        /// Get if meeting rooms are available.
        /// </summary>
        private string isMeetingRoomAvailable;

        /// <summary>
        /// Gets/Sets IsMeetingRoomAvailable
        /// </summary>
        protected string IsMeetingRoomAvailable
        {
            get { return isMeetingRoomAvailable; }
        }

        /// <summary>
        /// Get the eco label of this hotel.
        /// </summary>
        private string getEcoLabeled;

        /// <summary>
        /// Gets/Sets GetEcoLabeled
        /// </summary>
        protected string GetEcoLabeled
        {
            get { return getEcoLabeled; }
        }

        /// <summary>
        /// Get the city center distance.
        /// </summary>
        private string getCityCenterDistance;

        /// <summary>
        /// Gets/Sets GetCityCenterDistance
        /// </summary>
        protected string GetCityCenterDistance
        {
            get { return getCityCenterDistance; }
        }

        /// <summary>
        /// Get the nearest airpor name.
        /// </summary>        
        private string getAirPortName;

        /// <summary>
        /// Gets/Sets GetAirPortName
        /// </summary>
        protected string GetAirPortName
        {
            get { return getAirPortName; }
        }

        /// <summary>
        /// Get the airport distance.
        /// </summary>        
        private string getAirportDistance;

        /// <summary>
        /// Gets/Sets GetAirportDistance
        /// </summary>
        protected string GetAirportDistance
        {
            get { return getAirportDistance; }
        }

        /// <summary>
        /// Get distance to Train station.
        /// </summary>
        private string getTrainstationDistance;

        /// <summary>
        /// Gets/Sets GetTrainstationDistance
        /// </summary>
        protected string GetTrainstationDistance
        {
            get { return getTrainstationDistance; }
        }

        /// <summary>
        /// Get if rooms are wifii enabled.
        /// </summary>        
        private string isWiFiAvailable;

        /// <summary>
        /// Gets/Sets IsWiFiAvailable
        /// </summary>
        protected string IsWiFiAvailable
        {
            get { return isWiFiAvailable; }
        }

        protected PageReference hotelPageReference
        {
            get
            {
                return (((HotelLandingPage)this.Page).HotelPage).PageLink as PageReference;
            }
        }

        protected int PointsDisplayLimit
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["PointsDisplayLimit"]);
            }
        }

        #endregion Properties

        /// <summary>
        /// Constructor
        /// </summary>
        public HotelOverview()
        {
            this.yes = Translate("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Yes");
            this.no = Translate("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/No");
            this.km = Translate("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Km");
        }

        /// <summary>
        /// Gets the CSS class.
        /// </summary>
        /// <returns>CSS class string</returns>
        private string GetCssClass()
        {
            controller = !controller;
            if (controller)
                return "rowwhite";
            else
                return "rowgrey";
        }

        /// <summary>
        /// 1.Sets the individual facilities.
        /// 2.Sets the proper CSS in GRID.
        /// 3.Set visibility status of each facilities.
        /// </summary>
        private void SetVisibilityOfFacilities()
        {
            if (null != hotelPage["NoOfRooms"])
            {
                divNoOfRoom.Visible = true;
                divNoOfRoom.Attributes.Add("class", GetCssClass());
                noOfRooms = hotelPage["NoOfRooms"].ToString();
            }

            if (null != hotelPage["NonSmokingRooms"])
            {
                divNonSmokingRooms.Visible = true;
                noOfNonSmokingRooms = hotelPage["NonSmokingRooms"].ToString();
                divNonSmokingRooms.Attributes.Add("class", GetCssClass());
            }
            if (hotelPage["RoomsForDisabled"] != null)
            {
                isRoomsForDisabledAvailable = yes;
            }
            else
            {
                isRoomsForDisabledAvailable = no;
            }
            divRoomForDisabled.Attributes.Add("class", GetCssClass());
            if (hotelPage["RelaxCenter"] != null)
            {
                isRelaxCenterAvailable = yes;
            }
            else
            {
                isRelaxCenterAvailable = no;
            }
            divRelaxCenter.Attributes.Add("class", GetCssClass());
            if (hotelPage["RestaurantBar"] != null)
            {
                isResturantAndBarAvailable = yes;
            }
            else
            {
                isResturantAndBarAvailable = no;
            }
            divRestaurantBar.Attributes.Add("class", GetCssClass());
            if (hotelPage["Garage"] != null)
            {
                isGarageAvailable = yes;
            }
            else
            {
                isGarageAvailable = no;
            }
            divGarage.Attributes.Add("class", GetCssClass());
            if (hotelPage["OutdoorParking"] != null)
            {
                isOutDoorParkingAvailable = yes;
            }
            else
            {
                isOutDoorParkingAvailable = no;
            }
            divOutdoorParking.Attributes.Add("class", GetCssClass());
            if (hotelPage["Shop"] != null)
            {
                isShopAvailable = yes;
            }
            else
            {
                isShopAvailable = no;
            }
            divShop.Attributes.Add("class", GetCssClass());
            if (hotelPage["MeetingFacilities"] != null)
            {
                isMeetingRoomAvailable = yes;
            }
            else
            {
                isMeetingRoomAvailable = no;
            }
            divMeetingFacilities.Attributes.Add("class", GetCssClass());
            if (null != hotelPage["EcoLabeled"])
            {
                divEcoLable.Visible = true;
                getEcoLabeled = hotelPage["EcoLabeled"].ToString();
                divEcoLable.Attributes.Add("class", GetCssClass());
            }
            if (null != hotelPage["CityCenterDistance"])
            {
                divDistanceToCityCenter.Visible = true;
                getCityCenterDistance = hotelPage["CityCenterDistance"].ToString() + " " + km;
                divDistanceToCityCenter.Attributes.Add("class", GetCssClass());
            }
            if ((null != hotelPage["Airport1Name"]) && (null != hotelPage["Airport1Distance"]))
            {
                divDistanceToAirPort.Visible = true;
                getAirPortName = hotelPage["Airport1Name"].ToString();
                getAirportDistance = hotelPage["Airport1Distance"].ToString() + " " + km;
                divDistanceToAirPort.Attributes.Add("class", GetCssClass());
            }
            if (null != hotelPage["DistanceTrain"])
            {
                divDistanceToTrainStation.Visible = true;
                getTrainstationDistance = hotelPage["DistanceTrain"] + " " + km;
                divDistanceToTrainStation.Attributes.Add("class", GetCssClass());
            }
            if (hotelPage["WirelessInternet"] != null)
            {
                isWiFiAvailable = yes;
            }
            else
            {
                isWiFiAvailable = no;
            }
            divWireless.Attributes.Add("class", GetCssClass());
        }

        /// <summary>
        /// 1.Visibility of "View Full Description" link is set for German or Russian websites
        /// 2.If Visible, the text of Popup window is set, having Facility Description
        /// </summary>
        private void SetVisibilityOfFacilitiesDescription()
        {
            string currentLanguage = string.Empty;
            bool isFacilityDescriptionRequired = false;
            string facilityDescription = string.Empty;

            #region View Full Description : CR
            if (hotelPage["IsFacilityDescriptionRequired"] != null)
                isFacilityDescriptionRequired = (bool)hotelPage["IsFacilityDescriptionRequired"];

            if (hotelPage["FacilityDescription"] != null)
                facilityDescription = (hotelPage["FacilityDescription"] as string) ?? String.Empty;

            if (isFacilityDescriptionRequired && !string.IsNullOrEmpty(facilityDescription))
            {
                lnkViewFullDescription.Visible = true;
                divFacilityDetails.InnerHtml = facilityDescription;
                lnkViewFullDescription.InnerHtml =
                    WebUtil.GetTranslatedText(
                        "/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/HideFullDescription");
            }
            else
            {
                lnkViewFullDescription.Visible = false;
                divFacilityDetails.Visible = false;
            }

            #endregion
        }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            redemptionPoints.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(redemptionPoints_ItemDataBound);
        }


        /// <summary>
        /// Page load.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ShowHotelRedemptionPoints();

                ShowAddthis();
            }

            hotelPage = ((HotelLandingPage)this.Page).HotelPage;
            SetVisibilityOfFacilities();

            #region Sets properties for hotel information

            StreetAddress.Text = (hotelPage["StreetAddress"] as string) ?? String.Empty;
            if (string.IsNullOrEmpty(StreetAddress.Text))
            {
                StreetAddressDiv.Visible = false;
            }
            PostCode.Text = ((hotelPage["PostCode"] as string)).Trim() ?? String.Empty;
            if (hotelPage["PostalCity"] == null ||
                (hotelPage["PostalCity"] as string).Trim() == (hotelPage["City"] as string).Trim())
            {
                City.Text = ((hotelPage["City"] as string)).Trim() ?? String.Empty;
            }
            else
            {
                if (!string.IsNullOrEmpty(hotelPage["PostalCity"] as string))
                {
                    PostalCity.Text = (hotelPage["PostalCity"] as string).Trim() ?? String.Empty;
                }
                if (!string.IsNullOrEmpty(hotelPage["City"] as string))
                {
                    City.Text = (hotelPage["City"] as string).Trim() ?? String.Empty;
                }
            }
            Country.Text = ((hotelPage["Country"] as string)).Trim() ?? String.Empty;
            Phone.Text = (hotelPage["Phone"] as string) ?? String.Empty;
            if (!string.IsNullOrEmpty((hotelPage["CentralReservationNumber"] as string)))
                CentralPhoneNumber.Text = (hotelPage["CentralReservationNumber"] as string) ?? String.Empty;
            else
                PhoneNumberPlaceHolder.Visible = false;

            Fax.Text = (hotelPage["Fax"] as string) ?? String.Empty;
            if (string.IsNullOrEmpty(Fax.Text))
            {
                FaxDiv.Visible = false;
            }
            double GoeX = 0;
            double GoeY = 0;
            if (hotelPage.PendingPublish)
            {
                PageData pageData = PageValue.GetPageData(hotelPage.PageLink, hotelPage.LanguageBranch);
                if (null != pageData)
                {
                    GoeX = (double)pageData["GeoX"];
                    GoeY = (double)pageData["GeoY"];
                }
            }
            else
            {
                GoeX = (double)hotelPage["GeoX"];
                GoeY = (double)hotelPage["GeoY"];
            }

            Longitude.Text = GoeX.ToString();
            Latitude.Text = GoeY.ToString();

            #endregion

            #region GoogleMap

            CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            GMapControl1.GoogleMapKey = (ConfigurationManager.AppSettings["googlemaps." + Request.Url.Host] as string ?? (ConfigurationManager.AppSettings["DevKey"] as string));
            #region Collection with GoogleMapsHotelUnit

            IList<MapUnit> hotels = new List<MapUnit>();
            MapHotelUnit hotel = new MapHotelUnit(
                GoeX,
                GoeY,
                -1,
                string.Format("http://{0}/{1}", Request.Url.Host,
                              "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png"),
                string.Empty,
                hotelPage["PageName"] as string,
                "",
                hotelPage["StreetAddress"] as string,
                hotelPage["PostCode"] as string,
                hotelPage["PostalCity"] as string,
                hotelPage["City"] as string,
                hotelPage["Country"] as string,
                hotelPage["LinkURL"] as string,
                GetDeepLinkingURL(hotelPage["OperaID"] as string ?? string.Empty)
                );

            hotels.Add(hotel);

            #endregion

            GMapControl1.DataSource = hotels;
            GMapControl1.MarkerLatitudeField = "latitude";
            GMapControl1.MarkerLongitudeField = "longitude";
            GMapControl1.MapPageType = MapPageType.HOTELOVERVIEW;
            GMapControl1.Latitude = hotels[0].Latitude;
            GMapControl1.Longitude = hotels[0].Longitude;
            GMapControl1.CenterAndZoom(new MapPoint(hotels[0].Latitude, hotels[0].Longitude), 16);

            #endregion

            #region Alternative Image

            if (Page.Request.Url.ToString().StartsWith("https"))
            {
                GoogleMapsPlaceHolder.Visible = false;
                AlternativeMapPlaceHolder.Visible = true;
            }

            #endregion

            #region LinkList
           /* Commented out by Vaibhav: SCANMOB-465 */
            //LinkList1.PopulateByQueryString(Request.QueryString["hotelpage"] as string ?? string.Empty);

            #endregion

            #region View Full Description For Russia or Germany: CR

            SetVisibilityOfFacilitiesDescription();

            #endregion

            // Merchandising - Special Alert - Display special alert in booking flow

            string spAlert = ContentDataAccess.GetSpecialAlert(hotelPage["OperaID"] as string, true);
            if (!string.IsNullOrEmpty(spAlert))
            {
                divSpAlertWrap.Visible = true;
                divSpAlert.InnerHtml = spAlert;
            }
            else
            {
                divSpAlertWrap.Visible = false;
            }
            DataBind();
        }

        private void ShowAddthis()
        {
            addThis.InnerHtml = Utility.GetAddthisScript(CurrentPage.LanguageID);
        }

        private void ShowHotelRedemptionPoints()
        {
            Toggle.Visible = false;
            PageDataCollection hotelRedemptionPages = ContentDataAccess.GetChildrenWithPageTypeID(Convert.ToInt32(ConfigurationManager.AppSettings["HotelRedemptionPointsPageTypeID"]), hotelPageReference);

            PageDataCollection redemptionPages = new PageDataCollection();
            foreach (PageData pageData in hotelRedemptionPages)
            {
                if (pageData.StopPublish > DateTime.Today)
                {
                    if (pageData["Points"] != null)
                        redemptionPages.Add(pageData);
                }
            }

            if (hotelRedemptionPages.Count > 0)
            {
                redemptionPointsCount = 0;
                redemptionPoints.DataSource = redemptionPages;
                redemptionPoints.DataBind();
                if (redemptionPoints.Items.Count > PointsDisplayLimit)
                {
                    Toggle.Visible = true;
                }
            }
            else
            {
                FGPPoints.Visible = false;
            }
        }

        private void redemptionPoints_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                PageData redemptionPageData = e.Item.DataItem as PageData;
                if (redemptionPageData != null && redemptionPageData.CheckPublishedStatus(PagePublishedStatus.Published))
                {
                    
                    if (redemptionPageData["Points"] != null)
                    {
                        HtmlGenericControl points = e.Item.FindControl("points") as HtmlGenericControl;    
                        if (redemptionPageData["Startdate"] != null && redemptionPageData["Enddate"] != null)
                        {
                            points.InnerHtml = string.Format("{0} {1} {2} ({3}-{4})", redemptionPageData["Points"].ToString(),
                                Translate("/Templates/Scanweb/Units/Static/GuestProgramRedemptionList/Points"),"<br>",
                                Convert.ToDateTime(redemptionPageData["Startdate"]).ToString("dd MMM yy", cultureInfo),
                                Convert.ToDateTime(redemptionPageData["Enddate"]).ToString("dd MMM yy", cultureInfo));
                        }
                        else
                        {
                            points.InnerHtml = string.Format("{0} {1}", redemptionPageData["Points"].ToString(),
                                Translate("/Templates/Scanweb/Units/Static/GuestProgramRedemptionList/Points"));
                        }

                        if (e.Item.ItemIndex < PointsDisplayLimit)
                        {
                            points.Attributes.Add("class", "show");

                        }
                        else
                        {
                            points.Attributes.Add("class", "hide HiddenItems");
                        }

                        redemptionPointsCount++;
                    }
                }
            }
        }
    }
}
