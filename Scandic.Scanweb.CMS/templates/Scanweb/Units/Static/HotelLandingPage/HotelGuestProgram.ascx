<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelGuestProgram.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelGuestProgram" %>

<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>

<div>
      <Scanweb:SquaredCornerImage ID="SquaredCornersImage1" ImagePropertyName="GuestProgramTopImage" 
                                 TopCssClass="RoundedCornersTop472" 
                                 ImageCssClass="RoundedCornersImage472" 
                                 BottomCssClass="RoundedCornersBottom472" ImageWidth="472" runat="server" />
</div>
<div>
    <EPiServer:Property PropertyName="GuestProgramMainBody" runat="server" />
</div>