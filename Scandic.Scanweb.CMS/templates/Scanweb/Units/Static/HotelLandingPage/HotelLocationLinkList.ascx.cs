//  Description					:   HotelLocationLinkList                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// HotelLocationLinkList
    /// </summary>
    public partial class HotelLocationLinkList : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int localInt = 0;
            localInt = HideOrShowLinkLisLocalAttraction(GetChildren(CurrentPage.PageLink));

            if (localInt == 0) hideLocalAttraction.Visible = false;

            int transportInt = 0;
            transportInt = HideOrShowLinkLisTransportOptions(GetChildren(CurrentPage.PageLink));
            if (transportInt == 0)
                hideTransportOptions.Visible = false;
        }

        /// <summary>
        /// Check if current item is the active item
        /// </summary>
        /// <param name="linkstring">The string identifier for the current subpage</param>
        /// <returns>True if active item</returns>
        public bool IsActive(string linkstring)
        {
            string locationstring = (Request.QueryString.Get("location")) ?? String.Empty;

            if (locationstring.Equals(linkstring))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Returns the URL depending on variables 
        /// </summary>
        /// <param name="val">The string identifier for the current subpage</param>
        /// <returns>URL to page</returns>
        protected string GetLocationPageURL(string val)
        {
            UrlBuilder url = new UrlBuilder((UriSupport.AddQueryString(Request.Url.ToString(), "location", val)));
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal
                (url, CurrentPage.PageLink, System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }

        /// <summary>
        /// Hide Or Show Link List Local Attraction
        /// </summary>
        /// <param name="currPageTree">PageDataCollection</param>
        /// <returns></returns>
        protected int HideOrShowLinkLisLocalAttraction(PageDataCollection currPageTree)
        {
            int countlocal = 0;
            foreach (PageData pd in currPageTree)
            {
                if (pd.PageTypeName == "Hotel Local Attraction Container")
                {
                    foreach (PageData p in GetChildren(pd.PageLink))
                    {
                        countlocal = countlocal + 1;
                    }
                }
            }

            return countlocal;
        }

        /// <summary>
        /// HideOrShowLinkLisTransportOptions
        /// </summary>
        /// <param name="currPageTree">PageDataCollection</param>
        /// <returns></returns>
        protected int HideOrShowLinkLisTransportOptions(PageDataCollection currPageTree)
        {
            int countTransport = 0;
            foreach (PageData pd in currPageTree)
            {
                if (pd.PageTypeName == "Travel Instruction Container")
                {
                    foreach (PageData p in GetChildren(pd.PageLink))
                    {
                        countTransport = countTransport + 1;
                    }
                }
            }

            return countTransport;
        }

        /// <summary>
        /// getCssClass
        /// </summary>
        /// <returns>true or false</returns>
        protected bool getCssClass()
        {
            if ((hideLocalAttraction.Visible == false && hideTransportOptions.Visible == true) ||
                (hideLocalAttraction.Visible == true && hideTransportOptions.Visible == false))
                return true;
            else
                return false;
        }
    }
}