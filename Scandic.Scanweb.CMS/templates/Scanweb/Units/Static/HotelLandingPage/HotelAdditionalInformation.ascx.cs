//  Description					: HotelAdditionalInformation                              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.CMS.Util.ImageVault;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Code behind of HotelAdditionalInformation control.
    /// </summary>
    public partial class HotelAdditionalInformation : EPiServer.UserControlBase
    {
        /// <summary>
        /// Gets or sets the querystring with the ID of the page to be displayed.
        /// </summary>
        public string IdentifierString { get; set; }

        private PageData contentPage;

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int contentPageID = -1;
            if (int.TryParse(Request.QueryString.Get(IdentifierString), out contentPageID))
            {
                try
                {
                    if (contentPageID != -1)
                    {
                        PageReference contentPageLink = new PageReference(contentPageID);
                        contentPage = DataFactory.Instance.GetPage(contentPageLink);
                        WebUtil.CheckUnPublishedPage(contentPage, this.Page);
                        PageType hotelAdditionalInformationPageType =
                            PageType.Load(
                                new Guid(ConfigurationManager.AppSettings["HotelAdditionalInformationPageTypeGUID"]));
                        if (contentPage.PageTypeID == hotelAdditionalInformationPageType.ID)
                        {
                            PageNameProperty.PageLink = contentPageLink;
                            IntroProperty.PageLink = contentPageLink;
                            ContentProperty.PageLink = contentPageLink;

                            PageNameProperty.DataBind();
                            IntroProperty.DataBind();
                            ContentProperty.DataBind();
                        }
                        else
                            throw new ArgumentException();
                    }

                    if (String.IsNullOrEmpty(contentPage["MainImage"] as string))
                        ImagePlaceHolder.Visible = false;
                    else
                        SetRoomImage();
                }
                catch (PageNotFoundException notFoundEx)
                {
                    HideAllContent();
                }
                catch (AccessDeniedException)
                {
                    HideAllContent();
                }
                catch (ArgumentException)
                {
                    HideAllContent();
                }
            }
            else
            {
                HideAllContent();
            }
        }

        /// <summary>
        /// HideAllContent
        /// </summary>
        private void HideAllContent()
        {
            ImagePlaceHolder.Visible = false;
            ContentPH.Visible = false;
        }
                       
        protected void SetRoomImage()
        {
            string strImage = contentPage["MainImage"] as string;

            if (!String.IsNullOrEmpty(strImage))
            {
                LangAltText altText = new LangAltText();
                var alternateText = altText.GetAltText(contentPage.LanguageID, strImage);
                ContentImage.ImageUrl = WebUtil.GetImageVaultImageUrl(strImage, 472);
                ContentImage.AlternateText = alternateText;
                ContentImage.Attributes.Add("title", alternateText);
            }


        }
        
    }
}