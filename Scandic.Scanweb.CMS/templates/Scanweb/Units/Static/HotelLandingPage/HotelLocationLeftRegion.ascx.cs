using System;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Hotel Location Left Region
    /// </summary>
    public partial class HotelLocationLeftRegion : EPiServer.UserControlBase
    {
        /// <summary>
        /// Public hotelPage property for visibility in markup
        /// </summary>
        public PageData hotelPage;

        public string images = WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/MoreImagesPopUp/Images");
        public string images360 = WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/MoreImagesPopUp/Image360");

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            hotelPage = ((HotelLandingPage) this.Page).HotelPage;

            #region LinkList

            //LinkList1.PopulateByQueryString(Request.QueryString["hotelpage"] as string ?? string.Empty);

            #endregion

            if ((Request.QueryString["location"] as string) == null)
            {
                Response.Redirect(GetURL("detailedmap"));
            }

            if (!IsPostBack)
            {
                int HotelId = CurrentPage["OperaID"] != null ? Convert.ToInt32(CurrentPage["OperaID"].ToString()) : 0;
                string HotelName = CurrentPage["PageName"] != null ? Convert.ToString(CurrentPage["PageName"]) : null;
                //moreImgLink.Attributes.Add("rel", GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") +
                //                                  "?methodToCall=GetImageGallery&HotelID=" + HotelId + "&HotelName=" +
                //                                  HotelName);
                //moreImgLink.Attributes.Add("href", string.Format("{0}#", Request.RawUrl));

                if (!String.IsNullOrEmpty(Request.QueryString.Get("location")) &&
                    Request.QueryString.Get("location") != "detailedmap")
                    LocationLeftDistancesPH.Visible = false;

                ltlCentreDist.Text = (hotelPage["CityCenterDistance"] != null)
                                         ? ((double) hotelPage["CityCenterDistance"]).ToString("N1") + "&nbsp;"
                                         : String.Empty;
                ltlCentre.Text = hotelPage["CityCenterName"] as string ?? String.Empty;
                ltlAirport1Dist.Text = (hotelPage["Airport1Distance"] != null)
                                           ? ((double) hotelPage["Airport1Distance"]).ToString("N1") + "&nbsp;"
                                           : String.Empty;
                ltlAirport1.Text = hotelPage["Airport1Name"] as string ?? String.Empty;
                ltlAirport2Dist.Text = (hotelPage["Airport2Distance"] != null)
                                           ? ((double) hotelPage["Airport2Distance"]).ToString("N1") + "&nbsp;"
                                           : String.Empty;
                ltlAirport2.Text = hotelPage["Airport2Name"] as string ?? String.Empty;
                ltlLandmark1Dist.Text = (hotelPage["Landmark1Distance"] != null)
                                            ? ((double) hotelPage["Landmark1Distance"]).ToString("N1") + "&nbsp;"
                                            : String.Empty;
                ltlLandmark1.Text = hotelPage["Landmark1Name"] as string ?? String.Empty;
                ltlLandmark2Dist.Text = (hotelPage["Landmark2Distance"] != null)
                                            ? ((double) hotelPage["Landmark2Distance"]).ToString("N1") + "&nbsp;"
                                            : String.Empty;
                ltlLandmark2.Text = hotelPage["Landmark2Name"] as string ?? String.Empty;
                ShowOrHidePH();
            }
        }

        /// <summary>
        /// Returns the URL depending on variables 
        /// </summary>
        /// <param name="val">Subpage identifier as string</param>
        /// <returns>URL to subpage</returns>
        protected string GetURL(string val)
        {
            UrlBuilder url = new UrlBuilder((UriSupport.AddQueryString(Request.Url.ToString(), "location", val)));
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal
                (url, CurrentPage.PageLink, System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }

        /// <summary>
        /// Disable the Placeholder If no information avlilable for Distance in Kilometer Heading.
        /// </summary>
        protected void ShowOrHidePH()
        {
            if (ltlCentre.Text as string == String.Empty ? DistanceRow1.Visible = false : DistanceRow1.Visible = true) ;
            if (ltlAirport1.Text as string == String.Empty ? DistanceRow2.Visible = false : DistanceRow2.Visible = true)
                ;
            if (ltlAirport2.Text as string == String.Empty ? DistanceRow3.Visible = false : DistanceRow3.Visible = true)
                ;
            if (ltlLandmark1.Text as string == String.Empty ? DistanceRow4.Visible = false : DistanceRow4.Visible = true)
                ;
            if
                (ltlLandmark2.Text as string == String.Empty
                     ? DistanceRow5.Visible = false
                     : DistanceRow5.Visible = true) ;

            if (DistanceRow1.Visible == false && DistanceRow2.Visible == false && DistanceRow3.Visible == false
                && DistanceRow4.Visible == false && DistanceRow5.Visible == false)
                LocationLeftDistancesPH.Visible = false;
        }
    }
}