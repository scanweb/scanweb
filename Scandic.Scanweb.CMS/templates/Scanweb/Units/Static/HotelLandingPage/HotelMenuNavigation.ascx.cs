//  Description					: HotelMenuNavigation                                     //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region using

using System;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;

#endregion using

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Code behind of HotelMenuNavigation control.
    /// </summary>
    public partial class HotelMenuNavigation : EPiServer.UserControlBase
    {
        #region Protected Methods

        /// <summary>
        /// Page Load method used to bind the data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            DataBind();
        }

        /// <summary>
        /// Gets the css class for the tab
        /// </summary>
        /// <param name="tabName"></param>
        /// <returns></returns>
        protected string GetTabClassName(string tabName)
        {
            return (Request.QueryString["hotelpage"] as string ?? string.Empty).Equals(tabName) ? "ActiveTab" : "Tab";
        }

        /// <summary>
        /// Gets the Default Css for Overview tab
        /// </summary>
        /// <param name="tabName"></param>
        /// <returns></returns>
        protected string GetDefaultTabClassName(string tabName)
        {
            return Request.QueryString["hotelpage"] == null ? "ActiveTab" : GetTabClassName(tabName);
        }

        /// <summary>
        /// Gets the url for the tab selected
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        protected string GetHotelPageURL(string val)
        {
            UrlBuilder url = new UrlBuilder((UriSupport.AddQueryString(CurrentPage.LinkURL, "hotelpage", val)));
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, CurrentPage.PageLink,
                                                                  System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }

        /// <summary>
        /// Returns the value (true/false) for the visibility of Location tab 
        /// </summary>
        /// <returns></returns>
        protected bool HideOrShowLocationTab()
        {
            PageData hotelPage = DataFactory.Instance.GetPage(CurrentPage.PageLink);
            bool showLocationTab = false;

            if (Utility.CheckifFallback())
            {
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Location",
                    LanguageConstant.LANGUAGE_ENGLISH);
                showLocationTab = true;
            }
            else if (hotelPage["HotelMenuNavigationList"] != null &&
                     (hotelPage["HotelMenuNavigationList"].ToString().Contains("{" + WebUtil.GetTranslatedText(
                         "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Location") + "}")))
            {
                showLocationTab = true;
            }
            else if (hotelPage["HotelMenuNavigationList"] == null)
            {
                showLocationTab = true;
            }
            else
            {
                showLocationTab = false;
            }

            return showLocationTab;
        }

        /// <summary>
        /// Returns the value (true/false) for the visibility of Offers tab 
        /// </summary>
        /// <returns></returns>
        protected bool HideOrShowOffersTab()
        {
            PageData hotelPage = DataFactory.Instance.GetPage(CurrentPage.PageLink);
            bool showOffersTab = false;

            if (hotelPage["HotelMenuNavigationList"] != null &&
                (hotelPage["HotelMenuNavigationList"].ToString().Contains("{" + WebUtil.GetTranslatedText(
                    "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Offers") + "}")))
            {
                showOffersTab = true;
            }
            else if (hotelPage["HotelMenuNavigationList"] == null)
            {
                showOffersTab = true;
            }
            else if (Utility.CheckifFallback())
            {
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Offers",
                    LanguageConstant.LANGUAGE_ENGLISH);
                showOffersTab = true;
            }
            else
            {
                showOffersTab = false;
            }

            return showOffersTab;
        }

        /// <summary>
        /// Returns the value (true/false) for the visibility of Facilities tab 
        /// </summary>
        /// <param name="currPageTree"></param>
        /// <returns></returns>
        protected bool HideOrShowFacilitiesTab(PageDataCollection currPageTree)
        {
            PageData hotelPage = DataFactory.Instance.GetPage(CurrentPage.PageLink);

            int countFacilities = 0;
            bool showFacilitiesTab = false;

            if (hotelPage["HotelMenuNavigationList"] != null &&
                (hotelPage["HotelMenuNavigationList"].ToString().Contains(
                    "{" +
                    WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Facilities") +
                    "}")))
            {
                showFacilitiesTab = true;
            }
            else if (hotelPage["HotelMenuNavigationList"] == null)
            {
                showFacilitiesTab = true;
            }
            else if (Utility.CheckifFallback())
            {
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Facilities",
                    LanguageConstant.LANGUAGE_ENGLISH);
                showFacilitiesTab = true;
            }
            else
            {
                showFacilitiesTab = false;
            }

            return showFacilitiesTab;
        }

        /// <summary>
        /// Returns the value (true/false) for the visibility of Rooms tab 
        /// </summary>
        /// <param name="currPageTree"></param>
        /// <returns></returns>
        protected bool HideOrShowRoomsTab(PageDataCollection currPageTree)
        {
            PageData hotelPage = DataFactory.Instance.GetPage(CurrentPage.PageLink);
            int countRooms = 0;
            bool showRoomsTab = false;

            if (hotelPage["HotelMenuNavigationList"] != null &&
                (hotelPage["HotelMenuNavigationList"].ToString().Contains(
                    "{" +
                    WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Rooms") +
                    "}")))
            {
                showRoomsTab = true;
            }
            else if (hotelPage["HotelMenuNavigationList"] == null)
            {
                showRoomsTab = true;
            }
            else if (Utility.CheckifFallback())
            {
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Rooms",
                    LanguageConstant.LANGUAGE_ENGLISH);
                showRoomsTab = true;
            }
            else
            {
                showRoomsTab = false;
            }

            return showRoomsTab;
        }

        /// <summary>
        /// Returns the value (true/false) for the visibility of Meeting tab 
        /// </summary>
        /// <param name="currPageTree"></param>
        /// <returns></returns>
        protected bool HideOrShowMeetingsTab(PageDataCollection currPageTree)
        {
            PageData hotelPage = DataFactory.Instance.GetPage(CurrentPage.PageLink);
            bool showMeetingTab = false;
            int countMeetings = 0;

            if (hotelPage["HotelMenuNavigationList"] != null &&
                (hotelPage["HotelMenuNavigationList"].ToString().Contains("{"
                                                                          +
                                                                          WebUtil.GetTranslatedText(
                                                                              "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Meetings") +
                                                                          "}")))
            {
                showMeetingTab = true;
            }
            else if (hotelPage["HotelMenuNavigationList"] == null)
            {
                showMeetingTab = true;
            }
            else if (Utility.CheckifFallback())
            {
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Meetings",
                    LanguageConstant.LANGUAGE_ENGLISH);
                showMeetingTab = true;
            }
            else
            {
                showMeetingTab = false;
            }
            return showMeetingTab;
        }

        /// <summary>
        /// Returns the value (true/false) for the visibility of Guest tab 
        /// </summary>
        /// <returns></returns>
        protected bool HideOrShowGuestTab()
        {
            PageData hotelPage = DataFactory.Instance.GetPage(CurrentPage.PageLink);
            bool showGuestTab = false;

            if (hotelPage["HotelMenuNavigationList"] != null &&
                (hotelPage["HotelMenuNavigationList"].ToString().Contains("{"
                                                                          +
                                                                          WebUtil.GetTranslatedText(
                                                                              "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/GuestProgram") +
                                                                          "}")))
            {
                showGuestTab = true;
            }
            else if (hotelPage["HotelMenuNavigationList"] == null)
            {
                showGuestTab = true;
            }
            else if (Utility.CheckifFallback())
            {
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/GuestProgram",
                    LanguageConstant.LANGUAGE_ENGLISH);
                showGuestTab = true;
            }
            else
            {
                showGuestTab = false;
            }

            if (hotelPage["GuestProgramTopImage"] == null && hotelPage["GuestProgramMainBody"] == null
                && hotelPage["GuestProgramLeftBody"] == null)
                showGuestTab = false;
            else
                showGuestTab = (showGuestTab == false) ? false : true;

            return showGuestTab;
        }

        /// <summary>
        /// Returns the value (true/false) for the visibility of AdditionalFacilities tab 
        /// </summary>
        /// <param name="currPageTree"></param>
        /// <returns></returns>
        protected bool HideOrShowAdditionalFacilitiesTab(PageDataCollection currPageTree)
        {
            PageData hotelPage = DataFactory.Instance.GetPage(CurrentPage.PageLink);
            bool showAdditionalFacilitiesTab = false;
            int countAdditional = 0;

            if (hotelPage["HotelMenuNavigationList"] != null &&
                (hotelPage["HotelMenuNavigationList"].ToString().Contains("{" +
                                                                          CurrentPage["AdditionalFacilitiesTabName"] +
                                                                          "}")))
            {
                showAdditionalFacilitiesTab = true;
            }
            else if (hotelPage["HotelMenuNavigationList"] == null)
            {
                showAdditionalFacilitiesTab = true;
            }
            else if (Utility.CheckifFallback())
            {
                showAdditionalFacilitiesTab = true;
            }
            else
            {
                showAdditionalFacilitiesTab = false;
            }

            foreach (PageData pd in currPageTree)
            {
                if (pd.PageTypeName == "Additional Facility Container")
                {
                    foreach (PageData p in GetChildren(pd.PageLink))
                        countAdditional = countAdditional + 1;
                }
            }
            if (countAdditional == 0)
                showAdditionalFacilitiesTab = false;
            else
                showAdditionalFacilitiesTab = (showAdditionalFacilitiesTab == false) ? false : true;

            return showAdditionalFacilitiesTab;
        }

        /// <summary>
        /// Returns the value (true/false) for the visibility of TravellerReviews tab 
        /// </summary>       
        /// <returns></returns>
        protected bool HideOrShowTravellerReviewsTab()
        {
            PageData hotelPage = DataFactory.Instance.GetPage(CurrentPage.PageLink);
            return Utility.DisplayTripAdvisor(hotelPage);
        }

        #endregion Protected Methods
    }
}