<%@ Control Language="C#" AutoEventWireup="true" Codebehind="HotelLocationTransport.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelLocationTransport" %>
 <%@ Register Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3"
    TagPrefix="cc1" %>
    

<div id="HotelLocationTransport">
    <asp:Repeater ID="TravelInstructionListRepeater" OnItemDataBound="TravelInstruction_ItemDataBound"
        runat="server">
        <HeaderTemplate>
            <div class="borderContainer472">
                <div class="top">
                </div>
                <div class="middle">
                    <div class="content">
        </HeaderTemplate>
        <ItemTemplate>
            <div class="TransportRowItem">
                <div id="TransportOptionsContainer<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>" class='<%# JavaScriptEnabled()
                                         ? "TransportOptionsListNotExpanded"
                                         : "TransportOptionsListExpanded" %>'>
                    <div class="TransportOptionsContainer">
                        <div class="HeadLine">
                            <div class="HeadLineText">
                                <a href="#" class="TransportOptionsContractLink" onclick="SwitchClassName('TransportOptionsContainer<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>', 'TransportOptionsListExpanded', 'TransportOptionsListNotExpanded');return false;">
                                    <%# ((EPiServer.Core.PageData) Container.DataItem).PageName %>
                                </a><a href="#" class="TransportOptionsExpandLink" onclick="SwitchClassName('TransportOptionsContainer<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>', 'TransportOptionsListExpanded', 'TransportOptionsListNotExpanded');return false;">
                                    <%# ((EPiServer.Core.PageData) Container.DataItem).PageName %>
                                </a>
                            </div>
                        </div>
                        <div class="TransportOptionsListing">
                            <div id='HotelLocationTransport<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>'>
                                <div class="TransportContent">
                                    <h2>
                                        <EPiServer:Property ID="Property2" PropertyName="HeadLine1" runat="server" />
                                    </h2>
                                    <div>
                                        <EPiServer:Property ID="Property3" PropertyName="Description1" runat="server" />
                                    </div>
                                    <asp:PlaceHolder ID="ShowOnMap1PH" runat="server">
                                        <div class="LinkListItem">
                                            <div class="LastLink">
                                                <asp:LinkButton CssClass="IconLink" OnCommand="lbShowMap_Command" ID="lbShowMap1"
                                                    runat="server">
                                                    <EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                                        runat="server" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </asp:PlaceHolder>
                                </div>
                                <asp:PlaceHolder ID="TravelInstruction2PH" runat="server">
                                    <div class="TransportContent">
                                        <h2>
                                            <EPiServer:Property ID="Property6" PropertyName="HeadLine2" runat="server" />
                                        </h2>
                                        <div>
                                            <EPiServer:Property ID="Property7" PropertyName="Description2" runat="server" />
                                        </div>
                                        <asp:PlaceHolder ID="ShowOnMap2PH" runat="server">
                                            <div class="LinkListItem">
                                                <div class="LastItem">
                                                    <asp:LinkButton CssClass="IconLink" OnCommand="lbShowMap_Command" ID="lbShowMap2"
                                                        runat="server">
                                                        <EPiServer:Translate ID="Translate4" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                                            runat="server" />
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:PlaceHolder>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="TravelInstruction3PH" runat="server">
                                    <div class="TransportContent">
                                        <h2>
                                            <EPiServer:Property ID="Property4" PropertyName="HeadLine3" runat="server" />
                                        </h2>
                                        <div>
                                            <EPiServer:Property ID="Property5" PropertyName="Description3" runat="server" />
                                        </div>
                                        <asp:PlaceHolder ID="ShowOnMap3PH" runat="server">
                                            <div class="LinkListItem">
                                                <div class="LastItem">
                                                    <asp:LinkButton CssClass="IconLink" OnCommand="lbShowMap_Command" ID="lbShowMap3"
                                                        runat="server">
                                                        <EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                                            runat="server" />
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:PlaceHolder>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="TravelInstruction4PH" runat="server">
                                    <div class="TransportContent">
                                        <h2>
                                            <EPiServer:Property ID="Property8" PropertyName="HeadLine4" runat="server" />
                                        </h2>
                                        <div>
                                            <EPiServer:Property ID="Property9" PropertyName="Description4" runat="server" />
                                        </div>
                                        <asp:PlaceHolder ID="ShowOnMap4PH" runat="server">
                                            <div class="LinkListItem">
                                                <div class="LastItem">
                                                    <asp:LinkButton CssClass="IconLink" OnCommand="lbShowMap_Command" ID="lbShowMap4"
                                                        runat="server">
                                                        <EPiServer:Translate ID="Translate5" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                                            runat="server" />
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:PlaceHolder>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="TravelInstruction5PH" runat="server">
                                    <div class="TransportContent">
                                        <h2>
                                            <EPiServer:Property ID="Property10" PropertyName="HeadLine5" runat="server" />
                                        </h2>
                                        <div>
                                            <EPiServer:Property ID="Property11" PropertyName="Description5" runat="server" />
                                        </div>
                                        <asp:PlaceHolder ID="ShowOnMap5PH" runat="server">
                                            <div class="LinkListItem">
                                                <div class="LastItem">
                                                    <asp:LinkButton CssClass="IconLink" OnCommand="lbShowMap_Command" ID="lbShowMap5"
                                                        runat="server">
                                                        <EPiServer:Translate ID="Translate6" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                                            runat="server" />
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:PlaceHolder>
                                    </div>
                                </asp:PlaceHolder>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <div class="TransportRowAlternateItem">
                <div id="TransportOptionsContainer<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>"
                    class='<%# JavaScriptEnabled()
                                         ? "TransportOptionsListNotExpanded"
                                         : "TransportOptionsListExpanded" %>'>
                    <div class="TransportOptionsContainer">
                        <div class="HeadLine">
                            <div class="HeadLineText">
                                <a href="#" class="TransportOptionsContractLink" onclick="SwitchClassName('TransportOptionsContainer<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>', 'TransportOptionsListExpanded', 'TransportOptionsListNotExpanded');return false;">
                                    <%# ((EPiServer.Core.PageData) Container.DataItem).PageName %>
                                </a><a href="#" class="TransportOptionsExpandLink" onclick="SwitchClassName('TransportOptionsContainer<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>', 'TransportOptionsListExpanded', 'TransportOptionsListNotExpanded');return false;">
                                    <%# ((EPiServer.Core.PageData) Container.DataItem).PageName %>
                                </a>
                            </div>
                        </div>
                        <div class="TransportOptionsListing">
                            <div id='HotelLocationTransport<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>'>
                                <div class="TransportContent">
                                    <h2>
                                        <EPiServer:Property ID="Property2" PropertyName="HeadLine1" runat="server" />
                                    </h2>
                                    <div>
                                        <EPiServer:Property ID="Property3" PropertyName="Description1" runat="server" />
                                    </div>
                                    <asp:PlaceHolder ID="ShowOnMap1PH" runat="server">
                                        <div class="LinkListItem">
                                            <div class="LastLink">
                                                <asp:LinkButton CssClass="IconLink" OnCommand="lbShowMap_Command" ID="lbShowMap1"
                                                    runat="server">
                                                    <EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                                        runat="server" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </asp:PlaceHolder>
                                </div>
                                <asp:PlaceHolder ID="TravelInstruction2PH" runat="server">
                                    <div class="TransportContent">
                                        <h2>
                                            <EPiServer:Property ID="Property6" PropertyName="HeadLine2" runat="server" />
                                        </h2>
                                        <div>
                                            <EPiServer:Property ID="Property7" PropertyName="Description2" runat="server" />
                                        </div>
                                        <asp:PlaceHolder ID="ShowOnMap2PH" runat="server">
                                            <div class="LinkListItem">
                                                <div class="LastItem">
                                                    <asp:LinkButton CssClass="IconLink" OnCommand="lbShowMap_Command" ID="lbShowMap2"
                                                        runat="server">
                                                        <EPiServer:Translate ID="Translate4" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                                            runat="server" />
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:PlaceHolder>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="TravelInstruction3PH" runat="server">
                                    <div class="TransportContent">
                                        <h2>
                                            <EPiServer:Property ID="Property4" PropertyName="HeadLine3" runat="server" />
                                        </h2>
                                        <div>
                                            <EPiServer:Property ID="Property5" PropertyName="Description3" runat="server" />
                                        </div>
                                        <asp:PlaceHolder ID="ShowOnMap3PH" runat="server">
                                            <div class="LinkListItem">
                                                <div class="LastItem">
                                                    <asp:LinkButton CssClass="IconLink" OnCommand="lbShowMap_Command" ID="lbShowMap3"
                                                        runat="server">
                                                        <EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                                            runat="server" />
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:PlaceHolder>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="TravelInstruction4PH" runat="server">
                                    <div class="TransportContent">
                                        <h2>
                                            <EPiServer:Property ID="Property8" PropertyName="HeadLine4" runat="server" />
                                        </h2>
                                        <div>
                                            <EPiServer:Property ID="Property9" PropertyName="Description4" runat="server" />
                                        </div>
                                        <asp:PlaceHolder ID="ShowOnMap4PH" runat="server">
                                            <div class="LinkListItem">
                                                <div class="LastItem">
                                                    <asp:LinkButton CssClass="IconLink" OnCommand="lbShowMap_Command" ID="lbShowMap4"
                                                        runat="server">
                                                        <EPiServer:Translate ID="Translate5" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                                            runat="server" />
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:PlaceHolder>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="TravelInstruction5PH" runat="server">
                                    <div class="TransportContent">
                                        <h2>
                                            <EPiServer:Property ID="Property10" PropertyName="HeadLine5" runat="server" />
                                        </h2>
                                        <div>
                                            <EPiServer:Property ID="Property11" PropertyName="Description5" runat="server" />
                                        </div>
                                        <asp:PlaceHolder ID="ShowOnMap5PH" runat="server">
                                            <div class="LinkListItem">
                                                <div class="LastItem">
                                                    <asp:LinkButton CssClass="IconLink" OnCommand="lbShowMap_Command" ID="lbShowMap5"
                                                        runat="server">
                                                        <EPiServer:Translate ID="Translate6" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/ShowOnMap"
                                                            runat="server" />
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:PlaceHolder>
                                    </div>
                                </asp:PlaceHolder>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </div> </div>
            <div class="bottom">
                &nbsp;
            </div>
            </div>
        </FooterTemplate>
    </asp:Repeater>
    <div class="bottomGoogleMapContainer">
        <div class="borderContainer472map">
            <div class="top">
            </div>
            <div class="middle">
                <div class="map">
                <div id="GMapV3" style="height:265px;width:472px;"></div>  
                        <cc1:Map ID="GMapControl1"  runat="server" />
                
            </div>
            </div>
            <div class="bottom">
            </div>
        </div>
    </div>
</div>
