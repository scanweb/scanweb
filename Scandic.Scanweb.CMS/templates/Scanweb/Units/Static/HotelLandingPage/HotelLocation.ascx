<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelLocation.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelLocation" %>
<%--<%@ Register Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMap"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3"
    TagPrefix="cc1" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%--<script type="text/javascript" language="javascript" src="<%=ResolveUrl("~/Templates/Scanweb/Javascript/gmaps-utility-library/markermanager_packed.js")%>"></script>--%>
<div id="HotelLocationDetailedMapRight">
    <div class="borderContainer472map">
        <div class="top">
        </div>
        <div class="middle">
            <div class="map">
                <div id="GMapV3" style="height: 265px; width: 472px;">
                </div>
                <cc1:Map ID="GMapControl1" runat="server" />
            </div>
        </div>
        <%--<div class="bottom">
        </div>--%>
    </div>
    <div class="FindYourWay">
        <div class="headline">
            <episerver:translate id="Translate4" text="/Templates/Scanweb/Pages/HotelLandingPage/Location/FindYourWay"
                runat="server" />
        </div>
        <div>
            <episerver:property propertyname="HotelDrivingInstructionsHelpText" displaymissingmessage="false"
                runat="server" />
            <br />
        </div>
        <div id="ErrorFGP" class="redAlertIcon">
        </div>
        <div>
            <asp:RadioButtonList CssClass="rbList" RepeatDirection="Horizontal" ID="rbtnToFrom"
                runat="server" />
            <asp:TextBox CssClass="address" ID="tbDirection" runat="server" Style="float: left;" />
            <div class="actionBtn fltRt">
                <asp:Button ID="btnSubmit" CssClass="submi buttonInner travelinp scansprite" runat="server"
                    OnClick="btnSubmit_Click" />
                <%--<asp:Button ID="Button1" CssClass="buttonRt scansprite travelInst" runat="server" OnClick="btnSubmit_Click"/>--%>
            </div>
            <div class="clearAll">
                <!-- Clearing Float -->
            </div>
        </div>
        <div id="GoogleMapDirectionsInfo" class="RedText">
        </div>
        <!-- START Driving instruction -->
        <div class="Drive" id="drive" runat="server">
            <div class="Top">
            </div>
            <div class="middle" style="width: 100%;">
                <h3 class="darkHeading">
                    <episerver:translate id="Translate7" text="/Templates/Scanweb/Pages/HotelLandingPage/Location/DrivingInstructions"
                        runat="server" />
                </h3>
                <div id="Instructions">
                </div>
            </div>
            <div class="bottom">
            </div>
        </div>
    </div>
    <input type="hidden" id="valuenotentered" value="<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name")%>" />
</div>

<script type="text/javascript">

    $(document).ready(function() {
        $('input[id*="btnSubmit"]').click(function() {
            var searchFieldVal = $.trim($('input[id*="tbDirection"]').val());
            if (searchFieldVal == null || searchFieldVal == "") {
                validateDestination();
                return false;
            }
            else {
                $('#ErrorFGP').hide();
                return true;
            }
        });
    });

    function validateDestination() {
        var errorMessage = $('#valuenotentered').val();
        $('#ErrorFGP').show().html(errorMessage);
        $('.Drive').hide();
    }
</script>

