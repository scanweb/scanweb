<%@ Control Language="C#" AutoEventWireup="true" Codebehind="HotelOverview.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelOverview" %>
<%@ Register TagPrefix="Scanweb" TagName="LinkList" Src="~/Templates/Scanweb/Units/Placeable/LinkList.ascx" %>
<%@ Register Assembly="Scandic.Scanweb.CMS" Namespace="Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="ImageVault" Namespace="ImageStoreNET.Developer.WebControls"
    Assembly="ImageVault.Episerver6" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="OverviewBox" Src="~/Templates/Scanweb/Units/Placeable/OverviewBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="MoreImagesLink" Src="~/Templates/Scanweb/Units/Placeable/MoreImagesLink.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<script type="text/javascript">
    var addthis_config = { "data_track_addressbar": true };
    function TogglePoints() {    
     if ($('.HiddenItems').hasClass("hide")) {          
            $('.HiddenItems').attr("class", "show HiddenItems");
            //   $fn(_endsWith("Toggle")).text = '<%= Translate("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Hide")%>';
            $fn(_endsWith("Toggle")).style.display = 'none';   
        }
    };
    $(document).ready(function() {
        $('ul.fgpPoints li:last-child').css('border', 'none');
    });

 </script>
 <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50645171248e42f5"></script>
<div id="HotelOverview">
    <!-- Boxi : added hiddenfields for show/hide full description -->
    <input type="hidden" id="hdnViewFullDescription" name="hdnViewFullDescription" value="<% =
                WebUtil.GetTranslatedText(
                    "/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/ViewFullDescription") %>" />
    <input type="hidden" id="hdnHideFullDescription" name="hdnHideFullDescription" value="<% =
                WebUtil.GetTranslatedText(
                    "/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/HideFullDescription") %>" />
    
    <div>
    <Scanweb:OverviewBox ImagePropertyName="HotelImage" 
                         ImageWidth="718" 
                         StoolImagePropertyName="StoolImage"  
                         StoolImageWidth="200"
                         TextPropertyName="ImageTextArea" 
                         DisplayMoreImagesLink = "True"
                         ShowDefaultStoolImage = "true" 
                         runat="server" 
     />    
    </div>
     <!--START: ADDTHIS PLUGIN -->
    <div id ="addThis" runat="server" class="addthis_toolbox addthis_default_style" style="float: right; margin-bottom: 10px;">
      
    </div>   
		 <!--END: ADDTHIS PLUGIN -->
   	<div id="MainBodyLeftArea">
   	<div id="FGPPoints" runat="server" class="fgpPointsContent">
           <asp:Repeater ID="redemptionPoints" runat="server">
               <HeaderTemplate>
                   <span><h3 class="darkHeading headingPurple"><%= Translate("/Templates/Scanweb/Pages/HotelLandingPage/Overview/FGPPointsHeading")%>
                   <span class="help scansprite toolTipMe ie9tip fgpInfo" title="<%= Translate("/Templates/Scanweb/Pages/HotelLandingPage/Overview/FGPPointsHelp") %>"></span></h3></span>
                   <ul class="fgpPoints">
               </HeaderTemplate>
               <ItemTemplate>
                   <li id="points" runat="server"></li>
               </ItemTemplate>
               <FooterTemplate></ul></FooterTemplate>
           </asp:Repeater>
   	<a ID="Toggle" runat="server" onclick="TogglePoints()" class="showallLink" ><%= Translate("/Templates/Scanweb/Pages/HotelLandingPage/Overview/ShowAll")%></a>
   	</div>
   	<%--Commented out by Vaibhav: SCANMOB-465 -- %>
		<%--<div style="display:none" class="HotelLocationHeadLine">
				<h3  class="darkHeading"><%= Translate("/Templates/Scanweb/Pages/HotelLandingPage/Content/MoreInfoHeading") %></h3>
        </div>
        <div style="display:none" class="HotelOverviewTopRight">
            <Scanweb:LinkList  ID="LinkList1" runat="server" />
        </div>--%>
            <div class="HotelLocationHeadLine">
                <h3 class="darkHeading">
                    <EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/LocationInformation"
                        runat="server" />
                </h3>
            </div>
        <div id="HotelOverviewLocation">
            <div class="hotelInformation">
                <div id="StreetAddressDiv" runat="server">
                    <asp:Literal ID="StreetAddress" runat="server"></asp:Literal>
                </div>
                <div>
                    <asp:Literal ID="PostCode" runat="server"></asp:Literal>
                   <!-- Archana | Postal City Changes -->
                    <asp:Literal ID="PostalCity" runat="server"></asp:Literal>
                    <asp:Literal ID="City" runat="server"></asp:Literal>
                </div>
                <div>
                    <asp:Literal ID="Country" runat="server"></asp:Literal>
                </div>
               <div class="hotelPhone">
                <EPiServer:Translate ID="Translate4" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/Phone"
                    runat="server" />:
               
                    <asp:Literal ID="Phone" runat="server"></asp:Literal>
                </div>
                
                <div id="FaxDiv" runat="server">
                <EPiServer:Translate ID="Translate5" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/Fax"
                    runat="server" />:
               
                    <asp:Literal ID="Fax" runat="server"></asp:Literal>
                </div>
				<div>
                <EPiServer:Translate ID="Translate6" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/Email"
                    runat="server" />:
                
                <div class="HotelEmailLink">
                    <a href="mailto:<%# hotelPage["Email"] %>">
                        <%# hotelPage["Email"] %>
                    </a>
                </div>
				</div>
				 <!--R1.8.2 | Central reservation Number added after Email information-->
				<asp:PlaceHolder ID="PhoneNumberPlaceHolder" runat="server">
                    <div>
                        <EPiServer:Translate ID="Translate10" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/CentralReservationNumber"
                        runat="server" />:
                        <div>
                            <asp:Literal ID="CentralPhoneNumber" runat="server"></asp:Literal>
                        </div>
                    </div>
                </asp:PlaceHolder>
                <!-- START : CR 11 | Gps Co-ordinates in Hotel pages | Release 1.4  -->
                <!--<EPiServer:Translate ID="Translate7" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/GpsCordinate" runat="server" />:-->
                <div class="hotelLatitude">
                <EPiServer:Translate ID="Translte8" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/Latitude" runat="server"/>&nbsp;-&nbsp;
                <asp:Literal ID="Latitude" runat="server"></asp:Literal>
                <br />
                <EPiServer:Translate ID="Translate9" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/Longitude" runat="server"/>&nbsp;-&nbsp;
                <asp:Literal ID="Longitude" runat="server"></asp:Literal>
                </div>
                <!-- END : CR 11 | Gps Co-ordinates in Hotel pages | Release 1.4  -->
            </div>
			<div class="borderContainer226map">
				<h3 class="darkHeading">
				<EPiServer:Translate ID="Map" Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Map"
                        runat="server" />
				</h3>
                <div class="top">
        </div>
                <div class="middle">
                    <asp:PlaceHolder ID="GoogleMapsPlaceHolder" runat="server">
                    <div class="map">
                         <div id="GMapV3" style="height:230px;width:226px;"></div>  
                        <cc1:Map ID="GMapControl1" Width="226" Height="230" runat="server" />
                    </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="AlternativeMapPlaceHolder" runat="server" Visible="false">
                        <EPiServer:Property PropertyName="AlternativeGoogleMapImage" ImageWidth="226" runat="server" />       
                    </asp:PlaceHolder>
                </div>
                <div class="bottom">
                </div>
            </div>  
       </div>
	</div>
	<div id="MainBodyRightArea">
		 <div>
		
		 	<h2 class="clear heading1">
            <%--<EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Units/Static/HotelWelcome" runat="server" />--%>
                <%# hotelPage["WelcomeHeading"] as string %>
            </h2>
		</div>
        
        <!-- R2.2 Author: Sheril -->
        <!-- BEGIN Hotel Introduction Content -->
      	<div class="hotelIntroCnt">
      	    <EPiServer:Property ID="HotelIntro" PropertyName="HotelIntro" Visible="true" runat="server" />	
        </div>
        <!-- END Hotel Introduction Content -->

   		<div class="HotelOverviewBody">
       		<EPiServer:Property ID="HotelDescription" PropertyName="HotelDescription" Visible="true" runat="server" />	
		</div>
		<% if (!string.IsNullOrEmpty(hotelPage["FacilitiesDescription"] as string))
           {%>
		<div class="HotelFacilityHeadLine">                
                <h3 class="heading2">
                    <EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/Facilities"
                        runat="server" />
                </h3>
        </div>
        <% } %>
        <!-- START FACILITIES -->
        <div id="HotelOverviewFacilities">
            <div class="facilitesDescription">
                <EPiServer:Property ID="FacilitiesDescription" PropertyName="FacilitiesDescription" Visible="true" runat="server" />	
            </div>
            <!-- CODE ADDED FOR VIEW FULL DESCRIPTION LINK -->
            <!-- ADDED BY ANIL ON 12 SEP 2011 -->
            <!-- Text value needs to be altered so that language is read from xml file -->
                <div id ="divFacilityDetails" runat="server" class="hotelFacilites" />
                <!-- View More/Less Link -->
                <%--<asp:LinkButton class="hotelFacilitiesBtn" Text="View Full Description" runat="server" Visible="true" ID="lnkViewFullDescription" />--%>
                <a class="hotelFacilitiesBtn" href="javascript:showHideFacilities();" runat="server" Visible="true" ID="lnkViewFullDescription"/>
            <%--Special Alert - Start--%>
            <div class="alertwrappermiddlecolunm" id="divSpAlertWrap" runat="server">
                <div id="divSpAlert" runat="server" class="splalertcontent">
                </div>
            </div>
            <%--Special Alert - End--%>
            <!-- START FACILITY BOX -->
			<div class="HotelFacilityHeadLine">
                <h3 class="darkHeading">
					<EPiServer:Translate ID="Morefacts" Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/MoreFacts"
                        runat="server" />
				</h3>
			</div>
            <div class="borderContainer472">
                <div class="top">
                </div>
                <div class="middle">
                    <div class="content">
                        <div class="middleMiddleFacility">
                            <div id="divNoOfRoom" runat="server" visible="false">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/NoOfRooms"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                    <%= NoOfRooms %>
                                    </div>
                                </div>
                            </div>
                            <div id="divNonSmokingRooms" runat="server" visible="false">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/NonSmokingRooms"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                    <%= NoOfNonSmokingRooms %>
                                    </div>
                                </div>
                            </div>
                            <div id="divRoomForDisabled" runat="server">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RoomsForDisabled"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                        <%= IsRoomsForDisabledAvailable %>
                                    </div>
                                </div>
                            </div>
                            <div id="divRelaxCenter" runat="server">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RelaxCenter"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                    <%= IsRelaxCenterAvailable %>
                                    </div>
                                </div>
                            </div>
                            <div id="divRestaurantBar" runat="server">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RestaurantBar"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                    <%= IsResturantAndBarAvailable %>
                                    </div>
                                </div>
                            </div>
                            <div id="divGarage" runat="server">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Garage"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                    <%= IsGarageAvailable %>
                                    </div>
                                </div>
                            </div>
                            <div id="divOutdoorParking" runat="server">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/OutdoorParking"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                    <%= IsOutDoorParkingAvailable %>
                                    </div>
                                </div>
                            </div>
                            <div id="divShop" runat="server">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Shop"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                    <%= IsShopAvailable %>
                                    </div>
                                </div>
                            </div>
                            <div id="divMeetingFacilities" runat="server">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/MeetingFacilities"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                    <%= IsMeetingRoomAvailable %>
                                    </div>
                                </div>
                            </div>
                            <div id="divEcoLable" visible ="false" runat="server">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/EcoLabeled"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                    <%= GetEcoLabeled %>
                                    </div>
                                </div>
                            </div>
                            <div id="divDistanceToCityCenter" runat="server" visible="false">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceCentre"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                    <%= GetCityCenterDistance %>
                                    </div>
                                </div>
                            </div>
                            <div id="divDistanceToAirPort" runat="server" visible="false">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceAirport"
                                            runat="server" />&nbsp;<%= GetAirPortName %>
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                    <%= GetAirportDistance %>
                                    </div>
                                </div>
                            </div>
                            <div id="divDistanceToTrainStation" runat="server" visible="false">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceTrain"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                    <%= GetTrainstationDistance %>
                                    </div>
                                </div>
                            </div>
                            <div id="divWireless" runat="server">
                                <div class="leftcolumn">
                                    <div class="textcont">
                                        <EPiServer:Translate Text="/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/WirelessInternet"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="rightcolumn">
                                    <div class="textcont">
                                    <%= IsWiFiAvailable %>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%-- </div>--%>
                    </div>
                </div>
                <div class="bottom">
                </div>
            </div>
        </div>
        <!-- END FACILITY BOX -->
	</div><!-- END FACILITIES -->
</div>
<!-- END OVERVIEW -->
<%--<div class="jqmWindow dialog jqmID2">
	<div class="hd sprite"></div>
	<div id ="divFacilityDetails" runat="server" class="cnt">
	</div>
	<div class="ft sprite"></div>
</div>--%>