//  Description					:   HotelMeetingsLeftRegion                               //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// HotelMeetingsLeftRegion
    /// </summary>
    public partial class HotelMeetingsLeftRegion : EPiServer.UserControlBase
    {
        public string images = WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/MoreImagesPopUp/Images");
        public string images360 = WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/MoreImagesPopUp/Image360");

        /// <summary>
        /// Public hotelPage property for visibility in markup
        /// </summary>
        public PageData hotelPage;

        /// <summary>
        /// Public countryPage property for visibility in markup
        /// </summary>
        public PageData countryPage;

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int HotelId = CurrentPage["OperaID"] != null ? Convert.ToInt32(CurrentPage["OperaID"].ToString()) : 0;
                string HotelName = CurrentPage["PageName"] != null ? Convert.ToString(CurrentPage["PageName"]) : null;
                //moreImgLink.Attributes.Add("rel",
                                           //GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") +
                                           //"?methodToCall=GetImageGallery&HotelID=" +
                                           //HotelId + "&HotelName=" + HotelName);
                //moreImgLink.Attributes.Add("href", string.Format("{0}#", Request.RawUrl));
            }

            hotelPage = ((HotelLandingPage) this.Page).HotelPage;
            if (!string.IsNullOrEmpty(hotelPage["HotelMeetingEmail"] as string))
            {
                meetingEmail.HRef = string.Format("mailto:{0}", hotelPage["HotelMeetingEmail"] as string);
                meetingEmail.InnerText = hotelPage["HotelMeetingEmail"] as string;
            }
            if (string.IsNullOrEmpty(hotelPage["MeetingContact"] as string))
            {
                meetingPhone.InnerText = hotelPage["Phone"] as string;
            }
            else
            {
                meetingPhone.InnerText = hotelPage["MeetingContact"] as string;
            }
            #region LinkList

            //LinkList1.PopulateByQueryString(Request.QueryString["hotelpage"] as string ?? string.Empty);

            #endregion

            PageData p =
                DataFactory.Instance.GetPage(
                    (DataFactory.Instance.GetPage((DataFactory.Instance.GetPage(hotelPage.ParentLink)).ParentLink)).
                        ParentLink);
            int countryPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CountryPageTypeGUID"])).ID;
            countryPage = (p.PageTypeID == countryPageTypeID) ? p : null;

            if ((String.IsNullOrEmpty(hotelPage["MeetingContact"] as string ?? String.Empty))
                && (string.IsNullOrEmpty(hotelPage["Phone"] as string ?? string.Empty)))
            {
                HotelMeetingPlaceHolder.Visible = false;
            }

            if ((String.IsNullOrEmpty(countryPage["MeetingEmail"] as string ?? String.Empty))
                && (string.IsNullOrEmpty(countryPage["MeetingPhone"] as string ?? String.Empty)))
            {
                CentralMeetingPlaceHolder.Visible = false;
            }

            if (String.IsNullOrEmpty(countryPage["MeetingGuide"] as string ?? String.Empty))
                MeetingGuidePH.Visible = false;

            if (String.IsNullOrEmpty(hotelPage["HotelFloorplanImage"] as string ?? String.Empty))
                MeetingFloorplanImagePH.Visible = false;
            PageDataCollection pdc = MiscellaneousSessionWrapper.SearchedMeetingRoomsPDC as PageDataCollection ?? null;
            if (pdc != null)
            {
                BackToSearchPH.Visible = true;
            }
            else
                BackToSearchPH.Visible = false;
        }

        /// <summary>
        /// Gets search page url.
        /// </summary>
        /// <returns>search page url.</returns>
        protected string GetSearchPageURL()
        {
            string returnurl = (GetPage(hotelPage["MeetingSearchPage"] as PageReference).LinkURL ?? String.Empty);
            if (String.IsNullOrEmpty(returnurl))
            {
                BackToSearchPH.Visible = false;
                return String.Empty;
            }

            return returnurl;
        }

        /// <summary>
        /// Gets meeting guide url.
        /// </summary>
        /// <returns>meeting guide url.</returns>
        protected string GetMeetingGuideURL()
        {
            if (!String.IsNullOrEmpty(countryPage["MeetingGuide"] as string ?? String.Empty))
            {
                return countryPage["MeetingGuide"].ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets meeting floor plan popup.
        /// </summary>
        /// <returns>meeting floor plan popup.</returns>
        protected string MeetingFloorplanPopup()
        {
            var floorPlanUrl = string.Empty;
            var floorPlanImage = hotelPage["HotelFloorplanImage"] as string;
            if (!String.IsNullOrEmpty(floorPlanImage))
            {
                var imageWidth = Convert.ToInt32(AppConstants.HotelFloorPlanWidth);
                floorPlanUrl =  WebUtil.GetImageVaultImageUrl(floorPlanImage, imageWidth);
            }
            return floorPlanUrl;
        }
    }
}