<%@ Control Language="C#" AutoEventWireup="true" Codebehind="HotelMeetingsLeftRegion.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelMeetingsLeftRegion" %>
    <%@ Register TagPrefix="Scanweb" TagName="LinkList" Src="~/Templates/Scanweb/Units/Placeable/LinkList.ascx" %>
    <%@ Register TagPrefix="Scanweb" TagName="MoreImagesLink" Src="~/Templates/Scanweb/Units/Placeable/MoreImagesLink.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<div id="HotelMeetingsLeft">
    <div class="HotelMeetingInfoBox">
        <h3 class="darkHeading">
            <EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/RelevantLinks"
                runat="server" />
        </h3>
        <asp:PlaceHolder ID="BackToSearchPH" runat="server">
            <div class="LinkListItem">
                <div class="NotLastLink">
                    <a class="IconLink" href='<%= GetSearchPageURL() %>'>
                        <EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/BackToSearchResults"
                            runat="server" />
                    </a>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="MeetingGuidePH" runat="server">
        <div class="LinkListItem">
            <div class='<%= (!String.IsNullOrEmpty(countryPage["MeetingGuide"] as string ?? string.Empty) &&
                                !String.IsNullOrEmpty(hotelPage["HotelFloorplanImage"] as string ?? String.Empty))
                                   ? "NotLastLink"
                                   : "LastLink" %>'>
                <a class="DownloadIconLink" href='<%= GetMeetingGuideURL() %>'>
                    <EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/DownloadMeetingGuide"
                        runat="server" />
                </a>
            </div>
        </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="MeetingFloorplanImagePH" runat="server">
            <div class="LinkListItem">
                <div class="LastLink">
                    <a class="PopupIconLink" onclick="OpenPopUpwindow('<%= MeetingFloorplanPopup() %>','500','500');return false;"> 
                        <EPiServer:Translate ID="Translate4" Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/Floorplan"
                            runat="server" />
                    </a>
                </div>
            </div>
        </asp:PlaceHolder>
    </div>
    <div class="HotelMeetingInfoBox">
        <h3 class="darkHeading">
            <EPiServer:Translate ID="Translate5" Text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/ContactInformation"
                runat="server" />
        </h3>
         <div class="contactInfo">
            <asp:PlaceHolder ID="HotelMeetingPlaceHolder" runat="server">
            <EPiServer:Translate ID="Translate8" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/MeetingTeamAtHotel"
                runat="server" />:
				   <div>
              <label ID="meetingPhone" runat="server" type="text"></label>
            </div>
              <div class="HotelEmailLink">
               <a id="meetingEmail" runat="server"></a>
                </div>
            </asp:PlaceHolder>
             
            
        </div>
        
        <asp:PlaceHolder ID="CentralMeetingPlaceHolder" runat="server">
        <EPiServer:Translate ID="Translate7" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/CentralMeetingTeam"
                runat="server" />:
            <div class="HotelEmailLink">
                <a href='mailto:<%= (countryPage["MeetingEmail"] as string) ?? String.Empty %>'>
                    <%= (countryPage["MeetingEmail"] as string) ?? String.Empty %>
                </a>
                </div>
              <div>
                <%= (countryPage["MeetingPhone"] as string) ?? String.Empty %>
              </div>
        </asp:PlaceHolder>
    </div>
	<%--<div class="HotelLinkList">
    <div class="HotelLocationHeadLine">
		<h3 class="darkHeading"><%= Translate("/Templates/Scanweb/Pages/HotelLandingPage/Content/MoreInfoHeading") %></h3>
    </div>
    <div class="LinkListItem">
        <div class="NotLastLink">
            
            <!--R2.2.5-Parvathi-Artifact artf1193956 : Scanweb - links opens in wrong design-->
            <a href="#" class="IconLink jqModal" runat="server" id="moreImgLink"><EPiServer:Translate Text="/Templates/Scanweb/Units/Placeable/MoreImagesLink/MoreImagesLinkText" runat="server" /></a>
            <div class="jqmWindow dialog imgGalPopUp">
	        <div class="hd sprite"></div>
            <div class="cnt">
                <div id="imgGallery" class="scandicTabs">
                    <ul class="tabsCnt">
                    <!--artf1157344 : Image gallery | Tabs are in English on Swedish page -->
                        <li><a href="#" rel=".images" class="tabs active"><span><strong><%= images %></strong></span></a></li>
	                    <li><a href="#" class="tabs" rel=".galleryView"><span><strong><%= images360 %></strong></span></a></li>
                    </ul>
                </div>
                <div id ="imgGal" runat="server" class="bedTypeListclass">
                </div>
            </div>
            <!--R2.3.2-Parvathi-Artifact artf1193956 : Scanweb - links opens in wrong design-->
            <div class="ft sprite"></div>
        </div>
         <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/plugin/imageResizer.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>
        </div>
    </div>
    <Scanweb:LinkList ID="LinkList1" runat="server" />
    </div>--%>
</div>
