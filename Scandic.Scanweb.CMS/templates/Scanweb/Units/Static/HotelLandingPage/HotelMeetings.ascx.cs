//  Description					:   HotelMeetings                                         //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI.WebControls;
using EPiServer.Core;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.CMS.Util.DetectJavascript;
using Scandic.Scanweb.CMS.Util.ImageVault;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using System.Xml;
using EPiServer;
using System.Text;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Code behind of HotelMeetings control.
    /// </summary>
    public partial class HotelMeetings : EPiServer.UserControlBase
    {
        /// <summary>
        /// Gets/Sets MeetingRooms
        /// </summary>
        public PageDataCollection MeetingRooms { get; set; }

        private bool firstItem = true;

        /// <summary>
        /// Public hotelPage property for visibility in markup
        /// </summary>
        public PageData hotelPage;

        /// <summary>
        /// Page Load Method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            hotelPage = ((HotelLandingPage)this.Page).HotelPage;
            MeetingRoomsRepeater.DataSource = MeetingRooms;
            MeetingRoomsRepeater.DataBind();

            if (hotelPage != null)
            {
                PageData hotelContainer = GetPage(hotelPage.ParentLink);
                PageData cityPage = GetPage(hotelContainer.ParentLink);
                PageData countryPage = GetPage(cityPage.ParentLink);
                MiscellaneousSessionWrapper.CountryCurrency = Convert.ToString(countryPage["CountryCurrency"]);

                meetingCategoriesLeft.ItemDataBound += new RepeaterItemEventHandler(meetingCategoriesLeftCol_ItemDataBound);
                List<MeetingCategoryPrice> meetingCategoryLeft = GetMeetingCategories(hotelPage, true);
                if (meetingCategoryLeft != null && meetingCategoryLeft.Count > 0)
                {
                    meetingCategoriesLeft.DataSource = meetingCategoryLeft;
                    meetingCategoriesLeft.DataBind();
                }
                meetingCategoriesRight.ItemDataBound += new RepeaterItemEventHandler(meetingCategoriesRightCol_ItemDataBound);
                List<MeetingCategoryPrice> meetingCategoryRight = GetMeetingCategories(hotelPage, false);
                if (meetingCategoryRight != null && meetingCategoryRight.Count > 0)
                {
                    divMeetingCatRightCol.Visible = true;
                    meetingCategoriesRight.DataSource = meetingCategoryRight;
                    meetingCategoriesRight.DataBind();
                }
                if (!String.IsNullOrEmpty(hotelPage["MeetingImage"] as string))
                {
                    MeetingImgPH.Visible = true;
                    MeetingImage.DataBind();
                }
                PageDataCollection currPageTree = GetChildren(CurrentPage.PageLink);
                int countMeetings = 0;
                foreach (PageData pagedata in currPageTree)
                {
                    if (pagedata.PageTypeName == "Meeting Room Container")
                    {
                        foreach (PageData p in GetChildren(pagedata.PageLink))
                            countMeetings = countMeetings + 1;
                    }
                }
                if (countMeetings == 0)
                    MeetingRoomsPH.Visible = false;
            }
            SetVisibilityForProperty();
            bookAConference.Text = LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Static/SearchMeetingList/BookAConference");
        }

        private System.Collections.Generic.List<MeetingCategoryPrice> GetMeetingCategories(PageData pgData, bool leftCol)
        {
            List<MeetingCategoryPrice> meetingCategoryPrice = new List<MeetingCategoryPrice>();
            if (leftCol)
            {
                GetMeetingCategoryPriceCollection(pgData, meetingCategoryPrice, "1");
                GetMeetingCategoryPriceCollection(pgData, meetingCategoryPrice, "2");
                GetMeetingCategoryPriceCollection(pgData, meetingCategoryPrice, "3");
            }
            else
            {
                GetMeetingCategoryPriceCollection(pgData, meetingCategoryPrice, "4");
                GetMeetingCategoryPriceCollection(pgData, meetingCategoryPrice, "5");
                GetMeetingCategoryPriceCollection(pgData, meetingCategoryPrice, "6");
            }
            return meetingCategoryPrice;
        }

        private void GetMeetingCategoryPriceCollection(PageData pgData, System.Collections.Generic.List<MeetingCategoryPrice> meetingCategoryPrice, string number)
        {
            MeetingCategoryPrice categoryPrice = new MeetingCategoryPrice();
            string categoryData = "MeetingCategory" + number;
            string categoryPriceData = "MeetingCategoryPrice" + number;
            if (pgData[categoryData] != null)
            {
                categoryPrice = new MeetingCategoryPrice();
                categoryPrice.MeetingCategoryHotelPrice = Convert.ToString(pgData[categoryPriceData]);
                categoryPrice.MeetingCategoryPageData = GetPage((PageReference)pgData[categoryData]);
                meetingCategoryPrice.Add(categoryPrice);
            }
        }

        private void meetingCategoriesLeftCol_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                MeetingCategoryPrice pageData = e.Item.DataItem as MeetingCategoryPrice;
                if (pageData != null)
                {
                    var meetingCategory = e.Item.FindControl("meetingCategoryLeft") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (meetingCategory != null)
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(pageData.MeetingCategoryPageData["MeetingMetaDescription"])))
                            meetingCategory.Attributes.Add("title", Convert.ToString(pageData.MeetingCategoryPageData["MeetingMetaDescription"]));
                        else
                            meetingCategory.Attributes.Add("title", Convert.ToString(CurrentPage["HotelDescription"]));

                        meetingCategory.InnerHtml = Convert.ToString(pageData.MeetingCategoryPageData["Heading"]);
                    }
                    var price = e.Item.FindControl("priceLeft") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (price != null)
                        price.InnerHtml = pageData.MeetingCategoryHotelPrice;
                    var currency = e.Item.FindControl("currencyLeft") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (currency != null)
                        currency.InnerHtml = Convert.ToString(MiscellaneousSessionWrapper.CountryCurrency);
                }
            }
        }

        private void meetingCategoriesRightCol_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                MeetingCategoryPrice pageData = e.Item.DataItem as MeetingCategoryPrice;
                if (pageData != null)
                {
                    var meetingCategory = e.Item.FindControl("meetingCategoryRight") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (meetingCategory != null)
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(pageData.MeetingCategoryPageData["MeetingMetaDescription"])))
                            meetingCategory.Attributes.Add("title", Convert.ToString(pageData.MeetingCategoryPageData["MeetingMetaDescription"]));
                        else
                            meetingCategory.Attributes.Add("title", Convert.ToString(CurrentPage["HotelDescription"]));

                        meetingCategory.InnerHtml = Convert.ToString(pageData.MeetingCategoryPageData["Heading"]);
                    }
                    var price = e.Item.FindControl("priceRight") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (price != null)
                        price.InnerHtml = pageData.MeetingCategoryHotelPrice;
                    var currency = e.Item.FindControl("currencyRight") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (currency != null)
                        currency.InnerHtml = Convert.ToString(MiscellaneousSessionWrapper.CountryCurrency);
                }
            }
        }

        /// <summary>
        /// bookAConference_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bookAConference_Click(object sender, EventArgs e)
        {
            Dictionary<int, string> selectedHotelsDictionary = new Dictionary<int, string>();
            selectedHotelsDictionary.Add(CurrentPage.PageLink.ID, CurrentPage["Heading"] as string ??
                                                String.Empty + ", " + CurrentPage["City"] as string ?? String.Empty);
            MiscellaneousSessionWrapper.MeetingsSelectedHotels = selectedHotelsDictionary;
            PageData RootPage = DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess);
            PageData pageRFP = DataFactory.Instance.GetPage(RootPage["RequestForPorposalPage"] as PageReference);
            string LinkToRFPage = pageRFP.LinkURL.ToString();
            Response.Redirect(LinkToRFPage);
        }
        /// <summary>
        /// Handles the ItemDataBound event for the repeater
        /// </summary>
        /// <param name="Sender">Sender object</param>
        /// <param name="e">RepeaterItemEventArgs</param>
        public void MeetingRooms_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ((Property)e.Item.FindControl("MeetingRoomImage")).InnerProperty =
                    (((PageData)e.Item.DataItem)["MeetingRoomImage"] != null)
                        ? GetIVImage(((PageData)e.Item.DataItem)["MeetingRoomImage"],226)
                        : null;
                ((Property)e.Item.FindControl("FloorPlanImage")).InnerProperty =
                    (((PageData)e.Item.DataItem)["FloorPlanImage"] != null)
                        ? GetIVImage(((PageData)e.Item.DataItem)["FloorPlanImage"],226)
                        : null;

                if (((Property)e.Item.FindControl("MeetingRoomImage")).InnerProperty == null &&
                    ((Property)e.Item.FindControl("FloorPlanImage")).InnerProperty == null)
                    ((PlaceHolder)
                     e.Item.FindControl("HotelMeetingImagesToBeHidden" + (((PageData)e.Item.DataItem).PageLink.ID))).
                        Visible = false;

                ((Literal)e.Item.FindControl("DimensionLength")).Text =
                    (((PageData)e.Item.DataItem)["DimensionLength"] != null)
                        ? ((double)((PageData)e.Item.DataItem)["DimensionLength"]).ToString("N1")
                        : String.Empty;
                ((Literal)e.Item.FindControl("DimensionWidth")).Text =
                    (((PageData)e.Item.DataItem)["DimensionWidth"] != null)
                        ? ((double)((PageData)e.Item.DataItem)["DimensionWidth"]).ToString("N1")
                        : String.Empty;
                ((Literal)e.Item.FindControl("DimensionHeight")).Text =
                    (((PageData)e.Item.DataItem)["DimensionHeight"] != null)
                        ? ((double)((PageData)e.Item.DataItem)["DimensionHeight"]).ToString("N1")
                        : String.Empty;
                ((Literal)e.Item.FindControl("AccessSizeLength")).Text =
                    (((PageData)e.Item.DataItem)["AccessSizeLength"] != null)
                        ? ((double)((PageData)e.Item.DataItem)["AccessSizeLength"]).ToString("N1")
                        : String.Empty;
                ((Literal)e.Item.FindControl("AccessSizeHeight")).Text =
                    (((PageData)e.Item.DataItem)["AccessSizeHeight"] != null)
                        ? ((double)((PageData)e.Item.DataItem)["AccessSizeHeight"]).ToString("N1")
                        : String.Empty;

                if (((PageData)e.Item.DataItem)["Area"] == null)
                    ((PlaceHolder)e.Item.FindControl("MeetingRoomSqmPH")).Visible = false;

                if (((PageData)e.Item.DataItem)["CapacityUShape"] == null)
                    ((PlaceHolder)e.Item.FindControl("MeetingRoomUShapePH")).Visible = false;

                if (((PageData)e.Item.DataItem)["CapacityClassroom"] == null)
                    ((PlaceHolder)e.Item.FindControl("MeetingRoomClassroomPH")).Visible = false;

                if (((PageData)e.Item.DataItem)["CapacityBoardroom"] == null)
                    ((PlaceHolder)e.Item.FindControl("MeetingRoomBoardroomPH")).Visible = false;

                if (((PageData)e.Item.DataItem)["CapacityTheatre"] == null)
                    ((PlaceHolder)e.Item.FindControl("MeetingRoomTheatrePH")).Visible = false;

                if (((PageData)e.Item.DataItem)["LocationInHotel"] == null)
                    ((PlaceHolder)e.Item.FindControl("MeetingRoomLocationPH")).Visible = false;

                if (((PageData)e.Item.DataItem)["AccessSizeLength"] == null &&
                    ((PageData)e.Item.DataItem)["AccessSizeHeight"] == null)
                    ((PlaceHolder)e.Item.FindControl("MeetingRoomAccessPH")).Visible = false;
                if (((PageData)e.Item.DataItem)["Lighting"] == null)
                    ((PlaceHolder)e.Item.FindControl("MeetingRoomLightingPH")).Visible = false;

                if (((PageData)e.Item.DataItem)["DimensionLength"] == null &&
                    ((PageData)e.Item.DataItem)["DimensionWidth"] == null
                    && ((PageData)e.Item.DataItem)["DimensionHeight"] == null)
                    ((PlaceHolder)e.Item.FindControl("MeetingRoomDimensionPH")).Visible = false;
            }
        }

        /// <summary>
        /// Returns the <see cref="PropertyData"/> for the Meeting image
        /// </summary>
        /// <returns>The <see cref="PropertyData"/> for the Meeting image</returns>
        protected PropertyData GetMeetingImageFromImageVault(int imageWidth)
        {
            PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET", "ImageStoreNET.ImageType");
            string strImage = hotelPage["MeetingImage"] as string;
            if (!String.IsNullOrEmpty(strImage))
            {
                pd.Value = WebUtil.GetImageVaultImageUrl(strImage,imageWidth);
            }
            return pd;
        }


        /// <summary>
        /// Returns the alttext for the Meeting image
        /// </summary>
        /// <returns>The alttext as <see cref="String"/></returns>
        protected string GetMeetingImageAltText()
        {
            LangAltText altText = new LangAltText();

            return altText.GetAltText(hotelPage.LanguageID, hotelPage["MeetingImage"].ToString());
        }


        /// <summary>
        /// Gets the <see cref="PropertyData"/> from ImageVault for the attraction
        /// </summary>
        /// <param name="objectImage">The object containing the image</param>
        /// <returns>The <see cref="PropertyData"/> for the ImageVault Image</returns>
        public PropertyData GetIVImage(object objectImage, int imageWidth)
        {
            PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET", "ImageStoreNET.ImageType");
            string strImage = objectImage as string;

            if (!String.IsNullOrEmpty(strImage))
            {
                pd.Value = WebUtil.GetImageVaultImageUrl(strImage, imageWidth);
            }

            return pd;
        }

        /// <summary>
        /// Converts meter to feet
        /// </summary>
        /// <param name="meter">Meters</param>
        /// <returns>Feet as double</returns>
        protected double GetMeterToFeet(double meter)
        {
            return 3.2808 * meter;
        }

        /// <summary>
        /// Returns the alttext for the image
        /// </summary>
        /// <param name="p">The <see cref="PageData"/> for the image</param>
        /// <param name="propertyName">The propertyname of the page data for this image</param>
        /// <returns>The alt text for the image</returns>
        protected string GetAltText(PageData p, string propertyName)
        {
            LangAltText altText = new LangAltText();

            if (p[propertyName] != null)
            {
                return altText.GetAltText(p.LanguageID, p[propertyName].ToString());
            }

            return "";
        }

        /// <summary>
        /// Gets the status JavaScriptEnabled or not.
        /// </summary>
        /// <returns></returns>
        protected bool JavaScriptEnabled()
        {
            if (JavascriptControl.GetState() == JavascriptControl.JavaScriptState.Enabled)
                return true;

            return false;
        }

        /// <summary>
        /// IsFirstItem
        /// </summary>
        /// <returns>True/False</returns>
        protected bool IsFirstItem()
        {
            if (firstItem)
            {
                firstItem = false;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Set the Visibility for Episerver:Properties
        /// <remarks>
        /// If the property is absent then set the visibility to false
        /// </remarks>
        /// </summary>
        private void SetVisibilityForProperty()
        {
            MeetingIntro.Visible = !(hotelPage["MeetingIntro"] == null);
            MeetingDescription.Visible = !(hotelPage["MeetingDescription"] == null);
            MeetingAdditionalDescription.Visible = !(hotelPage["MeetingAdditionalDescription"] == null);
        }

        private const int _repeaterTotalColumns = 2;
        private int _repeaterCount = 0;
        private int _repeaterTotalBoundItems = 0;

        protected void litItem_DataBinding(object sender, System.EventArgs e)
        {
            MeetingCategoryPrice pageData = ((RepeaterItem)(((System.Web.UI.Control)(sender)).BindingContainer)).DataItem as MeetingCategoryPrice;

            Literal lt = (Literal)(sender);
            _repeaterCount++;
            if (_repeaterCount % _repeaterTotalColumns == 1)
            {
                lt.Text = "<tr>";
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("<span class='green_tick'></span>");
            //sb.Append("<span class='meetingCategory' id='meetingCategory'>" + Convert.ToString(pageData.MeetingCategoryPageData["Heading"]) + "</span>");
            sb.Append("<span class='meetingCategory' id='meetingCategory'>" + Convert.ToString(pageData.MeetingCategoryPageData["Heading"]));
            //sb.Append("<span>");
            sb.Append(LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Static/SearchMeetingList/From"));
            //sb.Append("</span>");
            //sb.Append("<span id='price' runat='server'> " + pageData.MeetingCategoryHotelPrice + " </span>");
            sb.Append(pageData.MeetingCategoryHotelPrice);
            //sb.Append("<span id='currency' runat='server'>" + Convert.ToString(MiscellaneousSessionWrapper.CountryCurrency) + "</span>");
            sb.Append(Convert.ToString(MiscellaneousSessionWrapper.CountryCurrency) + "</span>");

            lt.Text += string.Format("<td>{0}</td>", sb.ToString());

            if (_repeaterCount == _repeaterTotalBoundItems)
            {
                // Last item so put in the extra <td> if required
                for (int i = 0;
                     i < (_repeaterTotalColumns - (_repeaterCount % _repeaterTotalColumns));
                     i++)
                {
                    lt.Text += "<td></td>";
                }
                lt.Text += "</tr>";
            }

            if (_repeaterCount % _repeaterTotalColumns == 0)
            {
                lt.Text += "</tr>";
            }
        }


    }
}