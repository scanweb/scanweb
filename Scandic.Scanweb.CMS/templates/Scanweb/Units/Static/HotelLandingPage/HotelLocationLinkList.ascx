<%@ Control Language="C#" AutoEventWireup="true" Codebehind="HotelLocationLinkList.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelLocationLinkList" %>
<div class="LeftListNavigation">
<div id="Listlinkcontainer">
    <div class="LinkListItem">
        <div class="NotLastLink">
            <a href='<%= GetLocationPageURL("detailedmap") %>' class='<%= IsActive("detailedmap") ? "IconLink Active" : "IconLink" %>'>
                <EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/DetailedMap"
                    runat="server" />
            </a>
        </div>
    </div>
    <asp:PlaceHolder ID="hideTransportOptions" runat="server">
    <div class="LinkListItem">
        <div class="<%= getCssClass() ? "LastLink" : "NotLastLink" %>">
            <a href='<%= GetLocationPageURL("transportoptions") %>' class='<%= IsActive("transportoptions") ? "IconLink Active" : "IconLink" %>'>
                <EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/TransportOptions"
                    runat="server" />
            </a>
        </div>
    </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="hideLocalAttraction" runat="server">
    <div class="LinkListItem">
        <div class="<%= getCssClass() ? "NotLastLink" : "LastLink" %>">
            <a href='<%= GetLocationPageURL("localattractions") %>' class='<%= IsActive("localattractions") ? "IconLink Active" : "IconLink" %>'>
                <EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Pages/HotelLandingPage/Location/LocalAttractions"
                    runat="server" />
            </a>
        </div>
    </div>
    </asp:PlaceHolder>
</div>
</div>