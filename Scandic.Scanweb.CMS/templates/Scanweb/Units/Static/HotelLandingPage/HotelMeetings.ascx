<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelMeetings.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelMeetings" %>
<div id="HotelMeetings" class="newHotelMeetings">
    <asp:PlaceHolder ID="MeetingImgPH" Visible="false" runat="server">
        <div class="RoundedCornersTop472">
        </div>
        <div class="RoundedCornersImage472">
            <episerver:property id="MeetingImage" innerproperty="<%# GetMeetingImageFromImageVault(472) %>"
                imagewidth="472" runat="server" alt="<%# GetMeetingImageAltText() %>" />
        </div>
    </asp:PlaceHolder>
    <div class="RoundedCornersBottom472">
    </div>
    <asp:PlaceHolder ID="MeetingContentPH" runat="server">
        <h1>
            <%= hotelPage["MeetingTitle"] ?? String.Empty %>
        </h1>
        <h2>
            <episerver:property id="MeetingIntro" propertyname="MeetingIntro" visible="true"
                runat="server" />
        </h2>
        <div class="meetingHotelWrapper">
            <span class="meetingCatHeading">
                <episerver:translate id="conferenceText" text="/Templates/Scanweb/Units/Static/SearchMeetingList/Conference"
                    runat="server" />
            </span>
            <div class="meeting-cat-wrapper">
                <div id="divMeetingCatLeftCol" class="TwoColMeetingCat">
                    <ul class="meetingCatList" id="meetingCategoriesLeftDiv" runat="server">
                        <asp:Repeater ID="meetingCategoriesLeft" runat="server">
                            <ItemTemplate>
                                <li><span class="green_tick"></span><span class="meetingCategory toolTipMe" id="meetingCategoryLeft"
                                    runat="server"></span><span>
                                        <episerver:translate id="fromTextLeft" text="/Templates/Scanweb/Units/Static/SearchMeetingList/From"
                                            runat="server" />
                                    </span><span id="priceLeft" runat="server"></span>&nbsp;<span id="currencyLeft" runat="server">
                                    </span></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <div id="divMeetingCatRightCol" runat="server" visible="false" class="TwoColMeetingCat fltLft">
                    <ul class="meetingCatList" id="meetingCategoriesRightDiv" runat="server">
                        <asp:Repeater ID="meetingCategoriesRight" runat="server">
                            <ItemTemplate>
                                <li><span class="green_tick"></span><span class="meetingCategory toolTipMe" id="meetingCategoryRight"
                                    runat="server"></span><span>
                                        <episerver:translate id="fromTextRight" text="/Templates/Scanweb/Units/Static/SearchMeetingList/From"
                                            runat="server" />
                                    </span><span id="priceRight" runat="server"></span>&nbsp;<span id="currencyRight" runat="server">
                                    </span></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>
            <div class="actionBtn fltLft">
                <asp:Button ID="bookAConference" OnClick="bookAConference_Click" runat="server" CssClass="buttonInner" />
            </div>
        </div>
        <div>
            <episerver:property id="MeetingDescription" propertyname="MeetingDescription" visible="true"
                runat="server" />
        </div>
        <div>
            <episerver:property id="MeetingAdditionalDescription" propertyname="MeetingAdditionalDescription"
                visible="true" runat="server" />
        </div>
    </asp:PlaceHolder>
    <div id="MeetingsAndRooms">
        <asp:PlaceHolder ID="MeetingRoomsPH" runat="server">
            <h2>
                <episerver:translate id="Translate2" text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/MeetingRoomsAtThisHotel"
                    runat="server" />
            </h2>
            <asp:Repeater ID="MeetingRoomsRepeater" OnItemDataBound="MeetingRooms_ItemDataBound"
                runat="server">
                <ItemTemplate>
                    <div id="MeetingRoomContainer<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>"
                        class="<%= IsFirstItem() ? "MeetingRoomListExpanded" : "MeetingRoomListNotExpanded" %>">
                        <div class="MeetingRoomContainer">
                            <div class="MeetingRoomLeft">
                                <div class="MeetingRoomTopLeft">
                                    <div class="MeetingRoomContainer">
                                        <div class="expandcontract">
                                            <div class="MeetingRoomTitle">
                                                <a href="#" class="MeetingRoomContractLink" onclick="SwitchClassName('MeetingRoomContainer<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>', 'MeetingRoomListExpanded', 'MeetingRoomListNotExpanded');return false;">
                                                    <%# ((EPiServer.Core.PageData) Container.DataItem).PageName %>
                                                </a><a href="#" class="MeetingRoomExpandLink" onclick="SwitchClassName('MeetingRoomContainer<%# ((EPiServer.Core.PageData) Container.DataItem).PageLink.ID %>', 'MeetingRoomListExpanded', 'MeetingRoomListNotExpanded');return false;">
                                                    <%# ((EPiServer.Core.PageData) Container.DataItem).PageName %>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="MeetingRoomSqm" style="float: right">
                                            <asp:PlaceHolder ID="MeetingRoomSqmPH" runat="server">&nbsp;<%# ((EPiServer.Core.PageData) Container.DataItem)["Area"] ?? String.Empty %>&nbsp;<episerver:translate
                                                text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/Squaremeter" runat="server" />
                                            </asp:PlaceHolder>
                                        </div>
                                    </div>
                                </div>
                                <div class="MeetingRoomListing">
                                    <div>
                                        <episerver:property class="MeetingRoomImage" id="MeetingRoomImage" propertyname="MeetingRoomImage"
                                            imagewidth="226" runat="server" alt='<%# GetAltText((EPiServer.Core.PageData) Container.DataItem, "MeetingRoomImage") %>' />
                                    </div>
                                    <div>
                                        <episerver:property class="FloorPlanImage" id="FloorPlanImage" propertyname="FloorPlanImage"
                                            imagewidth="226" runat="server" alt='<%# GetAltText((EPiServer.Core.PageData) Container.DataItem, "FloorPlanImage") %>' />
                                    </div>
                                </div>
                            </div>
                            <div class="MeetingRoomRight">
                                <table>
                                    <asp:PlaceHolder ID="MeetingRoomUShapePH" runat="server">
                                        <tr>
                                            <td class="MeetingRightLeftColumn">
                                                <episerver:translate text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/UShape"
                                                    runat="server" />
                                            </td>
                                            <td class="MeetingRightRightColumn">
                                                <%# ((EPiServer.Core.PageData) Container.DataItem)["CapacityUShape"] ?? String.Empty %>
                                                &nbsp;<episerver:translate text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/People"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="MeetingRoomClassroomPH" runat="server">
                                        <tr>
                                            <td class="MeetingRightLeftColumn">
                                                <episerver:translate text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/Classroom"
                                                    runat="server" />
                                            </td>
                                            <td class="MeetingRightRightColumn">
                                                <%# ((EPiServer.Core.PageData) Container.DataItem)["CapacityClassroom"] ?? String.Empty %>
                                                &nbsp;<episerver:translate text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/People"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="MeetingRoomBoardroomPH" runat="server">
                                        <tr>
                                            <td class="MeetingRightLeftColumn">
                                                <episerver:translate text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/Boardroom"
                                                    runat="server" />
                                            </td>
                                            <td class="MeetingRightRightColumn">
                                                <%# ((EPiServer.Core.PageData) Container.DataItem)["CapacityBoardRoom"] ?? String.Empty %>
                                                &nbsp;<episerver:translate text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/People"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="MeetingRoomTheatrePH" runat="server">
                                        <tr>
                                            <td class="MeetingRightLeftColumn">
                                                <episerver:translate text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/Theatre"
                                                    runat="server" />
                                            </td>
                                            <td class="MeetingRightRightColumn">
                                                <%# ((EPiServer.Core.PageData) Container.DataItem)["CapacityTheatre"] ?? String.Empty %>
                                                &nbsp;<episerver:translate text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/People"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </asp:PlaceHolder>
                                </table>
                                <div class="MeetingRoomListing">
                                    <br />
                                    <asp:PlaceHolder ID="MeetingRoomLocationPH" runat="server">
                                        <div class="MeetingRoomLocation">
                                            <episerver:translate id="Translate3" text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/LocationInHotel"
                                                runat="server" />
                                            :
                                            <%# ((EPiServer.Core.PageData) Container.DataItem)["LocationInHotel"] ?? String.Empty %>
                                        </div>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="MeetingRoomAccessPH" runat="server">
                                        <div class="MeetingRoomAccess">
                                            <episerver:translate id="Translate4" text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/AccessSize"
                                                runat="server" />
                                            :
                                            <asp:Literal ID="AccessSizeLength" runat="server" />m x
                                            <asp:Literal ID="AccessSizeHeight" runat="server" />m (<episerver:translate id="Translate7"
                                                text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/LxH" runat="server" />)
                                        </div>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="MeetingRoomLightingPH" runat="server">
                                        <div class="MeetingRoomLighting">
                                            <episerver:translate id="Translate5" text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/Lighting"
                                                runat="server" />
                                            :
                                            <%# ((EPiServer.Core.PageData) Container.DataItem)["Lighting"] ?? String.Empty %>
                                        </div>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="MeetingRoomDimensionPH" runat="server">
                                        <div class="MeetingRoomDimension">
                                            <episerver:translate id="Translate6" text="/Templates/Scanweb/Pages/HotelLandingPage/Meetings/Dimension"
                                                runat="server" />
                                            :
                                            <asp:Literal ID="DimensionLength" runat="server" />m x
                                            <asp:Literal ID="DimensionWidth" runat="server" />m x
                                            <asp:Literal ID="DimensionHeight" runat="server" />m
                                        </div>
                                    </asp:PlaceHolder>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </asp:PlaceHolder>
    </div>
</div>
