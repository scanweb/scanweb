<%@ Control Language="C#" AutoEventWireup="true" Codebehind="HotelLocationLeftRegion.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelLocationLeftRegion" %>
<%@ Register TagPrefix="Scanweb" TagName="HotelInformation" Src="~/Templates/Scanweb/Units/Placeable/HotelPageHotelInformation.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="LinkList" Src="~/Templates/Scanweb/Units/Placeable/LinkList.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="HotelLocationLinkList" Src="~/Templates/Scanweb/Units/Static/HotelLandingPage/HotelLocationLinkList.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="MoreImagesLink" Src="~/Templates/Scanweb/Units/Placeable/MoreImagesLink.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<div id="HotelLocationLeft">
    <div class="HotelLocationLeftLinkList">
        <Scanweb:HotelLocationLinkList ID="LinkList2" runat="server" />
    </div>
	<%--<div class="HotelLinkList">
		<div class="HotelLocationHeadLine">
				<h3 class="darkHeading"><%= Translate("/Templates/Scanweb/Pages/HotelLandingPage/Content/MoreInfoHeading") %></h3>
		</div>
		<div class="LinkListItem">
            <div class="NotLastLink">
    	        
    	        <!--R2.3.2-Parvathi-Artifact artf1193956 : Scanweb - links opens in wrong design-->
    	        <a href="#" class="IconLink jqModal" runat="server" id="moreImgLink"><EPiServer:Translate Text="/Templates/Scanweb/Units/Placeable/MoreImagesLink/MoreImagesLinkText" runat="server" /></a>
    	    </div>
    	     <div class="jqmWindow dialog imgGalPopUp">
	        <div class="hd sprite"></div>
            <div class="cnt">
                <div id="imgGallery" class="scandicTabs">
                    <ul class="tabsCnt">
                    <!--artf1157344 : Image gallery | Tabs are in English on Swedish page -->
                        <li><a href="#" rel=".images" class="tabs active"><span><strong><%= images %></strong></span></a></li>
	                    <li><a href="#" class="tabs" rel=".galleryView"><span><strong><%= images360 %></strong></span></a></li>
                    </ul>
                </div>
                <div id ="imgGal" runat="server" class="bedTypeListclass">
                </div>
            </div>
            <div class="ft sprite"></div>
        </div>
        <!--R2.3.2-Parvathi-Artifact artf1193956 : Scanweb - links opens in wrong design-->
        
          <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/plugin/imageResizer.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>
    </asp:PlaceHolder>
    	</div>
    	<Scanweb:LinkList ID="LinkList1" runat="server" />
    </div>--%>
    <asp:PlaceHolder ID="LocationLeftDistancesPH" runat="server">
        <div class="HotelInfoBox">
            <h3 class="darkHeading">
            <EPiServer:Translate ID="Translate8" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/KilometersToDestination"
                    runat="server" />
            </h3>            
            <div>
                <table class="tbldist" cellpadding="0"; cellspacing="0">
           <asp:PlaceHolder ID="DistanceRow1" runat="server"> 
                    <tr>
                        <td class="leftcol">
                            <asp:Literal ID="ltlCentreDist" runat="server" /></td>
                        <td class="rightcol">
                            <asp:Literal ID="ltlCentre" runat="server" /></td>
                    </tr>
                    </asp:PlaceHolder>
                   
                   <asp:PlaceHolder ID="DistanceRow2" runat="server"> 
                    <tr>
                        <td class="leftcol">
                            <asp:Literal ID="ltlAirport1Dist" runat="server" /></td>
                        <td class="rightcol">
                            <asp:Literal ID="ltlAirport1" runat="server" /></td>
                    </tr>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="DistanceRow3" runat="server">
                    <tr>
                        <td class="leftcol">
                            <asp:Literal ID="ltlAirport2Dist" runat="server" /></td>
                        <td class="rightcol">
                            <asp:Literal ID="ltlAirport2" runat="server" /></td>
                    </tr>
                     </asp:PlaceHolder>
                    <asp:PlaceHolder ID="DistanceRow4" runat="server"> 
                    <tr>
                        <td class="leftcol">
                            <asp:Literal ID="ltlLandmark1Dist" runat="server" /></td>
                        <td class="rightcol">
                            <asp:Literal ID="ltlLandmark1" runat="server" /></td>
                    </tr>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="DistanceRow5" runat="server"> 
                    <tr>
                        <td class="leftcol">
                            <asp:Literal ID="ltlLandmark2Dist" runat="server" /></td>
                        <td class="rightcol">
                            <asp:Literal ID="ltlLandmark2" runat="server" /></td>
                    </tr>
                    </asp:PlaceHolder>
                </table>
            </div>
        </div>
    </asp:PlaceHolder>

    <div class="HotelInfoBox">        
        <Scanweb:HotelInformation ID="HotelInformation1" runat="server" />
    </div>
    <!-- START : R1.5 | artf812825 : Hotel contact info - GPS coordinates and email missing -->
    <%--<div>
        <EPiServer:Translate ID="Translte8" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/Latitude" runat="server"/>&nbsp;-&nbsp;
        <asp:Literal ID="Latitude" runat="server"></asp:Literal>
        <br />
        <EPiServer:Translate ID="Translate9" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/Longitude" runat="server"/>&nbsp;-&nbsp;
        <asp:Literal ID="Longitude" runat="server"></asp:Literal>
    </div>--%>
    <!-- END : R1.5 | artf812825 : Hotel contact info - GPS coordinates and email missing -->

</div>
