//  Description					:   HotelLocationLocalAttr                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Text;
using System.Web.UI.WebControls;
using EPiServer.Core;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.Util.ImageVault;
using Scandic.Scanweb.CMS.code.Util.Map;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Units.Static
{
    /// <summary>
    /// Code behind of HotelLocationLocalAttr control.
    /// </summary>
    public partial class HotelLocationLocalAttr : ScandicUserControlBase
    {
        private PageData hotelPage;

        /// <summary>
        /// The <see cref="PageDataCollection"/> for loacl attractions
        /// </summary>
        public PageDataCollection LocalAttrPages { get; set; }

        /// <summary>
        /// This method is used to return a combined string in a format
        /// streetAddress,PostCode City,Country
        /// </summary>
        /// <returns>addressbulder</returns>
        protected string GetAddress(PageData data)
        {
            StringBuilder addressString = new StringBuilder();
            string commma = ", ";

            if ((null != data["StreetAddress"]))
            {
                addressString.Append(data["StreetAddress"].ToString());


                if (null != data["PostCode"])
                {
                    addressString.Append(commma);
                    addressString.Append(data["PostCode"].ToString());
                    if (null != data["City"])
                    {
                        addressString.Append(commma);
                        addressString.Append(data["City"].ToString());

                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"].ToString());
                        }
                    }
                    else
                    {
                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"]);
                        }
                    }
                }
                else
                {
                    if (null != data["City"])
                    {
                        addressString.Append(commma);
                        addressString.Append(data["City"].ToString());

                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"].ToString());
                        }
                    }
                    else
                    {
                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"].ToString());
                        }
                    }
                }
            }
            else
            {
                if (null != data["PostCode"])
                {
                    addressString.Append(data["PostCode"].ToString());
                    if (null != data["City"])
                    {
                        addressString.Append(commma);
                        addressString.Append(data["City"].ToString());
                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"]);
                        }
                    }
                    else
                    {
                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"]);
                        }
                    }
                }
                else
                {
                    if (null != data["City"])
                    {
                        addressString.Append(data["City"].ToString());
                        if (null != data["Country"])
                        {
                            addressString.Append(commma);
                            addressString.Append(data["Country"]);
                        }
                    }
                    else
                    {
                        if (null != data["Country"])
                        {
                            addressString.Append(data["Country"]);
                        }
                    }
                }
            }
            return addressString.ToString();
        }

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            hotelPage = ((HotelLandingPage) this.Page).HotelPage;
            LocalAttrListRepeater.DataSource = LocalAttrPages;
            LocalAttrListRepeater.DataBind();

            if (!IsPostBack)
            {
                RenderGoogleMap(true, 0, 0, null);
            }
        }

        /// <summary>
        /// Renders the Google Map
        /// </summary>
        private void RenderGoogleMap(bool isHotel, double GeoX, double GeoY, IList<MapUnit> localAttractions)
        {
            #region GoogleMap

            CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            GMapControl1.GoogleMapKey = (ConfigurationManager.AppSettings["googlemaps." + Request.Url.Host] as string ?? (ConfigurationManager.AppSettings["DevKey"] as string));
            if (isHotel)
            {
                #region Collection with GoogleMapsHotelUnit

                IList<MapUnit> hotels = new List<MapUnit>();
                MapHotelUnit hotel = new MapHotelUnit(
                    (double) hotelPage["GeoX"],
                    (double) hotelPage["GeoY"],
                    -1,
                    string.Format("http://{0}/{1}", Request.Url.Host,
                                  "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_purple.png"),
                    string.Empty,
                    hotelPage["PageName"] as string,
                    "",
                    hotelPage["StreetAddress"] as string,
                    hotelPage["PostCode"] as string,
                    hotelPage["PostalCity"] as string,
                    hotelPage["City"] as string,
                    hotelPage["Country"] as string,
                    hotelPage["LinkURL"] as string,
                    GetDeepLinkingURL(hotelPage["OperaID"] as string ?? string.Empty)
                    );

                hotels.Add(hotel);

                #endregion
                GMapControl1.MapPageType = MapPageType.HOTELLOCATION;
                GMapControl1.Latitude = hotels[0].Latitude;
                GMapControl1.Longitude = hotels[0].Longitude;
                GMapControl1.DataSource = hotels;
                GMapControl1.MarkerLatitudeField = "latitude";
                GMapControl1.MarkerLongitudeField = "longitude";

                GMapControl1.CenterAndZoom(new MapPoint(hotels[0].Latitude, hotels[0].Longitude), 16);

                #endregion
            }
            else
            {
                if (localAttractions != null)
                {
                    GMapControl1.DataSource = localAttractions;
                  
                    GMapControl1.MapPageType = MapPageType.HOTELLOCATION;
                    GMapControl1.MarkerLatitudeField = "latitude";
                    GMapControl1.MarkerLongitudeField = "longitude";

                    GMapControl1.AutoCenterAndZoom();
                }
            }

            GMapControl1.DataBind();
        }

        /// <summary>
        /// Handles the ItemDataBound event for the repeater
        /// </summary>
        /// <param name="Sender">Sender object</param>
        /// <param name="e">RepeaterItemEventArgs</param>
        public void LocalAttr_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (((PageData) e.Item.DataItem)["GeoX"] != null &&
                    ((PageData) e.Item.DataItem)["GeoY"] != null)
                {
                    e.Item.FindControl("ShowOnMapPH").Visible = true;
                    ((LinkButton) e.Item.FindControl("lbShowMap")).CommandName = "Coordinates";
                    ((LinkButton) e.Item.FindControl("lbShowMap")).CommandArgument =
                        ((PageData) e.Item.DataItem)["GeoX"].ToString() + ";" +
                        ((PageData) e.Item.DataItem)["GeoY"].ToString() + ";" + ((PageData) e.Item.DataItem)["PageName"] +
                        ";" + ((PageData) e.Item.DataItem)["LinkText"] + ";" +
                        this.GetAddress((PageData) e.Item.DataItem) + ";" +
                        ((PageData) e.Item.DataItem)["AttractionURL"];
                }
                else
                    e.Item.FindControl("ShowOnMapPH").Visible = false;

                if (!string.IsNullOrEmpty((string) ((PageData) e.Item.DataItem)["LinkText"]))
                {
                    if (((string) ((PageData) e.Item.DataItem)["LinkText"]).Trim() != string.Empty)
                    {
                        e.Item.FindControl("AttractionDetailsPH").Visible = true;
                    }
                    else
                    {
                        e.Item.FindControl("AttractionDetailsPH").Visible = false;
                    }
                }
                else
                {
                    e.Item.FindControl("AttractionDetailsPH").Visible = false;
                }

                if (((PageData) e.Item.DataItem)["AttractionImage"] != null)
                    ((Property) e.Item.FindControl("LocalAttrImage")).InnerProperty =
                        GetIVImage(((PageData) e.Item.DataItem)["AttractionImage"], 226);
            }
        }

        /// <summary>
        /// LbShowMap command event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbShowMap_Command(Object sender, CommandEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.CommandName) && e.CommandName == "Coordinates")
            {
                string[] coords = e.CommandArgument.ToString().Split(new Char[] {';'});

                IList<MapUnit> localAttractions = new List<MapUnit>();
                MapLocalAttractionUnit localAttraction = new MapLocalAttractionUnit(
                    Convert.ToDouble(coords[0]),
                    Convert.ToDouble(coords[1]),
                    -1,
                    string.Format("http://{0}/{1}", Request.Url.Host,
                                  "/Templates/Scanweb/Styles/Default/Images/Icons/landmarks_marker.gif"),
                    string.Format("http://{0}/{1}", Request.Url.Host,
                                  "/Templates/Scanweb/Styles/Default/Images/Icons/shadow.png"),
                    coords[2],
                    coords[3],
                    coords[4],
                    coords[5]
                    );

                localAttractions.Add(localAttraction);


                RenderGoogleMap(false, double.Parse(coords[0]), double.Parse(coords[1]), localAttractions);
            }
        }

        /// <summary>
        /// Gets the URL for the attraction page
        /// </summary>
        /// <param name="attractionPage">The <see cref="PageData"/> for the attraction page</param>
        /// <returns>The URL for the attraction page</returns>
        public string GetURL(PageData attractionPage)
        {
            return (attractionPage["AttractionURL"] as string) ?? String.Empty;
        }

        /// <summary>
        /// Gets the Link Text for the Attraction Page
        /// </summary>
        /// <param name="attractionPage"></param>
        /// <returns>The Text for Link</returns>
        public string GetLinkText(PageData attractionPage)
        {
            return (attractionPage["LinkText"] as string) ?? String.Empty;
        }

        /// <summary>
        /// Gets the <see cref="PropertyData"/> from ImageVault for the attraction
        /// </summary>
        /// <param name="objectImage">The object containing the image</param>
        /// <returns>The <see cref="PropertyData"/> for the ImageVault Image</returns>
        public PropertyData GetIVImage(object objectImage, int imageWidth)
        {
            PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET", "ImageStoreNET.ImageType");
            string strImage = objectImage as string;

            if (!String.IsNullOrEmpty(strImage))
            {
                pd.Value = WebUtil.GetImageVaultImageUrl(strImage, imageWidth);
            }

            return pd;
        }

        /// <summary>
        /// Returns the alttext for the attraction image
        /// </summary>
        /// <param name="attractionPage">The <see cref="PageData"/> for the attraction</param>
        /// <returns>The alt text for the attraction image</returns>
        protected string GetAltText(PageData attractionPage)
        {
            LangAltText altText = new LangAltText();

            if (attractionPage["AttractionImage"] != null)
                return altText.GetAltText(attractionPage.LanguageID, attractionPage["AttractionImage"].ToString());

            return "";
        }
    }
}