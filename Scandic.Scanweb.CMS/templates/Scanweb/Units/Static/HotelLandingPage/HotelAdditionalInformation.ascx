<%@ Control Language="C#" AutoEventWireup="true" Codebehind="HotelAdditionalInformation.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.HotelAdditionalInformation" %>
<div id="HotelFacilities">
    <asp:PlaceHolder ID="ImagePlaceHolder" runat="server">
    <div class="RoundedCornersTop472">
    </div>
    <div class="RoundedCornersImage472">        
        <asp:Image ID="ContentImage" runat="server"/>
    </div>
    <div class="RoundedCornersBottom472">
    </div>    
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="ContentPH" runat="server">
        <h1>
            <EPiServer:Property ID="PageNameProperty" PropertyName="PageName" runat="server" />
        </h1>
        <h2>
            <EPiServer:Property ID="IntroProperty" PropertyName="MainIntroXHTML" runat="server" />
        </h2>
            <EPiServer:Property ID="ContentProperty" PropertyName="MainBody" runat="server" />
    </asp:PlaceHolder>
</div>
