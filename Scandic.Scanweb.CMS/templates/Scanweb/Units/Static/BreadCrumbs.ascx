<%@ Control Language="C#" EnableViewState="false" AutoEventWireup="False" CodeBehind="BreadCrumbs.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Static.BreadCrumbs" %>


<div class="bcrumbs" itemprop="breadcrumb"><asp:Literal runat="server" ID="Breadcrumbs" /></div>
<div class="pointsInfo"><span id="placeholderPoint" runat="server"></span></div>