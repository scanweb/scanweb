//  Description					: DisplayPoints                                           //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// Code behind DisplayPoints control.
    /// </summary>
    public partial class DisplayPoints : ScandicUserControlBase
    {
        /// <summary>
        /// DisplayBonusPoints
        /// </summary>
        public void DisplayBonusPoints()
        {
            string pointString = WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/pointsheading");
            if (LoyaltyDetailsSessionWrapper.LoyaltyDetails != null) 
            {
                double noOfPoints = LoyaltyDetailsSessionWrapper.LoyaltyDetails.CurrentPoints;
                string generatedString = string.Format(pointString, noOfPoints);
                placeholderPoint.InnerHtml = generatedString;
            }
        }

        /// <summary>
        /// Onload event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            //DisplayBonusPoints(); //No Need to display points here as they will be available on Logout Popup now.
        }
    }
}