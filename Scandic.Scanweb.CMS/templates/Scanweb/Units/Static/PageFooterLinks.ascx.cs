//  Description					:   PageFooterLinks                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region Using
using System;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util.Filters;
#endregion Using

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// Code behind of PageFooterLinks control.
    /// </summary>
    public partial class PageFooterLinks : EPiServer.UserControlBase
    {
        #region Constants
        private const string SITE_FOOTER_CONTAINER_STOOL_IMAGE = "ContainerStoolImage";
        private const string SITE_FOOTER_CONTAINER_TEXT_AREA = "ContainerTextArea";
        private const string LINK_TO_ANOTHER_PAGE = "LinkToAnotherPage";
        private const string SITE_FOOTER_HEADING = "SiteFooterHeading";
        #endregion

        #region Protected Methods
        /// <summary>
        ///  Repeater, QuickLinksAndCustomerService, item data bound event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RptQuickLinksAndCustomerService_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                string pageURL = string.Empty;
                PageData pageData = e.Item.DataItem as PageData;
                HyperLink lnkQuickLinksAndCustomerService = e.Item.FindControl("lnkQuickLinksAndCustomerService") as HyperLink;
                if ((pageData != null) && (lnkQuickLinksAndCustomerService != null))
                {

                    if (pageData[LINK_TO_ANOTHER_PAGE] != null)
                    {
                        pageURL = GlobalUtil.GetUrlToPage(pageData[LINK_TO_ANOTHER_PAGE] as PageReference);
                    }
                    else if (pageData.LinkURL != null)
                    {
                        pageURL = pageData.LinkURL;
                    }
                    lnkQuickLinksAndCustomerService.Text = pageData.PageName;
                    lnkQuickLinksAndCustomerService.NavigateUrl = pageURL;
                }

            }
        }
        #endregion 

        #region Public Members
        /// <summary>
        /// Property to Set the footer Links Container
        /// </summary>
        public PageReference SetFooterLinks
        {
            set
            {
                PageData displayPage = DataFactory.Instance.GetPage(value);

                if (displayPage != null)
                {
                    //Merchandising:R3:Initial Design Document - Removal of Images from Footer
                    //if (displayPage[SITE_FOOTER_CONTAINER_STOOL_IMAGE] != null)
                    //{
                    //    string imageString = displayPage[SITE_FOOTER_CONTAINER_STOOL_IMAGE] as string;

                    //    ImageStoreNET.Classes.Util.UrlBuilder urlBuilder =
                    //        ImageStoreNET.Classes.Util.UrlBuilder.ParseUrl(imageString);
                    //    if (urlBuilder != null)
                    //    {
                    //        urlBuilder.PreserveAspectRatio = true;
                    //        urlBuilder.ConversionFormatType = ImageStoreNET.Classes.Data.ConversionFormatTypes.WebSafe;
                    //        HeaderImage.ImageUrl = urlBuilder.ToString();
                    //        HeaderImage.Visible = true;
                    //    }
                    //}
                    //Merchandising:R3:Initial Design Document - Removal of Images from Footer
                    if (displayPage[SITE_FOOTER_HEADING] != null)
                    {
                        string headerText = displayPage[SITE_FOOTER_HEADING] as string;
                        HeaderText.Text = headerText;
                    }
                    else
                    {
                        //HeaderImage.Visible = false;
                    }

                    if (displayPage[SITE_FOOTER_CONTAINER_TEXT_AREA] != null)
                    {
                        ContainerDescription.Visible = true;
                        Description.Visible = true;
                        Description.Text = displayPage[SITE_FOOTER_CONTAINER_TEXT_AREA] as string;
                    }
                    else
                    {
                        ContainerDescription.Visible = false;
                        Description.Visible = false;
                    }

                    if (displayPage.PageLink != null)
                    {
                        rptQuickLinksAndCustomerService.ItemDataBound += new RepeaterItemEventHandler(RptQuickLinksAndCustomerService_ItemDataBound);
                        FooterLinks.Visible = true;
                        PageDataCollection quickLinksAndCustomerServicePages =
                          DataFactory.Instance.GetChildren(displayPage.PageLink);
                        GuestProgramPageFilter guestProgramPageFilter = new GuestProgramPageFilter();
                        PageCollection invisiblePageFilter = new PageCollection();
                        quickLinksAndCustomerServicePages = guestProgramPageFilter.Filter(quickLinksAndCustomerServicePages);
                        quickLinksAndCustomerServicePages = invisiblePageFilter.FilterInvisible(quickLinksAndCustomerServicePages);
                        rptQuickLinksAndCustomerService.DataSource = quickLinksAndCustomerServicePages;
                        rptQuickLinksAndCustomerService.DataBind();
                    }
                    else
                    {
                        FooterLinks.Visible = false;
                    }
                }
            }
        }
        #endregion
    }
}

