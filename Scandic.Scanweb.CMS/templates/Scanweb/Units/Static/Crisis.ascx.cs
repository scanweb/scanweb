////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: Code Behind class for Major Crisis User Control.        //
//                                It will show Major Crisis message to the user for       //
//                                the first time the application is accessed              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						: Priya Singh                                             //
//  Author email id				:                           							  //
//  Creation Date				: 23rd July 2008    									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region Namespaces

using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;

#endregion Namespaces

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static
{
    /// <summary>
    /// Code behind of Crisis control.
    /// </summary>
    public partial class Crisis : EPiServer.UserControlBase
    {
        private static string CurrentLanguageString
        {
            get { return EPiServer.Globalization.ContentLanguage.PreferredCulture.Name; }
        }

        /// <summary>
        /// Method to Check if the major crisis need to be shown to the user or not.
        /// </summary>
        /// <returns>Bool value, "true" if Crisis Major controle need to be shown
        /// or else "false"</returns>
        public string ShowCrisis()
        {
            string currentLanguageString = CurrentLanguageString;
            string sessionString = "CrisisShown" + currentLanguageString;
            if (MiscellaneousSessionWrapper.GetIsCrisisShown(sessionString).Equals(false))
            {
                if (CurrentPage["DisplayCrisis"] != null)
                {
                    if (CurrentPage["DisplayCrisis"].ToString().ToLower().Equals("true"))
                    {
                        MiscellaneousSessionWrapper.SetIsCrisisShown(sessionString,true);
                        return "true";
                    }
                }
            }
            return "false";
        }

        /// <summary>
        /// Method to get the Crisis Major Heading from EpiServer
        /// </summary>
        /// <returns>Crisis Major Heading</returns>
        public string GetCrisisHeader()
        {
            string returnval = string.Empty;
            PageReference crisisPageReference = CurrentPage["SelectCrisis"] as PageReference;
            if (crisisPageReference != null)
            {
                PageData crisisPage = DataFactory.Instance.GetPage(crisisPageReference,
                                                                   EPiServer.Security.AccessLevel.NoAccess);
                if (crisisPage != null && crisisPage["CrisisHeader"] != null)
                {
                    returnval = crisisPage["CrisisHeader"].ToString();
                }
            }
            return returnval;
        }

        /// <summary>
        /// Method to get the Crisis Major Text from EpiServer
        /// </summary>
        /// <returns>Crisis Major Text</returns>
        public string GetCrisisText()
        {
            string returnval = string.Empty;
            PageReference crisisPageReference = CurrentPage["SelectCrisis"] as PageReference;
            if (crisisPageReference != null)
            {
                PageData crisisPage = DataFactory.Instance.GetPage(crisisPageReference,
                                                                   EPiServer.Security.AccessLevel.NoAccess);
                if (crisisPage != null && crisisPage["CrisisText"] != null)
                {
                    returnval = crisisPage["CrisisText"].ToString();
                }
            }
            return returnval;
        }

        /// <summary>
        /// Method to get the Page Reference configured in EpiServer
        /// for this crisis
        /// </summary>
        /// <returns>Page reference for crisis to displayed in pop up</returns>
        public PageReference GetMoreInfoPage()
        {
            PageReference pageReference = null;
            PageReference crisisPageReference = CurrentPage["SelectCrisis"] as PageReference;
            if (crisisPageReference != null)
            {
                PageData crisisPage = DataFactory.Instance.GetPage(crisisPageReference,
                                                                   EPiServer.Security.AccessLevel.NoAccess);
                if (crisisPage != null && crisisPage["MoreInfo"] != null)
                {
                    pageReference = crisisPage["MoreInfo"] as PageReference;
                }
            }
            return pageReference;
        }
    }
}