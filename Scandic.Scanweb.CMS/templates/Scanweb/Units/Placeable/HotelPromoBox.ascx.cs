////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: New promotion box for select hotel page.                //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Ranajit Nayak                                  	      //
/// Author email id				:                           							  //
/// Creation Date				:26-04-2010									              //
///	Version	#					: 2.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				: -NA-													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region Namespace

using System;
using System.Configuration;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.BookingEngine.Web;

#endregion

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable
{
    /// <summary>
    /// This class encapsulate the Promo Box functionality.
    /// </summary>
    public partial class HotelPromoBox : ScandicUserControlBase
    {
        /// <summary>
        /// Page Load Method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            PageData offerPageData = GetSelectedPage();
            if (offerPageData != null)
            {
                bool read = offerPageData.QueryDistinctAccess(EPiServer.Security.AccessLevel.Read);
                bool published = offerPageData.CheckPublishedStatus(PagePublishedStatus.Published);

                if (read == false || published == false)
                {
                    this.Visible = false;
                }
                else
                {
                    PopulatePromoBoxOffer(offerPageData);
                }
            }
            else
            {
                this.Visible = false;
            }
        }

        #region Properties

        /// <summary>
        /// Promo image default property name
        /// </summary>
        private string imagePropertyName = "BoxImageMedium";

        /// <summary>
        /// Promo image property name
        /// </summary>
        public string ImagePropertyName
        {
            get { return imagePropertyName; }
            set { imagePropertyName = value; }
        }

        /// <summary>
        /// Promo header default property name
        /// </summary>
        private string promoHeaderPropertyName = "BoxHeading";

        /// <summary>
        /// Promo header property name
        /// </summary>
        public string PromoHeaderPropertyName
        {
            get { return promoHeaderPropertyName; }
            set { promoHeaderPropertyName = value; }
        }

        /// <summary>
        /// Promo text default property name
        /// </summary>
        private string promoTextPropertyName = "BoxContent";

        /// <summary>
        /// Promo text property name
        /// </summary>
        public string PromoTextPropertyName
        {
            get { return promoTextPropertyName; }
            set { promoTextPropertyName = value; }
        }

        /// <summary>
        /// Promo link text default property name
        /// </summary>
        private string linkTextPropertyName = "BoxLinkText";

        /// <summary>
        /// Promo link text property name
        /// </summary>
        public string LinkTextPropertyName
        {
            get { return linkTextPropertyName; }
            set { linkTextPropertyName = value; }
        }

        /// <summary>
        /// Offer page link default property name
        /// </summary>
        private string offerPageLinkPropertyName;

        /// <summary>
        /// Offer page link property name
        /// </summary>
        public string OfferPageLinkPropertyName
        {
            get { return offerPageLinkPropertyName; }
            set { offerPageLinkPropertyName = value; }
        }

        private PageData offerPageLink;

        /// <summary>
        /// Offer page
        /// </summary>
        public PageData OfferPageLink
        {
            get { return offerPageLink; }
            set { offerPageLink = value; }
        }

        /// <summary>
        /// Image Maxwidth
        /// </summary>
        private int imageMaxWidth;

        /// <summary>
        /// Image maximum width
        /// </summary>
        public int ImageMaxWidth
        {
            get { return imageMaxWidth; }
            set { imageMaxWidth = value; }
        }

        #endregion Properties

        #region Private Methods

        /// <summary>
        /// Populate the promo box 
        /// </summary>
        /// <param name="offerPageData">Offer page</param>
        private void PopulatePromoBoxOffer(PageData offerPageData)
        {
            if (offerPageData[imagePropertyName] != null)
            {
                string imageString = offerPageData[imagePropertyName] as string;
                BoxImage.ImageUrl = WebUtil.GetImageVaultImageUrl(imageString, imageMaxWidth);                
                ImagePlaceHolder.Visible = true;
            }
            string promoHeaderText = offerPageData[PromoHeaderPropertyName] as string;
            if (promoHeaderText != null)
            {
                PromoHeader.Text = promoHeaderText;
            }
            else
            {
                promoHeading.Visible = false;
            }
            string promoBoxContent = offerPageData[promoTextPropertyName] as string;
            if (!string.IsNullOrEmpty(promoBoxContent))
                PromoText.Text = promoBoxContent;
            if (offerPageData["HideLinkInLists"] == null)
            {
                PageLink.Text =
                    string.Format("<a href=\"{0}\" class=\"fltLft\"><span class=\"IconLink\">{1}</span></a>",
                                  GetLinkUrl(), GetLinkText(offerPageData));
                PageLinkPlaceHolder.Visible = true;
            }
        }

        /// <summary>
        /// Get Offer page
        /// </summary>
        /// <returns>Offer page</returns>
        private PageData GetSelectedPage()
        {
            if (offerPageLink == null)
            {
                PageReference offerPageReference = CurrentPage[offerPageLinkPropertyName] as PageReference;
                if (offerPageReference != null)
                {
                    offerPageLink = DataFactory.Instance.GetPage(offerPageReference);
                    if (offerPageLink["PageShortcutLink"] as PageReference != null)
                    {
                        PageReference shortCutReference = offerPageLink["PageShortcutLink"] as PageReference;
                        offerPageLink = DataFactory.Instance.GetPage(shortCutReference);
                    }
                }
            }
            return offerPageLink;
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Get the link text
        /// </summary>
        /// <param name="pageData">Offer page</param>
        /// <returns>link text</returns>
        public string GetLinkText(PageData pageData)
        {
            return (pageData[linkTextPropertyName] as string ??
                    LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Placeable/Box/LinkText"));
        }

        /// <summary>
        /// Get link url
        /// </summary>
        /// <returns>Link url</returns>
        public string GetLinkUrl()
        {
            if (offerPageLink.LinkType != PageShortcutType.External)
            {
                string boxLinkURL = GetFriendlyURLToPage(offerPageLink.PageLink, offerPageLink.LinkURL);
                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageReference meetingFormPageLink = RootPage["RequestForPorposalPage"] as PageReference;
                if (CurrentPage.PageTypeID == hotelPageTypeID &&
                    meetingFormPageLink != null && meetingFormPageLink.ID == offerPageLink.PageLink.ID)
                {
                    boxLinkURL =
                        UriSupport.AddQueryString(boxLinkURL, "SelectHotelID", CurrentPage.PageLink.ID.ToString());
                }

                return boxLinkURL;
            }
            else
            {
                return offerPageLink.LinkURL;
            }
        }

        #endregion Methods
    }
}