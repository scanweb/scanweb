//  Description					: HotelListing                                            //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region using

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

#endregion using

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// Code behind of hotel listing control.
    /// </summary>
    public partial class HotelListing : EPiServer.UserControlBase
    {
        #region Protected Methods

        /// <summary>
        /// Pageload method 
        /// 1.Sets the values for the dropdown for sorting
        /// 2.Clears the text to display the searched hotels
        /// 2.Clears Session variable for Sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                lblNoOfHotels.InnerText = string.Empty;
                lblMatchYourSearch.InnerText = string.Empty;
                FindAHotelSessionVariablesSessionWrapper.IsHotelSorted = false;
            }
        }

        /// <summary>
        /// Loading of the control is done here
        /// 1.Resets the pagination and Session sorting variable
        /// 2.Displays the Hotels on the control
        /// 3.Sets the Search result text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!FindAHotelSessionVariablesSessionWrapper.IsShowAll)
            {
                txtShowAll.Value = AppConstants.FALSE;
                FindAHotelSessionVariablesSessionWrapper.IsShowAll = true;
            }
            if (FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView != null)
            {
                if (FindAHotelSessionVariablesSessionWrapper.IsHotelSorted)
                {
                    txtPageNo.Value = AppConstants.ONE;
                    FindAHotelSessionVariablesSessionWrapper.IsHotelSorted = false;
                }

                DisplayHotelDestinations();

                if (FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults() != null)
                {
                    lblNoOfHotels.InnerText = string.Empty;
                    lblNoOfHotels.InnerText = FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults().Count.ToString();
                    lblMatchYourSearch.InnerText = string.Empty;

                    StringBuilder searchResultText = new StringBuilder();
                    searchResultText.Append(AppConstants.SPACE);
                    if (FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults().Count == 1)
                    {
                        searchResultText.Append(
                            WebUtil.GetTranslatedText(
                                "/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/HotelMatchText"));
                    }
                    else
                    {
                        searchResultText.Append(
                            WebUtil.GetTranslatedText(
                                "/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/HotelsMatchText"));
                    }

                    if (0 != string.Compare(CurrentPage.LanguageBranch, LanguageConstant.LANGUAGE_FINNISH, true))
                    {
                        searchResultText.Append(AppConstants.SPACE);
                    }

                    searchResultText.Append(WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/For"));
                    searchResultText.Append(AppConstants.SPACE);
                    searchResultText.Append("\"");
                    searchResultText.Append(GetDestinationName());
                    searchResultText.Append("\"");
                    lblMatchYourSearch.InnerText += searchResultText.ToString();
                }
                else
                {
                    lblNoOfHotels.InnerText = string.Empty;
                    lblMatchYourSearch.InnerText = string.Empty;
                }
            }
        }

        /// <summary>
        /// This method is required for pagination, to get the number of pages to be displayed
        /// 1.The total number of pages as per the pagination set in the Appsettings.config
        /// 2.total hotels are returned by the search.
        /// </summary>
        /// <returns>The total number of pages for the search</returns>
        protected int TotalHotelDestinationPages()
        {
            int pageSize = CurrentPage["HotelsPerPage"] != null
                               ? (int)CurrentPage["HotelsPerPage"]
                               : AppConstants.HOTELS_PER_PAGE;
            int totalHotels;
            int totalPages = 1;
            if (FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView != null)
            {
                List<HotelDestination> hotelsDestination = FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults();
                if (hotelsDestination != null)
                {
                    totalHotels = hotelsDestination.Count;
                    if (totalHotels > pageSize)
                        totalPages = (int)Math.Ceiling((double)totalHotels / pageSize);
                    else
                        totalPages = 1;
                }
            }
            return totalPages;
        }

        /// <summary>
        /// This method will return the pagination strings based on the number of hotels to be
        /// displayed per page and total number of hotels returned from the search.
        /// 
        /// Will create the list of strings for example 1-4, 5-8, 9-12 if the pagination size is 4
        /// and will only return the strings which are in the total size of the hotels
        /// </summary>
        /// <returns>The pagination strings for example 1-3, 4-6, 7-9 ...</returns>
        protected List<string> GetPaginationDetails()
        {
            int pageSize = CurrentPage["HotelsPerPage"] != null
                               ? (int)CurrentPage["HotelsPerPage"]
                               : AppConstants.HOTELS_PER_PAGE;
            int totalPages;

            List<string> pageDetails = null;

            if (FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView != null)
            {
                List<HotelDestination> hotelsDestination = FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults();

                int totalHotels = hotelsDestination.Count;

                if (hotelsDestination != null)
                {
                    totalPages = TotalHotelDestinationPages();

                    pageDetails = new List<string>();

                    int startVal = 1;
                    if (totalPages == 1)
                    {
                        int endVal = startVal + totalHotels - 1;
                        pageDetails.Add(startVal + AppConstants.HYPHEN + endVal);
                    }
                    else
                    {
                        for (int pageCount = 0; pageCount < totalPages; pageCount++)
                        {
                            int endVal = startVal + pageSize - 1;

                            if (endVal > totalHotels)
                                endVal = totalHotels;

                            if (startVal == endVal)
                                pageDetails.Add(startVal + string.Empty);
                            else
                                pageDetails.Add(startVal + AppConstants.HYPHEN + endVal);
                            startVal = endVal + 1;
                        }
                    }
                }
            }
            return pageDetails;
        }

        /// <summary>
        /// This method will be called when user choses the Show all from the select hotel page
        /// The page no is set to 1 so when user hides the show all hotels the first page needs
        /// to be displayed to the user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowAllHideAll(object sender, EventArgs e)
        {
            txtPageNo.Value = AppConstants.ONE;
        }


        /// <summary>
        /// This method will return the list of hotels to be displayed to the user
        /// 1.If user has selected the ShowAll option from Select Hotel page then all hotels are returned. 
        /// 2.If the user has selected different page from the select Hotel, then corresponding
        /// list of hotels to be displayed for the page selected will be displayed
        /// 3.If the user has done a sorting the hotels are already sorted before this method is called
        /// so the corresponding hotels for the page 1 is returned if user does a sorting
        /// </summary>
        /// <returns>The list of hotels to be displayed to the user</returns>
        protected List<HotelDestination> HotelDestinationsToBeDisplayed()
        {
            List<HotelDestination> hotelDestination = null;

            if (FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView != null)
            {
                hotelDestination = FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults();

                if (hotelDestination != null)
                {
                    if (!string.IsNullOrEmpty(txtShowAll.Value) &&
                        txtShowAll.Value.Equals(AppConstants.FALSE) &&
                        hotelDestination != null)
                    {
                        int hotelsPerPage = CurrentPage["HotelsPerPage"] != null
                                                ? (int)CurrentPage["HotelsPerPage"]
                                                : AppConstants.HOTELS_PER_PAGE;
                        int pageNo = int.Parse(txtPageNo.Value);

                        int startIndex = ((pageNo - 1) * hotelsPerPage);
                        int resultCount = hotelsPerPage;
                        int hotelCount = hotelDestination.Count;

                        if ((startIndex + hotelsPerPage - 1) >= hotelCount)
                        {
                            resultCount = hotelCount - startIndex;
                        }

                        hotelDestination = hotelDestination.GetRange(startIndex, resultCount);
                    }
                }
            }
            return hotelDestination;
        }

        #endregion Protected Methods

        #region Private Methods

        /// <summary>
        /// This Method is used to display the child control on parent control
        /// 1.HotelListingDetailBody is the child control which is displayed on the Parent HotelListing control
        /// 2.Loads the control dynamically 
        /// 3.Populates data into the HotelListingDetailBody from the hotelDestination list
        /// </summary>
        private void DisplayHotelDestinations()
        {
            if (string.IsNullOrEmpty(txtPageNo.Value))
                txtPageNo.Value = AppConstants.ONE;

            allHotels.Controls.Clear();
            List<HotelDestination> hotels = HotelDestinationsToBeDisplayed();

            if (hotels != null)
            {
                for (int hotelCount = 0; hotelCount < hotels.Count; hotelCount++)
                {
                    HotelDestination hotel = hotels[hotelCount];

                    int pageNo = int.Parse(txtPageNo.Value);
                    int hotelsPerPage = CurrentPage["HotelsPerPage"] != null
                                            ? (int)CurrentPage["HotelsPerPage"]
                                            : AppConstants.HOTELS_PER_PAGE;
                    int startIndex = ((pageNo - 1) * hotelsPerPage);

                    IHotelListingDisplayInformation hotelDisplay =
                        SelectHotelUtil.GetHotelDestinationDisplayObject(SearchType.REGULAR, hotel,
                                                                         startIndex + hotelCount);

                    if (hotelDisplay != null)
                    {
                        string virtualPathOfControl =
                            ResolveUrl("~/Templates/Scanweb/Units/Placeable/HotelListingDetailBody.ascx");
                        HotelListingDetailBody hotelBody =
                            (HotelListingDetailBody)LoadControl(virtualPathOfControl);

                        hotelDisplay.HideCityTARatings = CurrentPage["HideCityTARatings"] != null
                                            ? Convert.ToBoolean(CurrentPage["HideCityTARatings"]) : false;
                        hotelBody.Hotel = hotelDisplay;
                        allHotels.Controls.Add(hotelBody);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the Destination name from the selected node in tree view.
        /// </summary>
        /// <returns>Destination string</returns>
        /// <remarks>
        /// </remarks>
        private string GetDestinationName()
        {
            Node selectedNode = FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView;
            string destinationName = string.Empty;
            if (null != selectedNode)
            {
                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageData selectedPage = selectedNode.GetPageData();
                if (selectedPage.PageTypeID == hotelPageTypeID)
                {
                    destinationName = selectedPage.Property["Heading"].ToString();
                }
                else
                {
                    destinationName = selectedPage.PageName;
                }
            }
            return destinationName;
        }

        #endregion Private Methods
    }
}