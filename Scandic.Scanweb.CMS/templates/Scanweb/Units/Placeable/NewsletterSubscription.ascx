<%@ Control Language="C#" AutoEventWireup="true" Codebehind="NewsletterSubscription.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.NewsletterSubscription" %>
<asp:Panel runat="server" ID="pnlNewsletter">
<div id="Newsletter">
    <div id="Subscription" class="BE">
            <div class="box-top-grey">
                <span class="title"><%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/header") %></span>
            </div>                            
            <div class="box-middle">
                <div class="content">
                    <div id="ColumnOne" class="columnOne"  runat="server">
                        <p class="formRow">
                            <%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/firstname") %>
                            <br />
                            <asp:TextBox runat="server" ID="firstname" CssClass="frmInputText" />                            
                        </p>
                        <p class="formRow">
                            <%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/lastname") %>
                            <br />
                            <asp:TextBox runat="server" ID="lastname" CssClass="frmInputText" />                            
                        </p>
                        <p class="formRow" style="display:none;">
                            <%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/title") %>
                            <br /> 
                            <asp:TextBox runat="server" ID="title" CssClass="frmInputText" />
                        </p>
                        <p class="formRow">
                            <%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/street") %>
                            <br />
                            <asp:TextBox runat="server" ID="street" CssClass="frmInputText" />                            
                        </p>
                        <p class="formRow">
                            <%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/city") %>
                            <br />
                            <asp:TextBox runat="server" ID="city" CssClass="frmInputText" />                                                        
                        </p>
                        <p class="formRow">
                            <%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/zipcode") %>
                            <br />
                            <asp:TextBox runat="server" ID="zipcode" CssClass="frmInputText" />                                                        
                       <asp:RequiredFieldValidator runat="server" ID="zipvalidator"
                            ControlToValidate = "zipcode"                           
                            CssClass="red"
                            ValidationGroup="subscribeinfo"
                            EnableClientScript="true" />
                        </p>
                    </div>
                    <div id="ColumnTwo" class="columnTwo" runat="server">
                        <p class="formRow">
                            <%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/country") %>
                            <br />
                            <select runat="server" id="country" name="country" style="width:203px;">
                            </select>
                            
                        </p>
                         <asp:CompareValidator runat="server" ID="countryvalidator"
                            ControlToValidate = "country"
                            CssClass="red"                            
                            Operator="NotEqual"
                            ValueToCompare="none"
                            ValidationGroup="subscribeinfo"
                            EnableClientScript="true" />
                        <p class="formRow">
                            <%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/birthyear") %>&nbsp;<i><%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/birtheyarex") %></i>
                            <br />
                            <asp:TextBox runat="server" ID="birthyear" MaxLength="4" CssClass="frmInputText" />                             
                        </p>
                        <p class="formRow">
                            <%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/gender") %>
                            <asp:RadioButton runat="server" ID="male" GroupName="gender" /><%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/male") %>&nbsp;
                            <asp:RadioButton runat="server" ID="female" GroupName="gender" /><%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/female") %>
                        </p>
                        <p class="formRow">
                            <%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/mobile") %>
                            <br />
                            <asp:TextBox runat="server" ID="mobilenumber" CssClass="frmInputText" /> 
                        </p>
                        <p class="formRow">
                            <%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/companyorganization") %>
                            <br />
                            <asp:TextBox runat="server" ID="companyorganization" CssClass="frmInputText" />                            
                            <br class="clear" />
                        </p>
                        <p class="formRow">
                            <%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/companyaddress") %>
                            <br />
                            <asp:TextBox runat="server" ID="companyaddress" CssClass="frmInputText" />
                            <br class="clear" />
                        </p>
                        <p class="formRow">
                            <%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/interestedin") %>
                            <br />
                            <div id="leisureDiv" runat="server">
                                <asp:CheckBox runat="server" ID="leisure" />
                                <label for="leisure"><%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/leisure") %></label></div>
                            <div id="meetingandconferenceDiv" runat="server">
                                <asp:CheckBox runat="server" ID="meetingandconference" />
                                <label for="meetingandconference"><%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/meetingandconference") %></label></div>
                            <div id="pressreleasesDiv" runat="server">
                                <asp:CheckBox runat="server" ID="pressreleases" />
                                <label for="pressreleases"><%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/pressreleases") %></label></div>
                            <div id="corporateinformationDiv" runat="server">
                                <asp:CheckBox runat="server" ID="corporateinformation" />
                                <label for="corporateinformation"><%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/corporateinformation") %></label></div>
                            <br class="clear" />
                        </p>
                    </div>
                    <div class="clear">&nbsp;</div>
                    <!-- /details -->
                    <!-- Footer -->
                    <div>
                        <div id ="divEmail" runat ="server" >
                        <div style="display:block;"><%= Translate("/Templates/Scanweb/Pages/Newsletter/Subscribe/email") %></div>
                        <div style="float:left; margin-top:3px;"><asp:TextBox runat="server" ID="email" CssClass="frmInputText" /></div>                                                                           
                        <div style="float:left; margin-left: 10px;" class="actionBtn"><asp:Button runat="server" ValidationGroup="subscribeinfo" ID="btnSend" OnClick="btnSend_Click" CausesValidation="true" translate="/Templates/Scanweb/Pages/Newsletter/Subscribe/submit" CssClass="buttonInner scansprite" /></div>
                        <div class="clear">&nbsp;</div>
                        <asp:RequiredFieldValidator runat="server" ID="emailrequired" 
                            ControlToValidate = "email"                            
                            CssClass="red" 
                            ValidationGroup="subscribeinfo"
                            EnableClientScript="true" />
                        <asp:RegularExpressionValidator id="emailvalidator" runat="server"
                            ControlToValidate = "email"
                            ValidationExpression=".*@.*\..*"                         
                            CssClass="red"
                            ValidationGroup="subscribeinfo"
                            EnableClientScript="true" />                        
                        
                       
                            <br />
                            </div> 
                        <div class="NewsLetterStatus"><asp:Literal runat="server" ID="ltStatus" /></div><br />
                        <div class="clear">&nbsp;</div>
                    </div>                
                    <!-- Footer -->
                </div>
            </div>            
            <div class="box-bottom">&nbsp;</div>        
           
    </div>
</div>
</asp:Panel>