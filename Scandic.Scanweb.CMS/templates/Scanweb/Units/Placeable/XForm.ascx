<%@ Control Language="C#" AutoEventWireup="False" CodeBehind="XForm.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.XFormControl" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>

<%@ Register TagPrefix="Scanweb" TagName="MainBody" 	        Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<asp:PlaceHolder ID="MeetingsListHotelsPlaceHolder" runat="server" Visible="false">
<div id="MeetingsFormListHotels">
    <h2><episerver:translate ID="Translate1" Text="/Templates/Scanweb/Units/Placeable/Xform/ListHotelsHeading" runat="server" /></h2>
    <div id="MeetingsFormLeftColumn">
        <asp:Repeater ID="SelectedHotelsLeftRepeater" runat="server">
        <ItemTemplate>
            <%# ((System.Collections.Generic.KeyValuePair<int, string>) Container.DataItem).Value %><br />
        </ItemTemplate>
    </asp:Repeater>
    </div>
    <div id="MeetingsFormRightColumn">
        <asp:Repeater ID="SelectedHotelsRightRepeater" runat="server">
        <ItemTemplate>
            <%# ((System.Collections.Generic.KeyValuePair<int, string>) Container.DataItem).Value %><br />
        </ItemTemplate>
    </asp:Repeater>
    </div>    
</div>
</asp:PlaceHolder>
<div  class="XformMessage">
<asp:ValidationSummary runat="server" Visible="false" />
<asp:Label runat="server" id="PostedMessage" CssClass="thankyoumessage" />
</div>
<div id="XForm">
    <asp:Panel runat="server" ID="FormPanel" CssClass="xForm">       
	    <xforms:xformcontrol ID="FormControl" runat="server" EnableClientScript="true" ValidationGroup="xforms" />
    </asp:Panel>
</div>
<asp:Panel runat="server" ID="StatisticsPanel" Visible="false">
	<p>
		<asp:literal id="NumberOfVotes" runat="server" />
		<!-- Set StatisticsType to format output: N=numbers only, P=percentage -->
		<EPiServer:XFormStatistics StatisticsType="P" runat="server" id="Statistics" PropertyName="MainXForm"  />
	</p>
</asp:Panel>
<br />
<asp:LinkButton ID="Switch" runat="server" OnClick="SwitchView" CausesValidation="False">
	<episerver:translate Text="/templates/form/showstat" runat="server" />
</asp:LinkButton>
<!--Ramana: Added as part of site expansion -->
<div class="errorDivClient">
    <input type="hidden" id="RestrictSplChars" name="restrictSplChars" value="<%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/bookingDetails/noSpecialCaracters") %>" />
</div>
