//  Description					: OverviewBox                                             //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System Namespace
using System;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util.ImageVault;
using System.Configuration;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.Core;
#endregion

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable
{
    /// <summary>
    /// This class encaptulates the Overview Box functionality
    /// </summary>
    public partial class OverviewBox : EPiServer.UserControlBase
    {
        private string imagePropertyName;
        private int mainImageWidth;
        private int stoolImageWidth;
        private bool displayMoreImagesLink;
        private bool showDefaultStoolImage;
        private const string PromotionalPageURL = "PromotionalPageLink";
        public string images = WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/MoreImagesPopUp/Images");
        public string images360 = WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/MoreImagesPopUp/Image360");

        /// <summary>
        /// Content page
        /// </summary>
        public PageData ContentPage { get; set; }

        private string linkURL;

        /// <summary>
        /// Gets/Sets LinkURL
        /// </summary>
        public string LinkURL
        {
            get { return linkURL; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    string url = string.Empty;
                    PageReference shortCutReference = CurrentPage[value] as PageReference;
                    if (shortCutReference != null)
                    {
                        PageData boxPage = DataFactory.Instance.GetPage(shortCutReference,
                                                                        EPiServer.Security.AccessLevel.NoAccess);
                        url = boxPage.LinkURL;
                    }
                    if (!string.IsNullOrEmpty(url))
                    {
                        string urlText = string.Empty;
                        if (CurrentPage["PromotionalPageLinkText"] != null)
                            urlText = CurrentPage["PromotionalPageLinkText"].ToString();
                        else
                            urlText = "Link to promotional page";

                        PageLink1.Text
                            = string.Format("<a href=\"{0}\" class=\"IconLink\"{2}>{1}</a>", url, urlText, string.Empty);
                    }
                    else
                        PageLink1.Text = "&nbsp;";
                }
            }
        }


        public int CurrentPageLinkId
        {
            get { return CurrentPage.PageLink.ID; }

        }


        public string CurrentPageTypeId
        {
            get
            {
                return CurrentPage.PageTypeID.ToString();

            }

        }


        /// <summary>
        /// Get/Set Main Image Width
        /// </summary>
        public int MainImageWidth
        {
            get {
                    if (mainImageWidth == 0)
                    {
                        mainImageWidth = Convert.ToInt32(AppConstants.HotelTopImageWidth);
                    }
                    return mainImageWidth; 
                }
            set { mainImageWidth = value; }
        }

        /// <summary>
        /// Stool Image Width
        /// </summary>
        public int StoolImageWidth
        {
            get { return stoolImageWidth; }
            set { stoolImageWidth = value; }
        }

        /// <summary>
        /// Get/Set Image Property Name
        /// </summary>
        public string ImagePropertyName
        {
            get { return imagePropertyName; }
            set { imagePropertyName = value; }
        }

        /// <summary>
        /// Get/Set Stool Image Property Name
        /// </summary>
        public string StoolImagePropertyName { get; set; }

        /// <summary>
        /// Get/Set Text Property Name
        /// </summary>
        public string TextPropertyName { get; set; }

        /// <summary>
        /// Get/Set whether to display the MoreImages Link
        /// </summary>
        public bool DisplayMoreImagesLink
        {
            get { return displayMoreImagesLink; }
            set { displayMoreImagesLink = value; }
        }

        /// <summary>
        /// Get/Set whether to display the MoreImages Link
        /// </summary>
        public bool ShowDefaultStoolImage
        {
            get { return showDefaultStoolImage; }
            set { showDefaultStoolImage = value; }
        }

        /// <summary>
        /// Page Load Method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.Visible = (CurrentPage[imagePropertyName] != null);

                if (!MiscellaneousSessionWrapper.OverviewCarousel)
                    this.Visible = false;

                int HotelId = CurrentPage["OperaID"] != null ? Convert.ToInt32(CurrentPage["OperaID"].ToString()) : 0;
                string HotelName = CurrentPage["PageName"] != null ? CurrentPage["PageName"].ToString() : null;
                moreImgLink.Attributes.Add("rel",
                                           GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") +
                                           "?methodToCall=GetImageGallery&HotelID=" + HotelId + "&HotelName=" +
                                           HotelName);

                moreImgLink.Attributes.Add("href", string.Format("{0}#",Request.RawUrl));
                string text = CurrentPage[TextPropertyName] as string;
                if (!string.IsNullOrEmpty(text))
                {
                    TextProperty.Text = CurrentPage[TextPropertyName] as string ?? string.Empty;
                }
                if (displayMoreImagesLink)
                {
                    PagelinkPropertyPlaceHolder.Visible = true;
                }
                if( CurrentPage["ActivateImageCarousel"] != null && Convert.ToBoolean(CurrentPage["ActivateImageCarousel"].ToString()))
                {
                    if (CurrentPage["Width"] != null && Convert.ToInt32(CurrentPage["Width"]) == 0)
                    {
                        if(CurrentPage.PageTypeID != Convert.ToInt32(ConfigurationManager.AppSettings["OverviewPageTypeID"]))
                            HotelOverviewPlaceHolder.Visible = false;
                    }
                    else
                    {
                        MainImagePlaceHolder.Visible = false;
                    }

                    if (CurrentPage.PageTypeID == Convert.ToInt32(ConfigurationManager.AppSettings["OverviewPageTypeID"]))
                        this.Visible = false;
                }
                else
                {
                    GetMainImageURL();
                }
               
                GetStoolImageURL();
                if (CurrentPage["PromotionalPageLinkText"] != null)
                    LinkURL = PromotionalPageURL;
                else
                    PageLink1.Text = "&nbsp;";
            }
        }

        /// <summary>
        /// Get the Image Url
        /// </summary>
        /// <remarks>
        /// Its the main Hotel Image. 
        /// </remarks>
        /// <returns>
        /// Url of the Image
        /// </returns>
        protected void GetMainImageURL()
        {
            if (CurrentPage[imagePropertyName] != null)
            {
                string imageString = CurrentPage[imagePropertyName] as string;
                if (!string.IsNullOrEmpty(imageString))
                {
                    MainImage.ImageUrl = WebUtil.GetImageVaultImageUrl(imageString, MainImageWidth);
                    MainImage.AlternateText = GetMainImageAltText();
                    MainImage.Attributes.Add("title", MainImage.AlternateText);
                    MainImagePlaceHolder.Visible = true;
                }                    
            }
        }

        /// <summary>
        /// Get the StoolImage URL
        /// </summary>
        /// <returns>
        /// URL of the Stool Image
        /// </returns>
        protected void GetStoolImageURL()
        {
            StoolImage.Visible = false;
            if (CurrentPage[StoolImagePropertyName] != null)
            {
                string stoolImageString = CurrentPage[StoolImagePropertyName] as string;
                if (!string.IsNullOrEmpty(stoolImageString))
                {
                    StoolImage.ImageUrl = WebUtil.GetImageVaultImageUrl(stoolImageString, stoolImageWidth);
                    if (!string.IsNullOrEmpty(StoolImage.ImageUrl))
                    {
                        StoolImage.Visible = true;
                        StoolImage.AlternateText = GetStoolImageAltText();
                        StoolImage.Attributes.Add("title", StoolImage.AlternateText);
                    }                    
                }
            }
            else if (showDefaultStoolImage)
            {
                StoolImage.ImageUrl = GetDefaultStoolImagePath();
                StoolImage.Visible = true;
            }
        }

        /// <summary>
        /// GetDefaultStoolImagePath
        /// </summary>
        /// <returns>DefaultStoolImagePath</returns>
        protected string GetDefaultStoolImagePath()
        {
            string stoolImagePath;
            switch (CurrentPage.LanguageID.ToLower().Trim())
            {
                case "da":
                    stoolImagePath = "~/templates/Scanweb/Styles/Default/Images/look-feel-hotellsida-black-ENG.gif";
                    break;
                case "en":
                    stoolImagePath = "~/templates/Scanweb/Styles/Default/Images/look-feel-hotellsida-black-ENG.gif";
                    break;
                case "fi":
                    stoolImagePath = "~/templates/Scanweb/Styles/Default/Images/look-feel-hotellsida-black-ENG.gif";
                    break;
                case "no":
                    stoolImagePath = "~/templates/Scanweb/Styles/Default/Images/look-feel-hotellsida-black-ENG.gif";
                    break;
                case "sv":
                    stoolImagePath = "~/templates/Scanweb/Styles/Default/Images/look-feel-hotellsida-black-ENG.gif";
                    break;
                default:
                    stoolImagePath = "~/templates/Scanweb/Styles/Default/Images/look-feel-hotellsida-black-ENG.gif";
                    break;
            }
            ;
            return stoolImagePath;
        }

        /// <summary>
        /// Get the Alternate Text of the Image
        /// </summary>
        /// <remarks>
        /// The alternate text is displayed when the image is not available
        /// </remarks>
        /// <returns>
        /// Alternate text
        /// </returns>
        protected string GetMainImageAltText()
        {
            string returnvalue = string.Empty;
            if (CurrentPage != null)
            {
                LangAltText altText = new LangAltText();
                returnvalue = altText.GetAltText(CurrentPage.LanguageID,
                                                 CurrentPage[ImagePropertyName] as string ?? string.Empty);
            }
            return returnvalue;
        }

        /// <summary>
        /// Get the Alternate Text of the Image
        /// </summary>
        /// <remarks>
        /// The alternate text is displayed when the image is not available
        /// </remarks>
        /// <returns>
        /// Alternate text
        /// </returns>
        protected string GetStoolImageAltText()
        {
            string returnvalue = string.Empty;
            if (CurrentPage != null)
            {
                LangAltText altText = new LangAltText();
                returnvalue = altText.GetAltText(CurrentPage.LanguageID,
                                                 CurrentPage[StoolImagePropertyName] as string ?? string.Empty);
            }
            return returnvalue;
        }
    }
}