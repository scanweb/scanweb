//  Description					:   HotelListingDetailBody                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region using

using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;

#endregion using

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// This control is called on Find A Hotel Page
    /// 1.Control holds the details of the Hotel information to be displayed
    /// 2.Will be displayed multiple times on the parent control for HotelListing
    /// </summary>
    public partial class HotelListingDetailBody : EPiServer.UserControlBase
    {
        private string hotel;
        private string hotelOperaId;

        #region Properties

        /// <summary>
        /// Sets the property for the hotel name displayed on the control
        /// </summary>
        public string CounterValue
        {
            set { lblCounterValue.InnerText = value; }
        }

        /// <summary>
        /// Sets the the path for the Hotel Image displayed
        /// </summary>
        public string ImageURL
        {
            set { imgHotel.Src = value; }
        }

        /// <summary>
        /// Stores the distance from city centre
        /// </summary>
        public string CityCenterDistance
        {
            set { lblDistance.InnerText = value; }
        }

        /// <summary>
        /// Stores address
        /// </summary>
        public string Address
        {
            set { address.InnerText = value; }
        }

        /// <summary>
        /// Stores Details
        /// </summary>
        public string Details
        {
            set { details.InnerText = value; }
        }

        /// <summary>
        /// Hotel URL
        /// </summary>
        public string HotelURL
        {
            set { lnkFulldescription.HRef = value; }
        }

        public string HotelNameURL
        {
            set { lnkHotelName.HRef = value; }
        }

        /// <summary>
        /// Stores the HotelListing Display object
        /// </summary>
        public IHotelListingDisplayInformation Hotel
        {
            set
            {
                imgHotel.Src = value.ImageSrc;
                imgHotel.Alt = value.ImageAltText;
                address.InnerText = value.Address;
                details.InnerText = value.Description;
                lnkFulldescription.HRef = value.HotelURL;
                lnkFulldescription.InnerText =
                    WebUtil.GetTranslatedText
                        ("/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/FullDescription");
                lnkHotelName.InnerText = value.HotelName;
                hotel = value.HotelName;
                hotelOperaId = value.ID;
                lnkHotelName.HRef = value.HotelURL;
                lblCounterValue.InnerText =
                    value.Counter.ToString() + AppConstants.DOT.ToString() + AppConstants.SPACE.ToString();

                if (null != value.Distance && value.Distance != string.Empty)
                    lblDistance.InnerHtml =
                        AppConstants.BRACKET_OPEN + value.Distance + AppConstants.SPACE +
                        WebUtil.GetTranslatedText
                            ("/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/KmToCityCentre") +
                        AppConstants.BRACKET_CLOSE;
            }
        }

        /// <summary>
        /// Gets the hotel name.
        /// </summary>
        protected string GetHotelName
        {
            get { return hotel; }
        }

        protected string GetOperaId
        {
            get { return string.IsNullOrEmpty(hotelOperaId) ? string.Empty : "HOTEL:" + hotelOperaId; }
        }

        #endregion Properties
    }
}