//  Description					: SquaredCornerImage codebehind      			          //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						: Srinivas                                                //
/// Author email id				:                           							  //
/// Creation Date				:                                                         //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				:           											  //
///	Last Modified Date			:           											  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Util.ImageVault;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// This class <see cref="SquaredCornerImage"/> is a codebehind file for the SquaredCornerImage control. 
    /// </summary>
    public partial class SquaredCornerImage : EPiServer.UserControlBase
    {
        #region Private Variables

        private int imageWidth;

        #endregion

        #region Property

        /// <summary>
        /// Gets/Sets ContentPage
        /// </summary>
        public PageData ContentPage { get; set; }

        /// <summary>
        /// Gets/Sets ImagePropertyName
        /// </summary>
        public string ImagePropertyName { get; set; }

        /// <summary>
        /// Gets/Sets TopCssClass
        /// </summary>
        public string TopCssClass { get; set; }

        /// <summary>
        /// Gets/Sets ImageCssClass
        /// </summary>
        public string ImageCssClass { get; set; }

        /// <summary>
        /// Gets/Sets BottomCssClass
        /// </summary>
        public string BottomCssClass { get; set; }

        /// <summary>
        /// Gets/Sets ImageWidth
        /// </summary>
        public int ImageWidth
        {
            get { return imageWidth; }
            set { imageWidth = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// This is a constructor of <see cref="SquaredCornerImage"></see> class.
        /// </summary>
        public SquaredCornerImage()
        {
            ContentPage = CurrentPage;
        }

        /// <summary>
        /// This method is the page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ContentPage != null && ContentPage["ActivateImageCarousel"] != null 
                    && String.Equals(ImagePropertyName,"ContentTopImage",StringComparison.InvariantCultureIgnoreCase))
                    this.Visible = false;
                else
                    this.Visible = (ContentPage != null && ContentPage[ImagePropertyName] != null);
            }
        }

        /// <summary>
        /// Gets image url.
        /// </summary>
        /// <returns>image url.</returns>
        protected string GetImageURL()
        {
            if (ContentPage != null)
            {
                string imageString = ContentPage[ImagePropertyName] as string;

                if (imageString != null)
                {
                    return WebUtil.GetImageVaultImageUrl(imageString, imageWidth);                    
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets alt text.
        /// </summary>
        /// <returns>alt text.</returns>
        protected string GetAltText()
        {
            if (ContentPage != null)
            {
                LangAltText altText = new LangAltText();
                return altText.GetAltText(CurrentPage.LanguageID,
                                          ContentPage[ImagePropertyName] as string ?? string.Empty);
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion
    }
}