<%@ Control Language="C#" AutoEventWireup="False" CodeBehind="PagePuff.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.PagePuff" %>
<div id="PagePuff">
    <div class="PuffItem">
        <h2><%# GetHeader(FirstPageReference) %></h2>
        <div><%# GetBody(FirstPageReference) %></div>
        <div class="IconLink"><%# GetContainerLink(FirstPageReference) %></div>
    </div>
    <div class="PuffItem">
        <h2><%# GetHeader(SecondPageReference) %></h2>
        <div><%# GetBody(SecondPageReference) %></div>
        <div class="IconLink"><%# GetContainerLink(SecondPageReference) %></div>
    </div>
    <div class="PuffItem">
        <h2><%# GetHeader(ThirdPageReference) %></h2>
        <div><%# GetBody(ThirdPageReference) %></div>
        <div class="IconLink"><%# GetContainerLink(ThirdPageReference) %></div>
    </div>
    <div class="PuffItem">
        <h2><%# GetHeader(FourthPageReference) %></h2>
        <div><%# GetBody(FourthPageReference) %></div>
        <div class="IconLink"><%# GetContainerLink(FourthPageReference) %></div>
    </div>
</div>