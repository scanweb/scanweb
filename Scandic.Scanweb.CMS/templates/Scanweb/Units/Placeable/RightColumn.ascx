<%@ Control Language="C#" AutoEventWireup="true" Codebehind="RightColumn.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.RightColumn" %>
<%@ Register TagPrefix="Scanweb" TagName="PromoBoxOffer" Src="~/Templates/Scanweb/Units/Placeable/PromoBoxOffer.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="InfoBox" Src="~/Templates/Scanweb/Units/Static/InfoBox.ascx" %>
<%@ Register TagPrefix="Booking" TagName="Module" Src="~/Templates/Booking/Units/BookingModuleSmall.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="MeetingSearch" Src="~/Templates/Scanweb/Units/Placeable/MeetingSearch.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="BookingModuleAlternativeBox" Src="~/Templates/Scanweb/Units/Placeable/BookingModuleAlternativeBox.ascx" %>

<div class="<%= GetCSS() %>">
    <asp:PlaceHolder ID="BookingModulePlaceHolder" runat="server">
        <div class="BoxContainer">
            <Booking:Module ID="BE" runat="server" />
            <Scanweb:BookingModuleAlternativeBox CssClass="AlternativeBookingBoxSmall" ID="AlternativeBookingModule" Visible="false" runat="server" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="MeetingModulePlaceHolder" runat="server">
      <div class="BoxContainer">
            <div id="MeetingSearch226" runat="server">
            <div class="cnt">
                <div class="cntWrapper">                        
            <h3 class="stoolHeading" runat="server" id="searchHeader"></h3>       
            </div>
            </div>
            <Scanweb:MeetingSearch runat="server" ID="MeetingSearchBox" />
        </div>
        </div>
    </asp:PlaceHolder>
     
    <asp:PlaceHolder ID="RightInfoBox1PlaceHolder" runat="server">
        <div class="BoxContainer">
            <Scanweb:InfoBox runat="server" HeaderPropertyName="RightInfoBoxHeading1"
                BodyPropertyName="RightInfoBoxBody1" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="RightInfoBox2PlaceHolder" runat="server">
        <div class="BoxContainer">
            <Scanweb:InfoBox runat="server" HeaderPropertyName="RightInfoBoxHeading2"
                BodyPropertyName="RightInfoBoxBody2" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="BoxContainer1PlaceHolder">
        <div class="BoxContainer">
            <Scanweb:PromoBoxOffer OfferPageLinkPropertyName="BoxContainer1" 
            ImageMaxWidth="226" ImagePropertyName="BoxImageMedium" runat="server" id="rightColumnPromo1"/>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="BoxContainer2PlaceHolder">
        <div class="BoxContainer">
            <Scanweb:PromoBoxOffer OfferPageLinkPropertyName="BoxContainer2" 
            ImageMaxWidth="226" ImagePropertyName="BoxImageMedium" runat="server" id="rightColumnPromo2" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="BoxContainer3PlaceHolder">
        <div class="BoxContainer">
            <Scanweb:PromoBoxOffer OfferPageLinkPropertyName="BoxContainer3" 
            ImageMaxWidth="226" ImagePropertyName="BoxImageMedium" runat="server" id="rightColumnPromo3"/>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="BoxContainer4PlaceHolder">
        <div class="BoxContainer">
            <Scanweb:PromoBoxOffer OfferPageLinkPropertyName="BoxContainer4" 
            ImageMaxWidth="226" ImagePropertyName="BoxImageMedium" runat="server" id="rightColumnPromo4"/>
        </div>
    </asp:PlaceHolder>    
    <asp:PlaceHolder ID="RightDynamicTextBoxPlaceHolder" runat="server">
        <div class="BoxContainer" id="newletterDiv">
            <Scanweb:InfoBox runat="server" ID="RightDynamicTextBox" HeaderPropertyName="RightDynamicTextBoxHeading" BodyPropertyName="RightDynamicTextBoxBody" />
        </div>
    </asp:PlaceHolder>
</div>
