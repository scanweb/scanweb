<%@ Control Language="C#" AutoEventWireup="true" Codebehind="RightColumnWide.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.RightColumnWide" %>
<%@ Register TagPrefix="Scanweb" TagName="PromoBoxOffer" Src="~/Templates/Scanweb/Units/Placeable/PromoBoxOffer.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="InfoBox" Src="~/Templates/Scanweb/Units/Static/InfoBox.ascx" %>
<%@ Register TagPrefix="Booking" TagName="Module" Src="~/Templates/Booking/Units/BookingModuleMedium.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="MeetingSearch" Src="~/Templates/Scanweb/Units/Placeable/MeetingSearch.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="SearchMeetingGoogleMap" Src="~/Templates/Scanweb/Units/Static/MeetingSearchGoogleMap.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="BookingModuleAlternativeBox" Src="~/Templates/Scanweb/Units/Placeable/BookingModuleAlternativeBox.ascx" %>

<div class="<%= GetCSS() %>">
    <asp:PlaceHolder ID="BookingModulePlaceHolder" runat="server">
        <div class="BoxContainer">
            <Booking:Module ID="BE" runat="server" />
            <Scanweb:BookingModuleAlternativeBox CssClass="AlternativeBookingBoxMedium" ID="AlternativeBookingModule" runat="server" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="MeetingModulePlaceHolder" runat="server">
<%--        <asp:PlaceHolder ID="MeetingsSearchGoogleMap" runat="server">
            <Scanweb:SearchMeetingGoogleMap ID="SearchMeetingGoogleMap" runat="server"></Scanweb:SearchMeetingGoogleMap>
        </asp:PlaceHolder>--%>
       <div class="BoxContainer">
             <div id="MeetingSearch349" runat="server">
             <div class="cnt">
                <div class="cntWrapper">                        
                <h3 class="stoolHeading" runat="server" id="searchHeader"></h3>
                </div>
                </div>
                 <Scanweb:MeetingSearch runat="server" ID="MeetingSearchBox" />
            </div>
        </div>
    </asp:PlaceHolder>
   
    <asp:PlaceHolder ID="RightInfoBox1PlaceHolder" runat="server">
        <div class="BoxContainer">
            <Scanweb:InfoBox runat="server" ID="SubscribeBox" HeaderPropertyName="RightInfoBoxHeading1"
                BodyPropertyName="RightInfoBoxBody1 " />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="RightInfoBox2PlaceHolder" runat="server">
        <div class="BoxContainer">
            <Scanweb:InfoBox runat="server" ID="InfoBox1" HeaderPropertyName="RightInfoBoxHeading2"
                BodyPropertyName="RightInfoBoxHeading2" />
        </div>
    </asp:PlaceHolder>
    <div class="BoxContainer">
        <Scanweb:PromoBoxOffer ID="PromoBoxOffer1" CssClass="BoxLarge storyBox" OfferPageLinkPropertyName="BoxContainer1" ImagePropertyName="BoxImageLarge" ImageMaxWidth="349" runat="server" />
    </div>
    <div class="BoxContainer">
        <Scanweb:PromoBoxOffer ID="PromoBoxOffer2" CssClass="BoxLarge storyBox" OfferPageLinkPropertyName="BoxContainer2" ImagePropertyName="BoxImageLarge" ImageMaxWidth="349" runat="server" />
    </div>
    <div class="BoxContainer">
        <Scanweb:PromoBoxOffer ID="PromoBoxOffer3" CssClass="BoxLarge storyBox" OfferPageLinkPropertyName="BoxContainer3" ImagePropertyName="BoxImageLarge" ImageMaxWidth="349" runat="server" />
    </div>
    <div class="BoxContainer">
        <Scanweb:PromoBoxOffer ID="PromoBoxOffer4" CssClass="BoxLarge storyBox" OfferPageLinkPropertyName="BoxContainer4" ImagePropertyName="BoxImageLarge" ImageMaxWidth="349" runat="server" />
    </div>
     <asp:PlaceHolder ID="RightDynamicTextBoxPlaceHolder" runat="server">
        <div class="BoxContainer">
            <Scanweb:InfoBox runat="server" ID="RightDynamicTextBox" HeaderPropertyName="RightDynamicTextBoxHeading" BodyPropertyName="RightDynamicTextBoxBody" />
        </div>
    </asp:PlaceHolder>
</div>
