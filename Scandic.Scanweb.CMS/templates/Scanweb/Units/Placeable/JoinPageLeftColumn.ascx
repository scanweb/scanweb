﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JoinPageLeftColumn.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.JoinPageLeftColumn" %>
<!-- BenefitsSection starts Here !-->
<div id="BenefitsSection" runat="server" visible="False" class="benefitSection enrollFGPage">
    <div id="Header">
    <h2 runat="server" id="BenefitsTitle" class="borderH2">
    </h2></div>
    <hr id="hrHeading" runat="server" visible="false" />
    <div runat="server" id="BenefitText" class="benefitsTextplaceholder">
        <div class="benefitsectionLeft enrollFGPageContainer">
            <div class="benefitimage" runat="server" id="divBenefit1Image" visible="false">
                <asp:Image ID="Benefit1Image" runat="server" CssClass="benefitImage" />
            </div>
            <div class="benefitcontainer" runat="server" id="divBenefit1" visible="false">
                <div runat="server" id="Benefit1" class="benefittext">
                </div>
            </div>
            <hr id="hrBenefit1" runat="server" visible="false" />
            <div class="benefitimage" runat="server" id="divBenefit2Image" visible="false">
                <asp:Image ID="Benefit2Image" runat="server" CssClass="benefitImage" />
            </div>
            <div class="benefitcontainer" runat="server" id="divBenefit2" visible="false">
                <div runat="server" id="Benefit2" class="benefittext">
                </div>
            </div>
            <hr id="hrBenefit2" runat="server" visible="false" />
            <div class="benefitimage" runat="server" id="divBenefit3Image" visible="false">
                <asp:Image ID="Benefit3Image" runat="server" CssClass="benefitImage" />
            </div>
            <div class="benefitcontainer" runat="server" id="divBenefit3" visible="false">
                <div runat="server" id="Benefit3" class="benefittext">
                </div>
            </div>
            <hr id="hrBenefit3" runat="server" visible="false" />
             <div class="benefitimage" runat="server" id="divBenefit4Image" visible="false">
                <asp:Image ID="Benefit4Image" runat="server" CssClass="benefitImage" />
            </div>
            <div class="benefitcontainer" runat="server" id="divBenefit4" visible="false">
                <div runat="server" id="Benefit4" class="benefittext">
                </div>
            </div>
        </div>
    </div>
    
    <div id="myProfileImage" runat="server" visible="false"><asp:Image ID="imgMyProfile" runat="server" CssClass="benefitImageMyProfile" /></div>
    
    <div class="clear">
    </div>
</div>
<!-- BenefitsSection Ends Here !-->
