using System;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable
{
    /// <summary>
    /// PromoBoxStart
    /// </summary>
    public partial class PromoBoxStart : ScandicUserControlBase
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulatePromoBoxStart();
            }
        }

        #region Properties

        private string imagePropertyName;

        public string ImagePropertyName
        {
            get { return imagePropertyName; }
            set { imagePropertyName = value; }
        }

        public string PriceTextPropertyName { get; set; }

        public string FontColorProperty_Name { get; set; }

        private string linkTextPropertyName1;

        public string LinkTextPropertyName1
        {
            get { return linkTextPropertyName1; }
            set { linkTextPropertyName1 = value; }
        }

        private string pageLinkPropertyName1;

        public string PageLinkPropertyName1
        {
            get { return pageLinkPropertyName1; }
            set { pageLinkPropertyName1 = value; }
        }

        private string backgroundColourBlackPropertyName;

        public string BackgroundColourBlackPropertyName
        {
            get { return backgroundColourBlackPropertyName; }
            set { backgroundColourBlackPropertyName = value; }
        }

        private int imageMaxWidth;

        public int ImageMaxWidth
        {
            get { return imageMaxWidth; }
            set { imageMaxWidth = value; }
        }

        private int stoolImageMaxWidth;

        public int StoolImageMaxWidth
        {
            get { return stoolImageMaxWidth; }
            set { stoolImageMaxWidth = value; }
        }

        private int stoolImageMaxHeight;

        public int StoolImageMaxHeight
        {
            get { return stoolImageMaxHeight; }
            set { stoolImageMaxHeight = value; }
        }

        public string LinkText1
        {
            get { return (CurrentPage[linkTextPropertyName1] as string ?? string.Empty); }
        }

        public string TrackPromoboxLink1Id { get; set; }

        public string ContentNameTrackLink1Id { get; set; }

        public string BoxText { get; set; }

        public string LinkUrl1
        {
            get
            {
                string url = string.Empty;
                PageReference shortCutReference = CurrentPage[pageLinkPropertyName1] as PageReference;
                if (shortCutReference != null)
                {
                    PageData boxPage = DataFactory.Instance.GetPage(shortCutReference,
                                                                    EPiServer.Security.AccessLevel.NoAccess);
                    if (boxPage != null)
                    {
                        bool published = boxPage.CheckPublishedStatus(PagePublishedStatus.Published);

                        if (published)
                        {
                            url = boxPage.LinkURL;
                        }
                    }
                }
                return url;
            }
        }

        public PageData BoxPage { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// GetBackgroundColourCSS
        /// </summary>
        /// <returns>BackgroundColourCSS</returns>
        protected string GetBackgroundColourCSS()
        {
            //string cssClassforBGColour;
            //bool backGroundColourBlack = false;
            //if (CurrentPage[backgroundColourBlackPropertyName] != null)
            //{
            //    backGroundColourBlack = (bool)CurrentPage[backgroundColourBlackPropertyName];
            //}
            //if (backGroundColourBlack)
            //{
            //    cssClassforBGColour = "smallPromoBoxDark";
            //}
            //else
            //{
            //    cssClassforBGColour = "smallPromoBoxLight";
            //}
            return "smallPromoBoxLight storyBox promoOfferBox";//cssClassforBGColour;
        }

        /// <summary>
        /// PopulatePromoBoxStart
        /// </summary>
        protected void PopulatePromoBoxStart()
        {
            if (CurrentPage[imagePropertyName] != null)
            {
                string imageString = CurrentPage[imagePropertyName] as string;

                BoxImage.ImageUrl = WebUtil.GetImageVaultImageUrl(imageString, imageMaxWidth);                
                ImagePlaceHolder.Visible = true;
            }
            string priceText = CurrentPage[PriceTextPropertyName] as string;
            string FontColour = string.Empty;
            FontColour = CurrentPage[FontColorProperty_Name] as string;
            if (priceText != null)
            {
                PriceText.Text = priceText;
                PriceTextPlaceHolder.Visible = true;
                if (!string.IsNullOrEmpty(FontColour))
                    PriceRate.InnerHtml = CurrentPage[PriceTextPropertyName] as string;
                PriceRate.Attributes.Add("strong style", "color:" + FontColour);
            }
            if (LinkText1 != string.Empty && LinkUrl1 != string.Empty)
            {
                bool trackLink = false;
                var contentName = CurrentPage[ContentNameTrackLink1Id] as string;
                var linkUrl = string.Format("<a href=\"{0}\" class=\"IconLink\"{2}>{1}</a>", LinkUrl1, LinkText1, string.Empty);
                if (CurrentPage[TrackPromoboxLink1Id] != null)
                {
                    trackLink = (bool)CurrentPage[TrackPromoboxLink1Id];
                }
                if (trackLink && !string.IsNullOrEmpty(contentName))
                {
                    var trackingMethod = string.Format("trackPromobox('{0}A','{1}');", BoxText, contentName.Replace("'", "\\'"));
                    linkUrl = string.Format("<a href=\"{0}\" onclick=\"return {3}\" class=\"IconLink\"{2}>{1}</a>", LinkUrl1, LinkText1, string.Empty, trackingMethod);
                }
                PageLink1.Text = linkUrl;
                PageLinkPlaceHolder1.Visible = true;
            }
            if (string.IsNullOrEmpty(LinkUrl1))
                this.Visible = false;
        }

        protected string GetOfferPageLink()
        {
            return string.Format("javascript:window.location.href = '{0}{1}", LinkUrl1, "';return false");
        }

        #endregion
    }
}