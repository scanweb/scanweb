//  Description					: TopPageHeader                                           //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable
{
    /// <summary>
    /// TopPageHeader
    /// </summary>
    public partial class TopPageHeader : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (CurrentPage != null && !string.IsNullOrEmpty(CurrentPage.PageName))
                {
                    ltlHeading.Text = CurrentPage["Heading"] as string ?? CurrentPage.PageName;
                    PlaceHolder phSchemaOrgCountryCityName = (PlaceHolder)this.FindControl("phSchemaOrgCountryCityName");

                    if (phSchemaOrgCountryCityName != null)
                        Utility.AddMetaInfoForSchemaOrg(phSchemaOrgCountryCityName, "itemprop", "name", CurrentPage.PageName ?? "");
                }
            }
        }
    }
}