using System.Web;
using EPiServer.Core;
using EPiServer.Core.Html;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// An UserControl that renders some page information and presents
    /// it in a box
    /// </summary>
    public partial class PagePuff : ScandicUserControlBase
    {

        private int PreviewTextLength = 100;

        public string FourthPageReference { get; set; }

        public string ThirdPageReference { get; set; }

        public string SecondPageReference { get; set; }

        public string FirstPageReference { get; set; }

        /// <summary>
        /// GetHeader
        /// </summary>
        /// <param name="propName"></param>
        /// <returns>Header</returns>
        protected string GetHeader(string propName)
        {
            if (CurrentPage[propName] as PageReference != null)
            {
                PageData pd = GetPage((PageReference) CurrentPage[propName]);
                return GetPageTitle(pd);
            }
            return string.Empty;
        }

        /// <summary>
        /// GetBody
        /// </summary>
        /// <param name="propName"></param>
        /// <returns>Body</returns>
        protected string GetBody(string propName)
        {
            if (CurrentPage[propName] as PageReference != null)
            {
                PageData pd = GetPage((PageReference) CurrentPage[propName]);
                string previewText = pd["MainIntro"] as string;
                if (previewText != null)
                {
                    return StripPreviewText(previewText);
                }
                previewText = pd["MainBody"] as string;
                if (previewText == null)
                {
                    return string.Empty;
                }
                return TextIndexer.StripHtml(previewText, PreviewTextLength);
            }
            return string.Empty;
        }

        /// <summary>
        /// GetContainerLink
        /// </summary>
        /// <param name="propName"></param>
        /// <returns>ContainerLink</returns>
        protected string GetContainerLink(string propName)
        {
            if (CurrentPage[propName] as PageReference != null)
            {
                PageData pd = GetPage((PageReference) CurrentPage[propName]);

                if (pd["PuffLinkText"] != null && pd.Property["PuffLinkText"].ToString().Length > 0)
                {
                    string _linkTag = "<a href=\"{0}\" class=\"IconLink\" title=\"{1}\">{1}</a>";
                    return string.Format(_linkTag, HttpUtility.HtmlEncode(pd.LinkURL), pd["PuffLinkText"]);
                }
                else
                {
                    string _linkTag = "<a href=\"{0}\" class=\"IconLink\" title=\"{1}\">{1}</a>";
                    return string.Format(_linkTag, HttpUtility.HtmlEncode(pd.LinkURL), GetPageTitle(pd));
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Strips a text to a given length without splitting the last word.
        /// </summary>
        /// <param name="previewText">The string to shorten</param>
        /// <returns>A shortened version of the given string</returns>
        private string StripPreviewText(string previewText)
        {
            if (previewText.Length <= PreviewTextLength)
            {
                return previewText;
            }
            int nextWord = previewText.IndexOfAny(new char[] {' ', '.', ',', '!', '?'}, PreviewTextLength, 10);
            int length = nextWord > PreviewTextLength && nextWord < previewText.Length ? nextWord : PreviewTextLength;
            return previewText.Substring(0, length) + "...";
        }

        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            if (!IsPostBack)
                DataBind();
        }
    }
}