//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3053
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable {
    
    
    /// <summary>
    /// PromoBoxOffer class.
    /// </summary>
    /// <remarks>
    /// Auto-generated class.
    /// </remarks>
    public partial class PromoBoxOffer {
        
        /// <summary>
        /// PromoBoxOfferPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder PromoBoxOfferPlaceHolder;
        protected global::System.Web.UI.WebControls.PlaceHolder alertInfoContainer;
        /// <summary>
        /// ImagePlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        /// 
        protected global::System.Web.UI.WebControls.PlaceHolder ImagePlaceHolder;
        
        /// <summary>
        /// BoxImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image BoxImage;
        
        /// <summary>
        /// PromoHeaderPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder PromoHeaderPlaceHolder;
        
        /// <summary>
        /// PromoHeader control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal PromoHeader;
        protected global::System.Web.UI.WebControls.Literal alertPromoHeader;
        /// <summary>
        /// PromoTextPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder PromoTextPlaceHolder;
        
        /// <summary>
        /// PromoText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::EPiServer.Web.WebControls.Property PromoText;
        protected global::EPiServer.Web.WebControls.Property AlertPromoText;
        
        /// <summary>
        /// PromoText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Literal1;

        
        /// <summary>
        /// PriceTextPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder PriceTextPlaceHolder;
        
        /// <summary>
        /// PriceText control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal PriceText;
        
        /// <summary>
        /// PageLinkPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder PageLinkPlaceHolder;
        
        /// <summary>
        /// PageLink control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal PageLink;


        /// <summary>
        /// PageLinkPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder AlertInfoPlaceHolder;

        /// <summary>
        /// PageLink control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal AlertInfoLink;

        protected global::System.Web.UI.HtmlControls.HtmlAnchor ReadMoreLink1;
        protected global::System.Web.UI.HtmlControls.HtmlAnchor ReadMoreLink2;
        
    }
}
