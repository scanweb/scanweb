<%@ Control Language="C#" AutoEventWireup="true" Codebehind="PromoBoxOffer.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.PromoBoxOffer" %>
<div class="<%= GetCssClass() %><%= GetAlertBoxStyle() %>">
    <asp:PlaceHolder ID="PromoBoxOfferPlaceHolder" runat="server">
        <div class="regular">
            <div class="stoolHeader">
                <div class="hd sprite">
                </div>
                <div class="cnt">
                    <div class="cntWrapper">
                        <asp:PlaceHolder ID="PromoHeaderPlaceHolder" Visible="false" runat="server">
                            <h3 class="stoolHeading">
                                <asp:Literal ID="PromoHeader" runat="server" />
                            </h3>
                        </asp:PlaceHolder>
                    </div>
                </div>
                <div class="ft sprite">
                    &nbsp;</div>
            </div>
            <div class="cnt">
                <asp:PlaceHolder ID="ImagePlaceHolder" Visible="false" runat="server">
                    <asp:Image ID="BoxImage" runat="server" CssClass="smallPBoxStartImage" />
                </asp:PlaceHolder>
                <div class="innerContent">
                    <asp:PlaceHolder ID="PromoTextPlaceHolder" Visible="false" runat="server">
                        <p>
                            <episerver:property id="PromoText" runat="server" />
                        </p>
                    </asp:PlaceHolder>
                    <div class="priceRate">
                        <asp:PlaceHolder ID="PriceTextPlaceHolder" Visible="false" runat="server">
                            <asp:Literal ID="PriceText" runat="server"></asp:Literal>
                        </asp:PlaceHolder>
                    </div>
                    <asp:PlaceHolder ID="PageLinkPlaceHolder" Visible="false" runat="server">
                        <div><asp:Literal ID="PageLink" runat="server" /></div>
                    </asp:PlaceHolder>
                </div>
            </div>
            <div class="ft sprite">
            </div>
        </div>
    </asp:PlaceHolder>
<!--Suresh: Added a alert info place holder for reservation 2.0 purpose -->
    <asp:PlaceHolder ID="alertInfoContainer" runat="server">
        <div class="roundMe boxColor">
            <p>
                <span class="spriteIcon iconHolder"></span>
                <strong>
                    <asp:Literal ID="alertPromoHeader" runat="server" /></strong>
                <episerver:property id="AlertPromoText" runat="server" />
                <!--R2.0 Artf1193980  (Box style issue on Hotel landing page)- Ashish -->
                <asp:PlaceHolder ID="AlertInfoPlaceHolder" Visible="false" runat="server">
                        <asp:Literal ID="AlertInfoLink" runat="server" />
                </asp:PlaceHolder>
            </p>
        </div>
    </asp:PlaceHolder>
</div>
