<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingModuleAlternativeBox.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.BookingModuleAlternativeBox" %>

<div class="<%= CssClass %>">
    <div class="AlternativeBoxTop"></div>
    <div class="AlternativeBoxContent">
          <EPiServer:Property PageLink="<%# EPiServer.Core.PageReference.RootPage %>" PropertyName="BookingModuleAlternativeText" runat="server" />
    </div>
    <div class="AlternativeBoxBottom"></div>
</div>
    