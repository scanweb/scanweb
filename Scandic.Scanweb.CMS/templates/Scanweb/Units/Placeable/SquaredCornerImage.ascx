<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SquaredCornerImage.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.SquaredCornerImage" %>

<div class="<%= TopCssClass %>">
</div>
<div class="<%= ImageCssClass %>">
    <img src="<%= GetImageURL() %>" alt="<%= GetAltText() %>" />
</div>
<div class="<%= BottomCssClass %>">
</div>