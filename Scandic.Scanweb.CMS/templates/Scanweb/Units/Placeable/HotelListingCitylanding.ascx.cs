﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.CMS.Templates.Units.Placeable;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable
{
    public partial class HotelListingCitylanding : EPiServer.UserControlBase
    {
        private const string SORT_ALPHA_PATH = "/bookingengine/booking/selecthotel/sortAlphabetic";
        private const string SORT_CITY_CENTRE_PATH = "/bookingengine/booking/selecthotel/sortCiteCentre";
        private const string SORT_TRIPADVISOR = "/bookingengine/booking/selecthotel/sortTripAdvisor";

        #region Protected Methods

        /// <summary>
        /// Pageload method 
        /// 1.Sets the values for the dropdown for sorting
        /// 2.Clears the text to display the searched hotels
        /// 2.Clears Session variable for Sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetDropDowns();
                ddlSortHotel.SelectedValue = SortType.CITYCENTREDISTANCE.ToString();
            }
        }

        protected void SetDropDowns()
        {
            ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_ALPHA_PATH),
                                                SortType.ALPHABETIC.ToString()));
            ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_CITY_CENTRE_PATH),
                                                    SortType.CITYCENTREDISTANCE.ToString()));

            bool hideCityTARatings = CurrentPage["HideCityTARatings"] != null ? Convert.ToBoolean(CurrentPage["HideCityTARatings"]) : false;
            if (!Convert.ToBoolean(hideCityTARatings))
            {
                ddlSortHotel.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_TRIPADVISOR), SortType.TRIPADVISOR.ToString()));
            }


        }
        protected void Sort_Hotels(object sender, EventArgs e)
        {
            DisplayHotelDestinations(ddlSortHotel.SelectedValue.ToString());
        }
        /// <summary>
        /// Loading of the control is done here
        /// 1.Resets the pagination and Session sorting variable
        /// 2.Displays the Hotels on the control
        /// 3.Sets the Search result text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (FindAHotelSessionVariablesSessionWrapper.SelectedCity != null)
            {
                DisplayHotelDestinations(ddlSortHotel.SelectedValue.ToString());

                if (FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults() != null)
                {
                    StringBuilder searchResultText = new StringBuilder();
                    searchResultText.Append(AppConstants.SPACE);
                    if (FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults().Count == 1)
                    {
                        searchResultText.Append(
                            WebUtil.GetTranslatedText(
                                "/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/HotelMatchText"));
                    }
                    else
                    {
                        searchResultText.Append(
                            WebUtil.GetTranslatedText(
                                "/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/HotelsMatchText"));
                    }

                    if (0 != string.Compare(CurrentPage.LanguageBranch, LanguageConstant.LANGUAGE_FINNISH, true))
                    {
                        searchResultText.Append(AppConstants.SPACE);
                    }

                    searchResultText.Append(WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/For"));
                    searchResultText.Append(AppConstants.SPACE);
                    searchResultText.Append("\"");
                    searchResultText.Append(FindAHotelSessionVariablesSessionWrapper.SelectedCity);
                    searchResultText.Append("\"");
                }
            }
        }

        /// <summary>
        /// This method will return the list of hotels to be displayed to the user
        /// 1.If user has selected the ShowAll option from Select Hotel page then all hotels are returned. 
        /// 2.If the user has selected different page from the select Hotel, then corresponding
        /// list of hotels to be displayed for the page selected will be displayed
        /// 3.If the user has done a sorting the hotels are already sorted before this method is called
        /// so the corresponding hotels for the page 1 is returned if user does a sorting
        /// </summary>
        /// <returns>The list of hotels to be displayed to the user</returns>
        protected List<HotelDestination> HotelDestinationsToBeDisplayed()
        {
            List<HotelDestination> hotelDestination = null;

            if (FindAHotelSessionVariablesSessionWrapper.SelectedCity != null)
                hotelDestination = FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults();

            return hotelDestination;
        }

        #endregion Protected Methods

        #region Private Methods

        private SortType GetSortyByOption(string sortBySelected)
        {
            SortType sortType = SortType.NONE;
            try
            {
                sortType = (SortType)Enum.Parse(typeof(SortType), sortBySelected, true);
            }
            catch (Exception)
            {
            }
            return sortType;
        }

        /// <summary>
        /// This Method is used to display the child control on parent control
        /// 1.HotelListingDetailBody is the child control which is displayed on the Parent HotelListing control
        /// 2.Loads the control dynamically 
        /// 3.Populates data into the HotelListingDetailBody from the hotelDestination list
        /// </summary>
        private void DisplayHotelDestinations(string sortType)
        {
            allHotels.Controls.Clear();
            List<HotelDestination> hotels = HotelDestinationsToBeDisplayed();
            
            if (hotels != null)
            {
                switch (GetSortyByOption(sortType))
                {
                    case SortType.CITYCENTREDISTANCE:
                        hotels.Sort(new HotelDestinationCityCentreDistanceComparer());
                        break;
                    case SortType.ALPHABETIC:
                        hotels.Sort(new HotelDestinationNameComparer());
                        break;
                    case SortType.TRIPADVISOR:
                        hotels.Sort(new HotelDestinationNameComparer());
                        hotels.Sort(new HotelDestinationTripAdvisorRatingComparer());
                        //hotels = SelectHotelUtil.SortByTARatingMaxRateForCityLandingPages(hotels);
                        break;
                }

                bool hideCityTARatings = CurrentPage["HideCityTARatings"] != null
                                           ? Convert.ToBoolean(CurrentPage["HideCityTARatings"]) : false;
                for (int hotelCount = 0; hotelCount < hotels.Count; hotelCount++)
                {
                    HotelDestination hotel = hotels[hotelCount];
                    hotel.HideCityTARating = hideCityTARatings;
                    IHotelListingDisplayInformation hotelDisplay =
                        SelectHotelUtil.GetHotelDestinationDisplayObject(SearchType.REGULAR, hotel, hotelCount);

                    if (hotelDisplay != null)
                    {
                        string virtualPathOfControl =
                            ResolveUrl("~/Templates/Scanweb/Units/Placeable/HotelListingCityDetailBody.ascx");
                        HotelListingCityDetailBody hotelBody = (HotelListingCityDetailBody)LoadControl(virtualPathOfControl);
                        if (hotelBody != null)
                        {
                            hotelBody.HotelAddress = hotel.HotelAddress;
                            hotelBody.HotelTelephone = hotel.Telephone;
                            hotelDisplay.HideCityTARatings = hideCityTARatings;
                            hotelBody.GeoPoints = hotel.Coordinate;
                            hotelBody.Hotel = hotelDisplay;
                            allHotels.Controls.Add(hotelBody);
                        }
                    }
                }
            }
        }
        #endregion Private Methods
    }
}