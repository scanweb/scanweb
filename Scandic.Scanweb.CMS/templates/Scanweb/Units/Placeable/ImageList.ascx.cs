//  Description					:   ImageList                                             //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// Code behind of imagelist control.
    /// </summary>
    public partial class ImageList : ScandicUserControlBase
    {
        #region protected override void OnInit(EventArgs e)

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <remarks>When notified by this method, server controls must perform any initialization steps that are required to 
        /// create and set up an instance. In this stage of the server control's lifecycle, the control's view state has yet 
        /// to be populated. Additionally, you can not access other server controls when this method is called either, regardless 
        /// of whether it is a child or parent to this control. Other server controls are not certain to be created and ready 
        /// for access.</remarks>
        /// <example>
        /// // Override the OnInit method to write text to the 
        /// // containing page if the _text property is null.
        /// [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name="FullTrust")] 
        /// protected override void OnInit(EventArgs e) {
        /// 	base.OnInit(e);
        /// 	if ( _text == null)
        /// 		_text = "Here is some default text.";
        /// }
        /// </example>
        /// <param name="e">An <see cref="System.EventArgs">EventArgs</see> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        #endregion

        #region private void InitializeComponent()

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// Initiates the designer controls and settings (should be called from OnInit())
        /// </summary>
        private void InitializeComponent()
        {
            Load += new EventHandler(Page_Load);
        }

        #endregion

        #region private void Page_Load(object sender, System.EventArgs e)

        /// <summary>
        /// This method notifies the server control that it should perform actions common to each HTTP request for the page it 
        /// is associated with, such as setting up a database query. At this stage in the page lifecycle, server controls in 
        /// the hierarchy are created and initialized, view state is restored, and form controls reflect client-side data.
        /// </summary>
        /// <param name="sender">The object that launched the event</param>
        /// <param name="e">The <see cref="System.EventArgs">EventArgs</see> object that contains the event data.</param>
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FileList.DataBind();
            }
        }

        #endregion
    }
}