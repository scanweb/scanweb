//  Description					: Archiveb                                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Globalization;
using System.Web;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.Filters;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// Archive
    /// </summary>
    public partial class Archive : ScandicUserControlBase
    {
        protected PageDataCollection pages;
        private bool evenRow = true;

        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!IsPostBack)
            {
                if (CurrentPage["NewsArchiveContainer"] as PageReference != null)
                {
                    DateTime startDate = GetPageWithCriteria("Oldest").Created;
                    DateTime endDate = GetPageWithCriteria("Newest").Created;
                    CreateListItems(startDate, endDate);
                    archivePageList.PageLinkProperty = "NewsArchiveContainer";
                    archivePageList.DataBind();
                }
                else
                    this.Visible = false;
            }
        }

        /// <summary>
        /// Creates all quarters between and including the StartPublish date and StopPublish date
        /// </summary>
        /// <param name="startDate">First StartPublish date</param>
        /// <param name="endDate">Last StartPublish date</param>
        protected void CreateListItems(DateTime startDate, DateTime endDate)
        {
            string strListText;
            string strListValue;
            startDate = startDate.AddMonths(-3); 

            while (endDate >= startDate)
            {
                strListText = GetQuarter(endDate.Month).ToString() + " " + endDate.Year.ToString();
                strListValue = GetQuarterValues(endDate);
                ddlMonths.Items.Add(new ListItem(strListText, strListValue));
                endDate = endDate.AddMonths(-3); 
            }
            ddlMonths.Items[0].Selected = true;
            ddlMonths.DataBind();
        }

        /// <summary>
        /// GetQuarterValues
        /// </summary>
        /// <param name="startDate"></param>
        /// <returns>QuarterValues</returns>
        protected string GetQuarterValues(DateTime startDate)
        {
            switch (startDate.Month)
            {
                case 1:
                    return "1/1/" + startDate.Year + ";3/31/" + startDate.Year;
                case 2:
                    return "1/1/" + startDate.Year + ";3/31/" + startDate.Year;
                case 3:
                    return "1/1/" + startDate.Year + ";3/31/" + startDate.Year;
                case 4:
                    return "4/1/" + startDate.Year + ";6/30/" + startDate.Year;
                case 5:
                    return "4/1/" + startDate.Year + ";6/30/" + startDate.Year;
                case 6:
                    return "4/1/" + startDate.Year + ";6/30/" + startDate.Year;
                case 7:
                    return "7/1/" + startDate.Year + ";9/30/" + startDate.Year;
                case 8:
                    return "7/1/" + startDate.Year + ";9/30/" + startDate.Year;
                case 9:
                    return "7/1/" + startDate.Year + ";9/30/" + startDate.Year;
                case 10:
                    return "10/1/" + startDate.Year + ";12/31/" + startDate.Year;
                case 11:
                    return "10/1/" + startDate.Year + ";12/31/" + startDate.Year;
                case 12:
                    return "10/1/" + startDate.Year + ";12/31/" + startDate.Year;
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Map month with quarter
        /// </summary>
        /// <param name="iMonth"></param>
        /// <returns>month with quarter</returns>
        protected string GetQuarter(int iMonth)
        {
            switch (iMonth)
            {
                case 1:
                    return Translate("/Templates/Scanweb/Units/Placeable/Archive/FirstQuarter");
                case 2:
                    return Translate("/Templates/Scanweb/Units/Placeable/Archive/FirstQuarter");
                case 3:
                    return Translate("/Templates/Scanweb/Units/Placeable/Archive/FirstQuarter");
                case 4:
                    return Translate("/Templates/Scanweb/Units/Placeable/Archive/SecondQuarter");
                case 5:
                    return Translate("/Templates/Scanweb/Units/Placeable/Archive/SecondQuarter");
                case 6:
                    return Translate("/Templates/Scanweb/Units/Placeable/Archive/SecondQuarter");
                case 7:
                    return Translate("/Templates/Scanweb/Units/Placeable/Archive/ThirdQuarter");
                case 8:
                    return Translate("/Templates/Scanweb/Units/Placeable/Archive/ThirdQuarter");
                case 9:
                    return Translate("/Templates/Scanweb/Units/Placeable/Archive/ThirdQuarter");
                case 10:
                    return Translate("/Templates/Scanweb/Units/Placeable/Archive/FourthQuarter");
                case 11:
                    return Translate("/Templates/Scanweb/Units/Placeable/Archive/FourthQuarter");
                case 12:
                    return Translate("/Templates/Scanweb/Units/Placeable/Archive/FourthQuarter");
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Gets the first or last PageData object from a PageDataCollection depending on sortorder
        /// </summary>
        /// <param name="strCriteria"></param>
        /// <returns>first or last PageData object</returns>
        protected PageData GetPageWithCriteria(string strCriteria)
        {
            PageDataCollection oPages;
            FilterSortOrder sortOrder;
            oPages = DataFactory.Instance.GetChildren((PageReference) CurrentPage["NewsArchiveContainer"]);

            FilterAccess filter = new FilterAccess(EPiServer.Security.AccessLevel.Read);
            filter.Filter(oPages);

            FilterPublished published = new FilterPublished(PagePublishedStatus.Published);
            published.Filter(oPages);
            sortOrder = strCriteria.Equals("Oldest")
                            ? FilterSortOrder.CreatedAscending
                            : FilterSortOrder.CreatedDescending;

            FilterSort sorter = new FilterSort(sortOrder);
            sorter.Sort(oPages);
            return oPages[0];
        }

        /// <summary>
        /// Filters out all pages not in the choosen quarter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void archivePageList_Filter(object sender, FilterEventArgs e)
        {
            CultureInfo enCulture = new CultureInfo("en-US");
            DateTime FilterStartDate =
                DateTime.Parse(ddlMonths.SelectedValue.Substring(0, ddlMonths.SelectedValue.IndexOf(';')).ToString(),
                               enCulture);
            DateTime FilterEndDate =
                DateTime.Parse(
                    ddlMonths.SelectedValue.Substring(ddlMonths.SelectedValue.IndexOf(';') + 1,
                                                      ddlMonths.SelectedValue.Length -
                                                      (ddlMonths.SelectedValue.IndexOf(';') + 1)).ToString(), enCulture);
            FilterEndDate = FilterEndDate.AddDays(1);
            pages = e.Pages;
            FilterSort sorter = new FilterSort((FilterSortOrder)CurrentPage.Property["PageChildOrderRule"].Value);
            sorter.Sort(e.Pages);
            PageData pageData;
            for (int pageIndex = 0; pageIndex < pages.Count; pageIndex++)
            {
                pageData = pages[pageIndex];


                if (pageData.Created.CompareTo(FilterStartDate) < 0 || pageData.Created.CompareTo(FilterEndDate) >= 0)
                {
                    pages.RemoveAt(pageIndex);
                    pageIndex--;
                }
            }
        }

        /// <summary>
        /// Returns a string representation of the page created date for the specified PageData 
        /// </summary>
        /// <param name="page"></param>
        /// <returns>Date</returns>
        protected string GetDate(PageData page)
        {
            string _formattedDateTime = ((DateTime) page.Created).ToString("dd.MM.yyyy") ?? string.Empty;
            return _formattedDateTime;
        }

        /// <summary>
        /// A link to the contanier of the PageList
        /// </summary>
        /// <returns>A link to the contanier of the PageList</returns>
        /// <remarks>Both SeeMoreText and PageLinkProperty must be set in order for the link to function correctly.</remarks>
        protected string GetContainerLink(PageData page)
        {
            string seeMoreText = Translate("/Templates/Scanweb/Units/Placeable/Archive/LinkText");
            string _linkTag = "<a href=\"{0}\" class=\"IconLink\" title=\"{1}\">{1}</a>";
            return string.Format(_linkTag, HttpUtility.HtmlEncode(page.LinkURL), seeMoreText);
        }

        /// <summary>
        /// GetClass
        /// </summary>
        /// <returns>Class</returns>
        protected string GetClass()
        {
            evenRow = !evenRow;
            return evenRow ? "evenrow" : string.Empty;
        }

        /// <summary>
        /// OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            archivePageList.Filter += new FilterEventHandler(archivePageList_Filter);
        }
    }
}