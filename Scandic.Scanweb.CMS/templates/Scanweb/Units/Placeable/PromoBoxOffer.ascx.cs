//  Description					: PromoBoxOffer                                           //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable
{
    /// <summary>
    /// This class encapsulate the Promo Box functionality
    /// </summary>
    public partial class PromoBoxOffer : ScandicUserControlBase
    {
        /// <summary>
        /// Page Load Method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            PageData offerPageData = GetOfferPageData();
            if (offerPageData != null)
            {
                bool read = offerPageData.QueryDistinctAccess(EPiServer.Security.AccessLevel.Read);
                bool published = offerPageData.CheckPublishedStatus(PagePublishedStatus.Published);
                if (read == false || published == false || (offerPageData["OfferEndTime"] != null && Convert.ToDateTime(offerPageData["OfferEndTime"]) < DateTime.Now))
                {
                    this.Visible = false;
                }
                else
                {
                    PopulatePromoBoxOffer(offerPageData);
                }
            }
            else
            {
                this.Visible = false;
            }
        }

        #region Properties

        /// <summary>
        /// Promo image default property name
        /// </summary>
        private string imagePropertyName = "BoxImageMedium";

        /// <summary>
        /// Promo image property name
        /// </summary>
        public string ImagePropertyName
        {
            get { return imagePropertyName; }
            set { imagePropertyName = value; }
        }

        /// <summary>
        /// Promo header default property name
        /// </summary>
        private string promoHeaderPropertyName = "BoxHeading";

        /// <summary>
        /// Promo header property name
        /// </summary>
        public string PromoHeaderPropertyName
        {
            get { return promoHeaderPropertyName; }
            set { promoHeaderPropertyName = value; }
        }

        /// <summary>
        /// Promo text default property name
        /// </summary>
        private string promoTextPropertyName = "BoxContent";

        /// <summary>
        /// Promo price text default property name
        /// </summary>
        private string priceTextPropertyName = "PromoBoxPriceText";

        /// <summary>
        /// Promo price text property name
        /// </summary>
        public string PriceTextPropertyName
        {
            get { return priceTextPropertyName; }
            set { priceTextPropertyName = value; }
        }

        /// <summary>
        /// Promo link text default property name
        /// </summary>
        private string linkTextPropertyName = "BoxLinkText";

        /// <summary>
        /// Promo link text property name
        /// </summary>
        public string LinkTextPropertyName
        {
            get { return linkTextPropertyName; }
            set { linkTextPropertyName = value; }
        }

        /// <summary>
        /// Offer page link default property name
        /// </summary>
        private string offerPageLinkPropertyName;

        /// <summary>
        /// Offer page link property name
        /// </summary>
        public string OfferPageLinkPropertyName
        {
            get { return offerPageLinkPropertyName; }
            set { offerPageLinkPropertyName = value; }
        }

        private PageData offerPageLink;

        /// <summary>
        /// Offer page
        /// </summary>
        public PageData OfferPageLink
        {
            get { return offerPageLink; }
            set { offerPageLink = value; }
        }

        /// <summary>
        /// Booking link text property name
        /// </summary>
        public string BookingLinkTextPropertyName { get; set; }

        private int imageMaxWidth;

        /// <summary>
        /// Image maximum width
        /// </summary>
        public int ImageMaxWidth
        {
            get { return imageMaxWidth; }
            set { imageMaxWidth = value; }
        }

        public bool HideBookNow { get; set; }
        /// <summary>
        /// Promo default Css Class
        /// </summary>
        private string cssClass = "smallPromoBoxLight storyBox";

        /// <summary>
        /// Promo  Css Class
        /// </summary>
        public string CssClass
        {
            get { return cssClass; }
            set { cssClass = value; }
        }

        #endregion Properties

        #region Private Methods

        /// <summary>
        /// Populate the promo box 
        /// </summary>
        /// <param name="offerPageData">Offer page</param>
        private void PopulatePromoBoxOffer(PageData offerPageData)
        {
            if (offerPageData.IsDeleted == false)
            {
                if (offerPageData.LanguageID == CurrentPage.LanguageID)
                {
                    string str = offerPageData.PageLink.ToString();
                    if (offerPageData[imagePropertyName] != null)
                    {
                        string imageString = offerPageData[imagePropertyName] as string;
                        BoxImage.ImageUrl = WebUtil.GetImageVaultImageUrl(imageString, imageMaxWidth);                        
                        ImagePlaceHolder.Visible = true;
                    }

                    string promoHeaderText = offerPageData[PromoHeaderPropertyName] as string;
                    if (promoHeaderText != null)
                    {
                        PromoHeader.Text = promoHeaderText;
                        if (alertPromoHeader != null)
                        {
                            alertPromoHeader.Text = promoHeaderText;
                        }
                        PromoHeaderPlaceHolder.Visible = true;
                    }

                    PromoText.PageLink = offerPageData.PageLink;
                    PromoText.PropertyName = promoTextPropertyName;
                    if (AlertPromoText != null)
                    {
                        AlertPromoText.PageLink = offerPageData.PageLink;
                        AlertPromoText.PropertyName = promoTextPropertyName;
                    }

                    PromoTextPlaceHolder.Visible = !string.IsNullOrEmpty(promoTextPropertyName);

                    string priceText = offerPageData[PriceTextPropertyName] as string;
                    if (priceText != null)
                    {
                        PriceText.Text = priceText;
                        PriceTextPlaceHolder.Visible = true;
                    }

                    if (offerPageData["HideLinkInLists"] == null)
                    {
                        string targetFrameString = (offerPageData["PageTargetFrame"] != null &&
                                                    "1".Equals(offerPageData["PageTargetFrame"].ToString())) ? " target=\"_blank\"" : string.Empty;
                        if (PageLink != null)
                        {
                            PageLink.Text = string.Format("<a " + targetFrameString + " href=\"{0}\" class=\"IconLink\"{2}>{1}</a>",
                                              GetLinkUrl(), GetLinkText(offerPageData), string.Empty);
                            PageLinkPlaceHolder.Visible = true;
                        }

                        if (AlertInfoLink != null)
                        {
                            AlertInfoLink.Text = string.Format("<a href=\"{0}\" class=\"IconLink\"{2}>{1}</a>",
                                                               GetLinkUrl(), GetLinkText(offerPageData), string.Empty);
                        }
                        if (AlertInfoPlaceHolder != null)
                        {
                            AlertInfoPlaceHolder.Visible = true;
                        }
                    }
                }
                else
                {
                    this.Visible = false;
                }
            }
            else
            {
                this.Visible = false;
            }
        }

        /// <summary>
        /// Get Offer page
        /// </summary>
        /// <returns>Offer page</returns>
        private PageData GetOfferPageData()
        {
            if (offerPageLink == null)
            {
                PageReference offerPageReference = CurrentPage[offerPageLinkPropertyName] as PageReference;
                if (offerPageReference != null)
                {
                    offerPageLink = DataFactory.Instance.GetPage(offerPageReference, EPiServer.Security.AccessLevel.NoAccess);
                    if (offerPageLink["PageShortcutLink"] as PageReference != null)
                    {
                        PageReference shortCutReference = offerPageLink["PageShortcutLink"] as PageReference;
                        offerPageLink = DataFactory.Instance.GetPage(shortCutReference, EPiServer.Security.AccessLevel.NoAccess);
                    }
                }
            }
            return offerPageLink;
        }

        #endregion Private Methods

        #region Protected Methods

        /// <summary>
        /// Get Css Class
        /// </summary>
        /// <returns>Css class for offer as string</returns>
        protected string GetCssClass()
        {
            return cssClass;
        }
              

        protected string GetOfferPageLink()
        {
            string contentName = string.Empty;
            if (OfferPageLink["BoxHeading"] != null)
                contentName = Convert.ToString(OfferPageLink["BoxHeading"]);
            var trackingMethod = string.Format("trackPromobox(this,'{0}');", contentName);
            return string.Format("{0}javascript:window.location.href = '{1}{2}", trackingMethod, OfferPageLink.LinkURL, "';return false");
            //return string.Format("javascript:window.location.href = '{0}{1}", OfferPageLink.LinkURL, "';return false");
        }

        /// <summary>
        /// Get the Alter css class
        /// </summary>
        /// <returns></returns>
        protected string GetAlertBoxStyle()
        {
            string alertBoxStyle = offerPageLink["AlertBoxStyle"] as string;
            if (string.IsNullOrEmpty(alertBoxStyle))
            {
                PromoBoxOfferPlaceHolder.Visible = true;
                if (alertInfoContainer != null)
                {
                    alertInfoContainer.Visible = false;
                }
            }
            else
            {
                PromoBoxOfferPlaceHolder.Visible = false;
                if (alertInfoContainer != null)
                {
                    alertInfoContainer.Visible = true;
                }
            }
            return ((alertBoxStyle != null) ? (" " + alertBoxStyle) : string.Empty);
        }

        #endregion Protected Methods

        #region Public Methods

        /// <summary>
        /// Get the link text
        /// </summary>
        /// <param name="pageData">Offer page</param>
        /// <returns>link text</returns>
        public string GetLinkText(PageData pageData)
        {
            return (pageData[linkTextPropertyName] as string ??
                    LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Placeable/Box/LinkText"));
        }

        /// <summary>
        /// Get link url
        /// </summary>
        /// <returns>Link url</returns>
        public string GetLinkUrl()
        {
            if (offerPageLink.LinkType != PageShortcutType.External)
            {
                string boxLinkURL = GetFriendlyURLToPage(offerPageLink.PageLink, offerPageLink.LinkURL);

                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageReference meetingFormPageLink = RootPage["RequestForPorposalPage"] as PageReference;
                if (CurrentPage.PageTypeID == hotelPageTypeID && meetingFormPageLink != null &&
                    meetingFormPageLink.ID == offerPageLink.PageLink.ID)
                {
                    boxLinkURL = UriSupport.AddQueryString(boxLinkURL, "SelectHotelID", CurrentPage.PageLink.ID.ToString());
                }

                return boxLinkURL;
            }
            else
            {
                return offerPageLink.LinkURL;
            }
        }

        #endregion Methods
    }
}