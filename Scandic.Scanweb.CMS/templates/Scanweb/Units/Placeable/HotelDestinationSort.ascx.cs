//  Description					: HotelDestinationSort                                    //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region using
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI.WebControls;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
#endregion using

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// Code behind of HotelDestinationSort control.
    /// </summary>
    public partial class HotelDestinationSort : System.Web.UI.UserControl
    {
        #region Private Variables
        private const string SORT_ALPHA_PATH = "/bookingengine/booking/selecthotel/sortAlphabetic";
        private const string SORT_CITY_CENTRE_PATH = "/bookingengine/booking/selecthotel/sortCiteCentre";
        #endregion

        #region Protected Methods

        /// <summary>
        /// Page Load method 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetSortDestinationDropDowns();
                ddlSortHotelDestination.ClearSelection();
            }
        }

        /// <summary>
        /// Page_PreRender calls DisplayMoreLinks which has the logic to display the links
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView != null)
            {
                if (FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults() != null)
                {
                    if (FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults().Count == 1)
                    {
                        ClearSortOptions(false);
                    }
                    else
                    {
                        int countryPageTypeID =
                            PageType.Load(new Guid(ConfigurationManager.AppSettings["CountryPageTypeGUID"])).ID;
                        Node selectedNode = FindAHotelSessionVariablesSessionWrapper.SelectedNodeInTreeView;
                        if (selectedNode != null)
                        {
                            if (selectedNode.GetPageData().PageTypeID.Equals(countryPageTypeID))
                            {
                                ClearSortOptions(false);
                            }
                            else
                            {
                                ClearSortOptions(true);
                            }
                        }
                    }
                }
                else
                {
                    ClearSortOptions(false);
                }
                if (FindAHotelSessionVariablesSessionWrapper.HotelSortOrder == SortType.ALPHABETIC.ToString())
                {
                    ddlSortHotelDestination.ClearSelection();
                }
            }
            else
            {
                ClearSortOptions(false);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        /// <summary>
        /// Clears the controls
        /// </summary>
        private void ClearSortOptions(bool setVisibility)
        {
            ddlSortHotelDestination.Visible = setVisibility;
            lblViewHotelsBy.InnerHtml = WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/viewhotelsby");
            lblViewHotelsBy.Visible = setVisibility;
        }

        /// <summary>
        /// This method will set the dropdowns for the sort by dropdown box
        /// </summary>
        private void SetSortDestinationDropDowns()
        {
            ddlSortHotelDestination.ClearSelection();
            ddlSortHotelDestination.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_ALPHA_PATH),
                                                           SortType.ALPHABETIC.ToString()));
            ddlSortHotelDestination.Items.Add(new ListItem(WebUtil.GetTranslatedText(SORT_CITY_CENTRE_PATH),
                                                           SortType.DISTANCETOCITYCENTREOFSEARCHEDHOTEL.ToString()));
        }

        /// <summary>
        /// This method will read the value of the sort by option drop down of the Select Hotel page
        /// and returns the enumerated value the SortType
        /// </summary>
        /// <param name="sortBySelected">The sorty by option selected by the user</param>
        /// <returns>Returns the SortType option</returns>
        private SortType GetSortDestinationByOption(string sortBySelected)
        {
            SortType sortType = SortType.NONE;
            if (sortBySelected == SortType.ALPHABETIC.ToString())
                sortType = SortType.ALPHABETIC;
            else if (sortBySelected == SortType.DISTANCETOCITYCENTREOFSEARCHEDHOTEL.ToString())
                sortType = SortType.DISTANCETOCITYCENTREOFSEARCHEDHOTEL;

            return sortType;
        }

        /// <summary>
        /// Will be called when sorting changes
        /// 1.This method will sort the hotels based the sort option chosen by the user. 
        /// 2.It uses the Comparers for different kinds of sorting
        /// 3.The page number to be displayed is set to 1 as we should be displying the first page
        /// when user does the sorting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Sort_HotelsDestination(object sender, EventArgs e)
        {
            SortHotelDestinations();
        }

        private void SortHotelDestinations()
        {
            FindAHotelSessionVariablesSessionWrapper.IsHotelSorted = true;

            List<HotelDestination> hotelDestinationListResult = FindAHotelSessionVariablesSessionWrapper.GetHotelDestinationResults();

            if ((hotelDestinationListResult != null) && (hotelDestinationListResult.Count > 0))
            {
                string sortByOption = ddlSortHotelDestination.SelectedValue;
                FindAHotelSessionVariablesSessionWrapper.HotelSortOrder = sortByOption;

                switch (GetSortDestinationByOption(sortByOption))
                {
                    case SortType.ALPHABETIC:
                        {
                            FindAHotelSessionVariablesSessionWrapper.HotelSortOrder = SortType.ALPHABETIC.ToString();
                            SelectHotelUtil.SortHotelsDestinationByName();
                            break;
                        }
                    case SortType.DISTANCETOCITYCENTREOFSEARCHEDHOTEL:
                        {
                            FindAHotelSessionVariablesSessionWrapper.HotelSortOrder = SortType.DISTANCETOCITYCENTREOFSEARCHEDHOTEL.ToString();
                            SelectHotelUtil.SortHotelsDestinationByCityCenterDistance();
                            break;
                        }
                }
            }
        }

        #endregion Private Methods
    }
}