using System;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// HotelPageHotelInformation
    /// </summary>
    public partial class HotelPageHotelInformation : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PageData hotelPage = ((HotelLandingPage) this.Page).HotelPage;
                if (string.IsNullOrEmpty(hotelPage["StreetAddress"] as string))
                {
                    ltlAddress.Visible = false;
                }
                else
                {
                    ltlAddress.Text = hotelPage["StreetAddress"] as string
                    + "<br>" ?? String.Empty;
                }
                if (string.IsNullOrEmpty(hotelPage["PostCode"] as string))
                {
                    ltlPostCode.Visible = false;
                }
                else
                {
                    ltlPostCode.Text = hotelPage["PostCode"] as string
                    + "&nbsp;" ?? String.Empty;
                }

                if (hotelPage["PostalCity"] as string == hotelPage["City"] as string)
                {
                    ltlCity.Text = (hotelPage["PostalCity"] as string) ?? String.Empty;
                }
                else
                {
                    if (!string.IsNullOrEmpty(hotelPage["PostalCity"] as string))
                    {
                        ltlPostalCity.Text = hotelPage["PostalCity"] as string
                        + "&nbsp;" ?? String.Empty;
                    }
                    if (!string.IsNullOrEmpty(hotelPage["City"] as string))
                    {
                        ltlCity.Text = hotelPage["City"] as string ?? String.Empty;
                    }
                }

                ltlCountry.Text = hotelPage["Country"] as string ?? String.Empty;
                ltlPhone.Text =
                    LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/HotelLandingPage/Content/Phone") +
                    ": " + hotelPage["Phone"] ?? String.Empty;
                if (string.IsNullOrEmpty(hotelPage["Fax"] as string))
                {
                    ltlFax.Visible = false;
                }
                else
                {
                    ltlFax.Text =
                        LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/HotelLandingPage/Content/Fax") +
                        ": " + hotelPage["Fax"] ?? String.Empty;
                }

                ltlEmail.Text =
                    LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/HotelLandingPage/Content/Email") +
                    ": ";
                EmailInfo.InnerHtml =
                    "<a href=\"mailto:" + hotelPage["Email"] as string
                + "\">" + hotelPage["Email"] as string
                + "</a>";
                if (!string.IsNullOrEmpty((hotelPage["CentralReservationNumber"] as string)))
                {
                    ltlCentralReservationNumber.Text =
                        LanguageManager.Instance.Translate
                            ("/Templates/Scanweb/Pages/HotelLandingPage/Content/CentralReservationNumber") + ":";
                    ltlCentralReservationNumberText.Text = hotelPage["CentralReservationNumber"].ToString();
                }
                else
                    PhoneNumberPlaceHolder.Visible = false;

                GoeY.Text =
                    LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/HotelLandingPage/Content/Latitude") +
                    " - " + hotelPage["GeoY"] ?? string.Empty;
                GoeX.Text =
                    LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/HotelLandingPage/Content/Longitude") +
                    " - " + hotelPage["GeoX"] ?? string.Empty;
            }
        }
    }
}