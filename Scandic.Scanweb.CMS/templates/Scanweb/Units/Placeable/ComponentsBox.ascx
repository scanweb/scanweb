﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComponentsBox.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.ComponentsBox" %>
<div id="components" class="promoplaceholder">
    
    <div class="promotionBox">
    <h3 runat="server" id="componentsHeading1" class="lightHeading"></h3>
    <div runat="server" id="componentText1"></div>
    </div>
          
    <div class="promotionBox">  
    <h3 runat="server" id="componentsHeading2" class="lightHeading"></h3>
    <div runat="server" id="componentText2"></div>
    </div>
          
    <div class="promotionBox">  
    <h3 runat="server" id="componentsHeading3" class="lightHeading"></h3>
    <div runat="server" id="componentText3"></div>
    </div>
    <div class="clear"></div>
</div>
