<%@ Control Language="C#" AutoEventWireup="true" Codebehind="MeetingSearch.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.MeetingSearch" %>
	<%@ Register TagPrefix="Scanweb" TagName="DestinationSearch" Src="~/Templates/Booking/Units/DestinationSearch.ascx" %>
<div id="FindHotelVenue">
 
    <div class="FindHotelVenueContent">
        <div class="MeetingSearchHotelVenue">
        </div>
        <div class="MeetingSearchSearch" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnSearch.ClientID %>')" >
		<div>
		<p class="formRow fgpPointInput">    
	   <label class="fltLft"><EPiServer:Translate Text="/Templates/Scanweb/Units/Placeable/MeetingSearch/MinimumNumberOfParticipants" runat="server" /></label> 
	   <input type="text" name="txtParticipants" id="txtParticipants" maxlength="5" runat="server" tabindex="1" class="frmInputText" autocomplete="off" >
	   <span class="errorP1"></span>
       <label class="fltLft mrgTop5"><EPiServer:Translate Text="/Templates/Scanweb/Units/Placeable/MeetingSearch/EventPlace" runat="server" /></label> 
       <input type="text" name="txtdestName" id="txtdestName" onchange="ClearSelectedDestID();" runat="server" tabindex="2" class="frmInputText" autocomplete="off" >
				<input type="hidden" id="selectedDestIdFGP" runat="server" width="0" />
				<input type="hidden" id="notmatchingDest" value="notmatching" width="0" />
				<input type="hidden" id="redemptionPointsCampaignPeriod" value="cmppreiod" width="0"/>
				<input type="hidden" id="hiddenAll" value="hideall" width="0"/>
		</p>
			<div id="autosuggestFGP" class="autosuggest">
				<ul>
				</ul>
				<iframe src="javascript:false;" frameborder="0" scrolling="no"></iframe>
			</div>
		</div>
		<input id="participantError" class="error-txtp" type="hidden" runat="server">
		<input id="searchError" class="error-txt" type="hidden" runat="server">
		<div class="errorP"></div>
		        <div class="MeetingSearchButton">
                <div class="actionBtn" id="ctl00_FullBodyRegion_MainBodyRegion_ctl00_BookingContactDetails_SubmitButtonDiv">
                    <asp:Button ID="btnSearch" class="submit buttonInner scansprite" runat="server" translate="/Templates/Scanweb/Units/Static/FindHotelSearch/Search"
                        OnClick="btnSearch_Click" />
                        <asp:Button ID="spnSearch" class="buttonRt noBorder" runat="server" OnClick="btnSearch_Click" />
                </div>
            </div>
        </div>
    </div>
    <div class="FindHotelVenueBottom">
        &nbsp;</div>
		<script type="text/javascript" src="/Templates/Booking/Javascript/autoSuggestforFGP.js"></script>					
		<script type="text/javascript" src="/Templates/Booking/Javascript/crisis.js"></script>					
        <script type="text/javascript" language="javascript">
        function ClearSelectedDestID() {
            var destination = $("input[id*='txtdestName']")
            var selectedDestId = $("input[id*='selectedDestIdFGP']");
            if (destination != null) {
                if (destination.val() == "") {
                    if (selectedDestId != null) {
                        selectedDestId.val('');
                    }
                }
            }
        }
        $(document).ready(function() {
        $('#viewMoreResults').bind('keypress', function(e) {
            if (e.keyCode == 13 || e.which == 13 || e.keyCode == 32 || e.which == 32) {
                // Hit on search  validateCountryDestination();
            }
        });


        $('#ErrorFGP').hide();

        var Countrydestination = _endsWith("txtdestName");
        if (Countrydestination != null) {
            new AutoSuggestforFGP($fn(Countrydestination), 'autosuggestFGP', 'selectedDestIdFGP', false);
        }

    });
</script>
</div>
<div id="destinationSearch">
        <Scanweb:DestinationSearch id="DestinationSearch1" runat="server" />
    </div>