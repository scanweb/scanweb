﻿using System;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable
{
    public partial class ComponentsBox : EPiServer.UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(CurrentPage != null)
                BindComponentsData();
        }

        private void BindComponentsData()
        {
            if (CurrentPage["Component1Heading"] != null)
                componentsHeading1.InnerText = CurrentPage["Component1Heading"].ToString();
            if (CurrentPage["Component1Text"] != null)
                componentText1.InnerHtml = CurrentPage["Component1Text"].ToString();

            if (CurrentPage["Component2Heading"] != null)
                componentsHeading2.InnerText = CurrentPage["Component2Heading"].ToString();
            if (CurrentPage["Component2Text"] != null)
                componentText2.InnerHtml = CurrentPage["Component2Text"].ToString();

            if (CurrentPage["Component3Heading"] != null)
                componentsHeading3.InnerText = CurrentPage["Component3Heading"].ToString();
            if (CurrentPage["Component3Text"] != null)
                componentText3.InnerHtml = CurrentPage["Component3Text"].ToString();
        }
    }
}