//  Description					:   RightColumnWide                                       //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.CMS.SpecializedProperties;
using Scandic.Scanweb.CMS.Util;
using ImageStoreNET.Classes.Data;
using EPiServer;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// This class contains all members of RightColumnWide
    /// </summary>
    public partial class RightColumnWide : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            PromoBoxOffer1.HideBookNow = true;
            PromoBoxOffer2.HideBookNow = true;
            PromoBoxOffer3.HideBookNow = true;
            PromoBoxOffer4.HideBookNow = true;
            PageData startPage = EPiServer.DataFactory.Instance.GetPage(PageReference.StartPage, EPiServer.Security.AccessLevel.NoAccess);
            searchHeader.InnerText = Convert.ToString(startPage["MeetingSearchModuleHeader"]);
            if (Convert.ToBoolean(CurrentPage["MiddleSearchModuleVisible"]))
                MeetingSearch349.Visible = false;

            BookingModulePlaceHolder.Visible = PropertyValueEqualsVisible("RightBookingModuleVisible");
            MeetingModulePlaceHolder.Visible = PropertyValueEqualsVisible("RightMeetingModuleVisible");
            RightInfoBox1PlaceHolder.Visible = (CurrentPage["RightInfoBoxHeading1"] != null ||
                                                CurrentPage["RightInfoBoxBody1"] != null);
            RightInfoBox2PlaceHolder.Visible = (CurrentPage["RightInfoBoxHeading2"] != null ||
                                                CurrentPage["RightInfoBoxBody2"] != null);
            RightDynamicTextBoxPlaceHolder.Visible = PropertyValueEqualsVisible("RightDynamicTextBoxVisible");

            BE.Visible = OWSVisibilityControl.BookingModuleShouldBeVisible;
            AlternativeBookingModule.Visible = !OWSVisibilityControl.BookingModuleShouldBeVisible;
        }

        /// <summary>
        /// Gets required CSS
        /// </summary>
        /// <returns></returns>
        protected string GetCSS()
        {
            if (PropertyValueEqualsVisible("RightColumnShiftUp")
                &&
                (PropertyValueEqualsVisible("RightBookingModuleVisible") ||
                 PropertyValueEqualsVisible("RightMeetingModuleVisible")))
            {
                return "RightColumnShiftUp";
            }
            else
            {
                return "RightColumn";
            }
        }

        /// <summary>
        /// PropertyValueEqualsVisible
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private bool PropertyValueEqualsVisible(string propertyName)
        {
            if (CurrentPage[propertyName] != null)
            {
                return ((int) CurrentPage[propertyName] == (int) SelectVisibility.Visibility.Visible);
            }
            else
            {
                return false;
            }
        }
    }
}