<%@ Control Language="C#" AutoEventWireup="False" Codebehind="Archive.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.Archive" %>
<div id="archive">
    <h3 class="darkHeading">
        <%=
                EPiServer.Core.LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Placeable/Archive/Heading") %>
    </h3>
    <div class="archiveDropDown">
        <asp:DropDownList CssClass="dropdownlisttext" ID="ddlMonths" runat="server" AutoPostBack="true" /></div>
    <EPiServer:PageList runat="server" MaxCount="-1" ID="archivePageList">
        <HeaderTemplate>
            <div class="archiveContainer">
                <div class="top">
                </div>
                <div class="middle">
        </HeaderTemplate>
  <%--    <ItemTemplate>            
            <div class="itemRow <%#GetClass()%>">               
               <div class="itemName"><%#GetDate(Container.CurrentPage)%>&nbsp;-&nbsp;<%#GetPageTitle(Container.CurrentPage) %></div>
               <div class="itemLink IconLink"><%#GetContainerLink(Container.CurrentPage)%></div>
            </div>
        </ItemTemplate>--%>
       <ItemTemplate>
            <div class="IconLink <%# GetClass() %>">
                    <%# GetDate(Container.CurrentPage) %>&nbsp;-&nbsp;<EPiServer:Property ID="Property1" runat="server" PropertyName="PageLink" />
            </div>
        </ItemTemplate>
        <FooterTemplate>
            </div>
            <div class="bottom">
            </div>
            </div>
        </FooterTemplate>
    </EPiServer:PageList>
</div>
