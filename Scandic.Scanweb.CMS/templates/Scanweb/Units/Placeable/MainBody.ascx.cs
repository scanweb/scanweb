//  Description					:   MainBody                                              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using EPiServer;
using System;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// Gets the main body of text and a heading from either the "Heading" page property
    /// or simply the page name.
    /// </summary>
    public partial class MainBody : UserControlBase
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            this.Visible = CurrentPage["MainBody"] != null;

            if (CurrentPage["OfferEndTime"] != null && Convert.ToDateTime(CurrentPage["OfferEndTime"]) < DateTime.Now)
            {
                OfferMainBody.Attributes.Add("class", "OfferMainBodyGreyOut");
            }
        }
    }
}