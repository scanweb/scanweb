﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CookieLaw.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.CookieLaw" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<div id="CookieWrapper" runat="server"  style="display: none;" class="cookieLaw">
    <div id="cookieLawMessageDiv" class="fltLft cookieLawMessage" runat="server">
    <%--    <Episerver:property id="PropertyLink" propertyname="CookieLawMessage" runat="server" />--%>
    </div>
    <div id="Div3" class="acceptCookieWrapper">
        <a class="cookielink" id="cookieLawOkayLnk" runat="server"></a>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        var header = $('.cookieLaw').offset().top;

        $(window).bind("scroll", function() {
            if ($(window).scrollTop() > header) {
                $('.cookieLaw').addClass("sticky");
                //  var totalHeight = $(window).scrollTop() + $('.cookieLaw').height() + 25;
                //  $('#browserMsg').css('top', totalHeight + 'px');
                //   $('#browserMsg').css('z-index', '99999');
            } else {
                $('.cookieLaw').removeClass("sticky");
                //  $('#browserMsg').css('top', '0px');
            }
        });

    });
</script>

