<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelDestinationSort.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.HotelDestinationSort" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<div id="subHeader" class="subHeader" runat="server">
		<div id="sortBy" class="sortBy">
		    <input type="hidden" id="txtPageNo" value="1" runat="server"/>
		    <input type="hidden" id="txtShowAll"  value="FALSE" runat="server"/>
			<label id="lblViewHotelsBy" runat="server" class="searchResult"></label>
             <asp:DropDownList ID="ddlSortHotelDestination" OnSelectedIndexChanged="Sort_HotelsDestination" runat="server" AutoPostBack="True"> </asp:DropDownList>
		</div>
	</div>
 
   
      