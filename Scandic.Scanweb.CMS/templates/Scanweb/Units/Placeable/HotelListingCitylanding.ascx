﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelListingCitylanding.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.HotelListingCitylanding" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="System.Collections.Generic" %>

<label for="name"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/SortBy") %></label>
<asp:DropDownList CssClass="altHotelSortBy" TabIndex="10" ID="ddlSortHotel"  OnTextChanged ="Sort_Hotels" OnSelectedIndexChanged="Sort_Hotels" runat="server" AutoPostBack="True">
</asp:DropDownList>
<div id="hotelDetailContainer">
        <div id="allHotels" runat="server">
        </div>
	</div>
<div class="clear">&nbsp;</div>
