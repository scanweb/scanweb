<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PromoBoxOffer.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.PromoBoxOffer" %>
<div class="<%= GetCssClass() %><%= GetAlertBoxStyle() %>  promoOfferBox" onclick="<%=GetOfferPageLink() %>">
    <asp:PlaceHolder ID="PromoBoxOfferPlaceHolder" runat ="server" >
	    <div class="smallPBoxStartImage">
        <asp:PlaceHolder ID="ImagePlaceHolder" Visible="false" runat="server">
                <asp:Image ID="BoxImage" runat="server" CssClass="smallPBoxStartImage"/>
        </asp:PlaceHolder>
        </div>
        <div class="smallPBoxText">
            <asp:PlaceHolder ID="PromoHeaderPlaceHolder" Visible="false" runat="server">
                    <h2><asp:Literal ID="PromoHeader" runat="server"/></h2>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="PromoTextPlaceHolder" Visible="false" runat="server">
                        <p><EPiServer:Property ID="PromoText" runat="server" /></p>
            </asp:PlaceHolder>
        </div>
        <div class="priceRate">
            <asp:PlaceHolder ID="PriceTextPlaceHolder" Visible="false" runat="server">
                   <asp:Literal ID="PriceText" runat="server"></asp:Literal>
            </asp:PlaceHolder>
        </div>
       
       <div class="offerBox">
          <div class="actionBtn fltLft">
            <a class="buttonInner" title="Read More" id="ReadMoreLink1" runat="server"><EPiServer:Translate Text="/Templates/Scanweb/Units/Static/StartpagePuffs/Readmore" runat="server" /></a>
            <a class="buttonRt scansprite" id="ReadMoreLink2" runat="server"></a>
         </div>
        </div>
        
     </asp:PlaceHolder>   
 </div>


