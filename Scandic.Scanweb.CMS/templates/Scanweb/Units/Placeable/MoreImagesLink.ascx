<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MoreImagesLink.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.MoreImagesLink" %>
<asp:PlaceHolder ID="MoreImagesLinkPlaceHolder" runat="server" Visible="false">
    <a href="#" class="IconLink" onclick="OpenPopUpwindow('<%= GetMoreImagesURL() %>','700','800');"><EPiServer:Translate Text="/Templates/Scanweb/Units/Placeable/MoreImagesLink/MoreImagesLinkText" runat="server" /></a>
</asp:PlaceHolder>