<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelListing.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.HotelListing" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="System.Collections.Generic" %>
  
<!--Search Text displayed here-->   
<div class="searchResult"><strong><label id="lblNoOfHotels" runat="server"></label></strong><label id="lblMatchYourSearch" runat="server"></label></div>
<!-- hotel Listing Control -->
<!-- header -->
	<div id="subHeader" class="subHeader" runat="server">
        <input type="hidden" id="txtPageNo" value="1" runat="server"/>
	    <input type="hidden" id="txtShowAll"  value="FALSE" runat="server"/>
	</div>
<!-- /header -->
<!-- Details -->
	<div id="hotelDetailContainer">
        <div id="allHotels" runat="server">
        </div>
	</div>
<!-- /Details -->

<!-- Footer -->
	<div id="hotelFooterContainer">
	    <%
	        int totalPages = TotalHotelDestinationPages();
            if (totalPages > 1)
            {
%>
	    <div id="footerContainer" runat="server">
		<!-- page listing -->
		<div class="pageListingContainer">		
			<div id="pageListingContainer">		
			<div class="showAll fltLft">
				 <% if ("FALSE" == txtShowAll.Value)
                    {%>
                        <span class="showLink">
                        <asp:LinkButton ID="lnkShowAllHotels"  OnClientClick="javascript:SetHideUnHideListingControl()" OnClick="ShowAllHideAll" runat="server">
				        <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/showallhotels") %>
                        </asp:LinkButton>
				        </span>
                    <% }
                    else
                    {%> 
				            <span class="hideLink">
				            <asp:LinkButton ID="lnkHideAllHotels"  OnClientClick="javascript:SetHideUnHideListingControl()" OnClick="ShowAllHideAll" runat="server">
                           <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/hideallhotels") %>
                           </asp:LinkButton>
			               </span>
                    <% } %>
			</div>
			<div id="pageListing" class="pageListing fltLft">
			    <%
			        if ("FALSE" == txtShowAll.Value)
                    {
                        int currentPage = int.Parse(txtPageNo.Value);
                        int totalPages = TotalHotelDestinationPages();
%>
                    <span class="prevLink">
                <%
                    if (currentPage != 1)
                    {
%>
                    <a onclick="setfocus()" href="javascript:SelectPagination(<%= (currentPage - 1) %>);"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/previous") %></a>
				<%
                    }
                    else
                    {
%>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/previous") %>
                 <%
                    }
%>
                </span>
                <%
                    List<string> pageDetails = GetPaginationDetails();
                    for (int i = 0; i < pageDetails.Count; i++)
                    {
%>
                    | <span class="pageItem">
                    <%
                        if (currentPage != i + 1)
                        {
%>
                        <a onclick="setfocus()" href="javascript:SelectPagination(<%= i + 1 %>)";><%= pageDetails[i] %></a>
                    <%
                        }
                        else
                        {
%>
                        <span class="pageItemSelected"><%= pageDetails[i] %></span>
                    <%
                        }
%>
                     </span>
                 <% } %>
                 |
                 <span class="nextLink">
                 <% if (currentPage != totalPages)
                    { %>
                    <a onclick="setfocus()" href="javascript:SelectPagination(<%= (currentPage + 1) %>);"><%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/next") %></a>
                 <% }
                    else
                    { %>
                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/next") %>
                 <% } %>
                </span>
				<% } %>
			</div>			
			<div class="clear">&nbsp;</div>
			</div>
		</div>
		
		</div>
	    <%
            }
%>
	</div>
<!-- Footer -->
<div class="clear">&nbsp;</div>
<asp:LinkButton ID="SelectPagination" runat="server"></asp:LinkButton>
<script type="text/javascript" language="javascript">
 function setfocus()
 {
  $fn(_endsWith("Submit")).focus();
  }
   </script>
<!-- /page listing -->
<!-- End for hotel listing control-->                                                                   