<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftColumn.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.LeftColumn" %>
<%@ Register TagPrefix="Scanweb" TagName="InfoBox"          Src="~/Templates/Scanweb/Units/Static/InfoBox.ascx" %>

<div>
    <asp:PlaceHolder ID="LeftDynamicTextBoxPlaceHolder" runat="server">
        <div class="BoxContainer">
            <Scanweb:InfoBox runat="server" ID="LeftDynamicTextBox" HeaderPropertyName="LeftDynamicTextBoxHeading" BodyPropertyName="LeftDynamicTextBoxBody" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="LeftInfoBox1PlaceHolder" runat="server">
        <div class="BoxContainer">
            <Scanweb:InfoBox runat="server" ID="FirstInfoBox" HeaderPropertyName="LeftInfoBoxHeading1" BodyPropertyName="LeftInfoBoxBody1" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="LeftInfoBox2PlaceHolder" runat="server">
        <div class="BoxContainer">
            <Scanweb:InfoBox runat="server" ID="SecondInfoBox" HeaderPropertyName="LeftInfoBoxHeading2" BodyPropertyName="LeftInfoBoxBody2" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="LeftInfoBox3PlaceHolder" runat="server">
        <div class="BoxContainer">
            <Scanweb:InfoBox runat="server" ID="ThirdInfoBox" HeaderPropertyName="LeftInfoBoxHeading3" BodyPropertyName="LeftInfoBoxBody3" />
        </div>
    </asp:PlaceHolder>
</div>