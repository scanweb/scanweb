//  Description					:   XFormControl                                          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.XForms;
using EPiServer.XForms.WebControls;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using log4net;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// This user control creates a XForm based on a XForm page property 
    /// </summary>
    public partial class XFormControl : UserControlBase
    {
        private const string multiRecipientFieldName = "MultiRecipientOptions";

        private XForm form;
        private string heading;

        public string CountryMeetingEmail { get; set; }

        /// <summary>
        /// OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!IsPostBack)
            {
                DynamicProperty myDynamicProperty = DynamicProperty.Load(CurrentPage.PageLink, "GuestProgramVisibleWhenLoggedIn");

                bool isPageMarkedForLogin = myDynamicProperty != null && myDynamicProperty.Status == DynamicPropertyStatus.Defined;

                if (WebUtil.IsSessionValid() && !UserLoggedInSessionWrapper.UserLoggedIn && isPageMarkedForLogin)
                {
                    AppLogger.LogInfoMessage("User has to login to access the page marked as GuestProgramVisibleWhenLoggedIn");
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.LOGIN_FOR_DEEPLINK), false);
                }

                Dictionary<int, string> selectedHotelsDictionary =
                   MiscellaneousSessionWrapper.MeetingsSelectedHotels as Dictionary<int, string>;
                MiscellaneousSessionWrapper.MeetingsSelectedHotelsForRPF = selectedHotelsDictionary;
                Session.Remove("MeetingsSelectedHotels");

                int hotelID = 0;
                if (Int32.TryParse(Request.QueryString["SelectHotelID"], out hotelID))
                {
                    try
                    {
                        PageData hotelPage = DataFactory.Instance.GetPage(new PageReference(hotelID),
                                                                          EPiServer.Security.AccessLevel.Read);

                        int hotelPageTypeID =
                            PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                        if (hotelPage.PageTypeID == hotelPageTypeID)
                        {
                            Dictionary<int, string> newHotelDictionary = new Dictionary<int, string>();
                            newHotelDictionary.Add(hotelPage.PageLink.ID, hotelPage["Heading"] as string ?? string.Empty);
                            MiscellaneousSessionWrapper.MeetingsSelectedHotelsForRPF = newHotelDictionary;
                        }
                    }
                    catch (PageNotFoundException)
                    {

                    }
                    catch (AccessDeniedException)
                    {

                    }
                }
            }

            if (!Page.ClientScript.IsClientScriptIncludeRegistered("Xform"))
            {
                Page.ClientScript.RegisterClientScriptInclude("Xform",
                                                              Page.ResolveUrl("~/Templates/Scanweb/Javascript/Xform.js?v=") + CmsUtil.GetJSVersion());
            }

            if (IsMeetingsForm)
            {
                SetupMeetingSpecificFeatures();

                FormControl.BeforeSubmitPostedData += new SaveFormDataEventHandler(FormControl_BeforeSubmitPostedData);
                FormControl.AfterSubmitPostedData += new SaveFormDataEventHandler(FormControl_AfterSubmitPostedData);
            }

            if (!IsMeetingsForm && CurrentPage["MultiRecipientEmailAddresses"] != null)
                FormControl.BeforeSubmitPostedData += new SaveFormDataEventHandler(MultiRecipient_BeforeSubmitPostedData);

            Switch.Visible = EnableStatistics;

            if (Form == null)
            {
                return;
            }

            SetupForm();
        }

        /// <summary>
        /// FormControl BeforeSubmitPostedData
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void FormControl_BeforeSubmitPostedData(object sender, SaveFormDataEventArgs e)
        {
            NameValueCollection postedValues = e.FormData.GetValues();
            if (postedValues["Notify"] != null && postedValues["Notify"].Length > 0)
            {
                if (postedValues["Email"] != null)
                {
                    string originalRecipient = e.FormData.MailTo;
                    e.FormData.MailTo = postedValues["Email"] as string;
                    SendCustomEmail(e.FormData);

                    e.FormData.MailTo = originalRecipient;
                }
            }

            Dictionary<int, string> selectedHotelsDictionary = MiscellaneousSessionWrapper.MeetingsSelectedHotelsForRPF as Dictionary<int, string>;
            if (postedValues["EventLocation"] != null &&
                selectedHotelsDictionary != null &&
                selectedHotelsDictionary.Count > 0)
            {
                string eventLocation = postedValues["EventLocation"];

                eventLocation = eventLocation + " ";
                foreach (string hotelName in selectedHotelsDictionary.Values)
                {
                    eventLocation = eventLocation + hotelName + " ";
                }

                e.FormData.SetValue("EventLocation", eventLocation);
            }

            PageData p = null;

            if (selectedHotelsDictionary != null)
            {
                Session.Remove("MeetingsSelectedHotelsForRPF");

                Dictionary<int, string>.KeyCollection keyColl = selectedHotelsDictionary.Keys;
                if (selectedHotelsDictionary.Count == 1)
                {
                    foreach (int i in keyColl)
                    {
                        p = EPiServer.DataFactory.Instance.GetPage(new PageReference(i),
                                                                   EPiServer.Security.AccessLevel.NoAccess);
                    }

                    if (!String.IsNullOrEmpty(p["HotelMeetingEmail"] as string))
                        e.FormData.MailTo = p["HotelMeetingEmail"] as string;

                    if ((e.FormData.ChannelOptions & ChannelOptions.Email) != ChannelOptions.Email)
                        return;
                    if (!Page.IsValid)
                        return;
                    SendCustomEmail(e.FormData);
                    e.FormData.ChannelOptions &= ~ChannelOptions.Email;
                }
                else
                {
                    int anyKey = -1;
                    foreach (int key in keyColl)
                    {
                        anyKey = key;
                        break;
                    }

                    int pageLinkID = (anyKey != -1) ? anyKey : -1;
                    if (pageLinkID != -1)
                    {
                        p =
                            GetPage(
                                GetPage(GetPage(GetPage(new PageReference(pageLinkID)).ParentLink).ParentLink).
                                    ParentLink);
                        if (!String.IsNullOrEmpty(p["MeetingEmail"] as string))
                            e.FormData.MailTo = p["MeetingEmail"] as string;
                    }
                }
            }
        }

        /// <summary>
        /// FormControl_AfterSubmitPostedData
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormControl_AfterSubmitPostedData(object sender, SaveFormDataEventArgs e)
        {
            RedirectAfterSubmit();

            PostedMessage.Visible = true;
            PostedMessage.Text = Translate("/Templates/Scanweb/Pages/Form/Submitted");
            FormControl.Visible = false;

            try
            {
                ILog customLog = LogManager.GetLogger("sogeti");
                if (customLog.IsDebugEnabled)
                    customLog.Debug(string.Format("Form submission from {0} (ID:{1}) sent to {2}", CurrentPage.PageName,
                                                  e.FormData.Id.ToString(), e.FormData.MailTo));
            }
            catch (Exception)
            {
            }

            if (EnableStatistics)
                SwitchView(null, null);
        }

        /// <summary>
        /// SendCustomEmail
        /// </summary>
        /// <param name="formData">XFormData</param>
        private void SendCustomEmail(XFormData formData)
        {

            MailMessage message = new MailMessage();
            message.From = new MailAddress(formData.MailFrom);
            message.To.Add(formData.MailTo);

            message.Subject = formData.MailSubject;

            message.IsBodyHtml = true;
            StringBuilder body = new StringBuilder();
            body.Append("<html><body><br /><table>");

            NameValueCollection postedValues = formData.GetValues();

            foreach (string key in postedValues)
            {
                body.Append("<tr><td>");
                body.Append(key).Append(": ");
                body.Append("</td><td>");
                body.Append(postedValues[key]);
                body.Append("</td></tr>");
            }

            body.Append("</table></body></html>");

            message.Body = body.ToString();
            new SmtpClient().Send(message);
        }

        /// <summary>
        /// SetupMeetingSpecificFeatures
        /// </summary>
        private void SetupMeetingSpecificFeatures()
        {
            Dictionary<int, string> selectedHotelsDictionary =
                MiscellaneousSessionWrapper.MeetingsSelectedHotelsForRPF as Dictionary<int, string>;

            if (selectedHotelsDictionary != null)
            {
                MeetingsListHotelsPlaceHolder.Visible = true;
                Dictionary<int, string> leftDictionary = new Dictionary<int, string>();
                Dictionary<int, string> rightDictionary = new Dictionary<int, string>();

                bool switchColumn = false;
                foreach (KeyValuePair<int, string> k in selectedHotelsDictionary)
                {
                    if (switchColumn == false)
                    {
                        leftDictionary.Add(k.Key, k.Value);
                    }
                    else
                    {
                        rightDictionary.Add(k.Key, k.Value);
                    }
                    switchColumn = !switchColumn;
                }
                SelectedHotelsLeftRepeater.DataSource = leftDictionary;
                SelectedHotelsLeftRepeater.DataBind();
                SelectedHotelsRightRepeater.DataSource = rightDictionary;
                SelectedHotelsRightRepeater.DataBind();
            }
        }

        /// <summary>
        /// If the form field with name 'MultiRecipientOptions' exists and has a value, and that 
        /// value has a corresponding e-mail address in the 'MultiRecipientEmailAddresses' property, 
        /// the e-mail will be send to that e-mail address instead of the one specified in the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MultiRecipient_BeforeSubmitPostedData(object sender, SaveFormDataEventArgs e)
        {
            NameValueCollection formValues = e.FormData.GetValues();
            if (formValues[multiRecipientFieldName] != null)
            {
                string recipientName = formValues[multiRecipientFieldName];

                string addressesString = CurrentPage["MultiRecipientEmailAddresses"] as string;
                if (addressesString != null)
                {
                    string[] valuePairs = addressesString.Split(';');
                    foreach (string valuePair in valuePairs)
                    {
                        string[] valuePairArray = valuePair.Split(',');
                        if (valuePairArray.Length == 2)
                        {
                            if (recipientName.Equals(valuePairArray[0]))
                                e.FormData.MailTo = valuePairArray[1];
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Redirects to a defined redirect page in the XForm
        /// </summary>
        private void RedirectAfterSubmit()
        {
            if (FormControl.FormDefinition.IsSent)
            {
                if (FormControl.FormDefinition.PageAfterPost != 0)
                {
                    HygieneSessionWrapper.IsXFormSubmitted = true;
                    PageData pd = GetPage(new PageReference(FormControl.FormDefinition.PageAfterPost));
                    Page.Response.Redirect(pd.LinkURL);
                }
            }
        }

        /// <summary>
        /// Toggles the form and statistics views
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SwitchView(object sender, System.EventArgs e)
        {
            if (StatisticsPanel.Visible)
            {
                StatisticsPanel.Visible = false;
                Switch.Text = Translate("/form/showstat");
            }
            else
            {
                Statistics.DataBind();
                NumberOfVotes.Text = String.Format(Translate("/form/numberofvotes"), Statistics.NumberOfVotes);
                StatisticsPanel.Visible = true;
                Switch.Text = Translate("/form/showform");
            }
            FormPanel.Visible = !StatisticsPanel.Visible;
        }

        private void SetupForm()
        {
            Form.PageGuid = CurrentPage.PageGuid;
            if (Page.User.Identity.IsAuthenticated)
            {
                Form.PostingUser = Page.User.Identity.Name;
            }

            FormControl.FormDefinition = Form;
            FormControl.AfterSubmitPostedData += new SaveFormDataEventHandler(FormControl_AfterSubmitPostedData);
        }

        /// <summary>
        /// Gets or sets the XForm property
        /// </summary>
        public XForm Form
        {
            get
            {
                if (form == null)
                {
                    string formGuid = CurrentPage[XFormProperty] as string;
                    if (!String.IsNullOrEmpty(formGuid))
                    {
                        form = XForm.CreateInstance(new Guid(formGuid));
                    }
                }
                return form;
            }
            set { form = value; }
        }

        /// <summary>
        /// Name of the page property pointing out the current XForm
        /// </summary>
        /// <value>The name of the page property used for the XForm</value>
        public string XFormProperty { get; set; }

        /// <summary>
        /// The heading above the XForm
        /// </summary>
        /// <value>The heading that should be shown above the XForm</value>
        /// <remarks>If this property hasn't been set the page property pointed out by HeadingProperty will be used instead.</remarks>
        public string Heading
        {
            get
            {
                if (heading == null)
                {
                    if (HeadingProperty != null)
                    {
                        heading = CurrentPage[HeadingProperty] as string;
                    }
                }
                return heading;
            }
            set { heading = value; }
        }

        /// <summary>
        /// Name of the page property that should be used to generate the form heading
        /// </summary>
        /// <value>The page property name that should be used to generate the form heading</value>
        /// <remarks>If neither Heading or HeadingProperty are set, the form will be shown without any heading</remarks>
        public string HeadingProperty { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether XForm statistics should be shown.
        /// </summary>
        /// <value><c>true</c> if statistics should be shown otherwise, <c>false</c>.</value>
        public bool ShowStatistics { get; set; }

        /// <summary>
        /// Enables XForm statistics
        /// </summary>
        protected bool EnableStatistics
        {
            get
            {
                return CurrentPage["EnableStatistics"] == null
                           ? false
                           : (bool)CurrentPage["EnableStatistics"];
            }
        }

        /// <summary>
        /// Returns true if the "RequestForProposal" property is checked.
        /// </summary>
        public bool IsMeetingsForm
        {
            get { return (CurrentPage["RequestForProposal"] != null); }
        }

        /// <summary>
        /// Returns the name of the current page
        /// </summary>
        /// <returns>Page name</returns>
        protected string GetPageName()
        {
            return CurrentPage.PageName;
        }
    }
}