<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ImageList.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.ImageList" %>
<%@ Register TagPrefix="ImageVault" Namespace="ImageStoreNET.Developer.WebControls" Assembly="ImageVault.Episerver5" %>


<div>
    <Imagevault:IVFileList runat="server" FileListProperty="ImageList" id="FileList">    
	    <HEADERTEMPLATE>
            <div id="imagebank">
	    </HEADERTEMPLATE>
	    <ITEMTEMPLATE>
	        <div class="imageitemcontainer">
	            <h2><%# Container.CurrentFile.Created.ToString("dd.MM.yyyy") %>&nbsp;-&nbsp;<%# Container.CurrentFile.Name %></h2>
	            <div class="imagecontainer">
	                <div class="imagecomtainerleft"><ImageVault:IVImage runat="server" ImageWidth="226" ImageFormat="jpg"/></div>
	                <div class="imagecomtainerright">
	                    <div class="imagedescription"><%# Container.CurrentFile.MetaData["Description"].Value.ToString().Length > 0
                                         ? Container.CurrentFile.MetaData["Description"].Value
                                         : EPiServer.Core.LanguageManager.Instance.Translate(
                                             "/Templates/Scanweb/Pages/ImageVault/ImageList/Nodescription") %></div>
	                    <div class="imagelinks">	                    
	                        <ImageVault:IVConversionFormatList ID="IVConversionFormatList" runat="server">	                        	                        	                            
	                            <ITEMTEMPLATE>
	                                <div><a class="imagelinktext DownloadIconLink" href="<%# Container.CurrentFormat.LinkURL %>" onclick="window.open('<%# Container.CurrentFormat.LinkURL %>');return false"><%=
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/ImageVault/ImageList/Download") %>&nbsp;<%# Container.CurrentFormat.Name %>&nbsp;<%=
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Pages/ImageVault/ImageList/Version") %></a></div>
	                            </ITEMTEMPLATE>						        			        
						    </imagevault:IVConversionFormatList>						    
	                    </div>
	                </div>
	            </div>
	        </div>
	    </ITEMTEMPLATE>
	    <FOOTERTEMPLATE>
		    </div>
	    </FOOTERTEMPLATE>
    </imagevault:IVFileList>
</div>