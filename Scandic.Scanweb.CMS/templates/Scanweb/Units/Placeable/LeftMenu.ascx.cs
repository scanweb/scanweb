//  Description					:   LeftMenu                                              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using EPiServer;
using EPiServer.Core;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.CMS.Util.Filters;
using System.Web.UI.WebControls;


namespace Scandic.Scanweb.CMS.Templates.Public.Units
{
    /// <summary>
    /// Code behind of LeftMenu control.
    /// </summary>
    public partial class LeftMenu : UserControlBase
    {
        /// <summary>
        /// Gets or sets the data source for this control
        /// </summary>
        public Repeater MenuList { get; set; }

        /// <summary>
        /// On load event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            if (MenuList == null)
            {
                return;
            }

            PageDataCollection containerPages = DataFactory.Instance.GetChildren(CurrentPage.PageLink);
            if ((containerPages != null) && (containerPages.Count > 0))
            {
                LeftMenuCtrl.PageLink = CurrentPage.PageLink;
            }
            else
            {
                LeftMenuCtrl.PageLink = CurrentPage.ParentLink;
            }

            LeftMenuCtrl.Filter += new FilterEventHandler(new GuestProgramPageFilter().OnFilter);
            LeftMenuCtrl.DataBind();
        }

    }
}
