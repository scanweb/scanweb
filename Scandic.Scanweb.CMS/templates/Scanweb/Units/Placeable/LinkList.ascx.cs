//  Description					: LinkList                                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// Code behind of LinkList control.
    /// </summary>
    public partial class LinkList : ScandicUserControlBase
    {
        private DataTable myTable;
        private DataRow myRow;
        private DataColumn myColumn;
        private DataSet myDataSet;
        private PageDataCollection linkPages;

        /// <summary>
        /// Constructor of LinkList class.
        /// </summary>
        public LinkList()
        {
            myTable = new DataTable("PageProperty");
            myRow = myTable.NewRow();
            myDataSet = new DataSet();
            AddColumn("System.String", "PageName");
            AddColumn("System.String", "PageURL");
        }

        private string pageLinkPropertyName;

        /// <summary>
        /// Gets/Sets PageLinkPropertyName
        /// </summary>
        public string PageLinkPropertyName
        {
            get { return pageLinkPropertyName; }
            set { pageLinkPropertyName = value; }
        }

        /// <summary>
        /// AddColumn
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        private void AddColumn(string type, string name)
        {
            myColumn = new DataColumn();
            myColumn.DataType = System.Type.GetType(type);
            myColumn.ColumnName = name;
            myTable.Columns.Add(myColumn);
        }

        /// <summary>
        /// getChildren
        /// </summary>
        /// <param name="Pagelink"></param>
        /// <returns>PageDataCollection</returns>
        private PageDataCollection getChildren(PageReference Pagelink)
        {
            return DataFactory.Instance.GetChildren(Pagelink);
        }

        /// <summary>
        /// linkPagesToDataset
        /// </summary>
        private void linkPagesToDataset()
        {
            foreach (PageData pd in linkPages)
            {
                UrlBuilder url = new UrlBuilder(new Url(pd.LinkURL));
                if (pd.LinkType != PageShortcutType.External)
                {
                    EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, pd.PageLink,
                                                                          System.Text.UTF8Encoding.UTF8);
                }
                AddExternalUrl(pd.PageName.ToString(), url.ToString());
            }
        }

        /// <summary>
        /// getFriendlyUrl
        /// </summary>
        /// <param name="pd"></param>
        /// <returns>FriendlyUrl</returns>
        private string getFriendlyUrl(PageData pd)
        {
            UrlBuilder url = new UrlBuilder(new Url(pd.LinkURL));
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, pd.PageLink, System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }

        /// <summary>
        /// Checks whether the given itemURL is the last one or not.
        /// </summary>
        /// <param name="itemURL"></param>
        /// <returns>True/False</returns>
        protected bool IsLastItem(string itemURL)
        {
            if (myDataSet.Tables.Count > 0 && myDataSet.Tables[0].Rows.Count > 0)
            {
                return
                    (((string) myDataSet.Tables[0].Rows[myDataSet.Tables[0].Rows.Count - 1]["PageURL"]).Equals(itemURL));
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Adds internal page.
        /// </summary>
        /// <param name="pageIds"></param>
        public void addInternalPage(string pageIds)
        {
            PageDataCollection pdc = new PageDataCollection();
            char[] split = {','};
            string[] Pages = pageIds.Split(split);
            ;

            foreach (string temp in Pages)
            {
                if (!string.Empty.Equals(temp))
                    pdc.Add(DataFactory.Instance.GetPage(PageReference.Parse(temp),
                                                         EPiServer.Security.AccessLevel.NoAccess));
            }
            linkPages = pdc;
        }

        /// <summary>
        /// Gets ListItem Name and an External-URL.
        /// </summary>
        /// <param name="pageName"></param>
        /// <param name="pageURL"> </param>
        /// <returns>Adds an ListItem to the LinkList. Used for URL's outside Scanweb.</returns>
        public void AddExternalUrl(string pageName, string pageURL)
        {
            myRow = myTable.NewRow();
            myRow["PageName"] = pageName;
            myRow["PageURL"] = pageURL;
            myTable.Rows.Add(myRow);
        }

        /// <summary>
        /// Builds page string.
        /// </summary>
        /// <param name="link"></param>
        /// <param name="pagetoAdd"></param>
        /// <returns>page string.</returns>
        public string BuildPageString(string link, string pagetoAdd)
        {
            if (!string.Empty.Equals(pagetoAdd))
            {
                if (string.Empty.Equals(link))
                {
                    link = pagetoAdd;
                }
                else
                    link = link + "," + pagetoAdd;
            }
            return link;
        }

        /// <summary>
        /// Populates query string
        /// </summary>
        /// <param name="query"></param>
        public void PopulateByQueryString(string query)
        {
            PageData rootpage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            string links = string.Empty;
            switch (query)
            {
                case "overview":
                    {
                        if (rootpage["CommonHotelLinks"] != null)
                        {
                            foreach (PageData childPage in getChildren(rootpage["CommonHotelLinks"] as PageReference))
                            {
                                links = BuildPageString(links, childPage.PageLink.ID.ToString());
                            }
                        }
                        links = BuildPageString(links, CurrentPage.Property["OverviewFirstlink"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["OverviewSecondlink"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["OverviewSecondlink"].ToString());

                        addInternalPage(links);
                    }
                    break;
                case "location":
                    {
                        if (rootpage["CommonHotelLinks"] != null)
                        {
                            foreach (PageData childPage in getChildren(rootpage["CommonHotelLinks"] as PageReference))
                            {
                                links = BuildPageString(links, childPage.PageLink.ID.ToString());
                            }
                        }
                        links = BuildPageString(links, CurrentPage.Property["LocationFirstlink"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["LocationSecondlink"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["LocationThridlink"].ToString());

                        addInternalPage(links);
                    }
                    break;
                case "facilities":
                    {
                        if (rootpage["CommonHotelLinks"] != null)
                        {
                            foreach (PageData childPage in getChildren(rootpage["CommonHotelLinks"] as PageReference))
                            {
                                links = BuildPageString(links, childPage.PageLink.ID.ToString());
                            }
                        }
                        links = BuildPageString(links, CurrentPage.Property["FacilitiesFirstlink"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["FacilitiesSecondlist"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["FacilitiesThirdlink"].ToString());

                        addInternalPage(links);
                    }
                    break;
                case "rooms":
                    {
                        if (rootpage["CommonHotelLinks"] != null)
                        {
                            foreach (PageData childPage in getChildren(rootpage["CommonHotelLinks"] as PageReference))
                            {
                                links = BuildPageString(links, childPage.PageLink.ID.ToString());
                            }
                        }
                        links = BuildPageString(links, CurrentPage.Property["RoomsFirstlink"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["RoomsSecondlink"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["RoomsThirdlink"].ToString());

                        addInternalPage(links);
                    }
                    break;

                case "meetings":
                    {
                        if (rootpage["CommonHotelLinks"] != null)
                        {
                            foreach (PageData childPage in getChildren(rootpage["CommonHotelLinks"] as PageReference))
                            {
                                links = BuildPageString(links, childPage.PageLink.ID.ToString());
                            }
                        }
                        links = BuildPageString(links, CurrentPage.Property["MeetingFirstLink"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["MeetingSecondLink"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["MeetingThirdLink"].ToString());
                        addInternalPage(links);
                    }
                    break;

                case "guestprogram":
                    {
                        if (rootpage["CommonHotelLinks"] != null)
                        {
                            foreach (PageData childPage in getChildren(rootpage["CommonHotelLinks"] as PageReference))
                            {
                                links = BuildPageString(links, childPage.PageLink.ID.ToString());
                            }
                        }
                        links = BuildPageString(links, CurrentPage.Property["FrequentGuestFirstlink"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["FrequentGuestSecondlink"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["FrequentGuestThirdlink"].ToString());

                        addInternalPage(links);
                    }
                    break;
                default:
                    {
                        if (rootpage["CommonHotelLinks"] != null)
                        {
                            foreach (PageData childPage in getChildren(rootpage["CommonHotelLinks"] as PageReference))
                            {
                                links = BuildPageString(links, childPage.PageLink.ID.ToString());
                            }
                        }
                        links = BuildPageString(links, CurrentPage.Property["OverviewFirstlink"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["OverviewSecondlink"].ToString());
                        links = BuildPageString(links, CurrentPage.Property["OverviewSecondlink"].ToString());

                        addInternalPage(links);
                    }
                    break;
            }
        }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (linkPages != null)
                linkPagesToDataset();

            if (pageLinkPropertyName != null && CurrentPage[pageLinkPropertyName] != null)
            {
                PageDataCollection pdc = getChildren((PageReference) CurrentPage[pageLinkPropertyName]);
                foreach (PageData pd in pdc)
                    AddExternalUrl(pd.PageName.ToString(), getFriendlyUrl(pd));
            }
            myDataSet.Tables.Add(myTable);

            if (!IsPostBack)
            {
                linkLista.DataSource = myDataSet;
                linkLista.DataBind();
            }
        }
    }
}