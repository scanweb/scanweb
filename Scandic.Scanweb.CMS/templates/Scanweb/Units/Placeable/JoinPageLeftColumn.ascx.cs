﻿using System;
using Scandic.Scanweb.CMS.SpecializedProperties;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    public partial class JoinPageLeftColumn : EPiServer.UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CurrentPage["RemoveBenefitSection"] == null)
            {
                BenefitsSection.Visible = true;
                BindBenefitsData();
            }
            if (CurrentPage["RemoveMyProfileLeftColumnImage"] == null)
            {
                if (!Convert.ToBoolean(CurrentPage["RemoveMyProfileLeftColumnImage"]))
                {
                    if (CurrentPage["MyProfileLeftColumnImage"] != null)
                    {
                        myProfileImage.Visible = true;
                        var imageWidth = Convert.ToInt32(AppConstants.LeftColumnImageWidth);
                        WebUtil.GetBenefitsImageUrl(CurrentPage, "MyProfileLeftColumnImage", imgMyProfile, imageWidth);
                    }
                }
            }
        }

        private void BindBenefitsData()
        {
            var imageWidth = Convert.ToInt32(AppConstants.BenfitImageWidth);
            if (CurrentPage["JoinPageBenefitTitle"] != null)
            {
                hrHeading.Visible = true;
                BenefitsTitle.InnerText = CurrentPage["JoinPageBenefitTitle"].ToString();
            }

            if (CurrentPage["JoinPageBenefit1"] != null)
            {
                hrBenefit1.Visible = true;
                divBenefit1.Visible = true;
                Benefit1.InnerHtml = CurrentPage["JoinPageBenefit1"].ToString();
            }
            if (CurrentPage["JoinPageBenefit2"] != null)
            {
                hrBenefit2.Visible = true;
                divBenefit2.Visible = true;
                Benefit2.InnerHtml = CurrentPage["JoinPageBenefit2"].ToString();
            }
            if (CurrentPage["JoinPageBenefit3"] != null)
            {
                hrBenefit3.Visible = true;
                divBenefit3.Visible = true;
                Benefit3.InnerHtml = CurrentPage["JoinPageBenefit3"].ToString();
            }
            if (CurrentPage["JoinPageBenefit4"] != null)
            {
                divBenefit4.Visible = true;
                Benefit4.InnerHtml = CurrentPage["JoinPageBenefit4"].ToString();
            }

            if (CurrentPage["JoinPageBenefit1Image"] != null)
            {
                divBenefit1Image.Visible = true;
                WebUtil.GetBenefitsImageUrl(CurrentPage, "JoinPageBenefit1Image", Benefit1Image, imageWidth);
            }
            if (CurrentPage["JoinPageBenefit2Image"] != null)
            {
                divBenefit2Image.Visible = true;
                WebUtil.GetBenefitsImageUrl(CurrentPage, "JoinPageBenefit2Image", Benefit2Image, imageWidth);
            }
            if (CurrentPage["JoinPageBenefit3Image"] != null)
            {
                divBenefit3Image.Visible = true;
                WebUtil.GetBenefitsImageUrl(CurrentPage, "JoinPageBenefit3Image", Benefit3Image, imageWidth);
            }
            if (CurrentPage["JoinPageBenefit4Image"] != null)
            {
                divBenefit4Image.Visible = true;
                WebUtil.GetBenefitsImageUrl(CurrentPage, "JoinPageBenefit4Image", Benefit4Image, imageWidth);
            }
        }
    }
}