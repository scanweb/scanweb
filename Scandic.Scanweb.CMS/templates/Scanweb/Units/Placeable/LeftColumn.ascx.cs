using System;
using Scandic.Scanweb.CMS.SpecializedProperties;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// LeftColumn
    /// </summary>
    public partial class LeftColumn : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            LeftInfoBox1PlaceHolder.Visible =
                (CurrentPage["LeftInfoBoxHeading1"] != null || CurrentPage["LeftInfoBoxBody1"] != null);
            LeftInfoBox2PlaceHolder.Visible =
                (CurrentPage["LeftInfoBoxHeading2"] != null || CurrentPage["LeftInfoBoxBody2"] != null);
            LeftInfoBox3PlaceHolder.Visible =
                (CurrentPage["LeftInfoBoxHeading3"] != null || CurrentPage["LeftInfoBoxBody3"] != null);
            LeftDynamicTextBoxPlaceHolder.Visible = PropertyValueEqualsVisible("LeftDynamicTextBoxVisible");
        }

        /// <summary>
        /// PropertyValueEqualsVisible
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns>True/False</returns>
        private bool PropertyValueEqualsVisible(string propertyName)
        {
            if (CurrentPage[propertyName] != null)
            {
                return ((int) CurrentPage[propertyName] == (int) SelectVisibility.Visibility.Visible);
            }
            else
            {
                return false;
            }
        }
    }
}