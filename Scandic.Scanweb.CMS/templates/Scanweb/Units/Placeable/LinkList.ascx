<%@ Control Language="C#" AutoEventWireup="true" Codebehind="LinkList.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.LinkList" %>


<asp:Repeater ID="linkLista" runat="server" >
<ItemTemplate>
<div class="LinkListItem">
    <div class="<%#
                !IsLastItem(DataBinder.Eval(Container.DataItem, "PageURL") as string ?? string.Empty)
                    ? "NotLastLink"
                    : "LastLink" %>">
        <a href="<%# DataBinder.Eval(Container.DataItem, "PageURL") %>" class="IconLink"><%# DataBinder.Eval(Container.DataItem, "PageName") %></a>
    </div>
</div>   
</ItemTemplate>
</asp:Repeater>


<%--<div id="Listlinkcontainer">
<episerver:PageList runat="server" id="PageList1">
        <ItemTemplate>
         <div class="listcont">
                <div class="spl_listimage">
                    <img src="~/Templates/Scanweb/Styles/Default/Images/red_arrow_right.gif" alt=" " />
                 </div>
                 <div class="spl_listitem">
                    <episerver:property PropertyName="PageLink" runat="server"/>
                </div>
         </div>
         <asp:PlaceHolder runat="server" Visible="<%#!IsLastItem(Container.CurrentPage) %>">
           <div class="spl_listSeparator"></div>
         </asp:PlaceHolder>                
        </ItemTemplate>
</episerver:PageList>
</div>--%>

