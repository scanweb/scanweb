<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OverviewBox.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.OverviewBox" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<%--<%@ Register TagPrefix="Scanweb" TagName="MoreImagesLink" Src="~/Templates/Scanweb/Units/Placeable/MoreImagesLink.ascx" %>--%>
 <script type="text/javascript">
            $(document).ready(function() 
            {
                CarouselProcessing('<%=CurrentPageTypeId%>', '<%=CurrentPageLinkId%>');
            });

  
       </script>
<div id="hotelOverviewImageCarousel"></div>       
<asp:PlaceHolder runat="server" ID="HotelOverviewPlaceHolder">
<div id="hotelOverviewTopImg">
	<asp:PlaceHolder ID="MainImagePlaceHolder" Visible="false" runat="server">
 	    <div class="topMainImg">  
  		    <asp:Image ID="MainImage" runat="server"/>    
	    </div>
	</asp:PlaceHolder>

	<asp:PlaceHolder ID="StoolImagePlaceHolder" runat="server">
	    <div class="topStoolImg">
		    <asp:Image ID="StoolImage" runat="server" />    
	    </div>
	</asp:PlaceHolder>

	<asp:PlaceHolder ID="TextPropertyPlaceHolder" runat="server">
        <div class="topImgText" >
            <asp:Literal ID="TextProperty" runat="server"></asp:Literal>
        </div>
	</asp:PlaceHolder>
	
	<asp:PlaceHolder ID="PagelinkPropertyPlaceHolder" Visible="false" runat="server">
	     <div class="topImgLink">
                <asp:Literal ID="PageLink1" runat="server" />
         </div>
	     <div class="topImgLink">
	        <a href="#" class="IconLink jqModal" runat="server" id="moreImgLink"><EPiServer:Translate Text="/Templates/Scanweb/Units/Placeable/MoreImagesLink/MoreImagesLinkText" runat="server" /></a>
            <!--Image Gallery Implementation-->
	     </div>
	     <div class="jqmWindow dialog imgGalPopUp">
	        <div class="hd sprite"></div>
            <div class="cnt">
                <div id="imgGallery" class="scandicTabs">
                    <ul class="tabsCnt">
                    <!--artf1157344 : Image gallery | Tabs are in English on Swedish page -->
                        <li><a href="#" rel=".images" class="tabs active"><span><strong><%= images %></strong></span></a></li>
	                    <li><a href="#" class="tabs" rel=".galleryView"><span><strong><%= images360 %></strong></span></a></li>
                    </ul>
                </div>
                <div id ="imgGal" runat="server" class="bedTypeListclass">
                </div>
            </div>
            <div class="ft sprite"></div>
        </div>
        <!--Image Gallery Implementation-->
        
        <!-- R2.2 artf1157351 | Image Resizer Code -->
        <script type="text/javascript" language="javascript" src="<%= ResolveUrl("~/Templates/Booking/JavaScript/plugin/imageResizer.js") %>?v=<%=CmsUtil.GetJSVersion()%>"></script>
    </asp:PlaceHolder>
</div>
</asp:PlaceHolder>