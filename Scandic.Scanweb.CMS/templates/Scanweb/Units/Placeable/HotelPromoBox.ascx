<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelPromoBox.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.HotelPromoBox" %>
 <div id="alwaysAtScnMod06" class="storyBox">
 <asp:PlaceHolder ID="PromoBoxOfferPlaceHolder" runat ="server" >
	<div class="regular">
		<div class="stoolHeader">
			<!-- <div class="hd sprite"></div> commenting the code to remove the background --->
			<div class="cnt">
			    <!--Header of the PromoBox--->
				<h2 class="stoolHeading" runat="server" id="promoHeading"><asp:Literal ID="PromoHeader" runat="server"/></h2>
			</div>
			<!-- <div class="ft sprite">&nbsp;</div> commenting the code to remove the background --->
		</div>
		<div class="cnt">
		<!--PromoBox Image-->
	    <asp:PlaceHolder ID="ImagePlaceHolder" Visible="false" runat="server">
            <asp:Image ID="BoxImage" runat="server" CssClass="smallPBoxStartImage"/>
        </asp:PlaceHolder>
        <!--PromoBox Contents-->
         <div class="innerContent"><asp:Literal ID="PromoText" runat="server"/>
         <!--PromoBox link-->
		<asp:PlaceHolder ID="PageLinkPlaceHolder" Visible="false" runat="server">
             <asp:Literal ID="PageLink" runat="server"/>
        </asp:PlaceHolder>
         
         </div>
        
		</div>
		<div class="ft sprite"></div>
	</div>
	</asp:PlaceHolder>
</div>


