using System;
using System.Web;
using EPiServer.Core;
using EPiServer.Core.Html;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// An advanded page list commonly used for rendering news, event and article list.
    /// Can be customized to show/hide a heading, thumbnails, preview text, date and time and a link to
    /// the list container page.
    /// </summary>
    public partial class PageList : ScandicUserControlBase
    {
        private bool showHeading = true;
        protected PagingControl pc;


        /// <summary>
        /// Gets or sets the property pointing out the number of items per page
        /// </summary>
        /// <remarks>Needs to be an integer property. Setting this property enables paging.</remarks>
        public string ItemsPerPageProperty { get; set; }

        /// <summary>
        /// Gets or sets the name of the page property pointing out the parent page of this page list
        /// </summary>
        /// <returns>The name of the page property pointing out the parent page.</returns>
        public string PageLinkProperty { get; set; }

        /// <summary>
        /// Gets or Sets if a link to the Page should be visible
        /// </summary>
        /// <returns>A bool id the link shoul be visible or not
        ///</returns>
        public bool ShowItemLink { get; set; }

        /// <summary>
        /// Gets or Sets if the Page Created Date should be visible or not
        /// </summary>
        public bool ShowDate { get; set; }

        /// <summary>
        /// Gets or sets if the heading should should be visible or not
        /// </summary>
        public bool ShowHeading
        {
            get { return showHeading; }
            set { showHeading = value; }
        }


        /// <summary>
        /// Gets or sets the number of characters that should be shown in the preview text (default = 0)
        /// </summary>
        /// <returns>The character count of the preview text</returns>
        /// <remarks>
        /// The preview text is primarily created from the MainIntro property if exists, 
        /// otherwise parts of the MainBody property are being used. If neither a MainIntro nor a 
        /// MainBody property exists, the preview will not be shown.
        /// </remarks>
        public int PreviewTextLength { get; set; }

        /// <summary>
        /// Gets or sets the name of the page property that should be used to generate the date
        /// </summary>
        /// <returns>The name of the page property used for generating the date</returns>
        /// <remarks>
        /// Several built-in page properties could be used in this case, for instance PageSaved, 
        /// PageChanged, PageStartPublish, PageCreated.
        /// </remarks>
        public string DateProperty { get; set; }

        /// <summary>
        /// Gets or sets the name of the property pointing out the thumbnail image for the current item in the PageList.
        /// </summary>
        /// <returns>
        /// The name of the property pointing out the thumbnail image for the current item in the PageList.
        /// </returns>
        public string ThumbnailProperty { get; set; }

        public PageDataCollection DataSource { get; set; }

        protected string GetTextDivWidth(PageData page)
        {
            string strImage = page["ThumbnailImage"] as string;

            if (String.IsNullOrEmpty(strImage))
                return "fullwidth";
            else
                return "halfwidth";
        }

        protected string GetImageDivWidth(PageData page)
        {
            string strImage = page["ThumbnailImage"] as string;

            if (!String.IsNullOrEmpty(strImage))
                return "halfwidth";
            else
                return "nowidth";
        }

        #region 

        /// <summary>
        /// This event arg stores the current page id of the Paging container
        /// of this page list.
        /// </summary>
        public class PageChangedEventArg : EventArgs
        {
            private int currentPageNumber;

            public int CurrentPageNumber
            {
                get { return currentPageNumber; }
            }

            public PageChangedEventArg(int intCurrentPage)
            {
                currentPageNumber = intCurrentPage;
            }
        }

        public event EventHandler<PageChangedEventArg> PageChanged;

        /// <summary>
        /// This method is used to fire the page changed event on each click of 
        /// links(page number)of  Paging container.We need to do this in prerender 
        /// because in onLoad we will not get the CurrentPagingItemIndex
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            if (advancedPageList.PagingControl != null)
            {
                PageChangedEventArg pageChangedEventArg
                    = new PageChangedEventArg(advancedPageList.PagingControl.CurrentPagingItemIndex);             
                if (PageChanged != null)
                {
                    PageChanged(this, pageChangedEventArg);
                }
            }
        }

        #endregion

        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            if (MaxCountProperty != null && CurrentPage[MaxCountProperty] != null)
            {
                advancedPageList.MaxCount = (int) CurrentPage[MaxCountProperty];
            }
            if (ItemsPerPageProperty != null && CurrentPage[ItemsPerPageProperty] != null)
            {
                pc = new PagingControl();
                pc.PagesPerPagingItem = CurrentPage[ItemsPerPageProperty] != null
                                            ? (int) CurrentPage[ItemsPerPageProperty]
                                            : 0;
                pc.CssClassSelected = "pagingtextactive";
                advancedPageList.PagingControl = pc;
            }
            if (!IsPostBack)
            {
                if (DataSource != null)
                    advancedPageList.DataSource = DataSource;
                else
                    advancedPageList.PageLinkProperty = PageLinkProperty;

                advancedPageList.DataBind();
            }
        }

        /// <summary>
        /// Gets or sets the name of the page property indicating the amount of items in the PageList
        /// </summary>
        /// <returns>The name of the page property which points out the amount of items in the PageList</returns>
        public string MaxCountProperty { get; set; }

        /// <summary>
        /// GetThumbnail
        /// </summary>
        /// <param name="page"></param>
        /// <returns>PropertyData</returns>
        protected PropertyData GetThumbnail(PageData page)
        {
            PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET", "ImageStoreNET.ImageType");
            string strImage = page["ThumbnailImage"] as string;

            if (!String.IsNullOrEmpty(strImage))
                pd.Value = strImage;
            else
                pd.Value = string.Empty;

            return pd;
        }

        /// <returns>Returns a string representation of the page created date for the specified PageData 
        /// </returns>
        protected string GetDate(PageData page)
        {
            if (ShowDate)
            {
                string _formattedDateTime = ((DateTime) page.Created).ToString("dd.MM.yyyy");
                return _formattedDateTime;
            }
            return string.Empty;
        }

        /// <summary>
        /// Gets the listing headline of a specific page
        /// </summary>
        /// <param name="page">PageData object</param>
        /// <returns>The headline and/or date depending on ShowDate and ShowHeading property values</returns>
        protected string GetListHeadline(PageData page)
        {
            string dateString = ShowDate ? GetDate(page) : string.Empty;
            string separatorString = (ShowDate && ShowHeading) ? " - " : string.Empty;
            string headingString = ShowHeading ? GetPageTitle(page) : string.Empty;
            return dateString + separatorString + headingString;
        }

        /// <returns>Returns the preview text for the specified PageData</returns>
        /// <remarks>The preview text is primarily created from the MainIntro property if it exists, 
        /// otherwise parts of the MainBody property are being used. 
        /// If neither a MainIntro nor a MainBody property exists, the preview will not be shown. 
        /// The length of the preview text is defined in <code>PreviewTextLength</code></remarks>
        protected string GetPreviewText(PageData page)
        {
            if (PreviewTextLength <= 0)
            {
                return string.Empty;
            }
            string previewText = page["MainIntro"] as string;
            if (previewText != null)
            {
                return StripPreviewText(previewText);
            }
            previewText = page["MainBody"] as string;
            if (previewText == null)
            {
                return string.Empty;
            }
            return TextIndexer.StripHtml(previewText, PreviewTextLength);
        }

        /// <summary>
        /// ShowOrHidePH
        /// </summary>
        /// <param name="page"></param>
        /// <returns>True/False</returns>
        protected bool ShowOrHidePH(PageData page)
        {
            if (page["HideLinkInLists"] != null)
            {
                return false;
            }
            else
                return true;
        }


        /// <returns>A link to the contanier of the PageList</returns>
        /// <remarks>Both SeeMoreText and PageLinkProperty must be set in order for the link to function correctly.</remarks>
        protected string GetContainerLink(PageData page)
        {
            string seeMoreText = page["SeeMoreText"] as string;
            string targetFrameString = (page["PageTargetFrame"] != null &&
                                        "1".Equals(page["PageTargetFrame"].ToString()))
                                           ? " target=\"_blank\""
                                           : string.Empty;

            if (seeMoreText != null)
            {
                string _linkTag = "<a href=\"{0}\" class=\"IconLink\" title=\"{1}\"{2}>{1}</a>";
                return string.Format(_linkTag, HttpUtility.HtmlEncode(page.LinkURL), seeMoreText, targetFrameString);
            }

            else if (PageLinkProperty != null)
            {
                string _linkTag = "<a href=\"{0}\" class=\"IconLink\" title=\"{1}\"{2}>{1}</a>";
                return string.Format(_linkTag, HttpUtility.HtmlEncode(page.LinkURL), GetPageTitle(page),
                                     targetFrameString);
            }
            return string.Empty;
        }

        /// <summary>
        /// Strips a text to a given length without splitting the last word.
        /// </summary>
        /// <param name="previewText">The string to shorten</param>
        /// <returns>A shortened version of the given string</returns>
        private string StripPreviewText(string previewText)
        {
            return previewText;
        }

        /// <summary>
        /// ShouldBeVisible
        /// </summary>
        /// <param name="page"></param>
        /// <returns>Tue/false</returns>
        protected bool ShouldBeVisible(PageData page)
        {
            return (page["ThumbnailImage"] != null);
        }
    }
}