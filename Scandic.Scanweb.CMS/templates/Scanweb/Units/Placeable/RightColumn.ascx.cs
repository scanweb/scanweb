//  Description					:   RightColumn                                           //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.CMS.SpecializedProperties;
using Scandic.Scanweb.CMS.Util;
using EPiServer;
using EPiServer.Core;
using ImageStoreNET.Classes.Data;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// RightColumn
    /// </summary>
    public partial class RightColumn : EPiServer.UserControlBase
    {
        protected UserControl BE;

        /// <summary>
        /// Page_Load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            rightColumnPromo1.HideBookNow = true;
            rightColumnPromo2.HideBookNow = true;
            rightColumnPromo3.HideBookNow = true;
            rightColumnPromo4.HideBookNow = true;
            PageData startPage = EPiServer.DataFactory.Instance.GetPage(PageReference.StartPage, EPiServer.Security.AccessLevel.NoAccess);
            searchHeader.InnerText = Convert.ToString(startPage["MeetingSearchModuleHeader"]);
            if (Convert.ToBoolean(CurrentPage["MiddleSearchModuleVisible"]))
                MeetingSearch226.Visible = false;

            BookingModulePlaceHolder.Visible = PropertyValueEqualsVisible("RightBookingModuleVisible");
            MeetingModulePlaceHolder.Visible = PropertyValueEqualsVisible("RightMeetingModuleVisible");
            RightInfoBox1PlaceHolder.Visible =
                (CurrentPage["RightInfoBoxHeading1"] != null || CurrentPage["RightInfoBoxBody1"] != null);
            RightInfoBox2PlaceHolder.Visible =
                (CurrentPage["RightInfoBoxHeading2"] != null || CurrentPage["RightInfoBoxBody2"] != null);

            BoxContainer1PlaceHolder.Visible = (CurrentPage["BoxContainer1"] != null);
            BoxContainer2PlaceHolder.Visible = (CurrentPage["BoxContainer2"] != null);
            BoxContainer3PlaceHolder.Visible = (CurrentPage["BoxContainer3"] != null);
            BoxContainer4PlaceHolder.Visible = (CurrentPage["BoxContainer4"] != null);

            RightDynamicTextBoxPlaceHolder.Visible = PropertyValueEqualsVisible("RightDynamicTextBoxVisible");

            BE.Visible = OWSVisibilityControl.BookingModuleShouldBeVisible;
            AlternativeBookingModule.Visible = !OWSVisibilityControl.BookingModuleShouldBeVisible;

            CheckIfRightColumntIsDisplay();
        }

        /// <summary>
        /// Check if the Right Column is displaying any control.
        /// </summary>
        private void CheckIfRightColumntIsDisplay()
        {
            if (BookingModulePlaceHolder.Visible || MeetingModulePlaceHolder.Visible ||
                RightInfoBox1PlaceHolder.Visible || RightInfoBox2PlaceHolder.Visible ||
                BoxContainer1PlaceHolder.Visible || BoxContainer2PlaceHolder.Visible ||
                BoxContainer3PlaceHolder.Visible || BoxContainer4PlaceHolder.Visible ||
                RightDynamicTextBoxPlaceHolder.Visible || BE.Visible || AlternativeBookingModule.Visible)
            {
                CMSSessionWrapper.RightColumnPresent = true;
            }
            else
            {
                CMSSessionWrapper.RightColumnPresent = false;
            }
        }

        /// <summary>
        /// GetCSS
        /// </summary>
        /// <returns></returns>
        protected string GetCSS()
        {
            if (PropertyValueEqualsVisible("RightColumnShiftUp")
                && (PropertyValueEqualsVisible("RightBookingModuleVisible") ||
                    PropertyValueEqualsVisible("RightMeetingModuleVisible")))
            {
                return "RightColumnShiftUp";
            }
            else
            {
                return "RightColumn";
            }
        }

        /// <summary>
        /// PropertyValueEqualsVisible
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns>True/false</returns>
        private bool PropertyValueEqualsVisible(string propertyName)
        {
            if (CurrentPage[propertyName] != null)
            {
                return ((int) CurrentPage[propertyName] == (int) SelectVisibility.Visibility.Visible);
            }
            else
            {
                return false;
            }
        }
    }
}