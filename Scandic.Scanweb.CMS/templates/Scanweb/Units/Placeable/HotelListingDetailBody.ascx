<%@ Control Language="C#" AutoEventWireup="true" Codebehind="HotelListingDetailBody.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.HotelListingDetailBody" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<div class="hotelDetails hotelListing">
    <h3 class="darkHeading">
        <strong id="counter" runat="server" class="hotelName">
        <span id="lblCounterValue" runat="server"></span>
        <a id="lnkHotelName" runat="server"></a></strong>
        <span id="lblDistance" class="hotelDistance" runat="server" ></span>
         
    </h3>
   
       <img src ="" alt="" id="imgHotel" runat="server"/>
       <p id="details" runat="server" class="details"></p>
      <a class="IconLink" id="lnkFulldescription" target="_top" runat="server"></a>
         
   
    
      <div class="hotelDetailsWrapper">
     <div class="addressWrapper">
            <div class="hotelAddress">
                <span id="address" runat="server"></span>
                
            </div>
           
              <div  class="actionBtn fltRt">
                <a href="#" class="jqModal buttonInner" onclick="javascript:populateDestination('<%= GetHotelName %>','<%= GetOperaId %>');return false;">
                 <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/CheckRates") %>
                 </a>
                 <a href="#" class="buttonRt scansprite" onclick="javascript:populateDestination('<%= GetHotelName %>','<%= GetOperaId %>');return false;">
                 </a>                 
                 
                 </div>
           
      </div> 
    </div>
</div>
