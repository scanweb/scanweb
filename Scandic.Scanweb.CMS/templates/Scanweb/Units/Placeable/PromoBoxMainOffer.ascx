<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PromoBoxMainOffer.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.PromoBoxMainOffer" %>

<div id="pboxOfferBig" style="background-image:url(<%= GetImageURL() %>); background-repeat:no-repeat;" onclick="<%=GetOfferPageLink()%>">
<div class="pboxStoolImgBig">
<asp:PlaceHolder ID="StoolImagePlaceHolder" Visible="false" runat="server">
    <asp:Image ID="StoolImage" runat="server"/>
</asp:PlaceHolder>
<h2 id="MainPromoOfferHeading" class="mainPromoboxHeading" runat="server" visible="false" ></h2>
</div>
<div class="pboxTextBig">
    <asp:PlaceHolder ID="PromoTextPlaceHolder" Visible="false" runat="server">
                <p><asp:Literal ID="PromoText" runat="server"></asp:Literal></p>
    </asp:PlaceHolder>
    <div class="priceRate">
        <asp:PlaceHolder ID="PriceTextPlaceHolder" Visible="false" runat="server">
              <asp:Literal ID="PriceText" runat="server"></asp:Literal>
        </asp:PlaceHolder>
    </div>
    <div class="bottomLinks">
        <asp:PlaceHolder ID="PageLinkPlaceHolder" Visible="false" runat="server">
              <asp:Literal ID="PageLink" runat="server"/>
        </asp:PlaceHolder>
    </div>
    <div class="mainOfferBox">
      <div class="actionBtn fltLft">
            <a class="buttonInner" title="Read More" id="ReadMoreLink1" runat="server" ><EPiServer:Translate Text="/Templates/Scanweb/Units/Static/StartpagePuffs/Readmore" runat="server" /></a>
            <a class="buttonRt scansprite" id="ReadMoreLink2" runat="server" ></a>
     </div>       
    </div>
</div>
</div> 
