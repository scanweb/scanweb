<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PopulateBookingModuleInput.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.PopulateBookingModuleInput" %>

<script type="text/javascript">
function SetBookingInputFieldValue(containerID, text){
var defaultText=$('.flyOutWrapper input').attr('rel');
var inputid=$('.flyOutWrapper input');
if($fn(_endsWith(containerID))!=null){
$fn(_endsWith(containerID)).value = text;
}else{
$(inputid).val(defaultText);
}

if($fn(_endsWith('selectedDestId'))!=null){
$fn(_endsWith('selectedDestId')).value = "";
}  
  
}

<% if (DestinationName != null)
   { %>SetBookingInputFieldValue('txtHotelName', '<%= DestinationName.Trim() %>'); <% } %>
//Vrushali | Res 2.0 | Hotel name is not getting selected in the smal booking module of the hotel landing page for the  for BC and Reward night booking
<% if (DestinationName != null)
   { %>SetBookingInputFieldValue('txtHotelNameBNC', '<%= DestinationName.Trim() %>'); <% } %>
<% if (DestinationName != null)
   { %>SetBookingInputFieldValue('txtHotelNameRewardNights', '<%= DestinationName.Trim() %>'); <% } %>

<% if (PromotionCode != null)
   { %>SetBookingInputFieldValue('txtRegularCode', '<%= PromotionCode.Trim() %>');<% } %>
</script>