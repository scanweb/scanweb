<%@ Control Language="C#" AutoEventWireup="true" Codebehind="HotelPageHotelInformation.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.HotelPageHotelInformation" %> 
<h3 class="darkHeading"><EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Pages/HotelLandingPage/Content/LocationInformation"
        runat="server" /></h3>
<div>
    <div>
    <asp:Literal ID="ltlAddress" runat="server" />
        </div>
    <div>
    <!-- artf863018 | Look and feel | Release 1.5.1 -->
         <!-- Archana | PostalCity Changes for XMLAPI-->
        <asp:Literal ID="ltlPostCode" runat="server" /><asp:Literal ID="ltlPostalCity" runat="server" /><asp:Literal ID="ltlCity" runat="server" />
    </div>  
    <div>
        <asp:Literal ID="ltlCountry" runat="server" />
    </div>
    <div class="hotelPhone">
        <asp:Literal ID="ltlPhone" runat="server" />
    </div>

    <div>
        <asp:Literal ID="ltlFax" runat="server" />
    </div>
    <!--R1.5 | artf812825 : Hotel contact info - GPS coordinates and email missing-->
    <div>
        <asp:Literal ID="ltlEmail" runat="server" />
        <div id="EmailInfo" class="HotelEmailLink" runat="server">
    </div>
    </div>
     <!--R1.8.2 | Central reservation Number added after Email information-->
     <asp:PlaceHolder ID="PhoneNumberPlaceHolder" runat="server">
        <div>
            <asp:Literal ID="ltlCentralReservationNumber" runat="server" />
        </div>
        <div>
            <asp:Literal ID="ltlCentralReservationNumberText" runat="server" />
        </div>
    </asp:PlaceHolder>
    <!-- START: Look and feel changes 1.5.1 | Hotel page(Facility Tab) -->
    <div class="hotelLatitude">
        <asp:Literal ID="GoeY" runat="server"></asp:Literal>
    </div>
    <div>
        <asp:Literal ID="GoeX" runat="server"></asp:Literal>
    </div>
    <!-- END: Look and feel changes 1.5.1 | Hotel page(Facility Tab)-->
</div>
