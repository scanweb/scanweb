//  Description					: NewsletterSubscription                                  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// Code behind of NewsletterSubscription control.
    /// </summary>
    public partial class NewsletterSubscription : EPiServer.UserControlBase
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            emailrequired.ErrorMessage =
                Translate("/Templates/Scanweb/Units/Placeable/NewsLetterSubscription/Emailrequired");
            emailvalidator.ErrorMessage =
                Translate("/Templates/Scanweb/Units/Placeable/NewsLetterSubscription/InvalidEmail");
            zipvalidator.ErrorMessage = Translate("/Templates/Scanweb/Units/Placeable/NewsLetterSubscription/ZipCode");
            countryvalidator.ErrorMessage =
                Translate("/Templates/Scanweb/Units/Placeable/NewsLetterSubscription/InvalidCountry");

            SetCountryOptions();
            SetInterestedInOptions();
        }

        /// <summary>
        /// Returns a string representing the correct action for the form
        /// </summary>
        /// <returns>Action as string</returns>
        protected string GetAction()
        {
            if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_SWEDISH))
                return ActionConstant.ACTION_SWEDISH;
            else if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_ENGLISH))
                return ActionConstant.ACTION_ENGLISH;
            else if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_FINNISH))
                return ActionConstant.ACTION_FINNISH;
            else if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_DANISH))
                return ActionConstant.ACTION_DANISH;
            else if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_NORWEGIAN))
                return ActionConstant.ACTION_NORWEGIAN;
            else if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_GERMANY))
                return ActionConstant.ACTION_GERMAN;
            else if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGAUGE_RUSSIAN))
                return ActionConstant.ACTION_RUSSIAN;
            else
                return ActionConstant.ACTION_ENGLISH;
        }

        /// <summary>
        /// Return the correct language for the form
        /// </summary>
        /// <returns>Language as string</returns>
        protected string GetLanguage()
        {
            if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_SWEDISH))
                return LanguageNameConstant.LANGUAGE_SWEDISH.ToLower();
            else if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_ENGLISH))
                return LanguageNameConstant.LANGUAGE_ENGLISH.ToLower();
            else if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_FINNISH))
                return LanguageNameConstant.LANGUAGE_FINNISH.ToLower();
            else if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_DANISH))
                return LanguageNameConstant.LANGUAGE_DANISH.ToLower();
            else if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_NORWEGIAN))
                return LanguageNameConstant.LANGUAGE_NORWEGIAN.ToLower();
            else if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGAUGE_RUSSIAN))
                return LanguageNameConstant.LANGUAGE_RUSSIA.ToLower();
            else if (CurrentPage.LanguageID.ToUpper().Equals(CurrentPageLanguageConstant.LANGUAGE_GERMANY))
                return LanguageNameConstant.LANGUAGE_GERMANY.ToLower();
            else
                return LanguageNameConstant.LANGUAGE_ENGLISH.ToLower();
        }

        /// <summary>
        /// SetCountryOptions
        /// </summary>
        private void SetCountryOptions()
        {
            string countryOptionsString = CurrentPage["CountryOptions"] as string;
            if (countryOptionsString != null)
            {
                string[] countryNameValuePairs = countryOptionsString.Split('\n');
                foreach (string countryNameValuePair in countryNameValuePairs)
                {
                    string[] nameValue = countryNameValuePair.Split(';');
                    if (nameValue.Length == 2)
                    {
                        string name = nameValue[0].Trim();
                        string value = nameValue[1].Trim();
                        country.Items.Add(new ListItem(name, value));
                    }
                }
            }
        }

        /// <summary>
        /// Button send event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSend_Click(object sender, EventArgs e)
        {
            HttpPost("http://www2.carmamail.com/Forms/Action.aspx?form=" + GetAction(), BuildParameters());
        }

        /// <summary>
        /// Build the parameter list
        /// </summary>
        /// <returns>Parameters as string</returns>
        private string BuildParameters()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("firstname=" + firstname.Text);
            sb.Append("&lastname=" + lastname.Text);
            sb.Append("&title=" + title.Text);
            sb.Append("&street=" + street.Text);
            sb.Append("&city=" + city.Text);
            sb.Append("&zipcode=" + zipcode.Text);
            sb.Append("&country=" + country.Value);
            sb.Append("&birthyear=" + birthyear.Text);
            if (male.Checked)
                sb.Append("&gender=" + "M");
            else if (female.Checked)
                sb.Append("&gender=" + "F");
            else
                sb.Append("&gender=" + string.Empty);
            sb.Append("&mobilenumber=" + mobilenumber.Text);
            sb.Append("&companyorganization=" + companyorganization.Text);
            sb.Append("&companyaddress=" + companyaddress.Text);
            sb.Append("&leisure=" + leisure.Checked);
            sb.Append("&meetingandconference=" + meetingandconference.Checked);
            sb.Append("&pressreleases=" + pressreleases.Checked);
            sb.Append("&corporateinformation=" + corporateinformation.Checked);
            sb.Append("&sitelanguage=" + GetLanguage());
            sb.Append("&email=" + email.Text);

            return sb.ToString();
        }

        /// <summary>
        /// Sends the message via HTTP Post and checks the response.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"> </param>
        protected void HttpPost(string url, string parameters)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            using (StreamWriter writer = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.Default))
            {
                writer.Write(parameters);
            }
            try
            {
                WebResponse response = request.GetResponse();

                string ResponseUri = response.ResponseUri.ToString();
                StreamReader responseStream = new StreamReader(response.GetResponseStream());
                string responseString = responseStream.ReadToEnd();
                responseStream.Close();


                if (ResponseUri.Contains("response=success"))
                {
                    ltStatus.Text = Translate("/Templates/Scanweb/Pages/Newsletter/subscribe/success");
                    HideInputControls();
                }
                else if (ResponseUri.Contains("response=validationerror"))
                    ltStatus.Text = Translate("/Templates/Scanweb/Pages/Newsletter/subscribe/validationerror");
                else
                    ltStatus.Text = Translate("/Templates/Scanweb/Pages/Newsletter/subscribe/error");
            }

            catch
            {
                ltStatus.Text = Translate("/Templates/Scanweb/Pages/Newsletter/subscribe/error");
            }
        }

        /// <summary>
        /// this method will make all the input controls invisible
        /// </summary>
        private void HideInputControls()
        {
            ColumnOne.Visible = false;
            ColumnTwo.Visible = false;
            divEmail.Visible = false;
        }

        /// <summary>
        /// this method will show only those interested-in options as set in the CMS
        /// </summary>
        private void SetInterestedInOptions()
        {
            leisureDiv.Visible = !(CurrentPage["HideLeisureOption"] != null);
            meetingandconferenceDiv.Visible = !(CurrentPage["HideMeetingandconferenceOption"] != null);
            pressreleasesDiv.Visible = !(CurrentPage["HidePressreleasesOption"] != null);
            corporateinformationDiv.Visible = !(CurrentPage["HideCorporateinformationOption"] != null);
        }
    }
}