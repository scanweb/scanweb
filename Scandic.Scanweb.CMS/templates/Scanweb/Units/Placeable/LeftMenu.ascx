<%@ Control Language="C#" EnableViewState="false    " AutoEventWireup="true" CodeBehind="LeftMenu.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Public.Units.LeftMenu" %>
<div id="LeftMenuDiv">
    <EPiServer:PageTree ShowRootPage="false" EvaluateHasChildren="true" runat="server" id="LeftMenuCtrl">
    
        <IndentTemplate>
	        <ul>
        </IndentTemplate>
    	
        <TopTemplate>
            <span class="TopLinkTop"></span>
            <span class="TopLink"><EPiServer:Property ID="Property1" PropertyName="PageLink" title="<%# Container.CurrentPage.PageName %>" runat="server" /></span>
            <span class="TopLinkBottom"></span>
        </TopTemplate>
        
        <SelectedTopTemplate>
              <span class="SelectedTopLinkTop"></span>
              <span class="SelectedTopLink Selected"><%# Container.CurrentPage.PageName %></span> 
              <span class="SelectedTopLinkBottom"></span>
        </SelectedTopTemplate>
        
        <ExpandedTopTemplate>
            <span class="SelectedTopLinkTop"></span>
            <span class="SelectedTopLink"><EPiServer:Property ID="Property1" PropertyName="PageLink" title="<%# Container.CurrentPage.PageName %>" runat="server" /></span>
            <span class="SelectedTopLinkBottom"></span>
        </ExpandedTopTemplate>
        
        <SelectedExpandedTopTemplate>
            <span class="SelectedTopLinkTop"></span>
            <span class="SelectedTopLink Selected"><%# Container.CurrentPage.PageName %></span> 
            <span class="SelectedTopLinkBottom"></span>
        </SelectedExpandedTopTemplate>
        
        <ItemHeaderTemplate>
		        <li>
        </ItemHeaderTemplate>
    		
        <ItemTemplate>
			        <span class="SubLevelItem ItemLink"><EPiServer:Property ID="Property1" PropertyName="PageLink" title="<%# Container.CurrentPage.PageName %>" runat="server" /></span>
        </ItemTemplate>
        
        <ExpandedItemTemplate>
                    <span class="SubLevelItem ItemLink"><EPiServer:Property ID="Property1" PropertyName="PageLink" title="<%# Container.CurrentPage.PageName %>" runat="server" /></span>
        </ExpandedItemTemplate>
    	
        <SelectedItemTemplate>
			        <span class="SubLevelItem Selected"><%# Container.CurrentPage.PageName %></span>
        </SelectedItemTemplate>
   
        <SelectedExpandedItemTemplate>
                    <span class="SubLevelItem Selected"><%# Container.CurrentPage.PageName %></span>
        </SelectedExpandedItemTemplate> 	
        
        <ItemFooterTemplate>                
		        </li>		        
        </ItemFooterTemplate>
            	
        <UnindentTemplate>
	        </ul>
        </UnindentTemplate>
    </EPiServer:PageTree>
</div>