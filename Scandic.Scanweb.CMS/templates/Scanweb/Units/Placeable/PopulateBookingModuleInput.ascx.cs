//  Description					: PopulateBookingModuleInput                              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// Used to generate a javascript that pre-populates the booking module if available
    /// </summary>
    public partial class PopulateBookingModuleInput : EPiServer.UserControlBase
    {
        /// <summary>
        /// Text in the destination input box
        /// </summary>
        public string DestinationName { get; set; }

        /// <summary>
        /// Text in the promotion code input box
        /// </summary>
        public string PromotionCode { get; set; }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Visible = PromotionCode != null || DestinationName != null;
        }
    }
}