//  Description					: MoreImagesLink                                          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.Util.ImageVault;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// Code behind of MoreImagesLink page.
    /// </summary>
    public partial class MoreImagesLink : ScandicUserControlBase
    {
        /// <summary>
        /// Gets more images url. 
        /// </summary>
        /// <returns>more images url. </returns>
        protected string GetMoreImagesURL()
        {
            PageReference moreImagesPopUpLink = RootPage["MoreImagesPopUp"] as PageReference;
            PageData moreImagesPopupPage = DataFactory.Instance.GetPage(moreImagesPopUpLink,
                                                                        EPiServer.Security.AccessLevel.NoAccess);
            string moreImagesURL = GetFriendlyURLToPage(moreImagesPopUpLink, moreImagesPopupPage.LinkURL);
            moreImagesURL = UriSupport.AddQueryString(moreImagesURL, "hotelid", CurrentPage.PageLink.ID.ToString());
            return UriSupport.AddQueryString(moreImagesURL, "popuptype", "images");
        }

        /// <summary>
        /// Populates query string.
        /// </summary>
        /// <param name="query"></param>
        public void PopulateByQueryString(string query)
        {
            switch (query)
            {
                case "overview":
                    {
                        MoreImagesLinkPlaceHolder.Visible = true;
                    }
                    break;
                case "location":
                    {
                        MoreImagesLinkPlaceHolder.Visible = true;
                    }
                    break;
                case "facilities":
                    {
                        MoreImagesLinkPlaceHolder.Visible = true;
                    }
                    break;

                default:
                    {
                        MoreImagesLinkPlaceHolder.Visible = true;
                    }
                    break;
            }
        }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            PageData pd = DataFactory.Instance.GetPage(CurrentPage.PageLink);
            bool showMoreImagesLink = Utils.IsNoImagesForPopUp(pd);
            if (showMoreImagesLink == true)
                MoreImagesLinkPlaceHolder.Visible = true;
            else
                MoreImagesLinkPlaceHolder.Visible = false;
        }
    }
}