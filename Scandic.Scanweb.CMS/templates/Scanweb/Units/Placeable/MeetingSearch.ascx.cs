using System;
using System.Configuration;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Filters;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// MeetingSearch
    /// </summary>
    public partial class MeetingSearch : EPiServer.UserControlBase
    {
        /// <summary>
        /// Delegate for meeting search
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">EventArgs for meeting search</param>
        public delegate void MeetingSearchEventHandler(object sender, MeetingSearchEventArgs e);

        /// <summary>
        /// Event for meeting search
        /// </summary>
        public event MeetingSearchEventHandler SearchMeeting;

        public PageData MeetingSearchPage { get; set; }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["SelectHotelID"] != null)
            {
                SetPageIndexForSEO();
            }
            PageReference prfMeetingSearchPage = CurrentPage["MeetingSearchPage"] as PageReference ?? null;
            MeetingSearchPage = (prfMeetingSearchPage != null)
                                    ? DataFactory.Instance.GetPage(prfMeetingSearchPage,
                                                                   EPiServer.Security.AccessLevel.NoAccess)
                                    : null;
            PageData MeetingSearchDefaultValue = null;
            if (CurrentPage != null && CurrentPage["MeetingSearchDefaultCountry"] != null)
            {
                MeetingSearchDefaultValue =
                    DataFactory.Instance.GetPage(CurrentPage["MeetingSearchDefaultCountry"] as PageReference);
            }

            searchError.Value = LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Placeable/MeetingSearch/SearchError");
            participantError.Value = LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Placeable/MeetingSearch/SearchErrorParticipants");
            //txtdestName.Value = LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Placeable/MeetingSearch/EventPlace");
            //txtdestName.Attributes.Add("rel", LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Placeable/MeetingSearch/EventPlace"));
            //txtParticipants.Value = LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Placeable/MeetingSearch/MinimumNumberOfParticipants");
            //txtParticipants.Attributes.Add("rel", LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Placeable/MeetingSearch/MinimumNumberOfParticipants"));
        }

        /// <summary>
        /// btnSearch_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, System.EventArgs e)
        {
            if (MiscellaneousSessionWrapper.SearchedMeetingRoomsPDC != null)
                Session.Remove("SearchedMeetingRoomsPDC");

            if (MiscellaneousSessionWrapper.SearchedCityCountryPage != null)
                Session.Remove("SearchedCityCountryPage");

            if (MiscellaneousSessionWrapper.SearchedCapacity != null)
                Session.Remove("SearchedCapacity");

            if (MiscellaneousSessionWrapper.SearchedCountryPage != null)
                Session.Remove("SearchedCountryPage");

            int selectedPageLinkID = 0;
            int selectedPageCountryLinkID = 0;
            AvailabilityController _availabilityController = new AvailabilityController();
            System.Collections.Generic.List<CountryDestinatination> countryCityDestination = _availabilityController.FetchAllCountryDestinationsForAutoSuggest(false, false);
            string[] selectedCode = selectedDestIdFGP.Value.Split(':');
            switch (selectedCode[0].ToString().ToLower())
            {
                case "country":
                    CountryDestinatination countryDestination1 = countryCityDestination.Find(delegate(CountryDestinatination countryDestination2)
                    { return countryDestination2.CountryCode == selectedCode[1].ToString(); });
                    selectedPageLinkID = Convert.ToInt32(countryDestination1.CountryID);
                    selectedPageCountryLinkID = Convert.ToInt32(countryDestination1.CountryID);
                    break;
                case "city":
                    foreach (CountryDestinatination item in countryCityDestination)
                    {
                        CityDestination city = item.SearchedCities.Find(delegate(CityDestination cityDestination)
                        { return cityDestination.OperaDestinationId == selectedCode[1]; });
                        if (city != null && city.OperaDestinationId == selectedCode[1])
                        {
                            selectedPageLinkID = city.CityID;
                            selectedPageCountryLinkID = Convert.ToInt32(item.CountryID);
                            break;
                        }
                    }
                    break;
                case "hotel":
                    foreach (CountryDestinatination country in countryCityDestination)
                    {
                        bool countryBreak = false;
                        foreach (CityDestination city in country.SearchedCities)
                        {
                            HotelDestination hotel = city.SearchedHotels.Find(delegate(HotelDestination hotelDestination)
                            { return hotelDestination.OperaDestinationId == selectedCode[1]; });
                            if (hotel != null && hotel.OperaDestinationId == selectedCode[1])
                            {
                                selectedPageLinkID = Convert.ToInt32(hotel.Id);
                                selectedPageCountryLinkID = Convert.ToInt32(country.CountryID);
                                countryBreak = true;
                                break;
                            }
                        }
                        if (countryBreak)
                            break;
                    }
                    break;
            }

            if (selectedPageLinkID != 0)
            {
                PageData CityCountrySearchPage = GetPage(new PageReference(selectedPageLinkID));
                PageData countrySearchPage = GetPage(new PageReference(selectedPageCountryLinkID));
                SetCountryCurrency(CityCountrySearchPage);
                string minNumberOfParticipants = LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Placeable/MeetingSearch/MinimumNumberOfParticipants");
                int Capacity = default(int);

                if (txtParticipants.Value != null && txtParticipants.Value != string.Empty && txtParticipants.Value != minNumberOfParticipants)
                    Capacity = int.Parse(txtParticipants.Value);

                if ((CurrentPage != MeetingSearchPage) && (MeetingSearchPage != null))
                {
                    MiscellaneousSessionWrapper.SearchedCityCountryPage = CityCountrySearchPage;
                    MiscellaneousSessionWrapper.SearchedCountryPage = countrySearchPage;
                    MiscellaneousSessionWrapper.SearchedCapacity = Capacity;
                    Response.Redirect(MeetingSearchPage.LinkURL);
                }
                else
                {
                    MeetingSearchEventArgs mSearchEventArgs = new MeetingSearchEventArgs();
                    mSearchEventArgs.Capacity = Capacity;
                    mSearchEventArgs.CityCountrySearchPage = CityCountrySearchPage;
                    mSearchEventArgs.CountrySearchPage = countrySearchPage;
                    OnMeetingSearched(mSearchEventArgs);
                }
            }

            if (selectedPageLinkID == 0)
            {
                MeetingSearchEventArgs mSearchEventArgs = new MeetingSearchEventArgs();
                OnMeetingSearched(mSearchEventArgs);
                if ((CurrentPage != MeetingSearchPage) && (MeetingSearchPage != null))
                    Response.Redirect(MeetingSearchPage.LinkURL);
            }
            txtParticipants.Value = null;
            txtdestName.Value = null;
            selectedDestIdFGP.Value = string.Empty;
        }

        private void SetCountryCurrency(PageData pageData)
        {
            int cityPageTypeID = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.CityPageTypeID]);
            int hotelPageTypeID = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.HotelPageTypeID]);
            if (pageData.PageTypeID == hotelPageTypeID)
            {
                PageData cityPage = GetPage(pageData.ParentLink);
                PageData countryPage = GetPage(cityPage.ParentLink);
                MiscellaneousSessionWrapper.CountryCurrency = Convert.ToString(countryPage["CountryCurrency"]);
            }
            else if (pageData.PageTypeID == cityPageTypeID)
            {
                PageData countryPage = GetPage(pageData.ParentLink);
                MiscellaneousSessionWrapper.CountryCurrency = Convert.ToString(countryPage["CountryCurrency"]);
            }
            else
                MiscellaneousSessionWrapper.CountryCurrency = Convert.ToString(pageData["CountryCurrency"]);
        }

        /// <summary>
        /// OnMeetingSearched
        /// </summary>
        /// <param name="args"></param>
        protected virtual void OnMeetingSearched(MeetingSearchEventArgs args)
        {
            if (SearchMeeting != null)
            {
                SearchMeeting(this, args);
            }
        }

        /// <summary>
        /// Eventargs for Meeting search
        /// </summary>
        public class MeetingSearchEventArgs : EventArgs
        {
            public PageData CityCountrySearchPage { get; set; }
            public PageData CountrySearchPage { get; set; }
            public int Capacity { get; set; }
        }

        protected string IndexPageForSEO
        {
            get
            {
                return "noindex";
            }
        }

        /// <summary>
        /// SetPageIndexForSEO
        /// </summary>
        protected void SetPageIndexForSEO()
        {
            HtmlMeta meta = new HtmlMeta();
            meta.Name = "robots";
            meta.Content = IndexPageForSEO;
            bool flag = false;
            UserControl ucHeader = Page.Master.FindControl("ucHeader") as UserControl;

            PlaceHolder metaContentPlaceHolder = null;
            if (ucHeader != null)
            {
                metaContentPlaceHolder =
                  (PlaceHolder)ucHeader.FindControl("plhMetaDataArea");
                if (metaContentPlaceHolder != null && metaContentPlaceHolder.Controls != null
                    && metaContentPlaceHolder.Controls.Count > 0)
                {
                    foreach (Control ctrl in metaContentPlaceHolder.Controls)
                    {
                        if (ctrl.GetType() == typeof(HtmlMeta))
                        {
                            HtmlMeta existedMeta = (HtmlMeta)ctrl;
                            if (string.Equals(existedMeta.Name, "robots"))// && string.Equals(existedMeta.Content,"noindex"))
                            {
                                if (!string.Equals(existedMeta.Content, "noindex", StringComparison.InvariantCultureIgnoreCase))
                                    existedMeta.Content = "noindex";
                                flag = true;
                                break;
                            }
                        }
                    }
                }
            }
            if (!flag)
                this.Page.Header.Controls.Add(meta);
        }

    }
}