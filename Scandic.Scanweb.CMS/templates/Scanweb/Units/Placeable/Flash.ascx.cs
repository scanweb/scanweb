////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  Flash                                                 //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using EPiServer;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// Gets a flash animation from the page property "EmbeddedURL" and sets the
    /// the width and height of the animation based on the "EmbeddedObjectWidth" and 
    /// "EmbeddedObjectHeight" page properties.
    /// </summary>
    public partial class Flash : UserControlBase
    {
    }
}