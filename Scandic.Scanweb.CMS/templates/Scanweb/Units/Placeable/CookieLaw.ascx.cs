﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EPiServer;
using Scandic.Scanweb.BookingEngine.Web;
using EPiServer.Core;
using System.Text.RegularExpressions;
namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable
{
    public partial class CookieLaw : System.Web.UI.UserControl
    {
        /// <summary>
        /// Page load event hadndler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            cookieLawOkayLnk.InnerHtml = EPiServer.Core.LanguageManager.Instance.Translate("/bookingengine/booking/ManageCreditCard/CCSavedOK");
            PageData page = DataFactory.Instance.GetPage(PageReference.StartPage);
            if (page.Property["CookieLawMessage"] != null)
            {
                cookieLawMessageDiv.InnerHtml = Convert.ToString(page.Property["CookieLawMessage"]).Trim();
                string cookieMessage = HttpUtility.HtmlDecode(cookieLawMessageDiv.InnerText);
                if (!IsNullOrWhiteSpace(cookieMessage))
                    cookieLawMessageDiv.InnerHtml = cookieMessage;
                else
                {
                    cookieLawMessageDiv.InnerHtml = string.Empty;
                    CookieWrapper.Visible = false;
                }
            }
            if (!Convert.ToBoolean(page.Property["IsCookieAllowed"].Value))
            {
                if (Request.Cookies.Get("cookieLaw") == null)
                {
                    HttpCookie displayCookie = new HttpCookie("cookieLaw", "false");
                    Response.Cookies.Add(displayCookie);
                }
            }
        }
    
        private bool IsNullOrWhiteSpace(string value)
        {
            return String.IsNullOrEmpty(value) || value.Trim().Length == 0;
        }
    }
}