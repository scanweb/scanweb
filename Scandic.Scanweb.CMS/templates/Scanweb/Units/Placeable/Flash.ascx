<%@ Control Language="C#" AutoEventWireup="False" CodeBehind="Flash.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.Flash" %>
<object type="application/x-shockwave-flash" data="<%= CurrentPage["EmbeddedFlashURL"] %>" 
    width="<%= CurrentPage["EmbeddedFlashWidth"] %>"
    height="<%= CurrentPage["EmbeddedFlashHeight"] %>">
    <param name="movie" value="<%= CurrentPage["EmbeddedFlashURL"] %>" />
</object>