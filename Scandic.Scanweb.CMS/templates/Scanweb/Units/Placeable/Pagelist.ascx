<%@ Control Language="C#" AutoEventWireup="False" CodeBehind="PageList.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.PageList" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>

<EPiServer:PageList id="advancedPageList" runat="server" Paging="true">
	<HeaderTemplate>
		<div class="PageList">		    
	</HeaderTemplate>
	<ItemTemplate>
	    <div class="PageListItemContainer">	        
		    <h3 class="darkHeading">
		        <%# GetListHeadline(Container.CurrentPage) %>
		    </h3>		    
		    <div class="PageListItemContentContainer">
		         <div class="itemText">
		           <Scanweb:SquaredCornerImage ID="SquaredCornerImage1" ImagePropertyName="ThumbnailImage"
		                         ContentPage="<%# Container.CurrentPage %>"
                                 ImageCssClass="itemTextImage" 
                                 ImageWidth="226"
                                 Visible="<%# ShouldBeVisible(Container.CurrentPage) %>" 
                                 runat="server"/>
                                 <p><%# GetPreviewText(Container.CurrentPage) %>
					   
						<asp:PlaceHolder ID="PlaceHolder1" Visible="<%# ShowOrHidePH(Container.CurrentPage) %>" runat="server">            
		             	<asp:PlaceHolder ID="PlaceHolder2" Visible="<%# ShowItemLink %>" runat="server">
		                	<div class="PageListLink"><%# GetContainerLink(Container.CurrentPage) %></div>
		             	</asp:PlaceHolder>
                    	</asp:PlaceHolder></p>
                                 
  		            
  		             
		          </div>
		     </div>		    
		</div>
	</ItemTemplate>	
	<FooterTemplate>			
		</div>		
	</FooterTemplate>
	
</EPiServer:PageList>

    