using System;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable
{
    /// <summary>
    /// This class encapsulate the Main Promo Box functionality
    /// </summary>
    public partial class PromoBoxMainOffer : ScandicUserControlBase
    {
        /// <summary>
        /// Page Load Method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            PageData offerPageData = GetOfferPageData();
            if (offerPageData != null)
            {
                string readMoreLinksTitle = WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/Box/BookOfferText");
                ReadMoreLink1.Title = readMoreLinksTitle;
                ReadMoreLink2.Title = readMoreLinksTitle;
                bool read = offerPageData.QueryDistinctAccess(EPiServer.Security.AccessLevel.Read);
                bool published = offerPageData.CheckPublishedStatus(PagePublishedStatus.Published);
                if (read == false || published == false || (offerPageData["OfferEndTime"] != null && Convert.ToDateTime(offerPageData["OfferEndTime"]) < DateTime.Now))
                {
                    this.Visible = false;
                }
                else
                {
                    PopulatePromoBoxMainOffer(offerPageData);
                }
            }
            else
            {
                this.Visible = false;
            }
        }

        #region Properties

        /// <summary>
        /// Main Promo image property name
        /// </summary>
        private string imagePropertyName = "MainBoxImage";

        public string ImagePropertyName
        {
            get { return imagePropertyName; }
            set { imagePropertyName = value; }
        }

        /// <summary>
        /// Stool image property name
        /// </summary>
        private string stoolImagePropertyName = "MainBoxStoolImage";

        public string StoolImagePropertyName
        {
            get { return stoolImagePropertyName; }
            set { stoolImagePropertyName = value; }
        }

        /// <summary>
        /// Main promo text property name
        /// </summary>
        private string promoTextPropertyName = "MainBoxContent";

        public string PromoTextPropertyName
        {
            get { return promoTextPropertyName; }
            set { promoTextPropertyName = value; }
        }

        /// <summary>
        /// Main promo price text property name
        /// </summary>
        private string priceTextPropertyName = "MainPromoBoxPriceText";

        public string PriceTextPropertyName
        {
            get { return priceTextPropertyName; }
            set { priceTextPropertyName = value; }
        }

        /// <summary>
        /// Main promo link text property name
        /// </summary>
        private string linkTextPropertyName = "MainBoxLinkText";

        public string LinkTextPropertyName
        {
            get { return linkTextPropertyName; }
            set { linkTextPropertyName = value; }
        }

        /// <summary>
        /// Main offer page link property name
        /// </summary>
        private string offerPageLinkPropertyName;

        public string OfferPageLinkPropertyName
        {
            get { return offerPageLinkPropertyName; }
            set { offerPageLinkPropertyName = value; }
        }

        /// <summary>
        /// Main offer page link
        /// </summary>
        private PageData offerPageLink;

        public PageData OfferPageLink
        {
            get { return offerPageLink; }
            set { offerPageLink = value; }
        }

        /// <summary>
        /// Booking link text
        /// </summary>
        //public string bookingLinkText =
        //    LanguageManager.Instance.Translate("/Templates/Scanweb/Units/Placeable/Box/BookOfferText");

        public string BookingLinkTextPropertyName { get; set; }


        /// <summary>
        /// Image maximum width
        /// </summary>
        private int imageMaxWidth;

        public int ImageMaxWidth
        {
            get { return imageMaxWidth; }
            set { imageMaxWidth = value; }
        }

        public int StoolImageMaxWidth { get; set; }

        #endregion Properties

        #region Private Methods

        protected string GetOfferPageLink()
        {
            string contentName = string.Empty;
            if (OfferPageLink["BoxHeading"] != null)
                contentName = Convert.ToString(OfferPageLink["BoxHeading"]);
            var trackingMethod = string.Format("trackPromobox(this,'{0}');", contentName);
            return string.Format("{0}javascript:window.location.href = '{1}{2}", trackingMethod, OfferPageLink.LinkURL, "';return false");
        }

        /// <summary>
        /// Populate the promo box main offer
        /// </summary>
        /// <param name="offerPageData">Offer page</param>
        private void PopulatePromoBoxMainOffer(PageData offerPageData)
        {
            string mainImageUrl = GetImageURL();

            if (mainImageUrl != string.Empty )
            {
                PageData pageData = GetPage(PageReference.RootPage);
                if (CurrentPage.PageLink.ID == ((PageReference)pageData["OfferOverviewPage"]).ID)
                {
                    MainPromoOfferHeading.Visible = true;
                    MainPromoOfferHeading.InnerText = Convert.ToString(offerPageData["MainPromoHeading"]);
                }
                else if (offerPageData[stoolImagePropertyName] != null)
                {
                    string imageString = offerPageData[stoolImagePropertyName] as string;
                    StoolImage.ImageUrl = WebUtil.GetImageVaultImageUrl(imageString, imageMaxWidth);                    
                    StoolImagePlaceHolder.Visible = true;
                }
                string promoText = offerPageData[PromoTextPropertyName] as string;
                if (promoText != null)
                {
                    PromoText.Text = promoText;
                    PromoTextPlaceHolder.Visible = true;
                }
                string priceText = offerPageData[PriceTextPropertyName] as string;
                if (priceText != null)
                {
                    PriceText.Text = priceText;
                    PriceTextPlaceHolder.Visible = true;
                }
                ReadMoreLink1.HRef = offerPageData.LinkURL;
                ReadMoreLink2.HRef = offerPageData.LinkURL;
            }
            else
            {
                this.Visible = false;
            }
        }

        /// <summary>
        /// Get Offer page
        /// </summary>
        /// <returns>Offer page</returns>
        private PageData GetOfferPageData()
        {
            if (offerPageLink == null)
            {
                PageReference offerPageReference = CurrentPage[offerPageLinkPropertyName] as PageReference;
                if (offerPageReference != null)
                {
                    offerPageLink =
                        DataFactory.Instance.GetPage(offerPageReference, EPiServer.Security.AccessLevel.NoAccess);
                }
            }
            return offerPageLink;
        }

        #endregion Private Methods

        #region Protected Methods

        /// <summary>
        /// Get the image url
        /// </summary>
        /// <returns>Image url</returns>
        protected string GetImageURL()
        {
            string imgStr = string.Empty;
            PageData offerPageData = GetOfferPageData();
            if (offerPageData != null)
            {
                if (offerPageData[imagePropertyName] != null)
                {
                    string imageString = offerPageData[imagePropertyName] as string;
                    imgStr = WebUtil.GetImageVaultImageUrl(imageString, imageMaxWidth);                       
                }
            }
            return imgStr;
        }           

        #endregion Protected Methods

        #region Public Methods

        /// <summary>
        /// Get Link Text
        /// </summary>
        /// <param name="pageData"></param>
        /// <returns>Link Text</returns>
        public string GetLinkText(PageData pageData)
        {
            return (pageData[linkTextPropertyName] as string ?? string.Empty);
        }

        /// <summary>
        /// Get Link Url
        /// </summary>
        /// <returns> Link Url</returns>
        public string GetLinkUrl()
        {
            string url = string.Empty;
            if (offerPageLink != null)
            {
                url = offerPageLink.LinkURL;
            }
            return url;
        }

        #endregion Public Methods
    }
}