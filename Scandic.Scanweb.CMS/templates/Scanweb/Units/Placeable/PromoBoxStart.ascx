<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PromoBoxStart.ascx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.PromoBoxStart" %>
<div class="<%= GetBackgroundColourCSS() %>" onclick="<%=GetOfferPageLink()%>">
    <div class="smallPBoxImage">
        <asp:PlaceHolder ID="ImagePlaceHolder" Visible="false" runat="server">
            <asp:Image ID="BoxImage" runat="server" />
        </asp:PlaceHolder>
    </div>
    <h2 id="PriceRate" class="priceRate" runat="server">
        <asp:PlaceHolder ID="PriceTextPlaceHolder" Visible="false" runat="server">
            <asp:Literal ID="PriceText" runat="server"></asp:Literal>
        </asp:PlaceHolder>
    </h2>
    <div class="bottomLinks">
        <asp:PlaceHolder ID="PageLinkPlaceHolder1" Visible="false" runat="server">
            <asp:Literal ID="PageLink1" runat="server" />
        </asp:PlaceHolder>
    </div>
</div>
