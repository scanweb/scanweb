﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable
{
    public partial class HotelListingCityDetailBody : System.Web.UI.UserControl
    {
        private string hotel;
        //private string hotelOperaId;
        public int TALocationID { get; set; }
        public bool HideCityTARatings { get; set; }
        public bool HideHotelTARatings { get; set; }
        private string HotelReviewCount = string.Empty;
        private string HotelRatingsCount = string.Empty;
        public Address HotelAddress { get; set; }
        public string HotelTelephone { get; set; }
        public Point GeoPoints { get; set; }
        #region Properties

        /// <summary>
        /// Sets the property for the hotel name displayed on the control
        /// </summary>
        /// public string CounterValue
        /// {
        /// set { lblCounterValue.InnerText = value; }
        /// }

        /// <summary>
        /// Sets the the path for the Hotel Image displayed
        /// </summary>
        public string ImageURL
        {
            set { imgHotel.Src = value; }
        }

        /// <summary>
        /// Stores the distance from city centre
        /// </summary>
        public string CityCenterDistance
        {
            set { lblDistance.InnerText = value; }
        }
        /// <summary>
        /// Gets HotelID
        /// </summary>
        protected string hotelOperaId { get; private set; }
        /// <summary>
        /// Stores address
        /// </summary>
        public string Address
        {
            set { address.InnerText = value; }
        }

        /// <summary>
        /// Stores Details
        /// </summary>
        public string Details
        {
            set { details.InnerText = value; }
        }

        /// <summary>
        /// Hotel URL
        /// </summary>
        public string HotelURL
        {
            set { lnkFulldescription.HRef = value; }
        }

        ///  public string HotelNameURL
        ///  {
        ///      set { lnkHotelName.HRef = value; }
        ///  }

        /// <summary>
        /// Stores the HotelListing Display object
        /// </summary>
        public IHotelListingDisplayInformation Hotel
        {
            set
            {
                imgHotel.Src = value.ImageSrc;
                imgHotel.Alt = value.ImageAltText;
                address.HRef = value.HotelURL;
                address.InnerText = value.Address;
                details.InnerText = value.Description;
                lnkFulldescription.HRef = value.HotelURL;

                lnkFulldescription.InnerText =
                    WebUtil.GetTranslatedText
                        ("/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/FullDescription");
                lnkImageGallery.Attributes.Add("rel",
                                               GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") +
                                               "?methodToCall=GetImageGallery&HotelID=" + value.ID + "&HotelName=" +
                                               value.HotelName);
                lnkHotelName.InnerText = value.HotelName;
                hotel = value.HotelName;
                hotelOperaId = value.ID;
                // lnkHotelName.HRef = value.HotelURL;
                /// lblCounterValue.InnerText =
                ///     value.Counter.ToString() + AppConstants.DOT.ToString() + AppConstants.SPACE.ToString();

                if (null != value.Distance && value.Distance != string.Empty)
                    lblDistance.InnerHtml =
                        value.Distance + AppConstants.SPACE +
                        WebUtil.GetTranslatedText
                            ("/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/KmToCityCentre");
                TALocationID = value.TALocationID;

                HideCityTARatings = value.HideCityTARatings;
                HideHotelTARatings = value.HideHotelTARatings;
            }
        }

        /// <summary>
        /// Gets the hotel name.
        /// </summary>
        protected string GetHotelName
        {
            get { return hotel; }
        }

        protected string GetOperaId
        {
            get { return string.IsNullOrEmpty(hotelOperaId) ? string.Empty : "HOTEL:" + hotelOperaId; }
        }

        #endregion Properties

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!WebUtil.HideTARating(false, this.HideCityTARatings))
                if (!WebUtil.HideTARating(false, this.HideHotelTARatings))
                    divTripAdvisorRating.InnerHtml = WebUtil.CreateTripAdvisorRatingWidget(Convert.ToInt32(TALocationID));
                else
                    divTripAdvisorRating.InnerHtml = "";
            else
                divTripAdvisorRating.InnerHtml = "";


            PlaceHolder phMetaForSchemaOrgHotelURL = (PlaceHolder)this.FindControl("phMetaForSchemaOrgHotelURL");
            if (phMetaForSchemaOrgHotelURL != null)
            {
                AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelURL, "url", lnkFulldescription.HRef);
            }
            if (HotelAddress != null)
            {
                PlaceHolder phMetaForSchemaOrgHotelAddress = (PlaceHolder)this.FindControl("phMetaForSchemaOrgHotelAddress");
                if (phMetaForSchemaOrgHotelAddress != null)
                {
                    AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelAddress, "streetAddress", HotelAddress.StreetAddress);
                    AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelAddress, "telephone", HotelTelephone);
                    AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelAddress, "addressLocality", HotelAddress.City);
                    AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelAddress, "postalCode", HotelAddress.PostCode);
                    AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelAddress, "addressCountry", HotelAddress.Country);
                }
            }
            if (GeoPoints != null)
            {
                PlaceHolder phMetaForSchemaOrgHotelGeoCoords = (PlaceHolder)this.FindControl("phMetaForSchemaOrgHotelGeoCoords");
                if (phMetaForSchemaOrgHotelGeoCoords != null)
                {
                    AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelGeoCoords, "latitude", Convert.ToString(GeoPoints.Y));
                    AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelGeoCoords, "longitude", Convert.ToString(GeoPoints.X));
                }
            }
            
        }
        private void AddSchemaOrgHotelMetaTags(PlaceHolder metaContentPlaceHolder, string itemPropval, string contentValue)
        {
            if (metaContentPlaceHolder != null)
            {
                Utility.AddMetaInfoForSchemaOrg(metaContentPlaceHolder, "itemprop", itemPropval, contentValue);
            }
        }
    }
}