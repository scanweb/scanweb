////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  BookingModuleAlternativeBox                            //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// BookingModuleAlternativeBox
    /// </summary>
    public partial class BookingModuleAlternativeBox : EPiServer.UserControlBase
    {
        /// <summary>
        /// Gets/Sets CssClass
        /// </summary>
        public string CssClass { get; set; }
    }
}