<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RoundedCornersImage.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Units.Placeable.RoundedCornersImage" %>

<div class="<%=TopCssClass%>">
</div>
<div class="<%=ImageCssClass%>">
    <img src="<%=GetImageURL()%>" alt="<%=GetAltText()%>" />
</div>
<div class="<%=BottomCssClass%>">
</div>
