using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.CMS.Util.ImageVault;
using ImageStoreNET.Developer;
using ImageStoreNET.Developer.Core;
using ImageVault.EPiServer5;
using ImageStoreNET.Classes.Util;

namespace Scandic.Scanweb.CMS.Templates.Units.Placeable
{
    /// <summary>
    /// RoundedCornersImage
    /// </summary>
    public partial class RoundedCornersImage : EPiServer.UserControlBase
    {
        private string imagePropertyName;
        private string topCssClass;
        private string imageCssClass;
        private string bottomCssClass;
        private int imageWidth;
        private PageData contentPage;

        public PageData ContentPage
        {
            get { return contentPage; }
            set { contentPage = value; }
        }

        public string ImagePropertyName
        {
            get { return imagePropertyName; }
            set { imagePropertyName = value; }
        }    

        public string TopCssClass
        {
            get { return topCssClass; }
            set { topCssClass = value; }
        }

        public string ImageCssClass
        {
            get { return imageCssClass; }
            set { imageCssClass = value; }
        }

        public string BottomCssClass
        {
            get { return bottomCssClass; }
            set { bottomCssClass = value; }
        }

        public int ImageWidth
        {
            get { return imageWidth; }
            set { imageWidth = value; }
        }

        public RoundedCornersImage()
        {
            ContentPage = CurrentPage;
        }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.Visible = (ContentPage != null && ContentPage[ImagePropertyName] != null);
            }
        }

        /// <summary>
        /// GetImageURL
        /// </summary>
        /// <returns>image url</returns>
        protected string GetImageURL()
        {
            if (ContentPage != null)
            {
                string imageString = ContentPage[ImagePropertyName] as string;

                if (imageString != null)
                {
                    ImageStoreNET.Classes.Util.UrlBuilder ub = 
                        ImageStoreNET.Classes.Util.UrlBuilder.ParseUrl(imageString);
                    
                    if (ub != null)
                    {
                        ub.Width = imageWidth;
                        ub.PreserveAspectRatio = true;
                        ub.ConversionFormatType = ImageStoreNET.Classes.Data.ConversionFormatTypes.WebSafe; 
                        return ub.ToString();
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// GetAltText
        /// </summary>
        /// <returns></returns>
        protected string GetAltText()
        {
            if (ContentPage != null)
            {
                LangAltText altText = new LangAltText();
                return altText.GetAltText
                    (CurrentPage.LanguageID, ContentPage[ImagePropertyName] as string ?? string.Empty);
            }
            else
            {
                return string.Empty;
            }
        }

    }
}