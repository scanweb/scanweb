﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelListingCityDetailBody.ascx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.HotelListingCityDetailBody" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<div class="hotelInfoCnt cityLanding" itemscope itemtype="http://schema.org/hotel">
<div class="imageHolder">	
<img src ="" alt="" id="imgHotel" runat="server" width="172" height="111"/>
</div>
<div class="desc">
    <p id="lblDistance" class="cityCenterDist" runat="server" ></p>	
	<h2 id="lnkHotelName" runat="server" itemprop="name">Scandic Anglais</h2>
	<p><a id="address" class="lnkAdress" runat="server"></a></p>
    <p id="details" runat="server"></p>
	<ul class="links"><li><a class="goto scansprite" id="lnkFulldescription" target="_top" runat="server"></a></li><li>
	<a href="#" id="lnkImageGallery" class="overlay jqModal scansprite" runat="server">
	<%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/imagegallery") %></a>
	</li></ul>
</div>
     <asp:PlaceHolder ID="phMetaForSchemaOrgHotelURL" runat="server" ></asp:PlaceHolder>

<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" visible="false">
     <asp:PlaceHolder ID="phMetaForSchemaOrgHotelAddress" runat="server" ></asp:PlaceHolder>
</div>
 <div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates" visible="false">
    <asp:PlaceHolder ID="phMetaForSchemaOrgHotelGeoCoords" runat="server" ></asp:PlaceHolder>
</div>
<!-- Trip Advisor -->	
<div class="tripadvisorWrapper">
 <div id="divTripAdvisorRating" runat="server" class="divTripAdvisorRating">
</div>
<div class="roomsAndrates">
	<div class="actionBtnContainer">
		<div class="actionBtn">
		<a class="buttonInner checkRatebtn scansprite" href="javascript:populateDestination('<%= GetHotelName %>', '<%= string.Format("HOTEL:{0}",hotelOperaId)%>');" class="checkRateLink">
		<%= WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/roomsandrates") %></a>
	
		</div>
	</div>
</div>
</div>

</div>