using System;
using System.Collections.Generic;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Units
{
    /// <summary>
    /// Test CDAL
    /// </summary>
    public partial class TestCDAL : System.Web.UI.UserControl
    {
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Button1 Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            DateTime t1 = DateTime.Now;
            List<CityDestination> destinations = ContentDataAccess.GetCityAndHotel(false);

            DateTime t2 = DateTime.Now;
            TimeSpan t = t2.Subtract(t1);
            CDALTime.Text = t.TotalMilliseconds + " milliseconds";

            foreach (CityDestination c in destinations)
            {
                string cityString = string.Empty;
                string resultString = "<div class=\"city\"><h3>" + c.Name + "(" + c.OperaDestinationId + ")</h3>";
                results.Text = results.Text + resultString;

                foreach (HotelDestination h in c.SearchedHotels)
                {
                    resultString =
                        "<div class=\"hotel\">" + h.Name + " (" + h.OperaDestinationId + ") " +
                        h.SearchableString + " " + h.HotelPageURL + "</div>";
                    results.Text = results.Text + resultString;
                }

                results.Text = results.Text + "</div>";
            }

            TextBox1.Text = string.Empty;
        }
    }
}