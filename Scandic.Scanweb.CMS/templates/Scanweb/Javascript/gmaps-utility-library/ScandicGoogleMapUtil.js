
function AddMyOverlay()
{
    $fn(_endsWith("BookingProgressDiv")).style.display = "block";
    $fn(_endsWith("progressBar")).style.display = "block";
   
}

function RemoveMyOverlay()
{
    $fn(_endsWith("BookingProgressDiv")).style.display = "none";
    $fn(_endsWith("progressBar")).style.display = "none";
}

function PreventClientBrowserMemoryLeak()
{
//    GUnload();
    if (typeof CollectGarbage == 'function')
    {
        CollectGarbage();
    }
}

function AdjustAdvancedPopUpWidthAndHeight()
{
    var ffcssObj = {
        'height' : '135px',
        'top' : '34px',
        'width':'345px'
    } 
    var iecssObj={
        'top':'40px'
    }
       
    $('#custom_info_window_b,#custom_info_window_t').css('width','318px');
    $('#custom_info_window_l,#custom_info_window_r').css('height','130px');
    $('#custom_info_window_contents').css(ffcssObj);
    if ($.browser.msie && $.browser.version <= 6 )
    {
        $('#custom_info_window_contents').css(iecssObj);
        $('#custom_info_window_br,#custom_info_window_bl').css('top','189px');
        $('#custom_info_window_b,#custom_info_window_t').css('width','318px');
        $('#custom_info_window_l,#custom_info_window_r').css(iecssObj);
        $('#custom_info_window_l,#custom_info_window_r').css('height','154px');
        $('#custom_info_window_t').css('top','24px');
    }
}