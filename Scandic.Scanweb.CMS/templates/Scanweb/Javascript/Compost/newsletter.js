function validateForm()
{
    var emailRegEx = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;
    if (document.getElementById("email").value == "") { 
           document.getElementById("errormessage").value = "Email address is mandatory";           
           return false;
    }
    else if (!(emailRegEx.test(document.getElementById("email").value)))
    {
        document.getElementById("errormessage").value = "Email address is invalid";
        return false;
    }
    else if (document.getElementById("country").value == "none") { 
        document.getElementById("errormessage").value = "Country is mandatory";        
        return false;
    }
    else if (document.getElementById("zipcode").value == "") { 
        document.getElementById("errormessage").value = "Zip code is mandatory";
        return false;
    } 
    return true;
}

