
//creates overlay for googlemap

function fetchOverlayDataXML(requestUrl, CreateOverlaysFunctionName)
{
   //method call used by AjaxServer
    var paramList = "MethodToCall=GetMarkersDataXml";
    //parameter used in AjaxServer
    var async = true;
    var method = "POST";
    getDataFromServer(requestUrl, CreateOverlaysFunctionName, async, method, paramList);
}

function GetGoogleMapHotelContetOverlayInfo(marker, infoWindow, requestURL) {
	var mapHotelUnitJSON = eval('(' + marker.mapHotelUnit + ')');
	$.ajax({
            url: requestURL + "/GetGoogleMapHotelContetOverlayInfo",
            type: "POST",
            async: true,
            cache: false,
            data: "{'mapHotelUnit':" + JSON.stringify(mapHotelUnitJSON) + "}",
	        contentType: "application/json; charset=utf-8",
            success: function(result) {
	           infoWindow.setContent(result.d);
	           infoWindow.open(marker.map, marker); 
            },
            error: function(result) {
            }
        });
}