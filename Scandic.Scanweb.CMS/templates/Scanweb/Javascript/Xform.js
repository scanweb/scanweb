﻿$(function() {


    $('#id_matrix input[type=text], textarea').blur(function(e) {

        var validationExists = $(this).siblings('.xformvalidator').filter(':visible');

        //validate against cyrilic characters here.
        var errorMsg = $('#RestrictSplChars').val();

        var errMsgID = $(this).attr('id') + 'ErrMsg';
        if (validationExists.text() == "" && validateRestrictSplChars(this, IsEmailField(this))) {

            $('.' + errMsgID).remove();
            $(this).after('<span id="spnErrMsg"  class=' + errMsgID + ' style="color:red">' + errorMsg + '</span>');
        }
        else {
            $('.' + errMsgID).remove();
        }

    });

    //stop page submit if page has validation errors( cyrillic characters)
    $('input[type=submit]').click(function(e) {

        var success = $('#spnErrMsg').length > 0 ? 0 : 1;

        if (success)
            return true;
        else
            return false;
    });

});

//Finds whether the current field is for Email input
function IsEmailField(ctrl) {

    var emailValidatorId = $(ctrl).attr('id');
    emailValidatorId += 'regexvalidator';
    // Partner Redemption email validation error - artf1274043 - SE hot fix
    if (document.getElementById(emailValidatorId) != null || emailValidatorId.toLowerCase().indexOf('email') > -1 ||
           emailValidatorId.toLowerCase().indexOf('e_mail') > -1 || emailValidatorId.toLowerCase().indexOf('e-mail') > -1)  
        return true;
    else      
        return false;
}

// Site Expansion - Ramana: To allow valid characters.
function validateRestrictSplChars(caller, IsEmailField) {
    var frmFld = $(caller);
    var frmFldValue = frmFld.val();
    var invalidCharFound = false;
  
  
    var textareaValidate = frmFld.attr("type");
    if (textareaValidate != "textarea") {
		frmFldValue = frmFldValue.replace( /^\s+|\s+$/g , ""); //Ltrim and Rtrim - replace initial and terminating spaces
		frmFldValue = frmFldValue.replace( /[a-zA-Z0-9]/g , ""); //Replace alphabets
		frmFldValue = frmFldValue.replace( /[\/\-]/g , ""); //replace allowed characters
		frmFldValue = frmFldValue.replace( /[\s]/g , ""); //replace allowed spaces between words

		//handle @ and . characters for email field
		if (IsEmailField)
			
			frmFldValue = frmFldValue.replace( /[\@\.\_]/g , "");

		if (frmFldValue.length > 0) {
			for (var k = 0; k < frmFldValue.length; k++) {
				var code = frmFldValue.charCodeAt(k);
				if (code < 191 || code > 255) {
					invalidCharFound = true;
					break;
				} 
				else {
					invalidCharFound = testIfAllowedSpecialCharsFound(code);
					if (invalidCharFound == true)
						break;
				}
			}
		}
	}
	return invalidCharFound;
}
// Site Expansion: Check if input characters are valid or not
function testIfAllowedSpecialCharsFound(num) {
    var charFound = false;
    //Boxi: removed check for extra Cyrilic charecters
    if (num == 215 || num == 247) {
        charFound = true;
    }
    return charFound;
}
