/* 
  ------------------------------------
  Expand and Collapse functions 
  for content areas
  ------------------------------------
*/
var hide;
var hideMeeting;

function populate(id, flag, maxlang, minlang)		
    { 			
        var element = document.getElementById(id);
        var elementimg = document.getElementById(id + 'img');
        
        if (element)
        {
            if (flag && flag != null)
            {
                element.style.display = 'block';
                element.style.visibility = 'visible';
                elementimg.src = '/Templates/Scanweb/Styles/Default/Images/Icons/box_minus.gif';
                elementimg.alt = minlang;
                hide = false;
            }
            else
            {
                element.style.display = 'none';
                element.style.visibility = 'hidden';
                elementimg.src = '/Templates/Scanweb/Styles/Default/Images/Icons/box_plus.gif';
                elementimg.alt = maxlang;
                hide = true;
            }
         }
     }   
     
     
function ExpandCollapse(id, flag, maxlang, minlang)		
    { 			
        var element = document.getElementById(id);
        var elementimg = document.getElementById(id + 'img');
        
        if (element)
        {
            if (flag)
            {
                element.style.display = 'block';
                element.style.visibility = 'visible';
                elementimg.src = '/Templates/Scanweb/Styles/Default/Images/Icons/box_minus.gif';
                elementimg.alt = minlang;
                hide = false;
            }
            else
            {
                element.style.display = 'none';
                element.style.visibility = 'hidden';
                elementimg.src = '/Templates/Scanweb/Styles/Default/Images/Icons/box_plus.gif';
                elementimg.alt = maxlang;
                hide = true;
            }
         }
     }
     

function SwitchClassName(elementID, firstClassName, alternativeClassName)		
    { 		

        var element = document.getElementById(elementID);
   
        if (element)
        {
            if (element.className == firstClassName){
                element.className = alternativeClassName;
            }
            else{
                element.className = firstClassName;
            }
         }
     }

     
function populateMeeting(id, flag, maxlang, minlang)		
    { 			
        var elementimages = document.getElementById('HotelMeetingImagesToBeHidden' + id);
        var elementcontent = document.getElementById('HotelMeetingContentToBeHidden' + id);
        var elementimg = document.getElementById('HotelMeeting' + id + 'img');
        
        if (elementimages && elementcontent)
        {
            if (flag)
            {
                elementimages.style.display = 'block';
                elementimages.style.visibility = 'visible';
                elementcontent.style.display = 'block';
                elementcontent.style.visibility = 'visible';
                elementimg.src = '/Templates/Scanweb/Styles/Default/Images/Icons/box_minus.gif';
                elementimg.alt = minlang;
                hideMeeting = false;
            }
            else
            {
                elementimages.style.display = 'none';
                elementimages.style.visibility = 'hidden';
                elementcontent.style.display = 'none';
                elementcontent.style.visibility = 'hidden';
                elementimg.src = '/Templates/Scanweb/Styles/Default/Images/Icons/box_plus.gif';
                elementimg.alt = maxlang;
                hideMeeting = true;
            }
         }
     }
