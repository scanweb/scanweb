//  Description					:   MasterPageWide                                        //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.ConfigureSecurePage;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.CMS.Templates.MasterPages
{
    /// <summary>
    /// The masterpage defines the common look and feel and a standard behavior of the website. 
    /// </summary>
    public partial class MasterPageWide : ScandicMasterPageBase
    {
        protected UserControl MenuLoginStatus;
        private const string _title = "{0}{1}{2}";
        private string _titleSeparator = " - ";

        /// <summary>
        /// Gets or sets the title separator.
        /// </summary>
        public string TitleSeparator
        {
            get { return _titleSeparator; }
            set { _titleSeparator = value; }
        }

        private void SetImage()
        {
            PageBase page = (PageBase) Page;
            string strImage = page.CurrentPage["HeaderImage"] as string;
            if (!String.IsNullOrEmpty(strImage))
                HeaderImage.ImageUrl = strImage;
        }

        /// <summary>
        /// On init event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            PageBase page = (PageBase) Page;
            PageData pageData = page.CurrentPage;
            ConfigureSecurePage.MakeWebPageSecured(pageData);
        }

        /// <summary>
        /// On load event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            PageBase page = (PageBase) Page;
            if (SubMenu != null && MainMenu != null)
            {
                if (LastVisitedPageSessionWrapper.SelectedMainMenuLink != null)
                {
                    SubMenu.MainMenuSelectedPage = LastVisitedPageSessionWrapper.SelectedMainMenuLink;
                }
                              
            }
            if (LeftMenuCtrl != null && SubMenu != null)
            {
                LeftMenuCtrl.MenuList = SubMenu.SubMenuList;
            }
            Page.Title = GetTitle();
            if (page.CurrentPage.PageName.Equals(EpiServerPageConstants.SESSION_TIME_OUT) ||
                page.CurrentPage.PageName.Equals(EpiServerPageConstants.APP_ERROR))
            {
                LeftMenuCtrl.Visible = false;
            }
            else
            {
                LeftMenuCtrl.Visible = true;
            }
            SetImage();
            if (page.CurrentPage.PageGuid != null &&
                (page.CurrentPage.PageGuid.ToString() != "4fa2b351-446b-477f-bda2-49eddc8f0608"))
            {
                WebUtil.ClearBlockedRooms();
            }
            this.HtmlElement.Attributes["lang"] = page.CurrentPage.LanguageBranch;
            MenuLoginStatus.Visible = OWSVisibilityControl.LoginShouldBeVisible;

            System.Web.UI.HtmlControls.HtmlAnchor ancLogo =
                this.HtmlElement.FindControl("ancLogoPageWide") as System.Web.UI.HtmlControls.HtmlAnchor;
            ancLogo.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE);
            PageData startPageData = DataFactory.Instance.GetPage(PageReference.StartPage);
            Control cookieLawInfo = new Control();
            if (startPageData.Property["IsCookieAllowed"] != null && startPageData.Property["CookieLawMessage"] != null)
            {
                if (!Convert.ToBoolean(startPageData.Property["IsCookieAllowed"].Value) &&
                    !string.IsNullOrEmpty(Convert.ToString(startPageData.Property["CookieLawMessage"])))
                    cookieLawPlaceHolder.Controls.Add
                        (cookieLawInfo = LoadControl("~\\Templates\\Scanweb\\Units\\Placeable\\CookieLaw.ascx"));
            }
            string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();
            switch (siteLanguage)
            {
                case LanguageConstant.LANGUAGE_FINNISH:
                    formWide.Attributes.Add("class", LanguageConstant.LANGUAGE_FINNISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_DANISH:
                    formWide.Attributes.Add("class", LanguageConstant.LANGUAGE_DANISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_SWEDISH:
                    formWide.Attributes.Add("class", LanguageConstant.LANGUAGE_SWEDISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION:
                    formWide.Attributes.Add("class", LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_GERMAN:
                    formWide.Attributes.Add("class", LanguageConstant.LANGUAGE_GERMAN.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_RUSSIA:
                    formWide.Attributes.Add("class", LanguageConstant.LANGUAGE_RUSSIA.ToLower());
                    break;
                default:
                    break;
            }
        }
    }
}