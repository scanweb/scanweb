//  Description					: MasterPagePromotionalPage                               //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using EPiServer;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.Util;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.MasterPages
{
    /// <summary>
    /// Code behind of MasterPagePromotionalPage
    /// </summary>
    public partial class MasterPagePromotionalPage : ScandicMasterPageBase
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            WebUtil.ClearBlockedRooms();
            PageData startPageData = DataFactory.Instance.GetPage(PageReference.StartPage);
            Control cookieLawInfo = new Control();
            if (startPageData.Property["IsCookieAllowed"] != null && startPageData.Property["CookieLawMessage"] != null)
            {
                if (!Convert.ToBoolean(startPageData.Property["IsCookieAllowed"].Value) &&
                    !string.IsNullOrEmpty(Convert.ToString(startPageData.Property["CookieLawMessage"])))
                    cookieLawPlaceHolder.Controls.Add
                        (cookieLawInfo = LoadControl("~\\Templates\\Scanweb\\Units\\Placeable\\CookieLaw.ascx"));
            }
            string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();
            switch (siteLanguage)
            {
                case LanguageConstant.LANGUAGE_FINNISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_FINNISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_DANISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_DANISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_SWEDISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_SWEDISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_GERMAN:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_GERMAN.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_RUSSIA:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_RUSSIA.ToLower());
                    break;
                default:
                    break;
            }
        }
    }
}