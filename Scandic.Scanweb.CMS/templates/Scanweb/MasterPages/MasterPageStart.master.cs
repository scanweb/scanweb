//  Description					:   MasterPageStart                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using System.Web.UI;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.ConfigureSecurePage;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.IP2Country;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.CMS.Templates.MasterPages
{
    /// <summary>
    /// The masterpage defines the common look and feel and a standard behavior of the website. 
    /// </summary>
    public partial class MasterPageStart : ScandicMasterPageBase
    {
        protected UserControl MenuLoginStatus;
        private const string _title = "{0}{1}{2}";
        private string _titleSeparator = " - ";

        /// <summary>
        /// Gets or sets the title separator.
        /// </summary>
        /// <value>The title separator.</value>
        public string TitleSeparator
        {
            get { return _titleSeparator; }
            set { _titleSeparator = value; }
        }

        /// <summary>
        /// Gets the browser CSS.
        /// </summary>
        /// <returns>the browser CSS.</returns>
        public string GetBrowserCSS()
        {
            if (Request.Browser.Type.ToString().Substring(0, 2) == "IE" &&
                Request.Browser.MajorVersion.ToString().Equals("6"))
                return "IE6";
            else if (Request.Browser.Type.ToString().Substring(0, 2) == "IE" &&
                     Request.Browser.MajorVersion.ToString().Equals("7"))
                return "IE7";
            else if (Request.Browser.Type.ToString().Substring(0, 2) == "Fi" &&
                     Request.Browser.MajorVersion.ToString().Equals("2"))
                return "Firefox20";
            else
                return "Firefox15";
        }

        /// <summary>
        /// Sets the image.
        /// </summary>
        private void SetImage()
        {
            PageBase page = (PageBase)Page;
            //PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET", "ImageStoreNET.ImageType");
            string strImage = page.CurrentPage["HeaderImage"] as string;
            if (!String.IsNullOrEmpty(strImage))
            {
                HeaderIMage.ImageUrl = strImage;
            }
            //pd.Value = strImage;
            //HeaderIMage.InnerProperty = pd;
        }


        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            PageBase page = (PageBase)Page;
            PageData pageData = page.CurrentPage;
            RedirectOnUserIp(pageData);
            ConfigureSecurePage.MakeWebPageSecured(pageData);
            SetLanguageCookiee();
        }

        private void SetLanguageCookiee()
        {
            string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToLower();
            var hostname = ConfigurationManager.AppSettings["ScandicSiteHostNameEN"] as string;
            var frameSource = string.Format(@"{0}://{1}/templates/Scanweb/Pages/Language.aspx?{2}={3}"
                                                , Request.Url.Scheme, hostname, AppConstants.LANGUAGE, siteLanguage);
            //var frameSource = string.Format("\\templates\\Scanweb\\Pages\\Language.aspx?{0}={1}", AppConstants.LANGUAGE, siteLanguage);
            languagePage.Attributes.Add("src", frameSource);
        }

        /// <summary>
        /// Redirects the on user ip.
        /// </summary>
        /// <param name="pageData">The page data.</param>
        private void RedirectOnUserIp(PageData pageData)
        {
            bool isAdmin = false;
            isAdmin = string.Equals(ConfigurationManager.AppSettings["IsAdminServer"], "true", StringComparison.InvariantCultureIgnoreCase);
            bool homePageRedirection = Request.QueryString["RD"] != null ? true : false;
            if (pageData.PageLink.ID == PageId.HOME_PAGE && !isAdmin && !homePageRedirection)
            {
                if (pageData.Property["EnableIPRedirection"] != null &&
                    pageData.Property["EnableIPRedirection"].Value != null &&
                    pageData.Property["EnableIPRedirection"].Value.Equals(true))
                {
                    if (0 == string.Compare(pageData.LanguageBranch, CurrentPageLanguageConstant.LANGUAGE_ENGLISH, true) &&
                        Request.UrlReferrer == null)
                    {
                        if (Request != null && !string.IsNullOrEmpty(Request.Url.Host))
                        {
                            if (0 !=
                                string.Compare(Request.Url.Host,
                                               (Request.UrlReferrer != null) ? Request.UrlReferrer.Host : string.Empty,
                                               true))
                            {
                                string redirectlanguage = string.Empty;
                                HttpCookie userLocaleCookie = Request.Cookies[AppConstants.USER_LOCALE_COOKIE];
                                if (userLocaleCookie == null)
                                {
                                    string ipAddress = Request.UserHostAddress;
                                    if (HttpContext.Current.Request.Headers["X-Forwarded-For"] != null)
                                    {
                                        string strIPTemp =
                                            HttpContext.Current.Request.Headers["X-Forwarded-For"].Split(new char[] { ',' })[0].
                                                Trim();
                                        AppLogger.LogInfoMessage("IP redirect using X-Forwarded-For. HTTP header  : " +
                                                                 HttpContext.Current.Request.Headers["X-Forwarded-For"]
                                                                 + " ,IP address :" + strIPTemp);
                                        if (!string.IsNullOrEmpty(strIPTemp))
                                        {
                                            ipAddress = strIPTemp;
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(ipAddress))
                                    {
                                        string countryCode = Ip2CountryLookup.GetCountryCodeFromIp(ipAddress);

                                        if (!string.IsNullOrEmpty(countryCode))
                                        {
                                            countryCode = countryCode.ToUpper();
                                            switch (countryCode)
                                            {
                                                case CountryCodeConstant.COUNTRY_SWEDEN:
                                                    {
                                                        redirectlanguage = CurrentPageLanguageConstant.LANGUAGE_SWEDISH;
                                                    }
                                                    break;
                                                case CountryCodeConstant.COUNTRY_DANMARK:
                                                    {
                                                        redirectlanguage = CurrentPageLanguageConstant.LANGUAGE_DANISH;
                                                    }
                                                    break;
                                                case CountryCodeConstant.COUNTRY_NORWAY:
                                                    {
                                                        redirectlanguage =
                                                            CurrentPageLanguageConstant.LANGUAGE_NORWEGIAN;
                                                    }
                                                    break;
                                                case CountryCodeConstant.COUNTRY_FINLAND:
                                                    {
                                                        redirectlanguage = CurrentPageLanguageConstant.LANGUAGE_FINNISH;
                                                    }
                                                    break;
                                                case CountryCodeConstant.COUNTRY_RUSSIA:
                                                    {
                                                        redirectlanguage = CurrentPageLanguageConstant.LANGAUGE_RUSSIAN;
                                                    }
                                                    break;
                                                case CountryCodeConstant.COUNTRY_GERMANY:
                                                    {
                                                        redirectlanguage = CurrentPageLanguageConstant.LANGUAGE_GERMANY;
                                                    }
                                                    break;
                                            }
                                        }
                                        AppLogger.LogInfoMessage("Site is auto-redirected to lanaguage site : " +
                                                                 redirectlanguage);
                                    }
                                }
                                else
                                {
                                    redirectlanguage = userLocaleCookie.Value;

                                    //No redirection required in the case of english language.
                                    if (string.Equals(redirectlanguage, CurrentPageLanguageConstant.LANGUAGE_ENGLISH, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        redirectlanguage = string.Empty;
                                    }
                                    HygieneSessionWrapper.IsSiteRedirectedOnIp = true;
                                }
                                if (!string.IsNullOrEmpty(redirectlanguage))
                                {
                                    //redirectlanguage = string.Equals(LanguageConstant.LANGUAGE_RUSSIA,redirectlanguage, StringComparison.InvariantCultureIgnoreCase) ? 
                                    //                   CurrentPageLanguageConstant.LANGAUGE_RUSSIAN :
                                    //                   redirectlanguage;
                                    if (string.Equals(LanguageConstant.LANGUAGE_RUSSIA, redirectlanguage, StringComparison.InvariantCultureIgnoreCase))
                                        redirectlanguage = CurrentPageLanguageConstant.LANGAUGE_RUSSIAN;
                                    string homePageRedirectUrl = WebUtil.ConvertUrlToLocale(pageData.LinkURL,
                                                                                            redirectlanguage,
                                                                                            pageData.PageLink);
                                    if (!string.IsNullOrEmpty(homePageRedirectUrl))
                                    {
                                        HygieneSessionWrapper.IsSiteRedirectedOnIp = true;
                                        var queryStringIndex = Request.RawUrl.IndexOf("?");
                                        if (queryStringIndex >= 0)
                                        {
                                            var queryString = Request.RawUrl.Substring(queryStringIndex);
                                            homePageRedirectUrl = string.Format("{0}{1}", homePageRedirectUrl, queryString);
                                        }
                                        Response.Redirect(homePageRedirectUrl);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            PageBase page = (PageBase)Page;
            Page.Title = GetTitle();
            SetImage();
            WebUtil.ClearBlockedRooms();
            this.HtmlElement.Attributes["lang"] = page.CurrentPage.LanguageBranch;
            MenuLoginStatus.Visible = OWSVisibilityControl.LoginShouldBeVisible;
            System.Web.UI.HtmlControls.HtmlAnchor ancLogo =
                this.HtmlElement.FindControl("ancLogoMaster") as System.Web.UI.HtmlControls.HtmlAnchor;
            ancLogo.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE);
            PageData startPageData = DataFactory.Instance.GetPage(PageReference.StartPage);
            Control cookieLawInfo = new Control();
            if (startPageData.Property["IsCookieAllowed"] != null && startPageData.Property["CookieLawMessage"] != null)
            {
                if (!Convert.ToBoolean(startPageData.Property["IsCookieAllowed"].Value) &&
                    !string.IsNullOrEmpty(Convert.ToString(startPageData.Property["CookieLawMessage"])))
                    cookieLawPlaceHolder.Controls.Add
                        (cookieLawInfo = LoadControl("~\\Templates\\Scanweb\\Units\\Placeable\\CookieLaw.ascx"));
            }
            
            string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();
            switch (siteLanguage)
            {
                case LanguageConstant.LANGUAGE_FINNISH:
                    BookingForm.Attributes.Add("class", LanguageConstant.LANGUAGE_FINNISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_DANISH:
                    BookingForm.Attributes.Add("class", LanguageConstant.LANGUAGE_DANISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_SWEDISH:
                    BookingForm.Attributes.Add("class", LanguageConstant.LANGUAGE_SWEDISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION:
                    BookingForm.Attributes.Add("class", LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_GERMAN:
                    BookingForm.Attributes.Add("class", LanguageConstant.LANGUAGE_GERMAN.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_RUSSIA:
                    BookingForm.Attributes.Add("class", LanguageConstant.LANGUAGE_RUSSIA.ToLower());
                    break;
                default:
                    break;
            }
            SetSchemaOrgScandicFields(startPageData);
        }

        private void SetSchemaOrgScandicFields(PageData startPageData)
        {
            PlaceHolder metaContentPlaceHolder = (PlaceHolder)this.FindControl("phMetaInfoForSchemaOrgPostalAddress");
            if (metaContentPlaceHolder != null)
            {
                //Schema.Org Street Address
                Utility.AddMetaInfoForSchemaOrg(metaContentPlaceHolder, "itemprop", "streetAddress", Convert.ToString(startPageData.Property["SchemaOrgStreetAddress"]));
                //Schema.Org POBoxNo
                Utility.AddMetaInfoForSchemaOrg(metaContentPlaceHolder, "itemprop", "postOfficeBoxNumber", Convert.ToString(startPageData.Property["SchemaOrgPOBoxNo"]));
                //Schema.Org Address Locality
                Utility.AddMetaInfoForSchemaOrg(metaContentPlaceHolder, "itemprop", "addressLocality", Convert.ToString(startPageData.Property["SchemaOrgAddressLocality"]));
                //Schema.Org PostCode
                Utility.AddMetaInfoForSchemaOrg(metaContentPlaceHolder, "itemprop", "postalCode", Convert.ToString(startPageData.Property["SchemaOrgPostCode"]));
                //Schema.Org Address Country
                Utility.AddMetaInfoForSchemaOrg(metaContentPlaceHolder, "itemprop", "addressCountry", Convert.ToString(startPageData.Property["SchemaOrgAddressCountry"]));
            }

            PlaceHolder phMetaInfoForSchemaOrgOrganizationName = (PlaceHolder)this.FindControl("phMetaInfoForSchemaOrgOrganizationName");
            if (phMetaInfoForSchemaOrgOrganizationName != null)
            {
                //Schema.Org Street Address
                Utility.AddMetaInfoForSchemaOrg(phMetaInfoForSchemaOrgOrganizationName, "itemprop", "name", WebUtil.GetTranslatedText("/bookingengine/booking/OrganizationInfo/OrganizationName"));
            }

        }
    }
}