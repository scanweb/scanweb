using System;
using System.Web.UI;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.ConfigureSecurePage;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.MasterPages
{
    /// <summary>
    /// Masterpagealloffers
    /// </summary>
    public partial class Masterpagealloffers : ScandicMasterPageBase
    {
        protected UserControl MenuLoginStatus;
        private const string _title = "{0}{1}{2}";
        private string _titleSeparator = " - ";

       

        /// <summary>
        /// Gets or sets the title separator.
        /// </summary>
        public string TitleSeparator
        {
            get { return _titleSeparator; }
            set { _titleSeparator = value; }
        }

        /// <summary>
        /// SetImage
        /// </summary>
        private void SetImage()
        {
            PageBase page = (PageBase) Page;
            //PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET", "ImageStoreNET.ImageType");
            string strImage = page.CurrentPage["HeaderImage"] as string;

            if (!String.IsNullOrEmpty(strImage))
                HeaderImage.ImageUrl = strImage;
        }

        /// <summary>
        /// OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            PageBase page = (PageBase) Page;
            PageData pageData = page.CurrentPage;
            ConfigureSecurePage.MakeWebPageSecured(pageData);
        }


        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            PageBase page = (PageBase) Page;
            if (SubMenu != null && MainMenu != null)
            {
               // if (Request.QueryString[AppConstants.MAINMENU_SELECTED_PAGE_REFERENCE] != null)
                if (LastVisitedPageSessionWrapper.SelectedMainMenuLink != null)
                {
                    SubMenu.MainMenuSelectedPage =
                        LastVisitedPageSessionWrapper.SelectedMainMenuLink;
                }
                
                //SubMenu.MenuList = MainMenu.MenuList;302
            }
            Page.Title = GetTitle();
            SetImage();
            WebUtil.ClearBlockedRooms();
            this.HtmlElement.Attributes["lang"] = page.CurrentPage.LanguageBranch;
            MenuLoginStatus.Visible = OWSVisibilityControl.LoginShouldBeVisible;
            System.Web.UI.HtmlControls.HtmlAnchor ancLogo =
                this.HtmlElement.FindControl("ancLogoPageWideFull") as System.Web.UI.HtmlControls.HtmlAnchor;
            ancLogo.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE);
            PageData startPageData = DataFactory.Instance.GetPage(PageReference.StartPage);
            Control cookieLawInfo = new Control();
            if (startPageData.Property["IsCookieAllowed"] != null && startPageData.Property["CookieLawMessage"] != null)
            {
                if (!Convert.ToBoolean(startPageData.Property["IsCookieAllowed"].Value) &&
                    !string.IsNullOrEmpty(Convert.ToString(startPageData.Property["CookieLawMessage"])))
                    cookieLawPlaceHolder.Controls.Add
                        (cookieLawInfo = LoadControl("~\\Templates\\Scanweb\\Units\\Placeable\\CookieLaw.ascx"));
            }
            string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();
            switch (siteLanguage)
            {
                case LanguageConstant.LANGUAGE_FINNISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_FINNISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_DANISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_DANISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_SWEDISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_SWEDISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_GERMAN:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_GERMAN.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_RUSSIA:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_RUSSIA.ToLower());
                    break;
                default:
                    break;
            }
        }
    }
}