#region Description

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Description					: This master page look and feel is a bit different then the other master page.             //
//                                Top section will be as it is but the right side column will not be there.                 //
//                                Instead of the right hand side column the middle column will be taking the                //
//                                entire page width so that the goolge map will fit into this correctly.                    //    
//								  This is implemented as a part of Find a hotel release.                                    //
//--------------------------------------------------------------------------------------------------------------------------//
/// Author						: Raj Kishore Marandi	                                                                    //
/// Author email id				:                           							                                    //
/// Creation Date				: 18th September  2009									                                    //
///	Version	#					: 1.0													                                    //
///-------------------------------------------------------------------------------------------------------------------------//
/// Revision History			: -NA-													                                    //
///	Last Modified Date			:														                                    //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endregion Description

using System;
using System.Web.UI;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.ConfigureSecurePage;
using Scandic.Scanweb.Entity;
using EPiServer.DataAbstraction;
using System.Configuration;
using Scandic.Scanweb.CMS.Templates.Scanweb.Units.Static;

namespace Scandic.Scanweb.CMS.Templates.MasterPages
{
    /// <summary>
    /// This class <see cref="MasterPageFindAHotel"></see> is code behind of MasterPageFindAHotel page. 
    /// </summary>
    public partial class MasterPageFindAHotel : ScandicMasterPageBase
    {
        protected UserControl MenuLoginStatus;
        private const string _title = "{0}{1}{2}";
        private string _titleSeparator = " - ";

        /// <summary>
        /// Returns a the MenuList of the sub menu
        /// </summary>
        //public EPiServer.Web.WebControls.MenuList SubMenuList
        //{
        //    get { return SubMenu.SubMenuList; }
        //}

        /// <summary>
        /// Gets or sets the title separator.
        /// </summary>
        public string TitleSeparator
        {
            get { return _titleSeparator; }
            set { _titleSeparator = value; }
        }

        private void SetImage()
        {
            PageBase page = (PageBase) Page;
            string strImage = page.CurrentPage["HeaderImage"] as string;
            if (!String.IsNullOrEmpty(strImage))
                HeaderImage.ImageUrl = strImage;
        }

        /// <summary>
        /// Page on init event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Page.Header.DataBind();
            PageBase page = (PageBase) Page;
            PageData pageData = page.CurrentPage;
            ConfigureSecurePage.MakeWebPageSecured(pageData);
        }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            PageBase page = (PageBase) Page;
            if (SubMenu != null && MainMenu != null)
            {
                if (LastVisitedPageSessionWrapper.SelectedMainMenuLink != null)
                {
                    SubMenu.MainMenuSelectedPage = LastVisitedPageSessionWrapper.SelectedMainMenuLink;
                }
            }
            Page.Title = GetTitle();
            SetImage();
            WebUtil.ClearBlockedRooms();
            this.HtmlElement.Attributes["lang"] = page.CurrentPage.LanguageBranch;
            MenuLoginStatus.Visible = OWSVisibilityControl.LoginShouldBeVisible;
            System.Web.UI.HtmlControls.HtmlAnchor ancLogo =
                this.HtmlElement.FindControl("ancLogoFindAHotel") as System.Web.UI.HtmlControls.HtmlAnchor;
            ancLogo.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE);
            PageData startPageData = DataFactory.Instance.GetPage(PageReference.StartPage);
            Control cookieLawInfo = new Control();
            if (startPageData.Property["IsCookieAllowed"] != null && startPageData.Property["CookieLawMessage"] != null)
            {
                if (!Convert.ToBoolean(startPageData.Property["IsCookieAllowed"].Value) &&
                    !string.IsNullOrEmpty(Convert.ToString(startPageData.Property["CookieLawMessage"])))
                    cookieLawPlaceHolder.Controls.Add
                        (cookieLawInfo = LoadControl("~\\Templates\\Scanweb\\Units\\Placeable\\CookieLaw.ascx"));
            }
            string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();
            
            switch (siteLanguage)
            {
                case LanguageConstant.LANGUAGE_FINNISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_FINNISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_DANISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_DANISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_SWEDISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_SWEDISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_GERMAN:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_GERMAN.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_RUSSIA:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_RUSSIA.ToLower());
                    break;
                default:
                    break;
            }
        }

        public void ChangeVisibilityOfSubMenu(bool visiblity)
        {
            this.SubMenu.Visible = visiblity;
        } 

    }
}