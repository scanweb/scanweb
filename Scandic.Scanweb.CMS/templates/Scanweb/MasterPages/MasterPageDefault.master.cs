using System;
using System.Web.UI;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.ConfigureSecurePage;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.CMS.Templates.MasterPages
{
    /// <summary>
    /// The masterpage defines the common look and feel and a standard behavior of the website. 
    /// </summary>
    public partial class MasterPageDefault : ScandicMasterPageBase
    {
        protected UserControl MenuLoginStatus;
        private const string _title = "{0}{1}{2}";
        private string _titleSeparator = " - ";

        /// <summary>
        /// Gets or sets the title separator.
        /// </summary>
        public string TitleSeparator
        {
            get { return _titleSeparator; }
            set { _titleSeparator = value; }
        }

        /// <summary>
        /// SetImage
        /// </summary>
        private void SetImage()
        {
            PageBase page = (PageBase)Page;
            string strImage = page.CurrentPage["HeaderImage"] as string;
            if (!String.IsNullOrEmpty(strImage))
                HeaderImage.ImageUrl = strImage;
        }

        /// <summary>
        /// OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            PageBase page = (PageBase)Page;
            PageData pageData = page.CurrentPage;
            ConfigureSecurePage.MakeWebPageSecured(pageData);
        }

        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            PageBase page = (PageBase)Page;
            Page.Title = GetTitle();
            SetImage();
            this.HtmlElement.Attributes["lang"] = page.CurrentPage.LanguageBranch;
            MenuLoginStatus.Visible = OWSVisibilityControl.LoginShouldBeVisible;
            System.Web.UI.HtmlControls.HtmlAnchor ancLogo =
                this.HtmlElement.FindControl("ancLogoDefault") as System.Web.UI.HtmlControls.HtmlAnchor;
            ancLogo.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE);
            PageData startPageData = DataFactory.Instance.GetPage(PageReference.StartPage);
            Control cookieLawInfo = new Control();
            if (startPageData.Property["IsCookieAllowed"] != null && startPageData.Property["CookieLawMessage"] != null)
            {
                if (!Convert.ToBoolean(startPageData.Property["IsCookieAllowed"].Value) &&
                    !string.IsNullOrEmpty(Convert.ToString(startPageData.Property["CookieLawMessage"])))
                    cookieLawPlaceHolder.Controls.Add
                        (cookieLawInfo = LoadControl("~\\Templates\\Scanweb\\Units\\Placeable\\CookieLaw.ascx"));
            }
            PageData pageData = DataFactory.Instance.GetPage(PageReference.RootPage);
            int selectHotelID = ((PageReference)pageData[EpiServerPageConstants.SELECT_HOTEL_PAGE]).ID;
            int selectRateID = ((PageReference)pageData[EpiServerPageConstants.SELECT_RATE_PAGE]).ID;
            int bookingDetailsID = ((PageReference)pageData[EpiServerPageConstants.BOOKING_DETAILS_PAGE]).ID;
            int bookingConfirmationID = ((PageReference)pageData[EpiServerPageConstants.BOOKING_CONFIRMATION_PAGE]).ID;
            int modifySelectRateID = ((PageReference)pageData[EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE]).ID;
            int modifyBookingDetailsID =
                ((PageReference)pageData[EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS]).ID;
            int modifyCancelDatesID = ((PageReference)pageData[EpiServerPageConstants.MODIFY_CANCEL_CHANGE_DATES]).ID;
            int paymentProcessingPageID = ((PageReference)pageData[EpiServerPageConstants.PAYMENT_PROCESSING_PAGE]).ID;

            Boolean isInBookingFlow = false;
            if (page.CurrentPage.PageLink.ID == selectHotelID || page.CurrentPage.PageLink.ID == selectRateID
                || page.CurrentPage.PageLink.ID == bookingDetailsID ||
                page.CurrentPage.PageLink.ID == bookingConfirmationID ||
                page.CurrentPage.PageLink.ID == modifySelectRateID ||
                page.CurrentPage.PageLink.ID == modifyBookingDetailsID
                || page.CurrentPage.PageLink.ID == modifyCancelDatesID
                || page.CurrentPage.PageLink.ID == paymentProcessingPageID)
            {
                isInBookingFlow = true;
            }
            if (!isInBookingFlow)
            {
                WebUtil.ClearBlockedRooms();
            }

            string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();
            switch (siteLanguage)
            {
                case LanguageConstant.LANGUAGE_FINNISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_FINNISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_DANISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_DANISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_SWEDISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_SWEDISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_GERMAN:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_GERMAN.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_RUSSIA:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_RUSSIA.ToLower());
                    break;
                default:
                    break;
            }
        }
    }
}