using System;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Templates.Units.Static;
using Scandic.Scanweb.CMS.Util;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller;
using System.Configuration;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// OfferList
    /// </summary>
    public partial class OfferList : ScandicTemplatePage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            OfferCategory.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(OfferCategory_ItemDataBound);
        }

        private bool IsRedirectToMobileOffer()
        {
            bool isredirect = false;
            string referrer = Request.UrlReferrer == null ? "" : Request.UrlReferrer.AbsolutePath;
            if (referrer.Contains("/mobile") && CMS.Util.MobileRedirect.Wurfl.Instance.IsMobileDevice(Request.UserAgent) &&
                !CMS.Util.MobileRedirect.Wurfl.Instance.IsTablet(Request.UserAgent))
                isredirect = true;
            else
                isredirect = false;
            return isredirect;
        }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsRedirectToMobileOffer())
            {
                Response.Redirect(GetMobileOffersListPageURL());
            }

            PageReference offerContainerLink = CurrentPage["OfferContainer"] as PageReference;
            showAllOffer.InnerHtml = Convert.ToString(CurrentPage["ShowAllOffersText"]);
            if (offerContainerLink != null)
            {
                PageDataCollection offerCategoryPages = DataFactory.Instance.GetChildren(offerContainerLink);

                if (CurrentPage.PageTypeName == "Offer Category")
                {
                    if ((CurrentPage["HideOfferCategory"] == null) ||
                        (CurrentPage["HideOfferCategory"] != null
                         && CurrentPage["HideOfferCategory"].ToString().ToLower() != "true"))
                    {
                        OfferCategoryListing categoryListing =
                            (OfferCategoryListing)
                            LoadControl("~\\Templates\\Scanweb\\Units\\Static\\OfferCategoryListing.ascx");

                        categoryListing.OfferCategoryPage = CurrentPage;
                        OfferCategoryPlaceHolder.Controls.Add(categoryListing);                        
                    }
                    fillterButtons.Visible = false;
                    fillterWrapper.Visible = false;
                    categorySelectionText.Visible = false;
                }
                else
                {
                    PageDataCollection pgDataCollection = new PageDataCollection();
                    foreach (PageData offerCategoryPage in offerCategoryPages)
                    {
                        if ((offerCategoryPage["HideOfferCategory"] == null) ||
                            (offerCategoryPage["HideOfferCategory"] != null &&
                             offerCategoryPage["HideOfferCategory"].ToString().ToLower() != "true"))
                        {
                            pgDataCollection.Add(offerCategoryPage);
                            OfferCategoryListing categoryListing =
                                (OfferCategoryListing)
                                LoadControl("~\\Templates\\Scanweb\\Units\\Static\\OfferCategoryListing.ascx");

                            categoryListing.OfferCategoryPage = offerCategoryPage;
                            OfferCategoryPlaceHolder.Controls.Add(categoryListing);
                        }
                    }
                    OfferCategory.DataSource = pgDataCollection;
                    OfferCategory.DataBind();
                }
            }
        }

        private string GetMobileOffersListPageURL()//string linkURL, string language, PageReference pageLink
        {
            PageData pageData = GetPage(PageReference.RootPage);
            PageData pageOfferContainer = ContentDataAccessManager.GetPageData(((PageReference)pageData["MobileStartPage"]));
            PageData mobileOfferList = null;
            string finalUrl = string.Empty;
            if (pageOfferContainer != null)
                mobileOfferList = ContentDataAccessManager.GetPageData(((PageReference)pageOfferContainer["OffersContainer"]));
            if (mobileOfferList != null)
            {
                finalUrl = WebUtil.GetExternalUrl(mobileOfferList.LinkURL);
                //string languageSelectionURL = EPiServer.UriSupport.AddLanguageSelection(mobileOfferList.LinkURL, CurrentPage.LanguageID);
                //string fullLanguageSelectionURL = EPiServer.UriSupport.AbsoluteUrlBySettings(languageSelectionURL);
                //UrlBuilder finalUrlBuilder = new UrlBuilder(fullLanguageSelectionURL);
                //EPiServer.Global.UrlRewriteProvider.ConvertToExternal(finalUrlBuilder, mobileOfferList.PageLink, System.Text.UTF8Encoding.UTF8);
                //finalUrl = finalUrlBuilder.ToString();
                //string hostName = "ScandicMobileSiteHostName{0}";
                //string appSettingsKey = string.Format(hostName, CurrentPage.LanguageID.ToUpper());
                //string hostURL = ConfigurationManager.AppSettings[appSettingsKey] as string;
                //Url url = new Url(fullLanguageSelectionURL);
                //if (!string.IsNullOrEmpty(hostURL))
                //{
                //    finalUrl = finalUrl.Replace(url.Host, hostURL);
                //    finalUrl = finalUrl.Replace("/" + CurrentPage.LanguageID + "/", "/");
                //}
                //finalUrl = string.Format("{0}{1}{2}", finalUrl, "?pageid=", CurrentPage.PageLink.ID);
            }
            return finalUrl;
        }

        private void OfferCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var pageData = e.Item.DataItem as PageData;
                if (pageData != null)
                {
                    var offerButton = e.Item.FindControl("offerButton") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    offerButton.InnerHtml = pageData.PageName;
                    offerButton.Attributes.Add("data-id", Convert.ToString(pageData.PageLink.ID));
                }
            }
        }
    }
}