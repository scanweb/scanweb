﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.CMS.code.Util.Map;
using System.Collections;
using System.Collections.ObjectModel;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages
{
    public partial class DestinationLandingPageMap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapHotelUnit"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetGoogleMapHotelContetOverlayInfo(MapHotelUnit mapHotelUnit)
        {
            return mapHotelUnit.GenerateInfoWindowMarkUp();
        }
    }
}
