//  Description					:   ExpansionOverviewPage                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.Services;
using EPiServer.Globalization;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.Map;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// ExpansionOverviewPage
    /// </summary>
    public partial class ExpansionOverviewPage : Scandic.Scanweb.CMS.Util.ScandicTemplatePage
    {
        #region Constants
        private const string promoImage1 = "promoImage1";
        private const string PromoBoxHeading1 = "PromoBoxHeading1";
        private const string HotelDescription1 = "HotelDescription1";
        private const string PromoBoxLinkText1 = "PromoBoxLinkText1";
        private const string PromoBoxURL1 = "PromoBoxURL1";
        private const string promoImage2 = "promoImage2";
        private const string PromoBoxHeading2 = "PromoBoxHeading2";
        private const string HotelDescription2 = "HotelDescription2";
        private const string PromoBoxLinkText2 = "PromoBoxLinkText2";
        private const string PromoBoxURL2 = "PromoBoxURL2";
        private const string promoImage3 = "promoImage3";
        private const string PromoBoxHeading3 = "PromoBoxHeading3";
        private const string HotelDescription3 = "HotelDescription3";
        private const string PromoBoxLinkText3 = "PromoBoxLinkText3";
        private const string PromoBoxURL3 = "PromoBoxURL3";
        private const string COMINGSOON_VALUE = "ComingSoon";
        private const string RECENTLYOPENED_VALUE = "RecentlyOpened";
        #endregion

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                udAddAnotherHotel.Visible = OWSVisibilityControl.BookingModuleShouldBeVisible;
                AlternativeBookingModule.Visible = !OWSVisibilityControl.BookingModuleShouldBeVisible;

                List<HotelDestination> allHotelsList = null;
                if (!Page.IsPostBack)
                {
                    allHotelsList = ContentDataAccess.GetAllHotels();
                    if (allHotelsList != null && allHotelsList.Count > 0)
                    {
                        DigitalPlatformSessionWrapper.ExpansionPageHotelList = allHotelsList;
                    }
                    UpdatePromoBoxDetails();
                    ShowAddthis();
                }
                if (null != CurrentPage["XMLFilePath"])
                    XMLFilePath.Value = CurrentPage["XMLFilePath"].ToString();
                if (null != CurrentPage["FlashFilePath"])
                    FlashFilePath.Value = CurrentPage["FlashFilePath"].ToString();
                SelectedLanguage.Value = ContentLanguage.SpecificCulture.Parent.Name;


            }
            catch (Exception ex)
            {
            }
        }

        private void ShowAddthis()
        {
            addThis.InnerHtml = Utility.GetAddthisScript(CurrentPage.LanguageID);
        }

        /// <summary>
        /// Update Promobox information with current page.
        /// </summary>
        private void UpdatePromoBoxDetails()
        {
            if ((CurrentPage[promoImage1] == null) && (CurrentPage[PromoBoxHeading1] == null)
                && (CurrentPage[HotelDescription1] == null) && (CurrentPage[PromoBoxLinkText1] == null)
                && (CurrentPage[PromoBoxURL1] == null))
            {
                pnlBox1.Visible = false;
            }
            if ((CurrentPage[promoImage2] == null) && (CurrentPage[PromoBoxHeading2] == null)
                && (CurrentPage[HotelDescription2] == null) && (CurrentPage[PromoBoxLinkText2] == null)
                && (CurrentPage[PromoBoxURL2] == null))
            {
                pnlBox2.Visible = false;
            }
            if ((CurrentPage[promoImage3] == null) && (CurrentPage[PromoBoxHeading3] == null)
                && (CurrentPage[HotelDescription3] == null) && (CurrentPage[PromoBoxLinkText3] == null)
                && (CurrentPage[PromoBoxURL3] == null))
            {
                pnlBox3.Visible = false;
            }
        }

        /// <summary>
        /// This will get all hotel list from CMS for diffrent country.
        /// </summary>
        /// <returns> All hotels</returns>
        public string GetHotelList()
        {
            List<HotelDestination> allHotelsList = null;
            if (DigitalPlatformSessionWrapper.ExpansionPageHotelList != null)
            {
                allHotelsList = DigitalPlatformSessionWrapper.ExpansionPageHotelList;
            }
            string hotelList = "null";
            if (allHotelsList != null)
            {
                hotelList = "[";
                for (int i = 0; i < allHotelsList.Count; i++)
                {
                    HotelDestination hotelDestination = allHotelsList[i];
                    string hotelData = @"{name:'" + hotelDestination.Name + @"',country:'" +
                                       hotelDestination.CountryCode + @"',
                info:'" +
                                       string.Format("{0}{1}{2}", CheckHotelCategory(hotelDestination), " ",
                                                     CreateOpeningDate(hotelDestination)) +
                                       @"',
                text:'" +
                                       ReplaceSinglequotes(GetFilteredHotelDescription(hotelDestination.HotelDescription)) +
                                       @"',			
                thumb:'" + hotelDestination.ImageURL +
                                       @"',
                url:'" + hotelDestination.HotelPageURL +
                                       @"',
			    urltitle:	'" +
                                       WebUtil.GetTranslatedText(
                                           "/Templates/Scanweb/Units/Static/ExpansionPage/ReadMore") + @"'}";
                    if (i > 0)
                        hotelList = hotelList + "," + hotelData;
                    else
                        hotelList = hotelList + hotelData;
                }
                hotelList = hotelList + "]";
            }
            return hotelList;
        }

        /// <summary>
        /// This will format Opennig Date for Hotel.
        /// </summary>
        /// <param name="hotelDestination">HotelDestination object</param>
        /// <returns>Formatted Date for Hotel</returns>
        private string CreateOpeningDate(HotelDestination hotelDestination)
        {
            string formattedOpeningDate = string.Empty;
            if (hotelDestination.IgnoreOpeningDate)
                formattedOpeningDate = DateUtil.DateToMMMMYYYYWords(hotelDestination.OpeningDate);
            else
            {
                formattedOpeningDate = DateUtil.DateToDDMMMMYYYYWords(hotelDestination.OpeningDate);
                if (CurrentPage.LanguageID.Equals("no") || CurrentPage.LanguageID.Equals("da") ||
                    CurrentPage.LanguageID.Equals("fi"))
                    formattedOpeningDate = formattedOpeningDate.Substring(0, 2) + "." +
                                           formattedOpeningDate.Substring(2);
            }
            return formattedOpeningDate;
        }

        /// <summary>
        /// This will return opening or opened string based on the Hotel category
        /// </summary>
        /// <param name="hotelDestination">HotelDestination Object</param>
        /// <returns>opened or opening based on hotel category</returns>
        private string CheckHotelCategory(HotelDestination hotelDestination)
        {
            string openingText = string.Empty;
            if (!string.IsNullOrEmpty(hotelDestination.HotelCategory))
            {
                if (hotelDestination.HotelCategory.ToString().Equals(COMINGSOON_VALUE))
                    openingText = WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/ExpansionPage/Opening");
                else if (hotelDestination.HotelCategory.ToString().Equals(RECENTLYOPENED_VALUE))
                    openingText = WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/ExpansionPage/Opened");
            }
            return openingText;
        }

        /// <summary>
        /// This will replace single quotes from the description.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        private string ReplaceSinglequotes(string description)
        {
            if (!string.IsNullOrEmpty(description))
            {
                if (description.Contains("'"))
                {
                    description = description.Replace("'", "#");
                }
            }
            return description;
        }

        /// <summary>
        /// Remove all the html tags from the hotel description
        /// This will avoid formatting of the hotelDescription
        /// </summary>
        /// <param name="data">string to be formatted</param>
        /// <returns>resulting string</returns>
        private string RemoveHtmlTags(string data)
        {
            try
            {
                if (!string.IsNullOrEmpty(data))
                {
                    Regex tagPattern = new Regex("<[^>]*>");
                    data = tagPattern.Replace(data, string.Empty);
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Error while removing html tags from input data");
            }

            return data;
        }

        /// <summary>
        /// Format hotel description if user enters more characters.
        /// </summary>
        /// <returns></returns>
        private string GetFilteredHotelDescription(string data)
        {
            string taglessHotelDescription = RemoveHtmlTags(data);

            try
            {
                if (!string.IsNullOrEmpty(taglessHotelDescription))
                {
                    if (taglessHotelDescription.Length > AppConstants.MAX_LENGTH_SHORTDESCRIPTION_LISTING_CONTROL)
                    {
                        taglessHotelDescription = taglessHotelDescription.Substring(0,
                                                                                    AppConstants.
                                                                                        MAX_LENGTH_SHORTDESCRIPTION_LISTING_CONTROL);
                        taglessHotelDescription += AppConstants.TRIPPLE_DOTS;
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Error while parsing short description");
            }
            return taglessHotelDescription;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapHotelUnit"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetGoogleMapHotelContetOverlayInfo(MapHotelUnit mapHotelUnit)
        {
            return mapHotelUnit.GenerateInfoWindowMarkUp();
        }
    }
}