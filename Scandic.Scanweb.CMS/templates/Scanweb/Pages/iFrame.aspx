<%@ Page language="c#" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWide.master" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages.iFrame" Codebehind="iFrame.aspx.cs" %>

<%@ Register TagPrefix="Scanweb" TagName="LeftColumn" Src="~/Templates/Scanweb/Units/Placeable/LeftColumn.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumnRegion" runat="server">
    <Scanweb:LeftColumn ID="LeftColumn1" runat="server" />
</asp:Content>

<asp:Content  runat="server" ContentPlaceHolderID="MainBodyRegion">
<div id="<% = (CheckRightColumn() == true) ? "iframeWithRightCol" : "iframeWithoutRightCol" %>">
    <div>
        <h1><EPiServer:Property ID="Property2" PropertyName="PageName" runat="server" /></h1>&nbsp;
    </div>
    <div>
        <EPiServer:Property ID="Property1" PropertyName="MainBody" runat="server" />&nbsp;
    </div>
    <iframe id="IframePage" class="IframeLook" frameborder="0" scrolling="auto" runat="server" >
    </iframe>
 </div>
</asp:Content>    

