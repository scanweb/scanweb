<%@ Page Language="C#" Codebehind="ExpansionOverviewPage.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.ExpansionOverviewPage"
    MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageExpansionPage.Master" %>

<%@ Register TagPrefix="Scanweb" TagName="HotelList" Src="~/Templates/Scanweb/Units/Static/HotelExpansionHotelList.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="HotelPromo" Src="~/Templates/Scanweb/Units/Static/PromoBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="LeftColumn" Src="~/Templates/Scanweb/Units/Placeable/LeftColumn.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="MainBody" Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="Booking" TagName="BookingModule" Src="~/Templates/Booking/Units/BookingModuleBig.ascx" %>
<%@ Register TagPrefix="Booking" TagName="FlyOut" Src="~/Templates/Booking/Units/BookingModuleFlyout.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="BookingModuleAlternativeBox" Src="~/Templates/Scanweb/Units/Placeable/BookingModuleAlternativeBox.ascx" %>

<asp:Content ContentPlaceHolderID="MainBodyRegion" runat="server">
<script type="text/javascript">
    var addthis_config = { "data_track_addressbar": true };
 </script>
 <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50645171248e42f5"></script>
    <input type="hidden" runat="server" value="" id="XMLFilePath" />
    <input type="hidden" runat="server" value="" id="FlashFilePath" />
    <input type="hidden" runat="server" value="" id="SelectedLanguage" />
    <!-- Please put your content here -->
    <Scanweb:HotelList id="HotelList" runat="server" />
    <!-- Please put your content here -->
    <!-- flash calendar -->
    <div id="flashCalendar-wrap">
        <div id="flashCalendar">
        </div>
    </div>
    <div class="ExpansionPageBottomArea">
        <div class="singleCol">
            <Scanweb:LeftColumn ID="LeftColumn" runat="server" />
        </div>
        <div class="doubleCol flushMarginLeft">
       
            <Scanweb:MainBody runat="server" />
        </div>
        <!-- Add Promo Box here -->
        <div class="singleCol flushPromo">
         <!--START: ADDTHIS PLUGIN -->
    <div id ="addThis" runat="server" class="addthis_toolbox addthis_default_style" style="float: right; margin-bottom: 10px;">
   
    </div>   
        <!--END: ADDTHIS PLUGIN -->
        
            <asp:Panel ID="pnlBox1" runat="server">
                <div class="BoxContainer">
                    <Scanweb:HotelPromo id="HotelPromo" LinkUrlOpenOption="PromoBoxUrlOpenOption1" ImageMaxWidth="226" Image="promoImage1" Heading="PromoBoxHeading1"
                        Description="HotelDescription1" LinkText="PromoBoxLinkText1" LinkURL="PromoBoxURL1"
                        runat="server" />
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlBox2" runat="server">
                <div class="BoxContainer">
                    <Scanweb:HotelPromo id="HotelPromo1" LinkUrlOpenOption="PromoBoxUrlOpenOption2" ImageMaxWidth="226" Image="promoImage2" Heading="PromoBoxHeading2"
                        Description="HotelDescription2" LinkText="PromoBoxLinkText2" LinkURL="PromoBoxURL2"
                        runat="server" />
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlBox3" runat="server">
                <div class="BoxContainer">
                    <Scanweb:HotelPromo id="HotelPromo2" LinkUrlOpenOption="PromoBoxUrlOpenOption3" ImageMaxWidth="226" Image="promoImage3" Heading="PromoBoxHeading3"
                        Description="HotelDescription3" LinkText="PromoBoxLinkText3" LinkURL="PromoBoxURL3"
                        runat="server" />
                </div>
            </asp:Panel>
        </div>
    </div>
   <%-- <div class="jqmWindow" id="dialog">
        <a href="#" class="jqmClose">&nbsp;</a>
        RK: Commenting below code as bookingmodulemedium development is not completed and would break in CMS if not commented.
        This needs to be uncommented later when the development is completed
        <Scanweb:bookingModule id="mediumBookingModule" runat="server">
        </Scanweb:bookingModule>
    </div>--%>
    <div class="conf_Popup">
    <div class="jqmWindow dialog" id="dialog" style="display:none;" overflow="auto">
        <Booking:FlyOut ID="bookingFlyOut"></Booking:FlyOut>
        <div class="guestInfoPopup" id="RoomCategoryContainer">
            <div id="LeftContentArea">
                      <div class="closeBtn">
                    <a href="#" class="jqmClose scansprite blkClose" style="width:30px;height:27px;padding:0px;">&nbsp;</a>
                </div>
                <div id="BookingArea">
                    <Booking:BookingModule id="udAddAnotherHotel" runat="server"></Booking:BookingModule>
                    <Scanweb:BookingModuleAlternativeBox CssClass="AlternativeBookingBoxLarge" ID="AlternativeBookingModule" runat="server" Visible="false" />
                </div>
            </div>
        </div>
    </div>
    </div>    

    <script type="text/javascript" language="javascript">
    //Read Hotel List from CMS and create aHotels object.

    var aHotels = <%= GetHotelList() %>;
    if(aHotels == null)
    {
        aHotels = "";
    }
    
    function populateDestination(destinationText,destinationValue)
    {
        $('#dialog').jqm();
        var control = $fn(_endsWith('txtHotelName'));
        if(null != control)
        {
            control.value = destinationText;
        }
        var dest = $fn(_endsWith('selectedDestId'));
        //Following will be removed during UAT2.Temporary Fix for BUG artf1042621
        //Please consult RAJ for this.
        if(null != dest)
        {
           dest.value = destinationValue;
        }
        //Reset the booking module with initial state.
        resetBooikingModule();
        $('#dialog').jqmShow();
		 $('.closeBtn').css('margin-left','476px');
    }
    
    function resetBooikingModule()
    {
        /*Fix for Artifact artf1025725 : Find Hotel | Second time click on the hotel �check rates� button 
        earlier validation remains as it is */
        var errDiv = $fn(_endsWith('clientErrorDiv'));
        if (errDiv !=null && (errDiv.style.visibility != isHidden || errDiv.style.display != "none"))
        {
            errDiv.style.visibility     = isHidden;
            errDiv.style.display        = "none";
//            var controls = new Array("lblUserName","lblPassword","destCont");
//            for(var i=0;i<controls.length;i++)
//            {
//                var ctrlId = $fn(_endsWith(controls[i]));
//                ctrlId.className ="";
//            }
             //Vrushali | Res 2.0 | Commented this as it is not part of booking module big now.
             //500312 - Expansion Page | Map | Getting javascript error on clicking Check Rates button.
            //var ddlBT = $fn(_endsWith('ddlBookingType'));
           // ddlBT.selectedIndex=0;
        }
		 $('.closeBtn').css('margin-left','477px');
    }
    
    // This is tracked only from Expansion page map
    function UpdateSiteCatalystExpansionPageMap(selectedItem)
    {
        s.linkTrackVars='prop33,eVar33';
        s.prop33=selectedItem; 
        s.eVar33=selectedItem; 
        s.tl(this,'o','New Hotel Search');
    }
    </script>

</asp:Content>
