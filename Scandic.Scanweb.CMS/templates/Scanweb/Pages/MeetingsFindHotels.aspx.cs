//  Description					: MeetingsFindHotels                                      //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.Services;
using Scandic.Scanweb.CMS.SpecializedProperties;
using Scandic.Scanweb.CMS.Templates.Units.Placeable;
using Scandic.Scanweb.CMS.Templates.Units.Static;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.Map;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Code behind of MeetingsFindHotels control.
    /// </summary>
    public partial class MeetingsFindHotels : ScandicTemplatePage
    {
        /// <summary>
        /// Oninit event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
            PageData startPage = EPiServer.DataFactory.Instance.GetPage(PageReference.StartPage, EPiServer.Security.AccessLevel.NoAccess);
            searchHeader.InnerText = Convert.ToString(startPage["MeetingSearchModuleHeader"]);
            if (Convert.ToBoolean(CurrentPage["MiddleSearchModuleVisible"]))
                MeetingsSearch.Visible = false;
        }

        /// <summary>
        /// InitializeComponent
        /// </summary>
        private void InitializeComponent()
        {
            this.MeetingsSearch.SearchMeeting +=
                new MeetingSearch.MeetingSearchEventHandler(((SearchMeetingList) SearchMeetingList).ChangeSearched);

            this.SearchMeetingList.MeetingGoogleMapRender +=
                new SearchMeetingList.MeetingGoogleMapEventHandler(
                    ((MeetingSearchGoogleMap) SearchMeetingGoogleMap).GoogleMapRender);
        }

        /// <summary>
        /// Gets CSS
        /// </summary>
        /// <returns>CSS</returns>
        protected string GetCSS()
        {
            if (PropertyValueEqualsVisible("RightColumnShiftUp")
                &&
                (PropertyValueEqualsVisible("RightBookingModuleVisible") ||
                 PropertyValueEqualsVisible("RightMeetingModuleVisible")))
            {
                return "RightColumnShiftUp";
            }
            else
            {
                return "RightColumn";
            }
        }

        /// <summary>
        /// PropertyValueEqualsVisible
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns>True/False</returns>
        private bool PropertyValueEqualsVisible(string propertyName)
        {
            if (CurrentPage[propertyName] != null)
            {
                return ((int) CurrentPage[propertyName] == (int) SelectVisibility.Visibility.Visible);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapHotelUnit"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetGoogleMapHotelContetOverlayInfo(MapHotelUnit mapHotelUnit)
        {
            return mapHotelUnit.GenerateInfoWindowMarkUp();
        }
    }
}