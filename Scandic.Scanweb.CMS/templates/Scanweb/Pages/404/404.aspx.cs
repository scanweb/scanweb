﻿using System;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages._404
{
    public partial class _04 : EPiServer.TemplatePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.StatusCode = 404;
        }
    }
}