﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="404.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages._404._04" %>
<%@ Register TagPrefix="Scanweb" TagName="SiteCatalystCloudTag" Src="~/Templates/Scanweb/Units/Static/SiteCatalystCloudTag.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="AdobeDTMFooter" Src="~/Templates/Scanweb/Units/Static/AdobeDTMFooter.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<meta charset="utf-8">
<title>404 Page not found : Scandic</title>
<Scanweb:SiteCatalystCloudTag runat="server" />
<link rel="stylesheet" href="error.css?v=<%=CmsUtil.GetCSSVersion()%>" />
<style>
	/* custom links; shown on same line */
	a {display: inline}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="page-wrap">

        <div id="header">
            <img id="logo" src="/ImageVault/Images/id_14602/conversionFormatType_WebSafe/scope_0/ImageVaultHandler.aspx" alt="Scandic logo">
        </div>
        
        <div id="content" class="clearfix">
        
            <div id="sidebar">
                <h2>Contact us</h2>
                <p><strong>Scandic</strong><br>
                P.O. Box 6197<br>
                SE-102 33 Stockholm<br>
                Sweden<br>
                Visiting address: Sveav&#228;gen 167<br>
                Telephone: +46 8 517 350 00</p>
                
                <p><strong>If you like to make a reservation please contact Scandic Reservation and customer service at:</strong></p>
                
                <p>International: +46 8 517 517 20<br>
                Sweden:	08-517 517 00<br>
                Denmark: 33 48 04 00<br>
                Norway:	23 15 50 00<br>
                Finland: 0 200 818 00<br>
                or the hotel directly.</p>
            </div>
            
            <div id="main">
                <h1>Page not found</h1>
                <p><strong>Page is not found, please go to: </strong><a href="http://www.scandichotels.com">www.scandichotels.com</a></p>
                
                <p><strong>Sidan kan inte hittas, g&#229; till: </strong><a href="http://www.scandichotels.se">www.scandichotels.se</a></p>
                
                <p><strong>Siden findes ikke. Besøg </strong><a href="http://www.scandichotels.dk">www.scandichotels.dk</a></p>
                
                <p><strong>Sivua ei l&#246;ydy: </strong><a href="http://www.scandichotels.fi">www.scandichotels.fi</a></p>
                
                <p><strong>Finner ikke siden: </strong>
                <a href="http://www.scandichotels.no">www.scandichotels.no</a></p>
				
		<p><strong>Seite nicht gefunden. Bitte gehen Sie zu: </strong><a href="http://www.scandichotels.de">www.scandichotels.de</a></p>
				
		<p><strong>Страница не найдена, пожалуйста, перейдите на: </strong><a href="http://www.scandichotels.ru">www.scandichotels.ru</a></p>
				
            </div>
        </div>
        
        <div id="footer">
        	<p>&copy;Scandic</p>
        </div>
    
    </div>
    </div>
    </form>


<!-- Web Analythics JavaScripts -->
<script type="text/javascript" language="javascript" src="/Templates/Scanweb/Javascript/SiteCatalyst/s_code.js?v=<%=CmsUtil.GetJSVersion()%>"></script>
<!-- End Web Analythics JavaScripts -->

<script type="text/javascript" language="javascript">
s.pageName=""; 
s.pageType="errorPage";
s.prop41="404Page";

    var s_code = s.t();
    if (s_code) 
    document.write(s_code)//-->
</script>
<!-- Adobe Dynamic Tag Management Footer -->
<Scanweb:AdobeDTMFooter runat="server" /> 
<!-- End Adobe Dynamic Tag Management Footer -->
</body>
</html>