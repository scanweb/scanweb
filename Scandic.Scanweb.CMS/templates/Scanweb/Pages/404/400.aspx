﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="400.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages._404._00" %>
<%@ Register TagPrefix="Scanweb" TagName="SiteCatalystCloudTag" Src="~/Templates/Scanweb/Units/Static/SiteCatalystCloudTag.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="AdobeDTMFooter" Src="~/Templates/Scanweb/Units/Static/AdobeDTMFooter.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<% Response.StatusCode = 400; %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<meta charset="utf-8">
<title>400 Bad request : Scandic</title>
<Scanweb:SiteCatalystCloudTag runat="server" />
<link rel="stylesheet" href="error.css?v=<%=CmsUtil.GetCSSVersion()%>" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
	<div id="page-wrap">

        <div id="header">
            <img id="logo" src="/ImageVault/Images/id_14602/conversionFormatType_WebSafe/scope_0/ImageVaultHandler.aspx" alt="Scandic logo">
        </div>
        
        <div id="content" class="clearfix">
        
            <div id="sidebar">
                <h2>Contact us</h2>
                <p><strong>Scandic</strong><br>
                P.O. Box 6197<br>
                SE-102 33 Stockholm<br>
                Sweden<br>
                Visiting address: Sveav&#228;gen 167<br>
                Telephone: +46 8 517 350 00</p>
                
                <p><strong>If you like to make a reservation please contact Scandic Reservation and customer service at:</strong></p>
                
                <p>International: +46 8 517 517 20<br>
                Sweden:	08-517 517 00<br>
                Denmark: 33 48 04 00<br>
                Norway:	23 15 50 00<br>
                Finland: 0 200 818 00<br>
                or the hotel directly.</p>
            </div>
            
            <div id="main">
                <h1>bad request</h1>
              <p><strong>The request was denied due to a syntax error in the request: </strong><a href="http://www.scandichotels.com">www.scandichotels.com</a></p>
                
              <p><strong>Det gick inte att hitta webbsidan. <br></strong>
                (Webbl&#228;saren kan ansluta till webbservern, men det gick inte att hitta webbsidan p&#229; grund av ett problem med webbadressen url.)<strong><br>
              </strong><a href="http://www.scandichotels.se">www.scandichotels.se</a></p>
                
            <p><strong>Det var ikke muligt at finde websiden. </strong><br>
              (Internet Explorer kan tilslutte sig til webserveren, men det var ikke muligt at finde websiden p&#229; grund af et problem med webadressens url.)            <a href="http://www.scandichotels.dk">www.scandichotels.dk</a></p>
                
            <p><strong>Internetsivua ei l&#228;ytynyt. <br>
              </strong>(Internetselain pystyy yhdist&#228;m&#228;&#228;n internetserveriin, mutta internetsivua ei l&#228;ytynyt sivun URL-osoiteongelman takia.)            <a href="http://www.scandichotels.fi">www.scandichotels.fi</a></p>
                
                <p><strong>Websiden ble ikke funnet. <br>
                  </strong>(Webleser kan koble til webserveren, men fant ikke websiden p&#229; grunn av et problem med webadressens url.)                
                <a href="http://www.scandichotels.no">www.scandichotels.no</a></p>
				
		<p><strong>Die Webseite konnte nicht aufgerufen werden.<br>
                  </strong>(Die Anfrage konnte aufgrund eines Syntax-Fehlers in der Anfrage nicht ausgeführt werden.)                
                <a href="http://www.scandichotels.de">www.scandichotels.de</a></p>
				
		<p><strong>Показ страницы недоступен.<br>
                  </strong>(Запрос не может быть принят из-за ошибок в синтаксесе запроса.)                
                <a href="http://www.scandichotels.ru">www.scandichotels.ru</a></p>
				
            </div>
        </div>
        
        <div id="footer">
        	<p>&copy;Scandic</p>
        </div>
    
    </div>

	<!-- Google analytics code should go here -->
    </div>
      
    </form>


<!-- Web Analythics JavaScripts -->
<script type="text/javascript" language="javascript" src="/Templates/Scanweb/Javascript/SiteCatalyst/s_code.js?v=<%=CmsUtil.GetJSVersion()%>"></script>
<!-- End Web Analythics JavaScripts -->

<script type="text/javascript" language="javascript">
s.pageName=""; 
s.pageType="errorPage";
s.prop41="400Page";
/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_code=s.t();if(s_code)document.write(s_code)//--></script>
<script type="text/javascript" language="JavaScript"><!--
if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
//--></script><noscript><a href="http://www.omniture.com" title="Web Analytics"><img
src="http://schabdev.112.2O7.net/b/ss/schabdev/1/H.14--NS/0?[AQB]&cdp=3&[AQE]"
height="1" width="1" border="0" alt="" /></a></noscript><!--/DO NOT REMOVE/-->
<!-- End SiteCatalyst code version: H.14. -->
<!-- Adobe Dynamic Tag Management Footer -->
<Scanweb:AdobeDTMFooter runat="server" /> 
<!-- End Adobe Dynamic Tag Management Footer -->
</body>
</html>