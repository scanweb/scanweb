﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="409.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages._404._09" %>
<%@ Register TagPrefix="Scanweb" TagName="SiteCatalystCloudTag" Src="~/Templates/Scanweb/Units/Static/SiteCatalystCloudTag.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="AdobeDTMFooter" Src="~/Templates/Scanweb/Units/Static/AdobeDTMFooter.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<% Response.StatusCode = 409; %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<meta charset="utf-8">
<title>409 Conflict : Scandic</title>
<Scanweb:SiteCatalystCloudTag runat="server" />
<link rel="stylesheet" href="error.css?v=<%=CmsUtil.GetCSSVersion()%>" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="page-wrap">

        <div id="header">
            <img id="logo" src="/ImageVault/Images/id_14602/conversionFormatType_WebSafe/scope_0/ImageVaultHandler.aspx" alt="Scandic logo">
        </div>
        
        <div id="content" class="clearfix">
        
            <div id="sidebar">
                <h2>Contact us</h2>
                <p><strong>Scandic</strong><br>
                P.O. Box 6197<br>
                SE-102 33 Stockholm<br>
                Sweden<br>
                Visiting address: Sveav&#228;gen 167<br>
                Telephone: +46 8 517 350 00</p>
                
                <p><strong>If you like to make a reservation please contact Scandic Reservation and customer service at:</strong></p>
                
                <p>International: +46 8 517 517 20<br>
                Sweden:	08-517 517 00<br>
                Denmark: 33 48 04 00<br>
                Norway:	23 15 50 00<br>
                Finland: 0 200 818 00<br>
                or the hotel directly.</p>
            </div>
            
            <div id="main">
                <h1>Conflict</h1>
                <p><strong>The server has closed the socket due to communications between the client and server taking too long.<br>
                </strong>This could be due to server load, bandwidth issues, the client being disconnected from the internet, etc.<a href="http://www.scandichotels.com">www.scandichotels.com</a></p>
                
                <p><strong>Webbplatsen &#228;r f&#246;r h&#229;rt belastad f&#246;r att webbsidan ska kunna visas.</strong><br>
(Det tog f&#246;r l&#229;ng tid innan servern kunde visa webbsidan eller s&#229; begärde f&#246;r m&#229;nga anv&#228;ndare samma sida. F&#246;rs&#246;k igen senare.)<a href="http://www.scandichotels.se">www.scandichotels.se</a></p>
                
                <p><strong>Webpladsen er for h&#229;rdt belastet til at websiden skal kunne vises.</strong><br>
(Det tog for land tid inden serveren kunne vise websiden eller s&#229; s&#248;gte for mange brugere samme side. Fors&#248;g venligst igen senere.)<a href="http://www.scandichotels.dk">www.scandichotels.dk</a></p>
                
                <p><strong>Internetsivusto on liian kovassa k&#228;yt&#246;ss&#228; voidakseen n&#228;ytt&#228;&#228; internetsivua. </strong><br>
(Kesti liian kauan ennen kuin serveri pystyi n&#228;ytt&#228;m&#228;&#228;n internetsivua tai liian monta k&#228;ytt&#228;j&#228;&#228; pyrki samalle sivulle. Yrit&#228; my&#246;hemmin uudelleen.) <a href="http://www.scandichotels.fi">www.scandichotels.fi</a></p>
                
                <p><strong>Webplassen er for hardt belastet til at websiden kunne vises. </strong><br>
(Det tok for lang tid f&#248;r serveren kunne vise websiden eller for mange brukere s&#248;kte samme side. Pr&#248;v igjen senere) 
                <a href="http://www.scandichotels.no">www.scandichotels.no</a></p>
				
		<p><strong>Der Server hat die Verbindung eingestellt, da die Kommunikation zwischen Client und Server zu lange dauerte. </strong><br>
(Der Grund könnte die Serverauslastung sein, Probleme mit der Bandbreite, Abbruch der Verbindung zwischen Client und Internet, etc) <a href="http://www.scandichotels.de">www.scandichotels.de</a></p>

		<p><strong>Сетевое соединение было прервано между клиентом и сервером из-за задержки передачи запроса. </strong><br>
(Это может быть связано с загрузкой сервера, проблемами в сфере диапазона частот, выходом из сети и т.д.) <a href="http://www.scandichotels.ru">www.scandichotels.ru</a></p>
            </div>
        </div>
        
        <div id="footer">
        	<p>&copy;Scandic</p>
        </div>
    
    </div>

	<!-- Google analytics code should go here -->
    </div>
    </form>


<!-- Web Analythics JavaScripts -->
<script type="text/javascript" language="javascript" src="/Templates/Scanweb/Javascript/SiteCatalyst/s_code.js?v=<%=CmsUtil.GetJSVersion()%>"></script>
<!-- End Web Analythics JavaScripts -->

<script type="text/javascript" language="javascript">
 s.pageName=""; 
s.pageType="errorPage";
s.prop41="409Page";

/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_code=s.t();if(s_code)document.write(s_code)//--></script>
<script type="text/javascript" language="JavaScript"><!--
if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
//--></script><noscript><a href="http://www.omniture.com" title="Web Analytics"><img
src="http://schabdev.112.2O7.net/b/ss/schabdev/1/H.14--NS/0?[AQB]&cdp=3&[AQE]"
height="1" width="1" border="0" alt="" /></a></noscript><!--/DO NOT REMOVE/-->
<!-- End SiteCatalyst code version: H.14. -->
<!-- Adobe Dynamic Tag Management Footer -->
<Scanweb:AdobeDTMFooter runat="server" /> 
<!-- End Adobe Dynamic Tag Management Footer -->
</body>
</html>