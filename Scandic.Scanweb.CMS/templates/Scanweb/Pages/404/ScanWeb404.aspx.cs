using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.BookingEngine.Entity;
namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages
{
    /// <summary>
    /// ScanWeb404
    /// </summary>
    public partial class ScanWeb404 : System.Web.UI.Page
    {
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ancLogo404.HRef = GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE);
        }
    }
}
