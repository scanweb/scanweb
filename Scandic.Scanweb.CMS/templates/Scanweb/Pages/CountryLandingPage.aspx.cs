﻿using System;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using System.Web.Services;
using Scandic.Scanweb.Core;
using System.Collections.Generic;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Carousel;
using Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Templates.MasterPages;
using System.Web.UI.WebControls;
using Scandic.Scanweb.CMS.Util.ImageVault;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages
{
    public partial class CountryLandingPage : ScandicTemplatePage
    {
        protected string ajaxCallPath = "";
        public string AjaxCallPath
        {
            get { return string.Format("/{0}", ajaxCallPath); }
            set { ajaxCallPath = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                ShowAddthis();
            ajaxCallPath = "Templates/Scanweb/Pages/CountryLandingPage.aspx";
            ApplyPageData();
            FindAHotelSessionVariablesSessionWrapper.SelectedMapId = CurrentPage.PageLink.ID.ToString();
            BookingModuleBig bookingModule = udAddAnotherHotel as BookingModuleBig;
            bookingModule.Visible = OWSVisibilityControl.BookingModuleShouldBeVisible;
            AlternativeBookingModule.Visible = !OWSVisibilityControl.BookingModuleShouldBeVisible;
            bookingModule.IsBookingModuleReset = true;
            string view = Request.QueryString["initialview"];
            if (!string.IsNullOrEmpty(view))
            {
                if (Request.QueryString["initialview"].Equals("map"))
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "mapView", "CountryLandingSetMaps();", true);
                }
                else if (Request.QueryString["initialview"].Equals("list"))
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "listView", "CountryLandingSetList();", true);
                }
            }
            MasterPageFindAHotel master = (MasterPageFindAHotel)this.Master;
            master.ChangeVisibilityOfSubMenu(false);
            FindAHotelSessionVariablesSessionWrapper.SelectedNodeForMap = null;

            PlaceHolder metaContentPlaceHolder = (PlaceHolder)master.FindControl("phMetaForSchemaOrgCountryCityGeoCoordinates");
            if (metaContentPlaceHolder != null)
            {
                AddSchemaOrgCountryGeoInfoMetatags(metaContentPlaceHolder);
            }
        }

        [WebMethod]
        public static List<CountryDestinatination> GetCountryLandingJsonObject()
        {
            CountryCityDestinations countryCityDestinations = CountryCityDestinations.GetCountryCityDestinationsInstance();
            List<CountryDestinatination> countryCityList = new List<CountryDestinatination>();
            try
            {
                countryCityList = countryCityDestinations.GetCountryCityDestinations(null, EpiServerPageConstants.COUNTRY_LANDING);
                if (countryCityList == null || countryCityList.Count == 0)
                    OnError(countryCityList, "empty", WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/EmptyCountryList"));
            }
            catch (Exception ex)
            {
                OnError(countryCityList, "error", ex.Message);
            }
            return countryCityList;
        }

        private void ShowAddthis()
        {
            addThis.InnerHtml = Utility.GetAddthisScript(CurrentPage.LanguageID);
        }
        [WebMethod]
        public static string CityHotelLandingRedirect(string currentlyRenderedRoomTypes)
        {
            string[] pagelinksplit = currentlyRenderedRoomTypes.Split(':');
            PageData pageData = null;

            if (pagelinksplit[0].ToUpper() == "CITY")
                pageData = ContentDataAccess.GetCityPageDataByOperaID(Convert.ToString(pagelinksplit[1]));
            else if (pagelinksplit[0].ToUpper() == "HOTEL")
                pageData = ContentDataAccess.GetHotelPageDataByOperaID(Convert.ToString(pagelinksplit[1].ToString()), string.Empty);

            UrlBuilder url = new UrlBuilder(pageData.LinkURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, pageData.LinkURL, System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }
        private static void OnError(List<CountryDestinatination> countryCityList, string errorType, string errorMsg)
        {
            CountryDestinatination destination = new CountryDestinatination(null, null, null, null, null);
            destination.ClientMsg = string.Format("{0}{1}{2}{3}{4}{5}{6}", "<", errorType, ">", errorMsg, "<BR /></", errorType, ">");
            countryCityList.Add(destination);
        }
        private void ApplyPageData()
        {
            MainBody1.InnerHtml = Convert.ToString(CurrentPage["MainBody"]);
            countryid.Value = CurrentPage["CountryCode"].ToString();
            if (CurrentPage[CarouselConstants.ActivateImageCarousel] == null)
            {
                if (CurrentPage["TopImage"] != null)
                {
                    var langAltText = new LangAltText();
                    divTopImage.Visible = true;
                    TopImage.Visible = true;
                    var imageWidth = Convert.ToInt32(AppConstants.LandingPageTopImageWidth);
                    TopImage.ImageUrl = WebUtil.GetImageVaultImageUrl(CurrentPage["TopImage"].ToString(), imageWidth);

                    var altText = langAltText.GetAltText(CurrentPage.LanguageID, CurrentPage["TopImage"].ToString());
                    if (!string.IsNullOrEmpty(altText))
                    {
                        TopImage.AlternateText = altText;
                        TopImage.Attributes.Add("title", altText);
                    }
                }
            }
        }
        public int CurrentPageLinkId
        {
            get { return CurrentPage.PageLink.ID; }
        }
        public string CurrentPageTypeId
        {
            get { return CurrentPage.PageTypeID.ToString(); }
        }

        private void AddSchemaOrgCountryGeoInfoMetatags(PlaceHolder metaContentPlaceHolder)
        {
            if (metaContentPlaceHolder != null)
            {
                //Schema.Org Latitude for Country
                Utility.AddMetaInfoForSchemaOrg(metaContentPlaceHolder, "itemprop", "latitude", Convert.ToString(CurrentPage.Property["NothEastGeoY"]));
                //Schema.Org Longitude for Country
                Utility.AddMetaInfoForSchemaOrg(metaContentPlaceHolder, "itemprop", "longitude", Convert.ToString(CurrentPage.Property["NothEastGeoX"]));
            }
        }

    }
}
