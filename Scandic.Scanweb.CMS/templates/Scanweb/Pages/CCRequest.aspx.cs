////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  CCRequest                                              //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using EPiServer;

namespace Scandic.Scanweb.CMS.Templates.Pages
{
    /// <summary>
    /// Code behind of CCRequest page.
    /// </summary>
    public partial class CCRequest : TemplatePage
    {
    }
}