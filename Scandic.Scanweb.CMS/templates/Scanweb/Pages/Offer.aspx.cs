//  Description					: Offer                                                   //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                                                         //
///	Version	#					: 1.0													  //
///---------------------------------------------------------------------------------------//
/// Revison History				:                                                         //
///	Last Modified Date			:                                                         //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.code.Util.HotelOfferList;
using System.Web;
using Scandic.Scanweb.CMS.Util.MobileRedirect;


namespace Scandic.Scanweb.CMS.Templates.Pages
{
    /// <summary>
    /// Code behind of Offers page.
    /// </summary>
    public partial class Offer : ScandicTemplatePage
    {
        /// <summary>
        /// List of Hotels objects, each hotel have properties for the country and city the hotel is in.
        /// </summary>
        protected ListOfHotels HotelList;

        /// <summary>
        /// List of countrys, where each country contain a list of city's.
        /// </summary>
        protected List<OfferCountry> CountrysListFromXml;

        /// <summary>
        /// PriceType
        /// </summary>
        protected string PriceType;

        /// <summary>
        /// Hotel object.
        /// </summary>
        protected OfferHotel HotelsFromXml;

        /// <summary>
        /// List of City's, where each city contain a list of hotels.
        /// </summary>
        protected OfferCity CitysFromXml;

        /// <summary>
        /// 
        /// </summary>
        protected OfferCountry CountrysFromXml;

        /// <summary>
        /// 
        /// </summary>
        public Offer()
        {
            HotelList = new ListOfHotels();
            CountrysListFromXml = new List<OfferCountry>();
        }

        protected string activateimagecarousel;

        private string GetMobileOfferURL()//string linkURL, string language, PageReference pageLink
        {
            PageData pageData = GetPage(PageReference.RootPage);
            PageData pageOfferContainer = ContentDataAccessManager.GetPageData(((PageReference)pageData["OfferContainer"]));
            PageData mobileOfferPage = null;
            string finalUrl = string.Empty;
            if (pageOfferContainer != null)
                mobileOfferPage = ContentDataAccessManager.GetPageData(((PageReference)pageOfferContainer["MobileOfferDetailPage"]));
            if (mobileOfferPage != null)
            {
                //finalUrl = WebUtil.GetExternalUrl(mobileOfferPage.LinkURL);
                string languageSelectionURL = EPiServer.UriSupport.AddLanguageSelection(mobileOfferPage.LinkURL, CurrentPage.LanguageID);
                string fullLanguageSelectionURL = EPiServer.UriSupport.AbsoluteUrlBySettings(languageSelectionURL);
                UrlBuilder finalUrlBuilder = new UrlBuilder(fullLanguageSelectionURL);
                EPiServer.Global.UrlRewriteProvider.ConvertToExternal(finalUrlBuilder, mobileOfferPage.PageLink, System.Text.UTF8Encoding.UTF8);
                finalUrl = finalUrlBuilder.ToString();
                string hostName = "ScandicMobileSiteHostName{0}";
                string appSettingsKey = string.Format(hostName, CurrentPage.LanguageID.ToUpper());
                string hostURL = ConfigurationManager.AppSettings[appSettingsKey] as string;
                Url url = new Url(fullLanguageSelectionURL);
                if (!string.IsNullOrEmpty(hostURL))
                {
                    finalUrl = finalUrl.Replace(url.Host, hostURL);
                    finalUrl = finalUrl.Replace("/" + CurrentPage.LanguageID + "/", "/");
                }
                finalUrl = string.Format("{0}{1}{2}", finalUrl, "?pageid=", CurrentPage.PageLink.ID);
                var cmpidValue = Request.QueryString["cmpid"];
                if(!string.IsNullOrEmpty(cmpidValue))
                {
                    finalUrl = string.Format("{0}&cmpid={1}", finalUrl, cmpidValue);
                }                
            }
            return finalUrl;
        }


        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ShowAddthis();
            }

            if (CurrentPage["OfferEndTime"] != null && Convert.ToDateTime(CurrentPage["OfferEndTime"]) < DateTime.Now)
            {
                PageData pageData = GetPage(PageReference.RootPage);
                PageData pageOfferContainer = ContentDataAccessManager.GetPageData(((PageReference)pageData["OfferContainer"]));
                OfferExpiryText.InnerHtml = Convert.ToString(pageOfferContainer["OfferExpiryText"]);
                OfferExpiryDiv.Visible = true;
            }

            if (!WebUtil.IsNonSupportedMobileLanguage(Request))
            {
                //Redirect to Mobile Offer if Offer deeplink access from Mobile Device
                if (Wurfl.Instance.IsMobileDevice(Request.UserAgent) && !Wurfl.Instance.IsTablet(Request.UserAgent))
                    Response.Redirect(GetMobileOfferURL());

                //Adding Alternate Link Tag for Mobile Offers
                Control cntrl = this.Master.FindControl("AllOffersHeader");
                if (cntrl != null)
                {
                    PlaceHolder linkPH = (PlaceHolder)cntrl.FindControl("plhLinkOffer");
                    if (linkPH != null)
                    {
                        string mobileOfferURL = string.Empty;
                        mobileOfferURL = GetMobileOfferURL();
                        HtmlLink link = new HtmlLink();
                        link.Attributes.Add("rel", "alternate");
                        link.Attributes.Add("media", Convert.ToString(ConfigurationManager.AppSettings["LinkTagAlternate"]));
                        link.Href = mobileOfferURL;
                        linkPH.Controls.Add(link);
                    }
                }
            }

            PriceListOfferPlaceHolder.Visible = false;

            string PromotionCode = CurrentPage["PromotionCode"] as string ?? string.Empty;
            if (PromotionCode != string.Empty)
            {
                PopulateBookingModule.PromotionCode = PromotionCode;
            }

            string DestName = CurrentPage["PromotionDestination"] as string ?? string.Empty;
            if (DestName != string.Empty)
            {
                PopulateBookingModule.DestinationName = DestName;
            }

            activateimagecarousel = CurrentPage["ActivateImageCarousel"] != null ? CurrentPage["ActivateImageCarousel"].ToString() : "false";

        }         

        public string ActivateImageCarousel
        {

            get { return activateimagecarousel; }
        }


        public int CurrentPageLinkId
        {
            get { return CurrentPage.PageLink.ID; }

        }


        public string CurrentPageTypeId
        {
            get
            {
                return CurrentPage.PageTypeID.ToString();

            }

        }

        /// <summary>
        /// Gets country by contry Id
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public OfferCountry GetCountryObject(string countryID)
        {
            foreach (OfferCountry tempCountry in CountrysListFromXml)
            {
                if (tempCountry.CountryID == countryID)
                    return tempCountry;
            }
            return null;
        }

        /// <summary>
        /// Gets price type
        /// </summary>
        /// <returns></returns>
        public string GetPriceType()
        {
            return PriceType;
        }

        /// <summary>
        /// Gets city object by cityId
        /// </summary>
        /// <param name="CityID"></param>
        /// <param name="citylist"></param>
        /// <returns></returns>
        public OfferCity GetCityObject(string CityID, List<OfferCity> citylist)
        {
            foreach (OfferCity tempCity in citylist)
            {
                if (tempCity.CityID == CityID)
                    return tempCity;
            }
            return null;
        }

        /// <summary>
        /// Populates a list of hotels sorted by it's country and city. 
        /// </summary>
        /// <param name="xml"></param>
        protected bool PopulateList()
        {
            string priceListXML = CurrentPage["PriceListXML"] as string;
            XmlDocument xml = new XmlDocument();

            try
            {
                xml.LoadXml(priceListXML);
            }
            catch (Exception e)
            {
                return false;
            }

            XmlNodeList prices = xml.SelectNodes("/pricelist/price");

            string HotelID = string.Empty;
            string PriceA = string.Empty;
            string PriceB = string.Empty;
            string Currency = string.Empty;

            string PriceC = string.Empty;
            string SpecialRateName = string.Empty;

            PriceType = "Meeting";

            if (prices != null && prices.Count > 0)
            {
                if (!string.Empty.Equals(prices[0].SelectSingleNode("flex").InnerText)
                    || !string.Empty.Equals(prices[0].SelectSingleNode("early").InnerText)
                    || !string.Empty.Equals(prices[0].SelectSingleNode("specialrate").InnerText))
                    PriceType = "FlexEarlySpecial";
            }

            foreach (XmlNode node in prices)
            {
                PriceC = string.Empty;
                SpecialRateName = string.Empty;
                foreach (XmlNode child in node)
                {
                    if (PriceType.Equals("FlexEarlySpecial"))
                    {
                        switch (child.Name)
                        {
                            case "hotelid":
                                HotelID = child.InnerText;
                                break;
                            case "early":
                                PriceA = IsNumeric(child.InnerText) ? child.InnerText : string.Empty;
                                break;
                            case "flex":
                                PriceB = IsNumeric(child.InnerText) ? child.InnerText : string.Empty;
                                break;
                            case "specialrate":
                                PriceC = IsNumeric(child.InnerText) ? child.InnerText : string.Empty;
                                break;
                            case "specialratename":
                                SpecialRateName = child.InnerText;
                                break;
                            case "currency":
                                Currency = child.InnerText;
                                break;
                        }
                    }
                    else if (PriceType.Equals("Meeting"))
                    {
                        switch (child.Name)
                        {
                            case "hotelid":
                                HotelID = child.InnerText;
                                break;
                            case "meetinga":
                                PriceA = IsNumeric(child.InnerText) ? child.InnerText : string.Empty;
                                break;
                            case "meetingb":
                                PriceB = IsNumeric(child.InnerText) ? child.InnerText : string.Empty;
                                break;
                            case "currency":
                                Currency = child.InnerText;
                                break;
                        }
                    }
                }

                if (HotelList.CheckIfHotelExcistsInList(HotelID) &&
                    (!string.Empty.Equals(PriceA) || !string.Empty.Equals(PriceB)
                     || !string.Empty.Equals(PriceC)) && !string.Empty.Equals(Currency))
                {
                    HotelsFromXml = new OfferHotel(HotelID, HotelList.GetHotelName(HotelID), PriceA, PriceB, PriceC,
                                                   SpecialRateName, Currency);
                    if (GetCountryObject(HotelList.GetCountryID(HotelID)) == null)
                    {
                        CitysFromXml = new OfferCity(HotelList.GetCityID(HotelID), HotelList.GetCityName(HotelID),
                                                     HotelsFromXml);
                        CountrysFromXml = new OfferCountry(HotelList.GetCountryID(HotelID),
                                                           HotelList.GetCountryName(HotelID), CitysFromXml);
                        CountrysListFromXml.Add(CountrysFromXml);
                    }
                    else
                    {
                        CountrysFromXml = GetCountryObject(HotelList.GetCountryID(HotelID));
                        if (GetCityObject(HotelList.GetCityID(HotelID), CountrysFromXml.GetListOfCity()) == null)
                        {
                            CitysFromXml = new OfferCity(HotelList.GetCityID(HotelID), HotelList.GetCityName(HotelID),
                                                         HotelsFromXml);
                            CountrysFromXml.AddCityToCountry(CitysFromXml);
                        }
                        else
                        {
                            CitysFromXml = GetCityObject(HotelList.GetCityID(HotelID), CountrysFromXml.GetListOfCity());
                            if (!CitysFromXml.CheckIfHotelExcistsInCity(HotelID))
                                CitysFromXml.AddHotelToCity(HotelsFromXml);
                        }
                    }
                }
            }
            if (CountrysListFromXml.Count > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Gets country list name.
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        protected string GetCountryListName(OfferCountry country)
        {
            return country.CountryName;
        }

        /// <summary>
        /// Checks is numeric or not.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        protected bool IsNumeric(string str)
        {
            int temp;
            if (Int32.TryParse(str, out temp))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Gets city list name.
        /// </summary>
        /// <param name="City"></param>
        /// <returns></returns>
        protected string GetCityListName(OfferCity City)
        {
            return City.CityName;
        }

        private void ShowAddthis()
        {
            addThis.InnerHtml = Utility.GetAddthisScript(CurrentPage.LanguageID);
        }
    }
}