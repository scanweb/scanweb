﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CountryLandingPage.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageFindAHotel.master"   Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages.CountryLandingPage" %>
<%@ Register TagPrefix="Scanweb" TagName="DestinationSearch" Src="~/Templates/Booking/Units/DestinationSearch.ascx" %>
<%@ Register TagPrefix="Booking" TagName="BookingModule" Src="~/Templates/Booking/Units/BookingModuleBig.ascx" %>
<%@ Register TagPrefix="Booking" TagName="FlyOut" Src="~/Templates/Booking/Units/BookingModuleFlyout.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="BookingModuleAlternativeBox" Src="~/Templates/Scanweb/Units/Placeable/BookingModuleAlternativeBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="DestinationLandingPageGoogleMapIframe" Src="~/Templates/Scanweb/Units/Static/DestinationLandingPageGoogleMapIframe.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="LeftMenu" Src="~/Templates/Scanweb/Units/Placeable/LeftMenu.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="LeftColumn" Src="~/Templates/Scanweb/Units/Placeable/LeftColumn.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
 <div id="OverviewPageTopArea" class="formular">
 <div runat="server" id="divTopImage" visible="false" class="mgnBtm20">
	<asp:Image ID="TopImage" runat="server" Width="718" Height="265"/>    
</div>
 <div id="slides"></div>
 <input type="hidden" runat="server" id="countryid" />
 <div id ="addThis" runat="server" class="addthis_toolbox addthis_default_style" style="float: right; margin-bottom: 10px;">
 </div>  
 <div id="CountryBodyLeftArea">
 <div id="findStay">         
			<h3 class="darkHeading headingPurple"> <label for="txtFindaHotel"><episerver:translate id="Translate1" text="/Templates/Scanweb/Units/Static/FindHotelSearch/WhereDoYouWantToStay"
                runat="server" /></label></h3>
			<div id="ErrorFGP" class="redAlertIcon"></div>  
            <p class="formRow">  
                <input type="text" autocomplete="off" class="input defaultColor" tabindex="1" id="txtFindaHotel" name="txtFindaHotel" rel="<%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/EnterDestination")%>" value="<%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/EnterDestination")%>"  >
                <input type="hidden" id="selectedDestinationFYD" runat="server" />
                <input type="hidden" id="notmatchingDestination" value="<%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/requiredfielderror/destination_hotel_name")%>" />
            
			<a onclick="validateFindYourDestination();" tabindex="2" id="findYourDest" class="buttonInner brandSearchBtn scansprite" ><span><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/RedemptionPoints/RedemptionPointsSearchButton")%></span></a>
			</p>           
            <div id="autosuggestCountryLanding" class="autosuggest">
                <ul>
                </ul>
                <iframe src="javascript:false;" frameborder="0" scrolling="no"></iframe>
            </div> 
   	</div>
	<div class="HotelLocationHeadLine HotelLocationHeadLineno">
				<h3 class="darkHeading"><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/HotelsByCountry")%></h3>
    </div>
	<div class="HotelOverviewTopRight" id="CountryLinkContainer">            
	</div>   
   </div>
   <div id="CountryBodyRightArea"> 
        <div class="countryLandingpage">
            <p runat="server" id="MainBody1"></p>
            <div class="scandicTabs" id="tabWrapper">
                <ul>
                <li><a class="active tabs" rel=".listView" href="#" id="listViewTab"><span><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/ShowAsList")%></span></a></li>
                <li><a rel=".map" class="tabs" href="#" id="mapTab"><span><%= WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/ShowOnMap")%></span></a></li>
                </ul>
            <div class="clearAll"></div>
            </div>
			<div class="tabCntnt default listView" id="divList">
			<div class="countryCityContainer">
			</div>
			</div>
			<div class="tabCntnt map gmapclickevent"  id="divMap" >
                <div class="booking-progress" id="BookingListProgressDiv" style="display: none; text-align:center;padding-top: 100px;">
                    <img src=<%=ResolveUrl("~/Templates/Booking/Styles/Default/Images/rotatingclockani.gif")%>
                    alt="ProgressBar" align="middle" width="25px" height="25px" />
                </div>
			<Scanweb:DestinationLandingPageGoogleMapIframe id="DestinationLandingPageGoogleMapIframe"  runat="server" />
			</div>
        </div>    
        </div>    
    </div>
    <div id="destinationSearch">
        <Scanweb:DestinationSearch id="DestinationSearch1" runat="server" />
    </div>
     <div class="conf_Popup">
        <div class="jqmWindow dialog" id="dialog" style="z-index:30000; display:none;">
            <Booking:FlyOut ID="bookingFlyOut"></Booking:FlyOut>
            <div class="guestInfoPopup" id="RoomCategoryContainer">
                <div id="LeftContentArea">
                            <div class="closeBtn">
                        <a href="#" class="jqmClose scansprite blkClose" style="width:30px;height:27px;padding:0px;">&nbsp;</a></div>
                    <div id="BookingArea">
                        <Booking:BookingModule id="udAddAnotherHotel" runat="server"></Booking:BookingModule>
                        <Scanweb:BookingModuleAlternativeBox CssClass="AlternativeBookingBoxLarge" ID="AlternativeBookingModule" runat="server" Visible="false"/>
                    </div>
                </div>
            </div>
        </div>
        </div>
	<script type="text/javascript">
    var Url = "<%=AjaxCallPath %>";
    GetCountryLandingJsonObject();
    CarouselProcessing('<%=CurrentPageTypeId%>', '<%=CurrentPageLinkId%>');
         $(document).ready(function() {
         $('#mapTab').one('click', SetMaps);
             $('#ErrorFGP').hide();
             $('#findYourDest').bind('keypress', function(e) {
                 if (e.keyCode == 13 || e.which == 13 || e.keyCode == 32 || e.which == 32) {
                     validateFindYourDestination();
                 }
             });
             var yourDestination = _endsWith("txtFindaHotel");
             if (yourDestination != null) {
                 new AutoSuggest($fn(yourDestination), 'autosuggestCountryLanding', 'selectedDestinationFYD', false);
             }
         });
       
         function SetMaps() {
             $('#ErrorFGP').hide();
             ProcessCountryCityDestinationMap();
            
         }
         function remove_qs(url) {
    var a = document.createElement('a'); // dummy element
    a.href = url;   // set full url
    a.search = "";  // blank out query string
    return a.href;
}
         function CountryLandingSetMaps()
            {
                $('#listViewTab').removeClass('active');
                $('#mapTab').removeClass('tabs');
                $('#mapTab').addClass('active tabs');
                $('#divMap').show();
                $('#divList').hide();
              
                SetMaps();

            }
            function CountryLandingSetList()
            {
          
                $('#mapTab').removeClass('active');
                $('#listViewTab').removeClass('tabs');
                $('#listViewTab').addClass('active tabs');
                $('#divMap').hide();
                $('#divList').show();
            }
          function populateDestination(destinationText, destinationValue) {


             $('#dialog').jqm();
             var control = $fn(_endsWith('txtHotelName'));
             if (null != control) {
                 control.value = destinationText;
             }
             var controlBc = $fn(_endsWith('txtHotelNameBNC'));
             if (null != controlBc) {
                 controlBc.value = destinationText;
             }
             var controlRB = $fn(_endsWith('txtHotelNameRewardNights'));
             if (null != controlRB) {
                 controlRB.value = destinationText;
             }
             var dest = $fn(_endsWith('selectedDestId'));
             if (null != dest) {
                 dest.value = destinationValue;
             }
             resetBooikingModule();
             $('#dialog').jqmShow();
			  $('.closeBtn').css('margin-left','476px');
            
         }
         function resetBooikingModule() {
             var errDiv = $fn(_endsWith('clientErrorDiv'));
             if (errDiv.style.visibility != isHidden || errDiv.style.display != "none") {
                 errDiv.style.visibility = isHidden;
                 errDiv.style.display = "none";
                 var controls = new Array("lblUserName", "lblPassword", "destCont");
                 for (var i = 0; i < controls.length; i++) {
                     var ctrlId = $fn(_endsWith(controls[i]));
                     if (null != ctrlId)
                     { ctrlId.className = ""; }
                 }
                 var ddlBT = $fn(_endsWith('ddlBookingType'));
                 if (null != ddlBT)
                 { ddlBT.selectedIndex = 0; }
             }
			  $('.closeBtn').css('margin-left','477px');
         }
	</script>
	<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50645171248e42f5"></script>
</asp:Content>