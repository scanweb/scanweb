//  Description					: MoreImagesPopUpContainer                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer;
using EPiServer.Core;
using EPiServer.Security;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages
{
    /// <summary>
    /// Code behind of MoreImagesPopUpContainer page.
    /// </summary>
    public partial class MoreImagesPopUpContainer : ScandicTemplatePage
    {
        /// <summary>
        /// Gets/Sets HotelPage
        /// </summary>
        public PageData HotelPage { get; set; }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int hotelID = -1;
            if (int.TryParse(Request.QueryString.Get("hotelid"), out hotelID))
            {
                if (hotelID != -1)
                {
                    HotelPage = DataFactory.Instance.GetPage(new PageReference(hotelID), AccessLevel.NoAccess);
                    MoreImagesPopup1.HotelPage = HotelPage;
                }
            }
        }
    }
}