<%@ Page language="c#" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWide.master" AutoEventWireup="False" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages.ContactUs" Codebehind="ContactUs.aspx.cs" %>
<%@ Register TagPrefix="Scanweb" TagName="MainBody"  Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="ContactUs"  Src="~/Templates/Booking/Units/CustomerServiceContactUs.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyLeftRegion" runat="server">
	<div id="MainBody">
	    <Scanweb:SquaredCornerImage ImagePropertyName="ContentTopImage" 
                                 TopCssClass="RoundedCornersTop472" 
                                 ImageCssClass="RoundedCornersImage472" 
                                 BottomCssClass="RoundedCornersBottom472" ImageWidth="472" runat="server" />
	    <Scanweb:MainBody ID="MainBody1" runat="server" />
	    <Scanweb:ContactUs ID="ContactUs1" runat="server" />
	    
    </div>
</asp:Content>