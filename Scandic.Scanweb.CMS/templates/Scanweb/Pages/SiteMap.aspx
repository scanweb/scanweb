<%@ Page language="c#" Inherits="Scandic.Scanweb.CMS.Templates.SiteMap" Codebehind="SiteMap.aspx.cs" 
        MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageStart.master"%>

<%@ Register TagPrefix="Scanweb" TagName="TopPageHeader" Src="~/Templates/Scanweb/Units/Placeable/TopPageHeader.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="Breadcrumbs" Src="~/Templates/Scanweb/Units/Static/Breadcrumbs.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="SiteMap" Src="~/Templates/Scanweb/Units/Static/SiteMap.ascx" %>

<asp:Content ContentPlaceHolderID="MainRegion" runat="server">
  <div id="BreadcrumbsArea">
           <Scanweb:Breadcrumbs ID="Breadcrumbs" runat="server" />
   </div>
  <div id="PageHeaderArea" class="sitemapHeader">
    <Scanweb:TopPageHeader runat="server" />
  </div>
   
  <Scanweb:SiteMap runat="server" />
</asp:Content>

 
  