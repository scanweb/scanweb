//  Description					: ConfirmationEmailPage                                   //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region System namespaces

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Text;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System.Web;

#endregion System namespaces

#region Scandic Namespace

#endregion

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages
{
    /// <summary>
    /// Code behind of confirmation email page.
    /// </summary>
    public partial class ConfirmationEmailPage : System.Web.UI.Page
    {
        private bool showIndivisualGuarantee = false;
        private string bookingType = string.Empty;

        private Hashtable htblLegs = null;
        private string cancelNumbers = string.Empty;
        private int cancelCounter = 0;

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bookingType = ((System.Web.HttpContext.Current.Request.QueryString["BOOKINGTYPE"]) != null)
                                  ? (System.Web.HttpContext.Current.Request.QueryString["BOOKINGTYPE"].ToString())
                                  : string.Empty;
                showIndivisualGuarantee = IsIndividualGuarantee();
                SetConfimationPageImageSource();
                setConfimationPageFields();
                bool isAllRoomsTobeShown = true;
                int roomNumberToBeDisplayed = -1;
                if (System.Web.HttpContext.Current.Request.QueryString["AllROOMSTOBESHOWN"] != null)
                {
                    isAllRoomsTobeShown =
                        Convert.ToBoolean((System.Web.HttpContext.Current.Request.QueryString["AllROOMSTOBESHOWN"]));
                }
                if (System.Web.HttpContext.Current.Request.QueryString["ROOMNUMBERTOBEDISPLAYED"] != null)
                {
                    roomNumberToBeDisplayed =
                        Convert.ToInt32((System.Web.HttpContext.Current.Request.QueryString["ROOMNUMBERTOBEDISPLAYED"]));
                }
                SetAboutOurRatesDetails(isAllRoomsTobeShown, roomNumberToBeDisplayed);
                if (Request.QueryString["command"] != null)
                {
                    if (Request.QueryString["command"].ToString() == "CancelPageprint")
                    {
                        aboutOurRateHeaderDiv.Visible = false;
                    }
                }
            }
            catch (Exception exp)
            {
                AppLogger.LogFatalException(exp, "Exception occured in EMail Page");
            }
        }

        /// <summary>
        /// SetAboutOurRatesDetails
        /// </summary>
        /// <param name="isAllRoomsTobeShown"></param>
        /// <param name="roomNumberToBeDisplayed"></param>
        private void SetAboutOurRatesDetails(bool isAllRoomsTobeShown, int roomNumberToBeDisplayed)
        {
            Hashtable selectedRoomRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
            SelectedRoomAndRateEntity selectedRoomRates = null;
            List<RateDescriptionforEMAIL> uniqueRateDescriptionBoxes = new List<RateDescriptionforEMAIL>();
            RateCategory rate = null;
            Block block = null;
            string RatePlanCode = string.Empty;
            List<string> uniqueCategories = new List<string>();
            if (selectedRoomRatesHashTable != null)
            {
                if (isAllRoomsTobeShown)
                {
                    for (int i = 0; i < selectedRoomRatesHashTable.Count; i++)
                    {
                        selectedRoomRates = selectedRoomRatesHashTable[i] as SelectedRoomAndRateEntity;
                        if (selectedRoomRates != null && selectedRoomRates.RoomRates != null)
                        {
                            if (Utility.IsBlockCodeBooking)
                            {
                                block =
                                    Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(
                                        SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                                if (!uniqueCategories.Contains(block.BlockID))
                                {
                                    uniqueCategories.Add(block.BlockID);
                                    RateDescriptionforEMAIL rateDescriptionForBlock =
                                        LoadControl("/Templates/Booking/Units/RateDescriptionforEMAIL.ascx") as
                                        RateDescriptionforEMAIL;
                                    IList<BaseRoomRateDetails> listRoomRateDetails = null;
                                    if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null)
                                        listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                                    if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                                    {
                                        for (int CategoryCount = 0;
                                             CategoryCount < listRoomRateDetails.Count;
                                             CategoryCount++)
                                        {
                                            BaseRoomRateDetails roomRateDetails = listRoomRateDetails[CategoryCount];
                                            BaseRateDisplay rateDisplay =
                                                RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                                            foreach (
                                                RateCategoryHeaderDisplay rateHeaderDisplay in
                                                    rateDisplay.RateCategoriesDisplay)
                                            {
                                                if (rateHeaderDisplay.Title == block.BlockName)
                                                {
                                                    rateDescriptionForBlock.RateCaegoryDetails(rateHeaderDisplay);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        rateDescriptionForBlock.RateCategoryDetails(block);
                                    }
                                    uniqueRateDescriptionBoxes.Add(rateDescriptionForBlock);
                                }
                            }
                            else if (selectedRoomRates.RoomRates.Count > 0 &&
                                     selectedRoomRates.RoomRates[0].RatePlanCode != null)
                            {
                                RatePlanCode = selectedRoomRates.RoomRates[0].RatePlanCode;
                                rate = RoomRateUtil.GetRateCategoryByRatePlanCode(RatePlanCode);
                                if (!uniqueCategories.Contains(rate.RateCategoryId))
                                {
                                    uniqueCategories.Add(rate.RateCategoryId);
                                    RateDescriptionforEMAIL rateDescription =
                                        LoadControl("/Templates/Booking/Units/RateDescriptionforEMAIL.ascx") as
                                        RateDescriptionforEMAIL;
                                    IList<BaseRoomRateDetails> listRoomRateDetails = null;
                                    if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null)
                                        listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                                    if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                                    {
                                        for (int CategoryCount = 0;
                                             CategoryCount < listRoomRateDetails.Count;
                                             CategoryCount++)
                                        {
                                            BaseRoomRateDetails roomRateDetails = listRoomRateDetails[CategoryCount];
                                            BaseRateDisplay rateDisplay =
                                                RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                                            foreach (
                                                RateCategoryHeaderDisplay rateHeaderDisplay in
                                                    rateDisplay.RateCategoriesDisplay)
                                            {
                                                if (rateHeaderDisplay.Id == rate.RateCategoryId)
                                                {
                                                    rateDescription.RateCaegoryDetails(rateHeaderDisplay);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        rateDescription.RateCaegoryDetails(rate);
                                    }
                                    uniqueRateDescriptionBoxes.Add(rateDescription);
                                }
                            }
                        }
                    }
                }

                else
                {
                    selectedRoomRates = selectedRoomRatesHashTable[roomNumberToBeDisplayed] as SelectedRoomAndRateEntity;
                    if (selectedRoomRates != null && selectedRoomRates.RoomRates != null)
                    {
                        if (Utility.IsBlockCodeBooking)
                        {
                            block =
                                Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(
                                    SearchCriteriaSessionWrapper.SearchCriteria.CampaignCode);
                            if (!uniqueCategories.Contains(block.BlockID))
                            {
                                uniqueCategories.Add(block.BlockID);
                                RateDescriptionforEMAIL rateDescriptionForBlock =
                                    LoadControl("/Templates/Booking/Units/RateDescriptionforEMAIL.ascx") as
                                    RateDescriptionforEMAIL;

                                IList<BaseRoomRateDetails> listRoomRateDetails = null;
                                if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null)
                                    listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                                if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                                {
                                    for (int CategoryCount = 0;
                                         CategoryCount < listRoomRateDetails.Count;
                                         CategoryCount++)
                                    {
                                        BaseRoomRateDetails roomRateDetails = listRoomRateDetails[CategoryCount];
                                        BaseRateDisplay rateDisplay =
                                            RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                                        foreach (
                                            RateCategoryHeaderDisplay rateHeaderDisplay in
                                                rateDisplay.RateCategoriesDisplay)
                                        {
                                            if (rateHeaderDisplay.Title == block.BlockName)
                                            {
                                                rateDescriptionForBlock.RateCaegoryDetails(rateHeaderDisplay);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    rateDescriptionForBlock.RateCategoryDetails(block);
                                }
                                uniqueRateDescriptionBoxes.Add(rateDescriptionForBlock);
                            }
                        }
                        else if (selectedRoomRates.RoomRates.Count > 0 &&
                                 selectedRoomRates.RoomRates[0].RatePlanCode != null)
                        {
                            RatePlanCode = selectedRoomRates.RoomRates[0].RatePlanCode;

                            rate = RoomRateUtil.GetRateCategoryByRatePlanCode(RatePlanCode);
                            if (!uniqueCategories.Contains(rate.RateCategoryId))
                            {
                                uniqueCategories.Add(rate.RateCategoryId);
                                RateDescriptionforEMAIL rateDescription =
                                    LoadControl("/Templates/Booking/Units/RateDescriptionforEMAIL.ascx") as
                                    RateDescriptionforEMAIL;

                                IList<BaseRoomRateDetails> listRoomRateDetails = null;
                                if (HotelRoomRateSessionWrapper.ListHotelRoomRate != null)
                                    listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;
                                if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                                {
                                    for (int CategoryCount = 0;
                                         CategoryCount < listRoomRateDetails.Count;
                                         CategoryCount++)
                                    {
                                        BaseRoomRateDetails roomRateDetails = listRoomRateDetails[CategoryCount];
                                        BaseRateDisplay rateDisplay =
                                            RoomRateDisplayUtil.GetRateDisplayObject(roomRateDetails);
                                        foreach (
                                            RateCategoryHeaderDisplay rateHeaderDisplay in
                                                rateDisplay.RateCategoriesDisplay)
                                        {
                                            if (rateHeaderDisplay.Id == rate.RateCategoryId)
                                            {
                                                rateDescription.RateCaegoryDetails(rateHeaderDisplay);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    rateDescription.RateCaegoryDetails(rate);
                                }
                                uniqueRateDescriptionBoxes.Add(rateDescription);
                            }
                        }
                    }
                }
            }
            foreach (RateDescriptionforEMAIL rateDescription in uniqueRateDescriptionBoxes)
            {
                AboutRate.Controls.Add(rateDescription);
            }
            if (Reservation2SessionWrapper.AboutOurRateHeading != null)
                aboutOurRateHeading.Text = Reservation2SessionWrapper.AboutOurRateHeading;
            if (Reservation2SessionWrapper.AboutOurRateDescription != null)
                aboutOurRateDescription.Text = Reservation2SessionWrapper.AboutOurRateDescription;
        }
        #region Private Methods

        /// <summary>
        /// Sets the confimation page image source.
        /// </summary>
        private void SetConfimationPageImageSource()
        {
            string authority = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            logoImage.ImageUrl = authority + ResolveUrl("/templates/Scanweb/Images/Images_Email/logo.gif");
            h1BorderImage.ImageUrl = authority + ResolveUrl("/templates/Scanweb/Images/Images_Email/h1_Border.gif");
            boxBigBrd.ImageUrl = authority + ResolveUrl("/templates/Scanweb/Images/Images_Email/boxBigBrd_top.gif");
            boxBigBrdBottom.ImageUrl = authority +
                                       ResolveUrl("/templates/Scanweb/Images/Images_Email/boxBigBrd_btm.gif");
            //infoAlertTop.ImageUrl = authority + ResolveUrl("/templates/Scanweb/Images/Images_Email/infoAlert_top.gif");
            //infoAlertIcon.ImageUrl = authority + ResolveUrl("/templates/Scanweb/Images/Images_Email/infoAlert_icon.gif");
            //infoAlertIconBottom.ImageUrl = authority +
            ResolveUrl("/templates/Scanweb/Images/Images_Email/infoAlert_btm.gif");
            aboutOurRateTopBorder.ImageUrl = authority +
                                             ResolveUrl("/templates/Scanweb/Images/Images_Email/singleBrdrBox_top.gif");
            aboutOurRateBottomBorder.ImageUrl = authority +
                                                ResolveUrl(
                                                    "/templates/Scanweb/Images/Images_Email/singleBrdrBox_btm.gif");
            masterReservationHelpTopBorder.ImageUrl = authority +
                                                      ResolveUrl(
                                                          "/templates/Scanweb/Images/Images_Email/infoAlert_top.gif");
            masterReservationHelpBottomBorder.ImageUrl = authority +
                                                         ResolveUrl(
                                                             "/templates/Scanweb/Images/Images_Email/infoAlert_btm.gif");
            contactUsTopBorder.ImageUrl = authority +
                                          ResolveUrl("/templates/Scanweb/Images/Images_Email/infoAlert_top.gif");
            contactUsBottomBorder.ImageUrl = authority +
                                             ResolveUrl("/templates/Scanweb/Images/Images_Email/infoAlert_btm.gif");
        }

        /// <summary>
        /// Sets the confimation page fields.
        /// </summary>
        private void setConfimationPageFields()
        {
            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria as HotelSearchEntity;
            Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;

            SortedList<string, GuestInformationEntity> guestsList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;

            bool isAllRoomsTobeShown = true;
            int roomNumberToBeDisplayed = -1;
            if (System.Web.HttpContext.Current.Request.QueryString["AllROOMSTOBESHOWN"] != null)
            {
                isAllRoomsTobeShown =
                    Convert.ToBoolean((System.Web.HttpContext.Current.Request.QueryString["AllROOMSTOBESHOWN"]));
            }
            if (System.Web.HttpContext.Current.Request.QueryString["ROOMNUMBERTOBEDISPLAYED"] != null)
            {
                roomNumberToBeDisplayed =
                    Convert.ToInt32((System.Web.HttpContext.Current.Request.QueryString["ROOMNUMBERTOBEDISPLAYED"]));
            }
            if (hotelSearch != null && PageIdentifier.CancelBookingPageIdentifier != bookingType)
            {
                if (isAllRoomsTobeShown)
                    PopulateMainConfirmationBox(hotelSearch, selectedRoomAndRatesHashTable);
                PopulateConfirmationPageEmailRoomInfoBox(hotelSearch, selectedRoomAndRatesHashTable, isAllRoomsTobeShown,
                                                         roomNumberToBeDisplayed);


                if (!showIndivisualGuarantee)
                    DisplayCommonGuranteeText(selectedRoomAndRatesHashTable);

                PopulateMapHeaderHotelName();
                DisplayHotelInformation(hotelSearch);
            }
            else if (PageIdentifier.CancelBookingPageIdentifier == bookingType)
            {
                SortedList<int, Dictionary<string, string>> cancelInformation = PopulateCancelInfoHashTable();
                rowGroupForConfirmation.Visible = false;

                if (hotelSearch == null)
                {
                    hotelSearch = BookingEngineSessionWrapper.BookingDetails.HotelSearch;
                }
                if (isAllRoomsTobeShown)
                {
                    Hashtable selectedRoomAndRates = new Hashtable();
                    if (selectedRoomAndRatesHashTable == null)
                    {
                        List<BookingDetailsEntity> bookingList = BookingEngineSessionWrapper.AllBookingDetails;
                        int roomIndex = 0;
                        if (bookingList != null && bookingList.Count > 0)
                        {
                            foreach (BookingDetailsEntity bookingEntity in bookingList)
                            {
                                if (bookingEntity.HotelSearch.ListRooms != null &&
                                    bookingEntity.HotelSearch.ListRooms.Count > 0)
                                {
                                    SelectedRoomAndRateEntity selectedRoomRate = new SelectedRoomAndRateEntity();
                                    RoomRateEntity roomRate =
                                        new RoomRateEntity(bookingEntity.HotelRoomRate.RoomtypeCode,
                                                           bookingEntity.HotelRoomRate.RatePlanCode);
                                    RedeemptionPointsEntity redemptionEntity =
                                        new RedeemptionPointsEntity(bookingEntity.HotelRoomRate.Points,
                                                                    bookingEntity.HotelRoomRate.AwardType);
                                    roomRate.BaseRate = bookingEntity.HotelRoomRate.Rate;
                                    roomRate.IsSpecialRate = bookingEntity.HotelRoomRate.IsSpecialRate;
                                    roomRate.TotalRate = bookingEntity.HotelRoomRate.TotalRate;
                                    roomRate.PointsDetails = redemptionEntity;
                                    selectedRoomRate.RoomRates = new List<RoomRateEntity>();
                                    selectedRoomRate.RoomRates.Add(roomRate);
                                    selectedRoomAndRates.Add(roomIndex, selectedRoomRate);
                                    roomIndex++;
                                }
                            }
                        }
                    }

                    Hashtable htbl = null;
                    if (selectedRoomAndRatesHashTable != null)
                        htbl = GetCancelledBookingDetails(selectedRoomAndRatesHashTable);
                    else if (selectedRoomAndRates != null)
                        htbl = GetCancelledBookingDetails(selectedRoomAndRates);

                    cancelNumbers = cancelNumbers.Remove(cancelNumbers.Length - 2);

                    PopulateMainConfirmationBoxInCancelFlow(cancelInformation, roomNumberToBeDisplayed);
                    PopulateRoomSummaryForEmailInCancelFlow(hotelSearch, htbl, htblLegs, cancelInformation);

                    bool isHelpMessageRequired = false;
                    if (BookingEngineSessionWrapper.AllCancelledDetails != null)
                    {
                        foreach (CancelDetailsEntity entity in BookingEngineSessionWrapper.AllCancelledDetails)
                        {
                            if (entity.LegNumber == "1")
                            {
                                isHelpMessageRequired = true;
                                break;
                            }
                        }
                        if (!isHelpMessageRequired)
                        {
                            masterReservation.Visible = true;
                            masterReservationHelpText.Text =
                                WebUtil.GetTranslatedText("/bookingengine/booking/contactus/ReservationInfo1")
                                + AppConstants.SPACE
                                + WebUtil.GetTranslatedText("/bookingengine/booking/contactus/ReservationInfo2");
                        }
                        else
                        {
                            masterReservation.Visible = false;
                        }
                    }
                }
                PopulateConfirmationPageEmailRoomInfoBoxInCancelFlow(cancelInformation, roomNumberToBeDisplayed,
                                                                     isAllRoomsTobeShown);
            }
            SetDidYouKnowAndOtherImportantInfoTextForEmailTemplate(bookingType);
            SetConfirmationHeadings(bookingType);
        }

        /// <summary>
        /// To Fetch Cancelled Boking Details
        /// </summary>
        /// <param name="selectedRoomAndRatesHashTable"></param>
        /// <returns>Cancelled Boking Details</returns>
        private Hashtable GetCancelledBookingDetails(Hashtable selectedRoomAndRatesHashTable)
        {
            Hashtable htbl = new Hashtable();
            htblLegs = new Hashtable();
            bool isTrue = false;
            int counter = 0;
            cancelNumbers = string.Empty;
            cancelCounter = 0;
            if (BookingEngineSessionWrapper.AllBookingDetails != null)
            {
                if (BookingEngineSessionWrapper.AllCancelledDetails != null)
                {
                    foreach (CancelDetailsEntity cnclEntity in BookingEngineSessionWrapper.AllCancelledDetails)
                    {
                        isTrue = false;
                        foreach (BookingDetailsEntity bkngEntity in BookingEngineSessionWrapper.AllBookingDetails)
                        {
                            if (cnclEntity.ReservationNumber == bkngEntity.ReservationNumber && cnclEntity.LegNumber == bkngEntity.LegNumber)
                            {
                                foreach (SelectedRoomAndRateEntity selRoomnRateEntity in selectedRoomAndRatesHashTable.Values)
                                {
                                    foreach (RoomRateEntity rateEntity in selRoomnRateEntity.RoomRates)
                                    {
                                        if (rateEntity.PointsDetails != null)
                                        {
                                            if (bkngEntity.HotelRoomRate.RatePlanCode == rateEntity.RatePlanCode
                                               && bkngEntity.HotelRoomRate.RoomtypeCode == rateEntity.RoomTypeCode
                                               && string.Equals(bkngEntity.HotelRoomRate.Rate, rateEntity.BaseRate)
                                               && string.Equals(bkngEntity.HotelRoomRate.TotalRate, rateEntity.TotalRate)
                                               && bkngEntity.HotelRoomRate.Points == rateEntity.PointsDetails.PointsRequired)
                                            {
                                                htbl.Add(counter, selRoomnRateEntity);
                                                isTrue = true;
                                                htblLegs.Add(counter, "<" + bkngEntity.ReservationNumber + "-" + bkngEntity.LegNumber + ">");
                                                cancelNumbers += cnclEntity.CancelNumber + ", ";
                                                counter++;
                                                cancelCounter++;
                                                break;
                                            }
                                        }
                                        if (rateEntity.PointsDetails == null)
                                        {
                                            if (bkngEntity.HotelRoomRate.RatePlanCode == rateEntity.RatePlanCode
                                                && bkngEntity.HotelRoomRate.RoomtypeCode == rateEntity.RoomTypeCode
                                                && string.Equals(bkngEntity.HotelRoomRate.Rate.CurrencyCode, rateEntity.BaseRate.CurrencyCode)
                                                && string.Equals(bkngEntity.HotelRoomRate.Rate.Rate, rateEntity.BaseRate.Rate)
                                                && string.Equals(bkngEntity.HotelRoomRate.TotalRate.CurrencyCode, rateEntity.TotalRate.CurrencyCode)
                                                && string.Equals(bkngEntity.HotelRoomRate.TotalRate.Rate, rateEntity.TotalRate.Rate))
                                            {
                                                htbl.Add(counter, selRoomnRateEntity);
                                                isTrue = true;
                                                htblLegs.Add(counter, "<" + bkngEntity.ReservationNumber + "-" + bkngEntity.LegNumber + ">");
                                                cancelNumbers += cnclEntity.CancelNumber + ", ";
                                                counter++;
                                                cancelCounter++;
                                                break;
                                            }
                                        }
                                    }

                                    if (isTrue)
                                        break;
                                }
                            }
                            if (isTrue)
                                break;
                        }
                    }
                }
            }
            return htbl;
        }

        #region Booking and Modify booking Flow

        private string GetViewModifyDLRedirectLink()
        {
            string last_Name = string.Empty;
            if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REDEMPTION)
                last_Name = GuestBookingInformationSessionWrapper.FetchedGuestInformation.LastName;
            else
                last_Name = GuestBookingInformationSessionWrapper.GuestBookingInformation.LastName;

            return WebUtil.GetViewModifyDLRedirectLink(HttpContext.Current.Request.Url.Host, AppConstants.DLREDIRECT,
                AppConstants.DEEPLINK_VIEW_MODIFY_RESERVATION_ID, ReservationNumberSessionWrapper.ReservationNumber,
                AppConstants.DEEPLINK_VIEW_MODIFY_LAST_NAME, last_Name);

            //return HttpContext.Current.Request.Url.Host + "/" + AppConstants.DLREDIRECT + "?" + AppConstants.DEEPLINK_VIEW_MODIFY_RESERVATION_ID + "=" + ReservationNumberSessionWrapper.ReservationNumber + "&"
            //    + AppConstants.DEEPLINK_VIEW_MODIFY_LAST_NAME + "=" + last_Name;
        }

        /// <summary>
        /// Populates the main confirmation box.
        /// </summary>
        /// <param name="hotelSearch">The hotel search.</param>
        private void PopulateMainConfirmationBox(HotelSearchEntity hotelSearch, Hashtable selectedRoomAndRatesHashTable)
        {
            mainReservationInfoBox.Visible = true;
            mainBookingReservationNumberID.InnerHtml = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/reservationNumber") +
                ReservationNumberSessionWrapper.ReservationNumber;
            AvailabilityController availabilityController = new AvailabilityController();
            HotelDestination hotelDestination = availabilityController.GetHotelDestinationEntity(hotelSearch.SelectedHotelCode);

            ancViewModifyDLRedirectLink.HRef = "http://" + GetViewModifyDLRedirectLink();
            ancViewModifyDLRedirectLink.InnerHtml = GetViewModifyDLRedirectLink();

            hotelName.InnerHtml = hotelDestination.Name;

            checkInDate.InnerHtml = WebUtil.GetDayFromDate(hotelSearch.ArrivalDate)
                                    + AppConstants.SPACE + Utility.GetFormattedDate(hotelSearch.ArrivalDate);
            checkOutDate.InnerHtml = WebUtil.GetDayFromDate(hotelSearch.DepartureDate)
                                     + AppConstants.SPACE + Utility.GetFormattedDate(hotelSearch.DepartureDate);
            noOfNights.InnerHtml = hotelSearch.NoOfNights.ToString();
            noOfRooms.InnerHtml = hotelSearch.RoomsPerNight.ToString();
            int totalNoOfAdults = 0;
            int totalNoofChildern = 0;
            if (hotelSearch != null && hotelSearch.ListRooms.Count > 0)
            {
                for (int roomIterator = 0; roomIterator < hotelSearch.ListRooms.Count; roomIterator++)
                {
                    totalNoOfAdults += hotelSearch.ListRooms[roomIterator].AdultsPerRoom;
                    totalNoofChildern += hotelSearch.ListRooms[roomIterator].ChildrenPerRoom;
                }
                if (totalNoOfAdults > 1)
                {
                    occupantsDetails.InnerHtml = totalNoOfAdults + " "
                                                 +
                                                 WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults");
                }
                else
                {
                    occupantsDetails.InnerHtml = totalNoOfAdults + " "
                                                 + WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Adult");
                }

                if (totalNoofChildern > 1)
                {
                    occupantsDetails.InnerHtml = occupantsDetails.InnerHtml + "," + " " + totalNoofChildern + " "
                                                 +
                                                 WebUtil.GetTranslatedText(
                                                     "/bookingengine/booking/childrensdetails/children");
                }
                else if (totalNoofChildern == 1)
                {
                    occupantsDetails.InnerHtml = occupantsDetails.InnerHtml + "," + " " + totalNoofChildern + " "
                                                 +
                                                 WebUtil.GetTranslatedText(
                                                     "/bookingengine/booking/childrensdetails/child");
                }

                PopulateRoomSummaryForEmail(hotelSearch, selectedRoomAndRatesHashTable);
                SetTotalRateAndCurrencyCode();
            }
        }

        /// <summary>
        /// [ERROR: Unknown property access] the populate room summary for email.
        /// </summary>
        /// <value>The populate room summary for email.</value>
        private void PopulateRoomSummaryForEmail(HotelSearchEntity hotelSearch, Hashtable selectedRoomRateshashtable)
        {
            if (selectedRoomRateshashtable != null && selectedRoomRateshashtable.Count > 0)
            {
                List<RoomRateEntity> selectedRoomRateEntityList = new List<RoomRateEntity>();
                SelectedRoomAndRateEntity selectedRoomRates = null;
                string hotelCountryCode = string.Empty;
                
                for (int iterator = 0; iterator < selectedRoomRateshashtable.Keys.Count; iterator++)
                {
                    selectedRoomRates = null;
                    if (selectedRoomRateshashtable.ContainsKey(iterator))
                    {
                        selectedRoomRates = selectedRoomRateshashtable[iterator] as SelectedRoomAndRateEntity;
                    }                    
                    if (selectedRoomRates != null)
                    {
                        RoomSummaryInfoForEmail currentRoomSummaryInformation =
                            this.FindControl("RoomSummaryInformation"
                                             + iterator.ToString()) as RoomSummaryInfoForEmail;
                        if (currentRoomSummaryInformation != null)
                        {
                            currentRoomSummaryInformation.Visible = true;
                            currentRoomSummaryInformation.RoomIdentifier = iterator.ToString();
                            int uiRoomNumber = iterator + 1;
                            currentRoomSummaryInformation.RoomLabel =
                                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room") +
                                uiRoomNumber.ToString();

                            currentRoomSummaryInformation.RoomNumber = uiRoomNumber;

                            if (hotelSearch != null)
                            {
                                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                                {
                                    currentRoomSummaryInformation.PriceInfo =
                                        WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RatePerStay");
                                }

                                else
                                {
                                    currentRoomSummaryInformation.PriceInfo =
                                        WebUtil.GetTranslatedText(
                                            "/bookingengine/booking/searchhotel/RoomRateFirstNight");
                                }
                            }

                            if ((!IsTrueBlockCode(iterator, selectedRoomRates)))
                            {
                                BaseRateDisplay rateDisplay = Utility.GetRateCellDisplayObject(iterator,
                                                                                               selectedRoomRates);

                                currentRoomSummaryInformation.RoomTypeID =
                                    RoomRateUtil.GetRoomCategory(selectedRoomRates.RoomRates[0].RoomTypeCode).
                                        RoomCategoryName;

                                if (hotelSearch.SearchingType == SearchType.CORPORATE)
                                {
                                    if (rateDisplay != null)
                                    {
                                        foreach (RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                                        {
                                            if (selectedRoomRates != null && selectedRoomRates.RateCategoryID != null &&
                                                selectedRoomRates.RateCategoryID == hdrDisplay.Id)
                                            {
                                                currentRoomSummaryInformation.RoomRateCategory = hdrDisplay.Title;
                                            }
                                        }
                                    }
                                    else if (BookingEngineSessionWrapper.BookingDetails != null)
                                    {
                                        if (Utility.IsBlockCodeBooking)
                                        {
                                            currentRoomSummaryInformation.RoomRateCategory = BookingEngineSessionWrapper.BookingDetails.HotelSearch != null ?
                                                BookingEngineSessionWrapper.BookingDetails.HotelSearch.CampaignCode : "";
                                        }
                                        else
                                        {
                                            currentRoomSummaryInformation.RoomRateCategory = BookingEngineSessionWrapper.BookingDetails.GuestInformation != null ? 
                                                BookingEngineSessionWrapper.BookingDetails.GuestInformation.CompanyName : "";
                                        }
                                    }
                                }
                                else
                                {
                                    currentRoomSummaryInformation.RoomRateCategory =
                                        RoomRateUtil.GetRateCategoryByRatePlanCode(
                                                selectedRoomRates.RoomRates[0].RatePlanCode.ToString()).RateCategoryName;
                                }
                            }
                            else
                            {
                                BaseRateDisplay rateDisplay = Utility.GetRateCellDisplayObject(iterator,
                                                                                               selectedRoomRates);
                                currentRoomSummaryInformation.RoomTypeID =
                                    RoomRateUtil.GetRoomCategory(selectedRoomRates.RoomRates[0].RoomTypeCode).
                                        RoomCategoryName;

                                if (rateDisplay != null)
                                {
                                    foreach (RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                                    {
                                        if (selectedRoomRates != null && selectedRoomRates.RateCategoryID != null &&
                                            selectedRoomRates.RateCategoryID == hdrDisplay.Id)
                                        {
                                            currentRoomSummaryInformation.RoomRateCategory = hdrDisplay.Title;
                                        }
                                    }
                                }
                            }

                            string rateString = string.Empty;
                            if (BookingEngineSessionWrapper.HideARBPrice)
                            {
                                currentRoomSummaryInformation.PriceString = Utility.GetPrepaidString();
                            }
                            else
                            {
                                SetRateStringInForEmailPage(hotelSearch, hotelSearch.SearchingType, iterator, ref rateString, selectedRoomRates, false);
                                if (string.IsNullOrEmpty(rateString))
                                {
                                    rateString = GetPricePerNight(selectedRoomRates.RoomRates[0], hotelSearch);
                                }
                                currentRoomSummaryInformation.PriceString = rateString;                             

                            }

                            if (GuestBookingInformationSessionWrapper.AllGuestsBookingInformations != null)
                            {
                                int i = 0;
                                foreach (
                                    GuestInformationEntity guest in GuestBookingInformationSessionWrapper.AllGuestsBookingInformations.Values)
                                {
                                    if (i == iterator)
                                    {
                                        currentRoomSummaryInformation.ReservationLeg = "<" + guest.ReservationNumber +
                                                                                       "-" + guest.LegNumber + ">";
                                        break;
                                    }
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// [ERROR: Unknown property access] the populate room summary for email in case of cancel.
        /// </summary>
        /// <value>The populate room summary for email.</value>
        private void PopulateRoomSummaryForEmailInCancelFlow(HotelSearchEntity hotelSearch, Hashtable selectedRoomRateshashtable, Hashtable selectLegNumbers, SortedList<int, Dictionary<string, string>> cancelInformation)
        {
            if (selectedRoomRateshashtable != null && selectedRoomRateshashtable.Count > 0)
            {
                List<RoomRateEntity> selectedRoomRateEntityList = new List<RoomRateEntity>();
                SelectedRoomAndRateEntity selectedRoomRates = null;
                string hotelCountryCode = string.Empty;                
                for (int iterator = 0; iterator < selectedRoomRateshashtable.Keys.Count; iterator++)
                {
                    string rateString = string.Empty;
                    selectedRoomRates = null;
                    if (selectedRoomRateshashtable.ContainsKey(iterator))
                    {
                        selectedRoomRates = selectedRoomRateshashtable[iterator] as SelectedRoomAndRateEntity;
                    }                    
                    if (selectedRoomRates != null)
                    {
                        RoomSummaryInfoForEmail currentRoomSummaryInformation = this.FindControl("RoomSummaryInformation" + iterator.ToString()) as RoomSummaryInfoForEmail;
                        if (currentRoomSummaryInformation != null)
                        {
                            currentRoomSummaryInformation.Visible = true;
                            currentRoomSummaryInformation.RoomIdentifier = iterator.ToString();
                            int uiRoomNumber = iterator + 1;
                            currentRoomSummaryInformation.RoomLabel = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/room") + uiRoomNumber.ToString();

                            if (hotelSearch != null)
                            {
                                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                                {
                                    currentRoomSummaryInformation.PriceInfo = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RatePerStay");
                                }

                                else
                                {
                                    currentRoomSummaryInformation.PriceInfo = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RoomRateFirstNight");
                                }
                            }

                            if ((!IsTrueBlockCode(iterator, selectedRoomRates)))
                            {
                                currentRoomSummaryInformation.RoomTypeID = RoomRateUtil.GetRoomCategory(selectedRoomRates.RoomRates[0].RoomTypeCode).RoomCategoryName;

                                if (hotelSearch.SearchingType == SearchType.CORPORATE)
                                {
                                    if ((cancelInformation != null) && (cancelInformation[iterator] != null))
                                    {
                                        if ((cancelInformation[iterator].ContainsKey(CommunicationTemplateConstants.RATE_CATEGORY)) && ((cancelInformation[iterator][CommunicationTemplateConstants.RATE_CATEGORY]) != null))
                                            currentRoomSummaryInformation.RoomRateCategory = cancelInformation[iterator][CommunicationTemplateConstants.RATE_CATEGORY].ToString();
                                        else if (BookingEngineSessionWrapper.BookingDetails != null && BookingEngineSessionWrapper.BookingDetails.GuestInformation !=null && !Utility.IsBlockCodeBooking)
                                        {                                            
                                            currentRoomSummaryInformation.RoomRateCategory = BookingEngineSessionWrapper.BookingDetails.GuestInformation.CompanyName;                                            
                                        }
                                        
                                        if (BookingEngineSessionWrapper.HideARBPrice)
                                        {
                                            currentRoomSummaryInformation.PriceString = Utility.GetPrepaidString();
                                        }
                                        else
                                        {
                                            if ((cancelInformation[iterator].ContainsKey(CommunicationTemplateConstants.ROOM_RATE_SMS)) && ((cancelInformation[iterator][CommunicationTemplateConstants.ROOM_RATE_SMS]) != null))
                                                currentRoomSummaryInformation.PriceString = (cancelInformation[iterator][CommunicationTemplateConstants.ROOM_RATE_SMS]).ToString();
                                        }
                                    }
                                }
                                else
                                {
                                    currentRoomSummaryInformation.RoomRateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(selectedRoomRates.RoomRates[0].RatePlanCode.ToString()).RateCategoryName;
                                    SetRateStringInForEmailPage(hotelSearch, hotelSearch.SearchingType, iterator, ref rateString, selectedRoomRates, false);
                                    if (BookingEngineSessionWrapper.HideARBPrice)
                                    {
                                        rateString = Utility.GetPrepaidString();
                                    }
                                    else if (string.IsNullOrEmpty(rateString))
                                    {                                            
                                        rateString = GetPricePerNight(selectedRoomRates.RoomRates[0],hotelSearch);
                                    }                                                                             
                                    
                                    currentRoomSummaryInformation.PriceString = rateString; 
                                }
                            }
                            else
                            {
                                BaseRateDisplay rateDisplay = Utility.GetRateCellDisplayObject(iterator, selectedRoomRates);

                                currentRoomSummaryInformation.RoomTypeID = RoomRateUtil.GetRoomCategory(selectedRoomRates.RoomRates[0].RoomTypeCode).RoomCategoryName;

                                if (rateDisplay != null)
                                {
                                    foreach (RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                                    {
                                        if (selectedRoomRates != null && selectedRoomRates.RateCategoryID != null && selectedRoomRates.RateCategoryID == hdrDisplay.Id)
                                        {
                                            currentRoomSummaryInformation.RoomRateCategory = hdrDisplay.Title;
                                        }
                                    }
                                }

                                if (BookingEngineSessionWrapper.HideARBPrice)
                                {
                                    currentRoomSummaryInformation.PriceString = Utility.GetPrepaidString();
                                }
                                else
                                {
                                    SetRateStringInForEmailPage(hotelSearch, hotelSearch.SearchingType, iterator, ref rateString, selectedRoomRates, false);
                                    if (string.IsNullOrEmpty(rateString))
                                    {
                                        rateString = GetPricePerNight(selectedRoomRates.RoomRates[0], hotelSearch);
                                    }
                                    currentRoomSummaryInformation.PriceString = rateString;                                    
                                }
                            }


                            currentRoomSummaryInformation.ReservationLeg = selectLegNumbers[iterator].ToString();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets the rate string in room summary info for email.
        /// </summary>
        /// <param name="roomNumber">The room number.</param>
        /// <param name="currentRoomSummaryInformation">The current room summary information.</param>
        /// <param name="selectedRoomAndRateEntity">The selected room and rate entity.</param>
        /// <param name="showPricePerStay">if set to <c>true</c> [show price per stay].</param>
        private void SetRateStringInForEmailPage(HotelSearchEntity hotelSearch, SearchType currentSearchType
                                                 , int roomNumber, ref string stringTobeSet,
                                                 SelectedRoomAndRateEntity selectedRoomAndRateEntity,
                                                 bool showPricePerStay)
        {
            BaseRateDisplay rateDisplay = Utility.GetRateCellDisplayObject(roomNumber, selectedRoomAndRateEntity);
            if (rateDisplay != null)
            {
                RoomCategoryEntity currentRoomCategoryEntity = selectedRoomAndRateEntity.SelectedRoomCategoryEntity;
                if (currentRoomCategoryEntity != null)
                {
                    RateCellDisplay currentRateStringDisplay =
                        rateDisplay.GetRateDisplayStringForShoppingCart(currentRoomCategoryEntity,
                                                                        selectedRoomAndRateEntity.RateCategoryID,
                                                                        showPricePerStay, roomNumber);
                    if (hotelSearch != null && hotelSearch.SearchingType == SearchType.VOUCHER)
                        stringTobeSet = WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrate");
                    else
                    stringTobeSet = currentRateStringDisplay.RateString;
                }
                else
                    SetRateStringInEmailInModifyFlow(hotelSearch, currentSearchType, roomNumber
                                                     , ref stringTobeSet, selectedRoomAndRateEntity, showPricePerStay);
            }

            if (rateDisplay == null)
            {
                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                    stringTobeSet =
                        WebUtil.GetRedeemString(selectedRoomAndRateEntity.RoomRates[0].PointsDetails.PointsRequired);
            }
        }

        /// <summary>
        /// Sets the rate string in email in modify flow.
        /// </summary>
        /// <param name="hotelSearch">The hotel search.</param>
        /// <param name="currentSearchType">Type of the current search.</param>
        /// <param name="roomNumber">The room number.</param>
        /// <param name="stringTobeSet">The string tobe set.</param>
        /// <param name="selectedRoomAndRateEntity">The selected room and rate entity.</param>
        /// <param name="showPricePerStay">if set to <c>true</c> [show price per stay].</param>
        private void SetRateStringInEmailInModifyFlow(HotelSearchEntity hotelSearch, SearchType currentSearchType,
                                                      int roomNumber, ref string stringTobeSet,
                                                      SelectedRoomAndRateEntity selectedRoomAndRateEntity,
                                                      bool showPricePerStay)
        {
            switch (currentSearchType)
            {
                case SearchType.REDEMPTION:

                    stringTobeSet =
                        WebUtil.GetRedeemString(selectedRoomAndRateEntity.RoomRates[0].PointsDetails.PointsRequired *
                                                hotelSearch.NoOfNights);
                    break;
                case SearchType.BONUSCHEQUE:
                    RateEntity rateEntity = null;
                    AvailabilityController availabilityController = new AvailabilityController();
                    string hotelCountryCode = string.Empty;

                    if (hotelSearch != null)
                    {
                        hotelCountryCode = availabilityController.GetHotelCountryCode(hotelSearch,
                                                                                      hotelSearch.ListRooms[0]);
                    }

                    rateEntity = new RateEntity(selectedRoomAndRateEntity.RoomRates[0].BaseRate.Rate,
                                                selectedRoomAndRateEntity.RoomRates[0].BaseRate.CurrencyCode);
                    stringTobeSet = Utility.GetRoomRateString(null, rateEntity, 0, hotelSearch.SearchingType,
                                                              hotelSearch.ArrivalDate.Year, hotelCountryCode,
                                                              AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                    break;

                case SearchType.VOUCHER:
                    stringTobeSet = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher");
                    break;
                default:
                    stringTobeSet = selectedRoomAndRateEntity.RoomRates[0].BaseRate.Rate + AppConstants.SPACE +
                                    selectedRoomAndRateEntity.RoomRates[0].BaseRate.CurrencyCode;
                    break;
            }
        }

        /// <summary>
        /// Determines whether [is true block code].
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if [is true block code]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsTrueBlockCode(int roomNumber, SelectedRoomAndRateEntity selectedRoomAndRateEntity)
        {
            try
            {
                BaseRateDisplay rateDisplay = Utility.GetRateCellDisplayObject(roomNumber, selectedRoomAndRateEntity);
                BlockCodeRoomRateDisplay newBlockCodeRoomRateDisplay = rateDisplay as BlockCodeRoomRateDisplay;
                return (newBlockCodeRoomRateDisplay != null) ? newBlockCodeRoomRateDisplay.IsRateCodeNotAssociated : false;
            }
            catch
            {
                return false;
            }
        }

        private string GetPricePerNight(RoomRateEntity roomRateEntity, HotelSearchEntity hotelSearch)
        {
            var pricePerNight = string.Empty;
            
            switch (hotelSearch.SearchingType)
            {
                case SearchType.VOUCHER:
                    {
                        pricePerNight = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher");
                        break;
                    }
                case SearchType.BONUSCHEQUE:
                    pricePerNight = WebUtil.GetBonusChequeRateString(
                        "/bookingengine/booking/selectrate/bonuschequerate",
                        hotelSearch.ArrivalDate.Year,
                        hotelSearch.HotelCountryCode,
                        roomRateEntity.BaseRate.Rate, roomRateEntity.BaseRate.CurrencyCode, AppConstants.PER_NIGHT,
                        AppConstants.PER_ROOM);
                    break;
                case SearchType.REDEMPTION:

                    if (roomRateEntity != null && roomRateEntity.PointsDetails != null)
                        pricePerNight = WebUtil.GetRedeemString(roomRateEntity.PointsDetails.PointsRequired);

                    break;
                case SearchType.REGULAR:
                    
                    pricePerNight = roomRateEntity.BaseRate.Rate.ToString() + AppConstants.SPACE +
                                    roomRateEntity.BaseRate.CurrencyCode;
                    break;

                default:
                    
                    pricePerNight = roomRateEntity.BaseRate.Rate.ToString() + AppConstants.SPACE +
                                    roomRateEntity.BaseRate.CurrencyCode;
                    break;
            }
            return pricePerNight;
        }

        private string GetPricePerNight(BookingDetailsEntity bookingDetail)
        {

            var pricePerNight = string.Empty;
            if (bookingDetail != null)
            {
                switch (bookingDetail.HotelSearch.SearchingType)
                {
                    case SearchType.VOUCHER:
                        {
                            pricePerNight = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/voucher");
                            break;
                        }
                    case SearchType.BONUSCHEQUE:
                        if (Reservation2SessionWrapper.IsPerStaySelectedInSelectRatePage)
                            pricePerNight =
                                WebUtil.GetBonusChequeRateString("/bookingengine/booking/selectrate/bonuschequerate",
                                                                 bookingDetail.HotelSearch.ArrivalDate.Year,
                                                                 bookingDetail.HotelSearch.HotelCountryCode,
                                                                 bookingDetail.HotelRoomRate.TotalRate.Rate,
                                                                 bookingDetail.HotelRoomRate.Rate.CurrencyCode,
                                                                 RoomRateDisplayUtil.GetNoOfNights(),
                                                                 RoomRateDisplayUtil.GetNoOfRooms());
                        else
                            pricePerNight =
                                WebUtil.GetBonusChequeRateString("/bookingengine/booking/selectrate/bonuschequerate",
                                                                 bookingDetail.HotelSearch.ArrivalDate.Year,
                                                                 bookingDetail.HotelSearch.HotelCountryCode,
                                                                 bookingDetail.HotelRoomRate.Rate.Rate,
                                                                 bookingDetail.HotelRoomRate.Rate.CurrencyCode,
                                                                 AppConstants.PER_NIGHT, AppConstants.PER_ROOM);
                        break;
                    case SearchType.REDEMPTION:

                        pricePerNight = WebUtil.GetRedeemString(bookingDetail.HotelRoomRate.Points);
                        break;

                    case SearchType.REGULAR:

                        pricePerNight = bookingDetail.HotelRoomRate.Rate.Rate.ToString() + AppConstants.SPACE +
                                        bookingDetail.HotelRoomRate.Rate.CurrencyCode;
                        break;

                    default:

                        pricePerNight = bookingDetail.HotelRoomRate.Rate.Rate.ToString() + AppConstants.SPACE +
                                        bookingDetail.HotelRoomRate.Rate.CurrencyCode;
                        break;
                }
            }
            return pricePerNight;
        }

        /// <summary>
        /// Populates the confirmation page email room info box.
        /// </summary>
        /// <param name="hotelSearch">The hotel search.</param>
        /// <param name="selectedRoomRateshashtable">The selected room rateshashtable.</param>
        /// <param name="isAllRoomsTobeShown"></param>
        /// <param name="roomNumberToBeDisplayed"></param>
        private void PopulateConfirmationPageEmailRoomInfoBox(HotelSearchEntity hotelSearch, Hashtable selectedRoomRateshashtable,
                                                              bool isAllRoomsTobeShown, int roomNumberToBeDisplayed)
        {
            if (selectedRoomRateshashtable != null && selectedRoomRateshashtable.Count > 0)
            {
                List<RoomRateEntity> selectedRoomRateEntityList = new List<RoomRateEntity>();
                SelectedRoomAndRateEntity selectedRoomRates = null;

                Dictionary<string, string> returnValue = null;

                var mobileLanguage = Session[ScanwebMobile.MOBILE_CONTENT_LANGUAGE] as string;
                returnValue = ContentDataAccess.GetConfirmationEmailTextFormCMS(bookingType, mobileLanguage);                
                for (int iterator = 0; iterator < selectedRoomRateshashtable.Keys.Count; iterator++)
                {
                    int roomNumber = iterator + 1;
                    selectedRoomRates = null;
                    
                    if (selectedRoomRateshashtable.ContainsKey(iterator))
                    {
                        selectedRoomRates = selectedRoomRateshashtable[iterator] as SelectedRoomAndRateEntity;
                    }

                    ConfirmationPageEmailRoomInfoBox currentRoomInfoBox =
                        this.FindControl("RoomInformation" + iterator.ToString()) as ConfirmationPageEmailRoomInfoBox;
                    if (currentRoomInfoBox != null)
                    {
                        if ((isAllRoomsTobeShown) || ((!isAllRoomsTobeShown) && (roomNumberToBeDisplayed == iterator)))
                        {
                            currentRoomInfoBox.Visible = true;
                            currentRoomInfoBox.RoomIdentifier = iterator.ToString();

                            AvailabilityController availabilityController = new AvailabilityController();
                            HotelDestination hotelDestination =
                                availabilityController.GetHotelDestinationEntity(hotelSearch.SelectedHotelCode);
                            currentRoomInfoBox.HotelName = hotelDestination.Name;

                            if (hotelSearch != null)
                            {
                                if (hotelSearch.SearchingType == SearchType.REDEMPTION)
                                {
                                    currentRoomInfoBox.PriceInfo =
                                        WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/RatePerStay");
                                }

                                else
                                {
                                    currentRoomInfoBox.PriceInfo =
                                        WebUtil.GetTranslatedText(
                                            "/bookingengine/booking/searchhotel/RoomRateFirstNight");
                                }
                            }

                            currentRoomInfoBox.ArrivalDate = WebUtil.GetDayFromDate(hotelSearch.ArrivalDate)
                                                             + AppConstants.SPACE +
                                                             Utility.GetFormattedDate(hotelSearch.ArrivalDate);
                            currentRoomInfoBox.DeparturDate = WebUtil.GetDayFromDate(hotelSearch.DepartureDate)
                                                              + AppConstants.SPACE +
                                                              Utility.GetFormattedDate(hotelSearch.DepartureDate);

                            if ((!IsTrueBlockCode(iterator, selectedRoomRates)))
                            {
                                BaseRateDisplay rateDisplay = Utility.GetRateCellDisplayObject(iterator,
                                                                                               selectedRoomRates);

                                currentRoomInfoBox.RoomTypeName =
                                    RoomRateUtil.GetRoomCategory(selectedRoomRates.RoomRates[0].RoomTypeCode).
                                        RoomCategoryName;

                                if (hotelSearch.SearchingType == SearchType.CORPORATE)
                                {
                                    if (rateDisplay != null)
                                    {
                                        foreach (RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                                        {
                                            if (selectedRoomRates != null && selectedRoomRates.RateCategoryID != null
                                                && selectedRoomRates.RateCategoryID == hdrDisplay.Id)
                                            {
                                                currentRoomInfoBox.RateTypeID = hdrDisplay.Title;
                                            }
                                        }
                                    }
                                    else if (BookingEngineSessionWrapper.BookingDetails != null)
                                    {
                                        if (Utility.IsBlockCodeBooking)
                                        {
                                            currentRoomInfoBox.RateTypeID = BookingEngineSessionWrapper.BookingDetails.HotelSearch != null ?
                                                BookingEngineSessionWrapper.BookingDetails.HotelSearch.CampaignCode : "";
                                        }
                                        else
                                        {
                                            currentRoomInfoBox.RateTypeID = BookingEngineSessionWrapper.BookingDetails.GuestInformation != null ? 
                                                BookingEngineSessionWrapper.BookingDetails.GuestInformation.CompanyName : "";
                                        }
                                    }
                                }
                                else
                                {
                                    currentRoomInfoBox.RateTypeID =
                                        RoomRateUtil.GetRateCategoryByRatePlanCode(
                                            selectedRoomRates.RoomRates[0].RatePlanCode.ToString()).RateCategoryName;
                                }
                            }
                            else
                            {
                                BaseRateDisplay rateDisplay = Utility.GetRateCellDisplayObject(iterator,
                                                                                               selectedRoomRates);

                                currentRoomInfoBox.RoomTypeName =
                                    RoomRateUtil.GetRoomCategory(selectedRoomRates.RoomRates[0].RoomTypeCode).
                                        RoomCategoryName;

                                if (rateDisplay != null)
                                {
                                    foreach (RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                                    {
                                        if (selectedRoomRates != null && selectedRoomRates.RateCategoryID != null &&
                                            selectedRoomRates.RateCategoryID == hdrDisplay.Id)
                                        {
                                            currentRoomInfoBox.RateTypeID = hdrDisplay.Title;
                                        }
                                    }
                                }
                            }

                            string rateString = string.Empty;
                            if (BookingEngineSessionWrapper.HideARBPrice)
                            {
                                rateString = Utility.GetPrepaidString();
                            }
                            else
                            {
                                SetRateStringInForEmailPage(hotelSearch, hotelSearch.SearchingType, iterator, ref rateString, selectedRoomRates, false);
                                if (string.IsNullOrEmpty(rateString))
                                {                                   
                                    rateString = GetPricePerNight(selectedRoomRates.RoomRates[0], hotelSearch);                                    
                                }                                                              
                            }
                            currentRoomInfoBox.RateString = rateString; 

                            if (hotelSearch.ListRooms[iterator].AdultsPerRoom > 1)
                            {
                                currentRoomInfoBox.AdultAndChildernDetail = hotelSearch.ListRooms[iterator].
                                                                                AdultsPerRoom
                                                                            + " " +
                                                                            WebUtil.GetTranslatedText(
                                                                                "/bookingengine/booking/searchhotel/adults");
                            }
                            else
                            {
                                currentRoomInfoBox.AdultAndChildernDetail = hotelSearch.ListRooms[iterator].
                                                                                AdultsPerRoom
                                                                            + " " +
                                                                            WebUtil.GetTranslatedText(
                                                                                "/bookingengine/booking/searchhotel/Adult");
                            }


                            if (hotelSearch.ListRooms[iterator].ChildrenPerRoom > 1)
                            {
                                currentRoomInfoBox.AdultAndChildernDetail = currentRoomInfoBox.AdultAndChildernDetail +
                                                                            ","
                                                                            + " " +
                                                                            hotelSearch.ListRooms[iterator].
                                                                                ChildrenPerRoom + " "
                                                                            +
                                                                            WebUtil.GetTranslatedText(
                                                                                "/bookingengine/booking/childrensdetails/children");
                            }
                            else if (hotelSearch.ListRooms[iterator].ChildrenPerRoom == 1)
                            {
                                currentRoomInfoBox.AdultAndChildernDetail = currentRoomInfoBox.AdultAndChildernDetail
                                                                            + "," + " " +
                                                                            hotelSearch.ListRooms[iterator].
                                                                                ChildrenPerRoom + " "
                                                                            +
                                                                            WebUtil.GetTranslatedText(
                                                                                "/bookingengine/booking/childrensdetails/child");
                            }


                            if (GuestBookingInformationSessionWrapper.AllGuestsBookingInformations != null)
                            {
                                SortedList<string, GuestInformationEntity> guestsList =
                                    GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
                                GuestInformationEntity currentGuestInformationEntity = null;
                                string keyForGuestInfo = guestsList.Keys[iterator];
                                if (guestsList.Keys.Contains(keyForGuestInfo))
                                    currentGuestInformationEntity =
                                        guestsList[keyForGuestInfo] as GuestInformationEntity;
                                if (BookingEngineSessionWrapper.IsModifyingLegBooking)
                                    currentGuestInformationEntity = guestsList[guestsList.Keys[0]];
                                if (currentGuestInformationEntity != null)
                                {
                                    currentRoomInfoBox.AdultBedType = currentGuestInformationEntity.BedTypePreference;
                                    if ((currentGuestInformationEntity.ChildrensDetails != null)
                                        && (currentGuestInformationEntity.ChildrensDetails.ListChildren.Count != 0))
                                        AddChildDivToTheRoomDiv(ref currentRoomInfoBox,
                                                                currentGuestInformationEntity.ChildrensDetails.
                                                                    ListChildren);
                                    DisplaySpecialRequestDetailsFrRooms(ref currentRoomInfoBox, currentGuestInformationEntity);

                                    DisplayAdditionalRequest(ref currentRoomInfoBox, currentGuestInformationEntity);

                                    currentRoomInfoBox.Name =
                                        Utility.GetNameWithTitle(currentGuestInformationEntity.Title,
                                                                 currentGuestInformationEntity.FirstName,
                                                                 currentGuestInformationEntity.LastName);

                                    currentRoomInfoBox.CityOrTown = currentGuestInformationEntity.City;
                                    currentRoomInfoBox.DNumberValue = currentGuestInformationEntity.IsValidDNumber
                                                                          ? currentGuestInformationEntity.D_Number
                                                                          : string.Empty;

                                    OrderedDictionary countryCodes = DropDownService.GetCountryCodes();
                                    if (countryCodes != null)
                                    {
                                        currentRoomInfoBox.Country = countryCodes[currentGuestInformationEntity.Country] as string;
                                    }

                                    currentRoomInfoBox.EmailAddress = currentGuestInformationEntity.EmailDetails.EmailID;
                                    currentRoomInfoBox.PhoneNumber = currentGuestInformationEntity.Mobile.Number;

                                    currentRoomInfoBox.ReservationNumber = currentGuestInformationEntity.ReservationNumber
                                                                               + "<strong>" + AppConstants.HYPHEN +
                                                                              currentGuestInformationEntity.LegNumber +
                                                                              "</strong>";
                                    string reservationNo = string.Empty;
                                    currentRoomInfoBox.ShowDeepLinkForOtherRooms = false;
                                    if (!isAllRoomsTobeShown)
                                    {
                                        currentRoomInfoBox.ShowDeepLinkForOtherRooms = true;
                                        if (Convert.ToInt32(currentGuestInformationEntity.LegNumber) > 1)
                                        {
                                            reservationNo = currentGuestInformationEntity.ReservationNumber + AppConstants.HYPHEN +
                                                                                   currentGuestInformationEntity.LegNumber;
                                            currentRoomInfoBox.DeepLinkUrl = GetViewModifyDLRedirectLink(reservationNo, currentGuestInformationEntity.LastName);
                                        }
                                    }

                                    if (bookingType == PageIdentifier.CancelBookingPageIdentifier)
                                        SetCancellationFields(currentRoomInfoBox,
                                                              currentGuestInformationEntity.LegNumber);

                                    DisplayFGPAndCreditCardInfo(currentRoomInfoBox, currentGuestInformationEntity);
                                    if (showIndivisualGuarantee)
                                        DisplayGuaranteeInformationTextFrRooms(currentRoomInfoBox,
                                                                               currentGuestInformationEntity,
                                                                               selectedRoomRates, roomNumber,
                                                                               returnValue);
                                }
                            }
                        }
                    }
                }
            }
        }

        private string GetViewModifyDLRedirectLink(string reservationID, string lastName)
        {
            string last_Name = string.Empty;
            if (SearchCriteriaSessionWrapper.SearchCriteria.SearchingType == SearchType.REDEMPTION)
                last_Name = GuestBookingInformationSessionWrapper.FetchedGuestInformation.LastName;
            else
                last_Name = lastName;

            return WebUtil.GetViewModifyDLRedirectLink(HttpContext.Current.Request.Url.Host, AppConstants.DLREDIRECT, AppConstants.DEEPLINK_VIEW_MODIFY_RESERVATION_ID,
                reservationID, AppConstants.DEEPLINK_VIEW_MODIFY_LAST_NAME, last_Name);

            //return HttpContext.Current.Request.Url.Host + "/" + AppConstants.DLREDIRECT + "?" + AppConstants.DEEPLINK_VIEW_MODIFY_RESERVATION_ID + "=" + reservationID + "&"
            //    + AppConstants.DEEPLINK_VIEW_MODIFY_LAST_NAME + "=" + last_Name;
        }

        /// <summary>
        /// Adds the child div to the room div.
        /// </summary>
        /// <param name="currentRoomInfoBox">The childern details.</param>
        /// <param name="lstChildEntity">The LST child entity.</param>
        private void AddChildDivToTheRoomDiv(ref ConfirmationPageEmailRoomInfoBox currentRoomInfoBox,
                                             IList<ChildEntity> lstChildEntity)
        {
            StringBuilder newstringBuilder = new StringBuilder();
            if ((lstChildEntity != null) && (lstChildEntity.Count > 0))
            {
                for (int iChildCount = 0; iChildCount < lstChildEntity.Count; iChildCount++)
                {
                    int iCount = iChildCount;

                    newstringBuilder.Append("<p><strong>"
                                            + WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child") +
                                            " "
                                            + (iCount + 1) + "</strong>" + " " +
                                            lstChildEntity[iChildCount].AccommodationString + "</p>");
                }
            }
            currentRoomInfoBox.ChildernBedTypeDetails = newstringBuilder.ToString();
        }


        /// <summary>
        /// Function to set the Other Control and Special Request
        /// Information 
        /// </summary>
        /// <param name="currentRoomInfoBox"></param>
        /// <param name="guestInfoEntity">Entity from where other and special request control is to populate</param>       
        private void DisplaySpecialRequestDetailsFrRooms(ref ConfirmationPageEmailRoomInfoBox currentRoomInfoBox,
                                                         GuestInformationEntity guestInfoEntity)
        {
            if (guestInfoEntity != null)
            {
                if (guestInfoEntity.SpecialRequests != null)
                {
                    //string otherPeferencesList = string.Empty;
                    //string specialRequestList = string.Empty;

                    StringBuilder otherPeferencesList = new StringBuilder();
                    StringBuilder specialRequestList = new StringBuilder();

                    int totalSpecialRequest = guestInfoEntity.SpecialRequests.Length;
                    for (int spRequestCount = 0; spRequestCount < totalSpecialRequest; spRequestCount++)
                    {
                        switch (guestInfoEntity.SpecialRequests[spRequestCount].RequestValue)
                        {
                            case UserPreferenceConstants.LOWERFLOOR:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.LOWERFLOOR));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.HIGHFLOOR:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.HIGHERFLOOR));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.AWAYFROMELEVATOR:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.AWAYFROMELEVATOR));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.NEARELEVATOR:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.NEARELEVATOR));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.NOSMOKING:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.NOSMOKING));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.SMOKING:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.SMOKING));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.ACCESSIBLEROOM:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ACCESSABLEROOM));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ACCESSABLEROOMDESC));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.ALLERGYROOM:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ALLERYROOM));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.ALLERYROOMDESC));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.PETFRIENDLYROOM:
                                {
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.PETFRIENDLYROOM));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.PETFRIENDLYROOMDESC));
                                    otherPeferencesList.Append(" <br />");
                                    otherPeferencesList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.EARLYCHECKIN:
                                {
                                    specialRequestList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.EARLYCHECKIN));
                                    specialRequestList.Append(" <br />");
                                    specialRequestList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.EARLYCHECKINDESC));
                                    specialRequestList.Append(" <br />");
                                    specialRequestList.Append(" <br />");
                                    break;
                                }
                            case UserPreferenceConstants.LATECHECKOUT:
                                {
                                    specialRequestList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.LATECHECKOUT));
                                    specialRequestList.Append(" <br />");
                                    specialRequestList.Append(WebUtil.GetTranslatedText(TranslatedTextConstansts.LATECHECKOUTDESC));
                                    specialRequestList.Append(" <br />");
                                    break;
                                }
                            default:
                                break;
                        }
                    }
                    currentRoomInfoBox.OtherPreferences = otherPeferencesList.ToString();
                    currentRoomInfoBox.SpecialServiceRequest = specialRequestList.ToString();
                }
            }
        }

        /// <summary>
        /// Displays additional request
        /// </summary>
        /// <param name="currentRoomInfoBox"></param>
        /// <param name="guestInfoEntity"></param>
        public void DisplayAdditionalRequest(ref ConfirmationPageEmailRoomInfoBox currentRoomInfoBox,
                                             GuestInformationEntity guestInfoEntity)
        {
            string additionaRequestList = string.Empty;
            if (!string.IsNullOrEmpty(guestInfoEntity.AdditionalRequest))
            {
                additionaRequestList += guestInfoEntity.AdditionalRequest;
                additionaRequestList += " <br />";
            }
            currentRoomInfoBox.AdditionalRequest = additionaRequestList;
        }


        /// <summary>
        /// Determines whether [is individual guarantee].
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if [is individual guarantee]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsIndividualGuarantee()
        {
            bool showIndividualGuarantee = false;
            SortedList<string, GuestInformationEntity> guestsList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
            if (guestsList != null && guestsList.Count > 0)
            {
                GuranteeType gType = guestsList.Values[0].GuranteeInformation.GuranteeType;

                foreach (string key in guestsList.Keys)
                {
                    if (gType != guestsList[key].GuranteeInformation.GuranteeType)
                    {
                        showIndividualGuarantee = true;
                        break;
                    }
                }
            }
            Hashtable selectedRoomRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
            if (selectedRoomRatesHashTable != null && selectedRoomRatesHashTable.Count > 1)
            {
                SelectedRoomAndRateEntity selectedRoomRate = selectedRoomRatesHashTable[0] as SelectedRoomAndRateEntity;
                for (int iterator = 1; iterator < selectedRoomRatesHashTable.Count; iterator++)
                {
                    SelectedRoomAndRateEntity currentroomRate =
                        selectedRoomRatesHashTable[iterator] as SelectedRoomAndRateEntity;
                    if (selectedRoomRate.RateCategoryID != currentroomRate.RateCategoryID)
                    {
                        showIndividualGuarantee = true;
                        break;
                    }
                }
            }

            return showIndividualGuarantee;
        }

        /// <summary>
        /// Displays the guarantee information text fr rooms.
        /// </summary>
        private void DisplayGuaranteeInformationTextFrRooms(ConfirmationPageEmailRoomInfoBox currentRoomInfoBox,
                                                            GuestInformationEntity currentGuestInformationEntity,
                                                            SelectedRoomAndRateEntity selectedRoomRates,
                                                            int roomNumber, Dictionary<string, string> returnValue)
        {
            if (returnValue != null)
            {
                currentRoomInfoBox.GuaranteeTextHeader = ((returnValue.ContainsKey("GUARANTEEANDCANCELLATIONHEADER"))
                                                              ? returnValue["GUARANTEEANDCANCELLATIONHEADER"].ToString()
                                                              : string.Empty);
            }
            currentRoomInfoBox.ShowGuaranteeForEachRoom = true;
            currentRoomInfoBox.GuaranteeInformationForEachRoom =
                Utility.GetGuaranteeText(currentGuestInformationEntity.GuranteeInformation, selectedRoomRates,
                                         roomNumber);
        }

        /// <summary>
        /// Displays the common gurantee text.
        /// </summary>
        private void DisplayCommonGuranteeText(Hashtable selectedRoomRatesHashTable)
        {
            SortedList<string, GuestInformationEntity> guestsList = GuestBookingInformationSessionWrapper.AllGuestsBookingInformations;
            if (guestsList != null && guestsList.Count > 0)
            {
                SameGuaranteeInformationText.Visible = true;
                commonGuaranteeInfo.Visible = true;
                if (selectedRoomRatesHashTable != null && selectedRoomRatesHashTable.Count > 0)
                {
                    int roomNumber = 1;
                    SelectedRoomAndRateEntity selectedRoomandRate =
                        selectedRoomRatesHashTable[0] as SelectedRoomAndRateEntity;
                    commonGuaranteeInfo.InnerHtml = Utility.GetGuaranteeText(guestsList.Values[0].GuranteeInformation,
                                                                             selectedRoomandRate, roomNumber);
                }
            }
        }

        # region Hotel info

        /// <summary>
        /// PopulateMapHeaderHotelName
        /// </summary>
        private void PopulateMapHeaderHotelName()
        {
            HotelDestination hotelDestination = null;
            string hotelID = SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode;
            if (!string.IsNullOrEmpty(hotelID))
            {
                hotelDestination = ContentDataAccess.GetHotelByOperaID(hotelID);
                if (hotelDestination != null)
                    mapHeaderHotelname.InnerText = hotelDestination.Name;
                /*}
                if ((hotelDestination != null) && (!string.IsNullOrEmpty(hotelDestination.Name)))
                {*/
                //string hotelName = hotelDestination.Name.Replace(" ", "%20");
                string findAHotelPageRelativeUrl = string.Format("{0}{1}{2}{3}", GlobalUtil.GetUrlToPage(EpiServerPageConstants.SEARCH_USING_MAP_PAGE)
                                                   , "?MapID=", hotelID, "&initialview=map&source=mail");

                string findAHotelPageAbsoluteUrl = string.Format("http://{0}{1}", Request.Url.Host, findAHotelPageRelativeUrl);
                findAHotelLink.HRef = findAHotelPageAbsoluteUrl;
            }
        }

        /// <summary>
        /// Displays the hotel information.
        /// </summary>
        /// <param name="hotelSearch">The hotel search.</param>
        private void DisplayHotelInformation(HotelSearchEntity hotelSearch)
        {
            if (hotelSearch != null)
            {
                string hotelID = hotelSearch.SelectedHotelCode;
                if (!string.IsNullOrEmpty(hotelID))
                {
                    HotelDestination hotelDestination = ContentDataAccess.GetHotelByOperaID(hotelID);
                    if (hotelDestination != null)
                    {
                        ShowHotelLocationAndContactInfo(hotelDestination);
                        ShowHotelFacilitiesInfo(hotelDestination);
                    }
                }
            }
        }


        /// <summary>
        /// Function for Setting the data in the Hotel Description Div Just
        /// Below to the Google Map
        /// </summary>
        /// <param name="hotelDestination">The hotel destination.</param>
        private void ShowHotelLocationAndContactInfo(HotelDestination hotelDestination)
        {
            if (hotelDestination != null)
            {
                if (hotelDestination.HotelAddress != null)
                {
                    StringBuilder streetAddress = new StringBuilder();
                    string address = hotelDestination.HotelAddress.StreetAddress;
                    string zip = hotelDestination.HotelAddress.PostCode;
                    string city = hotelDestination.HotelAddress.City;
                    string country = hotelDestination.HotelAddress.Country;
                    string postalCity = hotelDestination.HotelAddress.PostalCity;
                    if (!string.IsNullOrEmpty(address))
                    {
                        streetAddress.Append(address.Trim());
                        if (!string.IsNullOrEmpty(zip) || !string.IsNullOrEmpty(city) || !string.IsNullOrEmpty(country))
                            streetAddress.Append(", ");
                    }
                    if (!string.IsNullOrEmpty(zip))
                    {
                        streetAddress.Append(zip.Trim());
                        if (!string.IsNullOrEmpty(city) || !string.IsNullOrEmpty(country))
                            streetAddress.Append(", ");
                    }
                    if (city.Trim() == postalCity.Trim())
                    {
                        streetAddress.Append(postalCity.Trim());
                        streetAddress.Append(", ");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(postalCity))
                        {
                            streetAddress.Append(postalCity.Trim());
                            if (!string.IsNullOrEmpty(city))
                                streetAddress.Append(", ");
                        }
                        if (!string.IsNullOrEmpty(city))
                        {
                            streetAddress.Append(city.Trim());
                            if (!string.IsNullOrEmpty(country))
                                streetAddress.Append(", ");
                        }
                    }

                    if (!string.IsNullOrEmpty(country))
                    {
                        streetAddress.Append(country.Trim());
                    }

                    if (streetAddress.Length == 0)
                    {
                        streetAddress.Append("&nbsp;");
                    }

                    string fullAddress = streetAddress.ToString();
                    if (!string.IsNullOrEmpty(fullAddress))
                    {
                        linkHotelAddress.InnerText = fullAddress;
                    }
                    else
                    {
                        linkHotelAddress.Visible = false;
                    }
                }

                if (!string.IsNullOrEmpty(hotelDestination.Telephone))
                    lblHotelPhoneNum.InnerText = hotelDestination.Telephone;
                else
                    phoneContainer.Visible = false;

                if (!string.IsNullOrEmpty(hotelDestination.Fax))
                    lblHotelFaxNum.InnerText = hotelDestination.Fax;
                else
                    faxContainer.Visible = false;

                if (!string.IsNullOrEmpty(hotelDestination.Email))
                {
                    linkHotelEmail.HRef = "mailto:" + hotelDestination.Email;
                    linkHotelEmail.InnerText = hotelDestination.Email;
                }
                else
                    emailContainer.Visible = false;

                if (!string.IsNullOrEmpty(hotelDestination.CustomerCarePhone))
                    lblCustomerCareNumber.InnerText = hotelDestination.CustomerCarePhone;
                else
                    customerCareNumberContainer.Visible = false;

                lblLatitude.InnerText = hotelDestination.Coordinate.Y.ToString();
                lblLongitude.InnerText = hotelDestination.Coordinate.X.ToString();
            }
        }

        /// <summary>
        /// Shows the hotel facilities info.
        /// </summary>
        /// <param name="hotelDestination">The hotel destination.</param>
        private void ShowHotelFacilitiesInfo(HotelDestination hotelDestination)
        {
            if (hotelDestination != null)
            {
                string yes = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Yes");
                string no = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/No");
                string km = WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Km");

                if (hotelDestination.NoOfRooms != null)
                    lblNoOfRooms.InnerText = hotelDestination.NoOfRooms.ToString();
                else
                    noOfRoomsContainer.Visible = false;

                if (hotelDestination.NoOfNonSmokingRooms != null)
                    lblNoOfNonSmokingRooms.InnerText = hotelDestination.NoOfNonSmokingRooms.ToString();
                else
                    noOfNonSmokingRoomsContainer.Visible = false;


                lblRoomsForDisabledAvailable.InnerText = hotelDestination.IsRoomsForDisabledAvailable ? yes : no;
                lblRelaxCenterAvailable.InnerText = hotelDestination.IsRelaxCenterAvailable ? yes : no;
                lblRestaurantBar.InnerText = hotelDestination.IsRestaurantAndBarAvailable ? yes : no;
                lblGarage.InnerText = hotelDestination.IsGarageAvailable ? yes : no;
                lblOutdoorParking.InnerText = hotelDestination.IsOutDoorParkingAvailable ? yes : no;
                lblShop.InnerText = hotelDestination.IsShopAvailable ? yes : no;
                lblMeetingFacilities.InnerText = hotelDestination.IsMeetingRoomAvailable ? yes : no;
                lblWirelessInternet.InnerText = hotelDestination.IsWiFiAvailable ? yes : no;

                if (!string.IsNullOrEmpty(hotelDestination.EcoLabeled))
                    lblEcoLabeled.InnerText = hotelDestination.EcoLabeled;
                else
                    ecoLabeledContainer.Visible = false;

                lblDistanceCentre.InnerText = hotelDestination.CityCenterDistance.ToString() + " " + km;

                if (!string.IsNullOrEmpty(hotelDestination.Airport1Name))
                {
                    lblAirportName.InnerText = hotelDestination.Airport1Name;
                    lblDistanceAirport.InnerText = hotelDestination.Airport1Distance.ToString() + " " + km;
                }
                else
                {
                    airportDistanceContainer.Visible = false;
                }

                if (hotelDestination.TrainStationDistance != null)
                    lblDistanceTrain.InnerText = hotelDestination.TrainStationDistance.ToString() + " " + km;
                else
                    trainStationDistContainer.Visible = false;
            }
        }

        /// <summary>
        /// Displays the FGP and credit card info.
        /// </summary>
        /// <param name="currentRoomInfoBox">The current room info box.</param>
        /// <param name="currentGuestInformationEntity">The current guest information entity.</param>
        private void DisplayFGPAndCreditCardInfo(ConfirmationPageEmailRoomInfoBox currentRoomInfoBox,
                                                 GuestInformationEntity currentGuestInformationEntity)
        {
            if (currentGuestInformationEntity.GuestAccountNumber == string.Empty
                || !currentGuestInformationEntity.IsValidFGPNumber)
            {
                currentRoomInfoBox.FGPNumber = string.Empty;
            }
            else
            {
                currentRoomInfoBox.FGPNumber = currentGuestInformationEntity.GuestAccountNumber;
            }


            if (currentGuestInformationEntity.GuranteeInformation != null)
            {
                if (GuranteeType.CREDITCARD == currentGuestInformationEntity.GuranteeInformation.GuranteeType)
                {
                    if (currentGuestInformationEntity.GuranteeInformation.CreditCard != null)
                    {
                        string cardnumber = currentGuestInformationEntity.GuranteeInformation.CreditCard.CardNumber;
                        if (cardnumber.Length >= AppConstants.CREDIT_CARD_DISPLAY_CHARS)
                        {
                            if (!cardnumber.Contains(AppConstants.CARD_MASK_CHAR))
                            {
                                currentRoomInfoBox.CreditCardNumber = AppConstants.CARD_MASK + cardnumber.Substring((cardnumber.Length -
                                                                                            AppConstants.
                                                                                                CREDIT_CARD_DISPLAY_CHARS));
                            }
                            else
                            {
                                currentRoomInfoBox.CreditCardNumber = cardnumber;
                            }
                        }
                        else
                        {
                            currentRoomInfoBox.CreditCardNumber = AppConstants.CARD_MASK + cardnumber;
                        }

                        currentRoomInfoBox.ExpiryDate =
                            currentGuestInformationEntity.GuranteeInformation.CreditCard.ExpiryDate.ToDateTime().
                                ToString("MM/yyyy");
                        currentRoomInfoBox.CreditCardType =
                            currentGuestInformationEntity.GuranteeInformation.CreditCard.CardType;
                        if (currentGuestInformationEntity.GuranteeInformation.CreditCard.CardType.StartsWith(AppConstants.PREPAID_CCTYPE_PREFIX))
                        {
                            currentRoomInfoBox.CreditCardHolder = string.Empty;
                            currentRoomInfoBox.CardHolderName = false;
                        }
                        else
                        {
                            currentRoomInfoBox.CreditCardHolder =
                                currentGuestInformationEntity.GuranteeInformation.CreditCard.NameOnCard;
                        }
                    }
                }
                else
                {
                    currentRoomInfoBox.CreditCardNumber = string.Empty;
                    currentRoomInfoBox.ExpiryDate = string.Empty;
                    currentRoomInfoBox.CreditCardType = string.Empty;
                    currentRoomInfoBox.CreditCardHolder = string.Empty;
                }
            }
        }

        #endregion

        /// <summary>
        /// SetTotalRateAndCurrencyCode
        /// </summary>
        private void SetTotalRateAndCurrencyCode()
        {
            HotelSearchEntity searchCriteria = SearchCriteriaSessionWrapper.SearchCriteria;
            Hashtable selectedRoomAndRatesHashTable = HotelRoomRateSessionWrapper.SelectedRoomAndRatesHashTable;
            double totalRate = 0;
            double totalRateNonBC = 0;
            int totalNonBCRooms = 0;
            string currencyCode = string.Empty;
            BaseRateDisplay firstRateDisplayObject = null;
            SelectedRoomAndRateEntity firstSelectedRoomAndRateEntity = null;
            int roomCount = searchCriteria.ListRooms.Count;
            if (searchCriteria != null && searchCriteria.ListRooms != null && roomCount > 0)
            {
                if (BookingEngineSessionWrapper.HideARBPrice)
                {
                    this.totalRateString.InnerText = Utility.GetPrepaidString();
                    pCartTerms.InnerHtml = string.Empty;
                }
                else
                {
                    for (int i = 0; i < roomCount; i++)
                    {

                        if (selectedRoomAndRatesHashTable != null && selectedRoomAndRatesHashTable.ContainsKey(i))
                        {
                            //getting a dummy rateCellDisplay Object to call total rate method.
                            int firstRoom = 0;
                            firstSelectedRoomAndRateEntity = selectedRoomAndRatesHashTable[0] as SelectedRoomAndRateEntity;
                            firstRateDisplayObject = Utility.GetRateCellDisplayObject(firstRoom, firstSelectedRoomAndRateEntity);
                            //
                            SelectedRoomAndRateEntity selectedRoomAndRateEntity = selectedRoomAndRatesHashTable[i] as SelectedRoomAndRateEntity;

                            if (selectedRoomAndRateEntity != null)
                            {
                                List<RoomRateEntity> selectedRoomRates = selectedRoomAndRateEntity.RoomRates;
                                if (selectedRoomRates != null && selectedRoomRates.Count > 0)
                                {
                                    RoomRateEntity roomRate = selectedRoomRates[0];
                                    if (roomRate != null)
                                    {
                                        HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
                                        if (search != null)
                                        {
                                            if (search.SearchingType == SearchType.REDEMPTION)
                                            {
                                                totalRate += roomRate.PointsDetails.PointsRequired;
                                                currencyCode = string.Empty;
                                            }
                                            else if (search.SearchingType == SearchType.BONUSCHEQUE)
                                            {
                                                if (roomRate.RatePlanCode == "BC")
                                                {
                                                    totalRate += roomRate.TotalRate.Rate;
                                                    currencyCode = roomRate.TotalRate.CurrencyCode;
                                                }
                                                else
                                                {
                                                    totalRateNonBC += roomRate.TotalRate.Rate;
                                                    currencyCode = roomRate.TotalRate.CurrencyCode;
                                                    totalNonBCRooms += 1;
                                                }
                                            }
                                            else
                                            {
                                                totalRate += roomRate.TotalRate.Rate;
                                                currencyCode = roomRate.TotalRate.CurrencyCode;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    this.totalRateString.InnerText = totalRate + " " + currencyCode;

                    if (firstRateDisplayObject != null)
                    {
                        string totalRateValue = string.Empty;
                        totalRateValue = firstRateDisplayObject.GetTotalRateDisplayStringForShoppingCart(
                            true, totalRate, currencyCode);

                        totalRateString.InnerText = totalRateValue.Trim().TrimEnd(currencyCode.ToCharArray()).Trim();
                        if (searchCriteria.SearchingType != SearchType.VOUCHER)
                            currencyCodeString.InnerText = currencyCode;
                    }
                    else
                    {
                        SetTotalPriceWhenContactDetaislModified(searchCriteria.HotelCountryCode);
                    }
                    pCartTerms.InnerHtml = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/InclTaxesFees");
                }
            }
        }

        /// <summary>
        /// This method to calculate total price should only be used for scenario where contact details of custormer are modified, but not rate or date.
        /// </summary>
        /// <param name="hotelCountryCode"></param>
        private void SetTotalPriceWhenContactDetaislModified(string hotelCountryCode)
        {
            
            BookingDetailsEntity bookingDetails = BookingEngineSessionWrapper.BookingDetails;

            if (bookingDetails != null)
            {
                HotelSearchEntity hotelSearchEntity = bookingDetails.HotelSearch;
                HotelRoomRateEntity hotelRoomRateEntity = bookingDetails.HotelRoomRate;

                if ((hotelSearchEntity != null) && (hotelRoomRateEntity != null))
                {

                    TimeSpan timeSpan = hotelSearchEntity.ArrivalDate.Subtract(hotelSearchEntity.DepartureDate);
                    double basePoints = 0;
                    double totalPoints = 0;
                    basePoints = hotelRoomRateEntity.Points;
                    try
                    {
                        if (hotelSearchEntity.NoOfNights > 1)
                            basePoints = hotelRoomRateEntity.Points / hotelSearchEntity.NoOfNights;
                    }
                    catch (Exception ex)
                    {
                        AppLogger.LogFatalException(ex, "Unable to calculate base points.");
                    }

                    if (hotelRoomRateEntity.TotalRate != null)
                    {
                        totalPoints = hotelRoomRateEntity.TotalRate.Rate;
                    }

                    hotelSearchEntity.HotelCountryCode = (string.IsNullOrEmpty(hotelSearchEntity.HotelCountryCode)) ? hotelCountryCode : hotelSearchEntity.HotelCountryCode;

                    double dblTotalRate = Utility.CalculateTotalRateInCaseOfModifyFlow();
                    string totalRateString = string.Empty;
                    switch (hotelSearchEntity.SearchingType)
                    {
                        case SearchType.REDEMPTION:
                            {
                                totalRateString =
                                    Utility.GetRoomRateString
                                        (null, null, hotelRoomRateEntity.Points, hotelSearchEntity.SearchingType,
                                         hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                         hotelSearchEntity.NoOfNights,
                                         hotelSearchEntity.RoomsPerNight);
                                break;
                            }
                        case SearchType.VOUCHER:
                            {
                                totalRateString =
                                    Utility.GetRoomRateString(null, null, totalPoints, hotelSearchEntity.SearchingType,
                                                              hotelSearchEntity.ArrivalDate.Year, hotelCountryCode,
                                                              hotelSearchEntity.NoOfNights,
                                                              hotelSearchEntity.RoomsPerNight);
                                break;
                            }
                        default:
                            {
                                RateEntity tempRateEntity =
                                    new RateEntity(dblTotalRate, hotelRoomRateEntity.TotalRate.CurrencyCode);
                                totalRateString =
                                    Utility.GetRoomRateString(null, tempRateEntity, totalPoints,
                                                              hotelSearchEntity.SearchingType,
                                                              hotelSearchEntity.ArrivalDate.Year,
                                                              hotelCountryCode, hotelSearchEntity.NoOfNights,
                                                              hotelSearchEntity.RoomsPerNight);
                                break;
                            }
                    }
                    this.totalRateString.InnerText = totalRateString;
                }
            }
            
        }

        #endregion

        #region cancel Flow

        /// <summary>
        /// PopulateCancelInfoHashTable
        /// </summary>
        /// <returns>SortedList</returns>
        private SortedList<int, Dictionary<string, string>> PopulateCancelInfoHashTable()
        {
            SortedList<int, Dictionary<string, string>> cancellationInfoList = new SortedList<int, Dictionary<string, string>>();

            List<CancelDetailsEntity> cancelDetailsList = BookingEngineSessionWrapper.AllCancelledDetails;
            if (cancelDetailsList != null && cancelDetailsList.Count > 0)
            {
                System.Globalization.TextInfo tInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
                for (int i = 0; i < cancelDetailsList.Count; i++)
                {
                    Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
                    CancelDetailsEntity cancelEntity = cancelDetailsList[i];
                    if (cancelEntity != null)
                    {
                        int currentRoomNumber = Convert.ToInt32(cancelEntity.LegNumber) - 1;

                        BookingDetailsEntity bookingDetails = Utility.GetBookingDetail(cancelEntity.LegNumber);

                        if (bookingDetails != null && (bookingDetails.HotelSearch != null))
                        {
                            HotelDestination hotelDestination = ContentDataAccess.GetHotelByOperaID(bookingDetails.HotelSearch.SelectedHotelCode);
                            if (hotelDestination != null)
                                cancellationInfo.Add(CommunicationTemplateConstants.CITY_HOTEL, hotelDestination.Name);

                            IFormatProvider cultureInfo =
                            new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
                            string arrivalDate = WebUtil.GetDayFromDate(bookingDetails.HotelSearch.ArrivalDate) + AppConstants.SPACE + Utility.GetFormattedDate(bookingDetails.HotelSearch.ArrivalDate);
                            string departureDate = WebUtil.GetDayFromDate(bookingDetails.HotelSearch.DepartureDate) + AppConstants.SPACE + Utility.GetFormattedDate(bookingDetails.HotelSearch.DepartureDate);

                            cancellationInfo.Add(CommunicationTemplateConstants.ARRIVAL_DATE, arrivalDate);
                            cancellationInfo.Add(CommunicationTemplateConstants.DEPARTURE_DATE, departureDate);
                            cancellationInfo.Add(CommunicationTemplateConstants.NUMBER_OF_DAYS, bookingDetails.HotelSearch.NoOfNights.ToString());
                            string currentNoOfRooms = ((BookingEngineSessionWrapper.AllCancelledDetails != null) ? BookingEngineSessionWrapper.AllCancelledDetails.Count.ToString() : string.Empty);
                            cancellationInfo.Add(CommunicationTemplateConstants.NUMBER_ROOM, currentNoOfRooms);
                            HotelRoomRateEntity hotelRoomRateEntity = bookingDetails.HotelRoomRate;
                            HotelSearchEntity hotelSearchEntity = bookingDetails.HotelSearch;
                            if (hotelRoomRateEntity != null)
                            {
                                double basePoints = 0;

                                basePoints = hotelRoomRateEntity.Points;
                                double totalPoints = 0;

                                basePoints = hotelRoomRateEntity.Points;
                                if (hotelRoomRateEntity.TotalRate != null)
                                {
                                    totalPoints = hotelRoomRateEntity.TotalRate.Rate;
                                }

                                string ratePlanCode = hotelRoomRateEntity.RatePlanCode;
                                string selectedRateCategoryName = string.Empty;

                                if ((ratePlanCode != null) && (ratePlanCode != string.Empty))
                                {
                                    Rate rate = RoomRateUtil.GetRate(ratePlanCode);
                                    if (rate != null && hotelSearchEntity.SearchingType != SearchType.CORPORATE)
                                    {
                                        cancellationInfo.Add(CommunicationTemplateConstants.RATE_CATEGORY, rate.RateCategoryName);
                                    }
                                }

                                #region CorporateBooking

                                if (hotelSearchEntity.SearchingType == SearchType.CORPORATE)
                                {
                                    BaseRateDisplay rateDisplay = null;
                                    bool isTrue = false;

                                    RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlanCode);
                                    string baseRateStringForCancelRoomSummary = hotelRoomRateEntity.Rate.Rate + AppConstants.SPACE + hotelRoomRateEntity.Rate.CurrencyCode;
                                    cancellationInfo.Add(CommunicationTemplateConstants.ROOM_RATE_SMS, baseRateStringForCancelRoomSummary);


                                    IList<BaseRoomRateDetails> listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;

                                    if (Utility.IsBlockCodeBooking)
                                    {
                                        if (!cancellationInfo.ContainsKey(CommunicationTemplateConstants.RATE_CATEGORY))
                                            cancellationInfo.Add(CommunicationTemplateConstants.RATE_CATEGORY, bookingDetails.HotelSearch.CampaignCode);
                                    }
                                    if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                                    {
                                        if (!Utility.IsBlockCodeBooking)
                                        {
                                            foreach (BaseRoomRateDetails roomDtls in listRoomRateDetails)
                                            {
                                                if (roomDtls != null)
                                                {
                                                    foreach (RateCategory rateCtgry in roomDtls.RateCategories)
                                                    {
                                                        rateDisplay = new NegotiatedRoomRateDisplay(roomDtls as NegotiatedRoomRateDetails);

                                                        if (rateCategory != null && rateCategory.RateCategoryId != null && rateCategory.RateCategoryId == rateCtgry.RateCategoryId)
                                                        {
                                                            isTrue = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (isTrue)
                                                {
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (rateDisplay != null)
                                    {


                                        foreach (RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                                        {

                                            if (rateCategory != null && rateCategory.RateCategoryId != null && rateCategory.RateCategoryId == hdrDisplay.Id)
                                            {
                                                if (!cancellationInfo.ContainsKey(CommunicationTemplateConstants.RATE_CATEGORY))
                                                    cancellationInfo.Add(CommunicationTemplateConstants.RATE_CATEGORY, hdrDisplay.Title);
                                            }
                                        }

                                    }
                                }
                                #endregion

                                /*if ((ratePlanCode != null) && (ratePlanCode != string.Empty))
                                    {
                                        Rate rate = RoomRateUtil.GetRate(ratePlanCode);
                                        if (rate != null)
                                        {
                                            cancellationInfo.Add(CommunicationTemplateConstants.RATE_CATEGORY, rate.RateCategoryName);
                                        }
                                    }
                                    else if (Utility.IsBlockCodeBooking)
                                    {
                                        Block block = Scandic.Scanweb.CMS.DataAccessLayer.ContentDataAccess.GetBlockCodePages(SessionWrapper.SearchCriteria.CampaignCode);
                                        if (block != null)
                                        {
                                            cancellationInfo.Add(CommunicationTemplateConstants.RATE_CATEGORY, block.BlockCategoryName);
                                        }
                                    }*/

                                string baseRateString = Utility.GetRoomRateString(selectedRateCategoryName, hotelRoomRateEntity.Rate, basePoints,
                                bookingDetails.HotelSearch.SearchingType, bookingDetails.HotelSearch.ArrivalDate.Year, bookingDetails.HotelSearch.HotelCountryCode, AppConstants.PER_NIGHT, AppConstants.PER_ROOM);

                                //Scandic Merchandising:SCANMOB-254:Room 2 cancelation email should not show no. of gift vouchers- it should show only gift voucher 
                                if (bookingDetails.HotelSearch.SearchingType == SearchType.VOUCHER)
                                {
                                    string currentLanguage = Utility.GetCurrentLanguage();
                                    baseRateString = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/normalratestringwithnocategory");
                                    baseRateString = string.Format(baseRateString, string.Empty, WebUtil.GetTranslatedText("/bookingengine/booking/selectrate/voucherrate", currentLanguage));

                                }

                                if (BookingEngineSessionWrapper.HideARBPrice)
                                {
                                    cancellationInfo.Add(CommunicationTemplateConstants.ROOM_RATE, Utility.GetPrepaidString());
                                }
                                else
                                {
                                    cancellationInfo.Add(CommunicationTemplateConstants.ROOM_RATE, baseRateString);
                                }

                                // Vrushali | Res 2.0 | New method to calculate total rate added now.
                                // double dblTotalRate = Utility.CalculateTotalRateInCaseOfModifyFlow();
                                //Res 2.0 - Parvathi : Cancelled Booking CR
                                double dblTotalRate = Utility.CalculateTotalRateInCaseOfCancelFlow();
                                string totalRateString = string.Empty;
                                if (hotelRoomRateEntity.TotalRate != null)
                                {
                                    // Change request in R1.2. Story Id: 501388.
                                    //Vrushali | Res 2.0 | This is fix for total rate.
                                    RateEntity tempRateEntity = new RateEntity(dblTotalRate, hotelRoomRateEntity.TotalRate.CurrencyCode);
                                    totalRateString = Utility.GetRoomRateString(null, tempRateEntity, totalPoints, hotelSearchEntity.SearchingType,
                                        hotelSearchEntity.ArrivalDate.Year, hotelSearchEntity.HotelCountryCode, hotelSearchEntity.NoOfNights, cancelDetailsList.Count);
                                }
                                if (bookingDetails.HotelSearch.SearchingType == SearchType.REDEMPTION)
                                {
                                    totalRateString = Utility.GetRoomRateString(null, null, basePoints,
                                        bookingDetails.HotelSearch.SearchingType, bookingDetails.HotelSearch.ArrivalDate.Year, bookingDetails.HotelSearch.HotelCountryCode, bookingDetails.HotelSearch.NoOfNights, bookingDetails.HotelSearch.RoomsPerNight);
                                }
                                cancellationInfo.Add(CommunicationTemplateConstants.TOTAL_PRICE, totalRateString);

                                string cancellationNumber = cancelEntity.CancelNumber;
                                cancellationInfo.Add(CommunicationTemplateConstants.CANCEL_CONFIRMATION_NUMBER, cancellationNumber);
                                string reservationNumberWithLeg = cancelEntity.ReservationNumber + AppConstants.HYPHEN + cancelEntity.LegNumber;
                                cancellationInfo.Add(CommunicationTemplateConstants.CONFIRMATION_NUMBER, reservationNumberWithLeg);
                                cancellationInfo.Add(CommunicationTemplateConstants.MAINRESERVATIONNUMBER, cancelEntity.ReservationNumber);
                                cancellationInfo.Add(CommunicationTemplateConstants.LEGNUMBER, cancelEntity.LegNumber);
                                //Ramana:Added as part of title for name.
                                cancellationInfo.Add(CommunicationTemplateConstants.TITLE, tInfo.ToTitleCase(bookingDetails.GuestInformation.Title));
                                cancellationInfo.Add(CommunicationTemplateConstants.FIRST_NAME, tInfo.ToTitleCase(bookingDetails.GuestInformation.FirstName));
                                cancellationInfo.Add(CommunicationTemplateConstants.LASTNAME, tInfo.ToTitleCase(bookingDetails.GuestInformation.LastName));
                                OrderedDictionary countryCodes = DropDownService.GetCountryCodes();
                                if (countryCodes != null)
                                {
                                    string country = countryCodes[bookingDetails.GuestInformation.Country] as string;
                                    cancellationInfo.Add(CommunicationTemplateConstants.COUNTRY, country);
                                }
                                // cancellationInfo.Add(CommunicationTemplateConstants.CITY, bookingDetails.GuestInformation.City);
                                cancellationInfo.Add(CommunicationTemplateConstants.EMAIL, (bookingDetails.GuestInformation != null && bookingDetails.GuestInformation.EmailDetails != null) ? bookingDetails.GuestInformation.EmailDetails.EmailID : string.Empty);
                                cancellationInfo.Add(CommunicationTemplateConstants.TELEPHONE1, (bookingDetails.GuestInformation != null && bookingDetails.GuestInformation.Mobile != null) ? Convert.ToString(bookingDetails.GuestInformation.Mobile.Number) : string.Empty);
                                //R2.0 - Bhavya - Aritifact - artf1162629: Membership number missing. - TO poulate Membership number n cancellation flow
                                if (bookingDetails.GuestInformation != null && bookingDetails.GuestInformation.GuestAccountNumber != null)
                                {
                                    cancellationInfo.Add(CommunicationTemplateConstants.MEMBERSHIPNUMBER, bookingDetails.GuestInformation.GuestAccountNumber.ToString());
                                }

                            }

                            cancellationInfoList.Add(i, cancellationInfo);
                        }


                    }

                }
            }
            return cancellationInfoList;
        }

        /// <summary>
        /// Populates the main confirmation box in cancel flow.
        /// </summary>
        /// <param name="cancellationInfoList"></param>
        /// <param name="cancelledRoomNumber"></param>
        private void PopulateMainConfirmationBoxInCancelFlow(
            SortedList<int, Dictionary<string, string>> cancellationInfoList, int cancelledRoomNumber)
        {
            mainReservationInfoBox.Visible = true;
            roomSummaryInformationGroup.Visible = true;
            string currentHotelName = string.Empty;
            string currentCheckInDate = string.Empty;
            string currentChekOutDate = string.Empty;
            string currentNoOfNights = string.Empty;
            string currentNoOfRooms = string.Empty;
            string currentMainReservationNumber = string.Empty;
            string mainReservationNumber = string.Empty;
            string totalRates = string.Empty;
            if (cancellationInfoList != null && cancellationInfoList.Count > 0)
            {
                foreach (Dictionary<string, string> currentDict in cancellationInfoList.Values)
                {
                    if (currentDict != null)
                    {
                        int cancelledLegNumber =
                            Convert.ToInt32(currentDict[CommunicationTemplateConstants.LEGNUMBER]) - 1;
                        mainReservationNumber =
                            currentDict[CommunicationTemplateConstants.MAINRESERVATIONNUMBER].ToString();
                        currentHotelName = currentDict[CommunicationTemplateConstants.CITY_HOTEL].ToString();
                        currentCheckInDate = currentDict[CommunicationTemplateConstants.ARRIVAL_DATE].ToString();
                        currentChekOutDate = currentDict[CommunicationTemplateConstants.DEPARTURE_DATE].ToString();
                        currentNoOfNights = currentDict[CommunicationTemplateConstants.NUMBER_OF_DAYS].ToString();
                        currentNoOfRooms = currentDict[CommunicationTemplateConstants.NUMBER_ROOM].ToString();
                        totalRates = currentDict[CommunicationTemplateConstants.TOTAL_PRICE].ToString();
                        break;
                    }
                }
            }

            mainBookingReservationNumberID.InnerHtml =
                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/reservationNumber") +
                mainReservationNumber + "<br />";

            if (cancelCounter == 1)
            {
                mainBookingReservationNumberID.InnerHtml += "<br/>"
                                                            +
                                                            WebUtil.GetTranslatedText(
                                                                "/bookingengine/booking/CancelledBooking/cancellationNumber") +
                                                            ":" + cancelNumbers;
            }
            else if (cancelCounter > 1)
            {
                mainBookingReservationNumberID.InnerHtml += "<br/>"
                                                            +
                                                            WebUtil.GetTranslatedText(
                                                                "/bookingengine/booking/CancelledBooking/cancellationNumbers") +
                                                            cancelNumbers;
            }

            hotelName.InnerHtml = currentHotelName;

            if (Utility.GetTotalAdultsCancellation() > 1)
            {
                occupantsDetails.InnerHtml = Utility.GetTotalAdultsCancellation() + " "
                                             + WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/adults");
            }
            else
            {
                occupantsDetails.InnerHtml = Utility.GetTotalAdultsCancellation() + " "
                                             + WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Adult");
            }

            if (Utility.GetTotalChildrenCancellation() > 1)
            {
                occupantsDetails.InnerHtml = occupantsDetails.InnerHtml + "," + " "
                                             + Utility.GetTotalChildrenCancellation() + " "
                                             +
                                             WebUtil.GetTranslatedText(
                                                 "/bookingengine/booking/childrensdetails/children");
            }
            else if (Utility.GetTotalChildrenCancellation() == 1)
            {
                occupantsDetails.InnerHtml = occupantsDetails.InnerHtml + ","
                                             + " " + Utility.GetTotalChildrenCancellation() + " "
                                             +
                                             WebUtil.GetTranslatedText("/bookingengine/booking/childrensdetails/child");
            }

            checkInDate.InnerHtml = currentCheckInDate;
            checkOutDate.InnerHtml = currentChekOutDate;
            noOfNights.InnerHtml = currentNoOfNights;
            noOfRooms.InnerHtml = currentNoOfRooms;
            if (BookingEngineSessionWrapper.HideARBPrice)
            {
                totalRateString.InnerHtml = Utility.GetPrepaidString();
                pCartTerms.InnerHtml = string.Empty;
            }
            else
            {
                totalRateString.InnerHtml = totalRates;
                pCartTerms.InnerHtml = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/InclTaxesFees");

            }
        }

        /// <summary>
        /// Populates the confirmation page email room info box.
        /// </summary>
        /// <param name="cancellationInfoList"></param>
        /// <param name="roomNumber"></param>
        /// <param name="isAllRoomsToBeShown"></param>
        private void PopulateConfirmationPageEmailRoomInfoBoxInCancelFlow(
            SortedList<int, Dictionary<string, string>> cancellationInfoList, int roomNumber, bool isAllRoomsToBeShown)
        {
            if (cancellationInfoList != null && cancellationInfoList.Count > 0)
            {
                foreach (Dictionary<string, string> currentDict in cancellationInfoList.Values)
                {
                    if (currentDict != null)
                    {
                        int currentRoom = Convert.ToInt32(currentDict[CommunicationTemplateConstants.LEGNUMBER]) - 1;
                        if ((isAllRoomsToBeShown) || ((!isAllRoomsToBeShown) && (roomNumber == currentRoom)))
                        {
                            ConfirmationPageEmailRoomInfoBox currentRoomInfoBox =
                                this.FindControl("RoomInformation" + currentRoom.ToString()) as
                                ConfirmationPageEmailRoomInfoBox;
                            if (currentRoomInfoBox != null)
                            {

                                {
                                    currentRoomInfoBox.Visible = true;
                                    currentRoomInfoBox.RoomIdentifier = currentRoom.ToString();
                                    currentRoomInfoBox.HotelName =
                                        currentDict[CommunicationTemplateConstants.CITY_HOTEL].ToString();
                                    currentRoomInfoBox.ArrivalDate =
                                        currentDict[CommunicationTemplateConstants.ARRIVAL_DATE].ToString();
                                    currentRoomInfoBox.DeparturDate =
                                        currentDict[CommunicationTemplateConstants.DEPARTURE_DATE].ToString();
                                    currentRoomInfoBox.IsRoomTypeRowToBeShown = false;

                                    if (currentDict != null &&
                                        currentDict.ContainsKey(CommunicationTemplateConstants.RATE_CATEGORY))
                                    {
                                        currentRoomInfoBox.RateTypeID =
                                            currentDict[CommunicationTemplateConstants.RATE_CATEGORY].ToString();
                                    }
                                    currentRoomInfoBox.RateString =
                                        currentDict[CommunicationTemplateConstants.ROOM_RATE].ToString();
                                    currentRoomInfoBox.IsRoomTypeRowToBeShown = false;
                                    currentRoomInfoBox.IsShowContactInfoTable = true;

                                    currentRoomInfoBox.Name =
                                        Utility.GetNameWithTitle(
                                            currentDict[CommunicationTemplateConstants.TITLE].ToString(),
                                            currentDict[CommunicationTemplateConstants.FIRST_NAME].ToString(),
                                            currentDict[CommunicationTemplateConstants.LASTNAME].ToString());

                                    currentRoomInfoBox.Country =
                                        currentDict[CommunicationTemplateConstants.COUNTRY].ToString();
                                    currentRoomInfoBox.EmailAddress =
                                        currentDict[CommunicationTemplateConstants.EMAIL].ToString();
                                    currentRoomInfoBox.PhoneNumber =
                                        currentDict[CommunicationTemplateConstants.TELEPHONE1].ToString();
                                    currentRoomInfoBox.IsAdultAndChildInfoTableToBeShown = false;
                                    currentRoomInfoBox.CancellationNumber =
                                        currentDict[CommunicationTemplateConstants.CANCEL_CONFIRMATION_NUMBER].ToString();
                                    currentRoomInfoBox.ReservationNumber =
                                        currentDict[CommunicationTemplateConstants.CONFIRMATION_NUMBER].ToString();

                                    if (currentDict.ContainsKey(CommunicationTemplateConstants.MEMBERSHIPNUMBER))
                                    {
                                        currentRoomInfoBox.FGPNumber =
                                            currentDict[CommunicationTemplateConstants.MEMBERSHIPNUMBER].ToString();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region common to Booking / Modify And Cancel Flow

        /// <summary>
        /// Sets the did you know and other important info text for email template.
        /// </summary>
        /// <param name="bookingType">Type of the booking.</param>
        private void SetDidYouKnowAndOtherImportantInfoTextForEmailTemplate(string bookingType)
        {
            Dictionary<string, string> returnValue = null;
            //mobileLangage variable should be set to null for Scanweb.
            var mobileLanguage = Session[ScanwebMobile.MOBILE_CONTENT_LANGUAGE] as string;
            returnValue = ContentDataAccess.GetConfirmationEmailTextFormCMS(bookingType, mobileLanguage);
            if (returnValue != null)
            {
                confirmationHeading.InnerHtml = ((returnValue.ContainsKey("CONFIRMATIONTEXT"))
                                                     ? returnValue["CONFIRMATIONTEXT"].ToString()
                                                     : string.Empty);
                string contactUsHelp = ((returnValue.ContainsKey("CONTACTUSTEXT"))
                                            ? returnValue["CONTACTUSTEXT"].ToString()
                                            : string.Empty);
                if (!string.IsNullOrEmpty(contactUsHelp))
                {
                    tblContactUs.Visible = true;
                    contactUsHelpText.Text = contactUsHelp;
                }
                else
                {
                    tblContactUs.Visible = false;
                }

                DidYouKnowHeader.InnerHtml = ((returnValue.ContainsKey("DIDYOUKNOWTEXTHEADER"))
                                                  ? returnValue["DIDYOUKNOWTEXTHEADER"].ToString()
                                                  : string.Empty);
                DidYouKnow.InnerHtml = ((returnValue.ContainsKey("DIDYOUKNOWTEXT"))
                                            ? returnValue["DIDYOUKNOWTEXT"].ToString()
                                            : string.Empty);
                OtherImportantInformationHeader.InnerHtml =
                    ((returnValue.ContainsKey("OTHERIMPORTANTINFORMATIONTEXTHEADER"))
                         ? returnValue["OTHERIMPORTANTINFORMATIONTEXTHEADER"].ToString()
                         : string.Empty);
                OtherImportantInformation.InnerHtml = ((returnValue.ContainsKey("OTHERIMPORTANTINFORMATIONTEXT"))
                                                           ? returnValue["OTHERIMPORTANTINFORMATIONTEXT"].ToString()
                                                           : string.Empty);
                commonGuaranteeInfoHeader.Text = ((returnValue.ContainsKey("GUARANTEEANDCANCELLATIONHEADER"))
                                                      ? returnValue["GUARANTEEANDCANCELLATIONHEADER"].ToString()
                                                      : string.Empty);

                if (DidYouKnow.InnerHtml == string.Empty)
                    didYouKnowRow.Visible = false;
                if (OtherImportantInformation.InnerHtml == string.Empty)
                    otherImportantInfoRow.Visible = false;
            }
        }


        /// <summary>
        /// Sets the confirmation headings.
        /// </summary>
        /// <param name="bookingType">Type of the booking.</param>
        private void SetConfirmationHeadings(string bookingType)
        {
            switch (bookingType)
            {
                case PageIdentifier.BookingPageIdentifier:
                    {
                        mainconfirmationHeading.InnerText =
                            WebUtil.GetTranslatedText("/bookingengine/booking/confirmation/subject");
                        break;
                    }
                case PageIdentifier.ModifyBookingPageIdentifier:
                    {
                        mainconfirmationHeading.InnerText =
                            WebUtil.GetTranslatedText("/bookingengine/booking/modifyconfirmation/subject");
                        break;
                    }
                case PageIdentifier.CancelBookingPageIdentifier:
                    {
                        mainconfirmationHeading.InnerText =
                            WebUtil.GetTranslatedText("/bookingengine/booking/cancelconfirmation/subject");

                        break;
                    }
            }
        }

        /// <summary>
        /// Sets the cancellation number.
        /// </summary>
        /// <param name="currentRoomInfoBox">The current room info box.</param>
        /// <param name="legNumber">The leg number.</param>
        private void SetCancellationFields(ConfirmationPageEmailRoomInfoBox currentRoomInfoBox, string legNumber)
        {
            List<CancelDetailsEntity> cancelDetailsList = BookingEngineSessionWrapper.AllCancelledDetails;
            if (cancelDetailsList != null && cancelDetailsList.Count > 0)
            {
                foreach (CancelDetailsEntity eachCancelDetails in cancelDetailsList)
                {
                    if (eachCancelDetails.LegNumber == legNumber)
                    {
                        currentRoomInfoBox.CancellationNumber = eachCancelDetails.CancelNumber;
                    }
                    else
                        currentRoomInfoBox.CancellationNumber = string.Empty;
                }
            }
            rowGroupForConfirmation.Attributes["style"] = "display : none;";
        }

        #endregion

        #endregion
    }
}