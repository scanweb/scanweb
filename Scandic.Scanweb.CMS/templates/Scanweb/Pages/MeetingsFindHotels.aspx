<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.MeetingsFindHotels"
    Codebehind="MeetingsFindHotels.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageDefault.master" %>

<%@ Register TagPrefix="Scanweb" TagName="SearchMeetingList" Src="~/Templates/Scanweb/Units/Static/SearchMeetingList.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="SearchMeeting" Src="~/Templates/Scanweb/Units/Placeable/MeetingSearch.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="SearchMeetingGoogleMap" Src="~/Templates/Scanweb/Units/Static/MeetingSearchGoogleMap.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="RightColumnWide" Src="~/Templates/Scanweb/Units/Placeable/RightColumnWide.ascx" %>
<asp:Content ContentPlaceHolderID="MainBodyRegion" runat="server">
    <div id="MeetingsSearchMainBodyArea">
        <Scanweb:SearchMeetingList ID="SearchMeetingList" runat="server"></Scanweb:SearchMeetingList>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
    <div id="MeetingsSearchSecondaryBodyArea">
        <%--        <Scanweb:RightColumnWide ID="RightColumnWide1" runat="server" />--%>
        <div class="<%= GetCSS() %>">
            <asp:PlaceHolder ID="MeetingsSearchGoogleMap" runat="server">
                <Scanweb:SearchMeetingGoogleMap ID="SearchMeetingGoogleMap" runat="server"></Scanweb:SearchMeetingGoogleMap>
            </asp:PlaceHolder>
            <div id="MeetingSearch349">
           <div class="cnt">
                <div class="cntWrapper">                        
            <h3 class="stoolHeading" runat="server" id="searchHeader"></h3>       
            </div>
            </div>
            <Scanweb:SearchMeeting ID="MeetingsSearch" runat="server"></Scanweb:SearchMeeting>
            </div>
        </div>
    </div>
</asp:Content>
