﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages
{
    public partial class Language : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var siteLanguage = Request.QueryString[AppConstants.LANGUAGE];
            if (!string.IsNullOrEmpty(siteLanguage))
            {
                //Imp: This line is adding a P3P attribute in response header to allow IE to write cross domain cookie in iFrame
                HttpContext.Current.Response.AddHeader("p3p", "CP=\"NOI COR NID IND\"");
                //End
                HttpCookie httpCookie = new HttpCookie(AppConstants.USER_LOCALE_COOKIE, siteLanguage);
                httpCookie.Expires = DateTime.MaxValue;
                Response.Cookies.Add(httpCookie);
            }
        }
    }
}
