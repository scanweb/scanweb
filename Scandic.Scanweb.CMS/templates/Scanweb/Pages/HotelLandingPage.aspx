<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.HotelLandingPage"
    Codebehind="HotelLandingPage.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageHotels.master" %>
<%@ Register TagPrefix="Scanweb" TagName="InfoBox"              Src="~/Templates/Scanweb/Units/Static/InfoBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="HotelMenuNavigation" Src="~/Templates/Scanweb/Units/Static/HotelLandingPage/HotelMenuNavigation.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="PageHeader" Src="~/Templates/Scanweb/Units/Placeable/TopPageHeader.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="HotelStoryBox" Src="~/Templates/Scanweb/Units/Static/HotelLandingPage/HotelStoryBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="PromoBoxOffer" Src="~/Templates/Scanweb/Units/Placeable/PromoBoxOffer.ascx" %>
<%@ Register TagPrefix="Booking" TagName="Module" Src="~/Templates/Booking/Units/BookingModuleSmall.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="BookingModuleAlternativeBox" Src="~/Templates/Scanweb/Units/Placeable/BookingModuleAlternativeBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="PopulateBookingModuleInput" Src="~/Templates/Scanweb/Units/Placeable/PopulateBookingModuleInput.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="TripAdvisorRatingsandAwards" Src="~/Templates/Scanweb/Units/Static/HotelLandingPage/TripAdvisorRatingsandAwards.ascx" %>

<asp:Content ID="MainBodyRegion" ContentPlaceHolderID="MainBodyRegion" runat="server">    
    <asp:PlaceHolder ID="HotelContentPH" runat="server" />
    <asp:PlaceHolder ID="HotelContentPHLeft" runat="server">
        <div id="MainBodyLeftArea">
            <asp:PlaceHolder ID="HotelContentInnerPHLeft" runat="server" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="HotelContentPHRight" runat="server">
        <div id="MainBodyRightArea" runat="server">
        <asp:PlaceHolder ID="HotelContentInnerPHRight" runat="server" />
        </div>
    </asp:PlaceHolder>
</asp:Content>

<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
<div id="landingPage" class="<%= GetCSS() %>">
   
   <asp:PlaceHolder ID="phTARatings" runat="server" Visible = "false">
            <!-- Iframe 1 start -->
            <div class="BoxMedium" style="margin-left:7px;*margin-left:17px;">
                <div class="regular">
                    <div class="stoolHeader">
                        <div class="hd sprite">
                        </div>
                        <div class="cnt">
                            <div class="cntWrapper">
                                <h3 class="stoolHeading">
                                <EPiServer:Translate ID="TrRatings" Text="/Templates/Scanweb/Pages/HotelLandingPage/RatingsandReviews/Ratings" runat="server" />
                                   
                                </h3>
                            </div>
                        </div>
                        <div class="ft sprite">
                            &nbsp;</div>
                    </div>
                    <div class="cnt" style="padding:10px 2px 2px;">
                        <div class="innerContent" style="padding: 0px; width: 222px;">
                            <div>
                                <iframe id="ratings" class="tripiframe1" runat="server" frameborder="0"
                                    scrolling="no"></iframe>
									<a style="padding-left:10px;margin-left:10px;" class="IconLink" href="<%=GetHotelPageURL() %>"><EPiServer:Translate ID="TrReadReviews" Text="/Templates/Scanweb/Pages/HotelLandingPage/RatingsandReviews/ReadReviews" runat="server" /></a>
                            </div>
                        </div>
                    </div>
                    <div class="ft sprite">
                    </div>
                </div>
            </div>
            <!-- Iframe 1 end -->
        </asp:PlaceHolder>
    
   <div class="clear"></div>
   <asp:PlaceHolder ID="RightInfoBox1PlaceHolder" runat="server">
        <div class="BoxContainer">
            <Scanweb:InfoBox runat="server" HeaderPropertyName="RightInfoBoxHeading1"
                BodyPropertyName="RightInfoBoxBody1" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="RightInfoBox2PlaceHolder" runat="server">
        <div class="BoxContainer">
            <Scanweb:InfoBox runat="server" HeaderPropertyName="RightInfoBoxHeading2"
                BodyPropertyName="RightInfoBoxBody2" />
        </div>
    </asp:PlaceHolder>  
   <asp:PlaceHolder ID="RightColumnBookingPlaceHolder" runat="server">
    <div class="BoxContainer"> 
        <Booking:Module ID="BE" runat="server" />
        <Scanweb:BookingModuleAlternativeBox CssClass="AlternativeBookingBoxSmall" ID="AlternativeBookingModule" runat="server" Visible="false" />
        </div>
    </asp:PlaceHolder>
    <div class="clear"></div>
    <asp:PlaceHolder ID="MeetingPlaceHolder" runat="server" Visible="false">
        <Scanweb:PromoBoxOffer ID="RFPBox" CssClass="BoxMedium" ImageMaxWidth="226" ImagePropertyName="BoxImageMedium" runat="server" />
    </asp:PlaceHolder>
     <div class="BoxContainer">
    <Scanweb:PromoBoxOffer ID="Box1" offerPageLinkPropertyName="BoxContainer1" ImageMaxWidth="226" CssClass="BoxMedium"
    ImagePropertyName="BoxImageMedium" runat="server" />
    </div>
    <div class="BoxContainer">
        <Scanweb:PromoBoxOffer ID="Box2" offerPageLinkPropertyName="BoxContainer2" CssClass="BoxMedium"
            ImageMaxWidth="226" ImagePropertyName="BoxImageMedium" runat="server" />
    </div>
    <div class="BoxContainer">
        <Scanweb:PromoBoxOffer ID="Box3" offerPageLinkPropertyName="BoxContainer3" CssClass="BoxMedium"
            ImageMaxWidth="226" ImagePropertyName="BoxImageMedium" runat="server" />
    </div>
    <div class="BoxContainer">
        <Scanweb:PromoBoxOffer ID="Box4" offerPageLinkPropertyName="BoxContainer4" CssClass="BoxMedium"
            ImageMaxWidth="226" ImagePropertyName="BoxImageMedium" runat="server" />
    </div>
    <div class="BoxContainer">
        <div class="smallPromoBoxLight storyBox" id="storyBoxDiv" runat="server">
        <asp:PlaceHolder ID="StoryBoxPlaceHolder" runat="server">
            
               <%-- <Scanweb:HotelStoryBox ID="StoryBox" runat="server" CssClass="BoxMedium" />--%>
            
        </asp:PlaceHolder>
        </div>
    </div>
    
   <div class="clear"></div>   
   <asp:PlaceHolder ID="phTripAdvisorAwards" runat ="server">       
   </asp:PlaceHolder>
</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="ScriptRegion" runat="server">
    <Scanweb:PopulateBookingModuleInput runat="server" ID="PopulateBookingModule" />
<script type="text/javascript" language="javascript">
    $('#landingPage .BoxContainer').each(function() {
        if ($.trim($(this).text()) == "")
        {
            $(this).hide();
        }
  });
</script>
</asp:Content>