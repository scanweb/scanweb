////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  Page                                                   //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.SpecializedProperties;
using Scandic.Scanweb.CMS.Templates.Units.Static;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Code behind of page.
    /// </summary>
    [AccessibleWhenSessionExpired(false, "FGPPage")]
    public partial class Page : ScandicTemplatePage
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected string activateimagecarousel;
        protected void Page_Load(object sender, EventArgs e)
        {
            DynamicProperty myDynamicProperty = DynamicProperty.Load(CurrentPage.PageLink, "GuestProgramVisibleWhenLoggedIn");

            bool isPageMarkedForLogin = myDynamicProperty != null && myDynamicProperty.Status == DynamicPropertyStatus.Defined;

            if (!WebUtil.IsSessionValid(CurrentPage, true) && !UserLoggedInSessionWrapper.UserLoggedIn && isPageMarkedForLogin)
            {
                AppLogger.LogInfoMessage("User has to login to access the page marked as GuestProgramVisibleWhenLoggedIn");
                Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.LOGIN_FOR_DEEPLINK), false);
            }
            pageList.PageChanged +=
                new EventHandler<Scandic.Scanweb.CMS.Templates.Units.Placeable.PageList.PageChangedEventArg>(
                    pageList_PageChanged);

            pageList.ShowItemLink = (CurrentPage["ShowItemLink"] != null);
            pageList.ShowDate = (CurrentPage["ShowDate"] != null);
            pageList.PreviewTextLength = CurrentPage["PreviewTextLength"] != null
                                             ? (int) CurrentPage["PreviewTextLength"]
                                             : 100;
            NewsArchiveCtrl.Visible = CurrentPage["NewsArchiveContainer"] as PageReference == null ? false : true;
            pagePuffsCtrl.Visible = (CurrentPage["ShowPuffListing"] != null);

            if (CurrentPage["OverviewHotelContainer"] as PageReference != null)
            {
                DestinationGoogleMapPlaceHolder.Visible = (CurrentPage["OverviewHotelContainer"] as PageReference !=
                                                           null);

                PopulateBookingModule.DestinationName = CurrentPage["BookingModuleDestinationName"] as string ??
                                                        string.Empty;
            }

            if (CurrentPage["GuestProgramFunction"] != null)
                AddGuestProgramControls();

            ShowBodyScriptsAreas();

            ShowTopArea();

             activateimagecarousel = CurrentPage["ActivateImageCarousel"] != null ? CurrentPage["ActivateImageCarousel"].ToString() : "false";

            string url = this.Page.Request.Url.Host;    
            
        }

        
        public int CurrentPageLinkId
        {
            get { return CurrentPage.PageLink.ID; }
        
        }


        public string CurrentPageTypeId
        {
            get
            {
                return CurrentPage.PageTypeID.ToString();
                
            }
        
        }

        public string ActivateImageCarousel
        {

            get { return activateimagecarousel;}
        }


        /// <summary>
        /// PageList page changed event handler
        /// </summary>
        /// <param name="sender">
        /// The PageList control
        /// </param>
        /// <param name="e">
        /// PageChangedEventArg
        /// </param>
        protected void pageList_PageChanged(object sender,
                                            Scandic.Scanweb.CMS.Templates.Units.Placeable.PageList.PageChangedEventArg
                                                pageEventArgs)
        {
            if (pageEventArgs.CurrentPageNumber != 0)
            {
                PageMainBodyPlaceHolder.Visible = false;
            }
            else
            {
                PageMainBodyPlaceHolder.Visible = true;
            }
        }

        /// <summary>
        /// AddGuestProgramControls
        /// </summary>
        private void AddGuestProgramControls()
        {
            Control controlGuestProgram = new Control();
            string guestProgramFunction = CurrentPage["GuestProgramFunction"] as string ?? string.Empty;

            switch (guestProgramFunction)
            {
                case ("EnrollGuestProgram.ascx"):
                    controlGuestProgram = LoadControl("~\\Templates\\Booking\\Units\\" + guestProgramFunction);
                    if (controlGuestProgram != null)
                    {
                        GuestProgramPlaceHolder.Controls.Add(controlGuestProgram);
                        GuestProgramPlaceHolder.Visible = true;
                    }
                    else
                        break;
                    break;
                case ("LostCardNoLogin.ascx"):
                    controlGuestProgram = LoadControl("~\\Templates\\Booking\\Units\\" + guestProgramFunction);
                    if (controlGuestProgram != null)
                    {
                        GuestProgramPlaceHolder.Controls.Add(controlGuestProgram);
                        GuestProgramPlaceHolder.Visible = true;
                    }
                    else
                        break;
                    break;
                case ("LostCardLogin.ascx"):
                    controlGuestProgram = LoadControl("~\\Templates\\Booking\\Units\\" + guestProgramFunction);
                    if (controlGuestProgram != null)
                    {
                        GuestProgramPlaceHolder.Controls.Add(controlGuestProgram);
                        GuestProgramPlaceHolder.Visible = true;
                    }
                    else
                        break;
                    break;
                case ("CurrentBookings.ascx"):
                    controlGuestProgram = LoadControl("~\\Templates\\Booking\\Units\\" + guestProgramFunction);
                    if (controlGuestProgram != null)
                    {
                        GuestProgramPlaceHolder.Controls.Add(controlGuestProgram);
                        GuestProgramPlaceHolder.Visible = true;
                    }
                    else
                        break;
                    break;
                case ("ContactUs.ascx"):
                    controlGuestProgram = LoadControl("~\\Templates\\Booking\\Units\\" + guestProgramFunction);
                    if (controlGuestProgram != null)
                    {
                        GuestProgramPlaceHolder.Controls.Add(controlGuestProgram);
                        GuestProgramPlaceHolder.Visible = true;
                    }
                    else
                        break;
                    break;
                case ("UpdateProfile.ascx"):
                    controlGuestProgram = LoadControl("~\\Templates\\Booking\\Units\\" + guestProgramFunction);
                    if (controlGuestProgram != null)
                    {
                        GuestProgramPlaceHolder.Controls.Add(controlGuestProgram);
                        GuestProgramPlaceHolder.Visible = true;
                    }
                    else
                        break;
                    break;
                case ("MissingPoints.ascx"):
                    controlGuestProgram = LoadControl("~\\Templates\\Booking\\Units\\" + guestProgramFunction);
                    if (controlGuestProgram != null)
                    {
                        GuestProgramPlaceHolder.Controls.Add(controlGuestProgram);
                        GuestProgramPlaceHolder.Visible = true;
                    }
                    else
                        break;
                    break;
                case ("FGPPointsHotelGrid.ascx"):
                    controlGuestProgram = LoadControl("~\\Templates\\Scanweb\\Units\\Static\\" + guestProgramFunction);
                    if (controlGuestProgram != null)
                    {
                        //PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                        //                                                 EPiServer.Security.AccessLevel.NoAccess);
                        //PageReference countryContainer = (PageReference) rootPage["CountryContainer"];

                        //((GuestProgramRedemptionList) controlGuestProgram).CountryContainerReference = countryContainer;
                        GuestProgramPlaceHolder.Controls.Add(controlGuestProgram);
                        GuestProgramPlaceHolder.Visible = true;
                    }
                    else
                        break;
                    break;
                case ("LoginError.ascx"):
                    controlGuestProgram = LoadControl("~\\Templates\\Booking\\Units\\" + guestProgramFunction);
                    if (controlGuestProgram != null)
                    {
                        GuestProgramPlaceHolder.Controls.Add(controlGuestProgram);
                        GuestProgramPlaceHolder.Visible = true;
                    }
                    else
                        break;
                    break;
                case ("InviteAFriend.ascx"):
                    controlGuestProgram = LoadControl("~\\Templates\\Booking\\Units\\" + guestProgramFunction);
                    if (controlGuestProgram != null)
                    {
                        GuestProgramPlaceHolder.Controls.Add(controlGuestProgram);
                        GuestProgramPlaceHolder.Visible = true;
                    }
                    else
                        break;
                    break;
                case ("ForgottenPassword.ascx"):
                    controlGuestProgram = LoadControl("~\\Templates\\Booking\\Units\\" + guestProgramFunction);
                    if (controlGuestProgram != null)
                    {
                        GuestProgramPlaceHolder.Controls.Add(controlGuestProgram);
                        GuestProgramPlaceHolder.Visible = true;
                    }
                    else
                        break;
                    break;
                case ("LogoutConfirmation.ascx"):
                    controlGuestProgram = LoadControl("~\\Templates\\Booking\\Units\\" + guestProgramFunction);
                    if (controlGuestProgram != null)
                    {
                        GuestProgramPlaceHolder.Controls.Add(controlGuestProgram);
                        GuestProgramPlaceHolder.Visible = true;
                    }
                    else
                        break;
                    break;
                case ("CampaignLanding.ascx"):
                    controlGuestProgram = LoadControl("~\\Templates\\Booking\\Units\\" + guestProgramFunction);
                    if (controlGuestProgram != null)
                    {
                        GuestProgramPlaceHolder.Controls.Add(controlGuestProgram);
                        GuestProgramPlaceHolder.Visible = true;
                    }
                    else
                        break;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// GetCSS
        /// </summary>
        /// <returns>CSS</returns>
        protected string GetCSS()
        {
            if (CurrentPage["ShowBookingModule"] != null)
                return "moduleAbsolutePositioning";

            return string.Empty;
        }

        /// <summary>
        /// Show different script areas depending on if URL starts with https or not
        /// </summary>
        private void ShowBodyScriptsAreas()
        {
            if (Request.Url.AbsoluteUri.ToLower().StartsWith("https"))
            {
                BodyScriptLiteral.Text = CurrentPage["BodyScriptAreaHttps"] as string ?? string.Empty;
            }
            else
            {
                BodyScriptLiteral.Text = CurrentPage["BodyScriptArea"] as string ?? string.Empty;
            }
        }

        /// <summary>
        /// Show different Top Area placeholders depending on value of the TopBodyVisibility property
        /// </summary>
        private void ShowTopArea()
        {
            if (CurrentPage["ActivateImageCarousel"] != null && bool.Parse(CurrentPage["ActivateImageCarousel"].ToString()))
            {
                var slidesDiv = new Panel(){ ID = "slides"};

                if ((int)CurrentPage["Width"] == (int)TopAreaWidth.Width.Center)
                {
                    TopAreaCenterPlaceHolder.Visible = true;
                    TopAreaCenterPlaceHolder.Controls.Clear();
                    TopAreaCenterPlaceHolder.Controls.Add(slidesDiv);
                }

                if ((int)CurrentPage["Width"] == (int)TopAreaWidth.Width.CenterRight)
                {
                    TopAreaCenterRightPlaceHolder.Visible = true;
                    TopAreaCenterRightPlaceHolder.Controls.Clear();
                    TopAreaCenterRightPlaceHolder.Controls.Add(slidesDiv);
                }

                if ((int)CurrentPage["Width"] == (int)TopAreaWidth.Width.Full)
                {
                    TopAreaFullWidthPlaceHolder.Visible = true;
                    TopAreaFullWidthPlaceHolder.Controls.Clear();
                    TopAreaFullWidthPlaceHolder.Controls.Add(slidesDiv);
                }
            }
            else if (CurrentPage["TopAreaVisibility"] != null)
            {
                if ((int) CurrentPage["TopAreaVisibility"] == (int) TopAreaWidth.Width.Center)
                    TopAreaCenterPlaceHolder.Visible = true;

                if ((int) CurrentPage["TopAreaVisibility"] == (int) TopAreaWidth.Width.CenterRight)
                    TopAreaCenterRightPlaceHolder.Visible = true;


                if ((int) CurrentPage["TopAreaVisibility"] == (int) TopAreaWidth.Width.Full)
                    TopAreaFullWidthPlaceHolder.Visible = true;
            }
        }
    }
}