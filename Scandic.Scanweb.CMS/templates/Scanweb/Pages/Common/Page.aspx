<%@ Page language="c#" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWide.master" Inherits="Scandic.Scanweb.CMS.Templates.Page" Codebehind="Page.aspx.cs" %>
<%@ Register TagPrefix="Scanweb" TagName="MainBody" 	        Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="PageList"	            Src="~/Templates/Scanweb/Units/Placeable/PageList.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="Archive"              Src="~/Templates/Scanweb/Units/Placeable/Archive.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="PagePuff"             Src="~/Templates/Scanweb/Units/Placeable/PagePuff.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="InfoBox"              Src="~/Templates/Scanweb/Units/Static/InfoBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="OverviewHotelGoogleMap"              Src="~/Templates/Scanweb/Units/Static/OverviewHotelGoogleMap.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="RightColumn"              Src="~/Templates/Scanweb/Units/Placeable/RightColumn.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="LeftColumn"              Src="~/Templates/Scanweb/Units/Placeable/LeftColumn.ascx" %>
<%@ Register TagPrefix="Booking" TagName="Module" 	            Src="~/Templates/Booking/Units/BookingModuleSmall.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="PopulateBookingModuleInput" Src="~/Templates/Scanweb/Units/Placeable/PopulateBookingModuleInput.ascx" %>


<asp:Content ContentPlaceHolderID="TopAreaCenterRightRegion"  runat="server">
  
    <asp:PlaceHolder ID="TopAreaCenterRightPlaceHolder"  Visible="false" runat="server">
        <div id="TopAreaCenterRightArea">
            <EPiServer:Property PropertyName="TopArea" runat="server" />
        </div>
    </asp:PlaceHolder>
</asp:Content>

<asp:Content ContentPlaceHolderID="TopAreaFullWidthRegion" runat="server">
    <asp:PlaceHolder ID="TopAreaFullWidthPlaceHolder" Visible="false" runat="server">
        <div id="TopAreaFullWidthArea">
            <EPiServer:Property PropertyName="TopArea" runat="server" />
        </div>
    </asp:PlaceHolder>
</asp:Content>

<asp:Content ContentPlaceHolderID="LeftColumnRegion" runat="server">
    <Scanweb:LeftColumn runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="MainBodyLeftRegion" runat="server">
    <asp:PlaceHolder ID="TopAreaCenterPlaceHolder" Visible="false" runat="server">
        <div id="TopAreaCenterArea">
            <EPiServer:Property PropertyName="TopArea" runat="server" />
        </div>
    </asp:PlaceHolder>     
    <div id="ImageDiv">
    <Scanweb:SquaredCornerImage ImagePropertyName="ContentTopImage" 
                                 TopCssClass="RoundedCornersTop472" 
                                 ImageCssClass="RoundedCornersImage472" 
                                 BottomCssClass="RoundedCornersBottom472" ImageWidth="472" runat="server" />
    </div>
    <div id="slides"></div>
    <asp:PlaceHolder ID="PageMainBodyPlaceHolder" runat="server" Visible="true">                          

<!--Commented section of Image Carousel implementation; This JS Code section initiates the code flow processing Image Carousel on UI:Standard Pages;
     This implementation is put on hold because of JQuery mismatch
     Commented by Vaibhav -->
     
     
       
        <script type="text/javascript">
            $(document).ready(function() {
                var activateimagecarousel = '<%=ActivateImageCarousel%>';
                if (activateimagecarousel == 'True') {
                    $('#ImageDiv').hide();
                    CarouselProcessing('<%=CurrentPageTypeId%>', '<%=CurrentPageLinkId%>');
                }
            });

  
       </script>
                
            
    
        <Scanweb:MainBody runat="server" />
        
    </asp:PlaceHolder> 
    
    <asp:PlaceHolder ID="GuestProgramPlaceHolder" runat="server" Visible="false">
    </asp:PlaceHolder>
    
    <Scanweb:PagePuff runat="server" FirstPageReference="FirstPuffPage" SecondPageReference="SecondPuffPage" ThirdPageReference="ThirdPuffPage" FourthPageReference="FourthPuffPage" ID="pagePuffsCtrl" />
    <Scanweb:PageList ID="pageList" PageLinkProperty="ListingContainer" ItemsPerPageProperty="ItemsPerPage" ThumbnailProperty="ThunbnailImage"  MaxCountProperty="MaxCount" runat="server" />    
    <Scanweb:Archive runat="server" ID="NewsArchiveCtrl" />
    
    <asp:PlaceHolder ID="DestinationGoogleMapPlaceHolder" runat="server" Visible="false">
        <Scanweb:OverviewHotelGoogleMap runat="server" />
    </asp:PlaceHolder>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainBodyRightRegion" runat="server">
    <Scanweb:RightColumn runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ScriptRegion" runat="server">
    <Scanweb:PopulateBookingModuleInput ID="PopulateBookingModule" runat="server" />
    
    <!-- Body script region -->
    <asp:Literal ID="BodyScriptLiteral" runat="server"></asp:Literal>
    
   <%-- <div id="Div1" runat="server" style="visibility:hidden">
        <asp:HiddenField  Id="hdnPageId" runat="server" />
    </div>
   --%>
   
    <!-- End body script region -->
</asp:Content>
