<%@ Page language="c#" Inherits="Scandic.Scanweb.CMS.Templates.CurrencyConverter" Codebehind="CurrencyConverter.aspx.cs"  %>

<%@ Register TagPrefix="Booking" TagName="CurrencyConverter" Src="~/templates/Scanweb/Units/Static/CurrencyConverter.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link rel="stylesheet" type="text/css" media="screen" href="/Templates/Scanweb/Styles/Default/Styles.css">
<link rel="stylesheet" type="text/css" media="screen" href="/Templates/Scanweb/Styles/Default/Popup.css?v=<%=CmsUtil.GetCSSVersion()%>" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head1" runat="server">
   <title><%= CurrentPage.PageName %></title>    
</head>
<body>
    <form id="Form1" runat="server">
<Booking:CurrencyConverter runat="server" />
 </form>
</body>
</html>
