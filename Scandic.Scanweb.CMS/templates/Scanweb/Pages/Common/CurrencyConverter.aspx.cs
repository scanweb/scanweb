//  Description					: CurrencyConverter                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Code behind of CurrencyConverter control.
    /// </summary>
    public partial class CurrencyConverter : ScandicTemplatePage
    {
    }
}