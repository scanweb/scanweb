/*
Copyright � 1997-2007 EPiServer AB. All Rights Reserved.

This code may only be used according to the EPiServer License Agreement.
The use of this code outside the EPiServer environment, in whole or in
parts, is forbidded without prior written permission from EPiServer AB.

EPiServer is a registered trademark of EPiServer AB. For
more information see http://www.episerver.com/license or request a
copy of the EPiServer License Agreement by sending an email to info@ep.se
*/

using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Displays a main body and an XForm.
    /// </summary>
    public partial class XFormPage : ScandicTemplatePage
    {
    }
}