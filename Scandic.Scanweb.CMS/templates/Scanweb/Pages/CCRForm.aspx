<%@ Page Language="C#" AutoEventWireup="True" Codebehind="CCRForm.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Pages.CCRForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<script type="text/javascript" language="JavaScript" src="<%# ResolveUrl("~/Templates/Booking/JavaScript/plugin/jqModal.js") %>" ></script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Content Change Request Form</title>
    <style id="Style1" type="text/css" runat="server">
    
    body{
        text-align:center;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
    
    #Container{
        margin:0 auto;
        text-align:left;
        width:1000px;
    }
      
    .FormHeader
    {
        font-size: 22pt; 
        margin-bottom:10px;
    }   
  
   .ReqCss
   {
    font-size:10px;
    line-height:12px;
    width:auto;
   }
   .CCRFTable
   {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    background-color:#EEEEEE;
    vertical-align:top;
    border: solid 1px #999999;
   }
   
   td{
    vertical-align:top;
   }
   
   .ContentChangeRequestFeedbackTable
    {
    background-color:#EEEEEE;
    font-size:16pt;
    text-align:left;
    width:100%;
    border: solid 1px #999999;
   }
   
   .ContentChangeRequestFeedbackTable td{
        padding: 10px;
        
   }
   
    h3{  
    font-size:12px;
    font-weight:bold;
    margin-bottom:5px;
    margin-top:0px;
 
   }
   .HelpText{
     font-size:11px;
     color:#666666;
   }
   
   h2, h2.title{

    font-size:15px;
    font-weight:bold;
    color:#333333;
    margin-bottom:5px;
    text-align: left;
   }
   h2.title{
   	float: left;
    padding: 0 0 0 6px;
   }
   
   .HelpTextVisible{
        visibility:visible;
        width:300px;
       
   }
   
   .HelpTextNotVisible{
        visibility:hidden; 
        width:300px;
   }
   
   a.HelpTextSwitch{
        font-weight:bold;
        text-decoration:none;
        color:black;    
        font-size:14px;
   }
   td.popupfields 
   {
   	text-align:left;
   	font-size:12px;
   	font-weight:normal;
   	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
   	word-wrap:break-word;
   	}


   </style>
   
    <style type="text/css">
    /* jqModal base Styling courtesy of;
    Brice Burgess <bhb@iceburg.net> */

    /* The Window's CSS z-index value is respected (takes priority). If none is supplied,
    the Window's z-index value will be set to 3000 by default (in jqModal.js). You
    can change this value by either;
    a) supplying one via CSS
    b) passing the "zIndex" parameter. E.g.  (window).jqm({zIndex: 500}); */

    .jqmWindow {
    display: none;
    position: fixed;
    top: 0%;
    left: 50%;
    margin-left: -300px;
    width: 580px;
    background-color: #EEE;
    color: #333;
    border: 4px solid #444444;
    padding: 12px;
    }
    .jqmClose{
    	margin: 11px 0 0;
    	*margin: 5px 0 0;
        float: right;    	
    	font-size:12px;
    	font-weight:bold;
    }
    .jqmOverlay { background-color: #000; }
    
    #Previewdv{
    	clear: both;
    	overflow-x: hidden;
    	overflow-y: auto;
    	margin: 11px 0 0;
    }

    /* Fixed posistioning emulation for IE6
     Star selector used to hide definition from browsers other than IE6
     For valid CSS, use a conditional include instead */
    * html .jqmWindow {
     position: absolute;
     top: expression((document.documentElement.scrollTop || document.body.scrollTop) + Math.round(17 * (document.documentElement.offsetHeight 

    || document.body.clientHeight) / 100) + 'px');
    }
    </style>

    <script type="text/javascript">
        function SwitchClassName(elementID, firstClassName, alternativeClassName) {
            var element = document.getElementById(elementID);
            if (element) {
                if (element.className == firstClassName) {
                    element.className = alternativeClassName;
                }
                else {
                    element.className = firstClassName;
                }
            }
        }
        //Boxi: Added for close popup if Esc Key pressed
        document.onkeydown = function(e) {
            if (e == null) { // ie
                keycode = event.keyCode;
            } else { // mozilla
                keycode = e.which;
            }
            if (keycode == 27) { // escape, close box
                $(".jqmWindow").jqmHide();
            }
        };
        //Boxi: Added for enabling/disabling validator
        function InitValidator() {

            //retrieve instance and value  of checkbox and TextBoxes
            var checkbox = document.getElementById("SpecialAlert_Change");
            var imageid_textbox = document.getElementById("ImageID_Change").value;
            var Desc_textbox = document.getElementById("CopyText_Change").value;
            // enable/disable  validator on page based on checkbox state,textbox field

            if (checkbox.checked || imageid_textbox != "" || Desc_textbox != "") {
                ValidatorEnable(document.getElementById("ReqWebSiteSection_Change"), true);
            }
            else {
                ValidatorEnable(document.getElementById("ReqWebSiteSection_Change"), false);
            }

        }
        //Boxi: Added for showing Preview PopUp
        function GetPreviewModalPopup() {
            var language = "";
            var special_alert = "";
            var special_alert_change = "";
            var content_change = "";
            var content_new = "";
            if ($("#SpecialAlert").attr("checked")) {
                special_alert = $("#lblSpecialAlert").text();
            }
            if ($("#SpecialAlert_Change").attr("checked")) {
                special_alert_change = $("#lblSpecialAlert_Change").text();
            }
            if ($("#WebSiteCOM").attr("checked")) {
                language = language + $("#lblCom").text() + ",";
            }
            if ($("#WebSiteSE").attr("checked")) {
                language = language + $("#lblSe").text() + ",";
            }
            if ($("#WebSiteNO").attr("checked")) {
                language = language + $("#lblNo").text() + ",";
            }
            if ($("#WebSiteDK").attr("checked")) {
                language = language + $("#lblDk").text() + ",";
            }
            if ($("#WebSiteFI").attr("checked")) {
                language = language + $("#lblFi").text() + ",";
            }
            if ($("#WebSiteDE").attr("checked")) {
                language = language + $("#lblDe").text() + ",";
            }
            if ($("#WebSiteRU").attr("checked")) {
                language = language + $("#lblRu").text() + ",";
            }
            if (language != "") {
                language = language.slice(0, language.length - 1);
            }
            if (special_alert_change != "" || $("#ImageID_Change").val() != "" || $("#CopyText_Change").val() != "" || $("#WebSiteSection_Change").val() != "") {
                content_change =
            "<table cellpadding='0' width='550px' align='center'>" +
            "<tr><td class='popupfields'><h3 style='margin-bottom:0px;'>Content change</h3></td></tr>" +
            "</table>" +
            "</div>" +
            "<div style='border:1px solid #bbbbbb; align:center; margin:3px;'>" +
            "<table cellpadding='2' width='550px' align='center'>" +
            "<tr><td class='popupfields' width='50%'>Special alert  </td><td class='popupfields' width='50%'>" + special_alert_change + "</td></tr>" +
            "<tr><td class='popupfields'>Web site section (URL) </td><td class='popupfields'>" + $("#WebSiteSection_Change").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Image ID  </td><td class='popupfields'>" + $("#ImageID_Change").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Change text/New text  </td><td class='popupfields'>" + $("#CopyText_Change").val() + "</td></tr>" +
            "</table>" +
            "</div>";
            }
            else {
                content_change = "";
            }
            //artf1261761: Special Alert pick-box at Content Change Request
            if (special_alert != "" || $("#ImageID").val() != "" || $("#CopyText").val() != "" || $("#WebSiteSection").val() != "") {
                content_new =
            "<table cellpadding='0' width='550px' align='center'>" +
            "<tr><td class='popupfields'><h3 style='margin-bottom:0px;'>New content</h3></td></tr>" +
            "</table>" +
            "</div>" +
            "<div style='border:1px solid #bbbbbb; align:center; margin:3px;'>" +
            "<table cellpadding='2' width='550px' align='center'>" +
            "<tr><td class='popupfields' width='50%'>Special alert  </td><td class='popupfields' width='50%'>" + special_alert + "</td></tr>" +
            "<tr><td class='popupfields'>Web site section (URL) </td><td class='popupfields'>" + $("#WebSiteSection").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Image ID  </td><td class='popupfields'>" + $("#ImageID").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Change text/New text  </td><td class='popupfields'>" + $("#CopyText").val() + "</td></tr>" +
            "</table>" +
            "</div>";
            }
            else {
                content_new = "";
            }

            var divContent = "<table border='0'>" +
            "<tr><td>" +
            "<div style='border:1px solid #bbbbbb; align:center; margin:3px;'>" +
            "<table cellpadding='2' width='550px' align='center'>" +
            "<tr><td width='50%' class='popupfields'>Header</td><td class='popupfields' width='50%'>" + $("#Header").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Your name</td><td class='popupfields'>" + $("#AuthorName").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Your telephone</td><td class='popupfields'>" + $("#AuthorEmail").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Your location (Hotel/SO)</td><td class='popupfields'>" + $("#AuthorTelephone").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Applies to web site </td><td class='popupfields'>" + $("#AuthorLocation").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Change request language</td><td class='popupfields'>" + language + "</td></tr>" +
            "<tr><td class='popupfields'>Description of the change request </td><td class='popupfields'>" + $("#DescriptionCCR").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Summary in English </td><td class='popupfields'>" + $("#EnglishSummary").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Requested change date  </td><td class='popupfields'>" + $("#ChangeRequestedDate").val() + "</td></tr>" +
            "</table>" +
            "</div>" +
            content_new +
            content_change +
            "<table cellpadding='0' width='550px' align='center'>" +
            "<tr><td class='popupfields'><h3 style='margin-bottom:0px;'>Create an offer</h3></td></tr>" +
            "</table>" +
            "</div>" +
            "<div style='border:1px solid #bbbbbb; align:center;margin:3px;'>" +
            "<table cellpadding='2' width='550px' align='center'>" +
            "<tr><td class='popupfields' width='50%'>Offer text  </td><td class='popupfields' width='50%'>" + $("#UniqueSellingPoints").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Offer booking details </td><td class='popupfields'>" + $("#OfferDetails").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Offer start date  </td><td class='popupfields'>" + $("#OfferStartDate").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Offer end date  </td><td class='popupfields'>" + $("#OfferEndDate").val() + "</td></tr>" +
            "<tr><td class='popupfields'>Offer image ID  </td><td class='popupfields'>" + $("#OfferImageID").val() + "</td></tr>" +
            "<tr><td class='popupfields'>External link </td><td class='popupfields'>" + $("#ExternalLink").val() + "</td></tr>" +
            "</table>" +
            "</div>" +
            "</tr></td>" +
            "</table>";
            //fill the div with the entered information
            $("#Previewdv").html(divContent);
            //set the height of Modal window as the screen height
            $('#dvPreview').css({ 'height': ($(window).height()) - 100 });
            $('#dvPreview').css({ 'top': '15px' });
            //set the height inner div lasser than Modal window height
            $('#Previewdv').css({ 'height': ($("#dvPreview").height() - 30) });

        }

        $(document).ready(function() {
            $("#dvPreview").jqm();
        });
     
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="Container">
            <div>
                &nbsp;<asp:PlaceHolder ID="ChangeRequestFormTablePlaceholder" runat="server" Visible="true">
                    <asp:Table ID="ContentChangeRequestFormHeader" CssClass="FormHeader" runat="server"
                        CellPadding="10">
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server"><asp:Image ImageUrl="/Templates/Scanweb/Styles/Default/Images/scandic_logo.gif" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell runat="server" VerticalAlign="Top">
                                &nbsp;&nbsp;<EPiServer:Property ID="Property1" runat="server" PropertyName="PageName" />
                                <br />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <asp:Table ID="ContentChangeRequestFormTable" CssClass="CCRFTable" runat="server"
                        CellPadding="10" Width="1000">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell Width="300" runat="server">
                           <h3><EPiServer:Translate Text="/Templates/Scanweb/Pages/contentChangeRequest/HeadLine" runat="server" /></h3>
                            </asp:TableCell>
                            <asp:TableCell Width="300" runat="server">
                                <asp:TextBox ID="Header" Width="300" runat="server"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="ReqHeader" CssClass="ReqCss" runat="server" ControlToValidate="Header"></asp:RequiredFieldValidator>
                            </asp:TableCell>
                            <asp:TableCell Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('HeadLineHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                                <div id="HeadLineHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate10" Text="/Templates/Scanweb/Pages/contentChangeRequest/HeadLineHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                                &nbsp;<br />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate18" Text="/Templates/Scanweb/Pages/contentChangeRequest/AuthorName"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                                <asp:TextBox ID="AuthorName" Width="300" runat="server"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="ReqAuthorName" runat="server" CssClass="ReqCss" ControlToValidate="AuthorName"></asp:RequiredFieldValidator>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell23" Width="10" runat="server">
                          
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                               
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate19" Text="/Templates/Scanweb/Pages/contentChangeRequest/AuthorEmail"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                                <asp:TextBox ID="AuthorEmail" Width="300" runat="server"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="ReqEmail" runat="server" ControlToValidate="AuthorEmail"
                                    Display="Dynamic" CssClass="ReqCss"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularEmail" runat="server" ControlToValidate="AuthorEmail"
                                    CssClass="ReqCss" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Pages/contentChangeRequest/AuthorTelephone"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                                <asp:TextBox ID="AuthorTelephone" Width="300" runat="server"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="ReqTelephone" runat="server" ControlToValidate="AuthorTelephone"
                                    CssClass="ReqCss"></asp:RequiredFieldValidator>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell24" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('AuthorTelephoneHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                                <div id="AuthorTelephoneHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate13" Text="/Templates/Scanweb/Pages/contentChangeRequest/AuthorTelephoneHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Pages/contentChangeRequest/AuthorLocation"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                                <asp:TextBox ID="AuthorLocation" Width="300" runat="server"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="ReqHotel" runat="server" ControlToValidate="AuthorLocation"
                                    CssClass="ReqCss"></asp:RequiredFieldValidator></asp:TableCell>
                            <asp:TableCell ID="TableCell25" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('AuthorLocationHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                                <div id="AuthorLocationHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate17" Text="/Templates/Scanweb/Pages/contentChangeRequest/AuthorLocationHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow2" runat="server">
                            <asp:TableCell ID="TableCell3" runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate6" Text="/Templates/Scanweb/Pages/contentChangeRequest/WebSite"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell4" runat="server">
                                <asp:CheckBox ID="WebSiteCOM" runat="server" Checked="true" /><label id="lblCom" for="<%= WebSiteCOM.ClientID %>">COM</label><br />
                                <asp:CheckBox ID="WebSiteSE" runat="server"  Checked="true" /><label id="lblSe" for="<%= WebSiteSE.ClientID %>">SE</label><br />
                                <asp:CheckBox ID="WebSiteNO" runat="server"  Checked="true" /><label id="lblNo" for="<%= WebSiteNO.ClientID %>">NO</label><br />
                                <asp:CheckBox ID="WebSiteDK" runat="server"  Checked="true" /><label id="lblDk" for="<%= WebSiteDK.ClientID %>">DK</label><br />
                                <asp:CheckBox ID="WebSiteFI" runat="server"  Checked="true" /><label id="lblFi" for="<%= WebSiteFI.ClientID %>">FI</label><br />
                                <asp:CheckBox ID="WebSiteDE" runat="server"  Checked="true" /><label id="lblDe" for="<%= WebSiteDE.ClientID %>">DE</label><br />
                                <asp:CheckBox ID="WebSiteRU" runat="server"  Checked="true" /><label id="lblRu" for="<%= WebSiteRU.ClientID %>">RU</label><br />
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell26" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('WebSiteHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;;return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell11" runat="server">
                                <div id="WebSiteHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate20" Text="/Templates/Scanweb/Pages/contentChangeRequest/WebSiteHelpText"
                                            runat="server" />
                                        <br />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow3" runat="server">
                            <asp:TableCell ID="TableCell5" runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate5" Text="/Templates/Scanweb/Pages/contentChangeRequest/ChangeRequestLanguage"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell6" runat="server">
                                <asp:DropDownList ID="Language" runat="server">
                                </asp:DropDownList>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell27" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('ChangeRequestLanguageHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell12" runat="server">
                                <div id="ChangeRequestLanguageHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate21" Text="/Templates/Scanweb/Pages/contentChangeRequest/ChangeRequestLanguageHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow4" runat="server">
                            <asp:TableCell ID="TableCell7" runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate8" Text="/Templates/Scanweb/Pages/contentChangeRequest/Description"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell8" runat="server">
                                <asp:TextBox ID="DescriptionCCR" Rows="7" Width="300" TextMode="MultiLine" runat="server" /><br />
                                <asp:RequiredFieldValidator ID="ReqCCR" runat="server" CssClass="ReqCss" ControlToValidate="DescriptionCCR"></asp:RequiredFieldValidator>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell28" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('DescriptionHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell13" runat="server">
                                <div id="DescriptionHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate22" Text="/Templates/Scanweb/Pages/contentChangeRequest/DescriptionHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow5" runat="server">
                            <asp:TableCell ID="TableCell9" runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate24" Text="/Templates/Scanweb/Pages/contentChangeRequest/EnglishSummary"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell10" runat="server">
                                <asp:TextBox ID="EnglishSummary" runat="server" Rows="7" Width="300" TextMode="MultiLine"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="ReqSum" CssClass="ReqCss" runat="server" ControlToValidate="EnglishSummary"></asp:RequiredFieldValidator>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell29" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('EnglishSummaryHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell14" runat="server">
                                <div id="EnglishSummaryHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate23" Text="/Templates/Scanweb/Pages/contentChangeRequest/EnglishSummaryHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate3" Text="/Templates/Scanweb/Pages/contentChangeRequest/RequestChangeDate"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                                <asp:TextBox ID="ChangeRequestedDate" Width="300" runat="server"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="Reqdate" Display="Dynamic" runat="server" ControlToValidate="ChangeRequestedDate"
                                    CssClass="ReqCss"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="validDate" CssClass="ReqCss" ValidationExpression="^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$"
                                    ControlToValidate="ChangeRequestedDate" runat="server"></asp:RegularExpressionValidator>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell30" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('RequestChangeDateHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell1" runat="server">
                                <div id="RequestChangeDateHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate25" Text="/Templates/Scanweb/Pages/contentChangeRequest/RequestChangeDateHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <br />
                    <h2>
                        Fill in below for new content</h2>
                    <asp:Table ID="Table1_NewContent" CssClass="CCRFTable" runat="server" CellPadding="10" Width="1000">
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server" Width="300">
                                <h3>
                                    <EPiServer:Translate ID="Translate100" Text="/Templates/Scanweb/Pages/contentChangeRequest/SpecialAlert"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell Width="300" runat="server">
                                <asp:CheckBox ID="SpecialAlert" runat="server" Width="49px" /><label id="lblSpecialAlert" for="<%= SpecialAlert.ClientID %>">Yes</label><br />
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell31" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('SpecialAlertHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                                <div id="SpecialAlertHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate101" Text="/Templates/Scanweb/Pages/contentChangeRequest/SpecialAlertHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                                &nbsp;
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate102" Text="/Templates/Scanweb/Pages/contentChangeRequest/WebSiteSection"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                                <asp:TextBox ID="WebSiteSection" Width="300" runat="server"></asp:TextBox><br />
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell32" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('WebSiteSectionHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell15" runat="server">
                                <div id="WebSiteSectionHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate103" Text="/Templates/Scanweb/Pages/contentChangeRequest/WebSiteSectionHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                                &nbsp;
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow6" runat="server">
                            <asp:TableCell>
                                <h3>
                                    <EPiServer:Translate ID="Translate104" Text="/Templates/Scanweb/Pages/contentChangeRequest/ImageID"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="ImageID" Width="300" runat="server"></asp:TextBox><br />
                                <asp:RegularExpressionValidator ErrorMessage="test" ID="ImageIntegerValidator" CssClass="ReqCss" ControlToValidate="ImageID" ValidationExpression="^[0-9]*" runat="server">
                                </asp:RegularExpressionValidator>
                                
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell33" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('ImageIDHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell16" runat="server">
                                <div id="ImageIDHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate105" Text="/Templates/Scanweb/Pages/contentChangeRequest/ImageIDHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate106" Text="/Templates/Scanweb/Pages/contentChangeRequest/CopyText"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                                <asp:TextBox ID="CopyText" Rows="7" Width="300" TextMode="MultiLine" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell34" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('CopyTextHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell17" runat="server">
                                <div id="CopyTextHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate107" Text="/Templates/Scanweb/Pages/contentChangeRequest/CopyTextHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <br />
                    <h2>
                        Fill in below for content change </h2>
                    <asp:Table ID="Table3_ContentChange" CssClass="CCRFTable" runat="server" CellPadding="10" Width="1000">
                        <asp:TableRow ID="TableRow8" runat="server">
                            <asp:TableCell ID="TableCell41" runat="server" Width="300">
                                <h3>
                                    <EPiServer:Translate ID="Translate4" Text="/Templates/Scanweb/Pages/contentChangeRequest/SpecialAlert"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell42" Width="300" runat="server">
                                <asp:CheckBox ID="SpecialAlert_Change" runat="server" Width="49px" /><label id="lblSpecialAlert_Change" for="<%= SpecialAlert_Change.ClientID %>">Yes</label>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell43" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('SpecialAlertHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell44" runat="server">
                                <div id="Div1" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate26" Text="/Templates/Scanweb/Pages/contentChangeRequest/SpecialAlertHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                                &nbsp;
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow9" runat="server">
                            <asp:TableCell ID="TableCell45" runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate_ConententChange" Text="" runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell46" runat="server">
                                <asp:TextBox ID="WebSiteSection_Change" Width="300" runat="server"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="ReqWebSiteSection_Change" runat="server" CssClass="ReqCss" ControlToValidate="WebSiteSection_Change"></asp:RequiredFieldValidator>
                                
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell47" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('WebSiteSectionHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell48" runat="server">
                                <div id="Div2" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate27" Text="/Templates/Scanweb/Pages/contentChangeRequest/WebSiteSectionHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                                &nbsp;
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow10" runat="server">
                            <asp:TableCell>
                                <h3>
                                    <EPiServer:Translate ID="Translate16" Text="/Templates/Scanweb/Pages/contentChangeRequest/ImageID"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="ImageID_Change" Width="300" runat="server"></asp:TextBox><br />
                                <asp:RegularExpressionValidator ErrorMessage="test" ID="ImageIntegerValidator_Change" CssClass="ReqCss" ControlToValidate="ImageID_Change" ValidationExpression="^[0-9]*" runat="server">
                                </asp:RegularExpressionValidator>
                                
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell49" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('ImageIDHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell50" runat="server">
                                <div id="Div3" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate28" Text="/Templates/Scanweb/Pages/contentChangeRequest/ImageIDHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow11" runat="server">
                            <asp:TableCell ID="TableCell51" runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate9" Text="/Templates/Scanweb/Pages/contentChangeRequest/CopyText"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell52" runat="server">
                                <asp:TextBox ID="CopyText_Change" Rows="7" Width="300" TextMode="MultiLine" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell53" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('CopyTextHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell54" runat="server">
                                <div id="Div4" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate29" Text="/Templates/Scanweb/Pages/contentChangeRequest/CopyTextHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <br />
                    <h2>
                        Fill in below to create an offer</h2>
                    <asp:Table ID="Table2" CssClass="CCRFTable" runat="server" CellPadding="10" Width="1000">
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server" Width="300">
                                <h3>
                                    <EPiServer:Translate ID="Translate11" Text="/Templates/Scanweb/Pages/contentChangeRequest/UniqueSellingPoints"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell runat="server" Width="300">
                                <asp:TextBox ID="UniqueSellingPoints" Rows="7" Width="300" TextMode="MultiLine" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell35" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('UniqueSellingPointsHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell18" runat="server">
                                <div id="UniqueSellingPointsHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate30" Text="/Templates/Scanweb/Pages/contentChangeRequest/UniqueSellingPointsHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                                &nbsp;
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate12" Text="/Templates/Scanweb/Pages/contentChangeRequest/OfferDetail"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                                <asp:TextBox ID="OfferDetails" Rows="7" Width="300" TextMode="MultiLine" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell36" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('OfferDetailHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell19" runat="server">
                                <div id="OfferDetailHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate31" Text="/Templates/Scanweb/Pages/contentChangeRequest/OfferDetailHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">
                                <h3>
                                    <EPiServer:Translate ID="Translate14" Text="/Templates/Scanweb/Pages/contentChangeRequest/OfferStartDate"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell runat="server">
                                <asp:TextBox ID="OfferStartDate" Width="300" runat="server"></asp:TextBox><br />
                                <asp:RegularExpressionValidator ID="validOfferStartDate" CssClass="ReqCss" ValidationExpression="^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$"
                                    ControlToValidate="OfferStartDate" runat="server"></asp:RegularExpressionValidator>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell37" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('OfferStartDateHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell20" runat="server">
                                <div id="OfferStartDateHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate32" Text="/Templates/Scanweb/Pages/contentChangeRequest/OfferStartDateHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell>
                                <h3>
                                    <EPiServer:Translate ID="Translate15" Text="/Templates/Scanweb/Pages/contentChangeRequest/OfferEndDate"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="OfferEndDate" Width="300" runat="server"></asp:TextBox><br />
                                <asp:RegularExpressionValidator ID="validOfferEndDate" CssClass="ReqCss" ValidationExpression="^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$"
                                    ControlToValidate="OfferEndDate" runat="server"></asp:RegularExpressionValidator>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell38" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('OfferEndDateHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell21" runat="server">
                                <div id="OfferEndDateHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate33" Text="/Templates/Scanweb/Pages/contentChangeRequest/OfferEndDateHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                          <asp:TableRow ID="TableRow7" runat="server">
                            <asp:TableCell>
                                <h3>
                                    <EPiServer:Translate ID="Translate35" Text="/Templates/Scanweb/Pages/contentChangeRequest/OfferImageID"
                                        runat="server" />
                                </h3>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="OfferImageID" Width="300" runat="server"></asp:TextBox><br />
                                <asp:RegularExpressionValidator ErrorMessage="test" ID="RegularExpressionValidator1" CssClass="ReqCss" ControlToValidate="OfferImageID" ValidationExpression="^[0-9]*" runat="server">
                                </asp:RegularExpressionValidator>      
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell2" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('OfferImageIDHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell40" runat="server">
                                <div id="OfferImageIDHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate36" Text="/Templates/Scanweb/Pages/contentChangeRequest/OfferImageIDHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                             <h3><EPiServer:Translate Text="/Templates/Scanweb/Pages/contentChangeRequest/ExternalLink" runat="server" /></h3>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="ExternalLink" Width="300" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell39" Width="10" runat="server">
                               <a href="#" class="HelpTextSwitch" onclick="SwitchClassName('ExternalLinkHelpText', 'HelpTextVisible', 'HelpTextNotVisible');return false;">
                                ?</a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell22" runat="server">
                                <div id="ExternalLinkHelpText" class="HelpTextNotVisible">
                                    <span class="HelpText">
                                        <EPiServer:Translate ID="Translate34" Text="/Templates/Scanweb/Pages/contentChangeRequest/ExternalLinkHelpText"
                                            runat="server" />
                                    </span>
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">
                                <asp:Button ID="Submit" translate="/Templates/Scanweb/Pages/contentChangeRequest/Submit"
                                    runat="server" OnClick="Submit_Click" />
                                    <input type ="button" id="Preview" value ="Preview" class="jqModal" onclick="GetPreviewModalPopup();" />                                                                      
                            </asp:TableCell>                           
                            
                        </asp:TableRow>
                    </asp:Table>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="ChangeRequestFeedback" runat="server" Visible="false">
                    <asp:Table ID="ContentChangeRequestFeedbackTable" CssClass="ContentChangeRequestFeedbackTable"
                        runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <EPiServer:Translate ID="FeedbackLanguage" Text="/Templates/Scanweb/Pages/ContentChangeRequest/Feedback"
                                    runat="server" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:PlaceHolder>
                &nbsp;
            </div>
        </div>
    </form>
    <div class="jqmWindow" id="dvPreview">
        <h2 class="title">Content Change Request Form - Preview</h2>
        <a href='#' class='jqmClose'>Close</a>
        <div id="Previewdv" />
    </div>
    

</body>
</html>
