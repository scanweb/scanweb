/*
Copyright � 1997-2007 EPiServer AB. All Rights Reserved.

This code may only be used according to the EPiServer License Agreement.
The use of this code outside the EPiServer environment, in whole or in
parts, is forbidded without prior written permission from EPiServer AB.

EPiServer is a registered trademark of EPiServer AB. For
more information see http://www.episerver.com/license or request a
copy of the EPiServer License Agreement by sending an email to info@ep.se
*/

namespace EPiServer.Templates.Public.Pages
{
    /// <summary>
    /// The standard page is the most commonly used page on the web site.
    /// The standard page disaplays the properties "MainBody" and "SecondaryBody"
    /// and optionally a main and secondary page list beneath the bodies if the 
    /// page properties "MainListRoot" and "SecondaryListRoot" is set.
    /// </summary>
    public partial class Page : TemplatePage
    {
    }
}