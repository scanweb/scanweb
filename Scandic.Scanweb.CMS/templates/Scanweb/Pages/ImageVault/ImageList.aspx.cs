////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  ImageList                                              //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Code behind of ImageList page.
    /// </summary>
    public partial class ImageList : ScandicTemplatePage
    {
    }
}