<%@ Page language="c#" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWide.master" Inherits="Scandic.Scanweb.CMS.Templates.ImageList" Codebehind="ImageList.aspx.cs" %>
<%@ Register TagPrefix="Scanweb" TagName="MainBody" 	        Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="InfoBox"              Src="~/Templates/Scanweb/Units/Static/InfoBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="ImageList"        Src="~/Templates/Scanweb/Units/Placeable/ImageList.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="RightColumn"              Src="~/Templates/Scanweb/Units/Placeable/RightColumn.ascx" %>

<asp:Content ContentPlaceHolderID="MainBodyLeftRegion" runat="server">           
    <Scanweb:ImageList ID="ImageList1" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="MainBodyRightRegion" runat="server">
   <Scanweb:RightColumn runat="server" />
</asp:Content>