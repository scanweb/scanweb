using System;
using Scandic.Scanweb.CMS.Templates.MasterPages;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.SpecializedProperties;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// OverviewPage
    /// </summary>
    public partial class OverviewPage : ScandicTemplatePage
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            MainIntroPlaceHolder.Visible = (CurrentPage["TopMiddleHeading"] as string != null);
            MainBodyPlaceHolder.Visible = (CurrentPage["TopMiddleBody"] as string != null);
            SecondaryLeftHeadlingPlaceHolder.Visible = (CurrentPage["SecondaryLeftHeading"] as string != null);
            SecondaryRightHeadingPlaceHolder.Visible = (CurrentPage["SecondaryRightHeading"] as string != null);
            SecondaryPageList.ShowItemLink = (CurrentPage["ShowItemLink"] != null);
            SecondaryPageList.ShowDate = (CurrentPage["ShowDate"] != null);
            SecondaryPageList.PreviewTextLength = CurrentPage["PreviewTextLength"] != null ? (int)CurrentPage["PreviewTextLength"] : 100;

            if (!Convert.ToBoolean(CurrentPage["MiddleSearchModuleVisible"]))
            {
                MeetingsSearch.Visible = false;
                purple_driver.Visible = false;
            }
            
            if (null != CurrentPage["MainBodyRight"])
            {
                InfoHeader.Visible = true;
            }
            if (CurrentPage["ShowLeftMenu"] != null)
            {
                MasterPageWideFull masterPageWideFull = (MasterPageWideFull)this.Master;
                if (LeftMenu != null && masterPageWideFull.SubMenuList != null)
                {
                    LeftMenu.MenuList = masterPageWideFull.SubMenuList;
                    LeftMenuPlaceHolder.Visible = true;
                }
            }

            var activateImageCarousel = CurrentPage["ActivateImageCarousel"] != null && Convert.ToBoolean(CurrentPage["ActivateImageCarousel"]);


            if (activateImageCarousel)
            {
                MiscellaneousSessionWrapper.OverviewCarousel = false;
                if (CurrentPage["Width"] != null && (Convert.ToInt32(CurrentPage["Width"]) == 0 || Convert.ToInt32(CurrentPage["Width"]) == 1))
                {
                    TopAreaPlaceHolder.Visible = true;
                    TopAreaMediaDiv.Visible = false;
                }

                if ((int)CurrentPage["Width"] == (int)FullWidthWithBox.Width.Full)
                {
                    var slidesDiv = new Panel() { ID = "slides" };
                    TopAreaFullWidthPlaceHolder.Visible = true;
                    TopAreaFullWidthPlaceHolder.Controls.Clear();
                    TopAreaFullWidthPlaceHolder.Controls.Add(slidesDiv);
                }
            }
            else
            {
                if (CurrentPage["TopArea"] != null)
                    TopAreaPlaceHolder.Visible = true;
                TopAreaMediaDiv.Visible = true;
            }
        }

        public string CurrentPageCarouselWidth
        {
            get { return CurrentPage["Width"] != null ? CurrentPage["Width"].ToString() : "0"; }
        }

        public int CurrentPageLinkId
        {
            get { return CurrentPage.PageLink.ID; }
        }

        public string CurrentPageTypeId
        {
            get
            {
                return CurrentPage.PageTypeID.ToString();
            }
        }
        public string MainOverviewImage
        {
            get 
            {
                if (CurrentPage["ActivateImageCarousel"] != null && Convert.ToBoolean(CurrentPage["ActivateImageCarousel"]))
                    return string.Empty;
                else
                    return Convert.ToString(CurrentPage["MainOvewImage"]);
            }
        }
    }
}