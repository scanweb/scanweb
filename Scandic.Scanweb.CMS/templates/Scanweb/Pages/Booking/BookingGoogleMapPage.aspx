<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BookingGoogleMapPage.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.BookingGoogleMapPage" %>
<%@ Register TagPrefix="Scanweb" TagName="SelectHotelGoogleMap" Src="~/Templates/Scanweb/Units/Static/Booking/SelectHotelGoogleMap.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="SiteCatalystCloudTag" Src="~/Templates/Scanweb/Units/Static/SiteCatalystCloudTag.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="AdobeDTMFooter" Src="~/Templates/Scanweb/Units/Static/AdobeDTMFooter.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" runat="server" id="HtmlElement">
<head runat="server">
    <title>Untitled Page</title>
    <Scanweb:SiteCatalystCloudTag runat="server" />
    <link   type="text/css" 
            rel="Stylesheet" 
            href="/Templates/Booking/Styles/Default/combined.css?v=<%=CmsUtil.GetCSSVersion()%>" >			
<link   type="text/css" 
            rel="Stylesheet" 
            href="/Templates/Booking/Styles/Default/flexslider.css?v=<%=CmsUtil.GetCSSVersion()%>" >
    <script type="text/javascript" language="JavaScript">
        <% DateTime ServerDate = DateTime.Now;%>
        var serverYear      = '<%= ServerDate.Year %>';
        var serverMonth     = '<%= ServerDate.Month %>';
        // Month sequence stats from 0 (ZERO). 
        // In .NET code, month sequence starts at 1. 
        // So we are deducting 1 from the month value returned by .NET.
        serverMonth         = serverMonth - 1;
        var serverDay       = '<%= ServerDate.Day %>';
        var serverHour      = '<%= ServerDate.Hour %>';
        var serverMinute    = '<%= ServerDate.Minute %>';
        var serverSecond    = '<%= ServerDate.Second %>';
        var serverMilliSec  = '<%= ServerDate.Millisecond %>';
        var serverDate = new Date(serverYear, serverMonth, serverDay, serverHour, serverMinute, serverSecond, serverMilliSec);
    </script>
    <!-- jQuery -->
    
    <script     
        type="text/javascript" 
        src="/Templates/Booking/JavaScript/final.js?v=<%=CmsUtil.GetJSVersion()%>" >
    </script>
<script type="text/javascript" language="javascript" src="/Templates/Scanweb/Javascript/SiteCatalyst/s_code.js?v=<%=CmsUtil.GetJSVersion()%>"></script>
</head>
<body>
    <form id="form1" runat="server">
   
    <div>
        <Scanweb:SelectHotelGoogleMap ID="SelectHotelGoogleMap1" runat="server" />
    </div>
    </form>
    <!-- Adobe Dynamic Tag Management Footer -->
	<Scanweb:AdobeDTMFooter runat="server" /> 
    <!-- End Adobe Dynamic Tag Management Footer -->
</body>
</html>
