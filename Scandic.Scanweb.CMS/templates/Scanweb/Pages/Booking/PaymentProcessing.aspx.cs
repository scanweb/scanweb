﻿using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System.Configuration;


namespace Scandic.Scanweb.CMS.Templates.Reservation
{
    [AccessibleWhenSessionExpired(false, "PaymentProcessing")]
    public partial class PaymentProcessing : ScandicTemplatePage
    {
        #region Properties
        private PaymentController paymentController = null;
        /// <summary>
        /// Gets PaymentController
        /// </summary>
        private PaymentController m_PaymentController
        {
            get
            {
                if (paymentController == null)
                {
                    IContentDataAccessManager contentDataAccessManager = new ContentDataAccessManager();
                    IAdvancedReservationDomain advancedReservationDomain = new AdvancedReservationDomain();
                    ILoyaltyDomain loyaltyDomain = new LoyaltyDomain();
                    IReservationDomain reservationDomain = new ReservationDomain();
                    paymentController = new PaymentController(contentDataAccessManager, advancedReservationDomain, loyaltyDomain, reservationDomain);
                }
                return paymentController;
            }
        }

        public string paymentInProgressInfoMessage = string.Empty;
        public string checkReservationStatusFailureMessage = string.Empty;
        public string browserExitPaymentInfoMessage = string.Empty;
        #endregion

        #region Private Methods
        /// <summary>
        /// Sends an email when there is an exception while processing the payment after the Nets-Payment has been authorized.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="ex"></param>
        private void SendPaymentFailedEmail(string transactionId, Exception ex)
        {
            EmailEntity emailEntity = new EmailEntity();
            emailEntity.Recipient = AppConstants.EmailRecipientsForOnlinePaymentFailures;
            emailEntity.Sender = AppConstants.EmailSenderForOnlinePaymentFailures;
            emailEntity.Subject = string.Format(
                "Online payment | Exception while processing the payment for the TransactionId: {0} after the Nets-Payment has been authorized.", transactionId);
            emailEntity.Body = string.Format("Exception occured in the PaymentProcessing Page for the TransactionId: {0}; the Nets-Payment for the same has been authorized.", transactionId);
            emailEntity.TextEmailBody = string.Format("Exception Details:\n {0}", ex);
            CommunicationService.SendMail(emailEntity, null);
        }
        #endregion

        #region Protected Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            string queryStringTransactionId = string.Empty;
            string sessionTransactionId = string.Empty;
            string paymentResponseCode = string.Empty;
            try
            {
                ReservationNumberSessionWrapper.IsAuthorizationDone = false;
                ReservationNumberSessionWrapper.IsAppErrorPageTriggeredFromThePaymentProcessingPage = false;

                checkReservationStatusFailureMessage = WebUtil.GetTranslatedText("/PSPErrors/ReservationNumberIfPaymentProcessingPageIsStuckFAILURE");
                paymentInProgressInfoMessage = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/PaymentInProgressInfoMessage");
                browserExitPaymentInfoMessage = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/BrowserExitPaymentInfoMessage");

                if (Request.QueryString[QueryStringConstants.PAYMENT_TRANSACTIONID] != null)
                    queryStringTransactionId = Request.QueryString[QueryStringConstants.PAYMENT_TRANSACTIONID].ToString();

                if (Request.QueryString[QueryStringConstants.PAYMENT_RESPONSECODE] != null)
                    paymentResponseCode = Request.QueryString[QueryStringConstants.PAYMENT_RESPONSECODE].ToString();

                string hotelID = string.Empty;
                if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                    hotelID = SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode;

                AppLogger.LogOnlinePaymentTransactionsInfoMessage(
                            string.Format("Redirected back from Nets window with the TransactionID: {0}, HotelId: {4}, Session Transaction ID: {5}, Reservation Number: {6} and the ResponseCode: {1} to the WebServer: {2} at {3}.",
                            queryStringTransactionId, paymentResponseCode, System.Environment.MachineName, DateTime.Now, hotelID, Reservation2SessionWrapper.PaymentTransactionId, ReservationNumberSessionWrapper.ReservationNumber));

                if (WebUtil.IsSessionValid() && ((Request.QueryString[QueryStringConstants.SESSION_EXPIRED] == null) ||
                   ((Request.QueryString[QueryStringConstants.SESSION_EXPIRED] != null) &&
                   !Convert.ToBoolean(Request.QueryString[QueryStringConstants.SESSION_EXPIRED]))))
                {

                    if (!IsPostBack && !string.IsNullOrEmpty(queryStringTransactionId) && !string.IsNullOrEmpty(paymentResponseCode))
                    {
                        sessionTransactionId = Reservation2SessionWrapper.PaymentTransactionId;

                        if (string.IsNullOrEmpty(sessionTransactionId) ||
                            !(sessionTransactionId.Equals(queryStringTransactionId, StringComparison.InvariantCultureIgnoreCase))
                            ||
                            !(paymentResponseCode.Equals(SessionBookingConstants.NETS_PAYMENT_RESPONSECODE, StringComparison.InvariantCultureIgnoreCase)))
                        {
                            m_PaymentController.IgnoreBooking();
                            m_PaymentController.ProcessErrorMessageForNetsErrorCode(paymentResponseCode, PaymentConstants.PaymentWindowErrorSource);
                            if (BookingEngineSessionWrapper.IsModifyBooking)
                            {
                                Response.Redirect(AppConstants.MODIFYBOOKINGDETAILSURL, false);
                            }
                            else
                            {
                                Response.Redirect(AppConstants.CHECKOUTURL, false);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessageHeader = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/PaymentProcessingExceptionInfoMessageHeader");
                string errorMessage = string.Empty;
                if (ReservationNumberSessionWrapper.IsAuthorizationDone)
                {
                    SendPaymentFailedEmail(queryStringTransactionId, ex);
                    errorMessage = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/PaymentProcessingExceptionInfoMessageWhenPaymentIsAuthorized");
                }
                else
                {
                    errorMessage = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/PaymentProcessingExceptionInfoMessageWhenPaymentIsNotAuthorized");
                }
                string sessionIdentifier = string.Empty;
                if (UNTSessionWrapper.UserActionStore != null && !string.IsNullOrEmpty(UNTSessionWrapper.UserActionStore.SessionIdentifier))
                    sessionIdentifier = UNTSessionWrapper.UserActionStore.SessionIdentifier;
                ReservationNumberSessionWrapper.IsAppErrorPageTriggeredFromThePaymentProcessingPage = true;
                string hotelID = string.Empty;
                if (SearchCriteriaSessionWrapper.SearchCriteria != null)
                    hotelID = SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode;

                AppLogger.LogOnlinePaymentFatalException(ex, string.Format("Exception in PaymentProcessing Page Load. Nets Transaction ID: {0}. Session Transaction ID: {1}. Session Identifier: {2}.  Nets Response Code {3}. Temp Reservation No: {4}. Search Hotel: {5}. Exception Message: {6}. Stack Trace: {7}. Page Header Message: {8}. Page Error Message: {9}. WebServer:{10}",
                    queryStringTransactionId, sessionTransactionId, sessionIdentifier, paymentResponseCode, ReservationNumberSessionWrapper.ReservationNumber, hotelID, ex.Message, ex.StackTrace, errorMessageHeader, errorMessage, System.Environment.MachineName));

                ShowApplicationError(errorMessageHeader, errorMessage);
            }
        }
        #endregion

        #region Timeout for Payment Processing Page

        public int GetTimeoutForPaymentProcessing
        {
            get { return AppConstants.PAYMENT_PROCESSING_TIMEOUT; }

        }

        #endregion
    }
}
