<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Reservation.Confirmation"
    Codebehind="Confirmation.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageDefault.master" %>
<%@ Register TagPrefix="Booking" TagName="Confirmation" Src="~/Templates/Booking/Units/BookingConfirmation.ascx" %>
<%@ Register TagPrefix="AddAnotherHotelPreFix" TagName="AddAnotherHotel" Src="~/Templates/Booking/Units/BookingModuleBig.ascx" %>
<%@ Register TagPrefix="Booking" TagName="FlyOut" Src="~/Templates/Booking/Units/BookingModuleFlyout.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="HotelPromoBox" Src="~/Templates/Scanweb/Units/Placeable/HotelPromoBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="AboutOurRates" Src="~/Templates/Booking/Units/AboutOurRates.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
    <Booking:Confirmation runat="server" />
    <!--START: Action Button: button hiddedn as it is not part of cuurent itration(3..0)-->
    <div class="conf_Popup">
        <!--START:Shameem:14th July:Add Another Htel:
     Added for Add New Hotel button on confirmation page-->
        <div id="idAddAnotherHotel" runat="server" class="jqmWindow dialog" style="display: none;"
            overflow="auto">
            <Booking:FlyOut ID="bookingFlyOut" runat="server">
            </Booking:FlyOut>
            <div class="guestInfoPopup" id="RoomCategoryContainer" runat="server">
                <div id="LeftContentArea">
                            <div class="closeBtn">
                        <a href="#" class="jqmClose scansprite blkClose" style="width:30px;height:27px;padding:0px;">&nbsp;</a></div>
                    <div id="BookingArea" class="confPopupbookingArea" runat="server">
                        <AddAnotherHotelPreFix:AddAnotherHotel id="udAddAnotherHotel" runat="server">
                        </AddAnotherHotelPreFix:AddAnotherHotel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" language="javascript">
        $(window).load(function() {
            var errInvalidRes = $fn(_endsWith('invalidReservationNumber'));
            if (errInvalidRes.value == 'true') {
                $('div[id$="idAddAnotherHotel"]').jqm();
                $('div[id$="idAddAnotherHotel"]').jqmShow();
            }
        });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
    <%--Commented by Shameem |Res 2.0 | Booking details page| Need to uncomment--%>
    <%--<Booking:Module runat="server" />--%>
    
    <div style="margin-top:20px;">
    <div>
    <scanweb:AboutOurRates Id ="AboutOurRates" runat="server"></scanweb:AboutOurRates>
    </div>
    <div>
    <scanweb:HotelPromoBox ID="Box1" OfferPageLinkPropertyName="BoxContainer1" CssClass="BoxLarge" ImagePropertyName ="BoxImageMedium" ImageMaxWidth="226" runat="server" />
  </div>
    <div>
    <scanweb:HotelPromoBox ID="Box2" OfferPageLinkPropertyName="BoxContainer2" CssClass="BoxLarge" ImagePropertyName ="BoxImageMedium" ImageMaxWidth="226" runat="server" />
  </div>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptRegion" runat="server">
    <!-- Body script region -->
    <asp:Literal ID="BodyScriptLiteral" runat="server"></asp:Literal>
    <asp:Literal ID="BodyScriptRegularBookingLiteral" runat="server"></asp:Literal>
    <!-- End body script region -->
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="HeaderScriptRegion" runat="server">
    <!-- Head script region -->
    <asp:literal id="HeadScriptLiteral" runat="server"></asp:literal>
    <!-- End head script region -->



</asp:Content>

