//  Description					:   Print Receipt Page                                    //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages.Booking
{
    /// <summary>
    /// PrintReceipt page is used for printing receipt and PDF generation
    /// </summary>
    public partial class PrintReceipt : ScandicTemplatePage
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("PrintReceipt.ASPX:Page_Load() StartTime");
            string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();
            switch (siteLanguage)
            {
                case LanguageConstant.LANGUAGE_FINNISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_FINNISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_DANISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_DANISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_SWEDISH:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_SWEDISH.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_GERMAN:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_GERMAN.ToLower());
                    break;
                case LanguageConstant.LANGUAGE_RUSSIA:
                    form1.Attributes.Add("class", LanguageConstant.LANGUAGE_RUSSIA.ToLower());
                    break;
                default:
                    break;
            }
            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("PrintReceipt.ASPX:Page_Load() EndTime");
        }

        /// <summary>
        /// IsSessionValid
        /// </summary>
        /// <returns>True/False</returns>
        public bool IsSessionValid()
        {
            if (HttpContext.Current.Session != null && HttpContext.Current.Session.IsNewSession)
            {
                string cookieHeader = HttpContext.Current.Request.Headers["Cookie"];
                if (!string.IsNullOrEmpty(cookieHeader) && cookieHeader.IndexOf("ASP.NET_SessionId") >= 0)
                {
                    Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE), true);
                }
            }
            return true;
        }
    }
}