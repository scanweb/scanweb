<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Reservation.BookingDetail"
    Codebehind="BookingDetail.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageDefault.master" %>

<%@ Register TagPrefix="Booking" TagName="BookingDetail" Src="~/Templates/Booking/Units/BookingDetail.ascx" %>
<%@ Register TagPrefix="Booking" TagName="ShoppingCart" Src="~/Templates/Booking/Units/ShoppingCart.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="BookingModuleAlternativeBox" Src="~/Templates/Scanweb/Units/Placeable/BookingModuleAlternativeBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="HotelPromoBox" Src="~/Templates/Scanweb/Units/Placeable/HotelPromoBox.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
    <Booking:BookingDetail runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
    <div class="singleCol">
        <Booking:ShoppingCart id="shoppingCart" runat="server" />
        <!--AMS Fix for OWS Monitor -->
        <Scanweb:BookingModuleAlternativeBox CssClass="AlternativeBookingBoxMedium" ID="AlternativeBookingModule"
            runat="server" />
        <div>
            <Scanweb:HotelPromoBox ID="Box1" OfferPageLinkPropertyName="BoxContainer1" CssClass="BoxLarge"
                ImagePropertyName="BoxImageMedium" ImageMaxWidth="226" runat="server" />
        </div>
        <div>
            <Scanweb:HotelPromoBox ID="Box2" OfferPageLinkPropertyName="BoxContainer2" CssClass="BoxLarge"
                ImagePropertyName="BoxImageMedium" ImageMaxWidth="226" runat="server" />
        </div>
        <%--        Special Alert - Begin--%>
            <div class="alertwrapperrightcolunm" id="divSpAlertWrap" runat="server">
                <div class="splalertcontent" id="divSpAlert" runat="server">
                </div>
            </div>
    <%--        special Alert - End--%>
    </div>
</asp:Content>
