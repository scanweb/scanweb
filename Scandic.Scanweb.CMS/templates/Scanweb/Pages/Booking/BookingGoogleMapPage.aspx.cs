//  Description					:   BookingGoogleMapPage                                  //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Web.Services;
using Scandic.Scanweb.CMS.code.Util.Map;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Code behind of BookingGoogleMapPage
    /// </summary>
    public partial class BookingGoogleMapPage : System.Web.UI.Page
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapHotelUnit"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetGoogleMapHotelContetOverlayInfo(MapHotelUnit mapHotelUnit)
        {
            return mapHotelUnit.GenerateInfoWindowMarkUp();
        }
    }
}