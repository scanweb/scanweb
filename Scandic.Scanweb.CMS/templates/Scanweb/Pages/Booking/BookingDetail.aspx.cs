////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  BookingDetail                                          //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Reservation
{
    /// <summary>
    /// BookingDetail
    /// </summary>
    [AccessibleWhenSessionExpired(false, "BookingDetail")]
    [AccessibleWhenSessionInValid(false)]
    public partial class BookingDetail : ScandicTemplatePage
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            shoppingCart.Visible = false;
            AlternativeBookingModule.Visible = false;

            bool shopingCartVisibility = OWSVisibilityControl.BookingModuleShouldBeVisible;

            if (shopingCartVisibility)
            {
                shoppingCart.Visible = shopingCartVisibility;
                shoppingCart.SetPageType(EpiServerPageConstants.BOOKING_DETAILS_PAGE);
            }
            else
            {
                AlternativeBookingModule.Visible = !shopingCartVisibility;
            }
            if (!IsPostBack && SearchCriteriaSessionWrapper.SearchCriteria != null 
                && !string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode))
            {
                string spAlert = ContentDataAccess.GetSpecialAlert(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode, false);
                if (!string.IsNullOrEmpty(spAlert))
                {
                    divSpAlertWrap.Visible = true;
                    divSpAlert.InnerHtml = spAlert;
                }
                else
                {
                    divSpAlertWrap.Visible = false;
                }
            }
        }
    }
}