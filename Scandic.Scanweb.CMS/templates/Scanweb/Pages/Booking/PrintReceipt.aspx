<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintReceipt.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages.Booking.PrintReceipt" EnableViewState="false"%>
<%@ Register TagPrefix="Booking" TagName="Receipt" Src="~/Templates/Booking/Units/BookingReceipt.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="Header"		        Src="~/Templates/Scanweb/Units/Static/Header.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <script type="text/javascript" language="javascript" src="/Templates/Scanweb/Javascript/gmaps-utility-library/ScandicGoogleMapUtil.js"></script>
    <form id="form1" runat="server">
    <div>
    <Scanweb:Header runat="server" />
    <Booking:Receipt runat="server" />
    </div>
    </form>
</body>
</html>
