<%@ Page Language="C#" CodeBehind="ChildrenDetail.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Reservation.ChildrenDetail" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageDefault.master"%>
<%@ Register TagPrefix="ChildDetails" TagName="ChildrenDetails" Src="~/Templates/Booking/Units/BookingKids.ascx" %>
<%--RK: Commenting below code as bookingmodulemedium development is not completed and would break in CMS if not commented.
        This needs to be uncommented later when the development is completed --%>
<%-- <%@ Register TagPrefix="Booking" TagName="Module" Src="~/Templates/Booking/Units/BookingModuleMedium.ascx" %> --%> 
<%@ Register TagPrefix="ChildrenOffer" TagName="PromoBoxOffer" Src="~/Templates/Scanweb/Units/Placeable/PromoBoxOffer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
    <ChildDetails:ChildrenDetails ID="ChildrenDetails" runat="server" />
    <asp:Literal ID="ChildrenAttraction" runat="server"></asp:Literal>
</asp:Content>

<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
  <%--RK: Commenting below code as bookingmodulemedium development is not completed and would break in CMS if not commented.
        This needs to be uncommented later when the development is completed --%>
  <%-- <Booking:Module ID="BookingModuleMedium" runat="server" />--%>
  <div class="BoxContainer">
        <ChildrenOffer:PromoBoxOffer ID="Box1" OfferPageLinkPropertyName="BoxContainer1" CssClass="BoxLarge" ImagePropertyName ="BoxImageLarge" ImageMaxWidth="349" runat="server" HideBookNow="true" />
  </div>
</asp:Content>
