<%@ Page language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Reservation.SelectRate" Codebehind="SelectRate.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageDefault.master" %>

<%@ Register TagPrefix="Booking" TagName="SelectRate" Src="~/Templates/Booking/Units/SelectRate.ascx" %>
<%@ Register TagPrefix="Booking" TagName="ShoppingCart" Src="~/Templates/Booking/Units/ShoppingCart.ascx" %>
<%@ Register TagPrefix="Booking" TagName="AboutOurRates" Src="~/Templates/Booking/Units/AboutOurRates.ascx"  %>
<%@ Register TagPrefix="Scanweb" TagName="BookingModuleAlternativeBox" Src="~/Templates/Scanweb/Units/Placeable/BookingModuleAlternativeBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="HotelPromoBox" Src="~/Templates/Scanweb/Units/Placeable/HotelPromoBox.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
    <Booking:SelectRate ID="SelectRateControl" runat="server" />
</asp:Content>


<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
 <div style="display: block;" class="scLoadIndicator" id="ShoppingCartProgressDiv">
    <img src="<%= ResolveUrl("~/templates/Scanweb/Styles/Default/Images/ajax-loader_big.gif") %>" alt="image" align="top" />
    <span class="ploadingText"><%=
                EPiServer.Core.LanguageManager.Instance.Translate(
                    "/Templates/Scanweb/Units/Static/FindHotelSearch/Loading") %></span>    
</div>
 <div style="display: none;" id="ShoppingCartData">
 <%-- <Booking:Module ID="BookingModuleMedium" runat="server" />--%>
 <Booking:ShoppingCart id="shoppingCart" runat="server"></Booking:ShoppingCart>
 <!--AMS Fix for OWS Monitor -->
  <Scanweb:BookingModuleAlternativeBox CssClass="AlternativeBookingBoxSmall" ID="AlternativeBookingModule" runat="server" />
<Booking:AboutOurRates id="OurRates" runat="server"></Booking:AboutOurRates>
<div>
    <scanweb:HotelPromoBox ID="Box1" OfferPageLinkPropertyName="BoxContainer1" CssClass="BoxLarge" ImagePropertyName ="BoxImageMedium" ImageMaxWidth="226" runat="server" />
  </div>
    <div>
    <scanweb:HotelPromoBox ID="Box2" OfferPageLinkPropertyName="BoxContainer2" CssClass="BoxLarge" ImagePropertyName ="BoxImageMedium" ImageMaxWidth="226" runat="server" />
  </div>
</div>
<script type="text/javascript" language="javascript">
$('#ShoppingCartProgressDiv').hide();
$('#ShoppingCartData').show();

$('#CalendarProgressDiv').hide();
$('#CalendarData').show();
</script>
</asp:Content>

