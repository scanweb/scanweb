<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrinterFriendlyConfirm.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages.Booking.PrinterFriendlyConfirm" EnableViewState="false"%>
<%@ Register TagPrefix="Booking" TagName="Confirmation" Src="~/Templates/Booking/Units/BookingConfirmation.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="Header"		        Src="~/Templates/Scanweb/Units/Static/Header.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <script type="text/javascript" language="javascript" src="/Templates/Scanweb/Javascript/gmaps-utility-library/ScandicGoogleMapUtil.js?v=<%=CmsUtil.GetJSVersion()%>"></script>
    <form id="form1" runat="server">
    <div>
    <Scanweb:Header runat="server" />
    <Booking:Confirmation runat="server" />
    </div>
    </form>
</body>
</html>
