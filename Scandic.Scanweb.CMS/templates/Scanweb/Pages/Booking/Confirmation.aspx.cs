//  Description					:   Confirmation                                          //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Web.UI;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;

namespace Scandic.Scanweb.CMS.Templates.Reservation
{
    /// <summary>
    /// Confirmation
    /// </summary>
    [AccessibleWhenSessionExpired(false, "Confirmation")]
    [AccessibleWhenSessionInValid(false)]
    public partial class Confirmation : ScandicTemplatePage
    {
        private List<string> viewstateAllowedControls = new List<string>
                                                            {
                                                                "btnSearch",
                                                                "spnSearch",
                                                                "btnBonusChequeSearch",
                                                                "spnBonusChequeSearch",
                                                                "btnRewardNightSearch",
                                                                "spnRewardNightSearch",                                                             
                                                            };

        private bool DisableViewState(ControlCollection controls)
        {
            bool flag = true;
            foreach (Control control in controls)
            {
                if (control.HasControls())
                    if (DisableViewState(control.Controls) && !viewstateAllowedControls.Contains(control.ID))
                        control.EnableViewState = false;
                    else
                    {
                        flag = false;
                    }
                else
                {
                    if (!viewstateAllowedControls.Contains(control.ID))
                        control.EnableViewState = false;
                    else
                    {
                        flag = false;
                    }
                }
            }
            return flag;
        }
         /// <summary>
        /// On init event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Error += new EventHandler(Confirmation_Error);
        }

        void Confirmation_Error(object sender, EventArgs e)
        {
            Exception excpn = Server.GetLastError();
            string deepLinkUrl = string.Format("http://{0}/{1}?{2}={3}&{4}={5}", Request.Url.Host, AppConstants.DLREDIRECT, AppConstants.DEEPLINK_VIEW_MODIFY_RESERVATION_ID, 
                           Convert.ToString(Request.QueryString["BNo"]), AppConstants.DEEPLINK_VIEW_MODIFY_LAST_NAME, Convert.ToString(Request.QueryString["LNm"]));
            string innerHtml = string.Format("<a href={0}>{1}</a>", deepLinkUrl, deepLinkUrl);
            string errMsg = string.Format(WebUtil.GetTranslatedText("/PSPErrors/ReservationNumberIfPaymentProcessingPageIsStuckFAILURE"), Convert.ToString(Request.QueryString["BNo"]));
            string viewModifyText = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/ViewModifyReservationMessage");
            MiscellaneousSessionWrapper.ErrorPageError = excpn;
            MiscellaneousSessionWrapper.IsMobileError = false;
            AppLogger.LogCustomException(excpn, AppConstants.APPLICATION_EXCEPTION, string.Format("{0} deeplinkUrl : {1}", errMsg, deepLinkUrl));
            Server.ClearError();
            ShowApplicationErrorWithQueryString(AppConstants.APPLICATION_EXCEPTION, string.Format("{0}<br/>{1}<br/>{2}", errMsg, viewModifyText, innerHtml),"ScanwebCnfrmn");
        }
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {   
            if (DisableViewState(Controls))
                Page.EnableViewState = false;

            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("Confirmation.ASPX:Page_Load() StartTime");
            HeadScriptLiteral.Text = CurrentPage["HeadScriptArea"] as string ?? string.Empty;
            BodyScriptLiteral.Text = CurrentPage["BodyScriptArea"] as string ?? string.Empty;

            HotelSearchEntity hotelSearch = SearchCriteriaSessionWrapper.SearchCriteria;
            if (null != hotelSearch)
            {
                if (hotelSearch.SearchingType == SearchType.REGULAR)
                {
                    BodyScriptRegularBookingLiteral.Text = CurrentPage["RegularBookingBodyScriptArea"] as string ??
                                                           string.Empty;
                }
            }

            BookingModuleBig bookingModule = udAddAnotherHotel as BookingModuleBig;
            bookingModule.IsBookingModuleReset = true;

            Scandic.Scanweb.Core.AppLogger.LogInfoMessage("Confirmation.ASPX:Page_Load() EndTime");
        }

        //protected void Page_Error(object sender, EventArgs e)
        //{
        //    Exception excpn = Server.GetLastError();
        //    ReservationNumberSessionWrapper.IsAppErrorPageTriggeredFromTheConfirmationPage = true;
        //    string deepLinkUrl = "http://" + Request.Url.Host + "/" + AppConstants.DLREDIRECT + "?" + AppConstants.DEEPLINK_VIEW_MODIFY_RESERVATION_ID + "=" + Convert.ToString(Request.QueryString["BookingNo"]) + "&"
        //                         + AppConstants.DEEPLINK_VIEW_MODIFY_LAST_NAME + "=" + Convert.ToString(Request.QueryString["LastName"]);
        //    string innerHtml = string.Format("<a href={0}>{1}</a>", deepLinkUrl, deepLinkUrl);
        //    string errMsg = WebUtil.GetTranslatedText("/PSPErrors/ReservationNumberIfPaymentProcessingPageIsStuckFAILURE");
        //    string viewModifyText = WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/ViewModifyReservationMessage");
        //    AppLogger.LogCustomException(excpn, AppConstants.APPLICATION_EXCEPTION);
        //    Server.ClearError();
        //    WebUtil.ShowApplicationError(AppConstants.APPLICATION_EXCEPTION, string.Format(errMsg + "<br/>" + viewModifyText + "{1}", Convert.ToString(Request.QueryString["BookingNo"]), innerHtml));
        //}
    }
}