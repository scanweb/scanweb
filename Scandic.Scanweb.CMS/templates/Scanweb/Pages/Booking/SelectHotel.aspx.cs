//  Description					:   SelectHotel                                           //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Web.UI;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;

namespace Scandic.Scanweb.CMS.Templates.Reservation
{
    /// <summary>
    /// Code behind of SelectHotel page
    /// </summary>
    [AccessibleWhenSessionExpired(false, "SelectHotel")]
    [AccessibleWhenSessionInValid(false)]
    public partial class SelectHotel : ScandicTemplatePage
    {
        private List<string> viewstateAllowedControls = new List<string>
                                                            {
                                                                "btnSearch",
                                                                "spnSearch",
                                                                "perNight",
                                                                "perStay",
                                                                "divSpAlertWrap",
                                                                "ddlSortHotel",
                                                                "ddlHotelsPerPage",
                                                                "childAgeforRoom1Child1",
                                                                "childAgeforRoom1Child2",
                                                                "childAgeforRoom1Child3",
                                                                "childAgeforRoom1Child4",
                                                                "childAgeforRoom1Child5",
                                                                "childAgeforRoom2Child1",
                                                                "childAgeforRoom2Child2",
                                                                "childAgeforRoom2Child3",
                                                                "childAgeforRoom2Child4",
                                                                "childAgeforRoom2Child5",
                                                                "childAgeforRoom3Child1",
                                                                "childAgeforRoom3Child2",
                                                                "childAgeforRoom3Child3",
                                                                "childAgeforRoom3Child4",
                                                                "childAgeforRoom3Child5",
                                                                "childAgeforRoom4Child1",
                                                                "childAgeforRoom4Child2",
                                                                "childAgeforRoom4Child3",
                                                                "childAgeforRoom4Child4",
                                                                "childAgeforRoom4Child5",
                                                                "bedTypeforRoom1Child1",                                                                
                                                                "bedTypeforRoom1Child2",                                                                
                                                                "bedTypeforRoom1Child3",                                                                
                                                                "bedTypeforRoom1Child4",                                                                
                                                                "bedTypeforRoom1Child5",
                                                                "bedTypeforRoom2Child1",                                                                
                                                                "bedTypeforRoom2Child2",                                                                
                                                                "bedTypeforRoom2Child3",                                                                
                                                                "bedTypeforRoom2Child4",                                                                
                                                                "bedTypeforRoom2Child5",
                                                                "bedTypeforRoom3Child1",                                                                
                                                                "bedTypeforRoom3Child2",                                                                
                                                                "bedTypeforRoom3Child3",                                                                
                                                                "bedTypeforRoom3Child4",                                                                
                                                                "bedTypeforRoom3Child5",
                                                                "bedTypeforRoom4Child1",                                                                
                                                                "bedTypeforRoom4Child2",                                                                
                                                                "bedTypeforRoom4Child3",                                                                
                                                                "bedTypeforRoom4Child4",                                                                
                                                                "bedTypeforRoom4Child5",
                                                                "rdoperNight",
                                                                "rdoperStay",
                                                                "LoginLink",
                                                                "btnLogIn",
                                                                "lnkBtnJoin",
                                                                "lnkBtnJoinHeader"
                                                            };

        private bool DisableViewState(ControlCollection controls)
        {
            bool flag = true;
            foreach (Control control in controls)
            {
                if (control.HasControls())
                    if (DisableViewState(control.Controls) && !viewstateAllowedControls.Contains(control.ID))
                        control.EnableViewState = false;
                    else
                    {
                        flag = false;
                    }
                else
                {
                    if (!viewstateAllowedControls.Contains(control.ID))
                        control.EnableViewState = false;
                    else
                    {
                        flag = false;
                    }
                }
            }
            return flag;
        }

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (DisableViewState(Controls))
                Page.EnableViewState = false;

            shoppingCart.Visible = OWSVisibilityControl.BookingModuleShouldBeVisible;
            AlternativeBookingModule.Visible = !OWSVisibilityControl.BookingModuleShouldBeVisible;
            shoppingCart.SetPageType(EpiServerPageConstants.SELECT_HOTEL_PAGE);
        }
    }
}