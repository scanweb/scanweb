﻿<%@ Page Language="C#" Inherits="Scandic.Scanweb.CMS.Templates.Reservation.PaymentProcessing"
    CodeBehind="PaymentProcessing.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageDefault.master" %>

<%@ Register TagPrefix="Booking" TagName="AboutOurRates" Src="~/Templates/Booking/Units/AboutOurRates.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Import Namespace="Scandic.Scanweb.CMS.DataAccessLayer" %>
<%@ Import Namespace="Scandic.Scanweb.Entity" %>
<asp:Content ID="contentMainBodyRegion" ContentPlaceHolderID="MainBodyRegion" runat="server">

    <script type="text/javascript">
	var isOperaAJAXCallResponseReceieved = false;
	var hiddenReservationNumber = '<%= ReservationNumberSessionWrapper.ReservationNumber %>';
	var checkReservationStatusFailureMessage = "<%=checkReservationStatusFailureMessage %>";
	var browserExitPaymentInfoMessage = "<%=browserExitPaymentInfoMessage%>";
	var promptBrowserExitInfoMessage = true;
	var isRedirectedConfirmationPage = false;
    $(document).ready(function(){       
        setTimeout(CheckReservationStatus,<%=GetTimeoutForPaymentProcessing%>);
        OnLoad();
    });
    function OnLoad(){
         var transactionId = '<%= Request.QueryString["transactionId"] %>';
         var responseCode = '<%= Request.QueryString["responseCode"] %>';
         if(responseCode.toUpperCase() == 'OK'){
            var paymentProcessingMsg = "<h1 class='loadingHeader'><%=paymentInProgressInfoMessage%></h1><br>" +
	                                   "<img src='/Templates/Booking/Styles/Default/Images/NetsTerminal/rotatingclockani.gif' alt='' />";
            BlockUI(paymentProcessingMsg);
             $.ajax({
                 url: "<%=GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") %>",
                 type: "GET",
                 data: { methodToCall: 'GetDynamicContent', transactionId: transactionId, responseCode: responseCode },
                 cache: false,
                 success: function(response) {
                     if((response != null) && (response != undefined) && (response != "")) {
                        promptBrowserExitInfoMessage = false;
                        isOperaAJAXCallResponseReceieved = true;
	                       window.location.replace(response);
                       } else {
                           if (!isRedirectedConfirmationPage) {
                               CheckReservationStatus();
                           }
                       }
                 },
                 error: function(msg) {
	                 promptBrowserExitInfoMessage = false;
                     if (!isRedirectedConfirmationPage) {
                         CheckReservationStatus();
                     }
                 }
             });
         }
         else
         {
            $('.loadingHeader').empty().html("You got an error. Payment can not be done at this point of time.");
         }
    }
    function BlockUI(infoMessage){
        $.blockUI({ 
                message: infoMessage,
                css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: 'white', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    color: '#fff',
                    left:'33%',
                    top:'35%' 
                }
         });
        $('.pointsInfo').css('margin-bottom','15px');
    }

    function CheckReservationStatus() {
        if (!isOperaAJAXCallResponseReceieved) {
            $.ajax({
                type: "GET",
                url: "<%=GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") %>",
                data: { methodToCall: 'CheckReservationStatus', reservationNumber: hiddenReservationNumber },
                success: function(response) {
                    if (!isOperaAJAXCallResponseReceieved) {
                        
                        if ((response != null) && (response != undefined) && (response != "") && (response.length > 0)) {
                            promptBrowserExitInfoMessage = false;
                            isRedirectedConfirmationPage = true;
                            window.location.replace(response);
                        } else {
                            var errorMsg = jQuery.validator.format(checkReservationStatusFailureMessage);
                            BlockUI('<h1 class="loadingHeader">' + errorMsg(hiddenReservationNumber) + "</h1>");
                        }
                    }
                },
                error: function (msg) {
		              if(!isOperaAJAXCallResponseReceieved) {
			              var errorMsg = jQuery.validator.format(checkReservationStatusFailureMessage);
                          BlockUI('<h1 class="loadingHeader">' + errorMsg(hiddenReservationNumber) + "</h1>");
		             }
                }
            });
        }
    }

    window.onbeforeunload = ShowExitPaymentInProgressPrompt;
    function ShowExitPaymentInProgressPrompt() {
	    var isEditOrPreviewMode = '<%=IsEditOrPreviewMode %>';
        if (isEditOrPreviewMode == 'False') {
            if (promptBrowserExitInfoMessage)
                return browserExitPaymentInfoMessage;
        }
    }
    </script>

    <br />
    <br />
    <br />
    <div id="content" style="text-align: center;">
    </div>
</asp:Content>
<asp:Content ID="contentSecondaryBodyRegion" ContentPlaceHolderID="SecondaryBodyRegion"
    runat="server">
    <Booking:AboutOurRates id="OurRates" runat="server">
    </Booking:AboutOurRates>
</asp:Content>
