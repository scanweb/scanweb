// <copyright file="ChildrenDetail.aspx.cs" company="Sapient">
// Copyright (c) 2008 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>06-April-2009</date>
// <version>1.0(Scanweb 1.6)</version>
// <summary>Contains code behind logic for the page ChildrenDetail.aspx</summary>

using System;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;

namespace Scandic.Scanweb.CMS.Templates.Reservation
{
    /// <summary>
    /// This page will host the booking kids control
    /// All the logic to capture the children's details
    /// is written in the BookingKids control. This page
    /// is used as a container to these controls.
    /// </summary>
    [AccessibleWhenSessionExpired(false, "ChildrenDetail")]
    public partial class ChildrenDetail : ScandicTemplatePage
    {
    }
}