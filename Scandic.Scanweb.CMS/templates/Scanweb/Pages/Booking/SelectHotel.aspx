<%@ Page language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Reservation.SelectHotel" Codebehind="SelectHotel.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageDefault.master" %>

<%@ Register TagPrefix="Booking" TagName="SelectHotel" Src="~/Templates/Booking/Units/SelectHotel.ascx" %>
<%--<%@ Register TagPrefix="Booking" TagName="Module" Src="~/Templates/Booking/Units/BookingModuleMedium.ascx" %>--%>
<%@ Register TagPrefix="Booking" TagName="ShoppingCart" Src="~/Templates/Booking/Units/ShoppingCart.ascx" %>
<%--<%@ Register TagPrefix="Scanweb" TagName="SelectHotelGoogleMapIframe" Src="~/Templates/Scanweb/Units/Static/Booking/SelectHotelGoogleMapIframe.ascx" %>--%>
<%@ Register TagPrefix="Scanweb" TagName="HotelPromoBox" Src="~/Templates/Scanweb/Units/Placeable/HotelPromoBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="BookingModuleAlternativeBox" Src="~/Templates/Scanweb/Units/Placeable/BookingModuleAlternativeBox.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
   <Booking:SelectHotel runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
<!--Reservation 2.0:Remove booking module from select hotel page---->
  <%--<Scanweb:SelectHotelGoogleMapIframe runat="server" />
  <Booking:Module runat="server" />--%>
  
  <Booking:ShoppingCart id="shoppingCart" runat="server" />
  <Scanweb:BookingModuleAlternativeBox CssClass="AlternativeBookingBoxSmall" ID="AlternativeBookingModule" runat="server" Visible="false" />
  
  <div>
    <scanweb:HotelPromoBox ID="Box1" OfferPageLinkPropertyName="BoxContainer1" CssClass="BoxLarge" ImagePropertyName ="BoxImageMedium" ImageMaxWidth="226" runat="server" />
  </div>
    <div>
    <scanweb:HotelPromoBox ID="Box2" OfferPageLinkPropertyName="BoxContainer2" CssClass="BoxLarge" ImagePropertyName ="BoxImageMedium" ImageMaxWidth="226" runat="server" />
  </div>
  <div>
    <scanweb:HotelPromoBox ID="Box3" OfferPageLinkPropertyName="BoxContainer3" CssClass="BoxLarge" ImagePropertyName ="BoxImageMedium" ImageMaxWidth="226" runat="server" />
  </div>
  <div>
    <scanweb:HotelPromoBox ID="Box4" OfferPageLinkPropertyName="BoxContainer4" CssClass="BoxLarge" ImagePropertyName ="BoxImageMedium" ImageMaxWidth="226" runat="server" />
  </div>
</asp:Content>

