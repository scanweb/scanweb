<%@ Page language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Preview" Codebehind="Preview.aspx.cs" %>
<%@ Register TagName="HotelMeetings" TagPrefix="Scanweb" Src="~/Templates/Scanweb/Units/Static/HotelLandingPage/HotelMeetings.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="Header"		Src="~/Templates/Scanweb/Units/Static/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head1" runat="server">
    <Scanweb:Header runat="server" />
</head>
<body>
    <form runat="server" id="form1">
    <asp:PlaceHolder ID="PreviewPH" runat="server">
   
      </asp:PlaceHolder> 
    </form>
</body>
</html>
