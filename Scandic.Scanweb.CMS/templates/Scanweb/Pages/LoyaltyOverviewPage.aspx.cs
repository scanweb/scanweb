//  Description					: LoyaltyOverviewPage                                     //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Templates.Pages
{
    /// <summary>
    /// LoyaltyOverviewPage
    /// </summary>
    public partial class LoyaltyOverviewPage : Scandic.Scanweb.CMS.Util.ScandicTemplatePage
    {
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool isLoggedIn = UserLoggedInSessionWrapper.UserLoggedIn;
            if (isLoggedIn)
            {
                Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.MYACCOUNT_BOOKING_PAGE),false);
            }
            else
            {
                bool removeBenefitsSection = false;

                if (CurrentPage["RemoveBenefitsandDisplayMainBody"] == null)
                {
                    BenefitsSection.Visible = true;
                    BindBenefitsData();
                }
                else
                {
                    NotLoggedInLoyaltyMainArea.Visible = true;
                }
                var imageWidth = Convert.ToInt32(AppConstants.ScandicFriendsLogoWidth);
                WebUtil.GetBenefitsImageUrl(CurrentPage, "ScandicFriendsLogo", ScandicsFriendsLogo, imageWidth);
            }
            SecondaryPageList.ShowItemLink = (CurrentPage["ShowItemLink"] != null);
            SecondaryPageList.ShowDate = (CurrentPage["ShowDate"] != null);
            SecondaryPageList.PreviewTextLength =
                CurrentPage["PreviewTextLength"] != null ? (int) CurrentPage["PreviewTextLength"] : 100;
        }

        private void BindBenefitsData()
        {
            var imageWidth = Convert.ToInt32(AppConstants.BenfitImageWidth);
            if (CurrentPage["BenefitsTitle"] != null)
                BenefitsTitle.InnerText = CurrentPage["BenefitsTitle"].ToString();

            if (CurrentPage["Benefit1"] != null)
                Benefit1.InnerHtml = CurrentPage["Benefit1"].ToString();
            if (CurrentPage["Benefit2"] != null)
                Benefit2.InnerHtml = CurrentPage["Benefit2"].ToString();
            if (CurrentPage["Benefit3"] != null)
                Benefit3.InnerHtml = CurrentPage["Benefit3"].ToString();

            if (CurrentPage["Benefit1Image"] != null)
            {
                divBenefit1.Visible = true;
                divBenefit1Image.Visible = true;
                WebUtil.GetBenefitsImageUrl(CurrentPage, "Benefit1Image", Benefit1Image, imageWidth);
            }
            if (CurrentPage["Benefit2Image"] != null)
            {
                divBenefit2.Visible = true;
                divBenefit2Image.Visible = true;
                WebUtil.GetBenefitsImageUrl(CurrentPage, "Benefit2Image", Benefit2Image, imageWidth);
            }
            if (CurrentPage["Benefit3Image"] != null)
            {
                divBenefit3.Visible = true;
                divBenefit3Image.Visible = true;
                WebUtil.GetBenefitsImageUrl(CurrentPage, "Benefit3Image", Benefit3Image, imageWidth);
            }

            if (CurrentPage["JoinNow"] != null)
               JoinNow.Attributes.Add("href", GetLinkUrl("JoinNow"));
            if (CurrentPage["LearnMore"] != null)
               LearnMore.Attributes.Add("href", GetLinkUrl("LearnMore"));
        }

        public string GetLinkUrl(string pageUrl)
        {
            string links = string.Empty;
            if (CurrentPage.LinkType != PageShortcutType.External)
            {
                var pageData = ContentDataAccessManager.GetPageData((PageReference)CurrentPage[pageUrl]);
                links = GetFriendlyUrlToPage(pageData.PageLink, pageData.LinkURL);
            }
            return links;
        }

        protected string GetFriendlyUrlToPage(PageReference pageLink, string linkUrl)
        {
            UrlBuilder url = new UrlBuilder(linkUrl);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, pageLink, System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }
    }
}