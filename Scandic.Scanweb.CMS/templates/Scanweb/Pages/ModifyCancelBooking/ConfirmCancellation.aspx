<%@ Page language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Pages.ConfirmCancellation" Codebehind="ConfirmCancellation.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageDefault.master" %>
<%@ Register TagPrefix="Scanweb" TagName="MainBody" Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="RightColumnWide" Src="~/Templates/Scanweb/Units/Placeable/RightColumnWide.ascx" %>
<%@ Register TagPrefix="ModifyBooking" TagName="CancelBookingConfirmation" Src="~/Templates/Booking/Units/CancelBookingConfirmation.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
    <Scanweb:MainBody ID="MainBody1" runat="server" />
    <ModifyBooking:CancelBookingConfirmation runat="server" />
</asp:Content>

<%--<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
        <Scanweb:RightColumnWide ID="RightColumnWide1" runat="server" />
</asp:Content>--%>