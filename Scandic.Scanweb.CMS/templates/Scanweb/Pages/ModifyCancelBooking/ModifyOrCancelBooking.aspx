<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages.ModifyOrCancelBooking"
    Codebehind="ModifyOrCancelBooking.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageDefault.master" %>

<%@ Register TagPrefix="Scanweb" TagName="MainBody" Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="RightColumnWide" Src="~/Templates/Scanweb/Units/Placeable/RightColumnWide.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>
<%--<%@ Register TagPrefix="ucSC" TagName="ModifyStay" Src="~/Templates/Booking/Units/ShoppingCartEditStayModule.ascx" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
    <%--  <Scanweb:SquaredCornerImage ID="SquaredCornerImage1" ImagePropertyName="ContentTopImage" 
                                 TopCssClass="RoundedCornersTop595" 
                                 ImageCssClass="RoundedCornersImage595" 
                                 BottomCssClass="RoundedCornersBottom595" ImageWidth="595" runat="server" />--%>
    <%-- <Scanweb:MainBody ID="MainBody1" runat="server" />--%>
    <%--   <div class="helpText">
   <p><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/CancelBookingHelpMessage")%></p></div> --%>
    <%--<div class="txtContnr fltLft">
	<p><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/CancelBookingHeader1")%>&lsquo;
	<%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/CancelBookingHeader2")%>&rsquo;
	<%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/CancelBookingHeader3")%>
	</p>
</div>	--%>
    <br class="clear" />
    <asp:PlaceHolder ID="ModifyOrCancelBookingPlaceHolder" runat="server" Visible="false">
    </asp:PlaceHolder>
    <%--<div class="guaranteTypeInfo">
	<h3><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformation")%></h3>
	<ul>
		<li><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformationLine1")%></li>
		<li><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformationLine2")%></li>
		<li><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformationLine3")%>
		<a href="#" title="www.scandichotels.com"><%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/ScandicSite")%></a>
		<%=WebUtil.GetTranslatedText("/bookingengine/booking/CancelBooking/GuaranteeInformationLine4")%></li>
	</ul>
</div>--%>
</asp:Content>
<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
    <div id="yourStayMod05">
        <div class="regular">        
            <%--<ucSC:ModifyStay ID="modifyStayCart" runat="server">
            </ucSC:ModifyStay>--%>
            <div ID="pHolderModifyStayCart" runat="server"></div>
            <div class="gradientft sprite" id="divGradient" runat="server"></div>
        </div>
    </div>
</asp:Content>
<%--<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
        <Scanweb:RightColumnWide runat="server" />
</asp:Content>--%>
