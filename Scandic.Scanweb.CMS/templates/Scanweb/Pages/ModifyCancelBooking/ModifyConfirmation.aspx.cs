//  Description					: ModifyConfirmation                                      //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;

namespace Scandic.Scanweb.CMS.Templates.Pages
{
    /// <summary>
    /// Code behind of ModifyConfirmation control.
    /// </summary>
    [AccessibleWhenSessionExpired(false, "ModifyConfirmation")]
    public partial class ModifyConfirmation : ScandicTemplatePage
    {
    }
}