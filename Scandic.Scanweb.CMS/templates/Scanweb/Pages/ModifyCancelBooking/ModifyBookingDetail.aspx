<%@ Page language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Pages.ModifyBookingDetail" Codebehind="ModifyBookingDetail.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageDefault.master" %>
<%@ Register TagPrefix="ModifyBooking" TagName="CancelledBooking" Src="~/Templates/Booking/Units/ModifyBookingDetails.ascx" %>


<%@ Register TagPrefix="Booking" TagName="ShoppingCart" Src="~/Templates/Booking/Units/ShoppingCart.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
    <br class="clear" />
    <ModifyBooking:CancelledBooking runat="server"></ModifyBooking:CancelledBooking>
</asp:Content>

<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
   <div id="yourStayMod05">
        <div class="regular">
            <Booking:ShoppingCart id="shoppingCart" runat="server"></Booking:ShoppingCart>
            <!-- vrushali:Res2.0 - Removed because it was added no need of this div and it was adding a line below shopping cart.
           <!--div class="gradientft sprite"></div-->
        </div>
    </div>
    <div class="alertwrapperrightcolunm" id="divSpAlertWrap" runat="server">
                <div class="splalertcontent" id="divSpAlert" runat="server">
                </div>
            </div>
</asp:Content>