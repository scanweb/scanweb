<%@ Page language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Pages.CancelledBooking" Codebehind="CancelledBooking.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageDefault.master" %>
<%@ Register TagPrefix="ModifyBooking" TagName="CancelledBooking" Src="~/Templates/Booking/Units/CancelledBooking.ascx" %>
<%--<%@ Register TagPrefix="ModifyBooking" TagName="Module" Src="~/Templates/Booking/Units/BookingModuleMedium.ascx" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
    <ModifyBooking:CancelledBooking runat="server" />
    <div id="UrlLink"> 
    <%--<a class="IconLink" href="<%=GetOfferURL()%>"><EPiServer:Translate ID="Translate2" Text="/Templates/Scanweb/Pages/CancelledBooking/LatestOffers" runat="server" /></a><br />
    <a class="IconLink" href="<%=GetHomePageURL()%>"><EPiServer:Translate ID="Translate1" Text="/Templates/Scanweb/Pages/CancelledBooking/HomePage" runat="server" /></a><br />--%>
    </div>
</asp:Content>

<%--<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
  <ModifyBooking:Module runat="server" />
</asp:Content>--%>