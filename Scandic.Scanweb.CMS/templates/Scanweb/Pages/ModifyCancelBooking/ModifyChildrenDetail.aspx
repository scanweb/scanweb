<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModifyChildrenDetail.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Pages.ModifyChildrenDetail" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageDefault.master"%>
<%@ Register TagPrefix="ModifyChildDetails" TagName="ModifyChildrenDetails" Src="~/Templates/Booking/Units/ModifyBookingKids.ascx" %>
<%@ Register TagPrefix="ModifyBooking" TagName="Module" Src="~/Templates/Booking/Units/ModifyBookingModuleMedium.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="PromoBoxOffer" Src="~/Templates/Scanweb/Units/Placeable/PromoBoxOffer.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
    <ModifyChildDetails:ModifyChildrenDetails ID="ChildrenDetails" runat="server" />
    <asp:Literal ID="ChildrenAttraction" runat="server"></asp:Literal>
</asp:Content>
  
<asp:Content ID="Content2" ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
  <ModifyBooking:Module ID="BookingModuleMedium" runat="server" />
   <div class="BoxContainer">
    <scanweb:PromoBoxOffer ID="Box4" OfferPageLinkPropertyName="BoxContainer1" CssClass="BoxLarge" ImagePropertyName ="BoxImageLarge" ImageMaxWidth="349" runat="server" HideBookNow="true"></scanweb:PromoBoxOffer>
  </div>
</asp:Content>


