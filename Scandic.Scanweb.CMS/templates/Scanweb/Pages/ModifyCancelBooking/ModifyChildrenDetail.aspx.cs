//  Description					:   ModifyChildrenDetail                                  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;

namespace Scandic.Scanweb.CMS.Templates.Pages
{
    /// <summary>
    /// ModifyChildrenDetail
    /// </summary>
    [AccessibleWhenSessionExpired(false, "ModifyChildrenDetail")]
    public partial class ModifyChildrenDetail : ScandicTemplatePage
    {
    }
}