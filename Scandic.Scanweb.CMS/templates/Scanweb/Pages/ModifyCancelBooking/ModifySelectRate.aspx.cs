//  Description					:   ModifySelectRate                                      //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;

namespace Scandic.Scanweb.CMS.Templates.Pages
{
    /// <summary>
    /// Code behind of ModifySelectRate page.
    /// </summary>
    [AccessibleWhenSessionExpired(false, "ModifySelectRate")]
    [AccessibleWhenSessionInValid(false)]
    public partial class ModifySelectRate : ScandicTemplatePage
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            shoppingCart.SetPageType(EpiServerPageConstants.MODIFY_CANCEL_SELECT_RATE);
            shoppingCart.SetRoomUnavailableMessage +=new EventHandler(shoppingCart_SetRoomUnavailableMessage);
        }
        void shoppingCart_SetRoomUnavailableMessage(object sender, EventArgs e)
        {
            SelectRate.RoomUnavailableErrorMessage = (string)sender;
        }
    }
}