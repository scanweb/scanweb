<%@ Page language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Pages.ModifyConfirmation" Codebehind="ModifyConfirmation.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageDefault.master" %>

<%@ Register TagPrefix="ModifyBooking" TagName="Confirmation" Src="~/Templates/Booking/Units/ModifyBookingConfirmation.ascx" %>
<%@ Register TagPrefix="ModifyBooking" TagName="Module" Src="~/Templates/Booking/Units/ModifyBookingModuleMedium.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
    <ModifyBooking:Confirmation runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
  <ModifyBooking:Module runat="server" />
</asp:Content>