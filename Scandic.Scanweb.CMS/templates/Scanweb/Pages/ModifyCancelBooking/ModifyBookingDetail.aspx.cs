//  Description					: ModifyBookingDetail                                     //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;

namespace Scandic.Scanweb.CMS.Templates.Pages
{
    /// <summary>
    /// ModifyBookingDetail
    /// </summary>
    [AccessibleWhenSessionExpired(false, "ModifyBookingDetail")]
    [AccessibleWhenSessionInValid(false)]
    public partial class ModifyBookingDetail : ScandicTemplatePage
    {
        /// <summary>
        /// Page Load Event Handler
        /// </summary>
        /// <param name="sender">
        /// Sender of the Event
        /// </param>
        /// <param name="e">
        /// Arguments for the Event
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            shoppingCart.SetPageType(EpiServerPageConstants.MODIFY_CANCEL_BOOKING_DETAILS);
            if (!IsPostBack && SearchCriteriaSessionWrapper.SearchCriteria != null 
                && !string.IsNullOrEmpty(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode))
            {
                string spAlert = ContentDataAccess.GetSpecialAlert(SearchCriteriaSessionWrapper.SearchCriteria.SelectedHotelCode, false);
                if (!string.IsNullOrEmpty(spAlert))
                {
                    divSpAlertWrap.Visible = true;
                    divSpAlert.InnerHtml = spAlert;
                }
                else
                {
                    divSpAlertWrap.Visible = false;
                }
            }
        }
    }
}