<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CancellBookingPrinterFreindly.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages.ModifyCancelBooking.CancellBookingPrinterFreindly" %>
<%@ Register TagPrefix="ModifyBooking" TagName="CancelledBooking" Src="~/Templates/Booking/Units/CancelledBooking.ascx" %>
<%@ Register TagName="ConfirmRoomInfo" TagPrefix="Booking" Src="~/Templates/Booking/Units/ConfirmationRoomInfoContainer.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="Header"	   Src="~/Templates/Scanweb/Units/Static/Header.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%--<%@ Register TagPrefix="ModifyBooking" TagName="RoomInfo" Src="~/Scandic.Scanweb.BookingEngine.Web/Templates/Booking/Units/ConfirmationRoomInfoContainer.ascx" %>--%>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <Scanweb:Header runat="server" />
            <ModifyBooking:CancelledBooking runat="server" />
        </div>
        <div class="tripleCol mgnLft20 flushMarginRight cancelFlowRoomContainer">
            <asp:PlaceHolder ID= "pHdrRoomInfo" runat="server"></asp:PlaceHolder>
        </div>
        
        <!-- BEGIN Help Text -->
        <div class="mgnLft20 cancelFlowRoomContainer pdfRoundedCorner">
            <div class="roomDetails expand" id="masterReservation" runat="server" visible="false">
                <!-- R2.2 | SJS | artf1162324 -->
                <div class="infoAlertBox" style="width: 720px; margin-left: 1px;">
                    <div class="threeCol alertContainer">
                        <div class="hd sprite">&#160;</div>
                        <div class="cnt " style="padding: 8px 10px; margin: 0px ! important;">
                            <asp:Label runat="server" ID="lblMasterReservation" CssClass="lblMasterReservation"></asp:Label>
                        </div>
                        <div class="ft sprite">&#160;</div>
                    </div>
                </div>
            </div>
            <div class="roomDetails expand" id="ContactUsText" runat="server" visible ="false">
                <!-- R2.2 | SJS | artf1162324 -->
                <div class="infoAlertBox" style="width: 720px;">
                    <div class="threeCol alertContainer">
                        <div class="hd sprite">&#160;</div>
                        <div class="cnt " style="padding: 8px 10px; margin: 0px ! important;">
                            <asp:Label runat="server" ID="lblContactUsText" CssClass="lblContactUsText"></asp:Label>
                        </div>
                        <div class="ft sprite">&#160;</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Help Text -->
    </form>
</body>
</html>