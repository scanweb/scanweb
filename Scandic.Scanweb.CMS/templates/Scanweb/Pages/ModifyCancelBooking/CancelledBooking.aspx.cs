//  Description					:   CancelledBooking                                      //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;

namespace Scandic.Scanweb.CMS.Templates.Pages
{
    /// <summary>
    /// This class contains members of cancelled booking.
    /// </summary>
    [AccessibleWhenSessionExpired(false, "CancelledBooking")]
    public partial class CancelledBooking : ScandicTemplatePage
    {
        /// <summary>
        /// Gets offer url
        /// </summary>
        /// <returns>offer url</returns>
        protected string GetOfferURL()
        {
            PageReference offerOverviewPageLink = ScandicUserControlBase.RootPage["OfferOverviewPage"] as PageReference;
            if (offerOverviewPageLink != null)
            {
                PageData offerOverviewPage = DataFactory.Instance.GetPage(offerOverviewPageLink,
                                                                          EPiServer.Security.AccessLevel.NoAccess);
                UrlBuilder url = new UrlBuilder(offerOverviewPage.LinkURL);
                EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, offerOverviewPage.PageLink,
                                                                      System.Text.UTF8Encoding.UTF8);
                return url.ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// Gets home page url.
        /// </summary>
        /// <returns>home page url.</returns>
        protected string GetHomePageURL()
        {
            PageData startPage = DataFactory.Instance.GetPage(PageReference.StartPage,
                                                              EPiServer.Security.AccessLevel.NoAccess);
            UrlBuilder url = new UrlBuilder(startPage.LinkURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, startPage.PageLink, System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }
    }
}