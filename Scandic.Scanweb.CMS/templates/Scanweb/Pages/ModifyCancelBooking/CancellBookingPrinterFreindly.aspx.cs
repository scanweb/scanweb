//  Description					: Code Behind class for CancelBookingPrinterFriendly Control
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                   	                  //
//  Author email id				:                           							  //
//  Creation Date				: 30th November  2010									  //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: Modified to display Room information in PDF			  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages.ModifyCancelBooking
{
    /// <summary>
    /// CancellBookingPrinterFreindly
    /// </summary>    
    public partial class CancellBookingPrinterFreindly : ScandicTemplatePage
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Boolean isPDF = false;
            Boolean allRoomsToBeShown = false;
            if (Request.QueryString["isPDF"] != null)
            {
                isPDF = Convert.ToBoolean(Request.QueryString["isPDF"]);
            }
            if (Request.QueryString["AllROOMSTOBESHOWN"] != null)
            {
                allRoomsToBeShown = Convert.ToBoolean(Request.QueryString["AllROOMSTOBESHOWN"]);
            }
            SortedList<int, Dictionary<string, string>> cancelledRoomData = PopulateRoomInfoData();
            PopulateRoomInfoContainer(cancelledRoomData, isPDF, allRoomsToBeShown);
            PopulateMasterReservationHelpText(isPDF, allRoomsToBeShown);
            PopulateContactUsText(isPDF);
        }

        /// <summary>
        /// This method builds the entity by collecting data from session variable to populate the Room Information
        /// </summary>
        /// <returns>collection  of data from session variable</returns>
        private SortedList<int, Dictionary<string, string>> PopulateRoomInfoData()
        {
            SortedList<int, Dictionary<string, string>> cancellationInfoList =
                new SortedList<int, Dictionary<string, string>>();
            List<CancelDetailsEntity> cancelDetailsList = BookingEngineSessionWrapper.AllCancelledDetails;
            if (cancelDetailsList != null && cancelDetailsList.Count > 0)
            {
                TextInfo tInfo = CultureInfo.CurrentCulture.TextInfo;
                for (int i = 0; i < cancelDetailsList.Count; i++)
                {
                    Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
                    CancelDetailsEntity cancelEntity = cancelDetailsList[i];
                    if (cancelEntity != null)
                    {
                        int currentRoomNumber = Convert.ToInt32(cancelEntity.LegNumber) - 1;
                        BookingDetailsEntity bookingDetails = Utility.GetBookingDetail(cancelEntity.LegNumber);
                        if (bookingDetails != null && (bookingDetails.HotelSearch != null))
                        {
                            HotelDestination hotelDestination =
                                ContentDataAccess.GetHotelByOperaID(bookingDetails.HotelSearch.SelectedHotelCode);
                            if (hotelDestination != null)
                            {
                                cancellationInfo.Add(CommunicationTemplateConstants.CITY_HOTEL, hotelDestination.Name);
                            }
                            IFormatProvider cultureInfo =
                                new CultureInfo(EPiServer.Globalization.ContentLanguage.SpecificCulture.Name);
                            string arrivalDate = WebUtil.GetDayFromDate(bookingDetails.HotelSearch.ArrivalDate) +
                                                 AppConstants.SPACE +
                                                 Utility.GetFormattedDate(bookingDetails.HotelSearch.ArrivalDate);
                            string departureDate = WebUtil.GetDayFromDate(bookingDetails.HotelSearch.DepartureDate) +
                                                   AppConstants.SPACE +
                                                   Utility.GetFormattedDate(bookingDetails.HotelSearch.DepartureDate);
                            cancellationInfo.Add(CommunicationTemplateConstants.ARRIVAL_DATE, arrivalDate);
                            cancellationInfo.Add(CommunicationTemplateConstants.DEPARTURE_DATE, departureDate);
                            cancellationInfo.Add(CommunicationTemplateConstants.NUMBER_OF_DAYS,
                                                 bookingDetails.HotelSearch.NoOfNights.ToString());
                            string currentNoOfRooms = ((BookingEngineSessionWrapper.AllCancelledDetails != null)
                                                           ? BookingEngineSessionWrapper.AllCancelledDetails.Count.ToString()
                                                           : string.Empty);
                            cancellationInfo.Add(CommunicationTemplateConstants.NUMBER_ROOM, currentNoOfRooms);
                            HotelRoomRateEntity hotelRoomRateEntity = bookingDetails.HotelRoomRate;
                            HotelSearchEntity hotelSearchEntity = bookingDetails.HotelSearch;
                            if (hotelRoomRateEntity != null)
                            {
                                double basePoints = 0;

                                basePoints = hotelRoomRateEntity.Points;
                                double totalPoints = 0;

                                basePoints = hotelRoomRateEntity.Points;
                                if (hotelRoomRateEntity.TotalRate != null)
                                {
                                    totalPoints = hotelRoomRateEntity.TotalRate.Rate;
                                }

                                string ratePlanCode = hotelRoomRateEntity.RatePlanCode;
                                string selectedRateCategoryName = string.Empty;
                                if ((ratePlanCode != null) && (ratePlanCode != string.Empty))
                                {
                                    Rate rate = RoomRateUtil.GetRate(ratePlanCode);
                                    if (rate != null && hotelSearchEntity.SearchingType != SearchType.CORPORATE)
                                    {
                                        cancellationInfo.Add(CommunicationTemplateConstants.RATE_CATEGORY,
                                                             rate.RateCategoryName);
                                    }
                                }

                                #region CorporateBooking
                                if (hotelSearchEntity.SearchingType == SearchType.CORPORATE)
                                {
                                    BaseRateDisplay rateDisplay = null;
                                    bool isTrue = false;

                                    RateCategory rateCategory = RoomRateUtil.GetRateCategoryByRatePlanCode(ratePlanCode);

                                    IList<BaseRoomRateDetails> listRoomRateDetails = HotelRoomRateSessionWrapper.ListHotelRoomRate;

                                    if (listRoomRateDetails != null && listRoomRateDetails.Count > 0)
                                    {
                                        if (Utility.IsBlockCodeBooking)
                                        {
                                            foreach (BaseRoomRateDetails roomDtls in listRoomRateDetails)
                                            {
                                                if (roomDtls != null)
                                                {
                                                    if (roomDtls.RateCategories != null &&
                                                        roomDtls.RateCategories.Count == 0)
                                                    {
                                                        rateDisplay =
                                                            new BlockCodeRoomRateDisplay(
                                                                roomDtls as BlockCodeRoomRateDetails);
                                                        foreach (
                                                            RateCategoryHeaderDisplay hdrDisplay in
                                                                rateDisplay.RateCategoriesDisplay)
                                                        {
                                                            if (
                                                                !cancellationInfo.ContainsKey(
                                                                    CommunicationTemplateConstants.RATE_CATEGORY))
                                                                cancellationInfo.Add(
                                                                    CommunicationTemplateConstants.RATE_CATEGORY,
                                                                    hdrDisplay.Title);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        foreach (RateCategory rateCtgry in roomDtls.RateCategories)
                                                        {
                                                            if (rateCategory != null && rateCategory.RateCategoryId == rateCtgry.RateCategoryId)
                                                            {
                                                                rateDisplay =
                                                                    new BlockCodeRoomRateDisplay(
                                                                        roomDtls as BlockCodeRoomRateDetails);
                                                                isTrue = true;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                                if (isTrue)
                                                {
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            foreach (BaseRoomRateDetails roomDtls in listRoomRateDetails)
                                            {
                                                if (roomDtls != null)
                                                {
                                                    foreach (RateCategory rateCtgry in roomDtls.RateCategories)
                                                    {
                                                        rateDisplay =
                                                            new NegotiatedRoomRateDisplay(
                                                                roomDtls as NegotiatedRoomRateDetails);
                                                        if (rateCategory != null && rateCategory.RateCategoryId == rateCtgry.RateCategoryId)
                                                        {
                                                            isTrue = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (isTrue)
                                                {
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (rateDisplay != null)
                                    {
                                        foreach (
                                            RateCategoryHeaderDisplay hdrDisplay in rateDisplay.RateCategoriesDisplay)
                                        {
                                            if (rateCategory != null && rateCategory.RateCategoryId != null &&
                                                rateCategory.RateCategoryId == hdrDisplay.Id)
                                            {
                                                if (
                                                    !cancellationInfo.ContainsKey(
                                                        CommunicationTemplateConstants.RATE_CATEGORY))
                                                    cancellationInfo.Add(CommunicationTemplateConstants.RATE_CATEGORY,
                                                                         hdrDisplay.Title);
                                            }
                                        }
                                    }
                                }

                                #endregion

                                string baseRateString = Utility.GetRoomRateString(selectedRateCategoryName,
                                                                                  hotelRoomRateEntity.Rate, basePoints,
                                                                                  bookingDetails.HotelSearch.
                                                                                      SearchingType,
                                                                                  bookingDetails.HotelSearch.ArrivalDate
                                                                                      .Year,
                                                                                  bookingDetails.HotelSearch.
                                                                                      HotelCountryCode,
                                                                                  AppConstants.PER_NIGHT,
                                                                                  AppConstants.PER_ROOM);

                                cancellationInfo.Add(CommunicationTemplateConstants.ROOM_RATE, baseRateString);
                                double dblTotalRate = Utility.CalculateTotalRateInCaseOfCancelFlow();
                                string totalRateString = string.Empty;
                                if (hotelRoomRateEntity.TotalRate != null)
                                {
                                    RateEntity tempRateEntity = new RateEntity(dblTotalRate,
                                                                               hotelRoomRateEntity.TotalRate.
                                                                                   CurrencyCode);
                                    totalRateString = Utility.GetRoomRateString(null, tempRateEntity, totalPoints,
                                                                                hotelSearchEntity.SearchingType,
                                                                                hotelSearchEntity.ArrivalDate.Year,
                                                                                hotelSearchEntity.HotelCountryCode,
                                                                                hotelSearchEntity.NoOfNights,
                                                                                hotelSearchEntity.RoomsPerNight);
                                }
                                if (bookingDetails.HotelSearch.SearchingType == SearchType.REDEMPTION)
                                {
                                    totalRateString = Utility.GetRoomRateString(null, null, basePoints,
                                                                                bookingDetails.HotelSearch.SearchingType,
                                                                                bookingDetails.HotelSearch.ArrivalDate.
                                                                                    Year,
                                                                                bookingDetails.HotelSearch.
                                                                                    HotelCountryCode,
                                                                                bookingDetails.HotelSearch.NoOfNights,
                                                                                bookingDetails.HotelSearch.RoomsPerNight);
                                }
                                cancellationInfo.Add(CommunicationTemplateConstants.TOTAL_PRICE, totalRateString);
                                string cancellationNumber = cancelEntity.CancelNumber;
                                cancellationInfo.Add(CommunicationTemplateConstants.CANCEL_CONFIRMATION_NUMBER,
                                                     cancellationNumber);
                                string reservationNumberWithLeg = cancelEntity.ReservationNumber + AppConstants.HYPHEN +
                                                                  cancelEntity.LegNumber;
                                cancellationInfo.Add(CommunicationTemplateConstants.CONFIRMATION_NUMBER,
                                                     reservationNumberWithLeg);
                                cancellationInfo.Add(CommunicationTemplateConstants.MAINRESERVATIONNUMBER,
                                                     cancelEntity.ReservationNumber);
                                cancellationInfo.Add(CommunicationTemplateConstants.LEGNUMBER, cancelEntity.LegNumber);
                                cancellationInfo.Add(CommunicationTemplateConstants.FIRST_NAME,
                                                     tInfo.ToTitleCase(bookingDetails.GuestInformation.FirstName));
                                cancellationInfo.Add(CommunicationTemplateConstants.LASTNAME,
                                                     tInfo.ToTitleCase(bookingDetails.GuestInformation.LastName));
                                cancellationInfo.Add(CommunicationTemplateConstants.TITLE,
                                                     tInfo.ToTitleCase(bookingDetails.GuestInformation.Title));
                                OrderedDictionary countryCodes = DropDownService.GetCountryCodes();
                                if (countryCodes != null)
                                {
                                    string country = countryCodes[bookingDetails.GuestInformation.Country] as string;
                                    cancellationInfo.Add(CommunicationTemplateConstants.COUNTRY, country);
                                }
                                cancellationInfo.Add(CommunicationTemplateConstants.CITY,
                                                     bookingDetails.GuestInformation.City);
                                cancellationInfo.Add(CommunicationTemplateConstants.EMAIL,
                                                     bookingDetails.GuestInformation.EmailDetails.EmailID);
                                cancellationInfo.Add(CommunicationTemplateConstants.TELEPHONE1,
                                                     bookingDetails.GuestInformation.Mobile.Number.ToString());
                                if (bookingDetails.GuestInformation != null &&
                                    bookingDetails.GuestInformation.GuestAccountNumber != null)
                                {
                                    cancellationInfo.Add(CommunicationTemplateConstants.MEMBERSHIPNUMBER,
                                                         bookingDetails.GuestInformation.GuestAccountNumber.ToString());
                                }
                            }

                            cancellationInfoList.Add(i, cancellationInfo);
                        }
                    }
                }
            }

            return cancellationInfoList;
        }

        /// <summary>
        /// This method populates the Roominfocaontainer to display in pdf
        /// </summary>
        /// <param name="cancelledRoomData"></param>
        /// <param name="isPDF"></param>
        /// <param name="allRoomsToBeShown"></param>
        private void PopulateRoomInfoContainer(SortedList<int, Dictionary<string, string>> cancelledRoomData,
                                               Boolean isPDF, Boolean allRoomsToBeShown)
        {
            int roomToBeDisplayed = 0;
            int legNumber = 0;
            const int MASTERRESERVATION = 1;

            if (Request.QueryString["CurrentLegNumber"] != null)
            {
                roomToBeDisplayed = Convert.ToInt32(Request.QueryString["CurrentLegNumber"]);
            }
            if (isPDF)
            {
                for (int roomIterator = 0; roomIterator < cancelledRoomData.Count; roomIterator++)
                {
                    Dictionary<string, string> cancellationRoomInfo = cancelledRoomData[roomIterator];
                    if (cancellationRoomInfo.ContainsKey(CommunicationTemplateConstants.LEGNUMBER))
                    {
                        legNumber = Convert.ToInt32(cancellationRoomInfo[CommunicationTemplateConstants.LEGNUMBER]);
                    }
                    if (allRoomsToBeShown)
                    {
                        if (legNumber == MASTERRESERVATION)
                        {
                            masterReservation.Visible = false;
                        }
                        else
                        {
                            masterReservation.Visible = true;
                            lblMasterReservation.Text = WebUtil.GetTranslatedText(
                                "/bookingengine/booking/contactus/ReservationInfo1")
                                                        + AppConstants.SPACE +
                                                        WebUtil.GetTranslatedText(
                                                            "/bookingengine/booking/contactus/ReservationInfo2");
                        }
                        ConfirmationRoomInfoContainer currentRoomInfoBox =
                            LoadControl(@"/Templates/Booking/Units/ConfirmationRoomInfoContainer.ascx") as
                            ConfirmationRoomInfoContainer;
                        currentRoomInfoBox.PopulateRoomInformation(cancellationRoomInfo);
                        pHdrRoomInfo.Controls.Add(currentRoomInfoBox);
                    }
                    else if (!allRoomsToBeShown && legNumber == (roomToBeDisplayed + 1))
                    {
                        ConfirmationRoomInfoContainer currentRoomInfoBox =
                            LoadControl(@"/Templates/Booking/Units/ConfirmationRoomInfoContainer.ascx") as
                            ConfirmationRoomInfoContainer;
                        currentRoomInfoBox.PopulateRoomInformation(cancellationRoomInfo);
                        pHdrRoomInfo.Controls.Add(currentRoomInfoBox);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// This method populates the PDf sent to master reservation with the help text whenever leg reservation is cancelled.
        /// </summary>
        /// <param name="isPDF"></param>
        /// <param name="allRoomsToBeShown"></param>
        private void PopulateMasterReservationHelpText(Boolean isPDF, Boolean allRoomsToBeShown)
        {  
            if (isPDF && allRoomsToBeShown)
            {
                bool isHelpMessageRequired = false;
                if (BookingEngineSessionWrapper.AllCancelledDetails != null)
                {
                    foreach (CancelDetailsEntity entity in BookingEngineSessionWrapper.AllCancelledDetails)
                    {
                        if (entity.LegNumber == "1")
                        {
                            isHelpMessageRequired = true;
                            break;
                        }
                    }
                    if (!isHelpMessageRequired)
                    {
                        masterReservation.Visible = true;
                        lblMasterReservation.Text =
                            WebUtil.GetTranslatedText("/bookingengine/booking/contactus/ReservationInfo1") +
                            AppConstants.SPACE +
                            WebUtil.GetTranslatedText("/bookingengine/booking/contactus/ReservationInfo2");
                    }
                    else
                    {
                        masterReservation.Visible = false;
                    }
                }
            }
        }

        /// <summary>
        /// This method populates the contact us helptext in PDF
        /// </summary>
        /// <param name="isPDF"></param>
        private void PopulateContactUsText(Boolean isPDF)
        {
            if (isPDF)
            {
                Dictionary<string, string> returnValue = null;
                string bookingType = "CancelBooking";
                returnValue = ContentDataAccess.GetConfirmationEmailTextFormCMS(bookingType);
                if (returnValue != null)
                {
                    ContactUsText.Visible = true;
                    string contactUsText = ((returnValue.ContainsKey("CONTACTUSTEXT"))
                                                ? returnValue["CONTACTUSTEXT"].ToString()
                                                : string.Empty);
                    if (!string.IsNullOrEmpty(contactUsText))
                    {
                        lblContactUsText.Text = contactUsText;
                    }
                    else
                    {
                        ContactUsText.Visible = false;
                    }
                }
                else
                {
                    ContactUsText.Visible = false;
                }
            }
        }
    }
}