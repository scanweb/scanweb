//  Description					: ConfirmCancellation                                     //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;

namespace Scandic.Scanweb.CMS.Templates.Pages
{
    /// <summary>
    /// Code behind of ConfirmCancellation page.
    /// </summary>
    [AccessibleWhenSessionExpired(false, "ConfirmCancellation")]
    public partial class ConfirmCancellation : ScandicTemplatePage
    {
    }
}