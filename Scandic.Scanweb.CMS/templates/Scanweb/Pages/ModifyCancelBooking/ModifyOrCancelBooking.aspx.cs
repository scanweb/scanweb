using System;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages
{
    /// <summary>
    /// ModifyOrCancelBooking
    /// </summary>
    [AccessibleWhenSessionExpired(false, "ModifyCancelBooking")]
    public partial class ModifyOrCancelBooking : ScandicTemplatePage
    {
        private bool ratePlanCodeDoesnotExists = false;

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ModifyCancelBookingControls();
            ShowModifyShoppingCart();
        }

        /// <summary>
        /// ShowModifyShoppingCart
        /// </summary>
        private void ShowModifyShoppingCart()
        {
            Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ShoppingCartEditStayModule controlShoppingCart =
                new Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ShoppingCartEditStayModule();
            controlShoppingCart =
                LoadControl("~/Templates/Booking/Units/ShoppingCartEditStayModule.ascx") as
                Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ShoppingCartEditStayModule;
            pHolderModifyStayCart.Controls.Add(controlShoppingCart);
            controlShoppingCart.PageType = EpiServerPageConstants.MODIFY_CANCEL_BOOKING_SEARCH;
            bool canModifyBooking = CanModifyBooking();
            divGradient.Visible = canModifyBooking;
            pHolderModifyStayCart.Visible = canModifyBooking;
            if (BookingEngineSessionWrapper.IsModifyingLegBooking)
            {
                divGradient.Attributes.Add("style", "display:none");
                pHolderModifyStayCart.Attributes.Add("style", "display:none");
            }
        }

        /// <summary>
        /// Indicates if the Booking can be modified. NonCancellable/Early/Already Cancelled bookings cant be modified
        /// </summary>
        /// <returns>True/False</returns>
        private bool CanModifyBooking()
        {
            bool canModify = true;
            List<BookingDetailsEntity> activeBookings = BookingEngineSessionWrapper.ActiveBookingDetails;
            bool hasNonCancellableReservation = false;
            System.Collections.Hashtable htblModifyRate = new System.Collections.Hashtable();
            System.Collections.Hashtable htblCancelPolicy = new System.Collections.Hashtable();
            int counter = 0;
            int cntrCombo = 0;
            foreach (BookingDetailsEntity booking in activeBookings)
            {
                if (booking.HotelSearch.SearchingType == SearchType.REDEMPTION)
                {
                    canModify = false;
                    break;
                }
                if (booking.HotelSearch.CancelPolicy != null)
                {
                    if (booking.HotelSearch != null &&
                        booking.HotelSearch.CancelPolicy == AppConstants.NON_CANCELLABLE_RESERVATION)
                        htblCancelPolicy.Add(counter, true);
                    else
                        htblCancelPolicy.Add(counter, false);
                }

                if (booking.HotelRoomRate != null && booking.HotelSearch != null)
                {
                    canModify = CheckBookingStatus(booking.HotelRoomRate.RatePlanCode, booking.HotelSearch);
                    htblModifyRate.Add(counter, canModify);

                    if (!canModify)
                        cntrCombo++;
                }
                counter++;
            }

            if (htblModifyRate != null && htblModifyRate.Count > 0 && htblModifyRate.ContainsValue(true))
                canModify = true;

            if (htblCancelPolicy != null && htblCancelPolicy.Count > 0 && htblCancelPolicy.ContainsValue(false))
                canModify = true;

            if ((activeBookings.Count < 1)
                ||
                (activeBookings[0].HotelSearch.ArrivalDate < DateTime.Today)
                )
            {
                canModify = false;
            }

            Rate rate = RoomRateUtil.GetRate
                (activeBookings.Count > 0 ? activeBookings[0].HotelRoomRate.RatePlanCode : null);
            if (rate == null)
            {
                canModify = false;
                ratePlanCodeDoesnotExists = true;
            }
            

            if (activeBookings!= null && activeBookings.Count > 0)
            {
                if (Utility.IsBlockCodeBooking)
                {
                    string bookingStatus = string.Empty;
                    canModify = Utility.BlockCodeBooking(canModify, activeBookings[0], out bookingStatus);
                }
                foreach (BookingDetailsEntity bkng in activeBookings)
                {
                    if (bkng.HotelRoomRate != null && bkng.HotelRoomRate.IsSessionBooking == true)
                    {
                        canModify = false;
                        break;
                    }
                }
            }

            return canModify;
        }

        /// <summary>
        /// CheckBookingStatus
        /// </summary>
        /// <param name="ratePlanCode"></param>
        /// <param name="hotelSearchEntity"></param>
        /// <returns>True/False</returns>
        private bool CheckBookingStatus(string ratePlanCode, HotelSearchEntity hotelSearchEntity)
        {
            bool canModify = true;
            if (!string.IsNullOrEmpty(ratePlanCode))
            {
                Rate rate = RoomRateUtil.GetRate(ratePlanCode);
                if (rate != null)
                {
                    if (!rate.IsModifiable)
                    {
                        canModify = false;
                    }
                }
            }
            return canModify;
        }

        ///<summary>
        /// Event Handler for Cancel Button Click
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="args">Arguments for the event</param>
        protected void btnCancelBooking_Click(object sender, EventArgs args)
        {
            AppLogger.LogInfoMessage("CancelBooking_Click");
            Response.Redirect(GlobalUtil.GetUrlToPage(EpiServerPageConstants.CONFIRM_CANCELLATION), false);
        }

        /// <summary>
        /// ModifyCancelBookingControls
        /// </summary>
        private void ModifyCancelBookingControls()
        {
            Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ModifyBookingDates controlModifyCancelBooking
                = LoadControl("~\\Templates\\Booking\\Units\\ModifyBookingDates.ascx") as
                  Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units.ModifyBookingDates;
            if (controlModifyCancelBooking != null)
            {
                ModifyOrCancelBookingPlaceHolder.Controls.Add(controlModifyCancelBooking);
                ModifyOrCancelBookingPlaceHolder.Visible = true;
                controlModifyCancelBooking.CanModifyContactDetails = CanModifyBooking();
                controlModifyCancelBooking.RatePlanCodeDoesnotExists = ratePlanCodeDoesnotExists;
            }
        }
    }
}