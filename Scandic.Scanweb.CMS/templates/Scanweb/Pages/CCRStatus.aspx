<%@ Page Language="C#" AutoEventWireup="False" CodeBehind="CCRStatus.aspx.cs" MasterPageFile="~/Templates/Public/MasterPages/MasterPage.master" Inherits="EPiServer.Templates.Public.Pages.Page" %>
<%@ Register TagPrefix="public" TagName="MainBody"      Src="~/Templates/Public/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="public" TagName="PageList"		Src="~/Templates/Public/Units/Placeable/PageList.ascx" %>

<asp:Content ContentPlaceHolderID="MainBodyRegion" runat="server">

	<div id="MainBody">
	    <public:MainBody runat="server" />
	    
        <hr />
    
        <public:PageList
            PreviewTextLength="200"
            PageLinkProperty="MainListRoot" 
            runat="server" />
	    
    </div>

</asp:Content>

<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">

	<div id="SecondaryBody">
		<EPiServer:Property PropertyName="SecondaryBody" EnableViewState="false" runat="server" />
		
		<public:PageList
		PageLinkProperty="SecondaryListRoot"
		DateProperty="PageSaved"
		runat="server" />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" /></div>
	
</asp:Content>