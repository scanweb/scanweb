<%@ Page Language="C#" AutoEventWireup="true" Codebehind="HotelPromotionalPage.aspx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.HotelPromotionalPage" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPagePromotionalPage.Master"%>

<asp:content contentplaceholderid="MainBodyRegion" runat="server">
    <input type="hidden" runat="server" value="" id="XMLFilePath" />
    <input type="hidden" runat="server" value="" id="FlashFilePath" />
    <input type="hidden" runat="server" value="" id="SelectedLanguage" />
    <div id="flashHolderLayer" style="width:100%;height:100%">
    <div id="flashcontent" align="center" style="height:100%"></div>
    </div>
     
    <script type="text/javascript" language="javascript">
     //function to return the object of the passed id.
	function $fn(element) {
		element = document.getElementById(element);
		return element;
	}
   
    function showFlash() {
			//Flashvars passed into the swf
			var flashvars = {
				language:  $fn(_endsWith('SelectedLanguage')).value, // get domain as default language (i.e. se, dk, no, fi, com)
				assetPath: $fn(_endsWith('XMLFilePath')).value///'/Global/global.xml' // path to assets for promo site
			};
			
			// override with custom language
			var params = location.search.substring(1).split("&");
			for (var i=0; i<params.length; i++) {
				var keyValue = params[i].split("=");
				if (keyValue[0] == 'lang' && keyValue[1]) {flashvars.language = keyValue[1];break;}
			}
			
			//Params for the swf object
			var params = {
				menu: "false",
				scale: "noScale",
				allowScriptAccess: "always",
				allowfullscreen: "true",
				bgcolor: "#FFFFFF",
				wmode:"opaque"			 
			};
	
			//Attributes for the swf object
			var attributes = {
				id: 'swfobjId',
				name: 'swfobjName'
			};
			
			//Object embedding
			swfobject.embedSWF(
				$fn(_endsWith('FlashFilePath')).value, // path to swf file
				'flashcontent', // id for div that wraps flash content
				'100%',
				'100%',
				'9.0.115',
				null,
				flashvars,
				params,
				attributes);
			
			//Scale flash content to always fit the browser
			swffit.fit("swfobjId", 1000, 650);
		}
    </script>
    <script type="text/javascript">showFlash()</script>
 </asp:content>
