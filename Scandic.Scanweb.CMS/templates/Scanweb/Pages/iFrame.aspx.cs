//  Description					: iFrame                                                  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages
{
    /// <summary>
    /// Code behind of iFrame page
    /// </summary>
    public partial class iFrame : ScandicTemplatePage
    {
        /// <summary>
        /// Page_Load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (CurrentPage.Property["iFrameUrl"].IsNull || CurrentPage.Property["iFrameUrl"].ToString() == "")
                {
                    IframePage.Visible = false;
                }
                else if (!CurrentPage.Property["iFrameUrl"].ToString().StartsWith("http://"))
                {
                    getUrlWithOrWithoutHttp(false, CurrentPage.Property["iFrameUrl"].ToString());
                }
                else
                    getUrlWithOrWithoutHttp(true, CurrentPage.Property["iFrameUrl"].ToString());

                if (!CurrentPage.Property["DisableiFrameScrollbar"].IsNull)
                    IframePage.Attributes["scrolling"] = "no";  //scroll = "no";   //scrolling="<%=scroll %>"
                else
                    IframePage.Attributes["scrolling"] = "auto";
            }
        }

        /// <summary>
        /// Check if the Right Column is displayed
        /// </summary>
        /// <returns>True/False</returns>
        public bool CheckRightColumn()
        {
            return CMSSessionWrapper.RightColumnPresent;
        }

        /// <summary>
        /// getUrlWithOrWithoutHttp
        /// </summary>
        /// <param name="exists"></param>
        /// <param name="url"></param>
        public void getUrlWithOrWithoutHttp(bool exists, string url)
        {
            if (exists == false)
            {
                url = url.Insert(0, "http://");
            }
            IframePage.Attributes["SRC"] = url.ToString();

            if (CurrentPage.Property["iFrameHeight"].IsNull || CurrentPage.Property["iFrameHeight"].ToString() == "")
                IframePage.Attributes["height"] = "100%";
            else
                IframePage.Attributes["height"] = CurrentPage.Property["iFrameHeight"].ToString();
        }
    }
}