<%@ Page language="c#" Inherits="Scandic.Scanweb.CMS.Templates.GuestProgramPage" Codebehind="GuestProgramPage.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWide.master"%>
<%@ Register TagPrefix="Scanweb" TagName="MainBody" 	        Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="InfoBox"              Src="~/Templates/Scanweb/Units/Static/InfoBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="Box"                  Src="~/Templates/Scanweb/Units/Placeable/Box.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="RoundedCornersImage"              Src="~/Templates/Scanweb/Units/Placeable/RoundedCornersImage.ascx" %>
<%@ Register TagPrefix="Booking" TagName="Module" 	            Src="~/Templates/Booking/Units/BookingModuleSmall.ascx" %>

<asp:Content ContentPlaceHolderID="MainBodyLeftRegion" runat="server">

<Scanweb:RoundedCornersImage ID="RoundedCornersImage1" ImagePropertyName="ContentTopImage" 
                                 TopCssClass="RoundedCornersTop472" 
                                 ImageCssClass="RoundedCornersImage472" 
                                 BottomCssClass="RoundedCornersBottom472" ImageWidth="472" runat="server" />
     <Scanweb:MainBody runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="MainBodyRightRegion" runat="server">
    <div>
        <div class="BoxContainer"><Booking:Module ID="BE" runat="server" /></div>
        <Scanweb:InfoBox runat="server" ID="SubscribeBox" HeaderPropertyName="NewsLetterBoxHeader" BodyPropertyName="NewsLetterBoxBody" />
        <div class="BoxContainer"><Scanweb:Box ID="Box1" PageLinkPropertyName="BoxContainer1" CssClass="BoxMedium" ImageMaxWidth="226" ImagePropertyName="BoxImageMedium" runat="server"/></div>
        <div class="BoxContainer"><Scanweb:Box ID="Box2" PageLinkPropertyName="BoxContainer2" CssClass="BoxMedium" ImageMaxWidth="226" ImagePropertyName="BoxImageMedium" runat="server"/></div>
        <div class="BoxContainer"><Scanweb:Box ID="Box3" PageLinkPropertyName="BoxContainer3" CssClass="BoxMedium" ImageMaxWidth="226" ImagePropertyName="BoxImageMedium" runat="server"/></div>
        <div class="BoxContainer"><Scanweb:Box ID="Box4" PageLinkPropertyName="BoxContainer4" CssClass="BoxMedium" ImageMaxWidth="226" ImagePropertyName="BoxImageMedium" runat="server"/></div>       
    </div>    
</asp:Content>