//  Description					:   Preview                                               //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Code behind of Preview page.
    /// </summary>
    public partial class Preview : EPiServer.TemplatePage
    {
        /// <summary>
        /// Gets/Sets HLPContent
        /// </summary>
        public Control HLPContent { get; set; }

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            switch (CurrentPage.PageTypeID)
            {
                case 36:
                    HLPContent =
                        LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\PreviewPage\\Facility.ascx");
                    PreviewPH.Controls.Add(HLPContent);
                    break;
                case 37:
                    HLPContent =
                        LoadControl(
                            "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\PreviewPage\\MeetingRoom.ascx");
                    PreviewPH.Controls.Add(HLPContent);
                    break;
                case 35:
                    HLPContent =
                        LoadControl(
                            "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\PreviewPage\\TransportOptions.ascx");
                    PreviewPH.Controls.Add(HLPContent);
                    break;
                case 32:
                    HLPContent =
                        LoadControl(
                            "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\PreviewPage\\LocalAttractions.ascx");
                    PreviewPH.Controls.Add(HLPContent);
                    break;
                case 34:
                    HLPContent =
                        LoadControl(
                            "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\PreviewPage\\RoomDescription.ascx");
                    PreviewPH.Controls.Add(HLPContent);
                    break;
                default:
                    HLPContent =
                        LoadControl(
                            "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\PreviewPage\\AdditionalInformation.ascx");
                    PreviewPH.Controls.Add(HLPContent);
                    break;
            }
        }
    }
}