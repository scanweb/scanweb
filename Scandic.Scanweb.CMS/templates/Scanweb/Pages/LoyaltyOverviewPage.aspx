<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Pages.LoyaltyOverviewPage"
    MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWideFull.master" Codebehind="LoyaltyOverviewPage.aspx.cs"
    EnableViewState="true" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>

<%@ Register TagPrefix="Scanweb" TagName="MainBody" Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="PageList" Src="~/Templates/Scanweb/Units/Placeable/PageList.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="FGPLogin" Src="~/Templates/Booking/Units/LoginError.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="ComponentsBox" Src="~/Templates/Scanweb/Units/Placeable/ComponentsBox.ascx" %>

<asp:Content ContentPlaceHolderID="MainBodyRegion" runat="server">
    <div id="OverviewPageTopArea">
        <div id="OverviewPageMainArea">
            <asp:PlaceHolder ID="NotLoggedInLoyaltyMainArea" runat="server" Visible="False">
                <Scanweb:MainBody ID="MainBody1" runat="server" />
            </asp:PlaceHolder>
        </div>
        <!-- BenefitsSection starts Here !-->
        <div id="BenefitsSection" runat="server" Visible="False" class="benefitSection">
             <h3 runat="server" id="BenefitsTitle" class="benefit"></h3>
                <div runat="server" id="BenefitText" class="benefitsTextplaceholder" >
				<div class="benefitsectionLeft">
                    <div class="benefitimage" runat="server" id="divBenefit1Image" visible="false">
					<asp:Image ID="Benefit1Image" runat="server" CssClass="benefitImage"/>    
					</div>
                    <div class="benefitcontainer" runat="server" id="divBenefit1" visible="false">
                    <div runat="server" id="Benefit1" class="benefittext" ></div>
                    </div>
                    <div class="benefitimage" runat="server" id="divBenefit2Image" visible="false">
					<asp:Image ID="Benefit2Image" runat="server" CssClass="benefitImage"/>    
					</div>
					  <div class="benefitcontainer" runat="server" id="divBenefit2" visible="false">
                   <div runat="server" id="Benefit2" class="benefittext"></div>
                   </div>
					<div class="benefitimage" runat="server" id="divBenefit3Image" visible="false">
					 <asp:Image ID="Benefit3Image" runat="server" CssClass="benefitImage"/>    
					</div>
					  <div class="benefitcontainer" runat="server" id="divBenefit3" visible="false">
                     <div runat="server" id="Benefit3" class="benefittext"></div>
                     </div>
					</div>
                    <div runat="server" id="BenefitJoinNow" class="benefitsJoinNowplaceholder" >
					<asp:Image ID="ScandicsFriendsLogo" runat="server" Width="213px" Height="150px"/>    
                    <div class="actionBtn fltRt">
                            <a runat="server" id="JoinNow" class="buttonInner joinLink" ><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/JoinNow") %></a>
                            <a href="" class="buttonRt scansprite" id=""></a>                            
                     </div>
					 <div class="learnMore">
					 <a id="LearnMore" class="fltRt" runat="server"><%= WebUtil.GetTranslatedText("/bookingengine/booking/loyaltylogin/LearnMore") %></a>
					 </div>
                    </div>
                </div>
            <div class="clear"></div>
      
        </div>
		<!-- BenefitsSection Ends Here !-->
        <div id="MemberLogin" class="memberLoginsection">
            <Scanweb:FGPLogin runat="server" ID="fgpLogin">
            </Scanweb:FGPLogin>
        </div>
        <div id="components">
            <Scanweb:ComponentsBox runat="server" ID="componentBox">
            </Scanweb:ComponentsBox>
        </div>
        
        <div id="OverviewPageMainRightArea">
            <div id="OverviewPageSecondaryLeftArea">
            <Scanweb:PageList ID="SecondaryPageList" PageLinkProperty="ListingContainer" 
                MaxCountProperty="MaxCount" ShowHeading="false" ThumbnailProperty="ThunbnailImage2"
                runat="server" />
        </div>
        </div>
    </div>
</asp:Content>
