<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.OfferList" CodeBehind="OfferList.aspx.cs"
    MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageAllOffers.master" %>

<%@ Register TagPrefix="Scanweb" TagName="RightColumnWide" Src="~/Templates/Scanweb/Units/Placeable/RightColumnWide.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="BookingModuleMedium" Src="~/Templates/Booking/Units/BookingModuleMedium.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="BookingModuleAlternativeBox" Src="~/Templates/Scanweb/Units/Placeable/BookingModuleAlternativeBox.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
    <div class="filter-wrapper" id="fillterWrapper" runat="server" >
        <div class="filter-icon">
        </div>
        <div class="filter-txt">
            <episerver:translate text="/Templates/Scanweb/Pages/OfferList/FilterText" runat="server" />
        </div>
        <div class="filters">
        </div>
    </div>
    <div style="border-bottom: 1px solid rgb(204, 204, 204); clear: both; overflow: hidden;
        margin-bottom: 20px; padding-bottom: 20px;" id="fillterButtons" runat="server">
        <h2 class="offerFilterBtn offerFilterActiveBtn" data-id="DummyContainer" id="showAllOffer"
            runat="server">
        </h2>
        <asp:Repeater ID="OfferCategory" runat="server">
            <ItemTemplate>
                <h2 class="offerFilterBtn" id="offerButton" runat="server">
                </h2>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div id="categorySelectionText" runat="server">
        <episerver:translate text="/Templates/Scanweb/Pages/OfferList/ResultText" runat="server" />
        <strong><span id="categoryTextcontainer"></span></strong>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.offerFilterBtn').bind('click', UpdateFilter);
            var selectedCategory = $('.offerFilterActiveBtn').text();
            $('#categoryTextcontainer').text(selectedCategory);
            function UpdateFilter(event) {
                var that = $(this);
                //var tragetID = $(this).attr("data-id").split(',');		
                $('.OfferCategoryListingItem').hide();

                if (that.attr("data-id") != "DummyContainer") {
                    $("h2[id$='showAllOffer']").removeClass('offerFilterActiveBtn');
                }

                if (that.hasClass('offerFilterActiveBtn')) {
                    that.removeClass('offerFilterActiveBtn');

                }
                else {
                    that.addClass('offerFilterActiveBtn');
                }

                var tragetElement = $('.offerFilterActiveBtn');

                $.each(tragetElement, function(index, value) {
                    var targetID = $(this).attr("data-id").split(',');

                    $.each(targetID, function(index, value) {
                        var tElement = $("div[data-id*='" + value + "']");
                        tElement.show();
                    });

                });

                if ((!$('.offerFilterBtn').hasClass('offerFilterActiveBtn')) || that.attr("data-id") == "DummyContainer") {
                    $('.offerFilterBtn').removeClass('offerFilterActiveBtn');
                    $("h2[id$='showAllOffer']").addClass('offerFilterActiveBtn');
                    $('.OfferCategoryListingItem').show();
                }

                var selectedCategorylist = $(".offerFilterActiveBtn").map(function() {
                    return $(this).text();
                }).get().join(", ");
                $('#categoryTextcontainer').text(selectedCategorylist);

                setOfferNo();
            }
            setOfferNo();
        });

        function setOfferNo() {
            var allOfferBox = $('.OfferCategoryListingItem');
            var offerBoxVisible = $('.OfferCategoryListingItem:visible');
            var i = 0;
            $(allOfferBox).each(function(item) {
                this.removeAttribute('name');
            });
            $(offerBoxVisible).each(function(item) {
                i++;
                this.setAttribute('name', 'Banner' + i);
            });
        }

        function trackPromobox(obj, prop44Value, prop50Value) {
            var prop54Value = $(obj).attr('name');
            var sfunc = s_gi(s_account);
            sfunc.linkTrackVars = 'prop44,prop54,prop50';
            sfunc.prop44 = prop44Value;
            sfunc.prop54 = prop54Value;
            sfunc.prop50 = prop50Value;
            sfunc.tl(this, 'o', 'scanweb promo list');
            return true;
        }

        function setSelectedCategoryText() {
            var selectedCategorylist = $(".offerFilterActiveBtn").map(function() {
                return $(this).text();
            }).get().join('|');
            return selectedCategorylist;
        }
        
    </script>

    <asp:PlaceHolder ID="OfferCategoryPlaceHolder" runat="server"></asp:PlaceHolder>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
       <Scanweb:RightColumnWide runat="server" />
</asp:Content>
