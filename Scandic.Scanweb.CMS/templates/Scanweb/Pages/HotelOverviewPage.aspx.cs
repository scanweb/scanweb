﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using EPiServer.SpecializedProperties;
using Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using EPiServer.Core;
using Scandic.Scanweb.CMS.DataAccessLayer;
using EPiServer;
using Scandic.Scanweb.BookingEngine.Controller;
using System.Linq;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using Scandic.Scanweb.CMS.Templates.MasterPages;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages
{
    public partial class HotelOverviewPage : ScandicTemplatePage
    {
        protected string ajaxCallPath = "";
        protected bool showOnMap = false;
        protected string mapHotelValue = string.Empty;
        AvailabilityController _availabilityController;
        public string AjaxCallPath
        {
            get { return string.Format("/{0}", ajaxCallPath); }
            set { ajaxCallPath = value; }
        }
        private static LinkItemCollection linksCollection = null;


        private string GetFilteredDestination(Destination filterType, string inputCriteria, List<Scandic.Scanweb.Core.CountryDestinatination> countryCityDestination)
        {
            string filteredPageURL = string.Empty;

            switch (filterType)
            {
                case Destination.Country:
                    var destination1 = countryCityDestination.Find(obj => string.Equals(obj.Name, inputCriteria, StringComparison.InvariantCultureIgnoreCase));
                    if (destination1 != null)
                        filteredPageURL = destination1.CountryLinkUrl;
                    break;
                case Destination.City:
                    Scandic.Scanweb.Core.CityDestination destination2 = (from cn in countryCityDestination
                                                                         from ct in cn.SearchedCities
                                                                         where string.Equals(ct.Name, inputCriteria, StringComparison.InvariantCultureIgnoreCase)
                                                                         select ct).FirstOrDefault();
                    if (destination2 != null)
                        filteredPageURL = destination2.CityLinkUrl;
                    break;
                case Destination.Hotel:
                    Scandic.Scanweb.Core.HotelDestination destination3 = (from cn in countryCityDestination
                                                                          from ct in cn.SearchedCities
                                                                          from ht in ct.SearchedHotels
                                                                          where string.Equals(ht.Name, inputCriteria, StringComparison.InvariantCultureIgnoreCase)
                                                                          select ht).FirstOrDefault();
                    if (destination3 != null)
                        filteredPageURL = destination3.HotelPageURL;
                    break;
            }
            return filteredPageURL;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string mapId = Request.QueryString["MapID"];
            string filteredPageURL = string.Empty;
            FindAHotelSessionVariablesSessionWrapper.SelectedNodeForMap = null;

            if (!string.IsNullOrEmpty(mapId))
            {
                string inputCriteria = mapId;
                _availabilityController = new AvailabilityController();
                var countryCityDestination = _availabilityController.FetchAllCountryDestinationsForAutoSuggest(false, false);

                if (countryCityDestination != null && countryCityDestination.Count > 0)
                {
                    filteredPageURL = GetFilteredDestination(Destination.Country, inputCriteria, countryCityDestination);
                    if (string.IsNullOrEmpty(filteredPageURL))
                    {
                        filteredPageURL = GetFilteredDestination(Destination.City, inputCriteria, countryCityDestination);
                    }
                    if (string.IsNullOrEmpty(filteredPageURL))
                    {
                        filteredPageURL = GetFilteredDestination(Destination.Hotel, inputCriteria, countryCityDestination);
                    }

                    if (!string.IsNullOrEmpty(filteredPageURL))
                    {
                        NameValueCollection nVc = new NameValueCollection();
                        nVc.Add("initialview", "map");

                        string url = WebUtil.GetCompletePageUrl(filteredPageURL, false);
                        //Response.Redirect(string.Format("{0}/?view=map", url));
                        Redirect(url, nVc);
                    }
                }
            }
            if (!Page.IsPostBack)
            {
                ajaxCallPath = "Templates/Scanweb/Pages/HotelOverviewPage.aspx";

                if (CurrentPage["CountrySortOrder"] != null)
                    linksCollection = CurrentPage.Property["CountrySortOrder"].Value as LinkItemCollection;

            }
            string view = Request.QueryString["initialview"];
            string source = Request.QueryString["source"];
            if (!string.IsNullOrEmpty(view))
            {
                if (Request.QueryString["initialview"].Equals("map"))
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "mapView", "FindDestinationSetMaps();", true);
                }
                else if (Request.QueryString["initialview"].Equals("list"))
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "listView", "FindDestinationSetList();", true);
                }
            }

            if (!string.IsNullOrEmpty(view) && !string.IsNullOrEmpty(source) && !string.IsNullOrEmpty(mapId))
            {
                if (view.ToLower().Equals("map") && source.ToLower().Equals("mail"))
                {
                    showOnMap = true;
                    mapHotelValue = string.Format("HOTEL:{0}", mapId);
                }
            }

            FindAHotelSessionVariablesSessionWrapper.SelectedMapId = null;
            BookingModuleBig bookingModule = udAddAnotherHotel as BookingModuleBig;
            bookingModule.Visible = OWSVisibilityControl.BookingModuleShouldBeVisible;
            AlternativeBookingModule.Visible = !OWSVisibilityControl.BookingModuleShouldBeVisible;
            bookingModule.IsBookingModuleReset = true;
            MasterPageFindAHotel master = (MasterPageFindAHotel)this.Master;
            master.ChangeVisibilityOfSubMenu(true);
        }
        protected void Redirect(string url)
        {
            Response.Redirect(url, false);
            Response.StatusCode = (int)System.Net.HttpStatusCode.MovedPermanently;
            Response.End();
        }
        protected void Redirect(string url, NameValueCollection querystrings)
        {
            StringBuilder redirectUrl = new StringBuilder(url);

            if (querystrings != null)
            {
                for (int index = 0; index < querystrings.Count; index++)
                {
                    if (index == 0)
                    {
                        redirectUrl.Append("?");
                    }

                    redirectUrl.Append(querystrings.Keys[index]);
                    redirectUrl.Append("=");
                    redirectUrl.Append(HttpUtility.UrlEncode(querystrings[index]));

                    if (index < querystrings.Count - 1)
                    {
                        redirectUrl.Append("&");
                    }
                }
            }

            this.Redirect(redirectUrl.ToString());
        }
        [WebMethod]
        public static List<CountryDestinatination> GetCountryCityDestinations()
        {
            CountryCityDestinations countryCityDestinations = CountryCityDestinations.GetCountryCityDestinationsInstance();
            List<CountryDestinatination> countryCityList = new List<CountryDestinatination>();
            try
            {
                countryCityList = countryCityDestinations.GetCountryCityDestinations(linksCollection, EpiServerPageConstants.FIND_YOUR_DESTINATION);

                if (countryCityList == null || countryCityList.Count == 0)
                    OnError(countryCityList, "empty", WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/EmptyCountryList"));
            }
            catch (Exception ex)
            {
                OnError(countryCityList, "error", ex.Message);
            }
            return countryCityList;
        }

        [WebMethod]
        public static string CityHotelLandingRedirect(string currentlyRenderedRoomTypes)
        {
            string[] pagelinksplit = currentlyRenderedRoomTypes.Split(':');
            PageData pageData = null;

            if (pagelinksplit[0].ToUpper() == "CITY")
                pageData = ContentDataAccess.GetCityPageDataByOperaID(Convert.ToString(pagelinksplit[1]));
            else if (pagelinksplit[0].ToUpper() == "HOTEL")
                pageData = ContentDataAccess.GetHotelPageDataByOperaID(Convert.ToString(pagelinksplit[1].ToString()), string.Empty);

            UrlBuilder url = new UrlBuilder(pageData.LinkURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, pageData.LinkURL, System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }
        private static void OnError(List<CountryDestinatination> countryCityList, string errorType, string errorMsg)
        {
            CountryDestinatination destination = new CountryDestinatination(null, null, null, null, null);
            destination.ClientMsg = string.Format("{0}{1}{2}{3}{4}{5}{6}", "<", errorType, ">", errorMsg, "<BR /></", errorType, ">");
            countryCityList.Add(destination);
        }
    }
    public enum Destination
    {
        Country,
        City,
        Hotel
    }
}
