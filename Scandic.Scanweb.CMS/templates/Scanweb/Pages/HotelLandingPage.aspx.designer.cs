//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3615
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Scandic.Scanweb.CMS.Templates {
    
    
    /// <summary>
    /// HotelLandingPage class.
    /// </summary>
    /// <remarks>
    /// Auto-generated class.
    /// </remarks>
    public partial class HotelLandingPage {
        
        /// <summary>
        /// HotelContentPH control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder HotelContentPH;
        
        /// <summary>
        /// HotelContentPHLeft control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder HotelContentPHLeft;
        
        /// <summary>
        /// HotelContentInnerPHLeft control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder HotelContentInnerPHLeft;
        
        /// <summary>
        /// HotelContentPHRight control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder HotelContentPHRight;
        
        /// <summary>
        /// MainBodyRightArea control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl MainBodyRightArea;
        
        /// <summary>
        /// HotelContentInnerPHRight control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder HotelContentInnerPHRight;
        
        /// <summary>
        /// phTARatings control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder phTARatings;
        
        /// <summary>
        /// ratings control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ratings;
        
        /// <summary>
        /// RightColumnBookingPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder RightColumnBookingPlaceHolder;
        
        /// <summary>
        /// BE control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UserControl BE;
        
        /// <summary>
        /// AlternativeBookingModule control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Scandic.Scanweb.CMS.Templates.Units.Placeable.BookingModuleAlternativeBox AlternativeBookingModule;
        
        /// <summary>
        /// MeetingPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder MeetingPlaceHolder;
        
        /// <summary>
        /// RFPBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.PromoBoxOffer RFPBox;
        
        /// <summary>
        /// Box1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.PromoBoxOffer Box1;
        
        /// <summary>
        /// Box2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.PromoBoxOffer Box2;
        
        /// <summary>
        /// Box3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.PromoBoxOffer Box3;
        
        /// <summary>
        /// Box4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable.PromoBoxOffer Box4;
        
        /// <summary>
        /// storyBoxDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl storyBoxDiv;
        
        /// <summary>
        /// StoryBoxPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder StoryBoxPlaceHolder;
        
        /// <summary>
        /// phTripAdvisorAwards control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder phTripAdvisorAwards;
        
        /// <summary>
        /// PopulateBookingModule control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Scandic.Scanweb.CMS.Templates.Units.Placeable.PopulateBookingModuleInput PopulateBookingModule;
    }
}
