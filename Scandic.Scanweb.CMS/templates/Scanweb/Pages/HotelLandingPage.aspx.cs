//  Description					:   HotelLandingPage                                      //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Filters;
using EPiServer.Security;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;
using Scandic.Scanweb.CMS.code.Util.Map;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Templates.MasterPages;
using Scandic.Scanweb.CMS.Templates.Units.Static;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Code behind of HotelLandingPage.
    /// </summary>
    public partial class HotelLandingPage : ScandicTemplatePage
    {
        #region Properties

        /// <summary>
        /// Gets/Sets HotelContent
        /// </summary>
        public PlaceHolder HotelContent { get; set; }

        /// <summary>
        /// Gets/Sets HLPContent
        /// </summary>
        public Control HLPContent { get; set; }

        /// <summary>
        /// Gets/Sets HLPContentLeft
        /// </summary>
        public Control HLPContentLeft { get; set; }

        /// <summary>
        /// Get page data for the selected Hotel
        /// </summary>
        /// <remarks>
        /// Returns null if no hotel page is selected or if the seleced hotel page is not published
        /// </remarks>
        public PageData HotelPage
        {
            get
            {
                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;

                if (CurrentPage.PageTypeID == hotelPageTypeID)
                {
                    return CurrentPage;
                }
                else
                {
                    if (CurrentPage["HotelPage"] != null)
                    {
                        PageData hotelPage =
                            DataFactory.Instance.GetPage((PageReference)CurrentPage["HotelPage"],
                                                         EPiServer.Security.AccessLevel.NoAccess);
                        return (FilterPublished.CheckPublishedStatus(hotelPage, PagePublishedStatus.Published))
                                   ? hotelPage
                                   : null;
                    }
                    else
                        return null;
                }
            }
        }

        public string TALocationID
        {
            get
            {
                var result = "0";
                //return HotelPage["TALocationID"].ToString();
                if(!string.IsNullOrEmpty(Convert.ToString(HotelPage["TALocationID"])))
                   result = Convert.ToString(HotelPage["TALocationID"]);
                return result;
            }
        }

        private string HotelReviewCount = string.Empty;
        private string HotelRatingsCount = string.Empty;


        #endregion

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Renders correct control in placeholder

            string tabName = Request.QueryString.Get("hotelpage");

            RFPBox.HideBookNow = true;
            Box1.HideBookNow = true;
            Box2.HideBookNow = true;
            Box3.HideBookNow = true;
            Box4.HideBookNow = true;

            if (Utility.DisplayTripAdvisor(HotelPage))
            {
                LoadTARatings(tabName);
                Control tripAdvisorAwards = LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\TripAdvisorRatingsandAwards.ascx");
                phTripAdvisorAwards.Controls.Add(tripAdvisorAwards);
            }

            switch (tabName)
            {
                case "overview":
                    HLPContent =
                        LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelOverview.ascx");
                    HotelContentPH.Visible = true;
                    HotelContentPHLeft.Visible = false;
                    HotelContentPHRight.Visible = false;
                    HotelContentPH.Controls.Add(HLPContent);
                    break;
                case "location":
                    if (!String.IsNullOrEmpty(Request.QueryString.Get("location")))
                    {
                        switch (Request.QueryString.Get("location"))
                        {
                            case "detailedmap":
                                HLPContent =
                                    LoadControl(
                                        "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelLocation.ascx");
                                break;
                            case "transportoptions":
                                HLPContent =
                                    LoadControl(
                                        "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelLocationTransport.ascx");
                                ((HotelLocationTransport)HLPContent).TransportPages = GetTravelInstructionPages();
                                break;
                            case "localattractions":
                                HLPContent =
                                    LoadControl(
                                        "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelLocationLocalAttr.ascx");
                                ((HotelLocationLocalAttr)HLPContent).LocalAttrPages = GetLocalAttractionPages();
                                break;
                            default:
                                HLPContent =
                                    LoadControl(
                                        "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelLocation.ascx");
                                break;
                        }
                    }
                    else
                        HLPContent =
                            LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelLocation.ascx");


                    HLPContentLeft =
                        LoadControl(
                            "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelLocationLeftRegion.ascx");
                    HotelContentPH.Visible = false;
                    HotelContentPHLeft.Visible = true;
                    HotelContentPHRight.Visible = true;
                    HotelContentInnerPHLeft.Controls.Add(HLPContentLeft);
                    HotelContentInnerPHRight.Controls.Add(HLPContent);
                    break;
                case "facilities":
                    if (PageIsHotelAdditionalInfoPage("facilityid"))
                    {
                        HLPContent =
                            LoadControl(
                                "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelAdditionalInformation.ascx");
                        ((Scandic.Scanweb.CMS.Templates.Units.Static.HotelAdditionalInformation)HLPContent).
                            IdentifierString = "facilityid";
                    }
                    else
                        HLPContent =
                            LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelFacilities.ascx");

                    HLPContentLeft =
                        LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelLeftRegion.ascx");
                    HotelContentPH.Visible = false;
                    HotelContentPHLeft.Visible = true;
                    HotelContentPHRight.Visible = true;
                    HotelContentInnerPHLeft.Controls.Add(HLPContentLeft);
                    HotelContentInnerPHRight.Controls.Add(HLPContent);
                    ((HotelLeftRegion)HLPContentLeft).Pages = GetFacilityPages();
                    ((HotelLeftRegion)HLPContentLeft).IdentifierQueryString = "facilityid";
                    break;
                case "rooms":
                    if (PageIsHotelAdditionalInfoPage("roomid"))
                    {
                        HLPContent =
                            (Scandic.Scanweb.CMS.Templates.Units.Static.HotelAdditionalInformation)
                            LoadControl(
                                "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelAdditionalInformation.ascx");
                        ((Scandic.Scanweb.CMS.Templates.Units.Static.HotelAdditionalInformation)HLPContent).
                            IdentifierString = "roomid";
                    }
                    else
                        HLPContent =
                            LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelRooms.ascx");

                    HLPContentLeft =
                        LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelLeftRegion.ascx");
                    HotelContentPH.Visible = false;
                    HotelContentPHLeft.Visible = true;
                    HotelContentPHRight.Visible = true;
                    HotelContentInnerPHLeft.Controls.Add(HLPContentLeft);
                    HotelContentInnerPHRight.Controls.Add(HLPContent);
                    ((HotelLeftRegion)HLPContentLeft).Pages = GetHotelRoomDescriptionPages();
                    ((HotelLeftRegion)HLPContentLeft).IdentifierQueryString = "roomid";
                    break;
                case "additionalfacilities":
                    if (PageIsHotelAdditionalInfoPage("facilityid"))
                    {
                        HLPContent =
                            LoadControl(
                                "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelAdditionalInformation.ascx");
                        ((Scandic.Scanweb.CMS.Templates.Units.Static.HotelAdditionalInformation)HLPContent).
                            IdentifierString = "facilityid";
                    }
                    else
                        HLPContent =
                            LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelFacilities.ascx");

                    HLPContentLeft =
                        LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelLeftRegion.ascx");
                    HotelContentPH.Visible = false;
                    HotelContentPHLeft.Visible = true;
                    HotelContentPHRight.Visible = true;
                    HotelContentInnerPHLeft.Controls.Add(HLPContentLeft);
                    HotelContentInnerPHRight.Controls.Add(HLPContent);
                    ((HotelLeftRegion)HLPContentLeft).Pages = GetAdditionalFacilities();
                    ((HotelLeftRegion)HLPContentLeft).IdentifierQueryString = "facilityid";
                    break;
                case "offers":
                    HLPContent = LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelOffers.ascx");
                    HotelContentPH.Visible = true;
                    HotelContentPHLeft.Visible = false;
                    HotelContentPHRight.Visible = false;
                    HotelContentPH.Controls.Add(HLPContent);
                    break;
                case "meetings":
                    HLPContent =
                        LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelMeetings.ascx");
                    HLPContentLeft =
                        LoadControl(
                            "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelMeetingsLeftRegion.ascx");
                    HotelContentPH.Visible = false;
                    HotelContentPHLeft.Visible = true;
                    HotelContentPHRight.Visible = true;
                    HotelContentInnerPHLeft.Controls.Add(HLPContentLeft);
                    HotelContentInnerPHRight.Controls.Add(HLPContent);
                    ((HotelMeetings)HLPContent).MeetingRooms = GetMeetingRoomPages();
                    PageReference rfpPageLink = RootPage["RequestForPorposalPage"] as PageReference;
                    if (!PageReference.IsNullOrEmpty(rfpPageLink))
                    {
                        try
                        {
                            RFPBox.OfferPageLink = DataFactory.Instance.GetPage(rfpPageLink, AccessLevel.NoAccess);
                            MeetingPlaceHolder.Visible = true;
                            RightColumnBookingPlaceHolder.Visible = false;
                        }
                        catch (Exception)
                        {
                        }
                    }

                    break;
                case "guestprogram":
                    HLPContent =
                        LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelGuestProgram.ascx");
                    HLPContentLeft =
                        LoadControl(
                            "~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelGuestProgramLeftRegion.ascx");
                    HotelContentPH.Visible = false;
                    HotelContentPHLeft.Visible = true;
                    HotelContentPHRight.Visible = true;
                    HotelContentInnerPHLeft.Controls.Add(HLPContentLeft);
                    HotelContentInnerPHRight.Controls.Add(HLPContent);
                    break;
                case "TravellerReviews":
                    phTARatings.Visible = false;
                    HotelContentPH.Visible = false;
                    HotelContentPHLeft.Visible = false;
                    MainBodyRightArea.Style.Add("float", "left");
                    HotelContentPHRight.Visible = true;
                    HLPContent = LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelTravellerReviews.ascx");
                    HotelContentInnerPHRight.Controls.Add(HLPContent);
                    break;
                default:
                    HLPContent =
                        LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelOverview.ascx");
                    HotelContentPH.Visible = true;
                    HotelContentPHLeft.Visible = false;
                    HotelContentPHRight.Visible = false;
                    HotelContentPH.Controls.Add(HLPContent);
                    break;
            }

            #endregion

            if (BE != null)
                BE.Visible = OWSVisibilityControl.BookingModuleShouldBeVisible;
            AlternativeBookingModule.Visible = !OWSVisibilityControl.BookingModuleShouldBeVisible;
            string hotelName = string.Empty;
            if (HotelPage != null) hotelName = HotelPage["Heading"] as string ?? string.Empty;

            if (CurrentPage.Property["AvailableInBooking"].IsNull)
                hotelName = WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/EnterHotelName");
            if (!string.IsNullOrEmpty(hotelName))
                hotelName = hotelName.Trim();
            PopulateBookingModule.DestinationName = hotelName;
            PopulateStoryBox();

            MasterPageHotels master = (MasterPageHotels)this.Master;
            PlaceHolder phMetaForSchemaOrgHotelUrl = (PlaceHolder)master.FindControl("phMetaForSchemaOrgHotelUrl");
            if (phMetaForSchemaOrgHotelUrl != null)
            {
                UrlBuilder url = new UrlBuilder(CurrentPage.LinkURL);
                EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, CurrentPage.PageLink, System.Text.UTF8Encoding.UTF8);
                string path = HttpContext.Current.Request.Url.Host + url.ToString();
                AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelUrl, "url", path);
            }

            PlaceHolder phMetaForSchemaOrgHotelAddress = (PlaceHolder)master.FindControl("phMetaForSchemaOrgHotelAddress");
            if (phMetaForSchemaOrgHotelAddress != null)
            {
                AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelAddress, "streetAddress", Convert.ToString(HotelPage["StreetAddress"]));
                AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelAddress, "telephone", Convert.ToString(HotelPage["Phone"]));
                AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelAddress, "addressLocality", Convert.ToString(HotelPage["City"]));
                AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelAddress, "postalCode", Convert.ToString(HotelPage["PostCode"]));
                AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelAddress, "addressCountry", Convert.ToString(HotelPage["Country"]));
            }
            PlaceHolder phMetaForSchemaOrgHotelGeoCoords = (PlaceHolder)master.FindControl("phMetaForSchemaOrgHotelGeoCoords");
            if (phMetaForSchemaOrgHotelGeoCoords != null)
            {
                AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelGeoCoords, "latitude", Convert.ToString(HotelPage["GeoY"]));
                AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelGeoCoords, "longitude", Convert.ToString(HotelPage["GeoX"]));
            }
            GetHotelReviewRatingsForSchemaOrg(Convert.ToInt32(TALocationID));
            PlaceHolder phMetaForSchemaOrgHotelRatings = (PlaceHolder)master.FindControl("phMetaForSchemaOrgHotelRatings");
            if (phMetaForSchemaOrgHotelRatings != null && !string.IsNullOrEmpty(HotelRatingsCount) && !string.IsNullOrEmpty(HotelReviewCount))
            {
                AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelRatings, "ratingValue", HotelRatingsCount);
                AddSchemaOrgHotelMetaTags(phMetaForSchemaOrgHotelRatings, "ratingcount", HotelReviewCount);
            }

        }

        private void AddSchemaOrgHotelMetaTags(PlaceHolder metaContentPlaceHolder, string itemPropval, string contentValue)
        {
            if (metaContentPlaceHolder != null)
            {
                Utility.AddMetaInfoForSchemaOrg(metaContentPlaceHolder, "itemprop", itemPropval, contentValue);
            }
        }

        /// <summary>
        /// This will add story box if no offers or special alert is not configure in CMS right column.
        /// </summary>
        private void PopulateStoryBox()
        {
            Control storyBox = null;
            if (CurrentPage.Property["BoxContainer1"].Value == null
                && CurrentPage.Property["BoxContainer2"].Value == null
                && CurrentPage.Property["BoxContainer3"].Value == null &&
                CurrentPage.Property["BoxContainer4"].Value == null)
            {
                storyBox = LoadControl("~\\Templates\\Scanweb\\Units\\Static\\HotelLandingPage\\HotelStoryBox.ascx");
                StoryBoxPlaceHolder.Controls.Add(storyBox);
            }
            else
            {
                StoryBoxPlaceHolder.Visible = false;
                storyBoxDiv.Visible = false;
            }
        }

        /// <summary>
        /// Gets required CSS
        /// </summary>
        /// <returns>CSS</returns>
        protected string GetCSS()
        {
            if (Request.QueryString["hotelpage"] != null && Request.QueryString["hotelpage"] == "meetings")
            {
                return "RightColumn";
            }
            else
            {
                return "RightColumnShiftUp";
            }
        }

        /// <summary>
        /// PageIsHotelAdditionalInfoPage
        /// </summary>
        /// <param name="idstring"></param>
        /// <returns>True/False</returns>
        private bool PageIsHotelAdditionalInfoPage(string idstring)
        {
            if (Request.QueryString[idstring] != null)
            {
                int pageID = -1;

                if (int.TryParse(Request.QueryString.Get(idstring), out pageID))
                {
                    try
                    {
                        PageData showPage = DataFactory.Instance.GetPage(new PageReference(pageID), AccessLevel.NoAccess);
                        PageType hotelAdditionalInformationPageType =
                            PageType.Load(
                                new Guid(ConfigurationManager.AppSettings["HotelAdditionalInformationPageTypeGUID"]));
                        return showPage.PageTypeID == hotelAdditionalInformationPageType.ID;
                    }
                    catch (PageNotFoundException)
                    {
                        return false;
                    }
                }
            }
            return false;
        }

        private void LoadTARatings(string tabName)
        {
            if (string.IsNullOrEmpty(tabName) || !tabName.Equals("TripAdvisor", StringComparison.InvariantCultureIgnoreCase))
            {
                ratings.Attributes.Add("src", string.Format(ConfigurationManager.AppSettings["TARatings"],
                TALocationID,
                ConfigurationManager.AppSettings["TAPartnerID"],
                Utility.CurrentLanguage));
                phTARatings.Visible = true;
            }
        }
        /// <summary>
        /// Gets the url for the tab selected
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        protected string GetHotelPageURL()
        {
            UrlBuilder url = new UrlBuilder((UriSupport.AddQueryString(CurrentPage.LinkURL, "hotelpage", "TravellerReviews")));
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, CurrentPage.PageLink,
                                                                  System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }

        private void GetHotelReviewRatingsForSchemaOrg(int taLocatiionID)
        {
            TripAdvisorPlaceListEntity m_tripAdvisorPlaceList = TripAdvisorRatingFeedManager.LoadTripAdvisorRatings();
            if (m_tripAdvisorPlaceList != null && m_tripAdvisorPlaceList.TAPlaces != null && m_tripAdvisorPlaceList.TAPlaces.Count > 0)
            {
                TripAdvisorPlaceEntity TAplace = m_tripAdvisorPlaceList.TAPlaces.Where(o => Convert.ToInt32(o.Id) == taLocatiionID).SingleOrDefault();
                if (TAplace != null && TAplace.ReviewCount > 0)
                {
                    HotelReviewCount = Convert.ToString(TAplace.ReviewCount);
                    HotelRatingsCount = Convert.ToString(TAplace.AverageRating);
                }
            }
        }


        #region Hotel Related Data Access Methods

        /// <summary>
        /// Get Facility Pages
        /// </summary>
        /// <returns>A collection of Facility pages associated with this hotel</returns>
        private PageDataCollection GetFacilityPages()
        {
            int facilityContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["FacilityContainerPageTypeGUID"])).ID;
            return GetChildPagesFromContainer(HotelPage.PageLink, facilityContainerPageTypeID);
        }

        /// <summary>
        /// Get Travel Instruction Pages
        /// </summary>
        /// <returns>A collection of Travel Instruction pages associated with this hotel</returns>
        private PageDataCollection GetTravelInstructionPages()
        {
            int travelInstructionContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["TravelInstructionContainerPageTypeGUID"])).ID;
            return GetChildPagesFromContainer(HotelPage.PageLink, travelInstructionContainerPageTypeID);
        }

        /// <summary>
        /// Get Meeting Room Pages
        /// </summary>
        /// <returns>A collection of Meeting Room pages associated with this hotel</returns>
        private PageDataCollection GetMeetingRoomPages()
        {
            int meetingRoomContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["MeetingRoomContainerPageTypeGUID"])).ID;
            return GetChildPagesFromContainer(HotelPage.PageLink, meetingRoomContainerPageTypeID);
        }

        /// <summary>
        /// Get Local Attraction Pages 
        /// </summary>
        /// <returns>A collection of Local Attraction pages pages associated with this hotel</returns>
        private PageDataCollection GetLocalAttractionPages()
        {
            int hotelLocalAttractionContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelLocalAttractionContainerPageTypeGUID"])).
                    ID;
            int localAttractionContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["LocalAttractionPageTypeGUID"])).ID;

            PageDataCollection hotelLocalAttractionPages =
                GetChildPagesFromContainer(HotelPage.PageLink, hotelLocalAttractionContainerPageTypeID);

            PageDataCollection localAttractionPages = new PageDataCollection();
            foreach (PageData hotelLocalAttraction in hotelLocalAttractionPages)
            {
                if (hotelLocalAttraction["LocalAttraction"] as PageReference != null)
                {
                    PageData localAttractionPage =
                        DataFactory.Instance.GetPage((PageReference)hotelLocalAttraction["LocalAttraction"],
                                                     AccessLevel.NoAccess);

                    if (localAttractionPage.CheckPublishedStatus(PagePublishedStatus.Published) &&
                        localAttractionPage.QueryDistinctAccess(AccessLevel.Read) &&
                        localAttractionPage.PageTypeID == localAttractionContainerPageTypeID)
                        localAttractionPages.Add(localAttractionPage);
                }
            }
            return localAttractionPages;
        }

        /// <summary>
        /// Get Hotel Room Descriptions
        /// </summary>
        /// <returns>A collection of Hotel Room Description pages associated with this hotel</returns>
        private PageDataCollection GetHotelRoomDescriptionPages()
        {
            int hotelRoomDescriptionContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelRoomDescriptionContainerPageTypeGUID"])).
                    ID;
            return GetChildPagesFromContainer(HotelPage.PageLink, hotelRoomDescriptionContainerPageTypeID);
        }


        /// <summary>
        /// Get Additional Facilities
        /// </summary>
        /// <returns>A collection of Hotel Room Description pages associated with this hotel</returns>
        private PageDataCollection GetAdditionalFacilities()
        {
            int additionalFacilityContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["AdditionalFacilityContainerPageTypeGUID"])).ID;
            return GetChildPagesFromContainer(HotelPage.PageLink, additionalFacilityContainerPageTypeID);
        }


        /// <summary>
        /// Gets the child pages from a specific container at the hotel page
        /// </summary>
        /// <param name="hotelPageLink">Link to the hotel page</param>
        /// <param name="containerPageTypeID">Container PageTypeID</param>
        /// <returns>All published child pages that the user has read access to</returns>
        private PageDataCollection GetChildPagesFromContainer(PageReference hotelPageLink, int containerPageTypeID)
        {
            PageDataCollection containerPages = DataFactory.Instance.GetChildren(hotelPageLink);
            foreach (PageData containerPage in containerPages)
            {
                if (containerPage.PageTypeID == containerPageTypeID)
                {
                    PageDataCollection childPages = DataFactory.Instance.GetChildren(containerPage.PageLink);

                    for (int i = childPages.Count - 1; i >= 0; i--)
                    {
                        bool pageIsPublished = childPages[i].CheckPublishedStatus(PagePublishedStatus.Published);
                        bool hasReadAccess = childPages[i].QueryDistinctAccess(AccessLevel.Read);

                        if (!(pageIsPublished && hasReadAccess))
                            childPages.Remove(childPages[i]);
                    }

                    return childPages;
                }
            }
            return null;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapHotelUnit"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetGoogleMapHotelContetOverlayInfo(MapHotelUnit mapHotelUnit)
        {
            return mapHotelUnit.GenerateInfoWindowMarkUp();
        }
    }
}