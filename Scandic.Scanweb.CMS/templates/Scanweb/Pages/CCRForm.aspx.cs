//  Description					: CCRForm                                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAccess;
using EPiServer.Security;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Templates.Pages
{
    /// <summary>
    /// Code behind of CCRForm page.
    /// </summary>
    public partial class CCRForm : TemplatePage
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load(object sender, System.EventArgs e)
        {
            Page.DataBind();
            List<LanguageBranch> LanguageBranchList = LanguageBranch.ListEnabled();
            foreach (LanguageBranch LanguageBranchItem in LanguageBranchList)
                Language.Items.Add(new ListItem(LanguageBranchItem.Name, LanguageBranchItem.LanguageID));
            ReqHeader.Text = Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/Header");
            ReqAuthorName.Text = Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/Name");
            ReqEmail.Text = Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/Email");
            RegularEmail.Text = Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/InvalidEmail");
            ReqTelephone.Text = Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/Telephone");
            ReqHotel.Text = Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/HotelorSO");
            Reqdate.Text = Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/Requestdate");
            ReqCCR.Text = Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/CCR");
            ReqSum.Text = Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/Summary");
            ImageIntegerValidator.Text = Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/Integer");
            ImageIntegerValidator_Change.Text =
                Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/Integer");

            Translate_ConententChange.Text =
                String.Concat(Translate("/Templates/Scanweb/Pages/contentChangeRequest/WebSiteSection"),
                              AppConstants.SPACE, AppConstants.STAR);
            ReqWebSiteSection_Change.Text =
                Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/WebsiteSection");
            validDate.ErrorMessage =
                Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/validRequestDate");
            validOfferStartDate.ErrorMessage =
                Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/validRequestDate");
            validOfferEndDate.ErrorMessage =
                Translate("/Templates/Scanweb/Pages/contentChangeRequest/RequiredField/validRequestDate");
            SpecialAlert_Change.Attributes.Add("OnCheckedChanged", "javascript:InitValidator();");
            ImageID_Change.Attributes.Add("OnTextChanged", "javascript:InitValidator();");
            CopyText_Change.Attributes.Add("OnTextChanged", "javascript:InitValidator();");
            WebSiteSection_Change.Attributes.Add("OnTextChanged", "javascript:InitValidator();");
            Submit.Attributes.Add("onclick", "javascript:InitValidator();");
        }

        /// <summary>
        /// Submit button click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Submit_Click(object sender, System.EventArgs e)
        {
            if (!Language.SelectedValue.Equals(string.Empty))
            {
                string pageLanguage = Language.SelectedValue.ToString();

                PageData formPage = DataFactory.Instance.GetPage(new PageReference(CurrentPage.PageLink.ID),
                                                                 new LanguageSelector(pageLanguage),
                                                                 AccessLevel.NoAccess);
                PageReference targetContainerLink = (PageReference) formPage["TargetContainer"];

                int contentChangeRequestPageTypeID =
                    PageType.Load(new Guid(ConfigurationManager.AppSettings["ContentChangeRequestPageTypeGUID"])).ID;
                PageData contentChangeRequest = DataFactory.Instance.GetDefaultPageData(targetContainerLink,
                                                                                        contentChangeRequestPageTypeID,
                                                                                        AccessLevel.NoAccess);


                contentChangeRequest.PageName = Header.Text;
                contentChangeRequest["AuthorName"] = AuthorName.Text;
                contentChangeRequest["AuthorEmail"] = AuthorEmail.Text;
                contentChangeRequest["AuthorTelephone"] = AuthorTelephone.Text;
                contentChangeRequest["AuthorLocation"] = AuthorLocation.Text;
                contentChangeRequest["SpecialAlert"] = SpecialAlert.Checked;
                contentChangeRequest["SpecialAlert_Change"] = SpecialAlert_Change.Checked;

                contentChangeRequest["WebSiteCOM"] = WebSiteCOM.Checked;
                contentChangeRequest["WebSiteSE"] = WebSiteSE.Checked;
                contentChangeRequest["WebSiteNO"] = WebSiteNO.Checked;
                contentChangeRequest["WebSiteDK"] = WebSiteDK.Checked;
                contentChangeRequest["WebSiteFI"] = WebSiteFI.Checked;
                contentChangeRequest["WebSiteDE"] = WebSiteDE.Checked;
                contentChangeRequest["WebSiteRU"] = WebSiteRU.Checked;

                contentChangeRequest["WebSiteSection"] = WebSiteSection.Text;
                contentChangeRequest["WebSiteSection_Change"] = WebSiteSection_Change.Text;
                contentChangeRequest["DescriptionCCR"] = DescriptionCCR.Text;
                contentChangeRequest["CopyText"] = CopyText.Text;
                contentChangeRequest["CopyText_Change"] = CopyText_Change.Text;

                contentChangeRequest["UniqueSellingPoints"] = UniqueSellingPoints.Text;
                contentChangeRequest["OfferDetails"] = OfferDetails.Text;

                int imageID;
                int imageID_Change;
                if (Int32.TryParse(ImageID.Text.Trim(), out imageID))
                {
                    contentChangeRequest["ImageID"] = imageID;
                }
                if (Int32.TryParse(ImageID_Change.Text.Trim(), out imageID_Change))
                {
                    contentChangeRequest["ImageID_Change"] = imageID_Change;
                }

                contentChangeRequest["ExternalLink"] = ExternalLink.Text;
                contentChangeRequest["SourceLanguage"] = Language.SelectedValue;
                contentChangeRequest["EnglishSummary"] = EnglishSummary.Text;

                int offerImageID;
                if (Int32.TryParse(OfferImageID.Text.Trim(), out offerImageID))
                {
                    contentChangeRequest["OfferImageID"] = offerImageID;
                }

                try
                {
                    IFormatProvider culture = new CultureInfo("sv-SE", true);

                    contentChangeRequest["ChangeRequestDate"] = DateTime.Parse(ChangeRequestedDate.Text,
                                                                               culture,
                                                                               DateTimeStyles.None);
                }
                catch (FormatException)
                {
                }
                try
                {
                    IFormatProvider culture = new CultureInfo("sv-SE", true);
                    contentChangeRequest["OfferStartDate"] = DateTime.Parse(OfferStartDate.Text,
                                                                            culture,
                                                                            DateTimeStyles.None);
                }
                catch (FormatException)
                {
                }
                try
                {
                    IFormatProvider culture = new CultureInfo("sv-SE", true);
                    contentChangeRequest["OfferEndDate"] = DateTime.Parse(OfferEndDate.Text,
                                                                          culture,
                                                                          DateTimeStyles.None);
                }
                catch (FormatException)
                {
                }

                DataFactory.Instance.Save(contentChangeRequest, SaveAction.Publish, AccessLevel.NoAccess);

                ChangeRequestFormTablePlaceholder.Visible = false;
                ChangeRequestFeedback.Visible = true;
            }
        }
    }
}