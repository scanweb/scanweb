<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Pages.OfferOverview"
    CodeBehind="OfferOverview.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageOffers.master" %>

<%@ Register TagPrefix="Booking" TagName="Module" Src="~/Templates/Booking/Units/BookingModuleSmall.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="BookingModuleAlternativeBox" Src="~/Templates/Scanweb/Units/Placeable/BookingModuleAlternativeBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="PromoBoxMainOffer" Src="~/Templates/Scanweb/Units/Placeable/PromoBoxMainOffer.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="PromoBoxOffer" Src="~/Templates/Scanweb/Units/Placeable/PromoBoxOffer.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="HotelStoryBox" Src="~/Templates/Scanweb/Units/Static/HotelLandingPage/HotelStoryBox.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyRegion" runat="server">
    <%--<script type="text/javascript" src="/Templates/Scanweb/Javascript/swfobject.js"></script>--%>
    <!--Start: R1.5.3| Artifact artf877397 : R1.5.3 | Offer Overview Page -->
    <div id="OfferPageFlashContent">
    </div>
    <% if (CurrentPage["EmbeddedFlashURL"] == null)
       { %>
    <div id="MainPromoOffer">
        <Scanweb:PromoBoxMainOffer ID="PromoBoxMainOffer1" Visible="true" OfferPageLinkPropertyName="MainPromoOffer"
            runat="server" />
    </div>
    <% } %>
    <%
        else
        {%>

    <script type="text/javascript">
        var so = new SWFObject('<%= CurrentPage["EmbeddedFlashURL"] %>', 'scandicpromotion', '<%= CurrentPage["EmbeddedFlashWidth"] %>', '<%= CurrentPage["EmbeddedFlashHeight"] %>', '<%= CurrentPage["EmbeddedFlashMinVersion"] %>', '#FFFFFF');
        so.addParam("wmode", "transparent");
        so.write("OfferPageFlashContent");
    </script>

    <% } %>
    <!--End: R1.5.3| Artifact artf877397 : R1.5.3 | Offer Overview Page -->
    <div id="smallOfferArea">
        <asp:PlaceHolder ID="PHSmallOfferArea" runat="server" />
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            setOfferNo();
        });

        function setOfferNo() {
            var bigOfferBox = $('#pboxOfferBig');
            var smallOfferBox = $('.smallPromoBoxLight');
            var bigOfferBoxVisible = $('#pboxOfferBig:visible');
            var smallOfferBoxVisible = $('.smallPromoBoxLight:visible');

            var i = 4;
            if (bigOfferBoxVisible)
                $(bigOfferBox).attr('name', 'Big top offer1');

            $(smallOfferBoxVisible).each(function(item) {
                this.setAttribute('name', 'Top offer' + i);
                i++;
            });
        }

        function trackPromobox(obj, prop44Value) {
            var prop54Value = $(obj).attr('name');
            var sfunc = s_gi(s_account);
            sfunc.linkTrackVars = 'prop44,prop54';
            sfunc.prop44 = prop44Value;
            sfunc.prop54 = prop54Value;
            sfunc.tl(this, 'o', 'weekend offers');
            return true;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
    <div class="RightColumnShiftUp">
        <div class="BoxContainer">
            <asp:PlaceHolder ID="RightColumnBookingPlaceHolder" runat="server">
                <Booking:Module ID="BE" runat="server" />
                <Scanweb:BookingModuleAlternativeBox CssClass="AlternativeBookingBoxSmall" ID="BookingModuleAlternativeBox1"
                    runat="server" Visible="false" />
            </asp:PlaceHolder>
        </div>
        <div class="BoxContainer">
            <Scanweb:PromoBoxOffer ID="Box4" OfferPageLinkPropertyName="BoxPage" ImagePropertyName="BoxImageMedium"
                runat="server" />
        </div>
    </div>
</asp:Content>
