﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Scandic.Scanweb.CMS.DataAccessLayer;
using EPiServer.Core;
using System.Web.Script.Serialization;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages
{
    public partial class CarouselAjax : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {   //Set response type
            Response.ContentType = "application/json";

            //get the values from request object
            int currentPageLinkID= Convert.ToInt32(Request["currentpagelinkID"]);
            string pageTypeID = Convert.ToString(Request["pagetypeID"]);
            
            //get the CurrentPage data based on the request object data.
            PageData CuurentPageData = new PageData();
            CuurentPageData= ContentDataAccess.GetPageData(currentPageLinkID);

            //Generate the JSON Object from Carousel Factory
            ////var genericobj = Carousel.CarouselFactory.CreateInstance(pageTypeID).GetCarouselData(CuurentPageData);

            //string jsonStr  = new JavaScriptSerializer().Serialize(genericobj);

            ////write to output stream
            //Response.Output.Write(genericobj);
        }
    }
}
