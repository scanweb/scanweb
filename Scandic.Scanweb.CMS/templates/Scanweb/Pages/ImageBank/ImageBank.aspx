<%@ Page Language="C#" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWide.master" AutoEventWireup="true" CodeBehind="ImageBank.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.ImageBank" %>
<%@ Register TagPrefix="Scanweb" TagName="MainBody" 	        Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="AlbumMenu" Src="~/Templates/Scanweb/Units/Static/ImageBank/AlbumMenu.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="AlbumImageList" Src="~/Templates/Scanweb/Units/Static/ImageBank/AlbumImageList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumnRegion" runat="server">
    <Scanweb:AlbumMenu runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainBodyLeftRegion" runat="server">       
    <Scanweb:MainBody runat="server" />
    <Scanweb:AlbumImageList runat="server" />
</asp:Content>