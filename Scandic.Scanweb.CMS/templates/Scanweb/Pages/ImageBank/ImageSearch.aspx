<%@ Page Language="C#" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWide.master" AutoEventWireup="true" CodeBehind="ImageSearch.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages.ImageBank.ImageSearch" %>
<%@ Register TagPrefix="Scanweb" TagName="ImageSearch" Src="~/Templates/Scanweb/Units/Static/ImageBank/ImageSearch.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="MainBody" 	        Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="LeftColumn"              Src="~/Templates/Scanweb/Units/Placeable/LeftColumn.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftColumnRegion" runat="server">
    <Scanweb:LeftColumn runat="server" />    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainBodyLeftRegion" runat="server">       
    <Scanweb:MainBody runat="server" />
    <Scanweb:ImageSearch runat="server" />
</asp:Content>
