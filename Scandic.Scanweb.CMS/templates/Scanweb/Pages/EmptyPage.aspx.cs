//  Description					:   EmptyPage                                             //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI.WebControls;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Code behind of Empty page.
    /// </summary>
    public partial class EmptyPage : ScandicTemplatePage
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Security.Principal.IPrincipal user = Context.User;
            if ((user == null) || !user.Identity.IsAuthenticated)
            {
                Response.Redirect("/");
            }

            if (IsValue("OperaID"))
                AddTableRow("OperaID", (string) CurrentPage["OperaID"]);

            if (IsValue("HiltonID"))
                AddTableRow("HiltonID", (string) CurrentPage["HiltonID"]);

            PropertyTable.Visible = IsValue("OperaID") || IsValue("HiltonID");
        }

        private void AddTableRow(string value1, string value2)
        {
            TableRow tableRow = new TableRow();
            TableCell propertyNameCell = new TableCell();
            propertyNameCell.Text = value1;
            TableCell propertyValueCell = new TableCell();
            propertyValueCell.Text = value2;
            tableRow.Cells.Add(propertyNameCell);
            tableRow.Cells.Add(propertyValueCell);
            PropertyTable.Rows.Add(tableRow);
        }
    }
}