<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.TestCDAL" Codebehind="TestCDAL.aspx.cs" %>


<%@ Register TagPrefix="Test" TagName="TestCDAL" Src="~/Templates/Scanweb/Units/TestCDAL.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="linklist" Src="~/Templates/Scanweb/Units/Placeable/LinkList.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head1" runat="server">
    <title>TestCDAL</title>
    <style type="text/css">
    body{
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
    
    img{
      
   
    }
    .hotel{
        width:600px;
        border: solid 1px black;
        padding:10px;
        margin-bottom:20px;
        background-color:#d3e660;
    }
    
    .city{
        width:800px;
        border: solid 1px black;
        padding:10px;
        margin-bottom:20px;
        background-color:#accccd;
    }
    
    .find{
        width:600px;
        border: solid 1px black;
        padding:10px;
        margin-bottom:20px;
        background-color:#eeeeee;
    }
 /*  ---Box style Test Start---  */
/* Box generellt Start */
.BoxContent
{
	font-size: 10px;
	background-color: #E7E7E7;
	padding:10px 10px 0px 10px;
}
.speratorImage
{
	background-image: url(/Templates/Scanweb/Styles/Default/Images/MenuPrick.gif);
	background-repeat: repeat-x;
	height: 3px;
	font-size: 4px;
}
.seperator
{
	background-color: #E7E7E7;
	height: 4px;
	padding: 10px 10px 10px 10px;
}
.fly
{
	margin-right: 10px;
	vertical-align: middle;
}
.link
{
	font-size: 12px;
	color: #333333;
}
.link:link
{
	text-decoration: none;
}

.link:hover
{
	text-decoration: underline;
}
.link:active
{
	text-decoration: none;
}


.BoxBottom
{
	background-repeat: no-repeat;
	padding-bottom: 20px;
	font-size: 12px;
	padding-left: 10px;
}
.BoxTexHeading
{
	font-size: 14pt;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	padding-left: 10px;
}

/* Box Generellt END */


/*  ---BoxImageSmall--*/
.BoxImageSmall
{
	width: 216px;
	background-color: white;
	overflow: hidden;
}
.BoxImageHeading
{
	background-color: #E7E7E7;
}
.BoxImageSmall .BoxTextHeading
{
	background-image: url(/Templates/Scanweb/Images/Box/boxSmallHeader.bmp);
}


.BoxImageSmall .BoxEmptyHeading
{
	background-image: url(/Templates/Scanweb/Images/Box/BoxSmallNoheader.bmp);
}


.BoxImageSmall .BoxBottom
{
	background-image: url(/Templates/Scanweb/Images/Box/boxSmallBottom.bmp);
}
/*---BoxImageSmall END --- */



/* BoxImageMedium START*/
.BoxImageMedium
{
	width: 226px;
	background-color: white;
	overflow: hidden;
}


.BoxImageMedium .BoxTextHeading
{
	background-image: url(/Templates/Scanweb/Images/Box/BoxMediumlHeader.bmp);
}
.BoxImageMedium .BoxEmptyHeading
{
	background-image: url(/Templates/Scanweb/Images/Box/BoxMediumNoheader.bmp);
}



.BoxImageMedium .BoxBottom
{
	background-image: url(/Templates/Scanweb/Images/Box/BoxMediumBottom.bmp);
}
/* BoxImageMedium End */

/* BoxImageLarge START */

.BoxImageLarge
{
	width: 349px;
	background-color: white;
	overflow: hidden;
}

.BoxImageLarge .BoxTextHeading
{
	background-image: url(/Templates/Scanweb/Images/Box/boxLargeHeader.bmp);
}
.BoxImageLarge .BoxEmptyHeading
{
	background-image: url(/Templates/Scanweb/Images/Box/BoxLargeNoheader.bmp);
}


.BoxImageLarge .BoxBottom
{
	background-image: url(/Templates/Scanweb/Images/Box/BoxLargeBottom.bmp);
}

/* BoxImageLarge END */
/* ---Box style Test End--- */ 
/*Dannes list css start*/
#Listlinkcontainer
{
	width: 226px;
}

#Listlinkcontainer .listcont
{
	float: left;
	vertical-align: bottom;
}

.listimage
{
	float: left;
	width: 10px;
	margin-top: 5px;
	margin-bottom: 5px;
}
.listitem a
{
	float: left;
	width: 216px;
	text-decoration: none;
	font-size: 0.8em;
	color: #000000;
	margin-top: 5px;
	margin-bottom: 5px;
}

.listitem a:hover
{
	text-decoration: underline;
}

.listitem a.active
{
	font-weight: bold;
}

.listSeparator
{
	float: left;
	width: 200px;
	height: auto;
	height: 1px;
	background: url(/Templates/Scanweb/Styles/Default//Images/horizontal_rule.gif) repeat-x;
}
/*dannes list css end*/
    

    </style>
</head>
<body>

    

    <form runat="server" id="form1">
        <div>
            <Test:TestCDAL runat="server" />
        </div>
    </form>
</body>
</html>
