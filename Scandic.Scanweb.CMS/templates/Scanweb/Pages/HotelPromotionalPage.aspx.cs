//  Description					:   HotelPromotionalPage                                  //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


#region using
using System;
using EPiServer;
using EPiServer.Globalization;
#endregion using

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Code behind of HotelPromotionalPage
    /// </summary>
    public partial class HotelPromotionalPage : Scandic.Scanweb.CMS.Util.ScandicTemplatePage
    {
        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int index = 0;
            if (null != CurrentPage["XMLFilePath"])
            {
                index = CurrentPage["XMLFilePath"].ToString().Substring(1).IndexOf('/');
                XMLFilePath.Value = CurrentPage["XMLFilePath"].ToString().Substring(index + 2);
            }
            if (null != CurrentPage["FlashFilePath"])
                FlashFilePath.Value = CurrentPage["FlashFilePath"].ToString();
            SelectedLanguage.Value = ContentLanguage.SpecificCulture.Parent.Name;
        }

        /// <summary>
        /// This will load all child controls and Bind header data for the page.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            this.Page.Title = GetTitle();
            this.Page.Header.DataBind();
        }

        /// <summary>
        /// Returns the right title tag. Title, if it is set, and PageName otherwise.
        /// </summary>
        protected string GetTitle()
        {
            PageBase page = (PageBase) Page;

            if (page.CurrentPage["Title"] != null)
            {
                return page.CurrentPage["Title"].ToString();
            }
            else
            {
                if (page.CurrentPage["Heading"] != null)
                {
                    return page.CurrentPage["Heading"].ToString();
                }
                else
                {
                    return page.CurrentPage.PageName.ToString();
                }
            }
        }
    }
}