////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  TestCDAL                                               //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Code behind of TestCDAL page.
    /// </summary>
    public partial class TestCDAL : EPiServer.TemplatePage
    {
    }
}