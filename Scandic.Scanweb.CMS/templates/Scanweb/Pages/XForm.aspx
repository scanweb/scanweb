<%@ Page Language="C#" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWide.master" AutoEventWireup="False" CodeBehind="XForm.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.XFormPage" %>
<%@ Register TagPrefix="Scanweb" TagName="MainBody"  Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="XForm"		Src="~/Templates/Scanweb/Units/Placeable/XForm.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>

<asp:Content ContentPlaceHolderID="MainBodyLeftRegion" runat="server">
	<div id="MainBody">
	    <Scanweb:SquaredCornerImage ID="SquaredCornerImage1" ImagePropertyName="ContentTopImage" 
                                 TopCssClass="RoundedCornersTop472" 
                                 ImageCssClass="RoundedCornersImage472" 
                                 BottomCssClass="RoundedCornersBottom472" ImageWidth="472" runat="server" />
	    <Scanweb:MainBody runat="server" />
	    <div id="ContactFormArea">
	        <Scanweb:XForm
	            runat="server" 
	            XFormProperty="XForm"
	            HeadingProperty="XFormHeading"
	            ShowStatistics="false" />
	    </div>
	    
    </div>
</asp:Content>