using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.Core;
using Scandic;
using Scandic.Scanweb.CMS.Util;


namespace Scandic.Scanweb.CMS.Templates.Util
{
    /// <summary>
    /// TradedoublerRedirect
    /// </summary>
    public partial class TradedoublerRedirect : ScandicTemplatePage
    {
        /// <summary>
        /// Checks if the Tradedoubler method should be run
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["tduid"] != null)
                HandleTradedoubler();
            else
                HandleDeepLinking();            
        }

        /// <summary>
        /// Handles URL:s from Tradedoubler and Redirect the user to the 
        /// right page in the booking process
        /// </summary>
        protected void HandleTradedoubler()
        {
            string defaultUrl = "http://www.scandic-hotels.se/";
            string domain = "scandic-hotels.se";
            if (Request.QueryString["tduid"] != null)
            {
                string cookieDomain = "." + domain;
                HttpCookie cookie = new HttpCookie("TRADEDOUBLER");
                cookie.Value = Request.QueryString["tduid"];
                cookie.Expires = DateTime.Now.AddDays(365);
                cookie.Domain = cookieDomain;
                cookie.Path = "/";
                Response.Cookies.Add(cookie);
                Session["TRADEDOUBLER"] = Request.QueryString["tduid"];
            }

            string url;
            if (Request.QueryString["url"] != null && Request.QueryString["url"] != "")
            {
                System.IO.StringWriter writer = new System.IO.StringWriter();
                url = Request.RawUrl.Substring(Request.RawUrl.IndexOf("url") + 4);
                Server.UrlDecode(url, writer);
                url = writer.ToString();
            }
            else
            {
                url = defaultUrl;
            }

            lblStatus.Text = "Tradedoublerl�nk";
        }

        /// <summary>
        /// Evaluates the querystring (via GetBookingPageUrl() in the BookingEngineDeeplinking.cs) 
        /// and redirect the user to the right page in the booking process where the correct 
        /// values have been pre-populated from the querystring parameters.
        /// </summary>
        protected void HandleDeepLinking()
        {
            string url = string.Empty;
            lblStatus.Text = "Deeplinking-l�nk";
        }

        /// <summary>
        /// Testmetod f�r att testa den globala felhanteringen som ligger ScandiTemplatePage.cs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SendError(object sender, System.EventArgs e)
        {
            ShowApplicationError("Application error", "Det h�r felet kommer fr�n Tradedoubler");
        }
    }
}
