<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Util.DeepLinkingRedirect"
    CodeBehind="DeepLinkingRedirect.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWide.master" %>

<asp:Content runat="server" ContentPlaceHolderID="ScriptRegion">
    <% if (isViewModifyReservationViaDeeplink)
       { %>
    <form action="/" method="post" name="frmDeeplink">
    <div>
        <input type="hidden" name="isViewModifyReservationViaDeeplink" value="<%= isViewModifyReservationViaDeeplink%>" />
        <input type="hidden" name="viewModifyDeeplinkReservationID" value="<%= viewModifyDeeplinkReservationID%>" />
        <input type="hidden" name="viewModifyDeeplinkLastName" value="<%= viewModifyDeeplinkLastName%>" />
    </div>
    </form>
    <script type="text/javascript" language="JavaScript" >
        
        $().ready(function() {
            $('form[name*="frmDeeplink"]').submit();
        });
    </script>
    <% } %>
</asp:Content>
