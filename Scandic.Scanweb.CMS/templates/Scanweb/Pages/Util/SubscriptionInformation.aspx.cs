//  Description					: SubscriptionInformation                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Util
{
    /// <summary>
    /// Code behind of SubscriptionInformation page.
    /// </summary>
    public partial class SubscriptionInformation : ScandicTemplatePage
    {
    }
}