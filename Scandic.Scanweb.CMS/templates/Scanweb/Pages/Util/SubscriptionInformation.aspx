<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Util.SubscriptionInformation" Codebehind="SubscriptionInformation.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWide.master"%> 
<%@ Register TagPrefix="Scanweb" TagName="MainBody" 	        Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="MainBodyLeftRegion">
    <h2><EPiServer:Property runat="server" PropertyName="InformationHeading" /></h2>
    <Scanweb:MainBody runat="server" />    
    <a href="http://localhost/tyen">Engelska</a><br />
    <a href="http://localhost/tysv">Svenska</a><br />
    <a href="http://localhost/tyda">Danska</a><br />
</asp:Content>