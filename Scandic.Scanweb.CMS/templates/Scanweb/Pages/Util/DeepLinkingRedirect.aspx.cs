//  Description					:   DeepLinkingRedirect                                   //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller.Session.BookingModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Core;
using System.Configuration;
using Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units;
using System.Linq;

namespace Scandic.Scanweb.CMS.Templates.Util
{
    /// <summary>
    /// This class contains members to support deep linking redirect.
    /// </summary>
    public partial class DeepLinkingRedirect : ScandicTemplatePage
    {
        private DeepLinkingEntity entity = new DeepLinkingEntity();
        private const string HOSTNAME_PREFIX = "ScandicSiteHostName";
        protected string DeeplinkPostAction = string.Empty;
        protected bool isViewModifyReservationViaDeeplink;
        protected string viewModifyDeeplinkReservationID;
        protected string viewModifyDeeplinkLastName;
        /// <summary>
        /// OnLoad overload method 
        /// </summary>
        /// <param name="e">
        /// Arguments
        /// </param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            Utility.CleanSession();
            this.ClearUserLocalCookie();
            if (Request.QueryString.Count > 0 &&
                Request.QueryString[QueryStringConstants.QUERY_STRING_PARTNER_ID] != null)
            {
                HygieneSessionWrapper.PartnerID = Request.QueryString[QueryStringConstants.QUERY_STRING_PARTNER_ID] as string;
            }
            if (Reservation2SessionWrapper.CurrentLanguage == null)
            {
                Reservation2SessionWrapper.CurrentLanguage = CurrentPage.LanguageID;
            }
            HandleDeepLinking();
        }

        private void ClearUserLocalCookie()
        {
            if (Request.Cookies[AppConstants.USER_LOCALE_COOKIE] != null)
            {
                HttpCookie userLocalCookie = new HttpCookie(AppConstants.USER_LOCALE_COOKIE);
                userLocalCookie.Expires = DateTime.Now.AddDays(-5d);
                Response.Cookies.Set(userLocalCookie);
            }
        }

        private void MobileDeeplink()
        {
            var queryString = Request.RawUrl.Substring(Request.RawUrl.IndexOf("?"));
            var mobileURl = string.Empty;

            var germanHostKey = string.Format("{0}{1}", HOSTNAME_PREFIX, CurrentPageLanguageConstant.LANGUAGE_GERMANY);
            var germanHostName = ConfigurationManager.AppSettings[germanHostKey] as string;
            var russianHostKey = string.Format("{0}{1}", HOSTNAME_PREFIX, LanguageConstant.LANGUAGE_RUSSIA_RU);
            var russianHostName = ConfigurationManager.AppSettings[russianHostKey] as string;
            if (string.Equals(Request.Url.DnsSafeHost, germanHostName, StringComparison.InvariantCultureIgnoreCase) ||
                string.Equals(Request.Url.DnsSafeHost, russianHostName, StringComparison.InvariantCultureIgnoreCase))
            {
                string appSettingsKey = string.Format("{0}{1}", HOSTNAME_PREFIX, CurrentPageLanguageConstant.LANGUAGE_ENGLISH);
                string englishHostName = ConfigurationManager.AppSettings[appSettingsKey] as string;
                mobileURl = string.Format("http://{0}/mobile/{1}", englishHostName, queryString);
            }
            else
            {
                mobileURl = string.Format("/mobile/{0}", queryString);
            }
            Response.Redirect(mobileURl);
        }


        /// <summary>
        /// Evaluates the querystring (via GetBookingPageUrl() in the BookingEngineDeeplinking.cs) 
        /// and redirect the user to the right page in the booking process where the correct 
        /// values have been pre-populated from the querystring parameters.
        /// </summary>
        protected void HandleDeepLinking()
        {
            DeepLinkingEntity myEntity = CreateDeepLinkingEntity();
            List<string> strIpAddresses = new List<string>();

            bool IsFamilyandFriendsDNumberValid = true;

            if (HttpContext.Current.Request.Headers["X-Forwarded-For"] != null)
                strIpAddresses = HttpContext.Current.Request.Headers["X-Forwarded-For"].Split(',').ToList();

            if (!string.IsNullOrEmpty(myEntity.NC) && WebUtil.IsFamilyandFriendsDnumber(myEntity.NC) &&
                !WebUtil.IsRequestedSourceValidForFamilyAndFriendsBooking(strIpAddresses))
                IsFamilyandFriendsDNumberValid = false;

            bool isMobileBookingPage;
            bool isHomePageRedirection;
            bool requestFromMobile = CMS.Util.MobileRedirect.Wurfl.Instance.CheckMobileRedirection(Request.UserAgent,
                                                                                      Request.Url.DnsSafeHost,
                                                                                      Request.UrlReferrer == null
                                                                                          ? ""
                                                                                          : Request.UrlReferrer.
                                                                                                DnsSafeHost);
            //Logic for Deeplink for View/Modify reservation
            if (!string.IsNullOrEmpty(myEntity.RID) && !string.IsNullOrEmpty(myEntity.LNM))
            {
                if (!requestFromMobile)
                {
                    this.Form.Visible = false;
                    isViewModifyReservationViaDeeplink = true;
                    viewModifyDeeplinkReservationID = myEntity.RID;
                    viewModifyDeeplinkLastName = myEntity.LNM;
                }
                else
                {
                    MobileDeeplink();
                }
            }
            else
            {
                string redirectURL = BookingEngineDeeplinking.GetBookingPageUrl(myEntity, requestFromMobile, out isMobileBookingPage, out isHomePageRedirection);

                //If QS will containf FS=1, redirect to Home Page
                if (string.Equals(myEntity.FS, "1"))
                {
                    redirectURL = GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE);
                    isHomePageRedirection = true;
                }
                else if (!IsFamilyandFriendsDNumberValid && !requestFromMobile)
                {
                    redirectURL = GlobalUtil.GetUrlToPage(EpiServerPageConstants.HOME_PAGE);
                    isHomePageRedirection = true;
                }
                if (!string.IsNullOrEmpty(entity.CMPID))
                {
                    redirectURL = string.Format("{0}?cmpid={1}", redirectURL, entity.CMPID);
                }
                if (isHomePageRedirection)
                {
                    if (redirectURL.Contains("?"))
                        redirectURL = string.Format("{0}{1}", redirectURL, "&RD=0"); // RD=0 is added to prevent the IP based redirection logic from getting executed on Home page
                    else
                        redirectURL = string.Format("{0}{1}", redirectURL, "?RD=0"); // RD=0 is added to prevent the IP based redirection logic from getting executed on Home page

                    if (!IsFamilyandFriendsDNumberValid)
                        redirectURL = string.Format("{0}{1}", redirectURL, "&ErrCode=D1");
                    isHomePageRedirection = false;
                }
                if (isMobileBookingPage)
                {
                    MobileDeeplink();
                }
                else
                    Response.Redirect(redirectURL);
            }
        }

        /// <summary>
        /// Creates a DeepLinkingEntity object and populates it with values from
        /// the QueryString
        /// </summary>
        /// <returns>DeepLinkingEntity object</returns>
        protected DeepLinkingEntity CreateDeepLinkingEntity()
        {
            if (Request.QueryString["HO"] != null)
            {
                if (!string.IsNullOrEmpty(GetEntityHO("HO")))
                    entity.HO = GetEntityHO("HO");
            }
            else if (Request.QueryString["CO"] != null)
            {
                if (!string.IsNullOrEmpty(GetEntityCO("CO")))
                    entity.CO = GetEntityCO("CO");
            }
            else if (Request.QueryString["CUO"] != null)
            {
                if (!string.IsNullOrEmpty(GetEntityCUO("CUO")))
                    entity.CUO = GetEntityCUO("CUO");
            }

            if (Request.QueryString["AD"] != null)
            {
                if (IsValidArrivalDate())
                {
                    entity.AD = GetIntParameter("AD");
                    entity.AM = GetIntParameter("AM");
                    entity.AY = GetIntParameter("AY");
                }
                else
                {
                    entity.AD = System.DateTime.Now.Day;
                    entity.AM = System.DateTime.Now.Month;
                    entity.AY = System.DateTime.Now.Year;
                }
            }

            SetDepartureDate();

            if (Request.QueryString["RN"] != null)
            {
                entity.RN = GetEntityRN("RN");
            }
            else
            {
                entity.RN = 1;
            }

            entity.ListDeepLinkingRooms = new List<DeepLinkingRoomEntity>();
            int noofAdults = 1;
            int noOfchilds = 0;
            string selectedRoomcategory = string.Empty;
            string selectedRatecategory = string.Empty;

            for (int i = 1; i <= entity.RN; i++)
            {
                if (i == 1)
                {
                    DeepLinkingRoomEntity room = new DeepLinkingRoomEntity();
                    if (Request.QueryString["AN"] != null)
                    {
                        room.AN = GetEntityAN("AN");
                    }
                    else if (Request.QueryString["AN1"] != null)
                    {
                        room.AN = GetEntityAN("AN1");
                    }
                    else
                    {
                        room.AN = 1;
                    }
                    if (Request.QueryString["RO"] != null)
                    {
                        room.SelectedRoomCategory = GetRoomOrRateCategoryEntityForSelectRate("RO");
                    }
                    else if (Request.QueryString["RO1"] != null)
                    {
                        room.SelectedRoomCategory = GetRoomOrRateCategoryEntityForSelectRate("RO1");
                    }
                    else
                    {
                        room.SelectedRoomCategory = string.Empty;
                    }
                    if (Request.QueryString["RA"] != null)
                    {
                        room.SelectedRateCategory = GetRoomOrRateCategoryEntityForSelectRate("RA");
                    }
                    else if (Request.QueryString["RA1"] != null)
                    {
                        room.SelectedRateCategory = GetRoomOrRateCategoryEntityForSelectRate("RA1");
                    }
                    else
                    {
                        room.SelectedRateCategory = string.Empty;
                    }
                    if (Request.QueryString["CN"] != null)
                    {
                        room.CN = GetEntityCN("CN");
                    }
                    else if (Request.QueryString["CN1"] != null)
                    {
                        room.CN = GetEntityCN("CN1");
                    }
                    else
                    {
                        room.CN = 0;
                    }
                    noofAdults = room.AN;
                    noOfchilds = room.CN;
                    if (room.CN > 0)
                    {
                        room.ListDeepLinkingChildren = new List<DeepLinkingChildEntity>();
                        for (int j = 1; j <= room.CN; j++)
                        {
                            DeepLinkingChildEntity child = new DeepLinkingChildEntity();
                            if (Request.QueryString["CA" + j.ToString()] != null)
                            {
                                child.Age = GetEntityCA("CA" + j.ToString());
                            }
                            else if (Request.QueryString["CA1" + j.ToString()] != null)
                            {
                                child.Age = GetEntityCA("CA1" + j.ToString());
                            }
                            if (Request.QueryString["CB" + j.ToString()] != null)
                            {
                                child.ChildAccommodationType = GetEntityCB("CB" + j.ToString());
                            }
                            else if (Request.QueryString["CB1" + j.ToString()] != null)
                            {
                                child.ChildAccommodationType = GetEntityCB("CB1" + j.ToString());
                            }
                            else
                            {
                                child.ChildAccommodationType = ChildAccommodationType.SelectBedType;
                            }
                            room.ListDeepLinkingChildren.Add(child);
                        }
                    }
                    entity.ListDeepLinkingRooms.Add(room);
                }
                else
                {
                    DeepLinkingRoomEntity room = new DeepLinkingRoomEntity();
                    room.AN = (Request.QueryString["AN" + i.ToString()] != null)
                                  ? GetEntityAN("AN" + i.ToString())
                                  : noofAdults;
                    room.CN = (Request.QueryString["CN" + i.ToString()] != null)
                                  ? GetEntityCN("CN" + i.ToString())
                                  : noOfchilds;
                    room.SelectedRateCategory = (Request.QueryString["RO" + i.ToString()] != null)
                                                    ? GetRoomOrRateCategoryEntityForSelectRate("RA" + i.ToString())
                                                    : selectedRatecategory;
                    room.SelectedRoomCategory = (Request.QueryString["RA" + i.ToString()] != null)
                                                    ? GetRoomOrRateCategoryEntityForSelectRate("RO" + i.ToString())
                                                    : selectedRoomcategory;
                    if (room.CN > 0)
                    {
                        room.ListDeepLinkingChildren = new List<DeepLinkingChildEntity>();
                        for (int j = 1; j <= room.CN; j++)
                        {
                            DeepLinkingChildEntity child = new DeepLinkingChildEntity();
                            child.Age = (Request.QueryString["CA" + i.ToString() + j.ToString()] != null)
                                            ? GetEntityCA("CA" + i.ToString() + j.ToString())
                                            : 0;
                            child.ChildAccommodationType = (Request.QueryString["CB" + i.ToString() + j.ToString()] !=
                                                            null)
                                                               ? GetEntityCB("CB" + i.ToString() + j.ToString())
                                                               : ChildAccommodationType.SelectBedType;
                            room.ListDeepLinkingChildren.Add(child);
                        }
                    }
                    entity.ListDeepLinkingRooms.Add(room);
                }
            }

            if (Request.QueryString["SC"] != null)
            {
                try
                {
                    entity.SC = Request.QueryString["SC"];
                }
                catch (ArgumentOutOfRangeException aore)
                {
                    Response.Redirect("/");
                }
            }

            if (Request.QueryString["NC"] != null)
                entity.NC = GetEntityNC("NC").ToUpper();
            if (Request.QueryString["BC"] != null)
                entity.BC = GetBonusChequeValue();
            if (Request.QueryString["VC"] != null)
            {
                try
                {
                    entity.VC = Request.QueryString["VC"];
                }
                catch (ArgumentOutOfRangeException aore)
                {
                    Response.Redirect("/");
                }
            }

            if (Request.QueryString["RB"] != null)
            {
                try
                {
                    entity.RB = Request.QueryString["RB"];
                }
                catch (ArgumentOutOfRangeException aore)
                {
                    Response.Redirect("/");
                }
            }
            entity.DeepLinkingPageId = 0;
            if (Request.QueryString["DLPAGEID"] != null)
            {
                int num = 0;
                bool res = int.TryParse(Request.QueryString["DLPAGEID"].ToString(), out num);
                if (res == true && num > 0)
                {
                    entity.DeepLinkingPageId = num;
                }
                else
                {
                    entity.DeepLinkingPageId = -1;
                }
            }
            if (Request.QueryString["cmpid"] != null)
            {
                string cmpid = GetEntityCMPID("cmpid");
                if (!string.IsNullOrEmpty(cmpid))
                    entity.CMPID = cmpid;
            }

            if (Request.QueryString["RID"] != null)
            {
                string rid = GetEntityCMPID("RID");
                if (!string.IsNullOrEmpty(rid))
                    entity.RID = rid;
            }

            if (Request.QueryString["LNM"] != null)
            {
                string lnm = GetEntityCMPID("LNM");
                if (!string.IsNullOrEmpty(lnm))
                    entity.LNM = lnm;
            }

            if (Request.QueryString["FS"] != null)
            {
                string fs = GetEntityCMPID("FS");
                if (!string.IsNullOrEmpty(fs))
                    entity.FS = fs;
            }

            if (Request.QueryString["cmpid"] != null && string.Equals(Request.QueryString["cmpid"].ToString(),
                ConfigurationManager.AppSettings["AffiliatedToTrivago"].ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                string trivagohost = string.Empty;
                string bucketid = string.Empty;
                string domain = string.Empty;
                if (Request.UrlReferrer != null)
                    trivagohost = Request.UrlReferrer.Host;
                if (!string.IsNullOrEmpty(trivagohost) && trivagohost.Split('.').Length > 1)
                {
                    domain = trivagohost.Split('.')[trivagohost.Split('.').Length - 1].ToString();
                    if (Request.Cookies["TrivagoDomain"] != null)
                    {
                        WebUtil.RemoveCookie("TrivagoDomain");
                        WebUtil.StoreCookie("TrivagoDomain", domain, "30");
                    }
                    else
                    {
                        WebUtil.StoreCookie("TrivagoDomain", domain, "30");
                    }
                }
                bucketid = Request.QueryString["bucket"] != null ? Convert.ToString(Request.QueryString["bucket"]) : string.Empty;
                if (!string.IsNullOrEmpty(bucketid))
                {
                    if (Request.Cookies["TrivagoBucketID"] != null)
                    {
                        WebUtil.RemoveCookie("TrivagoBucketID");
                        WebUtil.StoreCookie("TrivagoBucketID", bucketid, "30");
                    }
                    else
                    {
                        WebUtil.StoreCookie("TrivagoBucketID", bucketid, "30");
                    }

                }
            }

            return entity;
        }

        private string GetEntityCMPID(string parameter)
        {
            string returnValue = string.Empty;
            returnValue = Request.QueryString[parameter];
            return returnValue;
        }

        /// <summary>
        /// Set the Departure Date
        /// </summary>
        protected void SetDepartureDate()
        {
            TimeSpan spanDuration;
            DateTime localDeparture = new DateTime();
            int noNights = 1;
            if (Request.QueryString["NN"] != null)
            {
                try
                {
                    noNights = Convert.ToInt16(Request.QueryString["NN"].ToString());
                    if (noNights > 0 && noNights < 99)
                    {
                        entity.NN = noNights;
                        localDeparture = entity.ArrivalDate.AddDays(noNights);
                        entity.DD = localDeparture.Day;
                        entity.DM = localDeparture.Month;
                        entity.DY = localDeparture.Year;
                    }
                    else
                    {
                        if (IsValidDepartureDate())
                        {
                            spanDuration = GetDepartureDate().Subtract(entity.ArrivalDate);
                            if (spanDuration.Days > 1 && spanDuration.Days < 99)
                                noNights = spanDuration.Days;
                        }
                        if (noNights < 99)
                            localDeparture = entity.ArrivalDate.AddDays(noNights);
                        else
                        {
                            entity.AD = System.DateTime.Now.Day;
                            entity.AM = System.DateTime.Now.Month;
                            entity.AY = System.DateTime.Now.Year;
                            localDeparture = new DateTime(entity.AY, entity.AM, entity.AD);
                            noNights = 1;
                            localDeparture = entity.ArrivalDate.AddDays(1);
                        }

                        entity.DD = localDeparture.Day;
                        entity.DM = localDeparture.Month;
                        entity.DY = localDeparture.Year;
                        entity.NN = noNights;
                    }
                }
                catch
                {
                    localDeparture = entity.ArrivalDate.AddDays(1);
                    entity.DD = localDeparture.Day;
                    entity.DM = localDeparture.Month;
                    entity.DY = localDeparture.Year;
                }
            }
            else
            {
                if (IsValidArrivalDate() && IsValidDepartureDate())
                {
                    spanDuration = GetDepartureDate().Subtract(entity.ArrivalDate);
                    if (spanDuration.Days > 0 && spanDuration.Days < 99)
                    {
                        noNights = spanDuration.Days;
                        localDeparture = entity.ArrivalDate.AddDays(noNights);
                    }
                    else
                    {
                        entity.AD = System.DateTime.Now.Day;
                        entity.AM = System.DateTime.Now.Month;
                        entity.AY = System.DateTime.Now.Year;
                        noNights = 1;
                        localDeparture = new DateTime(entity.AY, entity.AM, entity.AD).AddDays(noNights);
                    }
                }
                else
                {
                    entity.AD = System.DateTime.Now.Day;
                    entity.AM = System.DateTime.Now.Month;
                    entity.AY = System.DateTime.Now.Year;
                    localDeparture = System.DateTime.Now.AddDays(1);
                    noNights = 1;
                }
                entity.DD = localDeparture.Day;
                entity.DM = localDeparture.Month;
                entity.DY = localDeparture.Year;
                entity.NN = noNights;
            }
        }

        /// <summary>
        /// Get the Departure Date
        /// </summary>
        /// <returns>
        /// DateTime
        /// </returns>
        protected DateTime GetDepartureDate()
        {
            int dDay;
            int dMonth;
            int dYear;
            DateTime depDate;
            try
            {
                if (IsValidDepartureDate())
                {
                    dDay = int.Parse(Request.QueryString["DD"]);
                    dMonth = int.Parse(Request.QueryString["DM"]);
                    dYear = int.Parse(Request.QueryString["DY"]);
                    depDate = new DateTime(dYear, dMonth, dDay);
                }
                else
                    depDate = DateTime.Parse(System.DateTime.Now.ToShortDateString());
            }
            catch (Exception)
            {
                return DateTime.Parse(System.DateTime.Now.ToShortDateString());
            }
            return depDate;
        }


        /// <summary>
        /// Returns the City Opera ID from the Querystring
        /// </summary>
        /// <param name="parameter">Querystring parameter</param>
        /// <returns>City Opera ID as string</returns>
        protected string GetEntityCO(string parameter)
        {
            try
            {
                return ContentDataAccess.GetValidDestinationID(Request.QueryString[parameter].ToUpper());
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Returns the Country Code ID from the Querystring
        /// </summary>
        /// <param name="parameter">Querystring parameter</param>
        /// <returns>Country Code ID as string</returns>
        protected string GetEntityCUO(string parameter)
        {
            try
            {
                return ContentDataAccess.GetValidCountryCode(Request.QueryString[parameter].ToUpper());
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Returns the Hotel Opera ID from the Querystring
        /// </summary>
        /// <param name="parameter">Querystring parameter</param>
        /// <returns>Hotel Opera ID as string</returns>
        protected string GetEntityHO(string parameter)
        {
            try
            {
                return ContentDataAccess.GetValidDestinationID(Request.QueryString[parameter]);
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Common method for converting a Querystring parameter to int and return it.
        /// </summary>
        /// <param name="param">Querystring parameter</param>
        /// <returns>Parameter as int</returns>
        protected int GetIntParameter(string param)
        {
            try
            {
                return Convert.ToInt16(Request.QueryString[param].ToString());
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Returns the number of nights provided in the Querystring
        /// </summary>
        /// <param name="parameter">Querystring parameter</param>
        /// <returns>Number of nights as string</returns>
        protected int GetEntityNN(string parameter)
        {
            try
            {
                int retVal = Convert.ToInt16(Request.QueryString[parameter].ToString());
                if (retVal < 0 || retVal > 99)
                    retVal = 1;
                return retVal;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Returns number of rooms, will always be 1
        /// </summary>
        /// <param name="parameter">Querystring parameter</param>
        /// <returns>Number of rooms as int (always 1)</returns>
        protected int GetEntityRN(string parameter)
        {
            int returnValue = 1;
            if (Int32.TryParse(Request.QueryString[parameter].ToString(), out returnValue))
            {
                if (returnValue < 1 || returnValue > 4)
                {
                    returnValue = 1;
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Returns the number of adults
        /// </summary>
        /// <param name="parameter">Querystring parameter</param>
        /// <returns>Number of adults as string</returns>
        protected int GetEntityAN(string parameter)
        {
            try
            {
                int retVal = Convert.ToInt16(Request.QueryString[parameter].ToString());
                if (retVal < 1 || retVal > 6)
                    retVal = 1;
                return retVal;
            }
            catch
            {
                return 1;
            }
        }

        /// <summary>
        /// Returns the number of children
        /// </summary>
        /// <param name="parameter">Querystring parameter</param>
        /// <returns>NUmber of children as string</returns>
        protected int GetEntityCN(string parameter)
        {
            try
            {
                int retVal = Convert.ToInt16(Request.QueryString[parameter].ToString());
                if (retVal < 0 || retVal >= 6)
                    retVal = 0;
                return retVal;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the entity CA.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        private int GetEntityCA(string parameter)
        {
            try
            {
                int retVal = Convert.ToInt16(Request.QueryString[parameter].ToString());
                if (retVal < 0 || retVal > 12)
                    retVal = 0;
                return retVal;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the entity CB.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        private ChildAccommodationType GetEntityCB(string parameter)
        {
            string CIPB = ChildAccommodationType.CIPB.ToString();
            string CRIB = ChildAccommodationType.CRIB.ToString();
            string XBED = ChildAccommodationType.XBED.ToString();

            ChildAccommodationType cb = ChildAccommodationType.CRIB;
            string bedType = string.Empty;
            bedType = Request.QueryString[parameter].ToString().ToUpper();

            if (0 == string.Compare(bedType, CIPB, true))
            {
                cb = ChildAccommodationType.CIPB;
            }
            else if (0 == string.Compare(bedType, XBED, true))
            {
                cb = ChildAccommodationType.XBED;
            }
            else if (0 == string.Compare(bedType, CRIB, true))
            {
                cb = ChildAccommodationType.CRIB;
            }
            return cb;
        }

        /// <summary>
        /// Returns corporate code
        /// </summary>
        /// <param name="parameter">Querystring parameter</param>
        /// <returns>Corporate code as string</returns>
        protected string GetEntityNC(string parameter)
        {
            try
            {
                return Request.QueryString[parameter] != null ? Request.QueryString[parameter].ToString() : string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// GetRoomOrRateCategoryEntityForSelectRate
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        protected string GetRoomOrRateCategoryEntityForSelectRate(string parameter)
        {
            try
            {
                return Request.QueryString[parameter] != null ? Request.QueryString[parameter].ToString() : string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// GetSelectedRateEntity
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        protected double GetSelectedRateEntity(string parameter)
        {
            try
            {
                return Request.QueryString[parameter] != null
                           ? System.Convert.ToDouble(Request.QueryString[parameter])
                           : 0;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Checks the QueryString for the parameter BC and it�s value. If it exists and
        /// has the value "Y", the retun value is true.
        /// </summary>
        /// <returns>Result as bool</returns>
        protected bool GetBonusChequeValue()
        {
            try
            {
                if (Request.QueryString["BC"].ToUpper() == "Y")
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Checks wether the arrival date from the QueryString is valid or not
        /// </summary>
        /// <returns>Result as bool</returns>
        protected bool IsValidArrivalDate()
        {
            bool isValid;
            int aDay;
            int aMonth;
            int aYear;
            DateTime dt;
            try
            {
                if (Request.QueryString["AD"] != null && Request.QueryString["AM"] != null &&
                    Request.QueryString["AY"] != null)
                {
                    aDay = int.Parse(Request.QueryString["AD"]);
                    aMonth = int.Parse(Request.QueryString["AM"]);
                    aYear = int.Parse(Request.QueryString["AY"]);

                    dt = new DateTime(aYear, aMonth, aDay);
                    if (dt < System.DateTime.Today.AddDays(AppConstants.ALLOWED_DAYS_FOR_DATE_OF_ARRIVAL))
                        isValid = true;
                    else
                        isValid = false;
                    if (System.DateTime.Now > dt)
                        isValid = false;
                    else
                        isValid = true;
                }
                else
                    throw new FormatException();
            }
            catch (FormatException)
            {
                return false;
            }
            catch (ArgumentOutOfRangeException)
            {
                return false;
            }
            return isValid;
        }

        /// <summary>
        /// Checks wether the departure date from the QueryString is valid or not
        /// </summary>
        /// <returns>Result as bool</returns>
        protected bool IsValidDepartureDate()
        {
            int dDay;
            int dMonth;
            int dYear;
            DateTime dt;
            try
            {
                if (Request.QueryString["DD"] != null && Request.QueryString["DM"] != null &&
                    Request.QueryString["DY"] != null)
                {
                    dDay = int.Parse(Request.QueryString["DD"]);
                    dMonth = int.Parse(Request.QueryString["DM"]);
                    dYear = int.Parse(Request.QueryString["DY"]);
                    dt = new DateTime(dYear, dMonth, dDay);
                    if (dt <= System.DateTime.Today.AddDays(AppConstants.ALLOWED_DAYS_FOR_DATE_OF_ARRIVAL))
                        return true;
                    else
                        return false;
                }
                else
                    throw new FormatException();
            }
            catch (FormatException)
            {
                return false;
            }
            catch (ArgumentOutOfRangeException)
            {
                return false;
            }
        }
    }
}