<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Util.NewsletterSubscription" Codebehind="NewsletterSubscription.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWide.master"%> 
<%@ Register TagPrefix="Scanweb" TagName="MainBody" 	        Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="PagePuff"             Src="~/Templates/Scanweb/Units/Placeable/PagePuff.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="InfoBox"              Src="~/Templates/Scanweb/Units/Static/InfoBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="RightColumn"          Src="~/Templates/Scanweb/Units/Placeable/RightColumn.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="LeftColumn"           Src="~/Templates/Scanweb/Units/Placeable/LeftColumn.ascx" %>
<%@ Register TagPrefix="Booking" TagName="Module" 	            Src="~/Templates/Booking/Units/BookingModuleSmall.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="Newsletter"           Src="~/Templates/Scanweb/Units/Placeable/NewsletterSubscription.ascx" %>

<asp:Content ContentPlaceHolderID="LeftColumnRegion" runat="server">
    <Scanweb:LeftColumn runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="MainBodyLeftRegion" runat="server">       
    <Scanweb:SquaredCornerImage ID="SquaredCornersImage1" ImagePropertyName="ContentTopImage" 
                                 TopCssClass="RoundedCornersTop472" 
                                 ImageCssClass="RoundedCornersImage472" 
                                 BottomCssClass="RoundedCornersBottom472" ImageWidth="472" runat="server" />
    <asp:PlaceHolder ID="PageMainBodyPlaceHolder" runat="server" Visible="true">                          
        <Scanweb:MainBody ID="MainBody1" runat="server" />
    </asp:PlaceHolder>
    <Scanweb:PagePuff runat="server" FirstPageReference="FirstPuffPage" SecondPageReference="SecondPuffPage" ThirdPageReference="ThirdPuffPage" FourthPageReference="FourthPuffPage" ID="pagePuffsCtrl" />
    <Scanweb:Newsletter ID="Newsletter1" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="MainBodyRightRegion" runat="server">
    <Scanweb:RightColumn runat="server" />
</asp:Content>