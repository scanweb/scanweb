//  Description					:   NewsletterSubscription                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Util
{
    /// <summary>
    /// Code behind of NewsletterSubscription page.
    /// </summary>
    public partial class NewsletterSubscription : ScandicTemplatePage
    {
        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        protected void Page_Load(object sender, EventArgs e)
        {
            pagePuffsCtrl.Visible = (CurrentPage["ShowPuffListing"] != null);
        }

        /// <summary>
        /// Gets required CSS
        /// </summary>
        /// <returns>required CSS</returns>
        protected string GetCSS()
        {
            if (CurrentPage["ShowBookingModule"] != null)
                return "moduleAbsolutePositioning";

            return string.Empty;
        }
    }
}