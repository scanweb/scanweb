<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.ErrorPage" Codebehind="ErrorPage.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWide.master"%> 

<asp:Content runat="server" ContentPlaceHolderID="MainBodyLeftRegion">
 <%--   <div>
        <div class="errorimage"><img src="/Templates/Scanweb/Styles/Default/Images/Icons/red_alert_icon.gif" /></div>
        <div class="errorheader"><asp:Label runat="server" ID="lblHeader" /></div>
        <div class="errormessage"><asp:Label runat="server" ID="lblMessage" /></div>
    </div>
    <div class="errorlinklistcontainer">
        <asp:PlaceHolder ID="LastLinkPlageHolder" runat="server">
            <div class="LinkListItem">
                <div class="NotLastLink">
                    <a class="IconLink" href="javascript:history.back()"><%= Translate("/Templates/Scanweb/Pages/Errorpage/previousscreen")%></a>
                </div>
            </div>
        </asp:PlaceHolder>
        <div class="LinkListItem">
            <div class="LastLink">
                <a class="IconLink" href="<%=GetURL()%>"><%= Translate("/Templates/Scanweb/Pages/Errorpage/homepage")%></a>
            </div>
        </div>
    </div>--%>
     
    <div class="scandicErrorMsg">
		<div class="scandicErrorMsgContent">
			<h3><asp:Label runat="server" ID="lblHeader" /></h3>
			<p><asp:Label runat="server" ID="lblMessage" /></p>
			<p>
			<a class="IconLink" href="javascript:history.back()"><%= Translate("/Templates/Scanweb/Pages/Errorpage/previousscreen") %></a>
			</p>
			<p>
            <a class="IconLink" href="<%= GetURL() %>"><%= Translate("/Templates/Scanweb/Pages/Errorpage/homepage") %></a>
            </p>
            <%-- START Patch7:Itr8:Sateesh Chandolu :Artifact artf1275204 : Improved Application Error page--%>
            <p>
            <asp:Label runat="server" ID="lblExMessage" />
            <asp:Label runat="server" ID="lblTimeStamp" />
            </p>
            <%-- END Patch7:Itr8:Sateesh Chandolu :Artifact artf1275204 : Improved Application Error page--%>
		</div>
	</div>
</asp:Content>