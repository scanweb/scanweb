using System;
using System.Web;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Error Page
    /// </summary>
    public partial class ErrorPage : ScandicTemplatePage
    {
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        protected void Page_Load(object sender, EventArgs e)
        {
            bool isMobileError = false;
            Exception exeption = null;
            if (MiscellaneousSessionWrapper.IsMobileError) 
            {
                isMobileError = MiscellaneousSessionWrapper.IsMobileError;
                MiscellaneousSessionWrapper.IsMobileError = false;
            }
            if (!isMobileError && string.Equals(Request.QueryString["errorCode"], "500"))
            {
                Response.Redirect("/404/500.aspx");
            }
            if (MiscellaneousSessionWrapper.ErrorPageError != null)
            {
                AppLogger.LogCustomException((Exception)MiscellaneousSessionWrapper.ErrorPageError, AppConstants.APPLICATION_EXCEPTION);
                exeption = (Exception)MiscellaneousSessionWrapper.ErrorPageError;
                MiscellaneousSessionWrapper.ErrorPageError = null;
            }

            if (HttpContext.Current.Session != null && HttpContext.Current.Session.IsNewSession)
            {
                string cookieHeader = HttpContext.Current.Request.Headers["Cookie"];
                if (!string.IsNullOrEmpty(cookieHeader) && cookieHeader.IndexOf("ASP.NET_SessionId") >= 0)
                {
                    PageData page = GetPage(PageReference.RootPage);
                    string url = GetPage((PageReference) page["SessionTimeoutPage"]).LinkURL;
                    Server.Transfer(url);
                }
            }

            if (Response.StatusCode == 200 && GenericErrorPageSessionWrapper.GenericErrorHeader == null && !IsPostBack && !isMobileError)
            {
                if (IsEditOrPreviewMode)
                {
                    MiscellaneousSessionWrapper.ExceptionMessage = null;
                    MiscellaneousSessionWrapper.ExceptionTime = null;
                    lblHeader.Text = Translate("/Templates/Scanweb/Pages/Errorpage/notfoundheader");
                    lblMessage.Text = Translate("/Templates/Scanweb/Pages/Errorpage/notfoundmessage");
                }
                else
                {
                    Response.Redirect("/");
                }
            }
            else
            {
                if (GenericErrorPageSessionWrapper.GenericErrorHeader != null)
                {
                    lblHeader.Text = GenericErrorPageSessionWrapper.GenericErrorHeader;
                    Session.Remove("ErrorHeader");
                }
                if (GenericErrorPageSessionWrapper.GenericErrorMessage != null)
                {
                    lblMessage.Text = GenericErrorPageSessionWrapper.GenericErrorMessage;
                    exeption = new Exception(GenericErrorPageSessionWrapper.GenericErrorMessage);
                    Session.Remove("ErrorMessage");
                }
            }
            if (MiscellaneousSessionWrapper.ExceptionMessage != null)
            {
                lblExMessage.Text = "Exception Message : " + MiscellaneousSessionWrapper.ExceptionMessage + "<br/>";
                exeption = new Exception(GenericErrorPageSessionWrapper.GenericErrorHeader);
                Session.Remove("ExceptionMessage");
            }
            if (MiscellaneousSessionWrapper.ExceptionTime != null)
            {
                lblTimeStamp.Text = "Exception Time : " + MiscellaneousSessionWrapper.ExceptionTime;
                Session.Remove("ExceptionTime");
            }
            if (isMobileError)
            {
                var errorPath = GlobalUtil.GetUrlToPage(ScanwebMobile.ERROR_PAGE_CMS_REFERENCE);
                Response.Redirect(errorPath);
            }
            if (TrackerConstants.Is_Tracker_Enabled)
            {
                lblExMessage.Text = string.Format(TrackerConstants.SESSION_IDENTITY_FORMAT, lblExMessage.Text,
                        string.Format(TrackerConstants.SESSION_IDENTITY, UNTSessionWrapper.UserActionStore.SessionIdentifier));
                UserNavTracker.LogAndClearTrackedData(exeption != null ? exeption : new Exception("Generic Error"));
            }
        }

        /// <summary>
        /// Get URL
        /// </summary>
        /// <returns>Url</returns>
        protected string GetURL()
        {
            PageData startPage = DataFactory.Instance.GetPage(PageReference.StartPage,
                                                              EPiServer.Security.AccessLevel.NoAccess);
            UrlBuilder url = new UrlBuilder(startPage.LinkURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, startPage.PageLink, System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }
    }
}