﻿//  Description					:   SessionTimeoutPage                                    //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


using System;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Code behind of Session timeout page. 
    /// </summary>
    public partial class SessionTimeoutPage : ScandicTemplatePage
    {
        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string siteLanguage = EPiServer.Globalization.ContentLanguage.SpecificCulture.Parent.Name.ToUpper();
            if (!siteLanguage.Equals(LanguageConstant.LANGUAGE_FINNISH) && !siteLanguage.Equals(LanguageConstant.LANGUAGE_ENGLISH))
                lblerrorMessage.Text = " " + WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/sessiontimeout/errormessage3");

            string sessionTimeOutPageReferenceKey = Request.QueryString[QueryStringConstants.SESSION_TIMEOUT_PAGE_REFERENCEKEY];
            lblerrorMsg.Text = WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/sessiontimeout/errormessage1");
            
            if (!string.IsNullOrEmpty(sessionTimeOutPageReferenceKey) &&
                sessionTimeOutPageReferenceKey.Equals(PaymentConstants.PaymentProcessingPageReferenceKeyValue, StringComparison.InvariantCultureIgnoreCase))
            {
                lblerrorMsg.Text = string.Format("{0}<br/><br/> {1}", lblerrorMsg.Text,
                                                 WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/sessiontimeout/PaymentNotCompletedInfoMessage"));
            }
        }
    }
}