﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SessionTimeoutPage.aspx.cs" Inherits="Scandic.Scanweb.CMS.Templates.SessionTimeoutPage" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWide.master" %>
<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainBodyLeftRegion">
    <div class="scandicErrorMsg">
		<div class="scandicErrorMsgContent">			
			<h3><%= WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/sessiontimeout/errorheader") %></h3>
			<p>
			    <asp:Label runat="server" ID="lblerrorMsg"></asp:Label>
			</p>
			<p><%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/sessiontimeout/errormessage2") %><a href="/."><%=
                WebUtil.GetTranslatedText("/scanweb/bookingengine/errormessages/sessiontimeout/redirecttostart") %></a><asp:Label ID ="lblerrorMessage" runat="server"></asp:Label>.</p>
		</div>
	</div>
</asp:Content>