﻿using System;
using System.Collections.Generic;
using System.Web;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Entity;
using System.Text;
using System.Collections;
using EPiServer.SpecializedProperties;
using System.Linq;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;
using System.Runtime.Serialization;
using Scandic.Scanweb.BookingEngine.Web;

namespace Scandic.Scanweb.CMS.Templates.Scanweb.Pages
{
    public class CountryCityDestinations
    {
        private AvailabilityController availabilityController = null;
        private static CountryCityDestinations countryCityDestinations = null;
        private AvailabilityController m_AvailabilityController
        {
            get{if (availabilityController == null)availabilityController = new AvailabilityController();
                return availabilityController;}
        }

        public static CountryCityDestinations GetCountryCityDestinationsInstance()
        {
            if(countryCityDestinations ==null)
                countryCityDestinations = new CountryCityDestinations();

            return countryCityDestinations;
        }

        public List<CountryDestinatination> GetCountryCityDestinations(LinkItemCollection linkCollection,string pageName)
        {
            List<Scandic.Scanweb.Core.CountryDestinatination> countryCityList;
            countryCityList =DeepCopy(m_AvailabilityController.FetchAllCountryDestinationsForAutoSuggest(false,false));
          
            List<Scandic.Scanweb.Core.CountryDestinatination> cityList = new List<CountryDestinatination>();
            switch (pageName)
            {
                case  EpiServerPageConstants.FIND_YOUR_DESTINATION:
                    foreach (CountryDestinatination countryDestinatination in countryCityList)
                    {
                        string countryName = null;
                        string countryNameNull = null;
                        countryDestinatination.DisplayName = string.Empty;
                        countryName = WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/HotelsIn", Utility.GetCurrentLanguage()) + countryDestinatination.Name;
                        countryNameNull = countryName +"(" + WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Static/FindHotelSearch/NoHotelsInCountry",Utility.GetCurrentLanguage()) + ")";
                        
                        if (countryDestinatination.SearchedCities.Count == 0)
                            countryDestinatination.DisplayName = countryNameNull;
                        else
                            countryDestinatination.DisplayName = countryName;

                        foreach (CityDestination cityDestination in countryDestinatination.SearchedCities)
                        {
                            cityDestination.SearchedHotels.ForEach(a => cityDestination.SearchedHotels.Remove(a));
                        }
                    }
                    GetCountrySortOrder(countryCityList, linkCollection);
                    countryCityList = countryCityList.OrderBy(x => x.SortOrder == -1).ThenBy(x => x.SortOrder).ToList();
                    break;
                case EpiServerPageConstants.CITY_LANDING:
                    foreach (CountryDestinatination countryDestinatination in countryCityList)
                    {
                        if (countryDestinatination.CountryID == FindAHotelSessionVariablesSessionWrapper.CountryCode)
                        {
                            countryDestinatination.SearchedCities.OrderBy(x => x.OperaDestinationId).ToList();
                            foreach (CityDestination cityDestination in countryDestinatination.SearchedCities)
                            {
                                cityDestination.SearchedHotels.ForEach(a => cityDestination.SearchedHotels.Remove(a));
                            }
                            cityList.Add(countryDestinatination);
                        }
                    }
                    countryCityList = cityList;
                    break;
                case EpiServerPageConstants.COUNTRY_LANDING:
                    foreach (CountryDestinatination countryDestinatination in countryCityList)
                    {
                        countryDestinatination.SearchedCities.OrderBy(x => x.Name).ToList();
                        foreach (CityDestination cityDestination in countryDestinatination.SearchedCities)
                        {
                            cityDestination.SearchedHotels.ForEach(a => cityDestination.SearchedHotels.Remove(a));
                        }
                    }
                    break;
            }
            return countryCityList;
        }
        
       
        private void GetCountrySortOrder(List<CountryDestinatination>  countryCityList, LinkItemCollection linkCollection)
        {
            if (linkCollection != null)
            {
                for (int i = 0; i < linkCollection.Count; i++)
                {
                    foreach (CountryDestinatination countryDestination in countryCityList)
                    {
                        if (linkCollection[i].Text == countryDestination.Name)
                            countryDestination.SortOrder = i;
                    }
                }
            }
        }

        public  T DeepCopy<T>(T obj)
        {
            IFormatter formatter = new BinaryFormatter();
            formatter.SurrogateSelector = new SurrogateSelector();
            formatter.SurrogateSelector.ChainSelector(
                new NonSerialiazableTypeSurrogateSelector());
            var ms = new MemoryStream();
            formatter.Serialize(ms, obj);
            ms.Position = 0;
            return (T)formatter.Deserialize(ms);
        }
    }

    /// <summary>
    /// This class offers the ability to save the fields
    /// of types that don't have the <c ref="System.SerializableAttribute">
    /// SerializableAttribute</c>.
    /// </summary>

    public class NonSerialiazableTypeSurrogateSelector :
        System.Runtime.Serialization.ISerializationSurrogate,
        System.Runtime.Serialization.ISurrogateSelector
    {
        #region ISerializationSurrogate Members

        public void GetObjectData(object obj,
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
        {
            FieldInfo[] fieldInfos = obj.GetType().GetFields(BindingFlags.Instance |
                BindingFlags.Public | BindingFlags.NonPublic);
            foreach (var fi in fieldInfos)
            {
                if (IsKnownType(fi.FieldType)
                    )
                {
                    info.AddValue(fi.Name, fi.GetValue(obj));
                }
                else
                    if (fi.FieldType.IsClass)
                    {
                        info.AddValue(fi.Name, fi.GetValue(obj));
                    }
            }
        }

        public object SetObjectData(object obj,
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context,
            System.Runtime.Serialization.ISurrogateSelector selector)
        {
            FieldInfo[] fieldInfos = obj.GetType().GetFields(
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (var fi in fieldInfos)
            {
                if (IsKnownType(fi.FieldType))
                {
                    //var value = info.GetValue(fi.Name, fi.FieldType);

                    if (IsNullableType(fi.FieldType))
                    {
                        // Nullable<argumentValue>
                        Type argumentValueForTheNullableType =
                            GetFirstArgumentOfGenericType(
                            fi.FieldType);//fi.FieldType.GetGenericArguments()[0];
                        fi.SetValue(obj, info.GetValue(fi.Name,
                            argumentValueForTheNullableType));
                    }
                    else
                    {
                        fi.SetValue(obj, info.GetValue(fi.Name, fi.FieldType));
                    }

                }
                else
                    if (fi.FieldType.IsClass)
                    {
                        fi.SetValue(obj, info.GetValue(fi.Name, fi.FieldType));
                    }
            }

            return obj;
        }
        private Type GetFirstArgumentOfGenericType(Type type)
        {
            return type.GetGenericArguments()[0];
        }
        private bool IsNullableType(Type type)
        {
            if (type.IsGenericType)

                return type.GetGenericTypeDefinition() == typeof(Nullable<>);
            return false;
        }
        private bool IsKnownType(Type type)
        {
            return
                type == typeof(string)
                || type.IsPrimitive
                || type.IsSerializable
                ;
        }
        #endregion

        #region ISurrogateSelector Members
        System.Runtime.Serialization.ISurrogateSelector _nextSelector;
        public void ChainSelector(
            System.Runtime.Serialization.ISurrogateSelector selector)
        {
            this._nextSelector = selector;
        }

        public System.Runtime.Serialization.ISurrogateSelector GetNextSelector()
        {
            return _nextSelector;
        }

        public System.Runtime.Serialization.ISerializationSurrogate GetSurrogate(
            Type type, System.Runtime.Serialization.StreamingContext context,
            out System.Runtime.Serialization.ISurrogateSelector selector)
        {
            if (IsKnownType(type))
            {
                selector = null;
                return null;
            }
            else if (type.IsClass || type.IsValueType)
            {
                selector = this;
                return this;
            }
            else
            {
                selector = null;
                return null;
            }
        }

        #endregion
    }
}
