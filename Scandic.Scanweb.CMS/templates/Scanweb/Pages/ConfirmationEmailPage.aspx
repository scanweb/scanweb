<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmationEmailPage.aspx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages.ConfirmationEmailPage" %>

<%@ Import Namespace="Scandic.Scanweb.BookingEngine.Web" %>
<%@ Register TagPrefix="Scanweb" TagName="AboutOurRates" Src="~/Templates/Booking/Units/RateDescriptionforEMAIL.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="RoomInformation" Src="~/Templates/Booking/Units/ConfirmationPageEmailRoomInfoBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="RoomInformationSummary" Src="~/Templates/Booking/Units/RoomSummaryInfoForEmail.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Confirmation email</title>
    <style type="text/css">
        table
        {
            line-height: normal;
        }
        table td
        {
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
    <table width="540" border="0" align="center" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-size: 0.75em;">
        <tr>
            <td width="9">
                &nbsp;
            </td>
            <td>
                <table width="522" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="24">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <asp:Image ID="logoImage" runat="server" Style="display: block;" Width="149" Height="32"
                                alt="Scandic" />
                        </td>
                    </tr>
                    <tr>
                        <td height="59">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <h1 style="color: #666666; font-size: 22px; padding-bottom: 14px; line-height: normal;
                                margin: 0;" id="mainconfirmationHeading" runat="server">
                            </h1>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <asp:Image ID="h1BorderImage" Style="display: block;" Width="522" Height="3" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td height="14">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="confirmationHeading" runat="server">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <br />
                            <br />
                            <!-- Main reservation info box -->
                            <div id="mainReservationInfoBox" visible="false" runat="server">
                                <table width="522" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:Image ID="boxBigBrd" runat="server" Width="524" Height="5" Style="display: block;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="background-color: #efebef; padding: 0 6px;">
                                                <table width="512" border="0" cellspacing="0" cellpadding="0" style="background: white">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <table width="510" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left" valign="top" style="width: 510px; padding: 15px 0;">
                                                                        <h2 style="color: #333333; font-size: 16px; margin: 0; line-height: normal; padding-left: 15px;
                                                                            border-bottom: 1px solid #cccccc; padding-bottom: 10px;">
                                                                            <span id="mainBookingReservationNumberID" runat="server"></span>
                                                                        </h2>
                                                                        <!--DLRedirect link-->
                                                                        <table width="495" border="0" cellspacing="0" cellpadding="0" style="margin-left: 15px;">
                                                                            <tr>
                                                                                <td height="30">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top" style="padding-bottom: 12px;">
                                                                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/ViewModifyReservationMessage")%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top">
                                                                                    <a id="ancViewModifyDLRedirectLink" runat="server"></a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table width="495" border="0" cellspacing="0" cellpadding="0" style="margin-left: 15px;">
                                                                            <tr>
                                                                                <td height="30">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top" width="166" style="padding-bottom: 12px;">
                                                                                    <strong>
                                                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/namehotelheader") %>
                                                                                    </strong>
                                                                                </td>
                                                                                <td align="left" valign="top" width="300" style="font-size: 15px;">
                                                                                    <span id="hotelName" runat="server"></span>
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top" style="padding-bottom: 12px;">
                                                                                    <strong>
                                                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Checkin") %>
                                                                                    </strong>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <span id="checkInDate" runat="server"></span>
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top" style="padding-bottom: 12px;">
                                                                                    <strong>
                                                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/CheckOut") %>
                                                                                    </strong>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <span id="checkOutDate" runat="server"></span>
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" style="width: 510px; padding: 15px 0; border-top: 1px solid #cccccc;">
                                                                        <table width="495" border="0" cellspacing="0" cellpadding="0" style="margin-left: 15px;">
                                                                            <tr>
                                                                                <td align="left" valign="top" width="166" style="padding-bottom: 12px;">
                                                                                    <strong>
                                                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/noofnights") %>
                                                                                    </strong>
                                                                                </td>
                                                                                <td align="left" valign="top" width="156">
                                                                                    <span id="noOfNights" runat="server">2</span>
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top" style="padding-bottom: 12px;">
                                                                                    <strong>
                                                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/noofrooms") %>
                                                                                    </strong>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <span id="noOfRooms" runat="server">1 </span>
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top" style="padding-bottom: 12px;">
                                                                                    <strong>
                                                                                        <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Occupants") %>
                                                                                    </strong>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <span id="occupantsDetails" runat="server"></span>
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tbody id="roomSummaryInformationGroup" runat="server">
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 510px; padding-bottom: 5px;">
                                                                            <Scanweb:RoomInformationSummary ID="RoomSummaryInformation0" visible="false" runat="server">
                                                                            </Scanweb:RoomInformationSummary>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 510px; padding-bottom: 5px;">
                                                                            <Scanweb:RoomInformationSummary ID="RoomSummaryInformation1" visible="false" runat="server">
                                                                            </Scanweb:RoomInformationSummary>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 510px; padding-bottom: 15px;">
                                                                            <Scanweb:RoomInformationSummary ID="RoomSummaryInformation2" visible="false" runat="server">
                                                                            </Scanweb:RoomInformationSummary>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 510px; padding-bottom: 15px;">
                                                                            <Scanweb:RoomInformationSummary ID="RoomSummaryInformation3" visible="false" runat="server">
                                                                            </Scanweb:RoomInformationSummary>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <p style="float: right; margin-right: 5px;">
                                                                            <strong><span>
                                                                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Total") %>
                                                                            </span></strong>&nbsp;<span id="totalRateString" runat="server" style="font-size: 24px;"></span>&nbsp;
                                                                            <span id="currencyCodeString" runat="server"></span>
                                                                        </p>
                                                                        <!-- 524072: On reservations, there needs to be stated that Total rate is incl. taxes and fees. Just as in the Your stay module.-->
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <div style="float: right; margin-right: 5px; padding-bottom: 10px;">
                                                                            <p id="pCartTerms" runat="server" style="font-weight: 400; margin-top: 5px; padding: 0 0 0 20px;
                                                                                text-align: left; vertical-align: top">
                                                                            </p>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <!--<asp:Image Width="6" Height="500" Style="display: block;" ID="greyBoxRight" runat="server" />-->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:Image runat="server" ID="boxBigBrdBottom" Width="524" Height="5" Style="display: block;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" height="17">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <!-- Room 1 -->
                            <div id="divRoomInfo0" runat="server">
                                <Scanweb:RoomInformation ID="RoomInformation0" visible="false" runat="server">
                                </Scanweb:RoomInformation>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <!--Room2 -->
                            <div id="divRoomInfo1" runat="server">
                                <Scanweb:RoomInformation ID="RoomInformation1" visible="false" runat="server">
                                </Scanweb:RoomInformation>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <!--Room2 -->
                            <div id="divRoomInfo2" runat="server">
                                <Scanweb:RoomInformation ID="RoomInformation2" visible="false" runat="server">
                                </Scanweb:RoomInformation>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <!--Room3 -->
                            <div id="divRoomInfo3" runat="server">
                                <Scanweb:RoomInformation ID="RoomInformation3" visible="false" runat="server">
                                </Scanweb:RoomInformation>
                            </div>
                        </td>
                    </tr>
                    <!-- BEGIN New Implemention -->
                    <div id="aboutOurRateHeaderDiv" runat="server">
                        <tr>
                            <td style="padding-bottom: 15px;">
                                <table cellspacing="0" cellpadding="0" border="0" width="522">
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top">
                                                <asp:Image ID="aboutOurRateTopBorder" runat="server" Width="522" Height="5" Style="height: 5px;
                                                    width: 522px; border-width: 0px; display: block;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" style="width: 505px; padding: 15px 0px; border-left: 1px solid #d4d4d4;
                                                border-right: 1px solid #d4d4d4;">
                                                <h2 id="reservationNumberText" style="margin: 0; padding-left: 16px; padding-right: 16px;
                                                    padding-bottom: 10px; font-size: 16px; line-height: normal; border-bottom: 1px solid #ccc;
                                                    color: #333;">
                                                    <asp:Label ID="aboutOurRateHeading" runat="server"></asp:Label>
                                                </h2>
                                                <table cellspacing="0" cellpadding="0" border="0" style="margin-left: 15px; margin-right: 15px;">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding-top: 15px;">
                                                                <asp:Label ID="aboutOurRateDescription" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <asp:Label ID="AboutRate" runat="server" class="aboutOurRate"></asp:Label>
                                                        <%--<tr>
					                                    <td style="padding-top: 15px; padding-bottom: 15px;" >
					                                        <div style="margin-bottom: 10px;"><strong>Flex</strong></div>
					                                        <div style="height: 5px; background-color: #cc3366;">&#160;</div>
					                                    </td>
					                                </tr>
					                                <tr>
						                                <td>
							                                Book right up to and including the day of arrival. Change or cancel the reservation until 18:00 local time on the day of arrival. Payment in conjunction with stay. Breakfast is included. VAT included. Book right up to and including the day of arrival.
						                                </td>
					                                </tr>--%>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                <asp:Image ID="aboutOurRateBottomBorder" runat="server" Width="522" Height="5" Style="height: 5px;
                                                    width: 522px; border-width: 0px; display: block;" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </div>
                    <!-- END New Implemention-->
                    <%--                <tr>
                        <td>
                            <div id="masterReservation" runat="server" visible="false">
                            </div>
                            <br />
                        </td>
                    </tr>--%>
                    <%--       <tr>
                        <td>
                            <div id="ContactUsText" runat="server">
                            </div>
                        </td>
                    </tr>--%>
                    <!-- BEGIN Help Text for Master Reservation -->
                    <tr>
                        <td>
                            <!-- R2.2 | SJS | artf1162324 -->
                            <div style="padding-bottom: 15px;" id="masterReservation" runat="server" visible="false">
                                <table cellspacing="0" cellpadding="0" border="0" width="522">
                                    <tbody>
                                        <tr>
                                            <td valign="top" align="left">
                                                <asp:Image ID="masterReservationHelpTopBorder" runat="server" Style="height: 4px;
                                                    width: 522px; border-width: 0px; display: block;"></asp:Image>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="515" valign="top" align="left" style="border-left: 1px solid #b2cccc;
                                                border-right: 1px solid #b2cccc; background-color: #DCEEF2; padding-left: 5px;
                                                padding-top: 6px; padding-bottom: 10px;">
                                                <table width="515" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" align="left" style="padding-right: 10px; padding-left: 10px;">
                                                                <asp:Label ID="masterReservationHelpText" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <asp:Image ID="masterReservationHelpBottomBorder" Style="height: 4px; width: 522px;
                                                    border-width: 0px; display: block;" runat="server"></asp:Image>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <!-- END Help Text for Master Reservation -->
                    <!-- BEGIN Help Text for ContactUs -->
                    <tr>
                        <td style="padding-bottom: 15px;">
                            <table cellspacing="0" cellpadding="0" border="0" width="522" id="tblContactUs" runat="server">
                                <tbody>
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:Image ID="contactUsTopBorder" runat="server" Style="height: 5px; width: 522px;
                                                border-width: 0px; display: block;"></asp:Image>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" style="width: 505px; padding: 10px; border-left: 1px solid #b2cccc;
                                            border-right: 1px solid #b2cccc; background-color: #dceef2">
                                            <asp:Label ID="contactUsHelpText" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:Image ID="contactUsBottomBorder" Style="height: 5px; width: 522px; border-width: 0px;
                                                display: block;" runat="server"></asp:Image>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <!-- END Help Text for ContactUs -->
                    <%-- <tr>
                        <td>
                            &nbsp;</td>
                    </tr>--%>
                    <tbody id="rowGroupForConfirmation" runat="server" style="display: block">
                        <tr>
                            <td align="left" valign="top">
                                <div id="SameGuaranteeInformationText" runat="server" visible="false">
                                    <%--<h3 style="color: #333333; font-size: 16px; margin: 0; line-height: normal; padding-left: 15px;
                                        border: none 0px; padding-bottom: 10px;">--%>
                                    <h3 style="color: #333333; font-size: 16px; margin: 0; line-height: normal; border: none 0px;
                                        padding-bottom: 10px;">
                                        <%--Defect fix - artf1148220 - Bhavya - to fetch the header from the CMS--%>
                                        <%--<%=WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/GuaranteeInformation")%>--%>
                                        <asp:Label ID="commonGuaranteeInfoHeader" runat="server"></asp:Label>
                                    </h3>
                                    <%-- <asp:Label ID="commonGuaranteeInfo" runat="server" style="padding-bottom: 5px;"></asp:Label>--%>
                                    <div style="margin-top: 5px;">
                                        <div style="padding-bottom: 5px; list-style: none" runat="server" id="commonGuaranteeInfo">
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" height="30">
                                &nbsp;
                            </td>
                        </tr>
                        <%--
                        <tr>
                            <td align="left" valign="top">
                                <asp:Image ID="infoAlertTop" runat="server" Style="display: block;" Width="522" Height="4" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" width="515" style="border-left: 1px solid #b2cccc;
                                border-right: 1px solid #b2cccc; background-color: #dceef2; padding-left: 5px;
                                padding-top: 6px; padding-bottom: 10px;">
                                <table width="515" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="left" valign="top" width="16">
                                            <asp:Image runat="server" ID="infoAlertIcon" Width="16" Height="17" alt="Info" /></td>
                                        <td align="left" valign="top" style="padding-right: 10px; padding-left: 10px;">
                                            <strong>
                                                <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/PleaseNote") %>
                                            </strong>
                                            <br />
                                            <%=
                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/notetext1") %>
                                            <%=
                WebUtil.GetTranslatedText("/bookingengine/booking/bookingdetail/guaranteepolicytext/notetext2") %>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                <asp:Image ID="infoAlertIconBottom" runat="server" Style="display: block;" Width="522"
                                    Height="4" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" height="40">
                                &nbsp;
                            </td>
                        </tr>--%>
                        <tr>
                            <td align="left" valign="top">
                                <h2 style="color: #333333; font-size: 16px; margin: 0; line-height: normal; padding-left: 0;
                                    border-bottom: 1px solid #cccccc; padding-bottom: 10px;">
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/Abouthotel") %>
                                </h2>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" height="30">
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="font-size: 16px; padding-bottom: 18px;">
                                <span id="mapHeaderHotelname" runat="server"></span><span><a href="#" id="findAHotelLink"
                                    runat="server" style="color: #006699;">&nbsp;(<%= WebUtil.GetTranslatedText("/bookingengine/booking/bookingconfirmation/ShowOnMap") %>)</a>
                                </span>
                            </td>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="font-size: 14px; padding-bottom: 18px;">
                                <strong>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/contactus/locationandcontactinfo") %>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="padding-bottom: 18px;">
                                <span id="linkHotelAddress" runat="server" title="hotel address"></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="padding-bottom: 18px;">
                                <span id="phoneContainer" runat="server"><strong>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/missingpoint/telephone") %>
                                    :&nbsp;</strong> <span id="lblHotelPhoneNum" runat="server"></span>
                                    <br />
                                </span><span id="faxContainer" runat="server"><strong>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/contactus/scandicfaxnumber") %>
                                    :&nbsp;</strong> <span id="lblHotelFaxNum" runat="server"></span>
                                    <br />
                                </span><span id="emailContainer" runat="server"><strong>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/lostcardwithoutlogin/email") %>
                                    :&nbsp;</strong> <span><a id="linkHotelEmail" runat="server" href="#"></a></span>
                                </span>
                                <br />
                                <span id="customerCareNumberContainer" runat="server"><strong>
                                    <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Content/CentralReservationNumber") %>
                                    :&nbsp;</strong> <span id="lblCustomerCareNumber" runat="server"></span></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="padding-bottom: 24px;">
                                <strong>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Coordinates") %>
                                    :&nbsp;</strong>
                                <br />
                                <strong>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Latitude") %>
                                    &nbsp;-&nbsp; </strong><span id="lblLatitude" runat="server"></span>
                                <br />
                                <strong>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/Longitude") %>
                                    &nbsp;-&nbsp; </strong><span id="lblLongitude" runat="server"></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="font-size: 14px; padding-bottom: 14px;">
                                <strong>
                                    <%= WebUtil.GetTranslatedText("/bookingengine/booking/searchhotel/OverView") %>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                <table width="400" border="0" cellspacing="0" cellpadding="0">
                                    <tr id="noOfRoomsContainer" runat="server">
                                        <td align="left" valign="top" width="350" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/NoOfRooms") %>
                                        </td>
                                        <td align="right" valign="top" width="40" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <span id="lblNoOfRooms" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr id="noOfNonSmokingRoomsContainer" runat="server">
                                        <td align="left" valign="top" width="350" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/NonSmokingRooms") %>
                                        </td>
                                        <td align="right" valign="top" width="40" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <span id="lblNoOfNonSmokingRooms" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="350" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RoomsForDisabled") %>
                                        </td>
                                        <td align="right" valign="top" width="40" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <span id="lblRoomsForDisabledAvailable" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="350" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RelaxCenter") %>
                                        </td>
                                        <td align="right" valign="top" width="40" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <span id="lblRelaxCenterAvailable" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="350" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/RestaurantBar") %>
                                        </td>
                                        <td align="right" valign="top" width="40" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <span id="lblRestaurantBar" runat="server" class="fltRt"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="350" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Garage") %>
                                        </td>
                                        <td align="right" valign="top" width="40" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <span id="lblGarage" runat="server" class="fltRt"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="350" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/OutdoorParking") %>
                                        </td>
                                        <td align="right" valign="top" width="40" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <span id="lblOutdoorParking" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="350" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <%= WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/Shop") %>
                                        </td>
                                        <td align="right" valign="top" width="40" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <span id="lblShop" runat="server"></span>
                                        </td>
                                    </tr>
                                    <%--artf1158124 | R2.2 | Lines missing in"Grid" on confirmation e-mail--%>
                                    <tr>
                                        <td align="left" valign="top" width="350" style="padding-top: 5px; padding-bottom: 2px;
                                            border-bottom: 1px solid #cccccc;">
                                            <%=
                WebUtil.GetTranslatedText(
                    "/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/MeetingFacilities") %>
                                        </td>
                                        <td align="right" valign="top" width="40" style="padding-top: 5px; padding-bottom: 2px;
                                            border-bottom: 1px solid #cccccc;">
                                            <span id="lblMeetingFacilities" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr id="ecoLabeledContainer" runat="server">
                                        <td align="left" valign="top" width="350" style="padding-top: 5px; padding-bottom: 2px;
                                            border-bottom: 1px solid #cccccc;">
                                            <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/EcoLabeled") %>
                                        </td>
                                        <td align="right" valign="top" width="40" style="padding-top: 5px; padding-bottom: 2px;
                                            border-bottom: 1px solid #cccccc;">
                                            <span id="lblEcoLabeled" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="350" style="padding-top: 5px; padding-bottom: 2px;
                                            border-bottom: 1px solid #cccccc;">
                                            <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceCentre") %>
                                        </td>
                                        <td align="right" valign="top" width="40" style="padding-top: 5px; padding-bottom: 2px;
                                            border-bottom: 1px solid #cccccc;">
                                            <span id="lblDistanceCentre" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr id="airportDistanceContainer" runat="server">
                                        <td align="left" valign="top" width="350" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceAirport") %>
                                            &nbsp;<span id="lblAirportName" runat="server"></span>
                                        </td>
                                        <td align="right" valign="top" width="40" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <span id="lblDistanceAirport" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr id="trainStationDistContainer" runat="server">
                                        <td align="left" valign="top" width="350" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/DistanceTrain") %>
                                        </td>
                                        <td align="right" valign="top" width="40" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <span id="lblDistanceTrain" runat="server"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="350" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <%=
                WebUtil.GetTranslatedText("/Templates/Scanweb/Pages/HotelLandingPage/Overview/Facility/WirelessInternet") %>
                                        </td>
                                        <td align="right" valign="top" width="40" style="border-bottom: 1px solid #cccccc;
                                            padding-top: 5px; padding-bottom: 2px;">
                                            <span id="lblWirelessInternet" runat="server"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                        <!-- below info not present in confimation page -->
                        <tbody runat="server" id="otherImportantInfoRow">
                            <tr>
                                <td align="left" valign="top">
                                    <h3 style="color: #333333; font-size: 16px; margin: 0; line-height: normal; border: none 0px;
                                        padding-bottom: 10px;">
                                        <div runat="server" id="OtherImportantInformationHeader">
                                        </div>
                                    </h3>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div runat="server" id="OtherImportantInformation">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="didYouKnowRow" runat="server">
                            <tr>
                                <td align="left" valign="top">
                                    <h3 style="color: #333333; font-size: 16px; margin: 0; line-height: normal; border: none 0px;
                                        padding-bottom: 10px;">
                                        <div runat="server" id="DidYouKnowHeader">
                                        </div>
                                    </h3>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div runat="server" id="DidYouKnow">
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </tbody>
                </table>
            </td>
            <td width="9">
                &nbsp;
            </td>
        </tr>
    </table>
</body>
</html>
