//  Description					:   ImageVaultTest2                                       //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer.Core;
using Scandic.Scanweb.CMS.Util;
using Scandic.Scanweb.CMS.Util.ImageVault;
using Scandic.Scanweb.CMS.Util.SMSManager;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// Code behind of ImageVaultTest2 page.
    /// </summary>
    public partial class ImageVaultTest2 : ScandicTemplatePage
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            DataBind();
        }

        /// <summary>
        /// Gets image from image vault.
        /// </summary>
        /// <returns></returns>
        protected PropertyData GetImageFromImageVault()
        {
            PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET", "ImageStoreNET.ImageType");
            string strImage = CurrentPage["TestImage"] as string;

            if (!String.IsNullOrEmpty(strImage))
            {
                pd.Value = strImage;
            }

            return pd;
        }

        /// <summary>
        /// Gets alt text.
        /// </summary>
        /// <returns></returns>
        protected string GetAltText()
        {
            LangAltText altText = new LangAltText();

            return altText.GetAltText(CurrentPage.LanguageID, CurrentPage["TestImage"].ToString());
        }

        /// <summary>
        /// Buttton1 click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            SMSManager smsManager = new SMSManager(TextBox3.Text, TextBox4.Text);
            if (!smsManager.SendSMS())
                Response.Write(smsManager.errMessage);
        }
    }
}