<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MoreImagesPopUpContainer.aspx.cs"
    Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages.MoreImagesPopUpContainer" %>

<%@ Register TagPrefix="Scanweb" TagName="MoreImagesPopup" Src="~/Templates/Scanweb/Units/Static/MoreImagesPopUp.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head1" runat="server">
    <title>
    </title>
    <link rel="stylesheet" type="text/css" media="screen" href="/Templates/Scanweb/Styles/Default/Styles.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/Templates/Scanweb/Styles/Default/HotelLandingPage.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/Templates/Scanweb/Styles/Default/Structure.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/Templates/Scanweb/Styles/Default/Editor.css?v=<%=CmsUtil.GetCSSVersion()%>" />
</head>
<body>
    <form id="Form1" runat="server">
    <Scanweb:MoreImagesPopup ID="MoreImagesPopup1" runat="server" />
    </form>
</body>
</html>
