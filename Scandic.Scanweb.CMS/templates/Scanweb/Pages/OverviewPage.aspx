<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.OverviewPage" CodeBehind="OverviewPage.aspx.cs"
    MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageWideFull.master" %>

<%@ Register TagPrefix="Scanweb" TagName="MainBody" Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="PageList" Src="~/Templates/Scanweb/Units/Placeable/PageList.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="OverviewBox" Src="~/Templates/Scanweb/Units/Placeable/OverviewBox.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="LeftMenu" Src="~/Templates/Scanweb/Units/Placeable/LeftMenu.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="SearchMeeting" Src="~/Templates/Scanweb/Units/Placeable/MeetingSearch.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopAreaFullWidthRegion" runat="server">
    <asp:PlaceHolder ID="TopAreaFullWidthPlaceHolder" Visible="false" runat="server">
        <div id="TopAreaFullWidthArea">
            <EPiServer:Property PropertyName="TopArea" runat="server" />
        </div>
    </asp:PlaceHolder>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainBodyRegion" runat="server">

    <script type="text/javascript">
        $(document).ready(function() {
            var carouselWidth = '<%=CurrentPageCarouselWidth %>';
            var overviewImage = '<%=MainOverviewImage %>';
            if (carouselWidth == "0")  //call Carousel Processing only when Carousel is Left Center with No box.*/
                CarouselProcessing('<%=CurrentPageTypeId%>', '<%=CurrentPageLinkId%>');
            else if (overviewImage == null || overviewImage == "") {
                CarouselProcessing('<%=CurrentPageTypeId%>', '<%=CurrentPageLinkId%>');
            }
        });
    </script>

    <div id="OverviewPageTopArea">
        <div>
            <asp:PlaceHolder ID="TopAreaPlaceHolder" Visible="false" runat="server">
                <div id="OverviewPageTopAreaContent">
                    <div id="TopAreaMediaDiv" runat="server">
                        <episerver:property propertyname="TopArea" runat="server" />
                    </div>
                </div>
            </asp:PlaceHolder>
            <Scanweb:OverviewBox ImagePropertyName="MainOvewImage" ImageWidth="718" StoolImagePropertyName="OverviewStoolImage"
                StoolImageWidth="200" TextPropertyName="OverviewImageTextArea" runat="server"  id="overviewBox"/>
        </div>
        <div id="OverviewPageMainRightArea">
            <asp:PlaceHolder ID="LeftMenuPlaceHolder" Visible="false" runat="server">
                <Scanweb:LeftMenu ID="LeftMenu" runat="server" />
            </asp:PlaceHolder>
            <div class="HotelLocationHeadLine" id="InfoHeader" visible="false" runat="server">
                <h3 class="darkHeading">
                    <%= Translate("/Templates/Scanweb/Pages/HotelLandingPage/Content/MoreInfoHeading") %></h3>
                <episerver:property id="Property3" propertyname="MainBodyRight" runat="server" />
            </div>
            <div id="OverviewPageSecondaryLeftArea">
                <asp:PlaceHolder ID="SecondaryLeftHeadlingPlaceHolder" runat="server">
                    <h3 class="darkHeading">
                        <episerver:property id="Property6" propertyname="SecondaryLeftHeading" runat="server" />
                    </h3>
                </asp:PlaceHolder>
                <episerver:property id="Property7" propertyname="SecondaryLeftBody" runat="server" />
                <Scanweb:PageList ID="SecondaryPageList" PageLinkProperty="ListingContainer" MaxCountProperty="MaxCount"
                    ShowHeading="false" ThumbnailProperty="ThunbnailImage2" runat="server" />
            </div>
        </div>
        <div id="OverviewPageMainArea">
            <asp:PlaceHolder ID="MainIntroPlaceHolder" runat="server">
                <h2 class="clear heading1">
                    <episerver:property id="Property5" propertyname="TopMiddleHeading" runat="server" />
                </h2>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="MainBodyPlaceHolder" runat="server">
                <div id="MainBody">
                    <div class="MainBodyText">
                        <episerver:property id="Property4" propertyname="TopMiddleBody" enableviewstate="false"
                            runat="server" />
                    </div>
                </div>
            </asp:PlaceHolder>
             <div class="purple-divider" id="purple_driver" runat="server">&nbsp</div>
            <div id="MeetingSearch349">
                 <Scanweb:SearchMeeting runat="server" ID="MeetingsSearch" />
            </div>
            <div id="OverviewPageSecondaryRightArea">
                <asp:PlaceHolder ID="SecondaryRightHeadingPlaceHolder" runat="server">
                    <h3 class="darkHeading">
                        <episerver:property id="Property2" propertyname="SecondaryRightHeading" runat="server" />
                    </h3>
                </asp:PlaceHolder>
                <episerver:property id="Property1" propertyname="SecondaryRightBody" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
