<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Pages.Offer" Codebehind="Offer.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageAllOffers.master" %>

<%@ Register TagPrefix="Scanweb" TagName="PopulateBookingModuleInput" Src="~/Templates/Scanweb/Units/Placeable/PopulateBookingModuleInput.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="RightColumnWide" Src="~/Templates/Scanweb/Units/Placeable/RightColumnWide.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="SquaredCornerImage" Src="~/Templates/Scanweb/Units/Placeable/SquaredCornerImage.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="MainBody" Src="~/Templates/Scanweb/Units/Placeable/MainBody.ascx" %>

<asp:Content ContentPlaceHolderID="MainBodyRegion" runat="server">
<script type="text/javascript">
    var addthis_config = { "data_track_addressbar": true };
</script>
 <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50645171248e42f5"></script>
 
        <script type="text/javascript">
            $(document).ready(function() {
                var activateimagecarousel = '<%=ActivateImageCarousel%>';
                if (activateimagecarousel == 'True') {
                    $('#ImageDiv').hide();
                    CarouselProcessing('<%=CurrentPageTypeId%>', '<%=CurrentPageLinkId%>');
                }
            });

  
       </script>
       
       
       
 
   <div id="ImageDiv">
       <Scanweb:SquaredCornerImage ID="SquaredCornersImage1" ImagePropertyName="ContentTopImage"
        TopCssClass="RoundedCornersTop595" ImageCssClass="RoundedCornersImage595 mgnAddthis" BottomCssClass="RoundedCornersBottom595"
        ImageWidth="595" runat="server" />
       
   </div>
   <div id="slides"></div>
    
    <!--START: ADDTHIS PLUGIN -->
    <div id ="addThis" runat="server" class="addthis_toolbox addthis_default_style" style="float: right; margin-bottom: 10px;">
    </div>   
    <!--END: ADDTHIS PLUGIN -->
    <div id="OfferExpiryDiv" class="OfferExpiryDiv" runat="server" visible="false">
        <div class="alertContainer">
        <div class="hd sprite">&nbsp;</div>
        <div class="cnt sprite">
                <span class="alertIconRed">&nbsp;</span>
                <div class="descContainer" id="">                                
                    <h2 id="OfferExpiryText" class="OfferExpiryText" runat="server">We're sorry, we can't find any available hotels. Try searching for another date or city.</h2>
                </div>
            <div class="clearAll">&nbsp;</div>
        </div>
        <div class="ft sprite">&nbsp;</div>
    </div> 
    </div>
    
    <Scanweb:MainBody id="mainBody" runat="server"/>
   <asp:PlaceHolder ID="PriceListOfferPlaceHolder" runat="server">
    <div class="PriceListOfferContentTop"></div>
       <div class="PriceListOfferContent">
            <asp:PlaceHolder ID="CountryListPlaceHolder" runat="server">
       </asp:PlaceHolder>
    </div><div class="PriceListOfferContentBottom" ></div>
    </asp:PlaceHolder>    
</asp:Content>
<asp:Content ContentPlaceHolderID="SecondaryBodyRegion" runat="server">
    <Scanweb:RightColumnWide runat="server" />
     <Scanweb:PopulateBookingModuleInput ID="PopulateBookingModule" runat="server" />
</asp:Content>

<%--<asp:Content ContentPlaceHolderID="ScriptRegion" runat="server">   
   
</asp:Content>--%>