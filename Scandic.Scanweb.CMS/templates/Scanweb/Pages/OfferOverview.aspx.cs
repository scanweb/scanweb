using System;
using System.Web.UI;
using EPiServer;
using EPiServer.Core;
using EPiServer.Security;
using Scandic.Scanweb.CMS.Templates.Scanweb.Units.Placeable;
using Scandic.Scanweb.CMS.Util;

namespace Scandic.Scanweb.CMS.Templates.Pages
{
    /// <summary>
    /// OfferOverview
    /// </summary>
    public partial class OfferOverview : ScandicTemplatePage
    {
        protected UserControl BE;
        private const int itemsPerRow = 3;

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Box4.HideBookNow = true;
            BE.Visible = OWSVisibilityControl.BookingModuleShouldBeVisible;
            BookingModuleAlternativeBox1.Visible = !OWSVisibilityControl.BookingModuleShouldBeVisible;
            int ctrOffer = 1;
            for (int offerPosition = 1; offerPosition <= 6; offerPosition++)
            {
                if (CurrentPage["Offer" + offerPosition] != null)
                {
                    PageData offerPageData =
                        DataFactory.Instance.GetPage(CurrentPage["Offer" + offerPosition] as PageReference);
                    if (offerPageData.CheckPublishedStatus(PagePublishedStatus.Published) &&
                        offerPageData.QueryDistinctAccess(AccessLevel.Read))
                    {
                        if (offerPageData["OfferEndTime"] == null )
                            ctrOffer = ShowOffers(ctrOffer, offerPageData);
                        else if (Convert.ToDateTime(offerPageData["OfferEndTime"]) > DateTime.Now)
                            ctrOffer = ShowOffers(ctrOffer, offerPageData);
                    }
                }
            }
        }

        private int ShowOffers(int ctrOffer, PageData offerPageData)
        {
            PromoBoxOffer promoBoxOffer =
                LoadControl("../Units/Placeable/PromoBoxOfferPage.ascx") as PromoBoxOffer;
            promoBoxOffer.OfferPageLink = offerPageData;

            if (IsMiddleOffer(ctrOffer))
            {
                promoBoxOffer.CssClass = "smallPromoBoxLight centerDiv";
            }
            else
            {
                promoBoxOffer.CssClass = "smallPromoBoxLight storyBox";
            }

            PHSmallOfferArea.Controls.Add(promoBoxOffer);

            ctrOffer += 1;
            return ctrOffer;
        }

        #region PrivateMethod

        /// <summary>
        /// Checks if the position of the offer box is in the middle of the row.
        /// </summary>
        /// <param name="offerPosition"></param>
        /// <returns>True/False</returns>
        private bool IsMiddleOffer(int offerPosition)
        {
            return (offerPosition%itemsPerRow == 2);
        }
        #endregion PrivateMethod
    }
}