<%@ Page Language="c#" Inherits="Scandic.Scanweb.CMS.Templates.Scanweb.Pages.PopUpContainer"
    CodeBehind="PopUpContainer.aspx.cs" %>

<%@ Register TagPrefix="scanweb" TagName="PopUp" Src="~/Templates/Scanweb/Units/Static/PopUp.ascx" %>
<%@ Import Namespace="Scandic.Scanweb.CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head1" runat="server">
    <title>
        <%= CurrentPage.PageName %>
    </title>
    <link rel="stylesheet" type="text/css" media="screen" href="/Templates/Scanweb/Styles/Default/Styles.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/Templates/Scanweb/Styles/Default/Structure.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/Templates/Scanweb/Styles/Default/Editor.css?v=<%=CmsUtil.GetCSSVersion()%>" />
</head>
<body>
    <form id="Form1" runat="server">
    <div class="popupHolder">
        <scanweb:PopUp ID="PopUp1" runat="server" />
    </div>
    </form>
</body>
</html>
