//  Description					:   Global                                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region using

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAccess;
using EPiServer.Filters;
using EPiServer.Globalization;
using EPiServer.Security;
using EPiServer.UI;
using EPiServer.UI.Edit;
using EPiServer.Web;
using EPiServer.Web.WebControls;
using Scandic.Scanweb.BookingEngine.Carousel;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Controller.Entity;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Web.Carousel;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;
using Scandic.Scanweb.CMS.Util.DetectJavascript;
using Scandic.Scanweb.CMS.Util.MailManager;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.Core.Core;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.CMS.DataAccessLayer;

#endregion using

namespace Scandic.Scanweb.CMS
{
    /// <summary>
    /// Global
    /// </summary>
    public class Global : EPiServer.Global
    {
        private static Dictionary<string, string> stringMapping = new Dictionary<string, string>();

        #region Protected Methods

        /// <summary>
        /// Application OnStart
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_OnStart(Object sender, EventArgs e)
        {
            DataFactory.Instance.CreatedPage += new PageEventHandler(OnCreatedPage);
            DataFactory.Instance.CreatingPage += new PageEventHandler(OnCreatingPage);
            DataFactory.Instance.MovedPage += new PageEventHandler(OnMovedPage);
            DataFactory.Instance.MovingPage += new PageEventHandler(OnMovingPage);
            DataFactory.Instance.SavingPage += new PageEventHandler(OnSavingPage);
            DataFactory.Instance.PublishingPage += new PageEventHandler(OnPublishingPage);
            GlobalPageValidation.Validators += new PageValidateEventHandler(GlobalPageValidation_Validators);
            UrlRewriteProvider.ConvertingToInternal += new EventHandler<EPiServer.Web.UrlRewriteEventArgs>(UrlRewriteProvider_ConvertingToInternal);
            Scandic.Scanweb.CMS.Util.OWSAvailability.GetInstance();
            if (!(ConfigurationManager.AppSettings["IsAdminServer"] != null &&
                ((string)ConfigurationManager.AppSettings["IsAdminServer"]) == "true"))
            {
                EPiServer.Web.UrlRewriteModule.HttpRewriteInit += UrlRewriteModule_HttpRewriteInit;
            }
        }

        /// <summary>
        /// This adds the hooks to the UrlRewriter instance
        /// </summary>
        private void UrlRewriteModule_HttpRewriteInit(object sender, UrlRewriteEventArgs e)
        {
            UrlRewriteModuleBase module = sender as UrlRewriteModuleBase;
            if (module != null)
            {
                UrlRewriteProvider.ConvertedToExternal += UrlRewriteProvider_ConvertedToExternal;
            }
        }

        /// <summary> 
        /// Remove language from path if it is the default for the hostname.
        /// </summary> 

        private void UrlRewriteProvider_ConvertedToExternal(object sender, UrlRewriteEventArgs e)
        {
            if (e.IsModified)
            {
                string lang = LanguageSelection.GetLanguageFromHost();
                if ((!string.IsNullOrEmpty(lang)) && e.Url.Path.StartsWith('/' + lang + '/'))
                {
                    e.Url.Path = e.Url.Path.Substring(lang.Length + 1);
                }
            }
        }

        /// <summary>
        /// Application OnEnd
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_OnEnd(Object sender, EventArgs e)
        {
            DataFactory.Instance.CreatedPage -= new PageEventHandler(OnCreatedPage);
            DataFactory.Instance.MovedPage -= new PageEventHandler(OnMovedPage);
            DataFactory.Instance.MovingPage -= new PageEventHandler(OnMovingPage);
            DataFactory.Instance.SavingPage -= new PageEventHandler(OnSavingPage);

            DataFactory.Instance.PublishingPage -= new PageEventHandler(OnPublishingPage);

            GlobalPageValidation.Validators -= new PageValidateEventHandler(GlobalPageValidation_Validators);
            DependencyResolver.Instance.ClearResources();
        }

        /// <summary>
        /// Application Error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_Error(Object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (HttpContext.Current.Session != null)
            {
                MiscellaneousSessionWrapper.ErrorPageError = ex;

            }

            if (HttpContext.Current.Request.Url.AbsoluteUri.IndexOf
                    ("mobile/", StringComparison.InvariantCultureIgnoreCase) != -1)
            {
                MiscellaneousSessionWrapper.IsMobileError = true;
            }
        }


        /// <summary>
        /// Raises the <see cref="E:ValidateUIAccess"/> event. Override this in inheriting classes to customize behavior,
        /// always calling the base-class implementation as well. Check for e.Cancel == true and do an early exit if so.
        /// </summary>
        /// <param name="e">
        /// The <see cref="EPiServer.ValidateUIAccessEventArgs"/> instance containing the event data.
        /// </param>
        protected override void OnValidateRequestAccess(ValidateRequestAccessEventArgs e)
        {
            base.OnValidateRequestAccess(e);
            if (e.Cancel)
            {
                return;
            }
        }

        /// <summary>
        /// Gets a list of default documents. Overide if you need to change which documents are actually
        /// tried.
        /// </summary>
        /// <param name="url">The URL of the request that is determined to need a default document</param>
        /// <returns>null or a list of default documents to try</returns>
        protected override string[] GetDefaultDocuments(Uri url)
        {
            return base.GetDefaultDocuments(url);
        }

        /// <summary>
        /// Session Start
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Session_Start(object sender, EventArgs e)
        {
            JavascriptControl.SetValue(JavascriptControl.JavaScriptState.Undefined);
        }

        /// <summary>
        /// This method clears all the blocked rooms on session end
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Session_End(object sender, EventArgs e)
        {
            try
            {
                ClearSelectedRateSessionData();
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, ex.Message);
            }
        }

        /// <summary>
        /// This method clears the selected rate information saved in session and also unblocks the rooms if any blocked,
        /// on rate selection in selectrate page.
        /// </summary>
        private void ClearSelectedRateSessionData()
        {
            HotelSearchEntity search = SearchCriteriaSessionWrapper.SearchCriteria;
            if (search != null && search.ListRooms != null && search.ListRooms.Count > 0)
            {
                foreach (HotelSearchRoomEntity room in search.ListRooms)
                {
                    room.IsSearchDone = false;
                    if (room.IsBlocked)
                    {
                        if (!string.IsNullOrEmpty(room.RoomBlockReservationNumber))
                        {
                            IgnoreBookingEntity ignoreBookingEntity = new IgnoreBookingEntity();
                            ignoreBookingEntity.ReservationNumber = room.RoomBlockReservationNumber;
                            ignoreBookingEntity.HotelCode = search.SelectedHotelCode;
                            ignoreBookingEntity.ChainCode = AppConstants.CHAIN_CODE;
                            ReservationController reservationCtrl = new ReservationController();
                            reservationCtrl.IgnoreBooking(ignoreBookingEntity);
                        }
                        room.RoomBlockReservationNumber = string.Empty;
                        room.IsBlocked = false;
                    }
                }
            }
        }

        /// <summary>
        /// If the requested URL path is "robots.txt", append the host domain suffix to the end of the path
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EPiServer.Web.UrlRewriteEventArgs</param>
        private void UrlRewriteProvider_ConvertingToInternal(object sender, EPiServer.Web.UrlRewriteEventArgs e)
        {
            if (e.Url != null)
            {
                if (e.Url.Path.ToUpper() == "/ROBOTS.TXT")
                {
                    Regex expression = new Regex("\\.(?<suffix>\\w+)$");
                    Match match = expression.Match(e.Url.Host);
                    if (match.Success)
                    {
                        if (match.Groups["suffix"] != null)
                        {
                            string s = match.Groups["suffix"].Value;
                            e.Url.Path = string.Format("/sitemap/robots.{0}.txt", s);
                        }
                    }
                }

                if (e.Url.Path.ToUpper() == "/SITEMAP.XML")
                {
                    Regex expression = new Regex("\\.(?<suffix>\\w+)$");
                    Match match = expression.Match(e.Url.Host);
                    if (match.Success)
                    {
                        if (match.Groups["suffix"] != null)
                        {
                            string s = match.Groups["suffix"].Value;
                            e.Url.Path = string.Format("/sitemap/sitemap.{0}.xml", s);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Has all the global validation methods for CMS
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GlobalPageValidation_Validators(object sender, PageValidateEventArgs e)
        {
            ValidatePageProperty(e, "HotelRoomDescriptionPageTypeGUID", "RoomCategoryPageTypeGUID", "RoomCategory");
            ValidatePageProperty(e, "HotelLocalAttractionPageTypeGUID", "LocalAttractionPageTypeGUID", "LocalAttraction");
            ValidatePageProperty(e, "AlternativeHotelPageTypeGUID", "HotelPageTypeGUID", "AlternativeHotel");
            ValidateHotelPageProperty(e);

            PageType partnerPage = PageType.Load(new Guid(ConfigurationManager.AppSettings["PartnerPageGUID"]));
            if (partnerPage != null)
            {
                if (e.Page != null && e.Page.PageTypeID == partnerPage.ID)
                {
                    ValidatePartnerPageproperty(e);
                    ValidateDuplicateTrackingID(e);
                }
            }

            PageType rateCategoryPageType =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["RateCategoryPageTypeGUID"]));
            if (e.Page != null && e.Page.PageTypeID == rateCategoryPageType.ID)
            {
                ValidateRateCategoryPageProperty(e);
            }

            if (e.Page != null)
            {
                //Validate Duplicate Opera ID in CMS
                if (!string.IsNullOrEmpty(e.Page["OperaID"] as string))
                {
                    int duplicateOperaIDValue =
                       ExistsPageWithPropertyValue(e.Page["OperaID"] as string, e.Page.PageTypeID, e.PageLink.ID, "OperaID");
                    e.IsValid = GlobalUtil.ValidateCMSDuplicateOperaID(e, "OperaID", duplicateOperaIDValue);
                }
                //Validate Image carousal in CMS
                CarouselUtility utilityObj = new CarouselUtility();
                int imageCount = 0;
                utilityObj.GetNumberOfImageCountHotelOverviewPage(e.Page, ref imageCount);
                bool isImagesOnPage = utilityObj.CheckForImageCountOtherTemplates(e.Page);
                e.IsValid = GlobalUtil.ValidateCMSImageCarousal(e, "ActivateImageCarousel", imageCount, Convert.ToInt32(CarouselConstants.HotelOverViewPageId),
                    isImagesOnPage, Convert.ToInt32(CarouselConstants.StandardPageId));

                //Validate Title Length in CMS
                e.IsValid = GlobalUtil.ValidateCMSFieldLength(e, "Title", 70);
                //Validate TripAdvisor Title Length in CMS
                e.IsValid = GlobalUtil.ValidateCMSFieldLength(e, "Title_TripAdvisor", 70);
                //Validate MeetingAndConf Title Length in CMS
                e.IsValid = GlobalUtil.ValidateCMSFieldLength(e, "Title_MeetingAndConf", 70);
                //Validate Offers Title Length in CMS
                e.IsValid = GlobalUtil.ValidateCMSFieldLength(e, "Title_Offers", 70);
                //Validate Location Title Length in CMS
                e.IsValid = GlobalUtil.ValidateCMSFieldLength(e, "Title_Location", 70);
                //Validate Mobile Bookmark ShortName Length in CMS
                e.IsValid = GlobalUtil.ValidateCMSFieldLength(e, "MobileBookmarkShortName", 12);
            }

            int offerPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["OfferPageTypeGUID"])).ID;
            if (e.Page.PageTypeID == offerPageTypeID && Convert.ToDateTime(e.Page["OfferStartTime"]) > Convert.ToDateTime(e.Page["OfferEndTime"]))
            {
                if (e.Page["OfferEndTime"] != null)
                {
                    e.IsValid = false;
                    e.ErrorMessage = string.Format("The date for {0} must be before {1}", "Offer Start Time", "Offer End Time");
                }
            }

            if (e.IsValid)
                MiscellaneousSessionWrapper.IsPageCopied = false;

        }

        #endregion Protected Methods

        #region Private Methods

        /// <summary>
        /// Validate Rate category id page.
        /// 1. Validate property Guarantee type.Accepts only 'GTDCC','GTDDEP','GTDRED'.
        /// 2. Validate property 'Rate category id' for duplicate values.
        /// </summary>
        /// <param name="e">page validation event</param>
        private void ValidateRateCategoryPageProperty(PageValidateEventArgs e)
        {
            string rateCategoryId = e.Page["RateCategoryID"] as string;
            if (!string.IsNullOrEmpty(rateCategoryId))
            {
                int duplicatePageID =
                    ExistsPageWithPropertyValue(rateCategoryId, e.Page.PageTypeID, e.PageLink.ID, "RateCategoryID");
                if (duplicatePageID != -1)
                {
                    e.IsValid = false;
                    if (string.IsNullOrEmpty(e.ErrorMessage))
                    {
                        e.ErrorMessage =
                            string.Format("Page of the same page type and the same RateCategoyID already exists ({0})",
                                          duplicatePageID.ToString());
                    }
                    else
                    {
                        e.ErrorMessage += "<li> " +
                                          string.Format(
                                              " Page of the same page type and the same RateCategoyID already exists ({0})",
                                              duplicatePageID.ToString()) + "</li>";
                    }
                }
            }
        }
        /// <summary>
        /// On Creating Page
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Page Event Args</param>
        private void OnCreatingPage(Object sender, PageEventArgs e)
        {
            CheckForDuplicateOperaIDWhileCopying(e);
        }
        /// <summary>
        /// On Created Page
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Page Event Args</param>
        private void OnCreatedPage(Object sender, PageEventArgs e)
        {
            #region Hotel

            int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
            int hotelRoomDescriptionContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelRoomDescriptionContainerPageTypeGUID"])).
                    ID;
            int meetingRoomContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["MeetingRoomContainerPageTypeGUID"])).ID;
            int travelInstructionContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["TravelInstructionContainerPageTypeGUID"])).ID;
            int facilityContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["FacilityContainerPageTypeGUID"])).ID;
            int alternativeHotelContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["AlternativeHotelContainerPageTypeGUID"])).ID;
            int hotelLocalAttractionContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelLocalAttractionContainerPageTypeGUID"])).
                    ID;
            int hotelRedemptionCategoryContainerPageTypeID =
                PageType.Load
                    (new Guid(ConfigurationManager.AppSettings["HotelRedemptionCategoryContainerPageTypeGUID"])).ID;

            if (e.Page.PageTypeID == hotelPageTypeID)
            {
                CreateContainerInAllLanguages(e.Page.PageLink, hotelRoomDescriptionContainerPageTypeID,
                                              ConfigurationManager.AppSettings["HotelRoomDescriptionContainerName"]);
                CreateContainerInAllLanguages(e.Page.PageLink, meetingRoomContainerPageTypeID,
                                              ConfigurationManager.AppSettings["MeetingRoomContainerName"]);
                CreateContainerInAllLanguages(e.Page.PageLink, travelInstructionContainerPageTypeID,
                                              ConfigurationManager.AppSettings["TravelInstructionContainerName"]);
                CreateContainerInAllLanguages(e.Page.PageLink, facilityContainerPageTypeID,
                                              ConfigurationManager.AppSettings["FacilityContainerName"]);
                CreateContainerInAllLanguages(e.Page.PageLink, alternativeHotelContainerPageTypeID,
                                              ConfigurationManager.AppSettings["AlternativeHotelContainerName"]);
                CreateContainerInAllLanguages(e.Page.PageLink, hotelLocalAttractionContainerPageTypeID,
                                              ConfigurationManager.AppSettings["HotelLocalAttractionContainerName"]);
                CreateContainerInAllLanguages(e.Page.PageLink, hotelRedemptionCategoryContainerPageTypeID,
                                              ConfigurationManager.AppSettings["HotelRedemptionCategoryCoontainerName"]);
            }

            #endregion

            #region City

            int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
            int localAttractionContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["LocalAttractionContainerPageTypeGUID"])).ID;
            int hotelContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelContainerPageTypeGUID"])).ID;

            if (e.Page.PageTypeID == cityPageTypeID)
            {
                CreateContainerInAllLanguages(e.Page.PageLink, hotelContainerPageTypeID,
                                              ConfigurationManager.AppSettings["HotelContainerName"]);
                CreateContainerInAllLanguages(e.Page.PageLink, localAttractionContainerPageTypeID,
                                              ConfigurationManager.AppSettings["LocalAttractionContainerName"]);
            }

            #endregion

            #region RussianURL

            PageData pd = DataFactory.Instance.GetPage(e.PageLink);
            PageData pdclone = pd.CreateWritableClone();
            if (pd.LanguageID == CurrentPageLanguageConstant.LANGAUGE_RUSSIAN)
            {
                Int32 url;
                if (pd.URLSegment == null || pd.URLSegment.Equals(string.Empty) ||
                    Int32.TryParse(e.Page.Property["PageURLSegment"].Value.ToString(), out url))
                {
                    if (pd.PageName != null)
                    {
                        e.Page.Property["PageURLSegment"].Value = createURLSegment(pd.PageName);
                    }
                }
            }

            DataFactory.Instance.Save(pdclone, SaveAction.ForceCurrentVersion | SaveAction.Save, AccessLevel.NoAccess);

            #endregion

            #region Partner

            PageType partnerPage = PageType.Load(new Guid(ConfigurationManager.AppSettings["PartnerPageGUID"]));
            string tempTrackingCode = string.Empty;
            if (partnerPage != null)
            {
                int partnerPageTypeID = partnerPage.ID;
                if (e.Page.PageTypeID == partnerPageTypeID)
                {
                    switch (e.Page.IsModified)
                    {
                        case false:
                            {
                                e.Page.Property["APIKey"].Value = Guid.NewGuid().ToString();
                                MiscellaneousSessionWrapper.InitialAPIKey = e.Page.Property["APIKey"].Value.ToString();

                                string partnerName = e.Page.Property["PageName"].Value.ToString();
                                if (Convert.ToString(e.Page.Property["SourceTrackingCode"].Value) ==
                                    AppConstants.SystemGenerated)
                                {
                                    MiscellaneousSessionWrapper.PreviousTrackingCode = null;
                                    tempTrackingCode = string.Format(CultureInfo.InvariantCulture, "{0}{1}",
                                                                     "pa_api_",
                                                                     partnerName.Replace(" ", string.Empty).Trim());
                                    e.Page.Property["TrackingCode"].Value = tempTrackingCode;
                                    DataFactory.Instance.Save(e.Page,
                                                              EPiServer.DataAccess.SaveAction.Publish |
                                                              EPiServer.DataAccess.SaveAction.ForceCurrentVersion,
                                                              EPiServer.Security.AccessLevel.NoAccess);
                                }
                                else
                                {
                                    MiscellaneousSessionWrapper.PreviousTrackingCode = null;
                                    DataFactory.Instance.Save(e.Page,
                                                              EPiServer.DataAccess.SaveAction.Publish |
                                                              EPiServer.DataAccess.SaveAction.ForceCurrentVersion,
                                                              EPiServer.Security.AccessLevel.NoAccess);
                                }
                                break;
                            }
                    }
                }
            }

            #endregion
        }

        /// <summary>
        /// On Moved Page
        /// </summary>
        /// <param name="sender">Object</param>
        /// <param name="e">Page Event Args</param>
        private void OnMovedPage(Object sender, PageEventArgs e)
        {
            int contentChangeRequestPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["ContentChangeRequestPageTypeGUID"])).ID;
            PageData requestPage = DataFactory.Instance.GetPage(e.PageLink, AccessLevel.NoAccess);
            PageData targetPage = DataFactory.Instance.GetPage(e.TargetLink, AccessLevel.NoAccess);

            if (requestPage.PageTypeID == contentChangeRequestPageTypeID)
            {
                string Header = requestPage.PageName;
                string AuthorName = requestPage["AuthorName"] as string ?? String.Empty;
                string AuthorEmail = requestPage["AuthorEmail"] as string ?? String.Empty;
                string Mailbody = requestPage["MailBody"] as string ?? String.Empty;


                string MailSubject = requestPage["MailSubject"] as string ?? String.Empty;
                string comments = requestPage["Comments"] as string ?? String.Empty;
                if (targetPage["ContentChangeRequestStatus"] as string == "completed")
                {
                    MailManager c = new MailManager(Header, AuthorName, AuthorEmail, Mailbody,
                                                    MailSubject, MailManager.ContentChangeRequestStatus.completed,
                                                    comments);
                    c.SendMail();
                }
                else if (targetPage["ContentChangeRequestStatus"] as string == "rejected")
                {
                    MailManager r = new MailManager(Header, AuthorName, AuthorEmail, Mailbody,
                                                    MailSubject, MailManager.ContentChangeRequestStatus.rejected,
                                                    comments);
                    r.SendMail();
                }
            }
        }

        /// <summary>
        /// On Moving Page
        /// </summary>
        /// <param name="sender">Object</param>
        /// <param name="e">Page Event Args</param>
        private void OnMovingPage(Object sender, PageEventArgs e)
        {
            if (HttpContext.Current != null && HttpContext.Current.User != null)
            {
                if (!HttpContext.Current.User.IsInRole("Administrators"))
                {
                    int hotelRoomDescriptionContainerPageTypeID =
                        PageType.Load(new Guid
                                          (ConfigurationManager.AppSettings["HotelRoomDescriptionContainerPageTypeGUID"]))
                            .ID;
                    int meetingRoomContainerPageTypeID =
                        PageType.Load(new Guid(ConfigurationManager.AppSettings["MeetingRoomContainerPageTypeGUID"])).ID;
                    int travelInstructionContainerPageTypeID =
                        PageType.Load(new Guid
                                          (ConfigurationManager.AppSettings["TravelInstructionContainerPageTypeGUID"])).
                            ID;
                    int facilityContainerPageTypeID =
                        PageType.Load(new Guid(ConfigurationManager.AppSettings["FacilityContainerPageTypeGUID"])).ID;
                    int alternativeHotelContainerPageTypeID =
                        PageType.Load(new Guid
                                          (ConfigurationManager.AppSettings["AlternativeHotelContainerPageTypeGUID"])).
                            ID;
                    int hotelLocalAttractionContainerPageTypeID =
                        PageType.Load(new Guid
                                          (ConfigurationManager.AppSettings["HotelLocalAttractionContainerPageTypeGUID"]))
                            .ID;
                    int localAttractionContainerPageTypeID =
                        PageType.Load(new Guid
                                          (ConfigurationManager.AppSettings["LocalAttractionContainerPageTypeGUID"])).ID;
                    int hotelContainerPageTypeID =
                        PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelContainerPageTypeGUID"])).ID;
                    int hotelRedemptionCategoryContainerPageTypeID =
                        PageType.Load(new Guid
                                          (ConfigurationManager.AppSettings[
                                              "HotelRedemptionCategoryContainerPageTypeGUID"])).ID;

                    if (e.PageLink != null)
                    {
                        PageData pd = DataFactory.Instance.GetPage(e.PageLink, AccessLevel.NoAccess);
                        if (pd.PageTypeID == hotelRoomDescriptionContainerPageTypeID ||
                            pd.PageTypeID == meetingRoomContainerPageTypeID ||
                            pd.PageTypeID == travelInstructionContainerPageTypeID ||
                            pd.PageTypeID == facilityContainerPageTypeID ||
                            pd.PageTypeID == alternativeHotelContainerPageTypeID ||
                            pd.PageTypeID == hotelLocalAttractionContainerPageTypeID ||
                            pd.PageTypeID == localAttractionContainerPageTypeID ||
                            pd.PageTypeID == hotelContainerPageTypeID ||
                            pd.PageTypeID == hotelRedemptionCategoryContainerPageTypeID)
                        {
                            e.CancelAction = true;
                            e.CancelReason = "Only administrators can move pages of this page type.";
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates a container page in all languages under a specific parent page
        /// </summary>
        /// <param name="parentLink">PageLink to the parent page</param>
        /// <param name="containerPageTypeID">Container PageTypeID</param>
        /// <param name="containerPageName">Container PageName</param>
        private void CreateContainerInAllLanguages
            (PageReference parentLink, int containerPageTypeID, string containerPageName)
        {
            PageData containerPage = DataFactory.Instance.GetDefaultPageData(parentLink, containerPageTypeID);
            containerPage.PageName = containerPageName;
            PageReference savedContainerPageLink =
                DataFactory.Instance.Save(containerPage, SaveAction.Publish, AccessLevel.NoAccess);
            PageData savedContainerPage = DataFactory.Instance.GetPage(savedContainerPageLink);

            foreach (LanguageBranch lb in LanguageBranch.ListEnabled())
            {
                if (lb.LanguageID != savedContainerPage.LanguageID)
                {
                    PageData containerLanguageBranch =
                        DataFactory.Instance.CreateLanguageBranch
                            (savedContainerPageLink, new LanguageSelector(lb.LanguageID), AccessLevel.NoAccess);
                    containerLanguageBranch.PageName = containerPageName;
                    DataFactory.Instance.Save(containerLanguageBranch, SaveAction.Publish, AccessLevel.NoAccess);
                }
            }
        }

        /// <summary>
        /// Validates that a certain Page property on a certain page type has a page of a specific page type selected. 
        /// Sets e.IsValid to false if not.
        /// </summary>
        /// <param name="e">PageValidateEventArgs</param>
        /// <param name="sourcePageTypeKey">The AppSettings key of the page type that should be validated</param>
        /// <param name="targetPageTypeKey">The AppSettings key of the page type the property must point at</param>
        /// <param name="pagePropertyName">The name of the page property to be validated</param>
        private void ValidatePageProperty
            (PageValidateEventArgs e, string sourcePageTypeKey, string targetPageTypeKey, string pagePropertyName)
        {
            PageType sourcePageType = PageType.Load(new Guid(ConfigurationManager.AppSettings[sourcePageTypeKey]));
            PageType targetPageType = PageType.Load(new Guid(ConfigurationManager.AppSettings[targetPageTypeKey]));

            if (e.Page != null && e.Page.PageTypeID == sourcePageType.ID)
            {
                PageReference selectedPage = e.Page[pagePropertyName] as PageReference;
                if (selectedPage != null)
                {
                    PageData selectedPageData = DataFactory.Instance.GetPage(selectedPage, AccessLevel.NoAccess);
                    if (selectedPageData.PageTypeID != targetPageType.ID)
                    {
                        e.IsValid = false;
                        e.ErrorMessage = string.Format("{0} property must be a page of type {1}",
                                                       pagePropertyName, targetPageType.Name);
                    }
                }
            }
        }

        /// <summary>
        /// This will validate required properties for Hotel page.
        /// This will create a custom validation message depending upon the values set from the 
        /// AppSettings.config file
        /// </summary>
        /// <param name="e">Page Validate Event Args</param>
        private void ValidateHotelPageProperty(PageValidateEventArgs e)
        {
            try
            {
                PageType hotelPageType = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"]));

                if (
                    (e != null) &&
                    (e.Page != null) &&
                    (hotelPageType != null) &&
                    (e.Page.PageTypeID == hotelPageType.ID) &&
                    (e.Page.Property["HotelCategory"] != null) &&
                    (e.Page.Property["HotelCategory"].Value != null)
                    )
                {
                    PageVersionCollection pageVersions = PageVersion.List(e.Page.PageLink);
                    if (pageVersions.Count > 0 && (e.Page.PendingPublish))
                    {
                        if (e.Page.IsMasterLanguageBranch)
                        {
                            SetErrorMessageforHotelPage(e, e.Page.Property["HotelCategory"].Value.ToString(),
                                                        IgnoreGlobalProperty.NOT_IGNORE);
                        }
                        else
                        {
                            PageReference lastVersion = pageVersions[0].ID;
                            foreach (PageVersion pageVersion in pageVersions)
                            {
                                if (pageVersion.IsMasterLanguageBranch)
                                {
                                    lastVersion = pageVersion.ID;
                                    break;
                                }
                            }
                            PageData page = DataFactory.Instance.GetPage(lastVersion, LanguageSelector.AutoDetect(true));
                            SetErrorMessageforHotelPage(e, page.Property["HotelCategory"].Value.ToString(),
                                                        IgnoreGlobalProperty.IGNORE);
                        }
                    }
                    else
                    {
                        string hotelCategory = ((PropertyData)e.Page.Property["HotelCategory"]).Value.ToString() ??
                                               string.Empty;
                        switch (hotelCategory)
                        {
                            case AppConstants.COMINGSOON_VALUE:
                                {
                                    SetErrorMessageforHotelPage(e, hotelCategory, IgnoreGlobalProperty.NOT_IGNORE);
                                    break;
                                }
                            case AppConstants.RECENTLYOPENED_VALUE:
                                {
                                    SetErrorMessageforHotelPage(e, hotelCategory, IgnoreGlobalProperty.NOT_IGNORE);
                                    break;
                                }
                            case AppConstants.DEFAULT:
                                {
                                    SetErrorMessageforHotelPage(e, AppConstants.DEFAULT, IgnoreGlobalProperty.NOT_IGNORE);
                                    break;
                                }

                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// This will validate required properties for Partner page.
        /// This will create a custom validation message depending upon the values set from the 
        /// AppSettings.config file
        /// </summary>
        /// <param name="e"></param>
        /// <returns>valid or not</returns>
        private string ValidatePartnerPageproperty(PageValidateEventArgs e)
        {
            try
            {
                string trackingValue = string.Empty;
                PageType partnerPageType = PageType.Load(new Guid(ConfigurationManager.AppSettings["PartnerPageGUID"]));
                if (
                    (e != null) &&
                    (partnerPageType != null) &&
                    (e.Page.Property["TrackingCode"] != null) &&
                    (e.Page.Property["TrackingCode"].Value != null)
                    )
                {
                    trackingValue = e.Page.Property["TrackingCode"].Value.ToString();
                    Regex objAlphaPattern = new Regex(@"^[a-zA-Z0-9_]*$");
                    if (!string.IsNullOrEmpty(trackingValue) && objAlphaPattern.IsMatch(trackingValue))
                    {
                        e.IsValid = true;
                    }
                    else
                    {
                        e.IsValid = false;
                        e.ErrorMessage = string.Format("Tracking Code property contain invalid characters/space");
                    }
                }
            }
            catch (Exception)
            {
            }

            return e.IsValid.ToString();
        }

        ///<summary>
        ///This will validate the duplicate TrackingCode
        ///</summary>
        ///<param name="e"></param>
        private void ValidateDuplicateTrackingID(PageValidateEventArgs e)
        {
            try
            {
                string tempTrackingCode = string.Empty;
                if (e.Page.Property["TrackingCode"] != null &&
                    e.Page.Property["TrackingCode"].Value != null &&
                    Convert.ToString(e.Page.Property["SourceTrackingCode"].Value) == AppConstants.ManuallyGenerated)
                    tempTrackingCode = e.Page.Property["TrackingCode"].Value.ToString();
                else if (e.Page.Property["TrackingCode"] != null &&
                         e.Page.Property["TrackingCode"].Value != null &&
                         Convert.ToString(e.Page.Property["SourceTrackingCode"].Value) == AppConstants.SystemGenerated)
                {
                    string partnerName = e.Page.Property["PageName"].Value.ToString();
                    e.Page.Property["TrackingCode"].Value =
                        string.Format(CultureInfo.InvariantCulture, "{0}{1}", "pa_api_", partnerName);
                    tempTrackingCode = e.Page.Property["TrackingCode"].Value.ToString();
                }
                if (!string.IsNullOrEmpty(tempTrackingCode))
                {
                    int duplicateTrackingIDValue =
                        ExistsPageWithPropertyValue(tempTrackingCode, e.Page.PageTypeID, e.PageLink.ID, "TrackingCode");
                    if (duplicateTrackingIDValue != -1)
                    {
                        e.IsValid = false;
                        if (string.IsNullOrEmpty(e.ErrorMessage))
                        {
                            e.ErrorMessage =
                                string.Format(
                                    "Page of the same page type and the same TrackingCode already exists ({0})",
                                    duplicateTrackingIDValue.ToString());
                        }
                        else
                        {
                            e.ErrorMessage = e.ErrorMessage + "<li> " +
                                             string.Format(
                                                 " Page of the same page type and the same TrackingCode already exists ({0})",
                                                 duplicateTrackingIDValue.ToString()) + "</li>";
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// This will get the masterLanguage Data from Last Version.
        /// </summary>
        /// <param name="pageRef"></param>
        /// <returns>Page Data</returns>
        public PageData GetLastVersion(PageReference pageRef)
        {
            PageVersionCollection pageVersions = PageVersion.List(pageRef);
            PageReference lastVersion = null;

            foreach (PageVersion pageVersion in pageVersions)
            {
                if (pageVersion.IsMasterLanguageBranch)
                {
                    lastVersion = pageVersion.ID;
                }
            }
            return DataFactory.Instance.GetPage(lastVersion, LanguageSelector.AutoDetect(true));
        }

        /// <summary>
        /// This will read Hotel Category section from AppSettings.config file.
        /// </summary>
        /// <param name="hotelCategory"></param>
        /// <returns>List of properties</returns>
        private string[] ReadHotelPagePropertySection(string hotelCategory)
        {
            string[] pagePropertyArray = null;

            string pageProperties = ConfigurationManager.AppSettings[hotelCategory];

            if (!string.IsNullOrEmpty(pageProperties))
            {
                pagePropertyArray = pageProperties.Split(';');
            }
            return pagePropertyArray;
        }

        /// <summary>
        /// This will set error message for diffrent category of Hotel.
        /// </summary>
        /// <param name="e">PageValidateEventArgs</param>
        /// <param name="hotelCategory">hotelCategory hotel Category</param>
        /// <param name="ignoreGlobalProperty">Ignore Global Property</param>
        private void SetErrorMessageforHotelPage
            (PageValidateEventArgs e, string hotelCategory, IgnoreGlobalProperty ignoreGlobalProperty)
        {
            string[] pagePropertyList = ReadHotelPagePropertySection(hotelCategory);

            if (pagePropertyList != null && pagePropertyList.Length > 0)
            {
                StringBuilder sr = new StringBuilder();

                for (int i = 0; i < pagePropertyList.Length; i++)
                {
                    string pageProperty = pagePropertyList[i];
                    if (e.Page.Property[pageProperty] == null)
                    {
                        e.IsValid = false;
                        if (sr.Length > 0)
                        {
                            sr = sr.Append("<li>" + '"' + e.Page.Property[pageProperty].TranslateDisplayName() +
                                           '"' + " " + "cannot empty." + "</li>");
                        }
                        else
                        {
                            sr = sr.Append('"' + e.Page.Property[pageProperty].TranslateDisplayName() +
                                           '"' + " " + "cannot empty.");
                        }
                    }
                    else
                    {
                        if (ignoreGlobalProperty.Equals(IgnoreGlobalProperty.NOT_IGNORE))
                        {
                            ValidateProperty(e, pageProperty, sr);
                        }
                        else if (e.Page.Property[pageProperty].IsLanguageSpecific)
                        {
                            ValidateProperty(e, pageProperty, sr);
                        }
                    }
                }
                if ((!hotelCategory.Equals(AppConstants.DEFAULT)) &&
                    (e.Page.Property["AvailableInBooking"].Value != null) &&
                    (e.Page.Property["AvailableInBooking"].Value.Equals(true)) &&
                    (e.Page.Property["OperaId"].Value == null)
                    )
                {
                    e.IsValid = false;
                    if (sr.Length > 0)
                    {
                        sr = sr.Append("<li>" + '"' + e.Page.Property["OperaId"].TranslateDisplayName() + '"' +
                                       " " + "cannot empty." + "</li>");
                    }
                    else
                    {
                        sr = sr.Append('"' + e.Page.Property["OperaId"].TranslateDisplayName() + '"' +
                                       " " + "cannot empty.");
                    }
                }
                e.ErrorMessage = sr.ToString();
            }
        }

        /// <summary>
        /// This will validate each required property for diffrent category of Hotel.
        /// </summary>
        /// <param name="e">PageValidateEventArgs</param>
        /// <param name="pageProperty"></param>
        /// <param name="errorString"></param>
        private void ValidateProperty(PageValidateEventArgs e, string pageProperty, StringBuilder errorString)
        {
            if (e.Page.Property[pageProperty].Value == null ||
                string.IsNullOrEmpty(e.Page.Property[pageProperty].Value.ToString().Trim())
                || e.Page.Property[pageProperty].Value.ToString() == AppConstants.SPACE)
            {
                e.IsValid = false;
                if (errorString.Length > 0)
                {
                    errorString = errorString.Append("<li>" + '"' +
                                                     e.Page.Property[pageProperty].TranslateDisplayName() + '"' + " " +
                                                     "cannot empty." + "</li>");
                }
                else
                {
                    errorString = errorString.Append('"' +
                                                     e.Page.Property[pageProperty].TranslateDisplayName() + '"' + " " +
                                                     "cannot empty.");
                }
            }
        }

        /// <summary
        /// Returns PageID if there exists a page (which is not the current page) that has a specific 
        // /PageTypeID and OperaID and -1 if no match is found.
        /// </summary>
        /// <param name="operaID">OperaID value</param>
        /// <param name="pageTypeID">PageTypeID value</param>
        /// <param name="currentPageID">Current Page ID</param>
        /// <param name="propertyName">Property name to be matched</param>
        /// <returns>pageId or -1</returns>
        private int ExistsPageWithPropertyValue(string operaID, int pageTypeID, int currentPageID, string propertyName)
        {
            PropertyCriteriaCollection criterias = new PropertyCriteriaCollection();

            PropertyCriteria duplicateOperaIDCriteria = new PropertyCriteria();
            duplicateOperaIDCriteria.Condition = CompareCondition.Equal;
            duplicateOperaIDCriteria.Name = propertyName;
            duplicateOperaIDCriteria.Required = true;
            duplicateOperaIDCriteria.Type = PropertyDataType.String;
            duplicateOperaIDCriteria.Value = operaID;
            criterias.Add(duplicateOperaIDCriteria);

            PropertyCriteria pageTypeCriteria = new PropertyCriteria();
            pageTypeCriteria.Condition = CompareCondition.Equal;
            pageTypeCriteria.Name = "PageTypeID";
            pageTypeCriteria.Required = true;
            pageTypeCriteria.Type = PropertyDataType.PageType;
            pageTypeCriteria.Value = pageTypeID.ToString();
            criterias.Add(pageTypeCriteria);

            PageDataCollection searchResults =
                DataFactory.Instance.FindPagesWithCriteria(PageReference.RootPage, criterias);

            foreach (PageData searchResult in searchResults)
            {
                if (searchResult.PageLink.ID != currentPageID)
                    return searchResult.PageLink.ID;
            }

            return -1;
        }

        private enum IgnoreGlobalProperty
        {
            IGNORE,
            NOT_IGNORE
        }

        private void OnSavingPage(Object sender, PageEventArgs e)
        {
            Int32 url;
            if (e.Page.LanguageID == CurrentPageLanguageConstant.LANGAUGE_RUSSIAN)
            {
                if (e.Page.Property["PageURLSegment"].Value == null ||
                    e.Page.Property["PageURLSegment"].Value.Equals(string.Empty) ||
                    Int32.TryParse(e.Page.Property["PageURLSegment"].Value.ToString(), out url))
                {
                    e.Page.Property["PageURLSegment"].Value = createURLSegment(e.Page.PageName);
                }
            }

            if (!e.Page.IsMasterLanguageBranch && e.Page.Property != null)
            {
                for (int i = 0; i < e.Page.Property.Count; i++)
                {
                    if (!e.Page.Property[i].IsLanguageSpecific)
                    {
                        e.Page.Property[i].IsModified = false;
                    }
                }
            }

            if (e.Page.Property != null && (e.Page.PageTypeName == EditModeWarning.SCANWEB_HOTEL_PAGETYPE &&
                e.Page.Property[EditModeWarning.AVAILABLEINBOOKING_PROP_NAME].IsModified))
            {
                var editMode = HttpContext.Current.Handler as SystemPageBase;
                if (editMode == null)
                    return;
                editMode.LoadComplete += new EventHandler(editMode_LoadComplete);
                AppLogger.LogFatalException(new Exception(string.Format("{0} {1} : {2} ({3})",
                    DateTime.Now.ToString(), EditModeWarning.VALIDATION_FOR_AVAILABILITY, e.Page.PageName, e.Page.Property[EditModeWarning.OPERAID])));
            }

            #region XMLAPI

            PageType partnerPageType = PageType.Load(new Guid(ConfigurationManager.AppSettings["PartnerPageGUID"]));
            if (partnerPageType != null && e.Page.IsModified == true && e.Page.PageTypeID == partnerPageType.ID)
            {
                string prevTrackingCode = string.Empty;
                prevTrackingCode = MiscellaneousSessionWrapper.PreviousTrackingCode;

                if (!string.IsNullOrEmpty(prevTrackingCode) &&
                    string.IsNullOrEmpty(e.Page.Property["TrackingCode"].Value.ToString()))
                {
                    e.Page.Property["TrackingCode"].Value = prevTrackingCode.Replace(" ", string.Empty).Trim();
                    MiscellaneousSessionWrapper.PreviousTrackingCode = null;
                }
            }
            if (partnerPageType != null && e.Page.IsModified == true &&
                e.Page.PageTypeID == partnerPageType.ID)
            {
                string prevAPIKey = string.Empty;
                prevAPIKey = MiscellaneousSessionWrapper.InitialAPIKey;

                e.Page.Property["APIKey"].Value = prevAPIKey;
                MiscellaneousSessionWrapper.InitialAPIKey = null;
            }

            #endregion
        }

        /// <summary>
        /// Displaying Validation Summary using static validator | REEP
        ///to warn the editor that availability for bookig property value changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void editMode_LoadComplete(object sender, EventArgs e)
        {
            var editPanel = sender as EditPanel;
            if (editPanel != null)
                editPanel.Validators.Add(new StaticValidator(EditModeWarning.VALIDATION_FOR_AVAILABILITY));

        }

        /// <summary>
        /// On Publishing Page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPublishingPage(Object sender, PageEventArgs e)
        {
            Int32 url = default(Int32);

            if (e.Page.LanguageID == CurrentPageLanguageConstant.LANGAUGE_RUSSIAN)
            {
                if (e.Page.Property["PageURLSegment"].Value == null ||
                    e.Page.Property["PageURLSegment"].Value.Equals(string.Empty)
                    || Int32.TryParse(e.Page.Property["PageURLSegment"].Value.ToString(), out url))
                {
                    e.Page.Property["PageURLSegment"].Value = createURLSegment(e.Page.PageName);
                }
            }

            if (e.Page.PageTypeID.ToString().Trim().Equals(ConfigurationManager.AppSettings[AppConstants.HotelRedemptionPointsPageTypeID]))
            {
                ClearHotelRedemptionFromCache();
            }

            SendEmailIfHotelisConfiguredAsNotavailableInBooking(e);
        }

        private void SendEmailIfHotelisConfiguredAsNotavailableInBooking(PageEventArgs e)
        {
            try
            {
                if (e.Page.PageTypeName == EditModeWarning.SCANWEB_HOTEL_PAGETYPE && e.Page.Property != null && string.IsNullOrEmpty(Convert.ToString(e.Page.Property["AvailableInBooking"])))
                {
                    EmailEntity emailEntity = new EmailEntity();
                    emailEntity.Recipient = EditModeWarning.EmailRecipientsForHotelNotAvailableInBookingEmailAlert;
                    emailEntity.Sender = EditModeWarning.EmailSenderForHotelNotAvailableInBookingEmailAlert;
                    emailEntity.Subject = string.Format(EditModeWarning.HotelNotAvailableInBookingEmailSubject, e.Page.PageName);
                    emailEntity.Body = string.Format(EditModeWarning.HotelNotAvailableInBookingEmailBody, e.Page.PageName, e.Page.Property[EditModeWarning.OPERAID]);
                    emailEntity.TextEmailBody = string.Format(EditModeWarning.HotelNotAvailableInBookingEmailBody, e.Page.PageName, e.Page.Property[EditModeWarning.OPERAID]);
                    CommunicationService.SendMail(emailEntity, null);
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Exception occured in SendEmailIfHotelisConfiguredAsNotavailableInBooking method");
            }
        }

        private void ClearHotelRedemptionFromCache()
        {
            string cacheKey = string.Format(AppConstants.HotelRedemptionsCacheKeyFormat, LanguageConstant.LANGUAGE_ENGLISH.ToLower());
            if (ScanwebCacheManager.Instance.LookInCache<List<HotelRedemptionPoints>>(cacheKey) != null)
            {
                ScanwebCacheManager.Instance.RemoveFromCache(cacheKey);
            }
            cacheKey = string.Format(AppConstants.HotelRedemptionsCacheKeyFormat, LanguageConstant.LANGUAGE_SWEDISH.ToLower());
            if (ScanwebCacheManager.Instance.LookInCache<List<HotelRedemptionPoints>>(cacheKey) != null)
            {
                ScanwebCacheManager.Instance.RemoveFromCache(cacheKey);
            }
            cacheKey = string.Format(AppConstants.HotelRedemptionsCacheKeyFormat, LanguageConstant.LANGUAGE_NORWEGIAN_EXTENSION.ToLower());
            if (ScanwebCacheManager.Instance.LookInCache<List<HotelRedemptionPoints>>(cacheKey) != null)
            {
                ScanwebCacheManager.Instance.RemoveFromCache(cacheKey);
            }
            cacheKey = string.Format(AppConstants.HotelRedemptionsCacheKeyFormat, LanguageConstant.LANGUAGE_DANISH.ToLower());
            if (ScanwebCacheManager.Instance.LookInCache<List<HotelRedemptionPoints>>(cacheKey) != null)
            {
                ScanwebCacheManager.Instance.RemoveFromCache(cacheKey);
            }
            cacheKey = string.Format(AppConstants.HotelRedemptionsCacheKeyFormat, LanguageConstant.LANGUAGE_FINNISH.ToLower());
            if (ScanwebCacheManager.Instance.LookInCache<List<HotelRedemptionPoints>>(cacheKey) != null)
            {
                ScanwebCacheManager.Instance.RemoveFromCache(cacheKey);
            }
            cacheKey = string.Format(AppConstants.HotelRedemptionsCacheKeyFormat, LanguageConstant.LANGUAGE_GERMAN.ToLower());
            if (ScanwebCacheManager.Instance.LookInCache<List<HotelRedemptionPoints>>(cacheKey) != null)
            {
                ScanwebCacheManager.Instance.RemoveFromCache(cacheKey);
            }
            cacheKey = string.Format(AppConstants.HotelRedemptionsCacheKeyFormat, LanguageConstant.LANGUAGE_RUSSIA_RU.ToLower());
            if (ScanwebCacheManager.Instance.LookInCache<List<HotelRedemptionPoints>>(cacheKey) != null)
            {
                ScanwebCacheManager.Instance.RemoveFromCache(cacheKey);
            }
        }

        /// <summary>
        /// create URL Segment
        /// </summary>
        /// <param name="p"></param>
        /// <returns>url segemnt</returns>
        private string createURLSegment(string p)
        {
            string russian = @"[a-zA-Z]+";
            string latin = @"\p{Alphabetic}&\p{Script=Latin}+";
            RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace;

            SetupStringMapping();
            string url = GetStringAfterReplaceRussian(p);

            return url;
        }


        /// <summary>
        /// Setup String Mapping
        /// </summary>
        private static void SetupStringMapping()
        {
            stringMapping["а"] = "a";
            stringMapping["б"] = "b";
            stringMapping["в"] = "v";
            stringMapping["г"] = "g";
            stringMapping["д"] = "d";
            stringMapping["е"] = "e";
            stringMapping["ё"] = "e";
            stringMapping["ж"] = "zh";
            stringMapping["з"] = "z";
            stringMapping["и"] = "i";
            stringMapping["й"] = "y";
            stringMapping["к"] = "k";
            stringMapping["л"] = "l";
            stringMapping["м"] = "m";
            stringMapping["н"] = "n";
            stringMapping["о"] = "o";
            stringMapping["п"] = "p";
            stringMapping["р"] = "r";
            stringMapping["с"] = "s";
            stringMapping["т"] = "t";
            stringMapping["у"] = "u";
            stringMapping["ф"] = "f";
            stringMapping["х"] = "kh";
            stringMapping["ц"] = "ts";
            stringMapping["ч"] = "ch";
            stringMapping["ш"] = "sh";
            stringMapping["ы"] = "y";
            stringMapping["э"] = "e";
            stringMapping["ю"] = "yu";
            stringMapping["я"] = "ya";
            stringMapping["щ"] = "shch";

            stringMapping["А"] = "a";
            stringMapping["Б"] = "b";
            stringMapping["В"] = "V";
            stringMapping["Г"] = "G";
            stringMapping["Д"] = "D";
            stringMapping["Ѓ"] = "Gj";
            stringMapping["Е"] = "Ye";
            stringMapping["Ё"] = "Ye";
            stringMapping["Ж"] = "Zh";
            stringMapping["З"] = "Z";
            stringMapping["Ѕ"] = "Dz";
            stringMapping["И"] = "I";
            stringMapping["Ј"] = "J";
            stringMapping["К"] = "K";
            stringMapping["Л"] = "L";
            stringMapping["Љ"] = "Lj";
            stringMapping["М"] = "M";
            stringMapping["Н"] = "N";
            stringMapping["Њ"] = "Nj";
            stringMapping["О"] = "O";
            stringMapping["П"] = "P";
            stringMapping["Р"] = "R";
            stringMapping["С"] = "S";
            stringMapping["Т"] = "T";
            stringMapping["Ќ"] = "Kj";
            stringMapping["У"] = "U";
            stringMapping["Ф"] = "F";
            stringMapping["Х"] = "Kh";
            stringMapping["Ц"] = "Ts";
            stringMapping["Ч"] = "Ch";
            stringMapping["Џ"] = "Dj";
            stringMapping["Ш"] = "Sh";
            stringMapping["Щ"] = "Shch";
            stringMapping["Ы"] = "y";
            stringMapping["Ю"] = "Yu";
            stringMapping["Я"] = "Ya";
            stringMapping[" "] = "-";

            stringMapping["А"] = "A";
            stringMapping["B"] = "B";
            stringMapping["C"] = "C";
            stringMapping["D"] = "D";
            stringMapping["E"] = "E";
            stringMapping["F"] = "F";
            stringMapping["G"] = "G";
            stringMapping["H"] = "H";
            stringMapping["I"] = "I";
            stringMapping["J"] = "J";
            stringMapping["K"] = "K";
            stringMapping["L"] = "L";
            stringMapping["M"] = "M";
            stringMapping["N"] = "N";
            stringMapping["O"] = "O";
            stringMapping["P"] = "P";
            stringMapping["Q"] = "Q";
            stringMapping["R"] = "R";
            stringMapping["S"] = "S";
            stringMapping["T"] = "T";
            stringMapping["U"] = "U";
            stringMapping["V"] = "V";
            stringMapping["W"] = "W";
            stringMapping["X"] = "X";
            stringMapping["Y"] = "Y";
            stringMapping["Z"] = "Z";


            stringMapping["a"] = "a";
            stringMapping["b"] = "b";
            stringMapping["c"] = "c";
            stringMapping["d"] = "d";
            stringMapping["e"] = "e";
            stringMapping["f"] = "f";
            stringMapping["g"] = "g";
            stringMapping["h"] = "h";
            stringMapping["i"] = "i";
            stringMapping["j"] = "j";
            stringMapping["k"] = "k";
            stringMapping["l"] = "l";
            stringMapping["m"] = "m";
            stringMapping["n"] = "n";
            stringMapping["o"] = "o";
            stringMapping["p"] = "p";
            stringMapping["q"] = "q";
            stringMapping["r"] = "r";
            stringMapping["s"] = "s";
            stringMapping["t"] = "t";
            stringMapping["u"] = "u";
            stringMapping["v"] = "v";
            stringMapping["w"] = "w";
            stringMapping["x"] = "x";
            stringMapping["y"] = "y";
            stringMapping["z"] = "z";
        }
        private void CheckForDuplicateOperaIDWhileCopying(PageEventArgs e)
        {
            if (MiscellaneousSessionWrapper.IsPageCopied && e.Page != null)
            {
                string disallowCopyPageTypes = System.Configuration.ConfigurationManager.AppSettings["PageDisallowCopy"];
                string pageTypeToCheck = string.Format("*-{0}-*", e.Page.PageTypeName);

                if (!string.IsNullOrEmpty(disallowCopyPageTypes) && disallowCopyPageTypes.IndexOf(pageTypeToCheck) != -1)
                {
                    e.CancelAction = true;
                    e.CancelReason = String.Format("Copy function disallowed for {0}", e.Page.PageTypeName);
                }
            }
            MiscellaneousSessionWrapper.IsPageCopied = true;
        }

        /// <summary>
        /// Get String After Replace Russian
        /// </summary>
        /// <param name="strToSearch"></param>
        /// <returns></returns>
        public string GetStringAfterReplaceRussian(string strToSearch)
        {
            string stringToReturn = "";
            int strCounter = 0;
            int stringLength = strToSearch.Length;
            while (strCounter < stringLength)
            {
                int stringLen = 1;
                bool matchFound = false;
                foreach (string key in stringMapping.Keys)
                {
                    int keyLen = key.Length;

                    int substringLength = keyLen;
                    if ((substringLength + strCounter) > stringLength)
                    {
                        substringLength = stringLength - strCounter;
                    }
                    string currentStr = strToSearch.Substring(strCounter, substringLength);
                    if (currentStr == key)
                    {
                        stringLen = keyLen;
                        stringToReturn += stringMapping[key];
                        matchFound = true;
                        break;
                    }
                }
                strCounter += stringLen;
            }

            return stringToReturn;
        }

        #endregion Private Methods
    }
}