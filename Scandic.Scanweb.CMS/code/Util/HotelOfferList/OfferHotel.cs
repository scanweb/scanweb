﻿//  Description					:   OfferHotel                                            //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using EPiServer.Core;

namespace Scandic.Scanweb.CMS.code.Util.HotelOfferList
{
    /// <summary>
    /// Hotel object built from HotelOffer-xml property
    /// </summary>
    public class OfferHotel
    {
        public string HotelID;
        public string HotelName;
        public string FirstPrice;

        public string SecondPrice;
        public string ThirdPrice;
        public string SpecialRateName;
        public string Currency;

        /// <summary>
        /// GetHotelPageData
        /// </summary>
        /// <returns>PageData</returns>
        public PageData GetHotelPageData()
        {
            ListOfHotels HotelList = new ListOfHotels();
            return HotelList.GetHotelPageData(this.HotelID);
        }

        /// <summary>
        /// OfferHotel
        /// </summary>
        /// <param name="hotelID"></param>
        /// <param name="hotelName"></param>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <param name="third"></param>
        /// <param name="specialRateName"></param>
        /// <param name="currency"></param>
        public OfferHotel(string hotelID, string hotelName, string first, string second, string third,
                          string specialRateName, string currency)
        {
            this.HotelID = hotelID;
            this.HotelName = hotelName;
            this.FirstPrice = first;
            this.SecondPrice = second;
            this.ThirdPrice = third;
            this.SpecialRateName = specialRateName;
            this.Currency = currency;
        }
    }
}
