﻿//  Description					:   OfferCity                                             //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

namespace Scandic.Scanweb.CMS.code.Util.HotelOfferList
{
    /// <summary>
    /// A City object with an list of it's containing hotels
    /// </summary>
    public class OfferCity
    {
        public string CityID;
        public string CityName;
        private List<OfferHotel> hotelsInCity;

        /// <summary>
        /// OfferCity
        /// </summary>
        /// <param name="CityID"></param>
        /// <param name="CityName"></param>
        /// <param name="hotel"></param>
        public OfferCity(string CityID, string CityName, OfferHotel hotel)
        {
            this.CityID = CityID;
            this.CityName = CityName;
            hotelsInCity = new List<OfferHotel>();
            if (!hotelsInCity.Contains(hotel))
                hotelsInCity.Add(hotel);
        }

        /// <summary>
        /// AddHotelToCity
        /// </summary>
        /// <param name="hotel"></param>
        public void AddHotelToCity(OfferHotel hotel)
        {
            if (!hotelsInCity.Contains(hotel))
                hotelsInCity.Add(hotel);
        }

        /// <summary>
        /// CheckIfHotelExcistsInCity
        /// </summary>
        /// <param name="HotelID"></param>
        /// <returns>True/False</returns>
        public bool CheckIfHotelExcistsInCity(string HotelID)
        {
            foreach (OfferHotel hotel in hotelsInCity)
            {
                if (hotel.HotelID == HotelID)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// GetListOfHotels
        /// </summary>
        /// <returns>ListOfHotels</returns>
        public List<OfferHotel> GetListOfHotels()
        {
            return hotelsInCity;
        }
    }
}
