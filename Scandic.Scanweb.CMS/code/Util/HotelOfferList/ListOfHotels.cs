//  Description					:   ListOfHotels                                          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;

namespace Scandic.Scanweb.CMS.code.Util.HotelOfferList
{
    /// <summary>
    /// Builds a complete list of all hotels. Each hotel has properties for the country and city the hotel is in.
    /// </summary>
    public class ListOfHotels
    {
        public struct HotelsIntree
        {
            public string HotelPageID;
            public string CityPageID;
            public string CountryPageID;

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="HotelPageID"></param>
            /// <param name="CityPageID"></param>
            /// <param name="CountryPageID"></param>
            public HotelsIntree(string HotelPageID, string CityPageID, string CountryPageID)
            {
                this.HotelPageID = HotelPageID;
                this.CityPageID = CityPageID;
                this.CountryPageID = CountryPageID;
            }
        }

        public List<HotelsIntree> HotelList;

        /// <summary>
        /// ListOfHotels
        /// </summary>
        public ListOfHotels()
        {
            HotelList = new List<HotelsIntree>();
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            PageReference countryContainer = (PageReference) rootPage["CountryContainer"];
            PageDataCollection countryPages = DataFactory.Instance.GetChildren(countryContainer);
            int hotelContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelContainerPageTypeGUID"])).ID;


            foreach (PageData Country in countryPages)
            {
                PageDataCollection cityPages = DataFactory.Instance.GetChildren(Country.PageLink);
                foreach (PageData city in cityPages)
                {
                    PageDataCollection CityContainer = DataFactory.Instance.GetChildren(city.PageLink);
                    PageDataCollection HotelPages = new PageDataCollection();
                    foreach (PageData CityContainerPage in CityContainer)
                    {
                        if (CityContainerPage.PageTypeID == hotelContainerPageTypeID)
                            HotelPages = DataFactory.Instance.GetChildren(CityContainerPage.PageLink);
                    }
                    foreach (PageData Hotel in HotelPages)
                    {
                        AddHoteltoList(Hotel.PageLink.ID.ToString(), city.PageLink.ID.ToString(),
                                       Country.PageLink.ID.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// AddHoteltoList
        /// </summary>
        /// <param name="HotelID"></param>
        /// <param name="CityID"></param>
        /// <param name="CountryID"></param>
        private void AddHoteltoList(string HotelID, string CityID, string CountryID)
        {
            this.HotelList.Add(new HotelsIntree(HotelID, CityID, CountryID));
        }

        /// <summary>
        /// GetHotelName
        /// </summary>
        /// <param name="HotelID"></param>
        /// <returns>Hotel Name</returns>
        public string GetHotelName(string HotelID)
        {
            foreach (HotelsIntree Hotel in HotelList)
            {
                PageData HotelPage = DataFactory.Instance.GetPage(PageReference.Parse(Hotel.HotelPageID),
                                                                  EPiServer.Security.AccessLevel.NoAccess);
                string OperaID = HotelPage["OperaID"] as string;
                if (OperaID == HotelID)
                {
                    PageData pd = DataFactory.Instance.GetPage(new PageReference(Int32.Parse(Hotel.HotelPageID)),
                                                               EPiServer.Security.AccessLevel.NoAccess);
                    return pd["Heading"] as string ?? String.Empty;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// GetHotelPageData
        /// </summary>
        /// <param name="HotelID"></param>
        /// <returns>PageData</returns>
        public PageData GetHotelPageData(string HotelID)
        {
            foreach (HotelsIntree Hotel in HotelList)
            {
                PageData HotelPage = DataFactory.Instance.GetPage(PageReference.Parse(Hotel.HotelPageID),
                                                                  EPiServer.Security.AccessLevel.NoAccess);
                string OperaID = HotelPage["OperaID"] as string;
                if (OperaID == HotelID)
                {
                    PageData pd = DataFactory.Instance.GetPage(new PageReference(Int32.Parse(Hotel.HotelPageID)),
                                                               EPiServer.Security.AccessLevel.NoAccess);
                    return pd;
                }
            }
            return null;
        }

        /// <summary>
        /// GetCityName
        /// </summary>
        /// <param name="HotelID"></param>
        /// <returns>CityName</returns>
        public string GetCityName(string HotelID)
        {
            foreach (HotelsIntree Hotel in HotelList)
            {
                PageData HotelPage = DataFactory.Instance.GetPage(PageReference.Parse(Hotel.HotelPageID),
                                                                  EPiServer.Security.AccessLevel.NoAccess);
                string OperaID = HotelPage["OperaID"] as string;
                if (OperaID == HotelID)
                {
                    PageData pd = DataFactory.Instance.GetPage(new PageReference(Int32.Parse(Hotel.CityPageID)),
                                                               EPiServer.Security.AccessLevel.NoAccess);
                    return pd.PageName;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// GetCityPageData
        /// </summary>
        /// <param name="HotelID"></param>
        /// <returns>CityPageData</returns>
        public PageData GetCityPageData(string HotelID)
        {
            foreach (HotelsIntree Hotel in HotelList)
            {
                PageData HotelPage = DataFactory.Instance.GetPage(PageReference.Parse(Hotel.HotelPageID),
                                                                  EPiServer.Security.AccessLevel.NoAccess);
                string OperaID = HotelPage["OperaID"] as string;
                if (OperaID == HotelID)
                {
                    PageData pd = DataFactory.Instance.GetPage(new PageReference(Int32.Parse(Hotel.CityPageID)),
                                                               EPiServer.Security.AccessLevel.NoAccess);
                    return pd;
                }
            }
            return null;
        }

        /// <summary>
        /// GetCityID
        /// </summary>
        /// <param name="HotelID"></param>
        /// <returns>CityID</returns>
        public string GetCityID(string HotelID)
        {
            foreach (HotelsIntree hotel in HotelList)
            {
                PageData HotelPage = DataFactory.Instance.GetPage(PageReference.Parse(hotel.HotelPageID),
                                                                  EPiServer.Security.AccessLevel.NoAccess);
                string OperaID = HotelPage["OperaID"] as string;
                if (OperaID == HotelID)
                    return hotel.CityPageID;
            }
            return string.Empty;
        }

        /// <summary>
        /// GetCountryName
        /// </summary>
        /// <param name="HotelID"></param>
        /// <returns>CountryName</returns>
        public string GetCountryName(string HotelID)
        {
            foreach (HotelsIntree Hotel in HotelList)
            {
                PageData HotelPage = DataFactory.Instance.GetPage(PageReference.Parse(Hotel.HotelPageID),
                                                                  EPiServer.Security.AccessLevel.NoAccess);
                string OperaID = HotelPage["OperaID"] as string;
                if (OperaID == HotelID)
                {
                    PageData pd = DataFactory.Instance.GetPage(new PageReference(Int32.Parse(Hotel.CountryPageID)),
                                                               EPiServer.Security.AccessLevel.NoAccess);
                    return pd.PageName;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// GetCountryPageData
        /// </summary>
        /// <param name="HotelID"></param>
        /// <returns>PageData</returns>
        public PageData GetCountryPageData(string HotelID)
        {
            foreach (HotelsIntree Hotel in HotelList)
            {
                PageData HotelPage = DataFactory.Instance.GetPage(PageReference.Parse(Hotel.HotelPageID),
                                                                  EPiServer.Security.AccessLevel.NoAccess);
                string OperaID = HotelPage["OperaID"] as string;
                if (OperaID == HotelID)
                {
                    PageData pd = DataFactory.Instance.GetPage(new PageReference(Int32.Parse(Hotel.CountryPageID)),
                                                               EPiServer.Security.AccessLevel.NoAccess);
                    return pd;
                }
            }
            return null;
        }

        /// <summary>
        /// GetCountryID
        /// </summary>
        /// <param name="HotelID"></param>
        /// <returns>CountryID</returns>
        public string GetCountryID(string HotelID)
        {
            foreach (HotelsIntree hotel in HotelList)
            {
                PageData HotelPage = DataFactory.Instance.GetPage(PageReference.Parse(hotel.HotelPageID),
                                                                  EPiServer.Security.AccessLevel.NoAccess);
                string OperaID = HotelPage["OperaID"] as string;
                if (OperaID == HotelID)
                    return hotel.CountryPageID;
            }
            return string.Empty;
        }

        /// <summary>
        /// getAmountOfHotelsInList
        /// </summary>
        /// <returns>AmountOfHotelsInList</returns>
        public string getAmountOfHotelsInList()
        {
            return HotelList.Count.ToString();
        }

        /// <summary>
        /// CheckIfHotelExcistsInList
        /// </summary>
        /// <param name="HotelID"></param>
        /// <returns>True/False</returns>
        public bool CheckIfHotelExcistsInList(string HotelID)
        {
            foreach (HotelsIntree hotel in HotelList)
            {
                PageData HotelPage = DataFactory.Instance.GetPage(PageReference.Parse(hotel.HotelPageID),
                                                                  EPiServer.Security.AccessLevel.NoAccess);
                string OperaID = HotelPage["OperaID"] as string;

                if (OperaID == HotelID)
                    return true;
            }
            return false;
        }
    }
}