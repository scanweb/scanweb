﻿//  Description					:   OfferCountry                                          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

namespace Scandic.Scanweb.CMS.code.Util.HotelOfferList
{
    /// <summary>
    /// A Country object with a list of it's containing Citys
    /// </summary>
    public class OfferCountry
    {
        public string CountryID;
        public string CountryName;
        private List<OfferCity> CityList;

        /// <summary>
        /// OfferCountry
        /// </summary>
        /// <param name="CountryID"></param>
        /// <param name="CountryName"></param>
        /// <param name="city"></param>
        public OfferCountry(string CountryID, string CountryName, OfferCity city)
        {
            this.CountryID = CountryID;
            this.CountryName = CountryName;
            CityList = new List<OfferCity>();
            if (!CityList.Contains(city))
                CityList.Add(city);
        }

        /// <summary>
        /// AddCityToCountry
        /// </summary>
        /// <param name="city"></param>
        public void AddCityToCountry(OfferCity city)
        {
            if (!CityList.Contains(city))
                CityList.Add(city);
        }

        /// <summary>
        /// CheckIfCityExcistsInCountry
        /// </summary>
        /// <param name="cityID"></param>
        /// <returns>True/False</returns>
        public bool CheckIfCityExcistsInCountry(string cityID)
        {
            foreach (OfferCity city in CityList)
            {
                if (city.CityID == cityID)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// GetListOfCity
        /// </summary>
        /// <returns>ListOfCity</returns>
        public List<OfferCity> GetListOfCity()
        {
            return CityList;
        }
    }
}
