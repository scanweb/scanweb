using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.Configuration;
using EPiServer;
using EPiServer.Core;
using EPiServer.Configuration;
using EPiServer.DataAbstraction;
using EPiServer.Web.PropertyControls;
using EPiServer.Web.WebControls;
using System.Xml;

namespace Scandic.Scanweb.CMS.code.Util.HotelOfferList
{
    /// <summary>
    /// HotelListInfo
    /// </summary>
    public class HotelListInfo
    {
        private struct HotelsIntree
        {
            public string HotelID;
            public string HotelName;
            public string CityName;
            public string CityID;
            public string CountryID;
            public string CountryName;
            public HotelsIntree
                (string HotelID, string HotelName, string CityID, string CityName, string CountryID, string CountryName)
            {
                this.HotelID = HotelID;
                this.HotelName = HotelName;
                this.CityID = CityID;
                this.CityName = CityName;
                this.CountryID = CountryID;
                this.CountryName = CountryName;
            }
        }

        private List<HotelsIntree> HotelList;

        /// <summary>
        /// Constructor
        /// </summary>
        public HotelListInfo()
        {
            HotelList = new List<HotelsIntree>();
            PageData rootPage = 
                DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess);
            PageReference countryContainer = (PageReference)rootPage["CountryContainer"];
            PageDataCollection countryPages = DataFactory.Instance.GetChildren(countryContainer);
            int hotelContainerPageTypeID = 
                PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelContainerPageTypeGUID"])).ID;


            foreach (PageData Country in countryPages)
            {
                PageDataCollection cityPages = DataFactory.Instance.GetChildren(Country.PageLink);
                foreach (PageData city in cityPages)
                {

                    PageDataCollection CityContainer = DataFactory.Instance.GetChildren(city.PageLink);
                    PageDataCollection HotelPages = new PageDataCollection();
                    foreach (PageData CityContainerPage in CityContainer)
                    {
                        if (CityContainerPage.PageTypeID == hotelContainerPageTypeID)
                            HotelPages = DataFactory.Instance.GetChildren(CityContainerPage.PageLink);

                    }
                    foreach (PageData Hotel in HotelPages)
                    {
                        addHoteltoList
                            (Hotel.PageLink.ID.ToString(), Hotel.PageName, city.PageLink.ID.ToString(), city.PageName,
                            Country.PageLink.ID.ToString(), Country.PageName);
                    }
                }
            }
        }

        /// <summary>
        /// addHoteltoList
        /// </summary>
        /// <param name="HotelID"></param>
        /// <param name="HotelName"></param>
        /// <param name="CityID"></param>
        /// <param name="CityName"></param>
        /// <param name="CountryID"></param>
        /// <param name="CountryName"></param>
        public void addHoteltoList
            (string HotelID, string HotelName, string CityID, string CityName, string CountryID, string CountryName)
        {
            this.HotelList.Add(new HotelsIntree(HotelID, HotelName, CityID, CityName, CountryID, CountryName));
        }

        /// <summary>
        /// getHotelName
        /// </summary>
        /// <param name="HotelID"></param>
        /// <returns>HotelName</returns>
        public string getHotelName(string HotelID)
        {
            foreach (HotelsIntree hi in HotelList)
            {
                if (hi.HotelID == HotelID)
                    return hi.HotelName;
            }
            return "";
        }

        /// <summary>
        /// getCityName
        /// </summary>
        /// <param name="HotelID"></param>
        /// <returns>CityName</returns>
        public string getCityName(string HotelID)
        {
            foreach (HotelsIntree hi in HotelList)
            {
                if (hi.HotelID == HotelID)
                    return hi.CityName;
            }
            return "";
        }

        /// <summary>
        /// getCountryName
        /// </summary>
        /// <param name="HotelID"></param>
        /// <returns>CountryName</returns>
        public string getCountryName(string HotelID)
        {
            foreach (HotelsIntree hi in HotelList)
            {
                if (hi.HotelID == HotelID)
                    return hi.CountryName;
            }
            return "";
        }

     
        /// <summary>
        /// getAmount
        /// </summary>
        /// <returns>Amount</returns>
        public string getAmount()
        {
            return HotelList.Count.ToString();
        }

    }
}
