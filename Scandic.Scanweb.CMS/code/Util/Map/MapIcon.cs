// <copyright file="MapIcon.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Map Icon implementation</summary>

namespace Scandic.Scanweb.CMS.code.Util.Map
{

    /// <summary>
    /// Class  representating  a Map Icon
    /// </summary>
    public class MapIcon
    {

        #region Public Properties

        /// <summary>
        /// Set or Get the URL of the Image
        /// </summary>
        /// <value>The image URL.</value>
        public string ImageURL { set; get; }

        /// <summary>
        /// Set or Get the URL of the Shadow Image
        /// </summary>
        /// <value>The shadow image URL.</value>
        public string ShadowImageURL { set; get; }

        /// <summary>
        /// Set or Get the size of the Icon
        /// </summary>
        /// <value>The size of the icon.</value>
        public MapSize IconSize { set; get; }

        /// <summary>
        /// Set or Get the point where the Icon has to be Anchored
        /// </summary>
        /// <value>The icon anchor.</value>
        public MapPoint IconAnchor { set; get; }

        /// <summary>
        /// Set or Get where the Information window has to be Anchored
        /// </summary>
        /// <value>The info window anchor.</value>
        public MapPoint InfoWindowAnchor { set; get; }

        /// <summary>
        /// Set or Get the size of the shadow
        /// </summary>
        /// <value>The size of the shadow.</value>
        public MapSize ShadowSize { set; get; }

        #endregion // Public Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapIcon()
        {
        }

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="ImageURL">URL of the Icon Image</param>
        /// <param name="ShadowImageURL">URL of the Shadow Image</param>
        /// <param name="IconSize">Size of the Icon</param>
        /// <param name="ShadowSize">Size of the shadow</param>
        /// <param name="IconAnchor">pixel where icon should be anchored to the map</param>
        /// <param name="InfoWindowAnchor">pixel where information window should be anchored to the icon</param>
        public MapIcon(string ImageURL, string ShadowImageURL, MapSize IconSize, MapSize ShadowSize, MapPoint IconAnchor,
                       MapPoint InfoWindowAnchor)
        {
            this.ImageURL = ImageURL;
            this.ShadowImageURL = ShadowImageURL;
            this.IconSize = IconSize;
            this.ShadowSize = ShadowSize;
            this.IconAnchor = IconAnchor;
            this.InfoWindowAnchor = InfoWindowAnchor;
        }

        #endregion 
    }
}