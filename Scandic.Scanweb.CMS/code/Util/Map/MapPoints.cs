// <copyright file="MapPoints.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>MapPoints implementation</summary>

using System.Collections;

namespace Scandic.Scanweb.CMS.code.Util.Map
{

    /// <summary>
    /// Class representing a collection of Points
    /// </summary>
    public class MapPoints : CollectionBase
    {

        #region Public Methods

        /// <summary>
        /// Add a MapPoint
        /// </summary>
        /// <param name="currPoint">Point to be added</param>
        public void Add(MapPoint currPoint)
        {
            this.InnerList.Add(currPoint);
        }

        /// <summary>
        /// Remove a MapPoint
        /// </summary>
        /// <param name="currPoint">Point to be removed</param>
        public void Remove(MapPoint currPoint)
        {
            this.InnerList.Remove(currPoint);
        }

        /// <summary>
        /// Remove Point at a particular position
        /// </summary>
        /// <param name="index">Postition at which point is to be removed</param>
        /// <exception cref="T:System.ArgumentOutOfRangeException">index is less than zero.-or-index is equal to or greater than <see cref="P:System.Collections.CollectionBase.Count"></see>.</exception>
        public new void RemoveAt(int index)
        {
            this.InnerList.RemoveAt(index);
        }

        /// <summary>
        /// Insert a point at a particular position
        /// </summary>
        /// <param name="index">Position at which point is to be inserted</param>
        /// <param name="currPoint">Point to be inserted</param>
        public void Insert(int index, MapPoint currPoint)
        {
            this.InnerList.Insert(index, currPoint);
        }

        #endregion 
    }
}