// <copyright file="MapPoint.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Implementation of the MapPoint</summary>

namespace Scandic.Scanweb.CMS.code.Util.Map
{

    /// <summary>
    /// Class representation of Map Point.It represents
    /// a 2 dimensional coordinate
    /// </summary>
    public class MapPoint
    {
        

        #region Public Properties

        /// <summary>
        /// set or get the longitude of the point
        /// </summary>
        /// <value>The longitude.</value>
        public double Longitude { get; set; }

        /// <summary>
        /// set or get the latitude of the point
        /// </summary>
        /// <value>The latitude.</value>
        public double Latitude { get; set; }

        #endregion 

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="Latitude">latitude of the point</param>
        /// <param name="Longitude">longitude of the point</param>
        public MapPoint(double Latitude, double Longitude)
        {
            this.Latitude = Latitude;
            this.Longitude = Longitude;
        }

        #endregion 
    }
}