using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Collections;
using System.Data;
using System.Globalization;
using EPiServer.Core;
using Scandic.Scanweb.CMS.code.Util.Map;

namespace Scandic.Scanweb.CMS.code.Util.Map.GoogleMap
{
    /// <summary>
    /// MapControl
    /// </summary>
    public class MapControl : WebControl, IMap
    {
        CultureInfo ci = new System.Globalization.CultureInfo("en-US");
        private string mapKey = string.Empty;
        private StringBuilder initFunction;
        private string zoomAndCenter = string.Empty;
        private StringBuilder pointOverlay;
        private StringBuilder lineOverlay;
        private string funcNameKey = "Init";
        private string functionName = "InitMap";
        private string mapVariable = "map";
        private DriveDirection destinationWay;
        private string viewStateLocale = "en_US";
        private string directionVariable = "directions";
        private string directionPanelVariable = "directionsPanel";
        private string geocodeVariable = "geocoder";
        private string viewStateDestinationTo = "to";
        private string viewStateDestinationFrom = "from";
        private string arrayVariable = "arrayPoints";
        private bool scandicIcon = true;
        private string viewStateEnableDirections = "false";
        private string viewStateDirectionsContainer = "div";
        private string viewStateLatitude = "latitudefield";
        private string viewStateLongitude = "longitudefield";
        private string viewStateIcon = "iconurl";
        private string viewStateIconShadow = "iconshadowurl";
        private string viewStateDataBoundText = "textdataboundurl";
        private string viewStateHTMLText = "textHTMLurl";

        private IMapScriptGenerator scriptGenerator;
        private MapType mapType;
        private MapScrollControl mapScrollType;
        private bool enableDrag, enableInfoWindow, enableMapTypeControl, overrideEnabledrag, enableDblClickZoom, enableMouseScrollZoom;
        private object dataSource = null;

        public string MapVariable
        {
            get
            {
                if (this.ViewState[mapVariable] != null)
                {
                    return (string)this.ViewState[mapVariable];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[mapVariable] = value; }
        }

        /// <summary>
        /// Set or get the locale of the page
        /// </summary>
        public string Locale
        {
            get
            {
                if (this.ViewState[viewStateLocale] != null)
                {
                    return (string)this.ViewState[viewStateLocale];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateLocale] = value; }
        }

        /// <summary>
        /// Set or get the <see cref="DriveDirection"/> way
        /// </summary>
        public DriveDirection DestinationWay
        {
            get { return destinationWay; }
            set { destinationWay = value; }
        }

        /// <summary>
        /// Set or get the destination-from string
        /// </summary>
        public string DestinationFrom
        {
            get
            {
                if (this.ViewState[viewStateDestinationFrom] != null)
                {
                    return (string)this.ViewState[viewStateDestinationFrom];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateDestinationFrom] = value; }
        }

        /// <summary>
        /// Set or get the destination-to string
        /// </summary>
        public string DestinationTo
        {
            get
            {
                if (this.ViewState[viewStateDestinationTo] != null)
                {
                    return (string)this.ViewState[viewStateDestinationTo];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateDestinationTo] = value; }
        }

        /// <summary>
        /// set or get the key for using Google maps
        /// </summary>
        public string MapKey { set { mapKey = value; } get { return mapKey; } }

        /// <summary>
        /// set or get the Map Type whether Satellite, map or hybrid
        /// </summary>
        public MapType MapType { set { mapType = value; } get { return mapType; } }

        /// <summary>
        /// Set or get the Type of Scroll control on the map
        /// </summary>
        public MapScrollControl ScrollControlType { set { mapScrollType = value; } get { return mapScrollType; } }

        /// <summary>
        /// Enable dragging of the Map
        /// </summary>
        public bool EnableDragging { set { enableDrag = value; } get { return enableDrag; } }

        /// <summary>
        /// Enable driving directions of the Map
        /// </summary>
        public bool EnableDirections
        {
            get
            {
                if (this.ViewState[viewStateEnableDirections] != null)
                {
                    return bool.Parse(this.ViewState[viewStateEnableDirections].ToString());
                }
                return bool.Parse("false");
            }
            set { this.ViewState[viewStateEnableDirections] = value.ToString(); }
        }

        /// <summary>
        /// Set or get the container that should be viewing the directions
        /// </summary>
        public string DirectionsContainer
        {
            get
            {
                if (this.ViewState[viewStateDirectionsContainer] != null)
                {
                    return (string)this.ViewState[viewStateDirectionsContainer];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateDirectionsContainer] = value; }
        }

        /// <summary>
        /// Enable zoom by doubleclick of the Map
        /// </summary>
        public bool EnableDblClickZoom { set { enableDblClickZoom = value; } get { return enableDblClickZoom; } }

        /// <summary>
        /// Enable zoom by mouse scroll of the Map
        /// </summary>
        public bool EnableMouseScrollZoom { set { enableMouseScrollZoom = value; } get { return enableMouseScrollZoom; } }

        /// <summary>
        /// Set or get whether Pop up windows can be opened on the Map
        /// </summary>
        public bool EnableInfoWindow { set { enableInfoWindow = value; } get { return enableInfoWindow; } }

        /// <summary>
        /// Set or get whether the Scandic icon should be shown on the map (default is true)
        /// </summary>
        public bool ScandicIcon { set { scandicIcon = value; } get { return scandicIcon; } }
        /// <summary>
        /// Set or get whether to show the map type control on the map
        /// </summary>
        public bool ShowMapTypeControl
        {
            set { enableMapTypeControl = value; }
            get { return enableMapTypeControl; }
        }

        /// <summary>
        /// Set or get the data source
        /// </summary>
        public object DataSource
        {
            set
            {
                if (value == null ||
                    value is IListSource ||
                    value is IEnumerable)
                    dataSource = value;
                else
                    throw new ArgumentException();
            }
            get { return dataSource; }
        }

        /// <summary>
        /// set or get the Marker Text. Can be formatted HTML
        /// Used for databinding
        /// </summary>
        public string MarkerHTMLText
        {
            get
            {
                if (this.ViewState[viewStateHTMLText] != null)
                {
                    return (string)this.ViewState[viewStateHTMLText];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateHTMLText] = value; }
        }

        /// <summary>
        /// set or get the Marker Text. Can be formatted HTML
        /// Used for databinding
        /// </summary>
        public string MarkerDataBoundText
        {
            get
            {
                if (this.ViewState[viewStateDataBoundText] != null)
                {
                    return (string)this.ViewState[viewStateDataBoundText];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateDataBoundText] = value; }
        }

        /// <summary>
        /// set or get the Marker Longitude
        /// Used for databinding
        /// </summary>
        public string MarkerLongitudeField
        {
            get
            {
                if (this.ViewState[viewStateLongitude] != null)
                {
                    return (string)this.ViewState[viewStateLongitude];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateLongitude] = value; }
        }

        /// <summary>
        /// Set or get the Marker Latitude
        /// Used for databinding
        /// </summary>
        public string MarkerLatitudeField
        {
            get
            {
                if (this.ViewState[viewStateLatitude] != null)
                {
                    return (string)this.ViewState[viewStateLatitude];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateLatitude] = value; }
        }

        /// <summary>
        /// Set or get the Marker icon
        /// </summary>
        public string MarkerIconURL
        {
            get
            {
                if (this.ViewState[viewStateIcon] != null)
                {
                    return (string)this.ViewState[viewStateIcon];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateIcon] = value; }
        }

        /// <summary>
        /// Set or get the Marker icon shadow
        /// </summary>
        public string MarkerIconShadowURL
        {
            get
            {
                if (this.ViewState[viewStateIconShadow] != null)
                {
                    return (string)this.ViewState[viewStateIconShadow];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateIconShadow] = value; }
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <remarks>
        /// Since Google Maps needs to rendered as a DIV
        /// We set the Textwriter to render as a DIV
        ///</remarks>
        public MapControl()
            : base(HtmlTextWriterTag.Div)
        {
            initFunction = new StringBuilder();
            pointOverlay = new StringBuilder();
            lineOverlay = new StringBuilder();
            scriptGenerator = new GMapScriptGenerator();
            this.mapType = MapType.MAP;
            this.mapScrollType = MapScrollControl.NONE;
            this.enableDrag = true;
            this.enableInfoWindow = true;
        }

        /// <summary>
        /// Init method
        /// </summary>
        /// <param name="e">Event Args</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.viewStateLatitude = viewStateLatitude + this.ClientID;
            this.viewStateLongitude = viewStateLongitude + this.ClientID;
            this.viewStateHTMLText = viewStateHTMLText + this.ClientID;
            this.viewStateDataBoundText = viewStateDataBoundText + this.ClientID;
            this.viewStateIcon = viewStateIcon + this.ClientID;
            this.viewStateIconShadow = viewStateIconShadow + this.ClientID;
            this.viewStateIconShadow = viewStateIconShadow + this.ClientID;
        }

        /// <summary>
        /// Pre Render method
        /// </summary>
        /// <param name="e">Event Args</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            funcNameKey = funcNameKey + this.ClientID;
            functionName = functionName + this.ClientID;
            mapVariable = mapVariable + this.ClientID;
            arrayVariable = arrayVariable + this.ClientID;
            RegisterAllScripts();
        }

        /// <summary>
        /// Render method
        /// </summary>
        /// <param name="writer">Instance of HtmlTextWriter</param>
        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
            if (this.Site != null && this.Site.DesignMode)
            {
                writer.RenderBeginTag("h2");
                writer.Write(this.ID);
                writer.RenderEndTag();
            }
        }

        /// <summary>
        /// Method which registers all JavaScripts and the map key
        /// </summary>
        private void RegisterAllScripts()
        {
            string sKeyScript = scriptGenerator.RegisterMapKey(mapKey);
            string sMapKey = scriptGenerator.GetMapKey();
            if (!this.Page.ClientScript.IsClientScriptBlockRegistered(sMapKey))
                this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), sMapKey, sKeyScript);

            if (!this.Page.ClientScript.IsStartupScriptRegistered(funcNameKey))
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), funcNameKey, InitializeMap());
        }

        /// <summary>
        /// Method that Generates all the Javascript
        /// </summary>
        /// <returns>Javascript code as a string</returns>
        private string InitializeMap()
        {
            InitParams initParams = new InitParams();
            initParams.arrayVariable = arrayVariable;
            initParams.clientID = this.ClientID;
            initParams.DestinationFrom = DestinationFrom;
            initParams.DestinationTo = DestinationTo;
            initParams.directionPanelVariable = directionPanelVariable;
            initParams.DirectionsContainer = DirectionsContainer;
            initParams.directionVariable = directionVariable;
            initParams.enableDblClickZoom = enableDblClickZoom;
            initParams.enableDirections = EnableDirections;
            if (overrideEnabledrag)
                initParams.enableDrag = true;
            initParams.enableInfoWindow = enableInfoWindow;
            initParams.enableMapTypeControl = enableMapTypeControl;
            initParams.enableMouseScrollZoom = enableMouseScrollZoom;
            initParams.geocodeVariable = geocodeVariable;
            initParams.lineOverlay = lineOverlay;
            initParams.Locale = Locale;
            initParams.mapScrollType = mapScrollType;
            initParams.mapVariable = mapVariable;
            initParams.pointOverlay = pointOverlay;
            initParams.zoomAndCenter = zoomAndCenter;
            initParams.destinationWay = destinationWay;
            return scriptGenerator.GenerateStartUpScript(initParams);
        }

        /// <summary>
        /// DataBinding Method
        /// </summary>
        /// <param name="e">Event Args</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);

            IEnumerable mapUnitList = HelperDataResolver.GetResolvedDataSource(dataSource);

            if (mapUnitList != null)
            {
                foreach (object ditem in mapUnitList)
                {
                    try
                    {
                        string sText = string.Empty;
                        double lat = 0;
                        double lng = 0;

                        GetLatitudeLongitude(ditem, out lat, out lng);

                        MapPoint gp = new MapPoint(lat, lng);
                        MapMarker gm = null;
                        MapUnit mapUnit = (MapUnit)ditem;
                        sText = mapUnit.GenerateInfoWindowMarkUp();
                        gm = new MapMarker(gp);

                        if (MarkerIconURL != null && this.MarkerIconURL.Length > 0)
                        {
                            MapIcon gi = 
                                new MapIcon(MarkerIconURL, MarkerIconShadowURL, new MapSize(31, 27), 
                                    new MapSize(45, 29), new MapPoint(15, 13), new MapPoint(15, 13));
                            gm.Icon = gi;
                        }

                        if (ditem.GetType().Name.Equals("MapHotelUnit"))
                        {
                            if (!ScandicIcon)
                            {
                                gm.Icon = null;
                            }
                        }
                        
                        if (sText.Trim() != string.Empty)
                        {
                            pointOverlay = scriptGenerator.GenerateCreateMarkerScriptCall(gm, sText);
                        }
                        else
                        {
                            pointOverlay = scriptGenerator.GenerateCreateMarkerScriptCall(gm);
                        }

                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Could not Resolve Data source Fields: " + ex.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// GetLatitudeLongitude
        /// </summary>
        /// <param name="ditem"></param>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        private void GetLatitudeLongitude(object ditem, out double lat, out double lng)
        {
            if (typeof(string) == DataBinder.GetPropertyValue(ditem, this.MarkerLatitudeField).GetType()
                            && typeof(string) == DataBinder.GetPropertyValue(ditem, this.MarkerLongitudeField).GetType())
            {
                lat = double.Parse(DataBinder.GetPropertyValue(ditem, this.MarkerLatitudeField).ToString(), ci);
                lng = double.Parse(DataBinder.GetPropertyValue(ditem, this.MarkerLongitudeField).ToString(), ci);
            }
            else
            {
                lat = (double)DataBinder.GetPropertyValue(ditem, this.MarkerLatitudeField);
                lng = (double)DataBinder.GetPropertyValue(ditem, this.MarkerLongitudeField);
            }
        }

        
        #region IMap Members

        /// <summary>
        /// Method to Center and Zoom the map at a particular point
        /// </summary>
        /// <param name="curPoint">Point at which the map should be centered</param>
        /// <param name="ZoomLevel">Zoom level</param>
        public void CenterAndZoom(MapPoint curPoint, uint zoomLevel)
        {
            zoomAndCenter = 
                string.Format
                (ci, "ZoomIn{3}({0},{1},{2});", curPoint.Longitude, curPoint.Latitude, zoomLevel, this.ClientID);
        }

        /// <summary>
        /// Method to auto-center and zoom the map at a particular point from objects in datasource
        /// </summary>
        public void AutoCenterAndZoom()
        {
            StringBuilder mapUnitsBoundsString = new StringBuilder();
            IEnumerable mapUnitList = HelperDataResolver.GetResolvedDataSource(DataSource);
            if (mapUnitList != null)
            {
                foreach (object ditem in mapUnitList)
                {
                    try
                    {
                        string sText = string.Empty;
                        double lat = 0;
                        double lng = 0;

                        GetLatitudeLongitude(ditem, out lat, out lng);
                        mapUnitsBoundsString.Append(scriptGenerator.ExtendBounds(lat.ToString().Replace(',', '.'), lng.ToString().Replace(',', '.')));
                    }
                    catch (Exception AutoCenterAndZoomEx) { throw new Exception(AutoCenterAndZoomEx.ToString()); }
                }
            }

            zoomAndCenter = mapUnitsBoundsString.ToString() + string.Format(ci, "ZoomIn{0}(locBounds.getCenter().lng(),locBounds.getCenter().lat(), map{0}.getBoundsZoomLevel(locBounds));", this.ClientID);
        }

        #endregion
    }

    /// <summary>
    /// InitParams
    /// </summary>
    public class InitParams
    {
        public string mapVariable;
        public string arrayVariable;
        public string clientID;

        public bool enableDrag;
        public bool enableInfoWindow;
        public bool enableMapTypeControl;
        public bool enableDblClickZoom;
        public bool enableMouseScrollZoom;
        public bool enableDirections;


        public string directionVariable;
        public string directionPanelVariable;
        public string geocodeVariable;
        public string DirectionsContainer;
        public string DestinationTo;
        public string DestinationFrom;
        public string Locale;
        public DriveDirection destinationWay;

        public MapScrollControl mapScrollType;
        public StringBuilder pointOverlay;
        public StringBuilder lineOverlay;

        public string zoomAndCenter;
    }
}
