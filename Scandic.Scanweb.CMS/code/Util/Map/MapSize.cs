// <copyright file="MapSize.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Implementation of the MapSize</summary>

namespace Scandic.Scanweb.CMS.code.Util.Map
{
    /// <summary>
    /// Class representing  Google GSize which is a 2 dimensional
    /// size measurement
    /// </summary>
    public class MapSize
    {
      
        #region Public Properties

        /// <summary>
        /// set or get width of the Size.Represents longitude degrees
        /// </summary>
        /// <value>The width.</value>
        public int Width { set; get; }

        /// <summary>
        /// set or get height of the Size.Represents latitude degrees
        /// </summary>
        /// <value>The height.</value>
        public int Height { set; get; }

        #endregion 

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Width">Width of the Size represents longitude degrees</param>
        /// <param name="Height">Height of the Size represents latitude degrees</param>
        public MapSize(int Width, int Height)
        {
            this.Width = Width;
            this.Height = Height;
        }
        #endregion 
    }
}