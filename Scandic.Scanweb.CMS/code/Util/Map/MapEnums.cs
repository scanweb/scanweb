// <copyright file="MapEnums.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Enums that all of the maps should use</summary>

namespace Scandic.Scanweb.CMS.code.Util.Map
{
    #region System NameSpaces

    #endregion // System NameSpaces

    /// <summary>
    /// Vendor of the map
    /// </summary>
    public enum MapVendor
    {
        /// <summary>
        /// Google map
        /// </summary>
        GOOGLE,

        /// <summary>
        /// Bing map
        /// </summary>
        BING,

        /// <summary>
        /// Navteq map
        /// </summary>
        NAVTEQ
    }

    /// <summary>
    /// Road driving directions
    /// </summary>
    public enum DriveDirection
    {
        /// <summary>
        /// To direction
        /// </summary>
        TO,

        /// <summary>
        /// From direction
        /// </summary>
        FROM
    }

    /// <summary>
    /// Enumerator for different types of Maps
    /// </summary>
    public enum MapType
    {
        /// <summary>
        /// Satellite map
        /// </summary>
        SATELLITE,

        /// <summary>
        /// Plain Map
        /// </summary>
        MAP,

        /// <summary>
        /// Hybrid map
        /// </summary>
        HYBRID
    }

    /// <summary>
    /// This enum should only be used if there are cases 
    /// to decide the zoom level and map center if there are
    /// few hotels on display
    /// </summary>

    public enum CenterAndZoomLevel
    {
        ///<summary>
        ///for City Zoom in level
        ///</summary>
        CITY,
        ///<summary>
        ///for HOTEL zoom in level
        ///</summary>
        HOTEL,
        ///<summary>
        ///for Country zoom in level
        ///</summary>
        COUNTRY,
        /// <summary>
        /// for showing all hotels in viewport
        /// </summary>
        ALL

    }

    /// <summary>
    /// This enum should only be used if there are some 
    /// cases where we would like to know on which page
    /// the map is sitting
    /// </summary>

    public enum MapPageType
    {
        /// <summary>
        /// All old Map Pages
        /// </summary>
        OLD,

        /// <summary>
        /// Find A Hotel Page
        /// </summary>
        FINDAHOTEL,

        /// <summary>
        /// Expansion Page
        /// </summary>
        EXPANSION,

        /// <summary>
        /// Select hotel page
        /// </summary>
        SELECTHOTEL,

        /// <summary>
        /// Confirmation Page
        /// </summary>
        CONFIRMATION,
        ///<SUMMARY>
        ///From HotelOverview Landing Page
        ///</summary>
        HOTELOVERVIEW,
        ///<SUMMARY>
        ///From HotelOverview - CITY
        ///</summary>
        HOTELOVERVIEWCITY,
        ///<SUMMARY>
        ///From HotelOverview -INDIVIDUAL HOTEL
        ///</summary>
        HOTELOVERVIEWHOTEL,
        ///<summary>
        ///From Hotel Overview- LocationPage
        ///</summary>
        HOTELLOCATION,
        }
}
