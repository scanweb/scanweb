// <copyright file="MapUnit.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>base clss for all the Unit classes</summary>

using System;

namespace Scandic.Scanweb.CMS.code.Util.Map
{

    /// <summary>
    /// Represents a Google Map Unit
    /// </summary>
    public class MapUnit
    {
        #region Private Fields

        /// <summary>
        /// html content of pop-up
        /// </summary>
        private string html;

        /// <summary>
        /// unit icon url
        /// </summary>
        private string iconUrl = string.Empty;

        /// <summary>
        /// unit shadow url
        /// </summary>
        private string shadowUrl = string.Empty;

        /// <summary>
        /// Type of infobox(BASIC/ADVANCED)
        /// </summary>
        private InfoBoxType infoBoxType = InfoBoxType.BASIC;

        #endregion 

        #region Public Properties

        /// <summary>
        /// Gets or sets longitude
        /// </summary>
        /// <value>The longitude.</value>
        public double Longitude { get; set; }

        /// <summary>
        /// Gets or sets latitude
        /// </summary>
        /// <value>The latitude.</value>
        public double Latitude { get; set; }

        /// <summary>
        /// Gets or sets zoom
        /// </summary>
        /// <value>The zoom.</value>
        public int Zoom { get; set; }

        /// <summary>
        /// Gets or sets the icon URL.
        /// </summary>
        /// <value>The icon URL.</value>
        public string IconUrl
        {
            get { return iconUrl; }
            set { iconUrl = value; }
        }

        /// <summary>
        /// Gets or sets the shadow URL.
        /// </summary>
        /// <value>The shadow URL.</value>
        public string ShadowUrl
        {
            get { return shadowUrl; }
            set { shadowUrl = value; }
        }

        /// <summary>
        /// Gets or sets html
        /// </summary>
        /// <value>The HTML.</value>
        public string Html
        {
            get { return this.html; }
            set { this.html = value; }
        }

        /// <summary>
        /// Gets or sets infoBoxType
        /// </summary>
        /// <value>The type of the info box.</value>
        public InfoBoxType InfoBoxType
        {
            get { return infoBoxType; }
            set { infoBoxType = value; }
        }

        /// <summary>
        /// Gets or sets enableUnitNumbering
        /// </summary>
        /// <value><c>true</c> if [enable unit numbering]; otherwise, <c>false</c>.</value>
        public bool EnableUnitNumbering { get; set; }

        /// <summary>
        /// Gets the width of the pop up max.
        /// </summary>
        /// <value>The width of the pop up max.</value>
        public int PopUpMaxWidth
        {
            get
            {
                int popUpMaxWidth = GoogleMapConstants.MAX_WIDTH_BASIC_INFOBOX;
                if (infoBoxType == InfoBoxType.ADVANCED)
                {
                    popUpMaxWidth = GoogleMapConstants.MAX_WIDTH_ADVANCED_INFOBOX;
                }
                return popUpMaxWidth;
            }
        }

        #endregion // Public Properties

        #region Constructors

        /// <summary>
        /// Contruction
        /// </summary>
        public MapUnit()
        {
        }

        /// <summary>
        /// Overloaded contructor
        /// </summary>
        /// <param name="longitude">Longitude</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="zoom">Zoom parameter</param>
        /// <param name="iconUrl">The icon URL.</param>
        /// <param name="shadowUrl">The shadow URL.</param>
        public MapUnit(double longitude, double latitude, int zoom, string iconUrl, string shadowUrl)
        {
            this.Longitude = longitude;
            this.Latitude = latitude;
            this.Zoom = zoom;
            this.iconUrl = iconUrl;
            this.shadowUrl = shadowUrl;
        }

        /// <summary>
        /// Overloaded contructor
        /// </summary>
        /// <param name="longitude">Longitude</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="zoom">Zoom parameter</param>
        /// <param name="html">Html to show in "bubble"</param>
        /// <param name="iconUrl">The icon URL.</param>
        /// <param name="shadowUrl">The shadow URL.</param>
        public MapUnit(double longitude, double latitude, int zoom, string html, string iconUrl, string shadowUrl)
        {
            this.Longitude = longitude;
            this.Latitude = latitude;
            this.Zoom = zoom;
            this.html = html;
            this.iconUrl = iconUrl;
            this.shadowUrl = shadowUrl;
        }

        #endregion 

        #region Internal Virtual Methods

        /// <summary>
        /// Generates the html markup for the pop-up of map unit
        /// </summary>
        /// <returns>returns pop-up html content</returns>
        internal virtual string GenerateInfoWindowMarkUp()
        {
            if (!String.IsNullOrEmpty(html))
            {
                return html;
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion 
    }

    /// <summary>
    /// Enum specifies the type of the InfoBox
    /// </summary>
    public enum InfoBoxType
    {
        /// <summary>
        /// All old maps falls into this category
        /// </summary>
        BASIC,

        /// <summary>
        /// Find A Hotel map falls into this category
        /// </summary>
        ADVANCED,

        /// <summary>
        /// Select hotel and Confirmation page maps
        /// </summary>
        ADVANCED_SELECTHOTEL,

        //Vrushali | artf1167579 : Corporate Identifier not present in Select Hotel listing | Styling changed.
        /// <summary>
        /// Select hotel and Confirmation page maps when corporate code has been entered. 
        /// This is used to increase the height of the icon.
        /// </summary>
        ADVANCED_SELECTHOTEL_DNUMBER,

        /// <summary>
        /// Confirmation Page
        /// </summary>
        ADVANCED_CONFIRMATION,

        FIND_YOUR_DESTINATION
    }

    public enum MarkerRateTextType
    {
        /// <summary>
        /// Rate per night
        /// </summary>
        RATEPERNIGHT,

        /// <summary>
        /// Rate per stay
        /// </summary>
        RATEPERSTAY,

        /// <summary>
        /// Other text
        /// </summary>
        OTHER
    }
}