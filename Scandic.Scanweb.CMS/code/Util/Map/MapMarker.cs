// <copyright file="MapMarker.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Implementation of the MapMarker</summary>

namespace Scandic.Scanweb.CMS.code.Util.Map
{

    /// <summary>
    /// Class Representing Google GMarker. It is a map overlay
    /// that shows an icon on the map.
    /// </summary>
    public class MapMarker
    {
        #region Private Fields

        #endregion

        #region Public Properties

        /// <summary>
        /// set or get Point to be displayed
        /// </summary>
        /// <value>The point.</value>
        public MapPoint Point { set; get; }

        /// <summary>
        /// set or get Icon to be used
        /// </summary>
        /// <value>The icon.</value>
        public MapIcon Icon { set; get; }

        #endregion 

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="point">The point.</param>
        public MapMarker(MapPoint point)
        {
            this.Point = point;
        }

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="point">Point to be displayed</param>
        /// <param name="icon">Icon to be used</param>
        public MapMarker(MapPoint point, MapIcon icon)
        {
            this.Point = point;
            this.Icon = icon;
        }

        #endregion 
    }
}