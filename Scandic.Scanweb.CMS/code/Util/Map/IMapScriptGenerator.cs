using System;
using System.Collections.Generic;
using System.Text;

namespace Scandic.Scanweb.CMS.code.Util.Map
{
    /// <summary>
    /// IMapScriptGenerator
    /// </summary>
    interface IMapScriptGenerator
    {
        string RegisterMapKey(string mapKey);
        string GetMapKey();
        string GenerateOverLayPointFuncCall(string sArrayVariable, string sControlClientID);
        string GenerateCustomOverLayPointFuncCall(string sArrayVariable, string sControlClientID);
        string GenerateOverlayLineFuncCall(string sArrayVariable, string sControlClientID);
        string GenerateNewOverLayFunction(string sGMapVariable, string sClientID);
        string GenerateZoomInFunction(string sGMapVariable, string sClientID);
        string ExtendBounds(string lat, string lng);
        string GenerateMapType(MapType mapType, string sGMapVariable);
        string GenerateGMapVariableInitialization(string sGMapVariable, string sClientID, 
            bool bEnableDrag, bool bEnableInfoWindow, bool bEnableMapTypeControl, bool bEnableDblClickZoom, 
            bool bEnableMouseScrollZoom);
        string GenerateScrollControl(MapScrollControl scrollType, string sGmapVariable);

        string GenerateStartUpScript(Scandic.Scanweb.CMS.code.Util.Map.GoogleMap.InitParams initParams);
    }
}
