using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using EPiServer.Core;
using System.Collections;

namespace Scandic.Scanweb.CMS.Util.ImageVault
{
    /// <summary>
    /// Utils
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// IsNoImagesForPopUp
        /// </summary>
        /// <param name="page"></param>
        /// <returns>True/False</returns>
        public static bool IsNoImagesForPopUp(PageData page)
        {
            bool showImagePopup = false;
            int countNulls = 0;

            if (page != null)
            {
                ArrayList listProperties = new ArrayList();

                if (!String.IsNullOrEmpty(page["GeneralImage1"] as string))
                    listProperties.Add(page["GeneralImage1"] as string);
                if (!String.IsNullOrEmpty(page["GeneralImage2"] as string))
                    listProperties.Add(page["GeneralImage2"] as string);
                if (!String.IsNullOrEmpty(page["GeneralImage3"] as string))
                    listProperties.Add(page["GeneralImage3"] as string);
                if (!String.IsNullOrEmpty(page["GeneralImage4"] as string))
                    listProperties.Add(page["GeneralImage4"] as string);

                if (!String.IsNullOrEmpty(page["RoomImage1"] as string))
                    listProperties.Add(page["RoomImage1"] as string);
                if (!String.IsNullOrEmpty(page["RoomImage2"] as string))
                    listProperties.Add(page["RoomImage2"] as string);
                if (!String.IsNullOrEmpty(page["RoomImage3"] as string))
                    listProperties.Add(page["RoomImage3"] as string);
                if (!String.IsNullOrEmpty(page["RoomImage4"] as string))
                    listProperties.Add(page["RoomImage4"] as string);

                if (!String.IsNullOrEmpty(page["RestBarImage1"] as string))
                    listProperties.Add(page["RestBarImage1"] as string);
                if (!String.IsNullOrEmpty(page["RestBarImage2"] as string))
                    listProperties.Add(page["RestBarImage2"] as string);
                if (!String.IsNullOrEmpty(page["RestBarImage3"] as string))
                    listProperties.Add(page["RestBarImage3"] as string);
                if (!String.IsNullOrEmpty(page["RestBarImage4"] as string))
                    listProperties.Add(page["RestBarImage4"] as string);

                if (!String.IsNullOrEmpty(page["LeisureImage1"] as string))
                    listProperties.Add(page["LeisureImage1"] as string);
                if (!String.IsNullOrEmpty(page["LeisureImage2"] as string))
                    listProperties.Add(page["LeisureImage2"] as string);
                if (!String.IsNullOrEmpty(page["LeisureImage3"] as string))
                    listProperties.Add(page["LeisureImage3"] as string);
                if (!String.IsNullOrEmpty(page["LeisureImage4"] as string))
                    listProperties.Add(page["LeisureImage4"] as string);

                foreach (string image in listProperties)
                {
                    if (!String.IsNullOrEmpty(image))
                    {
                        countNulls += 1;
                    }
                }

                if (countNulls != 0)
                    showImagePopup = true;
            }

            return showImagePopup;
        }
    }
}
