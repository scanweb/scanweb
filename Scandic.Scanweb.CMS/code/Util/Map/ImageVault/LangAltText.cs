using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ImageStoreNET.Developer;
using ImageStoreNET.Developer.Core;
using ImageVault.EPiServer5;

namespace Scandic.Scanweb.CMS.Util.ImageVault
{
    /// <summary>
    /// Class handles alttexts
    /// </summary>
    public class LangAltText
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public LangAltText()
        {

        }

        /// <summary>
        /// Gets alttext in the requested language
        /// </summary>
        /// <param name="language">language prefix</param>
        /// <param name="image">ImageVault image property as string</param>
        /// <returns>alternate text</returns>
        public string GetAltText(string language, string image)
        {
            string rightAltText = "Alttext_" + language.ToUpper();
            IVUrlBuilder ub = IVUrlBuilder.ParseUrl(image);

            try
            {
                IVFileData ivFile = IVDataFactory.GetFile
                    (ub.Id, ImageStoreNET.Developer.Security.IVAccessLevel.IgnoreAccess);

                foreach (IVMetaData meta in ivFile.MetaData)
                {
                    if (meta.Name == rightAltText)
                    {
                        if (meta.Value != "")
                            return HttpUtility.HtmlEncode(meta.Value); //Return requested Alt text
                    }
                }

                return HttpUtility.HtmlEncode(ivFile.Title);
            }
            catch
            {
                return String.Empty;
            }
        }
    }
}
