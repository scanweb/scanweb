using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ImageStoreNET.Developer;
using ImageStoreNET.Developer.Core;

namespace Scandic.Scanweb.CMS.Util.ImageVault
{
    /// <summary>
    /// Get Properties
    /// </summary>
    public class GetProperties
    {
        /// <summary>
        /// Gets the title of an ImageVault image
        /// </summary>
        /// <param name="language">Languge prefix</param>
        /// <param name="image">ImageVault image property as string</param>
        /// <returns>Image Title</returns>
        public string GetImageTitle(string language, string image)
        {
            IVUrlBuilder ub = IVUrlBuilder.ParseUrl(image);

            try
            {
                IVFileData ivFile = 
                    IVDataFactory.GetFile(ub.Id, ImageStoreNET.Developer.Security.IVAccessLevel.IgnoreAccess);
                return HttpUtility.HtmlEncode(ivFile.Title);
            }
            catch
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Gets the language controlled description of an ImageVault image
        /// </summary>
        /// <param name="language">Languge prefix</param>
        /// <param name="image">ImageVault image property as string</param>
        /// <returns>Image Description</returns>
        public string GetImageDescription(string language, string image)
        {
            string rightAltText = "Description_" + language.ToUpper();
            IVUrlBuilder ub = IVUrlBuilder.ParseUrl(image);

            try
            {
                IVFileData ivFile = 
                    IVDataFactory.GetFile(ub.Id, ImageStoreNET.Developer.Security.IVAccessLevel.IgnoreAccess);

                foreach (IVMetaData meta in ivFile.MetaData)
                {
                    if (meta.Name == rightAltText)
                    {
                        if (meta.Value != "")
                            return HttpUtility.HtmlEncode(meta.Value); 
                    }
                }
               
                return String.Empty;
            }
            catch
            {
                return String.Empty;
            }
        }
    }
}
