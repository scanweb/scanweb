// <copyright file="IMap.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Contract all map control classes to implement</summary>

namespace Scandic.Scanweb.CMS.code.Util.Map
{
    /// <summary>
    /// Contract all the map control classes should realize
    /// </summary>
    internal interface IMap
    {
        /// <summary>
        /// Normal center and zoom
        /// </summary>
        /// <param name="curPoint">point</param>
        /// <param name="zoomLevel">zoom level</param>
        void CenterAndZoom(MapPoint curPoint, int zoomLevel);

        /// <summary>
        /// auto center and zoom, where based on the bounds set, tell
        /// the map to center and zoom to a level all the bounds are
        /// within the visibility
        /// </summary>
        void AutoCenterAndZoom();
    }
}