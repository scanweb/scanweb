// <copyright file="MapLine.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Map Line implementation</summary>

using System;
using System.Drawing;
using Scandic.Scanweb.ExceptionManager;

namespace Scandic.Scanweb.CMS.code.Util.Map
{

    /// <summary>
    /// Class representing a Google Line
    /// </summary>
    public class MapLine
    {
        #region Private Fields

        /// <summary>
        /// opacity of the line
        /// </summary>
        private float opacity;

        #endregion 

        #region Public Properties

        /// <summary>
        /// set or get Color of the line
        /// </summary>
        /// <value>The color of the line.</value>
        public Color LineColor { get; set; }

        /// <summary>
        /// set or get Array of points that compose the line
        /// </summary>
        /// <value>The line points.</value>
        public MapPoints LinePoints { get; set; }

        /// <summary>
        /// set or get Opacity of the line
        /// </summary>
        /// <value>Between 0 and 1</value>
        public float Opacity
        {
            get { return opacity; }
            set { SetOpacity(value); }
        }

        /// <summary>
        /// set or get Thickness of the line in pixels
        /// </summary>
        /// <value>The weight.</value>
        public int Weight { get; set; }

        #endregion 

        #region Constructors

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="LinePoints">Array of Points through which the line passes</param>
        public MapLine(MapPoints LinePoints)
        {
            this.LinePoints = LinePoints;
            this.LineColor = Color.Black;
            this.Weight = 1;
            this.opacity = 1;
        }

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="LinePoints">Array of Points through which the line passes</param>
        /// <param name="LineColor">color of the line</param>
        /// <param name="Weight">Thickness of the line</param>
        /// <param name="Opacity">Opacity of the line</param>
        public MapLine(MapPoints LinePoints, Color LineColor, int Weight, float Opacity)
        {
            this.LinePoints = LinePoints;
            this.LineColor = LineColor;
            this.Weight = Weight;
            SetOpacity(Opacity);
        }

        #endregion 

        #region Private Methods

        /// <summary>
        /// Helper method to set the Opacity
        /// </summary>
        /// <param name="Opacity">Opacity value</param>
        private void SetOpacity(float Opacity)
        {
            if (Opacity < 0 || Opacity > 1)
                throw new ScanWebGenericException("Invalid Value for Opacity. Has to be between and and 1");
            else
                this.opacity = Opacity;
        }

        #endregion 
    }
}