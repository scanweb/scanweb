﻿// <copyright file="GMapControl.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Control which generates the javascript to render the google map</summary>

#undef FINDAHOTEL_PERFORMANCE

namespace Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3
{
    #region System NameSpaces
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using System.Xml;
    #endregion // System NameSpaces

    #region Scandic NameSpaces
    using Scandic.Scanweb.BookingEngine.Controller;
    using Scandic.Scanweb.BookingEngine.Entity;
    using Scandic.Scanweb.CMS.code.Util.Map;
    using Scandic.Scanweb.CMS.DataAccessLayer;
    using Scandic.Scanweb.Core;
    #endregion // Scandic NameSpaces

    #region Episerver NameSpaces
    using EPiServer.Core;
    #endregion // Episerver NameSpaces

    /// <summary>
    /// Webcontrol to utilize the Google Maps API.
    /// Lets Add a Tool box image for the control
    /// [ToolboxData("<{0}:GMapControl runat=server></{0}:GMapControl>"),
    /// ToolboxBitmap(typeof(GMapControl), "icon.bmp")]
    /// </summary>
    public class GMapControl : WebControl, IMap
    {
        #region Fields
        /// <summary>
        /// Xml document carrying the marker's data
        /// </summary>
        XmlDocument markersDataDocument;

        /// <summary>
        /// Root node of the markersDataDocument
        /// </summary>
        XmlElement markersDataDocRootNode;

        /// <summary>
        /// Culture information(language)
        /// </summary>
        CultureInfo ci = new System.Globalization.CultureInfo("en-US");

        /// <summary>
        /// Key through which map api key has to be added to script
        /// </summary>
        private string googleKey = string.Empty;

        /// <summary>
        /// String carrying all the startup script code
        /// </summary>
        private StringBuilder initFunction;

        /// <summary>
        /// String carrying the zoomIn() method call in the init script
        /// </summary>
        private string zoomAndCenter = string.Empty;

        /// <summary>
        /// string carrying the marker overlay calls
        /// </summary>
        private StringBuilder pointOverlay;

        /// <summary>
        /// OBSOLETE : string carrying line overlays
        /// </summary>
        private StringBuilder lineOverlay;

        /// <summary>
        /// Name key for the startup script
        /// </summary>
        private string funcNameKey = "Init";

        /// <summary>
        /// OBSOLETE
        /// </summary>
        private string functionName = "InitMap";

        /// <summary>
        /// map variable in the script on which we allocates GMap2 object
        /// </summary>
        private string gMapVariable = "map";

        /// <summary>
        /// Drive drirection enum either 'To' or 'From'
        /// </summary>
        private DriveDirection destinationWay;

        /// <summary>
        /// Locale
        /// </summary>
        private string viewStateLocale = "en_US";

        /// <summary>
        /// direction variable in script
        /// </summary>
        private string directionVariable = "directions";

        /// <summary>
        /// direction panel variable in script
        /// </summary>
        private string directionPanelVariable = "directionsPanel";

        /// <summary>
        /// geocode variable of script
        /// </summary>
        private string geocodeVariable = "geocoder";

        /// <summary>
        /// destination 'to'
        /// </summary>
        private string viewStateDestinationTo = "to";

        /// <summary>
        /// destination 'from'
        /// </summary>
        private string viewStateDestinationFrom = "from";

        /// <summary>
        /// array variable in script to carry default markers
        /// </summary>
        private string arrayVariable = "arrayPoints";

        /// <summary>
        /// array variable in script to carry recently opened hotel markers
        /// </summary>
        private string arrayVarRecentlyOpened = "arrRecentlyOpened";

        /// <summary>
        /// array variable in script to carry coming soon hotel markers
        /// </summary>
        private string arrayVarComingSoon = "arrComingSoon";

        private string arrayVarPerNightRate = "arrPerNightRate";
        private string arrayVarPerStayRate = "arrPerStayRate";

        /// <summary>
        /// 'true' for showing scandic icon, 'false' for default google icon
        /// </summary>
        private bool scandicIcon = true;

        /// <summary>
        /// direction enable status
        /// </summary>
        private string viewStateEnableDirections = "false";

        /// <summary>
        /// driving instructions container div
        /// </summary>
        private string viewStateDirectionsContainer = "div";

        /// <summary>
        /// marker latitude
        /// </summary>
        private string viewStateLatitude = "latitudefield";

        /// <summary>
        /// marker longitude
        /// </summary>
        private string viewStateLongitude = "longitudefield";

        /// <summary>
        /// marker icon url
        /// </summary>
        //private string viewStateIcon = "iconurl";

        /// <summary>
        /// marker shadow url
        /// </summary>
        //private string viewStateIconShadow = "iconshadowurl";

        /// <summary>
        /// data bound text url
        /// </summary>
        private string viewStateDataBoundText = "textdataboundurl";

        /// <summary>
        /// marker html url
        /// </summary>
        private string viewStateHTMLText = "textHTMLurl";

        /// <summary>
        /// Script genrator object
        /// </summary>
        private JScriptGenerator scriptGenerator;

        /// <summary>
        /// enum map type
        /// </summary>
        private GMapType mapType;

        /// <summary>
        /// type of scroll control
        /// </summary>
        private GMapScrollControl mapScrollType;

        /// <summary>
        /// Indicates whether dragging should be enabled in the map
        /// </summary>
        private bool enableDrag;

        /// <summary>
        /// Indicates whether info window should be available or not
        /// </summary>
        private bool enableInfoWindow;

        /// <summary>
        /// Indicates whther map type control is available or not
        /// </summary>
        private bool enableMapTypeControl;

        /// <summary>
        /// Hack to override the enableDrag variable : See comments where used
        /// </summary>
        private bool overrideEnabledrag;

        /// <summary>
        /// Indicates whether the double click zoom is available or not
        /// </summary>
        private bool enableDblClickZoom;

        /// <summary>
        /// Indicates whether mouse scroll zoom to be enabled or not
        /// </summary>
        private bool enableMouseScrollZoom;

        /*private bool enableMarkerNumbering;*/

        /// <summary>
        /// complexity of the GMap, for FAH and Expansion page it is
        /// GMapComplexity.COMPLEX and for all other pages it is 
        /// GMapComplexity.SIMPLE. This parameter governs whether AJAX
        /// is required or not to get the map data.
        /// </summary>
        private GMapComplexity mapComplexity = GMapComplexity.SIMPLE;

        /// <summary>
        /// Indicates the type of page carrying the map, see MapPageType enum
        /// </summary>
        private MapPageType mapPageType = MapPageType.OLD;

        /// <summary>
        /// data source
        /// </summary>
        private object dataSource = null;

        // Release - Digital Platform - START
        /// <summary>
        /// boolean value indicates whether recently opened hotels 
        /// check box in filter should be enabled by default or not
        /// </summary>
        private bool recentlyOpenedHotels = false;

        /// <summary>
        /// boolean value indicates whether coming soon hotels 
        /// check box in filter should be enabled by default or not
        /// </summary>
        private bool comingSoonHotels = false;

        /// <summary>
        /// boolean value indicates whether regular hotels
        /// check box in filter should be enabled by default or not
        /// </summary>
        private bool otherHotels = false;
        // Release - Digital Platform - END
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Sets or get GMapVariable
        /// </summary>
        /// <value>The Google map variable.</value>
        public string GMapVariable
        {
            get
            {
                if (this.ViewState[gMapVariable] != null)
                {
                    return (string)this.ViewState[gMapVariable];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[gMapVariable] = value; }
        }

        /// <summary>
        /// Set or get the locale of the page
        /// </summary>
        /// <value>The locale.</value>
        public string Locale
        {
            get
            {
                if (this.ViewState[viewStateLocale] != null)
                {
                    return (string)this.ViewState[viewStateLocale];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateLocale] = value; }
        }

        /// <summary>
        /// Set or get the <see cref="DriveDirection"/> way
        /// </summary>
        /// <value>The destination way.</value>
        public DriveDirection DestinationWay
        {
            get { return destinationWay; }
            set { destinationWay = value; }
        }

        /// <summary>
        /// Set or get the destination-from string
        /// </summary>
        /// <value>The destination from.</value>
        public string DestinationFrom
        {
            get
            {
                if (this.ViewState[viewStateDestinationFrom] != null)
                {
                    return (string)this.ViewState[viewStateDestinationFrom];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateDestinationFrom] = value; }
        }

        /// <summary>
        /// Set or get the destination-to string
        /// </summary>
        /// <value>The destination to.</value>
        public string DestinationTo
        {
            get
            {
                if (this.ViewState[viewStateDestinationTo] != null)
                {
                    return (string)this.ViewState[viewStateDestinationTo];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateDestinationTo] = value; }
        }

        /// <summary>
        /// set or get the key for using Google maps
        /// </summary>
        /// <value>The google map key.</value>
        public string GoogleMapKey { set { googleKey = value; } get { return googleKey; } }

        /// <summary>
        /// set or get the Map Type whether Satellite, map or hybrid
        /// </summary>
        /// <value>The type of the map.</value>
        public GMapType MapType { set { mapType = value; } get { return mapType; } }

        /// <summary>
        /// Set or get the Type of Scroll control on the map
        /// </summary>
        /// <value>The type of the scroll control.</value>
        public GMapScrollControl ScrollControlType { set { mapScrollType = value; } get { return mapScrollType; } }

        /// <summary>
        /// Enable dragging of the Map
        /// </summary>
        /// <value><c>true</c> if [enable dragging]; otherwise, <c>false</c>.</value>
        public bool EnableDragging { set { enableDrag = value; } get { return enableDrag; } }

        /// <summary>
        /// Enable driving directions of the Map
        /// </summary>
        /// <value><c>true</c> if [enable directions]; otherwise, <c>false</c>.</value>
        public bool EnableDirections
        {
            get
            {
                if (this.ViewState[viewStateEnableDirections] != null)
                {
                    return bool.Parse(this.ViewState[viewStateEnableDirections].ToString());
                }
                return bool.Parse("false");
            }
            set { this.ViewState[viewStateEnableDirections] = value.ToString(); }
        }

        /// <summary>
        /// Set or get the container that should be viewing the directions
        /// </summary>
        /// <value>The directions container.</value>
        public string DirectionsContainer
        {
            get
            {
                if (this.ViewState[viewStateDirectionsContainer] != null)
                {
                    return (string)this.ViewState[viewStateDirectionsContainer];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateDirectionsContainer] = value; }
        }

        /// <summary>
        /// Enable zoom by doubleclick of the Map
        /// </summary>
        /// <value><c>true</c> if [enable DBL click zoom]; otherwise, <c>false</c>.</value>
        public bool EnableDblClickZoom { set { enableDblClickZoom = value; } get { return enableDblClickZoom; } }

        /// <summary>
        /// Enable zoom by mouse scroll of the Map
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [enable mouse scroll zoom]; otherwise, <c>false</c>.
        /// </value>
        public bool EnableMouseScrollZoom { set { enableMouseScrollZoom = value; } get { return enableMouseScrollZoom; } }

        //public bool EnableMarkerNumbering { set { enableMarkerNumbering = value; } get { return enableMarkerNumbering; } }

        /// <summary>
        /// Set or get whether Pop up windows can be opened on the Map
        /// </summary>
        /// <value><c>true</c> if [enable info window]; otherwise, <c>false</c>.</value>
        public bool EnableInfoWindow { set { enableInfoWindow = value; } get { return enableInfoWindow; } }

        /// <summary>
        /// Set or get whether the Scandic icon should be shown on the map (default is true)
        /// </summary>
        /// <value><c>true</c> if [scandic icon]; otherwise, <c>false</c>.</value>
        public bool ScandicIcon { set { scandicIcon = value; } get { return scandicIcon; } }

        /// <summary>
        /// Set or get whether to show the map type control on the map
        /// </summary>
        /// <value><c>true</c> if [show map type control]; otherwise, <c>false</c>.</value>
        public bool ShowMapTypeControl
        {
            set { enableMapTypeControl = value; }
            get { return enableMapTypeControl; }
        }

        /// <summary>
        /// Gets or sets the mapComplexity.
        /// </summary>
        /// <value>The map complexity.</value>
        public GMapComplexity MapComplexity
        {
            get { return mapComplexity; }
            set { mapComplexity = value; }
        }

        /// <summary>
        /// Gets or sets the type of the map page.
        /// </summary>
        /// <value>The type of the map page.</value>
        public MapPageType MapPageType
        {
            get { return mapPageType; }
            set { mapPageType = value; }
        }

        /// <summary>
        /// Set or get the data source
        /// </summary>
        /// <value>The data source.</value>
        public object DataSource
        {
            set
            {
                if (value == null ||
                    value is IListSource ||
                    value is IEnumerable)
                    dataSource = value;
                else
                    throw new ArgumentException();
            }
            get { return dataSource; }
        }

        /// <summary>
        /// set or get the Marker Text. Can be formatted HTML
        /// Used for databinding
        /// </summary>
        /// <value>The marker HTML text.</value>
        public string MarkerHTMLText
        {
            get
            {
                if (this.ViewState[viewStateHTMLText] != null)
                {
                    return (string)this.ViewState[viewStateHTMLText];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateHTMLText] = value; }
        }

        /// <summary>
        /// set or get the Marker Text. Can be formatted HTML
        /// Used for databinding
        /// </summary>
        /// <value>The marker data bound text.</value>
        public string MarkerDataBoundText
        {
            get
            {
                if (this.ViewState[viewStateDataBoundText] != null)
                {
                    return (string)this.ViewState[viewStateDataBoundText];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateDataBoundText] = value; }
        }

        /// <summary>
        /// set or get the Marker Longitude
        /// Used for databinding
        /// </summary>
        /// <value>The marker longitude field.</value>
        public string MarkerLongitudeField
        {
            get
            {
                if (this.ViewState[viewStateLongitude] != null)
                {
                    return (string)this.ViewState[viewStateLongitude];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateLongitude] = value; }
        }

        /// <summary>
        /// Set or get the Marker Latitude
        /// Used for databinding
        /// </summary>
        /// <value>The marker latitude field.</value>
        public string MarkerLatitudeField
        {
            get
            {
                if (this.ViewState[viewStateLatitude] != null)
                {
                    return (string)this.ViewState[viewStateLatitude];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateLatitude] = value; }
        }

        ///// <summary>
        ///// Set or get the Marker icon
        ///// </summary>
        //public string MarkerIconURL
        //{
        //    get
        //    {
        //        if (this.ViewState[viewStateIcon] != null)
        //        {
        //            return (string)this.ViewState[viewStateIcon];
        //        }
        //        else
        //            return string.Empty;
        //    }
        //    set { this.ViewState[viewStateIcon] = value; }
        //}

        ///// <summary>
        ///// Set or get the Marker icon shadow
        ///// </summary>
        //public string MarkerIconShadowURL
        //{
        //    get
        //    {
        //        if (this.ViewState[viewStateIconShadow] != null)
        //        {
        //            return (string)this.ViewState[viewStateIconShadow];
        //        }
        //        else
        //            return string.Empty;
        //    }
        //    set { this.ViewState[viewStateIconShadow] = value; }
        //}

        // Release - Digital Platform - START
        /// <summary>
        /// Gets or sets a value indicating whether [recently opened hotels].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [recently opened hotels]; otherwise, <c>false</c>.
        /// </value>
        public bool RecentlyOpenedHotels
        {
            get { return recentlyOpenedHotels; }
            set { recentlyOpenedHotels = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [coming soon hotels].
        /// </summary>
        /// <value><c>true</c> if [coming soon hotels]; otherwise, <c>false</c>.</value>
        public bool ComingSoonHotels
        {
            get { return comingSoonHotels; }
            set { comingSoonHotels = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [other hotels].
        /// </summary>
        /// <value><c>true</c> if [other hotels]; otherwise, <c>false</c>.</value>
        public bool OtherHotels
        {
            get { return otherHotels; }
            set { otherHotels = value; }
        }
        // Release - Digital Platform - END
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <remarks>
        /// Since Google Maps needs to rendered as a DIV
        /// We set the Textwriter to render as a DIV
        /// </remarks>
        public GMapControl()
            : base(HtmlTextWriterTag.Div)
        {
            initFunction = new StringBuilder();
            pointOverlay = new StringBuilder();
            lineOverlay = new StringBuilder();
            scriptGenerator = new JScriptGenerator();
            this.mapType = GMapType.MAP;
            this.mapScrollType = GMapScrollControl.NONE;
            this.enableDrag = true;
            this.enableInfoWindow = true;
        }
        #endregion // Constructor

        #region Protected Events
        /// <summary>
        /// Init method
        /// </summary>
        /// <param name="e">Event Args</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.viewStateLatitude = viewStateLatitude + this.ClientID;
            this.viewStateLongitude = viewStateLongitude + this.ClientID;
            this.viewStateHTMLText = viewStateHTMLText + this.ClientID;
            this.viewStateDataBoundText = viewStateDataBoundText + this.ClientID;
            //this.viewStateIcon = viewStateIcon + this.ClientID;
            //this.viewStateIconShadow = viewStateIconShadow + this.ClientID;
            //this.viewStateIconShadow = viewStateIconShadow + this.ClientID;
        }

        /// <summary>
        /// Pre Render method
        /// </summary>
        /// <param name="e">Event Args</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            // Generate Unique names for the functions
            // and variables based on the Client ID.
            // Done so as to avoid issues if there are multiple
            // controls in the page
            funcNameKey = funcNameKey + this.ClientID;
            functionName = functionName + this.ClientID;
            gMapVariable = gMapVariable + this.ClientID;
            arrayVariable = arrayVariable + this.ClientID;
            arrayVarRecentlyOpened += this.ClientID;
            arrayVarComingSoon += this.ClientID;
            arrayVarPerNightRate += this.ClientID;
            arrayVarPerStayRate += this.ClientID;
            // Go ahead and register all javascripts
            RegisterAllScripts();
        }

        /// <summary>
        /// Render method
        /// </summary>
        /// <param name="writer">Instance of HtmlTextWriter</param>
        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
            //If design mode.. let's generate a simple Placeholder
            // for info 
            if (this.Site != null && this.Site.DesignMode)
            {
                writer.RenderBeginTag("h2");
                writer.Write(this.ID);
                writer.RenderEndTag();
            }
        }

        /// <summary>
        /// DataBinding Method
        /// </summary>
        /// <param name="e">Event Args</param>
        protected override void OnDataBinding(EventArgs e)
        {
#if FINDAHOTEL_PERFORMANCE
            long startTime = DateTime.Now.Ticks;
#endif // #if FINDAHOTEL_PERFORMANCE
            try
            {
                // Call the base method
                base.OnDataBinding(e);

                // Create the marker's data document.
                if (mapComplexity == GMapComplexity.COMPLEX)
                {
                    markersDataDocument = new XmlDocument();
                    markersDataDocRootNode = markersDataDocument.CreateElement("markers");
                    markersDataDocument.AppendChild(markersDataDocRootNode);
                }

                // Resolve the data supplied with the helper class
                IEnumerable mapUnitList = HelperDataResolver.GetResolvedDataSource(dataSource);

                if (mapUnitList != null)
                {
                    // Iterate through the MapUnits data supplied and process it.
                    foreach (object ditem in mapUnitList)
                    {
                        string sText = string.Empty;
                        double lat = 0;
                        double lng = 0;

                        // Get the latitude and longitude of the MapUnit.
                        GetLatitudeLongitude(ditem, out lat, out lng);

                        // Create the point where the MapUnit has to be placed.
                        MapPoint gp = new MapPoint(lat, lng);
                        MapMarker gm = null;

                        // TODO: fix icon proporties, check if string.empty genererates default GMaps icon
                        //if((string)DataBinder.GetPropertyValue(ditem, this.MarkerIconURL)

                        // Only unit, else generate hotelunit specific properties
                        MapUnit mapUnit = (MapUnit)ditem;
                        sText = mapUnit.GenerateInfoWindowMarkUp();
                        gm = new MapMarker(gp);

                        // Set marker icons
                        if (mapUnit.IconUrl != null && mapUnit.IconUrl.Length > 0)
                        {
                            MapIcon gi = null;
                            if (mapUnit.InfoBoxType == InfoBoxType.BASIC)
                            {
                                gi = new MapIcon(
                                    mapUnit.IconUrl,
                                    mapUnit.ShadowUrl,
                                    new MapSize(GoogleMapConstants.BASIC_MARKER_WIDTH, GoogleMapConstants.BASIC_MARKER_HEIGHT),
                                    new MapSize(GoogleMapConstants.BASIC_MARKER_SHADOW_WIDTH, GoogleMapConstants.BASIC_MARKER_SHADOW_HEIGHT),
                                    new MapPoint(GoogleMapConstants.BASIC_MARKER_ICON_ANCHOR_X, GoogleMapConstants.BASIC_MARKER_ICON_ANCHOR_Y),
                                    new MapPoint(GoogleMapConstants.BASIC_MARKER_INFOBOX_ANCHOR_X, GoogleMapConstants.BASIC_MARKER_INFOBOX_ANCHOR_Y)
                                    );
                            }
                            else if (mapUnit.InfoBoxType == InfoBoxType.ADVANCED)
                            {
                                gi = new MapIcon(
                                    mapUnit.IconUrl,
                                    mapUnit.ShadowUrl,
                                    new MapSize(GoogleMapConstants.ADVANCED_MARKER_WIDTH, GoogleMapConstants.ADVANCED_MARKER_HEIGHT),
                                    new MapSize(GoogleMapConstants.ADVANCED_MARKER_SHADOW_WIDTH, GoogleMapConstants.ADVANCED_MARKER_SHADOW_HEIGHT),
                                    new MapPoint(GoogleMapConstants.ADVANCED_MARKER_ICON_ANCHOR_X, GoogleMapConstants.ADVANCED_MARKER_ICON_ANCHOR_Y),
                                    new MapPoint(GoogleMapConstants.ADVANCED_MARKER_INFOBOX_ANCHOR_X, GoogleMapConstants.ADVANCED_MARKER_INFOBOX_ANCHOR_Y)
                                    );
                            }
                            else if (mapUnit.InfoBoxType == InfoBoxType.ADVANCED_SELECTHOTEL || mapUnit.InfoBoxType == InfoBoxType.ADVANCED_CONFIRMATION)
                            {
                                gi = new MapIcon(
                                    mapUnit.IconUrl,
                                    mapUnit.ShadowUrl,
                                    new MapSize(GoogleMapConstants.SH_ADVANCED_MARKER_WIDTH, GoogleMapConstants.SH_ADVANCED_MARKER_HEIGHT),
                                    new MapSize(GoogleMapConstants.SH_ADVANCED_MARKER_SHADOW_WIDTH, GoogleMapConstants.SH_ADVANCED_MARKER_SHADOW_HEIGHT),
                                    new MapPoint(GoogleMapConstants.SH_ADVANCED_MARKER_ICON_ANCHOR_X, GoogleMapConstants.SH_ADVANCED_MARKER_ICON_ANCHOR_Y),
                                    new MapPoint(GoogleMapConstants.SH_ADVANCED_MARKER_INFOBOX_ANCHOR_X, GoogleMapConstants.SH_ADVANCED_MARKER_INFOBOX_ANCHOR_Y)
                                    );
                            }
                            //Vrushali | artf1167579 : Corporate Identifier not present in Select Hotel listing | Styling changed.
                            else if (mapUnit.InfoBoxType == InfoBoxType.ADVANCED_SELECTHOTEL_DNUMBER)
                            {
                                gi = new MapIcon(
                                    mapUnit.IconUrl,
                                    mapUnit.ShadowUrl,
                                    new MapSize(GoogleMapConstants.SH_ADVANCED_MARKER_WIDTH_DNUMBER, GoogleMapConstants.SH_ADVANCED_MARKER_HEIGHT_DNUMBER),
                                    new MapSize(GoogleMapConstants.SH_ADVANCED_MARKER_SHADOW_WIDTH, GoogleMapConstants.SH_ADVANCED_MARKER_SHADOW_HEIGHT),
                                    new MapPoint(GoogleMapConstants.SH_ADVANCED_MARKER_ICON_ANCHOR_X, GoogleMapConstants.SH_ADVANCED_MARKER_ICON_ANCHOR_Y),
                                    new MapPoint(GoogleMapConstants.SH_ADVANCED_MARKER_INFOBOX_ANCHOR_X, GoogleMapConstants.SH_ADVANCED_MARKER_INFOBOX_ANCHOR_Y)
                                    );
                            }
                            gm.Icon = gi;
                        }

                        if (ditem.GetType().Name.Equals("MapHotelUnit"))
                        {
                            // If scandic icon is not present, reset to default Google Map icon
                            if (!ScandicIcon)
                            {
                                gm.Icon = null;
                            }
                        }

                        // Create the overlay(popup) data.
                        OverlayMarker(gm, mapUnit, sText);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Could not Resolve Data source Fields: " + ex.ToString());
            }
            finally
            {
                // For FAH set the marker's data to the Session.
                if (mapComplexity == GMapComplexity.COMPLEX)
                {
                    SessionWrapper.MarkerData = markersDataDocument;
                }
            }
#if FINDAHOTEL_PERFORMANCE
            long endTime = DateTime.Now.Ticks;
            TimeSpan tp = new TimeSpan(endTime - startTime);
            AppLogger.LogInfoMessage("GMapControl:OnDataBinding creating marker texts running time  ->  Minutes:" + tp.Minutes + " Seconds:" + tp.Seconds + " Milliseconds:" + tp.Milliseconds);
#endif // #if FINDAHOTEL_PERFORMANCE
        }
        #endregion // Protected Events

        #region Private Methods
        /// <summary>
        /// Method which registers all JavaScripts and the map key
        /// </summary>
        private void RegisterAllScripts()
        {
#if FINDAHOTEL_PERFORMANCE
            long startTime = DateTime.Now.Ticks;
#endif // #if FINDAHOTEL_PERFORMANCE

            //Register the Key 
            RegisterMapKey();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), funcNameKey, InitializeMap(), false);

#if FINDAHOTEL_PERFORMANCE
            long endTime = DateTime.Now.Ticks;
            TimeSpan tp = new TimeSpan(endTime - startTime);
            AppLogger.LogInfoMessage("GMapControl:RegisterScripts()+InitializeMap() running time  ->  Minutes:" + tp.Minutes + " Seconds:" + tp.Seconds + " Milliseconds:" + tp.Milliseconds);
#endif // #if FINDAHOTEL_PERFORMANCE
        }

        /// <summary>
        /// Method to register the Google map key
        /// </summary>
        private void RegisterMapKey()
        {
            string sKeyScript = string.Empty;
            // Merchandising - Google maps V2 - V3 migration - Bhavya
            //sKeyScript = string.Format("<script src='http://maps.google.com/maps?file=api&v=2.173&key={0}' type='text/javascript'></script>", googleKey);
            sKeyScript = string.Format("<script src='http://maps.google.com/maps/api/js?v=3&sensor=false&key={0}' type='text/javascript'></script>", googleKey);
            // Only one needed per page so check if registered
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "GMapKey", sKeyScript, false);
        }

        /// <summary>
        /// Method to return the Map Key
        /// </summary>
        /// <returns>The map key</returns>
        public string PrintMapKey()
        {
            string sKeyScript = string.Empty;
            // Merchandising - Google maps V2 - V3 migration - Bhavya
            //sKeyScript = string.Format("<script src='http://maps.google.com/maps?file=api&v=2.173&key={0}' type='text/javascript'></script>", googleKey);
            sKeyScript = string.Format("<script src='http://maps.google.com/maps/api/js?v=3&sensor=false&key={0}' type='text/javascript'></script>", googleKey);
            return sKeyScript;
        }

        /// <summary>
        /// Method to Overlay a marker on the Map.
        /// </summary>
        /// <param name="curMarker">Marker to be overlayed</param>
        /// <param name="mapUnit">The map unit.</param>
        /// <param name="sFormattedHtml">HTML to be displayed in the pop up</param>
        private void OverlayMarker(MapMarker curMarker, MapUnit mapUnit, string sFormattedHtml)
        {
            int enablePopUp = 0;
            if (!string.IsNullOrEmpty(sFormattedHtml))
            {
                //Doing below since it throws an error if enable drag is true
                //and the marker has a info windo attached to it
                overrideEnabledrag = true;

                enablePopUp = 1;

                // artf1022999: Java script error in hotel landing page in IE6 | Find a hotel.
                sFormattedHtml = sFormattedHtml.Replace("'", "&#39;");
            }

            if (mapComplexity == GMapComplexity.SIMPLE)
            {
                if (curMarker.Point != null)
                {
                    //Check if the marker has an Icon
                    if (curMarker.Icon != null)
                    {
                        pointOverlay.AppendFormat(
                            ci,
                            "OverlayCustomMarkerLocation{0}({1},{2},'{3}','{4}',{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},'{16}','{17}','{18}','{19}',{20},'{21}','{22}','{23}');\n",
                            this.ClientID,
                            curMarker.Point.Longitude,
                            curMarker.Point.Latitude,
                            curMarker.Icon.ImageURL,
                            curMarker.Icon.ShadowImageURL,
                            curMarker.Icon.IconSize.Width,
                            curMarker.Icon.IconSize.Height,
                            curMarker.Icon.ShadowSize.Width,
                            curMarker.Icon.ShadowSize.Height,
                            curMarker.Icon.IconAnchor.Longitude,
                            curMarker.Icon.IconAnchor.Latitude,
                            curMarker.Icon.InfoWindowAnchor.Longitude,
                            curMarker.Icon.InfoWindowAnchor.Latitude,
                            mapUnit.EnableUnitNumbering.ToString().ToLower(),
                            enablePopUp.ToString(),
                            mapUnit.PopUpMaxWidth.ToString(),
                            mapUnit.InfoBoxType.ToString(),
                            sFormattedHtml,
                            mapUnit is MapHotelUnit ? (mapUnit as MapHotelUnit).HotelName : string.Empty, // This value is used by the sitecatalyst to determine which hotel has been clicked by the user on the map
                            mapUnit is MapHotelUnit ? (mapUnit as MapHotelUnit).HotelCategory : string.Empty,
                            mapUnit is MapHotelUnit ? (mapUnit as MapHotelUnit).EnableUnitText.ToString().ToLower() : "false",
                            mapUnit is MapHotelUnit ? (mapUnit as MapHotelUnit).MarkerRateTextType.ToString() : MarkerRateTextType.RATEPERNIGHT.ToString(),
                            mapUnit is MapHotelUnit ? (mapUnit as MapHotelUnit).PerNightRate : string.Empty,
                            mapUnit is MapHotelUnit ? (mapUnit as MapHotelUnit).PerStayRate : string.Empty
                            );
                    }
                    else
                    {
                        pointOverlay.AppendFormat(
                            ci,
                            "OverlayLocation{0}({1},{2},{3},{4},{5},'{6}','{7}','{8}','{9}');\n",
                            this.ClientID,
                            curMarker.Point.Longitude,
                            curMarker.Point.Latitude,
                            mapUnit.EnableUnitNumbering.ToString().ToLower(),
                            enablePopUp.ToString(),
                            mapUnit.PopUpMaxWidth.ToString(),
                            mapUnit.InfoBoxType.ToString(),
                            sFormattedHtml,
                            mapUnit is MapHotelUnit ? (mapUnit as MapHotelUnit).HotelName : string.Empty, // This value is used by the sitecatalyst to determine which hotel has been clicked by the user on the map
                            mapUnit is MapHotelUnit ? (mapUnit as MapHotelUnit).HotelCategory : string.Empty
                            );
                    }
                }
            }
            else if (mapComplexity == GMapComplexity.COMPLEX)
            {
                if (markersDataDocument != null && markersDataDocRootNode != null)
                {
                    XmlElement marker = markersDataDocument.CreateElement("marker");

                    marker.SetAttribute("longitude", curMarker.Point.Longitude.ToString().Replace(',', '.')); // This make sure '.' will not be replaced by ',' in diff locales
                    marker.SetAttribute("latitude", curMarker.Point.Latitude.ToString().Replace(',', '.')); // This make sure '.' will not be replaced by ',' in diff locales

                    if (curMarker.Icon != null)
                    {
                        marker.SetAttribute("imageUrl", curMarker.Icon.ImageURL);
                        marker.SetAttribute("shadowUrl", curMarker.Icon.ShadowImageURL);
                        marker.SetAttribute("iconWidth", curMarker.Icon.IconSize.Width.ToString());
                        marker.SetAttribute("iconHeight", curMarker.Icon.IconSize.Height.ToString());
                        marker.SetAttribute("shadowWidth", curMarker.Icon.ShadowSize.Width.ToString());
                        marker.SetAttribute("shadowHeight", curMarker.Icon.ShadowSize.Height.ToString());
                        marker.SetAttribute("iconAnchorX", curMarker.Icon.IconAnchor.Longitude.ToString().Replace(',', '.')); // This make sure '.' will not be replaced by ',' in diff locales
                        marker.SetAttribute("iconAnchorY", curMarker.Icon.IconAnchor.Latitude.ToString().Replace(',', '.')); // This make sure '.' will not be replaced by ',' in diff locales
                        marker.SetAttribute("infoAnchorX", curMarker.Icon.InfoWindowAnchor.Longitude.ToString().Replace(',', '.')); // This make sure '.' will not be replaced by ',' in diff locales
                        marker.SetAttribute("infoAnchorY", curMarker.Icon.InfoWindowAnchor.Latitude.ToString().Replace(',', '.')); // This make sure '.' will not be replaced by ',' in diff locales
                    }
                    marker.SetAttribute("enableNumbering", mapUnit.EnableUnitNumbering.ToString().ToLower());
                    marker.SetAttribute("enablePopup", enablePopUp.ToString());
                    marker.SetAttribute("popupMaxWidth", mapUnit.PopUpMaxWidth.ToString());
                    marker.SetAttribute("infoBoxType", mapUnit.InfoBoxType.ToString());

                    sFormattedHtml = sFormattedHtml.Replace("<", "&lt;");
                    sFormattedHtml = sFormattedHtml.Replace(">", "&gt;");
                    sFormattedHtml = sFormattedHtml.Replace("\"", "&quot;");
                    marker.SetAttribute("infoHtml", sFormattedHtml);

                    // This value is used by the sitecatalyst to determine which hotel
                    // has been clicked by the user on the map
                    if (mapUnit is MapHotelUnit)
                    {
                        marker.SetAttribute("hotelName", (mapUnit as MapHotelUnit).HotelName);
                        marker.SetAttribute("hotelCategory", (mapUnit as MapHotelUnit).HotelCategory);

                        marker.SetAttribute("enableUnitText", (mapUnit as MapHotelUnit).EnableUnitText.ToString().ToLower());
                        marker.SetAttribute("markerRateTextType", (mapUnit as MapHotelUnit).MarkerRateTextType.ToString());
                        marker.SetAttribute("perNightRate", (mapUnit as MapHotelUnit).PerNightRate);
                        marker.SetAttribute("perStayRate", (mapUnit as MapHotelUnit).PerStayRate);
                    }
                    markersDataDocRootNode.AppendChild(marker);
                }
            }
        }

        /// <summary>
        /// OBSOLETE : Method to overlay a line on the Map
        /// </summary>
        /// <param name="curLine">Line to be overlayed</param>
        private void OverlayLine(MapLine curLine)
        {
            StringBuilder sPoints = new StringBuilder();
            if (curLine.LinePoints.Count > 0)
            {
                sPoints.Append("[");
                foreach (MapPoint pt in curLine.LinePoints)
                {
                    if (pt != null)
                        sPoints.AppendFormat(ci, "new GPoint({0},{1}),", pt.Longitude, pt.Latitude);

                }

                //Remove the extra comma at the end
                if (sPoints.Length > 1)
                {
                    sPoints.Replace(",", string.Empty, sPoints.ToString().LastIndexOf(","), 1);
                    sPoints.Append("]");
                }
                lineOverlay.AppendFormat(ci, "OverlayLine{0}({1},{2},'{3}',{4});\n", this.ClientID, sPoints.ToString(), curLine.Weight, HelperColorConvertor.ConvertColor(curLine.LineColor), curLine.Opacity);
            }
        }

        /// <summary>
        /// This method can resolve and get the latitude and longitude
        /// from the parameter passed.
        /// </summary>
        /// <param name="ditem">MapUnit object</param>
        /// <param name="latitude">out latitude</param>
        /// <param name="longitude">out longitude</param>
        private void GetLatitudeLongitude(object ditem, out double latitude, out double longitude)
        {

            if (!string.IsNullOrEmpty(this.MarkerLongitudeField) && !string.IsNullOrEmpty(this.MarkerLatitudeField))
            {
                if (typeof(string) == DataBinder.GetPropertyValue(ditem, this.MarkerLatitudeField).GetType()
                                && typeof(string) == DataBinder.GetPropertyValue(ditem, this.MarkerLongitudeField).GetType())
                {
                    latitude = double.Parse(DataBinder.GetPropertyValue(ditem, this.MarkerLatitudeField).ToString(), ci);
                    longitude = double.Parse(DataBinder.GetPropertyValue(ditem, this.MarkerLongitudeField).ToString(), ci);
                }
                else
                {
                    latitude = (double)DataBinder.GetPropertyValue(ditem, this.MarkerLatitudeField);
                    longitude = (double)DataBinder.GetPropertyValue(ditem, this.MarkerLongitudeField);
                }
            }
            else
            {
                latitude = 0;
                longitude = 0;
            }
        }

        /// <summary>
        /// Initializes the map.
        /// </summary>
        /// <returns></returns>
        private string InitializeMap()
        {
#if FINDAHOTEL_PERFORMANCE
            long startTime = DateTime.Now.Ticks;
#endif // #if FINDAHOTEL_PERFORMANCE

            //This is a work around for the thread abort error in IE
            //IE doesn't seem to like script tags in the body so close the form and 
            //open another form element
            //initFunction.Append("</form>"); // This line has been commented on FAH development.
            initFunction.Append("<script type='text/javascript' language='javascript'>\n");

            //artf1033826: R1.7.1 | Add load indicator
            // Load indicator is required only in FAH page.
            if (mapPageType == MapPageType.FINDAHOTEL || mapPageType == MapPageType.CONFIRMATION)
            {
                // Add the load indicator
                initFunction.Append("AddMyOverlay();");
            }


#if FINDAHOTEL_PERFORMANCE
            initFunction.Append("var start = new Date();");
#endif // #if FINDAHOTEL_PERFORMANCE

            initFunction.Append("var markerManager;\n");
            initFunction.Append("var recentlyOpenedHotels;\n");
            initFunction.Append("var comingSoonHotels;\n");
            initFunction.AppendLine("var labeledMarkerCount = 0;");
            initFunction.AppendFormat("var {0};", gMapVariable);
            initFunction.AppendFormat("var {0}= new Array();", arrayVariable);
            initFunction.AppendFormat("var {0}= new Array();", arrayVarRecentlyOpened);
            initFunction.AppendFormat("var {0}= new Array();", arrayVarComingSoon);

            initFunction.AppendFormat("var {0}= new Array();", arrayVarPerNightRate);
            initFunction.AppendFormat("var {0}= new Array();", arrayVarPerStayRate);

            initFunction.Append("\n$(document).ready(function(){\n");
            //Check if the Browser is compatible
            //initFunction.Append("if(GBrowserIsCompatible()){\n");
            initFunction.Append("if(true){\n");
            initFunction.Append("var locBounds = new google.maps.LatLngBounds();\n");

            // If pop windows are to be shown then the enable drag has to be set to true
            if (overrideEnabledrag)
                enableDrag = true;

            //            if (mapPageType == MapPageType.SELECTHOTEL)
            //            {
            //                initFunction.AppendFormat(@"
            //                    $('#{0}').css('width','399px');
            //                    $('#{0}').css('height','718px');
            //                ", this.ClientID);
            //            }

            initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateGMapVariableInitialization(gMapVariable, this.ClientID, enableDrag, enableInfoWindow, enableMapTypeControl, enableDblClickZoom, enableMouseScrollZoom, mapPageType));

            // No init of map-types as Google Maps v2 API sets G_NORMAL_MAP as default
            //initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateMapType(mapType, gMapVariable)); 

            if (EnableDirections)
            {
                initFunction.AppendFormat("var {0};\n", directionVariable);
                initFunction.AppendFormat("var {0};\n", directionPanelVariable);

                initFunction.AppendFormat("var {0} = new GClientGeocoder();\n", geocodeVariable);
                initFunction.Append("var locPoint = new google.maps.LatLng();\n");

                initFunction.AppendFormat("{0} = document.getElementById(\"" + DirectionsContainer + "\");\n", directionPanelVariable);
                initFunction.AppendFormat("{0} = new GDirections({1}, {2});\n", directionVariable, gMapVariable, directionPanelVariable);

                if (DestinationWay == DriveDirection.TO)
                {
                    initFunction.AppendFormat("{0}.getLatLng(\"" + DestinationTo + "\", function(point)\n", geocodeVariable);
                    initFunction.Append("{ if (!point){\n");
                    initFunction.Append("\n");
                    initFunction.Append("} else {\n");
                    initFunction.AppendFormat("{0}.loadFromWaypoints([\"" + DestinationFrom + "\", point], ", directionVariable);
                    initFunction.Append("{locale:\"" + Locale + "\"});\n");
                }
                else
                {
                    initFunction.AppendFormat("{0}.getLatLng(\"" + DestinationFrom + "\", function(point)\n", geocodeVariable);
                    initFunction.Append("{ if (!point){\n");
                    initFunction.Append("\n");
                    initFunction.Append("} else {\n");
                    initFunction.AppendFormat("{0}.loadFromWaypoints([point, \"" + DestinationTo + "\"], ", directionVariable);
                    initFunction.Append("{locale:\"" + Locale + "\"});\n");
                }

                initFunction.Append("}\n");
                initFunction.Append("}\n");
                initFunction.Append(");\n");
            }

            initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateScrollControl(mapScrollType, gMapVariable));
            initFunction.AppendFormat("{0}\n", zoomAndCenter);

            if (mapComplexity == GMapComplexity.SIMPLE)
            {
                initFunction.AppendFormat("setTimeout('CreateOverlays{0}()', 0);", this.ClientID);
            }
            else if (mapComplexity == GMapComplexity.COMPLEX)
            {
                string ajaxRequestUrl = GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage");
                initFunction.AppendFormat(@"
                        var requestUrl = ""{0}"";
                        fetchOverlayDataXML(requestUrl, CreateOverlays{1});
                    ", ajaxRequestUrl, this.ClientID);
            }

            // Filter control is only required for FAH and Expansion pages
            if (mapPageType == MapPageType.FINDAHOTEL || mapPageType == MapPageType.EXPANSION)
            {
                initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateFilterControlPrototypeMethodBody(gMapVariable, recentlyOpenedHotels, comingSoonHotels, otherHotels));

                initFunction.AppendFormat(@"
                    {0}.addControl(new FilterControl());
                ", gMapVariable);
            }

            if (mapPageType == MapPageType.FINDAHOTEL || mapPageType == MapPageType.EXPANSION || mapPageType == MapPageType.SELECTHOTEL || mapPageType == MapPageType.CONFIRMATION)
            {
                initFunction.AppendFormat(@"
                    google.maps.Event.addListener({0}, 'zoomend', function(oldZoomLeval, newZoomLeval) {{
                        {0}.closeExtInfoWindow();
                    }});"
                , gMapVariable);

                // As there is only one maptype available now, this is not a valid event to capture.
                //                initFunction.AppendFormat(@"
                //                    google.maps.Event.addListener({0}, 'maptypechanged', function() {{
                //                        {0}.closeExtInfoWindow();
                //                    }});"
                //                , gMapVariable);
            }

            //This is a workaround for hiding the copyright notice 
            //if it's offset is floating out of context.
            initFunction.Append("setTimeout('MakeCopyrightSmaller();', 500);\n");


            initFunction.Append("\n}\n");

            initFunction.Append("\n});\n");// document.ready - end


            /*****************HERE ENDS THE START UP SCRIPT**********************/

            //Render the function only if needed.This reduces the  client side  Javascript rendered
            if (zoomAndCenter.Trim().Length > 0)
            {
                initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateZoomInMethodBody(gMapVariable, this.ClientID, mapPageType));
            }

            initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateOverlayLocationMethodBody(arrayVariable, this.ClientID));
            initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateOverlayCustomMarkerLocationMethodBody(gMapVariable, arrayVariable, arrayVarRecentlyOpened, arrayVarComingSoon, this.ClientID, mapPageType, arrayVarPerNightRate, arrayVarPerStayRate));

            initFunction.AppendFormat("function CreateOverlays{0}()", this.ClientID);
            initFunction.AppendLine("{");

            initFunction.Append(lineOverlay.ToString());
            if (mapComplexity == GMapComplexity.COMPLEX)
            {
                initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateCreateOverlaysMethodBody(gMapVariable, this.ClientID));
            }

            if (mapPageType == MapPageType.OLD)
            {
                initFunction.Append(pointOverlay.ToString());

                initFunction.Append("var mgrOptions = {borderPadding: 10};\n");
                initFunction.AppendFormat(@"
                        markerManager = new MarkerManager({0}, mgrOptions);
                        markerManager.addMarkers({1}, 0);
                        markerManager.refresh();",
                gMapVariable, arrayVariable);
            }
            else if (mapPageType == MapPageType.SELECTHOTEL || mapPageType == MapPageType.CONFIRMATION)
            {
                initFunction.Append(pointOverlay.ToString());

                initFunction.AppendFormat(@"
                        for (var i = 0; i < {1}.length; i++)
                        {{
                            {0}.addOverlay({1}[i]);
                        }}",
                gMapVariable, arrayVariable);
            }
            else if (mapPageType == MapPageType.FINDAHOTEL || mapPageType == MapPageType.EXPANSION)
            {
                initFunction.AppendFormat(@"
                        var mgrOptions = {{borderPadding: 10}};
                        markerManager = new MarkerManager({0}, mgrOptions);
                        recentlyOpenedHotels = new MarkerManager({0}, mgrOptions);
                        comingSoonHotels = new MarkerManager({0}, mgrOptions);
                    ",
                gMapVariable);

                if (otherHotels == true)
                {
                    initFunction.AppendFormat(@"
                        markerManager.addMarkers({0}, 0);
                        markerManager.refresh();
                    ", arrayVariable);
                }
                if (recentlyOpenedHotels == true)
                {
                    initFunction.AppendFormat(@"
                        recentlyOpenedHotels.addMarkers({0}, 0);
                        recentlyOpenedHotels.refresh();
                    ", arrayVarRecentlyOpened);
                }
                if (comingSoonHotels == true)
                {
                    initFunction.AppendFormat(@"
                        comingSoonHotels.addMarkers({0}, 0);
                        comingSoonHotels.refresh();
                    ", arrayVarComingSoon);
                }

                // Enable the filter checkboxes as now we have markers on the map
                // and we can perform marker based operations on the map.
                initFunction.Append(@"
                        var recentlyOpened = document.getElementById(""RecentlyOpened"");
                        recentlyOpened.disabled = false;
                        var comingSoon = document.getElementById(""ComingSoon"");
                        comingSoon.disabled = false;
                        var others = document.getElementById(""Others"");
                        others.disabled = false;
                ");
            }

            //                if (mapPageType == MapPageType.SELECTHOTEL)
            //                {
            //                    // Enable the Per night/Per stay radio group as the markers are already been rendered 
            //                    // in the map. This is required only in select hotel page
            //                    initFunction.Append(@"
            //                        if(parent.isSelectHotelTabGoogleMapReloaded === true)
            //                        {
            //                            RemoveMyOverlay();
            //                            parent.isSelectHotelTabGoogleMapReloaded = false;
            //                            var rdoPerNight = this.parent.document.getElementById(""rdoPerNight"");
            //                            rdoPerNight.disabled = false;
            //                            var rdoPerStay = this.parent.document.getElementById(""rdoPerStay"");
            //                            rdoPerStay.disabled = false;
            //                        }
            //                    ");
            //                }

            // Load indicator is required only in FAH page.
            if (mapPageType == MapPageType.FINDAHOTEL || mapPageType == MapPageType.CONFIRMATION)
            {
                // Remove the load indicator
                initFunction.Append("RemoveMyOverlay();");
            }

#if FINDAHOTEL_PERFORMANCE
            initFunction.AppendFormat(@"
                                        var end = new Date();
                                        var diff = new Date();
                                        diff.setTime(Math.abs(start.getTime() - end.getTime()));
                                        timediff = diff.getTime();
                                        difference = timediff + "" milliseconds"";
                                        ");
            initFunction.AppendFormat("window.status = difference;");
#endif // #if FINDAHOTEL_PERFORMANCE

            initFunction.AppendLine("}");

            //Render the function only if needed.This reduces the client side Javascript rendered
            if (lineOverlay.Length > 0)
            {
                initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateOverlayLineMethodBody(arrayVariable, this.ClientID));
            }

            initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateMakeCopyrightSmallerMethodBody(gMapVariable));

            //initFunction.AppendFormat("{0}.getContainer().style.overflow=\"hidden\";\n", gMapVariable);
            //initFunction.AppendFormat("alert({0}.getContainer().innerHTML)\n", gMapVariable);

            if (mapPageType == MapPageType.FINDAHOTEL || mapPageType == MapPageType.EXPANSION)
            {
                initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateFilterSwitchMethodBody(gMapVariable, arrayVariable, arrayVarRecentlyOpened, arrayVarComingSoon));
            }

            if (mapPageType == MapPageType.EXPANSION)
            {
                initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateResetMapMethodBody(gMapVariable, recentlyOpenedHotels, comingSoonHotels, otherHotels));
            }

            if (mapPageType == MapPageType.SELECTHOTEL)
            {
                initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateSwitchUnitTextMethodBody(gMapVariable, arrayVariable, arrayVarPerNightRate, arrayVarPerStayRate));
            }

            initFunction.Append("</script>\n");
            //initFunction.Append("<form>"); // This line has been commented on FAH development.
#if FINDAHOTEL_PERFORMANCE
            long endTime = DateTime.Now.Ticks;
            TimeSpan tp = new TimeSpan(endTime - startTime);
            AppLogger.LogInfoMessage("InitializeMap() running time  ->  Minutes:" + tp.Minutes + " Seconds:" + tp.Seconds + " Milliseconds:" + tp.Milliseconds);
#endif // #if FINDAHOTEL_PERFORMANCE

            string strInitFunction = initFunction.ToString();
            //#if !DEBUG
            //            JSCompressor jsCompressor = new JSCompressor(true, false);
            //            strInitFunction = jsCompressor.Compress(strInitFunction);
            //#endif // #if !DEBUG
            return strInitFunction;
        }

        #endregion // Private Methods

        #region IMap Members Implementation
        /// <summary>
        /// Method to Center and Zoom the map at a particular point
        /// </summary>
        /// <param name="curPoint">Point at which the map should be centered</param>
        /// <param name="zoomLevel">zoom level</param>
        public void CenterAndZoom(MapPoint curPoint, int zoomLevel)
        {
            zoomAndCenter = string.Format(ci, "ZoomIn{3}({0},{1},{2});", curPoint.Longitude, curPoint.Latitude, zoomLevel, this.ClientID);
        }

        /// <summary>
        /// Method to auto-center and zoom the map at a particular point from objects in datasource
        /// </summary>
        public void AutoCenterAndZoom()
        {
            StringBuilder mapUnitsBoundsString = new StringBuilder();
            IEnumerable mapUnitList = HelperDataResolver.GetResolvedDataSource(DataSource);
            if (mapUnitList != null)
            {
                foreach (object ditem in mapUnitList)
                {
                    try
                    {
                        string sText = string.Empty;
                        double lat = 0;
                        double lng = 0;

                        GetLatitudeLongitude(ditem, out lat, out lng);

                        mapUnitsBoundsString.AppendFormat("locBounds.extend(new google.maps.LatLng({0}, {1}));\n", lat.ToString().Replace(',', '.'), lng.ToString().Replace(',', '.'));
                    }
                    catch (Exception AutoCenterAndZoomEx) { throw new Exception(AutoCenterAndZoomEx.ToString()); }
                }
            }

            zoomAndCenter = mapUnitsBoundsString.ToString() + string.Format(ci, "ZoomIn{0}(locBounds.getCenter().lng(),locBounds.getCenter().lat(), map{0}.getPosition(locBounds));", this.ClientID);
        }
        #endregion // IMap Members Implementation
    }
}
